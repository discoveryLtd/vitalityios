//
//  SAVOnBoardingTests.swift
//  VitalityActive
//
//  Created by Michelle Oratil on 21/11/2017.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import XCTest
import VitalityKit
@testable import XCTest_Gherkin
@testable import VIACore

class SAVOnBoardingTests: XCTestCase{
    func scenario(_ scenario: String) {
        NativeRunner(featureFile: "screeningsandvaccinations.feature", testCase: self).run(scenario: scenario)
    }
//    func testSAVOnBoardingFirstTime(){
//        
//        scenario("I can view SAV On Boarding screen")
//        
//    }
}
