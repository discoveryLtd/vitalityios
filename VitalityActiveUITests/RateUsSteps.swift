//
//  RateUsSteps.swift
//  VitalityActive
//
//  Created by Dexter Anthony Ambrad on 12/19/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import XCTest
import VitalityKit
@testable import XCTest_Gherkin
@testable import VIACore

class RateUsSteps: StepDefiner {
    
    override func defineSteps() {

        step("Rate us row is present") {
            let app = XCUIApplication()
            let tableRow = app.tables.staticTexts[ProfileStrings.SettingsRateTitle]
            self.test.waitFor10Secs(element: tableRow)
            XCTAssertTrue(tableRow.exists, "Rate us row is present")
        }
    }
}
