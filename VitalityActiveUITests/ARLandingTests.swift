//
//  ARLandingTests.swift
//  VitalityActive
//
//  Created by Wilmar van Heerden on 2016/10/25.
//  Copyright © 2016 Glucode. All rights reserved.
//

import XCTest

class ARLandingTests: XCTestCase {

    private var app: XCUIApplication = XCUIApplication()

    override func setUp() {
        super.setUp()
        self.app.launch()
    }

    override func tearDown() {
        super.tearDown()
    }

//    func testAROnboardingFlowIsConfiguredCorrectlyWhenAgreeingToTermsAndConditions() {
//        self.navigateToAROnboardingFromLogin(self.app)
//        self.app.tables.buttons["active_rewards_onboarding_common_main_button_title"].tap()
//        self.waitForTermsConditionsToolBarAndTapAgree(self.app)
//        self.app.buttons["Skip device linking"].tap()
//        self.app.buttons["generic_got_it_button_title"].tap()
//        XCTAssert(self.app.navigationBars["active_rewards_home_view_title"].staticTexts["active_rewards_home_view_title"].exists)
//        XCTAssert(self.app.staticTexts["active_rewards_landing_activity_cell_title"].exists)
//        XCTAssert(self.app.staticTexts["active_rewards_landing_rewards_cell_title"].exists)
//        XCTAssert(self.app.staticTexts["active_rewards_landing_learnmore_cell_title"].exists)
//        XCTAssert(self.app.staticTexts["active_rewards_landing_help_cell_title"].exists)
//    }
}
