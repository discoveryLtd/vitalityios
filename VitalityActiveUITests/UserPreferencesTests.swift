//
//  userpreferences.swift
//  VitalityActive
//
//  Created by Sebastian Cichon on 2017/04/04.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import XCTest
import VitalityKit
@testable import XCTest_Gherkin
@testable import VIACore

class UserPreferencesTests: XCTestCase {
    
    func scenario(_ scenario: String) {
        NativeRunner(featureFile: "userpreferences.feature", testCase: self).run(scenario: scenario)
    }
    
    func testPreferenceScreenShownAfterFirstLogin() {
        scenario("The user is shown the preferences screen after registering and logging in for the first time after accepting T&C")
    }
    
//    func testNotDisplayingUserPreferencesAfterAcceptingTerms() {
//        scenario("The user is not shown the preferences screen after he/she has already accepted T&C")
//    }
    
    func testCommunicationPreferencesHeaderAndText() {
        scenario("Communication Preferences are listed under a heading ‘Communication Preferences’, with some explanatory text")
    }
    
    func testEmailOptionHeaderAndSwitch() {
        scenario("User can view the Email option header")
    }
    
    //    func testEmailChoiceCachedForUser() {
    //        scenario("The email option choice is cached for the user")
    //    }
    
    func testPushNotificationsHeaderandSwitch() {
        scenario("User can view the Push Notifications option header and that the button is disabled by default.")
    }
    
//    func testTappingNextAndSavingSettings() {
//        scenario("The user can click ‘Next’ which saves these preferences and navigates the user to the Home screen.")
//    }
    
    func testPrivacySectionWithHeader() {
        scenario("Check that the user is able to view Privacy section under Privacy heading")
    }
    
    func testPrivacyExplanatoryText() {
        scenario("Check that the user is able to view explanatory text under the Privacy heading")
    }
    
    func testViewPrivacyStatementLink() {
        scenario("View Privacy Statement link")
    }
    
//    func testTapOnPrivacyStatementLink() {
//        scenario("Tap on the Privacy Statement link")
//    }
    
    func testAnalyticsSectionWithHeader() {
        scenario("Check that the user is able to see the Analytics header under Privacy")
    }
    
    func testAnalyticsExplanatoryText() {
        scenario("Check that the user is able to see the explanatory text under Analytics")
    }
    
    func testAnalyticsToggleEnabled() {
        scenario("Check that Analytics is enabled by default")
    }
    
    func testTappingOnAnalyticsSwitch() {
        scenario("Check that the user is able to toggle the Analytics button to disabled")
        
    }
    
    func testCrashReportHeader() {
        scenario("Check the the user is able to see the Crash Reports header under Privacy")
    }
    
    func testCrashReportExplanatoryText() {
        scenario("Check that the user is able to see the explanatory text under Crash Reports")
    }
    
    func testCrashReportToggleEnabled() {
        scenario("Check that Crash Reports is enabled by default")
    }
    
    //    func testTappingOnCrashReportSwitch() {
    //        scenario("Check that the user is able to toggle the Crash Reports button to disabled")
    //    }
    
    func testSecurityHeading() {
        scenario("Check that the user is able to view Security section under Security heading")
    }
    
    func testUserPreferencesFooter() {
        scenario("Check if I see the 'manage privacy and security settings' footer")
    }
    
    func testSecurityExplanatoryText() {
        scenario("Check that the user is able to view explanatory text under the Security heading")
    }
    
    func testRememberMeHeading() {
        scenario("Check that the user is able to see the Remember Me header under Privacy")
    }
    
    func testRememberMeExplanatoryText() {
        scenario("Check that the user is able to see the explanatory text under Remember Me")
    }
    
    //    func testRememberMeToggleEnabled() {
    //        scenario("Check that Remember Me is enabled by default")
    //    }
    //
    //    func testTappingOnRememberMeSwitch() {
    //        scenario("Check that the user is able to toggle the Remember Me button to disabled")
    //    }
    //
    //    func testPermissionAlertDisplayed() {
    //        scenario("Check that an alert appears if all user preferences are disabled")
    //    }
    //
    //    func testCancelOnPermissionAlert() {
    //        scenario("Check that the user is able to Cancel the alert and toggle preferences")
    //    }
    //
    //    func testContinueOnPermissionAlert() {
    //        scenario("Check that the user is able to Continue with no preferences enabled")
    //    }
    
//    func testUserNavigatedToHomeAfterNext() {
//        scenario("Check that the user is navigated to the home screen after selecting Next")
//    }
}
