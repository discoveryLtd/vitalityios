//
//  VitalityHelpers.swift
//  VitalityActive
//
//  Created by Sebastian Cichon on 2017/02/13.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import XCTest

extension XCTestCase {
    public class VitalityHelper {
        
        static func swipefromBottomToTop(swipeDistance: CGFloat, elementToSwipe: XCUIElement) {
            let bottom = elementToSwipe.coordinate(withNormalizedOffset: CGVector(dx: 0.5, dy: 0.9))
            let top = elementToSwipe.coordinate(withNormalizedOffset: CGVector(dx: 0.5, dy: 0.1))
            bottom.press(forDuration: 0.1, thenDragTo: top)
        }
        
        static func controlledSwipeDown(swipeDistance: CGFloat, elementToSwipe: XCUIElement) {
            let start = elementToSwipe.coordinate(withNormalizedOffset: CGVector(dx: 0, dy: 0))
            let finnish = elementToSwipe.coordinate(withNormalizedOffset: CGVector(dx: 0, dy: swipeDistance))
            start.press(forDuration: 0, thenDragTo: finnish)
        }
        
        static func scrollTo(element: XCUIElement) {
            while isVisible(element: element) == false {
                let startCoordinates = XCUIApplication().tables.element.coordinate(withNormalizedOffset: CGVector(dx: 0.5, dy: 0.5))
                let endCoordinates = startCoordinates.withOffset(CGVector(dx: 0.0, dy: -262))
                startCoordinates.press(forDuration: 0.01, thenDragTo: endCoordinates)
            }
        }
        
        static func customScroll(count: NSInteger){
        let relativeTouchPoint = XCUIApplication().coordinate(withNormalizedOffset: CGVector(dx: 0.5, dy: 0.5))
        let relativeOffset = XCUIApplication().coordinate(withNormalizedOffset: CGVector(dx: 0, dy: -1))
            
            for _ in 1...count
            {
                relativeTouchPoint.press(forDuration: 0, thenDragTo: relativeOffset)
            }
        }
        
//        // TO DO: Dynamic Card Scrolling
//        static func scrollToCard(count: NSInteger){
//            XCUIApplication().children(matching: .window).element(boundBy: 0).children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .collectionView).element.children(matching: .cell).element(boundBy: 0).collectionViews.children(matching: .cell).element(boundBy: 0).children(matching: .other).element.children(matching: .other).element.children(matching: .other).element(boundBy: 1).swipeLeft()
//            
//            sleep(2)
//            
//            XCUIApplication().children(matching: .window).element(boundBy: 0).children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .collectionView).element.children(matching: .cell).element(boundBy: 0).collectionViews.children(matching: .cell).element(boundBy: 1).children(matching: .other).element.children(matching: .other).element.children(matching: .other).element(boundBy: 1).swipeLeft()
//            
//            sleep(2)
//            
//            XCUIApplication().children(matching: .window).element(boundBy: 0).children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .collectionView).element.children(matching: .cell).element(boundBy: 0).collectionViews.children(matching: .cell).element(boundBy: 1).children(matching: .other).element.children(matching: .other).element.children(matching: .other).element(boundBy: 1).swipeLeft()
//        }
    
        // Scrolling in the "KNOW YOUR HEALTH" section
        
        static func scrollLeftTo(element: XCUIElement) {
            while isVisible(element: element) == false {
                let startCoordinates = XCUIApplication().coordinate(withNormalizedOffset: CGVector(dx: 0.5, dy: 0.3))
                let endCoordinates = startCoordinates.withOffset(CGVector(dx: 0.4, dy: 0.3))
                startCoordinates.press(forDuration: 0.05, thenDragTo: endCoordinates)
            }
        }
        
        static func scrollLeftToMWB(element: XCUIElement) {
//            while isVisible(element: element) == false {
//
//            }
            let startCoordinates = XCUIApplication().coordinate(withNormalizedOffset: CGVector(dx: 0.5, dy: 0.3))
            let endCoordinates = startCoordinates.withOffset(CGVector(dx: 0.25, dy: 0.3))
            startCoordinates.press(forDuration: 0.05, thenDragTo: endCoordinates)
        }
        
        //
        
        static func isVisible(element: XCUIElement) -> Bool {
            guard element.exists && element.isHittable && !element.frame.isEmpty else {
                return false
            }
            
            return XCUIApplication().tables.element(boundBy: 0).frame.contains(element.frame)
        }
        
        static func navigateBackTo(screenTitle: String) {
            var screenReached: Bool = XCUIApplication().navigationBars[screenTitle].exists
            let app = XCUIApplication()
            let logoutSwipe = XCUIApplication().staticTexts.matching(identifier: "Life").element(boundBy: 0)
            let cancelButton = XCUIApplication().buttons["Cancel"]
            let icnMenuButton = XCUIApplication().buttons["icn menu"]
            var loading = XCUIApplication().staticTexts["Loading..."].exists
            
            while (!screenReached) {
                if loading == true {
                    loading = XCUIApplication().staticTexts["Loading..."].exists
                } else {
                    if(cancelButton.exists) {
                        cancelButton.tap()
                    } else {
                        if(icnMenuButton.exists) {
                            icnMenuButton.tap()
                            if (screenTitle == "Log in") {
                                controlledSwipeDown(swipeDistance: 55, elementToSwipe: logoutSwipe)
                                app.staticTexts["Log out"].tap()
                                app.buttons["Yes"].tap()
                                return
                            } else {
                                app.staticTexts[screenTitle].tap()
                            }
                        } else {
                            app.buttons.matching(identifier: "Back").element(boundBy: 0).tap()
                        }
                    }
                    let tempApp: XCUIApplication = XCUIApplication()
                    screenReached = tempApp.navigationBars.staticTexts[screenTitle].exists
                }
            }
        }
        
        static func waitForElementToAppear(test: XCTestCase, element: XCUIElement, file: String = #file, line: UInt = #line) {
            let existsPredicate = NSPredicate(format: "exists == true")
            test.expectation(for: existsPredicate, evaluatedWith: element, handler: nil)
            test.waitForExpectations(timeout: 20) {
                (error) -> Void in
                if (error != nil) {
                    let message = "Failed to find \(element) after 5 seconds."
                    test.recordFailure(withDescription: message, inFile: file, atLine: line, expected: true)
                }
            }
        }
        
        static func launchAppAndNavigateToLogin(test: XCTestCase) {
            XCUIApplication().launch()
            if ((XCUIApplication().buttons["Log in"].exists) == false) {
                let emergencyDetailsScreen = XCUIApplication().navigationBars.staticTexts["Emergency details"]
                waitForElementToAppear(test: test, element: emergencyDetailsScreen)
                navigateBackTo(screenTitle: "Log in")
            }
        }
    }
}
