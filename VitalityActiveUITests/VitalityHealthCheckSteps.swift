//
//  VHCOnboardingSteps.swift
//  VitalityActive
//
//  Created by Sebastian Cichon on 2017/03/29.
//  Copyright © 2017 Glucode. All rights reserved.
//

import XCTest
@testable import VIACore
@testable import VitalityKit
@testable import XCTest_Gherkin
import Foundation


class VitalityHealthCheckSteps: StepDefiner {
    
    override func defineSteps() {
        
        step("I tap on VHC card") {
            let collectionViewsQuery = XCUIApplication().collectionViews
            
            let vhrCard = collectionViewsQuery.collectionViews.staticTexts["home_card.card_section_title_291"]
            self.test.waitFor10Secs(element: vhrCard)
            vhrCard.swipeLeft()
            let vnaCard = collectionViewsQuery.collectionViews.staticTexts["home_card.card_section_title_388"]
            self.test.waitFor10Secs(element: vnaCard)
            vnaCard.swipeLeft()
            let mwbCard = collectionViewsQuery.collectionViews.staticTexts["onboarding.title_1195"]
            self.test.waitFor10Secs(element: mwbCard)
            mwbCard.swipeLeft()
            let healthPartnersCard = collectionViewsQuery.collectionViews.staticTexts["card_heading_health_partners_843"]
            self.test.waitFor10Secs(element: healthPartnersCard)
            healthPartnersCard.swipeLeft()
            let vhcCard = collectionViewsQuery.collectionViews.staticTexts["home_card.card_title_125"]
            self.test.waitFor10Secs(element: vhcCard)
            vhcCard.tap()
        }
        
        step("I tap on VHC card in IGI") {
            let collectionViewsQuery = XCUIApplication().collectionViews
            
            let vhrCard = collectionViewsQuery.collectionViews.staticTexts["home_card.card_section_title_291"]
            self.test.waitFor10Secs(element: vhrCard)
            vhrCard.swipeLeft()
            let vnaCard = collectionViewsQuery.collectionViews.staticTexts["home_card.card_section_title_388"]
            self.test.waitFor10Secs(element: vnaCard)
            vnaCard.swipeLeft()
            let vhcCard = collectionViewsQuery.collectionViews.staticTexts["home_card.card_title_125"]
            self.test.waitFor10Secs(element: vhcCard)
            vhcCard.tap()
        }
        
        step("I see VHC onboarding modal") {
            let tablesQuery = XCUIApplication().tables
            let button = tablesQuery.buttons["generic_got_it_button_title_131"]
            sleep(5)
            XCTAssert(button.exists)
        }
        
        step("I tap on back button") {
            let app = XCUIApplication()
            let backButton = app.navigationBars.buttons["Back"]
            self.test.waitFor10Secs(element: backButton)
            backButton.tap()
        }
        
        step("I tap the Got It button") {
            let app = XCUIApplication()
            let gotItButton = app.buttons[CommonStrings.GenericGotItButtonTitle131]
            self.test.waitFor10Secs(element: gotItButton)
            gotItButton.tap()
        }
        
        step("I will be navigated to VHC landing screen") {
            let tablesQuery = XCUIApplication().tables
            let landingScreen = tablesQuery.staticTexts["home_card.card_title_125"]
            XCTestCase.VitalityHelper.waitForElementToAppear(test: self.test, element: landingScreen)
            let failureReason = "VHC landing screen not displayed"
            XCTAssertTrue(landingScreen.exists, failureReason)
        }
        
        step("I will see the Learn More button") {
            let app = XCUIApplication()
            let learnMoreButton = app.buttons[CommonStrings.LearnMoreButtonTitle104]
            self.test.waitFor10Secs(element: learnMoreButton)
            XCTAssert(learnMoreButton.exists)
        }
        
        step("I tap on VHC learn more button") {
            let app = XCUIApplication()
            let learnMoreButton = app.cells.staticTexts[CommonStrings.LearnMoreButtonTitle104]
            sleep(15)
            XCTestCase.VitalityHelper.scrollTo(element: learnMoreButton)
            learnMoreButton.tap()
        }
        
        step("I tap on Body Mass Index") {
            let app = XCUIApplication()
            let BMIbutton = app.otherElements.tables.cells.staticTexts[CommonStrings.Measurement.BodyMassIndexTitle134]
            if (BMIbutton.exists) {
                BMIbutton.tap()
            }
        }
        
        step("I tap on Waist Circumference") {
            let app = XCUIApplication()
            let circumferenceButton = app.otherElements.tables.cells.staticTexts[CommonStrings.Measurement.WaistCircumferenceTitle135]
            if (circumferenceButton.exists) {
                circumferenceButton.tap()
            }
        }
        
        step("I tap on Blood Glucose") {
            let app = XCUIApplication()
            let glucoseButton = app.otherElements.tables.cells.staticTexts[CommonStrings.Measurement.GlucoseTitle136]
            if (glucoseButton.exists) {
                glucoseButton.tap()
            }
        }
        
        step("I tap on Blood Pressure") {
            let app = XCUIApplication()
            let BPbutton = app.otherElements.tables.cells.staticTexts[CommonStrings.Measurement.BloodPressureTitle137]
            if (BPbutton.exists) {
                BPbutton.tap()
            }
        }
        
        step("I tap on Cholesterol") {
            let app = XCUIApplication()
            let cholesterolButton = app.otherElements.tables.cells.staticTexts[CommonStrings.Measurement.CholesterolTitle138]
            if (cholesterolButton.exists) {
                cholesterolButton.tap()
            }
        }
        
        step("I tap on HbA1c") {
            
            let app = XCUIApplication()
            
            if (app.tables.staticTexts[CommonStrings.UserPrefs.CommunicationGroupHeaderMessage65].exists) {
                let hba1cButton = app.otherElements.tables.cells.staticTexts[CommonStrings.Measurement.Hba1cTitle139]
                XCTestCase.VitalityHelper.scrollTo(element: hba1cButton)
                hba1cButton.tap()
            }
//            let hba1cButton = app.otherElements.tables.cells.staticTexts[VhcStrings.Measurement.Hba1cTitle139]
//            if (hba1cButton.exists) {
//                hba1cButton.tap()
//            }
        }
        
        step("I will read more about Body Mass Index") {
            let BMIscreen = XCUIApplication().navigationBars.staticTexts[CommonStrings.Measurement.BodyMassIndexTitle134]
            if (BMIscreen.exists) {
                let failureReason = "BMI screen is not displayed"
                XCTestCase.VitalityHelper.waitForElementToAppear(test: self.test, element: BMIscreen)
                XCTAssertTrue(BMIscreen.exists, failureReason)
            }
        }
        
        step("I will read more about Waist Circumference") {
            let circumferenceScreen = XCUIApplication().navigationBars.staticTexts["Waist Circumference"]
            if (circumferenceScreen.exists) {
                let failureReason = "Waist Circumference screen is not displayed"
                XCTestCase.VitalityHelper.waitForElementToAppear(test: self.test, element: circumferenceScreen)
                XCTAssertTrue(circumferenceScreen.exists, failureReason)
            }
        }
        
        step("I will read more about Blood Glucose") {
            let glucoseScreen = XCUIApplication().navigationBars.staticTexts["Glucose"]
            if (glucoseScreen.exists) {
                let failureReason = "Blood Glucose screen is not displayed"
                XCTestCase.VitalityHelper.waitForElementToAppear(test: self.test, element: glucoseScreen)
                XCTAssertTrue(glucoseScreen.exists, failureReason)
            }
        }
        
        step("I will read more about Blood Pressure") {
            let BPscreen = XCUIApplication().navigationBars.staticTexts["Blood Pressure"]
            if (BPscreen.exists) {
                let failureReason = "Blood Pressure screen is not displayed"
                XCTestCase.VitalityHelper.waitForElementToAppear(test: self.test, element: BPscreen)
                XCTAssertTrue(BPscreen.exists, failureReason)
            }
        }
        
        step("I will read more about Cholesterol") {
            let cholesterolScreen = XCUIApplication().navigationBars.staticTexts["Cholesterol"]
            if (cholesterolScreen.exists) {
                let failureReason = "Cholesterol screen is not displayed"
                XCTestCase.VitalityHelper.waitForElementToAppear(test: self.test, element: cholesterolScreen)
                XCTAssertTrue(cholesterolScreen.exists, failureReason)
            }
        }
        
        step("I will read more about HbA1c") {
            let hba1cScreen = XCUIApplication().navigationBars.staticTexts["HbA1c"]
            if (hba1cScreen.exists) {
                let failureReason = "HbA1c screen is not displayed"
                XCTestCase.VitalityHelper.waitForElementToAppear(test: self.test, element: hba1cScreen)
                XCTAssertTrue(hba1cScreen.exists, failureReason)
            }
        }
        
        step("I tap on one of the metrics") {
            let app = XCUIApplication()
            let button = app.tables.staticTexts["measurement.body_mass_index_title_134"]
            button.tap()
        }
        
        step("I will see meassurements greyed out") {
            let meassurementsTitle = XCUIApplication().otherElements.staticTexts[CommonStrings.LandingScreen.HealthMeasurementsTitle132]
            let failureReason = "Health Measurements screen is not displayed"
            sleep(8)
            XCTestCase.VitalityHelper.waitForElementToAppear(test: self.test, element: meassurementsTitle)
            XCTAssertTrue(meassurementsTitle.exists, failureReason)
            
        }
        
        step("I see measurements") {
            let meassurementsTitle = XCUIApplication().otherElements.staticTexts[CommonStrings.LandingScreen.HealthMeasurementsTitle132]
            let failureReason = "Health Measurements screen is not displayed"
            sleep(10)
            XCTestCase.VitalityHelper.waitForElementToAppear(test: self.test, element: meassurementsTitle)
            XCTAssertTrue(meassurementsTitle.exists, failureReason)
            
        }
        
        step("I will see more info on points") {
            let pointInfo = XCUIApplication().tables.cells.containing(.staticText, identifier:"measurement.body_mass_index_title_134").staticTexts["range.in_healthy_title_190"]
            let failureReason = "Point Information is not displayed"
            XCTestCase.VitalityHelper.waitForElementToAppear(test: self.test, element: pointInfo)
            XCTAssertTrue(pointInfo.exists, failureReason)
        }
        
        step("I tap on Capture Results") {
            let app = XCUIApplication()
            let captureResultsButton = app.otherElements.cells.buttons[CommonStrings.Onboarding.Section2Title128]
            sleep(15)
            captureResultsButton.tap()
        }
        
        step("I am on the Capture Results screen") {
            let captureLanding = XCUIApplication().otherElements.staticTexts[CommonStrings.CaptureResults.Intro1Title142]
            let failureReason = "Capture Results screen is not displayed"
            XCTestCase.VitalityHelper.waitForElementToAppear(test: self.test, element: captureLanding)
            XCTAssertTrue(captureLanding.exists, failureReason)
        }
        
        step("I am on the VHC screen") {
            let landingScreen =   XCUIApplication().tables.cells.staticTexts["landing_screen.health_measurements_title_132"]
            let failureReason = "Landing Screen is not displayed"
            XCTestCase.VitalityHelper.waitForElementToAppear(test: self.test, element: landingScreen)
            XCTAssertTrue(landingScreen.exists, failureReason)
        }
        
        step("I will see out of healthy range message and logo") {
            let outofhealthyRange = XCUIApplication().cells.staticTexts[CommonStrings.Range.OutOfHealthyTitle191]
            let failureReason = "Out of Healthy Range message is not displayed"
            XCTestCase.VitalityHelper.waitForElementToAppear(test: self.test, element: outofhealthyRange)
            XCTAssertTrue(outofhealthyRange.exists, failureReason)
        }
        
        step("I will see in healthy range message and logo") {
            let inHealthyrange = XCUIApplication().cells.staticTexts[CommonStrings.Range.InHealthyTitle190]
            let failureReason = "In Healthy Range message is not displayed"
            XCTestCase.VitalityHelper.waitForElementToAppear(test: self.test, element: inHealthyrange)
            XCTAssertTrue(inHealthyrange.exists, failureReason)
        }
        
        step("I tap on a meassurement") {
            let app = XCUIApplication()
 
            let measurementButton = app.tables.staticTexts["measurement.blood_pressure_title_137"]
            XCTestCase.VitalityHelper.scrollTo(element: measurementButton)
            
           sleep(3)
//            self.test.waitFor10Secs(element: measurementButton)
            measurementButton.tap()
        }
        
        step("I will see more details screen") {
            let app = XCUIApplication()
            let moreDetails = app.tables.staticTexts["measurement.blood_pressure_title_137"]
            let failureReason = "Detailed Measurement screen is not displayed"
            XCTestCase.VitalityHelper.waitForElementToAppear(test: self.test, element: moreDetails)
            XCTAssertTrue(moreDetails.exists, failureReason)
        }
        
        step("I will see points for meassurements") {
            sleep(999999)
            let measurementPoints = XCUIApplication().cells.staticTexts[CommonStrings.HomeCard.PointsEarnedMessage252("2250", "2250")]
            let failureReason = "Measurement points are not displayed"
            XCTestCase.VitalityHelper.waitForElementToAppear(test: self.test, element: measurementPoints)
            XCTAssertTrue(measurementPoints.exists, failureReason)
        }
        
        step("I tap on History") {
            let app = XCUIApplication()
            let historyButton = app.buttons[CommonStrings.HistoryButton140]
            self.test.waitFor10Secs(element: historyButton)
            historyButton.tap()
        }
        
        step("I tap on Help") {
            let app = XCUIApplication()
            let helpButton = app.cells.staticTexts[CommonStrings.LearnMoreButtonTitle104]
            sleep(5)
            XCTestCase.VitalityHelper.scrollTo(element: helpButton)
            helpButton.tap()
        }
        
        step("I tap VHC Learn More") {
            let app = XCUIApplication()
            let learnMore = app.buttons[CommonStrings.LearnMoreButtonTitle104]
            sleep(3)
            learnMore.tap()
        }
        
        step("The VHC learn more will show") {
            let learnMoreLanding = XCUIApplication().navigationBars[CommonStrings.LearnMoreButtonTitle104]
            let failureReason = "Learn More Screen is not displayed"
            XCTestCase.VitalityHelper.waitForElementToAppear(test: self.test, element: learnMoreLanding)
            XCTAssertTrue(learnMoreLanding.exists, failureReason)
        }
        
        step("I enter Systolic") {
            let app = XCUIApplication()
            let collectionViewsQuery = app.collectionViews

            XCTestCase.VitalityHelper.customScroll(count: 3)
            let systolicCell = collectionViewsQuery.textFields["measurement.systolic_title_157"]
            systolicCell.tap()
            systolicCell.typeText("130")

        }
        
        step("I enter Diastolic") {
            let app = XCUIApplication()
            let collectionViewsQuery = app.collectionViews
            let diastolicCell = collectionViewsQuery.textFields["measurement.diastolic_title_156"]
            let systolicCell = collectionViewsQuery.textFields["measurement.systolic_title_157"]
            diastolicCell.tap()
            diastolicCell.typeText("95")
            systolicCell.tap()
        }
        
        step("I enter a value out of range") {
            let app = XCUIApplication()
            let collectionViewsQuery = app.collectionViews
            
            XCTestCase.VitalityHelper.customScroll(count: 3)
            let systolicCell = collectionViewsQuery.textFields["measurement.systolic_title_157"]
            let diastolicCell = collectionViewsQuery.textFields["measurement.diastolic_title_156"]
            
            systolicCell.tap()
            systolicCell.typeText("6000")
            diastolicCell.tap()
        }
        
        step("I correct the value") {
            let app = XCUIApplication()
            let collectionViewsQuery = app.collectionViews
            let systolicCell = collectionViewsQuery.textFields["measurement.systolic_title_157"]
            let diastolicCell = collectionViewsQuery.textFields["measurement.diastolic_title_156"]
            
            XCTestCase.VitalityHelper.customScroll(count: 3)
            
            let outRangeText = "600"
            var text = systolicCell
            if (!text.exists) {
                text = app.otherElements.textFields[outRangeText]
            }
            
            text.clearAndEnterText(text: "140")
            
            diastolicCell.tap()
        }
        
        step("I tap on Next") {
            let app = XCUIApplication()
            let nextButton = app.navigationBars.buttons[CommonStrings.NextButtonTitle84]
            self.test.waitFor10Secs(element: nextButton)
            nextButton.tap()
        }
        
        step("Add proof screen is shown") {
            let app = XCUIApplication()
            let addProofScreen = app.otherElements.navigationBars[CommonStrings.Proof.AddProofScreenTitle163]
            let failureReason = "Add Proof Screen is not displayed"
            XCTAssertTrue(addProofScreen.exists, failureReason)
        }
        
        step("I have not entered a value") {
            let app = XCUIApplication()
            let collectionViewsQuery = app.collectionViews
            
            XCTestCase.VitalityHelper.customScroll(count: 3)
            let systolicCell = collectionViewsQuery.textFields["measurement.systolic_title_157"]
            systolicCell.tap()
        }
        
        step("The Next button is disabled") {
            let nextButton = XCUIApplication().navigationBars.buttons[CommonStrings.NextButtonTitle84]
            let failureReason = "Detailed Measurement screen is not displayed"
            XCTestCase.VitalityHelper.waitForElementToAppear(test: self.test, element: nextButton)
            XCTAssertTrue(nextButton.exists, failureReason)
        }
        
        step("I will see an error message being displayed") {
            let valueError = XCUIApplication().otherElements.staticTexts[CommonStrings.ErrorRange180("30", "200")]
            let failureReason = "Detailed Measurement screen is not displayed"
            XCTestCase.VitalityHelper.waitForElementToAppear(test: self.test, element: valueError)
            XCTAssertTrue(valueError.exists, failureReason)
        }
        
        step("I will not see the error message displayed") {
            let BPtextfield = XCUIApplication().otherElements.textFields[CommonStrings.Measurement.DiastolicTitle156]
            let failureReason = "Blood Pressure text field not visible"
            XCTestCase.VitalityHelper.waitForElementToAppear(test: self.test, element: BPtextfield)
            XCTAssertTrue(BPtextfield.exists, failureReason)
        }
        
        step("I will see the date option") {
            let dateSubmitted = XCUIApplication().otherElements.staticTexts[CommonStrings.CaptureResults.DateTested144]
            let failureReason = "Date tested is not displayed"
            XCTestCase.VitalityHelper.waitForElementToAppear(test: self.test, element: dateSubmitted)
            XCTAssertTrue(dateSubmitted.exists, failureReason)
        }
        
        step("I tap on the current unit of meassure") {
            let app = XCUIApplication()
            let unitMeassureButton = app.otherElements.buttons["kg"]
            self.test.waitFor10Secs(element: unitMeassureButton)
            unitMeassureButton.tap()
        }
        
        step(" am able to change unit of meassure") {
            let app = XCUIApplication()
            let unitMeassureButton = app.otherElements.buttons["lb"]
            self.test.waitFor10Secs(element: unitMeassureButton)
            unitMeassureButton.tap()
        }
        
        step("I enter a weight value") {
            let app = XCUIApplication()
            let diastolicCell = app.otherElements.textFields[CommonStrings.Measurement.DiastolicTitle156]
            let weightCell = app.otherElements.textFields[CommonStrings.Measurement.WeightTitle146]
        
            weightCell.tap()
            weightCell.typeText("90")
            
            XCTestCase.VitalityHelper.customScroll(count: 3)
            diastolicCell.tap()
        }
        
        step("I should see an alert") {
            let alertBMI = XCUIApplication().alerts["capture_results.missing_points_alert_title_1171"].staticTexts["capture_results.missing_points_alert_title_1171"]
            let failureReason = "BMI Alert is not displayed"
            XCTestCase.VitalityHelper.waitForElementToAppear(test: self.test, element: alertBMI)
            XCTAssertTrue(alertBMI.exists, failureReason)
        }
        
        step("I will see the Add button") {
            let app = XCUIApplication()
            let addProofButton = app.otherElements.buttons[CommonStrings.Proof.AddButton166]
            let failureReason = "Add Proof Button is not displayed"
            XCTestCase.VitalityHelper.waitForElementToAppear(test: self.test, element: addProofButton)
            XCTAssertTrue(addProofButton.exists, failureReason)
        }
        
        step("I tap on Add button") {
            let app = XCUIApplication()
            let addProofButton = app.otherElements.buttons[CommonStrings.Proof.AddButton166]
            self.test.waitFor10Secs(element: addProofButton)
            addProofButton.tap()
        }
        
        step("I will see library button") {
            let app = XCUIApplication()
            let libraryButton = app.otherElements.buttons[CommonStrings.Proof.ActionChooseFromLibraryAlertTitle168]
            let failureReason = "Library Button is not displayed"
            XCTestCase.VitalityHelper.waitForElementToAppear(test: self.test, element: libraryButton)
            XCTAssertTrue(libraryButton.exists, failureReason)
        }
        
        step("I will see cancel button") {
            let app = XCUIApplication()
            let cancelButton = app.otherElements.buttons[CommonStrings.CancelButtonTitle24]
            let failureReason = "Cancel Button is not displayed"
            XCTestCase.VitalityHelper.waitForElementToAppear(test: self.test, element: cancelButton)
            XCTAssertTrue(cancelButton.exists, failureReason)
        }
        
        step("I tap on Library button") {
            let app = XCUIApplication()
            let libraryButton = app.otherElements.buttons[CommonStrings.Proof.ActionChooseFromLibraryAlertTitle168]
            libraryButton.tap()
        }
        
        step("I will see the gallery") {
            let app = XCUIApplication()
            let galleryTitle = app.otherElements.navigationBars.staticTexts["Moments"]
            let failureReason = "Gallery Screen is not displayed"
            XCTestCase.VitalityHelper.waitForElementToAppear(test: self.test, element: galleryTitle)
            XCTAssertTrue(galleryTitle.exists, failureReason)
        }
        
        step("I tap gallery Cancel button") {
            let app = XCUIApplication()
            let galleryCancel = app.otherElements.navigationBars.buttons["Cancel"]
            self.test.waitFor10Secs(element: galleryCancel)
            galleryCancel.tap()
        }
        
        step("I select another photo") {
            let app = XCUIApplication()
            let secondPhoto = app.cells.element(boundBy: 2)
            secondPhoto.tap()
        }
        
        step("I select a photo") {
            let app = XCUIApplication()
            let cellCount = app.cells.count - 1
            let singlePhoto = app.cells.element(boundBy: cellCount)
            singlePhoto.tap()
        }
        
        step("I tap add icon") {
            let app = XCUIApplication()
            let cellCount = app.cells.count - 1
            let addIcon = app.cells.element(boundBy: cellCount)
            addIcon.tap()
        }
    }
}
