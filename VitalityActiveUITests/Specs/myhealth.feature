@tag
Feature: My Health

Scenario: My Health Onboarding screen is displayed
Given I have navigated to My Health user home screen
And I tap My Health on Menu
Then I see My Health onboarding modal

Scenario: Learn More is displayed when navigating to my health learn more screen
Given I have navigated to My Health user home screen
And I tap My Health on Menu
And I tap Get Started on onboarding screen
And I tap the Learn More on Vitality Age Card
Then I see Vitality Age learn more screen

Scenario: Unknown Vitality Age is displayed if no assessment has been completed
Given I have navigated to My Health user home screen
And I tap My Health on Menu
And I tap Get Started on onboarding screen
Then I should see Unknown Vitality Age

Scenario: Displays too high on Bad Vitality Age
Given I have logged in a my health user with Bad Vitality Age
And I tap My Health on Menu
And I tap on Get Started on Onboarding screen
And I tap on Vitality Age Card
Then I should see too high on Vitality Age card

Scenario: Displays Looking Good Vitality Age
Given I have logged in a my health user with Good Vitality Age
And I tap My Health on Menu
And I tap on Get Started on Onboarding screen
And I tap on Vitality Age Card
Then I should see Looking good on Vitality Age card

Scenario: Completed VHC but not VHR
Given I have logged in a my health user whose VHC is completed but not VHR
And I tap My Health on Menu
And I tap on Get Started on Onboarding screen
And I tap on Vitality Age Card
Then I should see text that says to complete my VHR

Scenario: Display Result on what you need to improve section
Given I have logged in a my health user with Bad Vitality Age
And I tap My Health on Menu
And I tap on Get Started on Onboarding screen
And I tap on Vitality Age Card
And I tap physical activity under what you need to improve
Then I should see tips to improve text

Scenario: Navigate to Help Screen
Given I have logged in a my health user with Bad Vitality Age
And I tap My Health on Menu
And I tap on Get Started on Onboarding screen
And I tap on Vitality Age Card
And I click the vitality age help button
Then I should be navigated to help screen

Scenario: Display more results on what you need to improve
Given I have logged in a my health user with Bad Vitality Age
And I tap My Health on Menu
And I tap on Get Started on Onboarding screen
And I tap on Vitality Age Card
And I tap on Display More under what you need to improve
Then I should be navigated to more results screen
