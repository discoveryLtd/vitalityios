@tag
Feature: Vitality Health Check

Scenario: I can view the learn more screen from VHC onboarding screen
Given I have navigated to a newly installed app home screen
And I tap on VHC card
And I see VHC onboarding modal
When I tap VHC Learn More
Then The VHC learn more will show

Scenario: I can go back to VHC landing screen from learn more screen
Given I have navigated to a newly installed app home screen
And I tap on VHC card
And I see VHC onboarding modal
And I tap the Got It button
And I will be navigated to VHC landing screen
And I tap VHC Learn More
When I tap on Back
Then I will be navigated to VHC landing screen

Scenario: Check the information shown on the learn more screen is correct and carrier insurers are also listed
Given I am on the VHC onboarding screen
When I tap VHC Learn More
Then I will see the correct information shown

Scenario: I can view the participating partners
Given I am on the Learn More screen
When I tap on Participating Partner
Then I will see the Participating Partner screen

Scenario: I can read more about each participating partner
Given I am on the Learn More screen
And I tap on ‘Participating Partners’ Link
When I tap on a Participating Partner
Then more information about that partner should show

Scenario: I can go back form the participating partners screen to the learn more screen
Given I am on the Participating Partners screen
When I tap on Back
Then I am on the Learn More screen

Scenario: I can go back to the participating screen from the more information screen
Given I am on the Participating Partner information screen
When I tap on Back
Then I am on the Participating Partners screen

Scenario: I can read more about Body Mass Index
Given I have navigated to a newly installed app home screen
And I tap on VHC card
And I see VHC onboarding modal
And I tap the Got It button
And I will be navigated to VHC landing screen
And I tap VHC Learn More
And I tap on Body Mass Index
Then I will read more about Body Mass Index

Scenario: I can read more about Waist Circumference
Given I have navigated to a newly installed app home screen
And I tap on VHC card
And I see VHC onboarding modal
And I tap the Got It button
And I will be navigated to VHC landing screen
And I tap VHC Learn More
And I tap on Waist Circumference
Then I will read more about Waist Circumference

Scenario: I can read more about Blood Glucose
Given I have navigated to a newly installed app home screen
And I tap on VHC card
And I see VHC onboarding modal
And I tap the Got It button
And I will be navigated to VHC landing screen
And I tap VHC Learn More
And I tap on Blood Glucose
Then I will read more about Blood Glucose

Scenario: I can read more about Blood Pressure
Given I have navigated to a newly installed app home screen
And I tap on VHC card
And I see VHC onboarding modal
And I tap the Got It button
And I will be navigated to VHC landing screen
And I tap VHC Learn More
And I tap on Blood Pressure
Then I will read more about Blood Pressure

Scenario: I can read more about Cholesterol
Given I have navigated to a newly installed app home screen
And I tap on VHC card
And I see VHC onboarding modal
And I tap the Got It button
And I will be navigated to VHC landing screen
And I tap VHC Learn More
And I tap on Cholesterol
Then I will read more about Cholesterol

Scenario: I can read more about HbA1c
Given I have navigated to a newly installed app home screen
And I tap on VHC card
And I see VHC onboarding modal
And I tap the Got It button
And I will be navigated to VHC landing screen
And I tap VHC Learn More
And I tap on HbA1c
Then I will read more about HbA1c

Scenario: I can go back to the Learn more screen from the more information screen
Given I have navigated to a newly installed app home screen
And I tap on VHC card
And I see VHC onboarding modal
And I tap the Got It button
And I will be navigated to VHC landing screen
And I tap VHC Learn More
And I tap on one of the metrics
When I tap on back button
Then The VHC learn more will show

Scenario: I can view the learn more screen from the VHC landing screen
Given I am on the VHC landing screen
When I tap VHC Learn More
Then I will see Learn More screen

Scenario: I can go back to VHC onboarding screen from learn more screen
Given I have navigated to a newly installed app home screen
And I tap on VHC card
And I see VHC onboarding modal
And I tap VHC Learn More
And The VHC learn more will show
When I tap on back button
Then I see VHC onboarding modal

Scenario: VHC on-boaridng modal is displayed when user clicks on VHC card on home screen
Given I have navigated to a newly installed app home screen
When I tap on VHC card
Then I see VHC onboarding modal


Scenario: Check if user is able to tap on Learn More
Given I have navigated to a newly installed app home screen
And I tap on VHC card
And I see VHC onboarding modal
When I tap VHC Learn More
Then The VHC learn more will show

Scenario: Check if user is able to return to Onboarding screen by taping on Back button from Learn More screen
Given I have navigated to a newly installed app home screen
And I tap on VHC card
When I tap VHC Learn More
And I tap on back button
Then I see VHC onboarding modal

Scenario: Check if user able to start VHC by taping on Get Started
Given I have navigated to a newly installed app home screen
And I tap on VHC card
When I tap on get started
Then I will be navigated to VHC landing screen

Scenario: Check the content displayed on Onboarding screen is returned from CMS
Given I have navigated to a newly installed app home screen
When I tap on VHC card
Then I will see content displayed from CMS

Scenario: Learn More link is displayed under Get Started button
Given I have navigated to a newly installed app home screen
When I tap on VHC card
Then I will see the Learn More button

Scenario: Check if all the cells under Health Measurements are Greyed out
Given I have navigated to a VHC user home screen
When I have navigated to VHC landing screen
Then I will see meassurements greyed out

Scenario: Check if every section displays information about how many points can be earned
Given I have navigated to a VHC user home screen
And I have navigated to VHC landing screen
When I see measurements
Then I will see more info on points

Scenario: Check if user can navigate back to Home screen by clicking on back arrow
Given I have navigated to a VHC user home screen
And I have navigated to VHC landing screen
When I tap on back button
Then I am on the home screen

Scenario: Check if user can navigate to Capture Results screen
Given I have navigated to a VHC user home screen
And I have navigated to VHC landing screen
When I tap on Capture Results
Then I am on the Capture Results screen

Scenario: Check if user can navigate back to VHC landing from Capture Results
Given I have navigated to a VHC user home screen
And I have navigated to VHC landing screen
When I tap on Capture Results
And I tap Cancel
Then I am on the VHC screen

Scenario: Out of Health Range comment if the result is out of range
Given I have navigated to a VHC user home screen
And I have navigated to VHC landing screen
When I see measurements
Then I will see out of healthy range message and logo

Scenario: In Healthy Range comment if the result is within healthy range
Given I have navigated to a VHC user home screen
And I have navigated to VHC landing screen
When I see measurements
Then I will see in healthy range message and logo

Scenario: Navigate back to VHC Landing screen from details screen
Given I have navigated to a VHC user home screen
And I have navigated to VHC landing screen
When I tap on a meassurement
Then I will see more details screen
And I tap on back button

Scenario: Check if number of points earned are displayed for captured result
Given I have navigated to a VHC user home screen
And I have navigated to VHC landing screen
When I see measurements
Then I will see points for meassurements

Scenario: Check if 'Points pending' message is displayed when Points are pending for any captured result
Given I have navigated to a VHC user home screen
And I have navigated to VHC landing screen
When I see completed meassurements
When I will see pending message for points pending

Scenario: Check if user can navigate to History screen and back to VHC landing screen
Given I have navigated to a VHC user home screen
And I have navigated to VHC landing screen
And I tap on History
When I tap on back button
Then I see measurements

Scenario: Check if user can navigate to Learn More screen and back to VHC landing
Given I have navigated to a VHC user home screen
And I have navigated to VHC landing screen
And I tap on VHC Learn more button
When I tap on back button
Then I see measurements

Scenario: Check if user can navigate to Help screen and back to VHC landing screen
Given I have navigated to a VHC user home screen
And I have navigated to VHC landing screen
And I tap on Help
When I tap on back button
Then I see measurements

Scenario: User enters Blood Pressure values
Given I have navigated to a VHC user home screen
And I have navigated to VHC landing screen
And I tap on Capture Results
And I enter Systolic
Then I enter Diastolic
When I tap on Next
Then Add proof screen is shown

Scenario: Next button status disabled
Given I have navigated to a VHC user home screen
And I have navigated to VHC landing screen
And I tap on Capture Results
When I have not entered a value
Then The Next button is disabled

Scenario: Value out of range error
Given I have navigated to a VHC user home screen
And I have navigated to VHC landing screen
And I tap on Capture Results
When I enter a value out of range
Then I will see an error message being displayed

Scenario: Out of range error removed after correcting value
Given I have navigated to a VHC user home screen
And I have navigated to VHC landing screen
And I tap on Capture Results
And I enter a value out of range
And I will see an error message being displayed
When I correct the value
Then I will not see the error message displayed

Scenario: Able to view date submitted
Given I have navigated to a VHC user home screen
And I have navigated to VHC landing screen
When I tap on Capture Results
Then I will see the date option

Scenario: Change units of meassure
Given I have navigated to a VHC user home screen
And I have navigated to VHC landing screen
And I tap on Capture Results
When I tap on the current unit of meassure
Then I am able to change unit of meassure

Scenario: Display BMI alert when entering one value
Given I have navigated to a VHC user home screen
And I have navigated to VHC landing screen
And I tap on Capture Results
And I enter a weight value
When I tap on Next
Then I should see an alert

Scenario: Cancel on BMI alert
Given I have navigated to a VHC user home screen
And I have navigated to VHC landing screen
And I tap on Capture Results
And I enter a weight value
When I tap on Next
Then I should see an alert
And I tap Cancel

Scenario: Continue on BMI alert
Given I have navigated to a VHC user home screen
And I have navigated to VHC landing screen
And I tap on Capture Results
And I enter a weight value
When I tap on Next
Then I should see an alert
And I tap Continue
And Add proof screen is shown

Scenario: Add button on Add Proof screen
Given I have navigated to Add Proof screen
When Add proof screen is shown
Then I will see the Add button

Scenario: Photo upload add button with relevant options
Given I have navigated to Add Proof screen
When I tap on Add button
Then I will see library button
And I will see cancel button

Scenario: Tap on cancel on the photo upload screen
Given I have navigated to Add Proof screen
And I tap on Add button
When I tap Cancel
Then Add proof screen is shown

Scenario: Gallery opened when tapping on choose from library
Given I have navigated to Add Proof screen
And I tap on Add button
When I tap on Library button

Scenario: Library is displayed
Then I will see the gallery

Scenario: Cancel when gallery is opened
Given I have navigated to Add Proof screen
And I tap on Add button
And I tap on Library button

Scenario: Cancel from gallery
Then I will see the gallery
When I tap gallery Cancel button
Then Add proof screen is shown

Scenario: Select a photo to upload as proof
Given I have navigated to Add Proof screen
And I tap on Add button
And I tap on Library button

Scenario: Photo selection from gallery
And I will see the gallery
When I select a photo
Then Add proof screen is shown
And I tap on Next

Scenario: Select more than 1 photo to upload as proof
Given I have navigated to Add Proof screen
And I tap on Add button
And I tap on Library button

Scenario: Multiple image selection from gallery
And I will see the gallery
And I select a photo
And Add proof screen is shown
When I tap add icon
And I tap on Library button
And I select another photo
Then Add proof screen is shown
