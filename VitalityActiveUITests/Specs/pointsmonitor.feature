@tag
Feature: Points Monitor

Scenario: Navigate to Points Monitor Screen
Given I have navigated to a Points Monitor user home screen
And I tap Points on Menu
Then I should see Points Monitor screen

Scenario: Filter Points Monitor by Date
Given I have navigated to a Points Monitor user home screen
And I tap Points on Menu
And I should see Points Monitor screen
Then I tap on other Months

Scenario: Filter Points Monitor by Assessments
Given I have navigated to a Points Monitor user home screen
And I tap Points on Menu
And I should see Points Monitor screen
And I tap dropdown button
Then I choose Assessments

Scenario: Filter Points Monitor by Screenings
Given I have navigated to a Points Monitor user home screen
And I tap Points on Menu
And I should see Points Monitor screen
And I tap dropdown button
Then I choose Screenings

Scenario: Filter Points Monitor by Fitness
Given I have navigated to a Points Monitor user home screen
And I tap Points on Menu
And I should see Points Monitor screen
And I tap dropdown button
Then I choose Fitness

Scenario: Filter Points Monitor by I choose All Points
Given I have navigated to a Points Monitor user home screen
And I tap Points on Menu
And I should see Points Monitor screen
And I tap dropdown button
Then I choose I choose All Points

Scenario: Pull to Refresh
Given I have navigated to a Points Monitor user home screen
And I tap Points on Menu
And I should see Points Monitor screen
And I tap on other Months
Then I Pull to Refresh
