@tag
Feature: Non Smokers

Scenario: View Home screen
Given I have installed a new app
And I skip the intro
And login is shown
And I have entered NSD email
And I have entered a correct password
And I tap Login
And I agree to terms and conditions
When I tap next on user preferences
Then I am on the home screen

Scenario: Read the non-smokers declaration
Given I have navigated to the NSD home screen
When I tap on non-smoker declaration
Then Onboarding screen should show with Get Started button

Scenario: Can not see privacy policy if user is not on the market
Given I have installed a new app
And I skip the intro
And login is shown
And I have entered an email outside of market
And I have entered a correct password
And I tap Login
And I agree to terms and conditions
When I tap next on user preferences
Then I am on the home screen

Scenario: User not part of NSD
Given I have installed a new app
And I skip the intro
And login is shown
And I have entered a VHC username
And I have entered a correct password
And I tap Login
And I agree to terms and conditions
When I tap next on user preferences
Then I am on the home screen

Scenario: Navigate back to Home screen from On-boarding screen
Given I have navigated to the NSD home screen
And I tap on non-smoker declaration
When Onboarding screen should show with Get Started button
Then I navigate back to home screen

Scenario: Non-smoker declaration shown
Given I have navigated to the NSD home screen
And I tap on non-smoker declaration
When I tap on get started
Then Non-smoker’s declaration should show

Scenario: Declare that the user is a non-smoker
Given I have navigated to the NSD home screen
And I tap on non-smoker declaration
When I tap on get started
Then Non-Smoker terms and conditions shown
And I tap on I declare
And I see the privacy policy

Scenario: Disagree with the non-smokers declaration policy
Given I have navigated to the NSD home screen
And I tap on non-smoker declaration
When I tap on get started
Then Non-Smoker terms and conditions shown
And I tap on I declare
And I see the privacy policy
And I tap Disagree button

Scenario: Agree with the non-smokers declaration policy
Given I have navigated to the NSD home screen
And I tap on non-smoker declaration
When I tap on get started
Then Non-Smoker terms and conditions shown
And I tap on I declare
And I see the privacy policy
And I tap Agree button
And Completed summary screen is shown

Scenario: Complete status for declaration for non-smokers
Given I have navigated to NSD home screen with user eligible for completion
Then Homescreen card is updated to completed

Scenario: Learn more about the non-smoker’s declaration
Given I have navigated to the NSD home screen
And I tap on non-smoker declaration
When I tap on learn more button
Then learn more screen should show

Scenario: Close the learn more screen
Given I am on the learn more screen
When I go back
Then Onboarding screen should show
