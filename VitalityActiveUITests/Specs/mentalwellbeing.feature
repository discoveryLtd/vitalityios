@tag
Feature: Mental Wellbeing

Scenario: MWB on-boaridng modal is displayed when user clicks on MWB card on home screen
Given I have navigated to a newly installed to test MWB
When I tap on MWB card
Then I see MWB onboarding modal

Scenario: MWB Learn More link is displayed under Get Started button
Given I have navigated to a newly installed to test MWB
When I tap on MWB card
Then I will see the MWB Learn More button

Scenario: Check if user is able to tap on MWB Learn More
Given  I have navigated to a newly installed to test MWB
And  I tap on MWB card
When I tap MWB Learn More

Scenario: I can view the learn more screen from MWB onboarding screen
Given I have navigated to a newly installed to test MWB
When I tap on MWB card
Then I see MWB onboarding modal
When I tap MWB Learn More
Then The MWB learn more will show

Scenario: Check if user is able to return to Onboarding screen by taping on Back button of MWB Learn More screen
Given I have navigated to a newly installed to test MWB
When I tap on MWB card
When I tap MWB Learn More
And I tap on back button
Then I see MWB onboarding modal

Scenario: Check if user able to start MWB by taping on Got it
Given  I have navigated to a newly installed to test MWB
And  I tap on MWB card
When I tap MWB Got It
Then I will be navigated to MWB landing screen

Scenario: Check if user will be able to see the Stressor Assessment cell
Given  I have navigated to a newly installed to test MWB
And  I tap on MWB card
When I tap MWB Got It
Then I will see Stressor Assessment

Scenario: Check if user will be able to see the Psychological Assessment cell
Given  I have navigated to a newly installed to test MWB
And  I tap on MWB card
When I tap MWB Got It
Then I will see Psychological Assessment

Scenario: Check if user will be able to see the Social Assessment cell
Given  I have navigated to a newly installed to test MWB
And  I tap on MWB card
When I tap MWB Got It
Then I will see Social Assessment

Scenario: Check if user will be able to see the Learn More cell
Given  I have navigated to a newly installed to test MWB
And  I tap on MWB card
When I tap MWB Got It
Then I will see Learn More button on MWB screen

Scenario: Check if user will be able to see the Help cell
Given  I have navigated to a newly installed to test MWB
And  I tap on MWB card
When I tap MWB Got It
Then I will see Help button on MWB screen

Scenario: Check if user will be able to complete the Stressor Assessment
Given  I have navigated to a newly installed to test MWB
And  I tap on MWB card
When I tap MWB Got It
Then I will complete Stressor Assessment

Scenario: Check if user will be able to complete the Psychological Assessment
Given  I have navigated to a newly installed to test MWB
And  I tap on MWB card
When I tap MWB Got It
Then I will complete Psychological Assessment

Scenario: Check if user will be able to complete the Social Assessment
Given  I have navigated to a newly installed to test MWB
And  I tap on MWB card
When I tap MWB Got It
Then I will complete Social Assessment
