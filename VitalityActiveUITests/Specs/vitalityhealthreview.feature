@tag
Feature: Vitality Health Review

Scenario: View VHR Onboarding
Given I have navigated to a new VHR user home screen
When I tap on VHR card
Then I will see VHR onboarding

Scenario: View VHR Disclaimer
Given I have navigated to a new VHR user home screen
And I tap on VHR card
And I will see VHR onboarding
When I tap on Disclaimer
Then I will see Disclaimer screen

Scenario: View Disclaimer button
Given I have navigated to a new VHR user home screen
And I tap on VHR card
When I will see VHR onboarding
Then I see Disclaimer button

Scenario: Navigate back to onboarding from disclaimer
Given I have navigated to a new VHR user home screen
And I tap on VHR card
And I will see VHR onboarding
And I tap on Disclaimer
And I will see Disclaimer screen
When I tap Back
Then I will see VHR onboarding

Scenario: Check VHR Onboarding header
Given I have navigated to a new VHR user home screen
When I tap on VHR card
Then I will see the VHR onboarding header

Scenario: Check VHR onboarding earn points
Given I have navigated to a new VHR user home screen
When I tap on VHR card
Then I will see VHR onboarding
And I will see earn points section

Scenario: Check VHR onboarding vitality age
Given I have navigated to a new VHR user home screen
When I tap on VHR card
Then I will see VHR onboarding
And I will see vitality age section

Scenario: Check VHR onboarding completion time
Given I have navigated to a new VHR user home screen
When I tap on VHR card
Then I will see VHR onboarding
And I will see completion time section

Scenario: View VHR Get Started button
Given I have navigated to a new VHR user home screen
And I tap on VHR card
When I will see VHR onboarding
Then I will see Get Started button

Scenario: View the VHR landing screen by dismissing onboarding
Given I have navigated to a new VHR user home screen
And I tap on VHR card
And I will see VHR onboarding
When I tap the Got It button
Then I will see VHR landing screen

Scenario: View VHR Learn More Button
Given I have navigated to a new VHR user home screen
And I tap on VHR card
And I will see VHR onboarding
When I tap the Got It button
Then VHR Learn More Button should show

Scenario: View VHR Learn More Screen
Given I have navigated to a new VHR user home screen
And I tap on VHR card
And I will see VHR onboarding
And I tap the Got It button
And VHR Learn More Button should show
When I tap on VHR learn more button
Then VHR Learn More Screen should show

Scenario: View the Help button
Given I have navigated to a new VHR user home screen
And I tap on VHR card
And I will see VHR onboarding
When I tap the Got It button
Then VHR Help Button should show

Scenario: Navigate back to VHR landing from Learn More
Given I have navigated to a new VHR user home screen
And I tap on VHR card
And I will see VHR onboarding
And I tap the Got It button
And VHR Learn More Button should show
And I tap on VHR learn more button
When I tap on VHR learn more back button
Then I will see VHR landing screen

Scenario: View Sections title on VHR landing screen
Given I have navigated to a new VHR user home screen
And I tap on VHR card
And I will see VHR onboarding
When I tap the Got It button
Then I will see the Sections title

Scenario: View General Health questionnaire
Given I have navigated to a new VHR user home screen
And I tap on VHR card
And I will see VHR onboarding
And I tap the Got It button
When I will see the Sections title
Then I will see General Health

Scenario: View Social Habits questionnaire
Given I have navigated to a new VHR user home screen
And I tap on VHR card
And I will see VHR onboarding
And I tap the Got It button
When I will see the Sections title
Then I will see Social Habits

Scenario: View Lifestyle Habits questionnaire
Given I have navigated to a new VHR user home screen
And I tap on VHR card
And I will see VHR onboarding
And I tap the Got It button
When I will see the Sections title
Then I will see Lifestyle Habits

Scenario: View the VHR sections footer text
Given I have navigated to a new VHR user home screen
And I tap on VHR card
And I will see VHR onboarding
And I tap the Got It button
When I will see the Sections title
Then I will see sections footer text

Scenario: Check the status of Sections Start button
Given I have navigated to a new VHR user home screen
And I tap on VHR card
And I will see VHR onboarding
And I tap the Got It button
When I will see the Sections title
Then I will see start button

Scenario: Check the status of Sections Edit button
Given I have navigated to a new VHR user home screen
And I tap on VHR card
And I will see VHR onboarding
And I tap the Got It button
When I will see the Sections title
Then I will see edit button

Scenario: Check the status of Sections Continue button
Given I have navigated to VHR home to complete questionnaire
And I tap on VHR card
And I will see VHR onboarding
And I tap the Got It button
And I tap on Start
And I select first answer
When I tap Close
Then I will see continue button

Scenario: Check the estimated time is displayed for sections
Given I have navigated to a new VHR user home screen
And I tap on VHR card
And I will see VHR onboarding
And I tap the Got It button
When I will see the Sections title
Then I will see estimated time to complete

Scenario: Check completed footer of section with finished questionnaire
Given I have navigated to a completed VHR user home screen
And I tap on VHR card
And I will see VHR onboarding
And I tap the Got It button
When I will see the Sections title
Then I will see completed questionnaire footer

Scenario: View the VHR completed state on the landing screen
Given I have navigated to a completed VHR user home screen
And I tap on VHR card
And I will see VHR onboarding
When I tap the Got It button
Then I will see completed state at the top of screen

Scenario: View completed points and cycle message on landing screen
Given I have navigated to a completed VHR user home screen
And I tap on VHR card
And I will see VHR onboarding
When I tap the Got It button
Then I will see points and cycle message

Scenario: View the VHR privacy policy after completing questionnaire
Given I have navigated to a completed VHR user home screen
And I tap on VHR card
And I will see VHR onboarding
When I tap the Got It button
And I clicked Edit on Social Habits
And I select first answer
And I tap Next on questionnaire
When I tap Done
Then I will see VHR privacy policy

Scenario: Disagree with the VHR privacy policy
Given I have navigated to a completed VHR user home screen
And I tap on VHR card
And I will see VHR onboarding
When I tap the Got It button
And I clicked Edit on Social Habits
And I select first answer
And I tap Next on questionnaire
And I tap Done
And I will see VHR privacy policy
When I tap VHR policy disagree button
Then I will see the done button in questionnaire

Scenario: Agree with the VHR privacy policy
Given I have navigated to a completed VHR user home screen
And I tap on VHR card
And I will see VHR onboarding
When I tap the Got It button
And I clicked Edit on Social Habits
And I select first answer
And I tap Next on questionnaire
And I tap Done
When I will see VHR privacy policy
Then I tap VHR policy agree button

Scenario: View the completion screen after completing questionnaire
Given I have navigated to a completed VHR user home screen
And I tap on VHR card
And I will see VHR onboarding
When I tap the Got It button
And I clicked Edit on Social Habits
And I select first answer
And I tap Next on questionnaire
And I tap Done
And I will see VHR privacy policy
When I tap VHR policy agree button
Then I see the VHR completion screen

Scenario: View the completed state on the completion screen
Given I have navigated to VHR who completed social habits
And I tap on VHR card
And I will see VHR onboarding
When I tap the Got It button
And I tap on Edit
And I select first answer
And I tap Next on questionnaire
And I tap Done
And I will see VHR privacy policy
And I tap VHR policy agree button
When I will see sections remaining message
Then I see completed state

Scenario: View how many sections remaining on completion screen
Given I have navigated to VHR who completed social habits
And I tap on VHR card
And I will see VHR onboarding
When I tap the Got It button
And I tap on Edit
And I select first answer
And I tap Next on questionnaire
And I tap Done
And I will see VHR privacy policy
When I tap VHR policy agree button
Then I will see sections remaining message

Scenario: View general health section on completion screen
Given I have navigated to a completed VHR user home screen
And I tap on VHR card
And I will see VHR onboarding
When I tap the Got It button
And I edit on General Health
And I tap Next on questionnaire
And I tap the next on the bottom
And I tap the done on the bottom
And I will see VHR privacy policy
When I tap VHR policy agree button
Then I see the VHR completion screen

Scenario: View lifestyle habits section on completion screen
Given I have navigated to a completed VHR user home screen
And I tap on VHR card
And I will see VHR onboarding
When I tap the Got It button
And I edit on Lifestyle Habit
And I tap the next on the bottom
And I tap the next on the bottom
And I tap the next on the bottom
And I tap the done on the bottom
And I will see VHR privacy policy
When I tap VHR policy agree button
Then I see the VHR completion screen

Scenario: I am able to tap on done and navigate back to VHR landing
Given I have navigated to VHR who completed social habits
And I tap on VHR card
And I will see VHR onboarding
When I tap the Got It button
And I tap on Edit
And I select first answer
And I tap Next on questionnaire
And I tap Done
And I will see VHR privacy policy
And I tap VHR policy agree button
When I will see sections remaining message
And I tap Done
Then I will see VHR landing screen
