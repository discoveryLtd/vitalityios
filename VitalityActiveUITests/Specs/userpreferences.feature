@tag
Feature: User Preferences

Scenario: The user is shown the preferences screen after registering and logging in for the first time after accepting T&C
Given I am on the Login screen
And I have entered User Pref email
And I have entered a correct password
And I tap Login
When I agree to terms and conditions
Then I will see user preferences

Scenario: The user is not shown the preferences screen after he/she has already accepted T&C
Given I have installed a new app
When I logged in successfully and accepted terms
Then I am on the home screen

Scenario: Communication Preferences are listed under a heading ‘Communication Preferences’, with some explanatory text
Given I have navigated to user preferences
Then I will see communication preferences with text

Scenario: User can view the Email option header
Given I have navigated to user preferences
When I will see email header
Then I am able to toggle the email button

Scenario: The email option choice is cached for the user
Given I have navigated to user preferences
And I disable the email switch
When I tap next on user preferences
And I have navigated to user preferences
Then Email switch should be disabled

Scenario: User can view the Push Notifications option header and that the button is disabled by default.
Given I have navigated to user preferences
When I will see push notifications header
Then I will see the push notification button is disabled

Scenario: The user is able to enable the push notification button and that the standard iOS permission alert it shown
Given I have navigated to user preferences
And I will see push notifications header
When I am able to toggle the push notification button to enabled
Then I will get a permission alert

Scenario: The user can accept or decline the permission alert.
Given I have navigated to user preferences
And I will see push notifications header
And I am able to toggle the button to enabled
When I will get a permission alert
Then I am able to see allow button
And I am able to see don't allow button

Scenario: The user is able to view a new link to manage app permissions in the device settings.
Given I have navigated to user preferences
And I will see push notifications header
And I am able to toggle the button to enabled
And I will get a permission alert
When I am able to tap allow
Then I will see a link to app permissions
And I am able to tap on the link to app permissions

Scenario: The user can not disable the push notification button once enabled
Given I have navigated to user preferences
When I am able to toggle the button to enabled
Then I am not able to disable the button

Scenario: The user can click ‘Next’ which saves these preferences and navigates the user to the Home screen.
Given I have navigated to user preferences
When I tap next on user preferences
Then I am on the home screen

Scenario: Check that the user is able to view Privacy section under Privacy heading
Given I have navigated to user preferences
When I will see user preferences
Then I will see privacy section

Scenario: View Privacy Statement link
Given I have navigated to user preferences
And I will see user preferences
When I will see privacy section
Then I will see privacy statement link

Scenario: Tap on the Privacy Statement link
Given I have navigated to user preferences
And I will see user preferences
And I will see privacy section
When I will see privacy statement link
Then I tap on Privacy statement

Scenario: Check that the user is able to view explanatory text under the Privacy heading
Given I have navigated to user preferences
When I will see user preferences
Then I will see text under privacy section

Scenario: Check that the user is able to view Privacy Statement link
Given I have navigated to user preferences
When I will see user preferences
Then I am able to view the privacy statement link

Scenario: Check that user is able to select the Privacy Statement link which will direct them to an external webview.
Given I have navigated to user preferences
When I will see user preferences
Then I am able to view the privacy statement link

Scenario: Check that the user is able to see the Analytics header under Privacy
Given I have navigated to user preferences
When I will see user preferences
Then I am able to view the analytics header

Scenario: Check that the user is able to see the explanatory text under Analytics
Given I have navigated to user preferences
When I will see user preferences
Then I am able to view the analytics header
And I will see text under analytics

Scenario: Check that Analytics is enabled by default
Given I have navigated to user preferences
And I will see user preferences
When I am able to view the analytics header
Then I will see the analytics toggle is enabled

Scenario: Check that the user is able to toggle the Analytics button to disabled
Given I have navigated to user preferences
And I will see user preferences
And I scroll to analytics
When I am able to view the analytics header
Then I can tap on the analytics toggle

Scenario: Check the the user is able to see the Crash Reports header under Privacy
Given I have navigated to user preferences
When I will see user preferences
Then I am able to view the crash reports header

Scenario: Check that the user is able to see the explanatory text under Crash Reports
Given I have navigated to user preferences
When I will see user preferences
Then I am able to view the crash reports header
And I will see text under crash reports

Scenario: Check that Crash Reports is enabled by default
Given I have navigated to user preferences
And I will see user preferences
When I am able to view the crash reports header
Then I will see the crash reports toggle is enabled

Scenario: Check that the user is able to toggle the Crash Reports button to disabled
Given I have navigated to user preferences
And I will see user preferences
And I scroll to crash reports
When I am able to view the crash reports header
Then I can tap on the crash reports toggle

Scenario: Check that the user is able to view Security section under Security heading
Given I have navigated to user preferences
When I will see user preferences
Then I will see security section

Scenario: Check if I see the 'manage privacy and security settings' footer
Given I have navigated to user preferences
When I will see user preferences
Then I will see manage privacy and security settings footer

Scenario: Check that the user is able to view explanatory text under the Security heading
Given I have navigated to user preferences
When I will see user preferences
Then I will see security section
And I will see text under security

Scenario: Check that the user is able to see the Remember Me header under Privacy
Given I have navigated to user preferences
When I will see user preferences
Then I am able to view the remember me header

Scenario: Check that the user is able to see the explanatory text under Remember Me
Given I have navigated to user preferences
When I will see user preferences
Then I am able to view the remember me header
And I will see text under remember me

Scenario: Check that Remember Me is enabled by default
Given I have navigated to user preferences
And I will see user preferences
And I scroll to the bottom of user preferences
When I am able to view the remember me header
Then I will see the remember me toggle is enabled

Scenario: Check that the user is able to toggle the Remember Me button to disabled
Given I have navigated to user preferences
And I will see user preferences
And I scroll to remember me
When I am able to view the remember me header
Then I can tap on the remember me toggle

Scenario: Check if the user has remember me on, the next login the username field is filled
Given I have navigated to user preferences
And I have enabled remember me
When I log out and back in
Then I will see my username

Scenario: Check that an alert appears if all user preferences are disabled
Given I have navigated to user preferences
And I have disabled all user preferences
When I tap next on user preferences
Then I see You Havent Allowed Any Permissions alert

Scenario: Check that the user is able to Cancel the alert and toggle preferences
Given I have navigated to user preferences
And I have disabled all user preferences
When I tap next on user preferences
Then I see You Havent Allowed Any Permissions alert
And I tap Cancel

Scenario: Check that the user is able to Continue with no preferences enabled
Given I have navigated to user preferences
And I have disabled all user preferences
When I tap next on user preferences
Then I see You Havent Allowed Any Permissions alert
And I tap Continue

Scenario: Check that the user is navigated to the home screen after selecting Next
Given I have navigated to user preferences
When I tap next on user preferences
Then I am on the home screen

Scenario: Check that preferences were saved and that the user is not presented with the User Preference screen on next login
Given I have navigated to user preferences
And I tap next on user preferences
When I log out and back in
Then I will see home screen
