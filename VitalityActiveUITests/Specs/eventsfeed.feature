@tag
Feature: Events Feed

Scenario: Events row
Given I have installed a new app
And I skip the intro
And login is shown
And I have entered a correct email
And I have entered a correct password
And I tap Login
And I agree to terms and conditions
And I tap next on user preferences
When I tap Profile Tab
Then Events row is shown

Scenario: See Category Filter
Given Events row is shown
And I tap Events Feed row
When I tap Category Filter
Then Login Category is shown
And Assessment Category is shown

Scenario: Filter Events by Assessment Category
Given Events row is shown
And I tap Events Feed row
And I tap Category Filter
And I select Assessment
When I close filter
Then Events under Assessment Category are shown
