@tag
Feature: Membership View Help

Scenario: Tap help button
Given I go to the user's profile tab
And I tap on Membership Pass
When I tap on View Help
Then Help screen is displayed

Scenario: Navigate back from view help page
Given I go to the user's profile tab
And I tap on Membership Pass
And I tap on View Help
When I click on Back button
Then Membership Pass screen is shown
