@tag
Feature: Profile

Scenario: View profile
Given I am on the Login screen
And I have entered Donette's email
And I have entered a correct password
And I tap Login
And I agree to terms and conditions
And I tap next on user preferences
And I tap Profile Tab
When I tap User Profile
Then User Profile is shown

Scenario: Edit profile picture using phone library
Given I navigate to the user's profile
And I tap Edit
And I select Choose From Library
And I allow app to access my photos

Scenario: Email invalid format
Given I navigate to Donette's profile
And I tap Email
When I type an invalid email
#Then Invalid email address error is shown

Scenario: Email exceed maximum number of characters
Given I navigate to Donette's profile
And I tap Email
When I type 100 characters
#Then Invalid email address error is shown

Scenario: Email existing
Given I navigate to Donette's profile
And I tap Email
And I type an existing email
When I tap Done
Then Unable to Change Email alert is shown

Scenario: Email available
Given I navigate to Donette's profile
And I tap Email
And I type a new valid email
When I tap Done
Then Confirmation alert is shown

Scenario: Email change #TODO: How to test email change?
Given I navigate to Donette's profile
And I tap Email
And I type a new valid email
And I tap Done
And I tap Continue
And I provide a correct password
#When I tap Ok
#Then..
