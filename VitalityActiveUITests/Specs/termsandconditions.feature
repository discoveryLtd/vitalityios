@tag
Feature: termsandconditions

Scenario: Terms and Conditions row is present
Given I am on the Login screen
And I have entered a correct email
And I have entered a correct password
And I tap Login
And I agree to terms and conditions
And I tap next on user preferences
When I navigate to Settings
Then Terms and Conditions row is present

Scenario: View Terms and Conditions
Given I am on the Login screen
And I have entered a correct email
And I have entered a correct password
And I tap Login
And I agree to terms and conditions
And I tap next on user preferences
When I navigate to Terms and Conditions
Then I can view Terms and Condition webview

Scenario: Navigate back from Terms and Conditions
Given I am on the Login screen
And I have entered a correct email
And I have entered a correct password
And I tap Login
And I agree to terms and conditions
And I tap next on user preferences
When I navigate back to Settings
Then I should be back at the Settings screen
