@tag
Feature: Active Rewards

Scenario: Active Rewards on-boaridng modal is displayed when user clicks on AR card on home screen
Given I have navigated to a non Activated Active Rewards user home screen
And I tap on AR card
Then I see AR onboarding modal

Scenario: AR Learn More link is displayed under Get Started button
Given II have navigated to a non Activated Active Rewards user home screen
When I tap on AR card
Then I will see the AR Learn More button

Scenario: Check if user is able to tap on AR Learn More
Given I have navigated to a non Activated Active Rewards user home screen
And I tap on AR card
When I tap AR Learn More

Scenario: I can view the learn more screen from AR onboarding screen
Given I have navigated to a non Activated Active Rewards user home screen
When I tap on AR card
Then I see AR onboarding modal
When I tap AR Learn More
Then I should see Learn More screen

Scenario: Check if user is able to return to Onboarding screen by taping on Back button of AR Learn More screen
Given I have navigated to a non Activated Active Rewards user home screen
When I tap on AR card
When I tap AR Learn More
And I tap on back button
Then I see AR onboarding modal

Scenario: Activate Active Rewards
Given I have navigated to a non Activated Active Rewards user home screen
And I tap on AR card
And I tap AR Get Started
And I tap Got It
When I tap Link Later
Then I will be navigated to AR landing screen

Scenario: Navigate to Activated Active Rewards
Given I have navigated to a new Active Rewards user home screen
And I tap on AR card
When I press Cancel in Link a device or app
Then I will be navigated to AR landing screen

Scenario: Check if user will be able to see the Points
Given I have navigated to a new Active Rewards user home screen
And I tap on AR card
And I press Cancel in Link a device or app
When I will be navigated to AR landing screen
Then I should see AR points

Scenario: Check if user will be able to see This Week Activity Cell
Given I have navigated to a new Active Rewards user home screen
And I tap on AR card
And I press Cancel in Link a device or app
When I will be navigated to AR landing screen
Then I should see This Week Activity Cell

Scenario: Navigate to Active Rewards Activity Screen
Given I have navigated to a new Active Rewards user home screen
And I tap on AR card
And I press Cancel in Link a device or app
And I will be navigated to AR landing screen
When I tap on Activity cell
Then I should see Activity screen

Scenario: Navigate In Progress Activity Screen
Given I have navigated to a new Active Rewards user home screen
And I tap on AR card
And I press Cancel in Link a device or app
And I will be navigated to AR landing screen
And I tap on Activity cell
And I should see Activity screen
When I tap In Progress Cell
Then I should see In Progress Screen

Scenario: Navigate to Active Rewards Rewards Screen
Given I have navigated to a new Active Rewards user home screen
And I tap on AR card
And I press Cancel in Link a device or app
And I will be navigated to AR landing screen
When I tap on Rewards cell
Then I should see Rewards screen

Scenario: Navigate to History in Rewards Screen
Given I have navigated to a new Active Rewards user home screen
And I tap on AR card
And I press Cancel in Link a device or app
And I will be navigated to AR landing screen
And I tap on Rewards cell
And I should see Rewards screen
When I tap History
Then I should see History Screen

Scenario: Navigate to Active Rewards Learn More Screen
Given I have navigated to a new Active Rewards user home screen
And I tap on AR card
And I press Cancel in Link a device or app
And I will be navigated to AR landing screen
When I tap on Learn More cell
Then I should see Learn More screen

Scenario: Navigate to Participating Partners in Learn More Screen
Given I have navigated to a new Active Rewards user home screen
And I tap on AR card
And I press Cancel in Link a device or app
And I will be navigated to AR landing screen
And I tap on Learn More cell
And I should see Learn More screen
When I tap Participating Partners
Then I should see Participating Partners Screen

Scenario: Navigate to Benefits Guide in Learn More Screen
Given I have navigated to a new Active Rewards user home screen
And I tap on AR card
And I press Cancel in Link a device or app
And I will be navigated to AR landing screen
And I tap on Learn More cell
And I should see Learn More screen
When I tap Benefits Guide
Then I should see Benefits Guide Screen

Scenario: Navigate to Active Rewards Help Screen
Given I have navigated to a new Active Rewards user home screen
And I tap on AR card
And I press Cancel in Link a device or app
And I will be navigated to AR landing screen
When I tap AR Help cell
Then I should see Help screen

Scenario: Display Wellness Devices
Given I have navigated to a new Active Rewards user home screen
And I tap on AR card
And I press Link Now in Link a device or app
When I press Got it on Wellness Device onboarding
Then I should see Wellness Devices screen
