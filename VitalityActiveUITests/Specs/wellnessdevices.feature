@tag
Feature: Wellness Devices

Scenario: View Wellness Devices home screen card
Given I have navigated to a Wellness Device user home screen
When I see Improve Your Health section
Then I see the wellness devices card

Scenario: View Wellness Devices onboarding screen
Given I have navigated to a Wellness Device user home screen
When I tap on Wellness Devices card
Then I see Wellness Devices onboarding screen

Scenario: Tap Got It button on Wellness Devices onboarding screen
Given I have navigated to a wellness device user home screen
And I tap on Wellness Devices card
When I see Wellness Devices onboarding screen
Then I tap on Got It button

Scenario: View I need device or app link on onboarding
Given I have navigated to a Wellness Device user home screen
And I tap on Wellness Devices card
When I see Wellness Devices onboarding screen
Then I will see I need a device or app link

Scenario: View Wellness Devices landing screen with a new user
Given I have navigated to a Wellness Device user home screen
And I tap on Wellness Devices card
When I tap Got It
Then I see Wellness Devices landing screen

Scenario: View Wellness Devices learn more button
Given I have navigated to a Wellness Device user home screen
When I have navigated to Wellness Devices landing screen
Then I will see WDA Learn More button

Scenario: View Learn More for Wellness Devices
Given I have navigated to a Wellness Device user home screen
And I have navigated to Wellness Devices landing screen
When I tap on WDA Learn More
Then I will see Learn More screen for Wellness Devices

Scenario: Navigate back to Wellness landing from Learn More
Given I have navigated to a Wellness Device user home screen
And I have navigated to Wellness Devices landing screen
And I tap on WDA Learn More
When I tap on back button
Then I see Wellness Devices landing screen

Scenario: Tap on Help button on Wellness Devices landing screen
Given I have navigated to a Wellness Device user home screen
And I have navigated to Wellness Devices landing screen
When I see WDA Help button
Then I tap on WDA Help

Scenario: Navigate back to Wellness landing from Help
Given I have navigated to a new Wellness Device user home screen
And I have navigated to Wellness Devices landing screen
And I tap on Help
When I tap on back button
Then I see Wellness Devices landing screen

Scenario: View Get Linking title when no devices have been linked
Given I have navigated to a new Wellness Device user home screen
When I have navigated to Wellness Devices landing screen
Then I will see Get Linking title

Scenario: View Get Linking message when no devices have been linked
Given I have navigated to a new Wellness Device user home screen
When I have navigated to Wellness Devices landing screen
Then I will see the Get Linking message

Scenario: View devices that are available to link
Given I have navigated to a Wellness Device user home screen
When I have navigated to Wellness Devices landing screen
Then I will see the available to link heading

Scenario: Always see at least one device to link
Given I have navigated to a Wellness Device user home screen
When I have navigated to Wellness Devices landing screen
Then I will see at least one device

Scenario: View Health App device details screen
Given I have navigated to a Wellness Device user home screen
And I have navigated to Wellness Devices landing screen
When I tap on Health App
Then I see the device details screen for Health App

Scenario: View points message at the top of device details screen
Given I have navigated to a Wellness Device user home screen
And I have navigated to Wellness Devices landing screen
When I tap on Adidas
Then I see the points message at the top

Scenario: View Link Now button on the device details screen
Given I have navigated to a Wellness Device user home screen
And I have navigated to Wellness Devices landing screen
When I tap on Adidas
Then I see Link Now button

Scenario: View Steps to Link button on device details screen
Given I have navigated to a Wellness Device user home screen
And I have navigated to Wellness Devices landing screen
When I tap on Adidas
Then I will see Steps to Link

Scenario: Tap on Steps to Link button
Given I have navigated to a Wellness Device user home screen
And I have navigated to Wellness Devices landing screen
And I tap on Adidas
When I will see Steps to Link
Then I will tap on Steps to Link

Scenario: View Points Earning Metrics on device details screen
Given I have navigated to a Wellness Device user home screen
And I have navigated to Wellness Devices landing screen
When I tap on Adidas
Then I will see points earning metrics

Scenario: View Learn More under the points earning metrics on device details screen
Given I have navigated to a Wellness Device user home screen
And I have navigated to Wellness Devices landing screen
When I tap on Adidas
Then I will tap learn more link under points earning metrics

Scenario: I will see Physical Activities available and grouped
Given I have navigated to a Wellness Device user home screen
And I have navigated to Wellness Devices landing screen
When I tap on Adidas
Then I will see Physical Activity header

Scenario: I will see more details on physical activity for a device or manufacturer
Given I have navigated to a new Wellness Device user home screen
And I have navigated to Wellness Devices landing screen
And I tap on Adidas
When I tap on Heart Rate
Then I will see more details on Heart Rate

Scenario: View About section on the device or manufacturer
Given I have navigated to a Wellness Device user home screen
And I have navigated to Wellness Devices landing screen
And I tap on Adidas
When I tap About Adidas
Then I will see the about screen for Adidas

Scenario: View Data Sharing Consent on Wellness Device
Given I have navigated to a Wellness Device user home screen
And I have navigated to Wellness Devices landing screen
And I tap on Adidas
When I tap on Link Now
Then I see the Data Sharing Consent

Scenario: Disagree the Data Sharing Consent on Wellness Device
Given I have navigated to a Wellness Device user home screen
And I have navigated to Wellness Devices landing screen
And I tap on Adidas
And I tap on Link Now
When I tap Disagree button
Then I will see Physical Activity header

Scenario: Agree to the Data Sharing Consent on Wellness Device
Given I have navigated to a Wellness Device user home screen
And I have navigated to Wellness Devices landing screen
And I tap on Adidas
When I tap on Link Now
Then I tap Agree button

Scenario: Manufacturer website for Adidas opens
Given I have navigated to a Wellness Device user home screen
And I have navigated to Wellness Devices landing screen
And I tap on Adidas
And I tap on Link Now
When I tap Agree button
Then I see the Done button on website

Scenario: Allow access for Health App with Wellness Devices
Given I have navigated to a Wellness Device user home screen
And I have navigated to Wellness Devices landing screen
And I tap on Health App
And I tap on Link Now
And I tap Agree button
When I tap WDA Get Started
Then I turn all categories on

Scenario: Do NOT allow access for Health App with Wellness Devices
Given I have navigated to a Wellness Device user home screen
And I have navigated to Wellness Devices landing screen
And I tap on Health App
And I tap on Link Now
And I tap Agree button
When I tap WDA Get Started
Then I tap on Dont Allow

Scenario: Information screen for Health App with Wellness Devices
Given I have navigated to a Wellness Device user home screen
And I have navigated to Wellness Devices landing screen
And I tap on Health App
And I tap on Link Now
When I tap Agree button
Then I see get started button

Scenario: View Wellness Devices landing screen with linked heading
Given I have navigated to a Wellness Device user home screen
When I have navigated to Wellness Devices landing screen
Then I see linked heading

Scenario: View more available to link devices
Given I have navigated to a Wellness Device user home screen
When I have navigated to Wellness Devices landing screen
Then I see available to link heading

Scenario: Link an additional device
Given I have navigated to a Wellness Device user home screen
And I have navigated to Wellness Devices landing screen
And I tap on Adidas
And I tap on Link Now
When I tap Agree button
Then I see the Done button on website

Scenario: View data time estimate to sync message
Given I have navigated to a Wellness Device user home screen
When I have navigated to Wellness Devices landing screen
Then I see data can take up to 2 days to sync message

Scenario: View Wellness Device detail screen with linked device
Given I have navigated to linked but un-synced Wellness Device home screen
And I have navigated to Wellness Devices landing screen
When I tap on Health App
Then I will see not synced message

Scenario: View Delink button on Wellness Devices landing screen
Given I have navigated Wellness Device to delink app
And I have navigated to Wellness Devices landing screen
When I tap on Health App
Then I see Delink button

Scenario: Delink the Health App from Wellness Devices
Given I have navigated Wellness Device to delink app
And I have navigated to Wellness Devices landing screen
And I tap on Health App
When I tap Delink
Then I will be delinked

Scenario: Sync Health App to Wellness Devices
Given I have navigated to Wellness Device to sync app
And I have navigated to Wellness Devices landing screen
And I tap on Health App
When I tap on Sync Now
Then Health App is synced




