@tag
Feature: Finger Print Login

Scenario: Log in success
Given I am on the Login screen
And I have entered a correct email
And I have entered a correct password
When I tap Login
Then I should be logged in successfully

Scenario: Navigate to Profile
Given I am on the Login screen
And I have entered a correct email
And I have entered a correct password
And I tap Login
And I should be logged in successfully
When I tap profile
Then I should be in profile screen

Scenario: Navigate to Settings
Given I am on the Login screen
And I have entered a correct email
And I have entered a correct password
And I tap Login
And I should be logged in successfully
And I tap profile
And I should be in profile screen
When I tap on settings
Then I should see the settings screen

Scenario: Navigate to Security
Given I am on the Login screen
And I have entered a correct email
And I have entered a correct password
And I tap Login
And I should be logged in successfully
And I tap profile
And I should be in profile screen
And I tap on settings
And I should see the settings screen
When I tap on Security
Then I should see the security screen

Scenario: Enable Touch ID on Settings
Given I am on the Login screen
And I have entered a correct email
And I have entered a correct password
And I tap Login
And I should be logged in successfully
And I tap profile
And I should be in profile screen
And I tap on settings
And I should see the settings screen
And I tap on Security
And I should see the security screen
When I toggle touch ID
Then I should see Authenticate with touch ID prompt

Scenario: User Preferences shown after login
Given I am on the Login screen
And I have entered a correct email
And I have entered a correct password
When I tap Login
Then I should see User Preferences screen

Scenario: Scroll to Security
Given I am on the Login screen
And I have entered a correct email
And I have entered a correct password
And I tap Login
And I should see User Preferences screen
Then I scroll to Security

Scenario: Enable Touch ID in User Preference after login
Given I am on the Login screen
And I have entered a correct email
And I have entered a correct password
And I tap Login
And I should see User Preferences screen
And I scroll to Security
When I toggle User Preferences touch ID 
Then I should see Authenticate with touch ID prompt

Scenario: Cancel Activation in Settings
Given I am on the Login screen
And I have entered a correct email
And I have entered a correct password
And I tap Login
And I should be logged in successfully
And I tap profile
And I should be in profile screen
And I tap on settings
And I should see the settings screen
And I tap on Security
And I should see the security screen
And I toggle touch ID
And I should see Authenticate with touch ID prompt
Then I press Cancel in touch ID prompt

Scenario: Cancel Activation in User Preference after login
Given I am on the Login screen
And I have entered a correct email
And I have entered a correct password
And I tap Login
And I should see User Preferences screen
And I scroll to Security
And I toggle User Preferences touch ID 
And I should see Authenticate with touch ID prompt
Then I press Cancel in touch ID prompt
