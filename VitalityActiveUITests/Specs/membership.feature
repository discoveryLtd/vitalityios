@tag
Feature: Membership

Scenario: View membership pass
Given I go to the user's profile tab
And I tap on Membership Pass
Then Membership Pass screen is shown

Scenario: View vitality number information
Given I go to the user's profile tab
And I tap on Membership Pass
And I tap on Vitality Number
Then Vitality Number information screen is shown

Scenario: Add digital pass image later
Given I go to the user's profile tab
And I tap on Membership Pass
And I tap on Add Digital Pass
And I select Add Profile Image Later
And I tap Add
Then Digital Pass is added

#TODO: Implement this once adding images when adding Digital Pass is implemented.
#Scenario: Add digital pass image now
#Given I go to the user's profile tab
#And I tap on Membership Pass
#And I tap on Add Digital Pass
#And I select Add Profile Image Now

Scenario: Remove pass
Given I go to the user's profile tab
And I add a Digital Pass
And I tap on the existing Digital Pass
And I select Remove Pass
And I remove pass
Then Digital Pass is removed

#TODO: Implement assertions. Impediment: Don't know how to detect UIElements outside the app.
#Scenario: View pass in wallet
#Given I go to the user's profile tab
#And I add a Digital Pass
#And I tap on the existing Digital Pass
#And I select Show Pass in Wallet
#Then Wallet is shown
