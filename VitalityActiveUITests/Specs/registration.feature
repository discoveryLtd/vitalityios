@tag
Feature: Registration

Scenario: User successfully clicks the 'Register' link and is able to view the online Registration form
Given I am on the Login screen
When I tap Register button
Then Registration form is shown

Scenario: User is successfully registered
Given I am on the Login screen
And I tap Register button
And Registration form is shown
And I have entered a new email
And I have entered new password
And I have entered new confirm password
And I have entered new date of birth
And I have entered new insurer code
When I tap on Register
Then I should be logged in successfully

Scenario: Incorrect but valid Email
Given I am on the Login screen
And I tap Register button
And Registration form is shown
And I have entered incorrect email
And I have entered correct password
And I have entered correct confirm password
And I have entered date of birth
And I have entered correct insurer code
When I tap on Register
Then I should get an incorrect code or email error message

Scenario: Incorrect but valid Insurer Code
Given I am on the Login screen
And I tap Register button
And Registration form is shown
And I have entered valid email
And I have entered correct password
And I have entered correct confirm password
And I have entered date of birth
And I have entered incorrect insurer code
When I tap on Register
Then I should get an incorrect code or email error message

Scenario: Already registered user
Given I am on the Login screen
And I tap Register button
And Registration form is shown
And I have entered existing email
And I have entered correct password
And I have entered correct confirm password
And I have entered date of birth
And I have entered existing insurer code
When I tap on Register
Then I should get an unable to register message

Scenario: Show registration screen with insurer code info
Given I am on the Login screen
When I tap Register button
Then Registration form is shown
And Information about how to receive Insurer code is displayed

Scenario: Insurer Code that has been used before
Given I am on the Login screen
And I tap Register button
And Registration form is shown
And I have entered correct email
And I have entered correct password
And I have entered correct confirm password
And I have entered date of birth
And I have entered existing insurer code
When I tap on Register
Then I should get an incorrect code or email error message

Scenario: Register link enabled with all valid fields
Given I am on the Registration screen
And I have entered all the correct details
Then the Register link becomes active

Scenario: Incorrect but valid Password
Given I am on the Login screen
And I tap Register button
And Registration form is shown
And I have entered correct email
And I have entered incorrect but valid password
And I have entered incorrect but valid confirm password
And I have entered date of birth
And I have entered valid insurer code
When I tap on Register
Then I should get an error message

Scenario: Register link disabled with no email
Given I am on the Login screen
And I tap Register button
And Registration form is shown
And I have not entered email address
And I have entered correct password
And I have entered correct confirm password
And I have entered date of birth
And I have entered correct insurer code
Then the register link should be disabled

Scenario: Register link disabled with no password
Given I am on the Login screen
And I tap Register button
And Registration form is shown
And I have entered correct email
And I have not entered password
And I have entered correct confirm password
And I have entered date of birth
And I have entered correct insurer code
Then the register link should be disabled

Scenario: Register link disabled with no confirm password
Given I am on the Login screen
And I tap Register button
And Registration form is shown
And I have entered correct email
And I have entered correct password
And I have not entered confirm password
And I have entered date of birth
And I have entered correct insurer code
Then the register link should be disabled

Scenario: Register link disabled with no insurer code
Given I am on the Login screen
And I tap Register button
And Registration form is shown
And I have entered correct email
And I have entered correct password
And I have entered correct confirm password
And I have entered date of birth
And I have not entered insurer code
Then the register link should be disabled

Scenario: Show home screen after successful registration
Given I am on the Registration screen
And I have entered all the correct details
And I tap on Register
And I agree to the terms
Then I should see Home screen

// REGEX USERNAME TESTS //

Scenario: Numeric and lower case email
Given I am on the Login screen
When I tap Register button
And Registration form is shown
And I have entered numeric and lowercase email
And I have entered correct password
And I have entered correct confirm password
And I have entered date of birth
And I have entered correct insurer code
Then I tap on Register

Scenario: Upper case email
Given I am on the Login screen
When I tap Register button
And Registration form is shown
And I have entered an email with an upper case
And I have entered correct password
And I have entered correct confirm password
And I have entered date of birth
And I have entered correct insurer code
Then I tap on Register

Scenario: Only upper case email
Given I am on the Login screen
When I tap Register button
And Registration form is shown
And I have entered an email with only upper case
And I have entered correct password
And I have entered correct confirm password
And I have entered date of birth
And I have entered correct insurer code
Then I tap on Register

Scenario: Email with only numerical characters
Given I am on the Login screen
When I tap Register button
And Registration form is shown
And I have entered an email with only numerical characters
And I have entered correct password
And I have entered correct confirm password
And I have entered date of birth
And I have entered correct insurer code
Then I tap on Register

Scenario: Upper case and numerical email
Given I am on the Login screen
When I tap Register button
And Registration form is shown
And I have entered an email with upper case and numerical characters
And I have entered correct password
And I have entered correct confirm password
And I have entered date of birth
And I have entered correct insurer code
Then I tap on Register

Scenario: Plus characted in email
Given I am on the Login screen
When I tap Register button
And Registration form is shown
And I have entered Plus character in email
And I have entered correct password
And I have entered correct confirm password
And I have entered date of birth
And I have entered correct insurer code
Then I tap on Register

Scenario: Hash character in email
Given I am on the Login screen
When I tap Register button
And Registration form is shown
And I have entered Hash character in email
And I have entered correct password
And I have entered correct confirm password
And I have entered date of birth
And I have entered correct insurer code
Then I tap on Register

Scenario: Underscore in email
Given I am on the Login screen
When I tap Register button
And Registration form is shown
And I have entered Underscore in email
And I have entered correct password
And I have entered correct confirm password
And I have entered date of birth
And I have entered correct insurer code
Then I tap on Register

Scenario: Dollar sign in email
Given I am on the Login screen
When I tap Register button
And Registration form is shown
And I have entered Dollar sign in email
And I have entered correct password
And I have entered correct confirm password
And I have entered date of birth
And I have entered correct insurer code
Then I tap on Register

Scenario: And sign in email
Given I am on the Login screen
When I tap Register button
And Registration form is shown
And I have entered And sign in email
And I have entered correct password
And I have entered correct confirm password
And I have entered date of birth
And I have entered correct insurer code
Then I tap on Register

Scenario: Percentage sign in email
Given I am on the Login screen
When I tap Register button
And Registration form is shown
And I have entered Percentage sign in email
And I have entered correct password
And I have entered correct confirm password
And I have entered date of birth
And I have entered correct insurer code
Then I tap on Register

Scenario: Semicolon character in email
Given I am on the Login screen
When I tap Register button
And Registration form is shown
And I have entered Semicolon character in email
And I have entered correct password
And I have entered correct confirm password
And I have entered date of birth
And I have entered correct insurer code
Then I tap on Register

Scenario: Star character in the email
Given I am on the Login screen
When I tap Register button
And Registration form is shown
And I have entered Star character in the email
And I have entered correct password
And I have entered correct confirm password
And I have entered date of birth
And I have entered correct insurer code
Then I tap on Register

Scenario: Hyphen in the email
Given I am on the Login screen
When I tap Register button
And Registration form is shown
And I have entered Hyphen in the email
And I have entered correct password
And I have entered correct confirm password
And I have entered date of birth
And I have entered correct insurer code
Then I tap on Register

Scenario: Forward slash in email
Given I am on the Login screen
When I tap Register button
And Registration form is shown
And I have entered Forward slash in email
And I have entered correct password
And I have entered correct confirm password
And I have entered date of birth
And I have entered correct insurer code
Then I tap on Register

Scenario: Equal sign in the email
Given I am on the Login screen
When I tap Register button
And Registration form is shown
And I have entered Equal sign in the email
And I have entered correct password
And I have entered correct confirm password
And I have entered date of birth
And I have entered correct insurer code
Then I tap on Register

Scenario: Question mark in the email
Given I am on the Login screen
When I tap Register button
And Registration form is shown
And I have entered Question mark in the email
And I have entered correct password
And I have entered correct confirm password
And I have entered date of birth
And I have entered correct insurer code
Then I tap on Register

Scenario: Caret character in email
Given I am on the Login screen
When I tap Register button
And Registration form is shown
And I have entered Caret character in email
And I have entered correct password
And I have entered correct confirm password
And I have entered date of birth
And I have entered correct insurer code
Then I tap on Register

Scenario: Grave accent in the email
Given I am on the Login screen
When I tap Register button
And Registration form is shown
And I have entered Grave accent in the email
And I have entered correct password
And I have entered correct confirm password
And I have entered date of birth
And I have entered correct insurer code
Then I tap on Register

Scenario: Curly brackets in the email
Given I am on the Login screen
When I tap Register button
And Registration form is shown
And I have entered Curly brackets in the email
And I have entered correct password
And I have entered correct confirm password
And I have entered date of birth
And I have entered correct insurer code
Then I tap on Register

Scenario: Line bar in the email
Given I am on the Login screen
When I tap Register button
And Registration form is shown
And I have entered Line bar in the email
And I have entered correct password
And I have entered correct confirm password
And I have entered date of birth
And I have entered correct insurer code
Then I tap on Register

Scenario: Tilde in the email
Given I am on the Login screen
When I tap Register button
And Registration form is shown
And I have entered Tilde in the email
And I have entered correct password
And I have entered correct confirm password
And I have entered date of birth
And I have entered correct insurer code
Then I tap on Register

Scenario: Email with only two local characters
Given I am on the Login screen
When I tap Register button
And Registration form is shown
And I have entered an email with two local characters
And I have entered correct password
And I have entered correct confirm password
And I have entered date of birth
And I have entered correct insurer code
Then I tap on Register

Scenario: Email with 64 local characters
Given I am on the Login screen
When I tap Register button
And Registration form is shown
And I have entered an email with 64 local characters
And I have entered correct password
And I have entered correct confirm password
And I have entered date of birth
And I have entered correct insurer code
Then I tap on Register

Scenario: Email with only lower case in domain
Given I am on the Login screen
When I tap Register button
And Registration form is shown
And I have entered an email with lower case domain
And I have entered correct password
And I have entered correct confirm password
And I have entered date of birth
And I have entered correct insurer code
Then I tap on Register

Scenario: Email with only upper case in domain
Given I am on the Login screen
When I tap Register button
And Registration form is shown
And I have entered an email with upper case domain
And I have entered correct password
And I have entered correct confirm password
And I have entered date of birth
And I have entered correct insurer code
Then I tap on Register

Scenario: Email with numerical in domain
Given I am on the Login screen
When I tap Register button
And Registration form is shown
And I have entered an email with numerical domain
And I have entered correct password
And I have entered correct confirm password
And I have entered date of birth
And I have entered correct insurer code
Then I tap on Register

Scenario: Email with uppercase and lower and numerical domain
Given I am on the Login screen
When I tap Register button
And Registration form is shown
And I have entered an email with uppercase, lowercase and numerical domain
And I have entered correct password
And I have entered correct confirm password
And I have entered date of birth
And I have entered correct insurer code
Then I tap on Register

Scenario: Email with 2 segments in domain
Given I am on the Login screen
When I tap Register button
And Registration form is shown
And I have entered an email with 2 segments in domain
And I have entered correct password
And I have entered correct confirm password
And I have entered date of birth
And I have entered correct insurer code
Then I tap on Register

Scenario: Email with 3 segments in domain
Given I am on the Login screen
When I tap Register button
And Registration form is shown
And I have entered an email with 3 segments in domain
And I have entered correct password
And I have entered correct confirm password
And I have entered date of birth
And I have entered correct insurer code
Then I tap on Register

Scenario: Email with 4 segments in domain
Given I am on the Login screen
When I tap Register button
And Registration form is shown
And I have entered an email with 4 segments in domain
And I have entered correct password
And I have entered correct confirm password
And I have entered date of birth
And I have entered correct insurer code
Then I tap on Register

Scenario: Email with 5 segments in domain
Given I am on the Login screen
When I tap Register button
And Registration form is shown
And I have entered an email with 5 segments in domain
And I have entered correct password
And I have entered correct confirm password
And I have entered date of birth
And I have entered correct insurer code
Then I tap on Register

Scenario: Email with 2 local and 247 domain characters
Given I am on the Login screen
When I tap Register button
And Registration form is shown
And I have entered an email with 2 local and 247 domain split into 4 segments
And I have entered correct password
And I have entered correct confirm password
And I have entered date of birth
And I have entered correct insurer code
Then I tap on Register

Scenario: Email with hyphen in domain
Given I am on the Login screen
When I tap Register button
And Registration form is shown
And I have entered an email with hyphen in domain
And I have entered correct password
And I have entered correct confirm password
And I have entered date of birth
And I have entered correct insurer code
Then I tap on Register

Scenario: Email with capital TLD
Given I am on the Login screen
When I tap Register button
And Registration form is shown
And I have entered an email with capital TLD
And I have entered correct password
And I have entered correct confirm password
And I have entered date of birth
And I have entered correct insurer code
Then I tap on Register

Scenario: Email with 2 TLD characters
Given I am on the Login screen
When I tap Register button
And Registration form is shown
And I have entered an email with 2 TLD characters
And I have entered correct password
And I have entered correct confirm password
And I have entered date of birth
And I have entered correct insurer code
Then I tap on Register

Scenario: Email with 24 TLD characters
Given I am on the Login screen
When I tap Register button
And Registration form is shown
And I have entered an email with 24 TLD characters
And I have entered correct password
And I have entered correct confirm password
And I have entered date of birth
And I have entered correct insurer code
Then I tap on Register
