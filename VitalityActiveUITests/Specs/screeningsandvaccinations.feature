@tag
Feature: Screenings and Vaccinations

Scenario: I can view SAV On Boarding screen
Given I have installed a new app
And I skip the intro
And login is shown
And I have entered SAV email
And I have entered a correct password
And I tap Login
And I agree to terms and conditions
And I tap next on user preferences
When I tap on SAV card
Then I see SAV onboarding modal
