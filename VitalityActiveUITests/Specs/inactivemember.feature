@tag
Feature: Inactive Member

Scenario: Inactive user is successfully registered
Given I am on the Login screen
And I tap Register button
And Registration form is shown
And I have entered new inactive member email
And I have entered new password
And I have entered new confirm password
And I have entered new inactive member date of birth
And I have entered new inactive member insurer code
When I tap on Register
Then I should be logged in successfully
And I should see the inactive membership screen

Scenario: Inactive membership screen is shown
Given I am on the Login screen
And I have entered a correct inactive member email
And I have entered a correct password
And I tap Login
And I should be logged in successfully
Then I should see the inactive membership screen

Scenario: Logout button is pressed
Given I am on the Login screen
And I have entered a correct inactive member email
And I have entered a correct password
And I tap Login
And I should be logged in successfully
And I should see the inactive membership screen
When I press logout
Then I should see a logout confirmation prompt

Scenario: Cancel Logout Inactive membership account
Given I am on the Login screen
And I have entered a correct inactive member email
And I have entered a correct password
And I tap Login
And I should be logged in successfully
And I should see the inactive membership screen
And I press logout
And I should see a logout confirmation prompt
Then I press cancel in logout prompt

Scenario: Logout Inactive membership account
Given I am on the Login screen
And I have entered a correct inactive member email
And I have entered a correct password
And I tap Login
And I should be logged in successfully
And I should see the inactive membership screen
And I press logout
And I should see a logout confirmation prompt
When I confirm logout
Then I should be back in login screen
