@tag
Feature: Privacy

Scenario: Log in the application
Given I have installed a new app
And I skip the intro
And Login is shown
And I have entered a correct account
And I have entered a correct password
And I tap Login
And I agree to terms and conditions
Then I tap next on user preferences

Scenario: Redirect to the Privacy screen
Given I have installed a new app
And I skip the intro
And Login is shown
And I have entered a correct account
And I have entered a correct password
And I tap Login
And I agree to terms and conditions
And I tap next on user preferences
And I go to Privacy screen
Then I am on the Privacy screen

Scenario: Redirect to the Privacy Statement page
Given I have installed a new app
And I skip the intro
And Login is shown
And I have entered a correct account
And I have entered a correct password
And I tap Login
And I agree to terms and conditions
And I tap next on user preferences
And I go to Privacy screen
When I am on the Privacy screen
And I tap the Privacy Statement link
Then I am on the Privacy Statement screen

Scenario: Check Analytics Preference toggle existence
Given I have installed a new app
And I skip the intro
And Login is shown
And I have entered a correct account
And I have entered a correct password
And I tap Login
And I agree to terms and conditions
And I tap next on user preferences
And I go to Privacy screen
When I am on the Privacy screen
Then I check if Privacy toggle is displayed

Scenario: Test Analytics Preference toggle
Given I have installed a new app
And I skip the intro
And Login is shown
And I have entered a correct account
And I have entered a correct password
And I tap Login
And I agree to terms and conditions
And I tap next on user preferences
And I go to Privacy screen
When I am on the Privacy screen
Then I tap Analytics toggle

Scenario: Check Crash Reports Preference toggle existence
Given I have installed a new app
And I skip the intro
And Login is shown
And I have entered a correct account
And I have entered a correct password
And I tap Login
And I agree to terms and conditions
And I tap next on user preferences
And I go to Privacy screen
When I am on the Privacy screen
Then I check if Crash Reports toggle is displayed

Scenario: Test Crash Reports Preference toggle
Given I have installed a new app
And I skip the intro
And Login is shown
And I have entered a correct account
And I have entered a correct password
And I tap Login
And I agree to terms and conditions
And I tap next on user preferences
And I go to Privacy screen
When I am on the Privacy screen
Then I tap Crash Reports toggle
