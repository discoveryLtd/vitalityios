@tag
Feature: Change Username

Scenario: Logged in succesfully
Given I am on the Login screen
When I provide credentials for successful login
Then I should be logged in successfully

Scenario: Navigate to Profile
Given I am on the Login screen
And I provide credentials for successful login
And I should be logged in successfully
When I tap profile
Then I should be in profile screen

Scenario: Navigate to Personal Details
Given I am on the Login screen
And I provide credentials for successful login
And I should be logged in successfully
And I tap profile
And I should be in profile screen
When I tap on my account
Then I should see the personal details screen

Scenario: Navigate to Change Email
Given I am on the Login screen
And I provide credentials for successful login
And I should be logged in successfully
And I tap profile
And I should be in profile screen
And I tap on my account
And I should see the personal details screen
When I tap on my email
Then I should see the change email screen

Scenario: Cancel Change Email
Given I am on the Login screen
And I provide credentials for successful login
And I should be logged in successfully
And I tap profile
And I should be in profile screen
And I tap on my account
And I should see the personal details screen
And I tap on my email
And I should see the change email screen
When I tap on cancel
Then I should see the personal details screen

Scenario: Change to New Email
Given I am on the Login screen
And I provide credentials for successful login
And I should be logged in successfully
And I tap profile
And I should be in profile screen
And I tap on my account
And I should see the personal details screen
And I tap on my email
And I should see the change email screen
And I entered new email
And I press Done
When I press continue in prompt
And I confirm with password
Then I succesfully changed my email

Scenario: Email already used
Given I am on the Login screen
And I provide credentials for successful login
And I should be logged in successfully
And I tap profile
And I should be in profile screen
And I tap on my account
And I should see the personal details screen
And I tap on my email
And I should see the change email screen
And I entered the already used email
When I press Done
Then I should see unable to change email prompt
