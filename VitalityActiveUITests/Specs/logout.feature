@tag
Feature: Settings

Scenario: Log Out row is present
Given I am on the Login screen
And I have entered a correct email
And I have entered a correct password
And I tap Login
And I agree to terms and conditions
And I tap next on user preferences
When I navigate to Log Out
Then Log Out row is present

Scenario: Cancel log out
Given I am on the Login screen
And I have entered a correct email
And I have entered a correct password
And I tap Login
And I agree to terms and conditions
And I tap next on user preferences
And I navigate to Log Out
When I select Cancel
#Then I should be back at the Login screen

Scenario: Log out success
Given I am on the Login screen
And I have entered a correct email
And I have entered a correct password
And I tap Login
And I agree to terms and conditions
And I tap next on user preferences
And I navigate to Log Out
When I select Log Out
Then I should be back at the Login screen
