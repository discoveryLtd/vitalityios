@tag
Feature: Communication Preferences

Scenario: Communication Preferences row is present
Given I am on the Login screen
And I have entered a correct email
And I have entered a correct password
And I tap Login
And I agree to terms and conditions
And I tap next on user preferences
And I tap Profile Tab
When I tap Settings
Then Communication Preferences row is present

Scenario: Toggle email communication
Given I am on the Login screen
And I have entered a correct email
And I have entered a correct password
And I tap Login
And I agree to terms and conditions
And I tap next on user preferences
And I tap Profile Tab
And I tap Settings
And I tap Communication Preferences
And I toggle Email communication

Scenario: Toggle push notifications
Given I am on the Login screen
And I have entered a correct email
And I have entered a correct password
And I tap Login
And I agree to terms and conditions
And I tap next on user preferences
And I tap Profile Tab
And I tap Settings
And I tap Communication Preferences
And I toggle Push Notifications
#And I tap Allow

Scenario: Manage in the Settings app
Given I am on the Login screen
And I have entered a correct email
And I have entered a correct password
And I tap Login
And I agree to terms and conditions
And I tap next on user preferences
And I tap Profile Tab
And I tap Settings
And I tap Communication Preferences
When I tap Manage in the Settings app
#Then I should be on the app's Settings

Scenario: Return back to the app
Given I am on the Login screen
And I have entered a correct email
And I have entered a correct password
And I tap Login
And I agree to terms and conditions
And I tap next on user preferences
And I tap Profile Tab
And I tap Settings
And I tap Communication Preferences
And I tap Manage in the Settings app
When I tap Return to Vitality
#Then I should be on Communication Preferences

