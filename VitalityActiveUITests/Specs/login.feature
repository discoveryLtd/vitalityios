@tag
Feature: Login

Scenario: Log in success
Given I am on the Login screen
And I have entered a correct email
And I have entered a correct password
When I tap Login
Then I should be logged in successfully

Scenario: Incorrect email and password
Given I am on the Login screen
And I have entered an incorrect email
And I have entered an incorrect password
When I tap Login
Then I should get an incorrect email or password message

Scenario: Valid email and Incorrect password
Given I am on the Login screen
And I have entered a valid email
And I have entered an incorrect password
When I tap Login
Then I should get an incorrect email or password message

Scenario: Incorrect email and correct password
Given I am on the Login screen
And I have entered an incorrect email
And I have entered a correct password
When I tap Login
Then I should get an incorrect email or password message

Scenario: Empty email
Given I am on the Login screen
When I have not entered an email
And I have entered a correct password
Then I can not tap Login

Scenario: Empty password
Given I am on the Login screen
When I have entered a valid email
And I have not entered a password
Then I can not tap Login

Scenario: Empty email and password
Given I am on the Login screen
Then I can not tap Login

Scenario: Forgot password prompt
Given I am on the Login screen
And I have entered a valid email
And I have entered an incorrect password
When I tap Login
Then I should get an incorrect email or password message
And I tap Ok
And I tap Login
And I should see a Forgot Password prompt

Scenario: Navigate to Forgot Password Screen
Given I am on the Login screen
And I have entered a valid email
And I have entered an incorrect password
And I tap Login
And I should get an incorrect email or password message
And I tap Ok
And I tap Login
And I should see a Forgot Password prompt
When I tap on Forgot Password
Then Forgot Password screen is displayed

Scenario: To check that I am navigated to forgot password when tapping on Forgot Password
Given I am on the Login screen
When I tap on the forgot password on login screen
Then Forgot Password screen is displayed

Scenario: To check entering valid email address for forgot password
Given I am on the Login screen
And I have entered a valid email
And I have entered an incorrect password
And I tap Login
And I should get an incorrect email or password message
And I tap Ok
And I tap Login
And I should see a Forgot Password prompt
When I tap on Forgot Password
Then The Done button becomes enabled

Scenario: To check successful submission of Email address for Forgot Password
Given I am on the Login screen
And I tap on the forgot password on login screen
And Forgot Password screen is displayed
And I entered email in forgot password
When I tap done
Then I should see a confirmation that an email has been sent

Scenario: Email not registered with forgot password
Given I am on the Login screen
And I have entered a not registered email
And I have entered an incorrect password
And I tap Login
And I should get an incorrect email or password message
And I tap Ok
And I tap Login
And I should see a Forgot Password prompt
When I tap on Forgot Password
And I tap Done
Then I see a not registered alert
And I tap Ok

