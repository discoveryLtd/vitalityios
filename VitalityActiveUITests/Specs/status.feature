@tag
Feature: Status

Scenario: I can view an Account's Status
Given I have installed a new app
And I skip the intro
And login is shown
And I have entered a username for account with Bronze status
And I have entered a correct password
And I tap Login
And I agree to terms and conditions
And I tap next on user preferences
And I am on the home screen
And I tap on Vitality Status
And I tap on Onboarding Screen's got it button to proceed
Then I should see Vitality Status screen

Scenario: Navigate to Earning Points Information of Vitality Status
Given I have navigated to a new user home screen
And I tap on Vitality Status
And I tap on Onboarding Screen's got it button to proceed
And I should see Vitality Status screen
And I tap Assessments button
And I navigate throughout Assessments information
And I tap Nutrition button
Then I navigate throughout Nutrition information

Scenario: Navigate to Get Active Information
Given I have navigated to a new user home screen
And I tap on Vitality Status
And I tap on Onboarding Screen's got it button to proceed
And I should see Vitality Status screen
And I tap Get Active button
Then I navigate throughout Get Active information

Scenario: Navigate to Helper Screen of Vitality Status
Given I have navigated to a new user home screen
And I tap on Vitality Status
And I tap on Onboarding Screen's got it button to proceed
And I should see Vitality Status screen
Then I tap Help button
