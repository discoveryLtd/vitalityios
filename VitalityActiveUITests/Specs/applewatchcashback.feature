@tag
Feature: Apple Watch Cashback

Scenario: Apple Watch Cashback Learn More
Given I have logged in with user that has AppleCashback card
And I tap AWC card
And I tap learn more on AWC onboarding screen
Then I should see AWC learn more screen

Scenario: Apple Watch Cashback Help
Given I have logged in with user that has AppleCashback card
And I tap AWC card
And I click Got it on AWC onboarding screen
And I click the AWC help button
Then I should be redirected to AWC help

Scenario: Apple Watch Cashback How to track vitality coin
Given I have logged in with user that has AppleCashback card
And I tap AWC card
And I click Got it on AWC onboarding screen
And I click on how to track Vitality Coin
Then I should be redirected to how to track Vitality Coin screen
