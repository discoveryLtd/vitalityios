
@tag
Feature: Organized Fitness Events

Scenario: OFE on-boaridng modal is displayed when user clicks on MWB card on home screen
Given I have navigated to an OFE user home screen
When I tap on OFE card
Then I see OFE onboarding modal

Scenario: OFE Learn More link is displayed under Get Started button
Given I have navigated to an OFE user home screen
And I tap on OFE card
When I see OFE onboarding modal
Then I will see the OFE Learn More button

Scenario: Check if user is able to tap on OFE Learn More
Given I have navigated to an OFE user home screen
And I tap on OFE card
When I see OFE onboarding modal
Then I tap OFE Learn More

Scenario: I can view the learn more screen from OFE onboarding screen
Given I have navigated to an OFE user home screen
And I tap on OFE card
And I see OFE onboarding modal
When I tap OFE Learn More
Then The OFE learn more will show

Scenario: Check if user is able to return to Onboarding screen by taping on Back button of OFE Learn More screen
Given I have navigated to an OFE user home screen
And I tap on OFE card
And I see OFE onboarding modal
And I tap OFE Learn More
When I tap on back button
Then I see OFE onboarding modal

Scenario: Check if user able to start OFE by taping on Got it
Given I have navigated to an OFE user home screen
And I tap on OFE card
And I see OFE onboarding modal
When I tap OFE Got It
Then I will be navigated to OFE landing screen

Scenario: Navigate to Events and Points Screen
Given I have navigated to an OFE user home screen
And I tap on OFE card
And I see OFE onboarding modal
And I tap OFE Got It
When I tap to Events and Points cell
Then I should see Events and Points Screen

Scenario: Navigate to History Screen
Given I have navigated to an OFE user home screen
And I tap on OFE card
And I see OFE onboarding modal
And I tap OFE Got It
When I tap to History cell
Then I should see OFE History Screen

Scenario: Check if Learn More cell is visible
Given I have navigated to an OFE user home screen
And I tap on OFE card
And I see OFE onboarding modal
And I tap OFE Got It
Then I should see Learn More cell

Scenario: Check if Help cell is visible
Given I have navigated to an OFE user home screen
And I tap on OFE card
And I see OFE onboarding modal
And I tap OFE Got It
Then I should see Help cell

Scenario: Navigate to Claim Points Screen
Given I have navigated to an OFE user home screen
And I tap on OFE card
And I see OFE onboarding modal
And I tap OFE Got It
When I tap to Claim Points
Then I should see Claim Points Screen

Scenario: Navigate to Event Type Screen
Given I have navigated to an OFE user home screen
And I tap on OFE card
And I see OFE onboarding modal
And I tap OFE Got It
And I tap to Claim Points
When I tap to Event Type
Then I should see Event Type Screen

Scenario: Test Search Events
Given I have navigated to an OFE user home screen
And I tap on OFE card
And I see OFE onboarding modal
And I tap OFE Got It
And I tap to Claim Points
And I tap to Event Type
When I type in Search textbox
Then I should see No Results label

Scenario: Test Cancel button on Search in menu bar
Given I have navigated to an OFE user home screen
And I tap on OFE card
And I see OFE onboarding modal
And I tap OFE Got It
And I tap to Claim Points
And I tap to Event Type
And I type in Search textbox
Then I press cancel in Search textbox

Scenario: Choose an Event type
Given I have navigated to an OFE user home screen
And I tap on OFE card
And I see OFE onboarding modal
And I tap OFE Got It
And I tap to Claim Points
And I tap to Event Type
When I choose an Event Type
Then I should be back to Claim Points Screen

Scenario: Set Distance in Claim Points Screen
Given I have navigated to an OFE user home screen
And I tap on OFE card
And I see OFE onboarding modal
And I tap OFE Got It
And I tap to Claim Points
And I tap to Event Type
And I choose an Event Type
And I should be back to Claim Points Screen
And I tap Distance
And I should see Distance Screen
When I choose a given Distance
Then I should be back to Claim Points Screen

Scenario: Show Event Date Pop up
Given I have navigated to an OFE user home screen
And I tap on OFE card
And I see OFE onboarding modal
And I tap OFE Got It
And I tap to Claim Points
And I tap to Event Type
And I choose an Event Type
And I should be back to Claim Points Screen
Then I tap Event Date

Scenario: Press Cancel in Event Date Pop up
Given I have navigated to an OFE user home screen
And I tap on OFE card
And I see OFE onboarding modal
And I tap OFE Got It
And I tap to Claim Points
And I tap to Event Type
And I choose an Event Type
And I should be back to Claim Points Screen
And I tap Event Date
And I should see Event Date Pop up
Then I press Cancel in Event Date Pop up

Scenario: Press Ok in Event Date Pop up
Given I have navigated to an OFE user home screen
And I tap on OFE card
And I see OFE onboarding modal
And I tap OFE Got It
And I tap to Claim Points
And I tap to Event Type
And I choose an Event Type
And I should be back to Claim Points Screen
And I tap Event Date
And I should see Event Date Pop up
Then I press Ok in Event Date Pop up

Scenario: Navigate to Image Proof Screen
Given I have navigated to an OFE user home screen
And I tap on OFE card
And I see OFE onboarding modal
And I tap OFE Got It
And I tap to Claim Points
And I tap to Event Type
And I choose an Event Type
And I should be back to Claim Points Screen
And I tap Distance
And I should see Distance Screen
And I choose a given Distance
And I add Event Name
And I add Organiser
When I press Next in Claim Points Screen
Then I should be navigated to Image Proof Screen

Scenario: Add Image Proof and navigate to Weblink Proof
Given I have navigated to an OFE user home screen
And I tap on OFE card
And I see OFE onboarding modal
And I tap OFE Got It
And I tap to Claim Points
And I tap to Event Type
And I choose an Event Type
And I should be back to Claim Points Screen
And I tap Distance
And I should see Distance Screen
And I choose a given Distance
And I add Event Name
And I add Organiser
And I press Next in Claim Points Screen
And I should be navigated to Image Proof Screen
And I press Add
And I press Choose from Library
And I press Allow
And I choose an Image
When I press Next in Images
Then I should see Weblink Proof Screen

Scenario: Add Weblink Proof and navigate to Summary
Given I have navigated to an OFE user home screen
And I tap on OFE card
And I see OFE onboarding modal
And I tap OFE Got It
And I tap to Claim Points
And I tap to Event Type
And I choose an Event Type
And I should be back to Claim Points Screen
And I tap Distance
And I should see Distance Screen
And I choose a given Distance
And I add Event Name
And I add Organiser
And I press Next in Claim Points Screen
And I should be navigated to Image Proof Screen
And I press Add
And I press Choose from Library
And I press Allow
And I choose an Image
And I press Next in Images
And I should see Weblink Proof Screen
And I add Website Link
When I press Next in Weblink
Then I should see Summary Screen

Scenario: Submit an Event
Given I have navigated to an OFE user home screen
And I tap on OFE card
And I see OFE onboarding modal
And I tap OFE Got It
And I tap to Claim Points
And I tap to Event Type
And I choose an Event Type
And I should be back to Claim Points Screen
And I tap Distance
And I should see Distance Screen
And I choose a given Distance
And I add Event Name
And I add Organiser
And I press Next in Claim Points Screen
And I should be navigated to Image Proof Screen
And I press Add
And I press Choose from Library
And I press Allow
And I choose an Image
And I press Next in Images
And I should see Weblink Proof Screen
And I add Website Link
And I press Next in Weblink
And I should see Summary Screen
And I press Confirm
When I press Agree in Agreement
Then I should see Fitness Event Submitted modal

