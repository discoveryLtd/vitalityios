@tag
Feature: Screenings and Vaccination

Scenario: I can tap Screenings and Vaccination card
Given I have installed a new app
And I skip the intro
And login is shown
And I have entered a VHC username
And I have entered a correct password
And I tap Login
And I agree to terms and conditions
And I tap next on user preferences
When I tap on SV card
Then Learn More screen is shown ####### And SV screen is shown #######

Scenario: I can navigate back from Screenings and Vaccination
Given I have installed a new app
And I skip the intro
And login is shown
And I have entered a VHC username
And I have entered a correct password
And I tap Login
And I agree to terms and conditions
And I tap next on user preferences
And I tap on SV card
And Learn More screen is shown ####### And SV screen is shown #######
When I tap Back
Then SV card is shown
