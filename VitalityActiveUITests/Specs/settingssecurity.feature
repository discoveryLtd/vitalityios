@tag
Feature: Security

Scenario: Log in the application
Given I have installed a new app
And I skip the intro
And Login is shown
And I have entered a correct account
And I have entered a correct password
And I tap Login
When I agree to terms and conditions
Then I tap next on user preferences

Scenario: Redirect to the Security screen
Given I have installed a new app
And I skip the intro
And Login is shown
And I have entered a correct account
And I have entered a correct password
And I tap Login
And I agree to terms and conditions
And I tap next on user preferences
When I go to Security screen
Then I am on the Security screen

Scenario: Check Touch ID toggle existence
Given I have installed a new app
And I skip the intro
And Login is shown
And I have entered a correct account
And I have entered a correct password
And I tap Login
And I agree to terms and conditions
And I tap next on user preferences
And I go to Security screen
When I am on the Security screen
Then I check if Touch ID toggle is displayed

Scenario: Test Touch ID Preference toggle
Given I have installed a new app
And I skip the intro
And Login is shown
And I have entered a correct account
And I have entered a correct password
And I tap Login
And I agree to terms and conditions
And I tap next on user preferences
And I go to Security screen
When I am on the Security screen
Then I tap Touch ID toggle

Scenario: Test Touch ID pop up
Given I have installed a new app
And I skip the intro
And Login is shown
And I have entered a correct account
And I have entered a correct password
And I tap Login
And I agree to terms and conditions
And I tap next on user preferences
And I go to Security screen
And I am on the Security screen
When I tap Touch ID toggle
Then I can see the Touch ID pop up

Scenario: Check Remember Me toggle existence
Given I have installed a new app
And I skip the intro
And Login is shown
And I have entered a correct account
And I have entered a correct password
And I tap Login
And I agree to terms and conditions
And I tap next on user preferences
And I go to Security screen
When I am on the Security screen
Then I check if Remember Me toggle is displayed

Scenario: Test Remember Me toggle
Given I have installed a new app
And I skip the intro
And Login is shown
And I have entered a correct account
And I have entered a correct password
And I tap Login
And I agree to terms and conditions
And I tap next on user preferences
And I go to Security screen
When I am on the Security screen
Then I tap Remember Me toggle

Scenario: Check Change Password cell existence
Given I have installed a new app
And I skip the intro
And Login is shown
And I have entered a correct account
And I have entered a correct password
And I tap Login
And I agree to terms and conditions
And I tap next on user preferences
And I go to Security screen
When I am on the Security screen
Then I check if the Change Password cell is displayed

Scenario: Redirect to the Change Password screen
Given I have installed a new app
And I skip the intro
And Login is shown
And I have entered a correct account
And I have entered a correct password
And I tap Login
And I agree to terms and conditions
And I tap next on user preferences
And I go to Security screen
When I am on the Security screen
Then I tap the Change Password cell

Scenario: Check error of non matching passwords
Given I have installed a new app
And I skip the intro
And Login is shown
And I have entered a correct account
And I have entered a correct password
And I tap Login
And I agree to terms and conditions
And I tap next on user preferences
And I go to Security screen
When I am on the Security screen
When I tap the Change Password cell
And I input non matching passwords
Then I will see the non matching password error

Scenario: Check change password confirmation
Given I have installed a new app
And I skip the intro
And Login is shown
And I have entered a correct account
And I have entered a correct password
And I tap Login
And I agree to terms and conditions
And I tap next on user preferences
And I go to Security screen
When I am on the Security screen
When I tap the Change Password cell
And I input correct passwords
Then I will see the change password confirmation
