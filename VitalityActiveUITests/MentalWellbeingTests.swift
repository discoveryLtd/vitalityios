//
//  MentalWellbeingTests.swift
//  VitalityActive
//
//  Created by Steven Layug on 3/21/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import XCTest
@testable import VIACore
@testable import VitalityKit
@testable import VIAUIKit

class MentalWellbeingTests: XCTestCase {
    
    func scenario(_ scenario: String) {
        NativeRunner(featureFile: "mentalwellbeing.feature", testCase: self).run(scenario: scenario)
        
    }
    
    func testMWBOnboardingFirstTime() {
        scenario("MWB on-boaridng modal is displayed when user clicks on MWB card on home screen")
        /*
         
         1st
         XCUIApplication().collectionViews.staticTexts["Not applicable"].tap()
         XCUIApplication().collectionViews.children(matching: .cell).element(boundBy: 10).staticTexts["Not applicable"].tap()
         XCUIApplication().collectionViews.children(matching: .cell).element(boundBy: 4).staticTexts["Not applicable"].tap()
         XCUIApplication().collectionViews.children(matching: .cell).element(boundBy: 9).staticTexts["Not applicable"].tap()
         
         let collectionViewsQuery = XCUIApplication().collectionViews
         collectionViewsQuery.children(matching: .cell).element(boundBy: 11).staticTexts["Not applicable"].tap()
         collectionViewsQuery.buttons["next_button_title_84"].tap()
         
         
         
         2nd
         XCUIApplication().collectionViews.staticTexts["Not applicable"].tap()
         XCUIApplication().collectionViews.children(matching: .cell).element(boundBy: 6).children(matching: .other).element.tap()
         XCUIApplication().collectionViews.children(matching: .cell).element(boundBy: 3).staticTexts["Not applicable"].tap()
         XCUIApplication().collectionViews.children(matching: .cell).element(boundBy: 9).staticTexts["Not applicable"].tap()
         
         let collectionViewsQuery = XCUIApplication().collectionViews
         collectionViewsQuery.children(matching: .cell).element(boundBy: 10).staticTexts["Not applicable"].tap()
         collectionViewsQuery.buttons["next_button_title_84"].tap()
         
         3rd
         XCUIApplication().collectionViews.children(matching: .cell).element(boundBy: 6).children(matching: .other).element.children(matching: .other).element.tap()
         
         let collectionViewsQuery = XCUIApplication().collectionViews
         collectionViewsQuery.children(matching: .cell).element(boundBy: 5).children(matching: .other).element.children(matching: .other).element.tap()
         collectionViewsQuery.children(matching: .cell).element(boundBy: 10).children(matching: .other).element.children(matching: .other).element.tap()
         
         let collectionViewsQuery = XCUIApplication().collectionViews
         collectionViewsQuery.children(matching: .cell).element(boundBy: 6).children(matching: .other).element.children(matching: .other).element.tap()
         collectionViewsQuery.children(matching: .cell).element(boundBy: 11).children(matching: .other).element.children(matching: .other).element.tap()
         
         let collectionViewsQuery = XCUIApplication().collectionViews
         collectionViewsQuery.children(matching: .cell).element(boundBy: 11).children(matching: .other).element.children(matching: .other).element.tap()
         collectionViewsQuery.buttons["done_button_title_53"].tap()
         
         sleep(5)
         
         XCUIApplication().toolbars.buttons["agree_button_title_50"].tap()
         
         sleep(13)

         
 */
    }
    
    func testLearnMoreButtonIsVisible() {
        scenario("MWB Learn More link is displayed under Get Started button")
    }
    
    func testMWBLearnMoreButton() {
        scenario("Check if user is able to tap on MWB Learn More")
    }
    
    func testMWBLearnMoreScreen(){
        scenario("I can view the learn more screen from MWB onboarding screen")
    }
    
    func testNavigateBackToOnboardingFromLearnMore() {
        scenario("Check if user is able to return to Onboarding screen by taping on Back button of MWB Learn More screen")
    }
    
    func testGotItAndLandingScreen() {
        scenario("Check if user able to start MWB by taping on Got it")
    }
    
    func testStressorAssessmentCell() {
        scenario("Check if user will be able to see the Stressor Assessment cell")
        
    }
    
    func testPsychologicalAssessmentCell() {
        scenario("Check if user will be able to see the Psychological Assessment cell")
    }
    
    func testSocialAssessmentCell() {
        scenario("Check if user will be able to see the Social Assessment cell")
    }
    
    func testMwbScreenLearnMoreCell() {
        scenario("Check if user will be able to see the Learn More cell")
    }
    
    func testMwbScreenHelpCell() {
        scenario("Check if user will be able to see the Help cell")
    }
    
    func testStressorAssessment() {
        scenario("Check if user will be able to complete the Stressor Assessment")
    }
    
    func testPsychologicalAssessment() {
        scenario("Check if user will be able to complete the Psychological Assessment")
    }
    
    func testSocialAssessment() {
        scenario("Check if user will be able to complete the Social Assessment")
    }

}
