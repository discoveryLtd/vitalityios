//
//  MembershipPassTests.swift
//  VitalityActive
//
//  Created by Val Tomol on 16/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import XCTest
import VitalityKit
@testable import XCTest_Gherkin
@testable import VIACore

class MembershipTests: XCTestCase {
    func scenario(_ scenario: String) {
        NativeRunner(featureFile: "membership.feature", testCase: self).run(scenario: scenario)
    }
    
    func testViewMembershipPass() {
        scenario("View membership pass")
    }
    
    func testViewVitalityNumberInformation() {
        scenario("View vitality number information")
    }
    
    func testAddDigitalPassImageLater() {
        scenario("Add digital pass image later")
    }
    
    // TODO: Implement this once adding images when adding Digital Pass is implemented.
    //    func testAddDigitalPassImageNow() {
    //        scenario("Add digital pass image now")
    //    }
    
    func testRemovePass() {
        scenario("Remove pass")
    }
    
    // TODO: Implement assertions. Impediment: Don't know how to detect UIElements outside the app.
    //    func testViewPassInWallet() {
    //        scenario("View pass in wallet")
    //    }
}
