//
//  CommonSteps.swift
//  VitalityActive
//
//  Created by Val Tomol on 18/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import XCTest
import VitalityKit
@testable import XCTest_Gherkin
@testable import VIACore

class SettingsCommonSteps: StepDefiner {
    
    override func defineSteps() {
        
        step("I tap Profile Tab") {
            let app = XCUIApplication()
            let button = app.tabBars.buttons["Profile"]
            self.test.waitFor10Secs(element: button)
            button.tap()
        }
    }
}
