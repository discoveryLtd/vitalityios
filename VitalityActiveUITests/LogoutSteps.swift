//
//  LogoutSteps.swift
//  VitalityActive
//
//  Created by Dexter Anthony Ambrad on 12/16/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import XCTest
import VitalityKit
@testable import XCTest_Gherkin
@testable import VIACore

class LogoutStep: StepDefiner {
        
    override func defineSteps() {
        step("I tap Log Out") {
            let app = XCUIApplication()
            let tableRow = app.tables.staticTexts[ProfileStrings.SettingsLogoutTitle]
            self.test.waitFor10Secs(element: tableRow)
            tableRow.tap()
        }

        step("I select Log Out") {
            let app = XCUIApplication()
            let alertButton = app.alerts[ProfileStrings.SettingsLogoutTitle].buttons[ProfileStrings.SettingsLogoutTitle]
            self.test.waitFor10Secs(element: alertButton)
            alertButton.tap()
        }

        step("I select Cancel") {
            let app = XCUIApplication()
            let alertButton = app.alerts[ProfileStrings.SettingsLogoutTitle].buttons[CommonStrings.CancelButtonTitle24]
            self.test.waitFor10Secs(element: alertButton)
            alertButton.tap()
        }

        step("I navigate to Log Out") {
            self.step("And I tap Profile Tab")
            self.step("And I tap Settings")
            self.step("And I tap Log Out")
        }

        step("I should be back at the Login screen") {
            let app = XCUIApplication()
            let loginButton = app.scrollViews.otherElements.tables.buttons[LoginStrings.Login.LoginButtonTitle20] // login.login_button_title_20
            XCTAssert(loginButton.exists, "Login screen is not being displayed.")
        }

        step("Log Out row is present") {
            let app = XCUIApplication()
            let tableRow = app.tables.staticTexts[ProfileStrings.SettingsLogoutTitle]
            self.test.waitFor10Secs(element: tableRow)
            //XCTAssertTrue(expression: Bool) // TODO: Assert if image/icon is present/correct. Still have to figure out how to get the XCUIElement of an image/imageView
            XCTAssertTrue(tableRow.exists, "Log Out row is not present")
        }
    }
}
