//
//  MentalWellbeingSteps.swift
//  VitalityActive
//
//  Created by Steven Layug on 3/21/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import XCTest
@testable import VIACore
@testable import VitalityKit
@testable import XCTest_Gherkin

class MentalWellbeingSteps: StepDefiner {
    
    override func defineSteps() {

        step("I tap on MWB card") {
            let app = XCUIApplication()
            let collectionViewsQuery = app.collectionViews
            
            let VHRCard = collectionViewsQuery.collectionViews.staticTexts["home_card.card_section_title_291"]
            if VHRCard.waitForExistence(timeout: 5) {
                VHRCard.swipeLeft()
            }

            let VNACard = collectionViewsQuery.collectionViews.staticTexts["home_card.card_section_title_388"]
            if VNACard.waitForExistence(timeout: 5) {
                VNACard.swipeLeft()
            }

            let MWBCard = collectionViewsQuery.collectionViews.staticTexts["onboarding.title_1195"]
            MWBCard.tap()
//
//            let NSDCard = collectionViewsQuery.collectionViews.staticTexts["home_card.card_title_96"]
//            if NSDCard.waitForExistence(timeout: 2) {
//                NSDCard.swipeLeft()
//            }
//
//            let VHCCard = collectionViewsQuery.collectionViews.staticTexts["home_card.card_title_125"]
//            if VHCCard.waitForExistence(timeout: 2) {
//                VHCCard.swipeLeft()
//            }
//
//            let SNVCard = collectionViewsQuery.collectionViews.staticTexts["home_card.card_section_title_365"]
//            if SNVCard.waitForExistence(timeout: 2) {
//                SNVCard.swipeLeft()
//            }
//
//            let healthPartnersCard = collectionViewsQuery.collectionViews.staticTexts["card_heading_health_partners_843"]
//            if healthPartnersCard.waitForExistence(timeout: 2) {
//                healthPartnersCard.swipeLeft()
//            }
//
//            let MWBCard = collectionViewsQuery.collectionViews.staticTexts["onboarding.title_1195"]
//            MWBCard.tap()
        }
        
        step("I see MWB onboarding modal") {
            sleep(5)
            let app = XCUIApplication()
            let tablesQuery = app.tables
            let MWBHeader = tablesQuery.staticTexts["onboarding.title_1195"]
            self.test.waitFor10Secs(element: MWBHeader)
            XCTAssert(MWBHeader.exists, "MWB onboarding modal not shown")
        }
        
        step("I tap MWB Learn More") {
            let app = XCUIApplication()
            let tablesQuery = app.tables
            let learnMore = tablesQuery.buttons["learn_more_button_title_104"]
            learnMore.tap()
        }
        
        step("The MWB learn more will show") {
            sleep(5)
            let app = XCUIApplication()
            let learnMoreLanding = app.navigationBars["learn_more_button_title_104"]
            let failureReason = "Learn More Screen is not displayed"
            XCTestCase.VitalityHelper.waitForElementToAppear(test: self.test, element: learnMoreLanding)
            XCTAssertTrue(learnMoreLanding.exists, failureReason)
        }
        
        step("I tap MWB Got It") {
            let app = XCUIApplication()
            let tablesQuery = app.tables
            let gotIt = tablesQuery.buttons["generic_got_it_button_title_131"]
            gotIt.tap()
        }
        
        step("I will be navigated to MWB landing screen") {
            let app = XCUIApplication()
            let landingScreen = app.navigationBars["onboarding.title_1195"]
            XCTestCase.VitalityHelper.waitForElementToAppear(test: self.test, element: landingScreen)
            let failureReason = "MWB landing screen not displayed"
            XCTAssertTrue(landingScreen.exists, failureReason)
        }
        
        step("I will see the MWB Learn More button") {
            sleep(5)
            let app = XCUIApplication()
            let tablesQuery = app.tables
            let learnMore = tablesQuery.buttons["learn_more_button_title_104"]
            XCTestCase.VitalityHelper.waitForElementToAppear(test: self.test, element: learnMore)
            let failureReason = "MWB Learn More button not displayed"
            XCTAssertTrue(learnMore.exists, failureReason)
        }

        step("I will see Stressor Assessment") {
            sleep(5)
            let app = XCUIApplication()
            let tablesQuery = app.tables
            let stressor = tablesQuery.staticTexts["Stressor"]
            XCTestCase.VitalityHelper.waitForElementToAppear(test: self.test, element: stressor)
            let failureReason = "Stressor button not displayed"
            XCTAssertTrue(stressor.exists, failureReason)
            
        }
        
        step("I will see Psychological Assessment") {
            sleep(5)
            let app = XCUIApplication()
            let tablesQuery = app.tables
            let psychological = tablesQuery.staticTexts["Psychological"]
            XCTestCase.VitalityHelper.waitForElementToAppear(test: self.test, element: psychological)
            let failureReason = "Psychological button not displayed"
            XCTAssertTrue(psychological.exists, failureReason)
        }
        
        step("I will see Social Assessment") {
            sleep(5)
            let app = XCUIApplication()
            let tablesQuery = app.tables
            let social = tablesQuery.staticTexts["Social"]
            XCTestCase.VitalityHelper.waitForElementToAppear(test: self.test, element: social)
            let failureReason = "Social button not displayed"
            XCTAssertTrue(social.exists, failureReason)
        }
        
        step("I will see Learn More button on MWB screen"){
            sleep(5)
            let app = XCUIApplication()
            let tablesQuery = app.tables
            let learnMore = tablesQuery.staticTexts["learn_more_button_title_104"]
            XCTestCase.VitalityHelper.waitForElementToAppear(test: self.test, element: learnMore)
            let failureReason = "Learn More button not displayed"
            XCTAssertTrue(learnMore.exists, failureReason)
        }
        
        step("I will see Help button on MWB screen"){
            sleep(5)
            let app = XCUIApplication()
            let tablesQuery = app.tables
            let help = tablesQuery.staticTexts["help_button_title_141"]
            XCTestCase.VitalityHelper.waitForElementToAppear(test: self.test, element: help)
            let failureReason = "Help button not displayed"
            XCTAssertTrue(help.exists, failureReason)
        }
        
        step("I will complete Stressor Assessment") {
            sleep(10)
            let app = XCUIApplication()
            let collectionViewsQuery = app.collectionViews
            let startButton = app.tables.cells.containing(.staticText, identifier:"Stressor").buttons["landing_screen.start_button_305"]
            
            self.test.waitFor(seconds: 10, element: startButton)
            startButton.tap()
            
            sleep(5)
            app.collectionViews.staticTexts["N/A"].tap()
            XCTestCase.VitalityHelper.customScroll(count: 2)
            collectionViewsQuery.children(matching: .cell).element(boundBy: 4).staticTexts["N/A"].tap()
            XCTestCase.VitalityHelper.customScroll(count: 1)
            collectionViewsQuery.children(matching: .cell).element(boundBy: 6).staticTexts["N/A"].tap()
            XCTestCase.VitalityHelper.customScroll(count: 1)
            collectionViewsQuery.children(matching: .cell).element(boundBy: 8).staticTexts["N/A"].tap()
            collectionViewsQuery.buttons["next_button_title_84"].tap()
            
            sleep(5)
            collectionViewsQuery.staticTexts["Not applicable"].tap()
            XCTestCase.VitalityHelper.customScroll(count: 2)
            collectionViewsQuery.children(matching: .cell).element(boundBy: 2).staticTexts["N/A"].tap()
            collectionViewsQuery.children(matching: .cell).element(boundBy: 10).staticTexts["N/A"].tap()
            XCTestCase.VitalityHelper.customScroll(count: 2)
            collectionViewsQuery.children(matching: .cell).element(boundBy: 5).staticTexts["N/A"].tap()
            XCTestCase.VitalityHelper.customScroll(count: 1)
            collectionViewsQuery.children(matching: .cell).element(boundBy: 8).staticTexts["N/A"].tap()
            collectionViewsQuery.buttons["next_button_title_84"].tap()
            
            sleep(5)
            collectionViewsQuery.children(matching: .cell).element(boundBy: 4).staticTexts["None of the above"].tap()
            collectionViewsQuery.children(matching: .cell).element(boundBy: 9).staticTexts["None of the above"].tap()
            XCTestCase.VitalityHelper.customScroll(count: 2)
            collectionViewsQuery.children(matching: .cell).element(boundBy: 0).staticTexts["None of the above"].tap()
            collectionViewsQuery.children(matching: .cell).element(boundBy: 5).staticTexts["None of the above"].tap()
            collectionViewsQuery.children(matching: .cell).element(boundBy: 10).staticTexts["None of the above"].tap()
            XCTestCase.VitalityHelper.customScroll(count: 1)
            collectionViewsQuery.children(matching: .cell).element(boundBy: 9).staticTexts["None of the above"].tap()
            collectionViewsQuery.buttons["done_button_title_53"].tap()
            
//            let agreeButton = app.toolbars.buttons["agree_button_title_50"]
            // TO DO: Service error encountered when accepting privacy statement
        }
        
        step("I will complete Psychological Assessment") {
            sleep(5)
            let app = XCUIApplication()
            let collectionViewsQuery = app.collectionViews
            let startButton = app.tables.cells.containing(.staticText, identifier:"Psychological").buttons["landing_screen.start_button_305"]
            self.test.waitFor(seconds: 10, element: startButton)
            startButton.tap()
            
            collectionViewsQuery.staticTexts["Very happy"].tap()
            XCTestCase.VitalityHelper.customScroll(count: 1)
            collectionViewsQuery.staticTexts["No/never low in past month"].tap()
            XCTestCase.VitalityHelper.customScroll(count: 1)
            collectionViewsQuery.staticTexts["Never"].tap()
            XCTestCase.VitalityHelper.customScroll(count: 1)
            collectionViewsQuery.staticTexts["Very energetic"].tap()
            XCTestCase.VitalityHelper.customScroll(count: 1)
            collectionViewsQuery.staticTexts["Never"].tap()
            XCTestCase.VitalityHelper.customScroll(count: 1)
            collectionViewsQuery.staticTexts["Never felt tense"].tap()
            XCTestCase.VitalityHelper.customScroll(count: 1)
            collectionViewsQuery.staticTexts["Never"].tap()
            XCTestCase.VitalityHelper.customScroll(count: 1)
            collectionViewsQuery.children(matching: .cell).element(boundBy: 7).staticTexts["Never"].tap()
            XCTestCase.VitalityHelper.customScroll(count: 2)
            collectionViewsQuery.staticTexts["Generally very active and enthusiastic"].tap()
            collectionViewsQuery.staticTexts["Never"].tap()
            XCTestCase.VitalityHelper.customScroll(count: 2)
            collectionViewsQuery.staticTexts["Never"].tap()
            collectionViewsQuery.buttons["done_button_title_53"].tap()
//            XCUIApplication().toolbars.buttons["agree_button_title_50"].tap()
            // TO DO: Service error encountered when accepting privacy statement
        }
        
        step("I will complete Social Assessment") {
            sleep(5)
            let app = XCUIApplication()
            let collectionViewsQuery = app.collectionViews
            let startButton = app.tables.cells.containing(.staticText, identifier:"Social").buttons["landing_screen.start_button_305"]
            self.test.waitFor(seconds: 10, element: startButton)
            startButton.tap()
            
            collectionViewsQuery.children(matching: .cell).element(boundBy: 5).staticTexts["Agree"].tap()
            XCTestCase.VitalityHelper.customScroll(count: 1)
            collectionViewsQuery.children(matching: .cell).element(boundBy: 6).staticTexts["Agree"].tap()
            XCTestCase.VitalityHelper.customScroll(count: 1)
            collectionViewsQuery.children(matching: .cell).element(boundBy: 6).staticTexts["Agree"].tap()
            XCTestCase.VitalityHelper.customScroll(count: 1)
            collectionViewsQuery.children(matching: .cell).element(boundBy: 6).staticTexts["Agree"].tap()
            XCTestCase.VitalityHelper.customScroll(count: 1)
            collectionViewsQuery.children(matching: .cell).element(boundBy: 6).staticTexts["Agree"].tap()
            XCTestCase.VitalityHelper.customScroll(count: 1)
            collectionViewsQuery.children(matching: .cell).element(boundBy: 6).staticTexts["Agree"].tap()
            XCTestCase.VitalityHelper.customScroll(count: 1)
            collectionViewsQuery.children(matching: .cell).element(boundBy: 7).staticTexts["Agree"].tap()
            XCTestCase.VitalityHelper.customScroll(count: 1)
            collectionViewsQuery.children(matching: .cell).element(boundBy: 7).staticTexts["Agree"].tap()
            XCTestCase.VitalityHelper.customScroll(count: 1)
            collectionViewsQuery.children(matching: .cell).element(boundBy: 8).staticTexts["Agree"].tap()
            XCTestCase.VitalityHelper.customScroll(count: 1)
            collectionViewsQuery.children(matching: .cell).element(boundBy: 8).staticTexts["Agree"].tap()
            XCTestCase.VitalityHelper.customScroll(count: 1)
            collectionViewsQuery.children(matching: .cell).element(boundBy: 8).staticTexts["Agree"].tap()
            XCTestCase.VitalityHelper.customScroll(count: 1)
            collectionViewsQuery.children(matching: .cell).element(boundBy: 8).staticTexts["Agree"].tap()
            
            collectionViewsQuery.buttons["done_button_title_53"].tap()
//            XCUIApplication().toolbars.buttons["agree_button_title_50"].tap()
            // TO DO: Service error encountered when accepting privacy statement
        }
    }
    
}
