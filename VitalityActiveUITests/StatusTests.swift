//
//  StatusTests.swift
//  VitalityActiveUITests
//
//  Created by Erik John T. Alicaya (ADMIN) on 04/12/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import XCTest
@testable import VIACore
@testable import VitalityKit
@testable import VIAUIKit

class StatusTests: XCTestCase {
        
    func scenario(_ scenario: String) {
        NativeRunner(featureFile: "status.feature", testCase: self).run(scenario: scenario)
    }
    
    func testViewAccountStatus() {
        scenario("I can view an Account's Status")
    }
    
    func testEarningPointsInformation() {
        scenario("Navigate to Earning Points Information of Vitality Status")
    }
    
    func testGetActiveInformation() {
        scenario("Navigate to Get Active Information")
    }
    
    func testHelperScreen() {
        scenario("Navigate to Helper Screen of Vitality Status")
    }
}
