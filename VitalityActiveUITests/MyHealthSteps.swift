//
//  MyHealthSteps.swift
//  VitalityActiveUITests
//
//  Created by Dexter Anthony Ambrad on 12/18/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import UIKit
@testable import VIACore
@testable import VitalityKit
@testable import XCTest_Gherkin

class MyHealthSteps: StepDefiner {
    
    override func defineSteps() {
        step("I tap My Health on Menu") {
            let app = XCUIApplication()
            app.tabBars.buttons["menu_myhealth_button_7"].tap()
        }
        
        step("I see My Health onboarding modal") {
            let app = XCUIApplication()
            let getStartedButton = app.tables.buttons["get_started_button_title_103"]
            sleep(5)
            XCTAssert(getStartedButton.exists)
        }
        
        step("I tap Get Started on onboarding screen") {
            let app = XCUIApplication()
            let getStartedButton = app.tables.buttons["get_started_button_title_103"]
            getStartedButton.tap()
        }
        
        step("I tap the Learn More on Vitality Age Card") {
            let app = XCUIApplication()
            let vitalityCard = app.tables.cells.containing(.staticText, identifier:"my_health.vitality_age_screen_title_613").staticTexts["my_health.vitality_age_unknown_627"]
            sleep(4)
            vitalityCard.tap()
        }
        
        step("I should see Unknown Vitality Age") {
            let app = XCUIApplication()
            let unknownVitalityAgeResult = app.tables.cells.containing(.staticText, identifier:"my_health.vitality_age_screen_title_613").staticTexts["my_health.vitality_age_unknown_627"]
            sleep(4)
            XCTAssert(unknownVitalityAgeResult.waitForExistence(timeout: 4), "Unknown Vitality Age result not displayed")
        }
        
        step("I see Vitality Age learn more screen") {
            let app = XCUIApplication()
            let learnMoreLanding = app.navigationBars["learn_more_button_title_104"]
            XCTAssert(learnMoreLanding.waitForExistence(timeout: 2), "My health Vitality Age Learn More screen not shown")
        }
        
        step("I tap on Vitality Age Card") {
            let app = XCUIApplication()
            let vitalityAgeCard = app.tables.staticTexts["my_health.vitality_age_screen_title_613"]
            sleep(10)
            vitalityAgeCard.tap()
        }
        
        step("I should see too high on Vitality Age card") {
            let app = XCUIApplication()
            let tooHighVitalityAge = app.tables.staticTexts["my_health.vitality_age_older_description_long_622"]
            XCTAssert(tooHighVitalityAge.waitForExistence(timeout: 10), "Too high result not shown")
        }
        
        step("I should see Looking good on Vitality Age card") {
            let app = XCUIApplication()
            let healthyVitalityAge = app.tables.staticTexts["my_health.vitality_age_healthy_description_long_2102"]
            XCTAssert(healthyVitalityAge.waitForExistence(timeout: 10), "Looking good on Vitality Age card not shown")
        }
        
        step("I should see text that says to complete my VHR") {
            let app = XCUIApplication()
            let completeVHRInfo = app.tables.staticTexts["my_health.vitality_age_vhc_completed_vhr_not_completed_not_enough_data_description_long_623"]
            XCTAssert(completeVHRInfo.waitForExistence(timeout: 10), "Complete VHR text not shown")
        }
        
        step("I tap physical activity under what you need to improve") {
            let app = XCUIApplication()
            let waistCircumResult = app.tables.staticTexts["Physical Activity"]
            sleep(5)
            waistCircumResult.tap()
        }
        
        step("I should see tips to improve text") {
            let app = XCUIApplication()
            let tipsToImproveText = app.tables.staticTexts["tips_to_improve_1121"]
            XCTAssert(tipsToImproveText.waitForExistence(timeout: 5), "Tips to improve not shown")
        }
        
        step("I click the vitality age help button") {
            sleep(10)
            let app = XCUIApplication()
            let vitalityAgeHeader = app.navigationBars["my_health.vitality_age_screen_title_613"]
            
            if (vitalityAgeHeader.exists) {
                let helpButton = app.tables.staticTexts["help_button_title_141"]
                XCTestCase.VitalityHelper.scrollTo(element: helpButton)
                helpButton.tap()
            }
        }
        
        step("I should be navigated to help screen") {
            let app = XCUIApplication()
            let suggesstionsText = app/*@START_MENU_TOKEN@*/.tables.containing(.button, identifier:"First").element/*[[".tables.containing(.button, identifier:\"Second\").element",".tables.containing(.button, identifier:\"First\").element"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
            sleep(10)
            XCTAssert(suggesstionsText.waitForExistence(timeout: 5), "Help screen not shown")
        }
        
        step("I tap on Display More under what you need to improve") {
            let app = XCUIApplication()
            let moreResultsButton = app.tables.children(matching: .cell).element(boundBy: 2).staticTexts["more_results_792"]
            sleep(10)
            moreResultsButton.tap()
        }
        step("I should be navigated to more results screen") {
            let app = XCUIApplication()
            let moreResultsHeader = app.navigationBars["more_results_792"]
            XCTAssert(moreResultsHeader.waitForExistence(timeout: 5), "More Results not shown")
        }
    }
}
