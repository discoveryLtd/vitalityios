//
//  NativeRunner.swift
//  Pods
//
//  Created by jacdevos on 2016/11/15.
//
import Foundation
import XCTest
@testable import XCTest_Gherkin

// Gives us the ability to run features or scenarios directly by specifying file and name
open class NativeRunner {
    let path: URL
    let testCase: XCTestCase
    
    init(featureFile: String, testCase: XCTestCase) {
        testCase.state.loadAllStepsIfNeeded()
        
        self.testCase = testCase
        self.path = (Bundle(for: type(of: testCase)).resourceURL?.appendingPathComponent(featureFile))!
    }
    
    public func run(scenario: String?) {
        let features = loadFeatures(path: self.path)
        
        for feature in features {
            let scenarios = feature.scenarios.filter({ scenario == nil || $0.scenarioDescription.range(of: scenario!) != nil })
            if scenarios.count < 1 {
                XCTFail("No scenario found with name: \(String(describing: scenario))")
            }
            
            for scenario in scenarios {
                let allScenarioStepsDefined = scenario.stepDescriptions.map(testCase.state.matchingGherkinStepExpressionFound).reduce(true) { $0 && $1 }
                var allFeatureBackgroundStepsDefined = true
                
                if let defined = feature.background?.stepDescriptions.map(testCase.state.matchingGherkinStepExpressionFound).reduce(true, { $0 && $1 }) {
                    allFeatureBackgroundStepsDefined = defined
                }
                
                guard allScenarioStepsDefined && allFeatureBackgroundStepsDefined else {
                    XCTFail("Some step definitions not found for the scenario: \(scenario.scenarioDescription)")
                    return
                }
                
                NSLog("Scenario: \(scenario.scenarioDescription)")
                
                if let background = feature.background {
                    background.stepDescriptions.forEach(testCase.performStep)
                }
                scenario.stepDescriptions.forEach(testCase.performStep)
            }
        }
    }
    
    public func runFeature() {
        run(scenario: nil)
    }
    
    private func loadFeatures(path: URL) -> [NativeFeature] {
        guard let features = NativeFeatureParser(path: path).parsedFeatures() else {
            assertionFailure("Could not retrieve features from the path '\(path)'")
            return []
        }
        
        return features
    }
}
