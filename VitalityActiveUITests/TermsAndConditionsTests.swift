//
//  TermsAndConditionsTests.swift
//  VitalityActive
//
//  Created by Dexter Anthony Ambrad on 12/16/17.
//  Copyright © 2017 Glucode. All rights reserved.
//
import Foundation
import XCTest
import VitalityKit
@testable import XCTest_Gherkin
@testable import VIACore

class TermsAndConditionsTests: XCTestCase {
    func scenario(_ scenario: String) {
        NativeRunner(featureFile: "termsandconditions.feature", testCase: self).run(scenario: scenario)
    }
    
    func testTermsAndConditionsRow() {
        scenario("Terms and Conditions row is present")
    }
    
    func testViewTermsAndConditions() {
        scenario("View Terms and Conditions")
    }
    
    func testNavigateBackFromTermsAndConditions() {
        scenario("Navigate back from Terms and Conditions")
    }

}
