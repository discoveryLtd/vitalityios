//
//  NonSmokingTests.swift
//  VitalityActive
//
//  Created by Sebastian Cichon on 2017/02/09.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import XCTest
import VitalityKit
@testable import XCTest_Gherkin
@testable import VIACore

class NonSmokingTests: XCTestCase {
    
    func scenario(_ scenario: String) {
        NativeRunner(featureFile: "nonsmokers.feature", testCase: self).run(scenario: scenario)
    }
    
//    func testHomeScreen() {
//        scenario("View Home screen")
//    }
//    
//    func testNonSmokerOnboarding() {
//        scenario("Read the non-smokers declaration")
//    }
//    
//    func testDeclarationShown() {
//        scenario("Non-smoker declaration shown")
//    }
//    
//    func testDeclareNonSmoker() {
//        scenario("Declare that the user is a non-smoker")
//    }
//    
//    func testDisagreeDeclaration() {
//        scenario("Disagree with the non-smokers declaration policy")
//    }
//    
//    func testViewLearnMore() {
//        scenario("Learn more about the non-smoker’s declaration")
//    }
//    
//    func testNavigateToHomeFromOnboarding() {
//        scenario("Navigate back to Home screen from On-boarding screen")
//    }
//    
//    func testUserNotOnMarket() {
//        scenario("Can not see privacy policy if user is not on the market")
//    }
//    
//    func testUserWithoutNSD() {
//        scenario("User not part of NSD")
//    }
//    
//    func testCompleteDeclaration() {
//        scenario("Complete status for declaration for non-smokers")
//    }
}
