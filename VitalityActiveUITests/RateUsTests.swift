//
//  RateUsTests.swift
//  VitalityActive
//
//  Created by Dexter Anthony Ambrad on 12/19/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import XCTest
import VitalityKit
@testable import XCTest_Gherkin
@testable import VIACore

class RateUsTests: XCTestCase {
    func scenario(_ scenario: String) {
        NativeRunner(featureFile: "rateus.feature", testCase: self).run(scenario: scenario)
    }
    func testRateUsRow() {
        scenario("Rate us row is present")        
    }
}
