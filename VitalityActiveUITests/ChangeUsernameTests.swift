//
//  ChangeUsernameTests.swift
//  VitalityActiveUITests
//
//  Created by ted.philip.l.lat on 04/10/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import XCTest
import VitalityKit
@testable import XCTest_Gherkin
@testable import VIACore

class ChangeUsernameTests: XCTestCase {
    
    func scenario(_ scenario: String) {
        NativeRunner(featureFile: "changeusername.feature", testCase: self).run(scenario: scenario)
    }
    
    func testLoggedInSuccesfully() {
        scenario("Logged in succesfully")
    }
    
    func testNavigateToProfile() {
        scenario("Navigate to Profile")
    }
    
    func testNavigateToPersonalDetails() {
        scenario("Navigate to Personal Details")
    }
    
    func testNavigateToChangeEmail() {
        scenario("Navigate to Change Email")
    }
    
    func testCancelChangeEmail() {
        scenario("Cancel Change Email")
    }
//  TODO: Rewrite this code
//  Causes the test to sigabrt
//  Step: step I succesfully changed my email
//    func testChangeToNewEmail() {
//        scenario("Change to New Email")
//    }
    
    func testEmailAlreadyUsed() {
        scenario("Email already used")
    }
    
}
