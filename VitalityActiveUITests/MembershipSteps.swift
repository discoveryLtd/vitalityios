//
//  MembershipPassSteps.swift
//  VitalityActive
//
//  Created by Val Tomol on 16/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import XCTest
import VitalityKit
@testable import XCTest_Gherkin
@testable import VIACore

class MembershipSteps: StepDefiner {
    
    override func defineSteps() {
        
        step("I tap on Membership Pass") {
            let app = XCUIApplication()
            let tableRow = app.tables.staticTexts[ProfileStrings.SettingsLandingMembershipPassTitle]
            self.test.waitFor10Secs(element: tableRow)
            tableRow.tap()
        }
        
        step("I tap on Vitality Number") {
            let app = XCUIApplication()
            let rowLabel = app.tables.staticTexts[ProfileStrings.ProfileFieldVitalityNumberTitle]
            self.test.waitFor10Secs(element: rowLabel)
            rowLabel.tap()
        }
        
        step("I tap on Add Digital Pass") {
            let app = XCUIApplication()
            let tableRow = app.tables.staticTexts[ProfileStrings.ProfileMembershipPassDescription]
            XCTestCase.VitalityHelper.scrollTo(element: tableRow)
            self.test.waitFor10Secs(element: tableRow)
            tableRow.tap()
        }
        
        step("I select Add Profile Image Later") {
            sleep(5)
            
            let app = XCUIApplication()
            let alertButton = app.alerts[ProfileStrings.ProfileDigitalPassAlertTitle].buttons[ProfileStrings.ProfileDigitalPassAddLaterPrompt]
            self.test.waitFor10Secs(element: alertButton)
            alertButton.tap()
        }
        
        step("I select Add Profile Image Later") {
            sleep(5)
            
            let app = XCUIApplication()
            let alertButton = app.alerts[ProfileStrings.ProfileDigitalPassAlertTitle].buttons[ProfileStrings.ProfileDigitalPassAddNowPrompt]
            self.test.waitFor10Secs(element: alertButton)
            alertButton.tap()
        }
        
        step("I tap Add") {
            sleep(7)
            
            let app = XCUIApplication()
            let button = app.navigationBars["Digital Pass"].buttons["Add"] // TODO: The strings being displayed are not localizable ???
            self.test.waitFor10Secs(element: button)
            button.tap()
        }
        
        step("I tap on the existing Digital Pass") {
            sleep(2)
            
            let app = XCUIApplication()
            let rowTitle = app.tables.staticTexts["Manage"]
            XCTestCase.VitalityHelper.scrollTo(element: rowTitle)
            self.test.waitFor10Secs(element: rowTitle)
            rowTitle.tap()
        }
        
        step("I select Remove Pass") {
            let app = XCUIApplication()
            let button = app.sheets.buttons[ProfileStrings.ProfileDigitalPassRemovePassPrompt]
            self.test.waitFor10Secs(element: button)
            button.tap()
        }
        
        step("I select Show Pass in Wallet") {
            let app = XCUIApplication()
            let button = app.sheets.buttons[ProfileStrings.ProfileDigitalPassShowInWalletPrompt]
            self.test.waitFor10Secs(element: button)
            button.tap()
        }
        
        step("I remove pass") {
            let app = XCUIApplication()
            let button = app.alerts[ProfileStrings.ProfileDigitalPassRemovePassPrompt].buttons[ProfileStrings.ProfileDigitalPassRemovePassPrompt]
            self.test.waitFor10Secs(element: button)
            button.tap()
        }
        
        step("Membership Pass screen is shown") {
            let app = XCUIApplication()
            let title = app.navigationBars.staticTexts[ProfileStrings.SettingsLandingMembershipPassTitle]
            self.test.waitFor10Secs(element: title)
            XCTAssert(title.exists, "Membership Pass screen is not being displayed.")
        }
        
        step("Vitality Number information screen is shown") {
            let app = XCUIApplication()
            let title = app.staticTexts[ProfileStrings.ProfileFieldVitalityNumberInformation]
            self.test.waitFor10Secs(element: title)
            XCTAssert(title.exists, "Vitality Number information screen is not being displayed.")
        }
        
        step("Digital Pass is added") {
            sleep(2)
            
            let app = XCUIApplication()
            let rowTitle = app.staticTexts["Manage"]
            XCTestCase.VitalityHelper.scrollTo(element: rowTitle)
            XCTAssert(rowTitle.exists, "Digital Pass is not being displayed.")
        }
        
        step("Digital Pass is removed") {
            sleep(2)
            
            let app = XCUIApplication()
            let tableRow = app.tables.staticTexts[ProfileStrings.ProfileMembershipPassDescription]
            XCTestCase.VitalityHelper.scrollTo(element: tableRow)
            XCTAssert(tableRow.exists, "Digital Pass is not removed.")
        }
        
        // TODO: Implement assertions. Impediment: Don't know how to detect UIElements outside the app.
        /*
         step("Wallet is shown") {
         sleep(2)
         
         let app = XCUIApplication()
         let button = app.scrollViews.otherElements.scrollViews.otherElements.buttons["More Info"]
         self.test.waitFor10Secs(element: button)
         XCTAssert(button.exists, "Wallet is now being displayed.")
         }
         */
        
        step("I go to the user's profile tab") {
            self.step("I am on the Login screen")
            self.step("I have entered a correct email")
            self.step("I have entered a correct password")
            self.step("I tap Login")
            self.step("I agree to terms and conditions")
            self.step("I tap next on user preferences")
            self.step("I tap Profile Tab")
        }
        
        step("I add a Digital Pass") {
            self.step("And I tap on Membership Pass")
            self.step("And I tap on Add Digital Pass")
            self.step("And I select Add Profile Image Later")
            self.step("And I tap Add")
        }
    }
}
