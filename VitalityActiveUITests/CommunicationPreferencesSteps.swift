//
//  SettingsSteps.swift
//  VitalityActive
//
//  Created by Val Tomol on 22/11/2017.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import XCTest
import VitalityKit
@testable import XCTest_Gherkin
@testable import VIACore

class CommunicationPreferencesSteps: StepDefiner {
    
    override func defineSteps() {
        //MARK: Communication Preferences
        step("I tap Settings") {
            let app = XCUIApplication()
            let tableRow = app.tables.staticTexts[ProfileStrings.SettingsLandingSettingsTitle]
            self.test.waitFor10Secs(element: tableRow)
            tableRow.tap()
        }
        
        step("I tap Communication Preferences") {
            let app = XCUIApplication()
            let tableRow = app.tables.staticTexts[ProfileStrings.SettingsCommunicationTitle]
            self.test.waitFor10Secs(element: tableRow)
            tableRow.tap()
        }
        
        step("I tap Allow") {
            let app = XCUIApplication()
            app.alerts["“Vitality” Would Like to Send You Notifications"].buttons["Allow"].tap()
        }
        
        step("I tap Manage in the Settings app") {
            let app = XCUIApplication()
            let buttonsQuery = app.buttons.matching(identifier: LoginStrings.UserPrefs.PushMessageToggleSettingsLinkButtonTitle69)
            if buttonsQuery.count > 0 {
                let button = buttonsQuery.element(boundBy: 0)
                self.test.waitFor10Secs(element: button)
                button.tap()
            }
        }
        
        step("I tap Return to Vitality") {
            let app = XCUIApplication()
            let button = app.statusBars.buttons["Return to Vitality"]
            button.tap()
        }
        
        step("I should be on the app's Settings") {
            let app = XCUIApplication()
            let appName = "Vitality"
            let navTitle = app.navigationBars[appName]
            let settingsButton = app.navigationBars[appName].buttons["Settings"]
            self.test.waitFor10Secs(element: navTitle)
            self.test.waitFor10Secs(element: settingsButton)
            XCTAssert(navTitle.exists, "Settings screen is not being displayed.")
            XCTAssert(settingsButton.exists, "Settings screen is not being displayed.")
        }
        
        step("I should be on Communication Preferences") {
            let app = XCUIApplication()
            let tableRow = app.staticTexts[LoginStrings.UserPrefs.CommunicationGroupHeaderMessage65]
            self.test.waitFor10Secs(element: tableRow)
            XCTAssert(tableRow.exists, "Communication Preferences screen is not being displayed.")
        }
        
        step("I toggle Email communication") {
            let app = XCUIApplication()
            let headerTitle = LoginStrings.EmailFieldPlaceholder18
            let description = LoginStrings.UserPrefs.EmailToggleMessage66
            let tableRow = app.tables.switches[headerTitle + ", " + description]
            self.test.waitFor10Secs(element: tableRow)
            tableRow.tap()
        }
        
        step("I toggle Push Notifications") {
            let app = XCUIApplication()
            let headerTitle = LoginStrings.UserPrefs.PushMessageToggleTitle67
            let description = LoginStrings.UserPrefs.PushMessageToggleMessage68
            let tableRow = app.tables.switches[headerTitle + ", " + description]
            self.test.waitFor10Secs(element: tableRow)
            tableRow.tap()
        }
        
        step("Communication Preferences row is present") {
            let app = XCUIApplication()
            let tableRow = app.tables.staticTexts[ProfileStrings.SettingsCommunicationTitle]
            self.test.waitFor10Secs(element: tableRow)
            //XCTAssertTrue(expression: Bool) // TODO: Assert if image/icon is present/correct. Still have to figure out how to get the XCUIElement of an image/imageView
            XCTAssertTrue(tableRow.exists, "Communication Preferences row is not present")
        }
    }
}
