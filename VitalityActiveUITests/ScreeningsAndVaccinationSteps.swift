//
//  ScreeningsAndVaccinationSteps.swift
//  VitalityActive
//
//  Created by Val Tomol on 25/11/2017.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import XCTest
import VitalityKit
@testable import XCTest_Gherkin
@testable import VIACore

class ScreeningsAndVaccinationSteps: StepDefiner {
    
    override func defineSteps() {
        
        step("I tap on SV card") {
            let app = XCUIApplication()
            let SVCard = app.otherElements.staticTexts[CommonStrings.HomeCard.CardSectionTitle365]
            sleep(10)
            app.staticTexts[CommonStrings.HomeCard.CardSectionTitle291].swipeLeft()
            sleep(1)
            app.staticTexts[CommonStrings.HomeCard.CardTitle96].swipeLeft()
            sleep(1)
            app.staticTexts[CommonStrings.HomeCard.CardTitle125].swipeLeft()
            self.test.waitFor10Secs(element: SVCard)
            SVCard.tap()
        }
        
        // TODO: Replace this once S&V inner pages are merged.
        step("I tap Back") {
            let app = XCUIApplication()
            let button = app.navigationBars[CommonStrings.LearnMoreButtonTitle104].buttons["Back"]
            self.test.waitFor10Secs(element: button)
            button.tap()
        }
        
        // TODO: Replace this once S&V inner pages are merged.
        step("Learn More screen is shown") {
            let app = XCUIApplication()
            let title = app.staticTexts[CommonStrings.LearnMoreButtonTitle104]
            self.test.waitFor10Secs(element: title)
            XCTAssertTrue(title.exists, "Learn More screen is not being displayed.")
        }
        
        step("SV card is shown") {
            let app = XCUIApplication()
            let SVCard = app.otherElements.staticTexts[CommonStrings.HomeCard.CardSectionTitle365]
            self.test.waitFor10Secs(element: SVCard)
            XCTAssertTrue(SVCard.exists, "Screenings and Vaccination card is not being displayed,")
        }
    }
}
