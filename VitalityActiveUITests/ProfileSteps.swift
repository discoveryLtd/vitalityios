//
//  ProfileSteps.swift
//  VitalityActive
//
//  Created by Val Tomol on 27/11/2017.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import XCTest
import VitalityKit
@testable import XCTest_Gherkin
@testable import VIACore

class ProfileSteps: StepDefiner {
    
    override func defineSteps() {
        let userEmail = "Donette_Cason@SITtest.com"
        let userEmailNew = "Donette_Cason2@SITtest.com"
                
        step("I tap User Profile") {
            let app = XCUIApplication()
            let button = app.tables.staticTexts[userEmail]
            self.test.waitFor10Secs(element: button)
            button.tap()
        }
        
        step("User Profile is shown") {
            let app = XCUIApplication()
            let tableRow = app.staticTexts[CommonStrings.ProfileTitle]
            self.test.waitFor10Secs(element: tableRow)
            XCTAssert(tableRow.exists, "User Profile screen is not being displayed.")
        }
        
        step("I tap Edit") {
            //        let app = XCUIApplication()
            //        app.tabBars.buttons["Profile"].tap()
            //
            //        let tablesQuery = app.tables
            //        tablesQuery.staticTexts["Donette_Cason@SITtest.com"].tap()
            //        tablesQuery.buttons["Edit"].tap()
            //        app.sheets.buttons["Choose From Library"].tap()
            //        app.alerts["“Vitality” Would Like to Access Your Photos"].buttons["OK"].tap()
            //        app.collectionViews["PhotosGridView"].cells["Photo, Portrait, August 09, 2012, 5:29 AM"].tap()
            let app = XCUIApplication()
            let button = app.tables.buttons["Edit"]
            self.test.waitFor10Secs(element: button)
            button.tap()
        }
        
        step("I select Choose From Library") {
            let app = XCUIApplication()
            let button = app.sheets.buttons[CommonStrings.Proof.ActionChooseFromLibraryAlertTitle168]
            self.test.waitFor10Secs(element: button)
            button.tap()
        }
        
//        step("I allow app to access my photos") {
//            let app = XCUIApplication()
////            let button = app.alerts["“Vitality” Would Like to Access Your Photos"].buttons["OK"]
////            button.tap()
//            
//            addUIInterruptionMonitorWithDescription("Push notifications") { (alert) -> Bool in
//                if alert.buttons["OK"].exists {
//                    alert.buttons["OK"].tap()
//                    return true
//                }
//                return false
//            }
//            app.tap()
//        }
        
        step("I tap Email") {            
            let app = XCUIApplication()
            let row = app.tables.staticTexts[CommonStrings.ProfileFieldEmailTitle]
            XCTestCase.VitalityHelper.scrollTo(element: row)
            self.test.waitFor10Secs(element: row)
            row.tap()
        }
        
        step("I type an invalid email") {
//            let tablesQuery = XCUIApplication().tables
//            tablesQuery.staticTexts["Donette_Cason@SITtest.com"].tap()
//            tablesQuery.textFields["name@email.com"].tap()
//            tablesQuery.children(matching: .cell).element(boundBy: 1).children(matching: .textField).element.typeText("inavlidEmailAddress")
            let app = XCUIApplication()
            app.textFields[CommonStrings.ProfileChangeEmailEmailPlaceholder].tap()
            app.textFields[CommonStrings.ProfileChangeEmailEmailPlaceholder].typeText("invalidEmailAddress")
        }
        
        step("I type 100 characters") {
            let app = XCUIApplication()
            app.textFields[CommonStrings.ProfileChangeEmailEmailPlaceholder].tap()
            app.textFields[CommonStrings.ProfileChangeEmailEmailPlaceholder].typeText("0000000000111111111122222222223333333333444444444455555555556666666666777777777788888888889999999999x")
        }
        
        step("I type an existing email") {
            let app = XCUIApplication()
            app.textFields[CommonStrings.ProfileChangeEmailEmailPlaceholder].tap()
            app.textFields[CommonStrings.ProfileChangeEmailEmailPlaceholder].typeText(userEmail)
        }
        
        step("I type a new valid email") {
            let app = XCUIApplication()
            app.textFields[CommonStrings.ProfileChangeEmailEmailPlaceholder].tap()
            app.textFields[CommonStrings.ProfileChangeEmailEmailPlaceholder].typeText(userEmailNew)
        }
        
        step("I provide a correct password") {
            let app = XCUIApplication()
            app.alerts[CommonStrings.ProfileChangeEmailPasswordRequiredHeading].collectionViews.secureTextFields[CommonStrings.ProfileChangeEmailPasswordPlaceholder].tap()
            app.alerts[CommonStrings.ProfileChangeEmailPasswordRequiredHeading].collectionViews.secureTextFields[CommonStrings.ProfileChangeEmailPasswordPlaceholder].typeText("TestPass123")
        }
        
        step("Invalid email address error is shown") {
            let app = XCUIApplication()
            let errorMessage = app.staticTexts["Enter a valid email address"]
            self.test.waitFor10Secs(element: errorMessage)
            XCTAssert(errorMessage.exists, "Invalid email address error is not being displayed")
        }
        
        step("Unable to Change Email alert is shown") {
            sleep(5)
            
            let app = XCUIApplication()
            let errorAlert = app.alerts[CommonStrings.Settings.AlertUnableToChangeTitle927]
            self.test.waitFor10Secs(element: errorAlert)
            XCTAssert(errorAlert.exists, "Unable to Change Email alert is not being displayed.")
            if (errorAlert.exists) {
                self.step("I tap Done")
            }
        }
        
        step("Confirmation alert is shown") {
            sleep(5)
            
            let app = XCUIApplication()
            let errorAlert = app.alerts[CommonStrings.ProfileChangeEmailPromptHeading]
            self.test.waitFor10Secs(element: errorAlert)
            XCTAssert(errorAlert.exists, "Confirmation alert is not being displayed.")
            if(errorAlert.exists) {
                self.step("I tap Cancel")
            }
        }
        
        step("I navigate to the user's profile") {
            self.step("I am on the Login screen")
            self.step("I have entered a correct email")
            self.step("I have entered a correct password")
            self.step("I tap Login")
            self.step("I agree to terms and conditions")
            self.step("I tap next on user preferences")
            self.step("I tap Profile Tab")
            self.step("I tap User Profile")
            self.step("User Profile is shown")
        }
        
        step("I navigate to Donette's profile") {
            self.step("I am on the Login screen")
            self.step("I have entered Donette's email")
            self.step("I have entered a correct password")
            self.step("I tap Login")
            self.step("I agree to terms and conditions")
            self.step("I tap next on user preferences")
            self.step("I tap Profile Tab")
            self.step("I tap User Profile")
            self.step("User Profile is shown")
        }
    }
}
