//
//  StatusSteps.swift
//  VitalityActiveUITests
//
//  Created by Erik John T. Alicaya (ADMIN) on 04/12/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import UIKit
@testable import VIACore
@testable import VitalityKit
@testable import XCTest_Gherkin

class StatusSteps: StepDefiner {
        
    override func defineSteps() {
        
        step("I tap on Vitality Status") {
            let app = XCUIApplication()
            let vitalityStatus = app.otherElements.staticTexts["Status.landing_points_target_message_797"]
            vitalityStatus.tap()
        }
        
        step("I should see Vitality Status screen") {
            let app = XCUIApplication()
            let landingScreen = app.navigationBars["Status.landing_main_title_status_796"]
            
            if landingScreen.waitForExistence(timeout: 2){
                XCTestCase.VitalityHelper.waitForElementToAppear(test: self.test, element: landingScreen)
                let failureReason = "Insurance Reward screen not displayed"
                XCTAssertTrue(landingScreen.exists, failureReason)
            }
        }
        
        step("I tap on Onboarding Screen's got it button to proceed") {
            let app = XCUIApplication()
            let onBoardingScreen = app.buttons["generic_got_it_button_title_131"]
            onBoardingScreen.tap()
        }
        
        step("I tap Assessments button") {
            let app = XCUIApplication()
            let assessmentsButton = app.otherElements.staticTexts["Status.points_indication_message_up_to_828"]
            XCTestCase.VitalityHelper.scrollTo(element: assessmentsButton)
            assessmentsButton.tap()
        }
        
        step("I tap Nutrition button") {
            let app = XCUIApplication()
            let nutritionButton = app.otherElements.staticTexts["Status.points_indication_message_limit_830"]
            XCTestCase.VitalityHelper.scrollTo(element: nutritionButton)
            nutritionButton.tap()
        }
        
        step("I tap Get Active button") {
            let app = XCUIApplication()
            let nutritionButton = app.otherElements.staticTexts["Status.points_indication_message_829"]
            XCTestCase.VitalityHelper.scrollTo(element: nutritionButton)
            nutritionButton.tap()
        }
        
        step("I navigate throughout Assessments information") {
            let app = XCUIApplication()
            let VHR = app.otherElements.staticTexts["Vitality Health Review"]
            VHR.tap()
            
            let backButton = app.navigationBars.buttons.element(boundBy: 0)
            self.test.waitFor10Secs(element: backButton)
            backButton.tap()
            
            let NSD = app.otherElements.staticTexts["Non Smokers Declaration"]
            NSD.tap()
            self.test.waitFor10Secs(element: backButton)
            backButton.tap()
            
            let VHC = app.otherElements.staticTexts["Vitality Health Check"]
            VHC.tap()
            
            let BMI = app.otherElements.staticTexts["Body Mass Index"]
            BMI.tap()
            self.test.waitFor10Secs(element: backButton)
            backButton.tap()
            
            let glucoseOrHba1c = app.otherElements.staticTexts["Fasting Glucose or HbA1c"]
            glucoseOrHba1c.tap()
            self.test.waitFor10Secs(element: backButton)
            backButton.tap()
            
            let BP = app.otherElements.staticTexts["Blood Pressure"]
            BP.tap()
            self.test.waitFor10Secs(element: backButton)
            backButton.tap()
            
            let cholesterol = app.otherElements.staticTexts["Cholesterol"]
            cholesterol.tap()
            self.test.waitFor10Secs(element: backButton)
            backButton.tap()
            
            let urineProtein = app.otherElements.staticTexts["Urine Protein"]
            urineProtein.tap()
            self.test.waitFor10Secs(element: backButton)
            backButton.tap()
            self.test.waitFor10Secs(element: backButton)
            backButton.tap()
            
            let MWB = app.otherElements.staticTexts["Mental Wellbeing Assessment"]
            MWB.tap()
            
            let stressor = app.otherElements.staticTexts["Mental Wellbeing Stressor Assessment Questionnaire"]
            stressor.tap()
            self.test.waitFor10Secs(element: backButton)
            backButton.tap()
            
            let psychological = app.otherElements.staticTexts["Mental Wellbeing Psychological Assessment Questionnaire"]
            psychological.tap()
            self.test.waitFor10Secs(element: backButton)
            backButton.tap()
            
            let social = app.otherElements.staticTexts["Mental Wellbeing Social Assessment Questionnaire"]
            social.tap()
            self.test.waitFor10Secs(element: backButton)
            backButton.tap()
            self.test.waitFor10Secs(element: backButton)
            backButton.tap()
            self.test.waitFor10Secs(element: backButton)
            backButton.tap()
        }
        
        step("I navigate throughout Nutrition information") {
            let app = XCUIApplication()
            let VNA = app.otherElements.staticTexts["Vitality Nutrition Assessment"]
            VNA.tap()
            
            let backButton = app.navigationBars.buttons.element(boundBy: 0)
            self.test.waitFor10Secs(element: backButton)
            backButton.tap()
            self.test.waitFor10Secs(element: backButton)
            backButton.tap()
        }
        
        step("I navigate throughout Get Active information") {
            let app = XCUIApplication()
            let OFE = app.otherElements.staticTexts["Organised Fitness Event"]
            OFE.tap()
            
            let running = app.otherElements.staticTexts["Running"]
            running.tap()
            let backButton = app.navigationBars.buttons.element(boundBy: 0)
            self.test.waitFor10Secs(element: backButton)
            backButton.tap()
            
            let triathlon = app.otherElements.staticTexts["Triathlon"]
            triathlon.tap()
            self.test.waitFor10Secs(element: backButton)
            backButton.tap()
            
            let cycling = app.otherElements.staticTexts["Cycling"]
            cycling.tap()
            self.test.waitFor10Secs(element: backButton)
            backButton.tap()
            
            let swimming = app.otherElements.staticTexts["Swimming"]
            swimming.tap()
            self.test.waitFor10Secs(element: backButton)
            backButton.tap()
            
            let walking = app.otherElements.staticTexts["Walking"]
            walking.tap()
            self.test.waitFor10Secs(element: backButton)
            backButton.tap()
            self.test.waitFor10Secs(element: backButton)
            backButton.tap()
            
            let steps = app.otherElements.staticTexts["Steps"]
            steps.tap()
            let stepsInfo = app.otherElements.staticTexts["potential_points.how_to_calculate_steps_message_500"]
            XCTestCase.VitalityHelper.scrollTo(element: stepsInfo)
            self.test.waitFor10Secs(element: backButton)
            backButton.tap()
            
            let heartRate = app.otherElements.staticTexts["Heart Rate"]
            heartRate.tap()
            let heartRateInfo = app.otherElements.staticTexts["potential_points.how_to_calculate_heart_rate_484"]
            XCTestCase.VitalityHelper.scrollTo(element: heartRateInfo)
            self.test.waitFor10Secs(element: backButton)
            backButton.tap()
            
            let gymVisits = app.otherElements.staticTexts["Gym Visits"]
            gymVisits.tap()
            self.test.waitFor10Secs(element: backButton)
            backButton.tap()
            self.test.waitFor10Secs(element: backButton)
            backButton.tap()
        }
        
        step("I tap Help button") {
            let app = XCUIApplication()
            let helpButton = app.otherElements.staticTexts["help_button_title_141"]
            XCTestCase.VitalityHelper.scrollTo(element: helpButton)
            helpButton.tap()
        }
    }
}
