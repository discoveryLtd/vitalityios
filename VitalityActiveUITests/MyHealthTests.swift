//
//  MyHealthTests.swift
//  VitalityActiveUITests
//
//  Created by Dexter Anthony Ambrad on 12/18/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import XCTest

class MyHealthTests: XCTestCase {
    
    func scenario(_ scenario: String) {
        NativeRunner(featureFile: "myhealth.feature", testCase: self).run(scenario: scenario)
    }
    
    func testMyHealthOnboardingScreen() {
        scenario("My Health Onboarding screen is displayed")
    }
    
    func testMyHealthLearnMore() {
        scenario("Learn More is displayed when navigating to my health learn more screen")
    }
    
    func testMyHealthNoCompletedAssessment() {
        scenario("Unknown Vitality Age is displayed if no assessment has been completed")
    }
    
    func testMyHealthGoodVitalityAge(){
        scenario("Displays Looking Good Vitality Age")
    }
    
    func testMyHealthBadVitalityAge(){
        scenario("Displays too high on Bad Vitality Age")
    }
    
    func testCompletedVHCButNotVHR() {
        scenario("Completed VHC but not VHR")
    }
    func testDisplayResults() {
        scenario("Display Result on what you need to improve section")
    }
    
    func testHelpScreen() {
        scenario("Navigate to Help Screen")
    }
    
    func testDisplayMoreResults(){
        scenario("Display more results on what you need to improve")
        
    }
}
