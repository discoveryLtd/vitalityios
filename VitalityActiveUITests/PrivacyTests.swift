//
//  SettingsPrivacyTests.swift
//  VitalityActive
//
//  Created by Steven Layug on 11/21/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import XCTest
import VitalityKit
@testable import XCTest_Gherkin
@testable import VIACore

class PrivacyTests: XCTestCase {
    
    private var app: XCUIApplication = XCUIApplication()
    
    func scenario(_ scenario: String) {
        NativeRunner(featureFile: "settingsprivacy.feature", testCase: self).run(scenario: scenario)
        
    }
    
    func testLogin() {
        scenario("Log in the application")
    }
    
    func testPrivacyScreenDisplay() {
        scenario("Redirect to the Privacy screen")
    }
    
    func testPrivacyStatementDisplay() {
        scenario("Redirect to the Privacy Statement page")
    }
    
    func testAnalyticsPreferencesToggleDisplay() {
        scenario("Check Crash Reports Preference toggle existence")
    }
    
    func testAnalyticsPreferencesToggle() {
        scenario("Test Analytics Preference toggle")
    }
    
    func testCrashReportsPreferencesToggleDisplay() {
        scenario("Check Crash Reports Preference toggle existence")
    }
    
    func testCrashReportsPreferencesToggle() {
        scenario("Test Crash Reports Preference toggle")
    }
}
