//
//  FingerPrintLoginSteps.swift
//  VitalityActiveUITests
//
//  Created by ted.philip.l.lat on 18/10/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import XCTest
import VIACore
import VitalityKit
@testable import XCTest_Gherkin

class FingerPrintLoginSteps: StepDefiner {
    
    override func defineSteps() {
        
        step("I tap on settings") {
            sleep(5)
            let app = XCUIApplication()
            app.tables.cells.staticTexts[CommonStrings.Settings.LandingSettingsTitle899].tap()
        }
        
        step("I should see the settings screen") {
            sleep(5)
            let app = XCUIApplication()
            let settingsScreen = app.navigationBars[CommonStrings.Settings.LandingSettingsTitle2447]
            XCTAssert(settingsScreen.exists, "Settings Screen not shown")
        }
        
        step("I tap on Security") {
            sleep(5)
            let app = XCUIApplication()
            let tablesQuery = app.tables
            tablesQuery.cells.staticTexts[CommonStrings.Settings.SecurityTitle904].tap()
        }
        
        step("I should see the security screen") {
            sleep(5)
            let app = XCUIApplication()
            let tablesQuery = app.tables
            let securityScreen = tablesQuery.cells.staticTexts[CommonStrings.UserPrefs.SecurityGroupHeaderTitle77]
            XCTAssert(securityScreen.exists, "Security Screen not shown")
        }
        
        step("I toggle touch ID") {
            sleep(5)
            let app = XCUIApplication()
            let touchIdSwitch = app.switches.element(boundBy: 0)
            touchIdSwitch.tap()
        }
        
        step("I should see Authenticate with touch ID prompt") {
            sleep(5)
            let springboard = XCUIApplication(bundleIdentifier: "com.apple.springboard")
            let alert = springboard.alerts.staticTexts["Touch ID for “Vitality”"]
            XCTAssert(alert.exists, "Touch ID prompt not shown")
        }
        
        step("I should see User Preferences screen") {
            sleep(5)
            let app = XCUIApplication()
            let userPreferencesScreen = app.tables.staticTexts[CommonStrings.UserPrefs.CommunicationGroupHeaderMessage65]
            self.test.waitFor(seconds: 50, element: userPreferencesScreen)
            XCTAssert(userPreferencesScreen.exists, "User Preferences screen not shown")
        }
        
        step("I scroll to Security") {
            let app = XCUIApplication()
            let securitySection = app.tables.cells.staticTexts[CommonStrings.UserPrefs.RememberMeToggleTitle81]
            XCTestCase.VitalityHelper.scrollTo(element: securitySection)
        }
        
        step("I toggle User Preferences touch ID") {
            let app = XCUIApplication()
            let touchIdSwitch = app.switches.element(boundBy: 4)
            touchIdSwitch.tap()
        }
        
        step("I press Cancel in touch ID prompt") {
            let springboard = XCUIApplication(bundleIdentifier: "com.apple.springboard")
            springboard.alerts.buttons["Cancel"].tap()
        }
        
    }
}
