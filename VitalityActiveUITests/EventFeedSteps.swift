//
//  EventFeedSteps.swift
//  VitalityActive
//
//  Created by OJ Garde on 20/11/2017.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import XCTest
import VIACore
import VitalityKit
@testable import XCTest_Gherkin

class EventsFeedSteps: StepDefiner {
    
    override func defineSteps() {
        
        step("Events row is shown") {
            let app = XCUIApplication()
            let eventsMenu = app.tables.staticTexts[CommonStrings.Settings.LandingEventsTitle900]
            self.test.waitFor10Secs(element: eventsMenu)
            XCTAssert(eventsMenu.exists, "Events Feed menu is not being displayed - Unable to find the events menu title.")
        }
        
        step("I tap Events Feed row") {
            let app = XCUIApplication()
            let button = app.tables.staticTexts[CommonStrings.Settings.LandingEventsTitle900]
            self.test.waitFor10Secs(element: button)
            button.tap()

        }
        
        step("I tap Category Filter") {
            let app = XCUIApplication()
            let button = app.navigationBars.buttons[CommonStrings.EventsAllEvents]
            self.test.waitFor10Secs(element: button)
            button.tap()
        }
        
        step("Category Filter is shown"){
            let app = XCUIApplication()
            let tablesQuery = app.tables
            let button = tablesQuery.staticTexts[CommonStrings.EventsCategoryLogin]
            XCTAssert(button.exists, "Filter is not being displayed")
        }
        
        step("Login Category is shown") {
            let app = XCUIApplication()
            let tablesQuery = app.tables
            let button = tablesQuery.staticTexts[CommonStrings.EventsCategoryLogin]
            XCTAssert(button.exists, "Login is not being displayed")
        }
        
        step("Assessment Category is shown") {
            let app = XCUIApplication()
            let tablesQuery = app.tables
            let button = tablesQuery.staticTexts[CommonStrings.EventsCategoryAssessment1129]
            XCTAssert(button.exists, "Assessment is not being displayed")
        }
        
        step("I close filter") {
            let app = XCUIApplication()
            let window = app.children(matching: .window).element(boundBy: 0)
            window.tap()
        }
        
        step("Then Events under Login Category are shown") {
            
        }
    }
}
