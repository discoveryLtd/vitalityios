//
//  SettingsPrivacySteps.swift
//  VitalityActive
//
//  Created by Steven Layug on 11/21/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import XCTest
import VitalityKit
@testable import XCTest_Gherkin
@testable import VIACore

class PrivacySteps: StepDefiner {
    
    override func defineSteps() {
        let app = XCUIApplication()
        let tablesQuery = app.tables
        let webViewsQuery = app.webViews
        
        step("I have entered a correct account") {
            app.scrollViews.otherElements.tables.cells.textFields[LoginStrings.Registration.EmailFieldPlaceholder27].tap()
            app.scrollViews.otherElements.tables.cells.textFields[LoginStrings.Registration.EmailFieldPlaceholder27].typeText("test--Danika_Brinkman@SITtest.com")
        }
        
        step("I go to Privacy screen"){
            app.tabBars.buttons["Profile"].tap()
            tablesQuery.staticTexts["settings_landing_settings_title"].tap()
            tablesQuery.staticTexts["settings_privacy_title"].tap()
        }
        
        step("I am on the Privacy screen"){
            let privacyHeader = tablesQuery.cells.staticTexts["user_prefs.privacy_group_header_title_70"]
            XCTAssertTrue(privacyHeader.exists , "Privacy screen is not displayed")
        }
        
        step("I tap the Privacy Statement link"){
            app.tables.buttons["user_prefs.privacy_group_header_link_button_title_72"].tap()
        }
        
        step("I am on the Privacy Statement screen"){
            sleep(3)
            let privacyStatement = webViewsQuery.staticTexts["Placeholder for first time preferences privacy policy for Central Asset - tenant 2 - english"]
            XCTAssertTrue(privacyStatement.exists , "Privacy Statement screen is not displayed")
        }
        
        step("I check if Privacy toggle is displayed"){
            let privacyToggle = tablesQuery.switches["user_prefs.analytics_toggle_title_73, user_prefs.analytics_toggle_message_74"]
            XCTAssertTrue(privacyToggle.exists , "Privacy toggle is not displayed")
        }
        
        step("I tap Analytics toggle"){
            let privacyToggle = tablesQuery.switches["user_prefs.analytics_toggle_title_73, user_prefs.analytics_toggle_message_74"]
            privacyToggle.tap()
        }
        
        step("I check if Crash Reports toggle is displayed"){
            let crashReportToggleToggle = tablesQuery.switches["user_prefs.crash_reports_toggle_title_75, user_prefs.crash_reports_toggle_message_76"]
            XCTAssertTrue(crashReportToggleToggle.exists , "Crash Report toggle is not displayed")
        }
        
        step("I tap Crash Reports toggle"){
            let crashReportToggleToggle = tablesQuery.switches["user_prefs.crash_reports_toggle_title_75, user_prefs.crash_reports_toggle_message_76"]
            crashReportToggleToggle.tap()
        }
        
    }
    
}

