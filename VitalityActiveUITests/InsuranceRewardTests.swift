//
//  InsuranceRewardTests.swift
//  VitalityActiveUITests
//
//  Created by Erik John T. Alicaya (ADMIN) on 03/12/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import XCTest
@testable import VIACore
@testable import VitalityKit
@testable import VIAUIKit

class InsuranceRewardTests: XCTestCase {
        
    func scenario(_ scenario: String) {
        NativeRunner(featureFile: "insurancereward.feature", testCase: self).run(scenario: scenario)
    }
    
    func testDisplayFromHomepage() {
        scenario("Navigate to Insurance Reward from Homepage")
    }
}
