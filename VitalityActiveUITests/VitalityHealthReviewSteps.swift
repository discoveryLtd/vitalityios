//
//  VitalityHealthReviewSteps.swift
//  VitalityActive
//
//  Created by Sebastian Cichon on 2017/07/12.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import XCTest
import VitalityKit
@testable import XCTest_Gherkin
@testable import VIACore

class VitalityHealthReviewSteps: StepDefiner {
    
    override func defineSteps() {
        
//        step("I tap on VHR card") {
//            let app = XCUIApplication()
//            let VHRcard = app.otherElements.staticTexts[CommonStrings.HomeCard.CardSectionTitle291]
//            self.test.waitFor10Secs(element: VHRcard)
//            VHRcard.tap()
//        }
        
        step("I tap on VHR card") {
            let app = XCUIApplication()
            let VHRcard = app.otherElements.staticTexts[CommonStrings.HomeCard.CardSectionTitle291]
//            app.staticTexts[CommonStrings.HomeCard.CardSectionTitle365].swipeLeft()
//            app.staticTexts[CommonStrings.Onboarding.Title1195].swipeLeft()
//            app.staticTexts[CommonStrings.HomeCard.CardSectionTitle388].swipeLeft()
//            app.staticTexts[NSDStrings.HomeCard.CardTitle96].swipeLeft()
            self.test.waitFor10Secs(element: VHRcard)
            VHRcard.tap()
        }
        
        step("I will see VHR onboarding") {
            let app = XCUIApplication()
            let VHROnboarding = app.otherElements.staticTexts[CommonStrings.Onboarding.Title2312]
            let failureReason = "VHR Onboarding not displayed"
            XCTestCase.VitalityHelper.waitForElementToAppear(test: self.test, element: VHROnboarding)
            XCTAssertTrue(VHROnboarding.exists, failureReason)
        }
        
        step("I will see Get Started button") {
            let app = XCUIApplication()
            let getStartedButton = app.buttons[CommonStrings.GenericGotItButtonTitle131]
            let failureReason = "VHR Onboarding not displayed"
            XCTestCase.VitalityHelper.waitForElementToAppear(test: self.test, element: getStartedButton)
            XCTAssertTrue(getStartedButton.exists, failureReason)
        }
        
        step("I see Disclaimer button") {
            let app = XCUIApplication()
            let Disclaimer = app.buttons[CommonStrings.DisclaimerTitle265]
            let failureReason = "Disclaimer button not visible"
            XCTestCase.VitalityHelper.waitForElementToAppear(test: self.test, element: Disclaimer)
            XCTAssertTrue(Disclaimer.exists, failureReason)
        }
        
        step("I tap on Disclaimer") {
            let app = XCUIApplication()
            let Disclaimer = app.buttons[CommonStrings.DisclaimerTitle265]
            self.test.waitFor10Secs(element: Disclaimer)
            Disclaimer.tap()
        }
        
        step("I will see Disclaimer screen") {
            let app = XCUIApplication()
            let disclaimerHeader = app.navigationBars[CommonStrings.DisclaimerTitle265]
            let failureReason = "Disclaimer screen is not being displayed"
            XCTestCase.VitalityHelper.waitForElementToAppear(test: self.test, element: disclaimerHeader)
            XCTAssertTrue(disclaimerHeader.exists, failureReason)
        }
        
        step("I will see the VHR onboarding header") {
            let app = XCUIApplication()
            let VHRheader = app.staticTexts[CommonStrings.Onboarding.Title2312]
            let failureReason = "VHR Onboarding title is not being displayed"
            XCTestCase.VitalityHelper.waitForElementToAppear(test: self.test, element: VHRheader)
            XCTAssertTrue(VHRheader.exists, failureReason)
        }
        
        step("I will see VHR landing screen") {
            let app = XCUIApplication()
            let VHRscreen = app.navigationBars[CommonStrings.HomeCard.CardSectionTitle291]
            let failureReason = "VHR Landing Screen not displayed"
            XCTestCase.VitalityHelper.waitForElementToAppear(test: self.test, element: VHRscreen)
            XCTAssertTrue(VHRscreen.exists, failureReason)
        }
        
        step ("VHR Learn More Button should show") {
            let app = XCUIApplication()
            let learnMoreButton = app.cells.staticTexts[CommonStrings.LearnMoreButtonTitle104]
            let failureReason = "VHR Learn More button not displayed"
            sleep(15)
            XCTestCase.VitalityHelper.scrollTo(element: learnMoreButton)
            XCTAssertTrue(learnMoreButton.exists, failureReason)
        }
        
        step("I tap on VHR learn more button") {
            let app = XCUIApplication()
            let learnMoreButton = app.cells.staticTexts[CommonStrings.LearnMoreButtonTitle104]
            sleep(15)
            XCTestCase.VitalityHelper.scrollTo(element: learnMoreButton)
            learnMoreButton.tap()
        }
        
        step("I tap on VHR learn more back button") {
            let app = XCUIApplication()
            let vhrLearnMoreBackButton = app.navigationBars.buttons.element(boundBy: 0)
            self.test.waitFor10Secs(element: vhrLearnMoreBackButton)
            vhrLearnMoreBackButton.tap()
        }
        
        
        step ("VHR Help Button should show") {
            let app = XCUIApplication()
            let helpButton = app.cells.staticTexts[CommonStrings.HelpButtonTitle141]
            let failureReason = "VHR Help button not displayed"
            sleep(10)
            XCTestCase.VitalityHelper.scrollTo(element: helpButton)
            XCTAssertTrue(helpButton.exists, failureReason)
        }
        
        step("I will see the Sections title") {
            let app = XCUIApplication()
            let sectionsTitle = app.staticTexts[CommonStrings.LandingScreen.GroupHeading300]
            let failureReason = "VHR Sections not displayed - Possible Server Error"
            XCTestCase.VitalityHelper.waitForElementToAppear(test: self.test, element: sectionsTitle)
            XCTAssertTrue(sectionsTitle.exists, failureReason)
        }
        
        step("I will see General Health") {
            let app = XCUIApplication()
            let generalHealth = app.cells.staticTexts["General Health"]
            let failureReason = "General Health questionnaire not displayed"
            sleep(18)
            XCTAssertTrue(generalHealth.exists, failureReason)
        }
        
        step("I will see Social Habits") {
            let app = XCUIApplication()
            let socialHabits = app.cells.staticTexts["Social Habits"]
            let failureReason = "Social Habits questionnaire not displayed"
            sleep(18)
            XCTAssertTrue(socialHabits.exists, failureReason)
        }
        
        step("I will see Lifestyle Habits") {
            let app = XCUIApplication()
            let lifestyleHabits = app.cells.staticTexts["Lifestyle Habits"]
            let failureReason = "Lifestyle Habits questionnaire not displayed"
            sleep(18)
            XCTAssertTrue(lifestyleHabits.exists, failureReason)
        }
        
        step("I will see sections footer text") {
            let app = XCUIApplication()
            let sectionsFooter = app.staticTexts[CommonStrings.LandingScreen.CompletedMessage327]
            let failureReason = "VHR Sections footer not displayed"
            XCTestCase.VitalityHelper.waitForElementToAppear(test: self.test, element: sectionsFooter)
            XCTAssertTrue(sectionsFooter.exists, failureReason)
        }
        
        step("I will see start button") {
            let app = XCUIApplication()
            let startButton = app.otherElements.cells.buttons[CommonStrings.LandingScreen.StartButton305]
            let failureReason = "Start button not displayed"
            sleep(18)
            XCTAssertTrue(startButton.exists, failureReason)
        }
        
        step("I tap on Start") {
            let app = XCUIApplication()
            // We fetch all buttons matching "WDA.home_card.title_415" (accordianButtonsQuery is a XCUIElementQuery)
            let accordianButtonsQuery = app.otherElements.cells.buttons.matching(identifier: "landing_screen.start_button_305")
            // Delay due to services
            sleep(5)
            // If there is at least one
            if accordianButtonsQuery.count > 0 {
                // We take the first one and tap it
                let firstButton = accordianButtonsQuery.element(boundBy: 1)
                firstButton.tap()
            }
        }
        
        step("I tap on Edit") {
            let app = XCUIApplication()
            let editButton = app.otherElements.cells.buttons[CommonStrings.LandingScreen.EditButton307]
            sleep(18)
            editButton.tap()
        }
        
        step("I clicked Edit on Social Habits") {
            let app = XCUIApplication()
            let accordianButtonsQuery = app.otherElements.cells.buttons.matching(identifier: CommonStrings.LandingScreen.EditButton307)
            sleep(30)
            if accordianButtonsQuery.count > 0 {
                let secondButton = accordianButtonsQuery.element(boundBy: 1)
                secondButton.tap()
            }
        }
        
        step("I edit on General Health") {
            let app = XCUIApplication()
            let accordianButtonsQuery = app.otherElements.cells.buttons.matching(identifier: CommonStrings.LandingScreen.EditButton307)
            sleep(30)
            if accordianButtonsQuery.count > 0 {
                let secondButton = accordianButtonsQuery.element(boundBy: 0)
                secondButton.tap()
            }
        }
        
        step("I edit on Lifestyle Habit") {
            let app = XCUIApplication()
            let accordianButtonsQuery = app.otherElements.cells.buttons.matching(identifier: CommonStrings.LandingScreen.EditButton307)
            sleep(30)
            if accordianButtonsQuery.count > 0 {
                let secondButton = accordianButtonsQuery.element(boundBy: 2)
                secondButton.tap()
            }
        }
        
        step("I tap Next on questionnaire") {
            let app = XCUIApplication()
            let button = app.otherElements.buttons[CommonStrings.NextButtonTitle84]
            app.swipeUp()
            button.tap()
        }
        
        step("I tap the next on the bottom"){
            let app = XCUIApplication()
            let button = app.otherElements.buttons[CommonStrings.NextButtonTitle84]
            app.swipeUp()
            app.swipeUp()
            app.swipeUp()
            app.swipeUp()
            app.swipeUp()
            app.swipeUp()
            app.swipeUp()
            button.tap()
        }
        
        step("I tap the done on the bottom"){
            let app = XCUIApplication()
            let doneButton = app.otherElements.buttons[CommonStrings.DoneButtonTitle53]
            app.swipeUp()
            app.swipeUp()
            app.swipeUp()
            app.swipeUp()
            app.swipeUp()
            app.swipeUp()
            app.swipeUp()
            doneButton.tap()
        }
        
        step("I tap Done") {
            let app = XCUIApplication()
            let doneButton = app.otherElements.buttons[CommonStrings.DoneButtonTitle53]
            doneButton.tap()
        }
        
        step("I select first answer") {
            let app = XCUIApplication()
            //let questionVHR = app.collectionViews.cells.otherElements.staticTexts["No, never used tobacco products"]
            let questionVHR = app.collectionViews.cells.otherElements.staticTexts["No, Never"]
            questionVHR.tap()
        }
        
        step("I will see edit button") {
            let app = XCUIApplication()
            let editButton = app.otherElements.cells.buttons[CommonStrings.LandingScreen.EditButton307]
            let failureReason = "Edit button not displayed"
            sleep(18)
            XCTAssertTrue(editButton.exists, failureReason)
        }
        
        step("I will see continue button") {
            let app = XCUIApplication()
            let continueButton = app.buttons[CommonStrings.LandingScreen.ContinueButton306]
            let failureReason = "Continue button not displayed"
            XCTAssertTrue(continueButton.exists, failureReason)
        }
        
        step("I will see estimated time to complete") {
            let app = XCUIApplication()
            let estimateTime = app.staticTexts["7 min"]
            let failureReason = "Time estimated to complete not displayed"
            sleep(18)
            XCTAssertTrue(estimateTime.exists, failureReason)
        }
        
        step("I will see completed questionnaire footer") {
            let app = XCUIApplication()
            let completedFootnote = app.staticTexts[CommonStrings.LandingScreen.CompletedMessage327]
            let failureReason = "Questionnaire completed footnote not displayed"
            XCTestCase.VitalityHelper.waitForElementToAppear(test: self.test, element: completedFootnote)
            sleep(18)
            XCTAssertTrue(completedFootnote.exists, failureReason)
        }
        
        step("I will see completed state at the top of screen") {
            let app = XCUIApplication()
            let completedHeader = app.staticTexts[CommonStrings.LandingScreen.CompletedMessage327]
            let failureReason = "Completed header is not displayed"
            sleep(18)
            XCTAssertTrue(completedHeader.exists, failureReason)
        }
        
        step("I will see points and cycle message") {
            let app = XCUIApplication()
            let pointsMessage = app.staticTexts[CommonStrings.LandingHeader.DescriptionCompleted2258("1,000")]
            let failureReason = "Points and Cycle message is not displayed / Incorrect point amount returned"
            sleep(18)
            XCTAssertTrue(pointsMessage.exists, failureReason)
        }
        
        step("I will see VHR privacy policy") {
            let app = XCUIApplication()
            //let VHRPrivacy = app.toolbars.buttons[CommonStrings.AgreeButtonTitle50]
            let VHRPrivacy = app.toolbars.buttons[CommonStrings.BackButton336]
            let failureReason = "VHR PRivacy Policy not displayed."
            sleep(15)
            XCTAssertTrue(VHRPrivacy.exists, failureReason)
        }
        
        step("I will see the done button in questionnaire"){
            let app = XCUIApplication()
            let doneButton = app.otherElements.buttons[CommonStrings.DoneButtonTitle53]
            let failureReason = "VHR last question not displayed"
            sleep(15)
            XCTAssertTrue(doneButton.exists, failureReason)
        }
        
        step("I tap VHR policy disagree button"){
            let app = XCUIApplication()
            let disagreeButton = app.toolbars.buttons[CommonStrings.BackButton336]
            disagreeButton.tap()
        }
        
        step("I tap VHR policy agree button"){
            let app = XCUIApplication()
            let agreeButton = app.toolbars.buttons[CommonStrings.NextButtonTitle84]
            agreeButton.tap()
        }
        
        step("I see the VHR completion screen") {
            let app = XCUIApplication()
            //let VHRCompletionScreen = app.otherElements.buttons[CommonStrings.DoneButtonTitle53]
            let VHRCompletionScreen = app.otherElements.buttons[CommonStrings.GreatButtonTitle120]
            let failureReason = "VHR Completion Screen not displayed."
            XCTestCase.VitalityHelper.waitForElementToAppear(test: self.test, element: VHRCompletionScreen)
            XCTAssertTrue(VHRCompletionScreen.exists, failureReason)
        }
        
        step("I see completed state") {
            let app = XCUIApplication()
            let VHRCompletionScreen = app.otherElements.staticTexts[CommonStrings.Completed.SingleQuestionnaireCompletedTitle2211("Social Habits")]
            let failureReason = "VHR Completion Screen not displayed."
            XCTestCase.VitalityHelper.waitForElementToAppear(test: self.test, element: VHRCompletionScreen)
            XCTAssertTrue(VHRCompletionScreen.exists, failureReason)
        }
        
        step("I will see sections remaining message") {
            let app = XCUIApplication()
//            let VHRCompletionScreen = app.otherElements.staticTexts[VhrStrings.Completed.MultipleRemainingQuestionnairesEarnPointsMessage9992("1", "1000")]
            let VHRCompletionScreen = app.otherElements.staticTexts[CommonStrings.Completed.MultipleRemainingQuestionnairesEarnPointsMessage2210("2", "750")]
            let failureReason = "VHR Completion Screen not displayed."
            XCTestCase.VitalityHelper.waitForElementToAppear(test: self.test, element: VHRCompletionScreen)
            XCTAssertTrue(VHRCompletionScreen.exists, failureReason)
        }
        
        step("General Health will be available to start") {
            let app = XCUIApplication()
            let VHRCompletionScreen = app.otherElements.staticTexts["General Health"]
            let failureReason = "VHR Completion Screen not displayed."
            XCTestCase.VitalityHelper.waitForElementToAppear(test: self.test, element: VHRCompletionScreen)
            XCTAssertTrue(VHRCompletionScreen.exists, failureReason)
        }
        
        step("Lifestyle Habits will be available to start") {
            let app = XCUIApplication()
            let VHRCompletionScreen = app.otherElements.staticTexts["Lifestyle Habits"]
            let failureReason = "VHR Completion Screen not displayed."
            XCTestCase.VitalityHelper.waitForElementToAppear(test: self.test, element: VHRCompletionScreen)
            XCTAssertTrue(VHRCompletionScreen.exists, failureReason)
        }
    }
}
