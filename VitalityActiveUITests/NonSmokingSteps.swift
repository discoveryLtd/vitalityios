//
//  NonSmokingSteps.swift
//  VitalityActive
//
//  Created by Sebastian Cichon on 2017/02/09.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import XCTest
import VitalityKit
@testable import XCTest_Gherkin
@testable import VIACore

class NonSmokingSteps: StepDefiner {
    
    override func defineSteps() {
        
        step("I tap on non-smoker declaration") {
            let app = XCUIApplication()
            let NSDCard = app.otherElements.staticTexts[CommonStrings.HomeCard.CardTitle96]
            self.test.waitFor10Secs(element: NSDCard)
            NSDCard.tap()
        }
        
        step("Homescreen card is updated to completed") {
            let app = XCUIApplication()
            sleep(15)
            let NSDComplete = app.otherElements.staticTexts[CommonStrings.HomeCard.CardCompletedTitle290]
            self.test.waitFor10Secs(element: NSDComplete)
            XCTAssertTrue(NSDComplete.exists , "NSD Card not updated to show completed state.")
        }
        
        step("Onboarding screen should show with Get Started button") {
            let app = XCUIApplication()
            let button = app.buttons[CommonStrings.GetStartedButtonTitle103]
            self.test.waitFor10Secs(element: button)
            XCTAssertTrue(button.exists)
        }
        
        step("I tap on get started") {
            let app = XCUIApplication()
            let getStarted = app.otherElements.buttons[CommonStrings.GetStartedButtonTitle103]
            if (getStarted.exists) {
                getStarted.tap()
            }
        }
        
        step("Non-smoker’s declaration should show") {
            let app = XCUIApplication()
            let button = app.buttons[CommonStrings.DeclareButtonTitle114]
            sleep(3)
            XCTAssertTrue(button.exists)
        }
        
        step("Non-smoker’s declaration should show with Get Started button") {
            let app = XCUIApplication()
            let button = app.buttons[CommonStrings.DeclareButtonTitle114]
            self.test.waitFor10Secs(element: button)
            XCTAssertTrue(button.exists)
        }
        
        step("I tap on I declare") {
            let app = XCUIApplication()
            let declareButton = app.buttons[CommonStrings.DeclareButtonTitle114]
            sleep(3)
            declareButton.tap()
        }
        
        step("I tap Disagree button") {
            let app = XCUIApplication()
            let disagreeButton = app.toolbars.buttons[CommonStrings.DisagreeButtonTitle49]
            sleep(8)
            disagreeButton.tap()
        }
        
        step("Non-Smoker terms and conditions shown") {
            let app = XCUIApplication()
            let button = app.buttons[CommonStrings.DeclareButtonTitle114]
            sleep(3)
            XCTAssertTrue(button.exists)
        }
        
        step("I tap Agree button") {
            let app = XCUIApplication()
            let agreeButton = app.toolbars.buttons[CommonStrings.AgreeButtonTitle50]
            sleep(8)
            agreeButton.tap()
        }
        
        step("I navigate back to home screen") {
            let app = XCUIApplication()
            let cancelButton = app.otherElements.navigationBars.buttons[CommonStrings.CancelButtonTitle24]
            self.test.waitFor10Secs(element: cancelButton)
            cancelButton.tap()
        }
        
        step("I see the privacy policy") {
            let app = XCUIApplication()
            let privacyPolicy = app.toolbars.buttons[CommonStrings.AgreeButtonTitle50]
            let failureReason = "Privacy Policy is not displayed"
            XCTestCase.VitalityHelper.waitForElementToAppear(test: self.test, element: privacyPolicy)
            XCTAssertTrue(privacyPolicy.exists, failureReason)
        }
        
        step("Completed summary screen is shown") {
            let app = XCUIApplication()
            let button = app.buttons[CommonStrings.GreatButtonTitle120]
            self.test.waitFor10Secs(element: button)
            XCTAssertTrue(button.exists)
        }
        
        step("I tap on Great") {
            let app = XCUIApplication()
            let greatButton = app.buttons[CommonStrings.GreatButtonTitle120]
            self.test.waitFor10Secs(element: greatButton)
            greatButton.tap()
        }
        
        step("I tap on learn more button") {
            let app = XCUIApplication()
            let learnMoreButton = app.otherElements.buttons[CommonStrings.LearnMoreButtonTitle104]
            learnMoreButton.tap()
        }
        
        step("learn more screen should show") {
            let learnMoreLanding = XCUIApplication().navigationBars[CommonStrings.LearnMoreButtonTitle104]
            let failureReason = "Learn More Screen is not displayed"
            XCTestCase.VitalityHelper.waitForElementToAppear(test: self.test, element: learnMoreLanding)
            XCTAssertTrue(learnMoreLanding.exists, failureReason)
        }
        
        //        step("I tap on Participating Partners") {
        //            let app = XCUIApplication()
        //            let cellCount = app.cells.count - 1
        //            let participatingPartnersButton = app.cells.element(boundBy: cellCount).staticTexts
        //            participatingPartnersButton.element.tap()
        //        }
        //
        //        step("Participating Partners screen should show") {
        //            let partnerDetailScreen = XCUIApplication().staticTexts[NSDStrings.ParticipatingPartnersButtonText]
        //            let failureReason = "Participating Partners Screen is not displayed"
        //            XCTestCase.VitalityHelper.waitForElementToAppear(test: self.test, element: partnerDetailScreen)
        //            XCTAssertTrue(partnerDetailScreen.exists, failureReason)
        //        }
        //
        //        step("I tap on a Participating Partner") {
        //            let app = XCUIApplication()
        //            let cellCount = app.cells.count - 1
        //            let participatingPartnersButton = app.cells.element(boundBy: cellCount).buttons
        //            participatingPartnersButton.element.tap()
        //        }
        //
        //        step("more information about that partner should show") {
        //            XCTAssertTrue(XCUIApplication().otherElements.staticTexts["partner detail"].exists)
        //        }
    }
}
