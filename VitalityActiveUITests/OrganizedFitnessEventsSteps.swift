//
//  OrganizedFitnessEventsSteps.swift
//  VitalityActiveUITests
//
//  Created by ted.philip.l.lat on 23/11/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import UIKit
@testable import VIACore
@testable import VitalityKit
@testable import XCTest_Gherkin

class OrganizedFitnessEventsSteps: StepDefiner {
    
    override func defineSteps() {
        
        step("I tap on OFE card") {
            let app = XCUIApplication()
            let collectionViewsQuery = app.collectionViews
            
//            let activationBarCodeCard = collectionViewsQuery.staticTexts["card_heading_gym_activation_2073"]
//            if activationBarCodeCard.waitForExistence(timeout: 2) {
//                activationBarCodeCard.swipeLeft()
//            }
            
            let awcCard = collectionViewsQuery.staticTexts["home_card.card_section_title_364"]
            if awcCard.waitForExistence(timeout: 5) {
                awcCard.swipeLeft()
            }
            
            let wellnessPartnersCard = collectionViewsQuery.staticTexts["card_heading_wellness_partners_844"]
            if wellnessPartnersCard.waitForExistence(timeout: 5) {
                wellnessPartnersCard.swipeLeft()
            }
            
            let wdaCard = collectionViewsQuery.staticTexts["WDA.title_414"]
            if wdaCard.waitForExistence(timeout: 5) {
                wdaCard.swipeLeft()
            }
            
            let OFECard = collectionViewsQuery.staticTexts["ofe.heading.title_1335"]
            OFECard.tap()
        }
        
        step("I see OFE onboarding modal") {
            let app = XCUIApplication()
            let OFEOnboardingModal = app.tables.staticTexts["ofe.heading.title_1335"]
            XCTAssert(OFEOnboardingModal.waitForExistence(timeout: 2), "OFE Onboarding not shown")
        }
        
        step("I will see the OFE Learn More button") {
            let app = XCUIApplication()
            let learnMore = app.tables.buttons["learn_more_button_title_104"]
            XCTAssert(learnMore.waitForExistence(timeout: 2), "OFE Learn More button not shown")
        }
        
        step("I tap OFE Learn More") {
            let app = XCUIApplication()
            let learnMore = app.tables.buttons["learn_more_button_title_104"]
            learnMore.tap()
        }
        
        step("The OFE learn more will show") {
            let app = XCUIApplication()
            let learnMoreLanding = app.navigationBars["learn_more_button_title_104"]
            XCTAssert(learnMoreLanding.waitForExistence(timeout: 2), "OFE Learn More screen not shown")
        }
        
        step("I tap on back button") {
            let app = XCUIApplication()
            app.navigationBars["learn_more_button_title_104"].buttons["Back"].tap()
        }
        
        step("I tap OFE Got It") {
            let app = XCUIApplication()
            app.tables.buttons["dc.onboarding.got_it_131"].tap()
        }
        
        step("I will be navigated to OFE landing screen") {
            let app = XCUIApplication()
            let OFELandingScreen = app.navigationBars["ofe.heading.title_1335"]
            XCTAssert(OFELandingScreen.waitForExistence(timeout: 5), "OFE Landing screen not shown")
        }
        
        step("I tap to Events and Points cell") {
            sleep(4)
            let app = XCUIApplication()
            let tablesQuery = app.tables
            tablesQuery.staticTexts["OFE.LANDING.SECTION_HEADER_1396"].swipeUp()
            tablesQuery.staticTexts["ofe.landing.action_title_1344"].tap()
        }
        
        step("I should see Events and Points Screen") {
            let app = XCUIApplication()
            let eventsAndPointsScreen = app.navigationBars["ofe.landing.action_title_1344"]
            XCTAssert(eventsAndPointsScreen.waitForExistence(timeout: 2), "Events and Points screen not shown")
        }
        
        step("I tap to History cell") {
            sleep(4)
            let app = XCUIApplication()
            let tablesQuery = app.tables
            tablesQuery.staticTexts["OFE.LANDING.SECTION_HEADER_1396"].swipeUp()
            tablesQuery.staticTexts["history_button_140"].tap()
        }
        
        step("I should see OFE History Screen") {
            let app = XCUIApplication()
            let historyScreen = app.navigationBars["history_button_140"]
            XCTAssert(historyScreen.waitForExistence(timeout: 2), "History screen not shown")
        }
        
        step("I should see Learn More cell") {
            let app = XCUIApplication()
            let tablesQuery = app.tables
            let learnMoreCell = tablesQuery.staticTexts["learn_more_button_title_104"]
            XCTAssert(learnMoreCell.waitForExistence(timeout: 2), "Learn More cell not shown")
        }
        
        step("I should see Help cell") {
            let app = XCUIApplication()
            let tablesQuery = app.tables
            let helpCell = tablesQuery.staticTexts["menu_help_button_8"]
            XCTAssert(helpCell.waitForExistence(timeout: 2), "Help cell not shown")
        }
        
        step("I tap to Claim Points") {
            sleep(10)
            let app = XCUIApplication()
            app.tables.buttons["ofe.action.title_1337"].tap()
        }
        
        step("I should see Claim Points Screen") {
            let app = XCUIApplication()
            let claimPointsScreen = app.navigationBars["ofe.action.title_1337"]
            XCTAssert(claimPointsScreen.waitForExistence(timeout: 2), "Claim Points Screen not shown")
        }
        
        step("I tap to Event Type") {
            let app = XCUIApplication()
            app.tables.staticTexts["ofe.claim_points.placeholder_1366"].tap()
        }
        
        step("I should see Event Type Screen") {
            let app = XCUIApplication()
            let eventTypeScreen = app.navigationBars["ofe.claim_points.placeholder_1366"]
            XCTAssert(eventTypeScreen.waitForExistence(timeout: 2), "Event Type Screen not shown")
        }
        
        step("I type in Search textbox") {
            let app = XCUIApplication()
            app.tables.searchFields["Search"].tap()
            app.searchFields["Search"].tap()
            app.searchFields["Search"].typeText("No results")
        }
        
        step("I should see No Results label") {
            let app = XCUIApplication()
            let noResultsScreen = app.tables["ofe.search.no_results_1368, ofe.search.no_result_description_1369"]
            XCTAssert(noResultsScreen.waitForExistence(timeout: 2), "No Results label not shown")
        }
        
        step("I press cancel in Search textbox") {
            let app = XCUIApplication()
            app.buttons["Cancel"].tap()
        }
        
        step("I choose an Event Type") {
            let app = XCUIApplication()
            app.tables.cells.staticTexts["Running"].tap()
        }
        
        step("I should be back to Claim Points Screen") {
            let app = XCUIApplication()
            let claimPointsScreen = app.navigationBars["ofe.action.title_1337"]
            XCTAssert(claimPointsScreen.waitForExistence(timeout: 7), "Claim Points Screen not shown")
        }
        
        step("I tap Distance") {
            sleep(5)
            let app = XCUIApplication()
            app.tables.cells.staticTexts["Distance"].tap()
        }
        
        step("I should see Distance Screen") {
            let app = XCUIApplication()
            let distanceScreen = app.navigationBars["ofe.claim_points.distance_1371"]
            XCTAssert(distanceScreen.waitForExistence(timeout: 7), "Distance Screen not shown")
        }
        
        step("I choose a given Distance") {
            sleep(5)
            let app = XCUIApplication()
            app.tables.cells.staticTexts["5km to 10km"].tap()
        }
        
        step("I tap Event Date") {
            let app = XCUIApplication()
            app.tables.cells.staticTexts["ofe.claim_points.event_date_1373"].tap()
        }
        
        step("I should see Event Date Pop up") {
            let app = XCUIApplication()
            let eventDatePopup = app.alerts["\n\n\n\n\n\n\n\n"]
            let cancelButton = eventDatePopup.buttons["cancel_button_title_24"]
            let okButton = eventDatePopup.buttons["ok_button_title_40"]
            XCTAssert(cancelButton.exists && okButton.exists, "Even Date Pop up not shown")
        }
        
        step("I press Cancel in Event Date Pop up") {
            let app = XCUIApplication()
            let eventDatePopup = app.alerts["\n\n\n\n\n\n\n\n"]
            eventDatePopup.buttons["cancel_button_title_24"].tap()
        }
        
        step("I press Ok in Event Date Pop up") {
            let app = XCUIApplication()
            let eventDatePopup = app.alerts["\n\n\n\n\n\n\n\n"]
            eventDatePopup.buttons["ok_button_title_40"].tap()
        }
        
        step("I add Event Name") {
            let app = XCUIApplication()
            app.tables.textFields["Event Name"].tap()
            app.tables.textFields["Event Name"].typeText("Test Event Name")
        }
        
        step("I add Organiser") {
            let app = XCUIApplication()
            app.tables.textFields["Organiser"].tap()
            app.tables.textFields["Organiser"].typeText("Test Organiser")
        }
        
        step("I press Next in Claim Points Screen") {
            sleep(5)
            let app = XCUIApplication()
            app.tables.staticTexts["ofe.claim_points.header_1363"].tap()
            app.navigationBars["ofe.action.title_1337"].buttons["next_button_title_84"].tap()
        }
        
        step("I should be navigated to Image Proof Screen") {
            sleep(5)
            let app = XCUIApplication()
            let imageProofScreen = app.navigationBars["ofe.proof.title_1379"]
            XCTAssert(imageProofScreen.waitForExistence(timeout: 5), "Image Proof Screen not shown")
        }
        
        step("I press Add") {
            sleep(5)
            let app = XCUIApplication()
            app.collectionViews.buttons["ofe.proof.add_title_1385"].tap()
        }
        
        step("I press Choose from Library") {
            sleep(5)
            let app = XCUIApplication()
            app.sheets.buttons["proof.action_choose__from_library_alert_title_168"].tap()
        }
        
        step("I press Allow") {
            let app = XCUIApplication()
            sleep(5)
            let alertView = self.test.addUIInterruptionMonitor(withDescription: "“Vitality” Would Like to Access Your Photos") { (alert) -> Bool in
                if alert.staticTexts["“Vitality” Would Like to Access Your Photos"].exists {
                    alert.buttons["OK"].tap()
                } else {
                    alert.buttons["OK"].tap()
                }
                return true
            }
            app.tap()
            self.test.removeUIInterruptionMonitor(alertView)
        }
        
        step("I choose an Image") {
            let app = XCUIApplication()
            sleep(5)
            let collectionView = app.collectionViews["PhotosGridView"]
            collectionView.cells.element(boundBy: collectionView.cells.count - 1).tap()
        }
        
        step("I press Next in Images") {
            sleep(5)
            let app = XCUIApplication()
            app.navigationBars["ofe.proof.title_1379"].buttons["next_button_title_84"].tap()
        }
        
        step("I should see Weblink Proof Screen") {
            let app = XCUIApplication()
            let weblinkProofScreen = app.navigationBars["ofe.common.web_link_proof_1381"]
            XCTAssert(weblinkProofScreen.waitForExistence(timeout: 5), "Weblink Proof Screen not shown")
        }
        
        step("I add Website Link") {
            let app = XCUIApplication()
            let tablesQuery = app.tables
            tablesQuery.cells.textFields["ofe.proof.title_1388"].tap()
            tablesQuery.cells.textFields["ofe.proof.title_1388"].typeText("testlink.com")
            app.navigationBars["ofe.common.web_link_proof_1381"].buttons["next_button_title_84"].tap()
        }
        
        step("I press Next in Weblink") {
            let app = XCUIApplication()
            app.navigationBars["ofe.common.web_link_proof_1381"].buttons["next_button_title_84"].tap()
        }
        
        step("I should see Summary Screen") {
            let app = XCUIApplication()
            let summaryScreen = app.navigationBars["ofe.summary.title_1389"]
            XCTAssert(summaryScreen.waitForExistence(timeout: 2), "Summary Screen not shown")
        }
        
        step("I press Confirm") {
            let app = XCUIApplication()
            app.navigationBars["ofe.summary.title_1389"].buttons["confirm_title_button_182"].tap()
        }
        
        step("I press Agree in Agreement") {
            sleep(10)
            let app = XCUIApplication()
            app.toolbars["Toolbar"].buttons["agree_button_title_50"].tap()
        }
        
        step("I should see Fitness Event Submitted modal") {
            let app = XCUIApplication()
            let submittedModal = app.staticTexts["ofe.completed.header_1394"]
            XCTAssert(submittedModal.waitForExistence(timeout: 30), "Fitness Event Submitted modal not shown")
        }
        
    }
}
