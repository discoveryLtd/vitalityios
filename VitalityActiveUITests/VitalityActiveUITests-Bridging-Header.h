//
//  VIACoreTests-Bridging-Header.h
//  VitalityActive
//
//  Created by jacdevos on 2016/10/24.
//  Copyright © 2016 Glucode. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCUIApplication.h>
#import <XCTest/XCUIElement.h>

@interface XCUIApplication (Private)
- (id)initPrivateWithPath:(NSString *)path bundleID:(NSString *)bundleID;
- (void)resolve;
@end
