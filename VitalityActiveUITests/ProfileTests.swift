//
//  ProfileTests.swift
//  VitalityActive
//
//  Created by Val Tomol on 27/11/2017.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import XCTest
import VitalityKit
@testable import XCTest_Gherkin
@testable import VIACore

class ProfileTests: XCTestCase {
    func scenario(_ scenario: String) {
        NativeRunner(featureFile: "profile.feature", testCase: self).run(scenario: scenario)
    }
    
//    func testViewProfile() {
//        scenario("View profile")
//    }
//    
//    func testEmailInvalidFormat() {
//        scenario("Email invalid format")
//    }
//    
//    func testEmailMaximumNumberOfCharacters() {
//        scenario("Email exceed maximum number of characters")
//    }
//    
//    func testEmailExsisting() {
//        scenario("Email existing")
//    }
//    
//    func testEmailAvailable() {
//        scenario("Email available")
//    }
//    
//    // TODO: How to implement change email test?
//    // If this test is to be implemented, the next test will fail as the email address has changed.
//    func testEmailChange() {
//        scenario("Email change")        
//    }
    
//    func testEditProfilePic() {
//        scenario("Edit profile picture using phone library")
//    }
}
