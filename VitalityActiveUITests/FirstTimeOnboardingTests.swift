//
//  FirstTimeOnboardingTests.swift
//  VitalityActive
//
//  Created by Wilmar van Heerden on 2016/10/14.
//  Copyright © 2016 Glucode. All rights reserved.
//

import XCTest

class FirstTimeOnboardingTests: XCTestCase {

    private var app: XCUIApplication = XCUIApplication()

    override func setUp() {
        super.setUp()
        self.app.launch()
    }

    override func tearDown() {
        super.tearDown()
    }

//    func testOnboardingCopy() {
//        let scrollViewsQuery = XCUIApplication().scrollViews
//        let elementsQuery = scrollViewsQuery.otherElements
//        elementsQuery.staticTexts["onboarding_know_your_health_title"].tap()
//        elementsQuery.staticTexts["onboarding_know_your_health_description"].tap()
//
//        let element2 = scrollViewsQuery.children(matching: .other).element
//        let element = element2.children(matching: .other).element(boundBy: 5)
//        element.swipeLeft()
//        elementsQuery.staticTexts["onboarding_improve_your_health_title"].tap()
//        elementsQuery.staticTexts["onboarding_improve_your_health_description"].tap()
//        element2.children(matching: .other).element(boundBy: 4).swipeLeft()
//        elementsQuery.staticTexts["onboarding_enjoy_weekly_rewards_title"].tap()
//        elementsQuery.staticTexts["onboarding_enjoy_weekly_rewards_description"].tap()
//        element.swipeLeft()
//    }
}
