//
//  TermsAndConditionsSteps.swift
//  VitalityActive
//
//  Created by Dexter Anthony Ambrad on 12/16/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import XCTest
import VitalityKit
@testable import XCTest_Gherkin
@testable import VIACore

class TermsAndConditionsSteps: StepDefiner {
    
    override func defineSteps() {
        
        step("I tap Terms and Conditions") {
            let app = XCUIApplication()
            let tableRow = app.tables.staticTexts[ProfileStrings.SettingsTermsTitle]
            self.test.waitFor10Secs(element: tableRow)
            tableRow.tap()
        }
        step("I tap the Back from Terms and Conditions") {
            let app = XCUIApplication()
            let button = app.navigationBars[ProfileStrings.SettingsLandingTitle].buttons["Back"]
            self.test.waitFor10Secs(element: button)
            button.tap()
        }
        
        step("I navigate back to Settings") {
            self.step("I tap Profile Tab")
            self.step("I tap Settings")
            self.step("I tap Terms and Conditions")
            self.step("I can view Terms and Condition webview")
            self.step("I tap the Back from Terms and Conditions")
        }
        step("I navigate to Terms and Conditions") {
            self.step("I tap Profile Tab")
            self.step("I tap Settings")
            self.step("I tap Terms and Conditions")
        }
        
        step("I navigate to Settings") {
            self.step("I tap Profile Tab")
            self.step("I tap Settings")
        }
        step("I should be back at the Settings screen") {
            let app = XCUIApplication()
            let navigationTitle = app.navigationBars.staticTexts[ProfileStrings.SettingsLandingSettingsTitle]
            XCTestCase.VitalityHelper.waitForElementToAppear(test: self.test, element: navigationTitle)
            XCTAssertTrue(navigationTitle.exists, "Settings screen is not being displayed")
        }
        step("I can view Terms and Condition webview"){
            let app = XCUIApplication()
            let termsAndConditionsStaticText = app.webViews.staticTexts["Terms and Conditions"];
            self.test.waitFor10Secs(element: termsAndConditionsStaticText)
            XCTAssertTrue(termsAndConditionsStaticText.exists, "Terms and Conditions webview is not present")
        }
        step("Terms and Conditions row is present") {
            let app = XCUIApplication()
            let tableRow = app.tables.staticTexts[ProfileStrings.SettingsTermsTitle]
            self.test.waitFor10Secs(element: tableRow)
            XCTAssertTrue(tableRow.exists, "Terms and Conditions row is not present")
        }
    }
}
