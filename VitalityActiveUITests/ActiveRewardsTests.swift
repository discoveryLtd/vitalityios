//
//  ExampleFeatureTest.swift
//  VitalityActive
//
//  Created by jacdevos on 2016/11/23.
//  Copyright © 2016 Glucode. All rights reserved.
//

import Foundation
import XCTest
import VitalityKit
@testable import XCTest_Gherkin
@testable import VIACore

class ActiveRewardsTests: XCTestCase {
    func scenario(_ scenario: String) {
        NativeRunner(featureFile:  "activeRewards.feature", testCase: self).run(scenario: scenario)
    }

//    NON ACTIVATED ACTIVE REWARDS SCENARIOS
    
    func testARNotActivatedModal() {
        scenario("Active Rewards on-boaridng modal is displayed when user clicks on AR card on home screen")
    }
    
    func testLearnMoreIsVisible() {
        scenario("AR Learn More link is displayed under Get Started button")
    }
    
    func testARLearnMoreButton() {
        scenario("Check if user is able to tap on AR Learn More")
    }
    
    func testARLearnMoreScreen() {
        scenario("I can view the learn more screen from AR onboarding screen")
    }
    
    func testNavigateBackToARModal() {
        scenario("Check if user is able to return to Onboarding screen by taping on Back button of AR Learn More screen")
    }
    
/*    ACTIVATES THE ACTIVE REWARDS, ONCE ACTIVATED, THE NON ACTIVATED ACTIVE REWARDS SCENARIOS WILL FAIL
    func testActivateActiveRewards() {
        scenario("Activate Active Rewards")
    }
*/
 
//    ACTIVATED AR SCENARIOS
    
    func testActivatedRewardsScreen() {
        scenario("Navigate to Activated Active Rewards")
    }
    
    func testARPoints() {
        scenario("Check if user will be able to see the Points")
    }
    
    func testARThisWeekActivityCell() {
        scenario("Check if user will be able to see This Week Activity Cell")
    }
    
    
    func testARActivityScreen() {
        scenario("Navigate to Active Rewards Activity Screen")
    }
    
    func testInProgressActivityScreen() {
        scenario("Navigate In Progress Activity Screen")
    }
    
    func testARRewardsScreen() {
        scenario("Navigate to Active Rewards Rewards Screen")
    }
    
    func testHistoryInARRewardsScreen() {
        scenario("Navigate to History in Rewards Screen")
    }
    
    func testActivatedRewardsLearnMoreScreen() {
        scenario("Navigate to Active Rewards Learn More Screen")
    }
    
    func testParticipatingPartnersInActivatedRewardsLearnMoreScreen() {
        scenario("Navigate to Participating Partners in Learn More Screen")
    }
    
    func testBenefitsGuideInActivatedRewardsLearnMoreScreen() {
        scenario("Navigate to Benefits Guide in Learn More Screen")
    }
    
    func testARHelpScreen() {
        scenario("Navigate to Active Rewards Help Screen")
    }
    
    func testDisplayWellnessDevices() {
        scenario("Display Wellness Devices")
    }

//    TO DO: CACHING and DISPLAY WELLNESS DEVICES
    
}
