//
//  RegistrationTests.swift
//  VitalityActive
//
//  Created by Wilmar van Heerden on 2016/11/07.
//  Copyright © 2016 Glucode. All rights reserved.
//

import Foundation
import XCTest
import VitalityKit
import XCTest_Gherkin
@testable import VIACore

class RegistrationTests: XCTestCase {
    
    func scenario(_ scenario: String) {
        NativeRunner(featureFile: "registration.feature", testCase: self).run(scenario: scenario)
    }
    
    func testViewRegistrationForm() {
        scenario("User successfully clicks the 'Register' link and is able to view the online Registration form")
    }
    
//    func testSuccessfulRegistration() {
//        scenario("User is successfully registered")
//    }
    
    func testIncorrectButValidEmail() {
        scenario("Incorrect but valid Email")
    }
    
    func testIncorrectButValidInsureCode() {
        scenario("Incorrect but valid Insurer Code")
    }
    
    func testAlreadyRegisteredUser() {
        scenario("Already registered user")
    }
    
    func testInsurerCodeFooter() {
        scenario("Show registration screen with insurer code info")
    }
    
    func testInsurerCodeUsedBefore() {
        scenario("Insurer Code that has been used before")
    }
    
    func testRegisterDisabledWithoutEmail() {
        scenario("Register link disabled with no email")
    }
    
    func testResgisterDisabledWithoutPassword() {
        scenario("Register link disabled with no password")
    }
    
    func testRegisterDisabledWithoutConfirmPassword() {
        scenario("Register link disabled with no confirm password")
    }
    
    func testRegisterDisabledWithoutInsurerCode() {
        scenario("Register link disabled with no insurer code")
    }
    
    func testShowRegistrationScreenWithInsurerCodeInfo() {
        scenario("Show registration screen with insurer code info")
    }
    
    // REGEX USERNAME TESTS //
    
    func testNumericAndLowerCaseEmail() {
        scenario("Numeric and lower case email")
    }
    
    func testUpperCaseEmail() {
        scenario("Upper case email")
    }
    
    func testOnlyUpperCaseEmail() {
        scenario("Only upper case email")
    }
    
    func testNumbericalEmailCharacters() {
        scenario("Email with only numerical characters")
    }
    
    func testUppercaseAndNumberical() {
        scenario("Upper case and numerical email")
    }
    
    func testSpecialCharacterPlus() {
        scenario("Plus characted in email")
    }
    
    func testSpecialCharacterHash() {
        scenario("Hash character in email")
    }
    
    func testSpecialCharacterUnderscore() {
        scenario("Underscore in email")
    }
    
    func testSpecialCharacterDollar() {
        scenario("Dollar sign in email")
    }
    
    func testSpecialCharacterAnd() {
        scenario("And sign in email")
    }
    
    func testSpecialCharacterPercentage() {
        scenario("Percentage sign in email")
    }
    
    func testSpecialCharacterSemiColon() {
        scenario("Semicolon character in email")
    }
    
    func testSpecialCharacterStar() {
        scenario("Star character in the email")
    }
    
    func testSpecialCharacterHyphen() {
        scenario("Hyphen in the email")
    }
    
    func testSpecialCharacterDash() {
        scenario("Forward slash in email")
    }
    
    func testSpecialCharacterEqualSign() {
        scenario("Equal sign in the email")
    }
    
    func testSpecialCharacterQuestionMark() {
        scenario("Question mark in the email")
    }
    
    func testSpecialCharacterUpArrow() {
        scenario("Caret character in email")
    }
    
    func testSpecialCharacterBackTick() {
        scenario("Grave accent in the email")
    }
    
    func testSpecialCharacterBrackets() {
        scenario("Curly brackets in the email")
    }
    
    func testSpecialCharacterTilde() {
        scenario("Tilde in the email")
    }
    
    func testSpecialCharacterVerticalBar() {
        scenario("Line bar in the email")
    }
    
    func testEmailWithOnlyTwoLocalCharacters() {
        scenario("Email with only two local characters")
    }
    
    func testEmailWith64LocalCharacters() {
        scenario("Email with 64 local characters")
    }
    
    func testEmailWithOnlyLowerCaseInDomain() {
        scenario("Email with only lower case in domain")
    }
    
    func testEmailWithOnlyUpperCaseInDomain() {
        scenario("Email with only upper case in domain")
    }
    
    func testEmailWithNumericalInDomain() {
        scenario("Email with numerical in domain")
    }
    
    func testEmailWithUpperLowerAndNumericalDomain() {
        scenario("Email with uppercase and lower and numerical domain")
    }
    
    func testEmailWith2SegmentsDomain() {
        scenario("Email with 2 segments in domain")
    }
    
    func testEmailWith3SegmentsDomain() {
        scenario("Email with 3 segments in domain")
    }
    
    func testEmailWith4SegmentsDomain() {
        scenario("Email with 4 segments in domain")
    }
    
    func testEmailWith5SegmentsDomain() {
        scenario("Email with 5 segments in domain")
    }
    
    func testEmailWith2LocalAnd247Domain() {
        scenario("Email with 2 local and 247 domain characters")
    }
    
    func testEmailWithHyphenInDomain() {
        scenario("Email with hyphen in domain")
    }
    
    func testEmailWithCapitalTLD() {
        scenario("Email with capital TLD")
    }
    
    func testEmailWith2TLDCharacters() {
        scenario("Email with 2 TLD characters")
    }
    
    func testEmailWith24TLDCharacters() {
        scenario("Email with 24 TLD characters")
    }
}
