//
//  ExampleFeatureTest.swift
//  VitalityActive
//
//  Created by jacdevos on 2016/11/13.
//  Copyright © 2016 Glucode. All rights reserved.
//

import Foundation
import XCTest
import VitalityKit
@testable import XCTest_Gherkin
@testable import VIACore

class LoginTests: XCTestCase {
   
    func scenario(_ scenario: String) {
        NativeRunner(featureFile: "login.feature", testCase: self).run(scenario: scenario)
    }
    
    func testIncorrectEmailAndPassword() {
        scenario("Incorrect email and password")
    }
    
    func testValidEmailAndIncorrectPassword() {
        scenario("Valid email and Incorrect password")
    }
    
    func testIncorrectEmailAndCorrectPassword() {
        scenario("Incorrect email and correct password")
    }
    
    func testEmptyEmail() {
        scenario("Empty email")
    }
    
    func testEmptyPassword() {
        scenario("Empty password")
    }
    
    func testEmptyEmailAndPassword() {
        scenario("Empty email and password")
    }
    
    func testForgotPassword() {
        scenario("Forgot password prompt")
    }
    
    func testNavigateToForgotPasswordScreen() {
        scenario("Navigate to Forgot Password Screen")
    }
    
    func testTapOnForgotPassword() {
        scenario("To check that I am navigated to forgot password when tapping on Forgot Password")
    }
    
    func testDoneButtonEnabledWithValidEmail() {
        scenario("To check entering valid email address for forgot password")
    }
    
    func testTapOnDoneButtonWithValidEmail() {
        scenario("To check successful submission of Email address for Forgot Password")
    }
    
    func testEmailNotRegistered() {
        scenario("Email not registered with forgot password")
    }
    
    func testLoginSuccess() {
        scenario("Log in success")
    }
}
