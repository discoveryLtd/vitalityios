//
//  WellnessDevicesSteps.swift
//  VitalityActive
//
//  Created by Sebastian Cichon on 2017/08/02.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import XCTest
import VitalityKit
@testable import XCTest_Gherkin
@testable import VIACore

class WellnessDevicesSteps: StepDefiner {
    
    override func defineSteps() {
        
        step("I tap on Wellness Devices card") {
            let app = XCUIApplication()
            // We fetch all buttons matching "WDA.home_card.title_415" (accordianButtonsQuery is a XCUIElementQuery)
            let accordianButtonsQuery = app.otherElements.staticTexts.matching(identifier: "WDA.home_card.title_415")
            // Delay due to services
            sleep(5)
            // If there is at least one
            if accordianButtonsQuery.count > 0 {
                // We take the first one and tap it
                let firstButton = accordianButtonsQuery.element(boundBy: 0)
                firstButton.tap()
            }
        }
        
        step("I see Wellness Devices onboarding screen") {
            let app = XCUIApplication()
            let WDAOnboarding = app.otherElements.staticTexts[CommonStrings.Wda.Onboarding.Heading416]
            let failureReason = "WDA Onboarding not displayed"
            XCTestCase.VitalityHelper.waitForElementToAppear(test: self.test, element: WDAOnboarding)
            XCTAssertTrue(WDAOnboarding.exists, failureReason)
        }
        
        step("I tap on Got It button") {
            let app = XCUIApplication()
            let GotItbutton = app.otherElements.buttons[CommonStrings.Wda.Onboarding.GotItTitle2423]
            self.test.waitFor10Secs(element: GotItbutton)
            GotItbutton.tap()
        }
        
        step("I will see I need a device or app link") {
            let app = XCUIApplication()
            let deviceText = app.otherElements.buttons[CommonStrings.Wda.Onboarding.INeedADeviceOrAppTitle423]
            let failureReason = "Device or App link not displayed"
            XCTestCase.VitalityHelper.waitForElementToAppear(test: self.test, element: deviceText)
            XCTAssertTrue(deviceText.exists, failureReason)
        }
        
        step("I will see WDA Learn More button") {
            let app = XCUIApplication()
            let learnMore = app.otherElements.staticTexts[CommonStrings.LearnMoreButtonTitle104]
            let failureReason = "WDA Learn More button not displayed"
            XCTestCase.VitalityHelper.waitForElementToAppear(test: self.test, element: learnMore)
            XCTAssertTrue(learnMore.exists, failureReason)
        }
        
        step("I tap on WDA Learn More") {
            let app = XCUIApplication()
            let learnMoreButton = app.otherElements.staticTexts[CommonStrings.LearnMoreButtonTitle104]
            self.test.waitFor10Secs(element: learnMoreButton)
            learnMoreButton.tap()
        }
        
        step("I will see Learn More screen for Wellness Devices") {
            let app = XCUIApplication()
            let learnMore = app.navigationBars.staticTexts[CommonStrings.LearnMoreButtonTitle104]
            let failureReason = "WDA Learn More screen not displayed"
            XCTestCase.VitalityHelper.waitForElementToAppear(test: self.test, element: learnMore)
            XCTAssertTrue(learnMore.exists, failureReason)
        }
        
        step("I see Wellness Devices landing screen") {
            let app = XCUIApplication()
            let WDAlanding = app.navigationBars.staticTexts[CommonStrings.Wda.Title414]
            let failureReason = "WDA Learn More screen not displayed"
            XCTestCase.VitalityHelper.waitForElementToAppear(test: self.test, element: WDAlanding)
            XCTAssertTrue(WDAlanding.exists, failureReason)
        }
        
        step("I will see the available to link heading") {
            let app = XCUIApplication()
            let availableDevices = app.staticTexts[CommonStrings.Wda.Landing.AvailableLinkSectionTitle426]
            let failureReason = "WDA Learn More screen not displayed"
            XCTestCase.VitalityHelper.waitForElementToAppear(test: self.test, element: availableDevices)
            XCTAssertTrue(availableDevices.exists, failureReason)
        }
        
        step("I will see at least one device") {
            let app = XCUIApplication()
            let deviceName = app.staticTexts[CommonStrings.Wda.HealthApp455]
            let failureReason = "WDA Learn More screen not displayed"
            XCTestCase.VitalityHelper.waitForElementToAppear(test: self.test, element: deviceName)
            XCTAssertTrue(deviceName.exists, failureReason)
        }
        
        step("I see WDA Help button") {
            let app = XCUIApplication()
            let helpButton = app.cells.staticTexts[CommonStrings.HelpButtonTitle141]
            let failureReason = "WDA Learn More screen not displayed"
            XCTestCase.VitalityHelper.waitForElementToAppear(test: self.test, element: helpButton)
            XCTAssertTrue(helpButton.exists, failureReason)
        }
        
        step("I tap on WDA Help") {
            let app = XCUIApplication()
            let helpButton = app.cells.staticTexts[CommonStrings.LearnMoreButtonTitle104]
            self.test.waitFor10Secs(element: helpButton)
            helpButton.tap()
        }
        
        step("I tap on Health App") {
            let app = XCUIApplication()
            let healthApp = app.cells.staticTexts[CommonStrings.Wda.HealthApp455]
            sleep(15)
            healthApp.tap()
        }
        
        step("I see the device details screen for Health App") {
            let app = XCUIApplication()
            let healthAppDetails = app.navigationBars.staticTexts[CommonStrings.Wda.HealthApp455]
            let failureReason = "Health App detail screen not displayed"
            XCTestCase.VitalityHelper.waitForElementToAppear(test: self.test, element: healthAppDetails)
            XCTAssertTrue(healthAppDetails.exists, failureReason)
        }
        
        step("I tap on Adidas") {
            let app = XCUIApplication()
            let adidasButton = app.cells.staticTexts["Adidas"]
            sleep(10)
            adidasButton.tap()
        }
        
        step("I will see points earning metrics") {
            let app = XCUIApplication()
            let pointsEarning = app.otherElements.staticTexts[CommonStrings.Wda.PointsEarning.MetricsHeader435]
            let failureReason = "Health App detail screen not displayed"
            XCTestCase.VitalityHelper.waitForElementToAppear(test: self.test, element: pointsEarning)
            XCTAssertTrue(pointsEarning.exists, failureReason)
        }
        
        step("I will see Steps to Link") {
            let app = XCUIApplication()
            let stepsLink = app.cells.buttons[CommonStrings.StepsToLinkButtonText471]
            let failureReason = "Health App detail screen not displayed"
            XCTestCase.VitalityHelper.waitForElementToAppear(test: self.test, element: stepsLink)
            XCTAssertTrue(stepsLink.exists, failureReason)
        }
        
        step("I will tap on Steps to Link") {
            let app = XCUIApplication()
            let healthApp = app.cells.buttons[CommonStrings.StepsToLinkButtonText471]
            self.test.waitFor10Secs(element: healthApp)
            healthApp.tap()
        }
        
        step("I will tap learn more link under points earning metrics") {
            let app = XCUIApplication()
            // We fetch all buttons matching "WDA.home_card.title_415" (accordianButtonsQuery is a XCUIElementQuery)
            let accordianButtonsQuery = app.otherElements.staticTexts.matching(identifier: "WDA.points_earning_metrics.content_436")
            // If there is at least one
            if accordianButtonsQuery.count > 0 {
                // We take the second one and tap it
                let secondLink = accordianButtonsQuery.element(boundBy: 1)
                secondLink.tap()
            }
        }
        
        step("I tap About Adidas") {
            let app = XCUIApplication()
            let aboutButton = app.cells.staticTexts[CommonStrings.AboutText450("Adidas")]
            self.test.waitFor10Secs(element: aboutButton)
            aboutButton.tap()
        }
        
        step("I will see the about screen for Adidas") {
            let app = XCUIApplication()
            let aboutAdidas = app.navigationBars[CommonStrings.AboutText450("Adidas")]
            let failureReason = "About Adidas screen not displayed"
            XCTestCase.VitalityHelper.waitForElementToAppear(test: self.test, element: aboutAdidas)
            XCTAssertTrue(aboutAdidas.exists, failureReason)
        }
        
        step("I will see Physical Activity header") {
            let app = XCUIApplication()
            let physicalActivity = app.otherElements.staticTexts[CommonStrings.Wda.PhysicalActivity.SectionHeader438]
            let failureReason = "Physical Activities are not displayed"
            XCTestCase.VitalityHelper.waitForElementToAppear(test: self.test, element: physicalActivity)
            XCTAssertTrue(physicalActivity.exists, failureReason)
        }
        
        step("I tap on Link Now") {
            let app = XCUIApplication()
            let linkButton = app.cells.buttons[CommonStrings.LinkNowButtonText434]
            self.test.waitFor10Secs(element: linkButton)
            linkButton.tap()
        }
        
        step("I see the Data Sharing Consent") {
            let app = XCUIApplication()
            let dataSharing = app.toolbars.buttons[CommonStrings.AgreeButtonTitle50]
            let failureReason = "Data Sharing screen not displayed"
            sleep(6)
            XCTAssertTrue(dataSharing.exists, failureReason)
        }
        
        step("I turn all categories on") {
            let app = XCUIApplication()
            let allowCategories = app.staticTexts["Turn All Categories On"]
            self.test.waitFor10Secs(element: allowCategories)
            allowCategories.tap()
        }
        
        step("I tap Allow") {
            let app = XCUIApplication()
            let allowButton = app.buttons["Allow"]
            self.test.waitFor10Secs(element: allowButton)
            allowButton.tap()
        }
        
        step("I tap on Dont Allow") {
            let app = XCUIApplication()
            let dontAllow = app.buttons["Don't Allow"]
            self.test.waitFor10Secs(element: dontAllow)
            dontAllow.tap()
        }
        
        step("I see linked heading") {
            let app = XCUIApplication()
            let dataSharing = app.staticTexts[CommonStrings.Wda.Landing.LinkedSectionTitle472]
            let failureReason = "Linked heading not displayed"
            sleep(5)
            XCTAssertTrue(dataSharing.exists, failureReason)
        }
        
        step("I see get started button") {
            let app = XCUIApplication()
            let getStarted = app.buttons[CommonStrings.HealthKit.Onboarding.ButtonTitle453]
            let failureReason = "Get Started button not displayed"
            XCTAssertTrue(getStarted.exists, failureReason)
        }
        
        step("I tap WDA Get Started") {
            let app = XCUIApplication()
            let getStarted = app.buttons[CommonStrings.HealthKit.Onboarding.ButtonTitle453]
            self.test.waitFor10Secs(element: getStarted)
            getStarted.tap()
        }
        
        step("I see the Done button on website") {
            let app = XCUIApplication()
            let doneButton = app.buttons["Done"]
            let failureReason = "Website screen not displayed"
            sleep(5)
            XCTAssertTrue(doneButton.exists, failureReason)
        }
        
        step("I see available to link heading") {
            let app = XCUIApplication()
            let dataSharing = app.staticTexts[CommonStrings.Wda.Landing.AvailableLinkSectionTitle426]
            let failureReason = "Available to link heading not displayed"
            sleep(5)
            XCTAssertTrue(dataSharing.exists, failureReason)
        }
        
        step("I see data can take up to 2 days to sync message") {
            let app = XCUIApplication()
            let dataSharing = app.staticTexts[CommonStrings.Wda.Landing.SyncFooterText473]
            let failureReason = "Available to link heading not displayed"
            sleep(5)
            XCTAssertTrue(dataSharing.exists, failureReason)
        }
    }
}
