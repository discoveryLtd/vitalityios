//
//  UserPreferencesSteps.swift
//  VitalityActive
//
//  Created by Sebastian Cichon on 2017/04/04.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import XCTest
import VitalityKit
@testable import XCTest_Gherkin
@testable import VIACore

class UserPreferencesSteps: StepDefiner {
    
    override func defineSteps() {
        
        step("I will see user preferences") {
            let app = XCUIApplication()
            let preferenceHeader = app.tables.staticTexts[CommonStrings.UserPrefs.CommunicationGroupHeaderTitle64]
            self.test.waitFor10Secs(element: preferenceHeader)
            XCTAssert(preferenceHeader.exists, "User Preferences screen not shown. Expected to show.")
        }
        
        step("I will see communication preferences with text") {
            let app = XCUIApplication()
            let communicationHeader = app.tables.staticTexts[CommonStrings.UserPrefs.CommunicationGroupHeaderMessage65]
            self.test.waitFor10Secs(element: communicationHeader)
            XCTAssert(communicationHeader.exists, "Communication Header not shown. Expected to show.")
        }
        
        step("I will see email header") {
            let app = XCUIApplication()
            let emailHeader = app.tables.staticTexts[CommonStrings.EmailFieldPlaceholder18]
            self.test.waitFor10Secs(element: emailHeader)
            XCTAssert(emailHeader.exists, "Email Header not shown. Expected to show.")
        }
        
        step("I am able to toggle the email button") {
            let app = XCUIApplication()
            let emailToggle = app.cells.switches.element(boundBy: 0)
            self.test.waitFor10Secs(element: emailToggle)
            emailToggle.tap()
        }
        
        step("I disable the email switch") {
            let app = XCUIApplication()
            let emailToggle = app.cells.switches.element(boundBy: 0)
            self.test.waitFor10Secs(element: emailToggle)
            emailToggle.tap()
        }
        
        //        step("Email switch should be disabled") {
        //            let app = XCUIApplication()
        //            let emailToggle = app.cells.switches.element(boundBy: 0)
        //            self.test.waitFor10Secs(element: emailToggle)
        //            XCTAssert(UISwitch.off)
        //        }
        
        step("I will see push notifications header") {
            let app = XCUIApplication()
            let emailHeader = app.tables.staticTexts[CommonStrings.UserPrefs.PushMessageToggleTitle67]
            self.test.waitFor10Secs(element: emailHeader)
            XCTAssert(emailHeader.exists, "Push Notifications Header not shown. Expected to show.")
        }
        
        step("I will see the push notification button is disabled") {
            let app = XCUIApplication()
            let emailToggle = app.cells.switches.element(boundBy: 2)
            self.test.waitFor10Secs(element: emailToggle)
            XCTAssert(emailToggle.exists, "Push Notification button is not visible. Expected to show.")
        }
        
        step("I am able to toggle the push notification button to enabled") {
            let app = XCUIApplication()
            let pushToggle = app.cells.switches.element(boundBy: 2)
            self.test.waitFor10Secs(element: pushToggle)
            XCTestCase.VitalityHelper.scrollTo(element: pushToggle)
            pushToggle.tap()
        }
        
        step("I will see privacy section") {
            let app = XCUIApplication()
            let privacyHeader = app.tables.staticTexts[CommonStrings.UserPrefs.PrivacyGroupHeaderTitle70]
            self.test.waitFor10Secs(element: privacyHeader)
            XCTAssert(privacyHeader.exists, "Privacy Header not shown. Expected to show.")
        }
        
        step("I will see text under privacy section") {
            let app = XCUIApplication()
            let privacyText = app.tables.staticTexts[CommonStrings.UserPrefs.PrivacyGroupHeaderMessage71]
            self.test.waitFor10Secs(element: privacyText)
            XCTAssert(privacyText.exists, "Privacy explanatory text not shown. Expected to show.")
        }
        
        step("I will see privacy statement link") {
            let app = XCUIApplication()
            let privacyLink = app.cells.buttons[CommonStrings.UserPrefs.PrivacyGroupHeaderLinkButtonTitle72]
            self.test.waitFor10Secs(element: privacyLink)
            XCTAssert(privacyLink.exists, "Privacy explanatory text not shown. Expected to show.")
        }
        
        step("I tap on Privacy statement") {
            let app = XCUIApplication()
            let privacyLink = app.cells.buttons[CommonStrings.UserPrefs.PrivacyGroupHeaderLinkButtonTitle72]
            self.test.waitFor10Secs(element: privacyLink)
            privacyLink.tap()
        }
        
        step("I am able to view the analytics header") {
            let app = XCUIApplication()
            let analyticsHeader = app.tables.staticTexts[CommonStrings.UserPrefs.AnalyticsToggleTitle73]
            self.test.waitFor10Secs(element: analyticsHeader)
            XCTAssert(analyticsHeader.exists, "Analytics Header not shown. Expected to show.")
        }
        
        step("I will see text under analytics") {
            let app = XCUIApplication()
            let analyticsText = app.tables.staticTexts[CommonStrings.UserPrefs.AnalyticsToggleMessage74]
            self.test.waitFor10Secs(element: analyticsText)
            XCTAssert(analyticsText.exists, "Analytics explanatory text not shown. Expected to show.")
        }
        
        step("I will see the analytics toggle is enabled") {
            let app = XCUIApplication()
            let emailToggle = app.cells.switches.element(boundBy: 2)
            self.test.waitFor10Secs(element: emailToggle)
            XCTAssert(emailToggle.exists, "Analytics button is not visible. Expected to show.")
        }
        
        step("I can tap on the analytics toggle") {
            let app = XCUIApplication()
            let analyticsToggle = app.cells.switches.element(boundBy: 2)
            sleep(1)
            if (analyticsToggle.exists) {
                analyticsToggle.tap()
            } else {
                XCUIApplication().cells.switches.element(boundBy: 3).tap()
            }
        }
        
        step("I am able to view the crash reports header") {
            let app = XCUIApplication()
            let crashHeader = app.tables.staticTexts[CommonStrings.UserPrefs.CrashReportsToggleTitle75]
            self.test.waitFor10Secs(element: crashHeader)
            XCTAssert(crashHeader.exists, "Crash Report header not shown. Expected to show.")
        }
        
        step("I will see text under crash reports") {
            let app = XCUIApplication()
            let crashText = app.tables.staticTexts[CommonStrings.UserPrefs.CrashReportsToggleMessage76]
            self.test.waitFor10Secs(element: crashText)
            XCTAssert(crashText.exists, "Crash Report text not shown. Expected to show.")
        }
        
        step("I will see the crash reports toggle is enabled") {
            let app = XCUIApplication()
            let crashToggle = app.cells.switches.element(boundBy: 4)
            self.test.waitFor10Secs(element: crashToggle)
            XCTAssert(crashToggle.exists, "Crash Report button is not visible. Expected to show.")
        }
        
        step("I can tap on the crash reports toggle") {
            let app = XCUIApplication()
            let crashToggle = app.cells.switches.element(boundBy: 4)
            sleep(1)
            if (crashToggle.exists) {
                crashToggle.tap()
            } else {
                XCUIApplication().cells.switches.element(boundBy: 5).tap()
            }
        }
        
        step("I will see security section") {
            let app = XCUIApplication()
            let securityHeading = app.tables.staticTexts[CommonStrings.UserPrefs.SecurityGroupHeaderTitle77]
            self.test.waitFor10Secs(element: securityHeading)
            XCTAssert(securityHeading.exists, "Security Heading not shown. Expected to show.")
        }
        
        step("I will see manage privacy and security settings footer") {
            let app = XCUIApplication()
            let preferencesFooter = app.tables.staticTexts[CommonStrings.UserPrefs.UserPrefsScreenFootnoteMessage83]
            self.test.waitFor10Secs(element: preferencesFooter)
            XCTAssert(preferencesFooter.exists, "User Preferences footer not shown. Expected to show.")
        }
        
        step("I will see text under security") {
            let app = XCUIApplication()
            let securityText = app.tables.staticTexts[CommonStrings.UserPrefs.SecurityGroupHeaderMessage78]
            self.test.waitFor10Secs(element: securityText)
            XCTAssert(securityText.exists, "Security explanatory text not shown. Expected to show.")
        }
        
        step("I am able to view the remember me header") {
            let app = XCUIApplication()
            let remembermeHeader = app.tables.staticTexts[CommonStrings.UserPrefs.RememberMeToggleTitle81]
            self.test.waitFor10Secs(element: remembermeHeader)
            XCTAssert(remembermeHeader.exists, "Remember Me header not shown. Expected to show.")
        }
        
        step("I will see text under remember me") {
            let app = XCUIApplication()
            let remembermeText = app.tables.staticTexts[CommonStrings.UserPrefs.RememberMeToggleTitle81]
            self.test.waitFor10Secs(element: remembermeText)
            XCTAssert(remembermeText.exists, "Remember Me explanatory text not shown. Expected to show.")
        }
        
        step("I will see the remember me toggle is enabled") {
            let app = XCUIApplication()
            let rememberToggle = app.cells.switches.element(boundBy: 6)
            self.test.waitFor10Secs(element: rememberToggle)
            XCTAssert(rememberToggle.exists, "Remember Me button is not visible. Expected to show.")
        }
        
        step("I scroll to remember me") {
            let app = XCUIApplication()
            let rememberScroll = app.buttons[CommonStrings.NextButtonTitle84]
            XCTestCase.VitalityHelper.scrollTo(element: rememberScroll)
            XCTAssert(rememberScroll.exists, "Remember Me button is not visible. Expected to show.")
        }
        
        step("I can tap on the remember me toggle") {
            let app = XCUIApplication()
            let rememberToggle = app.cells.switches.element(boundBy: 6)
            sleep(1)
            if (rememberToggle.exists) {
                rememberToggle.tap()
            } else {
                XCUIApplication().cells.switches.element(boundBy: 7).tap()
            }
        }
        
        step("I scroll to analytics") {
            let app = XCUIApplication()
            let analyticsScroll = app.buttons[CommonStrings.NextButtonTitle84]
            XCTestCase.VitalityHelper.scrollTo(element: analyticsScroll)
            XCTAssert(analyticsScroll.exists, "Analytics button is not visible. Expected to show.")
        }
        
        step("I scroll to crash reports") {
            let app = XCUIApplication()
            let crashreportsScroll = app.buttons[CommonStrings.NextButtonTitle84]
            XCTestCase.VitalityHelper.scrollTo(element: crashreportsScroll)
            XCTAssert(crashreportsScroll.exists, "Crash Reports button is not visible. Expected to show.")
        }
        
        step("I tap next on user preferences") {
            sleep(10)
            let app = XCUIApplication()
            let userPreferencesScreen = app.tables.staticTexts[CommonStrings.UserPrefs.CommunicationGroupHeaderMessage65]

            if (userPreferencesScreen.exists) {
                let nextButton = app.buttons[CommonStrings.NextButtonTitle84]
                XCTestCase.VitalityHelper.scrollTo(element: nextButton)
                nextButton.tap()
            }
            
            let alert = app.alerts["user_prefs.no_preferences_set_alert_title_85"]
            if (alert.exists) {
                alert.buttons["continue_button_title_87"].tap()
            }
        }
        
        step("I scroll to the bottom of user preferences") {
            sleep(10)
            let app = XCUIApplication()
            let userPreferencesScreen = app.tables.staticTexts[CommonStrings.UserPrefs.CommunicationGroupHeaderMessage65]
            
            if (userPreferencesScreen.exists) {
                let nextButton = app.buttons[CommonStrings.NextButtonTitle84]
                XCTestCase.VitalityHelper.scrollTo(element: nextButton)
                //nextButton.tap()
            }
            
//            let alert = app.alerts["user_prefs.no_preferences_set_alert_title_85"]
//            if (alert.exists) {
//                alert.buttons["continue_button_title_87"].tap()
//            }
        }
        
        
        step("I have disabled all user preferences") {
            self.step("I scroll to analytics")
            self.step("I can tap on the analytics toggle")
            self.step("I can tap on the crash reports toggle")
            self.step("I can tap on the remember me toggle")
        }
        
        step("I see You Havent Allowed Any Permissions alert") {
            let app = XCUIApplication()
            let permissionAlert = app.alerts.staticTexts["You Haven't Allowed Any Permissions"]
            self.test.waitFor10Secs(element: permissionAlert)
            XCTAssert(permissionAlert.exists, "Permission Alert is not visible. Expected to show.")
        }
        
        //        step("I will get a permission alert") {
        //            let app = XCUIApplication()
        //            let emailHeader = app.staticTexts["Would Like to Send You Notifications"]
        //            self.test.waitFor10Secs(element: emailHeader)
        //            XCTAssert(emailHeader.exists , "Permission alert is not visible. Expected to show.") 
        //        }
    }
}
