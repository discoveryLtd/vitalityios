//
//  FingerPrintLoginTests.swift
//  VitalityActiveUITests
//
//  Created by ted.philip.l.lat on 18/10/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import XCTest
import VitalityKit
@testable import XCTest_Gherkin
@testable import VIACore

class FingerPrintLoginTests: XCTestCase {
        
    func scenario(_ scenario: String) {
        NativeRunner(featureFile: "fingerprintlogin.feature", testCase: self).run(scenario: scenario)
    }
    
    func testNavigateToProfile() {
        scenario("Navigate to Profile")
    }
    
    func testNavigateToSettings() {
        scenario("Navigate to Settings")
    }
    
    func testNavigateToSecurity() {
        scenario("Navigate to Security")
    }
    
    func testEnableTouchIDinSettings() {
        scenario("Enable Touch ID on Settings")
    }
    
    func testUserPreferencesShownAfterLogin() {
        scenario("User Preferences shown after login")
    }
    
    func testScrollToSecurity() {
        scenario("Scroll to Security")
    }
    
    func testEnableTouchIDinUserPreferences() {
        scenario("Enable Touch ID in User Preference after login")
    }
    
    func testCancelActivationInSettings() {
        scenario("Cancel Activation in Settings")
    }
    
    func testCancelActivationInUserPreferences() {
        scenario("Cancel Activation in User Preference after login")
    }
}
