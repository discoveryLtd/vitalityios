//
//  InactiveMemberSteps.swift
//  VitalityActiveUITests
//
//  Created by ted.philip.l.lat on 09/10/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import XCTest
import VIACore
import VitalityKit
@testable import XCTest_Gherkin

class InactiveMemberSteps: StepDefiner {
    
    override func defineSteps() {
        
        step("I have entered a correct inactive member email") {
            sleep(1)
            let app = XCUIApplication()
            app.scrollViews.otherElements.tables.cells.textFields[CommonStrings.EmailFieldPlaceholder18].tap()
            app.scrollViews.otherElements.tables.cells.textFields[CommonStrings.EmailFieldPlaceholder18].typeText("ntest--Elayne_McBinkley@mailinator.com")
        }
        
        step("I have entered new inactive member email") {
            //  Create new test data
            let app = XCUIApplication()
            app.otherElements.cells.textFields[CommonStrings.Registration.EmailFieldPlaceholder27].tap()
            app.otherElements.cells.textFields[CommonStrings.Registration.EmailFieldPlaceholder27].typeText("ntest--Elayne_McBinkley@mailinator.com")
        }
        
        step("I have entered new inactive member date of birth") {
            //  Create new test data
            let app = XCUIApplication()
            app.otherElements.cells.textFields[CommonStrings.Common.DateFormat.Yyyymmdd2197].tap()
            app.otherElements.cells.textFields[CommonStrings.Common.DateFormat.Yyyymmdd2197].typeText("19880120")
        }
        
        step("I have entered new inactive member insurer code") {
            //  Create new test data
            let app = XCUIApplication()
            app.otherElements.cells.textFields[CommonStrings.Registration.RegistrationCodeFieldPlaceholder33].tap()
            app.otherElements.cells.textFields[CommonStrings.Registration.RegistrationCodeFieldPlaceholder33].typeText("2507590568")
        }
        
        step("I should see the inactive membership screen") {
            let app = XCUIApplication()
            let inactiveMemberScreen = app.staticTexts[CommonStrings.Login.LoginRestrictionGeneralErrorMessageTitle2263]
            self.test.waitFor(seconds: 10, element: inactiveMemberScreen)
            XCTAssert(inactiveMemberScreen.exists, "Inactive Member Screen not shown")
        }
        
        step("I should see a logout confirmation prompt") {
            let app = XCUIApplication()
            let logoutPrompt = app.alerts[CommonStrings.Settings.LogoutTitle908]
            XCTAssert(logoutPrompt.exists, "Logout prompt not shown")
        }
        
        step("I press logout") {
            let app = XCUIApplication()
            let logoutButton = app.buttons[CommonStrings.Settings.LogoutTitle908]
            logoutButton.tap()
        }
        
        step("I press cancel in logout prompt") {
            let app = XCUIApplication()
            let logoutPrompt = app.alerts[CommonStrings.Settings.LogoutTitle908]
            logoutPrompt.buttons["cancel_button_title_24"].tap()
        }
        
        step("I confirm logout") {
            let app = XCUIApplication()
            app.alerts[CommonStrings.Settings.LogoutTitle908].buttons[CommonStrings.Settings.LogoutTitle908].tap()
        }
        
        step("I should be back in login screen") {
            let app = XCUIApplication()
            let loginScreenIndicator = app.scrollViews.otherElements.tables.cells.textFields[CommonStrings.EmailFieldPlaceholder18]
            self.test.waitFor(seconds: 5, element: loginScreenIndicator)
            XCTAssert(loginScreenIndicator.exists, "Login Screen not shown")
        }
        
    }
}
