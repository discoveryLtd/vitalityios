//
//  MembershipPassViewHelpTests.swift
//  VitalityActive
//
//  Created by Erik John T. Alicaya (ADMIN) on 19/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import XCTest
import VitalityKit
@testable import XCTest_Gherkin
@testable import VIACore

class MembershipPassViewHelpTests: XCTestCase {
        
    func scenario(_ scenario: String) {
        NativeRunner(featureFile: "membershipviewhelp.feature", testCase: self).run(scenario: scenario)
    }
    
    func testViewHelp() {
        scenario("Tap help button")
    }
    
    func testNavigateBackFromViewHelp() {
        scenario("Navigate back from view help page")
    }

    //TODO: Implement UI Test for tapping the suggested questions after these strings are localized
    // Example: func testOneOfTheSuggestedQuestions() {
    //              scenario("Tap What is my Digital Pass?")
    //          }
    
}
