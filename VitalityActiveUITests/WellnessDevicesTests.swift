//
//  WellnessDevicesTests.swift
//  VitalityActive
//
//  Created by Sebastian Cichon on 2017/08/02.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import XCTest
import VitalityKit
@testable import XCTest_Gherkin
@testable import VIACore

class WellnessDevicesTests: XCTestCase {
    
    func scenario(_ scenario: String) {
        NativeRunner(featureFile: "wellnessdevices.feature", testCase: self).run(scenario: scenario)
    }
    
//    func testWellnessDeviceOnboarding() {
//        scenario("View Wellness Devices onboarding screen")
//    }
//    
//    func testWDAGotItButton() {
//        scenario("Tap Got It button on Wellness Devices onboarding screen")
//    }
//    
//    func testViewINeedDeviceOrAppLink() {
//        scenario("View I need device or app link on onboarding")
//    }
//    
//    func testViewLearnMoreButtonForWDA() {
//        scenario("View Wellness Devices learn more button")
//    }
//    
//    func testLearnMoreScreenView() {
//        scenario("View Learn More for Wellness Devices")
//    }
//    
//    func testNavigateBackToLandingFromLearnMore() {
//        scenario("Navigate back to Wellness landing from Learn More")
//    }
//    
//    func testAvailableToLinkDevices() {
//        scenario("View devices that are available to link")
//    }
//    
//    func testAtLeastOneDeviceAvailableToLink() {
//        scenario("Always see at least one device to link")
//    }
//    
//    func testViewHelpScreenForWDA() {
//        scenario("Tap on Help button on Wellness Devices landing screen")
//    }
//    
//    func testViewHealthAppDetailScreen() {
//        scenario("View Health App device details screen")
//    }
//    
//    func testViewStepsToLinkButton() {
//        scenario("View Steps to Link button on device details screen")
//    }
//    
//    func testViewPointsEarningMetricsOnDetails() {
//        scenario("View Points Earning Metrics on device details screen")
//    }
//    
//    func testTapOnStepsToLinkButton() {
//        scenario("Tap on Steps to Link button")
//    }
//    
//    func testLearnMorePointsEarningPotential() {
//        scenario("View Learn More under the points earning metrics on device details screen")
//    }
//    
//    func testViewAboutSectionForDeviceOrApp() {
//        scenario("View About section on the device or manufacturer")
//    }
//    
//    func testPhysicalActivityListGrouped() {
//        scenario("I will see Physical Activities available and grouped")
//    }
//    
//    func testViewDataSharingConsent() {
//        scenario("View Data Sharing Consent on Wellness Device")
//    }
//    
//    func testDisagreeToDataSharingConsent() {
//        scenario("Disagree the Data Sharing Consent on Wellness Device")
//    }
//    
//    func testAgreeToDataSharingConsent() {
//        scenario("Agree to the Data Sharing Consent on Wellness Device")
//    }
//    
//    func testManufacturerWebsiteOpens() {
//        scenario("Manufacturer website for Adidas opens")
//    }
//    
//    func testAllowAccessForHealthAppToLink() {
//        scenario("Allow access for Health App with Wellness Devices")
//    }
//    
//    func testDontAllowAccessForHealthApp() {
//        scenario("Do NOT allow access for Health App with Wellness Devices")
//    }
//    
//    func testInformationScreenForHealthApp() {
//        scenario("Information screen for Health App with Wellness Devices")
//    }
//    
//    func testLinkedDevicesHeadingOnLanding() {
//        scenario("View Wellness Devices landing screen with linked heading")
//    }
//    
//    func testMoreAvailableToLinkHeading() {
//        scenario("View more available to link devices")
//    }
//    
//    func testLinkAdditionalDeviceJourney() {
//        scenario("Link an additional device")
//    }
//    
//    func testLinkedFooterTextDisplayingTimeToSync() {
//        scenario("View data time estimate to sync message")
//    }
}
