//
//  InsuranceRewardSteps.swift
//  VitalityActiveUITests
//
//  Created by Erik John T. Alicaya (ADMIN) on 03/12/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import UIKit
@testable import VIACore
@testable import VitalityKit
@testable import XCTest_Gherkin

class InsuranceRewardSteps: StepDefiner {
        
    override func defineSteps() {
        
        step("I tap on Insurance Reward card") {
            let app = XCUIApplication()
            let collectionViewsQuery = app.collectionViews
            
            let VHRCard = collectionViewsQuery.collectionViews.staticTexts["home_card.card_section_title_291"]
            if VHRCard.waitForExistence(timeout: 2) {
                VHRCard.swipeUp()
            }
            
            let availableRewardsCard = collectionViewsQuery.collectionViews.staticTexts["card_title_available_rewards_999"]
            if availableRewardsCard.waitForExistence(timeout: 2) {
                availableRewardsCard.swipeLeft()
            }
            
            let rewardPartnersCard = collectionViewsQuery.collectionViews.staticTexts["card_heading_reward_partners_845"]
            if rewardPartnersCard.waitForExistence(timeout: 2) {
                rewardPartnersCard.swipeLeft()
            }
            
            let IRCard = collectionViewsQuery.collectionViews.staticTexts["home_card.insurance.section_title_1963"]
            IRCard.tap()
        }
        
        step("I should see Insurance Reward screen") {
            let app = XCUIApplication()
            let landingScreen = app.navigationBars["home_card.insurance.section_title_1963"]
            
            if landingScreen.waitForExistence(timeout: 2){
                XCTestCase.VitalityHelper.waitForElementToAppear(test: self.test, element: landingScreen)
                let failureReason = "Insurance Reward screen not displayed"
                XCTAssertTrue(landingScreen.exists, failureReason)
            }
        }
    }
}
