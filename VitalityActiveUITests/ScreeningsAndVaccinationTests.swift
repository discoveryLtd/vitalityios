//
//  ScreeningsAndVaccinationTests.swift
//  VitalityActive
//
//  Created by Val Tomol on 25/11/2017.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import XCTest
import VitalityKit
@testable import XCTest_Gherkin
@testable import VIACore

class ScreeningsAndVaccinationTests: XCTestCase {
    
    func scenario(_ scenario: String) {
        NativeRunner(featureFile: "screeningsandvaccination.feature", testCase: self).run(scenario: scenario)
    }
    
//    func testSVCard() {
//        scenario("I can tap Screenings and Vaccination card")
//    }
    
//    func testNavigateBackFromSV() {
//        scenario("I can navigate back from Screenings and Vaccination")
//    }
}
