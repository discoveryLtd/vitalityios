//
//  SAVOnBoardingSteps.swift
//  VitalityActive
//
//  Created by Michelle Oratil on 21/11/2017.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import XCTest
import VIACore
import VitalityKit
@testable import XCTest_Gherkin

class SAVOnBoardingSteps: StepDefiner {
    
    override func defineSteps() {

        step("I have entered SAV email") {
            let app = XCUIApplication()

            let field = app.scrollViews.otherElements.tables.cells.textFields[CommonStrings.Registration.EmailFieldPlaceholder27]
            UIPasteboard.general.string = "test--Danika_Brinkman@SITtest.com"
            field.doubleTap()
            app.menuItems.element(boundBy: 0).tap()
        }
        
        step("I tap on SAV card"){
            sleep(15)
            let app = XCUIApplication()
            let SAVcard = app.otherElements.staticTexts[CommonStrings.HomeCard.CardSectionTitle365]
            sleep(5)
            app.staticTexts[CommonStrings.HomeCard.CardTitle96].swipeLeft()
            sleep(1)
            app.staticTexts[CommonStrings.HomeCard.CardTitle125].swipeLeft()
            sleep(1)

            SAVcard.tap()
            sleep(5)
            
        }
        
        step("I see SAV onboarding modal"){
            let app = XCUIApplication()
            let getStartedButton = app.buttons[CommonStrings.GetStartedButtonTitle103]
            let learnMoreButton = app.buttons[CommonStrings.LearnMoreButtonTitle104]
            
            XCTAssert(app.staticTexts[CommonStrings.HomeCard.CardSectionTitle365].exists)
            XCTAssert(getStartedButton.exists)
            XCTAssert(learnMoreButton.exists)
        }
        
        
    }
    
}
