//
//  SettingsTests.swift
//  VitalityActive
//
//  Created by Val Tomol on 22/11/2017.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import XCTest
import VitalityKit
@testable import XCTest_Gherkin
@testable import VIACore

class CommunicationPreferencesTests: XCTestCase {
    func scenario(_ scenario: String) {
        NativeRunner(featureFile: "communicationpreferences.feature", testCase: self).run(scenario: scenario)
    }
    //MARK: Communication Preferences
    func testCommunicationPreferencesRow() {
        scenario("Communication Preferences row is present")
    }
    
    func testToggleEmailCommunication() {
        scenario("Toggle email communication")
    }
    
    func testTogglePushNotifications() {
        scenario("Toggle push notifications")
    }
//    func testManageSettings() {
//        scenario("Manage in the Settings app")
//    }
    
//    func testReturnBackToTheApp() {
//        scenario("Return back to the app")
//    }
}
