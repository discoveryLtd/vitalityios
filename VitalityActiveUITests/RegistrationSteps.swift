//
//  RegistrationSteps.swift
//  VitalityActive
//
//  Created by Wilmar van Heerden on 2016/12/02.
//  Copyright © 2016 Glucode. All rights reserved.
//

import Foundation
import XCTest
@testable import VitalityKit
@testable import XCTest_Gherkin
@testable import VIACore

class RegistrationSteps: StepDefiner {
    
    override func defineSteps() {
        
        step("I tap Register button") {
            let app = XCUIApplication()
            app.scrollViews.otherElements.tables.buttons[CommonStrings.Login.RegisterButtonTitle23].tap()
        }
        
        step("Registration form is shown") {
            let app = XCUIApplication()
            XCTAssert(app.otherElements.cells.textFields[CommonStrings.Registration.EmailFieldPlaceholder27].exists)
        }
        
        step("I have entered valid email") {
            let app = XCUIApplication()
            app.otherElements.cells.textFields[CommonStrings.Registration.EmailFieldPlaceholder27].tap()
            app.otherElements.cells.textFields[CommonStrings.Registration.EmailFieldPlaceholder27].typeText("test--valid@email.com")
        }
        
        step("I have entered a new email") {
            let app = XCUIApplication()
            app.otherElements.cells.textFields[CommonStrings.Registration.EmailFieldPlaceholder27].tap()
            app.otherElements.cells.textFields[CommonStrings.Registration.EmailFieldPlaceholder27].typeText("test--Carlo_Roth@SITtest.com")
        }
        
        step("I have entered incorrect email") {
            let app = XCUIApplication()
            app.otherElements.cells.textFields[CommonStrings.Registration.EmailFieldPlaceholder27].tap()
            app.otherElements.cells.textFields[CommonStrings.Registration.EmailFieldPlaceholder27].typeText("test--incorrect@email.com")
        }
        
        step("I have entered date of birth") {
            let app = XCUIApplication()
            app.otherElements.cells.textFields[CommonStrings.Common.DateFormat.Yyyymmdd2197].tap()
            app.otherElements.cells.textFields[CommonStrings.Common.DateFormat.Yyyymmdd2197].typeText("19951015")
        }
        
        step("I have entered correct insurer code") {
            let app = XCUIApplication()
            app.otherElements.cells.textFields[CommonStrings.Registration.RegistrationCodeFieldPlaceholder33].tap()
            app.otherElements.cells.textFields[CommonStrings.Registration.RegistrationCodeFieldPlaceholder33].typeText("2004260255")
        }
        
        step("I have entered new insurer code") {
            //create test data in fitnesse
            let app = XCUIApplication()
            app.otherElements.cells.textFields[CommonStrings.Registration.RegistrationCodeFieldPlaceholder33].tap()
            app.otherElements.cells.textFields[CommonStrings.Registration.RegistrationCodeFieldPlaceholder33].typeText("2004891938")
        }
        
        step("I have entered correct password") {
            let app = XCUIApplication()
            app.tables.secureTextFields[CommonStrings.Registration.PasswordFieldPlaceholder28].tap()
            app.tables.secureTextFields[CommonStrings.Registration.PasswordFieldPlaceholder28].typeText("TestPass123")
        }
        
        step("I have entered new password") {
            let app = XCUIApplication()
            app.tables.secureTextFields[CommonStrings.Registration.PasswordFieldPlaceholder28].tap()
            app.tables.secureTextFields[CommonStrings.Registration.PasswordFieldPlaceholder28].typeText("TestPass123")
        }
        
        step("I have entered correct confirm password") {
            let app = XCUIApplication()
            app.tables.secureTextFields[CommonStrings.Registration.ConfirmPasswordFieldPlaceholder31].tap()
            app.tables.secureTextFields[CommonStrings.Registration.ConfirmPasswordFieldPlaceholder31].typeText("TestPass123")
        }
        
        step("I have entered new confirm password") {
            let app = XCUIApplication()
            app.tables.secureTextFields[CommonStrings.Registration.ConfirmPasswordFieldPlaceholder31].tap()
            app.tables.secureTextFields[CommonStrings.Registration.ConfirmPasswordFieldPlaceholder31].typeText("TestPass123")
        }
        
        step("I have entered new date of birth") {
            //create test data in fitnesse
            let app = XCUIApplication()
            app.otherElements.cells.textFields[CommonStrings.Common.DateFormat.Yyyymmdd2197].tap()
            app.otherElements.cells.textFields[CommonStrings.Common.DateFormat.Yyyymmdd2197].typeText("19951015")
        }
        
        step("I tap on Register") {
            let app = XCUIApplication()
            app.navigationBars.buttons[CommonStrings.Login.RegisterButtonTitle23].tap()
        }
        
        step("I should get an incorrect code or email error message") {
            let IncorrectEmailCodeCombination = XCUIApplication().alerts[CommonStrings.Registration.InvalidEmailOrRegistrationCodeAlertTitle38]
            let failureReason = "Incorrect Email or Password Message not shown"
            XCTestCase.VitalityHelper.waitForElementToAppear(test: self.test, element: IncorrectEmailCodeCombination)
            XCTAssertTrue(IncorrectEmailCodeCombination.staticTexts[CommonStrings.Registration.InvalidEmailOrRegistrationCodeAlertTitle38].exists, failureReason)
        }
        
        step("I have entered correct email") {
            let app = XCUIApplication()
            app.tables.textFields[CommonStrings.Registration.EmailFieldPlaceholder27].tap()
            app.tables.textFields[CommonStrings.Registration.EmailFieldPlaceholder27].typeText("test--correct@email.com")
        }
        
        step("I have entered incorrect insurer code") {
            let app = XCUIApplication()
            app.otherElements.cells.textFields[CommonStrings.Registration.RegistrationCodeFieldPlaceholder33].tap()
            app.otherElements.cells.textFields[CommonStrings.Registration.RegistrationCodeFieldPlaceholder33].typeText("12312345734")
        }
        
        step("I have entered existing email") {
            let app = XCUIApplication()
            app.tables.textFields[CommonStrings.Registration.EmailFieldPlaceholder27].tap()
            app.tables.textFields[CommonStrings.Registration.EmailFieldPlaceholder27].typeText("test--santi@test.com")
        }
        
        step("I have entered existing insurer code") {
            let app = XCUIApplication()
            app.otherElements.cells.textFields[CommonStrings.Registration.RegistrationCodeFieldPlaceholder33].tap()
            app.otherElements.cells.textFields[CommonStrings.Registration.RegistrationCodeFieldPlaceholder33].typeText("2003917797")
        }
        
        step("I should get an unable to register message") {
            let ExistingUserError = XCUIApplication().alerts[CommonStrings.Registration.UnableToRegisterAlertTitle41]
            let failureReason = "Existing User Error message not displaying"
            XCTestCase.VitalityHelper.waitForElementToAppear(test: self.test, element: ExistingUserError)
            XCTAssertTrue(ExistingUserError.staticTexts[CommonStrings.Registration.UnableToRegisterAlertTitle41].exists, failureReason)
        }
        
        step("Information about how to receive Insurer code is displayed") {
            let app = XCUIApplication()
            XCTAssert(app.staticTexts[CommonStrings.Registration.RegistrationCodeFieldFootnote34].exists)
        }
        
        step("I have entered incorrect confirm password") {
            let app = XCUIApplication()
            app.tables.secureTextFields[CommonStrings.Registration.ConfirmPasswordFieldPlaceholder31].tap()
            app.tables.secureTextFields[CommonStrings.Registration.ConfirmPasswordFieldPlaceholder31].typeText("Password1")
        }
        
        step("I should get password mismatch error") {
            let app = XCUIApplication()
            app.tables.textFields[CommonStrings.Registration.RegistrationCodeFieldPlaceholder33].tap()
            sleep(5)
            XCTAssert(app.staticTexts[CommonStrings.Registration.MismatchedPasswordFootnoteError36].exists, "Field not found on automation - These tests will be tested manually.")
        }
        
        step("I have not entered email address") {
            let app = XCUIApplication()
            app.otherElements.cells.textFields[CommonStrings.Registration.EmailFieldPlaceholder27].tap()
            app.otherElements.cells.textFields[CommonStrings.Registration.EmailFieldPlaceholder27].typeText("")
        }
        
        step("I have not entered password") {
            let app = XCUIApplication()
            app.tables.secureTextFields[CommonStrings.Registration.PasswordFieldPlaceholder28].tap()
            app.tables.secureTextFields[CommonStrings.Registration.PasswordFieldPlaceholder28].typeText("")
        }
        
        step("I have not entered confirm password") {
            let app = XCUIApplication()
            app.tables.secureTextFields[CommonStrings.Registration.ConfirmPasswordFieldPlaceholder31].tap()
            app.tables.secureTextFields[CommonStrings.Registration.ConfirmPasswordFieldPlaceholder31].typeText("")
        }
        
        step("I have not entered insurer code") {
            let app = XCUIApplication()
            app.otherElements.cells.textFields[CommonStrings.Registration.RegistrationCodeFieldPlaceholder33].tap()
            app.otherElements.cells.textFields[CommonStrings.Registration.RegistrationCodeFieldPlaceholder33].typeText("")
        }
        
        step("the register link should be disabled") {
            let app = XCUIApplication()
            let button = app.navigationBars.buttons[CommonStrings.Login.RegisterButtonTitle23]
            XCTAssert(button.exists, "Register and Cancel button is present but unable to identify.")
        }
        
        step("I select Register") {
            let app = XCUIApplication()
            app.scrollViews.otherElements.tables.buttons[CommonStrings.Login.RegisterButtonTitle23].tap()
        }
        
        //        step("I tap on Register on login screen") {
        //            let app = XCUIApplication()
        //            app.buttons[LoginStrings.Registration.RegistrationCodeFieldTitle32].tap()
        //        }
        
        step("I should see Registration screen") {
            let app = XCUIApplication()
            XCTAssert(app.navigationBars[CommonStrings.Registration.ScreenTitle25].staticTexts[CommonStrings.Registration.ScreenTitle25].exists, "Registration screen is not being displayed.")
        }
        
        step("Information about how to receive Insurer code is displayed") {
            let app = XCUIApplication()
            XCTAssert(app.tables.staticTexts[CommonStrings.Registration.ScreenTitle25].exists, "Insurer Code text is not being displayed.")
        }
        
        step("I am on the Registration screen") {
            self.step("I am on the Login screen")
            self.step("I select Register")
            self.step("I should see Registration screen")
        }
        
        step("I have entered all the correct details") {
            self.step("I have entered correct email")
            self.step("I have entered correct password")
            self.step("I have entered correct confirm password")
            self.step("I have entered correct insurer code")
        }
        
        step("I agree to the terms") {
            let app = XCUIApplication()
            let agreeButton = app.toolbars.buttons[CommonStrings.AgreeButtonTitle50]
            self.test.waitFor10Secs(element: agreeButton)
            agreeButton.tap()
        }
        
        step("I have entered correct password") {
            let app = XCUIApplication()
            app.tables.secureTextFields[CommonStrings.Registration.PasswordFieldPlaceholder28].tap()
            app.tables.secureTextFields[CommonStrings.Registration.PasswordFieldPlaceholder28].typeText("CorrectPassword")
        }
        
        step("I have entered correct confirm password") {
            let app = XCUIApplication()
            app.tables.secureTextFields[CommonStrings.Registration.ConfirmPasswordFieldPlaceholder31].tap()
            app.tables.secureTextFields[CommonStrings.Registration.ConfirmPasswordFieldPlaceholder31].typeText("CorrectPassword")
        }
        
        step("I have entered incorrect password") {
            let app = XCUIApplication()
            app.tables.secureTextFields[CommonStrings.Registration.PasswordFieldPlaceholder28].tap()
            app.tables.secureTextFields[CommonStrings.Registration.PasswordFieldPlaceholder28].typeText("IncorrectPassword")
        }
        
        step("I have entered incorrect confirm password") {
            let app = XCUIApplication()
            app.tables.secureTextFields[CommonStrings.Registration.ConfirmPasswordFieldPlaceholder31].tap()
            app.tables.secureTextFields[CommonStrings.Registration.ConfirmPasswordFieldPlaceholder31].typeText("IncorrectPassword")
        }
        
        step("I should get password mismatch error") {
            let app = XCUIApplication()
            app.tables.textFields[CommonStrings.Registration.RegistrationCodeFieldPlaceholder33].tap()
            let errorMessage = app.tables.secureTextFields[CommonStrings.Registration.RegistrationCodeFieldPlaceholder33].staticTexts[CommonStrings.Registration.MismatchedPasswordFootnoteError36]
            self.test.waitFor10Secs(element: errorMessage)
            XCTAssert(errorMessage.exists, "Password mismatch error is not being displayed.")
        }
        
        // REGEX USERNAMES
        
        step("I have entered numeric and lowercase email") {
            let app = XCUIApplication()
            app.tables.textFields[CommonStrings.Registration.EmailFieldPlaceholder27].tap()
            app.tables.textFields[CommonStrings.Registration.EmailFieldPlaceholder27].typeText("test--james01@smith.com")
        }
        
        step("I have entered an email with an upper case") {
            let app = XCUIApplication()
            app.tables.textFields[CommonStrings.Registration.EmailFieldPlaceholder27].tap()
            app.tables.textFields[CommonStrings.Registration.EmailFieldPlaceholder27].typeText("test--John@smith.com")
        }
        
        step("I have entered an email with only upper case") {
            let app = XCUIApplication()
            app.tables.textFields[CommonStrings.Registration.EmailFieldPlaceholder27].tap()
            app.tables.textFields[CommonStrings.Registration.EmailFieldPlaceholder27].typeText("test--JSCAPS@smith.com")
        }
        
        step("I have entered an email with only numerical characters") {
            let app = XCUIApplication()
            app.tables.textFields[CommonStrings.Registration.EmailFieldPlaceholder27].tap()
            app.tables.textFields[CommonStrings.Registration.EmailFieldPlaceholder27].typeText("test--1234567890@test.com")
        }
        
        step("I have entered an email with upper case and numerical characters") {
            let app = XCUIApplication()
            app.tables.textFields[CommonStrings.Registration.EmailFieldPlaceholder27].tap()
            app.tables.textFields[CommonStrings.Registration.EmailFieldPlaceholder27].typeText("test--JOHN01@smith.com")
        }
        
        step("I have entered an email with upper & case and numerical characters") {
            let app = XCUIApplication()
            app.tables.textFields[CommonStrings.Registration.EmailFieldPlaceholder27].tap()
            app.tables.textFields[CommonStrings.Registration.EmailFieldPlaceholder27].typeText("test--John01@smith.com")
        }
        
        step("I have entered Plus character in email") {
            let app = XCUIApplication()
            app.tables.textFields[CommonStrings.Registration.EmailFieldPlaceholder27].tap()
            app.tables.textFields[CommonStrings.Registration.EmailFieldPlaceholder27].typeText("test--john+01@smith.com")
        }
        
        step("I have entered Hash character in email") {
            let app = XCUIApplication()
            app.tables.textFields[CommonStrings.Registration.EmailFieldPlaceholder27].tap()
            app.tables.textFields[CommonStrings.Registration.EmailFieldPlaceholder27].typeText("test--john#01@smith.com")
        }
        
        step("I have entered Underscore in email") {
            let app = XCUIApplication()
            app.tables.textFields[CommonStrings.Registration.EmailFieldPlaceholder27].tap()
            app.tables.textFields[CommonStrings.Registration.EmailFieldPlaceholder27].typeText("test--john_01@smith.com")
        }
        
        step("I have entered Dollar sign in email") {
            let app = XCUIApplication()
            app.tables.textFields[CommonStrings.Registration.EmailFieldPlaceholder27].tap()
            app.tables.textFields[CommonStrings.Registration.EmailFieldPlaceholder27].typeText("test--john$01@smith.com")
        }
        
        step("I have entered And sign in email") {
            let app = XCUIApplication()
            app.tables.textFields[CommonStrings.Registration.EmailFieldPlaceholder27].tap()
            app.tables.textFields[CommonStrings.Registration.EmailFieldPlaceholder27].typeText("test--john&01@smith.com")
        }
        
        step("I have entered Percentage sign in email") {
            let app = XCUIApplication()
            app.tables.textFields[CommonStrings.Registration.EmailFieldPlaceholder27].tap()
            app.tables.textFields[CommonStrings.Registration.EmailFieldPlaceholder27].typeText("test--john%01@smith.com")
        }
        
        step("I have entered Semicolon character in email") {
            let app = XCUIApplication()
            app.tables.textFields[CommonStrings.Registration.EmailFieldPlaceholder27].tap()
            app.tables.textFields[CommonStrings.Registration.EmailFieldPlaceholder27].typeText("test--john;01@smith.com")
        }
        
        step("I have entered Star character in the email") {
            let app = XCUIApplication()
            app.tables.textFields[CommonStrings.Registration.EmailFieldPlaceholder27].tap()
            app.tables.textFields[CommonStrings.Registration.EmailFieldPlaceholder27].typeText("test--john*01@smith.com")
        }
        
        step("I have entered Hyphen in the email") {
            let app = XCUIApplication()
            app.tables.textFields[CommonStrings.Registration.EmailFieldPlaceholder27].tap()
            app.tables.textFields[CommonStrings.Registration.EmailFieldPlaceholder27].typeText("test--john-01@smith.com")
        }
        
            step("I have entered Forward slash in email") {
                let app = XCUIApplication()
                app.tables.textFields[CommonStrings.Registration.EmailFieldPlaceholder27].tap()
                app.tables.textFields[CommonStrings.Registration.EmailFieldPlaceholder27].typeText("test--john/01@smith.com")
            }
        
        step("I have entered Equal sign in the email") {
            let app = XCUIApplication()
            app.tables.textFields[CommonStrings.Registration.EmailFieldPlaceholder27].tap()
            app.tables.textFields[CommonStrings.Registration.EmailFieldPlaceholder27].typeText("test--john=01@smith.com")
        }
        
        step("I have entered Question mark in the email") {
            let app = XCUIApplication()
            app.tables.textFields[CommonStrings.Registration.EmailFieldPlaceholder27].tap()
            app.tables.textFields[CommonStrings.Registration.EmailFieldPlaceholder27].typeText("test--john?01@smith.com")
        }
        
        step("I have entered Caret character in email") {
            let app = XCUIApplication()
            app.tables.textFields[CommonStrings.Registration.EmailFieldPlaceholder27].tap()
            app.tables.textFields[CommonStrings.Registration.EmailFieldPlaceholder27].typeText("test--john^01@smith.com")
        }
        
        step("I have entered Grave accent in the email") {
            let app = XCUIApplication()
            app.tables.textFields[CommonStrings.Registration.EmailFieldPlaceholder27].tap()
            app.tables.textFields[CommonStrings.Registration.EmailFieldPlaceholder27].typeText("test--john`01@smith.com")
        }
        
        step("I have entered Curly brackets in the email") {
            let app = XCUIApplication()
            app.tables.textFields[CommonStrings.Registration.EmailFieldPlaceholder27].tap()
            app.tables.textFields[CommonStrings.Registration.EmailFieldPlaceholder27].typeText("test--john{01}@smith.com")
        }
        
        step("I have entered Line bar in the email") {
            let app = XCUIApplication()
            app.tables.textFields[CommonStrings.Registration.EmailFieldPlaceholder27].tap()
            app.tables.textFields[CommonStrings.Registration.EmailFieldPlaceholder27].typeText("test--john|01@smith.com")
        }
        
        step("I have entered Tilde in the email") {
            let app = XCUIApplication()
            app.tables.textFields[CommonStrings.Registration.EmailFieldPlaceholder27].tap()
            app.tables.textFields[CommonStrings.Registration.EmailFieldPlaceholder27].typeText("test--john~01@smith.com")
        }
        
        step("I have entered an email with two local characters") {
            let app = XCUIApplication()
            app.tables.textFields[CommonStrings.Registration.EmailFieldPlaceholder27].tap()
            app.tables.textFields[CommonStrings.Registration.EmailFieldPlaceholder27].typeText("test--js@smith.com")
        }
        
        step("I have entered an email with 64 local characters") {
            let app = XCUIApplication()
            app.tables.textFields[CommonStrings.Registration.EmailFieldPlaceholder27].tap()
            app.tables.textFields[CommonStrings.Registration.EmailFieldPlaceholder27].typeText("test--ed4DZ3rS3Zul0KfnJFsLh9uyg5BhaoLxxEUzXuXNejL4pGnyvHbYqvKngh4mQSnT@test.com")
        }
        
        step("I have entered an email with lower case domain") {
            let app = XCUIApplication()
            app.tables.textFields[CommonStrings.Registration.EmailFieldPlaceholder27].tap()
            app.tables.textFields[CommonStrings.Registration.EmailFieldPlaceholder27].typeText("test--john@smith.com")
        }
        
        step("I have entered an email with upper case domain") {
            let app = XCUIApplication()
            app.tables.textFields[CommonStrings.Registration.EmailFieldPlaceholder27].tap()
            app.tables.textFields[CommonStrings.Registration.EmailFieldPlaceholder27].typeText("test--john@SMITH.com")
        }
        
        step("I have entered an email with numerical domain") {
            let app = XCUIApplication()
            app.tables.textFields[CommonStrings.Registration.EmailFieldPlaceholder27].tap()
            app.tables.textFields[CommonStrings.Registration.EmailFieldPlaceholder27].typeText("test--john@12345.net")
        }
        
        step("I have entered an email with uppercase, lowercase and numerical domain") {
            let app = XCUIApplication()
            app.tables.textFields[CommonStrings.Registration.EmailFieldPlaceholder27].tap()
            app.tables.textFields[CommonStrings.Registration.EmailFieldPlaceholder27].typeText("test--john@Smith21.org")
        }
        
        step("I have entered an email with 2 segments in domain") {
            let app = XCUIApplication()
            app.tables.textFields[CommonStrings.Registration.EmailFieldPlaceholder27].tap()
            app.tables.textFields[CommonStrings.Registration.EmailFieldPlaceholder27].typeText("test--user@mail.box.com")
        }
        
        step("I have entered an email with 3 segments in domain") {
            let app = XCUIApplication()
            app.tables.textFields[CommonStrings.Registration.EmailFieldPlaceholder27].tap()
            app.tables.textFields[CommonStrings.Registration.EmailFieldPlaceholder27].typeText("test--user@domain.mail.box.com")
        }
        
        step("I have entered an email with 4 segments in domain") {
            let app = XCUIApplication()
            app.tables.textFields[CommonStrings.Registration.EmailFieldPlaceholder27].tap()
            app.tables.textFields[CommonStrings.Registration.EmailFieldPlaceholder27].typeText("test--user@sub.domain.mail.box.com")
        }
        
        step("I have entered an email with 5 segments in domain") {
            let app = XCUIApplication()
            app.tables.textFields[CommonStrings.Registration.EmailFieldPlaceholder27].tap()
            app.tables.textFields[CommonStrings.Registration.EmailFieldPlaceholder27].typeText("test--user@buried.sub.domain.mail.com")
        }
        
        step("I have entered an email with 2 local and 247 domain split into 4 segments") {
            let app = XCUIApplication()
            app.tables.textFields[CommonStrings.Registration.EmailFieldPlaceholder27].tap()
            app.tables.textFields[CommonStrings.Registration.EmailFieldPlaceholder27].typeText("test--ax@aQEnDJDSoIowLwSoDEjCNcmhJJvlgLSZHcmTPLAzJDuXPudvUjsjoZGiryRaKvh.tdyAnxLzIfbvQYxrfRsOwCyLVyoIyTaoarTZtmQQalYHoNcvIUphMETQunehtjZ.YIifpMmxhCItrVfvOUPokpHKMgKwimUsLpMhiajDEkNiHcbXVdwEzWfRUWNEfGS.hZKHoDQxUljYNcBEfEakGReRZeSPkCgIrEFozGmXOehfkuDbVQsMcnz.com")
        }
        
        step("I have entered an email with hyphen in domain") {
            let app = XCUIApplication()
            app.tables.textFields[CommonStrings.Registration.EmailFieldPlaceholder27].tap()
            app.tables.textFields[CommonStrings.Registration.EmailFieldPlaceholder27].typeText("test--user@mail-box.com")
        }
        
        step("I have entered an email with capital TLD") {
            let app = XCUIApplication()
            app.tables.textFields[CommonStrings.Registration.EmailFieldPlaceholder27].tap()
            app.tables.textFields[CommonStrings.Registration.EmailFieldPlaceholder27].typeText("test--abc@testing.COM")
        }
        
        step("I have entered an email with 2 TLD characters") {
            let app = XCUIApplication()
            app.tables.textFields[CommonStrings.Registration.EmailFieldPlaceholder27].tap()
            app.tables.textFields[CommonStrings.Registration.EmailFieldPlaceholder27].typeText("test--abc@testing.za")
        }
        
        step("I have entered an email with 24 TLD characters") {
            let app = XCUIApplication()
            app.tables.textFields[CommonStrings.Registration.EmailFieldPlaceholder27].tap()
            app.tables.textFields[CommonStrings.Registration.EmailFieldPlaceholder27].typeText("test--user@test.XN--VERMGENSBERATUNG-PWB")
        }
    }
}
