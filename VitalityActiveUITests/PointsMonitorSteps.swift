//
//  PointsMonitorSteps.swift
//  VitalityActiveUITests
//
//  Created by ted.philip.l.lat on 19/11/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import UIKit
@testable import VIACore
@testable import VitalityKit
@testable import XCTest_Gherkin

class PointsMonitorSteps: StepDefiner {
    
    override func defineSteps() {
        
        step("I tap Points on Menu") {
            let app = XCUIApplication()
            app.tabBars.buttons["menu_points_button_6"].tap()
        }
        
        step("I should see Points Monitor screen") {
            let app = XCUIApplication()
            let PMScreenIndicator = app.navigationBars.element(boundBy: 0)
            XCTAssert(PMScreenIndicator.waitForExistence(timeout: 10), "Points Monitor not shown")
        }
        
        step("I tap on other Months") {
            let app = XCUIApplication()
            let collectionViewsQuery = app.collectionViews
            let otherMonths = collectionViewsQuery.cells.element(boundBy: 4)
            otherMonths.forceTapElement()
        }
        
        step("I tap dropdown button") {
            let app = XCUIApplication()
            let PMScreenIndicator = app.navigationBars.element(boundBy: 0)
            PMScreenIndicator.forceTapElement()
        }
        
        step("I choose Assessments") {
            let app = XCUIApplication()
            let tablesQuery = app.tables
            tablesQuery.cells.staticTexts["PM.category_filter_assessment_title_516"].tap()
        }
        
        step("I choose Screenings") {
            let app = XCUIApplication()
            let tablesQuery = app.tables
            tablesQuery.cells.staticTexts["PM.category_filter_screening_title_518"].tap()
        }
        
        step("I choose Fitness") {
            let app = XCUIApplication()
            let tablesQuery = app.tables
            tablesQuery.cells.staticTexts["PM.category_filter_fitness_title_519"].tap()
        }
        
        step("I choose All Points") {
            let app = XCUIApplication()
            let tablesQuery = app.tables
            tablesQuery.cells.staticTexts["PM.category_filter_all_points_title_515"].tap()
        }
        
        step("I Pull to Refresh") {
            sleep(5)
            let app = XCUIApplication()
            let pullToRefresh = app.collectionViews.cells.tables.staticTexts["PM.refresh_update_message_1_552"]
            let pullToRefreshEmpty = app.collectionViews.cells.tables["PM.refresh_update_message_1_552, PM.empty_state_all_points_title_524, PM.empty_state_all_points_message_525"].staticTexts["PM.refresh_update_message_1_552"]
            if (pullToRefresh.exists) {
                XCTestCase.VitalityHelper.controlledSwipeDown(swipeDistance: 6, elementToSwipe: pullToRefresh)
            } else {
                XCTestCase.VitalityHelper.controlledSwipeDown(swipeDistance: 6, elementToSwipe: pullToRefreshEmpty)
            }
            
        }
    }
}
