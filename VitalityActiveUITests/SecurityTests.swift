//
//  SettingsSecurityTests.swift
//  VitalityActive
//
//  Created by Steven Layug on 12/18/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import XCTest
import VitalityKit
@testable import XCTest_Gherkin
@testable import VIACore

class SecurityTests: XCTestCase {
    
    private var app: XCUIApplication = XCUIApplication()
    
    func scenario(_ scenario: String) {
        NativeRunner(featureFile: "settingssecurity.feature", testCase: self).run(scenario: scenario)
    }
    
    func testSecurityScreenDisplay() {
        scenario("Redirect to the Security screen")
    }
    
    func testTouchIdToggleDisplay() {
        scenario("Check Touch ID toggle existence")
    }
    
    func testTouchIdToggle() {
        scenario("Test Touch ID Preference toggle")
    }
    
    func testRememberMeToggleDisplay() {
        scenario("Check Remember Me toggle existence")
    }
    
    func testRememberMeToggle() {
        scenario("Test Remember Me toggle")
    }
    
    func testChangePasswordButtonDisplay() {
        scenario("Check Change Password cell existence")
    }
    
    func testChangePasswordScreenDisplay() {
        scenario("Redirect to the Change Password screen")
    }
    
    func testNonMatchingPassword() {
        scenario("Check error of non matching passwords")
    }
    
    func testChangePasswordConfirmation() {
        scenario("Check change password confirmation")
    }
}
