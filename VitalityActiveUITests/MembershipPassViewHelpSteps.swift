//
//  MembershipPassViewHelpSteps.swift
//  VitalityActive
//
//  Created by Erik John T. Alicaya (ADMIN) on 19/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import XCTest
import VitalityKit
@testable import XCTest_Gherkin
@testable import VIACore

class MembershipPassViewHelpSteps: StepDefiner {
        
    override func defineSteps() {
        
        step("I tap on View Help") {
            let app = XCUIApplication()
            let helpRow = app.tables.staticTexts[ProfileStrings.ProfileHelpTitle]
            XCTestCase.VitalityHelper.scrollTo(element: helpRow)
            self.test.waitFor10Secs(element: helpRow)
            helpRow.tap()
        }
        
        step("I click on Back button") {
            let app = XCUIApplication()
            let backButton = app.buttons["Back"]
            self.test.waitFor10Secs(element: backButton)
            backButton.tap()
        }
        
        step("Help screen is displayed") {
            let app = XCUIApplication()
            let suggestionTitle = app.tables[CommonStrings.Help.EmptyTitle316]
            self.test.waitFor10Secs(element: suggestionTitle)
            XCTAssert(suggestionTitle.exists, "Suggestion View screen is not being displayed.")
        }
        
    }
    
}
