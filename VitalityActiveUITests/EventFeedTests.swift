//
//  EventFeedTests.swift
//  VitalityActive
//
//  Created by OJ Garde on 20/11/2017.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import XCTest
import VitalityKit
@testable import XCTest_Gherkin
@testable import VIACore

class EventsFeedTests: XCTestCase {
    func scenario(_ scenario: String) {
        NativeRunner(featureFile: "eventsfeed.feature", testCase: self).run(scenario: scenario)
    }
    
//    func testEventsRow() {
//        scenario("Events row")
//    }
    
//    func testFilterByCategory() {
//        scenario("See Category Filter")
//    }
}

