//
//  AppleWatchCashbackTests.swift
//  VitalityActiveUITests
//
//  Created by Dexter Anthony Ambrad on 1/2/19.
//  Copyright © 2019 Glucode. All rights reserved.
//

import XCTest

class AppleWatchCashbackTests: XCTestCase {
    
    func scenario(_ scenario: String) {
        NativeRunner(featureFile: "applewatchcashback.feature", testCase: self).run(scenario: scenario)
    }
    
    func testAppleWatchCashbackLearnMore() {
        scenario("Apple Watch Cashback Learn More")
    }
    
    func testAWCHelp() {
        scenario("Apple Watch Cashback Help")
    }
    
    func testAWCHowToTrackVitalityCoin() {
        scenario("Apple Watch Cashback How to track vitality coin")
    }
}
