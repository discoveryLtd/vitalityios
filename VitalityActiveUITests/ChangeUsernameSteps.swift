//
//  ChangeUsernameSteps.swift
//  VitalityActiveUITests
//
//  Created by ted.philip.l.lat on 04/10/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import XCTest
import VIACore
import VitalityKit
@testable import XCTest_Gherkin

class ChangeUsernameSteps: StepDefiner {
    
    override func defineSteps() {
        
        step("I provide credentials for successful login") {
            let app = XCUIApplication()
            let tablesQuery = app.scrollViews.otherElements.tables
            
            tablesQuery.cells.textFields[CommonStrings.EmailFieldPlaceholder18].tap()
            tablesQuery.cells.textFields[CommonStrings.EmailFieldPlaceholder18].typeText("test--Armandina_Daily8@mailinator.com")
            
            app.secureTextFields[CommonStrings.PasswordFieldPlaceholder19].tap()
            app.secureTextFields[CommonStrings.PasswordFieldPlaceholder19].typeText("TestPass123")
            
            let loginButton = tablesQuery.buttons["login.login_button_title_20"]
            loginButton.tap()
        }
        
        step("I tap profile") {
            let app = XCUIApplication()
            app.tabBars.buttons["menu_profile_button_9"].tap()
        }
        
        step("I should be in profile screen") {
            let app = XCUIApplication()
            let profileScreen = app.navigationBars[CommonStrings.Settings.LandingTitle901]
            XCTAssert(profileScreen.exists, "Profile Screen not shown")
        }
        
        step("I tap on my account") {
            let app = XCUIApplication()
            let tablesQuery = app.tables
            let myAccount = tablesQuery.cells.staticTexts["Armandina McDaily"]
            myAccount.tap()
        }
        
        step("I should see the personal details screen") {
            let app = XCUIApplication()
            let personalDetailsScreen = app.navigationBars[CommonStrings.Settings.ProfileLandingPersonalDetailsTitle912]
            XCTAssert(personalDetailsScreen.exists, "Personal Details Screen not shown")
        }
        
        step("I tap on my email") {
            let app = XCUIApplication()
            let tablesQuery = app.tables
            tablesQuery.cells.buttons["More Info"].tap()
        }
        
        step("I should see the change email screen") {
            let app = XCUIApplication()
            let changeEmailScreen = app.navigationBars[CommonStrings.Settings.ProfileChangeEmailTitle921]
            XCTAssert(changeEmailScreen.exists, "Change Email Screen not shown")
        }
        
        step("I tap on cancel") {
            let app = XCUIApplication()
            app.navigationBars["Settings.profile_change_email_title_921"].buttons["profile_change_email_cancel"].tap()
        }
        
        step("I entered new email") {
            let app = XCUIApplication()
            let tablesQuery = app.tables
            tablesQuery.cells.textFields[CommonStrings.ProfileChangeEmailEmailPlaceholder].tap()
            tablesQuery.cells.textFields[CommonStrings.ProfileChangeEmailEmailPlaceholder].typeText("Armandina_Daily8@mailinator.com")
        }
        
        step("I press Done") {
            let app = XCUIApplication()
            app.navigationBars["Settings.profile_change_email_title_921"].buttons["done_button_title_53"].tap()
        }
        
        step("I press continue in prompt") {
            let app = XCUIApplication()
            let alert = app.alerts["Settings.alert_change_confirmation_title_929"]
            self.test.waitFor(seconds: 10, element: alert)
            alert.buttons["continue_button_title_87"].tap()
        }
        
        step("I confirm with password") {
            let app = XCUIApplication()
            let passwordRequiredAlert = app.alerts["Settings.alert_password_required_title_931"]
            passwordRequiredAlert.collectionViews.secureTextFields["profile_change_email_password_placeholder"].tap()
            passwordRequiredAlert.collectionViews.secureTextFields["profile_change_email_password_placeholder"].typeText("TestPass123")
            passwordRequiredAlert.buttons["profile_change_email_ok"].tap()
        }
        
        step("I succesfully changed my email") {
            // When change username is succesful, it goes back to Personal Details screen
            let app = XCUIApplication()
            let personalDetailsScreen = app.navigationBars[CommonStrings.Settings.ProfileLandingPersonalDetailsTitle912]
            self.test.waitFor(seconds: 90, element: personalDetailsScreen)
        }
        
        step("I entered the already used email") {
            let app = XCUIApplication()
            let tablesQuery = app.tables
            tablesQuery.cells.textFields[CommonStrings.ProfileChangeEmailEmailPlaceholder].tap()
            tablesQuery.cells.textFields[CommonStrings.ProfileChangeEmailEmailPlaceholder].typeText("Armandina_Daily@mailinator.com")
        }
        
        step("I should see unable to change email prompt") {
            let app = XCUIApplication()
            let alert = app.alerts["Settings.alert_unable_to_change_title_927"]
            self.test.waitFor(seconds: 30, element: alert)
            XCTAssert(alert.exists, "Unable to Change Email not shown")
        }
        
    }
}


