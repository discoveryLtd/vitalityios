//
//  AppleWatchCashbackSteps.swift
//  VitalityActiveUITests
//
//  Created by Dexter Anthony Ambrad on 1/2/19.
//  Copyright © 2019 Glucode. All rights reserved.
//

import UIKit
@testable import VIACore
@testable import VitalityKit
@testable import XCTest_Gherkin

class AppleWatchCashbackSteps: StepDefiner {
    
    override func defineSteps() {
        
        step("I tap AWC card"){
            let app = XCUIApplication()
            let collectionViewsQuery = app.collectionViews

            //let vitalityActiveRewardsCard = collectionViewsQuery.staticTexts["home_card.card_section_title_609"]
            //if vitalityActiveRewardsCard.waitForExistence(timeout: 2) {
            //    vitalityActiveRewardsCard.swipeLeft()
            //}
            
            //let wellnessDevicesCard = collectionViewsQuery.staticTexts["WDA.title_414"]
            //if wellnessDevicesCard.waitForExistence(timeout: 2) {
            //   wellnessDevicesCard.swipeLeft()
            //}
            
            let appleWatchCashbackCard = collectionViewsQuery.staticTexts["home_card.card_section_title_364"]
            appleWatchCashbackCard.tap()
        }
        
        step("I click Got it on AWC onboarding screen") {
            let app = XCUIApplication()
            let getStartedButton = app.tables.buttons["dc.onboarding.got_it_131"]
            getStartedButton.tap()
        }
        
        step("I tap learn more on AWC onboarding screen") {
            let app = XCUIApplication()
            let learnMoreButton = app.tables.buttons["learn_more_button_title_104"]
            sleep(3)
            learnMoreButton.tap()
        }
        
        step("I should see AWC learn more screen") {
            let app = XCUIApplication()
            let learnMoreLanding = app.navigationBars["learn_more_button_title_104"]
            XCTAssert(learnMoreLanding.waitForExistence(timeout: 2), "AWC Learn More screen not shown")
        }
        
        step("I click the AWC help button") {
            sleep(10)
            let app = XCUIApplication()
            let appleWatchHeader = app.navigationBars["home_card.card_section_title_364"]
            
            if (appleWatchHeader.exists) {
                let helpButton = app.tables.staticTexts["help_button_title_141"]
                XCTestCase.VitalityHelper.scrollTo(element: helpButton)
                helpButton.tap()
            }
        }
        
        step("I should be redirected to AWC help") {
            let app = XCUIApplication()
            let suggesstionsText = app/*@START_MENU_TOKEN@*/.tables.containing(.button, identifier:"First").element/*[[".tables.containing(.button, identifier:\"Second\").element",".tables.containing(.button, identifier:\"First\").element"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
            sleep(10)
            XCTAssert(suggesstionsText.waitForExistence(timeout: 5), "Help screen not shown")
        }

        step("I click on how to track Vitality Coin") {
            sleep(10)
            let app = XCUIApplication()
            let appleWatchHeader = app.navigationBars["home_card.card_section_title_364"]
            
            if (appleWatchHeader.exists) {
                let howToTrackVitalityCoinButton = app.tables.staticTexts["awc.landing.action_menu_title_2000"]
                XCTestCase.VitalityHelper.scrollTo(element: howToTrackVitalityCoinButton)
                howToTrackVitalityCoinButton.tap()
            }
        }
        
        step("I should be redirected to how to track Vitality Coin screen") {
            let app = XCUIApplication()
            let howToTrackVitalityCoinScreen = app.tables.staticTexts["awc.track_coins.header_title_2006"]
            XCTAssert(howToTrackVitalityCoinScreen.waitForExistence(timeout: 5), "How to track Vitality Coin screen not shown")
        }
    }
}
