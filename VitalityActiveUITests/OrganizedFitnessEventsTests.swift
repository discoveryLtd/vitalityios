//
//  OrganizedFitnessEventsTests.swift
//  VitalityActiveUITests
//
//  Created by ted.philip.l.lat on 23/11/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import XCTest
@testable import VIACore
@testable import VitalityKit
@testable import VIAUIKit

class OrganizedFitnessEventsTests: XCTestCase {
    
    func scenario(_ scenario: String) {
        NativeRunner(featureFile: "organizedfitnessevents.feature", testCase: self).run(scenario: scenario)
    }
    
    func testOFEOnBoarding() {
        scenario("OFE on-boaridng modal is displayed when user clicks on MWB card on home screen")
    }
    
    func testOFELearnMoreLinkIsVisible() {
        scenario("OFE Learn More link is displayed under Get Started button")
    }
    
    func testTapOFELearnMore() {
        scenario("Check if user is able to tap on OFE Learn More")
    }
    
    func testOFELearnMoreOnBoarding() {
        scenario("I can view the learn more screen from OFE onboarding screen")
    }
    
    func testBackToOFEOnboarding() {
        scenario("Check if user is able to return to Onboarding screen by taping on Back button of OFE Learn More screen")
    }
    
    func testOFEGotIt() {
        scenario("Check if user able to start OFE by taping on Got it")
    }
    
    func testNavigateToEventsAndPoints() {
        scenario("Navigate to Events and Points Screen")
    }
    
    func testNavigateToOFEHistory() {
        scenario("Navigate to History Screen")
    }
    
    func testLearnMoreCellIsVisible() {
        scenario("Check if Learn More cell is visible")
    }
    
    func testHelpCellIsVisible() {
        scenario("Check if Help cell is visible")
    }
    
    func testNavigateToClaimPoints() {
        scenario("Navigate to Claim Points Screen")
    }
    
    func testNavigateToEventType() {
        scenario("Navigate to Event Type Screen")
    }
    
    func testSearchEvent() {
        scenario("Test Search Events")
    }
    
    func testCancelSearch() {
        scenario("Test Cancel button on Search in menu bar")
    }
    
    func testChooseEventType() {
        scenario("Choose an Event type")
    }
    
    func testSetDistanceInClaimPoints() {
        scenario("Set Distance in Claim Points Screen")
    }
    
    func testEventDatePopup() {
        scenario("Show Event Date Pop up")
    }
    
    func testCancelInEventDate() {
        scenario("Press Cancel in Event Date Pop up")
    }
    
    func testOkInEventDate() {
        scenario("Press Ok in Event Date Pop up")
    }
    
    func testNavigateToImageProof() {
        scenario("Navigate to Image Proof Screen")
    }
    
    func testAddImageProofAndNavigateToWeblinkProof() {
        scenario("Add Image Proof and navigate to Weblink Proof")
    }
    
    func testAddWeblinkProofAndNavigateToSummary() {
            scenario("Add Weblink Proof and navigate to Summary")
    }
    
    func testSubmitAnEvent() {
        scenario("Submit an Event")
    }
    
}
