//
//  VHCOnboardingTests.swift
//  VitalityActive
//
//  Created by Sebastian Cichon on 2017/03/29.
//  Copyright © 2017 Glucode. All rights reserved.
//

import XCTest
@testable import VIACore
@testable import VitalityKit
@testable import VIAUIKit

class VitalityHealthCheckTests: XCTestCase {
    
    func scenario(_ scenario: String) {
        NativeRunner(featureFile: "vitalityhealthcheck.feature", testCase: self).run(scenario: scenario)
    }
    
    func testVHCOnboardingFirstTime() {
        scenario("VHC on-boaridng modal is displayed when user clicks on VHC card on home screen")
    }
    
    func testVHCLearnMoreButtbon() {
        scenario("Check if user is able to tap on Learn More")
    }
    
    func testNavigateBackToOnboardingFromLearnMore() {
        scenario("Check if user is able to return to Onboarding screen by taping on Back button from Learn More screen")
    }

    func testGetStartedAndLandingScreen() {
        scenario("Check if user able to start VHC by taping on Get Started")
    }
    
    func testLearnMoreButtonIsVisible() {
        scenario("Learn More link is displayed under Get Started button")
    }
    
    func testLearnMoreScreenView() {
        scenario("I can view the learn more screen from VHC onboarding screen")
    }
    
    func testBackToOnboardingScreenFromLearnMore() {
        scenario("I can go back to VHC onboarding screen from learn more screen")
    }
    
    func testBMIbuttonAndScreen() {
        scenario("I can read more about Body Mass Index")
    }
    
    func testWaistCircumferenceButtonAndScreen() {
        scenario("I can read more about Waist Circumference")
    }
    
    func testBloodGlucoseButtonAndScreen() {
        scenario("I can read more about Blood Glucose")
    }
    
    func testBloodPressureButtonAndScreen() {
        scenario("I can read more about Blood Pressure")
    }
    
    func testCholesterolButtonAndScreen() {
        scenario("I can read more about Cholesterol")
    }
    
    func testHBA1CButtonAndScreen() {
        scenario("I can read more about HbA1c")
    }

    func testNavigateBackToLearnMoreFromInfo() {
        scenario("I can go back to the Learn more screen from the more information screen")
    }
    
    func testGreyedOutCellsOnHealthMeassurements() {
        scenario("Check if all the cells under Health Measurements are Greyed out")
    }
    
    func testMeassurementPointInformation() {
        scenario("Check if every section displays information about how many points can be earned")
    }

    func testNavigateBackToHomeScreenFromVHCLanding() {
        scenario("Check if user can navigate back to Home screen by clicking on back arrow")
    }
    
    func testCaptureResultsLandingScreen() {
        scenario("Check if user can navigate to Capture Results screen")
    }
    
    func testNavigateBackToVHCLandingFromCapture() {
        scenario("Check if user can navigate back to VHC landing from Capture Results")
    }
    
// TO DO: Will be needing a specific user for these tests
//    func testOutOfHealthyRangeMessage() {
//        scenario("Out of Health Range comment if the result is out of range")
//    }
//    
//    func testInHealthyRangeMessage() {
//        scenario("In Healthy Range comment if the result is within healthy range")
//    }
//    func testNumberOfPointsEarnedShown() {
//        scenario("Check if number of points earned are displayed for captured result")
//    }
    
    func testNavigateBackToVHCLandingFromDetails() {
        scenario("Navigate back to VHC Landing screen from details screen")
    }
    
    func testNavigateToVHCFromLearnMore() {
        scenario("Check if user can navigate to Learn More screen and back to VHC landing")
    }
    
    func testNavigateToVHCFromHelp() {
        scenario("Check if user can navigate to Help screen and back to VHC landing screen")
    }
    
    func testBloodPressureValues() {
        scenario("User enters Blood Pressure values")
    }
    
    func testNextButtonStatusDisabled() {
        scenario("Next button status disabled")
    }
    
    func testOutofRangeValuesErrorMessage() {
        scenario("Value out of range error")
    }
    
    func testCorrectOutOfRangeValue() {
        scenario("Out of range error removed after correcting value")
    }
    
    func testDateValueSubmitted() {
        scenario("Able to view date submitted")
    }
    
    func testChangingUnitsOfMeassure() {
        scenario("Change units of meassure")
    }
    
    func testBMINotFullyCapturedAlert() {
        scenario("Display BMI alert when entering one value")
    }
    
    func testCancelOnBMIAlert() {
        scenario("Cancel on BMI alert")
    }
    
    func testContinueOnBMIAlert() {
        scenario("Continue on BMI alert")
    }
    
    func testAddButtonOnAddProofScreen() {
        scenario("Add button on Add Proof screen")
    }
    
    func testAddButtonWithLibraryAndCancel() {
        scenario("Photo upload add button with relevant options")
    }
    
    func testTappingOnCancelOnAddProof() {
        scenario("Tap on cancel on the photo upload screen")
    }
    
    func testGalleryDisplayWhenTappingOnLibrary() {
        scenario("Gallery opened when tapping on choose from library")
        self.dismissAlert()
        scenario("Library is displayed")
    }
    
    func testCancelWhenGalleryIsOpened() {
        scenario("Cancel when gallery is opened")
        self.dismissAlert()
        scenario("Cancel from gallery")
    }
    
    func testSelectPhotoToUploadAsProof() {
        scenario("Select a photo to upload as proof")
        self.dismissAlert()
        scenario("Photo selection from gallery")
    }
    
    func testAddingMultiplePhotosAsProof() {
        scenario("Select more than 1 photo to upload as proof")
        self.dismissAlert()
        scenario("Multiple image selection from gallery")
    }
    
    func dismissAlert() {
        let app = XCUIApplication()
        
        addUIInterruptionMonitor(withDescription: "“Vitality” Would Like to Access Your Photos")
        { (alert) -> Bool in if alert.buttons["OK"].exists {
            alert.buttons["OK"].tap()
            return true
            }
            return false
        }
        
        app.tap()
    }
}
