//
//  XCUIApplication+Additions.swift
//  VitalityActive
//
//  Created by Wilmar van Heerden on 2016/11/07.
//  Copyright © 2016 Glucode. All rights reserved.
//

import XCTest

extension XCUIElement {
    /**
     Removes any current text in the field before typing in the new value
     - Parameter text: the text to enter into the field
     */
    func clearAndEnterText(text: String) {
        guard let stringValue = self.value as? String else {
            XCTFail("Tried to clear and enter text into a non string value")
            return
        }
        
        self.tap()
        
        let deleteString = stringValue.characters.map { _ in XCUIKeyboardKeyDelete }.joined(separator: "")
        
        self.typeText(deleteString + text)
        //self.typeText(text)
    }
}

extension XCUIElement {
    
    func forceTapElement() {
        if self.isHittable {
            sleep(1)
            self.tap()
        }
        else {
            let coordinate: XCUICoordinate = self.coordinate(withNormalizedOffset: CGVector(dx: 0.5, dy: 0.5))
            sleep(1)
            coordinate.tap()
        }
    }
}

class Springboard {
    static let springboard = XCUIApplication(privateWithPath: nil, bundleID: "com.apple.springboard")!
    static let settings = XCUIApplication(privateWithPath: nil, bundleID: "com.apple.Preferences")!
    
    /**
     Terminate and delete the app via springboard
     */
    
    class func deleteMyApp() {
        XCUIApplication().terminate()
        
        // Resolve the query for the springboard rather than launching it
        
        springboard.resolve()
        
        // Force delete the app from the springboard
        let icon = springboard.icons["Vitality"] /// change to correct app name
        if icon.exists {
            let iconFrame = icon.frame
            let springboardFrame = springboard.frame
            icon.press(forDuration: 1.3)
            
            // Tap the little "X" button at approximately where it is. The X is not exposed directly
            
            springboard.coordinate(withNormalizedOffset: CGVector(dx: (iconFrame.minX + 3) / springboardFrame.maxX, dy: (iconFrame.minY + 3) / springboardFrame.maxY)).tap()
            
            sleep(2)
            springboard.alerts.buttons["Delete"].tap()
            
            // Press Delete for any Health data previously stored
            
            let deleteHealth = springboard.alerts.buttons["Delete"]
            sleep(1)
            if deleteHealth.exists {
                deleteHealth.tap()
            }
            
            // Press home once make the icons stop wiggling
            
            sleep(1)
            XCUIDevice.shared().press(.home)
            
            // Press home again to go to the first page of the springboard
            
            //            sleep(1)
            //            XCUIDevice.shared().press(.home)
            
            // Wait some time for the animation end
            
            Thread.sleep(forTimeInterval: 0.5)
            
            // Reset Location and Privacy Settings
            
            //            let settingsIcon = springboard.icons["Settings"]
            //            if settingsIcon.exists {
            //                settingsIcon.tap()
            //                settings.tables.staticTexts["General"].tap()
            //                settings.tables.staticTexts["Reset"].tap()
            //                settings.tables.staticTexts["Reset Location & Privacy"].tap()
            //                settings.buttons["Reset Warnings"].tap()
            //                settings.terminate()
            //           }
        }
    }
}

public extension XCTestCase {
    
    func waitForConditionToBeTrue(seconds: Int, conditionCheck:() -> (Bool)) -> Bool {
        //sleep 100ms to check until found
        for _ in 0..<seconds {
            if (conditionCheck()) {
                return true
            }
            sleep(1)
        }
        return false
    }
    
    func waitFor10Secs(element: XCUIElement) {
        self.waitFor(seconds: 10, element: element)
    }
    
    func waitFor1Sec(element: XCUIElement) {
        self.waitFor(seconds: 1, element: element)
    }
    
    func waitFor(seconds: TimeInterval, element: XCUIElement) {
        let exists = NSPredicate(format: "exists == true && enabled == true")
        self.expectation(for: exists, evaluatedWith: element, handler: nil)
        self.waitForExpectations(timeout: seconds, handler: nil)
    }
    
    public func navigateToAROnboardingFromLogin(_ app: XCUIApplication) {
        app.buttons["generic_skip_button_title"].tap()
        
        app.tables.textFields["login_view_email_field_placeholder"].tap()
        app.tables.textFields["login_view_email_field_placeholder"].typeText("test")
        app.tables.secureTextFields["login_view_password_field_placeholder"].tap()
        app.tables.secureTextFields["login_view_password_field_placeholder"].typeText("test")
        
        app.scrollViews.otherElements.tables.buttons["login_view_login_button_title"].tap()
        self.waitForTermsConditionsToolBarAndTapAgree(app)
        app.buttons["Activate Active Rewards"].tap()
    }
    
    public func waitForTermsConditionsToolBarAndTapAgree(_ app: XCUIApplication) {
        let agreeButton = app.toolbars.buttons["terms_conditions_button_title_agree"]
        let exists = NSPredicate(format: "exists == true && enabled == true")
        expectation(for: exists, evaluatedWith: agreeButton, handler: nil)
        waitForExpectations(timeout: 10, handler: nil)
        agreeButton.tap()
    }
}
