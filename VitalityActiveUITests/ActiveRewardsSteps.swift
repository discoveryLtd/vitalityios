//
//  LoginSteps.swift
//  VitalityActive
//
//  Created by jacdevos on 2016/11/23.
//  Copyright © 2016 Glucode. All rights reserved.
//

import XCTest
@testable import VIACore
@testable import VitalityKit
@testable import XCTest_Gherkin

class ActiveRewardsSteps: StepDefiner {

    override func defineSteps() {
    
        step("I tap on AR card") {
            let app = XCUIApplication()
            let collectionViewsQuery = app.collectionViews
            collectionViewsQuery.collectionViews.staticTexts["home_card.card_section_title_609"].tap()
        }
        
        step("I see AR onboarding modal") {
            let app = XCUIApplication()
            let tablesQuery = app.tables
            let AROnboarding = tablesQuery.staticTexts["AR.onboarding.common_heading_682"]
            XCTAssert(AROnboarding.exists, "AR onboarding not shown")
        }
        
        step("I will see the AR Learn More button") {
            let app = XCUIApplication()
            let tablesQuery = app.tables
            let learnMore = tablesQuery.buttons["learn_more_button_title_104"]
            XCTAssert(learnMore.exists, "AR Learn More button not displayed")
        }
        
        step("I tap AR Learn More") {
            let app = XCUIApplication()
            let tablesQuery = app.tables
            let learnMore = tablesQuery.buttons["learn_more_button_title_104"]
            learnMore.tap()
        }
        
        step("I tap on back button") {
            let app = XCUIApplication()
            let learnMoreScreen = app.navigationBars["learn_more_button_title_104"]
            learnMoreScreen.buttons["Back"].tap()
        }
        
        step("I tap AR Get Started") {
            let app = XCUIApplication()
            let tablesQuery = app.tables
            tablesQuery.buttons["get_started_button_title_103"].tap()
        }
        
//        step("I agree to AR Terms and Conditions") {
//
//        }
        
        step("I tap Got It") {
            let app = XCUIApplication()
            let gotItButton = app.buttons["generic_got_it_button_title_131"]
            if(gotItButton.waitForExistence(timeout: 25)) {
                gotItButton.tap()
            }
        }
        
        step("I tap Link Later") {
            let app = XCUIApplication()
            let tablesQuery = app.tables
            let linkLaterButton = tablesQuery.buttons["WDA.onboarding.link_later_title_778"]
            if(linkLaterButton.waitForExistence(timeout: 25)) {
                linkLaterButton.tap()
            }
        }
        
        step("I will be navigated to AR landing screen") {
            let app = XCUIApplication()
            let landingScreen = app.navigationBars["menu_home_button_5"].staticTexts["AR.Landing.home_view_title_710"]
            XCTAssertTrue(landingScreen.waitForExistence(timeout: 10), "AR landing screen not displayed")
        }
        
        step("I press Cancel in Link a device or app") {
            let app = XCUIApplication()
            let alert = app.alerts["AR.landing.link_device_dialog_title_766"]
            self.test.waitFor(seconds: 15, element: alert)
            alert.buttons["cancel_button_title_24"].tap()
        }
        
        step("I should see AR points") {
            let app = XCUIApplication()
            let tablesQuery = app.tables
            let ARPoints = tablesQuery.cells.staticTexts["AR.goal_progress_message_647"];
            XCTAssert(ARPoints.exists, "AR Points doesn't exist")
        }
        
        step("I should see This Week Activity Cell") {
            let app = XCUIApplication()
            let tablesQuery = app.tables
            let thisWeeksActivityCell = tablesQuery.staticTexts["AR.landing.this_weeks_activity_section_header_title_660"]
            XCTAssert(thisWeeksActivityCell.exists, "This Week Activity Cell doesn't exist")
        }
        
        step("I tap on Activity cell") {
            let app = XCUIApplication()
            app.tables.cells.staticTexts["AR.landing.activity_cell_title_711"].tap()
        }
        
        step("I should see Activity screen") {
            let app = XCUIApplication()
            let activityScreen = app.navigationBars["AR.landing.activity_cell_title_711"]
            XCTAssert(activityScreen.exists, "Activity screen not shown")
        }
        
        step("I tap In Progress Cell") {
            let app = XCUIApplication()
            let tablesQuery = app.tables
            tablesQuery.cells.staticTexts["AR.goal_in_progress_title_726"].tap()
        }
        
        step("I should see In Progress Screen") {
            let app = XCUIApplication()
            let tablesQuery = app.tables
            let progressScreen = tablesQuery.staticTexts["AR.activity_detail.activity_section_footer_text_673"]
            XCTAssert(progressScreen.exists, "In Progress Screen not shown")
        }
        
        step("I tap on Rewards cell") {
            let app = XCUIApplication()
            app.tables.cells.staticTexts["AR.landing.rewards_cell_title_695"].tap()
        }
        
        step("I should see Rewards screen") {
            let app = XCUIApplication()
            let tablesQuery = app.tables
            let rewardsScreen = tablesQuery.staticTexts["AR.rewards.available_spins_section_title_721"]
            XCTAssert(rewardsScreen.exists, "Rewards screen not shown")
        }
        
        step("I tap History") {
            let app = XCUIApplication()
            app.navigationBars["menu_home_button_5"].segmentedControls.buttons["AR.rewards.history_segment_title_670"].tap()
        }
        
        step("I should see History Screen") {
            let app = XCUIApplication()
            let historyButton = app.navigationBars["menu_home_button_5"].segmentedControls.buttons["AR.rewards.history_segment_title_670"]
            XCTAssert(historyButton.isSelected, "History Screen not shown")
        }
        
        step("I tap on Learn More cell") {
            let app = XCUIApplication()
            let learnMoreCell = app.tables.cells.staticTexts["learn_more_button_title_104"]
            XCTestCase.VitalityHelper.scrollTo(element: learnMoreCell)
            learnMoreCell.tap();
        }
        
        step("I should see Learn More screen") {
            let app = XCUIApplication()
            let learnMoreScreen = app.navigationBars["learn_more_button_title_104"]
            XCTAssert(learnMoreScreen.exists, "Learn More screen not shown")
        }
        
        step("I tap Participating Partners") {
            let app = XCUIApplication()
            let tablesQuery = app.tables
            tablesQuery.cells.staticTexts["AR.learn_more.participating_partners_title_696"].tap()
        }
        
        step("I should see Participating Partners Screen") {
            let app = XCUIApplication()
            let tablesQuery = app.tables
            let participatingPartnersScreen = app.navigationBars["AR.learn_more.participating_partners_title_696"]
            XCTAssert(participatingPartnersScreen.exists, "Participating Partners screen not shown")
        }
        
        step("I tap Benefits Guide") {
            let app = XCUIApplication()
            let tablesQuery = app.tables
            tablesQuery.cells.staticTexts["AR.learn_more.benefit_guide_title_728"].tap()
        }
        
        step("I should see Benefits Guide Screen") {
            let app = XCUIApplication()
            let benefitsGuide = app.navigationBars["AR.learn_more.benefit_guide_title_728"]
            XCTAssert(benefitsGuide.exists, "Benefits Guide screen not shown")
        }
        
        step("I tap AR Help cell") {
            let app = XCUIApplication()
            let helpCell = app.tables.cells.staticTexts["help_button_title_141"]
            helpCell.tap();
        }
        
        step("I should see Help screen") {
            let app = XCUIApplication()
            let helpScreen = app.navigationBars["menu_help_button_8"].searchFields["help_view_searchbar_placeholder_263"]
            XCTAssert(helpScreen.exists, "Learn More screen not shown")
        }
    
        step("I press Link Now in Link a device or app") {
            let app = XCUIApplication()
            app.alerts["AR.landing.link_device_dialog_title_766"].buttons["AR.alert_button_title_link_now_779"].tap()
        }
        
        step("I press Got it on Wellness Device onboarding") {
            let app = XCUIApplication()
            let tablesQuery = app.tables
            let gotItButton = tablesQuery.buttons["generic_got_it_button_title_131"]
            if(gotItButton.waitForExistence(timeout: 10)) {
                gotItButton.tap()
            }
        }
        
        step("I should see Wellness Devices screen") {
            let app = XCUIApplication()
            let tablesQuery = app.tables
            let WDScreen = app.navigationBars["WDA.title_414"]
            XCTAssert(WDScreen.waitForExistence(timeout: 10), "Wellness Devices screen not shown")
            let WDList = tablesQuery.staticTexts["WDA.landing.available_link_section_title_426"]
            XCTAssert(WDList.exists, "Wellness Devices List not shown")
        }
    }
}
