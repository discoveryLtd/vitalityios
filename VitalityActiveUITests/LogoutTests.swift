//
//  LogoutTests.swift
//  VitalityActive
//
//  Created by Dexter Anthony Ambrad on 12/16/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import XCTest
import VitalityKit
@testable import XCTest_Gherkin
@testable import VIACore

class LogoutTests: XCTestCase {
    func scenario(_ scenario: String) {
        NativeRunner(featureFile: "logout.feature", testCase: self).run(scenario: scenario)
    }
    
    func testLogoutRow() {
        scenario("Log Out row is present")
    }
    
    func testCancelLogout() {
        scenario("Cancel log out")
    }
    
    func testLogout() {
        scenario("Log out success")
    }
}
