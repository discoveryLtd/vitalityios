import XCTest

class AROnboardingActiveGoalTests: XCTestCase {
    
    private var app: XCUIApplication = XCUIApplication()
    
    override func setUp() {
        super.setUp()
        self.app.launch()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
//    func testAROnboardingFlowIsConfiguredCorrectlyWhenAgreeingToTermsAndConditions() {
//        self.navigateToAROnboardingFromLogin(self.app)
//        self.app.tables.buttons["active_rewards_onboarding_common_main_button_title"].tap()
//        self.app.toolbars.buttons["terms_conditions_button_title_agree"].tap()
//        self.app.buttons["Skip device linking"].tap()
//        self.app.buttons["generic_got_it_button_title"].tap()
//        XCTAssert(self.app.navigationBars["active_rewards_home_view_title"].staticTexts["active_rewards_home_view_title"].exists)
//    }
//    
//    func testAROnboardingFlowIsConfiguredCorrectlyWhenDisagreeingToTermsAndConditions() {
//        self.navigateToAROnboardingFromLogin(self.app)
//        self.app.tables.buttons["active_rewards_onboarding_common_main_button_title"].tap()
//        self.app.toolbars.buttons["terms_conditions_button_title_agree"].tap()
//        XCTAssert(self.app.staticTexts["active_rewards_onboarding_common_heading"].exists)
//    }
//    
//    func testAROnboardingContentIsCorrect() {
//        self.navigateToAROnboardingFromLogin(self.app)
//        XCTAssert(self.app.staticTexts["active_rewards_onboarding_common_heading"].exists)
//        XCTAssert(self.app.buttons["active_rewards_onboarding_common_main_button_title"].exists)
//        XCTAssert(self.app.buttons["active_rewards_landing_learnmore_cell_title"].exists)
//    }
}
