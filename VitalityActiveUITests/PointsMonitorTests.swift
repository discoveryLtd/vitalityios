//
//  PointsMonitorTests.swift
//  VitalityActiveUITests
//
//  Created by ted.philip.l.lat on 19/11/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import XCTest
@testable import VIACore
@testable import VitalityKit
@testable import VIAUIKit

class PointsMonitorTests: XCTestCase {
    
    func scenario(_ scenario: String) {
        NativeRunner(featureFile: "pointsmonitor.feature", testCase: self).run(scenario: scenario)
    }
    
    func testNavigateToPointsMonitor() {
        scenario("Navigate to Points Monitor Screen")
    }
    
    func testFilterByDate() {
        scenario("Filter Points Monitor by Date")
    }
    
    func testFilterByAssessments() {
        scenario("Filter Points Monitor by Assessments")
    }
    
    func testFilterByScreenings() {
        scenario("Filter Points Monitor by Screenings")
    }
    
    func testFilterByFitness() {
        scenario("Filter Points Monitor by Fitness")
    }
    
    func testFilterByAllPoints() {
        scenario("Filter Points Monitor by I choose All Points")
    }
    
    func testPullToRefresh() {
        scenario("Pull to Refresh")
    }
    
}
