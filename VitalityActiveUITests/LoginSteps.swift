//
//  LoginSteps.swift
//  VitalityActive
//
//  Created by jacdevos on 2016/11/15.
//  Copyright © 2016 Glucode. All rights reserved.
//

import Foundation
import XCTest
import VIACore
import VitalityKit
@testable import XCTest_Gherkin

class LoginSteps: StepDefiner {
    static var isFirstLaunchInTestRun = true
    
    func resetWiremockState() {
        //Reset wiremock state, see http://wiremock.org/docs/stateful-behaviour/
        var wiremockResetRequest = URLRequest(url: URL(string: "http://localhost:8080/__admin/scenarios/reset")!)
        wiremockResetRequest.httpMethod = "POST"
        URLSession.shared.dataTask(with: wiremockResetRequest) {data, response, err in
            if let error = err {
                print("Call failed to reset wiremock state:" + error.localizedDescription)
            }
            }.resume()
    }
    
    override func defineSteps() {
        
        //        step("I have launched the app") {
        //            let app = XCUIApplication()
        //            self.resetWiremockState()
        //
        //                app.launchArguments += ["-AppleLanguages", "(en-US)"]
        //                app.launchArguments += ["-AppleLocale", "\"en-US\""]
        //                app.enableStringsToReturnAsKeys()
        //                sleep(2)
        //                app.launch()
        //        }
        
        step("I have installed a new app") {
            let app = XCUIApplication()
            Springboard.deleteMyApp()
            app.launchArguments += ProcessInfo().arguments // add all process info from scheme config
            sleep(3)
            app.launch()
        }
        
        step("I skip the intro") {
            sleep(2)
            let app = XCUIApplication()
            let button = app.buttons[CommonStrings.SkipButtonTitle11]
            if (button.exists) {
                button.tap()
            }
        }
        
        step("login is shown") {
//            let app = XCUIApplication()
//            app.alerts["“Vitality” Would Like to Send You Notifications"].buttons["Don’t Allow"].tap()
//            app.children(matching: .window).element(boundBy: 5).children(matching: .other).element(boundBy: 1).tap()
//            app.buttons["skip_button_title_11"].tap()
//            app.scrollViews.otherElements.tables.textFields["email_field_placeholder_18"].tap()
            
            let app = XCUIApplication()
            let loginField = app.scrollViews.otherElements.tables.textFields["email_field_placeholder_18"]
            self.test.waitFor10Secs(element: loginField)
            XCTAssert(loginField.exists, "Login screen is not being displayed.")
        }
        
        step("I have entered a correct email") {
            let app = XCUIApplication()
            app.scrollViews.otherElements.tables.cells.textFields[CommonStrings.EmailFieldPlaceholder18].tap()
            //app.scrollViews.otherElements.tables.cells.textFields[LoginStrings.EmailFieldPlaceholder18].typeText("test--Darline.Darling@mailinator.com")
            app.scrollViews.otherElements.tables.cells.textFields[CommonStrings.EmailFieldPlaceholder18].typeText("ntest--Cecily_McArmenta@mailinator.com")
        }
        
        step("I have entered Donette's email") {
            let app = XCUIApplication()
            app.scrollViews.otherElements.tables.cells.textFields[CommonStrings.EmailFieldPlaceholder18].tap()
            app.scrollViews.otherElements.tables.cells.textFields[CommonStrings.EmailFieldPlaceholder18].typeText("Donette_Cason@SITtest.com")
        }
        
        step("I have entered User Pref email") {
            let app = XCUIApplication()
            app.scrollViews.otherElements.tables.cells.textFields[CommonStrings.EmailFieldPlaceholder18].tap()
            //app.scrollViews.otherElements.tables.cells.textFields[LoginStrings.EmailFieldPlaceholder18].typeText("Sanai_Frye@SITtest.com")
            app.scrollViews.otherElements.tables.cells.textFields[CommonStrings.EmailFieldPlaceholder18].typeText("ntest--Mitch_McBrowder@mailinator.com")
        }
        
        step("I have entered NSD email") {
            let app = XCUIApplication()
            app.scrollViews.otherElements.tables.cells.textFields[CommonStrings.EmailFieldPlaceholder18].tap()
            app.scrollViews.otherElements.tables.cells.textFields[CommonStrings.EmailFieldPlaceholder18].typeText("jayden@test.com")
        }
        
        step("I have entered NSD completion email") {
            let app = XCUIApplication()
            app.scrollViews.otherElements.tables.cells.textFields[CommonStrings.EmailFieldPlaceholder18].tap()
            app.scrollViews.otherElements.tables.cells.textFields[CommonStrings.EmailFieldPlaceholder18].typeText("skylar@test.com")
        }
        
        step("I have entered a VHC username") {
            let app = XCUIApplication()
            let loginField = app.scrollViews.otherElements.tables.textFields["email_field_placeholder_18"]
            
            loginField.tap()    
            loginField.typeText("ntest--Mitch_McBrowder@mailinator.com")
        }
        
        step("I have entered an MWB username") {
            let app = XCUIApplication()
            let tablesQuery = app.scrollViews.otherElements.tables
        
            tablesQuery.textFields["email_field_placeholder_18"].tap()
            //tablesQuery.textFields["email_field_placeholder_18"].typeText("ntest--Alethea_McCooley@mailinator.com")
            tablesQuery.textFields["email_field_placeholder_18"].typeText("ntest--Clint_McBright@mailinator.com")
        }
        
        self.step("I have entered an AR username") {
            let app = XCUIApplication()
            let tablesQuery = app.scrollViews.otherElements.tables
            
            tablesQuery.textFields["email_field_placeholder_18"].tap()
            tablesQuery.textFields["email_field_placeholder_18"].typeText("ntest--Alethea_McCooley@mailinator.com")
        }
        
        self.step("I have entered a non Activated AR username") {
            let app = XCUIApplication()
            let tablesQuery = app.scrollViews.otherElements.tables
            
            tablesQuery.textFields["email_field_placeholder_18"].tap()
            tablesQuery.textFields["email_field_placeholder_18"].typeText("ntest--Darwin_McBrandon@mailinator.com")
        }
        
        step("I have entered a Points Monitor username") {
            let app = XCUIApplication()
            let tablesQuery = app.scrollViews.otherElements.tables
            
            tablesQuery.textFields["email_field_placeholder_18"].tap()
            tablesQuery.textFields["email_field_placeholder_18"].typeText("ntest--Michael_McBragg@mailinator.com")
        }
        
        step("I have entered an OFE username") {
            let app = XCUIApplication()
            let tablesQuery = app.scrollViews.otherElements.tables
            
            tablesQuery.textFields["email_field_placeholder_18"].tap()
            tablesQuery.textFields["email_field_placeholder_18"].typeText("ntest--Homer_McBlackwood@mailinator.com")
        }
        
        step("I have entered an Insurance Reward username") {
            let app = XCUIApplication()
            let tablesQuery = app.scrollViews.otherElements.tables
            
            tablesQuery.textFields["email_field_placeholder_18"].tap()
            tablesQuery.textFields["email_field_placeholder_18"].typeText("ntest--Michael_McBragg@mailinator.com")
        }
        
        step("I have entered a username for account with Bronze status") {
            let app = XCUIApplication()
            let tablesQuery = app.scrollViews.otherElements.tables
            
            tablesQuery.textFields["email_field_placeholder_18"].tap()
            tablesQuery.textFields["email_field_placeholder_18"].typeText("ntest--Eugenio_McBarclay@mailinator.com")
        }
        
        step("I have entered a not registered email") {
            let app = XCUIApplication()
            app.scrollViews.otherElements.tables.cells.textFields[CommonStrings.EmailFieldPlaceholder18].tap()
            app.scrollViews.otherElements.tables.cells.textFields[CommonStrings.EmailFieldPlaceholder18].typeText("test--notreg@test.com")
        }
        
        step("I have entered an incorrect email") {
            let app = XCUIApplication()
            app.scrollViews.otherElements.tables.cells.textFields[CommonStrings.EmailFieldPlaceholder18].tap()
            app.scrollViews.otherElements.tables.cells.textFields[CommonStrings.EmailFieldPlaceholder18].typeText("test--incorrect@email.com")
        }
        
        step("I have entered an email outside of market") {
            let app = XCUIApplication()
            app.scrollViews.otherElements.tables.cells.textFields[CommonStrings.EmailFieldPlaceholder18].tap()
            app.scrollViews.otherElements.tables.cells.textFields[CommonStrings.EmailFieldPlaceholder18].typeText("levi@test.com")
        }
        
        step("I have entered new VHR username") {
            let app = XCUIApplication()
        app.scrollViews.otherElements.tables.cells.textFields[CommonStrings.EmailFieldPlaceholder18].tap()
        app.scrollViews.otherElements.tables.cells.textFields[CommonStrings.EmailFieldPlaceholder18].typeText("ntest--Corrie_McBedford@mailinator.com")
            
        //app.scrollViews.otherElements.tables.cells.textFields[LoginStrings.EmailFieldPlaceholder18].typeText("qa--Dionna_McBusch@mailinator.com")

        }
        
//      User does not complete any VHR assessments
        step("I have entered new QA VHR username") {
            let app = XCUIApplication()
            app.scrollViews.otherElements.tables.cells.textFields[CommonStrings.EmailFieldPlaceholder18].tap()
            app.scrollViews.otherElements.tables.cells.textFields[CommonStrings.EmailFieldPlaceholder18].typeText("qa--Barton_McAdams@mailinator.com")
        }
        
//      Only social habits is completed
        step("I have entered QA user that completes Social Habits") {
            let app = XCUIApplication()
            app.scrollViews.otherElements.tables.cells.textFields[CommonStrings.EmailFieldPlaceholder18].tap()
            app.scrollViews.otherElements.tables.cells.textFields[CommonStrings.EmailFieldPlaceholder18].typeText("qa--Elden_McBlanchette@mailinator.com")
        }
        
//      New user answers social habits but did not submit.. only to show continue button
        step("I have entered QA VHR username which is in progresss") {
            let app = XCUIApplication()
            app.scrollViews.otherElements.tables.cells.textFields[CommonStrings.EmailFieldPlaceholder18].tap()
            app.scrollViews.otherElements.tables.cells.textFields[CommonStrings.EmailFieldPlaceholder18].typeText("qa--Ambrose_McBurnside@mailinator.com")
        }
        
        step("I have entered a VHR username in progress") {
            let app = XCUIApplication()
            app.scrollViews.otherElements.tables.cells.textFields[CommonStrings.EmailFieldPlaceholder18].tap()
            app.scrollViews.otherElements.tables.cells.textFields[CommonStrings.EmailFieldPlaceholder18].typeText("Ethan_Gross@SITtest.com")
        }
        
        step("I have entered a QA user with completed VHR username") {
            let app = XCUIApplication()
            app.scrollViews.otherElements.tables.cells.textFields[CommonStrings.EmailFieldPlaceholder18].tap()
            app.scrollViews.otherElements.tables.cells.textFields[CommonStrings.EmailFieldPlaceholder18].typeText("qa--Dionna_McBusch@mailinator.com")
        }
        
        step("I have entered VHR user to complete Social Habits") {
            let app = XCUIApplication()
            app.scrollViews.otherElements.tables.cells.textFields[CommonStrings.EmailFieldPlaceholder18].tap()
            app.scrollViews.otherElements.tables.cells.textFields[CommonStrings.EmailFieldPlaceholder18].typeText("Justus_Hays@SITtest.com")
        }
        
        step("I have entered a new WDA username") {
            let app = XCUIApplication()
            app.scrollViews.otherElements.tables.cells.textFields[CommonStrings.EmailFieldPlaceholder18].tap()
            app.scrollViews.otherElements.tables.cells.textFields[CommonStrings.EmailFieldPlaceholder18].typeText("Leticia_Leblanc@SITtest.com")
        }
        
        step("I have not entered an email") {
            let app = XCUIApplication()
            app.scrollViews.otherElements.tables.cells.textFields[CommonStrings.EmailFieldPlaceholder18].tap()
            app.scrollViews.otherElements.tables.cells.textFields[CommonStrings.EmailFieldPlaceholder18].typeText("")
        }
        
        step("I have entered an invalid email") {
            let app = XCUIApplication()
            app.scrollViews.otherElements.tables.cells.textFields[CommonStrings.EmailFieldPlaceholder18].tap()
            app.scrollViews.otherElements.tables.cells.textFields[CommonStrings.EmailFieldPlaceholder18].typeText("test--invalidemail$^##")
        }
        
        step("I have entered a valid email") {
            let app = XCUIApplication()
            app.scrollViews.otherElements.tables.cells.textFields[CommonStrings.EmailFieldPlaceholder18].tap()
            app.scrollViews.otherElements.tables.cells.textFields[CommonStrings.EmailFieldPlaceholder18].typeText("test--valid@email.com")
        }
        
        step("I have entered a correct password") {
            let app = XCUIApplication()
            app.secureTextFields[CommonStrings.PasswordFieldPlaceholder19].tap()
            app.secureTextFields[CommonStrings.PasswordFieldPlaceholder19].typeText("TestPass123")
        }
        
        step("I have entered an incorrect password") {
            let app = XCUIApplication()
            app.secureTextFields[CommonStrings.PasswordFieldPlaceholder19].tap()
            app.secureTextFields[CommonStrings.PasswordFieldPlaceholder19].typeText("incorrectpassword")
        }
        
        step("I tap Login") {
            let app = XCUIApplication()
            let tablesQuery = app.scrollViews.otherElements.tables
            let loginButton = tablesQuery.buttons["login.login_button_title_20"]
            loginButton.tap()
        }
        
        step("I am on the Login screen") {
            self.step("I have installed a new app")
            self.step("I skip the intro")
            self.step("login is shown")
        }
        
        step("I should be logged in successfully") {
            self.step("I agree to terms and conditions")
            self.step("I tap next on user preferences")
            //self.step("I am on the home screen")
        }
        
        step("I logged in successfully and accepted terms") {
            self.step("I skip the intro")
            self.step("login is shown")
            self.step("I have entered a correct email")
            self.step("I have entered a correct password")
            self.step("I tap Login")
            self.step("I agree to terms and conditions")
            self.step("I tap next on user preferences")
        }
        
        step("I have navigated to the NSD home screen") {
            self.step("I have installed a new app")
            self.step("I skip the intro")
            self.step("login is shown")
            self.step("I have entered NSD email")
            self.step("I have entered a correct password")
            self.step("I tap Login")
            self.step("I agree to terms and conditions")
            self.step("I tap next on user preferences")
            self.step("I am on the home screen")
        }
        
        step("I have navigated to NSD home screen with user eligible for completion") {
            self.step("I have installed a new app")
            self.step("I skip the intro")
            self.step("login is shown")
            self.step("I have entered NSD completion email")
            self.step("I have entered a correct password")
            self.step("I tap Login")
            self.step("I agree to terms and conditions")
            self.step("I tap next on user preferences")
            self.step("I am on the home screen")
        }
        
        step("I have navigated to a newly installed app home screen") {
            self.step("I have installed a new app")
            self.step("I skip the intro")
            self.step("login is shown")
            self.step("I have entered a VHC username")
            self.step("I have entered a correct password")
            self.step("I tap Login")
            self.step("I agree to terms and conditions")
            self.step("I tap next on user preferences")
            self.step("I am on the home screen")
        }
        
        step("I have navigated to a newly installed IGI app home screen") {
            self.step("I have installed a new app")
            self.step("I skip the intro")
            self.step("login is shown")
            self.step("I have entered an IGI VHC username")
            self.step("I have entered a correct password")
            self.step("I tap Login")
            self.step("I agree to terms and conditions")
            self.step("I tap next on user preferences")
            self.step("I am on the home screen")
        }
        
        step("I have navigated to user preferences") {
            self.step("I have installed a new app")
            self.step("I skip the intro")
            self.step("login is shown")
            self.step("I have entered User Pref email")
            self.step("I have entered a correct password")
            self.step("I tap Login")
            self.step("I agree to terms and conditions")
        }
        
        step("I have navigated to a VHC user home screen") {
            self.step("I have installed a new app")
            self.step("I skip the intro")
            self.step("login is shown")
            self.step("I have entered a VHC username")
            self.step("I have entered a correct password")
            self.step("I tap Login")
            self.step("I agree to terms and conditions")
            self.step("I tap next on user preferences")
            self.step("I am on the home screen")
        }
        
        step("I have navigated to a newly installed to test MWB") {
            self.step("I have installed a new app")
            self.step("I skip the intro")
            self.step("login is shown")
            self.step("I have entered an MWB username")
            self.step("I have entered a correct password")
            self.step("I tap Login")
            self.step("I agree to terms and conditions")
            self.step("I tap next on user preferences")
            self.step("I am on the home screen")
        }
        
        step("I have navigated to a new Active Rewards user home screen") {
            self.step("I have installed a new app")
            self.step("I skip the intro")
            self.step("login is shown")
            self.step("I have entered an AR username")
            self.step("I have entered a correct password")
            self.step("I tap Login")
            self.step("I agree to terms and conditions")
            self.step("I tap next on user preferences")
            self.step("I am on the home screen")
        }
        
        step("I have navigated to a non Activated Active Rewards user home screen") {
            self.step("I have installed a new app")
            self.step("I skip the intro")
            self.step("login is shown")
            self.step("I have entered a non Activated AR username")
            self.step("I have entered a correct password")
            self.step("I tap Login")
            self.step("I agree to terms and conditions")
            self.step("I tap next on user preferences")
            self.step("I am on the home screen")
        }
        
        step("I have navigated to a Points Monitor user home screen") {
            self.step("I have installed a new app")
            self.step("I skip the intro")
            self.step("login is shown")
            self.step("I have entered a Points Monitor username")
            self.step("I have entered a correct password")
            self.step("I tap Login")
            self.step("I agree to terms and conditions")
            self.step("I tap next on user preferences")
            self.step("I am on the home screen")
        }
        
        step("I have navigated to an OFE user home screen") {
            self.step("I have installed a new app")
            self.step("I skip the intro")
            self.step("login is shown")
            self.step("I have entered an OFE username")
            self.step("I have entered a correct password")
            self.step("I tap Login")
            self.step("I agree to terms and conditions")
            self.step("I tap next on user preferences")
            self.step("I am on the home screen")
        }
        
        step("I have navigated to a new VHR user home screen") {
            self.step("I have installed a new app")
            self.step("I skip the intro")
            sleep(15)
            self.step("login is shown")
            //self.step("I have entered new VHR username")
            self.step("I have entered new QA VHR username")
            self.step("I have entered a correct password")
            self.step("I tap Login")
            self.step("I agree to terms and conditions")
            self.step("I tap next on user preferences")
            self.step("I am on the home screen")
        }
        
        step("I have navigated to My Health user home screen") {
            self.step("I have installed a new app")
            self.step("I skip the intro")
            sleep(15)
            self.step("login is shown")
            self.step("I have entered new QA My Health username")
            self.step("I have entered a correct password")
            self.step("I tap Login")
            self.step("I agree to terms and conditions")
            self.step("I tap next on user preferences")
            self.step("I am on the home screen")
        }
        
        step("I have logged in a my health user with Bad Vitality Age") {
            self.step("I have installed a new app")
            self.step("I skip the intro")
            sleep(15)
            self.step("login is shown")
            self.step("I have entered a QA My Health username with Bad Vitality Age")
            self.step("I have entered a correct password")
            self.step("I tap Login")
            self.step("I agree to terms and conditions")
            self.step("I tap next on user preferences")
            self.step("I am on the home screen")
        }
        
        step("I have logged in a my health user with Good Vitality Age") {
            self.step("I have installed a new app")
            self.step("I skip the intro")
            sleep(15)
            self.step("login is shown")
            self.step("I have entered a QA My Health username with Looking Good Vitality Age")
            self.step("I have entered a correct password")
            self.step("I tap Login")
            self.step("I agree to terms and conditions")
            self.step("I tap next on user preferences")
            self.step("I am on the home screen")
        }
        
        step("I have logged in with user that has AppleCashback card") {
            self.step("I have installed a new app")
            self.step("I skip the intro")
            sleep(15)
            self.step("login is shown")
            self.step("I have entered a QA with apple watch cashback card")
            self.step("I have entered a correct password")
            self.step("I tap Login")
            self.step("I agree to terms and conditions")
            self.step("I tap next on user preferences")
            self.step("I am on the home screen")
        }
        
        step("I have logged in a my health user whose VHC is completed but not VHR") {
            self.step("I have installed a new app")
            self.step("I skip the intro")
            sleep(15)
            self.step("login is shown")
            self.step("I have entered a QA My Health username which completed VHC but not VHR")
            self.step("I have entered a correct password")
            self.step("I tap Login")
            self.step("I agree to terms and conditions")
            self.step("I tap next on user preferences")
            self.step("I am on the home screen")
        }
        
        step("I have navigated to a VHR user home screen which is in progress") {
            self.step("I have installed a new app")
            self.step("I skip the intro")
            self.step("login is shown")
//            self.step("I have entered a VHR username in progress")
            self.step("I have entered new QA VHR username")
            self.step("I have entered a correct password")
            self.step("I tap Login")
            self.step("I agree to terms and conditions")
            self.step("I tap next on user preferences")
            self.step("I am on the home screen")
        }
        
        step("I have navigated to a completed VHR user home screen") {
            self.step("I have installed a new app")
            self.step("I skip the intro")
            self.step("login is shown")
//            self.step("I have entered a completed VHR username")
            self.step("I have entered a QA user with completed VHR username")
            self.step("I have entered a correct password")
            self.step("I tap Login")
            self.step("I agree to terms and conditions")
            self.step("I tap next on user preferences")
            self.step("I am on the home screen")
        }
        
        step("I have navigated to VHR who completed social habits") {
            self.step("I have installed a new app")
            self.step("I skip the intro")
            self.step("login is shown")
            //            self.step("I have entered a completed VHR username")
            self.step("I have entered QA user that completes Social Habits")
            self.step("I have entered a correct password")
            self.step("I tap Login")
            self.step("I agree to terms and conditions")
            self.step("I tap next on user preferences")
            self.step("I am on the home screen")
        }
        
        step("I have navigated to VHR home to complete questionnaire") {
            self.step("I have installed a new app")
            self.step("I skip the intro")
            self.step("login is shown")
//            self.step("I have entered VHR user to complete Social Habits")
            self.step("I have entered QA VHR username which is in progresss")
            self.step("I have entered a correct password")
            self.step("I tap Login")
            self.step("I agree to terms and conditions")
            self.step("I tap next on user preferences")
            self.step("I am on the home screen")
        }
        
        step("I have entered new QA My Health username") {
            let app = XCUIApplication()
            app.scrollViews.otherElements.tables.cells.textFields[CommonStrings.EmailFieldPlaceholder18].tap()
            app.scrollViews.otherElements.tables.cells.textFields[CommonStrings.EmailFieldPlaceholder18].typeText("ntest--Myles_McArmijo@mailinator.com")
        }
        
        step("I have entered a QA My Health username with Bad Vitality Age") {
            let app = XCUIApplication()
            app.scrollViews.otherElements.tables.cells.textFields[CommonStrings.EmailFieldPlaceholder18].tap()
            app.scrollViews.otherElements.tables.cells.textFields[CommonStrings.EmailFieldPlaceholder18].typeText("ntest--Mitch_McBrowder@mailinator.com")
        }
        
        step("I have entered a QA with apple watch cashback card") {
            let app = XCUIApplication()
            app.scrollViews.otherElements.tables.cells.textFields[CommonStrings.EmailFieldPlaceholder18].tap()
            app.scrollViews.otherElements.tables.cells.textFields[CommonStrings.EmailFieldPlaceholder18].typeText("ntest--Mitch_McBrowder@mailinator.com")
        }
        
        step("I have entered a QA My Health username with Looking Good Vitality Age") {
            let app = XCUIApplication()
            app.scrollViews.otherElements.tables.cells.textFields[CommonStrings.EmailFieldPlaceholder18].tap()
            app.scrollViews.otherElements.tables.cells.textFields[CommonStrings.EmailFieldPlaceholder18].typeText("qa--Dionna_McBusch@mailinator.com")
        }
        
        step("I have entered a QA My Health username which completed VHC but not VHR") {
            let app = XCUIApplication()
            app.scrollViews.otherElements.tables.cells.textFields[CommonStrings.EmailFieldPlaceholder18].tap()
            app.scrollViews.otherElements.tables.cells.textFields[CommonStrings.EmailFieldPlaceholder18].typeText("ntest--Colin_McBauer@mailinator.com")
        }
        
        step("I have navigated to a Wellness Device user home screen") {
            self.step("I have installed a new app")
            self.step("I skip the intro")
            self.step("login is shown")
            self.step("I have entered a new WDA username")
            self.step("I have entered a correct password")
            self.step("I tap Login")
            self.step("I agree to terms and conditions")
            self.step("I tap next on user preferences")
            self.step("I am on the home screen")
        }
        
        step("I have navigated to Wellness Devices landing screen") {
            self.step("I tap on Wellness Devices card")
            self.step("I tap on Got It button")
        }
        
        step("I have navigated to VHR landing screen") {
            self.step("I tap on VHR card")
            self.step("I tap on get started")
        }
        
        step("I have navigated to VHC landing screen") {
            self.step("I tap on VHC card")
            self.step("I tap the Got It button")
        }
        
        step("I have navigated to Add Proof screen") {
            self.step("I have navigated to a VHC user home screen")
            self.step("I have navigated to VHC landing screen")
            self.step("I tap on Capture Results")
            self.step("I enter Systolic")
            self.step("I enter Diastolic")
            self.step("I tap on Next")
            self.step("Add proof screen is shown")
        }
        
        step("I have navigated to a new user home screen") {
            self.step("I have installed a new app")
            self.step("I skip the intro")
            self.step("login is shown")
            self.step("I have entered an Insurance Reward username")
            self.step("I have entered a correct password")
            self.step("I tap Login")
            self.step("I agree to terms and conditions")
            self.step("I tap next on user preferences")
            self.step("I am on the home screen")
        }
        
        step("I am on the home screen") {
            let app = XCUIApplication()
            let homeScreen = app.tabBars.buttons["menu_home_button_5"]
            self.test.waitFor10Secs(element: homeScreen)
            XCTAssert(homeScreen.exists, "Home Screen is not being displayed - Unable to find the Home title.")
        }
        
        step("I should get an incorrect email or password message") {
            let app = XCUIApplication()
            let incorrectMessage = app.alerts.staticTexts[CommonStrings.Login.IncorrectEmailPasswordErrorTitle47]
            self.test.waitFor10Secs(element: incorrectMessage)
            XCTAssert(incorrectMessage.exists, "Login Alert is not being displayed. Unable to find the Error text.")
        }
        
        step("I see a not registered alert") {
            let app = XCUIApplication()
            let notRegisteredAlert = app.alerts.staticTexts[CommonStrings.ForgotPassword.EmailNotRegisteredAlertTitle56]
            self.test.waitFor10Secs(element: notRegisteredAlert)
            XCTAssert(notRegisteredAlert.exists, "Not Registered alert is not being displayed.")
        }
        
        step("terms and conditions is shown") {
            let agreeButton = XCUIApplication().toolbars.buttons[CommonStrings.AgreeButtonTitle50]
            self.test.waitFor10Secs(element: agreeButton)
            XCTAssert(agreeButton.exists, "Expected agree button to exist")
        }
        
        step("I agree to terms and conditions") {
            sleep(40)
            let agreeButton = XCUIApplication().toolbars.buttons[CommonStrings.AgreeButtonTitle50]
            if (agreeButton.exists) {
                agreeButton.tap()
            }
        }
        
        step("I have not entered a password") {
            let app = XCUIApplication()
            app.tables.secureTextFields[CommonStrings.PasswordFieldPlaceholder19].tap()
            app.tables.secureTextFields[CommonStrings.PasswordFieldPlaceholder19].typeText("")
        }
        
        step("I tap Ok") {
            let app = XCUIApplication()
            let button = app.buttons[CommonStrings.OkButtonTitle40]
            self.test.waitFor10Secs(element: button)
            button.tap()
        }
        
        step("I tap Close") {
            let app = XCUIApplication()
            let closeButton = app.buttons[CommonStrings.GenericCloseButtonTitle10]
            self.test.waitFor10Secs(element: closeButton)
            closeButton.tap()
        }
        
        step("I see Help button") {
            let app = XCUIApplication()
            let button = app.buttons[CommonStrings.HelpButtonTitle141]
            let failureReason = "Help button element not found. Screen not loaded."
            sleep(15)
            XCTAssertTrue(button.exists, failureReason)
        }
        
        step("I tap Continue") {
            let app = XCUIApplication()
            let button = app.buttons[CommonStrings.ContinueButtonTitle87]
            self.test.waitFor10Secs(element: button)
            button.tap()
        }
        
        step("I should get an incorrect email or password message") {
            let forgotIncorrectEmailorPassword = XCUIApplication().alerts[CommonStrings.Login.IncorrectEmailPasswordErrorTitle47]
            let failureReason = "Incorrect Email or Password Message not shown"
            XCTestCase.VitalityHelper.waitForElementToAppear(test: self.test, element: forgotIncorrectEmailorPassword)
            XCTAssertTrue(forgotIncorrectEmailorPassword.exists, failureReason)
        }
        
        step("I should see a Forgot Password prompt") {
            let forgotPasswordAlert = XCUIApplication().alerts[CommonStrings.Login.IncorrectEmailPasswordErrorTitle47]
            let failureReason = "Forgot Password button is not displayed"
            XCTestCase.VitalityHelper.waitForElementToAppear(test: self.test, element: forgotPasswordAlert)
            XCTAssertTrue(forgotPasswordAlert.exists, failureReason)
        }
        
        step("Forgot Password screen is displayed") {
            let forgotPasswordScreen = XCUIApplication().navigationBars[CommonStrings.ForgotPassword.ScreenTitle52]
            let failureReason = "Forgot Password screen is not displayed"
            XCTestCase.VitalityHelper.waitForElementToAppear(test: self.test, element: forgotPasswordScreen)
            XCTAssertTrue(forgotPasswordScreen.exists, failureReason)
        }
        
        step("I entered email in forgot password") {
            let app = XCUIApplication()
            let tablesQuery = app.tables
            tablesQuery.cells.textFields["registration.email_field_placeholder_27"].tap()
            tablesQuery.cells.textFields["registration.email_field_placeholder_27"].typeText("ntest--Corrie_McBedford@mailinator.com")
        }
        
        step("I should see a confirmation that an email has been sent") {
            let app = XCUIApplication()
            let emailSent = app.staticTexts["forgot_password.confirmation_screen.email_sent_message_title_58"]
            self.test.waitFor(seconds: 20, element: emailSent)
            XCTAssert(emailSent.exists, "Email sent screen not shown")
        }
        
        step("I am on the Forgot password screen") {
            let app = XCUIApplication()
            let forgotPasswordScreen = app.navigationBars[CommonStrings.ForgotPassword.ScreenTitle52]
            let failureReason = "Forgot Password screen is not displayed"
            XCTestCase.VitalityHelper.waitForElementToAppear(test: self.test, element: forgotPasswordScreen)
            XCTAssertTrue(forgotPasswordScreen.exists, failureReason)
        }
        
        step("I tap on the forgot password on login screen") {
            let app = XCUIApplication()
            let forgotPasswordButton = app.buttons[CommonStrings.Login.ForgotPasswordButtonTitle22]
            self.test.waitFor10Secs(element: forgotPasswordButton)
            forgotPasswordButton.tap()
        }
        
        step("Screen for Email sent with a Done button displayed") {
            let app = XCUIApplication()
            let sentEmailScreen = app.staticTexts[CommonStrings.ForgotPassword.ConfirmationScreen.EmailSentMessageTitle58]
            let failureReason = "Summary screen is not displayed. Unable to see the heading text."
            XCTestCase.VitalityHelper.waitForElementToAppear(test: self.test, element: sentEmailScreen)
            XCTAssertTrue(sentEmailScreen.exists, failureReason)
        }
        
        step("the Done button becomes enabled") {
            let app = XCUIApplication()
            let button = app.navigationBars[CommonStrings.ForgotPassword.ScreenTitle52]
            XCTAssert(button.exists, "Done button does not exist. Unable to find Done button.")
        }
        
        step("I tap Done") {
            let app = XCUIApplication()
            let button = app.buttons[CommonStrings.DoneButtonTitle53]
            self.test.waitFor10Secs(element: button)
            button.tap()
        }
        
        step("I tap on Forgot Password") {
            let app = XCUIApplication()
            let button = app.alerts.otherElements.buttons[CommonStrings.Login.ForgotPasswordButtonTitle22]
            self.test.waitFor10Secs(element: button)
            button.tap()
        }
        
        step("I can not tap Login") {
            let app = XCUIApplication()
            let button = app.scrollViews.otherElements.tables.buttons["login.login_button_title_20"]
            XCTAssertFalse(button.isEnabled, "Login button not displayed or incorrect state.")
        }
        
        step("I tap Try again") {
            let app = XCUIApplication()
            let button = app.buttons[CommonStrings.TryAgainButtonTitle43]
            self.test.waitFor10Secs(element: button)
            button.tap()
        }
        
        step("I tap Cancel") {
            let app = XCUIApplication()
            let button = app.buttons[CommonStrings.CancelButtonTitle24]
            self.test.waitFor10Secs(element: button)
            button.tap()
        }
    }
}
