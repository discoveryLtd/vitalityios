//
//  VitalityHealthReviewTests.swift
//  VitalityActive
//
//  Created by Sebastian Cichon on 2017/07/12.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import XCTest
import VitalityKit
@testable import XCTest_Gherkin
@testable import VIACore

class VitalityHealthReviewTests: XCTestCase {
    
    func scenario(_ scenario: String) {
        NativeRunner(featureFile: "vitalityhealthreview.feature", testCase: self).run(scenario: scenario)
    }
    
    func testViewVHROnboarding() {
        scenario("View VHR Onboarding")
    }
    
    //func testDisclaimerButtonStyle() {
    //    scenario("View Disclaimer button")
    //}
   
    func testVHRGetStartedButtonStyle() {
        scenario("View VHR Get Started button")
    }
//    
//    func testViewVHRdisclaimer() {
//        scenario("View VHR Disclaimer")
//    }
//    
////    func testNavigateBackToOnboardingFromDisclaimer() {
////        scenario("Navigate back to onboarding from disclaimer")
////    }
//    
    func testVHROnboardingHeader() {
        scenario("Check VHR Onboarding header")
    }
    
    func testViewVHRLandingScreen() {
        scenario("View the VHR landing screen by dismissing onboarding")
    }
    
    func testVHRLearnMoreButton() {
        scenario("View VHR Learn More Button")
    }
    
    func testVHRLearnMoreScreen() {
        scenario("View VHR Learn More Screen")
    }

    func testNavigateBackToLandingFromLearnMore() {
        scenario("Navigate back to VHR landing from Learn More")
    }

    func testViewVHRHelpButton() {
        scenario("View the Help button")
    }

    func testVHRSectionsTitle() {
        scenario("View Sections title on VHR landing screen")
    }
    
    func testViewGeneralHealthQuestionnaire() {
        scenario("View General Health questionnaire")
    }
    
    func testViewLifestyleHabitsQuestionnaire() {
        scenario("View Lifestyle Habits questionnaire")
    }
    
    func testViewSocialHabitsQuestionnaire() {
        scenario("View Social Habits questionnaire")
    }
    
    func testVHRSectionFooterText() {
        scenario("View the VHR sections footer text")
    }

    func testVHRQuestionnaireStartButton() {
        scenario("Check the status of Sections Start button")
    }
    
//    User should completed all questionnaires
    func testVHRQuestionnaireEditButton() {
        scenario("Check the status of Sections Edit button")
    }
//  User is in progress in VHR
    func testVHRQuestionnaireContinueButton() {
        scenario("Check the status of Sections Continue button")
    }

    func testVHRQuestionnaireTimeToComplete() {
        scenario("Check the estimated time is displayed for sections")
    }
    
//    User should completed all questionnaires
    func testCompletedQuestionnaireFooterText() {
        scenario("Check completed footer of section with finished questionnaire")
    }

//    User should completed all questionnaires
    func testVHRCompletedState() {
        scenario("View the VHR completed state on the landing screen")
    }
    
//    User should completed all questionnaires
    func testVHRCompletedPointsCycleMessage() {
        scenario("View completed points and cycle message on landing screen")
    }
    
    func testViewVHRPrivacyPolicy() {
        scenario("View the VHR privacy policy after completing questionnaire")
    }

    func testDisagreeVHRPrivacyPolicy() {
        scenario("Disagree with the VHR privacy policy")
    }

    func testAgreeVHRPrivacyPolicy() {
        scenario("Agree with the VHR privacy policy")
    }

    func testVHRCompletionScreen() {
        scenario("View the completion screen after completing questionnaire")
    }

    func testCompleteStateForVHR() {
        scenario("View the completed state on the completion screen")
    }

    func testViewRemainingVHRSections() {
        scenario("View how many sections remaining on completion screen")
    }

    func testViewGeneralHealthOnCompletion() {
        scenario("View general health section on completion screen")
    }

    func testViewLifeStyleHabitsOnCompletion() {
        scenario("View lifestyle habits section on completion screen")
    }
    
    func testTapDoneOnCompletionScreen() {
        scenario("I am able to tap on done and navigate back to VHR landing")
    }
}
