//
//  InactiveMemberTests.swift
//  VitalityActiveUITests
//
//  Created by ted.philip.l.lat on 09/10/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import XCTest
import VitalityKit
@testable import XCTest_Gherkin
@testable import VIACore

class InactiveMemberTests: XCTestCase {
    
    func scenario(_ scenario: String) {
        NativeRunner(featureFile: "inactivemember.feature", testCase: self).run(scenario: scenario)
    }
    
//  The step "I should see the inactive membership screen" should be rewritten
//  it will cause unfullfilled expectation that will cause the UI Test to sigabrt
//
//  Error:
//    Terminating app due to uncaught exception 'NSInternalInconsistencyException',
//    reason: 'Unable to report test assertion failure 'Asynchronous wait failed:
//    Exceeded timeout of 10 seconds, with unfulfilled expectations
    
//    func testInactiveUserSuccessfulRegistration() {
//        scenario("Inactive user is successfully registered")
//    }
//
//    func testInactiveMemberScreen() {
//        scenario("Inactive membership screen is shown")
//    }
//
//    func testLogoutPrompt() {
//        scenario("Logout button is pressed")
//    }
//
//    func testCancelLogut() {
//        scenario("Cancel Logout Inactive membership account")
//    }
//
//    func testLogoutInactiveMember() {
//        scenario("Logout Inactive membership account")
//    }
    
}
