//
//  SettingsSecuritySteps.swift
//  VitalityActive
//
//  Created by Steven Layug on 12/18/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import XCTest
import VitalityKit
@testable import XCTest_Gherkin
@testable import VIACore

class SecuritySteps: StepDefiner {
    
    override func defineSteps() {
        let app = XCUIApplication()
        let tablesQuery = app.tables
        
        step("I have entered a correct account for security tests") {
            app.scrollViews.otherElements.tables.cells.textFields[LoginStrings.Registration.EmailFieldPlaceholder27].tap()
            app.scrollViews.otherElements.tables.cells.textFields[LoginStrings.Registration.EmailFieldPlaceholder27].typeText("test--Danika_Brinkman@SITtest.com")
        }
        
        step("I go to Security screen"){
            app.tabBars.buttons["Profile"].tap()
            tablesQuery.staticTexts["settings_landing_settings_title"].tap()
            tablesQuery.staticTexts["settings_security_title"].tap()
        }
        
        step("I am on the Security screen"){
            let privacyStatement = XCUIApplication().tables.staticTexts["user_prefs.security_group_header_title_77"]
            XCTAssertTrue(privacyStatement.exists , "Security screen is not displayed")
        }
        
        step("I check if Touch ID toggle is displayed"){
            let touchIdToggle = tablesQuery.switches["user_prefs.touchid_toggle_title_79, user_prefs.touchid_toggle_message_80"]
            XCTAssertTrue(touchIdToggle.exists , "Privacy toggle is not displayed")
        }
        
        step("I tap Touch ID toggle"){
            let touchIdToggle = tablesQuery.cells.switches["user_prefs.touchid_toggle_title_79, user_prefs.touchid_toggle_message_80"]
            touchIdToggle.tap()
        }
        
        step("I check if Remember Me toggle is displayed"){
            let rememberMeToggle = tablesQuery.cells.switches["user_prefs.remember_me_toggle_title_81, user_prefs.remember_me_toggle_message_82"]
            XCTAssertTrue(rememberMeToggle.exists , "Privacy toggle is not displayed")
        }
        
        step("I tap Remember Me toggle"){
            let rememberMeToggle = tablesQuery.cells.switches["user_prefs.remember_me_toggle_title_81, user_prefs.remember_me_toggle_message_82"]
            rememberMeToggle.tap()
        }
        
        step("I check if the Change Password cell is displayed"){
            let changePasswordButton = tablesQuery.staticTexts["settings_change_password_title"]
            changePasswordButton.tap()
        }
        
        step("I tap the Change Password cell"){
            let changePasswordButton = tablesQuery.staticTexts["settings_change_password_title"]
            changePasswordButton.tap()
        }
        
        step("I input non matching passwords"){
            let newPasswordField = tablesQuery.secureTextFields["At least 7 characters"]
            newPasswordField.tap()
            newPasswordField.typeText("test")
            
            let confirmPasswordField = tablesQuery.secureTextFields["Confirm Password"]
            confirmPasswordField.tap()
            confirmPasswordField.typeText("test1")
            
            app.navigationBars["Change Password"].buttons["Done"].tap()
            
            let nonMatchingError = tablesQuery.children(matching: .cell).element(boundBy: 2).children(matching: .secureTextField).element
            
            XCTAssertTrue(nonMatchingError.exists, "Error not displayed")
        }
        
        step("I will see the non matching password error"){
            app.navigationBars["Change Password"].buttons["Done"].tap()
            let nonMatchingError = tablesQuery.children(matching: .cell).element(boundBy: 2).children(matching: .secureTextField).element
            XCTAssertTrue(nonMatchingError.exists, "Error message is not displayed up is not displayed")
        }
        
        step("I input correct passwords"){
            let oldPasswordField = tablesQuery.secureTextFields["Current Password"]
            oldPasswordField.tap()
            oldPasswordField.typeText("TestPass123")
            
            let newPasswordField = tablesQuery.secureTextFields["At least 7 characters"]
            newPasswordField.tap()
            newPasswordField.typeText("test")
            
            let confirmPasswordField = tablesQuery.secureTextFields["Confirm Password"]
            confirmPasswordField.tap()
            confirmPasswordField.typeText("test")
        }
        
        step("I will see the change password confirmation"){
            app.navigationBars["Change Password"].buttons["Done"].tap()
            
            sleep(30)
            
            let confirmation = app.children(matching: .window).element(boundBy: 0).children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element
            
            XCTAssertTrue(confirmation.exists, "Change Password confirmation is not displayed")
        }
        
    }
    
}
