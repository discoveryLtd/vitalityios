//
//  XCTestCase+Extension.swift
//  VIAAppleWatchCashbackTests
//
//  Created by Von Kervin R. Tuico on 21/09/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import XCTest
import VitalityKit

extension XCTestCase {
    
    var _goodUserName: String {
        get {
            #if WIREMOCK
            return "qa--a24000000715w@mailinator.com"
            #else
            return "qa--a24000000715w@mailinator.com"
            #endif
        }
    }
    var _goodPassword: String {
        get {
            return "TestPass123"
            //return "iO$active13"
        }
    }
    
    var expectationTimeout: Double {
        get {
            return 70.0
        }
    }
    
    public func makeExpectation(_ callingFunctionName: String) -> XCTestExpectation {
        return expectation(description: "Expectation failure for \(callingFunctionName)")
    }
    
    public func login(callback: @escaping ((_: Error?, _: String?) -> Void)) {
        let ex = makeExpectation(#function)
        Wire.Member.login(email: _goodUserName, password: _goodPassword, completion: { upgradeURL, error in
            ex.fulfill()
            callback(upgradeURL, error)
        })
    }
    open override func setUp() {
        self.continueAfterFailure = false
        #if !WIREMOCK
        //XCTFail("Don't run unit tests against LIVE")
        #endif
    }
    
    func createDate(value: String) -> Date {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd"
        return formatter.date(from: value)!
    }
}
