//
//  GetBenefitGoalsAndRewardsFeatureTests.swift
//  VIAAppleWatchCashbackTests
//
//  Created by Michelle R. Oratil on 26/09/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import XCTest
import Alamofire
import VitalityKit

class GetBenefitGoalsAndRewardsFeatureTests: XCTestCase {
    
    #if WIREMOCK
    let partyId: Int = 100002
    let tenantId = 2
    let vitalityMembershipId: Int = 123456
    #else
    var partyId: Int = 0
    var tenantId: Int = 0
    var vitalityMembershipId: Int = 0
    #endif
    
    var expectation: XCTestExpectation?
    var realm = DataProvider.newRealm()
    var gdcRealm = DataProvider.newGDCRealm()
    
    override func setUp() {
        super.setUp()
        
        expectation = makeExpectation(#function)
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testGetBenefitGoalsAndRewardsFeature() {
        login { (error) in
            #if !WIREMOCK
            self.tenantId = self.realm.getTenantId()
            self.partyId = self.realm.getPartyId()
            #endif
            let productKey = 2
            let benefitId = 1000388777
            
            let request = GetBenefitGoalsAndRewardsParameters(benefitId: benefitId, partyId: self.partyId, productKey: productKey)
            
            Wire.Events.getBenefitGoalsAndRewardsFeature(tenantId: self.tenantId, request: request, completion: { (error) in
                guard error == nil else {
                    XCTFail("unexpected error of type: \(String(describing: error))")
                    return (self.expectation?.fulfill())!
                }
                
                (self.expectation?.fulfill())!
            })
        }
        waitForExpectations(timeout: expectationTimeout) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
        }
    }
}
