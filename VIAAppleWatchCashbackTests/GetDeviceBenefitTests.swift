//
//  GetDeviceBenefitTests.swift
//  VIAAppleWatchCashbackTests
//
//  Created by Von Kervin R. Tuico on 21/09/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import XCTest
import Alamofire
import VitalityKit
import VIAUtilities

class GetDeviceBenefitTests: XCTestCase {
    #if WIREMOCK
    let partyId: Int = 100002
    let tenantId = 2
    let goodVitalityMembershipId: Int = 123456
    #else
    var partyId: Int = 0
    var tenantId: Int = 0
    var goodVitalityMembershipId: Int = 0
    #endif
    
    var expectation: XCTestExpectation?
    let realm = DataProvider.newRealm()
    
    override func setUp() {
        
        super.setUp()
        
        expectation = makeExpectation(#function)
    }
    
    func testGetDeviceBenefit() {
        
        login { (error, upgradeURL) in
            
            //gets the credentials of the account used in XCTestCase+Extension
            self.partyId = self.realm.getPartyId()
            self.tenantId = self.realm.getTenantId()
            self.goodVitalityMembershipId = self.realm.getMembershipId()
            
            let productFeatureKey = self.getCardMetaData() ?? "223"
            let getDeviceBenefit = GetDeviceBenefit(productFeatureKey: Int(productFeatureKey)!, swap: false, partyId: self.partyId)
            let getDeviceBenefitRequest = GetDeviceBenefitParameters(getDeviceBenefit)
            
            Wire.Events.getDeviceBenefit(tenantId: self.tenantId, request: getDeviceBenefitRequest, completion: { (error) in
                guard error == nil else {
                    XCTFail("unexpected error of type: \(String(describing: error))")
                    return (self.expectation?.fulfill())!
                }
                
                (self.expectation?.fulfill())!
            })
        }
        
        waitForExpectations(timeout: expectationTimeout) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
        }
    }
    
    func getCardMetaData() -> String? {
        /* Get Device Cashback Home Card Details */
        let deviceCashbackCard = self.realm.homeCard(by: CardTypeRef.AppleWatch)
        
        /* Get "productFeatureKey" value */
        if let cardMetaDatas = deviceCashbackCard?.cardMetadatas {
            for cardMetaData in cardMetaDatas {
                if cardMetaData.type.rawValue == CardMetadataTypeRef.CardFeatureDeviceCashback.rawValue {
                    return cardMetaData.value
                }
            }
        }
        return nil
    }
    
}

