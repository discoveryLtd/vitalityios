fastlane documentation
================
# Installation

Make sure you have the latest version of the Xcode command line tools installed:

```
xcode-select --install
```

Install _fastlane_ using
```
[sudo] gem install fastlane -NV
```
or alternatively using `brew cask install fastlane`

# Available Actions
## iOS
### ios xcode_plugins
```
fastlane ios xcode_plugins
```
Install awesome plugins for Xcode.
### ios keychain_hack
```
fastlane ios keychain_hack
```
For CI use. Jenkins keychain hack
### ios manage_workspace_codesigning
```
fastlane ios manage_workspace_codesigning
```
Configure xcode workspace to enable `Automatically manage signing`.
### ios build
```
fastlane ios build
```
Build a scheme. Defaults to VitalityActive.
### ios build_and_test
```
fastlane ios build_and_test
```
Equivalent of Xcode `Build for Testing`.
### ios coverage_report
```
fastlane ios coverage_report
```
Run coverage report via Slather.
### ios swift_lint
```
fastlane ios swift_lint
```
Run Swiftlint using .swiftlint.yml config file.
### ios scan_report_publish
```
fastlane ios scan_report_publish
```

### ios build_and_export
```
fastlane ios build_and_export
```
Build and Export a scheme.
### ios ez_match
```
fastlane ios ez_match
```
Fastlane match wrapper.
### ios dist_build
```
fastlane ios dist_build
```
Build wrappers
### ios adhoc_build
```
fastlane ios adhoc_build
```
Build wrappers - Adhoc
### ios appstore_build
```
fastlane ios appstore_build
```
Build wrappers - Production
### ios debug_build
```
fastlane ios debug_build
```
Build wrappers - Production
### ios simulator_build
```
fastlane ios simulator_build
```
Build wrappers - Production
### ios ca_dist_build
```
fastlane ios ca_dist_build
```
Build and export Central Asset app.
### ios caa_dist_build
```
fastlane ios caa_dist_build
```
Build and export Central Advance app.
### ios igi_dist_build
```
fastlane ios igi_dist_build
```
Build and export IGI app.
### ios uke_dist_build
```
fastlane ios uke_dist_build
```
Build and export UK Essentials - Adhoc
### ios sli_dist_build
```
fastlane ios sli_dist_build
```
Build and export Sumitomo app.
### ios ec_dist_build
```
fastlane ios ec_dist_build
```
Build and export Ecuador app.
### ios ml_dist_build
```
fastlane ios ml_dist_build
```
Build and export Manulife app.
### ios de_dist_build
```
fastlane ios de_dist_build
```
Build and export Generali app.
### ios us_dist_build
```
fastlane ios us_dist_build
```
Build and export Maverick app.
### ios asrnl_dist_build
```
fastlane ios asrnl_dist_build
```
Build and export ASRVitality app.
### ios build_igi_dist
```
fastlane ios build_igi_dist
```

### ios build_ca_dist
```
fastlane ios build_ca_dist
```

### ios build_caa_dist
```
fastlane ios build_caa_dist
```

### ios build_sli_dist
```
fastlane ios build_sli_dist
```

### ios build_uke_dist
```
fastlane ios build_uke_dist
```

### ios build_ec_dist
```
fastlane ios build_ec_dist
```

### ios build_ml_dist
```
fastlane ios build_ml_dist
```

### ios build_de_dist
```
fastlane ios build_de_dist
```

### ios build_us_dist
```
fastlane ios build_us_dist
```

### ios build_asrnl_dist
```
fastlane ios build_asrnl_dist
```

### ios git_reset_hard
```
fastlane ios git_reset_hard
```
For CI use. Use with Care!!!
### ios get_latest_tag
```
fastlane ios get_latest_tag
```
For CI use. Get the latest tag according to ENV['CI_BUILD_TYPE'].
### ios make_changelog
```
fastlane ios make_changelog
```
For CI use. Create a fastlane/CHANGELOG_FULL.md file.
### ios tag_current_commit
```
fastlane ios tag_current_commit
```
For CI use. Automatically tags the HEAD commit.
### ios adhoc_upload_to_s3
```
fastlane ios adhoc_upload_to_s3
```
Upload an IPA file to Amazon S3.

Example file name built by beta lane is `HealthConnect_app-store_Release_1.0.0_219.ipa`

The lane will extract info from file name convention and use it as an information for build notification
### ios upload_xcarchive_to_s3
```
fastlane ios upload_xcarchive_to_s3
```

### ios upload_ipa_to_s3
```
fastlane ios upload_ipa_to_s3
```
A wrapper for aws_s3 plugin with slack notification.
### ios post_to_slack
```
fastlane ios post_to_slack
```
Post to build to Slack
### ios post_to_teams
```
fastlane ios post_to_teams
```
Post to build to Teams
### ios increment_and_print_version
```
fastlane ios increment_and_print_version
```
Prints and write the version variables to file
### ios cat_app_identifier
```
fastlane ios cat_app_identifier
```
For Slack post use. This will temporarily store the app identifier while the job is cruising to Android slave.
### ios cat_version_number
```
fastlane ios cat_version_number
```
For CI use. Versioning helper script.
### ios cat_build_number
```
fastlane ios cat_build_number
```
For CI use. Versioning helper script.
### ios cat_changelog
```
fastlane ios cat_changelog
```
For CI use. Filter changelogs.
### ios cat_version_classifier
```
fastlane ios cat_version_classifier
```
For CI use. Versioning helper script.
### ios cat_branch
```
fastlane ios cat_branch
```
For CI use. Versioning helper script.
### ios cat_tag_name
```
fastlane ios cat_tag_name
```
For CI use. Versioning helper script.
### ios sonarqube_preview
```
fastlane ios sonarqube_preview
```
Runs sonarqube in preview mode
### ios sonarqube_publish
```
fastlane ios sonarqube_publish
```
Runs sonarqube in publish mode
### ios get_version_classifier
```
fastlane ios get_version_classifier
```
Returns the version classifer according to what is built or type of build
### ios build_scheme_tag
```
fastlane ios build_scheme_tag
```
Build a scheme tag using regex /^adhoc\/[0-9]\.+[0-9]+\.[0-9]+\-SCHEME_TAG\.[0-9]+/ pattern
### ios wiremock
```
fastlane ios wiremock
```

### ios wiremock_record
```
fastlane ios wiremock_record
```

### ios phraseapp_all
```
fastlane ios phraseapp_all
```
Downloads all the latest strings files from PhraseApp for all targets
### ios phraseapp_ca
```
fastlane ios phraseapp_ca
```
Downloads latest strings files from PhraseApp for Central Asset
### ios phraseapp_caa
```
fastlane ios phraseapp_caa
```
Downloads latest strings files from PhraseApp for Central Advance
### ios phraseapp_sli
```
fastlane ios phraseapp_sli
```
Downloads latest strings files from PhraseApp for Sumitomo 🇯🇵
### ios phraseapp_uke
```
fastlane ios phraseapp_uke
```
Downloads latest strings files from PhraseApp for UK Essentials 🇬🇧
### ios phraseapp_igi
```
fastlane ios phraseapp_igi
```
Downloads latest strings files from PhraseApp for IGI Life 🇵🇰
### ios phraseapp_ec
```
fastlane ios phraseapp_ec
```
Downloads latest strings files from PhraseApp for Ecuador 🇪🇨
### ios phraseapp_mli
```
fastlane ios phraseapp_mli
```
Downloads latest strings files from PhraseApp for Manulife 🇨🇦
### ios phraseapp_de
```
fastlane ios phraseapp_de
```
Downloads latest strings files from PhraseApp for Generali 🇩🇪
### ios phraseapp_us
```
fastlane ios phraseapp_us
```
Downloads latest strings files from PhraseApp for Maverick 🇺🇸
### ios phraseapp_asrnl
```
fastlane ios phraseapp_asrnl
```
Downloads latest strings files from PhraseApp for ASR 🇳🇱
### ios register_new_device
```
fastlane ios register_new_device
```

### ios refresh_profiles
```
fastlane ios refresh_profiles
```


----

This README.md is auto-generated and will be re-generated every time [fastlane](https://fastlane.tools) is run.
More information about fastlane can be found on [fastlane.tools](https://fastlane.tools).
The documentation of fastlane can be found on [docs.fastlane.tools](https://docs.fastlane.tools).
