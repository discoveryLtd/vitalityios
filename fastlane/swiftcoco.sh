#!/bin/bash

rm -fv swift-coverage.xml
./xccov-to-sonar-generic.sh ../DerivedData/Logs/Test/*.xcresult/*_Test/action.xccovarchive/ > swift-coverage.xml
mv -v swift-coverage.xml ../sonar-reports/
