#!/bin/bash

filtered=$(cat CHANGELOG_FULL.md | grep -Ev '.+\[Internal\].+|.+\[Fastlane\].+')

# Filter it first! The script exits with error if filtered is empty!
if [ ! -z "${filtered}" ]; then
	cat CHANGELOG_FULL.md | grep -Ev '.+\[Internal\].+|.+\[Fastlane\].+' | grep -E "$SCHEME_TAG" > CHANGELOG.md
	# Ensure that the script runs something successfully..
	if [ $? -gt 0 ]; then
		echo "There are no changes related to [$SCHEME_TAG] from the last successful build." > CHANGELOG.md
	fi
else
	echo "--" > CHANGELOG.md
fi
