import WebKit
import SnapKit
import MessageUI

import VIAUIKit
import VitalityKit
import VIACommon

public class VIAWebActivationBarcodeViewController: VIAViewController {
    
    @IBOutlet var webView: UIWebView!
    internal var htmlContent: Data?{
        didSet{
            self.load(htmlContent: htmlContent)
        }
    }
    var viewModel = VIAWebActivationBarcodeViewModel()
    var isLoading = false
    fileprivate let STRING_TO_EVALUATE = "document.querySelector('meta[name=viewport]').setAttribute('content', 'user-scalable = 1;', false); "
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.makeNavigationBarInvisibleWithoutTint()
        self.configureWebView()
        self.configureBarButtons()
        self.configureAppearance()
        self.generateActivationBarcode()
    }
    
    func configureAppearance() {
        navigationController?.makeNavigationBarTransparent()
        self.webView.backgroundColor = .white
        self.webView.isOpaque = true
        self.webView.scalesPageToFit = true
        self.webView.scrollView.minimumZoomScale = 1.0
        self.webView.scrollView.maximumZoomScale = 5.0
        self.webView.stringByEvaluatingJavaScript(from: STRING_TO_EVALUATE)
        
        //TODO: to clarify where to get title from response
        //self.title = "Renaissance.co"
    }
    
    func configureBarButtons() {
        let leftItem = UIBarButtonItem(title: CommonStrings.DoneButtonTitle53, style: .plain, target: self, action: #selector(doneButtonTapped(_:)))
        let refreshButton = UIBarButtonItem(barButtonSystemItem: .refresh, target: self, action: #selector(generateActivationBarcode))
        let shareButton = UIBarButtonItem(barButtonSystemItem: .action, target: self, action: #selector(emailContent))
        navigationItem.rightBarButtonItems = [refreshButton,shareButton]
        navigationItem.leftBarButtonItem = leftItem
        navigationItem.hidesBackButton = true
    }
    
    func generateActivationBarcode() {
        
        if isLoading {
            return
        }
        
        self.showHUDOnWindow()
        
        viewModel.generateActivationBarcodePDF(completion: { [weak self] error in
            
            self?.hideHUDFromWindow()
            self?.isLoading = false
            
            guard error == nil else {
                self?.handleBackendErrorWithAlert((error as? BackendError) ?? BackendError.other, tryAgainAction: { [weak self] in
                    self?.generateActivationBarcode()
                })
                return
            }
            
            if let htmlContent = self?.viewModel.htmlContent {
                self?.htmlContent = htmlContent
            } else {
                self?.handleBackendErrorWithAlert(BackendError.other, tryAgainAction: { [weak self] in
                    self?.generateActivationBarcode()
                })
            }
        })
    }
    
    func doneButtonTapped (_ sender: UIBarButtonItem) {
        handleDoneTappedInRewardJourney()
    }
    
    func handleDoneTappedInRewardJourney() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func configureWebView() {
        self.view.addSubview(self.webView)
        self.edgesForExtendedLayout = []
        self.webView.snp.makeConstraints { make in
            make.width.equalTo(self.view)
            make.top.equalTo(self.view)
            make.bottom.equalTo(self.view)
        }
    }
    
    func load(htmlContent: Data?) {
        
        if let htmlContent = htmlContent {
            self.webView.load(htmlContent, mimeType: "application/pdf", textEncodingName: "UTF-8", baseURL: NSURL() as URL)
        }
    }
    
    func emailContent(){
        if let htmlContent = htmlContent {
            self.sendPDFToEmail(data: htmlContent)
        }
    }
}

extension VIAWebActivationBarcodeViewController: UIWebViewDelegate{
    
    public func webViewDidStartLoad(_ webView: UIWebView) {
        self.showHUDOnView(view: webView)
    }
    
    public func webViewDidFinishLoad(_ webView: UIWebView) {
        self.hideHUDFromView(view: webView)
        self.isLoading = false
    }
    
    public func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        self.hideHUDFromView(view: webView)
        self.isLoading = false
        
        self.handleBackendErrorWithAlert((error as? BackendError) ?? BackendError.other, tryAgainAction: { [weak self] in
            self?.generateActivationBarcode()
        })
        return
    }
}

extension VIAWebActivationBarcodeViewController: MFMailComposeViewControllerDelegate{
    
    public func sendPDFToEmail(data: Data){
        
        /* Check to see the device can send email. */
        if MFMailComposeViewController.canSendMail(){
            
            let mailComposer = MFMailComposeViewController()
            mailComposer.mailComposeDelegate = self
            
            mailComposer.addAttachmentData(data, mimeType: "application/pdf", fileName: "All_about_tax.pdf")
            
            /* This will compose and present mail to user. */
            self.present(mailComposer, animated: true, completion: nil)
        }else{
            debugPrint("email is not supported")
        }
    }
    
    public func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        self.dismiss(animated: true, completion: nil)
    }
}
