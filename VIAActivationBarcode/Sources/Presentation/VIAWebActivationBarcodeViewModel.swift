import Foundation

import VitalityKit
import VIAUtilities
import VIAUIKit
import VIACommon

import RealmSwift
import SwiftDate

protocol VIAWebActivationBarcodeViewModelDelegate: class {
    
    func viewModelDidLoadData()
    
    func viewModelDidUpdateData(immediately: Bool)
    
    func shouldEndRefreshing()
}

class VIAWebActivationBarcodeViewModel {

    var delegate: VIAWebActivationBarcodeViewModelDelegate?
    var coreRealm = DataProvider.newRealm()
    var htmlContent: Data?

    func generateActivationBarcodePDF(completion: @escaping (Error?) -> Void) {
        
        let partyId = coreRealm.getPartyId()
        let tenantId = coreRealm.getTenantId()
        
        guard let useDeflateHeader = VIAApplicableFeatures.default.useAcceptEncodingDeflateHeader else { return }
        
        Wire.ActivationCode.generate(tenantId: tenantId, partyId: partyId, useAcceptEncodingDeflateHeader: useDeflateHeader, completion: { error, response in
            
            guard error == nil else {
                completion(error)
                return
            }
            
            self.htmlContent = response
            
            completion(nil)
        })
    }
}
