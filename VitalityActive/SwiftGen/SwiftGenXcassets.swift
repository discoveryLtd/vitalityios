// Generated using SwiftGen, by O.Halligon — https://github.com/SwiftGen/SwiftGen

#if os(OSX)
import AppKit.NSImage
public typealias VitalityActiveColor = NSColor
public typealias VitalityActiveImage = NSImage
#elseif os(iOS) || os(tvOS) || os(watchOS)
import UIKit.UIImage
public typealias VitalityActiveColor = UIColor
public typealias VitalityActiveImage = UIImage
#endif

// swiftlint:disable file_length

public typealias VitalityActiveAssetType = VitalityActiveImageAsset

public struct VitalityActiveImageAsset: Equatable {
	fileprivate var name: String

	public var templateImage: VitalityActiveImage {
		let bundle = Bundle(for: BundleToken.self)
		#if os(iOS) || os(tvOS)
		let image = VitalityActiveImage(named: name, in: bundle, compatibleWith: nil)?.withRenderingMode(.alwaysTemplate)
		#elseif os(OSX)
		let image = bundle.image(forResource: name).withRenderingMode(.alwaysTemplate)
		#elseif os(watchOS)
		let image = VitalityActiveImage(named: name).withRenderingMode(.alwaysTemplate)
		#endif
		guard let result = image else { fatalError("Unable to load template image named \(name).") }
		return result
	}

	public var image: VitalityActiveImage {
		let bundle = Bundle(for: BundleToken.self)
		#if os(iOS) || os(tvOS)
		let image = VitalityActiveImage(named: name, in: bundle, compatibleWith: nil)
		#elseif os(OSX)
		let image = bundle.image(forResource: name)
		#elseif os(watchOS)
		let image = VitalityActiveImage(named: name)
		#endif
		guard let result = image else { fatalError("Unable to load image named \(name).") }
		return result
	}

	public static func ==(lhs: VitalityActiveImageAsset, rhs: VitalityActiveImageAsset) -> Bool {
        return lhs.image == rhs.image
    }
}

public struct VitalityActiveColorAsset {
fileprivate var name: String

#if swift(>=3.2)
@available(iOS 11.0, tvOS 11.0, watchOS 4.0, OSX 10.13, *)
var color: VitalityActiveColor {
return VitalityActiveColor(asset: self)
}
#endif
}

// swiftlint:disable identifier_name line_length nesting type_body_length type_name
public enum VitalityActiveAsset {
  public static let vitalityLogo = VitalityActiveImageAsset(name: "vitalityLogo")

  // swiftlint:disable trailing_comma
  public static let allColors: [VitalityActiveColorAsset] = [
  ]
  public static let allImages: [VitalityActiveImageAsset] = [
    vitalityLogo,
  ]
  // swiftlint:enable trailing_comma
  @available(*, deprecated, renamed: "allImages")
  public static let allValues: [VitalityActiveAssetType] = allImages
}
// swiftlint:enable identifier_name line_length nesting type_body_length type_name

extension VitalityActiveImage {
@available(iOS 1.0, tvOS 1.0, watchOS 1.0, *)
@available(OSX, deprecated,
message: "This initializer is unsafe on macOS, please use the VitalityActiveImageAsset.image property")
convenience init!(asset: VitalityActiveAssetType) {
#if os(iOS) || os(tvOS)
let bundle = Bundle(for: BundleToken.self)
self.init(named: asset.name, in: bundle, compatibleWith: nil)
#elseif os(OSX) || os(watchOS)
self.init(named: asset.name)
#endif
}
}

extension VitalityActiveColor {
#if swift(>=3.2)
@available(iOS 11.0, tvOS 11.0, watchOS 4.0, OSX 10.13, *)
convenience init!(asset: VitalityActiveColorAsset) {
let bundle = Bundle(for: BundleToken.self)
#if os(iOS) || os(tvOS)
self.init(named: asset.name, in: bundle, compatibleWith: nil)
#elseif os(OSX)
self.init(named: asset.name, bundle: bundle)
#elseif os(watchOS)
self.init(named: asset.name)
#endif
}
#endif
}

private final class BundleToken {}
