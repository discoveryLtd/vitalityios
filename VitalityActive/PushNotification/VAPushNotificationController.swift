//
//  PushNotificationController.swift
//  VitalityActive
//
//  Created by ritchie.r.rolloque on 08/05/2019.
//  Copyright © 2019 Glucode. All rights reserved.
//

import Foundation
import VIAPushwoosh
import VitalityKit
import Pushwoosh

class VAPushNotificationController: PushNotificationController {
    
    override class var sharedManager: PushNotificationController {
        struct Static {
            static let instance = VAPushNotificationController()
        }
        return Static.instance
    }
    
    override func setSectionWithPushNotification(_ delegate: UIApplicationDelegate?) {
        if UIApplication.shared.applicationState == .active {
            if screen >= 0 {
                (delegate?.window??.rootViewController as? UITabBarController)?.tabBar.items?[screen].badgeValue = String(getSectionBadge())
                
                if isInApp {
                    if (delegate?.window??.rootViewController as? UITabBarController)?.selectedIndex == screen {
                        showMessageForInAppNotification()
                    } else {
                        setMessageForInAppNotification()
                    }
                }
            }
        }
    }
    
    override func goToSectionWithPushNotification(_ delegate: UIApplicationDelegate?) {
        if let pushNotif = pushNotification {
            let customPushData = PushNotificationManager.push().getCustomPushData(asNSDict: pushNotif)
            var screenIndex: Int = 0
            
            if customPushData != nil {
                screenIndex = customPushData!["screen"] as! Int
                screenIndex -= 1
            }
            
            if screenIndex >= 0 {
                if AppSettings.isSelectedNotificationEnabled() {
                    NotificationCenter.default.post(name: .VIAPushNotificationDidTapped, object: nil)
                }
                
                (delegate?.window??.rootViewController as? UITabBarController)?.dismiss(animated: true, completion: nil)
                
                switch screenIndex{
                case 0...3:
                    (delegate?.window??.rootViewController as? UITabBarController)?.selectedIndex =  screenIndex
                    break
                default:
                    /* If screen number is more than the displayed tabs. Let's navigate to home screen. */
                    (delegate?.window??.rootViewController as? UITabBarController)?.selectedIndex = 0
                    break
                }
                
                AppSettings.setSelectedScreenFromNotification(screen: screenIndex)
                NotificationCenter.default.post(name: .VIAPushNotificationWillOpen, object: nil)
            }
        }
    }
}

