//
//  AppDelegate+ApplicableFeatures.swift
//  VitalityActive
//
//  Created by OJ Garde on 1/10/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import VIACommon
import VitalityKit
import VIAUIKit
import VIACore

/**
 * Main
 **/
extension AppDelegate: ApplicableFeaturesDataSource{

    public var hideHelpTab: Bool?{
        get{
            return false
        }
    }
    
    public var showHelpContactFooter: Bool?{
        get{
            return true
        }
    }
    
    public var showPartnerGetStartedLink: Bool?{
        get{
            return true
        }
    }
    
    public var hideMembershipPassAndUpdateTitle: Bool?{
        get{
            return false
        }
    }
    
    public var showPreviousMembershipYearTitle: Bool?{
        get{
            return true
        }
    }
    
    public var getPointPeriod: PointsPeriod? {
        
        return PointsPeriod.current
    }
    
    public var enableKeyboardAutoToolbar: Bool? {
        get {
            return false
        }
    }
    
    public func getDataSharingLeftBarButtonTitle() -> String{
        return CommonStrings.DisagreeButtonTitle49
    }
    
    public func getDataSharingRightBarButtonTitle() -> String{
        return CommonStrings.AgreeButtonTitle50
    }
    
    public var showPartnersTabItem: Bool? {
        get{
            return false
        }
    }
    
    public var applyPushNotifToggle: Bool? {
        get{
            return false
        }
    }
    
    public var enableNotificationDialogConfirmation: Bool {
        get{
            return true
        }
    }

    public func shouldDisplayEarningPointsAndPointsLimits(for eventKey: Int) -> Bool{
        return true
    }
    
    public func showHomeInitialScreen(){
        var storyboard = UIStoryboard(coreStoryboard: .home)
        if !VitalityPartyMembership.startedMembership(){
            storyboard = UIStoryboard(coreStoryboard: .loginRestriction)
        }
        let viewController = storyboard.instantiateInitialViewController()
        UIApplication.shared.keyWindow?.rootViewController = viewController
    }

    public func getTextFieldViewCell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        return tableView.dequeueReusableCell(withIdentifier: VIATextFieldTableViewCell.defaultReuseIdentifier, for: indexPath)
    }
    
    public func getImageControllerConfiguration() -> AnyObject?{
        return UIImagePickerController.ImageControllerConfiguration(shouldLimitFileSize: false, maxFileSize: 2000000,
                                                                    shouldLimitNumberOfAttachProof: true,
                                                                    maxAttachmentCount: 11,
                                                                    includeAllProofs: true) as AnyObject
    }
    
    public func setMinimumDate() -> Date? {
        return DataProvider.currentMembershipPeriodStartDate()?.setMinimumDate()
    }
    
    public func navigateToHomeScreen(){
        VIAHomeViewController.showHome()
    }
    
    public func navigateToLoginScreen(){
        VIALoginViewController.showLogin()
    }
    
    public var numberOfDecimalPlaces: Int? {
        get {
            return 1
        }
    }
    
    public func getFirstTimePreferenceViewModel() -> AnyObject? {
        return VIAFirstTimePreferenceViewModel()
    }
}
