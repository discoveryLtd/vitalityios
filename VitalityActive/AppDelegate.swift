import Foundation
import UIKit

import VIACore
import VIAUtilities
import VitalityKit
import VIACommon
import VIAPushwoosh
import VIAUIKit

import Firebase
import Pushwoosh

@UIApplicationMain
public class AppDelegate: UIResponder, UIApplicationDelegate, VitalityActiveConfigurer {

    // MARK: Properties

    public var window: UIWindow?

    // MARK: Convenience

    public class func currentDelegate() -> AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }

    // MARK: UIApplicationDelegate

    public func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]? = nil) -> Bool {

        UserDefaults.standard.removeObject(forKey:"inAppMessages")
        UIApplication.shared.applicationIconBadgeNumber = 0
        
        /*
        //Change notification name
        NotificationCenter.default.addObserver(self, selector: #selector(setTagsForPushNotification), name: Notification.Name("SetTagsForPushNotification"), object: nil)
        //PUSHWOOSH
        */
        
        // Analytics
        FirebaseConfiguration.shared.setLoggerLevel(.min)
        FirebaseApp.configure()
        GoogleTagManagerPatch.patchGoogleTagManagerLogging()

        configureVitalityActiveDelegate()
        
        /* 
         * This will disable the logging of Storyboard and XIB files related warnings.
         * Let's enable this during performance testing or refactoring.
         */
        UserDefaults.standard.setValue(false, forKey: "_UIConstraintBasedLayoutLogUnsatisfiable")
        UserDefaults.standard.setValue(false, forKey: "_UIViewAlertForUnsatisfiableConstraints")
        
        let pushDelegate = VIAPushNotificationAppDelegate()
        /* VA-34986 [IOS] Align Pushwoosh tags to Android */
        
        pushDelegate.application(application, didFinishLaunchingWithOptions: launchOptions, delegate: self)

        return true
    }

    public func applicationWillResignActive(_ application: UIApplication) {
    }

    public func applicationDidEnterBackground(_ application: UIApplication) {
    }

    public func applicationWillEnterForeground(_ application: UIApplication) {
    }

    public func applicationDidBecomeActive(_ application: UIApplication) {
        debugPrint("applicationDidBecomeActive")
        
        if !VitalityParty.accessTokenForCurrentParty().isEmpty{
            VIAUpdaterUtil.checkUpdateForNewAppVersion(shouldLogoutOnNilTenantID: false)
        }
    }

    public func applicationWillTerminate(_ application: UIApplication) {
    }

    // MARK: URL scheme launch

    public func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        NotificationInfo.postNotification(from: url)
        return true
    }
}

//PUSHWOOSH
// MARK: Push Notification
extension AppDelegate: PushNotificationDelegate {
    public func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        PushNotificationManager.push().handlePushRegistration(deviceToken as Data!)
        print("didRegisterForRemoteNotificationsWithDeviceToken")
    }
    
    public func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        PushNotificationManager.push().handlePushRegistrationFailure(error)
        print("didFailToRegisterForRemoteNotificationsWithError")
    }
    
    public func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any],
                            fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        PushNotificationManager.push().handlePushReceived(userInfo)
        print("didReceiveRemoteNotification")
        completionHandler(UIBackgroundFetchResult.noData)
    }
    
    // this event is fired when the push is received in the app
    public func onPushReceived(_ pushManager: PushNotificationManager!, withNotification pushNotification: [AnyHashable : Any]!, onStart: Bool) {
        print("Push notification received: \(pushNotification)")
        
        VAPushNotificationController.sharedManager.pushNotificationParser(pushNotification: pushNotification)
        
        // shows a push is received. Implement passive reaction to a push, such as UI update or data download.
    }
    
    // this event is fired when user clicks on notification
    public func onPushAccepted(_ pushManager: PushNotificationManager!, withNotification pushNotification: [AnyHashable : Any]!, onStart: Bool) {
        print("Push notification accepted: \(pushNotification)")
        
        VAPushNotificationController.sharedManager.pushNotification = pushNotification
        VAPushNotificationController.sharedManager.goToSectionWithPushNotification(self)
        
        // shows a user tapped the notification. Implement user interaction, such as showing push details
    }
    
    /*
    public func setTagsForPushNotification(notification: NSNotification) {
        // set tags
        if let uIDs = notification.userInfo, let partyID = uIDs["partyId"], let tenantID = uIDs["tenantId"] {
            let environment = Wire.default.currentEnvironment.name().lowercased()
            let tags : [NSObject : AnyObject] = [NSString(string: "Party ID") : partyID as AnyObject, NSString(string: "Tenant ID") : tenantID as AnyObject, NSString(string: "Environment") : environment as AnyObject]
            PushNotificationManager.push().setTags(tags)
        }
    }
    */
}

extension AppDelegate: ViewControllerDelegateDataSource{
    
    public func analyticsLogEvent(from originClass: AnyClass) {
        let screenName = NSStringFromClass(originClass)
        debugPrint("logEvent: \(screenName)")
        Analytics.logEvent("screen_view_dup", parameters: ["screen_name": screenName])
    }
}
