// Generated using SwiftGen, by O.Halligon — https://github.com/SwiftGen/SwiftGen

// swiftlint:disable sorted_imports
import Foundation
import UIKit
import VIAUIKit

// swiftlint:disable file_length

protocol StoryboardType {
  static var storyboardName: String { get }
}

extension StoryboardType {
  static var storyboard: UIStoryboard {
    return UIStoryboard(name: self.storyboardName, bundle: Bundle(for: BundleToken.self))
  }
}

struct SceneType<T: Any> {
  let storyboard: StoryboardType.Type
  let identifier: String

  func instantiate() -> T {
    guard let controller = storyboard.storyboard.instantiateViewController(withIdentifier: identifier) as? T else {
      fatalError("ViewController '\(identifier)' is not of the expected class \(T.self).")
    }
    return controller
  }
}

struct InitialSceneType<T: Any> {
  let storyboard: StoryboardType.Type

  func instantiate() -> T {
    guard let controller = storyboard.storyboard.instantiateInitialViewController() as? T else {
      fatalError("ViewController is not of the expected class \(T.self).")
    }
    return controller
  }
}

protocol SegueType: RawRepresentable { }

extension UIViewController {
  func perform<S: SegueType>(segue: S, sender: Any? = nil) where S.RawValue == String {
    performSegue(withIdentifier: segue.rawValue, sender: sender)
  }
}

// swiftlint:disable explicit_type_interface identifier_name line_length type_body_length type_name
enum VIAWellnessDevicesStoryboard {
  enum WellnessDevicesApps: StoryboardType {
    static let storyboardName = "WellnessDevicesApps"

    static let initialScene = InitialSceneType<VIAWellnessDevices.VIAWellnessDevicesLandingViewController>(storyboard: WellnessDevicesApps.self)

    static let viaWellnessDevicesLandingViewController = SceneType<VIAWellnessDevices.VIAWellnessDevicesLandingViewController>(storyboard: WellnessDevicesApps.self, identifier: "VIAWellnessDevicesLandingViewController")

    static let viaWellnessDevicesOnboardingFromARViewController = SceneType<VIAWellnessDevices.VIAWellnessDevicesOnboardingViewController>(storyboard: WellnessDevicesApps.self, identifier: "VIAWellnessDevicesOnboardingFromARViewController")

    static let viaWellnessDevicesOnboardingViewController = SceneType<VIAWellnessDevices.VIAWellnessDevicesOnboardingViewController>(storyboard: WellnessDevicesApps.self, identifier: "VIAWellnessDevicesOnboardingViewController")
  }
}

enum StoryboardSegue {
  enum WellnessDevicesApps: String, SegueType {
    case cancelToHealthKitDeviceDetail
    case segueShowGenericAppDetail
    case segueShowHealthAppDetail
    case segueShowHelp
    case segueShowLearnMore
    case showArticle
    case showDataConsent
    case showHealthKitOnboarding
    case showOnboardingSegue
    case showPointsEarningPotential
    case unWindToDevicesLanding
    case unwindToActiveRewardsLandingView
    case unwindToDeviceDetail
    case unwindToHealthKitDeviceDetail
  }
}
// swiftlint:enable explicit_type_interface identifier_name line_length type_body_length type_name

private final class BundleToken {}
