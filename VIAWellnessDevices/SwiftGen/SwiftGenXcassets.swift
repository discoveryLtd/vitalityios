// Generated using SwiftGen, by O.Halligon — https://github.com/SwiftGen/SwiftGen

#if os(OSX)
import AppKit.NSImage
public typealias VIAWellnessDevicesColor = NSColor
public typealias VIAWellnessDevicesImage = NSImage
#elseif os(iOS) || os(tvOS) || os(watchOS)
import UIKit.UIImage
public typealias VIAWellnessDevicesColor = UIColor
public typealias VIAWellnessDevicesImage = UIImage
#endif

// swiftlint:disable file_length

public typealias VIAWellnessDevicesAssetType = VIAWellnessDevicesImageAsset

public struct VIAWellnessDevicesImageAsset: Equatable {
	fileprivate var name: String

	public var templateImage: VIAWellnessDevicesImage {
		let bundle = Bundle(for: BundleToken.self)
		#if os(iOS) || os(tvOS)
		let image = VIAWellnessDevicesImage(named: name, in: bundle, compatibleWith: nil)?.withRenderingMode(.alwaysTemplate)
		#elseif os(OSX)
		let image = bundle.image(forResource: name).withRenderingMode(.alwaysTemplate)
		#elseif os(watchOS)
		let image = VIAWellnessDevicesImage(named: name).withRenderingMode(.alwaysTemplate)
		#endif
		guard let result = image else { fatalError("Unable to load template image named \(name).") }
		return result
	}

	public var image: VIAWellnessDevicesImage {
		let bundle = Bundle(for: BundleToken.self)
		#if os(iOS) || os(tvOS)
		let image = VIAWellnessDevicesImage(named: name, in: bundle, compatibleWith: nil)
		#elseif os(OSX)
		let image = bundle.image(forResource: name)
		#elseif os(watchOS)
		let image = VIAWellnessDevicesImage(named: name)
		#endif
		guard let result = image else { fatalError("Unable to load image named \(name).") }
		return result
	}

	public static func ==(lhs: VIAWellnessDevicesImageAsset, rhs: VIAWellnessDevicesImageAsset) -> Bool {
        return lhs.image == rhs.image
    }
}

public struct VIAWellnessDevicesColorAsset {
fileprivate var name: String

#if swift(>=3.2)
@available(iOS 11.0, tvOS 11.0, watchOS 4.0, OSX 10.13, *)
var color: VIAWellnessDevicesColor {
return VIAWellnessDevicesColor(asset: self)
}
#endif
}

// swiftlint:disable identifier_name line_length nesting type_body_length type_name
public enum VIAWellnessDevicesAsset {
  public enum WellnessDevicesAndApps {
    public static let wellnessDevicesPoints = VIAWellnessDevicesImageAsset(name: "wellnessDevicesPoints")
    public static let wellnessDevicesHeartRate = VIAWellnessDevicesImageAsset(name: "wellnessDevicesHeartRate")
    public static let wellnessDevicesLearnMoreMed = VIAWellnessDevicesImageAsset(name: "wellnessDevicesLearnMoreMed")
    public static let wellnessDevicesLearnMore = VIAWellnessDevicesImageAsset(name: "wellnessDevicesLearnMore")
    public static let wellnessDevicesWeight = VIAWellnessDevicesImageAsset(name: "wellnessDevicesWeight")
    public static let wellnessDevicesHeight = VIAWellnessDevicesImageAsset(name: "wellnessDevicesHeight")
    public static let wellnessDevicesRings = VIAWellnessDevicesImageAsset(name: "wellnessDevicesRings")
    public static let wellnessDevicesSleep = VIAWellnessDevicesImageAsset(name: "wellnessDevicesSleep")
    public static let wellnessDevicesHelp = VIAWellnessDevicesImageAsset(name: "wellnessDevicesHelp")
    public static let wellnessDevicesDistance = VIAWellnessDevicesImageAsset(name: "wellnessDevicesDistance")
    public static let wellnessDevicesSpeed = VIAWellnessDevicesImageAsset(name: "wellnessDevicesSpeed")
    public static let wellnessDevicesSteps = VIAWellnessDevicesImageAsset(name: "wellnessDevicesSteps")
    public static let wellnessDevicesLearnMorePoints = VIAWellnessDevicesImageAsset(name: "wellnessDevicesLearnMorePoints")
    public static let wellnessDevicesCalories = VIAWellnessDevicesImageAsset(name: "wellnessDevicesCalories")
    public static let wellnessDevicesLink = VIAWellnessDevicesImageAsset(name: "wellnessDevicesLink")
    public static let healthKitOnboardingHealthAccess = VIAWellnessDevicesImageAsset(name: "healthKitOnboardingHealthAccess")
  }

  // swiftlint:disable trailing_comma
  public static let allColors: [VIAWellnessDevicesColorAsset] = [
  ]
  public static let allImages: [VIAWellnessDevicesImageAsset] = [
    WellnessDevicesAndApps.wellnessDevicesPoints,
    WellnessDevicesAndApps.wellnessDevicesHeartRate,
    WellnessDevicesAndApps.wellnessDevicesLearnMoreMed,
    WellnessDevicesAndApps.wellnessDevicesLearnMore,
    WellnessDevicesAndApps.wellnessDevicesWeight,
    WellnessDevicesAndApps.wellnessDevicesHeight,
    WellnessDevicesAndApps.wellnessDevicesRings,
    WellnessDevicesAndApps.wellnessDevicesSleep,
    WellnessDevicesAndApps.wellnessDevicesHelp,
    WellnessDevicesAndApps.wellnessDevicesDistance,
    WellnessDevicesAndApps.wellnessDevicesSpeed,
    WellnessDevicesAndApps.wellnessDevicesSteps,
    WellnessDevicesAndApps.wellnessDevicesLearnMorePoints,
    WellnessDevicesAndApps.wellnessDevicesCalories,
    WellnessDevicesAndApps.wellnessDevicesLink,
    WellnessDevicesAndApps.healthKitOnboardingHealthAccess,
  ]
  // swiftlint:enable trailing_comma
  @available(*, deprecated, renamed: "allImages")
  public static let allValues: [VIAWellnessDevicesAssetType] = allImages
}
// swiftlint:enable identifier_name line_length nesting type_body_length type_name

extension VIAWellnessDevicesImage {
@available(iOS 1.0, tvOS 1.0, watchOS 1.0, *)
@available(OSX, deprecated,
message: "This initializer is unsafe on macOS, please use the VIAWellnessDevicesImageAsset.image property")
convenience init!(asset: VIAWellnessDevicesAssetType) {
#if os(iOS) || os(tvOS)
let bundle = Bundle(for: BundleToken.self)
self.init(named: asset.name, in: bundle, compatibleWith: nil)
#elseif os(OSX) || os(watchOS)
self.init(named: asset.name)
#endif
}
}

extension VIAWellnessDevicesColor {
#if swift(>=3.2)
@available(iOS 11.0, tvOS 11.0, watchOS 4.0, OSX 10.13, *)
convenience init!(asset: VIAWellnessDevicesColorAsset) {
let bundle = Bundle(for: BundleToken.self)
#if os(iOS) || os(tvOS)
self.init(named: asset.name, in: bundle, compatibleWith: nil)
#elseif os(OSX)
self.init(named: asset.name, bundle: bundle)
#elseif os(watchOS)
self.init(named: asset.name)
#endif
}
#endif
}

private final class BundleToken {}
