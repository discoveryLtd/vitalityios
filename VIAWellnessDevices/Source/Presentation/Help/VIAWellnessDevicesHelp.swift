//
//  VIAWellnessDevicesHelp.swift
//  VitalityActive
//
//  Created by Simon Stewart on 5/24/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import VIAUIKit

public final class VIAWellnessDevicesHelp: VIAViewController {

    @IBAction func closeClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
