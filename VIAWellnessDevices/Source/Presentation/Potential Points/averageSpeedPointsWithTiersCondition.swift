import Foundation
import VIAUtilities

public struct AverageSpeedRateCondition: PotentialPointsCondition {
    var pointsWithTiersCondition: Conditions
    var unit: Unit? {
        get {
            return self.pointsWithTiersCondition.unitOfMeasure.unit()
        }
    }

    public init (pointsWithTiersCondition: Conditions) {
        self.pointsWithTiersCondition = pointsWithTiersCondition
    }

    internal func asTextGreaterThan(_ greaterThan: String) -> String {
        return WellnessDevicesStrings.PotentialPoints.SpeedAverageSpeedLowerBound487(greaterThan)
    }

    internal func asTextLessThan(_ lessThan: String) -> String {
        return WellnessDevicesStrings.PotentialPoints.SpeedAverageSpeedUpperBound489(lessThan)
    }

    internal func asTextLessAndGreaterThan(_ lessThan: String, _ greaterThan: String) -> String {
        return WellnessDevicesStrings.PotentialPoints.SpeedAverageSpeedLowerAndUpperBound488(greaterThan, lessThan)
    }

    internal func asTextLessOrEqualThan(_ lessOrEqualThan: String) -> String {
        return WellnessDevicesStrings.PotentialPoints.SpeedAverageSpeedLessOrEqualBound601(lessOrEqualThan)
    }

    internal func asTextLessOrEqualAndGreaterThan(_ lessOrEqualThan: String, _ greaterThan: String) -> String {
        return WellnessDevicesStrings.PotentialPoints.SpeedAverageSpeedLessOrEqualAndGreaterBound598(greaterThan, lessOrEqualThan)
    }

    internal func asTextGreaterOrEqualThan(_ greaterOrEqual: String) -> String {
        return WellnessDevicesStrings.PotentialPoints.SpeedAverageSpeedGreaterOrEqualBound597(greaterOrEqual)
    }

    internal func asTextLessAndGreaterOrEqualThan(_ lessThan: String, _ greaterOrEqualThan: String) -> String {
        return WellnessDevicesStrings.PotentialPoints.SpeedAverageSpeedLessThanAndGreaterOrEqualBound600(greaterOrEqualThan, lessThan)
    }

    internal func asTextLessOrEqualAndGreaterOrEqualThan(_ lessOrEqualThan: String, _ greaterOrEqualThan: String) -> String {
        return WellnessDevicesStrings.PotentialPoints.SpeedAverageSpeedLessOrEqualAndGreaterOrEqualBound599(greaterOrEqualThan, lessOrEqualThan)
    }
}
