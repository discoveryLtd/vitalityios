import UIKit
import VIAUtilities

public class VIAStatusPointsEarningPotentialViewModel: PointsEarningPotentialTableViewModel {
    let coreRealm = DataProvider.newRealm()
    let earningActivityKey: Int
    
    var eventType: StatusPointsEntry? {
        get {
            if let eventsTypeEntry = coreRealm.statusPointsEntry(with: earningActivityKey).first {
                return eventsTypeEntry
            }
            return nil
        }
    }
    
    required public init(earningActivityKey: Int) {
        self.earningActivityKey = earningActivityKey
    }
    
    public func item(at index: Int) -> StatusPotentialPointsWithTiersEntryDetails? {
        if index < self.eventType?.potentialPointsEntries.count ?? 0 {
            return self.eventType?.potentialPointsEntries[index]
        }
        return nil
    }
    
    public func pointValueForItem(at index: Int) -> String {
        guard let pointValue = self.item(at: index)?.potentialPoints.value else {
            return ""
        }
        return String(describing: pointValue)
    }
    
    public func numberOfPotentialPointsItems() -> Int {
        return self.eventType?.potentialPointsEntries.count ?? 1
    }
    
    public func pointEarningPotentialItemPoints(for index: Int) -> String {
        if let potentialPoints = self.item(at:index)?.potentialPoints.value {
            return string(for: potentialPoints)
        } else if let totalEventPoints = self.eventType?.potentialPoints {
            return string(for: totalEventPoints)
        }
        return ""
    }
    
    func string(for potentialPoints: Int) -> String {
        var pointValue = ""
        pointValue = String(describing: potentialPoints)
        pointValue.append(" ")
        pointValue.append(WellnessDevicesStrings.PotentialPoints.SectionHeader495)
        return pointValue
    }
    
    public func conditions(for index: Int) -> Array<Conditions> {
        guard let conditions = self.item(at:index)?.conditions else {
            return []
        }
        return Array(conditions)
    }
    
    public func pointEarningPotentialItemDescription(condition: Conditions) -> PointsCondition? {
        var conditionRequirement: PointsCondition? = nil
        if let conditionTypeRef = condition.metadataTypeKey {
            switch conditionTypeRef {
            case EventMetaDataTypeRef.TotalSteps.rawValue:
                conditionRequirement = StepsCondition(pointsWithTiersCondition: condition).asText()
                break
            case EventMetaDataTypeRef.Duration.rawValue:
                conditionRequirement = DurationCondition(pointsWithTiersCondition: condition).asText()
                break
            case EventMetaDataTypeRef.AverageSpeed.rawValue:
                conditionRequirement = AverageSpeedRateCondition(pointsWithTiersCondition: condition).asText()
                break
            case EventMetaDataTypeRef.EnergyExpenditure.rawValue:
                conditionRequirement = CalorieCondition(pointsWithTiersCondition: condition).asText()
                break
            case EventMetaDataTypeRef.AverageHeartRate.rawValue:
                conditionRequirement = HeartRateCondition(pointsWithTiersCondition: condition).asText()
                break
            default:
                break
            }
        }
        return conditionRequirement
    }
    
    public func howToEarnPointsTitle(for pointsActivity: PointsEntryTypeRef) -> String? {
        switch pointsActivity {
        case .Heartrate :
            return WellnessDevicesStrings.PotentialPoints.HowToCalculateHeartRateTitle483
        case .Distance :
            return  nil
        case .Speed :
            return WellnessDevicesStrings.PotentialPoints.HowToCalculateSpeedTitle498
        case .EnergyExpend :
            return WellnessDevicesStrings.PotentialPoints.HowToCalculateCaloriesBurnedTitle499
        case .Steps :
            return WellnessDevicesStrings.PotentialPoints.HowToCalculateStepsTitle497
        case .GymVisit :
            return WellnessDevicesStrings.PotentialPoints.HeaderTitle476
        default:
            return nil
        }
    }
    
    public func howToEarnPointsDescription(for pointsActivity: PointsEntryTypeRef) -> String? {
        switch pointsActivity {
        case .Heartrate :
            return WellnessDevicesStrings.PotentialPoints.HowToCalculateHeartRate484
        case .Distance :
            return nil
        case .Speed :
            return WellnessDevicesStrings.PotentialPoints.HowToCalculateSpeedMessage501
        case .EnergyExpend :
            return WellnessDevicesStrings.PotentialPoints.HowToCalculateCaloriesBurnedMessage502
        case .Steps :
            return WellnessDevicesStrings.PotentialPoints.HowToCalculateStepsMessage500
        case .GymVisit :
            return StatusStrings.Status.Pointsentry.GymVisit867
        default:
            return nil
        }
    }
    
    public func howToEarnPointsAWCDescription(for pointsActivity: PointsEntryTypeRef) -> String? {
        switch pointsActivity {
        case .Heartrate :
            return AWCStrings.Awc.HowToCalculateHeartRate484
        case .Steps :
            return AWCStrings.PotentialPoints.HowToCalculateStepsMessage500
        case .GymVisit:
            return AWCStrings.Awc.TrackCoins.ActionMenuTitle2012
        default:
            return nil
        }
    }
    
    public func totalPotentialBMIPoints() -> String?{
        return string(for: (coreRealm.getAllPotentialPointsWithTiersEventType().first?.totalPotentialPoints.value ?? 0))
    }
    
    
}
