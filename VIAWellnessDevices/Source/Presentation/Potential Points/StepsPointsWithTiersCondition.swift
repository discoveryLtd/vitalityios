import Foundation
import VIAUtilities

public struct StepsCondition: PotentialPointsCondition {
    var pointsWithTiersCondition: Conditions
    var unit: Unit? {
        get {
            return VIAUnits.dimention(with:"")
        }
    }

    public init (pointsWithTiersCondition: Conditions) {
        self.pointsWithTiersCondition = pointsWithTiersCondition
    }

    internal func asTextGreaterThan(_ greaterThan: String) -> String {
        return WellnessDevicesStrings.PotentialPoints.StepsLowerBoundOnly486(greaterThan)
    }

    internal func asTextLessThan(_ lessThan: String) -> String {
        return WellnessDevicesStrings.PotentialPoints.StepsUpperBoundOnly508(lessThan)
    }

    internal func asTextLessAndGreaterThan(_ lessThan: String, _ greaterThan: String) -> String {
        return WellnessDevicesStrings.PotentialPoints.StepsLowerAndUpperBound485(greaterThan, lessThan)
    }

    internal func asTextLessOrEqualThan(_ lessOrEqualThan: String) -> String {
        return WellnessDevicesStrings.PotentialPoints.StepsLessThanOrEqualBound577(lessOrEqualThan)
    }

    internal func asTextLessOrEqualAndGreaterThan(_ lessOrEqualThan: String, _ greaterThan: String) -> String {
        return WellnessDevicesStrings.PotentialPoints.StepsLessThanOrEqualAndGreaterThanBound580(greaterThan, lessOrEqualThan)
    }

    internal func asTextGreaterOrEqualThan(_ greaterOrEqualThan: String) -> String {
        return WellnessDevicesStrings.PotentialPoints.StepsGreaterThanOrEqualBound578(greaterOrEqualThan)
    }

    internal func asTextLessAndGreaterOrEqualThan(_ lessThan: String, _ greaterOrEqualThan: String) -> String {
        return WellnessDevicesStrings.PotentialPoints.StepsLessAndGreaterThanOrEqualThanBound581(greaterOrEqualThan, lessThan)
    }

    internal func asTextLessOrEqualAndGreaterOrEqualThan(_ lessOrEqualThan: String, _ greaterOrEqualThan: String) -> String {
        return WellnessDevicesStrings.PotentialPoints.StepsLessThanOrEqualAndGreaterThanOrEqualBound579(greaterOrEqualThan, lessOrEqualThan)
    }
}
