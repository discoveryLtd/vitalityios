import Foundation
import VIAUtilities

public struct DurationCondition: PotentialPointsCondition {
    var pointsWithTiersCondition: Conditions
    var unit: Unit? {
        get {
            return self.pointsWithTiersCondition.unitOfMeasure.unit()
        }
    }

    public init (pointsWithTiersCondition: Conditions) {
        self.pointsWithTiersCondition = pointsWithTiersCondition
    }

    internal func asTextGreaterThan(_ greaterThan: String) -> String {
        return WellnessDevicesStrings.PotentialPoints.SpeedDurationUpperBound477(greaterThan)
    }

    func asTextLessThan(_ lessThan: String) -> String {
        return WellnessDevicesStrings.PotentialPoints.SpeedDurationLowerBound479(lessThan)
    }

    func asTextLessAndGreaterThan(_ lessThan: String, _ greaterThan: String) -> String {
        return WellnessDevicesStrings.PotentialPoints.SpeedDurationLowerAndUpperBound478(greaterThan, lessThan)
    }

    func asTextLessOrEqualThan(_ lessOrEqualThan: String) -> String {
        return WellnessDevicesStrings.PotentialPoints.SpeedDurationLessOrEqualThanBound583(lessOrEqualThan)
    }

    func asTextLessOrEqualAndGreaterThan(_ lessOrEqual: String, _ greaterThan: String) -> String {
        return WellnessDevicesStrings.PotentialPoints.SpeedDurationLessOrEqualAndGreaterOrEqualThanBound586(greaterThan, lessOrEqual)
    }

    func asTextGreaterOrEqualThan(_ greaterOrEqualThan: String) -> String {
        return WellnessDevicesStrings.PotentialPoints.SpeedDurationGreaterOrEqualBound582(greaterOrEqualThan)
    }

    func asTextLessAndGreaterOrEqualThan(_ lessThan: String, _ greaterOrEqualThan: String) -> String {
        return WellnessDevicesStrings.PotentialPoints.SpeedDurationLessAndGreaterOrEqualThanBound585(greaterOrEqualThan, lessThan)
    }

    func asTextLessOrEqualAndGreaterOrEqualThan(_ lessOrEqual: String, _ greaterOrEqualThan: String) -> String {
        return WellnessDevicesStrings.PotentialPoints.SpeedDurationLessOrEqualAndGreaterOrEqualThanBound586(greaterOrEqualThan, lessOrEqual)
    }
}
