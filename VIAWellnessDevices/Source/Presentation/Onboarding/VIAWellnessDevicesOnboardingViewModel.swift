import Foundation

import VitalityKit
import VIAUIKit
import VIACommon

class VIAWellnessDevicesOnboardingViewModel: OnboardingViewModel {
    let source: WellnessAndAppsSource
    init(accessSource: WellnessAndAppsSource) {
        self.source = accessSource
    }

    public var heading: String {
        return CommonStrings.Wda.Onboarding.Heading416
    }

    public var contentItems: [OnboardingContentItem] {
        var items = [OnboardingContentItem]()
        items.append(OnboardingContentItem(heading: CommonStrings.Wda.Onboarding.Item1Heading417,
                                           content: CommonStrings.Wda.Onboarding.Item1418,
                                           image: VIAWellnessDevicesAsset.WellnessDevicesAndApps.wellnessDevicesLink.image))

        items.append(OnboardingContentItem(heading: CommonStrings.Wda.Onboarding.Item2Heading419,
                                           content: CommonStrings.Wda.Onboarding.Item2420,
                                           image: VIAWellnessDevicesAsset.WellnessDevicesAndApps.wellnessDevicesRings.image))

        //TODO CREATE VIAAPPLICABLEFEATURE FOR EACH MARKET PASSING ONBOARDINGCONTENTITEM
        items.append(VIAApplicableFeatures.default.getThirdOnboardingItem())
        return items
    }

    public var buttonTitle: String {
        if self.source == .homescreen {
            return CommonStrings.GenericGotItButtonTitle131
        } else if self.source == .activeRewards {
            return CommonStrings.Wda.Onboarding.LinkNowTitle777
        }
        return ""
    }

    public var alternateButtonTitle: String? {
        if self.source == .homescreen {
            return CommonStrings.Wda.Onboarding.INeedADeviceOrAppTitle423
        } else if self.source == .activeRewards {
            return CommonStrings.Wda.Onboarding.LinkLaterTitle778
        }
        return nil
    }

    public var gradientColor: Color {
        return .blue
    }

    public var alternateButtonIsVisible: Bool {
        if self.source == .activeRewards {
            return true
        }
        return false
    }
}
