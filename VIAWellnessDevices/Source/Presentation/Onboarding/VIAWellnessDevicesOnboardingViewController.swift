//
//  VIAWellnessDevicesOnboardingViewController.swift
//  VitalityActive
//
//  Created by Wilmar van Heerden on 2016/11/07.
//  Copyright © 2016 Glucode. All rights reserved.
//

import Foundation

import VIAUIKit
import VIACommon
import VitalityKit

class VIAWellnessDevicesOnboardingViewController: VIAOnboardingViewController {

    // MARK: View lifecycle
    public var source: WellnessAndAppsSource?

    override func viewDidLoad() {
        //Have to set viewModel here before calling super.viewDidLoad() otherwise heading and link now and link later buttons are not displayed in AR source scenario
        self.viewModel = VIAWellnessDevicesOnboardingViewModel(accessSource: source ?? .homescreen)
        super.viewDidLoad()
        self.navigationItem.hidesBackButton = true
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isHidden = false
    }

    open override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: VIAOnboardingTableViewCell.defaultReuseIdentifier, for: indexPath) as! VIAOnboardingTableViewCell

        if let viewModel = self.viewModel {
            let item = viewModel.contentItems[indexPath.row]

            cell.setHeadingFont(font: UIFont.onboardingCellHeading())
            cell.contentFont(font: UIFont.onboardingSubtitle())

            cell.heading = item.heading
            cell.content = item.content
            cell.contentImage = item.image
        }
        cell.selectionStyle = .none

        return cell
    }

    // MARK: Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "unWindToDevicesLanding" {
            if let viewController = segue.destination as? VIAWellnessDevicesLandingViewController {
                viewController.configureForActiveRewardsJourney = true
            }
        }
    }

    // MARK: Button actions

    override func mainButtonTapped(_ sender: UIButton) {
        AppSettings.setHasShownWellnessDevicesOnboarding()
        self.performSegue(withIdentifier: "unWindToDevicesLanding", sender: nil)
    }

    override func alternateButtonTapped(_ sender: UIButton) {
        AppSettings.setHasShownWellnessDevicesOnboarding()
        self.performSegue(withIdentifier: "unwindToActiveRewardsLandingView", sender: nil)
    }

    // MARK: Unwind navigation
    @IBAction func cancelTapped(_ sender: Any) {
    }
}

public class VIAWellnessDevicesOnboardingFromARSegue: UIStoryboardSegue {
    
    public override func perform() {
        
        guard let identifier = self.identifier,
            identifier == "showWellnessDevicesOnboardingFromAR",
            let navigationController = self.source.navigationController,
            let wellnessDevicesOnboardingViewController = self.destination as? VIAWellnessDevicesOnboardingViewController
            else
        {
            debugPrint("Objects missing, cannot perform custom segue")
            return
        }
        
        wellnessDevicesOnboardingViewController.source = .activeRewards
        navigationController.pushViewController(wellnessDevicesOnboardingViewController, animated: true)
    }
    
}
