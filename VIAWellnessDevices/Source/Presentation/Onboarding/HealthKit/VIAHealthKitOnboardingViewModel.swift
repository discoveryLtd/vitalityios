import VIAUIKit
import VitalityKit

class VIAHealthKitOnboardingViewModel: CirclesViewModel {

    public required init() {
    }

    public var image: UIImage? {
        return VIAWellnessDevicesAsset.WellnessDevicesAndApps.healthKitOnboardingHealthAccess.image
    }

    public var heading: String? {
        return CommonStrings.HealthKit.OnboardingHeaders451
    }

    public var message: String? {
        return CommonStrings.HealthKit.OnboardingContent452
    }

    public var buttonTitle: String? {
        return CommonStrings.HealthKit.Onboarding.ButtonTitle453
    }

    public var footnote: String? {
        return nil
    }

    public var gradientColor: Color {
        return .blue
    }
}
