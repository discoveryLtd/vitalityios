import Foundation
import UIKit

import VIAUIKit
import VitalityKit

class WDAHealthKitOnboardingViewController: VIACirclesViewController {

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    override func awakeFromNib() {
        super.awakeFromNib()

        self.viewModel = VIAHealthKitOnboardingViewModel()
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        navigationController?.navigationBar.isHidden = false
        navigationController?.makeNavigationBarInvisible()
        addBarButtonItems()
    }

    func addBarButtonItems() {
        let button = UIBarButtonItem(title: CommonStrings.CancelButtonTitle24, style: .plain, target: self, action: #selector(cancel))
        self.navigationItem.leftBarButtonItem = button
    }

    func cancel() {
        self.performSegue(withIdentifier: "cancelToHealthKitDeviceDetail", sender: nil)
    }

    override func buttonTapped(_ sender: UIButton) {
        self.performSegue(withIdentifier: "unwindToHealthKitDeviceDetail", sender: nil)
    }
}
