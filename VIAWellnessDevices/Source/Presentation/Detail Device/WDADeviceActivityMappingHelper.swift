import Foundation

import VitalityKit
import VIAUtilities

import RealmSwift

protocol WDADeviceActivityMappingHelper: class {
    func fetchAndProcessDeviceActivityMappings(completion: @escaping (Error?, Bool) -> Void)
}

extension WDADeviceActivityMappingHelper {

    func localFileURL(for fileName: String) -> URL {
        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        let destination = documentsURL.appendingPathComponent("\(fileName)")
        return destination
    }

    public func fetchAndProcessDeviceActivityMappings(completion: @escaping (Error?, Bool) -> Void) {

        guard let fileName = AppConfigFeature.getFileName(type: AppConfigFeature.AppConfigFeatureType.WDAActivityMapping) else {
            completion(nil, false)
            return
        }
        let fileUrlOnDisk = localFileURL(for: fileName)

        if FileManager.default.fileExists(atPath: fileUrlOnDisk.path) {
            do {
                let fileData = try Data(contentsOf: fileUrlOnDisk)
                processDeviceActivityMappingData(data: fileData)
                completion(nil, true)
                return
            } catch {
                print("Error loading WDA device activity mapping file from disk")
            }
        }

        guard let insurerCMSGroupId = AppConfigFeature.insurerCMSGroupId() else {
            completion(nil, false)
            return
        }

        let lastModifiedDate = CMSFile.downloadDate(for: fileName)
        Wire.Content.getFileByGroupId(fileName: fileName, groupId: insurerCMSGroupId, modifiedSince: lastModifiedDate, completion: { data, error in
            guard error == nil else {
                completion(error, false)
                return
            }
            
            guard let responseData = data else {
                completion(nil, false)
                return
            }
            
            let realm = DataProvider.newRealm()
            realm.updateFileLastAccessedDate(for: fileName)
            self.processDeviceActivityMappingData(data: responseData)
            completion(nil, true)
        })
    }

    internal func processDeviceActivityMappingData(data: Data) {
        guard let jsonDictionary = JSONSerialization.jsonObject(data: data) else { return }

        let wdaRealm = DataProvider.newWDARealm()
        try! wdaRealm.write {
            for device in jsonDictionary {
                guard let activityKeys = device.value as? [Int] else { continue }
                let pointsEntryTypeRefs = activityKeys.map({  PointsEntryTypeRef(rawValue: $0) ?? .Unknown })
                addAvailableActivitiesToRealm(device: device.key, pointsEntryTypeRefs: pointsEntryTypeRefs, wdaRealm: wdaRealm)
            }
        }
    }

    internal func addAvailableActivitiesToRealm(device: String, pointsEntryTypeRefs: [PointsEntryTypeRef], wdaRealm: Realm) {
        let availableActivity = WDAAvailableDeviceActivities()
        availableActivity.device = device

        for pointsEntryTypeRef in pointsEntryTypeRefs {
            let wdaPointsEntryTypeRef = WDAPointsEntryTypeRef()
            wdaPointsEntryTypeRef.value = pointsEntryTypeRef

            availableActivity.availableActivitiesTypeRefs.append(wdaPointsEntryTypeRef)
            wdaRealm.add(availableActivity, update: true)
        }

    }

}
