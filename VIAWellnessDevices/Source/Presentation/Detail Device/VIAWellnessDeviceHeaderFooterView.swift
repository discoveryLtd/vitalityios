//
//  VIAWellnessDeviceViewHeaderFooterView.swift
//  VitalityActive
//
//  Created by Lionel Camacho on 2017/06/30.
//  Copyright © 2017 Glucode. All rights reserved.
//

import UIKit
import VIAUIKit
import VitalityKit
import TTTAttributedLabel

class VIAWellnessDeviceHeaderFooterView: UITableViewHeaderFooterView, Nibloadable {

    @IBOutlet weak var controlHealthDataLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var messageLabel: TTTAttributedLabel!
    @IBOutlet weak var stackView: UIStackView!

    @IBOutlet weak var controlHealthDataHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var controlHealthDataTopConstraint: NSLayoutConstraint!


    
    internal var title: String?
    internal var message: String?
    internal let labelAttributes: [String:Any] = [NSFontAttributeName: UIFont.footnoteFont(),
                                    NSParagraphStyleAttributeName: NSMutableParagraphStyle(),
                                    NSForegroundColorAttributeName: UIColor.darkGrey().cgColor]

    override public func awakeFromNib() {
        super.awakeFromNib()
        setupView()
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        setupView()
    }

    func setupView() {
        self.titleLabel.text = CommonStrings.Wda.PointsEarning.MetricsHeader435
        self.titleLabel.textColor = UIColor.night()
        self.titleLabel.font = UIFont.headlineFont()
        
        controlHealthDataLabel.text = CommonStrings.ControlHealthAppDataMessage463
        controlHealthDataLabel.textColor = UIColor.darkGrey()
        controlHealthDataLabel.font = UIFont.footnoteFont()
        controlHealthDataLabel.isHidden = true

        let pointsEarningMetricContent = CommonStrings.Wda.PointsEarningMetrics.Content436(String())
        let attributedpointsEarningMetricContent = NSMutableAttributedString(string: pointsEarningMetricContent, attributes: labelAttributes)

        self.messageLabel.setText(attributedpointsEarningMetricContent)

        self.backgroundColor = UIColor.textFieldPlaceholder()
    }
    
    func configureSoftbankHeaderFooterView() {
        
        self.titleLabel.text = CommonStrings.Wda.PointsEarning.MetricsHeaderSoftbank435
        
        let pointsEarningMetricContent = CommonStrings.Wda.PointsEarningMetrics.ContentSoftbank436(String())
        let attributedpointsEarningMetricContent = NSMutableAttributedString(string: pointsEarningMetricContent, attributes: labelAttributes)
        
        self.messageLabel.setText(attributedpointsEarningMetricContent)
    }

    func configureAttributedLabelWith(url: URL, delegate: TTTAttributedLabelDelegate, isSoftBank: Bool) {
        messageLabel.delegate = delegate
        
        //TODO
        let learnMoreText = CommonStrings.Wda.PointsEarningMetrics.LearnMoreLinkText437
        let pointsEarningMetricContent = isSoftBank ? CommonStrings.Wda.PointsEarningMetrics.ContentSoftbank436(learnMoreText) : CommonStrings.Wda.PointsEarningMetrics.Content436(learnMoreText)

        let attributedpointsEarningMetricContent = NSMutableAttributedString(string: pointsEarningMetricContent, attributes: labelAttributes)

        messageLabel.linkAttributes = [
            NSForegroundColorAttributeName: UIColor.rewardBlue(),
            NSUnderlineStyleAttributeName: NSNumber(value: NSUnderlineStyle.styleNone.rawValue)
        ]
        messageLabel.activeLinkAttributes = [
            NSForegroundColorAttributeName: UIColor.rewardBlue(),
            NSUnderlineStyleAttributeName: NSNumber(value: NSUnderlineStyle.styleNone.rawValue)
        ]

        messageLabel.setText(attributedpointsEarningMetricContent)

        let range = (pointsEarningMetricContent as NSString).range(of: learnMoreText)

        messageLabel.addLink(to: url, with:range)
    }

    public func setHeaderFooter(title: String) {
        self.titleLabel.text = title
    }

    public func setHeaderFooter(message: String) {
        self.messageLabel.text = message
    }

    func configureForHealthApp(linked: Bool) {
        if linked {
            controlHealthDataLabel.isHidden = false
            stackView.snp.remakeConstraints{ (make) in
                make.top.equalTo(controlHealthDataLabel.snp.bottom).offset(35)
            }
        } else {
             controlHealthDataLabel.isHidden = true
            stackView.snp.remakeConstraints{ (make) in
                 make.top.equalToSuperview().offset(35)
            }
        }
    }
    
    func configureForWellnessDevice() {
        controlHealthDataLabel.isHidden = true
        stackView.snp.remakeConstraints{ (make) in
            make.top.equalToSuperview().offset(35)
        }
    }

}
