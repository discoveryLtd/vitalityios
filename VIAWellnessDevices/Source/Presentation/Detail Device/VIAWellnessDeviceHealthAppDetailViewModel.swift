import VIAUtilities
import VitalityKit
import VIAHealthKit
import HealthKit

import RealmSwift

protocol VIAWellnessDeviceHealthAppDetailViewModelDelegate: class {
    func healthKitWillBeginSync()
    func healthKitDidSync()
    func displayHealthKitOnboardingView()
    func didFinishHealthKitLinking(error: Error?)
    func handleSyncingErrorOccurred(error: Error?)
    func didFinishHealthKitSyncing()
}

class VIAWellnessDeviceHealthAppDetailViewModel {

    struct CellItem {
        let image: UIImage?
        let title: String
        let activity: DeviceDetailActivity
        let activityTypeKey: PointsEntryTypeRef?
    }

    struct Constants {
        struct DeviceLinkingRequestConstants {
            static let userIdentifierTypePartyId = "PARTY_ID"
        }
    }

    var devicePhysicalActivities: [WDAPointsEntryTypeRef] = []
    var deviceHealthMeasurements: [WDAPointsEntryTypeRef] = []
    var deviceOtherTrackableAcitvities: [WDAPointsEntryTypeRef] = []
    
    let coreRealm = DataProvider.newRealm()

    // MARK: Properties

    weak var delegate: VIAWellnessDeviceHealthAppDetailViewModelDelegate?

    // MARK: Lifecycle

    init() {
        NotificationCenter.default.addObserver(self, selector: #selector(displayHealthKitOnboardingView), name: .ShouldDisplayHealthKitOnboardingViewController, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(healthKitDidSync), name: .HealthKitSyncDidComplete, object: nil)
        coreRealm.refresh()
        let partyId = "\(coreRealm.getPartyId())"
        VITHealthKitHelper.shared().setUserEntityNumber(partyId)
    }

    deinit {
        NotificationCenter.default.removeObserver(self, name: .ShouldDisplayHealthKitOnboardingViewController, object: nil)
        NotificationCenter.default.removeObserver(self, name: .HealthKitSyncDidComplete, object: nil)
    }

    // MARK: Data
    func configureDeviceActivities() {
        var activities = [WDAPointsEntryTypeRef]()
        let fitnessTypes = VITHealthKitHelper.shared().readTypesForFitness()
        // heart rate
        if fitnessTypes.contains(HKQuantityType.quantityType(forIdentifier: HKQuantityTypeIdentifier.heartRate)!) {
            let activity = WDAPointsEntryTypeRef()
            activity.value = .Heartrate
            activities.append(activity)
        }
        // steps
        if fitnessTypes.contains(HKQuantityType.quantityType(forIdentifier: HKQuantityTypeIdentifier.stepCount)!) {
            let activity = WDAPointsEntryTypeRef()
            activity.value = .Steps
            activities.append(activity)
        }
        
        devicePhysicalActivities = activities.sorted(by: sortingAlphabetically)

        // Possibly add BMI to deviceHealthMeasurements for Health App in future once clarified since height and weight do not have points entry type keys
    }
    
    func healthAppLearnMoreUrl() -> URL? {
        coreRealm.refresh()
   
        guard let learnMoreContent = coreRealm.feature(of: AppConfigFeature.AppConfigFeatureType.AppleHealthAppLearnMoreContent) else {
            return nil
        }
        
        guard let urlString = learnMoreContent.featureParameters.first?.value, let url = URL(string: urlString) else {
            return nil
        }
        
        return url
    }

    func sortingAlphabetically(this: WDAPointsEntryTypeRef, that: WDAPointsEntryTypeRef) -> Bool {
        return this.alphabeticalSortOrder() < that.alphabeticalSortOrder()
    }

    var sections: NSMutableIndexSet {
        let indexSet = NSMutableIndexSet()
        indexSet.add(DeviceDetailSections.tableHeader.rawValue)
        if devicePhysicalActivities.count > 0 { indexSet.add(DeviceDetailSections.physicalActivity.rawValue) }
        if deviceHealthMeasurements.count > 0 { indexSet.add(DeviceDetailSections.healthMeasurement.rawValue) }
        if deviceOtherTrackableAcitvities.count > 0 { indexSet.add(DeviceDetailSections.other.rawValue) }
        indexSet.add(DeviceDetailSections.about.rawValue)
        return indexSet
    }

    func sectionCount() -> Int {
        return sections.count
    }

    func hasActivitiesToDisplay() -> Bool {
        let totalActivites = devicePhysicalActivities.count + deviceHealthMeasurements.count + deviceOtherTrackableAcitvities.count

        if totalActivites == 0 {
            return false
        }

        return true
    }

    func numberOfItems(in section: Int) -> Int {
        let integer = sections.integer(at: section)
        guard let sectionIndex = DeviceDetailSections(rawValue:integer) else { return 0 }

        if sectionIndex == DeviceDetailSections.tableHeader {
            return 1
        } else if sectionIndex == DeviceDetailSections.physicalActivity {
            return devicePhysicalActivities.count
        } else if sectionIndex == DeviceDetailSections.healthMeasurement {
            return deviceHealthMeasurements.count
        } else if sectionIndex == DeviceDetailSections.other {
            return deviceOtherTrackableAcitvities.count
        } else if sectionIndex == DeviceDetailSections.about {
            return 1
        }
        return 0
    }

    func item(for indexPath: IndexPath) -> CellItem? {
        let integer = sections.integer(at: indexPath.section)
        guard let sectionIndex = DeviceDetailSections(rawValue:integer) else { return nil }

        if sectionIndex == DeviceDetailSections.tableHeader {
            return nil
        } else if sectionIndex == DeviceDetailSections.physicalActivity {
            return physicalActivity(for: indexPath.row)
        } else if sectionIndex == DeviceDetailSections.healthMeasurement {
            return healthMeasurementItem(for: indexPath.row)
        } else if sectionIndex == DeviceDetailSections.other {
            return otherItem()
        } else if sectionIndex == DeviceDetailSections.about {
            return aboutItem()
        }
        return nil
    }

    func physicalActivity(for index: Int) -> CellItem {
        let activity = devicePhysicalActivities[index]

        switch activity.value {
        case .Heartrate :
            return CellItem(image: UIImage(asset: VIAWellnessDevicesAsset.WellnessDevicesAndApps.wellnessDevicesHeartRate),
                            title: CommonStrings.PhysicalActivity.ItemHeartRateText439,
                            activity: DeviceDetailActivity.HeartRate, activityTypeKey: PointsEntryTypeRef.Heartrate)
        case .Speed:
            return CellItem(image: UIImage(asset: VIAWellnessDevicesAsset.WellnessDevicesAndApps.wellnessDevicesSpeed),
                            title: CommonStrings.Wda.PhysicalActivity.ItemSpeedText441,
                            activity: DeviceDetailActivity.Speed, activityTypeKey: PointsEntryTypeRef.Speed)
        case .Distance :
            return CellItem(image: UIImage(asset: VIAWellnessDevicesAsset.WellnessDevicesAndApps.wellnessDevicesDistance),
                            title: CommonStrings.PhysicalActivity.ItemDistanceText440,
                            activity: DeviceDetailActivity.Distance, activityTypeKey: PointsEntryTypeRef.Distance)
        case .Steps :
            return CellItem(image: UIImage(asset: VIAWellnessDevicesAsset.WellnessDevicesAndApps.wellnessDevicesSteps),
                            title: CommonStrings.PhysicalActivity.ItemStepsText442,
                            activity: DeviceDetailActivity.Steps, activityTypeKey: PointsEntryTypeRef.Steps)
        case .EnergyExpend :
            return CellItem(image: UIImage(asset: VIAWellnessDevicesAsset.WellnessDevicesAndApps.wellnessDevicesCalories),
                            title: CommonStrings.PhysicalActivity.ItemCalorliesBurnedText443,
                            activity: DeviceDetailActivity.CaloriesBurned, activityTypeKey: PointsEntryTypeRef.EnergyExpend)
        default:
            return CellItem(image: UIImage(asset: VIAWellnessDevicesAsset.WellnessDevicesAndApps.wellnessDevicesCalories),
                            title: CommonStrings.PhysicalActivity.ItemActiveCaloriesText444,
                            activity: DeviceDetailActivity.CaloriesBurned, activityTypeKey: PointsEntryTypeRef.EnergyExpend)
        }
    }

    enum HealthMeasurementIndex: Int, EnumCollection {
        case BMI = 0

        static func count() -> Int {
            return self.allValues.count
        }
    }

    func healthMeasurementItem(for index: Int) -> CellItem {
        return CellItem(image: UIImage(asset: VIAWellnessDevicesAsset.WellnessDevicesAndApps.wellnessDevicesWeight),
                        title: CommonStrings.Wda.HealthMeasurements.ItemBmiText496,
                        activity: DeviceDetailActivity.BMI,
                        activityTypeKey: PointsEntryTypeRef.BMI)
    }

    func otherItem() -> CellItem {
        return CellItem(image: UIImage(asset: VIAWellnessDevicesAsset.WellnessDevicesAndApps.wellnessDevicesSleep),
                        title: CommonStrings.Wda.Other.Item.SleepText449,
                        activity: DeviceDetailActivity.Sleep,
                        activityTypeKey: nil)
    }

    func aboutItem() -> CellItem {
        let deviceName = CommonStrings.Wda.HealthApp455

        return CellItem(image: nil, title: CommonStrings.AboutText450(deviceName), activity: DeviceDetailActivity.About, activityTypeKey: nil)
    }

    func aboutContentID() -> String {
        return AppConfigFeature.contentId(for: .AppleHealthAppAboutContent) ?? ""
    }

    func aboutDeviceString() -> String {
        let deviceName = CommonStrings.Wda.HealthApp455
        return CommonStrings.AboutText450(deviceName)
    }

    public func getPointsActivityRef(for physicalActivity: DeviceDetailActivity) -> Int {
        var pointsActivityRef = -1
        switch physicalActivity {
        case .HeartRate:
            pointsActivityRef = PointsEntryTypeRef.Heartrate.rawValue
            break
        case .Distance:
            pointsActivityRef = PointsEntryTypeRef.Distance.rawValue
            break
        case .Steps:
            pointsActivityRef = PointsEntryTypeRef.Steps.rawValue
            break
        case .CaloriesBurned:
            pointsActivityRef = PointsEntryTypeRef.EnergyExpend.rawValue
            break
        default:
            break
        }
        return pointsActivityRef
    }

    // MARK: Linking

    func performLink() {
        VITHealthKitHelper.shared().request(.fitness) { [weak self] granted, error in
            self?.didFinishHealthKitLinking(granted: granted, error: error)
        }
    }

    func didFinishHealthKitLinking(granted: Bool, error: Error?) {
        
        self.delegate?.didFinishHealthKitLinking(error: error)
    }

    func isDeviceLinked() -> Bool {
        switch  VITHealthKitHelper.shared().status {
        case .available:
            return true
        default:
            return false
        }
    }

    // MARK: Delink

    func delinkHealthApp(completion: @escaping (Bool) -> Void) {
        var wasSuccessfullyDelinked = false

        guard isDeviceLinked() else { completion(wasSuccessfullyDelinked); return }

        VITHealthKitHelper.shared().delink()

        if isDeviceLinked() == false {
            wasSuccessfullyDelinked = true
        }

        completion(wasSuccessfullyDelinked)
    }

    // MARK: Syncing

    func lastSynced() -> Date? {
        return VITHealthKitHelper.shared().lastSubmittedDate()
    }

    func performSync() {
        if VITHealthKitHelper.shared().executionStatus == .idle {
            self.delegate?.healthKitWillBeginSync()

            VITHealthKitHelper.shared().generateReadings(withForcedResubmission: true, completion: { [weak self] generatedReadings in
                
                self?.healthKitDidGenerateReadings(generatedReadings)
            })
        }
    }

    func healthKitDidGenerateReadings(_ generatedReadings: [VITGDLReading]?) {
        guard let readings = generatedReadings, readings.count > 0 else {
            debugPrint("No HealthKit readings to submit")
            DispatchQueue.main.async { [weak self] in
                NotificationCenter.default.post(name: .HealthKitSyncDidComplete, object: nil)
                self?.scheduleNotification()
                self?.delegate?.healthKitDidSync()
                self?.delegate?.didFinishHealthKitSyncing()
            }
            return
        }
        
        coreRealm.refresh()
        
        let device = VITHealthKitHelper.deviceForSubmission()
        let model = self.generateUploadModel(device: device, readings: readings)
        let tenantId = coreRealm.getTenantId()
        Wire.Events.upload(tenantId: tenantId, payload: model, completion: { [weak self] error in
            if error != nil {
                self?.handleSyncingErrorOccurred(error: error)
            } else {
                VITHealthKitHelper.shared().readingsSubmittedSuccessfully()
                self?.completeSync()
                self?.delegate?.didFinishHealthKitSyncing()
            }
        })
    }

    public func linkHealthAppProcessEvents(event: EventTypeRef, onStart: (()->Void)?, completion: ((_ error:Error?)-> Void)?) {
        let coreRealm = DataProvider.newRealm()
        let tenantID = coreRealm.getTenantId()
        let partyID = coreRealm.getPartyId()
        var eventMetaDataValues: Array<String>? = [CommonStrings.Wda.HealthApp455]
        
        
        if let tenantId = AppSettings.getAppTenant(), tenantId == .CA{
            eventMetaDataValues?.remove(object: CommonStrings.Wda.HealthApp455)
            eventMetaDataValues?.append(CommonStrings.Wda.Healthapp2900)
        }
       
        
        var processEvents = [ProcessEventsEvent]()
        let processEvent = ProcessEventsEvent(date: Date(),
                                              eventTypeKey: event,
                                              eventSourceTypeKey: .MobileApp,
                                              partyId: partyID,
                                              reportedBy: partyID,
                                              associatedEvents: [ProcessEventsAssociatedEvent](),
                                              eventMetaDataTypeKey: EventMetaDataTypeRef(rawValue: EventMetaDataTypeRef.Manufacturer.rawValue),
                                              eventMetaDataUnitOfMeasure: nil,
                                              eventMetaDataValues: eventMetaDataValues)
        processEvents.append(processEvent)
        
        Wire.Events.processEvents(tenantId: tenantID, events: processEvents) { [weak self] (response, error) in
            completion!(error)
        }
    }

    func completeSync() {
        //VITHealthKitHelper.shared().readingsSubmittedSuccessfully()
        scheduleNotification()
        self.delegate?.healthKitDidSync()
    }
    
    func scheduleNotification() {
        let message = CommonStrings.Notification.HealthappSyncingReminder2305
        VITHealthKitHelper.shared().scheduleLocalReminderNotification(withMessage: message)
    }

    func handleSyncingErrorOccurred(error: Error?) {
        self.delegate?.handleSyncingErrorOccurred(error: error)
        self.delegate?.healthKitDidSync()
    }

    func generateUploadModel(device: VITDevice, readings: [VITGDLReading]) -> VDPGenericUploadViewModel {
        let uploadViewModel = VDPGenericUploadViewModel()
        uploadViewModel.readings = [VDPGenericUploadReadingViewModel]()
        coreRealm.refresh()
        // header
        let partyId = coreRealm.getPartyId()
        uploadViewModel.header?.user?.userIdentifier = "\(partyId)"
        uploadViewModel.header?.user?.identifierType = "PARTY_ID"
        uploadViewModel.header?.uploadDate = Date()
        uploadViewModel.header?.partyId = partyId
        uploadViewModel.header?.sessionId = "\(partyId)-\(Date())"
        uploadViewModel.header?.partnerSystem = "Apple"
        uploadViewModel.header?.partnerSystemSource = "Apple Health"
        uploadViewModel.header?.verified = true
        uploadViewModel.header?.tenantId = coreRealm.getTenantId()
        uploadViewModel.header?.processingType = "REALTIME"

        // device
        uploadViewModel.device?.deviceId = device.deviceID
        uploadViewModel.device?.manufacturer = device.manufacturer
        uploadViewModel.device?.make = device.make
        uploadViewModel.device?.model = device.model

        // readings
        for item in readings {
            let reading = item.uploadModel()
            if reading.workoutIsPopulated {
                uploadViewModel.readings?.append(reading)
            }
        }

        uploadViewModel.header?.uploadCount = uploadViewModel.readings?.count
        return uploadViewModel
    }

    @objc func healthKitDidSync() {
        self.delegate?.healthKitDidSync()
    }

    @objc func displayHealthKitOnboardingView() {
        self.delegate?.displayHealthKitOnboardingView()
    }

}

extension VITGDLReading {

    func uploadModel() -> VDPGenericUploadReadingViewModel {
        // not mapped:
        // speed*
        // location*
        var readingsArray: [[String: String]] = []
        let reading = VDPGenericUploadReadingViewModel()
        let tenantID = AppSettings.getAppTenant()
        
        reading.integrity = "VERIFIED"

        if self.readingClassification == .exercise {
            reading.dataCategory = "FITNESS"
            reading.readingType = self.readingDescription
        } else if self.readingClassification == .steps {
            reading.dataCategory = "ROUTINE"
            reading.readingType = "WALK"
        }

        
        // start / end time
        if let startTime = self.startTime {
            reading.startTime = startTime
        }
        if let endTime = self.endTime {
            reading.endTime = endTime
        }
        
        /* Pass UUID instead of Readings Identifier to sync with Android */
        
        if (VITHealthKitHelper.shared().lastSubmittedDate() != nil){
            //PROD ISSUE 03/29
//            let formatter = DateFormatter()
//            formatter.dateFormat = "yyyy-MM-dd"
            
//            let startTimeString = formatter.string(from: self.startTime)
            let startTimeString = self.formatDate(dateValue: self.startTime)
            //PROD ISSUE 03/29
            
            if var readingValues = UserDefaults.standard.array(forKey: "readings") as? [[String: String]] {
                print(readingValues)
                print("InApp Count:", readingValues.count)
                //FC-24524: Fixed the Prod issue on workout (fitness.) Applying to ALL tenants
                if reading.dataCategory == "FITNESS" {
                        reading.partnerReadingId = self.identifier
                } else {
                    for index in 0..<readingValues.count {
                        
                        if startTimeString == readingValues[index]["startTime"]{
                            reading.partnerReadingId = readingValues[index]["readingId"]
                        }
                    }
                    // FC-20985 Blank Partner Reading ID Passed on Succeeding Sync
                    if (reading.partnerReadingId == nil) {
                        let id = UUID().uuidString
                        reading.partnerReadingId = id
                        readingsArray = readingValues
                        readingsArray.append(["startTime": startTimeString, "readingId": id])
                        UserDefaults.standard.set(readingsArray, forKey: "readings")
                    }
                }
            }
        }else{
            //PROD ISSUE 03/29
//            let formatter = DateFormatter()
//            formatter.dateFormat = "yyyy-MM-dd"
//            let startTimeString = formatter.string(from: self.startTime)
            let startTimeString = self.formatDate(dateValue: self.startTime)
            //PROD ISSUE 03/29
            
            //FC-24524: Fixed the Prod issue on workout (fitness.) Applying to ALL tenants
            if reading.dataCategory == "FITNESS" {
                reading.partnerReadingId = self.identifier
            } else {
                let id = UUID().uuidString
                reading.partnerReadingId = id
                
                if UserDefaults.standard.object(forKey: "readings") != nil {
                    let readingValue = (UserDefaults.standard.array(forKey: "readings") as? [[String: String]])!
                    print(readingValue)
                    for index in 0..<readingValue.count {
                        let startTime = readingValue[index]["startTime"]
                        let reading_id = readingValue[index]["readingId"]
                        readingsArray.append(["startTime": startTime!, "readingId": reading_id!])
                        //break
                    }
                    
                    readingsArray.append(["startTime": startTimeString, "readingId": id])
                    print(readingsArray)
                    debugPrint(readingsArray.count)
                    UserDefaults.standard.set(readingsArray, forKey: "readings")
                
                }else{
                    readingsArray.append(["startTime": startTimeString, "readingId": id])
                    
                    debugPrint(readingsArray.count)
                    
                    UserDefaults.standard.set(readingsArray, forKey: "readings")
                }
            }
        }
        
        if let description = self.readingDescription {
            reading.description = description
        }

        // duration
        if let duration = self.duration {
//            let model = VDPGenericValueUnitofMeasurementViewModel(value: Int(duration), unitOfMeasurement: "SECONDS") //TODO: temp value
//            reading.addDuration(model)
        }

        // steps
        if let totalSteps = self.totalSteps {
            reading.addToWorkout()?.totalSteps = totalSteps.intValue
        }

        // distance
        if let distance = self.distance {
            let model = VDPGenericValueUnitofMeasurementViewModel(value: Int(distance), unitOfMeasurement: "METERS")
            reading.addToWorkout()?.addDistance(model)
        }

        // energy
        if let energy = self.energyExpenditureValue {
            let model = VDPGenericValueUnitofMeasurementViewModel(value: Int(energy), unitOfMeasurement: "KILOCALORIES")
            reading.addToWorkout()?.addEnergy(model)
        }

        // heart rate
        if let avg = self.heartRateAverage, let max = self.heartRateMaximum, let min = self.heartRateMinimum {
            let avgMinMax = VDPGenericUploadReadingWorkoutHeartRateAvgMinMaxViewModel(average: Int(avg),
                                                                                      maximum: Int(max),
                                                                                      minimum: Int(min))
            let heartRate = VDPGenericUploadReadingWorkoutHeartRateViewModel(avgMinMax: avgMinMax)
            reading.addToWorkout()?.addHeartRate(heartRate)
        }

        return reading
    }

}

//PROD ISSUE 03/29
extension VITGDLReading {
    func formatDate(dateValue: Date) -> String{
        let formatter = DateFormatter()
        
        if AppSettings.getAppTenant() == .SLI {
            formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"
            
            formatter.locale = Locale(identifier: DeviceLocale.toString())
            
            if let timezone = Bundle.main.object(forInfoDictionaryKey: "VIATimezone") as? String{
                formatter.timeZone = TimeZone(identifier: timezone)
            }
        } else {
            formatter.dateFormat = "yyyy-MM-dd"
        }
        
        return formatter.string(from: dateValue)
    }
}
//PROD ISSUE 03/29
