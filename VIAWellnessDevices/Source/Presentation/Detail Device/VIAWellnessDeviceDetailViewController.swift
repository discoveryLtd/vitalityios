import SafariServices

import VIAUIKit
import VIAUtilities
import VitalityKit
import VIACommon

import TTTAttributedLabel

struct WellnessDevicesSegueInfo {
    let title: String
    let articleId: String
}

class VIAWellnessDeviceDetailViewController: VIATableViewController, SFSafariViewControllerDelegate, TTTAttributedLabelDelegate, ImproveYourHealthTintable {
    
    static let linkDevicesHost = VIAURLScheme.Feature.wellnessDevicesApps.rawValue
    static let linkDevicesQueryItemName = "success"
    static let linkDevicesQueryItemValue = "TRUE"
    static let SBDevicesQueryItemName = "devicepartner"
    static let SBDevicesQueryItemValue = "softbank"
    static let polarDevicesQueryItemValue = "polarflow"
    
    let viewModel: VIAWellnessDeviceDetailViewModel = VIAWellnessDeviceDetailViewModel()
    let coreRealm = DataProvider.newRealm()
    weak var dataPrivacyViewController: DeviceAndAppsDataPrivacyPolicyViewController?
    var didPerformLinkAction = false
    var didPerformDelinkAction = false
    var isSoftBank = false
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        isSoftBank = self.viewModel.deviceName?.lowercased() == VIAWellnessDeviceDetailViewController.SBDevicesQueryItemValue.lowercased()
        if isSoftBank {
            self.title = CommonStrings.Wda.Device.TitleSoftbank2418
        } else {
            self.title = viewModel.deviceName
        }
        configureTableView()
        configureAppearance()
        registerForNotifications()
        viewModel.configureDeviceActivities()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.configureAppearance()
    }
    
    public override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        hideBackButtonTitle()
    }
    
    // MARK: Setup
    func configureTableView() {
        self.tableView.register(VIATableViewSectionHeaderFooterView.nib(), forHeaderFooterViewReuseIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier)
        self.tableView.register(VIAWellnessDeviceHeaderFooterView.nib(), forHeaderFooterViewReuseIdentifier: VIAWellnessDeviceHeaderFooterView.defaultReuseIdentifier)
        self.tableView.register(VIATableViewCell.nib(), forCellReuseIdentifier: VIATableViewCell.defaultReuseIdentifier)
        self.tableView.register(VIAWellnessDeviceDetailHeaderViewCell.nib(), forCellReuseIdentifier: VIAWellnessDeviceDetailHeaderViewCell.defaultReuseIdentifier)
        self.tableView.estimatedRowHeight = 75
        self.tableView.estimatedSectionHeaderHeight = 25
        self.tableView.estimatedSectionFooterHeight = 25
        self.tableView.sectionHeaderHeight = UITableViewAutomaticDimension
        self.tableView.sectionFooterHeight = UITableViewAutomaticDimension
        self.tableView.rowHeight = UITableViewAutomaticDimension
    }
    
    func configureAppearance() {
        navigationController?.makeNavigationBarTransparent()
        tableView.backgroundColor = UIColor.tableViewBackground()
    }
    
    func updateViewWithLatestDataAfterLinkOrDelink() {
        self.viewModel.configureDeviceActivities()
        self.hideHUDFromView(view: self.view)
        if didPerformLinkAction {
            didPerformLinkAction = false
            showLinkedSuccessfullyAlert()
        }
        if didPerformDelinkAction {
            didPerformDelinkAction = false
            showDelinkedSuccessfullyAlert()
        }
        self.tableView.reloadData()
    }
    
    func registerForNotifications() {
        NotificationCenter.default.addObserver(forName: .VIAApplicationDidOpenURL, object: nil, queue: nil) { [weak self] notification in
            guard let notificationInfo = notification.object as? NotificationInfo else { return }
            guard let components = notificationInfo.urlComponents else { return }
            guard let host = components.host?.lowercased() else { return }
            guard let queryItems = components.queryItems else { return }
            var urlQueryItemSuccessValue: String?
            for item in queryItems {
                if item.name.lowercased() == VIAWellnessDeviceDetailViewController.linkDevicesQueryItemName.lowercased() {
                    urlQueryItemSuccessValue = item.value?.lowercased()
                }else if item.name.lowercased() == VIAWellnessDeviceDetailViewController.SBDevicesQueryItemName.lowercased(){
                    urlQueryItemSuccessValue = item.value?.lowercased()
                }
            }
            guard let successValue = urlQueryItemSuccessValue else { return }
            let linkDevicesHostValue = VIAWellnessDeviceDetailViewController.linkDevicesHost.lowercased()
            let linkDevicesQueryItemValue = VIAWellnessDeviceDetailViewController.linkDevicesQueryItemValue.lowercased()
            
            // Create a separate 'success' handler specific per market.
            let tenantId = AppSettings.getAppTenant()
            if tenantId == .SLI {
                self?.processSuccessResponseValueForSLI(host: host,
                                                        successValue: successValue,
                                                        linkDevicesHostValue: linkDevicesHostValue,
                                                        linkDevicesQueryItemValue: linkDevicesQueryItemValue,
                                                        queryItems: queryItems)
            } else if tenantId == .EC {
                self?.processSuccessResponseValueforEC(host: host,
                                                       successValue: successValue,
                                                       linkDevicesHostValue: linkDevicesHostValue,
                                                       linkDevicesQueryItemValue: linkDevicesQueryItemValue)
            } else {
                self?.processSuccessResponseValue(host: host,
                                                  successValue: successValue,
                                                  linkDevicesHostValue: linkDevicesHostValue,
                                                  linkDevicesQueryItemValue: linkDevicesQueryItemValue,
                                                  queryItems: queryItems)
            }
        }
        
        NotificationCenter.default.addObserver(forName: .VIAWDADidFetchDevices, object: nil, queue: OperationQueue.main) { [weak self] notification in
            self?.updateViewWithLatestDataAfterLinkOrDelink()
        }
    }
    
    func processSuccessResponseValueForSLI(host: String, successValue: String, linkDevicesHostValue: String, linkDevicesQueryItemValue: String, queryItems: [URLQueryItem]) {
        var isSuccessValue: Bool = true
        /* Check if Selected Device is PolarFlow */
        if let selectedDeviceName = self.viewModel.deviceName, selectedDeviceName.lowercased() == VIAWellnessDeviceDetailViewController.polarDevicesQueryItemValue {
            isSuccessValue = NSString(string: successValue).boolValue
        }
        
        if isSuccessValue {
            self.callAPIProcessEvent(eventType: EventTypeRef.DeviceLinking)
            
            if host == linkDevicesHostValue && successValue == linkDevicesQueryItemValue {
                self.requestLatestDeviceData(deviceLinked: true)
            }
                /* If component is for devicepartner */
            else if queryItems.first?.name.lowercased() == VIAWellnessDeviceDetailViewController.SBDevicesQueryItemName.lowercased(){
                /* Sample token:
                 * ?devicePartner=softbank?token=66eBLIi9Bkr5P2bBuFajVyQwnZrZgXcUqPYfaS3tHPZDw1q0whTls7Iy8HaqslGr */
                /* We need to check the subcomponent if there's a item named "token" */
                guard let subcomponent = URLComponents(string: successValue) else {  return }
                guard let token = subcomponent.getValue(withParam: "token") else { return }
                self.callAPIToLinkSoftBankDevice(thisToken: token)
            }
            self.dismiss(animated: true, completion: nil)
        }
        else {
            self.dismiss(animated: true, completion: nil)
            self.showLinkedErrorAlert()
        }
    }
    
    func processSuccessResponseValueforEC(host: String, successValue: String, linkDevicesHostValue: String, linkDevicesQueryItemValue: String) {
        self.callAPIProcessEvent(eventType: EventTypeRef.DeviceLinking)
        
        if host == linkDevicesHostValue && successValue == linkDevicesQueryItemValue {
            self.requestLatestDeviceData(deviceLinked: true)
        } else {
            self.showLinkedErrorAlert()
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    func processSuccessResponseValue(host: String, successValue: String, linkDevicesHostValue: String, linkDevicesQueryItemValue: String, queryItems: [URLQueryItem]) {
        /**
         * FC-21717 - Removed the "if" since processEvents with eventType = 43 should be called after linking
         to add points in the points tab
         **/
        //if tenantId == .SLI || tenantId == .UKE{
        self.callAPIProcessEvent(eventType: EventTypeRef.DeviceLinking)
        //}
        if host == linkDevicesHostValue && successValue == linkDevicesQueryItemValue {
            self.requestLatestDeviceData(deviceLinked: true)
        }
            /* If component is for devicepartner */
        else if queryItems.first?.name.lowercased() == VIAWellnessDeviceDetailViewController.SBDevicesQueryItemName.lowercased(){
            /* Sample token:
             * ?devicePartner=softbank?token=66eBLIi9Bkr5P2bBuFajVyQwnZrZgXcUqPYfaS3tHPZDw1q0whTls7Iy8HaqslGr */
            /* We need to check the subcomponent if there's a item named "token" */
            guard let subcomponent = URLComponents(string: successValue) else {  return }
            guard let token = subcomponent.getValue(withParam: "token") else { return }
            self.callAPIToLinkSoftBankDevice(thisToken: token)
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    func callAPIToLinkSoftBankDevice(thisToken token: String) {
        //TODO: Do something with this token
        self.showHUDOnView(view: self.view)
        self.viewModel.linkSoftBankDevice(refreshToken: token, token: token, userId: token, completion: { (error) in
            self.hideHUDFromView(view: self.view)
            if error != nil {
                self.showLinkNowNetworkErrorAlert()
                return
            }
            self.requestLatestDeviceData(deviceLinked: true)
        })
    }
    
    func callAPIProcessEvent(eventType: EventTypeRef) {
        self.viewModel.linkDeviceProcessEvents(event: eventType, onStart: { [weak self] in
            self?.showFullScreenHUD()
            }, completion: { (error) in
                DispatchQueue.main.async {
                    self.hideFullScreenHUD()
                    // add error checking
                }
        })
    }
    
    // MARK: UITableView delegate
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == DeviceDetailSections.tableHeader.rawValue {
            return CGFloat.leastNormalMagnitude
        }
        
        return UITableViewAutomaticDimension
    }
    
    public override func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.sectionCount()
    }
    
    public override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.numberOfItems(in: section)
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if section == DeviceDetailSections.tableHeader.rawValue && viewModel.hasActivitiesToDisplay() {
            return configurePointsEarningMetricsFooter()
        }
        return nil
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let integer = viewModel.sections.integer(at: section)
        guard let sectionIndex = DeviceDetailSections(rawValue:integer) else { return nil }
        
        if sectionIndex == DeviceDetailSections.physicalActivity {
            if let title = DeviceDetailSections.physicalActivity.title() {
                return configureSectionHeader(title: title)
            }
        } else if sectionIndex == DeviceDetailSections.healthMeasurement {
            if let title = isSoftBank ? CommonStrings.Wda.HealthMeasurements.SectionHeaderSoftbank445 : DeviceDetailSections.healthMeasurement.title() {
                return configureSectionHeader(title: title)
            }
        } else if sectionIndex == DeviceDetailSections.other {
            if let title = DeviceDetailSections.other.title() {
                return configureSectionHeader(title: title)
            }
        }
        return nil
    }
    
    func configureSectionHeader(title: String) -> UITableViewHeaderFooterView {
        let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier) as! VIATableViewSectionHeaderFooterView
        let labelText = title
        view.configureWithExtraSpacing(labelText: labelText, extraTopSpacing: 15, extraBottomSpacing: 10)
        view.set(font: .title2Font())
        view.set(textColor: .night())
        return view
    }
    
    func configurePointsEarningMetricsFooter() -> UITableViewHeaderFooterView {
        let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: VIAWellnessDeviceHeaderFooterView.defaultReuseIdentifier) as! VIAWellnessDeviceHeaderFooterView
        
        if isSoftBank {
            view.configureSoftbankHeaderFooterView()
        }
        view.configureForWellnessDevice()
        if let url = viewModel.partnerWebsiteUrl() {
            view.configureAttributedLabelWith(url: url, delegate: self, isSoftBank: isSoftBank)
        }
        return view
    }
    
    public override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == DeviceDetailSections.tableHeader.rawValue {
            let cell = tableView.dequeueReusableCell(withIdentifier: VIAWellnessDeviceDetailHeaderViewCell.defaultReuseIdentifier) as! VIAWellnessDeviceDetailHeaderViewCell
            let lastSynced = viewModel.lastSynced()
            let isLinked = viewModel.isDeviceLinked()
            cell.configureMessageLabel(isSoftbank: isSoftBank)
            cell.configureForWellnessDevice(lastSynced: lastSynced, isLinked: isLinked)
            configureHeaderButtonActions(cell: cell, isLinked: isLinked)
            cell.selectionStyle = .none
            return cell
        }
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: VIATableViewCell.defaultReuseIdentifier) as? VIATableViewCell else {
            return UITableViewCell()
        }
        if let cellContent = self.viewModel.item(for: indexPath) {
            cell.labelText = cellContent.title
            cell.cellImage = cellContent.image
        }
        cell.accessoryType = .disclosureIndicator
        cell.separatorInset = UIEdgeInsets(top: 0, left: 55, bottom: 0, right: 0)
        return cell
    }
    
    func configureHeaderButtonActions(cell: VIAWellnessDeviceDetailHeaderViewCell, isLinked: Bool) {
        if isLinked {
            cell.didTapStepsTroubleButton = { [weak self] in
                self?.havingTroubleTapped()
            }
        } else {
            cell.didTapStepsTroubleButton = {
                [weak self] in
                self?.stepsToLinkTapped()
            }
        }
        
        cell.didTapLinkSyncButton = { [weak self] in
            self?.linkNowTapped()
        }
        cell.didTapDelinkButton = { [weak self] in
            self?.delinkTapped()
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let itemForRow = self.viewModel.item(for: indexPath) {
            if itemForRow.activity == DeviceDetailActivity.About {
                self.aboutTapped()
                return
            } else {
                switch indexPath.section {
                case DeviceDetailSections.physicalActivity.rawValue:
                    self.performSegue(withIdentifier: StoryboardSegue.WellnessDevicesApps.showPointsEarningPotential.rawValue, sender: itemForRow)
                default:
                    break
                }
            }
        }
        
        self.tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func item(for index: IndexPath) {
        switch index.section {
        case DeviceDetailSections.tableHeader.rawValue:
            break
        case DeviceDetailSections.physicalActivity.rawValue:
            break
        case DeviceDetailSections.healthMeasurement.rawValue:
            break
        case DeviceDetailSections.other.rawValue:
            break
        case DeviceDetailSections.about.rawValue:
            break
        default:
            break
        }
    }
    
    // MARK: Navigation
    
    @IBAction public func unwindToDeviceDetail(segue: UIStoryboardSegue) {
        debugPrint("unwindToDeviceDetail")
    }
    
    func linkNowTapped() {
        if let redirectUrl = viewModel.deviceLinkingRedirectUrl {
            if VitalityProductFeature.isEnabled(.WDADSConsent) && VIAApplicableFeatures.default.shouldDisplayWellnessDevicesAndDataSharingConsent!{
                self.showWDADataSharingConsentViewController()
            } else {
                self.performDeviceLinking(linkingRedirectUrl: redirectUrl)
            }
        } else {
            /* Check if Selected Device is Softbank */
            if let selectedDeviceName = self.viewModel.deviceName, selectedDeviceName.lowercased() == VIAWellnessDeviceDetailViewController.SBDevicesQueryItemValue {
                /* Determine if Softbank URL is Empty or not */
                guard let url = self.viewModel.getPartnerURL() else { return }
                if VitalityProductFeature.isEnabled(.WDADSConsent) && VIAApplicableFeatures.default.shouldDisplayWellnessDevicesAndDataSharingConsent!{
                    self.showWDADataSharingConsentViewController()
                } else {
                    self.performDeviceLinking(linkingRedirectUrl: url)
                }
            } else {
                getLinkingRedirectUrl { [weak self] error in
                    guard error == nil else {return}
                    guard let redirectUrl = self?.viewModel.deviceLinkingRedirectUrl else {return}
                    if VitalityProductFeature.isEnabled(.WDADSConsent) && VIAApplicableFeatures.default.shouldDisplayWellnessDevicesAndDataSharingConsent!{
                        self?.showWDADataSharingConsentViewController()
                    } else {
                        self?.performDeviceLinking(linkingRedirectUrl: redirectUrl)
                    }
                }
            }
        }
    }
    
    func performDeviceLinking(linkingRedirectUrl: URL) {
        if let parentViewController = dataPrivacyViewController {
            presentModally(url: linkingRedirectUrl, parentViewController: parentViewController)
        } else {
            presentModally(url: linkingRedirectUrl, parentViewController: self)
        }
    }
    
    func delinkTapped() {
        showDelinkWarningAlert()
    }
    
    func performDeviceDelinking() {
        showHUDOnView(view: self.view)
        viewModel.delinkDevice { [weak self] error in
            self?.hideHUDFromView(view: self?.view)
            if error != nil {
                self?.showDelinkNetworkErrorAlert()
                return
            }
            if let tenantId = AppSettings.getAppTenant(), tenantId == .SLI {
                self?.callAPIProcessEvent(eventType: EventTypeRef.DeviceDelinking)
            }
            self?.requestLatestDeviceData(deviceDelinked: true)
            
            self?.tableView.reloadData()
        }
    }
    
    func showWDADataSharingConsentViewController() {
        self.performSegue(withIdentifier:"showDataConsent", sender: nil)
    }
    
    func stepsToLinkTapped() {
        let segueInfo = WellnessDevicesSegueInfo(title: CommonStrings.StepsToLinkButtonText471, articleId: viewModel.stepsContentID())
        self.performSegue(withIdentifier: "showArticle", sender: segueInfo)
    }
    
    func aboutTapped() {
        let segueInfo = WellnessDevicesSegueInfo(title: viewModel.aboutDeviceString(device: isSoftBank ? CommonStrings.Wda.Device.TitleSoftbank2418 : viewModel.deviceName), articleId: viewModel.aboutContentID())
        self.performSegue(withIdentifier: "showArticle", sender: segueInfo)
    }
    
    func havingTroubleTapped() {
        self.performSegue(withIdentifier: "segueShowHelp", sender: nil)
    }
    
    func showLinkedSuccessfullyAlert() {
        showHud(on: self.view.window, with: VIAUIKitAsset.checkMarkCircle.image, display: CommonStrings.DeviceLinking.SuccessfullyLinked454)
    }
    
    func showLinkedErrorAlert() {
        let partner = viewModel.selectedDeviceDetails?.partner
        let alertController = UIAlertController(title: CommonStrings.Wda.DeviceDetail.SyncFailedDialogTitle512, message: CommonStrings.Wda.DeviceDetail.SyncFailedDialogMessage513((partner?.device)!), preferredStyle: .alert)
        let tryAgainAction = UIAlertAction(title: CommonStrings.TryAgainButtonTitle43, style: .default, handler: { [weak self] action in
            self?.linkNowTapped()
        })
        let cancelAction = UIAlertAction(title: CommonStrings.CancelButtonTitle24, style: .cancel, handler: nil)
        
        alertController.addAction(tryAgainAction)
        alertController.addAction(cancelAction)
        
        present(alertController, animated: true, completion: nil)
    }
    
    func showDelinkedSuccessfullyAlert() {
        showHud(on: self.view.window, with: VIAUIKitAsset.checkMarkCircle.image, display: CommonStrings.DeviceLinking.SuccessfullyDelinked470)
    }
    
    // MARK: navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "showDataConsent" {
            if let viewController = segue.destination as? DeviceAndAppsDataPrivacyPolicyViewController {
                dataPrivacyViewController = viewController
                dataPrivacyViewController?.deviceName = viewModel.deviceName!
                dataPrivacyViewController?.completion = { [weak self] in
                    /* Check if Selected Device is Softbank */
                    if let selectedDeviceName = self?.viewModel.deviceName, selectedDeviceName.lowercased() == VIAWellnessDeviceDetailViewController.SBDevicesQueryItemValue.lowercased() {
                        /* Determine if Softbank URL is Empty or not */
                        guard let url = self?.viewModel.getPartnerURL() else { return }
                        self?.performDeviceLinking(linkingRedirectUrl: url)
                    } else {
                        guard let redirectUrl = self?.viewModel.deviceLinkingRedirectUrl else {return}
                        self?.performDeviceLinking(linkingRedirectUrl: redirectUrl)
                    }
                }
            }
        }
        
        if let webContentViewController = segue.destination as? VIAWebContentViewController, let segueInfo = sender as? WellnessDevicesSegueInfo {
            
            let webContentViewModel = VIAWebContentViewModel(articleId: segueInfo.articleId)
            webContentViewController.viewModel = webContentViewModel
            webContentViewController.title = segueInfo.title
        }
        
        if segue.identifier == StoryboardSegue.WellnessDevicesApps.showPointsEarningPotential.rawValue {
            guard let physicalActivity = sender as? VIAWellnessDeviceDetailViewModel.CellItem else {
                print("No event for this activity")
                return
            }
            guard let destination = segue.destination as? VIAPointsEarningPotentialWithTiersTableViewController else {
                print("Incorrect view controller as destination")
                return
            }

            destination.title = physicalActivity.title
            destination.isSoftbank = isSoftBank
            destination.activityImage = physicalActivity.image
            if let physicalActivityTypeKey = physicalActivity.activityTypeKey?.rawValue {
                destination.pointsActivityTypeKey = PointsEntryTypeRef(rawValue: physicalActivityTypeKey) ?? .Unknown
                destination.viewModel = VIAWellnessDevicesPointsEarningPotentialViewModel(earningActivityKey: physicalActivityTypeKey)
            }
        }
    }
    // MARK: Safari Delegate
    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        // user tapped done, we can't know if the process finished, assumed they bailed early.
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: Networking
    
    func getLinkingRedirectUrl(completion: @escaping (Error?) -> Void) {
        showHUDOnView(view: self.view)
        viewModel.linkDevice { [weak self] error in
            self?.hideHUDFromView(view: self?.view)
            if error != nil {
                self?.showLinkNowNetworkErrorAlert()
                completion(error)
                return
            }
            completion(nil)
        }
    }
    
    func requestLatestDeviceData(deviceLinked: Bool = false, deviceDelinked: Bool = false) {
        // post notification
        NotificationCenter.default.post(name: .VIAWDANeedToFetchUpdatedDevices, object: nil)
        showHUDOnView(view: self.view)
        if deviceLinked {
            didPerformLinkAction = true
        }
        if deviceDelinked {
            didPerformDelinkAction = true
        }
    }
    
    public func presentModally(url: URL, parentViewController: UIViewController) {
        let safariVC = SFSafariViewController(url: url)
        if UIDevice.current.userInterfaceIdiom == .phone {
            safariVC.modalPresentationStyle = .popover
        } else {
            safariVC.modalPresentationStyle = .fullScreen
        }
        safariVC.delegate = self
        safariVC.preferredControlTintColor = UIColor.currentGlobalTintColor()
        parentViewController.present(safariVC, animated: true, completion: nil)
    }
    
    func showNetworkErrorAlert() {
        let alertController = UIAlertController(title: CommonStrings.ConnectivityErrorAlertTitle44, message: CommonStrings.ConnectivityErrorAlertMessage45, preferredStyle: .alert)
        let tryAgainAction = UIAlertAction(title: CommonStrings.TryAgainButtonTitle43, style: .default, handler: { [weak self] action in
            self?.requestLatestDeviceData()
        })
        let cancelAction = UIAlertAction(title: CommonStrings.CancelButtonTitle24, style: .cancel, handler: nil)
        
        alertController.addAction(tryAgainAction)
        alertController.addAction(cancelAction)
        
        present(alertController, animated: true, completion: nil)
    }
    
    func showLinkNowNetworkErrorAlert() {
        let alertController = UIAlertController(title: CommonStrings.ConnectivityErrorAlertTitle44, message: CommonStrings.ConnectivityErrorAlertMessage45, preferredStyle: .alert)
        let tryAgainAction = UIAlertAction(title: CommonStrings.TryAgainButtonTitle43, style: .default, handler: { [weak self] action in
            self?.linkNowTapped()
        })
        let cancelAction = UIAlertAction(title: CommonStrings.CancelButtonTitle24, style: .cancel, handler: nil)
        
        alertController.addAction(tryAgainAction)
        alertController.addAction(cancelAction)
        
        present(alertController, animated: true, completion: nil)
    }
    
    func showDelinkNetworkErrorAlert() {
        let deviceName = isSoftBank ? CommonStrings.Wda.DeviceDetail.DelinkDialogDeviceSoftbank467 : viewModel.deviceName ?? CommonStrings.Wda.DeviceDetail.DelinkDialogDevice467
        let alertController = UIAlertController(title: CommonStrings.Wda.DeviceDetail.DelinkFailedDialogTitle468, message: CommonStrings.DelinkFailedDialogDescription469(deviceName), preferredStyle: .alert)
        let tryAgainAction = UIAlertAction(title: CommonStrings.TryAgainButtonTitle43, style: .default, handler: { [weak self] action in
            self?.performDeviceDelinking()
        })
        let cancelAction = UIAlertAction(title: CommonStrings.CancelButtonTitle24, style: .cancel, handler: nil)
        
        alertController.addAction(tryAgainAction)
        alertController.addAction(cancelAction)
        
        present(alertController, animated: true, completion: nil)
    }
    
    func showDelinkWarningAlert() {
        let deviceName = isSoftBank ? CommonStrings.Wda.DeviceDetail.DelinkDialogDeviceSoftbank467 : viewModel.deviceName ?? CommonStrings.Wda.DeviceDetail.DelinkDialogDevice467
        let alertController = UIAlertController(title: isSoftBank ? CommonStrings.Wda.DelinkDialogTitleSoftbank464 : CommonStrings.Wda.DelinkDialogTitle464, message: CommonStrings.DelinkDialogDescription465(deviceName), preferredStyle: .alert)
        let okAction = UIAlertAction(title: isSoftBank ? CommonStrings.Wda.DeviceDetail.DelinkButtonTextSoftbank462 : CommonStrings.Wda.DeviceDetail.DelinkButtonText462 , style: .destructive, handler: { [weak self] action in
            self?.performDeviceDelinking()
        })
        let cancelAction = UIAlertAction(title: CommonStrings.CancelButtonTitle24, style: .cancel, handler: nil)
        
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        
        present(alertController, animated: true, completion: nil)
    }
    
    // MARK: TTTAttributedLabel delegate
    func attributedLabel(_ label: TTTAttributedLabel!, didSelectLinkWith url: URL!) {
        presentModally(url: url, parentViewController: self)
    }
}
