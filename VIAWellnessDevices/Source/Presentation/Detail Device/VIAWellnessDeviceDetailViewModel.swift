import VIAUtilities
import VitalityKit

import RealmSwift

// MARK: UITableView datasource

enum DeviceDetailSections: Int, EnumCollection, Equatable {
    case tableHeader = 0
    case physicalActivity = 1
    case healthMeasurement = 2
    case other = 3
    case about = 4
    
    func title() -> String? {
        switch self {
        case .physicalActivity :
            return CommonStrings.Wda.PhysicalActivity.SectionHeader438
        case .healthMeasurement :
            return CommonStrings.Wda.HealthMeasurements.SectionHeader445
        case .other:
            return CommonStrings.Wda.DeviceDetail.Other.SectionHeader448
        default :
            return nil
        }
    }
    static func count() -> Int {
        return self.allValues.count
    }
}

enum DeviceDetailActivity: String, EnumCollection {
    case HeartRate
    case Distance
    case Steps
    case Speed
    case CaloriesBurned
    case BMI
    case Sleep
    case About
}

class VIAWellnessDeviceDetailViewModel {
    
    var wdaRealm = DataProvider.newWDARealm()
    
    struct CellItem {
        let image: UIImage?
        let title: String
        let activity: DeviceDetailActivity
        let activityTypeKey: PointsEntryTypeRef?
    }
    
    struct Constants {
        struct DeviceLinkingRequestConstants {
            static let userIdentifierTypePartyId = "PARTY_ID"
        }
    }
    
    var selectedDeviceDetails: WDAMarket? {
        get {
            return self.getDeviceDetails()
        }
    }
    
    func aboutContentID() -> String {
        return selectedDeviceDetails?.assets?.aboutPartnerContentId ?? ""
    }
    
    func stepsContentID() -> String {
        return selectedDeviceDetails?.assets?.stepsToLinkContentId ?? ""
    }
    
    var devicePhysicalActivities: [WDAPointsEntryTypeRef] = []
    var deviceHealthMeasurements: [WDAPointsEntryTypeRef] = []
    var deviceOtherTrackableAcitvities: [WDAPointsEntryTypeRef] = []
    
    var deviceName: String?
    var deviceLinkingRedirectUrl: URL?
    
    func numberOfItems(in section: Int) -> Int {
        let integer = sections.integer(at: section)
        guard let sectionIndex = DeviceDetailSections(rawValue:integer) else { return 0 }
        
        if sectionIndex == DeviceDetailSections.tableHeader {
            return 1
        } else if sectionIndex == DeviceDetailSections.physicalActivity {
            return devicePhysicalActivities.count
        } else if sectionIndex == DeviceDetailSections.healthMeasurement {
            return deviceHealthMeasurements.count
        } else if sectionIndex == DeviceDetailSections.other {
            return deviceOtherTrackableAcitvities.count
        } else if sectionIndex == DeviceDetailSections.about {
            return 1
        }
        return 0
    }
    
    func getDeviceDetails() -> WDAMarket? {
        wdaRealm.refresh()
        if let device = deviceName {
            return wdaRealm.wdaMarketFor(device: device)
        }
        return nil
    }
    
    func getPartnerURL() -> URL? {
        guard let partner = selectedDeviceDetails?.partner else { return nil }
        guard let partnerLink = partner.partnerLink else { return nil }
        guard let partnerURL = URL(string: partnerLink.url.replacingOccurrences(of: " ", with: "%20")) else { return nil }
        return partnerURL
    }
    
    func configureDeviceActivities() {
        devicePhysicalActivities.removeAll()
        deviceHealthMeasurements.removeAll()
        deviceOtherTrackableAcitvities.removeAll()
        wdaRealm.refresh()
        
        guard let device = deviceName else { return }
        guard let deviceActivities = wdaRealm.availableDeviceActivitiesFor(device: device).first else { return }
        let deviceActivityTypeKeys = deviceActivities.availableActivitiesTypeRefs
        
        for deviceActivityTypeKey in deviceActivityTypeKeys {
            let pointsEntryTypeRef = WDAPointsEntryTypeRef()
            pointsEntryTypeRef.value = deviceActivityTypeKey.value
            switch pointsEntryTypeRef.value {
            case .Heartrate :
                devicePhysicalActivities.append(pointsEntryTypeRef)
            case .Distance :
                devicePhysicalActivities.append(pointsEntryTypeRef)
            case .Speed :
                devicePhysicalActivities.append(pointsEntryTypeRef)
            case .Steps :
                devicePhysicalActivities.append(pointsEntryTypeRef)
            case .EnergyExpend :
                devicePhysicalActivities.append(pointsEntryTypeRef)
            case .BMI :
                deviceHealthMeasurements.append(pointsEntryTypeRef)
            default:
                deviceOtherTrackableAcitvities.append(pointsEntryTypeRef)
            }
        }
        devicePhysicalActivities.sort(by: sortingAlphabetically)
        deviceHealthMeasurements.sort(by: sortingAlphabetically)
        deviceOtherTrackableAcitvities.sort(by: sortingAlphabetically)
        
    }
    
    func sortingAlphabetically(this: WDAPointsEntryTypeRef, that: WDAPointsEntryTypeRef) -> Bool {
        return this.alphabeticalSortOrder() < that.alphabeticalSortOrder()
    }
    
    var sections: NSMutableIndexSet {
        let indexSet = NSMutableIndexSet()
        indexSet.add(DeviceDetailSections.tableHeader.rawValue)
        if devicePhysicalActivities.count > 0 { indexSet.add(DeviceDetailSections.physicalActivity.rawValue) }
        if deviceHealthMeasurements.count > 0 { indexSet.add(DeviceDetailSections.healthMeasurement.rawValue) }
        if deviceOtherTrackableAcitvities.count > 0 { indexSet.add(DeviceDetailSections.other.rawValue) }
        indexSet.add(DeviceDetailSections.about.rawValue)
        return indexSet
    }
    
    func sectionCount() -> Int {
        return sections.count
    }
    
    func hasActivitiesToDisplay() -> Bool {
        let totalActivites = devicePhysicalActivities.count + deviceHealthMeasurements.count + deviceOtherTrackableAcitvities.count
        
        if totalActivites == 0 {
            return false
        }
        
        return true
    }
    
    func isDeviceLinked() -> Bool {
        guard let linkedStatus = selectedDeviceDetails?.partner?.partnerLinkedStatus else { return false }
        switch  linkedStatus {
        case .Active, .Pending:
            return true
        default:
            return false
        }
    }
    
    func lastSynced() -> Date? {
        return selectedDeviceDetails?.partner?.partnerLastSync
    }
    
    func partnerWebsiteUrl() -> URL? {
        guard let urlString = selectedDeviceDetails?.assets?.partnerWebsiteUrl else { return nil }
        guard let url = URL(string: urlString) else { return nil }
        return url
    }
    
    func item(for indexPath: IndexPath) -> CellItem? {
        let integer = sections.integer(at: indexPath.section)
        guard let sectionIndex = DeviceDetailSections(rawValue:integer) else { return nil }
        
        if sectionIndex == DeviceDetailSections.tableHeader {
            return nil
        } else if sectionIndex == DeviceDetailSections.physicalActivity {
            return physicalActivityItem(for: indexPath.row)
        } else if sectionIndex == DeviceDetailSections.healthMeasurement {
            return healthMeasurementItem(for: indexPath.row)
        } else if sectionIndex == DeviceDetailSections.other {
            return otherItem()
        } else if sectionIndex == DeviceDetailSections.about {
            return aboutItem()
        }
        return nil
    }
    
    func physicalActivityItem(for index: Int) -> CellItem {
        let activity = devicePhysicalActivities[index]

        switch activity.value {
        case .Heartrate :
            return CellItem(image: UIImage(asset: VIAWellnessDevicesAsset.WellnessDevicesAndApps.wellnessDevicesHeartRate), title: CommonStrings.PhysicalActivity.ItemHeartRateText439, activity: DeviceDetailActivity.HeartRate, activityTypeKey: PointsEntryTypeRef.Heartrate)
        case .Speed :
            return CellItem(image: UIImage(asset: VIAWellnessDevicesAsset.WellnessDevicesAndApps.wellnessDevicesSpeed), title: CommonStrings.Wda.PhysicalActivity.ItemSpeedText441, activity: DeviceDetailActivity.Speed, activityTypeKey: PointsEntryTypeRef.Speed)
        case .Steps :
            return CellItem(image: UIImage(asset: VIAWellnessDevicesAsset.WellnessDevicesAndApps.wellnessDevicesSteps), title: CommonStrings.PhysicalActivity.ItemStepsText442, activity: DeviceDetailActivity.Steps, activityTypeKey: PointsEntryTypeRef.Steps)
        case .EnergyExpend :
            return CellItem(image: UIImage(asset: VIAWellnessDevicesAsset.WellnessDevicesAndApps.wellnessDevicesCalories), title: CommonStrings.PhysicalActivity.ItemCalorliesBurnedText443, activity: DeviceDetailActivity.CaloriesBurned, activityTypeKey: PointsEntryTypeRef.EnergyExpend)
        case .Distance :
            return CellItem(image: UIImage(asset: VIAWellnessDevicesAsset.WellnessDevicesAndApps.wellnessDevicesDistance), title: CommonStrings.PhysicalActivity.ItemDistanceText440, activity: DeviceDetailActivity.Distance, activityTypeKey: PointsEntryTypeRef.Distance)
        default:
            return CellItem(image: UIImage(asset: VIAWellnessDevicesAsset.WellnessDevicesAndApps.wellnessDevicesCalories), title: CommonStrings.PhysicalActivity.ItemCalorliesBurnedText443, activity: DeviceDetailActivity.CaloriesBurned, activityTypeKey: PointsEntryTypeRef.EnergyExpend)
        }
    }
    
    enum HealthMeasurementIndex: Int, EnumCollection {
        case BMI = 0
        
        static func count() -> Int {
            return self.allValues.count
        }
    }
    
    func healthMeasurementItem(for index: Int) -> CellItem {
        return CellItem(image: UIImage(asset: VIAWellnessDevicesAsset.WellnessDevicesAndApps.wellnessDevicesWeight),
                        title: CommonStrings.Wda.HealthMeasurements.ItemBmiText496,
                        activity: DeviceDetailActivity.BMI,
                        activityTypeKey: PointsEntryTypeRef.BMI)
    }
    
    func otherItem() -> CellItem {
        return CellItem(image: UIImage(asset: VIAWellnessDevicesAsset.WellnessDevicesAndApps.wellnessDevicesSleep),
                        title: CommonStrings.Wda.Other.Item.SleepText449,
                        activity: DeviceDetailActivity.Sleep,
                        activityTypeKey: nil)
    }
    
    
    func aboutItem() -> CellItem {
        var device = ""
        var title = ""
        if let name = self.deviceName {
            device = name
            if name.caseInsensitiveCompare("softbank") == .orderedSame {
                title = CommonStrings.Wda.AboutTextSoftbank450
            } else {
                title = CommonStrings.AboutText450(device)
            }
        }
        return CellItem(image: nil, title: title, activity: DeviceDetailActivity.About, activityTypeKey: nil)
    }
    
    func aboutDeviceString(device name: String?) -> String {
        var device = ""
        if let name = deviceName {
            device = name
        }
        return CommonStrings.AboutText450(device)
    }
    
    public func getPointsActivityRef(for physicalActivity: DeviceDetailActivity) -> Int {
        var pointsActivityRef = -1
        switch physicalActivity {
        case .HeartRate:
            pointsActivityRef = PointsEntryTypeRef.Heartrate.rawValue
            break
        case .Distance:
            pointsActivityRef = PointsEntryTypeRef.Distance.rawValue
            break
        case .Speed:
            pointsActivityRef = PointsEntryTypeRef.Speed.rawValue
            break
        case .Steps:
            pointsActivityRef = PointsEntryTypeRef.Steps.rawValue
            break
        case .CaloriesBurned:
            pointsActivityRef = PointsEntryTypeRef.EnergyExpend.rawValue
            break
        default:
            break
        }
        return pointsActivityRef
    }
    
    func delinkDevice(completion: @escaping (Error?) -> Void) {
        guard let partner = selectedDeviceDetails?.partner else { return }
        guard let partnerDelink = partner.partnerDelink else { return }
        
        let coreRealm = DataProvider.newRealm()
        let tenantId = coreRealm.getTenantId()
        let partyId = coreRealm.getPartyId()
        
        let partnerSystem = partner.partnerSystem
        /**
         * TODO: Interim fix.
         * This will cover the fix for UKE, IGI, CA and SLI.
         */
        let delinkUrl = partnerDelink.url.replacingOccurrences(of: "&redirect_uri", with: "%26redirect_uri")
        let delinkMethod = partnerDelink.method
        
        Wire.Events.delinkDevice(tenantId: tenantId, identifier: partyId, partnerSystem: partnerSystem, delinkUrl: delinkUrl, delinkMethod: delinkMethod) { error in
            if let serviceError = error {
                completion(serviceError)
                return
            }
            completion(nil)
        }
    }
    
    func linkDevice(completion: @escaping (Error?) -> Void) {
        var email: String = ""
        
        let coreRealm = DataProvider.newRealm()
        let tenantId = coreRealm.getTenantId()
        let partyId = coreRealm.getPartyId()
        
        
        if let userEmail = coreRealm.allPartyEmailAddresses().first?.value {
            email = userEmail
        }
        
        let request = configureLinkDeviceRequest(partyId: partyId, email: email)
        
        guard let completeRequest = request else { return }
        
        Wire.Events.linkDevice(tenantId: tenantId, request: completeRequest) { [weak self] redirectLocation, error in
            DispatchQueue.main.async {
                if let redirect = redirectLocation {
                    self?.deviceLinkingRedirectUrl = URL(string: redirect.replacingOccurrences(of: " ", with: "%20"))
                    completion(nil)
                } else if let serviceError = error {
                    completion(serviceError)
                }
            }
        }
    }
    
    func linkSoftBankDevice(refreshToken: String, token: String, userId: String, completion: @escaping (Error?) -> Void) {
        let coreRealm = DataProvider.newRealm()
        let tenantId = coreRealm.getTenantId()
        let partyId = coreRealm.getPartyId()
        
        let request = configureLinkSoftBankDeviceRequest(refreshToken: refreshToken, token: token, userId: userId)
        guard let completeRequest = request else { return }
        
        Wire.Events.linkSoftBankDevice(tenantId: tenantId, identifier: partyId, partnerSystem: "Softbank", request: completeRequest) { [weak self] error in
            DispatchQueue.main.async {
                guard error == nil else {
                    completion(error)
                    return
                }
                completion(nil)
            }
        }
    }
    
    public func linkDeviceProcessEvents(event: EventTypeRef, onStart: (()->Void)?, completion: ((_ error:Error?)-> Void)?) {
        guard let partner = selectedDeviceDetails?.partner else { return }
        
        let coreRealm = DataProvider.newRealm()
        let tenantID = coreRealm.getTenantId()
        let partyID = coreRealm.getPartyId()
        let eventMetaDataValues: Array<String>? = [partner.device]
        
        var processEvents = [ProcessEventsEvent]()
        let processEvent = ProcessEventsEvent(date: Date(),
                                              eventTypeKey: event,
                                              eventSourceTypeKey: .MobileApp,
                                              partyId: partyID,
                                              reportedBy: partyID,
                                              associatedEvents: [ProcessEventsAssociatedEvent](),
                                              eventMetaDataTypeKey: EventMetaDataTypeRef(rawValue: EventMetaDataTypeRef.Manufacturer.rawValue),
                                              eventMetaDataUnitOfMeasure: nil,
                                              eventMetaDataValues: eventMetaDataValues)
        processEvents.append(processEvent)
        
        Wire.Events.processEvents(tenantId: tenantID, events: processEvents) { [weak self] (response, error) in
            completion!(error)
        }
    }
    
    func configureLinkSoftBankDeviceRequest(refreshToken: String, token: String, userId: String) -> VDPLinkSoftBankDeviceRequest? {
        let request = VDPLinkSoftBankDeviceRequest(refreshToken: refreshToken, token: token, userId: userId)
        return request
    }
    
    func configureLinkDeviceRequest(partyId: Int, email: String) -> VDPLinkDeviceRequest? {
        guard let partner = selectedDeviceDetails?.partner else { return nil }
        guard let partnerLink = partner.partnerLink else { return nil }
        guard let internalRedirectURL = VIAURLScheme.urlScheme(for: .wellnessDevicesApps) else { return nil }
        
        let userEmail: String = email
        let userIdentifier: String = String(partyId)
        let userIdentifierType: String = Constants.DeviceLinkingRequestConstants.userIdentifierTypePartyId
        let partnerSystem: String = partner.partnerSystem
        let partnerDevice: String = partner.device
        let partnerLinkedStatus: String = partner.partnerLinkedStatus.statusString()
        let partnerLinkUrl: String = partnerLink.url
        let partnerLinkMethod: String = partnerLink.method
        let partnerLinkRedirectUrl: String = internalRedirectURL.absoluteString
        let partnerRedirectUrl: String = internalRedirectURL.absoluteString
        
        let request = VDPLinkDeviceRequest(userEmail: userEmail, userIdentifier: userIdentifier, userIdentifierType: userIdentifierType, partnerSystem: partnerSystem,
                                           partnerDevice: partnerDevice, partnerLinkUrl: partnerLinkUrl, partnerLinkMethod: partnerLinkMethod, partnerLinkedStatus: partnerLinkedStatus, partnerLinkRedirectUrl: partnerLinkRedirectUrl, partnerRedirectUrl: partnerRedirectUrl)
        
        return request
    }
}
