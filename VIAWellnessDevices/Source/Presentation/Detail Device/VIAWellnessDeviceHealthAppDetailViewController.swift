import SafariServices

import VIAUIKit
import VitalityKit
import VIAUtilities
import VIAHealthKit
import VIACommon

import TTTAttributedLabel

public class VIAWellnessDeviceHealthAppDetailViewController: VIATableViewController, VIAWellnessDeviceHealthAppDetailViewModelDelegate, ImproveYourHealthTintable, SFSafariViewControllerDelegate, TTTAttributedLabelDelegate {
    
    func didFinishHealthKitSyncing() {
        if let tenantId = AppSettings.getAppTenant(), tenantId == .SLI {
            self.callAPIProcessEvent(eventType: EventTypeRef.DeviceSyncing)
        }
    }
    
    // MARK: Properties

    lazy var viewModel: VIAWellnessDeviceHealthAppDetailViewModel = {
        let viewModel = VIAWellnessDeviceHealthAppDetailViewModel()
        viewModel.delegate = self
        return viewModel
    }()
    
    var didPerformDelinkAction = false
    
    // MARK: View lifecycle

    public override func viewDidLoad() {
        super.viewDidLoad()

        self.title = CommonStrings.Wda.HealthApp455
        configureTableView()
        configureAppearance()
        viewModel.configureDeviceActivities()
    }

    public override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        hideBackButtonTitle()
    }

    // MARK: Setup

    func configureTableView() {
        self.tableView.register(VIATableViewSectionHeaderFooterView.nib(), forHeaderFooterViewReuseIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier)
        self.tableView.register(VIAWellnessDeviceHeaderFooterView.nib(), forHeaderFooterViewReuseIdentifier: VIAWellnessDeviceHeaderFooterView.defaultReuseIdentifier)
        self.tableView.register(VIATableViewCell.nib(), forCellReuseIdentifier: VIATableViewCell.defaultReuseIdentifier)
        self.tableView.register(VIAWellnessDeviceDetailHeaderViewCell.nib(), forCellReuseIdentifier: VIAWellnessDeviceDetailHeaderViewCell.defaultReuseIdentifier)
        self.tableView.estimatedRowHeight = 75
    }

    func configureAppearance() {
        tableView.backgroundColor = UIColor.tableViewBackground()
    }
    
    func updateViewWithLatestDataAfterLinkOrDelink() {
        self.viewModel.configureDeviceActivities()
        self.hideHUDFromView(view: self.view)
        if didPerformDelinkAction {
            didPerformDelinkAction = false
            showDelinkedSuccessfullyAlert()
        }
        self.tableView.reloadData()

    }
 
    // MARK: UITableView delegate

    override public func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == DeviceDetailSections.tableHeader.rawValue {
            return CGFloat.leastNormalMagnitude
        }

        return UITableViewAutomaticDimension
    }

    public override func numberOfSections(in tableView: UITableView) -> Int {
        return self.viewModel.sectionCount()
    }

    public override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.numberOfItems(in: section)
    }

    override public func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if section == DeviceDetailSections.tableHeader.rawValue && viewModel.hasActivitiesToDisplay() {
            return configurePointsEarningMetricsFooter()
        }
        return nil
    }

    override public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let integer = viewModel.sections.integer(at: section)
        guard let sectionIndex = DeviceDetailSections(rawValue:integer) else { return nil }

        if sectionIndex == DeviceDetailSections.physicalActivity {
            if let title = DeviceDetailSections.physicalActivity.title() {
                return configureSectionHeader(title: title)
            }
        } else if sectionIndex == DeviceDetailSections.healthMeasurement {
            if let title = DeviceDetailSections.healthMeasurement.title() {
                return configureSectionHeader(title: title)
            }
        } else if sectionIndex == DeviceDetailSections.other {
            if let title = DeviceDetailSections.other.title() {
                return configureSectionHeader(title: title)
            }
        }
        return nil
    }

    func configureSectionHeader(title: String) -> UITableViewHeaderFooterView {
        let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier) as! VIATableViewSectionHeaderFooterView
        let labelText = title
          view.configureWithExtraSpacing(labelText: labelText, extraTopSpacing: 15, extraBottomSpacing: 10)
        view.set(font: .title2Font())
        view.set(textColor: .night())
        return view
    }

    func configurePointsEarningMetricsFooter() -> UITableViewHeaderFooterView {
        let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: VIAWellnessDeviceHeaderFooterView.defaultReuseIdentifier) as! VIAWellnessDeviceHeaderFooterView
        view.configureForHealthApp(linked: viewModel.isDeviceLinked())
     
        guard let url = viewModel.healthAppLearnMoreUrl() else {
            return view
        }
        
        view.configureAttributedLabelWith(url: url, delegate: self, isSoftBank: false)
        return view
    }

    public override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == DeviceDetailSections.tableHeader.rawValue {
            let cell = tableView.dequeueReusableCell(withIdentifier: VIAWellnessDeviceDetailHeaderViewCell.defaultReuseIdentifier) as! VIAWellnessDeviceDetailHeaderViewCell
            let lastSynced = viewModel.lastSynced()
            let isLinked = viewModel.isDeviceLinked()
            cell.configureForHealthApp(lastSynced: lastSynced, isLinked: isLinked)
            configureHeaderButtonActions(cell: cell, isLinked: isLinked)
            cell.selectionStyle = .none
            return cell
        }

        guard let cell = tableView.dequeueReusableCell(withIdentifier: VIATableViewCell.defaultReuseIdentifier) as? VIATableViewCell else {
            return UITableViewCell()
        }
        if let cellContent = self.viewModel.item(for: indexPath) {
            cell.labelText = cellContent.title
            cell.cellImage = cellContent.image
        }
        cell.accessoryType = .disclosureIndicator
        cell.separatorInset = UIEdgeInsets(top: 0, left: 55, bottom: 0, right: 0)
        return cell
    }

    func configureHeaderButtonActions(cell: VIAWellnessDeviceDetailHeaderViewCell, isLinked: Bool) {
        if isLinked {
            cell.didTapLinkSyncButton = { [weak self] in
                self?.syncNowTapped()
            }
        } else {
            cell.didTapLinkSyncButton = { [weak self] in
                self?.linkNowTapped()
            }
        }

        cell.didTapDelinkButton = { [weak self] in
            self?.delinkTapped()
        }
        cell.didTapStepsTroubleButton = { [weak self] in
            self?.havingTroubleTapped()
        }
    }

    override public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let itemForRow = self.viewModel.item(for: indexPath) {
            if itemForRow.activity == DeviceDetailActivity.About {
                self.aboutTapped()
                return
            } else {
                switch indexPath.section {
                case DeviceDetailSections.physicalActivity.rawValue:
                    self.performSegue(withIdentifier: StoryboardSegue.WellnessDevicesApps.showPointsEarningPotential.rawValue, sender: itemForRow)
                default:
                    break
                }
            }
        }
    }

    // MARK: Navigation

    @IBAction public func dismissedHealthKitOnboarding(segue: UIStoryboardSegue) {
        NotificationCenter.default.post(name: .DidDismissHealthKitOnboardingViewController, object: nil)
    }

    @IBAction public func unwindToDeviceDetail(segue: UIStoryboardSegue) {
        debugPrint("unwindToDeviceDetail")
    }

    func linkNowTapped() {
        self.viewModel.performLink()
    }

    func syncNowTapped() {
        performSync()
    }

    func havingTroubleTapped() {
       self.performSegue(withIdentifier: "segueShowHelp", sender: nil)
    }

    func delinkTapped() {
        showDelinkWarningAlert()
    }

    public func performSync() {
        viewModel.performSync()
    }

    func performDeviceDelinking() {
        viewModel.delinkHealthApp { [weak self] wasSuccessfullyDelinked in
            if wasSuccessfullyDelinked {
                //Post notification to refesh landing screen
                NotificationCenter.default.post(name: .VIAWDANeedToFetchUpdatedDevices, object: nil)
                self?.updateViewWithLatestDataAfterLinkOrDelink()
                if let tenantId = AppSettings.getAppTenant(), tenantId == .SLI || tenantId == .CA {
                    self?.callAPIProcessEvent(eventType: EventTypeRef.DeviceDelinking)
                }
            } else {
                self?.showDelinkErrorAlert()
            }
        }
    }
    
    func callAPIProcessEvent(eventType: EventTypeRef) {
        self.viewModel.linkHealthAppProcessEvents(event: eventType, onStart: { [weak self] in
            self?.showFullScreenHUD()
            }, completion: { (error) in
                DispatchQueue.main.async {
                    self.hideFullScreenHUD()
                    // add error checking
                }
        })
    }

    func showDelinkWarningAlert() {
        let alertController = UIAlertController(title: CommonStrings.Wda.DelinkDialogTitle464, message: CommonStrings.DelinkDialogDescription465(CommonStrings.Wda.HealthApp455), preferredStyle: .alert)
        let okAction = UIAlertAction(title: CommonStrings.Wda.DeviceDetail.DelinkButtonText462 , style: .destructive, handler: { [weak self] action in
            self?.performDeviceDelinking()
            self?.showDelinkedSuccessfullyAlert()
        })
        let cancelAction = UIAlertAction(title: CommonStrings.CancelButtonTitle24, style: .cancel, handler: nil)

        alertController.addAction(okAction)
        alertController.addAction(cancelAction)

        present(alertController, animated: true, completion: nil)
    }

    func showDelinkErrorAlert() {
        let alertController = UIAlertController(title: CommonStrings.Wda.DeviceDetail.DelinkFailedDialogTitle468, message: CommonStrings.DelinkFailedDialogDescription469(CommonStrings.Wda.HealthApp455), preferredStyle: .alert)
        let tryAgainAction = UIAlertAction(title: CommonStrings.TryAgainButtonTitle43, style: .default, handler: { [weak self] action in
            self?.performDeviceDelinking()
        })
        let cancelAction = UIAlertAction(title: CommonStrings.CancelButtonTitle24, style: .cancel, handler: nil)

        alertController.addAction(tryAgainAction)
        alertController.addAction(cancelAction)

        present(alertController, animated: true, completion: nil)
    }
    
    func showLinkedSuccessfullyAlert() {
        showHud(on: self.view, with: VIAUIKitAsset.checkMarkCircle.image, display: CommonStrings.DeviceLinking.SuccessfullyLinked454)
    }
    
    func showLinkedErrorAlert() {
        
        let alertController = UIAlertController(title: CommonStrings.Wda.DeviceDetail.SyncFailedDialogTitle512, message: CommonStrings.Wda.DeviceDetail.SyncFailedDialogMessage513(CommonStrings.Wda.HealthApp455), preferredStyle: .alert)
        let tryAgainAction = UIAlertAction(title: CommonStrings.TryAgainButtonTitle43, style: .default, handler: { [weak self] action in
            self?.viewModel.performLink()
        })
        let cancelAction = UIAlertAction(title: CommonStrings.CancelButtonTitle24, style: .cancel, handler: nil)
        
        alertController.addAction(tryAgainAction)
        alertController.addAction(cancelAction)
        
        present(alertController, animated: true, completion: nil)
    }
    
    func showDelinkedSuccessfullyAlert() {
        showHud(on: self.view.window, with: VIAUIKitAsset.checkMarkCircle.image, display: CommonStrings.DeviceLinking.SuccessfullyDelinked470)
    }
    
    func aboutTapped() {
        let segueInfo = WellnessDevicesSegueInfo(title: self.viewModel.aboutDeviceString(), articleId: self.viewModel.aboutContentID())
        self.performSegue(withIdentifier: StoryboardSegue.WellnessDevicesApps.showArticle.rawValue, sender: segueInfo)
    }

    override public func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == StoryboardSegue.WellnessDevicesApps.showPointsEarningPotential.rawValue {
            guard let physicalActivity = sender as? VIAWellnessDeviceHealthAppDetailViewModel.CellItem else {
                print("No event for this activity")
                return
            }
            guard let destination = segue.destination as? VIAPointsEarningPotentialWithTiersTableViewController else {
                print("Incorrect view controller as destination")
                return
            }

            destination.title = physicalActivity.title
            if let physicalActivityTypeKey = physicalActivity.activityTypeKey?.rawValue {
                destination.pointsActivityTypeKey = PointsEntryTypeRef(rawValue: physicalActivityTypeKey) ?? .Unknown
                destination.activityImage = physicalActivity.image
                destination.viewModel = VIAWellnessDevicesPointsEarningPotentialViewModel(earningActivityKey: physicalActivityTypeKey)
            }
        } else if segue.identifier == StoryboardSegue.WellnessDevicesApps.showDataConsent.rawValue {
            debugPrint("")
        } else if let webContentViewController = segue.destination as? VIAWebContentViewController, let segueInfo = sender as? WellnessDevicesSegueInfo {

            let webContentViewModel = VIAWebContentViewModel(articleId: segueInfo.articleId)
            webContentViewController.viewModel = webContentViewModel
            webContentViewController.title = segueInfo.title
        }
    }

    // MARK: VIAWellnessDeviceHealthAppDetailViewModelDelegate

    func healthKitWillBeginSync() {
        self.showHUDOnView(view: self.view)
    }

    func healthKitDidSync() {
        NotificationCenter.default.post(name: .VIAWDADidFetchDevices, object: nil) // reload WDA landing
        self.tableView.reloadData()
        self.hideHUDFromView(view: self.view)
    }

    func displayHealthKitOnboardingView() {
        if VitalityProductFeature.isEnabled(.WDADSConsent) && VIAApplicableFeatures.default.shouldDisplayWellnessDevicesAndDataSharingConsent!{
            self.performSegue(withIdentifier: StoryboardSegue.WellnessDevicesApps.showDataConsent.rawValue, sender: nil)
        } else {
            self.performSegue(withIdentifier: StoryboardSegue.WellnessDevicesApps.showHealthKitOnboarding.rawValue, sender: nil)
        }
    }

    func didFinishHealthKitLinking(error: Error?) {
        
        if nil == error {
            
            NotificationCenter.default.post(name: .VIAWDANeedToFetchUpdatedDevices, object: nil)
            if let tenantId = AppSettings.getAppTenant(), tenantId == .SLI || tenantId == .CA{
                self.callAPIProcessEvent(eventType: EventTypeRef.DeviceLinking)
            }
            showLinkedSuccessfullyAlert()
            tableView.reloadData()
        } else {
            
            showLinkedErrorAlert()
        }
    }

    func handleSyncingErrorOccurred(error: Error?) {
        let backendError = error as? BackendError ?? BackendError.other
        self.handleBackendErrorWithAlert(backendError, tryAgainAction: { [weak self] in
            self?.performSync()
        })
    }
    
    // MARK: TTTAttributedLabel delegate
    public func attributedLabel(_ label: TTTAttributedLabel!, didSelectLinkWith url: URL!) {
        presentModally(url: url, parentViewController: self)
    }
    
    // MARK: Safari Delegate
    public func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        // user tapped done, we can't know if the process finished, assumed they bailed early.
        self.dismiss(animated: true, completion: nil)
    }

    
    public func presentModally(url: URL, parentViewController: UIViewController) {
        let safariVC = SFSafariViewController(url: url)
        safariVC.modalPresentationStyle = .popover
        safariVC.delegate = self
        safariVC.preferredControlTintColor = UIColor.currentGlobalTintColor()
        parentViewController.present(safariVC, animated: true, completion: nil)
    }

}
