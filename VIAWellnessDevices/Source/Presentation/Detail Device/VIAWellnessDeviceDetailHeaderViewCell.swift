import Foundation

import VIAUIKit
import VitalityKit

class VIAWellnessDeviceDetailHeaderViewCell: UITableViewCell, Nibloadable {

    // MARK: Properties

    public var message: String? {
        get {
            return self.messageLabel.text
        }
        set {
            self.messageLabel.text = newValue
        }
    }

    public var didTapLinkSyncButton: () -> Void = {
        debugPrint("VIAWellnessDeviceDetailHeaderView didTapButton")
    }
    public var didTapStepsTroubleButton: () -> Void = {
        debugPrint("VIAWellnessDeviceDetailHeaderView didTapButton")
    }
    public var didTapDelinkButton: () -> Void = {
        debugPrint("VIAWellnessDeviceDetailHeaderView didTapButton")
    }

    // MARK: Outlets

    @IBOutlet private weak var lastSyncedHeaderHeightConstraint: NSLayoutConstraint!
    @IBOutlet private weak var lastSyncedDateHeightConstraint: NSLayoutConstraint!
    @IBOutlet private weak var lastSyncedHeaderLabel: UILabel!
    @IBOutlet private weak var lastSyncedDateLabel: UILabel!
    @IBOutlet private weak var messageLabel: UILabel!
    @IBOutlet private weak var linkSyncButton: VIAButton!
    @IBOutlet private weak var stepsTroubleButton: VIAButton!
    @IBOutlet private weak var delinkButton: VIAButton!
    @IBOutlet private weak var syncStackView: UIStackView!

    override func awakeFromNib() {
        super.awakeFromNib()
        setupCell()
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        setupCell()
    }

    func setupCell() {
        syncStackView.isHidden = true
        
        lastSyncedHeaderLabel.text = nil
        lastSyncedHeaderLabel.font = UIFont.title2Font()
        lastSyncedHeaderLabel.isHidden = true
        lastSyncedHeaderHeightConstraint.constant = lastSyncedHeaderLabel.font.lineHeight

        lastSyncedDateLabel.text = nil
        lastSyncedDateLabel.font = UIFont.bodyFont()
        lastSyncedDateLabel.isHidden = true
        lastSyncedDateHeightConstraint.constant = lastSyncedDateLabel.font.lineHeight

        messageLabel.text = CommonStrings.Wda.DeviceDetail.EarnMessage433
        messageLabel.numberOfLines = 5
        messageLabel.font = UIFont.subheadlineFont()
        messageLabel.textColor = UIColor.night()
        messageLabel.isHidden = true

        linkSyncButton.tintColor = UIColor.currentGlobalTintColor()
        linkSyncButton.setTitle(nil, for: .normal)
        linkSyncButton.titleLabel?.font = UIFont.bodyFont()
        linkSyncButton.isHidden = true

        stepsTroubleButton.tintColor = UIColor.currentGlobalTintColor()
        stepsTroubleButton.setTitle(nil, for: .normal)
        stepsTroubleButton.titleLabel?.font = UIFont.bodyFont()
        stepsTroubleButton.hidesBorder = true
        stepsTroubleButton.isHidden = true

        delinkButton.tintColor = UIColor.crimsonRed()
        delinkButton.setTitle(CommonStrings.Wda.DeviceDetail.DelinkButtonText462, for: .normal)
        delinkButton.titleLabel?.font = UIFont.bodyFont()
        delinkButton.hidesBorder = true
        delinkButton.isHidden = true
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        self.layer.removeAllBorderLayers()
        self.layer.addBorder(edge: .top)
        self.layer.addBorder(edge: .bottom)
    }

    func hideStepsButton() {
        stepsTroubleButton.isHidden = true
    }
    
    func configureMessageLabel(isSoftbank: Bool) {
        if isSoftbank {
            messageLabel.text = CommonStrings.Wda.DeviceDetail.EarnMessageSoftbank433
        } else {
            messageLabel.text = CommonStrings.Wda.DeviceDetail.EarnMessage433
        }
        messageLabel.numberOfLines = 5
        messageLabel.font = UIFont.subheadlineFont()
        messageLabel.textColor = UIColor.night()
        messageLabel.isHidden = true
    }

    func configureForWellnessDevice(lastSynced: Date?, isLinked: Bool) {
        if isLinked {
            if let date = lastSynced {
                lastSyncedHeaderLabel.text = CommonStrings.Wda.DeviceDetail.LastSyncedHeader456
                lastSyncedDateLabel.text = configureLastSyncedDisplayString(syncDate: date)
                syncStackView.isHidden = false
                lastSyncedHeaderLabel.isHidden = false
                lastSyncedDateLabel.isHidden = false
            } else {
                lastSyncedHeaderLabel.text = CommonStrings.Wda.DeviceDetail.LastSyncedHeader456
                lastSyncedDateLabel.text = CommonStrings.Wda.DeviceDetail.NotSynced493
                syncStackView.isHidden = false
                lastSyncedHeaderLabel.isHidden = false
                lastSyncedDateLabel.isHidden = false
            }
            stepsTroubleButton.setTitle(CommonStrings.HavingTroubleButtonText461, for: .normal)
            linkSyncButton.isHidden = true
            // TODO: reset to false after help screen is implemented
            stepsTroubleButton.isHidden = true
            delinkButton.isHidden = false
            messageLabel.isHidden = true
        } else {
            linkSyncButton.setTitle(CommonStrings.LinkNowButtonText434, for: .normal)
            stepsTroubleButton.setTitle(CommonStrings.StepsToLinkButtonText471, for: .normal)
            linkSyncButton.isHidden = false
            messageLabel.isHidden = false
            stepsTroubleButton.isHidden = false
            delinkButton.isHidden = true
            syncStackView.isHidden = true
            lastSyncedDateLabel.isHidden = true
            lastSyncedHeaderLabel.isHidden = true
        }
    }

    func configureForHealthApp(lastSynced: Date?, isLinked: Bool) {
        if isLinked {
            if let date = lastSynced {
                lastSyncedHeaderLabel.text = CommonStrings.Wda.DeviceDetail.LastSyncedHeader456
                lastSyncedDateLabel.text = configureLastSyncedDisplayString(syncDate: date)
                syncStackView.isHidden = false
                lastSyncedHeaderLabel.isHidden = false
                lastSyncedDateLabel.isHidden = false
            } else {
                lastSyncedHeaderLabel.text = CommonStrings.Wda.DeviceDetail.NotSynced493
                lastSyncedDateLabel.text = nil
                syncStackView.isHidden = false
                lastSyncedHeaderLabel.isHidden = false
                lastSyncedDateLabel.isHidden = true
            }

            linkSyncButton.setTitle(CommonStrings.Wda.DeviceDetail.SyncNowButtonText460, for: .normal)
            stepsTroubleButton.setTitle(CommonStrings.HavingTroubleButtonText461, for: .normal)
            linkSyncButton.isHidden = false
            // TODO: reset to false after help screen is implemented
            stepsTroubleButton.isHidden = true
            delinkButton.isHidden = false
            messageLabel.isHidden = true
        } else {
            linkSyncButton.setTitle(CommonStrings.LinkNowButtonText434, for: .normal)
            linkSyncButton.isHidden = false
            messageLabel.isHidden = false
            stepsTroubleButton.isHidden = true
            delinkButton.isHidden = true
            syncStackView.isHidden = true
            lastSyncedDateLabel.isHidden = true
            lastSyncedHeaderLabel.isHidden = true
        }
    }

    private func configureLastSyncedDisplayString(syncDate: Date) -> String {
        if Calendar.current.isDateInToday(syncDate) {
            let timeString = Localization.shortTimeFormatter.string(from:syncDate)
            return   CommonStrings.Wda.DeviceDetail.LastSyncedToday474(timeString)
        } else if Calendar.current.isDateInYesterday(syncDate) {
            let timeString = Localization.shortTimeFormatter.string(from:syncDate)
            return CommonStrings.Wda.DeviceDetail.SyncedYesterday475(timeString)
        }

        let dateString = Localization.dayLongMonthLongYearShortTimeFormatter.string(from:syncDate)
        return dateString
    }



    @IBAction func didTapLinkSyncButton(_ sender: Any) {
         self.didTapLinkSyncButton()
    }

    @IBAction func didTapStepsTroubleButton(_ sender: Any) {
         self.didTapStepsTroubleButton()
    }

    @IBAction func didTapDelinkButton(_ sender: Any) {
         self.didTapDelinkButton()
    }
}
