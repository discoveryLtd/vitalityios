//
//  VIAAppleHealthHandler.swift
//  VitalityActive
//
//  Created by Michelle R. Oratil on 27/03/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import VIAHealthKit
import VitalityKit

import RealmSwift

public protocol VIAAppleHealthHandler: class{
    func configureAutoSync()
}

public extension VIAAppleHealthHandler{
    
    public func configureAutoSync(){
        
        if let frequency = Bundle.main.object(forInfoDictionaryKey: "VIAAppleSyncFrequency") as? Double{
            
            guard frequency != -1 else { return }
            
            if let syncedDate = VITHealthKitHelper.shared().lastSubmittedDate(){
                
                let currentDate = Date()
                
                let shouldPerformSync = currentDate >= syncedDate.addingTimeInterval(frequency * 60.0)
                if shouldPerformSync{
                    triggerSync()
                }
            }else{
                /* 
                 * If last submitted date is nil, automatically trigger sync.
                 * We no longer need to wait for the interval time to kick-off.
                 */
                triggerSync()
            }
        }
    }
    
    private func triggerSync(){
        VITHealthKitHelper.shared().generateReadings(withForcedResubmission: true, completion: { [weak self] generatedReadings in
            self?.healthKitDidGenerateReadings(generatedReadings)
        })
    }
    
    func healthKitDidGenerateReadings(_ generatedReadings: [VITGDLReading]?) {
        
        let coreRealm = DataProvider.newRealm()
        
        guard let readings = generatedReadings, readings.count > 0 else {
            debugPrint("No HealthKit readings to submit")
            return
        }
        
        coreRealm.refresh()
        
        let device = VITHealthKitHelper.deviceForSubmission()
        let model = self.generateUploadModel(device: device, readings: readings, coreRealm: coreRealm)
        let tenantId = coreRealm.getTenantId()
        Wire.Events.upload(tenantId: tenantId, payload: model, completion: { [weak self] error in
            if error == nil {
                VITHealthKitHelper.shared().readingsSubmittedSuccessfully()
            }
        })
    }
    
    func generateUploadModel(device: VITDevice, readings: [VITGDLReading], coreRealm: Realm) -> VDPGenericUploadViewModel {
        let uploadViewModel = VDPGenericUploadViewModel()
        uploadViewModel.readings = [VDPGenericUploadReadingViewModel]()
        coreRealm.refresh()
        // header
        let partyId = coreRealm.getPartyId()
        uploadViewModel.header?.user?.userIdentifier = "\(partyId)"
        uploadViewModel.header?.user?.identifierType = "PARTY_ID"
        uploadViewModel.header?.uploadDate = Date()
        uploadViewModel.header?.sessionId = "\(partyId)-\(Date())"
        uploadViewModel.header?.partnerSystem = "Apple"
        uploadViewModel.header?.partnerSystemSource = "Apple Health"
        uploadViewModel.header?.verified = true
        uploadViewModel.header?.tenantId = coreRealm.getTenantId()
        uploadViewModel.header?.processingType = "REALTIME"
        
        // device
        uploadViewModel.device?.deviceId = device.deviceID
        uploadViewModel.device?.manufacturer = device.manufacturer
        uploadViewModel.device?.make = device.make
        uploadViewModel.device?.model = device.model
        
        // readings
        for item in readings {
            let reading = item.uploadModel()
            if reading.workoutIsPopulated {
                uploadViewModel.readings?.append(reading)
            }
        }
        uploadViewModel.header?.uploadCount = uploadViewModel.readings?.count
        return uploadViewModel
    }
    
}
