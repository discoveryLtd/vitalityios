import UIKit
import VIAUIKit
import VitalityKit
import VIAUtilities
import VitalityKit

public class VIAPointsEarningPotentialWithTiersTableViewController: VIATableViewController, ImproveYourHealthTintable {
    
    public var isSoftbank: Bool = false
    public var numberOfSections: Int?
    public var pointsActivityTypeKey = PointsEntryTypeRef.Unknown
    public var activityImage: UIImage?
    public var viewModel: PointsEarningPotentialTableViewModel = VIAWellnessDevicesPointsEarningPotentialViewModel(earningActivityKey: -1)
    public var viewController: String?
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
        NotificationCenter.default.addObserver(self, selector: #selector(potentialPointsLoaded), name: .VIAPotentialPointsWithTiersReceived, object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func potentialPointsLoaded() {
        self.tableView.reloadData()
    }
    
    func setupTableView() {
        self.tableView.register(VIASubtitleTableViewCell.nib(), forCellReuseIdentifier: VIASubtitleTableViewCell.defaultReuseIdentifier)
        self.tableView.register(VIALabelTableViewCell.nib(), forCellReuseIdentifier: VIALabelTableViewCell.defaultReuseIdentifier)
        self.tableView.register(TitleImageHeaderView.nib(), forHeaderFooterViewReuseIdentifier: TitleImageHeaderView.defaultReuseIdentifier)
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 50
        
        self.tableView.estimatedSectionHeaderHeight = 100
        self.tableView.sectionHeaderHeight = UITableViewAutomaticDimension
    }
    
    override public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if let header = self.tableView.dequeueReusableHeaderFooterView(withIdentifier: TitleImageHeaderView.defaultReuseIdentifier) as? TitleImageHeaderView {
            if section == 0 {
                header.title = isSoftbank ? WellnessDevicesStrings.PotentialPoints.HeaderTitleSoftbank476 : WellnessDevicesStrings.PotentialPoints.HeaderTitle476
                header.image = activityImage
            } else if section == 1 {
                header.title = viewModel.howToEarnPointsTitle(for: pointsActivityTypeKey)
                header.image = activityImage
            }
            return header
        }
        return nil
    }
    // MARK: - Table view data source
    
    override public func numberOfSections(in tableView: UITableView) -> Int {
        if VIAApplicableFeatures.default.earnPointsByBMI! &&  pointsActivityTypeKey == PointsEntryTypeRef.BMI{
            return 1
        }else{
            return numberOfSections ?? 2
        }
    }
    
    override public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 1 {
            return 1 // How to earn points row
        } else {
            let rowCount = self.viewModel.numberOfPotentialPointsItems()
            return rowCount == 0 ? 1 : rowCount
        }
    }
    
    override public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let labelCell = tableView.dequeueReusableCell(withIdentifier: VIALabelTableViewCell.defaultReuseIdentifier, for: indexPath)
        if indexPath.section == 1 {
            if let contentInfoCell = labelCell as? VIALabelTableViewCell {
                if viewController == "AWC" {
                    contentInfoCell.labelText = viewModel.howToEarnPointsAWCDescription(for: pointsActivityTypeKey)
                } else {
                    contentInfoCell.labelText = viewModel.howToEarnPointsDescription(for: pointsActivityTypeKey)
                }
                
                contentInfoCell.selectionStyle = UITableViewCellSelectionStyle.none
                return contentInfoCell
            }
        } else {
            if let subtitleCell = tableView.dequeueReusableCell(withIdentifier: VIASubtitleTableViewCell.defaultReuseIdentifier, for: indexPath)  as? VIASubtitleTableViewCell {
                subtitleCell.isUserInteractionEnabled = false
                let requirements = NSMutableAttributedString(string: "")
                
                if pointsActivityTypeKey == .GymVisit {
                    if let contentText = viewModel.howToEarnPointsDescription(for: pointsActivityTypeKey) {
                        let boldText = NSMutableAttributedString(string: "\u{2022}\t"+contentText)
                        requirements.append(boldText)
                    }
                } else {
                    if isSoftbank {
                        var boldText = NSMutableAttributedString(string:String(format: "\u{2022}\t%@", WellnessDevicesStrings.PotentialPoints.BmiRangingBetween9999("18.5", "24.9")))
                        boldText = generateAttributedParagraph(for: boldText)
                        requirements.append(boldText)
                        
                    } else {
                        let conditions = self.viewModel.conditions(for: indexPath.row)
                        for condition in conditions {
                            if requirements != NSMutableAttributedString(string: "") {
                                requirements.append(newLineString())
                            }
                            if let conditionDescription =  viewModel.pointEarningPotentialItemDescription(condition: condition) {
                                var boldText = NSMutableAttributedString(string: "\u{2022}\t"+conditionDescription.conditionString)
                                boldText = generateAttributedParagraph(for: boldText)
                                for conditionItem in conditionDescription.arrayOfConditions {
                                    boldText = bold(substring: conditionItem, in: boldText, with: subtitleCell.fontForContentLabel().fontDescriptor)
                                }
                                requirements.append(boldText)
                            }
                            if condition.metadataTypeKey != conditions[conditions.endIndex - 1].metadataTypeKey {
                                requirements.append(newLineString())
                            }
                        }
                    }
                }
                
                let pointsValueString: String?
                
                if VIAApplicableFeatures.default.earnPointsByBMI! &&  pointsActivityTypeKey == PointsEntryTypeRef.BMI{
                    pointsValueString = self.viewModel.totalPotentialBMIPoints()
                    subtitleCell.attributedContentText = bmiRequirements(subtitleTableViewCell: subtitleCell)
                }else{
                    pointsValueString = self.viewModel.pointEarningPotentialItemPoints(for: indexPath.row)
                    subtitleCell.attributedContentText = requirements
                }
                
                let headingText = NSMutableAttributedString(string: pointsValueString!)
                
                let pointValue: String = self.viewModel.pointValueForItem(at: indexPath.row)
                let font = UIFont(descriptor: UIFont.title1Font().fontDescriptor, size: UIFont.title1Font().pointSize)
                let attributes: [String : Any] = [NSFontAttributeName: font as Any]
                headingText.addAttributes(attributes, range: headingText.string.nsRange(of: pointValue))
                
                subtitleCell.attributedHeadingText = headingText
                return subtitleCell
            }
        }
        return labelCell
    }
    
    func bmiRequirements(subtitleTableViewCell: VIASubtitleTableViewCell) -> NSMutableAttributedString{
        let requirements = NSMutableAttributedString(string: "")
        
        if requirements != NSMutableAttributedString(string: "") {
            requirements.append(newLineString())
        }
        
        let stringArray: [String] = [String(18.5), String(24.9)]
        let bmiRequirement = WellnessDevicesStrings.PotentialPoints.BmiRangingBetween9999(String(18.5), String(24.9))
        var boldText = NSMutableAttributedString(string: "\u{2022}\t"+bmiRequirement)
        
        boldText = generateAttributedParagraph(for: boldText)
        for conditionItem in stringArray {
            boldText = bold(substring: conditionItem, in: boldText, with:subtitleTableViewCell.fontForContentLabel().fontDescriptor)
        }
        requirements.append(boldText)
        
        return requirements
    }
    
    func newLineString() -> NSAttributedString {
        let newLineString = NSMutableAttributedString(string: "\n")
        
        let font = UIFont(descriptor: UIFont.bodyFont().fontDescriptor, size: 10)
        let attributes: [String : Any] = [NSFontAttributeName: font as Any]
        newLineString.addAttributes(attributes,
                                    range: newLineString.string.nsRange(of: newLineString.string))
        
        return newLineString
    }
    
    func bold(substring: String, in mainString: NSMutableAttributedString, with baseFontDescriptor: UIFontDescriptor) -> NSMutableAttributedString {
        guard  let substringRange = mainString.string.range(of: substring) else {
            return mainString
        }
        let boldFontDescriptor = baseFontDescriptor.addingAttributes([UIFontWeightTrait: UIFontWeightBold])
        
        let font = UIFont(descriptor: boldFontDescriptor, size: boldFontDescriptor.pointSize)
        let fontName = font.fontName.components(separatedBy: "-").first
        let newFont = UIFont(name: "\(fontName!)-Heavy", size: font.pointSize)
        
        let attributes: [String : Any] = [NSFontAttributeName: newFont as Any]
        mainString.addAttributes(attributes, range: mainString.string.nsRange(from:substringRange))
        
        return mainString
    }
    
    func generateAttributedParagraph(for condition: NSMutableAttributedString) -> NSMutableAttributedString {
        let paragrahStyle = NSMutableParagraphStyle()
        paragrahStyle.paragraphSpacing = 0
        paragrahStyle.paragraphSpacingBefore = 0.0
        paragrahStyle.firstLineHeadIndent = 0.0  // First line is the one with bullet point
        paragrahStyle.headIndent = 30 // Set the indent for given bullet character and size font
        
        condition.addAttribute(NSParagraphStyleAttributeName, value: paragrahStyle, range: NSRange(location: 0, length: condition.string.characters.count))
        
        return condition
    }
    
    // MARK: - Navigation
    override public func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }
}

extension String {
    func nsRange(from range: Range<Index>) -> NSRange {
        let lower = UTF16View.Index(range.lowerBound, within: utf16)
        let upper = UTF16View.Index(range.upperBound, within: utf16)
        guard lower != nil, upper != nil else {
            return NSMakeRange(0, 0)
        }
        //Xcode 9
        return NSRange(location: utf16.startIndex.distance(to: lower), length: lower!.distance(to: upper))
        //Xcode 9
        //return NSRange(location: utf16.startIndex.distance(to: lower), length: lower.distance(to: upper))
    }
}
