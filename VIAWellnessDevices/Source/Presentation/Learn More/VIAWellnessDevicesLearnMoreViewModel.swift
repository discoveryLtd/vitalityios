import VIAUIKit
import VitalityKit
import VIACommon

public class VIAWellnessDevicesLearnMoreViewModel: VIALearnMoreViewModel {
    public var title: String {
        return CommonStrings.LearnMoreButtonTitle104
    }
    
    public var contentItems: [LearnMoreContentItem] {
        var items = [LearnMoreContentItem]()
        
        items.append(LearnMoreContentItem(
            header: CommonStrings.Wda.LearnMore.Item1Heading427,
            content: CommonStrings.Wda.LearnMore.Item1428
        ))
        
        items.append(LearnMoreContentItem(
            header: CommonStrings.Wda.LearnMore.Item2Heading429,
            content: CommonStrings.Wda.LearnMore.Item2430,
            image: VIAWellnessDevicesAsset.WellnessDevicesAndApps.wellnessDevicesLearnMoreMed.image
        ))

        items.append(LearnMoreContentItem(
            header: CommonStrings.Wda.LearnMore.Item3Heading431,
            content: CommonStrings.Wda.LearnMore.Item3432,
            image: VIAWellnessDevicesAsset.WellnessDevicesAndApps.wellnessDevicesLearnMorePoints.image
        ))
        
        return items
    }
    
    public var imageTint: UIColor {
        return .primaryColor()
    }
}
