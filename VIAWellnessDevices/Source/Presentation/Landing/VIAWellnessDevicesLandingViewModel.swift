import Foundation

import VitalityKit
import VIAHealthKit
import VIAUtilities
import VIACommon

public struct WellnessDevice {
    var name: String
    var lastSynced: String?
    init(name: String, lastSynced: String?) {
        self.name = name
        self.lastSynced = lastSynced
    }
}

public struct WellnessDeviceSection {
    public enum WellnessDeviceSectionType {
        case available
        case linked
        case other
    }

    var type: WellnessDeviceSectionType
    var devices: [WellnessDevice]

    public init(type: WellnessDeviceSectionType, devices: [WellnessDevice]) {
        self.type = type
        self.devices = devices
    }
}

protocol WellnessDevicesLandingViewModel {
    var sections: [WellnessDeviceSection] { get }
    var unlinkedSection: WellnessDeviceSection { get }
    var linkedSection: WellnessDeviceSection { get }
}

class VIAWellnessDevicesLandingViewModel: AnyObject, WellnessDevicesLandingViewModel, WDAFetchDevicesManager, WDADeviceActivityMappingHelper {
    var wdaRealm = DataProvider.newWDARealm()
    var coreRealm = DataProvider.newRealm()
    var linkedDevices = [WellnessDevice]()
    var unlinkedDevices = [WellnessDevice]()

    lazy var sections = [WellnessDeviceSection]()

    internal var unlinkedSection = WellnessDeviceSection(type: .available, devices: [])

    internal var linkedSection = WellnessDeviceSection(type: .linked, devices: [])

    func configureDeviceData() {
        unlinkedDevices.removeAll()
        linkedDevices.removeAll()
        wdaRealm.refresh()

        processHealthAppData()

        let marketDevices = wdaRealm.allWDAMarkets()

        for device in marketDevices {
            processDeviceData(device: device)
        }

        configureSections()
    }

    func processDeviceData(device: WDAMarket) {
        var deviceName = String()
        if let name = device.partner?.device {
            deviceName = name
        }

        let linkedStatus = device.partner?.partnerLinkedStatus
        if linkedStatus == .Unlinked {
            let wellnessDevice = WellnessDevice(name: deviceName, lastSynced: nil)
            unlinkedDevices.append(wellnessDevice)
        } else if linkedStatus == .Active || linkedStatus == .Pending {
            let syncedDate = device.partner?.partnerLastSync
            let lastSyncedString = configureLastSyncedDisplayString(syncDate: syncedDate)
            let wellnessDevice = WellnessDevice(name: deviceName, lastSynced: lastSyncedString)
            linkedDevices.append(wellnessDevice)
        }
    }

    func processHealthAppData() {
    let deviceName = CommonStrings.Wda.HealthApp455
    let linkedStatus = VITHealthKitHelper.shared().status

        if linkedStatus == .notLinkedYet {
            let wellnessDevice = WellnessDevice(name: deviceName, lastSynced: nil)
            unlinkedDevices.append(wellnessDevice)
        } else if linkedStatus == .available {
            let syncedDate = VITHealthKitHelper.shared().lastSubmittedDate()
            let lastSyncedString = configureLastSyncedDisplayString(syncDate: syncedDate)
            let wellnessDevice = WellnessDevice(name: deviceName, lastSynced: lastSyncedString)
            linkedDevices.append(wellnessDevice)
        }

    }

    func configureLastSyncedDisplayString(syncDate: Date?) -> String {
        guard let date = syncDate else { return CommonStrings.Wda.Landing.NotSynced494 }

        if Calendar.current.isDateInToday(date) {
            let timeString = Localization.shortTimeFormatter.string(from:date)
            return   CommonStrings.Wda.LastSyncedToday458(timeString)
        } else if Calendar.current.isDateInYesterday(date) {
            let timeString = Localization.shortTimeFormatter.string(from:date)
            return CommonStrings.Wda.LastSyncedYesterday459(timeString)
        }

        let dateString = Localization.dayLongMonthLongYearShortTimeFormatter.string(from:date)
        return CommonStrings.Wda.LastSynced457(dateString)
    }

    func getSelectedDeviceDetailsFromRealm(deviceName: String) -> WDAMarket? {
        wdaRealm.refresh()
        return wdaRealm.wdaMarketFor(device:deviceName)
    }

    func configureSections() {
        sections.removeAll()
        unlinkedSection = WellnessDeviceSection(type: .available, devices: unlinkedDevices)
        linkedSection = WellnessDeviceSection(type: .linked, devices: linkedDevices)

        if linkedSection.devices.count > 0 {
            sections.append(self.linkedSection)
        }

        if unlinkedSection.devices.count > 0 {
            sections.append(self.unlinkedSection)
        }

        sections.append(WellnessDeviceSection(type: .other, devices: []))
    }

    func hasLinkedDevices() -> Bool {
        guard linkedDevices.count > 0 else { return false }
        return true
    }

    func hasUnlinkedDevices() -> Bool {
        guard unlinkedDevices.count > 0 else { return false }
        return true
    }

    func hasOnlyLinkedDevices() -> Bool {
        if hasLinkedDevices() && !hasUnlinkedDevices() {
            return true
        }
        return false
    }

    func hasOnlyUnlinkedDevices() -> Bool {
        if hasUnlinkedDevices() && !hasLinkedDevices() {
            return true
        }
        return false
    }

    func hasLinkedAndUnlinkedDevices() -> Bool {
        if hasUnlinkedDevices() && hasLinkedDevices() {
            return true
        }
        return false
    }

    func hasLinkedOrUnlinkedDevices() -> Bool {
        if hasUnlinkedDevices() || hasLinkedDevices() {
            return true
        }
        return false
    }

    func hasDeviceMappingData() -> Bool {
        wdaRealm.refresh()
        let devicesWithActivities = wdaRealm.allWDAAvailableDeviceActivitiesWithTypeRefs()
        guard devicesWithActivities.first != nil else {return false}
        return true
    }

    func hasPointsWithTiersData() -> Bool {
        coreRealm.refresh()
        let pointsWithTiersData = coreRealm.getAllPotentialPointsWithTiersEventType()
        guard pointsWithTiersData.first != nil else {return false}
        return true
    }

    // MARK: Networking

    public func performFetchDevices(forceUpdate: Bool = false, completion: @escaping (Error?, Bool?) -> Void) {
        self.fetchDevices(forceUpdate: forceUpdate, completion: { [weak self] error, isRealmEmpty in
            guard error == nil else {
                if isRealmEmpty == false {
                    self?.configureDeviceData()
                }
                completion(error, isRealmEmpty)
                return
            }
            self?.configureDeviceData()
            completion(nil, nil)
        })
    }

    public func performFetchDeviceActivityMappings(completion: @escaping (Error?, Bool) -> Void) {

        self.fetchAndProcessDeviceActivityMappings { error, recievedData in
            completion(error, recievedData)
        }
    }

    public func requestPotentialPointsWithTiers(completion: @escaping (Error?) -> Void) {
            let backgroundCoreRealm = DataProvider.newRealm()
            var eventTypeKeys: Array<Int> = Array<Int>()
            eventTypeKeys.append(EventTypeRef.DeviceDataUpload.rawValue)
            eventTypeKeys.append(EventTypeRef.BMI.rawValue)
            let partyId = backgroundCoreRealm.getPartyId()
            let tenantId = backgroundCoreRealm.getTenantId()
            let request = PotentialPointsWithTiersRequest(eventTypeKeys:eventTypeKeys)
            Wire.Events.getPotentialPointsWithTiers(tenantId: tenantId, partyId: partyId, request: request) { (error) in
                completion(error)
            }
    }
}
