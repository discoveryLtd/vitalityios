//
//  VIAWellnessDevicesLandingViewController.swift
//  VitalityActive
//
//  Created by Wilmar van Heerden on 2016/11/07.
//  Copyright © 2016 Glucode. All rights reserved.
//

import Foundation

import VIAUIKit
import VitalityKit
import VIACommon

enum WellnessAndAppsSource {
    case homescreen
    case activeRewards
}

class VIAWellnessDevicesLandingViewController: VIATableViewController, ImproveYourHealthTintable {

    private var viewModel: VIAWellnessDevicesLandingViewModel = VIAWellnessDevicesLandingViewModel()

    var source: WellnessAndAppsSource?

    var configureForActiveRewardsJourney: Bool = false

    // MARK: View lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = CommonStrings.Wda.Title414
        self.configureAppearance()
        self.configureTableView()
        self.showOnboardingIfRequired()
        self.registerForNotifications()
        loadData()
        configureForActiveRewardsJourneyIfNeeded()
    }

    public override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        hideBackButtonTitle()
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }

    func registerForNotifications() {
        NotificationCenter.default.addObserver(forName: .VIAWDADidFetchDevices, object: nil, queue: OperationQueue.main) { [weak self] notification in
            self?.fetchDeviceActivityMappingAndPointsWithTiersIfNeeded()
            self?.reloadWithLatestRealmData()
        }
        NotificationCenter.default.addObserver(forName: .VIAWDANeedToFetchUpdatedDevices, object: nil, queue: OperationQueue.main ) { [weak self] notification in
            self?.loadData()
        }
    }

    func configureForActiveRewardsJourneyIfNeeded() {
        if self.configureForActiveRewardsJourney {
            self.navigationItem.hidesBackButton = true
            let doneButton = UIBarButtonItem(title: CommonStrings.DoneButtonTitle53, style: .plain, target: self, action: #selector(done(_:)))
            self.navigationItem.rightBarButtonItem = doneButton
        }
    }

    func done(_ sender: Any?) {
        self.performSegue(withIdentifier: "unwindToActiveRewardsLandingView", sender: nil)
    }

    func fetchDeviceActivityMappingAndPointsWithTiersIfNeeded() {
        guard viewModel.hasDeviceMappingData() == false else { return }
        guard WDACache.wdaDataIsOutdated() == true else { return }
        
        //TODO : Swap with Lee's full screen loading indicator
        hideHUDFromView(view: view)
        showHUDOnView(view: view)
        viewModel.performFetchDeviceActivityMappings(completion: { [weak self] error, didRecieveData in
            guard error == nil else {
                self?.hideHUDFromView(view: self?.view)
                self?.handleErrorOccurred(error!, isRealmEmpty: false)
                return
            }
            guard didRecieveData == true, self?.viewModel.hasPointsWithTiersData() == false else {
                self?.hideHUDFromView(view: self?.view)
                return
            }
            self?.fetchPotentialPointsWithTiers()
        })
    }
    
    func fetchPotentialPointsWithTiers() {
        viewModel.requestPotentialPointsWithTiers(completion: { [weak self] error in
            self?.hideHUDFromView(view: self?.view)
            guard error == nil else {
                self?.handleErrorOccurred(error!, isRealmEmpty: false)
                return
            }
        })

    }

    func reloadWithLatestRealmData() {
        viewModel.configureDeviceData()
        tableView.reloadData()
    }

    func loadData(forceUpdate: Bool = false) {
        showHUDOnView(view: view)
        viewModel.performFetchDevices(forceUpdate: forceUpdate) { [weak self] error, realmIsEmpty in
            self?.hideHUDFromView(view: self?.view)

            guard error == nil else {
                self?.handleErrorOccurred(error!, isRealmEmpty: realmIsEmpty)
                return
            }

            self?.viewModel.configureDeviceData()
            self?.removeStatusView()
            self?.tableView.reloadData()
            self?.configureTableViewHeader()
        }
    }

    func handleErrorOccurred(_ error: Error, isRealmEmpty: Bool?) {
        let backendError = error as? BackendError ?? BackendError.other
        if let realmIsEmpty = isRealmEmpty, realmIsEmpty == true {
            let statusView = self.handleBackendErrorWithStatusView(backendError, tryAgainAction: { [weak self] in
                self?.loadData(forceUpdate: true)
            })
            configureStatusView(statusView)
        } else {
            viewModel.configureDeviceData()
            removeStatusView()
            tableView.reloadData()
            configureTableViewHeader()
            self.handleBackendErrorWithAlert(backendError, tryAgainAction: { [weak self] in
                self?.loadData(forceUpdate: true)
            })
        }
    }

    func showOnboardingIfRequired() {
        if !AppSettings.hasShownWellnessDevicesOnboarding() {
            self.performSegue(withIdentifier: "showOnboardingSegue", sender: self)
        }
    }

    func configureAppearance() {
        self.navigationItem.hidesBackButton = false
        navigationController?.makeNavigationBarTransparent()
    }

    func configureTableView() {
        self.tableView.register(VIATableViewSectionHeaderFooterView.nib(), forHeaderFooterViewReuseIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier)
        self.tableView.register(ActivityDetailSubtitleCell.nib(), forCellReuseIdentifier: ActivityDetailSubtitleCell.defaultReuseIdentifier)
        self.tableView.register(VIATableViewCell.nib(), forCellReuseIdentifier: VIATableViewCell.defaultReuseIdentifier)
        self.tableView.estimatedRowHeight = 75
    }

    func configureTableViewHeader() {
        if (viewModel.hasOnlyUnlinkedDevices()) {
            if let view = VIAWellnessDeviceAppsHeaderView.viewFromNib(owner: nil) {
                view.header = CommonStrings.Wda.Landing.HeaderTitle424
                view.content = CommonStrings.Wda.Landing.HeaderContent425
                view.action = nil
                view.backgroundColor = UIColor.tableViewBackground()
                self.tableView.tableHeaderView = view

                self.tableView.sizeHeaderViewToFit()
            }
        } else {
            removeHeaderView()
        }
    }

    func removeHeaderView() {
        self.tableView.tableHeaderView?.isHidden = true
        self.tableView.tableHeaderView = nil
    }

    // MARK: UITableView data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.sections.count
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var helpAndLearnMoreCount: Int
        if !VIAApplicableFeatures.default.hideHelpTab! {
            // Show help row.
            helpAndLearnMoreCount = 2
        } else {
            // Hide help row.
            helpAndLearnMoreCount = 1
        }

        if section == 0 {
            if viewModel.hasLinkedDevices() {
                return viewModel.linkedSection.devices.count
            } else if viewModel.hasOnlyUnlinkedDevices() {
                return viewModel.unlinkedSection.devices.count
            }
        }

        if section == 1 {
            if viewModel.hasLinkedAndUnlinkedDevices() {
                return viewModel.unlinkedSection.devices.count
            }
        }

        return helpAndLearnMoreCount
    }

    // MARK: UITableView delegate

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        if indexPath.section == 0 && viewModel.hasLinkedOrUnlinkedDevices() {
            handleDeviceTapped(indexPath: indexPath)
        }

        if indexPath.section == 1 && viewModel.hasLinkedAndUnlinkedDevices() {
            handleDeviceTapped(indexPath: indexPath)
        }

        if indexPath.section == tableView.numberOfSections - 1 && indexPath.row == 0 {
            learnMoreTapped()
            return
        } else if indexPath.section == tableView.numberOfSections - 1 && indexPath.row == 1 {
            helpTapped()
            return
        }
    }

    func handleDeviceTapped(indexPath: IndexPath) {
        if viewModel.sections[indexPath.section].devices[indexPath.row].name == CommonStrings.Wda.HealthApp455 {
            healthAppTapped()
            return
        }
        genericDeviceTapped()
        return
    }

    func genericDeviceTapped() {
        self.performSegue(withIdentifier: "segueShowGenericAppDetail", sender: nil)
    }

    func healthAppTapped() {
        self.performSegue(withIdentifier: "segueShowHealthAppDetail", sender: nil)
    }

    func learnMoreTapped() {
        self.performSegue(withIdentifier: "segueShowLearnMore", sender: nil)
    }

    func helpTapped() {
        self.performSegue(withIdentifier: "segueShowHelp", sender: nil)
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        if indexPath.section == 0 {
            if viewModel.hasLinkedDevices() {
                return configureLinkedSectionCell(indexPath: indexPath)
            } else if viewModel.hasOnlyUnlinkedDevices() {
                return configureUnlinkedSectionCell(indexPath: indexPath)
            }
        }

        if indexPath.section == 1 && viewModel.hasLinkedAndUnlinkedDevices() {
            return configureUnlinkedSectionCell(indexPath: indexPath)
        }

        return configureOtherItemCell(indexPath: indexPath)
    }

    func configureLinkedSectionCell(indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ActivityDetailSubtitleCell.defaultReuseIdentifier) as? ActivityDetailSubtitleCell else {
            return tableView.defaultTableViewCell()
        }
        
        self.configureCellForDevice(indexPath: indexPath, section: viewModel.linkedSection, cell: cell)
        return cell
    }

    func configureUnlinkedSectionCell(indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ActivityDetailSubtitleCell.defaultReuseIdentifier) as? ActivityDetailSubtitleCell else {
            return tableView.defaultTableViewCell()
        }
        
        self.configureCellForDevice(indexPath: indexPath, section: viewModel.unlinkedSection, cell: cell)
        return cell
    }

    func configureOtherItemCell(indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: VIATableViewCell.defaultReuseIdentifier) as? VIATableViewCell else {
            return tableView.defaultTableViewCell()
        }
        
        self.configureCellForOther(indexPath: indexPath, cell: cell)
        return cell
    }

    func configureCellForDevice(indexPath: IndexPath, section: WellnessDeviceSection, cell: ActivityDetailSubtitleCell) {
        cell.headingText = section.devices[indexPath.row].name
        if let lastSyncText = section.devices[indexPath.row].lastSynced {
            cell.contentText = lastSyncText
        }
        cell.accessoryType = .disclosureIndicator
    }

    func configureCellForOther(indexPath: IndexPath, cell: VIATableViewCell) {
        cell.separatorInset = UIEdgeInsets(top: 0, left: 55, bottom: 0, right: 0)
        switch indexPath.row {
        case 0:
            cell.labelText = CommonStrings.LearnMoreButtonTitle104
            cell.cellImage = VIAWellnessDevicesAsset.WellnessDevicesAndApps.wellnessDevicesLearnMore.image
            break
        case 1:
            cell.labelText = CommonStrings.HelpButtonTitle141
            cell.cellImage = VIAWellnessDevicesAsset.WellnessDevicesAndApps.wellnessDevicesHelp.image
            break
        default:
            debugPrint("")
        }

        cell.accessoryType = .disclosureIndicator
    }

    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {

        if section == 0 {
            if viewModel.hasLinkedDevices() {
                let view = configureLinkedSectionHeader()
                return view
            } else if viewModel.hasOnlyUnlinkedDevices() {
                let view = configureUnlinkedSectionHeader()
                return view
            }
        }

        if section == 1 && viewModel.hasLinkedAndUnlinkedDevices() {
            let view = configureUnlinkedSectionHeader()
            return view
        }
        return nil
    }

    func configureLinkedSectionHeader() -> UITableViewHeaderFooterView {
        guard let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier) as? VIATableViewSectionHeaderFooterView else {
            return tableView.defaultTableViewHeaderFooterView()
        }
        
        let labelText = CommonStrings.Wda.Landing.LinkedSectionTitle472
        view.configureWithExtraSpacing(labelText: labelText, extraTopSpacing: 0, extraBottomSpacing: 5)
        view.set(font: .title2Font())
        view.set(textColor: .night())
        return view
    }

    func configureUnlinkedSectionHeader() -> UITableViewHeaderFooterView {
        guard let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier) as? VIATableViewSectionHeaderFooterView else {
            return tableView.defaultTableViewHeaderFooterView()
        }
        
        let labelText = CommonStrings.Wda.Landing.AvailableLinkSectionTitle426
        view.configureWithExtraSpacing(labelText: labelText, extraTopSpacing: 25, extraBottomSpacing: 5)
        view.set(font: .title2Font())
        view.set(textColor: .night())
        return view
    }

    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if section == 0 && viewModel.hasLinkedDevices() {
            guard let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier) as? VIATableViewSectionHeaderFooterView else {
                return nil
            }
            
            let labelText = CommonStrings.Wda.Landing.SyncFooterText473
            view.configureWithExtraSpacing(labelText: labelText, extraTopSpacing: 0, extraBottomSpacing: linkedFooterBottomSpacing())
            view.set(textColor: .darkGrey())
            return view
        }
        return nil
    }

    func linkedFooterBottomSpacing() -> Int {
        var bottomSpacing = 10
        if viewModel.hasUnlinkedDevices() {
            bottomSpacing = 0
        }
        return bottomSpacing
    }

    // MARK: Navigation

    @IBAction func unWindToDevicesLanding(segue: UIStoryboardSegue) {
        debugPrint("unWindToDevicesLandingWithSegue:")
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

        if segue.identifier == "segueShowGenericAppDetail" {
            if let indexPath = self.tableView.indexPathForSelectedRow {
                let destinationViewController = segue.destination as? VIAWellnessDeviceDetailViewController
                let selectedDeviceName = viewModel.sections[indexPath.section].devices[indexPath.row].name
                let selectedDeviceDetails = viewModel.getSelectedDeviceDetailsFromRealm(deviceName: selectedDeviceName)
                destinationViewController?.viewModel.deviceName = selectedDeviceDetails?.partner?.device
            }
        }

        if segue.identifier == "showOnboardingSegue" {
            if let onboardingNavigationController = segue.destination as? UINavigationController {
                if let onboardingViewController = onboardingNavigationController.childViewControllers.first as? VIAWellnessDevicesOnboardingViewController {
                    onboardingViewController.source = self.source
                }
            }
        }
    }

}
