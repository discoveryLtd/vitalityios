import UIKit
import VitalityKit

class HealthKitDeviceAndAppsDataPrivacyPolicyViewController: DeviceAndAppsDataPrivacyPolicyViewController {

    override func awakeFromNib() {
        super.awakeFromNib()

        self.viewModel = CustomTermsConditionsViewModel(articleId: AppConfigFeature.contentId(for: .WDADSConsentContent))
    }

    override func rightButtonTapped(_ sender: UIBarButtonItem?) {
        self.performSegue(withIdentifier: "showHealthKitOnboarding", sender: nil)
    }

}
