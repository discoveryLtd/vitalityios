import SafariServices
import UIKit

import VitalityKit
import VIAUIKit
import VIACommon
import VIAUtilities

class DeviceAndAppsDataPrivacyPolicyViewController: CustomTermsConditionsViewController {
    
    var deviceName = ""

    // MARK: Properties

    var completion: () -> Void = {
        debugPrint("Lets detail screen know that user accepted the privacy policy")
    }

    // MARK: Lifecycle

    override func awakeFromNib() {
        super.awakeFromNib()

        viewModel = CustomTermsConditionsViewModel(articleId: AppConfigFeature.contentId(for: .WDADSConsentContent))
    }

    override func configureAppearance() {
        super.configureAppearance()
        navigationController?.makeNavigationBarInvisible()
        navigationController?.navigationBar.isHidden = true
    }

    // MARK: Action buttons

    override func leftButtonTapped(_ sender: UIBarButtonItem?) {
        performSegue(withIdentifier: "unwindToDeviceDetail", sender: nil)
    }

    override func rightButtonTapped(_ sender: UIBarButtonItem?) {
        showHUDOnWindow()
        let events: [EventTypeRef] = [EventTypeRef.DPPolicyAgree]

        viewModel?.agreeToTermsAndConditions(with: events, completion: { [weak self] (error) in
            self?.hideHUDFromWindow()
            guard error == nil else {
                self?.handleErrorOccurred(error)
                return
            }
            self?.completion()
        })
    }

    func handleErrorOccurred(_ error: Error?) {
        
        hideHUDFromWindow()
        
        let alertController = UIAlertController(title: CommonStrings.Wda.DeviceDetail.SyncFailedDialogTitle509, message: CommonStrings.Wda.DeviceDetail.SyncFailedDialogMessage513(deviceName), preferredStyle: .alert)
        let tryAgainAction = UIAlertAction(title: CommonStrings.TryAgainButtonTitle43, style: .default, handler: { [weak self] action in
            self?.rightButtonTapped(nil)
        })
        let cancelAction = UIAlertAction(title: CommonStrings.CancelButtonTitle24, style: .cancel, handler: nil)
        
        alertController.addAction(tryAgainAction)
        alertController.addAction(cancelAction)
        
        present(alertController, animated: true, completion: nil)
    }

    // MARK: Navigation

    @IBAction public func unwindToDeviceDetail() {
        performSegue(withIdentifier: "unwindToDeviceDetail", sender: nil)
    }

    override func returnToPreviousView() {
        performSegue(withIdentifier: "unwindToDeviceDetail", sender: nil)
    }
}
