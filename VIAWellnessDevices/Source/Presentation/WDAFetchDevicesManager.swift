//
//  DeviceLinker.swift
//  VitalityActive
//
//  Created by Lionel Camacho on 2017/07/06.
//  Copyright © 2017 Glucode. All rights reserved.
//
import Foundation
import VitalityKit
import VIAUtilities

public protocol WDAFetchDevicesManager: class {
    func fetchDevices(forceUpdate: Bool, completion: @escaping (Error?, Bool?) -> Void)
}

public extension WDAFetchDevicesManager {

    public func fetchDevices(forceUpdate: Bool = false, completion: @escaping (Error?, Bool?) -> Void) {

        let coreRealm = DataProvider.newRealm()
        let partyId = coreRealm.getPartyId()
        let tenantId = coreRealm.getTenantId()
        let wdaRealm = DataProvider.newWDARealm()

        Wire.Events.getDevices(tenantId: tenantId, partyId: partyId) { response, error in
            guard error == nil else {
                completion(error, wdaRealm.isEmpty)
                return
            }
            completion(nil, wdaRealm.isEmpty)
            return
        }
    }

}
