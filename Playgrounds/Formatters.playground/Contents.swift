//: Playground - noun: a place where people can play

import UIKit

////////////////////////////////////////////////////////////////////

let locale = Locale(identifier: "ja_JP")

let number: NSNumber = 12000.456

let decimalFormatter = NumberFormatter()
decimalFormatter.locale = locale
decimalFormatter.numberStyle = .decimal
decimalFormatter.string(from: number as NSNumber)

let integerFormatter = NumberFormatter()
integerFormatter.locale = locale
integerFormatter.numberStyle = .decimal
integerFormatter.maximumFractionDigits = 0
integerFormatter.string(from: number as NSNumber)