import UIKit
import VIAUIKit
import VitalityKit
import VIACommon

public class MyHealthCardUpdatedTableViewCell: UITableViewCell, Nibloadable {

    @IBOutlet weak var myHealthImage: UIImageView!
    @IBOutlet weak var myHealthUpdatedLabel: UILabel!
    @IBOutlet weak var myHealthUpdatedBlurbLabel: UILabel!

    public override func awakeFromNib() {
        super.awakeFromNib()
    }

    public func setupViewFromCard(at index: Int!) {
        myHealthImage.image = VIAMyHealthAsset.myHealthCircle.image

        myHealthUpdatedLabel.text = CommonStrings.MyHealth.VitalityAgeMyHealthUpdatedTitle619
        myHealthUpdatedLabel.font = UIFont.title3Font()

        var updatedBlurbText: String! = ""
        if index == 0 || index == -1 {
            updatedBlurbText = CommonStrings.MyHealth.VitalityAgeMyHealthUpdatedDescription620
        } else {
            updatedBlurbText = CommonStrings.Mwb.ResultMessage1231
        }
        myHealthUpdatedBlurbLabel.text = updatedBlurbText
        
        if VIAApplicableFeatures.default.shouldSkipMyHealthLandingScreen(){
            // VACR181
            myHealthUpdatedBlurbLabel.text = CommonStrings.MyHealth.VitalityAgeMyHealthUpdatedDescription620
        }
    }
}
