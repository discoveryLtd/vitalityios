import Foundation
import VitalityKit
import VIAUIKit
import VIAUtilities
import VitalityKit

public struct VALearnMoreRow {
    let header: String
    let content: String
    let asset: MYHAsset?
    let headerFont: UIFont
    let contentFont: UIFont

    init(header: String, content: String, asset: MYHAsset?, headerFont: UIFont, contentFont: UIFont) {
        self.header = header
        self.content = content
        self.asset = asset
        self.headerFont = headerFont
        self.contentFont = contentFont
    }
}

enum MyHealthLearnMoreSections: Int, EnumCollection {
    case Content = 0
    case Disclaimer = 1
}

public protocol VitalityAgeLearnMoreViewModel{
    func numberOfSections() -> Int
    func numberOfRows() -> Int
    func learnMoreRowAt(index: Int) -> VALearnMoreRow?
}

public class VitalityAgeLearnMoreViewModelImpl: VitalityAgeLearnMoreViewModel {
    let vitalityAgeAttribute = HealthInformationRepository().getHealthAttributeBy(attributeType: PartyAttributeTypeRef.VitalityAge)
    var vitalityAgeStatus : PartyAttributeFeedbackRef
    let learnMoreRows : [VALearnMoreRow]

    public func numberOfSections() -> Int {
        return MyHealthLearnMoreSections.allValues.count
    }

    public func numberOfRows() -> Int {
        return learnMoreRows.count
    }
    
    static func buildGoodLearnMore(ageVariance: Int) -> [VALearnMoreRow] {
        var content = CommonStrings.MyHealth.VitalityAgeHealthyDescriptionLong2103
        
        if AppSettings.getAppTenant() == .UKE {
            content = CommonStrings.MyHealth.SameVitalityAgeAsRealAge1151
        }
        
        return [
            VALearnMoreRow(
                header: CommonStrings.MyHealth.LearnMoreGoodTitle756,
                //content: CommonStrings.MyHealth.VitalityAgeHealthyDescriptionLong2103,
                content: content,
                asset: nil,
                headerFont: UIFont.title1Font(),
                contentFont: UIFont.subheadlineFont()
                ),
            VALearnMoreRow(
                header: CommonStrings.MyHealth.LearnMoreHowDoIMaintainTitle758,
                content: CommonStrings.MyHealth.LearnMoreHowDoIMaintainContent759,
                asset: MYHAsset.understand,
                headerFont: UIFont.title2Font(),
                contentFont: UIFont.bodyFont()
                ),
            VALearnMoreRow(
                header: CommonStrings.MyHealth.LearnMoreHowIsThisCalculatedTitle746,
                content: CommonStrings.MyHealth.LearnMoreHowIsThisCalculatedContent747,
                asset: MYHAsset.calculate,
                headerFont: UIFont.title2Font(),
                contentFont: UIFont.bodyFont()
                )
        ]
    }

    
    static func buildTooLowLearnMore(ageVariance: Int) -> [VALearnMoreRow] {
        return [
            VALearnMoreRow(
                header: CommonStrings.MyHealth.LearnMoreGoodTitle756,
                content: CommonStrings.MyHealth.VitalityAgeLowerThanActualAge1152(NSNumber(value: ageVariance).stringValue),
                asset: nil,
                headerFont: UIFont.title1Font(),
                contentFont: UIFont.subheadlineFont()
            ),
            VALearnMoreRow(
                header: CommonStrings.MyHealth.LearnMoreHowDoIMaintainTitle758,
                content: CommonStrings.MyHealth.LearnMoreHowDoIMaintainContent759,
                asset: MYHAsset.understand,
                headerFont: UIFont.title2Font(),
                contentFont: UIFont.bodyFont()
            ),
            VALearnMoreRow(
                header: CommonStrings.MyHealth.LearnMoreHowIsThisCalculatedTitle746,
                content: CommonStrings.MyHealth.LearnMoreHowIsThisCalculatedContent747,
                asset: MYHAsset.calculate,
                headerFont: UIFont.title2Font(),
                contentFont: UIFont.bodyFont()
            )
        ]
    }
    
    static func buildTooHighLearnMore(ageVariance: Int) -> [VALearnMoreRow] {
        
        return [
            VALearnMoreRow(
                header: CommonStrings.MyHealth.LearnMoreTooHighTitle752,
                content: CommonStrings.MyHealth.LearnMoreTooHighContent753(NSNumber(value: ageVariance).stringValue),
                asset: nil,
                headerFont: UIFont.title1Font(),
                contentFont: UIFont.subheadlineFont()
                ),
            VALearnMoreRow(
                header: CommonStrings.MyHealth.LearnMoreHowDoILowerTitle754,
                content: CommonStrings.MyHealth.LearnMoreHowDoILowerContent755,
                asset: MYHAsset.lowerAge,
                headerFont: UIFont.title2Font(),
                contentFont: UIFont.bodyFont()
                ),
            VALearnMoreRow(
                header: CommonStrings.MyHealth.LearnMoreHowIsThisCalculatedTitle746,
                content: CommonStrings.MyHealth.LearnMoreHowIsThisCalculatedContent747,
                asset: MYHAsset.calculate,
                headerFont: UIFont.title2Font(),
                contentFont: UIFont.bodyFont()
                )
        ]
    }

    static func buildUnknownLearnMore() -> [VALearnMoreRow] {
        return [
            VALearnMoreRow(
                header: CommonStrings.MyHealth.LearnMoreTitle744,
                content: CommonStrings.MyHealth.LearnMoreSubtitle745,
                asset: nil,
                headerFont: UIFont.title1Font(),
                contentFont: UIFont.subheadlineFont()
                ),
            VALearnMoreRow(
                header: CommonStrings.MyHealth.LearnMoreHowIsThisCalculatedTitle746,
                content: CommonStrings.MyHealth.LearnMoreHowIsThisCalculatedContent747,
                asset: MYHAsset.calculate,
                headerFont: UIFont.title2Font(),
                contentFont: UIFont.bodyFont()
                ),
            VALearnMoreRow(
                header: CommonStrings.MyHealth.LearnMoreFindYourVitalityAgeTitle748,
                content: CommonStrings.MyHealth.LearnMoreFindYourVitalityAgeContent749,
                asset: MYHAsset.findAge,
                headerFont: UIFont.title2Font(),
                contentFont: UIFont.bodyFont()
                ),
            VALearnMoreRow(
                header: CommonStrings.MyHealth.LearnMoreUnderstandYourHealthTitle750,
                content: CommonStrings.MyHealth.LearnMoreUnderstandYourHealthContent751,
                asset: MYHAsset.understand,
                headerFont: UIFont.title2Font(),
                contentFont: UIFont.bodyFont()
                )
        ]
    }
    
    static func buildOutdatedLearnMore(ageVariance: Int) -> [VALearnMoreRow] {
        return [
            VALearnMoreRow(
                header: CommonStrings.MyHealth.LearnMoreYourVitalityAgeIsOutdated1100,
                content: CommonStrings.MyHealth.LearnMoreVitalityAgeOutdatedDescription1101("\(ageVariance)"),
                asset: nil,
                headerFont: UIFont.title1Font(),
                contentFont: UIFont.subheadlineFont()
            ),
            VALearnMoreRow(
                header: CommonStrings.MyHealth.LearnMoreHowIsThisCalculatedTitle746,
                content: CommonStrings.MyHealth.LearnMoreHowIsThisCalculatedContent747,
                asset: MYHAsset.calculate,
                headerFont: UIFont.title2Font(),
                contentFont: UIFont.bodyFont()
            ),
            VALearnMoreRow(
                header: CommonStrings.MyHealth.LearnMoreFindYourVitalityAgeTitle748,
                content: CommonStrings.MyHealth.LearnMoreFindYourVitalityAgeContent749,
                asset: MYHAsset.findAge,
                headerFont: UIFont.title2Font(),
                contentFont: UIFont.bodyFont()
            ),
            VALearnMoreRow(
                header: CommonStrings.MyHealth.LearnMoreUnderstandYourHealthTitle750,
                content: CommonStrings.MyHealth.LearnMoreUnderstandYourHealthContent751,
                asset: MYHAsset.understand,
                headerFont: UIFont.title2Font(),
                contentFont: UIFont.bodyFont()
            )
        ]
    }
    
    static func buildLearnMoreRows(status: PartyAttributeFeedbackRef, ageVariance: Int) -> [VALearnMoreRow] {
        switch status {
            case .VAgeBelow:
                return VitalityAgeLearnMoreViewModelImpl.buildTooLowLearnMore(ageVariance: ageVariance)
            case .VAgeHealthy:
                return VitalityAgeLearnMoreViewModelImpl.buildGoodLearnMore(ageVariance: ageVariance)
            case .VAgeAbove:
                return VitalityAgeLearnMoreViewModelImpl.buildTooHighLearnMore(ageVariance: ageVariance)
            case .VAgeOutdated:
                return VitalityAgeLearnMoreViewModelImpl.buildOutdatedLearnMore(ageVariance: ageVariance)
            default:
                return VitalityAgeLearnMoreViewModelImpl.buildUnknownLearnMore()
        }
    }

    public func learnMoreRowAt(index: Int) -> VALearnMoreRow? {
        guard let learnMoreRow = learnMoreRows.element(at: index) else { return nil }
        return learnMoreRow
    }

    public init() {
        if let vitalityAge = vitalityAgeAttribute {
            vitalityAgeStatus = PartyAttributeFeedbackRef.vitalityAgeStatus(vitalityAge: vitalityAge)
        } else {
            vitalityAgeStatus = PartyAttributeFeedbackRef.Unknown
        }
       
        var age: Int = 0
        
        if let vitalityAge = vitalityAgeAttribute {
            if let ageVarianceMetadata = vitalityAge.metadataWith(keys: [.VAgeVariance]) {
                if !String.isNilOrEmpty(string: ageVarianceMetadata.value) {
                    if let variance = Int(ageVarianceMetadata.value!) {
                        age = variance
                    }
                }
            }
        }
        learnMoreRows = VitalityAgeLearnMoreViewModelImpl.buildLearnMoreRows(status: vitalityAgeStatus, ageVariance: age)
    }
}
