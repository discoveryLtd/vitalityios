//
//  VitalityAgeOnboardingViewModel.swift
//  VIAMyHealth
//
//  Created by Dexter Anthony Ambrad on 7/23/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import VitalityKit
import VIAUIKit
import VitalityKit

public class VitalityAgeOnboardingViewModel: OnboardingViewModel {
    
    public var buttonTitle: String {
        return CommonStrings.GenericGotItButtonTitle131
    }
    
    public var labelTextColor: UIColor {
        return UIColor.night()
    }
    
    public var mainButtonTint: UIColor {
        return UIColor.primaryColor()
    }
    
    public var mainButtonHighlightedTextColor: UIColor? {
        return UIColor.day()
    }
    
    public var gradientColor: Color {
        return .none
    }
    
    public var heading: String {
        // TO DO: change when available
        return CommonStrings.MyHealth.VitalityAgeScreenTitle613
    }
    
    public var alternateButtonTitle: String? {
        return nil
    }
    
    public var imageTint: UIColor? {
        return UIColor.primaryColor()
    }
    
    public var contentItems: [OnboardingContentItem] {
        var items: [OnboardingContentItem] = [OnboardingContentItem]()
        
        items.append(OnboardingContentItem(heading: CommonStrings.MyHealth.OnboardingSection1Title628,
                                           content: CommonStrings.MyHealth.OnboardingSection1Description629,
                                           image: UIImage.templateImage(asset: MYHAsset.questionMarkSmall)))
        items.append(OnboardingContentItem(heading: CommonStrings.MyHealth.OnboardingSection2Title632,
                                           content: CommonStrings.MyHealth.OnboardingSection2Description633,
                                           image: UIImage.templateImage(asset: MYHAsset.manageHealthSmall)))
        items.append(OnboardingContentItem(heading: CommonStrings.MyHealth.OnboardingSection3Title634,
                                           content: CommonStrings.MyHealth.OnboardingSection3Description635,
                                           image: UIImage.templateImage(asset: MYHAsset.tips)))
        return items
    }
    
    public func setOnboardingHasShown() {
        AppSettings.setHasShownMyHealthOnboarding()
    }
}
