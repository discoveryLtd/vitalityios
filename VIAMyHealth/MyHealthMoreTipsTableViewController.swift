import Foundation
import UIKit
import VIAUIKit
import VitalityKit
import VIAUtilities
import VIACommon

public class MyHealthMoreTipsTableViewController: VIATableViewController {
    
    public var reportedHealthAttribute: GMIReportedHealthAttribute?
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        
        self.configureTable()
    }
    
    func configureTable() {

        self.tableView.register(MyHealthAttributePropertyTableViewCell.nib(), forCellReuseIdentifier: MyHealthAttributePropertyTableViewCell.defaultReuseIdentifier)
        
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 150
        
        self.tableView.backgroundColor = UIColor.tableViewBackground()
    }
    
    override public func numberOfSections(in tableView: UITableView) -> Int {
        if reportedHealthAttribute != nil {
            return 1
        }
        return 0
    }
    
    override public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let healthAttribute = reportedHealthAttribute {
            return self.attributeTips(reportedHealthAttribute: healthAttribute).count
        }
        return 0
    }
    
    func attributeTips(reportedHealthAttribute: GMIReportedHealthAttribute) -> [GMIReportedFeedbackTip] {
        var tips = [GMIReportedFeedbackTip]()
        
        // ignore the first 3
        self.reportedHealthAttribute?.feedbacks.forEach({ (feedback) in
            tips.append(contentsOf: feedback.feedbackTips)
        })
        
        let count = tips.count
        
        return Array(tips.suffix(count - 3))
    }
    
    
    public override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        if let healthAttribute = self.reportedHealthAttribute {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: MyHealthAttributePropertyTableViewCell.defaultReuseIdentifier, for: indexPath) as? MyHealthAttributePropertyTableViewCell else {return tableView.defaultTableViewCell()}
            
            cell.setupViewAsTip(feedbackTip: self.attributeTips(reportedHealthAttribute: healthAttribute)[indexPath.row])
            cell.accessoryType = .disclosureIndicator
            
            return cell
        }

        return tableView.defaultTableViewCell()
    }
    
    public override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "showTipDetailsFromMoreTips", sender: self)
    }
    
    public override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showTipDetailsFromMoreTips", let controller = segue.destination as? VIAWebContentViewController {
            
            guard let cell = self.tableView.cellForRow(at: self.tableView.indexPathForSelectedRow!) as? MyHealthAttributePropertyTableViewCell else {return}
            
            if let tip = cell.feedbackTip {
                let tipIdentifier = "\(tip.typeKey.rawValue)-\(tip.typeCode!.lowercased())"
                let webContentViewModel = VIAWebContentViewModel(articleId: tipIdentifier)

                controller.viewModel = webContentViewModel
                controller.title = CommonStrings.MyHealth.DetailMainTitleTip1068
            }
        }
    }
}
