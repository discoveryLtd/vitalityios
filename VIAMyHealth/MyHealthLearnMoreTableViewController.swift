import Foundation
import VitalityKit
import VIAUIKit
import VitalityKit
import VIACommon

public final class MyHealthLearnMoreTableViewController: VIATableViewController {

    public var viewModel:VitalityAgeLearnMoreViewModel?
    public var cardIndex: Int! = -1

    override public func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = CommonStrings.LearnMoreButtonTitle104
        
        configureTableView()
    }
    
    override public func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.hideBackButtonTitle()        
    }

    
    func configureTableView() {
        self.tableView.register(VIAGenericContentCell.nib(), forCellReuseIdentifier: VIAGenericContentCell.defaultReuseIdentifier)
        self.tableView.register(VIATableViewCell.self, forCellReuseIdentifier: VIATableViewCell.defaultReuseIdentifier)
        self.tableView.estimatedRowHeight = 75
        self.tableView.sectionHeaderHeight = 0
        self.tableView.sectionFooterHeight = 0
        self.tableView.estimatedSectionHeaderHeight = 0
        self.tableView.estimatedSectionFooterHeight = 0
        self.tableView.contentInset = UIEdgeInsets(top: 25, left: 0, bottom: 0, right: 0)
        self.tableView.backgroundColor = .white
        self.tableView.tableFooterView  = UIView() /* Set this to remove the preceeding empty rows in the table. */
    }
    
    // MARK: - UITableView datasource

    override public func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel?.numberOfSections() ?? 0
    }
    
    override public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let learnMoreSection = MyHealthLearnMoreSections(rawValue: section)
        
        if learnMoreSection == .Content {
            return viewModel?.numberOfRows() ?? 0
        }
        else if learnMoreSection == .Disclaimer {
            return 1
        }
        return 0
        
    }
    
    // MARK: UITableView delegate
    
    override public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let learnMoreSection = MyHealthLearnMoreSections(rawValue: indexPath.section)
        
        if learnMoreSection == .Disclaimer {
            viewDisclaimer()
        }
    }
    
    override public func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {

        let learnMoreSection = MyHealthLearnMoreSections(rawValue: indexPath.section)
        
        if learnMoreSection == .Content {

            if indexPath.row == viewModel?.numberOfRows() ?? 0 - 1 {
                cell.preservesSuperviewLayoutMargins = false
                cell.separatorInset = UIEdgeInsets.zero
                cell.layoutMargins = UIEdgeInsets.zero
            } else {
                cell.separatorInset = UIEdgeInsetsMake(0, max(UIScreen.main.bounds.size.width, UIScreen.main.bounds.size.height), 0, 0)
            }
        }
    }
    
    override public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let learnMoreSection = MyHealthLearnMoreSections(rawValue: indexPath.section)
        
        if learnMoreSection == .Content {
            return self.configureContentCell(indexPath)
        }
        else {
            return configureDisclaimerItemCell(indexPath)
        }
        
    }

    func configureContentCell(_ indexPath: IndexPath) -> UITableViewCell {
        
        let cell = self.tableView.dequeueReusableCell(withIdentifier: VIAGenericContentCell.defaultReuseIdentifier, for: indexPath)
        
        if let genericContentCell = cell as? VIAGenericContentCell {
         
            genericContentCell.isUserInteractionEnabled = false
            genericContentCell.header = ""
            genericContentCell.content = ""
            
            if let learnMoreRow = viewModel?.learnMoreRowAt(index: indexPath.row) {
                if let asset = learnMoreRow.asset {
                    genericContentCell.cellImage = UIImage.templateImage(asset: asset)
                    genericContentCell.cellImageTintColor = UIColor.primaryColor()
                }
                genericContentCell.customHeaderFont = learnMoreRow.headerFont
                genericContentCell.customContentFont = learnMoreRow.contentFont
                
                genericContentCell.header = learnMoreRow.header
                genericContentCell.content = learnMoreRow.content
            }
            
            return genericContentCell
        }
        
        return cell
    }
    
    func configureDisclaimerItemCell(_ indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: VIATableViewCell.defaultReuseIdentifier, for: indexPath)
            
        if let tableViewCell = cell as? VIATableViewCell {

            tableViewCell.textLabel?.text = CommonStrings.DisclaimerTitle265
            
            tableViewCell.imageView?.image = UIImage.templateImage(asset: MYHAsset.generalDocsSmall)
            tableViewCell.imageView?.tintColor = UIColor.primaryColor()
            
            tableViewCell.accessoryType = .disclosureIndicator
            tableViewCell.selectionStyle = .none
            
            return tableViewCell
        }
        

        return cell
    }
    
    override public func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDisclaimer", let controller = segue.destination as? VIAWebContentViewController {
            
            switch cardIndex {
            case 1:
                let webContentViewModel = VIAWebContentViewModel(articleId: "vna-disclaimer-content")
                controller.viewModel = webContentViewModel
            case 2:
                let webContentViewModel = VIAWebContentViewModel(articleId: "mwb-disclaimer-content")
                controller.viewModel = webContentViewModel
            case 3:
                let webContentViewModel = VIAWebContentViewModel(articleId: "mwb-disclaimer-content")
                controller.viewModel = webContentViewModel
            case 4:
                let webContentViewModel = VIAWebContentViewModel(articleId: "mwb-disclaimer-content")
                controller.viewModel = webContentViewModel
            default:
                let webContentViewModel = VIAWebContentViewModel(articleId: AppConfigFeature.contentId(for: .VHRDisclaimerContent))
                controller.viewModel = webContentViewModel
            }
            
            controller.title = CommonStrings.DisclaimerTitle265
        }
    }
    
    func viewDisclaimer() {
        self.performSegue(withIdentifier: "showDisclaimer", sender: self)
    }
}
