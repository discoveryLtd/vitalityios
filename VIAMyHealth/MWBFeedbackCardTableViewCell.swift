//
//  MWBFeedbackCardTableViewCell
//  VitalityActive
//
//  Created by Val Tomol on 28/02/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import UIKit
import VIAUIKit
import VitalityKit
import VitalityKit
import VIAUtilities
import Foundation
import VIACommon
import Lottie

public class MWBFeedbackCardTableViewCell: UITableViewCell, Nibloadable {
    // Properties.
    @IBOutlet weak var mwbHeaderLabel: UILabel!
    @IBOutlet weak var mwbCardLabel: UILabel!
    @IBOutlet weak var backgroundContainer: UIView!
    @IBOutlet weak var animationView: UIView!
    
    @IBOutlet weak var mwbStatusImage: UIImageView!
    @IBOutlet weak var mwbStatusLabel: UILabel!
    @IBOutlet weak var mwbBlurbLabel: UILabel!
    
    @IBOutlet weak var mwbLearnMoreButton: UIImageView!
    
    public var cardItemRow: Int! = 0
    
    public override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    public var mwbHealthAttribute: GMIReportedHealthAttribute? {
        didSet {
            setupView(mwbResult: mwbHealthAttribute)
        }
    }
    
    public override func layoutSubviews() {
        switch cardItemRow {
        case MyHealthCardItem.StressorFeedback.rawValue:
            self.drawStressorGradient()
        case MyHealthCardItem.PsychologicalFeedback.rawValue:
            self.drawPsychologicalGradient()
        case MyHealthCardItem.SocialFeedback.rawValue:
            self.drawSocialGradient()
        default:
            return
        }
        
        super.layoutSubviews()
    }
    
    public func isMWBUnknown() -> Bool {
        if let mwbAttr = mwbHealthAttribute {
            return PartyAttributeFeedbackRef.mwbStatus(mwb: mwbAttr) == .MWBUnknown
        }
        return true
    }
    
    func setupView(mwbResult: GMIReportedHealthAttribute?) {
        self.selectionStyle = .none
        self.backgroundContainer.layer.cornerRadius = 8
        self.backgroundColor = UIColor.tableViewBackground()
        
        if let result = mwbResult, let resultValue = result.value {
            self.setupCardStatus(status: PartyAttributeFeedbackRef.mwbStatus(mwb: result), statusValue: resultValue, mwbAttribute: result)
        } else {
            self.setupCardStatus(status: .MWBUnknown, statusValue: "", mwbAttribute: nil)
        }
        
        self.mwbHeaderLabel.font = UIFont.bodyFont()
        self.mwbHeaderLabel.textColor = UIColor.feedbackViewBackground()
    }
    
    func showUnknownTextWithString(_ text: String) {
        self.mwbCardLabel.text = text
        self.mwbCardLabel.font = UIFont.title1Font()
        self.mwbCardLabel.numberOfLines = 2
    }
    
    func setupCardStatus(status: PartyAttributeFeedbackRef, statusValue: String, mwbAttribute: GMIReportedHealthAttribute?) {
        self.setupStatusValueLabel(status: status)
        
        self.setupAnimatedBackgroudView(status: status)
        
        self.mwbStatusImage.image = status.mwbStatusImage()
        self.mwbStatusImage.tintColor = UIColor.feedbackViewBackground()
        
        self.mwbStatusLabel.text = {
            if let useMyHealthBackendFeedback = Bundle.main.object(forInfoDictionaryKey: "VIAUseMyHealthBackendFeedback") as? Bool, useMyHealthBackendFeedback {
                return status.mwbSummaryStatusText(mwbAttribute: mwbAttribute)
            } else {
                return status.mwbSummaryStatusText()
            }
        }()
        self.mwbStatusLabel.font = UIFont.title3Font()
        self.mwbStatusLabel.textColor = UIColor.feedbackViewBackground()
        
        self.mwbBlurbLabel.text = {
            if let useMyHealthBackendFeedback = Bundle.main.object(forInfoDictionaryKey: "VIAUseMyHealthBackendFeedback") as? Bool, useMyHealthBackendFeedback {
                return status.mwbBlurbSummaryFromBackEnd(mwbAttribute: mwbAttribute)
            } else {
                return status.mwbBlurbSummary(mwbAttribute: mwbAttribute)
            }
        }()
        
        self.mwbBlurbLabel.font = UIFont.subheadlineFont()
        self.mwbBlurbLabel.textColor = UIColor.feedbackViewBackground()
        
        self.mwbBlurbLabel.isHidden = status == .MWBUnknown
        self.mwbLearnMoreButton.isHidden = status != .MWBUnknown
    }
    
    func setupAnimatedBackgroudView(status: PartyAttributeFeedbackRef) {
        let animation = LOTAnimationView(name: status.mwbStatusAnimationName())
        animation.loopAnimation = true
        animation.frame.origin.x = 0
        animation.frame.origin.y = -200
        animation.frame.size.width = animationView.frame.width
        animation.contentMode = .scaleAspectFit
        self.animationView.subviews.forEach({ $0.removeFromSuperview() })
        self.animationView.addSubview(animation)
        animation.play()
    }
    
    func setupStatusValueLabel(status: PartyAttributeFeedbackRef) {
        if status == .MWBUnknown {
            self.showUnknownTextWithString(CommonStrings.Mwb.Result.CardTitle1229)
        } else {
            self.showUnknownTextWithString("")
        }
        
        self.mwbCardLabel.textColor = UIColor.feedbackViewBackground()
    }
    
    func drawStressorGradient() {
        let top = UIColor.stressorBlueTop()
        let bottom = UIColor.stressorBlueBottom()
        self.backgroundContainer.backgroundColor = UIColor(gradientStyle: .topToBottom, withFrame: frame, andColors: [top, bottom])
    }
    
    func drawPsychologicalGradient() {
        let top = UIColor.psychologicalBlueTop()
        let bottom = UIColor.psychologicalBlueBottom()
        self.backgroundContainer.backgroundColor = UIColor(gradientStyle: .topToBottom, withFrame: frame, andColors: [top, bottom])
    }
    
    func drawSocialGradient() {
        let top = UIColor.socialBlueTop()
        let bottom = UIColor.socialBlueBottom()
        self.backgroundContainer.backgroundColor = UIColor(gradientStyle: .topToBottom, withFrame: frame, andColors: [top, bottom])
    }
}
