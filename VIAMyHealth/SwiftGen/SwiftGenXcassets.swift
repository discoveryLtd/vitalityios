// Generated using SwiftGen, by O.Halligon — https://github.com/SwiftGen/SwiftGen

#if os(OSX)
import AppKit.NSImage
public typealias VIAMyHealthColor = NSColor
public typealias VIAMyHealthImage = NSImage
#elseif os(iOS) || os(tvOS) || os(watchOS)
import UIKit.UIImage
public typealias VIAMyHealthColor = UIColor
public typealias VIAMyHealthImage = UIImage
#endif

// swiftlint:disable file_length

public typealias VIAMyHealthAssetType = VIAMyHealthImageAsset

public struct VIAMyHealthImageAsset: Equatable {
	fileprivate var name: String

	public var templateImage: VIAMyHealthImage {
		let bundle = Bundle(for: BundleToken.self)
		#if os(iOS) || os(tvOS)
		let image = VIAMyHealthImage(named: name, in: bundle, compatibleWith: nil)?.withRenderingMode(.alwaysTemplate)
		#elseif os(OSX)
		let image = bundle.image(forResource: name).withRenderingMode(.alwaysTemplate)
		#elseif os(watchOS)
		let image = VIAMyHealthImage(named: name).withRenderingMode(.alwaysTemplate)
		#endif
		guard let result = image else { fatalError("Unable to load template image named \(name).") }
		return result
	}

	public var image: VIAMyHealthImage {
		let bundle = Bundle(for: BundleToken.self)
		#if os(iOS) || os(tvOS)
		let image = VIAMyHealthImage(named: name, in: bundle, compatibleWith: nil)
		#elseif os(OSX)
		let image = bundle.image(forResource: name)
		#elseif os(watchOS)
		let image = VIAMyHealthImage(named: name)
		#endif
		guard let result = image else { fatalError("Unable to load image named \(name).") }
		return result
	}

	public static func ==(lhs: VIAMyHealthImageAsset, rhs: VIAMyHealthImageAsset) -> Bool {
        return lhs.image == rhs.image
    }
}

public struct VIAMyHealthColorAsset {
fileprivate var name: String

#if swift(>=3.2)
@available(iOS 11.0, tvOS 11.0, watchOS 4.0, OSX 10.13, *)
var color: VIAMyHealthColor {
return VIAMyHealthColor(asset: self)
}
#endif
}

// swiftlint:disable identifier_name line_length nesting type_body_length type_name
public enum VIAMyHealthAsset {
  public static let tips = VIAMyHealthImageAsset(name: "tips")
  public static let vitalityAgeDisabledLarge = VIAMyHealthImageAsset(name: "vitalityAgeDisabledLarge")
  public static let chevron = VIAMyHealthImageAsset(name: "chevron")
  public static let doingwellSmall = VIAMyHealthImageAsset(name: "doingwellSmall")
  public static let tabBarHealthInactive = VIAMyHealthImageAsset(name: "tabBarHealthInactive")
  public static let vitalityAgeXlarge = VIAMyHealthImageAsset(name: "vitalityAgeXlarge")
  public static let stressor = VIAMyHealthImageAsset(name: "stressor")
  public static let manageHealthSmall = VIAMyHealthImageAsset(name: "manageHealthSmall")
  public static let arrow = VIAMyHealthImageAsset(name: "arrow")
  public static let outdatedResult = VIAMyHealthImageAsset(name: "outdatedResult")
  public static let vitalityageDisabled = VIAMyHealthImageAsset(name: "vitalityageDisabled")
  public static let generalDocsSmall = VIAMyHealthImageAsset(name: "generalDocsSmall")
  public static let social = VIAMyHealthImageAsset(name: "social")
  public static let vitalityAgeDisabledXlarge = VIAMyHealthImageAsset(name: "vitalityAgeDisabledXlarge")
  public static let psychological = VIAMyHealthImageAsset(name: "psychological")
  public static let unknownLightSmall = VIAMyHealthImageAsset(name: "unknownLightSmall")
  public static let myHealthCircle = VIAMyHealthImageAsset(name: "myHealthCircle")
  public static let nutrition = VIAMyHealthImageAsset(name: "nutrition")
  public static let vitalityNutritionLarge = VIAMyHealthImageAsset(name: "vitalityNutritionLarge")
  public static let unknownSmall = VIAMyHealthImageAsset(name: "unknownSmall")
  public static let understand = VIAMyHealthImageAsset(name: "understand")
  public static let badResult = VIAMyHealthImageAsset(name: "badResult")
  public static let questionMarkSmall = VIAMyHealthImageAsset(name: "questionMarkSmall")
  public static let ageUnknownResult = VIAMyHealthImageAsset(name: "ageUnknownResult")
  public static let learnMoreSmall = VIAMyHealthImageAsset(name: "learnMoreSmall")
  public static let lowerAge = VIAMyHealthImageAsset(name: "lowerAge")
  public static let moderateResult = VIAMyHealthImageAsset(name: "moderateResult")
  public static let ageHighResult = VIAMyHealthImageAsset(name: "ageHighResult")
  public static let vitalityNutritionDisabled = VIAMyHealthImageAsset(name: "vitalityNutritionDisabled")
  public static let improveSmall = VIAMyHealthImageAsset(name: "improveSmall")
  public static let learnMoreGreySmall = VIAMyHealthImageAsset(name: "learnMoreGreySmall")
  public static let findAge = VIAMyHealthImageAsset(name: "findAge")
  public static let tabBarHealthActive = VIAMyHealthImageAsset(name: "tabBarHealthActive")
  public static let help = VIAMyHealthImageAsset(name: "help")
  public static let goodResult = VIAMyHealthImageAsset(name: "goodResult")
  public static let vitalityageActive = VIAMyHealthImageAsset(name: "vitalityageActive")
  public static let vitalityAgeLarge = VIAMyHealthImageAsset(name: "vitalityAgeLarge")
  public static let calculate = VIAMyHealthImageAsset(name: "calculate")
  public static let tipsDetail = VIAMyHealthImageAsset(name: "tipsDetail")
  public static let ageGoodResult = VIAMyHealthImageAsset(name: "ageGoodResult")
  public static let nutritionResult = VIAMyHealthImageAsset(name: "nutritionResult")

  // swiftlint:disable trailing_comma
  public static let allColors: [VIAMyHealthColorAsset] = [
  ]
  public static let allImages: [VIAMyHealthImageAsset] = [
    tips,
    vitalityAgeDisabledLarge,
    chevron,
    doingwellSmall,
    tabBarHealthInactive,
    vitalityAgeXlarge,
    stressor,
    manageHealthSmall,
    arrow,
    outdatedResult,
    vitalityageDisabled,
    generalDocsSmall,
    social,
    vitalityAgeDisabledXlarge,
    psychological,
    unknownLightSmall,
    myHealthCircle,
    nutrition,
    vitalityNutritionLarge,
    unknownSmall,
    understand,
    badResult,
    questionMarkSmall,
    ageUnknownResult,
    learnMoreSmall,
    lowerAge,
    moderateResult,
    ageHighResult,
    vitalityNutritionDisabled,
    improveSmall,
    learnMoreGreySmall,
    findAge,
    tabBarHealthActive,
    help,
    goodResult,
    vitalityageActive,
    vitalityAgeLarge,
    calculate,
    tipsDetail,
    ageGoodResult,
    nutritionResult,
  ]
  // swiftlint:enable trailing_comma
  @available(*, deprecated, renamed: "allImages")
  public static let allValues: [VIAMyHealthAssetType] = allImages
}
// swiftlint:enable identifier_name line_length nesting type_body_length type_name

extension VIAMyHealthImage {
@available(iOS 1.0, tvOS 1.0, watchOS 1.0, *)
@available(OSX, deprecated,
message: "This initializer is unsafe on macOS, please use the VIAMyHealthImageAsset.image property")
convenience init!(asset: VIAMyHealthAssetType) {
#if os(iOS) || os(tvOS)
let bundle = Bundle(for: BundleToken.self)
self.init(named: asset.name, in: bundle, compatibleWith: nil)
#elseif os(OSX) || os(watchOS)
self.init(named: asset.name)
#endif
}
}

extension VIAMyHealthColor {
#if swift(>=3.2)
@available(iOS 11.0, tvOS 11.0, watchOS 4.0, OSX 10.13, *)
convenience init!(asset: VIAMyHealthColorAsset) {
let bundle = Bundle(for: BundleToken.self)
#if os(iOS) || os(tvOS)
self.init(named: asset.name, in: bundle, compatibleWith: nil)
#elseif os(OSX)
self.init(named: asset.name, bundle: bundle)
#elseif os(watchOS)
self.init(named: asset.name)
#endif
}
#endif
}

private final class BundleToken {}
