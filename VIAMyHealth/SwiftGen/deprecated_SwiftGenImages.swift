// Generated using SwiftGen, by O.Halligon — https://github.com/AliSoftware/SwiftGen

internal class MyHealthImagesPlaceholder {
    // Placeholder class to reference lower down
}

#if os(iOS) || os(tvOS) || os(watchOS)
  import UIKit.UIImage
  typealias Image = UIImage
#elseif os(OSX)
  import AppKit.NSImage
  typealias Image = NSImage
#endif

// swiftlint:disable file_length
// swiftlint:disable line_length

// swiftlint:disable type_body_length
@available(*, deprecated, message: "Use MyHealthAsset.{image}.image or MyHealthAsset.{image}.templateImage instead")
internal enum MYHAsset: String {
  case ageGoodResult = "ageGoodResult"
  case ageHighResult = "ageHighResult"
  case ageUnknownResult = "ageUnknownResult"
  case arrow = "arrow"
  case calculate = "calculate"
  case findAge = "findAge"
  case generalDocsSmall = "generalDocsSmall"
  case learnMoreSmall = "learnMoreSmall"
  case lowerAge = "lowerAge"
  case manageHealthSmall = "manageHealthSmall"
  case myHealthCircle = "myHealthCircle"
  case psychological = "psychological"
  case questionMarkSmall = "questionMarkSmall"
  case tabBarHealthActive = "tabBarHealthActive"
  case tabBarHealthInactive = "tabBarHealthInactive"
  case tips = "tips"
  case understand = "understand"
  case vitalityAgeDisabledLarge = "vitalityAgeDisabledLarge"
  case vitalityAgeDisabledXlarge = "vitalityAgeDisabledXlarge"
  case vitalityAgeLarge = "vitalityAgeLarge"
  case vitalityAgeXlarge = "vitalityAgeXlarge"
  case nutrition = "nutrition"
  case stressor = "stressor"
  case social = "social"

  var image: Image {
    return Image(asset: self)
  }
}
// swiftlint:enable type_body_length

internal extension UIImage {
    @available(*, deprecated, message: "Use MyHealthAsset.{image}.image or MyHealthAsset.{image}.templateImage instead")
    class func templateImage(asset: MYHAsset) -> UIImage? {
        let bundle = Bundle(for: MyHealthImagesPlaceholder.self) // placeholder declared at top of file
        let image = UIImage(named: asset.rawValue, in: bundle, compatibleWith: nil)?.withRenderingMode(.alwaysTemplate)
        return image
    }
}

internal extension Image {
    @available(*, deprecated, message: "Use MyHealthAsset.{image}.image or MyHealthAsset.{image}.templateImage instead")
    convenience init!(asset: MYHAsset) {
        let bundle = Bundle(for: MyHealthImagesPlaceholder.self) // placeholder declared at top of file
        self.init(named: asset.rawValue, in: bundle, compatibleWith: nil)
    }
}
