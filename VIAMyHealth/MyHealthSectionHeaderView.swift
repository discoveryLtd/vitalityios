import UIKit
import VIAUIKit
import VitalityKit
import VIAUtilities

public class MyHealthSectionHeaderView: UITableViewHeaderFooterView, Nibloadable {
    
    public weak var parentViewController: VIATableViewController?

    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var headerImage: UIImageView!
    @IBOutlet weak var headerImageViewWidth: NSLayoutConstraint!
    @IBOutlet weak var headerImageViewLeading: NSLayoutConstraint!
    
    public override func awakeFromNib() {
        super.awakeFromNib()
    }

    func setupView(myHealthSecton: GMIReportedSection?) {
        if let section = myHealthSecton {
            self.headerLabel.text = section.typeName
            self.headerImage.image = self.imageForTypeKey(sectionRef: section.typeKey)
        }
        
        self.headerLabel.font = UIFont.title2Font()
    }
    
    public func setHeaderText(text: String?) {
        self.headerLabel.text = text
        self.headerLabel.font = UIFont.title2Font()
    }
    
    public func setHeaderImage(image: UIImage?) {
        self.headerImage.image = image
    }
    
    func imageForTypeKey(sectionRef: SectionRef) -> UIImage {
        switch sectionRef {
        case .VAgeResultBad:
            return VIAMyHealthAsset.improveSmall.image
        case .VAgeResultGood:
            return VIAMyHealthAsset.doingwellSmall.image
        case .VAgeResultUnknown:
            return VIAMyHealthAsset.unknownSmall.image
        case .NutritionWhatWell:
            return VIAMyHealthAsset.doingwellSmall.image
        case .NutritionWhatImprove:
            return VIAMyHealthAsset.improveSmall.image
        default:
            return VIAMyHealthAsset.tipsDetail.image
        }
    }
    
    public func hideHeaderImage() {
        headerImage.image = nil
        headerImageViewWidth.constant = 0
        headerImageViewLeading.constant = 0
    }
    
    public var myHealthSecton: GMIReportedSection? {
        didSet {
            setupView(myHealthSecton: myHealthSecton)
        }
    }
}
