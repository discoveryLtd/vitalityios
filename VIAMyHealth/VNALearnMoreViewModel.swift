//
//  MWBLearnMoreViewModel.swift
//  VitalityActive
//
//  Created by OJ Garde on 3/13/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import VitalityKit
import VIAUtilities

public class VNALearnMoreViewModel: VitalityAgeLearnMoreViewModel {
    let learnMoreRows : [VALearnMoreRow]
    
    public func numberOfSections() -> Int {
        return MyHealthLearnMoreSections.allValues.count
    }
    
    public func numberOfRows() -> Int {
        return learnMoreRows.count
    }
    
    static func buildUnknownLearnMore() -> [VALearnMoreRow] {
        return [
            VALearnMoreRow(
                header: CommonStrings.Vna.LearnMore.Content1971,
                content: CommonStrings.Vna.LearnMore.Content1972,
                asset: nil,
                headerFont: UIFont.title1Font(),
                contentFont: UIFont.subheadlineFont()
            ),
            VALearnMoreRow(
                header: CommonStrings.Vna.LearnMore.Content1973,
                content: CommonStrings.Vna.LearnMore.Content1974,
                asset: MYHAsset.nutrition,
                headerFont: UIFont.title2Font(),
                contentFont: UIFont.bodyFont()
            ),
            VALearnMoreRow(
                header: CommonStrings.Vna.LearnMore.Content1975,
                content: CommonStrings.Vna.LearnMore.Content1976,
                asset: MYHAsset.understand,
                headerFont: UIFont.title2Font(),
                contentFont: UIFont.bodyFont()
            )
        ]
    }
    
    static func buildOutdatedLearnMore() -> [VALearnMoreRow] {
        return [
            VALearnMoreRow(
                header: CommonStrings.Vna.LearnMore.Content1980,
                content: CommonStrings.Vna.LearnMore.Content1981,
                asset: nil,
                headerFont: UIFont.title1Font(),
                contentFont: UIFont.subheadlineFont()
            ),
            VALearnMoreRow(
                header: CommonStrings.Vna.LearnMore.Content1973,
                content: CommonStrings.Vna.LearnMore.Content1974,
                asset: MYHAsset.nutrition,
                headerFont: UIFont.title2Font(),
                contentFont: UIFont.bodyFont()
            ),
            VALearnMoreRow(
                header: CommonStrings.Vna.LearnMore.Content1975,
                content: CommonStrings.Vna.LearnMore.Content1976,
                asset: MYHAsset.understand,
                headerFont: UIFont.title2Font(),
                contentFont: UIFont.bodyFont()
            )
        ]
    }
    
    
    static func buildLearnMoreRows(status: PartyAttributeFeedbackRef) -> [VALearnMoreRow] {
        switch status {
        case .NutritionOutdated:
            return VNALearnMoreViewModel.buildOutdatedLearnMore()
        default:
            return VNALearnMoreViewModel.buildUnknownLearnMore()
        }
    }
    
    public func learnMoreRowAt(index: Int) -> VALearnMoreRow? {
        guard let learnMoreRow = learnMoreRows.element(at: index) else { return nil }
        return learnMoreRow
    }
    
    public init(status: PartyAttributeFeedbackRef) {
        learnMoreRows = VNALearnMoreViewModel.buildLearnMoreRows(status: status)
    }
}
