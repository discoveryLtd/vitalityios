import UIKit
import VIAUIKit
import VitalityKit
import VitalityKit
import VIAUtilities
import Foundation
import Lottie
import VIACommon

// MARK: Vitality Age Section
extension PartyAttributeFeedbackRef {
    static func statusFeedbackRefs() -> [PartyAttributeFeedbackRef] {
        return [PartyAttributeFeedbackRef.VAgeAbove,
                PartyAttributeFeedbackRef.VAgeBelow,
                PartyAttributeFeedbackRef.VAgeHealthy,
                PartyAttributeFeedbackRef.VAgeNotEnoughData]
    }
    
    private func ageVariance(vitalityAgeAttribute: GMIReportedHealthAttribute?) -> Int {
        var age: Int = 0
        
        if let vitalityAge = vitalityAgeAttribute {
            if let ageVarianceMetadata = vitalityAge.metadataWith(keys: [.VAgeVariance]) {
                if !String.isNilOrEmpty(string: ageVarianceMetadata.value) {
                    if let variance = Int(ageVarianceMetadata.value!) {
                        age = variance
                    }
                }
            }
        }
        
        return age
    }

    func blurbForAgeDifference(vitalityAgeAttribute: GMIReportedHealthAttribute?) -> String {
        let ageVariance = self.ageVariance(vitalityAgeAttribute: vitalityAgeAttribute)
        
        switch self {
        case .VAgeAbove:
            return CommonStrings.MyHealth.VitalityAgeOlderDescriptionLong622("\(ageVariance)")
        case .VAgeHealthy:
            return CommonStrings.MyHealth.VitalityAgeHealthyDescriptionLong2102
        case .VAgeBelow:
            return CommonStrings.MyHealth.VitalityAgeYoungerDescriptionLong631("\(ageVariance)")
        case .VAgeOutdated:
            return CommonStrings.MyHealth.VitalityAgeOutdatedDescriptionShort1073
        default:
            return CommonStrings.MyHealth.VitalityAgeVhcCompletedVhrNotCompletedNotEnoughDataDescriptionLong623
        }
    }
    
    func blurbSummaryForAgeDifference(vitalityAgeAttribute: GMIReportedHealthAttribute?) -> String {
        let ageVariance = self.ageVariance(vitalityAgeAttribute: vitalityAgeAttribute)
        
        switch self {
        case .VAgeAbove:
            return CommonStrings.MyHealth.VitalityAgeOlderDescriptionShort621("\(ageVariance)")
        case .VAgeHealthy:
            if AppSettings.getAppTenant() == .UKE {
                return ""
            }
            
            return CommonStrings.MyHealth.VitalityAgeHealthyDescriptionShort2101
            
        case .VAgeBelow:
            return CommonStrings.MyHealth.VitalityAgeYoungerDescriptionShort630("\(ageVariance)")
        case .VAgeNotEnoughData:
            return CommonStrings.MyHealth.VitalityAgeVhcCompletedVhrNotCompletedNotEnoughDataDescriptionShort618
        case .VAgeOutdated:
            return CommonStrings.MyHealth.VitalityAgeOutdatedDescriptionShort1073
        default:
            return ""
        }
    }

    func vitalityAgeImage() -> UIImage? {
        switch self {
        case .VAgeAbove:
            return VIAMyHealthAsset.vitalityAgeXlarge.image
        case .VAgeHealthy:
            return VIAMyHealthAsset.vitalityAgeXlarge.image
        case .VAgeBelow:
            return VIAMyHealthAsset.vitalityAgeXlarge.image
        case .VAgeOutdated:
            return VIAMyHealthAsset.vitalityAgeDisabledXlarge.image
        default:
            return VIAMyHealthAsset.vitalityAgeDisabledXlarge.image
        }
    }

    func myHealthAgeImage() -> UIImage? {
        switch self {
        case .VAgeAbove:
            return VIAMyHealthAsset.vitalityAgeLarge.image
        case .VAgeHealthy:
            return VIAMyHealthAsset.vitalityAgeLarge.image
        case .VAgeBelow:
            return VIAMyHealthAsset.vitalityAgeLarge.image
        case .VAgeOutdated:
            return VIAMyHealthAsset.vitalityAgeDisabledXlarge.image
        default:
            return VIAMyHealthAsset.vitalityAgeDisabledLarge.image
        }
    }

    func indicatorImage() -> UIImage? {
        switch self {
        case .VAgeAbove:
            return VIAMyHealthAsset.ageHighResult.image
        case .VAgeHealthy:
            return VIAMyHealthAsset.ageGoodResult.image
        case .VAgeBelow:
            return VIAMyHealthAsset.ageGoodResult.image
        case .VAgeOutdated:
            return VIAMyHealthAsset.unknownLightSmall.image
        default:
            return VIAMyHealthAsset.ageUnknownResult.image
        }
    }

    func summaryIndicatorImage() -> UIImage? {
        switch self {
        case .VAgeAbove:
            return VIAMyHealthAsset.ageHighResult.image
        case .VAgeHealthy:
            return VIAMyHealthAsset.ageGoodResult.image
        case .VAgeBelow:
            return VIAMyHealthAsset.ageGoodResult.image
        case .VAgeNotEnoughData:
            return VIAMyHealthAsset.ageUnknownResult.image
        case .VAgeOutdated:
            return VIAMyHealthAsset.unknownLightSmall.image
        default:
            return VIAMyHealthAsset.learnMoreSmall.image
        }
    }

    func statusText(vitalityAgeAttribute: GMIReportedHealthAttribute?) -> String {
        if let vitalityAge = vitalityAgeAttribute {
            if let outdated = vitalityAge.feedbackWith(keys: [.VAgeOutdated]) {
                return outdated.feedbackTypeName
            }
            
            if (vitalityAge.feedbackWith(keys: [.VAgeNotEnoughData]) != nil), VIAApplicableFeatures.default.shouldSkipMyHealthLandingScreen(){
                return CommonStrings.MyHealth.VitalityAgeAfterVhcSubmissionResultUnknownTitle615
            }
            
            if let feedback = vitalityAge.feedbackWith(keys: PartyAttributeFeedbackRef.statusFeedbackRefs()) {
                if let mhFeedback = vitalityAge.mhFeedback() {
                    return mhFeedback.feedbackTypeName
                }
                
                return feedback.feedbackTypeName
            }
        }
        
        if VIAApplicableFeatures.default.shouldSkipMyHealthLandingScreen(){
            return CommonStrings.MyHealth.VitalityAgeAfterVhcSubmissionResultUnknownTitle615
        }
        
        return CommonStrings.MyHealth.VitalityAgeVhcCompletedVhrNotCompletedNotEnoughDataTitle617
    }

    func summaryStatusText(vitalityAgeAttribute: GMIReportedHealthAttribute?) -> String {
        if self == .Unknown {
            return CommonStrings.LearnMoreButtonTitle104
        } else {
            if let vitalityAge = vitalityAgeAttribute {

                if let outdated = vitalityAge.feedbackWith(keys: [.VAgeOutdated]) {
                    return outdated.feedbackTypeName
                }

                if let feedback = vitalityAge.feedbackWith(keys: PartyAttributeFeedbackRef.statusFeedbackRefs()) {
                    if let mhFeedback = vitalityAge.mhFeedback() {
                        return mhFeedback.feedbackTypeName
                    }
                    
                    return feedback.feedbackTypeName
                }
            }
            
            return CommonStrings.LearnMoreButtonTitle104
        }
    }

    static func vitalityAgeStatus(vitalityAge: GMIReportedHealthAttribute) -> PartyAttributeFeedbackRef {
        if vitalityAge.feedbackWith(keys: [.VAgeOutdated]) != nil {
            return PartyAttributeFeedbackRef.VAgeOutdated
        }
        
        if let feedback = vitalityAge.feedbackWith(keys: PartyAttributeFeedbackRef.statusFeedbackRefs()) {
            if feedback.feedbackTypeKey == PartyAttributeFeedbackRef.VAgeAbove {
                return .VAgeAbove
            } else if feedback.feedbackTypeKey == PartyAttributeFeedbackRef.VAgeHealthy {
                return .VAgeHealthy
            } else if feedback.feedbackTypeKey == PartyAttributeFeedbackRef.VAgeBelow {
                return .VAgeBelow
            } else if feedback.feedbackTypeKey == PartyAttributeFeedbackRef.VAgeNotEnoughData {
                return .VAgeNotEnoughData
            }
        }
        
        return .Unknown
    }
}

// MARK: Nutrition Section
extension PartyAttributeFeedbackRef {
    static func vnaStatusFeedbackRefs() -> [PartyAttributeFeedbackRef] {
        return [PartyAttributeFeedbackRef.NutritionResults]
    }
    
    func vnaImage() -> UIImage? {
        // TODO: Replace with VNA images
        switch self {
        case .NutritionOutdated:
            return VIAMyHealthAsset.vitalityNutritionDisabled.image
        case .NutritionUnknown:
            return VIAMyHealthAsset.vitalityNutritionDisabled.image
        default:
            return VIAMyHealthAsset.vitalityNutritionLarge.image
        }
    }
    
    func vnaStatusImage() -> UIImage? {
        switch self {
        case .NutritionResults:
            return VIAMyHealthAsset.nutritionResult.image
        case .NutritionOutdated:
            return VIAMyHealthAsset.outdatedResult.image
        default:
            return VIAMyHealthAsset.learnMoreSmall.image
        }
    }
    
    func vnaSummaryStatusText() -> String {
        switch self {
        case .NutritionResults:
            return CommonStrings.Vna.Card.Label1967
        case .NutritionOutdated:
            return CommonStrings.Vna.Card.Label1969
        default:
            return CommonStrings.LearnMoreButtonTitle104
        }
    }
    
    func vnaSummaryStatusText(vnaAttribute: GMIReportedHealthAttribute?) -> String {
        if self == .Unknown {
            return CommonStrings.LearnMoreButtonTitle104
        } else {
            if let attribute = vnaAttribute {
                if let outdated = attribute.feedbackWith(keys: [.NutritionOutdated]) {
                    return outdated.feedbackTypeName
                }
                
                if let feedback = vnaAttribute?.feedbackWith(keys: PartyAttributeFeedbackRef.vnaStatusFeedbackRefs()) {
                    if let mhFeedback = attribute.mhFeedback() {
                        return mhFeedback.feedbackTypeName
                    }
                    
                    return feedback.feedbackTypeName
                }
            }
            
            return CommonStrings.LearnMoreButtonTitle104
        }
    }
    
    func vnaStatusText() -> String {
        switch self {
        case .NutritionResults:
            return CommonStrings.Vna.Card.Label1967
        case .NutritionOutdated:
            return CommonStrings.Vna.Card.Label1969
        default:
            return CommonStrings.Vna.Card.Label1966
        }
    }
    
    func vnaStatusText(vnaAttribute: GMIReportedHealthAttribute?) -> String {
        if let attribute = vnaAttribute {
            if let outdated = attribute.feedbackWith(keys: [.NutritionOutdated]) {
                return outdated.feedbackTypeName
            }
            
            if let feedback = attribute.feedbackWith(keys: PartyAttributeFeedbackRef.vnaStatusFeedbackRefs()) {
                if let mhFeedback = attribute.mhFeedback() {
                    return mhFeedback.feedbackTypeName
                }
                
                return feedback.feedbackTypeName
            }
        }
        
        return ""
    }
    
    func vnaBlurbSummary() -> String {
        switch self {
        case .NutritionResults:
            return CommonStrings.Vna.Card.Label1968
        case .NutritionOutdated:
            return CommonStrings.Vna.Card.Label1970
        default:
            return ""
        }
    }
    
    static func nutritionStatus(nutrition: GMIReportedHealthAttribute) -> PartyAttributeFeedbackRef {
        if nutrition.feedbackWith(keys: [.NutritionOutdated]) != nil {
            return PartyAttributeFeedbackRef.NutritionOutdated
        }
        
        if let feedback = nutrition.feedbackWith(keys: PartyAttributeFeedbackRef.vnaStatusFeedbackRefs()) {
            if feedback.feedbackTypeKey == PartyAttributeFeedbackRef.NutritionResults {
                return .NutritionResults
            }
        }
        
        return .NutritionUnknown
    }
}

// MARK: MWB Section
extension PartyAttributeFeedbackRef {
    static func mwbStatusFeedbackRefs() -> [PartyAttributeFeedbackRef] {
        return [PartyAttributeFeedbackRef.StressorWorkTogether,
                PartyAttributeFeedbackRef.StressorGettingThere,
                PartyAttributeFeedbackRef.StressorRightTrack,
                PartyAttributeFeedbackRef.PsychoWorkTogether,
                PartyAttributeFeedbackRef.PsychoGettingThere,
                PartyAttributeFeedbackRef.PsychoRightTrack,
                PartyAttributeFeedbackRef.SocialWorkTogether,
                PartyAttributeFeedbackRef.SocialGettingThere,
                PartyAttributeFeedbackRef.SocialRightTrack]
    }
    
    func mwbStatusAnimationName() -> String {
        let MWB_WORK_TOGETHER = "mwb_lets_work_together_animation"
        let MWB_OUT_OF_DATE = "mwb_out_of_date_animation"
        let MWB_UNKNOWN = "mwb_unknown_animation"
        let MWB_GETTING_THERE = "mwb_youre_getting_there_animation"
        let MWB_RIGHT_TRACK = "mwb_youre_on_the_right_track_animation"
        
        switch self {
        case .StressorWorkTogether:
            return MWB_WORK_TOGETHER
        case .StressorGettingThere:
            return MWB_GETTING_THERE
        case .StressorRightTrack:
            return MWB_RIGHT_TRACK
        case .PsychoWorkTogether:
            return MWB_WORK_TOGETHER
        case .PsychoGettingThere:
            return MWB_GETTING_THERE
        case .PsychoRightTrack:
            return MWB_RIGHT_TRACK
        case .SocialWorkTogether:
            return MWB_WORK_TOGETHER
        case .SocialGettingThere:
            return MWB_GETTING_THERE
        case .SocialRightTrack:
            return MWB_RIGHT_TRACK
        case .MWBOutdated:
            return MWB_OUT_OF_DATE
        default:
            return MWB_UNKNOWN
        }
    }
    
    func mwbStatusImage() -> UIImage? {
        switch self {
        case .StressorWorkTogether:
            return VIAMyHealthAsset.badResult.image
        case .StressorGettingThere:
            return VIAMyHealthAsset.moderateResult.image
        case .StressorRightTrack:
            return VIAMyHealthAsset.goodResult.image
        case .PsychoWorkTogether:
            return VIAMyHealthAsset.badResult.image
        case .PsychoGettingThere:
            return VIAMyHealthAsset.moderateResult.image
        case .PsychoRightTrack:
            return VIAMyHealthAsset.goodResult.image
        case .SocialWorkTogether:
            return VIAMyHealthAsset.badResult.image
        case .SocialGettingThere:
            return VIAMyHealthAsset.moderateResult.image
        case .SocialRightTrack:
            return VIAMyHealthAsset.goodResult.image
        default:
            return VIAMyHealthAsset.learnMoreSmall.image
        }
    }
    
    func mwbSummaryStatusText() -> String {
        switch self {
        case .StressorWorkTogether:
            return CommonStrings.Mwb.Result.CardTitle1230
        case .StressorGettingThere:
            return CommonStrings.Mwb.Result.CardTitle1232
        case .StressorRightTrack:
            return CommonStrings.Mwb.Result.CardTitle1234
        case .PsychoWorkTogether:
            return CommonStrings.Mwb.Result.CardTitle1230
        case .PsychoGettingThere:
            return CommonStrings.Mwb.Result.CardTitle1232
        case .PsychoRightTrack:
            return CommonStrings.Mwb.Result.CardTitle1234
        case .SocialWorkTogether:
            return CommonStrings.Mwb.Result.CardTitle1230
        case .SocialGettingThere:
            return CommonStrings.Mwb.Result.CardTitle1232
        case .SocialRightTrack:
            return CommonStrings.Mwb.Result.CardTitle1234
        default:
            return CommonStrings.LearnMoreButtonTitle104
        }
    }
    
    func mwbSummaryStatusText(mwbAttribute: GMIReportedHealthAttribute?) -> String {
        if self == .Unknown {
            return CommonStrings.LearnMoreButtonTitle104
        } else {
            if let attribute = mwbAttribute {
                if let outdated = attribute.feedbackWith(keys: [.MWBOutdated]) {
                    return outdated.feedbackTypeName
                }
                
                if let feedback = mwbAttribute?.feedbackWith(keys: PartyAttributeFeedbackRef.mwbStatusFeedbackRefs()) {
                    if let mhFeedback = attribute.mhFeedback() {
                        return mhFeedback.feedbackTypeName
                    }
                    
                    return feedback.feedbackTypeName
                }
            }
            
            return CommonStrings.LearnMoreButtonTitle104
        }
    }
    
    func mwbStatusText() -> String {
        switch self {
        case .StressorWorkTogether:
            return CommonStrings.Mwb.Result.CardTitle1230
        case .StressorGettingThere:
            return CommonStrings.Mwb.Result.CardTitle1232
        case .StressorRightTrack:
            return CommonStrings.Mwb.Result.CardTitle1234
        case .PsychoWorkTogether:
            return CommonStrings.Mwb.Result.CardTitle1230
        case .PsychoGettingThere:
            return CommonStrings.Mwb.Result.CardTitle1232
        case .PsychoRightTrack:
            return CommonStrings.Mwb.Result.CardTitle1234
        case .SocialWorkTogether:
            return CommonStrings.Mwb.Result.CardTitle1230
        case .SocialGettingThere:
            return CommonStrings.Mwb.Result.CardTitle1232
        case .SocialRightTrack:
            return CommonStrings.Mwb.Result.CardTitle1234
        default:
            return CommonStrings.Mwb.Result.CardTitle1229
        }
    }
    
    func mwbStatusText(mwbAttribute: GMIReportedHealthAttribute?) -> String {
        if let attribute = mwbAttribute {
            if let outdated = attribute.feedbackWith(keys: [.MWBOutdated]) {
                return outdated.feedbackTypeName
            }
            
            if let feedback = attribute.feedbackWith(keys: PartyAttributeFeedbackRef.mwbStatusFeedbackRefs()) {
                if let mhFeedback = attribute.mhFeedback() {
                    return mhFeedback.feedbackTypeName
                }
                
                return feedback.feedbackTypeName
            }
        }
        
        return ""
    }
    
    func mwbBlurbSummary(mwbAttribute: GMIReportedHealthAttribute?) -> String {
        var outdatedString: String! = ""
        if let attribute = mwbAttribute {
            switch attribute.attributeTypeKey {
            case .Stressor:
                outdatedString = CommonStrings.Mwb.Result.CardDescription1237
            case .Psychological:
                outdatedString = CommonStrings.Mwb.Result.CardDescription1264
            case .Social:
                outdatedString = CommonStrings.Mwb.Result.CardDescription1281
            default:
                outdatedString = ""
            }
        }
        
        switch self {
        case .StressorWorkTogether:
            return CommonStrings.Mwb.Result.CardDescription1231
        case .StressorGettingThere:
            return CommonStrings.Mwb.Result.CardDescription1233
        case .StressorRightTrack:
            return CommonStrings.Mwb.Result.CardDescription1235
        case .PsychoWorkTogether:
            return CommonStrings.Mwb.Result.CardDescription1261
        case .PsychoGettingThere:
            return CommonStrings.Mwb.Result.CardDescription1262
        case .PsychoRightTrack:
            return CommonStrings.Mwb.Result.CardDescription1263
        case .SocialWorkTogether:
            return CommonStrings.Mwb.Result.CardDescription1278
        case .SocialGettingThere:
            return CommonStrings.Mwb.Result.CardDescription1279
        case .SocialRightTrack:
            return CommonStrings.Mwb.Result.CardDescription1280
        case .MWBOutdated:
            return outdatedString
        default:
            return ""
        }
    }
    
    func mwbBlurbSummaryFromBackEnd(mwbAttribute: GMIReportedHealthAttribute?) -> String {
        if let attribute = mwbAttribute {
            if let outdated = attribute.feedbackWith(keys: [.MWBOutdated]) {
                return outdated.whyIsThisImportant!
            }
            
            if let feedback = attribute.feedbackWith(keys: PartyAttributeFeedbackRef.mwbStatusFeedbackRefs()) {
                if let mhFeedback = attribute.mhFeedback() {
                    return mhFeedback.whyIsThisImportant!
                }
                guard let feedBack = feedback.whyIsThisImportant else { return "" }
                return feedBack
            }
        }
        
        return ""
    }
    
    static func mwbStatus(mwb: GMIReportedHealthAttribute) -> PartyAttributeFeedbackRef {
        if mwb.feedbackWith(keys: [.MWBOutdated]) != nil {
            return PartyAttributeFeedbackRef.MWBOutdated
        }
        
        if let feedback = mwb.feedbackWith(keys: PartyAttributeFeedbackRef.mwbStatusFeedbackRefs()) {
            if feedback.feedbackTypeKey == PartyAttributeFeedbackRef.StressorWorkTogether {
                return .StressorWorkTogether
            } else if feedback.feedbackTypeKey == PartyAttributeFeedbackRef.StressorGettingThere {
                return .StressorGettingThere
            } else if feedback.feedbackTypeKey == PartyAttributeFeedbackRef.StressorRightTrack {
                return .StressorRightTrack
            } else if feedback.feedbackTypeKey == PartyAttributeFeedbackRef.PsychoWorkTogether {
                return .PsychoWorkTogether
            } else if feedback.feedbackTypeKey == PartyAttributeFeedbackRef.PsychoGettingThere {
                return .PsychoGettingThere
            } else if feedback.feedbackTypeKey == PartyAttributeFeedbackRef.PsychoRightTrack {
                return .PsychoRightTrack
            } else if feedback.feedbackTypeKey == PartyAttributeFeedbackRef.SocialWorkTogether {
                return .SocialWorkTogether
            } else if feedback.feedbackTypeKey == PartyAttributeFeedbackRef.SocialGettingThere {
                return .SocialGettingThere
            } else if feedback.feedbackTypeKey == PartyAttributeFeedbackRef.SocialRightTrack {
                return .SocialRightTrack
            }
        }
        
        return .MWBUnknown
    }
}

public class MyHealthCardTableViewCell: UITableViewCell, Nibloadable {
    @IBOutlet weak var cardLabel: UILabel!
    @IBOutlet weak var cardStatusImage: UIImageView!
    @IBOutlet weak var cardStatusLabel: UILabel!
    @IBOutlet weak var cardBlurb: UILabel!
    @IBOutlet weak var learnMoreButton: UIButton!
    @IBOutlet weak var cardImage: UIImageView!
    @IBOutlet weak var animationView: UIView!
    
    @IBOutlet weak var cardLabelTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var cardLabelHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var animationViewHeightConstraint: NSLayoutConstraint!

    public weak var parentViewController: VIATableViewController?
    public var showBarButton: Bool = true
    
    var cardIndex: Int! = -1
    
    public override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setupCardView(atIndex index: Int!, attributeTypeRef: PartyAttributeTypeRef) {
        if let section = HealthInformationRepository().getHealthAttributeBy(attributeType: attributeTypeRef), let sectionValue = section.value {
            var attributeFeedbackTypeRef: PartyAttributeFeedbackRef
            
            switch attributeTypeRef {
            case .Nutrition:
                attributeFeedbackTypeRef = PartyAttributeFeedbackRef.nutritionStatus(nutrition: section)
            case .Stressor:
                attributeFeedbackTypeRef = PartyAttributeFeedbackRef.mwbStatus(mwb: section)
            case .Psychological:
                attributeFeedbackTypeRef = PartyAttributeFeedbackRef.mwbStatus(mwb: section)
            case .Social:
                attributeFeedbackTypeRef = PartyAttributeFeedbackRef.mwbStatus(mwb: section)
            default:
                attributeFeedbackTypeRef = PartyAttributeFeedbackRef.vitalityAgeStatus(vitalityAge: section)
            }
            self.setupCardHeaderDetails(cardIndex: index, value: sectionValue, status: attributeFeedbackTypeRef, sectionAttribute: section)
        } else {
            self.setupCardHeaderDetails(cardIndex: index, value: "", status: .Unknown, sectionAttribute: nil)
        }

        cardIndex = index
        
        switch cardIndex {
        case 1:
            setupLearnMoreButtonWithTintColor(UIColor.nutritionBottom())
        case 2:
            setupLearnMoreButtonWithTintColor(UIColor.stressorBlueBottom())
        case 3:
            setupLearnMoreButtonWithTintColor(UIColor.psychologicalBlueBottom())
        case 4:
            setupLearnMoreButtonWithTintColor(UIColor.socialBlueBottom())
        default:
            setupLearnMoreButtonWithTintColor(UIColor.deepSeaBlue())
        }
    }

    public override func layoutSubviews() {
        switch cardIndex {
        case 1:
            drawNutritionGradient()
        case 2:
            drawStressorGradient()
        case 3:
            drawPsychologicalGradient()
        case 4:
            drawSocialGradient()
        default:
            drawVitalityAgeGradient()
        }
        
        super.layoutSubviews()
    }
    
    func hideAgeLabelView() {
        cardLabelTopConstraint.constant = 0
        cardLabelHeightConstraint.constant = 0
    }
    
    func hideAnimationView() {
        animationViewHeightConstraint.constant = 0
    }
    
    func setupLearnMoreButtonWithTintColor(_ color: UIColor) {
        self.learnMoreButton.tintColor = color
        self.learnMoreButton.backgroundColor = UIColor.day()
        
        self.learnMoreButton.titleLabel?.font = UIFont.bodyFont()
        self.learnMoreButton.contentEdgeInsets = UIEdgeInsets(top: 13.0, left: 16.0, bottom: 13.0, right: 16.0)
        self.learnMoreButton.layer.cornerRadius = 5

        self.learnMoreButton.setTitle(CommonStrings.LearnMoreButtonTitle104, for: UIControlState.normal)
        self.learnMoreButton.addTarget(self, action: #selector(showLearnMore(sender:)), for: .touchUpInside)
    }

    func showLearnMore(sender: UIButton) {
        if let parent = parentViewController {
            var status:PartyAttributeFeedbackRef! = .Unknown
            switch cardIndex {
            case 1:
                if let section = HealthInformationRepository().getHealthAttributeBy(attributeType: .Nutrition) {
                    status = PartyAttributeFeedbackRef.nutritionStatus(nutrition: section)
                }
            case 2:
                if let section = HealthInformationRepository().getHealthAttributeBy(attributeType: .Stressor) {
                    status = PartyAttributeFeedbackRef.mwbStatus(mwb: section)
                }
            case 3:
                if let section = HealthInformationRepository().getHealthAttributeBy(attributeType: .Psychological) {
                    status = PartyAttributeFeedbackRef.mwbStatus(mwb: section)
                }
            case 4:
                if let section = HealthInformationRepository().getHealthAttributeBy(attributeType: .Social) {
                    status = PartyAttributeFeedbackRef.mwbStatus(mwb: section)
                }
            default:
                status = .Unknown
            }
            
            parent.performSegue(withIdentifier: "myHealthLearnMore", sender: status)
        }
    }

    func drawVitalityAgeGradient() {
        let top = UIColor.tealBlue()
        let bottom = UIColor.deepSeaBlue()
        self.backgroundColor = UIColor(gradientStyle: .topToBottom, withFrame: frame, andColors: [top, bottom])
    }
    
    func drawNutritionGradient() {
        let top = UIColor.nutritionTop()
        let bottom = UIColor.nutritionBottom()
        self.backgroundColor = UIColor(gradientStyle: .topToBottom, withFrame: frame, andColors: [top, bottom])
    }
    
    func drawStressorGradient() {
        let top = UIColor.stressorBlueTop()
        let bottom = UIColor.stressorBlueBottom()
        self.backgroundColor = UIColor(gradientStyle: .topToBottom, withFrame: frame, andColors: [top, bottom])
    }
    
    func drawPsychologicalGradient() {
        let top = UIColor.psychologicalBlueTop()
        let bottom = UIColor.psychologicalBlueBottom()
        self.backgroundColor = UIColor(gradientStyle: .topToBottom, withFrame: frame, andColors: [top, bottom])
    }
    
    func drawSocialGradient() {
        let top = UIColor.socialBlueTop()
        let bottom = UIColor.socialBlueBottom()
        self.backgroundColor = UIColor(gradientStyle: .topToBottom, withFrame: frame, andColors: [top, bottom])
    }

    func setupCardHeaderDetails(cardIndex: Int, value: String, status: PartyAttributeFeedbackRef, sectionAttribute: GMIReportedHealthAttribute?) {
        self.setupAgeLabel(status: status, vitalityAge: value)

        if cardIndex == 0 {
            self.cardImage.image = status.vitalityAgeImage()
        }
        
        let blurbText = status.blurbForAgeDifference(vitalityAgeAttribute: sectionAttribute)
        let blurbSummaryText = status.blurbSummaryForAgeDifference(vitalityAgeAttribute: sectionAttribute)
        var ageBlurbText: String? {
            return VIAApplicableFeatures.default.concatenateAgeBlurb! ? blurbSummaryText + " " + blurbText : blurbText
        }
        
        /*
         * TODO: Implement only on CA for now
         * reference ticket: https://jira.vitalityservicing.com/browse/VA-33771
         * Remove implementation of 'VIAUseMyHealthBackendFeedback' (.plist) once this CR is promoted to the other markets.
         */
        let vnaStatusText: String = {
            if let useMyHealthBackendFeedback = Bundle.main.object(forInfoDictionaryKey: "VIAUseMyHealthBackendFeedback") as? Bool, useMyHealthBackendFeedback {
                return status.vnaStatusText(vnaAttribute: sectionAttribute)
            } else {
                return status.vnaStatusText()
            }
        }()
        let mwbStatusText: String = {
            if let useMyHealthBackendFeedback = Bundle.main.object(forInfoDictionaryKey: "VIAUseMyHealthBackendFeedback") as? Bool, useMyHealthBackendFeedback {
                return status.mwbStatusText(mwbAttribute: sectionAttribute)
            } else {
                return status.mwbStatusText()
            }
        }()
        let mwbStatusDescription: String = {
            if let useMyHealthBackendFeedback = Bundle.main.object(forInfoDictionaryKey: "VIAUseMyHealthBackendFeedback") as? Bool, useMyHealthBackendFeedback {
                return status.mwbBlurbSummaryFromBackEnd(mwbAttribute: sectionAttribute)
            } else {
                return status.mwbBlurbSummary(mwbAttribute: sectionAttribute)
            }
        }()
        
        switch cardIndex {
        case 1: // VNA
            self.cardStatusImage.image = status.vnaStatusImage()
            self.cardStatusLabel.text = vnaStatusText
            self.cardBlurb.text = status.vnaBlurbSummary()
        case 2: // Stressor
            setupAnimatedBackgroudView(status: status)
            self.cardStatusImage.image = status.mwbStatusImage()
            self.cardStatusLabel.text = mwbStatusText
            self.cardBlurb.text = mwbStatusDescription
        case 3: // Psychological
            setupAnimatedBackgroudView(status: status)
            self.cardStatusImage.image = status.mwbStatusImage()
            self.cardStatusLabel.text = mwbStatusText
            self.cardBlurb.text = mwbStatusDescription
        case 4: // Social
            setupAnimatedBackgroudView(status: status)
            self.cardStatusImage.image = status.mwbStatusImage()
            self.cardStatusLabel.text = mwbStatusText
            self.cardBlurb.text = mwbStatusDescription
        default: // Vitality Age
            self.cardStatusImage.image = status.indicatorImage()
            self.cardStatusLabel.text = status.statusText(vitalityAgeAttribute: sectionAttribute)
            self.cardBlurb.text = ageBlurbText
        }
        
        self.cardStatusImage.tintColor = UIColor.feedbackViewBackground()

        self.cardStatusLabel.font = UIFont.title3Font()
        self.cardStatusLabel.textColor = UIColor.feedbackViewBackground()
        
        self.cardBlurb.font = UIFont.bodyFont()
        self.cardBlurb.font = UIFont.subheadlineFont()

        self.cardBlurb.textColor = UIColor.feedbackViewBackground()
    }
    
    func showUnknownText(status: PartyAttributeFeedbackRef){
        if status == .Unknown && VIAApplicableFeatures.default.shouldSkipMyHealthLandingScreen() {
            self.cardLabel.text = CommonStrings.MyHealth.VitalityAgeUnknown627
        } else if status == .VAgeNotEnoughData{
            if showBarButton && VIAApplicableFeatures.default.shouldSkipMyHealthLandingScreen(){
                self.cardLabel.text = CommonStrings.MyHealth.VitalityAgeVhcDoneVhrPendingTitle614
            }
            else {
                self.cardLabel.text = CommonStrings.MyHealth.VitalityAgeVhcCompletedVhrNotCompletedNotEnoughDataTitle617
            }
        }
        else {
                self.cardLabel.text = CommonStrings.MyHealth.VitalityAgeVhcCompletedVhrNotCompletedNotEnoughDataTitle617
        }
        self.cardLabel.font = UIFont.title1Font()
        self.cardLabel.numberOfLines = 2
        
        //if VIAApplicableFeatures.default.shouldSkipMyHealthLandingScreen(){
        //    self.cardLabel.text = CommonStrings.MyHealth.VitalityAgeUnknown627
        //}
    }
    
    func setupAgeLabel(status: PartyAttributeFeedbackRef, vitalityAge: String) {
        if status == .VAgeNotEnoughData || status == .Unknown {
            self.showUnknownText(status: status)
        } else {
            if let doubleAge = Double(vitalityAge) {
                let age = Int(round(doubleAge))
                self.cardLabel.text = String(age)
                
                self.cardLabel.font = UIFont.systemFont(ofSize: 80, weight:UIFontWeightHeavy)
            }
            else {
                self.showUnknownText(status: status)
            }
        }
        self.cardLabel.textColor = UIColor.feedbackViewBackground()

        if status == .VAgeOutdated {
            self.cardLabel.textColor = self.cardLabel.textColor.withAlphaComponent(0.5)
        }
    }
    
    func setupAnimatedBackgroudView(status: PartyAttributeFeedbackRef) {
        let animation = LOTAnimationView(name: status.mwbStatusAnimationName())
        animation.loopAnimation = true
        animation.frame.origin.x = 0
        animation.frame.origin.y = -200
        animation.frame.size.width = animationView.frame.width
        animation.contentMode = .scaleAspectFit
        self.animationView.subviews.forEach({ $0.removeFromSuperview() })
        self.animationView.addSubview(animation)
        animation.play()
    }
}
