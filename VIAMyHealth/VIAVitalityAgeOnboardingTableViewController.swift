import Foundation
import VitalityKit
import VIAUIKit
import UIKit

public class VIAVitalityAgeOnboardingTableViewController: VIAOnboardingViewController {

    var myHealthOnboardingViewmodel: MyHealthOnboardingViewModel? {
        didSet {
            self.viewModel = myHealthOnboardingViewmodel
        }
    }
    
    /*
     * For UKE, MyHealth is changed to Vitality age, so we need to use vitality age
     * model
     */
    var vitalityAgeOnboardingViewmodel: VitalityAgeOnboardingViewModel? {
        didSet {
            self.viewModel = vitalityAgeOnboardingViewmodel
        }
    }
    
    override public func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        /* Now that it is already displayed to the user, we can now flagged the my health onboarding. */
        AppSettings.setHasShownMyHealthOnboarding()
    }
    
    override public func mainButtonTapped(_ sender: UIButton) {
        self.dismiss(animated: true)
    }
}
