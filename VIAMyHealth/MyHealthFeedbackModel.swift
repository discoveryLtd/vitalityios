import Foundation
import VIAUtilities
import VitalityKit

public class MyHealthFeedbackModel {
    var section: GMIReportedSection?

    init(cardSection: SectionRef) {
        switch cardSection {
        case .VAgeSection:
            section = HealthInformationRepository().getSectionBy(sectionKey: SectionRef.VAgeSection)
        case .NutritionSection:
            section = HealthInformationRepository().getSectionBy(sectionKey: SectionRef.NutritionSection)
        case .Stressor:
            section = HealthInformationRepository().getSectionBy(sectionKey: SectionRef.Stressor)
        case .Psycological:
            section = HealthInformationRepository().getSectionBy(sectionKey: SectionRef.Psycological)
        case .Social:
            section = HealthInformationRepository().getSectionBy(sectionKey: SectionRef.Social)
        default:
            section = HealthInformationRepository().getSectionBy(sectionKey: SectionRef.Unknown)
        }
    }
    
    func numberOfSections() -> Int {
        return getSectionCount()
    }
    
    private func validSections() -> [GMIReportedSection] {
        var sections = [GMIReportedSection]()

        if let section = section {
            sections.append(contentsOf: section.sections.sorted(by: { (left, right) -> Bool in
                return left.sortOrder < right.sortOrder
            }))
        }

        let validSections = sections.filter {$0.healthAttributes.first?.value != "None"}
        
        return validSections
    }
    
    func numberOfRowsInSection(section: Int) -> Int{
        if section == 0 {
            return 1
        }else if section < getSectionCount(){
            let myHealthSection = sectionAt(sectionNumber: section)
            
            let attributeCount = myHealthSection.healthAttributes.count
            
            if attributeCount == 0 {
                return 1
            }
            
            //Xcode 9
            let showMoreResults = myHealthSection.sections.lazy.filter {
                return $0.healthAttributes.count > 0
                }.count > 0
            //Xcode 9
            //            let showMoreResults = myHealthSection.sections.filter { (section) -> Bool in
            //                return section.healthAttributes.count > 0
            //                }.count > 0

            return attributeCount + (showMoreResults ? 1 : 0)
        }else{
            return 1 /* This is for the Help row */
        }
    }
    
    func sectionIsEmpty(section: Int) -> Bool {
        let myHealthSection = sectionAt(sectionNumber: section)
        let attributeCount = myHealthSection.healthAttributes.count
        return attributeCount == 0
    }

    func sectionAt(sectionNumber: Int) -> GMIReportedSection {
        return self.validSections()[sectionNumber - 1]
    }

    func emptyHealthAttributeForSection(sectionRef: SectionRef) -> GMIReportedHealthAttribute {
        let emptyHealthAttribute = GMIReportedHealthAttribute()
        switch sectionRef {
        case .VAgeResultGood, .NutritionWhatWell:
            emptyHealthAttribute.attributeTypeName = CommonStrings.MyHealth.NoResultsInHealthyRange1070
        case .VAgeResultBad:
            emptyHealthAttribute.attributeTypeName = CommonStrings.MyHealth.NoImprovementsNeeded1069
        case .VAgeResultUnknown:
            emptyHealthAttribute.attributeTypeName = CommonStrings.MyHealth.NoUnknownHealthResults1071
        default:
            /* If there are no recommendation string from BE response.
             * This will always be the default as to sync with Android implementation.
             */
            emptyHealthAttribute.attributeTypeName = CommonStrings.MyHealth.NoImprovementsNeeded1069
        }

        return emptyHealthAttribute
    }
    
    private func getSectionCount() -> Int {
        return self.validSections().count + 1 // +1 for the card header section.
    }
    
    func getSection() -> GMIReportedSection? {
        return section
    }
}
