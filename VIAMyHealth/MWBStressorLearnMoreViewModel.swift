//
//  MWBLearnMoreViewModel.swift
//  VitalityActive
//
//  Created by OJ Garde on 3/13/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import VitalityKit
import VIAUtilities

public class MWBStressorLearnMoreViewModel: VitalityAgeLearnMoreViewModel {
    let learnMoreRows : [VALearnMoreRow]
    
    public func numberOfSections() -> Int {
        return MyHealthLearnMoreSections.allValues.count
    }
    
    public func numberOfRows() -> Int {
        return learnMoreRows.count
    }
    
    static func buildUnknownLearnMore() -> [VALearnMoreRow] {
        return [
            VALearnMoreRow(
                header: CommonStrings.Mwb.Learnmore.Landing.HeaderTitle1238,
                content: CommonStrings.Mwb.Learnmore.Landing.HeaderDescription1239,
                asset: nil,
                headerFont: UIFont.title1Font(),
                contentFont: UIFont.subheadlineFont()
            ),
            VALearnMoreRow(
                header: CommonStrings.Mwb.Learnmore.Landing.SectionHeaderTitle1240,
                content: CommonStrings.Mwb.Learnmore.Landing.SectionHeaderDescription1241,
                asset: MYHAsset.calculate,
                headerFont: UIFont.title2Font(),
                contentFont: UIFont.bodyFont()
            ),
            VALearnMoreRow(
                header: CommonStrings.Mwb.Learnmore.Landing.SectionHeaderTitle1242,
                content: CommonStrings.Mwb.Learnmore.Landing.SectionHeaderDescription1243,
                asset: MYHAsset.stressor,
                headerFont: UIFont.title2Font(),
                contentFont: UIFont.bodyFont()
            ),
            VALearnMoreRow(
                header: CommonStrings.Mwb.Learnmore.Landing.SectionHeaderTitle1244,
                content: CommonStrings.Mwb.Learnmore.Landing.SectionHeaderDescription1245,
                asset: MYHAsset.understand,
                headerFont: UIFont.title2Font(),
                contentFont: UIFont.bodyFont()
            )
        ]
    }
    
    static func buildLetsWorkTogetherLearnMore() -> [VALearnMoreRow] {
        return [
            VALearnMoreRow(
                header: CommonStrings.Mwb.Learnmore.Landing.HeaderTitle1253,
                content: CommonStrings.Mwb.Learnmore.Landing.HeaderDescription1254,
                asset: nil,
                headerFont: UIFont.title1Font(),
                contentFont: UIFont.subheadlineFont()
            ),
            VALearnMoreRow(
                header: CommonStrings.Mwb.Learnmore.Landing.SectionTitle1255,
                content: CommonStrings.Mwb.Learnmore.Landing.SectionDescription1256,
                asset: MYHAsset.understand,
                headerFont: UIFont.title2Font(),
                contentFont: UIFont.bodyFont()
            ),
            VALearnMoreRow(
                header: CommonStrings.Mwb.Learnmore.Landing.SectionHeaderTitle1240,
                content: CommonStrings.Mwb.Learnmore.Landing.SectionHeaderDescription1241,
                asset: MYHAsset.calculate,
                headerFont: UIFont.title2Font(),
                contentFont: UIFont.bodyFont()
            )
        ]
    }
    
    static func buildYoureGettingThereLearnMore() -> [VALearnMoreRow] {
        return [
            VALearnMoreRow(
                header: CommonStrings.Mwb.Learnmore.Landing.HeaderTitle1292,
                content: CommonStrings.Mwb.Learnmore.Landing.HeaderDescription1293,
                asset: nil,
                headerFont: UIFont.title1Font(),
                contentFont: UIFont.subheadlineFont()
            ),
            VALearnMoreRow(
                header: CommonStrings.Mwb.Learnmore.Landing.SectionTitle1255,
                content: CommonStrings.Mwb.Learnmore.Landing.SectionDescription1256,
                asset: MYHAsset.understand,
                headerFont: UIFont.title2Font(),
                contentFont: UIFont.bodyFont()
            ),
            VALearnMoreRow(
                header: CommonStrings.Mwb.Learnmore.Landing.SectionHeaderTitle1240,
                content: CommonStrings.Mwb.Learnmore.Landing.SectionHeaderDescription1241,
                asset: MYHAsset.calculate,
                headerFont: UIFont.title2Font(),
                contentFont: UIFont.bodyFont()
            )
        ]
    }
    
    static func buildYoureOnTheRightTrackLearnMore() -> [VALearnMoreRow] {
        return [
            VALearnMoreRow(
                header: CommonStrings.Mwb.Learnmore.Landing.HeaderTitle1294,
                content: CommonStrings.Mwb.Learnmore.Landing.HeaderDescription1295,
                asset: nil,
                headerFont: UIFont.title1Font(),
                contentFont: UIFont.subheadlineFont()
            ),
            VALearnMoreRow(
                header: CommonStrings.Mwb.Learnmore.Landing.SectionTitle1255,
                content: CommonStrings.Mwb.Learnmore.Landing.SectionDescription1256,
                asset: MYHAsset.understand,
                headerFont: UIFont.title2Font(),
                contentFont: UIFont.bodyFont()
            ),
            VALearnMoreRow(
                header: CommonStrings.Mwb.Learnmore.Landing.SectionHeaderTitle1240,
                content: CommonStrings.Mwb.Learnmore.Landing.SectionHeaderDescription1241,
                asset: MYHAsset.calculate,
                headerFont: UIFont.title2Font(),
                contentFont: UIFont.bodyFont()
            )
        ]
    }
    
    static func buildOutdatedLearnMore() -> [VALearnMoreRow] {
        return [
            VALearnMoreRow(
                header: CommonStrings.Mwb.Learnmore.Landing.HeaderTitle1290,
                content: CommonStrings.Mwb.Learnmore.Landing.HeaderDescription1291,
                asset: nil,
                headerFont: UIFont.title1Font(),
                contentFont: UIFont.subheadlineFont()
            ),
            VALearnMoreRow(
                header: CommonStrings.Mwb.Learnmore.Landing.SectionTitle1255,
                content: CommonStrings.Mwb.Learnmore.Landing.SectionDescription1256,
                asset: MYHAsset.understand,
                headerFont: UIFont.title2Font(),
                contentFont: UIFont.bodyFont()
            ),
            VALearnMoreRow(
                header: CommonStrings.Mwb.Learnmore.Landing.SectionHeaderTitle1240,
                content: CommonStrings.Mwb.Learnmore.Landing.SectionHeaderDescription1241,
                asset: MYHAsset.calculate,
                headerFont: UIFont.title2Font(),
                contentFont: UIFont.bodyFont()
            )
        ]
    }
    
    
    static func buildLearnMoreRows(status: PartyAttributeFeedbackRef) -> [VALearnMoreRow] {
        switch status {
        case .StressorWorkTogether:
            return MWBStressorLearnMoreViewModel.buildLetsWorkTogetherLearnMore()
        case .StressorGettingThere:
            return MWBStressorLearnMoreViewModel.buildYoureGettingThereLearnMore()
        case .StressorRightTrack:
            return MWBStressorLearnMoreViewModel.buildYoureOnTheRightTrackLearnMore()
        case .MWBOutdated:
            return MWBStressorLearnMoreViewModel.buildOutdatedLearnMore()
        default:
            return MWBStressorLearnMoreViewModel.buildUnknownLearnMore()
        }
    }
    
    public func learnMoreRowAt(index: Int) -> VALearnMoreRow? {
        guard let learnMoreRow = learnMoreRows.element(at: index) else { return nil }
        return learnMoreRow
    }
    
    public init(status: PartyAttributeFeedbackRef) {
        learnMoreRows = MWBStressorLearnMoreViewModel.buildLearnMoreRows(status: status)
    }
}
