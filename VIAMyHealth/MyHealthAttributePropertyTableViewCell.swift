import UIKit
import VIAUIKit
import VitalityKit
import Foundation

public class MyHealthAttributePropertyTableViewCell: UITableViewCell, Nibloadable {
    @IBOutlet weak var attributeProperty: UILabel!
    @IBOutlet weak var attributeDetail: UILabel!
    @IBOutlet weak var attributeFeedback: UILabel!
    
    public var feedbackTip: GMIReportedFeedbackTip?
    var detailText: String?

    public weak var parentTableView: UITableView?
    
    public override func awakeFromNib() {
        super.awakeFromNib()
        
        setupView()
    }
    
    func setupViewAsEvent(){
        
        self.attributeProperty.font = UIFont.footnoteFont()
        self.attributeProperty.textColor = UIColor.darkGray

        self.attributeDetail.font = UIFont.headlineFont()
        self.attributeDetail.textColor = UIColor.night()

        if self.attributeFeedback != nil {
            self.attributeFeedback.font = UIFont.footnoteFont()
            self.attributeFeedback.textColor = UIColor.darkGray
        }
    }

    func setupViewAsTip(feedbackTip: GMIReportedFeedbackTip, asPreview: Bool = true) {

        self.feedbackTip = feedbackTip

        self.attributeProperty.font = UIFont.headlineFont()
        self.attributeProperty.textColor = UIColor.night()

        self.attributeDetail.font = UIFont.subheadlineFont()
        self.attributeDetail.textColor = UIColor.darkGray
        
        if !asPreview {
            self.attributeDetail.numberOfLines = 0
        }
        
        self.attributeDetail.gestureRecognizers?.forEach({ (recognizer) in
            self.attributeDetail.removeGestureRecognizer(recognizer)
        })

        self.attributeProperty.text = feedbackTip.typeName
        self.attributeDetail.text = feedbackTip.note
        
        self.hideFeedback()
    }
    
    func setupView(){

        self.attributeProperty.font = UIFont.footnoteFont()
        self.attributeProperty.textColor = UIColor.darkGray
        
        self.attributeDetail.font = UIFont.headlineFont()
        self.attributeDetail.textColor = UIColor.night()
        
        self.attributeDetail.isUserInteractionEnabled = true
        self.attributeDetail.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTap(_:) )))

        self.attributeFeedback.font = UIFont.footnoteFont()
        self.attributeFeedback.textColor = UIColor.crimsonRed()
    }
    
    func didTap(_ sender: UITapGestureRecognizer) {

        if self.numberOfLinesDisplayed(label: self.attributeDetail) >= 3 {
            if let text = self.detailText {
                self.attributeDetail.numberOfLines = 0
                self.attributeDetail.text = text
                self.attributeDetail.font = UIFont.bodyFont()
                
                self.setNeedsDisplay()
                
                self.parentTableView?.beginUpdates()
                self.parentTableView?.endUpdates()
            }
        }
    }
    
    func setDetail(detailText: String?) {

        self.detailText = detailText
        self.attributeDetail.text = detailText

        if self.numberOfLinesDisplayed(label: self.attributeDetail) > 3 {
            let attributedText = self.buildAttributedText(detail: detailText!)
            self.attributeDetail.attributedText = attributedText
        }
    }

    func buildAttributedText(detail: String) -> NSMutableAttributedString {
        let detailText = [ NSFontAttributeName: UIFont.bodyFont() ]
        
        var numberOfVisibleChars = self.numberOfCharsDisplayed(label: self.attributeDetail)

        if detail.characters.count < numberOfVisibleChars {
            numberOfVisibleChars = detail.characters.count - 1
        }

        let firstPart = detail.substring(to:detail.index(detail.startIndex, offsetBy: numberOfVisibleChars))
        
        let resultString = NSMutableAttributedString(string: "\(firstPart)... ", attributes: detailText)

        let linkText = [NSForegroundColorAttributeName: UIColor.primaryColor(), NSFontAttributeName: UIFont.bodyFont() ]
        let remainder = NSAttributedString(string: "\(CommonStrings.FootnoteMoreButton342)", attributes: linkText)

        resultString.append(remainder)
        
        return resultString
    }
    
    func numberOfCharsDisplayed(label: UILabel) -> Int {
        let charSize = label.font.lineHeight
        return Int( (label.frame.size.width / charSize)) * self.numberOfLinesDisplayed(label: label)
    }
    
    func numberOfLinesDisplayed(label: UILabel) -> Int {
        
        let textSize = CGSize(width: label.frame.size.width, height: CGFloat(MAXFLOAT))
        
        let height = label.sizeThatFits(textSize).height
        let charSize = label.font.lineHeight
        
        return Int(height/charSize);
    }

    func hideFeedback(){
        if self.attributeFeedback != nil{
            self.attributeFeedback.removeFromSuperview()
        }
    }
}
