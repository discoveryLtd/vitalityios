//
//  MyHealthLearnMoreViewController.swift
//  VitalityActive
//
//  Created by Erik John T. Alicaya (ADMIN) on 06/03/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import VitalityKit
import VIAUIKit
import VIAUtilities
import VIACommon

public final class MyHealthLearnMoreViewController: VIATableViewController {
    
    fileprivate var hasLoadedContent                        = false
    fileprivate var htmlContentHeight: CGFloat              = 0.0
    fileprivate var validContent:String                     = ""
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = CommonStrings.LearnMoreButtonTitle104
        setupNavBar()
        configureAppearance()
        configureTableView()
    }
    
    override public func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        loadArticleContent(contentID: "mwb-stressor-content")
    }
    
    override public func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.hideBackButtonTitle()
    }
    
    func configureAppearance() {
        navigationController?.makeNavigationBarTransparent()
        navigationController?.navigationBar.barTintColor = UIColor.white
    }
    
    func setupNavBar() {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        
        self.hideBackButtonTitle()
    }
    
    func configureTableView() {
        self.tableView.register(VIAGenericContentCell.nib(), forCellReuseIdentifier: VIAGenericContentCell.defaultReuseIdentifier)
        self.tableView.register(VIATableViewCell.self, forCellReuseIdentifier: VIATableViewCell.defaultReuseIdentifier)
        self.tableView.register(VIAWebViewCell.nib(), forCellReuseIdentifier: VIAWebViewCell.defaultReuseIdentifier)
        self.tableView.estimatedRowHeight = 75
        self.tableView.sectionHeaderHeight = 0
        self.tableView.sectionFooterHeight = 0
        self.tableView.estimatedSectionHeaderHeight = 0
        self.tableView.estimatedSectionFooterHeight = 0
        self.tableView.contentInset = UIEdgeInsets(top: 25, left: 0, bottom: 0, right: 0)
        self.tableView.backgroundColor = .white
        self.tableView.tableFooterView  = UIView() /* Set this to remove the preceeding empty rows in the table. */
    }
    
    enum StressorLearnMoreSections: Int, EnumCollection {
        case Content = 0
        case Disclaimer = 1
        
        func numberOfRows() -> Int {
            switch self {
            case .Content:
                return 1
            case .Disclaimer:
                return 1
            }
        }
    }
    // MARK: - UITableView datasource
    
    override public func numberOfSections(in tableView: UITableView) -> Int {
        return StressorLearnMoreSections.allValues.count
    }
    
    /* Number of Rows per section in the table. */
    override public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let learnMoreSection = StressorLearnMoreSections(rawValue: section)
        
        if learnMoreSection == .Content {
            return 1
        }
        else if learnMoreSection == .Disclaimer {
            return 1
        }
        return 0
        
    }
    
    override public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let section = StressorLearnMoreSections(rawValue: indexPath.section)
        
        if section == .Content {
            return (hasLoadedContent && htmlContentHeight != 0) ? htmlContentHeight: UITableViewAutomaticDimension
        }else if section == .Disclaimer{
            return 50
        }else{
            return 0
        }
    }
    
    override public func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let section = StressorLearnMoreSections(rawValue: indexPath.section) {
            if section == .Content && indexPath.row < section.numberOfRows() - 1 {
                cell.separatorInset = UIEdgeInsetsMake(0, max(UIScreen.main.bounds.size.width, UIScreen.main.bounds.size.height), 0, 0)
            }
        }
    }

    
    override public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let learnMoreSection = StressorLearnMoreSections(rawValue: indexPath.section)
        
        if learnMoreSection == .Content {
            return self.configureContentCell(indexPath)
        }
        else if learnMoreSection == .Disclaimer {
            return configureDisclaimerItemCell(indexPath)
        }
        
        return tableView.dequeueReusableCell(withIdentifier: VIATableViewCell.defaultReuseIdentifier, for: indexPath)
    }
    
    override public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let learnMoreSection = StressorLearnMoreSections(rawValue: indexPath.section)
        
        if learnMoreSection == .Disclaimer {
            viewDisclaimer()
        }
    }
    
    func configureContentCell(_ indexPath: IndexPath) -> UITableViewCell {
        
        let cell = self.tableView.dequeueReusableCell(withIdentifier: VIAWebViewCell.defaultReuseIdentifier, for: indexPath) as! VIAWebViewCell
        
        cell.webView.scrollView.isScrollEnabled = false
        
        cell.webView.tag        = indexPath.row /* Keep this tag for reloading of table row after the content is loading from CMS*/
        cell.webView.delegate   = self
        cell.webView.frame      = CGRect(x: 0, y: 0, width: cell.frame.size.width, height: self.htmlContentHeight)
        cell.selectionStyle     = UITableViewCellSelectionStyle.none
        
        /* Load our content into the webview */
        if !validContent.isEmpty && !hasLoadedContent{
            self.hasLoadedContent = true
            cell.webView.loadHTMLString(validContent, baseURL: nil)
        }
        
        
        return cell
    }
    
    func configureDisclaimerItemCell(_ indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: VIATableViewCell.defaultReuseIdentifier, for: indexPath)
        
        if let tableViewCell = cell as? VIATableViewCell {
            
            tableViewCell.textLabel?.text = CommonStrings.DisclaimerTitle265
            
            tableViewCell.imageView?.image = UIImage.templateImage(asset: MYHAsset.generalDocsSmall)
            tableViewCell.imageView?.tintColor = UIColor.primaryColor()
            
            tableViewCell.accessoryType = .disclosureIndicator
            tableViewCell.selectionStyle = .none
            
            return tableViewCell
        }
        
        
        return cell
    }
    
    override public func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDisclaimer", let controller = segue.destination as? VIAWebContentViewController {
            let webContentViewModel = VIAWebContentViewModel(articleId: "mwb-disclaimer-content")
            controller.viewModel = webContentViewModel
            controller.title = CommonStrings.DisclaimerTitle265
        }
    }
    
    func viewDisclaimer() {
        self.performSegue(withIdentifier: "showDisclaimer", sender: self)
    }
}

// MARK: Data + Networking
extension MyHealthLearnMoreViewController : CMSConsumer {
    
    public func loadArticleContent(contentID: String) {
        if !hasLoadedContent {
            showHUDOnView(view: self.view)
            
            getCMSArticleContent(articleId: contentID, completion: { [weak self] error, content in
                self?.hideHUDFromView(view: self?.view)
                if let error = error {
                    self?.handle(error: error)
                }
                if let validContent = content {
                    self?.validContent = validContent
                    self?.tableView.reloadData()
                }
            })
        }
    }
    
    private func handle(error: Error) {
        switch error {
        case is BackendError:
            if let backendError = error as? BackendError {
                self.handleBackendErrorWithAlert(backendError, tryAgainAction: { [weak self] in
                    // TODO: Handle fallback
                })
            }
            break
        case is CMSError:
            displayErrorWithUnwindAction()
            break
        default:
            displayErrorWithUnwindAction()
            break
        }
    }
    
    private func displayErrorWithUnwindAction() {
        displayUnknownServiceErrorOccurredAlert(okAction: { [weak self] in
            // TODO: Handle fallback
        })
    }
}

// MARK: Web View Delegate
extension MyHealthLearnMoreViewController: UIWebViewDelegate{
    public func webViewDidStartLoad(_ webView: UIWebView) {
        /* Disable user interaction */
        webView.stringByEvaluatingJavaScript(from: "document.documentElement.style.webkitUserSelect='none'")
        webView.stringByEvaluatingJavaScript(from: "document.documentElement.style.webkitTouchCallout='none'")
    }
    
    public func webViewDidFinishLoad(_ webView: UIWebView) {
        htmlContentHeight = webView.scrollView.contentSize.height
        
        self.tableView.reloadData()
    }
}
