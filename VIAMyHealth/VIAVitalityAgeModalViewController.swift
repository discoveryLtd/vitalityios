import Foundation
import UIKit
import VIAUIKit
import VitalityKit
import VIAUtilities
import VIACommon

public class VIAVitalityHealthModalTableViewController: VIATableViewController {
    public var showBarButton: Bool = true
    var feedbackModel = MyHealthFeedbackModel(cardSection: SectionRef.Unknown)
    public var cardIndex: Int! = -1

    override public func awakeFromNib() {
        super.awakeFromNib()
        self.setupTitle()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setupTitle()
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        self.setupTitle()
    }
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        
        if !VIAApplicableFeatures.default.shouldSkipMyHealthLandingScreen() {
            setTitleAndFeedBackModel()
        }
    
        addBarButtonItems()
        self.configureTable()
        
    }
    public func setTitleAndFeedBackModel(){
        var titleString: String? = ""
        switch cardIndex {
        case 1: // Nutrition Results.
            titleString = CommonStrings.Vna.Label1965
            feedbackModel = MyHealthFeedbackModel(cardSection: SectionRef.NutritionSection)
        case 2: // Stressor Feedback.
            titleString = CommonStrings.Mwb.Result.CardTitle1228
            feedbackModel = MyHealthFeedbackModel(cardSection: SectionRef.Stressor)
        case 3: // Psychological Feedback.
            titleString = CommonStrings.Mwb.Result.CardTitle1260
            feedbackModel = MyHealthFeedbackModel(cardSection: SectionRef.Psycological)
        case 4: // Social Feedback.
            titleString = CommonStrings.Mwb.Result.CardTitle1277
            feedbackModel = MyHealthFeedbackModel(cardSection: SectionRef.Social)
        default: // Vitality Age.
            titleString = CommonStrings.MyHealth.VitalityAgeScreenTitle613
            feedbackModel = MyHealthFeedbackModel(cardSection: SectionRef.VAgeSection)
        }
        title = titleString
    }

    override public func viewWillAppear(_ animated: Bool) {
        showOnboardingIfRequired()
        loadHealthInformation()
    }
    
    func showOnboardingIfRequired() {
        if VIAApplicableFeatures.default.shouldSkipMyHealthLandingScreen(){
            if (!AppSettings.hasShownMyHealthOnboarding()) {
                AppSettings.setHasShownMyHealthOnboarding()
                performSegue(withIdentifier: "myHealthOnboardingFromVitalityAge", sender: self)
            }
        }
    }
    
    func isAgeUnknown() -> Bool {
        if let vitalityAge = HealthInformationRepository().getHealthAttributeBy(attributeType: PartyAttributeTypeRef.VitalityAge){
            return PartyAttributeFeedbackRef.vitalityAgeStatus(vitalityAge: vitalityAge) == .Unknown
        }
        return true
    }
    
    func loadHealthInformation(){
        /**
         If market is UKE, healthInformation is not pulled
         because MyHealthLandingScreen is skipped.
         **/
        if VIAApplicableFeatures.default.shouldSkipMyHealthLandingScreen() {
            let helper = HeatlhAttributesHelper()
            
            self.showHUDOnView(view: self.view)
            helper.healthAttributes { [weak self] (error) in
                self?.hideHUDFromView(view: self?.view)
                
                guard let _ = error else {
                    self?.removeStatusView()
                    self?.setTitleAndFeedBackModel()
                    self?.tableView.reloadData()
                    self?.tableView.refreshControl?.endRefreshing()
                    return
                }
                
                guard error == nil else {
                    self?.handleErrorOccurred(error!, isCacheOutdated: true)
                    return
                }
            }
        }
    }
    
    func handleErrorOccurred(_ error: Error, isCacheOutdated: Bool?) {
        let backendError = error as? BackendError ?? BackendError.other
        if let outdated = isCacheOutdated, outdated == true {
            let statusView = self.handleBackendErrorWithStatusView(backendError, tryAgainAction: { [weak self] in
                self?.loadHealthInformation()
            })
            configureStatusView(statusView)
        } else {
            self.handleBackendErrorWithAlert(backendError, tryAgainAction: { [weak self] in
                self?.loadHealthInformation()
            })
        }
    }
    
    func configureTable() {
        self.tableView.register(VitalityAgeCardTableViewCell.nib(), forCellReuseIdentifier: VitalityAgeCardTableViewCell.defaultReuseIdentifier)
        self.tableView.register(VIATableViewCell.nib(), forCellReuseIdentifier: VIATableViewCell.defaultReuseIdentifier)
        self.tableView.register(MyHealthCardTableViewCell.nib(), forCellReuseIdentifier: MyHealthCardTableViewCell.defaultReuseIdentifier)
        self.tableView.register(MyHealthCardUpdatedTableViewCell.nib(), forCellReuseIdentifier: MyHealthCardUpdatedTableViewCell.defaultReuseIdentifier)
        self.tableView.register(MyHealthFeedbackTableViewCell.nib(), forCellReuseIdentifier: MyHealthFeedbackTableViewCell.defaultReuseIdentifier)
        self.tableView.register(MyHealthSectionHeaderView.nib(), forHeaderFooterViewReuseIdentifier: MyHealthSectionHeaderView.defaultReuseIdentifier)
        self.tableView.register(MyHealthAttributePropertyTableViewCell.nib(), forCellReuseIdentifier: MyHealthAttributePropertyTableViewCell.defaultReuseIdentifier)
        
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 150
        self.tableView.contentInset = UIEdgeInsets(top: -18, left: 0, bottom: 30, right: 0)
        self.tableView.backgroundColor = UIColor.tableViewBackground()
    }
    
    public override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
//        self.tableView.reloadData()
    }
    
    override public func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.hideBackButtonTitle()
    }
    
    func setupTitle() {
        self.title = CommonStrings.MyHealth.VitalityAgeScreenTitle613
        self.tabBarItem.title = CommonStrings.MenuMyhealthButton7
        self.navigationController?.tabBarItem.title = CommonStrings.MenuMyhealthButton7
        
        if VIAApplicableFeatures.default.shouldSkipMyHealthLandingScreen(){
            self.navigationController?.navigationBar.topItem?.title = CommonStrings.MenuMyhealthButton7
        }
    }
    
    func addBarButtonItems() {
        if showBarButton {
            let doneButton = UIBarButtonItem(title: CommonStrings.DoneButtonTitle53, style: .plain, target: self, action: #selector(done(_:)))
            doneButton.tintColor = UIColor.jungleGreen()
            self.navigationItem.rightBarButtonItem = doneButton
        }
    }

    func done(_ sender: Any?) {
        self.dismiss(animated: true)
    }
    
    // MARK - TableView enums

    enum SocialStaticSections: Int, EnumCollection {
        /*
         Always return 4 since the number of sections in 'Social' are static.
         */
        case header = 0
        case staticInfo = 1
        case tip = 2
        case help = 3
    }
    
    // MARK - TableView delegate

    override public func numberOfSections(in tableView: UITableView) -> Int {

        
        if self.showBarButton ||
            (isAgeUnknown() && VIAApplicableFeatures.default.shouldSkipMyHealthLandingScreen()){ // Display like myHealthLandingScreen
            return 1
        }
        
        if let section = feedbackModel.getSection() {
            if section.typeKey == SectionRef.Social {
                return SocialStaticSections.allValues.count
            }
        }
        
        return getSectionCount()
    }
    
    override public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section > 0 {
            if indexPath.section < getSectionCount() - 1 {
                guard let cell = tableView.cellForRow(at: indexPath) as? MyHealthFeedbackTableViewCell else {
                    self.showMoreResults()
                    return
                }
                
                if let attributeAndSection = cell.myHealthAttribute {
                    self.showAttributeDetails(myHealthAttribute: attributeAndSection.attribute)
                }
            } else {
                let socialSectionValue = SocialStaticSections(rawValue: indexPath.section)
                
                if isSocialCard() && (socialSectionValue == .staticInfo || socialSectionValue == .tip) {
                    return
                } else {
                    showHelp()
                }
            }
        }
    }
    
    override public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView: MyHealthSectionHeaderView = tableView.dequeueReusableHeaderFooterView(withIdentifier: MyHealthSectionHeaderView.defaultReuseIdentifier) as! MyHealthSectionHeaderView
        
        if (cardIndex == 0 || cardIndex == 1) && section < getSectionCount() - 1 {
            if section == 0 {
                return nil
            }
            
            headerView.myHealthSecton = feedbackModel.sectionAt(sectionNumber: section)
            
            return headerView
        } else {
            let socialSectionValue = SocialStaticSections(rawValue: section)
            
            if isSocialCard() && socialSectionValue == .tip {
                headerView.setHeaderText(text: CommonStrings.Mwb.Learnmore.Landing.SectionTitle1259)
                headerView.setHeaderImage(image: VIAMyHealthAsset.tipsDetail.image)
                
                return headerView
            }
            
            return nil
        }
    }

    override public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let socialSectionValue = SocialStaticSections(rawValue: section)
        
        if self.showBarButton {
            return 2
        } else if isSocialCard() && socialSectionValue == .staticInfo {
            return 1
        }
        
        /* We need to make sure that the current section is not for the Help section */
        else if section < getSectionCount() {
            return feedbackModel.numberOfRowsInSection(section: section)
        } else {
            return 1 /* This is for the Help row */
        }
    }

    override public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let row = indexPath.row
        let socialSectionValue = SocialStaticSections(rawValue: indexPath.section)

        if indexPath.section == 0 {
            if row == 0 {
                guard let cell = self.tableView.dequeueReusableCell(withIdentifier: MyHealthCardTableViewCell.defaultReuseIdentifier, for: indexPath) as? MyHealthCardTableViewCell else { return tableView.defaultTableViewCell() }
                
                switch cardIndex {
                case 1:
                    cell.setupCardView(atIndex: cardIndex, attributeTypeRef: .Nutrition)
                    cell.hideAnimationView()
                case 2:
                    cell.setupCardView(atIndex: cardIndex, attributeTypeRef: .Stressor)
                case 3:
                    cell.setupCardView(atIndex: cardIndex, attributeTypeRef: .Psychological)
                case 4:
                    cell.setupCardView(atIndex: cardIndex, attributeTypeRef: .Social)
                default:
                    cell.setupCardView(atIndex: cardIndex, attributeTypeRef: .VitalityAge)
                    cell.showBarButton = self.showBarButton
                    cell.hideAnimationView()
                }
                
                if cardIndex != 0 {
                    cell.hideAgeLabelView()
                }
                
                cell.parentViewController = self
                cell.selectionStyle = .none
                
                return cell
            }
            
            if row == 1 {
                guard let cell = self.tableView.dequeueReusableCell(withIdentifier: MyHealthCardUpdatedTableViewCell.defaultReuseIdentifier, for: indexPath) as? MyHealthCardUpdatedTableViewCell else { return tableView.defaultTableViewCell() }
                cell.setupViewFromCard(at: cardIndex)
                
                return cell
            }
        } else if indexPath.section < getSectionCount() - 1 {
            let numberOfRowsInSection = feedbackModel.numberOfRowsInSection(section: indexPath.section)
            
            if feedbackModel.sectionIsEmpty(section: indexPath.section) {
                let section = feedbackModel.sectionAt(sectionNumber: indexPath.section)

                let emptyHealthAttribute = feedbackModel.emptyHealthAttributeForSection(sectionRef: section.typeKey)
                return self.buildMyHealthFeedbackTableViewCell(indexPath: indexPath, healthAttribute: emptyHealthAttribute)
            } else if indexPath.row < numberOfRowsInSection - 1 || numberOfRowsInSection == 1 {
                let myHealthSection = feedbackModel.sectionAt(sectionNumber: indexPath.section)
                
                let sortedAttributes = myHealthSection.healthAttributes.sorted(by: { (left, right) -> Bool in
                    return left.sortOrder < right.sortOrder
                })

                return self.buildMyHealthFeedbackTableViewCell(indexPath: indexPath, healthAttribute: sortedAttributes[indexPath.row], sectionRef: myHealthSection.typeKey)
            } else {
                let defaultTableViewCell = tableView.defaultTableViewCell();
                defaultTableViewCell.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
                defaultTableViewCell.textLabel?.text = CommonStrings.MoreResults792
                defaultTableViewCell.selectionStyle = .none
                
                return defaultTableViewCell
            }
        } else if isSocialCard() && socialSectionValue == .staticInfo {
            return buildCustomSocialCell(at: indexPath)
        } else if isSocialCard() && socialSectionValue == .tip {
            return buildCustomSocialTipCell(at: indexPath)
        } else {
            return helpCell(at: indexPath)
        }
        
        return tableView.defaultTableViewCell()
    }
    
    func buildMyHealthFeedbackTableViewCell(indexPath: IndexPath, healthAttribute: GMIReportedHealthAttribute, sectionRef: SectionRef = .VAgeNeedToKnow) -> UITableViewCell {
        
        // there is a reason for this - the cells are dynamically creating and removing subviews and when we deque it is reusing cells and sometimes those cells are not in the correct state
        // this will therefore force the use of a new cell
        let cell = MyHealthFeedbackTableViewCell.viewFromNib(owner: self)!
        
        cell.parentViewController = self
        let attribute = healthAttribute
        cell.myHealthAttribute = (attribute, sectionRef)
        cell.selectionStyle = .none
        
        return cell
    }
    
    func helpCell(at indexPath: IndexPath) -> VIATableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: VIATableViewCell.defaultReuseIdentifier, for: indexPath) as! VIATableViewCell
        
        cell.labelText      = CommonStrings.HelpButtonTitle141
        cell.cellImage      = VIAMyHealthAsset.help.templateImage
        cell.accessoryType  = .disclosureIndicator
        cell.selectionStyle = .none
        
        cell.cellImageView.tintColor = UIColor.currentGlobalTintColor()
        return cell
    }
    
    func buildCustomSocialCell(at indexPath: IndexPath) -> UITableViewCell {
        guard let cell = self.tableView.dequeueReusableCell(withIdentifier: MyHealthAttributePropertyTableViewCell.defaultReuseIdentifier, for: indexPath) as? MyHealthAttributePropertyTableViewCell else { return tableView.defaultTableViewCell() }
        
        cell.parentTableView = self.tableView
        cell.accessoryType = .none
        cell.selectionStyle = .none
        
        // Source row.
        cell.attributeProperty.text = CommonStrings.DetailScreen.SourceTitle198
        if let event = feedbackModel.section?.healthAttributes.first?.event {
            cell.setDetail(detailText: event.typeName)
            if let formattedDate = formatDate(date: event.dateLogged) {
                cell.attributeFeedback.text = CommonStrings.SummaryScreen.DateTestedTitle185(formattedDate)
            }
        }
        cell.setupViewAsEvent()
        
        cell.attributeDetail.font = UIFont.bodyFont()
        
        return cell
    }
    
    func buildCustomSocialTipCell(at indexPath: IndexPath) -> UITableViewCell {
        guard let cell = self.tableView.dequeueReusableCell(withIdentifier: MyHealthAttributePropertyTableViewCell.defaultReuseIdentifier, for: indexPath) as? MyHealthAttributePropertyTableViewCell else { return tableView.defaultTableViewCell() }
        
        cell.accessoryType = .none
        cell.selectionStyle = .none
        
        if let section = HealthInformationRepository().getHealthAttributeBy(attributeType: .Social) {
            cell.setupViewAsTip(feedbackTip: (section.feedbacks.first?.feedbackTips.first)!, asPreview: false)
        }
        
        return cell

    }
    
    func formatDate(date: String?) -> String? {
        if let dateToFormat = date {
            guard let actualDate = DateFormatter.apiManagerFormatterWithMilliseconds().date(from:dateToFormat) else {return ""}
            return DateFormatter.dayLongMonthLongYearShortTimeFormatter().string(from: actualDate)
        }
        
        return date
    }
    
    func showAttributeDetails(myHealthAttribute: GMIReportedHealthAttribute?){
        if let attribute = myHealthAttribute {
            if attribute.attributeTypeCode != "" {
                self.performSegue(withIdentifier: "showHealthAttributeFromSummary", sender: self)
            }
        }
    }
    
    func showMoreResults(){
        self.performSegue(withIdentifier: "myHealthMoreResults", sender: self)
    }
    
    func showHelp() {
        self.performSegue(withIdentifier: "showHelp", sender: self)
    }
    
    override public func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "myHealthOnboardingFromVitalityAge" {
            
            guard let navController = segue.destination as? UINavigationController else { return }
            guard let viewController = navController.topViewController as? VIAVitalityAgeOnboardingTableViewController else { return }
            
            viewController.vitalityAgeOnboardingViewmodel = VitalityAgeOnboardingViewModel()
        }
        
        if segue.identifier == "myHealthMoreResults" {
            let selectedSection = feedbackModel.sectionAt(sectionNumber: tableView.indexPathForSelectedRow!.section)
            guard let controller = segue.destination as? VIAMyHealthMoreResultsTableViewController else {return}
            controller.myHealthSection = selectedSection
        }
        
        if segue.identifier == "showHealthAttributeFromSummary" {
            guard let cell = tableView.cellForRow(at: tableView.indexPathForSelectedRow!) as? MyHealthFeedbackTableViewCell else { return }
            
            if let healthAttributeAndSection = cell.myHealthAttribute {
                guard let controller = segue.destination as? VIAMyHealthAttributeDetailsTableViewController else {
                    return
                }
                controller.reportedHealthAttributeAndSection = healthAttributeAndSection
            }
        }
        
        if segue.identifier == "myHealthLearnMore" {
            guard let destination = segue.destination as? MyHealthLearnMoreTableViewController else { return }
            let status = sender as! PartyAttributeFeedbackRef
            switch cardIndex {
            case 1: // Nutrition Results.
                destination.viewModel = VNALearnMoreViewModel(status: status)
                destination.cardIndex = 1
            case 2: // Stressor Feedback.
                destination.viewModel = MWBStressorLearnMoreViewModel(status: status)
                destination.cardIndex = 2
            case 3:  // Psychological Feedback.
                destination.viewModel = MWBPsychologicalLearnMoreViewModel(status: status)
                destination.cardIndex = 3
            case 4:  // Social Feedback.
                destination.viewModel = MWBSocialLearnMoreViewModel(status: status)
                destination.cardIndex = 4
            default: // Vitality Age.
                destination.viewModel = VitalityAgeLearnMoreViewModelImpl()
            }
        }
    }
    
    private func getSectionCount() -> Int{
        return feedbackModel.numberOfSections() + 1 // +1 for help section.
    }
    
    func isSocialCard() -> Bool {
        if let section = feedbackModel.getSection() {
            if section.typeKey == SectionRef.Social {
                return true
            }
        }
        
        return false
    }
}
