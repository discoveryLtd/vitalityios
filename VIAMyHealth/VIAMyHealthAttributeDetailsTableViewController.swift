import Foundation
import UIKit
import VIAUIKit
import VitalityKit
import VIAUtilities
import VIACommon

public enum AttributePropertyType {
    case Source
    case Tip
    case Value
    case Generic
}

public class AttributeProperty {

    var attributePropertyType: AttributePropertyType = .Generic
    let heading: String?
    let property: String?
    var subTitle: String?
    
    init(heading: String?, property: String?, subTitle: String? = nil) {
        self.heading = heading
        self.property = property
        self.subTitle = subTitle
    }
    
    func showSubTitle() -> Bool {
        return !String.isNilOrEmpty(string: self.subTitle)
    }
}

class AttributeDetailsModel {
    
    var reportedHealthAttribute: GMIReportedHealthAttribute?
    
    init(_ reportedHealthAttribute: GMIReportedHealthAttribute?) {
        self.reportedHealthAttribute = reportedHealthAttribute
    }
    
    func numberOfSections() -> Int {
        if self.attributeTips().count == 0 {
            return 1
        }
        return 2
    }
    
    func attributeTips() -> [GMIReportedFeedbackTip] {
        var tips = [GMIReportedFeedbackTip]()
        
        self.reportedHealthAttribute?.feedbacks.forEach({ (feedback) in
            tips.append(contentsOf: feedback.feedbackTips)
        })
        
        return tips
    }
    
    private func mostRelevantFeedback(myHealthAttribute: GMIReportedHealthAttribute) -> GMIReportedFeedback? {
        
        if myHealthAttribute.feedbacks.count > 0 {
            if let validFeedback = myHealthAttribute.feedbacks.first(where: { (feedback) -> Bool in return feedback.feedbackTips.count > 0 }) {
                return validFeedback
            }
            return myHealthAttribute.feedbacks.sorted(by: { (this, that) -> Bool in
                this.feedbackTypeKey.rawValue > that.feedbackTypeKey.rawValue
            })[0]
        }
        
        return nil
    }
    
    func healthAttributeProperties() -> [AttributeProperty] {
        var properties = [AttributeProperty]()
        
        if let attribute = reportedHealthAttribute {
            if isNotMWBCard() {
                if String.isNilOrEmpty(string: attribute.value){
                    properties.append( AttributeProperty(heading: CommonStrings.DetailScreen.CaptureResultTitle1074, property: CommonStrings.MyHealth.PhysicalActivityCaptureResultMessage1075))
                } else {
                    
                    /* Isolate displayValue for BMI (Do not round off) */
                    var displayValue = ""
                    guard let attributeValue = attribute.displayValue else { return [] }
                    if attribute.attributeTypeKey == .BMI {
                        displayValue = attributeValue
                    } else {
                        displayValue = self.formatValue(value: attributeValue)!
                    }
                    
                    let attributeProperty = AttributeProperty(heading: CommonStrings.RecentResult1144, property: displayValue)
                    if let feedback = self.mostRelevantFeedback(myHealthAttribute: attribute) {
                        attributeProperty.subTitle = feedback.feedbackTypeName
                    }
                    attributeProperty.attributePropertyType = .Value
                    properties.append(attributeProperty)
                }
            }

            if attribute.recommendations.count > 0 {
                let recommendation = attribute.recommendations[0]
                
                if !String.isNilOrEmpty(string: recommendation.friendlyValue) {
                    let attributeProperty = AttributeProperty(heading: CommonStrings.DetailScreen.RecommendedResultTitle1076, property: recommendation.friendlyValue)
                    
                    properties.append(attributeProperty)
                }
            }
            
            if attribute.feedbacks.count > 0 {
                let feedback = attribute.feedbacks.first(where: { (feedback) -> Bool in
                    return !String.isNilOrEmpty(string: feedback.whyIsThisImportant)
                })

                if let validFeedback = feedback {
                    var headingText: String! = ""
                    if isNotMWBCard() {
                        headingText = CommonStrings.DetailScreen.WhyIsThisImportantTitle1077
                    }
                    let attributeProperty = AttributeProperty(heading: headingText, property: validFeedback.whyIsThisImportant)

                    properties.append(attributeProperty)
                }
            }

            if let event = attribute.event {
                if !String.isNilOrEmpty(string: event.typeName) {
                    let attributeProperty = AttributeProperty(heading: CommonStrings.DetailScreen.SourceTitle198, property: event.typeName)
                    attributeProperty.attributePropertyType = .Source
                    
                    if let formattedDate = self.formatDate(date: event.dateLogged){
                        attributeProperty.subTitle = CommonStrings.SummaryScreen.DateTestedTitle185(formattedDate)
                    }
                    
                    properties.append(attributeProperty)
                }
            }
        }

        return properties
    }

    func numberOfRowsInSection(section: Int) -> Int {
        if section == 0 {
            guard self.reportedHealthAttribute != nil else { return 0 }
            return self.healthAttributeProperties().count
        } else {
            let count = attributeTips().count
            return (count > 3) ? 3 : count
        }
    }
    
    func shouldShowMoreTips() -> Bool {
        return self.attributeTips().count > 3
    }
    
    func tipAtIndex(index:Int) -> GMIReportedFeedbackTip {
        return attributeTips()[index]
    }

    func formatValue(value: String?) -> String? {
        if let actualValue = value {
            if let doubleValue = Double(actualValue) {
                let roundedValue = Int(round(doubleValue))
                return String(roundedValue)
            }
        }
        return value
    }

    func formatDate(date: String?) -> String? {
        
        if let dateToFormat = date {
            guard let actualDate = DateFormatter.apiManagerFormatterWithMilliseconds().date(from:dateToFormat) else {return ""}
            return DateFormatter.dayLongMonthLongYearShortTimeFormatter().string(from: actualDate)
        }
        
        return date
    }
    
    public func isNotMWBCard() -> Bool {
        if reportedHealthAttribute?.attributeTypeKey == .Stressor ||
            reportedHealthAttribute?.attributeTypeKey == .TraumaticEvents ||
            reportedHealthAttribute?.attributeTypeKey == .PsychoPositive ||
            reportedHealthAttribute?.attributeTypeKey == .PsychoEnergy ||
            reportedHealthAttribute?.attributeTypeKey == .PsycoEmotion ||
            reportedHealthAttribute?.attributeTypeKey == .PsycoAnxiety ||
            reportedHealthAttribute?.attributeTypeKey == .Social {
            return false
        } else {
            return true
        }
    }
    
    public func isStressorCard() -> Bool {
        if reportedHealthAttribute?.attributeTypeKey == .Stressor {
            return true
        } else {
            return false
        }
    }
    
    public func cardFeedbackValue() -> PartyAttributeFeedbackRef? {
        return reportedHealthAttribute?.feedbacks.last?.feedbackTypeKey
    }
}

class CausesOfStressModel {
    private var causesString: String? = ""
    private let delimiter = "##"
    
    init(feedbackTypeKey: PartyAttributeFeedbackRef?) {
        if feedbackTypeKey == .StressorRightTrack {
            self.causesString = CommonStrings.Mwb.Feedback.Stressor.CauseNoStress1258
        } else if feedbackTypeKey == .StressorGettingThere {
             self.causesString = CommonStrings.Mwb.Feedback.Stressor.CauseMedHeader1258
        } else if feedbackTypeKey == .StressorWorkTogether {
            self.causesString = CommonStrings.Mwb.Feedback.Stressor.CauseHighHeader1258
        }
    }
    
    public func causesCount() -> Int {
        return causesString!.components(separatedBy: delimiter).count
    }
    
    public func causesList() -> Array<String> {
        return causesString!.components(separatedBy: delimiter)
    }
}

class VIAMyHealthAttributeDetailsTableViewController: VIATableViewController {

    private var reportedHealthAttribute: GMIReportedHealthAttribute?
    
    override public func viewDidLoad() {
        super.viewDidLoad()

        self.configureTable()
    }
    
    override public func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.hideBackButtonTitle()
    }
    
    public var reportedHealthAttributeAndSection: (attribute: GMIReportedHealthAttribute, section: SectionRef)? {
        didSet {
            self.reportedHealthAttribute = reportedHealthAttributeAndSection?.attribute
            self.title = reportedHealthAttribute?.attributeTypeName
        }
    }
    
    func configureTable() {
        self.tableView.register(MyHealthSectionHeaderView.nib(), forHeaderFooterViewReuseIdentifier: MyHealthSectionHeaderView.defaultReuseIdentifier)
        self.tableView.register(MyHealthAttributePropertyTableViewCell.nib(), forCellReuseIdentifier: MyHealthAttributePropertyTableViewCell.defaultReuseIdentifier)
        
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 150
        
        self.tableView.backgroundColor = UIColor.tableViewBackground()
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        if AttributeDetailsModel(self.reportedHealthAttribute).isNotMWBCard() {
            // Show tip section.
            return AttributeDetailsModel(self.reportedHealthAttribute).numberOfSections()
        } else {
            //  Hide tip section.
            if AttributeDetailsModel(self.reportedHealthAttribute).isStressorCard() {
                // Show "What your results suggests" and "Likely causes of stress" sections.
                return 2
            } else {
                // "What your results suggests" only.
                return 1
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let model = AttributeDetailsModel(self.reportedHealthAttribute)
        let count = model.numberOfRowsInSection(section: section)

        if section == 0 {
            return count
        } else if AttributeDetailsModel(self.reportedHealthAttribute).isStressorCard() && section == 1 {
            return CausesOfStressModel(feedbackTypeKey: model.cardFeedbackValue()).causesCount()
        } else {
            return model.shouldShowMoreTips() ? count + 1 : count
        }
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if let sectionName = self.sectionTitle() {
            let headerView: MyHealthSectionHeaderView = tableView.dequeueReusableHeaderFooterView(withIdentifier: MyHealthSectionHeaderView.defaultReuseIdentifier) as! MyHealthSectionHeaderView
            
            let reportedSection = GMIReportedSection()

            reportedSection.typeName = sectionName
            headerView.myHealthSecton = reportedSection
            
            if !AttributeDetailsModel(self.reportedHealthAttribute).isNotMWBCard() {
                headerView.hideHeaderImage()
                
                // Insert "Likely causes of stress" section header.
                if AttributeDetailsModel(self.reportedHealthAttribute).isStressorCard() && section == 1 {
                    headerView.setHeaderText(text: CommonStrings.Mwb.Learnmore.Landing.SectionTitle1258)
                }
            }
            
            return headerView
        }
        
        return nil
    }
    
    private func sectionTitle() -> String? {
        if let reportedSection = reportedHealthAttributeAndSection {
            switch reportedSection.section {
            case .VAgeResultGood:
                return CommonStrings.TipsToMaintain1122
            case .VAgeResultBad:
                return CommonStrings.TipsToImprove1121
            case .VAgeNeedToKnow:
                return CommonStrings.MyHealth.MetricDetailTipsToImportance1123
            case .StressorDiag:
                return CommonStrings.Mwb.Learnmore.Landing.SectionTitle1257
            case .Traumatic:
                return CommonStrings.Mwb.Learnmore.Landing.SectionTitle1257
            case .Psycological:
                return CommonStrings.Mwb.Learnmore.Landing.SectionTitle1257
            case .Positive:
                return CommonStrings.Mwb.Learnmore.Landing.SectionTitle1257
            case .Energy:
                return CommonStrings.Mwb.Learnmore.Landing.SectionTitle1257
            case .Negative:
                return CommonStrings.Mwb.Learnmore.Landing.SectionTitle1257
            case .Anxiety:
                return CommonStrings.Mwb.Learnmore.Landing.SectionTitle1257
            default:
                return nil
            }
        }
        return nil
    }
    
    private func isVNAInHealthyRangeSection() -> Bool {
        if let reportedSection = reportedHealthAttributeAndSection {
            switch reportedSection.section {
            case .NutritionWhatWell:
                return true
            case .NutritionLookingGood:
                return true
            default:
                return false
            }
        }
        
        return false
    }

    func buildAttributeProperty(cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = MyHealthAttributePropertyTableViewCell.viewFromNib(owner: self)!
        
        cell.parentTableView = self.tableView
        cell.accessoryType = .none
        
        let properties = AttributeDetailsModel(self.reportedHealthAttribute).healthAttributeProperties()
        
        let healthAttributeProperty = properties[indexPath.row]
        
        cell.attributeProperty.text = healthAttributeProperty.heading
        cell.setDetail(detailText: healthAttributeProperty.property)
        
        if !healthAttributeProperty.showSubTitle() {
            cell.hideFeedback()
        } else {
            if cell.attributeFeedback != nil {
                cell.attributeFeedback.text = healthAttributeProperty.subTitle
            }
        }
        
        if self.isVNAInHealthyRangeSection() && healthAttributeProperty.attributePropertyType == .Generic {
            cell.hideFeedback()
        }
        
        if healthAttributeProperty.attributePropertyType == .Source {
            cell.setupViewAsEvent()
        } else if healthAttributeProperty.attributePropertyType == .Generic {
            cell.attributeDetail.font = UIFont.bodyFont()
        }
       
        cell.selectionStyle = .none
        
        return cell
    }
    
    func buildCausesOfStressRows(cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let defaultTableViewCell = tableView.defaultTableViewCell();
        let model = AttributeDetailsModel(self.reportedHealthAttribute)
        defaultTableViewCell.accessoryType = .none
        defaultTableViewCell.selectionStyle = .none
        defaultTableViewCell.textLabel?.text = CausesOfStressModel(feedbackTypeKey: model.cardFeedbackValue()).causesList()[indexPath.row]
        return defaultTableViewCell
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            return self.buildAttributeProperty(cellForRowAt: indexPath)
        } else if indexPath.section == 1 {
            if AttributeDetailsModel(self.reportedHealthAttribute).isStressorCard() {
                return self.buildCausesOfStressRows(cellForRowAt: indexPath)
            } else {
                let model = AttributeDetailsModel(self.reportedHealthAttribute)
                let count = model.numberOfRowsInSection(section: indexPath.section)
                
                if count == 1 || (indexPath.row < count) {
                    let tip = model.tipAtIndex(index: indexPath.row)
                    guard let cell = self.tableView.dequeueReusableCell(withIdentifier: MyHealthAttributePropertyTableViewCell.defaultReuseIdentifier, for: indexPath) as? MyHealthAttributePropertyTableViewCell else { return tableView.defaultTableViewCell() }
                    
                    cell.setupViewAsTip(feedbackTip: tip)
                    cell.accessoryType = .disclosureIndicator
                    
                    return cell
                } else if model.shouldShowMoreTips() {
                    let defaultTableViewCell = tableView.defaultTableViewCell();
                    defaultTableViewCell.accessoryType = .disclosureIndicator
                    defaultTableViewCell.textLabel?.text = CommonStrings.MyHealth.DetailMainTitleMoreTips1067
                    return defaultTableViewCell
                }
            }
        }
        
        return tableView.defaultTableViewCell()
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 1 && !AttributeDetailsModel(self.reportedHealthAttribute).isStressorCard() {
            let model = AttributeDetailsModel(self.reportedHealthAttribute)
            let count = model.numberOfRowsInSection(section: indexPath.section)

            if count == 1 || (indexPath.row < count) {
                guard (tableView.cellForRow(at: indexPath) as? MyHealthAttributePropertyTableViewCell) != nil else { return }
                
                self.performSegue(withIdentifier: "showTipDetails", sender: self)
            } else if model.shouldShowMoreTips() {
                self.performSegue(withIdentifier: "showMoreTips", sender: self)
            }
        }
    }
    
    override public func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showMoreTips" {
            guard let controller = segue.destination as? MyHealthMoreTipsTableViewController else { return }
            controller.reportedHealthAttribute = self.reportedHealthAttribute
        }
        
        if segue.identifier == "showTipDetails", let controller = segue.destination as? VIAWebContentViewController {
            guard let cell = self.tableView.cellForRow(at: self.tableView.indexPathForSelectedRow!) as? MyHealthAttributePropertyTableViewCell else {return}
            
            if let tip = cell.feedbackTip {
                if !String.isNilOrEmpty(string: tip.typeCode) {
                    let tipIdentifier = "\(tip.typeKey.rawValue)-\(tip.typeCode!.lowercased())"
                    let webContentViewModel = VIAWebContentViewModel(articleId: tipIdentifier)

                    controller.viewModel = webContentViewModel
                    controller.title = CommonStrings.MyHealth.DetailMainTitleTip1068
                }
            }
        }
    }
}
