import Foundation
import VitalityKit
import VIAUIKit
import VitalityKit

public class MyHealthOnboardingViewModel: OnboardingViewModel {

    public var buttonTitle: String {
        return CommonStrings.GetStartedButtonTitle103
    }

    public var labelTextColor: UIColor {
        return UIColor.night()
    }

    public var mainButtonTint: UIColor {
        return UIColor.primaryColor()
    }
    
    public var mainButtonHighlightedTextColor: UIColor? {
        return UIColor.day()
    }

    public var gradientColor: Color {
        return AppSettings.isAODAEnabled() ? .green : .none
    }

    public var heading: String {
        return CommonStrings.MenuMyhealthButton7
    }

    public var alternateButtonTitle: String? {
        return nil
    }

    public var imageTint: UIColor? {
        return UIColor.primaryColor()
    }
    
    public var contentItems: [OnboardingContentItem] {
        var items: [OnboardingContentItem] = [OnboardingContentItem]()
        
        items.append(OnboardingContentItem(heading: CommonStrings.MyHealth.OnboardingSection1Title628,
                                           content: CommonStrings.MyHealth.OnboardingSection1Description629,
                                           image: UIImage.templateImage(asset: MYHAsset.questionMarkSmall)))
        items.append(OnboardingContentItem(heading: CommonStrings.MyHealth.OnboardingSection2Title632,
                                           content: CommonStrings.MyHealth.OnboardingSection2Description633,
                                           image: UIImage.templateImage(asset: MYHAsset.manageHealthSmall)))
        items.append(OnboardingContentItem(heading: CommonStrings.MyHealth.OnboardingSection3Title634,
                                           content: CommonStrings.MyHealth.OnboardingSection3Description635,
                                           image: UIImage.templateImage(asset: MYHAsset.tips)))
        return items
    }

    public func setOnboardingHasShown() {
        AppSettings.setHasShownVHROnboarding()
    }
}
