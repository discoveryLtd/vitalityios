//
//  MWBLearnMoreViewModel.swift
//  VitalityActive
//
//  Created by OJ Garde on 3/13/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import VitalityKit
import VIAUtilities

public class MWBPsychologicalLearnMoreViewModel: VitalityAgeLearnMoreViewModel {
    let learnMoreRows : [VALearnMoreRow]
    
    public func numberOfSections() -> Int {
        return MyHealthLearnMoreSections.allValues.count
    }
    
    public func numberOfRows() -> Int {
        return learnMoreRows.count
    }
    
    static func buildUnknownLearnMore() -> [VALearnMoreRow] {
        return [
            VALearnMoreRow(
                header: CommonStrings.Mwb.Learnmore.Landing.HeaderTitle1265,
                content: CommonStrings.Mwb.Learnmore.Landing.HeaderDescription1266,
                asset: nil,
                headerFont: UIFont.title1Font(),
                contentFont: UIFont.subheadlineFont()
            ),
            VALearnMoreRow(
                header: CommonStrings.Mwb.Learnmore.Landing.SectionHeaderTitle1240,
                content: CommonStrings.Mwb.Learnmore.Landing.SectionDescription1267,
                asset: MYHAsset.calculate,
                headerFont: UIFont.title2Font(),
                contentFont: UIFont.bodyFont()
            ),
            VALearnMoreRow(
                header: CommonStrings.Mwb.Learnmore.Landing.SectionTitle1268,
                content: CommonStrings.Mwb.Learnmore.Landing.SectionDescription1269,
                asset: MYHAsset.psychological,
                headerFont: UIFont.title2Font(),
                contentFont: UIFont.bodyFont()
            ),
            VALearnMoreRow(
                header: CommonStrings.Mwb.OnboardingSection3Title1200,
                content: CommonStrings.Mwb.Learnmore.Landing.SectionDescription1270,
                asset: MYHAsset.understand,
                headerFont: UIFont.title2Font(),
                contentFont: UIFont.bodyFont()
            )
        ]
    }
    
    static func buildLetsWorkTogetherLearnMore() -> [VALearnMoreRow] {
        return [
            VALearnMoreRow(
                header: CommonStrings.Mwb.Learnmore.Landing.HeaderTitle1298,
                content: CommonStrings.Mwb.Learnmore.Landing.HeaderDescription1299,
                asset: nil,
                headerFont: UIFont.title1Font(),
                contentFont: UIFont.subheadlineFont()
            ),
            VALearnMoreRow(
                header: CommonStrings.Mwb.Learnmore.Landing.SectionTitle1255,
                content: CommonStrings.Mwb.Learnmore.Landing.SectionDescription1308,
                asset: MYHAsset.understand,
                headerFont: UIFont.title2Font(),
                contentFont: UIFont.bodyFont()
            ),
            VALearnMoreRow(
                header: CommonStrings.Mwb.Learnmore.Landing.SectionHeaderTitle1240,
                content: CommonStrings.Mwb.Learnmore.Landing.SectionDescription1267,
                asset: MYHAsset.calculate,
                headerFont: UIFont.title2Font(),
                contentFont: UIFont.bodyFont()
            )
        ]
    }
    
    static func buildYoureGettingThereLearnMore() -> [VALearnMoreRow] {
        return [
            VALearnMoreRow(
                header: CommonStrings.Mwb.Learnmore.Landing.HeaderTitle1275,
                content: CommonStrings.Mwb.Learnmore.Landing.HeaderDescription1276,
                asset: nil,
                headerFont: UIFont.title1Font(),
                contentFont: UIFont.subheadlineFont()
            ),
            VALearnMoreRow(
                header: CommonStrings.Mwb.Learnmore.Landing.SectionTitle1255,
                content: CommonStrings.Mwb.Learnmore.Landing.SectionDescription1308,
                asset: MYHAsset.understand,
                headerFont: UIFont.title2Font(),
                contentFont: UIFont.bodyFont()
            ),
            VALearnMoreRow(
                header: CommonStrings.Mwb.Learnmore.Landing.SectionHeaderTitle1240,
                content: CommonStrings.Mwb.Learnmore.Landing.SectionDescription1267,
                asset: MYHAsset.calculate,
                headerFont: UIFont.title2Font(),
                contentFont: UIFont.bodyFont()
            )
        ]
    }
    
    static func buildYoureOnTheRightTrackLearnMore() -> [VALearnMoreRow] {
        return [
            VALearnMoreRow(
                header: CommonStrings.Mwb.Learnmore.Landing.HeaderTitle1300,
                content: CommonStrings.Mwb.Learnmore.Landing.HeaderDescription1301,
                asset: nil,
                headerFont: UIFont.title1Font(),
                contentFont: UIFont.subheadlineFont()
            ),
            VALearnMoreRow(
                header: CommonStrings.Mwb.Learnmore.Landing.SectionTitle1255,
                content: CommonStrings.Mwb.Learnmore.Landing.SectionDescription1308,
                asset: MYHAsset.understand,
                headerFont: UIFont.title2Font(),
                contentFont: UIFont.bodyFont()
            ),
            VALearnMoreRow(
                header: CommonStrings.Mwb.Learnmore.Landing.SectionHeaderTitle1240,
                content: CommonStrings.Mwb.Learnmore.Landing.SectionDescription1267,
                asset: MYHAsset.calculate,
                headerFont: UIFont.title2Font(),
                contentFont: UIFont.bodyFont()
            )
        ]
    }
    
    static func buildOutdatedLearnMore() -> [VALearnMoreRow] {
        return [
            VALearnMoreRow(
                header: CommonStrings.Mwb.Learnmore.Landing.HeaderTitle1296,
                content: CommonStrings.Mwb.Learnmore.Landing.HeaderDescription1297,
                asset: nil,
                headerFont: UIFont.title1Font(),
                contentFont: UIFont.subheadlineFont()
            ),
            VALearnMoreRow(
                header: CommonStrings.Mwb.Learnmore.Landing.SectionTitle1255,
                content: CommonStrings.Mwb.Learnmore.Landing.SectionDescription1308,
                asset: MYHAsset.understand,
                headerFont: UIFont.title2Font(),
                contentFont: UIFont.bodyFont()
            ),
            VALearnMoreRow(
                header: CommonStrings.Mwb.Learnmore.Landing.SectionHeaderTitle1240,
                content: CommonStrings.Mwb.Learnmore.Landing.SectionDescription1267,
                asset: MYHAsset.calculate,
                headerFont: UIFont.title2Font(),
                contentFont: UIFont.bodyFont()
            )
        ]
    }
    
    
    static func buildLearnMoreRows(status: PartyAttributeFeedbackRef) -> [VALearnMoreRow] {
        switch status {
        case .PsychoWorkTogether:
            return MWBPsychologicalLearnMoreViewModel.buildLetsWorkTogetherLearnMore()
        case .PsychoGettingThere:
            return MWBPsychologicalLearnMoreViewModel.buildYoureGettingThereLearnMore()
        case .PsychoRightTrack:
            return MWBPsychologicalLearnMoreViewModel.buildYoureOnTheRightTrackLearnMore()
        case .MWBOutdated:
            return MWBPsychologicalLearnMoreViewModel.buildOutdatedLearnMore()
        default:
            return MWBPsychologicalLearnMoreViewModel.buildUnknownLearnMore()
        }
    }
    
    public func learnMoreRowAt(index: Int) -> VALearnMoreRow? {
        guard let learnMoreRow = learnMoreRows.element(at: index) else { return nil }
        return learnMoreRow
    }
    
    public init(status: PartyAttributeFeedbackRef) {
        learnMoreRows = MWBPsychologicalLearnMoreViewModel.buildLearnMoreRows(status: status)
    }
}
