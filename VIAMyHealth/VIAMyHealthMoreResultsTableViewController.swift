import Foundation
import UIKit
import VIAUIKit
import VitalityKit
import VIAUtilities

class VIAMyHealthMoreResultsTableViewController: VIATableViewController {
    
    public var myHealthSection: GMIReportedSection?
    
    override public func viewDidLoad() {
        super.viewDidLoad()

        self.title = CommonStrings.MoreResults792
        
        self.configureTable()
    }
    
    override public func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.hideBackButtonTitle()
    }

    func configureTable() {

        self.tableView.register(MyHealthFeedbackTableViewCell.nib(), forCellReuseIdentifier: MyHealthFeedbackTableViewCell.defaultReuseIdentifier)
        self.tableView.register(MyHealthSectionHeaderView.nib(), forHeaderFooterViewReuseIdentifier: MyHealthSectionHeaderView.defaultReuseIdentifier)
        
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 150
        
        self.tableView.backgroundColor = UIColor.tableViewBackground()
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.showAttributeDetails()
    }
    
    func showAttributeDetails(){
        self.performSegue(withIdentifier: "showHealthAttributeFromDetail", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showHealthAttributeFromDetail" {
            guard let cell = tableView.cellForRow(at: tableView.indexPathForSelectedRow!) as? MyHealthFeedbackTableViewCell else {
                return
            }

            guard let controller = segue.destination as? VIAMyHealthAttributeDetailsTableViewController else {
                return
            }
            
            if let attributeAndSection = cell.myHealthAttribute {
                controller.reportedHealthAttributeAndSection = (attributeAndSection.attribute, attributeAndSection.section)
            }
        }
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return validSections().count
    }
    
    func validSections() -> [GMIReportedSection]{
        guard let section = self.myHealthSection else { return [GMIReportedSection]() }
        
        var sections = [GMIReportedSection]()

        let validSections = section.sections.filter { (section) -> Bool in
            return section.healthAttributes.count > 0
        }
        
        sections.append(contentsOf: validSections)
        
        return sections
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerView: MyHealthSectionHeaderView = tableView.dequeueReusableHeaderFooterView(withIdentifier: MyHealthSectionHeaderView.defaultReuseIdentifier) as! MyHealthSectionHeaderView
        
        if self.myHealthSection != nil {
            headerView.myHealthSecton = validSections()[section]
        }

        return headerView
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if let moreResultsSection = self.myHealthSection {
            let focusSection = moreResultsSection.sections[section]
            return focusSection.healthAttributes.count
        }

        return 0
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let section = self.myHealthSection {
            
            let focusSection = section.sections[indexPath.section]

            // there is a reason for this - the cells are dynamically creating and removing subviews and when we deque it is reusing cells and sometimes those cells are not in the correct state
            // this will therefore force the use of a new cell
            let cell = MyHealthFeedbackTableViewCell.viewFromNib(owner: self)!
            
            cell.parentViewController = self
            
            let sortedAttributes = focusSection.healthAttributes.sorted(by: { (left, right) -> Bool in
                return left.sortOrder < right.sortOrder
            })
            
            let attribute = sortedAttributes[indexPath.row]
           
            cell.showTips = (focusSection.typeKey != .VAgeMoreImprovements && focusSection.typeKey != .VAgeMoreDoingWell && focusSection.typeKey != .VAgeMoreNeedToKnow)
            cell.myHealthAttribute = (attribute, section.typeKey)
            cell.selectionStyle = .none
            
            return cell
        }

        return tableView.defaultTableViewCell()
    }
}
