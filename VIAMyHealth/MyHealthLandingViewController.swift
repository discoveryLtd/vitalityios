import Foundation
import UIKit

import VIAUIKit
import VitalityKit
import VIAUtilities
import VIACommon

class MyHealthLandingViewController: VIATableViewController, PrimaryColorTintable {

    var selectedCardIndex: Int? = -1
//    var doInitialLoad: Bool = true /* Please document the purpose of this. */
    private var hasSuccessfullyLoadedAtleastOnce = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupTitle()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setupTitle()
    }

    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        self.setupTitle()
    }

    func setupTitle() {
        self.title = CommonStrings.MenuMyhealthButton7
        self.tabBarItem.title = CommonStrings.MenuMyhealthButton7
        self.navigationController?.tabBarItem.title = CommonStrings.MenuMyhealthButton7
    }

    override public func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.hideHUDFromView(view: self.view)
        self.hideBackButtonTitle()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.configureAppearance()
        self.configureTable()
        

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.showOnboardingIfRequired()
    }

    func configureAppearance() {
        navigationController?.makeNavigationBarTransparent()
    }
    
    func configureTable() {
        // Vitality Age card.
        self.tableView.register(VitalityAgeCardTableViewCell.nib(), forCellReuseIdentifier: VitalityAgeCardTableViewCell.defaultReuseIdentifier)
        // VNA Result card.
        self.tableView.register(VNAResultCardTableViewCell.nib(), forCellReuseIdentifier: VNAResultCardTableViewCell.defaultReuseIdentifier)
        // WMB Feedback card.
        self.tableView.register(MWBFeedbackCardTableViewCell.nib(), forCellReuseIdentifier: MWBFeedbackCardTableViewCell.defaultReuseIdentifier)

        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 150

        self.tableView.refreshControl = UIRefreshControl()
        self.tableView.refreshControl?.tintColor = UIColor.cellSeperator()
        self.tableView.refreshControl?.addTarget(self, action: #selector(performRefresh), for: .valueChanged)
        
        self.tableView.backgroundColor = UIColor.tableViewBackground()
    }

    func performRefresh() {
        self.loadHealthCards()
        self.tableView.refreshControl?.endRefreshing()
    }

    func loadHealthCards() {
        let helper = HeatlhAttributesHelper()

        self.showHUDOnView(view: self.view)
        helper.healthAttributes { [weak self] (error) in
            self?.hideHUDFromView(view: self?.view)
            
            guard let _ = error else {
                self?.hasSuccessfullyLoadedAtleastOnce = true
                self?.removeStatusView()
                self?.tableView.reloadData()
                self?.tableView.refreshControl?.endRefreshing()
                return
            }
            
            guard error == nil else {
                self?.handleErrorOccurred(error!, isCacheOutdated: true)
                return
            }
        }
    }
    
    func handleErrorOccurred(_ error: Error, isCacheOutdated: Bool?) {
        let backendError = error as? BackendError ?? BackendError.other
        if let outdated = isCacheOutdated, outdated == true {
            let statusView = self.handleBackendErrorWithStatusView(backendError, tryAgainAction: { [weak self] in
                self?.loadHealthCards()
            })
            configureStatusView(statusView)
        } else {
            self.handleBackendErrorWithAlert(backendError, tryAgainAction: { [weak self] in
                self?.loadHealthCards()
            })
        }
    }

    func showOnboardingIfRequired() {
        /* Let's check if there's a need to show the onboarding screen. Forget about loading the cards yet. */
        if !AppSettings.hasShownMyHealthOnboarding() {
            performSegue(withIdentifier: "myHealthOnboarding", sender: self)
        }else{
            /* On view appear, if onboarding has already shown, start loading the health cards.
             * This is also the scenario wherein we load the health cards after the user dismiss the onboarding screen.
             */
            if  shouldReloadOnViewWillAppear() || !hasSuccessfullyLoadedAtleastOnce{
                self.loadHealthCards()
                self.tableView.refreshControl?.endRefreshing()
            }
        }
    }
    
    private func shouldReloadOnViewWillAppear() -> Bool{
        return VIAApplicableFeatures.default.reloadMyHealthOnViewAppear ?? false
    }
    
    // MARK: TableRow Enums
    
//    enum CardItemRows: Int, EnumCollection {
//        case VitalityAge = 0
//        case Nutrition = 1
//        case StressorFeedback = 2
//        case PsychologicalFeedback = 3
//        case SocialFeedback = 4
//
//        func cardHeaderText() -> String? {
//            switch self {
//            case .Nutrition:
//                return VnaStrings.Vna.NutritionScore.Result.CardTitle1422
//            case .StressorFeedback:
//                return MWBStrings.Mwb.Result.CardTitle1228
//            case .PsychologicalFeedback:
//                return MWBStrings.Mwb.Result.CardTitle1260
//            case .SocialFeedback:
//                return MWBStrings.Mwb.Result.CardTitle1277
//            default:
//                return ""
//            }
//        }
//    }
    
    func showHealthCardDetails() {
        performSegue(withIdentifier: "showHealthCardDetails", sender: self)
    }
    
    func showLearnMore() {
        performSegue(withIdentifier: "showLearnMore", sender: self)
    }

    // MARK: - TableView

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedRow = indexPath.row
        let row = VIAApplicableFeatures.default.getMyHealthCards()[selectedRow]
        
        if row == MyHealthCardItem.VitalityAge {
            if let cell = tableView.cellForRow(at: indexPath) as? VitalityAgeCardTableViewCell {
                if cell.isAgeUnknown() {
                    self.showLearnMore()
                } else {
                    selectedCardIndex = selectedRow
                    self.showHealthCardDetails()
                }
            }
        } else if row == MyHealthCardItem.Nutrition {
            if let cell = tableView.cellForRow(at: indexPath) as? VNAResultCardTableViewCell {
                if cell.isVNAUnknown() {
                    self.showLearnMore()
                } else {
                    selectedCardIndex = selectedRow
                    self.showHealthCardDetails()
                }
            }
        } else if row == MyHealthCardItem.StressorFeedback
            || row == MyHealthCardItem.PsychologicalFeedback
            || row == MyHealthCardItem.SocialFeedback {
            if let cell = tableView.cellForRow(at: indexPath) as? MWBFeedbackCardTableViewCell {
                if cell.isMWBUnknown() {
                    self.showLearnMore()
                } else {
                    selectedCardIndex = selectedRow
                    self.showHealthCardDetails()
                }
            }
        }
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return VIAApplicableFeatures.default.getMyHealthCards().count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let row = VIAApplicableFeatures.default.getMyHealthCards()[indexPath.row]

        if row == MyHealthCardItem.VitalityAge {
            let cell = self.tableView.dequeueReusableCell(withIdentifier: VitalityAgeCardTableViewCell.defaultReuseIdentifier, for: indexPath) as! VitalityAgeCardTableViewCell
            let vitalityAge = HealthInformationRepository().getHealthAttributeBy(attributeType: PartyAttributeTypeRef.VitalityAge)
            cell.vitalityAgeHealthAttribute = vitalityAge
            
            return cell
        } else if row == MyHealthCardItem.Nutrition {
            let cell = self.tableView.dequeueReusableCell(withIdentifier: VNAResultCardTableViewCell.defaultReuseIdentifier, for: indexPath) as! VNAResultCardTableViewCell
            let vnaResult = HealthInformationRepository().getHealthAttributeBy(attributeType: PartyAttributeTypeRef.Nutrition)
            cell.vnaHealthAttribute = vnaResult
            cell.vnaHeaderLabel.text = row.cardHeaderText()
            
            return cell
        } else if row == MyHealthCardItem.StressorFeedback {
            let cell = self.tableView.dequeueReusableCell(withIdentifier: MWBFeedbackCardTableViewCell.defaultReuseIdentifier, for: indexPath) as! MWBFeedbackCardTableViewCell
            let stressorResult = HealthInformationRepository().getHealthAttributeBy(attributeType: PartyAttributeTypeRef.Stressor)
            cell.cardItemRow = row.rawValue
            cell.mwbHealthAttribute = stressorResult
            cell.mwbHeaderLabel.text = row.cardHeaderText()
            
            return cell
        } else if row == MyHealthCardItem.PsychologicalFeedback {
            let cell = self.tableView.dequeueReusableCell(withIdentifier: MWBFeedbackCardTableViewCell.defaultReuseIdentifier, for: indexPath) as! MWBFeedbackCardTableViewCell
            let psychologicalResult = HealthInformationRepository().getHealthAttributeBy(attributeType: PartyAttributeTypeRef.Psychological)
            cell.cardItemRow = row.rawValue
            cell.mwbHealthAttribute = psychologicalResult
            cell.mwbHeaderLabel.text = row.cardHeaderText()
            
            return cell
        } else if row == MyHealthCardItem.SocialFeedback {
            let cell = self.tableView.dequeueReusableCell(withIdentifier: MWBFeedbackCardTableViewCell.defaultReuseIdentifier, for: indexPath) as! MWBFeedbackCardTableViewCell
            let socialResult = HealthInformationRepository().getHealthAttributeBy(attributeType: PartyAttributeTypeRef.Social)
            cell.cardItemRow = row.rawValue
            cell.mwbHealthAttribute = socialResult
            cell.mwbHeaderLabel.text = row.cardHeaderText()
            
            return cell
        }
        
        return UITableViewCell()
    }
    
     // MARK: - Navigation

     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "myHealthOnboarding" {

            guard let navController = segue.destination as? UINavigationController else { return }
            guard let viewController = navController.topViewController as? VIAVitalityAgeOnboardingTableViewController else { return }

            viewController.myHealthOnboardingViewmodel = MyHealthOnboardingViewModel()
        }

        if segue.identifier == "showHealthCardDetails" {
            guard let destination = segue.destination as? VIAVitalityHealthModalTableViewController else { return }
            destination.showBarButton = false
            destination.cardIndex = selectedCardIndex
        }
        
        if segue.identifier == "showLearnMore" {
            guard let destination = segue.destination as? MyHealthLearnMoreTableViewController else { return }
            if let cardIndex = tableView.indexPathForSelectedRow?.row {
                // Set status to 'unknown' since learn more is only clickable once card is 'unknown'.
                switch cardIndex {
                case 1: // Nutrition Results.
                    destination.viewModel = VNALearnMoreViewModel(status: .Unknown)
                    destination.cardIndex = 1
                case 2: // Stressor Feedback.
                    destination.viewModel = MWBStressorLearnMoreViewModel(status: .Unknown)
                    destination.cardIndex = 2
                case 3:  // Psychological Feedback.
                    destination.viewModel = MWBPsychologicalLearnMoreViewModel(status: .Unknown)
                    destination.cardIndex = 3
                case 4:  // Social Feedback.
                    destination.viewModel = MWBSocialLearnMoreViewModel(status: .Unknown)
                    destination.cardIndex = 4
                default: // Vitality Age.
                    destination.viewModel = VitalityAgeLearnMoreViewModelImpl()
                }
            }
        }
     }

}
