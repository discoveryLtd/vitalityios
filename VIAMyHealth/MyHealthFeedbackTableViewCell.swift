import UIKit
import VIAUIKit
import VitalityKit
import VIAUtilities

public class MyHealthFeedbackTableViewCell: UITableViewCell, Nibloadable {
    
    public weak var parentViewController: VIATableViewController?
    
    @IBOutlet weak var healthAttributeName: UILabel!
    @IBOutlet weak var healthAttributeValue: UILabel!
    @IBOutlet weak var feedbackName: UILabel!
    @IBOutlet weak var tipName: UILabel!
    
    @IBOutlet weak var tipNotes: UILabel!
    @IBOutlet weak var tipBackground: UIView!
    @IBOutlet weak var tipImage: UIImageView!
    
    @IBOutlet weak var chevron: UIImageView!

    public override func awakeFromNib() {
        super.awakeFromNib()
        
        setupView()
    }
    
    func setupView(){
        self.tipBackground.layer.cornerRadius = 8
    }
    
    func setupView(myHealthAttribute: GMIReportedHealthAttribute?) {
        self.healthAttributeName.font = UIFont.bodyFont()
        self.healthAttributeValue.font = UIFont.headlineFont()
        
        if let attribute = myHealthAttribute {

            self.healthAttributeName.text = attribute.attributeTypeName
            self.chevron.isHidden = attribute.attributeTypeKey == .Unknown
            self.showValue(myHealthAttribute: attribute)
            self.showFeedback(myHealthAttribute: attribute)
            self.showFirstTip(myHealthAttribute: attribute)
        }
    }
    
    func formatValue(value: String?) -> String? {
        if let actualValue = value {
            if let doubleValue = Double(actualValue) {
                let roundedValue = Int(round(doubleValue))
                return String(roundedValue)
            }
        }
        return value
    }
    
    
    private func mostRelevantFeedback(myHealthAttribute: GMIReportedHealthAttribute) -> GMIReportedFeedback? {
        if myHealthAttribute.feedbacks.count > 0 {
            if let validFeedback = myHealthAttribute.feedbacks.first(where: { (feedback) -> Bool in return feedback.feedbackTips.count > 0 }) {
                return validFeedback
            }
            
            return myHealthAttribute.feedbacks.sorted(by: { (this, that) -> Bool in
                this.feedbackTypeKey.rawValue > that.feedbackTypeKey.rawValue
            })[0]
        }
        
        return nil
    }
    
    private func allTips(myHealthAttribute: GMIReportedHealthAttribute) -> [GMIReportedFeedbackTip] {
        var tips = [GMIReportedFeedbackTip]()
        
        myHealthAttribute.feedbacks.forEach { (feedback) in
            tips.append(contentsOf: feedback.feedbackTips)
        }
        
        return tips
    }
    
    private func showFirstTip(myHealthAttribute: GMIReportedHealthAttribute){
        let tips = self.allTips(myHealthAttribute: myHealthAttribute)

        if tips.count > 0 {
            self.showTip(reportedTip: tips[0])
        }
        else {
            self.hideTipBackground()
        }
    }
    
    private func showValue(myHealthAttribute: GMIReportedHealthAttribute) {
        if isMWBCard(myHealthAttribute: myHealthAttribute) {
            self.healthAttributeValue.text = ""
            self.healthAttributeValue.isHidden = true
        } else {
            /* Isolate displayValue for BMI (Do not round off) */
            if myHealthAttribute.attributeTypeKey == .BMI {
                self.healthAttributeValue.text = myHealthAttribute.displayValue
            } else {
                self.healthAttributeValue.text = self.formatValue(value: myHealthAttribute.displayValue)
            }
        }
    }
    
    private func showFeedback(myHealthAttribute: GMIReportedHealthAttribute) {
        self.feedbackName.text = ""
        if !isMWBCard(myHealthAttribute: myHealthAttribute) {
            if let feedback = self.mostRelevantFeedback(myHealthAttribute: myHealthAttribute), self.feedbackName != nil {
                self.feedbackName.font = UIFont.footnoteFont()
                self.feedbackName.textColor = UIColor.crimsonRed()
                
                if !isVNAInHealthyRangeSection() {
                    self.feedbackName.text = feedback.feedbackTypeName
                }
            } else {
                if self.feedbackName != nil {
                    self.feedbackName.removeFromSuperview()
                }
            }
        }
    }
    
    private func isMWBCard(myHealthAttribute: GMIReportedHealthAttribute) -> Bool {
        if myHealthAttribute.attributeTypeKey == .Stressor ||
            myHealthAttribute.attributeTypeKey == .TraumaticEvents ||
            myHealthAttribute.attributeTypeKey == .PsychoPositive ||
            myHealthAttribute.attributeTypeKey == .PsychoEnergy ||
            myHealthAttribute.attributeTypeKey == .PsycoEmotion ||
            myHealthAttribute.attributeTypeKey == .PsycoAnxiety ||
            myHealthAttribute.attributeTypeKey == .Social {
            return true
        } else {
            return false
        }
    }
    
    private func isVNAInHealthyRangeSection() -> Bool {
        if let attributeSection = myHealthAttribute {
            switch attributeSection.section {
            case .NutritionWhatWell:
                return true
            case .NutritionLookingGood:
                return true
            default:
                return false
            }
        }
        
        return false
    }

    private func hideTipBackground() {
        if self.tipBackground != nil {
            self.tipBackground.removeFromSuperview()
        }
    }
    
    private func showTip(reportedTip: GMIReportedFeedbackTip) {
        if self.showTips && self.tipBackground != nil {
            self.tipName.font = UIFont.subheadlineSemiBoldFont()
            self.tipNotes.font = UIFont.footnoteFont()

            self.tipName.textColor = UIColor.darkGrey()
            self.tipNotes.textColor = UIColor.darkGrey()
            
            self.tipImage.tintColor = UIColor.darkGrey()

            self.tipName.text = reportedTip.typeName
            self.tipNotes.text = reportedTip.note
        }
    }

    public var myHealthAttribute: (attribute: GMIReportedHealthAttribute, section: SectionRef)? {
        didSet {
            setupView(myHealthAttribute: myHealthAttribute?.attribute)
        }
    }

    public var showTips: Bool = true {
        didSet {
            if !showTips {
                self.hideTipBackground()
            }
        }
    }
    
}
