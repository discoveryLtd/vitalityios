//
//  VNAResultCardTableViewCell.swift
//  VitalityActive
//
//  Created by Val Tomol on 06/03/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import UIKit
import VIAUIKit
import VitalityKit
import VitalityKit
import VIAUtilities
import Foundation
import VIACommon

public class VNAResultCardTableViewCell: UITableViewCell, Nibloadable {
    // Properties
    @IBOutlet weak var vnaHeaderLabel: UILabel!
    @IBOutlet weak var vnaCardLabel: UILabel!
    @IBOutlet weak var vnaImage: UIImageView!
    @IBOutlet weak var backgroundContainer: UIView!
    
    @IBOutlet weak var vnaStatusImage: UIImageView!
    @IBOutlet weak var vnaStatusLabel: UILabel!
    @IBOutlet weak var vnaBlurbLabel: UILabel!
    
    @IBOutlet weak var vnaLearnMoreButton: UIImageView!
    
    public override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    public var vnaHealthAttribute: GMIReportedHealthAttribute? {
        didSet {
            setupView(vnaResult: vnaHealthAttribute)
        }
    }
    
    public override func layoutSubviews() {
        self.drawNutritionGradient()
        
        super.layoutSubviews()
    }
    
    public func isVNAUnknown() -> Bool {
        if let nutritionAttr = vnaHealthAttribute {
            return PartyAttributeFeedbackRef.nutritionStatus(nutrition: nutritionAttr) == .NutritionUnknown
        }
        return true
    }
    
    func setupView(vnaResult: GMIReportedHealthAttribute?) {
        self.selectionStyle = .none
        self.backgroundContainer.layer.cornerRadius = 8
        self.backgroundColor = UIColor.tableViewBackground()
        
        if let result = vnaResult, let resultValue = result.value {
            self.setupCardStatus(status: PartyAttributeFeedbackRef.nutritionStatus(nutrition: result), statusValue: resultValue, vnaAttribute: result)
        } else {
            self.setupCardStatus(status: .NutritionUnknown, statusValue: "", vnaAttribute: nil)
        }
        
        self.vnaHeaderLabel.font = UIFont.bodyFont()
        self.vnaHeaderLabel.textColor = UIColor.feedbackViewBackground()
    }
    
    func showUnknownTextWithString(_ text: String) {
        self.vnaCardLabel.text = text
        self.vnaCardLabel.font = UIFont.title1Font()
        self.vnaCardLabel.numberOfLines = 2
    }
    
    func setupCardStatus(status: PartyAttributeFeedbackRef, statusValue: String, vnaAttribute: GMIReportedHealthAttribute?) {
        self.setupStatusValueLabel(status: status)
        
        // TODO: Implement this once the assets are made available.
        //self.vnaImage.image = status.vnaImage()
        
        self.vnaStatusImage.image = status.vnaStatusImage()
        self.vnaImage.image = status.vnaImage()
        self.vnaStatusImage.tintColor = UIColor.feedbackViewBackground()
        
        self.vnaStatusLabel.text = {
            if let useMyHealthBackendFeedback = Bundle.main.object(forInfoDictionaryKey: "VIAUseMyHealthBackendFeedback") as? Bool, useMyHealthBackendFeedback {
                return status.vnaSummaryStatusText(vnaAttribute: vnaAttribute)
            } else {
                return status.vnaSummaryStatusText()
            }
        }()
        self.vnaStatusLabel.font = UIFont.title3Font()
        self.vnaStatusLabel.textColor = UIColor.feedbackViewBackground()
        
        self.vnaBlurbLabel.text = status.vnaBlurbSummary()
        self.vnaBlurbLabel.font = UIFont.subheadlineFont()
        self.vnaBlurbLabel.textColor = UIColor.feedbackViewBackground()
        
        self.vnaBlurbLabel.isHidden = status == .NutritionUnknown
        self.vnaLearnMoreButton.isHidden = status != .NutritionUnknown
    }
    
    func setupStatusValueLabel(status: PartyAttributeFeedbackRef) {
        if status == .NutritionUnknown {
            self.showUnknownTextWithString(CommonStrings.Vna.Card.Label1966)
        } else {
            self.showUnknownTextWithString("")
        }
        
        self.vnaCardLabel.textColor = UIColor.feedbackViewBackground()
    }
    
    func drawNutritionGradient() {
        let top = UIColor.nutritionTop()
        let bottom = UIColor.nutritionBottom()
        self.backgroundContainer.backgroundColor = UIColor(gradientStyle: .topToBottom, withFrame: frame, andColors: [top, bottom])
    }
}
