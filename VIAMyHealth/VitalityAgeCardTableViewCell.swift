import UIKit
import VIAUIKit
import VitalityKit
import VitalityKit
import VIAUtilities
import Foundation

public class VitalityAgeCardTableViewCell: UITableViewCell, Nibloadable {

    @IBOutlet weak var ageLabel: UILabel!
    @IBOutlet weak var ageStatusImage: UIImageView!
    @IBOutlet weak var ageStatusLabel: UILabel!
    @IBOutlet weak var ageBlurb: UILabel!

    @IBOutlet weak var vitalityAgeHeading: UILabel!
    @IBOutlet weak var backgroundContainer: UIView!

    @IBOutlet weak var vitalityAgeImage: UIImageView!

    @IBOutlet weak var learnMoreButton: UIImageView!

    public override func awakeFromNib() {
        super.awakeFromNib()
    }

    public var vitalityAgeHealthAttribute: GMIReportedHealthAttribute? {
        didSet {
            setupView(vitalityAge: vitalityAgeHealthAttribute)
        }
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        self.drawGradient()
    }

    public func isAgeUnknown() -> Bool {
        if let vitalityAge = vitalityAgeHealthAttribute {
            return PartyAttributeFeedbackRef.vitalityAgeStatus(vitalityAge: vitalityAge) == .Unknown
        }
        return true
    }
    
    func setupView(vitalityAge: GMIReportedHealthAttribute?) {
        self.selectionStyle = .none
        self.backgroundContainer.layer.cornerRadius = 8
        self.backgroundColor = UIColor.tableViewBackground()
        self.drawGradient()
        
        if let vitalityAge = vitalityAge, let vitalityAgeValue = vitalityAge.value {
            self.setupAgeStatus(status: PartyAttributeFeedbackRef.vitalityAgeStatus(vitalityAge: vitalityAge), vitalityAge: vitalityAgeValue, vitalityAgeAttribute: vitalityAge)
        } else {
            self.setupAgeStatus(status: .Unknown, vitalityAge: "", vitalityAgeAttribute: nil)
        }

        self.vitalityAgeHeading.text = CommonStrings.MyHealth.VitalityAgeScreenTitle613
        self.vitalityAgeHeading.font = UIFont.bodyFont()
        self.vitalityAgeHeading.textColor = UIColor.feedbackViewBackground()

        self.ageBlurb.font = UIFont.bodyFont()
        self.ageBlurb.font = UIFont.subheadlineFont()
    }

    func drawGradient() {
        let top = UIColor.tealBlue()
        let bottom = UIColor.deepSeaBlue()
        self.backgroundContainer.backgroundColor = UIColor(gradientStyle: .topToBottom, withFrame: frame, andColors: [top, bottom])
    }

    func setupAgeStatus(status: PartyAttributeFeedbackRef, vitalityAge: String, vitalityAgeAttribute: GMIReportedHealthAttribute?) {

        self.setupAgeLabel(status: status, vitalityAge: vitalityAge)

        self.vitalityAgeImage.image = status.myHealthAgeImage()

        self.ageStatusImage.image = status.summaryIndicatorImage()
        self.ageStatusImage.tintColor = UIColor.feedbackViewBackground()

        self.ageStatusLabel.text = status.summaryStatusText(vitalityAgeAttribute: vitalityAgeAttribute)
        self.ageStatusLabel.font = UIFont.title3Font()
        self.ageStatusLabel.textColor = UIColor.feedbackViewBackground()

        self.ageBlurb.text = status.blurbSummaryForAgeDifference(vitalityAgeAttribute: vitalityAgeAttribute)
        self.ageBlurb.font = UIFont.bodyFont()
        self.ageBlurb.textColor = UIColor.feedbackViewBackground()

        self.ageBlurb.isHidden = status == .Unknown
        self.learnMoreButton.isHidden = status != .Unknown
    }

    func showUnknownText(text: String){
        self.ageLabel.text = text
        self.ageLabel.font = UIFont.title1Font()
        self.ageLabel.numberOfLines = 2
    }

    func setupAgeLabel(status: PartyAttributeFeedbackRef, vitalityAge: String) {

        if status == .VAgeNotEnoughData {
            self.showUnknownText(text: CommonStrings.MyHealth.VitalityAgeVhcCompletedVhrNotCompletedNotEnoughDataTitle617)
        } else if status == .Unknown {
            self.showUnknownText(text: CommonStrings.MyHealth.VitalityAgeUnknown627)
        } else {
            if let doubleAge = Double(vitalityAge) {
                let age = Int(round(doubleAge))
                self.ageLabel.text = String(age)
                
                self.ageLabel.font = UIFont.systemFont(ofSize: 80, weight:UIFontWeightHeavy)
            }
            else {
                self.showUnknownText(text: CommonStrings.MyHealth.VitalityAgeVhcCompletedVhrNotCompletedNotEnoughDataTitle617)
            }
        }
        self.ageLabel.textColor = UIColor.feedbackViewBackground()

        if status == .VAgeOutdated {
            self.ageLabel.textColor = self.ageLabel.textColor.withAlphaComponent(0.5)
        }
    }
}
