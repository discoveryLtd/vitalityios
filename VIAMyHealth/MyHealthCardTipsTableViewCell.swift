//
//  MyHealthCardTipsTableViewCell.swift
//  VitalityActive
//
//  Created by Yuson, Alyosha M. on 08/05/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import UIKit
import VIAUIKit
import VitalityKit
import VIAUtilities
import Foundation

public class MyHealthCardTipsTableViewCell: UITableViewCell, Nibloadable {
    
    @IBOutlet weak var myHealthTipsImage: UIImageView!
    @IBOutlet weak var myHealthTipsHeader: UILabel!
    @IBOutlet weak var myHealthTipsSummary: UILabel!
    
    public override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    public func setupViewFromCard(attributeTypeRef: PartyAttributeTypeRef) {
        myHealthTipsImage.image = VIAMyHealthAsset.tipsDetail.image
        myHealthTipsHeader.font = UIFont.title3Font()
        
        if let section = HealthInformationRepository().getHealthAttributeBy(attributeType: attributeTypeRef) {
            myHealthTipsHeader.text = section.feedbacks.first?.feedbackTips.first?.typeName
            myHealthTipsSummary.text = section.feedbacks.first?.feedbackTips.first?.note
        }
    }
}
