import Foundation
import VIAUtilities
import VitalityKit

class HeatlhAttributesHelper {

    func healthAttributes(completion: @escaping (_ error: Error?) -> Void) {
        let coreRealm = DataProvider.newRealm()
        let tenantID = coreRealm.getTenantId()
        let partyID = coreRealm.getPartyId()

        Wire.MyHealth.shared().healthInformationFor(isSummary: false, tenantId: tenantID, partyId: partyID) { (response, error) in
            completion(error)
        }
    }

}
