//
//  MWBLearnMoreViewModel.swift
//  VitalityActive
//
//  Created by OJ Garde on 3/13/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import VitalityKit
import VIAUtilities

public class MWBSocialLearnMoreViewModel: VitalityAgeLearnMoreViewModel {
    let learnMoreRows : [VALearnMoreRow]
    
    public func numberOfSections() -> Int {
        return MyHealthLearnMoreSections.allValues.count
    }
    
    public func numberOfRows() -> Int {
        return learnMoreRows.count
    }
    
    static func buildUnknownLearnMore() -> [VALearnMoreRow] {
        return [
            VALearnMoreRow(
                header: CommonStrings.Mwb.Learnmore.Landing.HeaderTitle1282,
                content: CommonStrings.Mwb.Learnmore.Landing.HeaderDescription1283,
                asset: nil,
                headerFont: UIFont.title1Font(),
                contentFont: UIFont.subheadlineFont()
            ),
            VALearnMoreRow(
                header: CommonStrings.Mwb.Learnmore.Landing.SectionHeaderTitle1240,
                content: CommonStrings.Mwb.Learnmore.Landing.SectionDescription1284,
                asset: MYHAsset.calculate,
                headerFont: UIFont.title2Font(),
                contentFont: UIFont.bodyFont()
            ),
            VALearnMoreRow(
                header: CommonStrings.Mwb.Learnmore.Landing.SectionTitle1285,
                content: CommonStrings.Mwb.Learnmore.Landing.SectionDescription1286,
                asset: MYHAsset.social,
                headerFont: UIFont.title2Font(),
                contentFont: UIFont.bodyFont()
            ),
            VALearnMoreRow(
                header: CommonStrings.Mwb.OnboardingSection3Title1200,
                content: CommonStrings.Mwb.Learnmore.Landing.SectionDescription1287,
                asset: MYHAsset.understand,
                headerFont: UIFont.title2Font(),
                contentFont: UIFont.bodyFont()
            )
        ]
    }
    
    static func buildLetsWorkTogetherLearnMore() -> [VALearnMoreRow] {
        return [
            VALearnMoreRow(
                header: CommonStrings.Mwb.Learnmore.Landing.HeaderTitle1304,
                content: CommonStrings.Mwb.Learnmore.Landing.HeaderDescription1305,
                asset: nil,
                headerFont: UIFont.title1Font(),
                contentFont: UIFont.subheadlineFont()
            ),
            VALearnMoreRow(
                header: CommonStrings.Mwb.Learnmore.Landing.SectionTitle1255,
                content: CommonStrings.Mwb.Learnmore.Landing.SectionDescription1308,
                asset: MYHAsset.understand,
                headerFont: UIFont.title2Font(),
                contentFont: UIFont.bodyFont()
            ),
            VALearnMoreRow(
                header: CommonStrings.Mwb.Learnmore.Landing.SectionHeaderTitle1240,
                content: CommonStrings.Mwb.Learnmore.Landing.SectionDescription1284,
                asset: MYHAsset.calculate,
                headerFont: UIFont.title2Font(),
                contentFont: UIFont.bodyFont()
            )
        ]
    }
    
    static func buildYoureGettingThereLearnMore() -> [VALearnMoreRow] {
        return [
            VALearnMoreRow(
                header: CommonStrings.Mwb.Learnmore.Landing.HeaderTitle1306,
                content: CommonStrings.Mwb.Learnmore.Landing.HeaderDescription1307,
                asset: nil,
                headerFont: UIFont.title1Font(),
                contentFont: UIFont.subheadlineFont()
            ),
            VALearnMoreRow(
                header: CommonStrings.Mwb.Learnmore.Landing.SectionTitle1255,
                content: CommonStrings.Mwb.Learnmore.Landing.SectionDescription1308,
                asset: MYHAsset.understand,
                headerFont: UIFont.title2Font(),
                contentFont: UIFont.bodyFont()
            ),
            VALearnMoreRow(
                header: CommonStrings.Mwb.Learnmore.Landing.SectionHeaderTitle1240,
                content: CommonStrings.Mwb.Learnmore.Landing.SectionDescription1284,
                asset: MYHAsset.calculate,
                headerFont: UIFont.title2Font(),
                contentFont: UIFont.bodyFont()
            )
        ]
    }
    
    static func buildYoureOnTheRightTrackLearnMore() -> [VALearnMoreRow] {
        return [
            VALearnMoreRow(
                header: CommonStrings.Mwb.Learnmore.Landing.HeaderTitle1288,
                content: CommonStrings.Mwb.Learnmore.Landing.HeaderDescription1289,
                asset: nil,
                headerFont: UIFont.title1Font(),
                contentFont: UIFont.subheadlineFont()
            ),
            VALearnMoreRow(
                header: CommonStrings.Mwb.Learnmore.Landing.SectionTitle1255,
                content: CommonStrings.Mwb.Learnmore.Landing.SectionDescription1308,
                asset: MYHAsset.understand,
                headerFont: UIFont.title2Font(),
                contentFont: UIFont.bodyFont()
            ),
            VALearnMoreRow(
                header: CommonStrings.Mwb.Learnmore.Landing.SectionHeaderTitle1240,
                content: CommonStrings.Mwb.Learnmore.Landing.SectionDescription1284,
                asset: MYHAsset.calculate,
                headerFont: UIFont.title2Font(),
                contentFont: UIFont.bodyFont()
            )
        ]
    }
    
    static func buildOutdatedLearnMore() -> [VALearnMoreRow] {
        return [
            VALearnMoreRow(
                header: CommonStrings.Mwb.Learnmore.Landing.HeaderTitle1302,
                content: CommonStrings.Mwb.Learnmore.Landing.HeaderDescription1303,
                asset: nil,
                headerFont: UIFont.title1Font(),
                contentFont: UIFont.subheadlineFont()
            ),
            VALearnMoreRow(
                header: CommonStrings.Mwb.Learnmore.Landing.SectionTitle1255,
                content: CommonStrings.Mwb.Learnmore.Landing.SectionDescription1308,
                asset: MYHAsset.understand,
                headerFont: UIFont.title2Font(),
                contentFont: UIFont.bodyFont()
            ),
            VALearnMoreRow(
                header: CommonStrings.Mwb.Learnmore.Landing.SectionHeaderTitle1240,
                content: CommonStrings.Mwb.Learnmore.Landing.SectionDescription1284,
                asset: MYHAsset.calculate,
                headerFont: UIFont.title2Font(),
                contentFont: UIFont.bodyFont()
            )
        ]
    }
    
    
    static func buildLearnMoreRows(status: PartyAttributeFeedbackRef) -> [VALearnMoreRow] {
        switch status {
        case .SocialWorkTogether:
            return MWBSocialLearnMoreViewModel.buildLetsWorkTogetherLearnMore()
        case .SocialGettingThere:
            return MWBSocialLearnMoreViewModel.buildYoureGettingThereLearnMore()
        case .SocialRightTrack:
            return MWBSocialLearnMoreViewModel.buildYoureOnTheRightTrackLearnMore()
        case .MWBOutdated:
            return MWBSocialLearnMoreViewModel.buildOutdatedLearnMore()
        default:
            return MWBSocialLearnMoreViewModel.buildUnknownLearnMore()
        }
    }
    
    public func learnMoreRowAt(index: Int) -> VALearnMoreRow? {
        guard let learnMoreRow = learnMoreRows.element(at: index) else { return nil }
        return learnMoreRow
    }
    
    public init(status: PartyAttributeFeedbackRef) {
        learnMoreRows = MWBSocialLearnMoreViewModel.buildLearnMoreRows(status: status)
    }
}
