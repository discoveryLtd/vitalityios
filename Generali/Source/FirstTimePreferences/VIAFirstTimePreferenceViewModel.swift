//
//  VIAFirstTimePreferenceViewModel.swift
//  Generali
//
//  Created by Von Kervin R. Tuico on 14/05/2019.
//  Copyright © 2019 Glucode. All rights reserved.
//

import Foundation
import VIACore
import VIAUIKit
import VitalityKit

public class VIAFirstTimePreferenceViewModel: FirstTimePreferenceViewModel {
    
    let GENERALI_URL = URL(string: CommonStrings.Profile.Settings.ProfileEmailMessageForCommunicationPreferenceHyperlink2625)!
    var hyperlinkTextRange: NSRange!
    var detailLabel: UILabel!
    
    public override func setDetailText(detailText: String, detailLabel: UILabel, switchButton: UISwitch) {
        /** FC-26909 : Generali Germany : Change Request
         * Added a Hyperlink Text and Hide the Switch Button.
         */
        let currentEmail = DataProvider.newRealm().currentEmailAddress()?.value ?? ""
        if detailText.contains(CommonStrings.Settings.EmailCommunication985(currentEmail)) {
            self.setHyperlinkText(detailText, detailLabel)
            switchButton.isHidden = true
        } else {
            detailLabel.text = detailText
        }
    }
    
    /** FC-26909 : Generali Germany : Change Request
     * Action Event on Tapped Hyperlink.
     */
    @objc
    func setActionForHyperlink(_ sender: UITapGestureRecognizer) {
        if sender.didTapAttributedText(label: self.detailLabel, inRange: hyperlinkTextRange) {
            UIApplication.shared.open(GENERALI_URL, options: [:], completionHandler: nil)
        }
    }
    
    /** FC-26909 : Generali Germany : Change Request
     * Construct the texts with Hyperlink.
     */
    func setHyperlinkText(_ detailText: String, _ detailLabel: UILabel) {
        let subTexts = "\(detailText) \(CommonStrings.Profile.Settings.ProfileEmailMessageForCommunicationPreferenceTextlink2626)"
        let hyperlinkTextRange = (subTexts as NSString).range(of: CommonStrings.Profile.Settings.ProfileEmailMessageForCommunicationPreferenceTextlink2626)
        let attributedString = NSMutableAttributedString(string: subTexts)
        
        /* Set how links should appear: blue and underlined */
        attributedString.addAttribute(NSForegroundColorAttributeName, value: UIColor.primaryColor() , range: hyperlinkTextRange)
        attributedString.addAttribute(NSUnderlineStyleAttributeName, value: NSUnderlineStyle.styleSingle.rawValue, range: hyperlinkTextRange)
        
        /* Set the hyperlinkTextRange to be the link */
        attributedString.addAttribute(NSLinkAttributeName, value: self.GENERALI_URL, range: hyperlinkTextRange)
        detailLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(setActionForHyperlink(_:))))
        detailLabel.attributedText = attributedString
        detailLabel.isUserInteractionEnabled = true
        
        self.hyperlinkTextRange = hyperlinkTextRange
        self.detailLabel = detailLabel
    }
}
