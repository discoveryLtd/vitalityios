//
//  VIAAnnualStatusViewModel.swift
//  VitalityActive
//
//  Created by OJ Garde on 7/29/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import VIAStatus
import VitalityKit
import RealmSwift

public class VAAnnualStatusViewModel: VIAAnnualStatusViewModel {
    
    public override func potentialPointsForCategory(for index: Int, productCategories: Results<StatusProductFeatureCategory>?) -> String? {
        
        /** List of TypeKeys and its corresponding product feature category
         * Assessments : 19
         * Nutrition : 20
         * Screenings : 21
         * Get Active : 22
         */
        guard let typeKey = productCategories?[index].key else { return nil }
        guard index < productCategories?.count ?? 0 else { return nil }
        guard let potentialPoints = productCategories?[index].potentialPoints else { return nil }
        guard let pointEarnFlag = productCategories?[index].pointsEarningFlag else { return nil }
        
        let pointsCategoryLimit = productCategories?[index].pointsCategoryLimit.value ?? 0
        if pointEarnFlag == "EarnUpToPoints" {
            if pointsCategoryLimit > 0 {
                return CommonStrings.Status.PointsIndicationMessageUpTo828(String(describing: pointsCategoryLimit))
            }
            
            /** FC-24406 : Generali Germany
             * Control the displayed points and set its maximum to 4000 for Screenings
             */
            /* Filter out the Screenings using the typeKey on product feature category */
            if typeKey == 21 {
                return CommonStrings.Status.PointsIndicationMessageUpTo828(String(describing: setMaximumLimitPoints(points: potentialPoints, maximumPoints: 4000, decrementBy: 1000)))
            } else {
                return CommonStrings.Status.PointsIndicationMessageUpTo828(String(describing: potentialPoints))
            }
        } else if pointEarnFlag == "EarnUpToPointsLimits" {
            if pointsCategoryLimit > 0 {
                return CommonStrings.Status.PointsIndicationMessage829(String(describing: pointsCategoryLimit))
            }
            return CommonStrings.Status.PointsIndicationMessage829(String(describing: potentialPoints))
        } else if pointEarnFlag == "EarnPoints" {
            if pointsCategoryLimit > 0 {
                return CommonStrings.Status.PointsIndicationMessageLimit830(String(describing: pointsCategoryLimit))
            }
            return CommonStrings.Status.PointsIndicationMessageLimit830(String(describing: potentialPoints))
        }
        return String(describing: potentialPoints)
    }
    
    public override func totalPointsEarnedForCategory(for index: Int, productCategories: Results<StatusProductFeatureCategory>?) -> String? {
        
        /** List of TypeKeys and its corresponding product feature category
         * Assessments : 19
         * Nutrition : 20
         * Screenings : 21
         * Get Active : 22
         */
        guard let typeKey = productCategories?[index].key else { return nil }
        guard index < productCategories?.count ?? 0 else { return nil }
        guard let potentialPoints = productCategories?[index].potentialPoints else { return nil }
        guard let earnedPoints = productCategories?[index].pointsEarned else { return nil }
        
        if let categoryLimit = productCategories?[index].pointsCategoryLimit.value {
            if earnedPoints >= categoryLimit && categoryLimit > 0 {
                return CommonStrings.Status.PointsEarnedIndicationMessage896(String(describing: earnedPoints))
            }
        }
        
        /** FC-24406 : Generali Germany
         * Control the displayed points and set its maximum to 4000 for Screenings
         */
        
        /* Filter out the Screenings using the typeKey on product feature category */
        if typeKey == 21 {
            if earnedPoints >= 4000 {
                return CommonStrings.Status.PointsEarnedIndicationMessage896(String(describing: setMaximumLimitPoints(points: earnedPoints, maximumPoints: 4000, decrementBy: 1000)))
            }
            return nil
        } else {
            if earnedPoints >= potentialPoints {
                return CommonStrings.Status.PointsEarnedIndicationMessage896(String(describing: earnedPoints))
            }
        }
        
        return nil
    }
    
    func setMaximumLimitPoints(points: Int, maximumPoints: Int, decrementBy: Int) -> Int {
        var newPointsValue = points
        /* Check if points is greater than the maximumPoints */
        if points > maximumPoints {
            repeat {
                /* If new points value is not equal to maximumPoints, decrement the points value */
                if newPointsValue != maximumPoints {
                    newPointsValue -= decrementBy
                } else {
                    return newPointsValue
                }
            } while newPointsValue >= maximumPoints
        }
        return newPointsValue
    }
}
