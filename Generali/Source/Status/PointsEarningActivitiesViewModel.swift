//
//  PointsEarningActivitiesViewModel.swift
//  Generali
//
//  Created by Von Kervin R. Tuico on 08/03/2019.
//  Copyright © 2019 Glucode. All rights reserved.
//

import Foundation
import VIAStatus
import VitalityKit
import RealmSwift

public class VAPointsEarningActivitiesViewModel: PointsEarningActivitiesViewModel {
    
    public override func earnUpToString(for index: Int) -> String? {
        guard let pointsEarningFlag = pointsEarningActivity(at: index)?.pointsEarningFlag else {
            return nil
        }
        guard let potentialPoints = pointsEarningActivity(at: index)?.potentialPoints else {
            return nil
        }
        
        if pointsEarningFlag == "EarnUpToPoints" {
            
            /** FC-24406 : Generali Germany
             * Control the displayed points and set its maximum to 4000 for Screenings
             */
            if let tenantID = AppSettings.getAppTenant(), tenantID == .GI {
                guard let key = pointsEarningActivity(at: index)?.key else {
                    return nil
                }
                
                if key == 109 || key == 110 {
                    return CommonStrings.Status.PointsIndicationMessageUpTo828(String(describing: setMaximumLimitOfPotentialPoints(potentialPoints: potentialPoints, maximumPoints: 2000, decrementBy: 1000)))
                } else {
                    return CommonStrings.Status.PointsIndicationMessageUpTo828(String(describing: potentialPoints))
                }
            } else {
                return CommonStrings.Status.PointsIndicationMessageUpTo828(String(describing: potentialPoints))
            }
        } else if pointsEarningFlag == "EarnUpToPoinsLimit" {
            return CommonStrings.Status.PointsIndicationMessage829(String(describing: potentialPoints))
        } else if pointsEarningFlag == "EarnPoints" {
            return CommonStrings.Status.PointsIndicationMessageLimit830(String(describing: potentialPoints))
        }
        return nil
    }
    
    func setMaximumLimitOfPotentialPoints(potentialPoints: Int, maximumPoints: Int, decrementBy: Int) -> Int {
        var points = potentialPoints
        /* Check if potentialPoints is greater than the maximumPoints */
        if potentialPoints > maximumPoints {
            repeat {
                /* If points value is not equal to maximumPoints, decrement the points value */
                if points != maximumPoints {
                    points -= decrementBy
                } else {
                    return points
                }
            } while points >= maximumPoints
        }
        return points
    }
}
