import VIACommon
import VIAUIKit
import VIACore
import UIKit
import VitalityKit

extension AppDelegate: AppearanceColorDataSource {
    class func generaliColor() -> UIColor {
        return UIColor(red: 194.0 / 255.0, green: 27.0 / 255.0, blue: 23.0 / 255.0, alpha: 1.0)
    }
    
    // MARK: AppearanceDataSource
    
    public var primaryColor: UIColor {
        return AppDelegate.generaliColor()
    }
    
    public var tabBarBackgroundColor: UIColor {
        return .white
    }
    
    public var tabBarBarTintColor: UIColor {
        return .white
    }
    
    public var tabBarTintColor: UIColor? {
        return UIColor.primaryColor()
    }
    
    public var unselectedItemTintColor: UIColor? {
        return nil
    }
    
    public var getInsurerGlobalTint: UIColor? {
        return VIAAppearance.default.primaryColorFromServer
    }
    
    public func getSplashScreenGradientTop() -> UIColor{
        return UIColor(hexString: AppConfigFeature.insurerGradientColor1Hex())
    }
    
    public func getSplashScreenGradientBottom() -> UIColor{
        return UIColor(hexString: AppConfigFeature.insurerGradientColor2Hex())
    }
}

extension AppDelegate: AppearanceIconographyDataSource {
    
    public func homeLogo(for locale: Locale) -> UIImage {
        return GeneraliAsset.homeLogo.image
    }
    
    public func loginLogo(for locale: Locale) -> UIImage {
        return GeneraliAsset.homeLogo.image
    }
    
    public func splashLogo(for locale: Locale = Locale.current) -> UIImage {
        return GeneraliAsset.loginLogo.image
    }
}
