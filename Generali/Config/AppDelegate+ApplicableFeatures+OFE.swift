//
//  AppDelegate+ApplicableFeatures+OFE.swift
//  VitalityActive
//
//  Created by Val Tomol on 02/05/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import VIACommon
import VitalityKit

extension AppDelegate {
    /**
     * Usage:
     * OFEClaimPointsViewController.swift
     * OFESelectedPhotosCollectionViewController.swift
     * OFEAddWeblinkProofViewController.swift
     * OFESummaryViewController.swift
     **/
    public func includeAllProofs() -> Bool {
        return false
    }
    
    /**
     * Usage:
     * OFEHistoryViewController
     **/
    public func getOFEFormattedDate(dateString: String) -> String{
        
        let index = dateString.index(dateString.startIndex, offsetBy: 19)
        let mySubstring = dateString.substring(to: index)
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        let date = formatter.date(from: mySubstring)
        
        let displayFormatter: DateFormatter = {
            let dateFormatter = DateFormatter.dayDateShortMonthyearFormatter()
            return dateFormatter
        }()
        
        return displayFormatter.string(from: date!)

    }
    
    /**
     * Usage:
     * OFEHistoryDetailsViewController
     **/
    public func configureOFEHistoryDetailsRow() -> Bool{
        return false
    }

    /**
     * Usage:
     * OFEEventTypeViewController
     * OFEClaimPointsViewController
     **/
    public func withDefaultStringValueForOFEEventDate() -> Bool{
        return false
    }
}
