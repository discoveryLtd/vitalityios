// Generated using SwiftGen, by O.Halligon — https://github.com/SwiftGen/SwiftGen

#if os(OSX)
import AppKit.NSImage
public typealias GeneraliColor = NSColor
public typealias GeneraliImage = NSImage
#elseif os(iOS) || os(tvOS) || os(watchOS)
import UIKit.UIImage
public typealias GeneraliColor = UIColor
public typealias GeneraliImage = UIImage
#endif

// swiftlint:disable file_length

public typealias GeneraliAssetType = GeneraliImageAsset

public struct GeneraliImageAsset: Equatable {
	fileprivate var name: String

	public var templateImage: GeneraliImage {
		let bundle = Bundle(for: BundleToken.self)
		#if os(iOS) || os(tvOS)
		let image = GeneraliImage(named: name, in: bundle, compatibleWith: nil)?.withRenderingMode(.alwaysTemplate)
		#elseif os(OSX)
		let image = bundle.image(forResource: name).withRenderingMode(.alwaysTemplate)
		#elseif os(watchOS)
		let image = GeneraliImage(named: name).withRenderingMode(.alwaysTemplate)
		#endif
		guard let result = image else { fatalError("Unable to load template image named \(name).") }
		return result
	}

	public var image: GeneraliImage {
		let bundle = Bundle(for: BundleToken.self)
		#if os(iOS) || os(tvOS)
		let image = GeneraliImage(named: name, in: bundle, compatibleWith: nil)
		#elseif os(OSX)
		let image = bundle.image(forResource: name)
		#elseif os(watchOS)
		let image = GeneraliImage(named: name)
		#endif
		guard let result = image else { fatalError("Unable to load image named \(name).") }
		return result
	}

	public static func ==(lhs: GeneraliImageAsset, rhs: GeneraliImageAsset) -> Bool {
        return lhs.image == rhs.image
    }
}

public struct GeneraliColorAsset {
fileprivate var name: String

#if swift(>=3.2)
@available(iOS 11.0, tvOS 11.0, watchOS 4.0, OSX 10.13, *)
var color: GeneraliColor {
return GeneraliColor(asset: self)
}
#endif
}

// swiftlint:disable identifier_name line_length nesting type_body_length type_name
public enum GeneraliAsset {
  public static let loginLogo = GeneraliImageAsset(name: "loginLogo")
  public static let homeLogo = GeneraliImageAsset(name: "homeLogo")

  // swiftlint:disable trailing_comma
  public static let allColors: [GeneraliColorAsset] = [
  ]
  public static let allImages: [GeneraliImageAsset] = [
    loginLogo,
    homeLogo,
  ]
  // swiftlint:enable trailing_comma
  @available(*, deprecated, renamed: "allImages")
  public static let allValues: [GeneraliAssetType] = allImages
}
// swiftlint:enable identifier_name line_length nesting type_body_length type_name

extension GeneraliImage {
@available(iOS 1.0, tvOS 1.0, watchOS 1.0, *)
@available(OSX, deprecated,
message: "This initializer is unsafe on macOS, please use the GeneraliImageAsset.image property")
convenience init!(asset: GeneraliAssetType) {
#if os(iOS) || os(tvOS)
let bundle = Bundle(for: BundleToken.self)
self.init(named: asset.name, in: bundle, compatibleWith: nil)
#elseif os(OSX) || os(watchOS)
self.init(named: asset.name)
#endif
}
}

extension GeneraliColor {
#if swift(>=3.2)
@available(iOS 11.0, tvOS 11.0, watchOS 4.0, OSX 10.13, *)
convenience init!(asset: GeneraliColorAsset) {
let bundle = Bundle(for: BundleToken.self)
#if os(iOS) || os(tvOS)
self.init(named: asset.name, in: bundle, compatibleWith: nil)
#elseif os(OSX)
self.init(named: asset.name, bundle: bundle)
#elseif os(watchOS)
self.init(named: asset.name)
#endif
}
#endif
}

private final class BundleToken {}
