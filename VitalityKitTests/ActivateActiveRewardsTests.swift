//
//  ActivateActiveRewardsTests.swift
//  VitalityActive
//
//  Created by Simon Stewart on 7/19/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation

import XCTest
import VitalityKit
import Alamofire
import VIAUtilities

class ActivateActiveRewardsTests: XCTestCase {
    var tenantId = 2
    var partyId = 100001
    
    var expectation: XCTestExpectation?
    
    override func setUp() {
        
        super.setUp()
        
        expectation = makeExpectation(#function)
    }
    
    func test1() {
        _ = makeExpectation(#function)
        
        let request = ActivateActiveRewardsRequest(effectiveFrom: createDate(value: "2017/08/20"), effectiveTo: createDate(value: "2017/08/25"))

        Wire.Rewards.activate(tenantId: tenantId, partyId: partyId, request: request) { (error) in
            guard error == nil else {
                XCTFail("failed")
                return (self.expectation?.fulfill())!
            }
            print("logged in")
            (self.expectation?.fulfill())!
        }
        
        // Wait for the async request to complete
        waitForExpectations(timeout: expectationTimeout, handler: nil)
    }

}
