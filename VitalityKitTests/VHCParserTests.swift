import XCTest
import VIAUtilities
import RealmSwift
@testable import VitalityKit

class VHCParserTests: XCTestCase {
    var response = GetVHCPotentialPointsResponse(eventType: nil, warnings: nil)
    var vitalityProductFeatureModel = VitalityProductFeature()
    
    let realm = DataProvider.newVHCRealm()
    
    override func setUp() {
        super.setUp()

        //Realm.Configuration.defaultConfiguration.inMemoryIdentifier = self.name
        setupSampleResponseObject()
        setupProductFeature()

    }

    override func tearDown() {
        super.tearDown()
        
        try! realm.write {
            realm.deleteAll()
        }
    }

    func testParseEvent() {
        
        VHCEvent.persistGetPotentialPoints(response: response)
        if let object = realm.objects(VHCEvent.self).first {
            XCTAssertNotNil(object)
            XCTAssertEqual(1234, object.eventId)
            XCTAssertNotNil(object.eventDateTime)
            XCTAssertNotNil(object.eventSource)
            XCTAssertEqual(1, object.eventMetaData.count)
            XCTAssertEqual(1, object.healthAttributeReadings.count)
        } else {
            XCTAssertTrue(false)
        }
    }

    func testParseEventSource() {
        VHCEvent.persistGetPotentialPoints(response: response)
        if let object = realm.objects(VHCEventSource.self).first {
            XCTAssertNotNil(object)
            XCTAssertEqual("Vitality Device Platform", object.eventSourceName)
            XCTAssertEqual(" ", object.note)
            XCTAssertEqual(1, object.type.rawValue)
        } else {
            XCTAssertTrue(false)
        }
    }

    func testParseEventMetaData() {
        VHCEvent.persistGetPotentialPoints(response: response)
        if let object = realm.objects(VHCEventMetaData.self).first {
            XCTAssertNotNil(object)
            XCTAssertEqual("KG", object.code)
            XCTAssertEqual("Kilogram", object.name)
            XCTAssertEqual(" ", object.note)
            XCTAssertEqual("100102", object.unitOfMeasure)
            XCTAssertEqual(64, object.reading.value)
            XCTAssertEqual(106, object.key)

        } else {
            XCTAssertTrue(false)
        }
    }

    func testParseHealthAttributeFeedback() {
        VHCEvent.persistGetPotentialPoints(response: response)
        if let object = realm.objects(VHCHealthAttributeFeedback.self).first {
            XCTAssertNotNil(object)
            XCTAssertEqual("Observation", object.feedbackCategoryName)
            XCTAssertEqual(1, object.category.rawValue)
            XCTAssertEqual("Healthy Range", object.feedbackTypeName)
            XCTAssertEqual(1, object.type.rawValue)

        } else {
            XCTAssertTrue(false)
        }
    }

    func testParseHealthAttributeReading() {
        VHCEvent.persistGetPotentialPoints(response: response)
        if let object = realm.objects(VHCHealthAttributeReading.self).first {
            XCTAssertNotNil(object)
            XCTAssertEqual(11, object.healthAttributeTypeKey)
            XCTAssertEqual("Weight", object.healthAttributeTypeCode)
            XCTAssertEqual("The weight measurement of the member", object.healthAttributeTypeName)
            XCTAssertEqual("100102", object.unitOfMeasure)
            XCTAssertEqual(64, object.value.value)
            XCTAssertEqual(1, object.healthAttributeFeedbacks.count)
            XCTAssertNotNil(object.measuredOn)
        } else {
            XCTAssertTrue(false)
        }
    }

    func testParseHealthAttribute() {
        VHCEvent.persistGetPotentialPoints(response: response)
        if let object = realm.objects(VHCHealthAttribute.self).first {
            XCTAssertNotNil(object)
            XCTAssertEqual(11, object.type.rawValue)
            XCTAssertEqual("The weight measurement of the member", object.typeName)
            XCTAssertEqual(2, object.validValues.count)

        } else {
            XCTAssertTrue(false)
        }
    }

    func testParseHealthAttributeTypeValidValues() {
        VHCEvent.persistGetPotentialPoints(response: response)
        let object = realm.objects(VHCHealthAttributeValidValues.self)
        var count = 0
        for validValue in object {
            if count == 0 {
                XCTAssertNotNil(validValue)
                XCTAssertEqual("Number Range", validValue.typeTypeName)
                XCTAssertEqual(1, validValue.lowerLimit.value)
                XCTAssertEqual(501, validValue.upperLimit.value)
                //XCTAssertEqual(500, validValue.maxValue.value)
                //XCTAssertEqual(2, validValue.minValue.value)
                XCTAssertEqual(100102, validValue.unitOfMeasureType.rawValue)
                count += 1
            } else {
                XCTAssertNotNil(validValue)
                XCTAssertEqual("Number Range", validValue.typeTypeName)
                XCTAssertEqual(2, validValue.lowerLimit.value)
                XCTAssertEqual(1103, validValue.upperLimit.value)
                //XCTAssertEqual(1102.31, validValue.maxValue.value)
                //XCTAssertEqual(4.41, validValue.minValue.value)
                XCTAssertEqual(100109, validValue.unitOfMeasureType.rawValue)
            }
        }
    }

    func testParsePotentialPointsEventType() {
        VHCEvent.persistGetPotentialPoints(response: response)
        if let object = realm.objects(VHCEventType.self).first {
            XCTAssertNotNil(object)
            XCTAssertEqual(88, object.type.rawValue)
            XCTAssertEqual(100, object.totalEarnedPoints)
            XCTAssertEqual(200, object.totalPotentialPoints)
            XCTAssertEqual(1, object.events.count)
            XCTAssertEqual(1, object.healthAttributes.count)

        } else {
            XCTAssertTrue(false)
        }
    }

    func testVHCHealthAttributeGroupCreation() {
        let coreRealm = DataProvider.newRealm()
        try! coreRealm.write {
            coreRealm.add(vitalityProductFeatureModel)
        }
        VHCEvent.persistGetPotentialPoints(response: response)
        if let object = realm.objects(VHCHealthAttributeGroup.self).first {
            XCTAssertNotNil(object)
            //TODO:- Group name comes from resource file, find how to use resource file in test runs
            XCTAssertEqual(7, object.type.rawValue)
            XCTAssertEqual(1, object.healthAttributes.count)
        } else {
            XCTAssertTrue(false)
        }
    }

    func setupSampleResponseObject() {
        
        let eventMetaData = GPPEEventMetaDataType.init(code: "KG", key: 106, name: "Kilogram", note: " ", unitOfMeasure: "100102", value: "64")

        let eventSource = GPPEEventSource.init(eventSourceCode: "VDP", eventSourceKey: 1, eventSourceName: "Vitality Device Platform", note: " ")
        
        let healthAttributeFeedback = GPPEHealthAttributeFeedbacks.init(feedbackTypeCode: "HealthyRange", feedbackTypeKey: 1, feedbackTypeName: "Healthy Range", feedbackTypeTypeCode: "Observation", feedbackTypeTypeKey: 1, feedbackTypeTypeName: "Observation")

        let healthAttributeReadings = GPPEHealthAttributeReadings.init(healthAttributeFeedbacks: [healthAttributeFeedback],
                                                                       healthAttributeTypeCode: "Weight", healthAttributeTypeKey: 11, healthAttributeTypeName: "The weight measurement of the member", measuredOn: "2017-03-03", unitOfMeasure: "100102",
                                                                        value: "64")
        
        let event = GPPEEvent.init(eventDateTime: "2017-02-01T10:15:30+01:00[Africa/Johannesburg]", eventId: 1234, eventMetaDataType: [eventMetaData],
                                   eventSource: eventSource, healthAttributeReadings: [healthAttributeReadings], pointsEntries: nil)
        
        let validValue1 = GPPEHealthAttributeTypeValidValues.init(lowerLimit: "1",maxValue: "500", minValue: "2", typeTypeCode: "NumberRange", typeTypeKey: 1,
                                                                  typeTypeName: "Number Range", unitofMeasure: "100102", upperLimit: "501", validValuesList: nil)
        let validvalue2 = GPPEHealthAttributeTypeValidValues.init(lowerLimit: "2", maxValue: "1102.31", minValue: "4.41", typeTypeCode: "NumberRange", typeTypeKey: 1,
                                                                  typeTypeName: "Number Range", unitofMeasure: "100109", upperLimit: "1103", validValuesList: nil)

        let heathAttribute = GPPEHealthAttribute.init(healthAttributeTypeValidValueses: [validValue1, validvalue2],
                                                      typeCode: "Weight", typeName: "The weight measurement of the member",
                                                      typekey: 11)
        
        let eventTypes = GPPEEventType.init(categoryCode: "Assessment", categoryKey: 1, categoryName: "Assessment", event: [event],healthAttribute: [heathAttribute], reasonCode: "", reasonKey: 1, reasonName: "", totalEarnedPoints: 100, totalPotentialPoints: 200, typeCode: "WeightCaptured", typeKey: 88, typeName: "Weight Captured")

        response = GetVHCPotentialPointsResponse.init(eventType: [eventTypes], warnings: nil)
    }
    
    func setupProductFeature() {
        vitalityProductFeatureModel = VitalityProductFeature()
        vitalityProductFeatureModel.effectiveFrom = Date()
        vitalityProductFeatureModel.effectiveTo = Date()
        vitalityProductFeatureModel.typeName = "Weight"
        vitalityProductFeatureModel.typeCode = "Weight"
        vitalityProductFeatureModel.type = ProductFeatureRef.Weight
        vitalityProductFeatureModel.productFeatureType = ProductFeatureTypeRef.VHCBMI
    }
}
