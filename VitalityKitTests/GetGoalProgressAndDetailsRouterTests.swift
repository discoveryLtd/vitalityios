//
//  GetGoalProgressAndDetailsRouterTests.swift
//  VitalityActive
//
//  Created by Simon Stewart on 8/2/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import XCTest
import VitalityKit
import RealmSwift

class GetGoalProgressAndDetailsRouterTests: XCTestCase {
    let realmCore = DataProvider.newRealm()
    let dateFrom = "2017/08/01"
    let dateTo = "2017/08/11"
    let goalKeys = [1]
    let goalStatusTypeKeys = [2]
    
    var expectation: XCTestExpectation?
    var tenantId: Int = 2
    var partyId: Int = 15282952
    
    override func setUp() {
        super.setUp()
        
        expectation = makeExpectation(#function)
        tenantId = realmCore.getTenantId()
        partyId = realmCore.getPartyId()
    }
    
    func testGetGoalProgressAndDetails() {
        login { (error) in
            let request = GetGoalProgressAndDetailsParameters(effectiveDateFrom: self.createDate(value: self.dateFrom), effectiveDateTo: self.createDate(value: self.dateTo), goalKeys: self.goalKeys, goalStatusTypeKeys: self.goalStatusTypeKeys, partyId: self.partyId)
            
            Wire.Events.getGoalProgressAndDetails(tenantId: self.tenantId, request: request) { (error, response) in
                guard error == nil else {
                    XCTFail("failed")
                    return (self.expectation?.fulfill())!
                }
                print("logged in")
                (self.expectation?.fulfill())!
            }
        }
        
        // Wait for the async request to complete
        waitForExpectations(timeout: expectationTimeout, handler: nil)
    }
    
    func testGetGDCGoalProgressAndDetails() {
        login { (error) in
            let request = GetGoalProgressAndDetailsParameters(effectiveDateFrom: self.createDate(value: self.dateFrom), effectiveDateTo: self.createDate(value: self.dateTo), goalKeys: self.goalKeys, goalStatusTypeKeys: self.goalStatusTypeKeys, partyId: self.partyId)
            
            Wire.Events.getGDCGoalProgressAndDetails(tenantId: self.tenantId, request: request) { (error, response) in
                guard error == nil else {
                    XCTFail("failed")
                    return (self.expectation?.fulfill())!
                }
                print("logged in")
                (self.expectation?.fulfill())!
            }
        }
        
        // Wait for the async request to complete
        waitForExpectations(timeout: expectationTimeout, handler: nil)
    }
}
