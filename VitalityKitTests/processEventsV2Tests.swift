//
//  processEventsV2Tests.swift
//  VitalityActive
//
//  Created by Simon Stewart on 12/1/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation

import XCTest
import VitalityKit
import Alamofire
import VIAUtilities

class ProcessEventsV2Tests: XCTestCase {
    
    var expectation: XCTestExpectation?
    
    override func setUp() {
        
        super.setUp()
        
        expectation = makeExpectation(#function)
    }
    
    func testBasic() {
        
        login { (error) in
            
            let tenantId = 1
            
            
            let events = [ProcessEventsV2PartyReferenceEvent]()
            
            Wire.Events.processEventsV2(tenantId: tenantId, events: events, completion: { (response, error) in
                guard error == nil else {
                    XCTFail("unexpected error of type: \(String(describing: error))")
                    return (self.expectation?.fulfill())!
                }
                
                (self.expectation?.fulfill())!
            })
        }
        
        waitForExpectations(timeout: expectationTimeout, handler: nil)
        
    }
}

