//
//  RegisterTests.swift
//  VitalityActive
//
//  Created by Simon Stewart on 2/6/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import XCTest
import Alamofire
import VitalityKit

class RegisterTests: XCTestCase {
    let vitalityMembershipId = 100002
    
    //Use FitNesse Test Data
    let goodRegistrationCode = "2001882260"
    let goodEmail = "test--Bruce_McBarnard@mailinator.com"
    let goodPassword = "TestPass123"
    
    //Use FitNesse Test Data
    let goodRegistrationCodeForWithDOB = "2500496738"
    let goodEmailForWithDOB = "test--Lionel_McAlarcon@mailinator.com"
    let goodPasswordForWithDOB = "TestPass123"
    let goodDOB = "1976-07-13"

    let badRegistrationCode = "-"
    let badEmail = "test@glucode"
    let badPassword = ""
}

extension RegisterTests {
    override func setUp() {
//        #if !WIREMOCK
//            XCTFail("Don't run unit tests against LIVE")
//        #endif
    }
    func testDelete() {
        let expect = makeExpectation(#function)
        // TODO: still not working.  May be a Wiremock config issue
        Wire.Member.delete(userId: goodEmail) { (error) in
            guard error == nil else {
                XCTFail("unexpected error of type: \(String(describing: error))")
                return expect.fulfill()
            }

            return expect.fulfill()

        }

        waitForExpectations(timeout: expectationTimeout) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
        }
    }

    func testWithCorrectParameters() {
        let expect = makeExpectation(#function)

        Wire.Member.register(email: goodEmail, password: goodPassword, registrationCode: goodRegistrationCode, completion: { error in
            guard error == nil else {
                XCTFail("unexpected error of type: \(String(describing: error))")
                return expect.fulfill()
            }

            return expect.fulfill()
        })

        waitForExpectations(timeout: expectationTimeout) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
        }
    }
    
    func testWithDOBCorrectParameters() {
        let expect = makeExpectation(#function)
        
        Wire.Member.registerWithDOB(email: goodEmailForWithDOB, password: goodPasswordForWithDOB, registrationCode: goodRegistrationCodeForWithDOB, bornOn: goodDOB) { (error) in
            guard error == nil else {
                XCTFail("unexpected error of type: \(String(describing: error))")
                return expect.fulfill()
            }
            
            return expect.fulfill()
        }
        
        waitForExpectations(timeout: expectationTimeout) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
        }
    }
    
    func testWithBadEmail() {
        let expect = makeExpectation(#function)

        Wire.Member.register(email: "something@doesntexist.com", password: goodPassword, registrationCode: goodRegistrationCode, completion: { error in

            guard error != nil else { // expected error
                XCTFail("unexpected error of type: \(String(describing: error))")
                return expect.fulfill()
            }

            return expect.fulfill()
        })

        waitForExpectations(timeout: expectationTimeout) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
        }
    }
}
