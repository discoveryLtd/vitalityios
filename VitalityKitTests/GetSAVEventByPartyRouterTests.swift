//
//  GetSAVEventByPartyRouterTests.swift
//  VitalityKitTests
//
//  Created by ted.philip.l.lat on 21/09/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import XCTest
import VitalityKit
import Alamofire
import VIAUtilities

class GetSAVEventByPartyRouterTests: XCTestCase {
    var partyId = 0
    var tenantId = 0
    var expectation: XCTestExpectation?
    var credentials = DataProvider.newRealm()
    
    override func setUp() {
        super.setUp()
        
        expectation = makeExpectation(#function)
    }
    
    func testGetSAVEventByParty() {
        
        login { (error) in
            
            self.partyId = self.credentials.getPartyId()
            self.tenantId = self.credentials.getTenantId()
            
            var eventTypes = [EventTypeRef]()
            eventTypes.append(.DocumentUploadsSV)
            
            guard let effectiveFromDate = DataProvider.currentMembershipPeriodStartDate() else {
                return
            }
            
            guard let effectiveToDate = DataProvider.currentMembershipPeriodEndDate() else {
                return
            }
            
            let parameter = GetSAVEventByPartyParameter(partyId: self.partyId, effectiveFrom: effectiveFromDate, effectiveTo: effectiveToDate, eventTypesTypeKeys: eventTypes)
            
            Wire.Events.getEventByParty(tenantId: self.tenantId, request: parameter,
                                        completion: { error, response in
                                            
                                            guard error == nil else {
                                                XCTFail("unexpected error of type: \(String(describing: error))")
                                                return (self.expectation?.fulfill())!
                                            }
                                            
                                            (self.expectation?.fulfill())!
            })
        }
        
        waitForExpectations(timeout: expectationTimeout) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
        }
    }
   
}
