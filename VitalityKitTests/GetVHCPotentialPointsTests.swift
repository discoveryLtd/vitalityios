//
//  GetPotentialPointsTests.swift
//  VitalityActive
//
//  Created by Simon Stewart on 3/8/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import XCTest
import VitalityKit
import VIAUtilities

class GetVHCPotentialPointsTests: XCTestCase {
    let goodTypeKey = EventTypeRef.WeightCaptured
    
    let badPartyId = 123123
    let badTenantId = 25
    
    var expectation: XCTestExpectation?
    
    override func setUp() {
        
        super.setUp()
        
        expectation = makeExpectation(#function)
    }
    
    func testWithGoodParams() {

        login { (error) in

        Wire.Events.getVHCPotentialPointsEventsAndHealthyRanges(tenantId: self._goodTenantId, partyId: self._goodPartyId, eventTypes: [self.goodTypeKey]) { (error) in
            guard error == nil else {
                XCTFail("unexpected error of type: \(String(describing: error))")
                return (self.expectation?.fulfill())!
            }

            (self.expectation?.fulfill())!
        }
        }

        waitForExpectations(timeout: expectationTimeout) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
        }
    }

    func testWithBadParams() {
        
        login { (error) in
            
            Wire.Events.getVHCPotentialPointsEventsAndHealthyRanges(tenantId: self.badTenantId, partyId: self.badPartyId, eventTypes: [self.goodTypeKey]) { (error) in
                // Expected to fail
                guard error != nil else {
                    XCTFail("unexpected error of type: \(String(describing: error))")
                    return (self.expectation?.fulfill())!
                }
                
                (self.expectation?.fulfill())!
            }
        }
        
        waitForExpectations(timeout: expectationTimeout) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
        }
    }
    
}
