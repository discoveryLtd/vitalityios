//
//  GetPartyByIdTests.swift
//  VitalityActive
//
//  Created by Simon Stewart on 11/29/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation

import XCTest
import Alamofire

import VitalityKit

class GetPartyByIdTests: XCTestCase {
    #if WIREMOCK
    let goodPartyId: Int = 1
    let goodTenantId = 1
    #else
    let goodPartyId: Int = 1
    let goodTenantId = 1
    #endif
    
    var expectation: XCTestExpectation?
    
    override func setUp() {
        
        super.setUp()
        
        expectation = makeExpectation(#function)
    }
    
    func testWithGoodParameters() {
        
        login { (error) in
            
            Wire.Rewards.getPartyById(tenantId: self._goodTenantId, partyId: self._goodPartyId, completion: { error, response in
                guard error == nil else {
                    XCTFail("unexpected error of type: \(String(describing: error))")
                    return (self.expectation?.fulfill())!
                }
                
                (self.expectation?.fulfill())!
            })
        }
        
        waitForExpectations(timeout: expectationTimeout) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
        }
    }
}
