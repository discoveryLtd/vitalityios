//
//  CreateUserInstructionTests.swift
//  VitalityActive
//
//  Created by Simon Stewart on 2/14/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

import XCTest
import Alamofire

import VitalityKit

class CaptureLoginEventsTests: XCTestCase {
    let goodPartyId = 100001
    let goodTenantId = 2
    let goodLoginSuccess = false
    let goodUserInstructionResponseId = "0"
    let goodUserInstructionResponseType = "0"
    
    let badPartyId = 200001
    
    var expectation: XCTestExpectation?
    
    override func setUp() {
        
        super.setUp()
        
        expectation = makeExpectation(#function)
    }
    
    func testWithGoodParameters() {
        login { (error) in
            
            Wire.Events.captureLoginEvents(tenantId: self.goodTenantId, partyId: self.goodPartyId, loginSuccess: self.goodLoginSuccess, userInstructionResponseId: self.goodUserInstructionResponseId, userInstructionResponseType: self.goodUserInstructionResponseType) { (error) in
                guard error == nil else {
                    XCTFail("unexpected error of type: \(String(describing: error))")
                    return (self.expectation?.fulfill())!
                }
                
                (self.expectation?.fulfill())!
            }
        }
        
        waitForExpectations(timeout: expectationTimeout) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
        }
    }
    
    func testWithBadParameters() {
        login { (error) in
            
            Wire.Events.captureLoginEvents(tenantId: self.goodTenantId, partyId: self.badPartyId, loginSuccess: self.goodLoginSuccess, userInstructionResponseId: self.goodUserInstructionResponseId, userInstructionResponseType: self.goodUserInstructionResponseType) { (error) in
                guard error != nil else {
                    XCTFail("unexpected error of type: \(String(describing: error))")
                    return (self.expectation?.fulfill())!
                }
                
                (self.expectation?.fulfill())!
            }
        }
        
        waitForExpectations(timeout: expectationTimeout) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
        }
    }
}
