//
//  CaptureAssessmentTests.swift
//  VitalityActive
//
//  Created by Simon Stewart on 6/1/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import XCTest
import VitalityKit

class CaptureAssessmentTests: XCTestCase {
    
    var expectation: XCTestExpectation?
    
    override func setUp() {
        
        super.setUp()
        
        expectation = makeExpectation(#function)
    }
    
    func test_General_Health() {
        
        login { (error) in
            
            var selectedValues = [SelectedValues]()
            //selectedValues.append(SelectedValues(value: "Good", valueType: 9, fromValue: "string", toValue: "string", unitOfMeasure: "meter"))
            selectedValues.append(SelectedValues(value: "Good", valueType: 9, fromValue: nil, toValue: nil, unitOfMeasure: nil))
            var assessmentAnswers = [AssessmentAnswer]()
            assessmentAnswers.append(AssessmentAnswer(answerStatusTypeKey: 3, questionTypeKey: 5, selectedValues: selectedValues))
            
            var assessment = [Assessment]()
            /*
             When submitting we set typeKey which is the typeKey of theQuestionaire
             And a statusTypeKey which is hardcoded as 1 and means "new submission"
             (Info from werner)
             */
            
            assessment.append(Assessment(assessmentAnswer: assessmentAnswers, statusTypeKey: 1, typeKey: 1))
            
            let request = CaptureAssessmentRequest(assessment: assessment, partyId: 1874150, questionnaireSetTypeKey: 1)
            Wire.Events.captureAssessment(tenantId: 2, request: request) { (response, error) in
                guard error == nil else {
                    XCTFail("unexpected error of type: \(String(describing: error))")
                    return (self.expectation?.fulfill())!
                }
                (self.expectation?.fulfill())!
            }
        }
        
        waitForExpectations(timeout: expectationTimeout) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
        }
    }
    func test_Social_Habits() {
        
        login { (error) in
            
            var selectedValues = [SelectedValues]()
            //selectedValues.append(SelectedValues(value: "Good", valueType: 9, fromValue: "string", toValue: "string", unitOfMeasure: "meter"))
            selectedValues.append(SelectedValues(value: "Yes, smoke or use tobacco products", valueType: 9, fromValue: nil, toValue: nil, unitOfMeasure: nil))
            var assessmentAnswers = [AssessmentAnswer]()
            assessmentAnswers.append(AssessmentAnswer(answerStatusTypeKey: 3, questionTypeKey: 83, selectedValues: selectedValues))
            
            var assessment = [Assessment]()
            assessment.append(Assessment(assessmentAnswer: assessmentAnswers, statusTypeKey: 1, typeKey: 2))
            
            let request = CaptureAssessmentRequest(assessment: assessment, partyId: 1874150, questionnaireSetTypeKey: 1)
            
            Wire.Events.captureAssessment(tenantId: 2, request: request) { (response, error) in
                guard error == nil else {
                    XCTFail("unexpected error of type: \(String(describing: error))")
                    return (self.expectation?.fulfill())!
                }
                (self.expectation?.fulfill())!
            }
        }
        
        waitForExpectations(timeout: expectationTimeout) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
        }
    }
}
