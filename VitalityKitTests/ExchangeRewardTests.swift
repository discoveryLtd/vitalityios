//
//  ExchangeRewardTests.swift
//  VitalityKitTests
//
//  Created by ritchie.r.rolloque on 24/09/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import XCTest
import VitalityKit
import Foundation
import VIAUtilities

class ExchangeRewardTests: XCTestCase {
    
    let unclaimedRewardId = 7922688
    let linkId = 0
    let type = ExchangeTypeRef.Selected
    let badTenantId = 123123
    let badUnclaimedRewardId = 123123
    
    var expectation: XCTestExpectation?

    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        expectation = makeExpectation(#function)
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExchangeRewardGoodParameters() {
        login { (error) in
            Wire.Rewards.exchangeReward(tenantId: self._goodTenantId,
                                        awardedRewardId: self.unclaimedRewardId,
                                        rewardValueLinkId: self.linkId,
                                        type: self.type) { (error, newRewardId, voucherNumber) in
                                            guard error == nil else {
                                                XCTFail("failed")
                                                return (self.expectation?.fulfill())!
                                            }
                                            (self.expectation?.fulfill())!
            }
        }
        
        // Wait for the async request to complete
        waitForExpectations(timeout: expectationTimeout, handler: nil)
    }
    
    func testExchangeRewardBadParameters() {
        login { (error) in
            Wire.Rewards.exchangeReward(tenantId: self.badTenantId,
                                        awardedRewardId: self.badUnclaimedRewardId,
                                        rewardValueLinkId: self.linkId,
                                        type: self.type) { (error, newRewardId, voucherNumber) in
                                            guard error != nil else {
                                                XCTFail("failed")
                                                return (self.expectation?.fulfill())!
                                            }
                                            (self.expectation?.fulfill())!
            }
        }
        
        // Wait for the async request to complete
        waitForExpectations(timeout: expectationTimeout, handler: nil)
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
