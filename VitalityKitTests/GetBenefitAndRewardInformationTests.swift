//
//  GetBenefitAndRewardInformationTests.swift
//  VitalityActive
//
//  Created by Simon Stewart on 10/11/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation

import XCTest
import Alamofire

import VitalityKit

class GetBenefitAndRewardInformationTests: XCTestCase {
    #if WIREMOCK
    let goodPartyId: Int = 2
    let goodTenantId = 2
    let goodVitalityMembershipId: Int = 2
    #else
    let goodPartyId: Int = 26557219
    let goodTenantId = 25
    let goodVitalityMembershipId: Int = 59049883
    #endif
    
    var expectation: XCTestExpectation?
    
    override func setUp() {
        
        super.setUp()
        
        expectation = makeExpectation(#function)
    }
    
    func testGetBenefitAndRewardInformationTestsWithGoodParameters() {
        
        login { (error) in
            
            Wire.Member.getBenefitAndRewardInfo(tenantId: self._goodTenantId, partyId: self.goodPartyId, vitalityMembershipId: self._goodMembershipId) { error, reward in
                guard error == nil else {
                    XCTFail("unexpected error of type: \(String(describing: error))")
                    return (self.expectation?.fulfill())!
                }
                
                (self.expectation?.fulfill())!
            }
        }
        
        waitForExpectations(timeout: expectationTimeout) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
        }
    }
}
