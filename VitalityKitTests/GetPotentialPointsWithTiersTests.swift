//
//  GetPotentialPointsWithTiersTests.swift
//  VitalityActive
//
//  Created by Simon Stewart on 7/5/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import XCTest
import Alamofire

import VitalityKit


class GetPotentialPointsWithTiersTests: XCTestCase {
    
    var expectation: XCTestExpectation?
    
    override func setUp() {
        
        super.setUp()
        
        expectation = makeExpectation(#function)
    }
    
    func test() {
        
        login { (error) in
            
            // 121 = DeviceDataUpload
            let request = PotentialPointsWithTiersRequest(eventTypeKeys: [121])
            
            Wire.Events.getPotentialPointsWithTiers(tenantId: self._goodTenantId, partyId: self._goodPartyId, request: request) { (error) in
                guard error == nil else {
                    XCTFail("unexpected error of type: \(String(describing: error))")
                    return (self.expectation?.fulfill())!
                }
                
                (self.expectation?.fulfill())!
            }
        }
        
        waitForExpectations(timeout: expectationTimeout) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
        }
    }
}
