//
//  Parser+LoginTests.swift
//  VitalityKitTests
//
//  Created by Valiant Lamban on 20/05/2019.
//  Copyright © 2019 Glucode. All rights reserved.
//

import XCTest
import UIKit

@testable import VitalityKit

class ParserLoginTests: XCTestCase {
    
    var reponse: LoginResponse?
    var expectation: XCTestExpectation?
    
    
    override func setUp() {
        super.setUp()
    }
    
    private func readJSON() -> URL? {
        guard let path = Bundle(for: type(of: self)).url(forResource: "loginResponseStub", withExtension: "json") else {
            return nil
        }
        return path
    }
    
    private func getLoginResponse() -> Any? {
        guard let path = readJSON() else {
            XCTFail("file not found")
            return nil
        }
        guard let json = jsonDictionary(url: path) else {
            XCTFail("incorrect json format")
            return nil
        }
        guard let result = LoginResponse(json: json) else {
            return WireError.parsing
        }
        return result
    }
    
    func testParserLoginCompletePayload() {
        var responseErr: Error?
        expectation = makeExpectation(#function)
        if let result = getLoginResponse() as? LoginResponse {
            Parser.Login.parse(response: result, completion: { error in
                if let error = error {
                    XCTFail("unexpected error of type: \(String(describing: error))")
                }
                self.expectation?.fulfill()
            })
        } else if let result = getLoginResponse() as? WireError {
            responseErr = result
        }
        
        waitForExpectations(timeout: 7) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
        }
        XCTAssertNil(responseErr);
    }
    
    func testParserLoginWithVitalityPartyMembership() {
        let period = VitalityPartyMembership.currentVitalityMembershipPeriod()
        XCTAssertNotNil(period)
    }
    
    func testParserLoginCurrentVitalityProduct() {
        let product = VitalityPartyMembership.currentVitalityProduct()
        XCTAssertNotNil(product)
    }
    
    func testParserLoginMembershipStartDate() {
        let start = VitalityPartyMembership.membershipStartDate()
        XCTAssertNotNil(start)
    }

    func testParserLoginStartedMembership() {
        let isStarted = VitalityPartyMembership.startedMembership()
        XCTAssertFalse(!isStarted, "")
    
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
}
