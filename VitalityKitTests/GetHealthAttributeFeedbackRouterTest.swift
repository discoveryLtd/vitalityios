//
//  GetHealthAttributeFeedbackRouter.swift
//  VitalityActive
//
//  Created by Chris on 2017/08/31.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import XCTest
import VIAUtilities
import VitalityKit

class GetHealthAttributeFeedbackRouterTest :XCTestCase {
    
    var expectation: XCTestExpectation?
    let badPartyId = 123123
    let badTenantId = 123123
    
    override func setUp() {
        
        super.setUp()
        
        expectation = makeExpectation(#function)
    }
    
    func test_Get_Health_Attribute_Feedback_With_Good_Parameters() {
        
        login { (error) in
            
            var healthAttributeTypes = [HealthAttributeType]()
            
            healthAttributeTypes.append(HealthAttributeType(typeKey: 37))
            
            let request = GetHealthAttributeFeedbackRequest(healthAttributeTypes: healthAttributeTypes, partyId: self._goodPartyId)
            
            Wire.Events.healthAttributeFeedbackFor(tenantId: self._goodTenantId, request: request) { (response, error) in
                debugPrint("Done")
                guard error == nil else {
                    XCTFail("unexpected error of type: \(String(describing: error))")
                    return (self.expectation?.fulfill())!
                }
                (self.expectation?.fulfill())!
            }
        }
        
        waitForExpectations(timeout: expectationTimeout) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
        }
    }
    
    func test_Get_Health_Attribute_Feedback_With_Bad_Parameters() {
        
        login { (error) in
            
            var healthAttributeTypes = [HealthAttributeType]()
            
            healthAttributeTypes.append(HealthAttributeType(typeKey: 37))
            
            let request = GetHealthAttributeFeedbackRequest(healthAttributeTypes: healthAttributeTypes, partyId: self.badPartyId)
            
            Wire.Events.healthAttributeFeedbackFor(tenantId: self.badTenantId, request: request) { (response, error) in
                // expected fail
                guard error != nil else {
                    XCTFail("unexpected error of type: \(String(describing: error))")
                    return (self.expectation?.fulfill())!
                }
                (self.expectation?.fulfill())!
            }
        }
        
        waitForExpectations(timeout: expectationTimeout) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
        }
    }
    
    func testGetHealthAttributeWithGoodTenantIdAndPartyId() {
        
        login { (error) in
            
            var healthAttributeTypes = [HealthAttributeType]()
            
            healthAttributeTypes.append(HealthAttributeType(typeKey: 37))
            
            let request = GetHealthAttributeFeedbackRequest(healthAttributeTypes: healthAttributeTypes, partyId: self._goodPartyId)
            
            Wire.Events.healthAttributeFeedbackFor(tenantId: self._goodTenantId, partyId: self._goodPartyId) { (response, error) in

                guard error == nil else {
                    XCTFail("unexpected error of type: \(String(describing: error))")
                    return (self.expectation?.fulfill())!
                }
                (self.expectation?.fulfill())!
            }
        }
        
        waitForExpectations(timeout: expectationTimeout) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
        }
    }
    
    func testGetHealthAttributeWithBadTenantIdAndPartyId() {
        
        login { (error) in
            
            var healthAttributeTypes = [HealthAttributeType]()
            
            healthAttributeTypes.append(HealthAttributeType(typeKey: 37))
            
            let request = GetHealthAttributeFeedbackRequest(healthAttributeTypes: healthAttributeTypes, partyId: self.badPartyId)
            
            Wire.Events.healthAttributeFeedbackFor(tenantId: self.badTenantId, partyId: self.badPartyId) { (response, error) in
                
                guard error != nil else {
                    XCTFail("unexpected error of type: \(String(describing: error))")
                    return (self.expectation?.fulfill())!
                }
                (self.expectation?.fulfill())!
            }
        }
        
        waitForExpectations(timeout: expectationTimeout) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
        }
    }
//    func test_PartyAttributeTypeRef_all_ShouldReturnAsumOf861() {
//        /*This tet failing is most likely due to changes to the enum
//         * Hopefully, the solution is just to update the expected
//         * count and sum in the assertions
//         */
//        for item in PartyAttributeTypeRef.allRawValues() {
//            XCTAssertTrue(PartyAttributeTypeRef(rawValue: item) != nil)
//        }
//    }
    
}
