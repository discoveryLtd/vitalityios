//
//  UpdatedRewardStatusTests.swift
//  VitalityKitTests
//
//  Created by Dexter Anthony Ambrad on 9/28/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import XCTest
import VitalityKit
import VIAUtilities

class UpdatedRewardStatusTests: XCTestCase {
    
    var expectation: XCTestExpectation?
    let usedStatus = AwardedRewardStatusRef.Used
    let unclaimedRewardId = 7922688     // Change this using unclaimedRewardId
    let unclaimedRewardId2 = 7922689
    let badTenantId = 123123
    let badPartyId = 123123
    let badUnclaimedRewardId = 123123
    
    override func setUp() {
        
        super.setUp()
        
        expectation = makeExpectation(#function)
    }
    
    func testUpdateRewardStatusUsingGoodParameters() {
        login { (error) in
            Wire.Rewards.updateRewardStatus(tenantId: self._goodTenantId, partyId: self._goodPartyId, awardedRewardId: self.unclaimedRewardId, status: self.usedStatus) { error, response in
                guard error == nil else {
                    XCTFail("unexpected error of type: \(String(describing: error))")
                    return (self.expectation?.fulfill())!
                }
                
                (self.expectation?.fulfill())!
            }
        }
        
        waitForExpectations(timeout: expectationTimeout) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
        }
    }
    
    func testUpdateRewardStatusUsingBadParameters() {
        login { (error) in
            
            Wire.Rewards.updateRewardStatus(tenantId: self.badTenantId, partyId: self.badPartyId, awardedRewardId: self.badUnclaimedRewardId, status: self.usedStatus) { error, response in
                guard error != nil else {
                    XCTFail("unexpected error of type: \(String(describing: error))")
                    return (self.expectation?.fulfill())!
                }
                
                (self.expectation?.fulfill())!
            }
        }
        
        waitForExpectations(timeout: expectationTimeout) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
        }
    }
    
    func testupdateRewardAcknowledgedStatusWithBadParameter() {
        login { (error) in
            
            Wire.Rewards.updateRewardAcknowledgedStatus(tenantId: self.badTenantId, partyId: self.badPartyId, awardedRewardId: self.badUnclaimedRewardId, status: self.usedStatus) { error, response in
                guard error != nil else {
                    XCTFail("unexpected error of type: \(String(describing: error))")
                    return (self.expectation?.fulfill())!
                }
                
                (self.expectation?.fulfill())!
            }
        }
        
        waitForExpectations(timeout: expectationTimeout) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
        }
    }
    
    func testupdateRewardAcknowledgedStatusWithGoodParameter() {
        login { (error) in
            
            Wire.Rewards.updateRewardAcknowledgedStatus(tenantId: self._goodTenantId, partyId: self._goodPartyId, awardedRewardId: self.unclaimedRewardId2, status: self.usedStatus) { error, response in
                guard error == nil else {
                    XCTFail("unexpected error of type: \(String(describing: error))")
                    return (self.expectation?.fulfill())!
                }
                
                (self.expectation?.fulfill())!
            }
        }
        
        waitForExpectations(timeout: expectationTimeout) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
        }
    }
}
