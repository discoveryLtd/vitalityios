//
//  VDPDelinkDevicesTests.swift
//  VitalityActive
//
//  Created by Simon Stewart on 6/7/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import XCTest
import VitalityKit

class VDPDelinkDevicesTests: XCTestCase {
    
    var expectation: XCTestExpectation?
    
    override func setUp() {
        
        super.setUp()
        
        expectation = makeExpectation(#function)
    }
    
    func testWithGoodParameters() {
        
        login { (error) in
            
            //TODO: double check params and move redirectUrl into separate param
            Wire.Events.delinkDevice(tenantId: self._goodTenantId, identifier: self._goodPartyId, partnerSystem: "Garmin", delinkUrl: "https://qa.vitalityservicing.com/deviceapi/Device/User?partnerSystem=Garmin&tenantId=25&entityNo=zv6aJMmcfT7-oHtDeLxbiQ", delinkMethod: "GET") { (err) in
                if(err != nil) {
                    XCTFail("Call failed with \(String(describing: err))")
                }
                return (self.expectation?.fulfill())!
            }
        }
        
        waitForExpectations(timeout: expectationTimeout) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
        }
    }
}
