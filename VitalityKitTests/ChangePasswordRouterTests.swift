//
//  ChangePasswordRouterTests.swift
//  VitalityKitTests
//
//  Created by ted.philip.l.lat on 18/09/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import XCTest
import VitalityKit
import Alamofire
import VIAUtilities

class ChangePasswordRouterTests: XCTestCase {

    var expectation: XCTestExpectation?
    
    override func setUp() {
        super.setUp()
        
        expectation = makeExpectation(#function)
    }
    
    func testChangePassword() {
        
        login { (error) in
            
            let email = "Armandina_Daily@mailinator.com"
            let currentPassword = "TestPass123"
            let newPassword = "TestPass1234"
            
            let request = ChangePasswordParam(existingPassword: currentPassword, newPassword: newPassword, userName: email)
            
            Wire.Party.changePassword(request: request) { [weak self] (success, response) in
                if success{
                    (self?.expectation?.fulfill())!
                }else{
                    XCTFail("unexpected error of type: \(String(describing: response))")
                    (self!.expectation?.fulfill())!
                }
                
            }
        }
        
        waitForExpectations(timeout: expectationTimeout) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
        }
        
        
    }

}
