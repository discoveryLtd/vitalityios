//
//  WireTests.swift
//  VitalityActive
//
//  Created by Wilmar van Heerden on 2016/10/31.
//  Copyright © 2016 Glucode. All rights reserved.
//

import XCTest
import VitalityKit
import Alamofire

let expectedAPIManagerURL = "https://test.vitalityservicing.com/v1/api/"

enum TestRequest: URLRequestConvertible {
    case apple
    case apimanager

    func asURLRequest() throws -> URLRequest {
        switch self {
        case .apple:
            return URLRequest(url: URL(string: "https://www.apple.com")!)
        case .apimanager:
            return URLRequest(url: URL(string: expectedAPIManagerURL)!)
        }
    }
}

class WireTests: XCTestCase {

    override func setUp() {
        super.setUp()
    }

    override func tearDown() {
        super.tearDown()
    }

    func testBaseURL() {
        let expected = URL(string: expectedAPIManagerURL)?.absoluteString
        let url = try? Wire.urlForPath(path: "").absoluteString
        XCTAssertTrue(url!.hasPrefix(expected!), "Base url is incorrect. Expected '\(String(describing: expected))', received '\(String(describing: url))'")
    }

    func testAPIManagerAccessAdapterAddsAdditionalHeadersForAPIManagerURLs() {
        let dataRequest = Wire.sessionManager.request(TestRequest.apimanager)
        let urlRequest = dataRequest.task?.originalRequest
        let headers = urlRequest?.allHTTPHeaderFields
        XCTAssert(headers?["Authorization"] != nil)
        XCTAssert(headers?["session-id"] != nil)
        XCTAssert(headers?["correlation-id"] != nil)
    }

    func testAPIManagerAccessAdapterOmitsAdditionalHeadersForNonAPIManagerURLs() {
        let dataRequest = Wire.sessionManager.request(TestRequest.apple)
        let urlRequest = dataRequest.task?.originalRequest
        let headers = urlRequest?.allHTTPHeaderFields
        XCTAssert(headers?["Authorization"] == nil)
        XCTAssert(headers?["session-id"] == nil)
        XCTAssert(headers?["correlation-id"] == nil)
    }

    func testAPIManagerAccessAdapterAddsCorrectAdditionalHeadersForAPIManagerCalls() {
        let dataRequest = Wire.sessionManager.request(expectedAPIManagerURL)
        let urlRequest = dataRequest.request
        let headers = urlRequest?.allHTTPHeaderFields
        XCTAssert(headers?["session-id"] == Wire.default.sessionID)

        let authHeader = headers?["Authorization"]
        XCTAssertNotNil(authHeader, "Missing authorization header")
        if authHeader != nil {
            XCTAssert((headers?["Authorization"]?.hasPrefix("Bearer "))!)
        }
    }
}
