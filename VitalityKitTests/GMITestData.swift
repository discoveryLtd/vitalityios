import Foundation
import XCTest
import VitalityKit
import VIAUtilities

class GMITestData {

    public static  let getHealthInformation =
        [
          "sections": [
            [
              "attributes": [
                [
                  "event": [
                    "eventMetaDatas": [],
                    "applicableTo": 14553157,
                    "dateLogged": "2017-11-13T14:46:03.142000000Z[UTC]",
                    "effectiveDateTime": "2017-11-13T14:46:02.425000000Z[UTC]",
                    "eventId": 26570196,
                    "eventSourceTypeCode": "MobileApp",
                    "eventSourceTypeKey": 1,
                    "eventSourceTypeName": "MobileApp",
                    "reportedBy": 14553157,
                    "typeCode": "GnrlHealthComp",
                    "typeKey": 138,
                    "typeName": "Completion of the Genereal Health Section of the VHR"
                  ],
                  "healthAttributeFeedbacks": [
                    [
                      "feedbackTypeCode": "VAgeAbove",
                      "feedbackTypeKey": 36,
                      "feedbackTypeName": "Too High",
                      "feedbackTypeTypeCode": "Observation",
                      "feedbackTypeTypeKey": 3,
                      "feedbackTypeTypeName": "Feedback that is giving an observation"
                    ]
                  ],
                  "attributeTypeCode": "VitalityAge",
                  "attributeTypeKey": 25,
                  "attributeTypeName": "VitalityAge",
                  "measuredOn": "2017-11-13",
                  "sortOrder": 1,
                  "value": "43.39645845137561"
                ]
              ],
              "sections": [
                [
                  "attributes": [
                    [
                      "event": [
                        "eventMetaDatas": [],
                        "applicableTo": 14553157,
                        "dateLogged": "2017-09-14T09:10:52.364000000Z[UTC]",
                        "effectiveDateTime": "2017-09-14T09:10:51.974000000Z[UTC]",
                        "eventId": 23266556,
                        "eventSourceTypeCode": "MobileApp",
                        "eventSourceTypeKey": 1,
                        "eventSourceTypeName": "MobileApp",
                        "reportedBy": 14553157,
                        "typeCode": "GnrlHealthComp",
                        "typeKey": 138,
                        "typeName": "Completion of the Genereal Health Section of the VHR"
                      ],
                      "healthAttributeFeedbacks": [
                        [
                          "feedbackTips": [
                              "note" : "Some note",
                              "sortOrder" : 1,
                              "typeCode" : "code",
                              "typeKey" : 1,
                              "typeName" : "Some Name"
                          ],
                          "feedbackTypeCode": "FGlucoseOutRange",
                          "feedbackTypeKey": 21,
                          "feedbackTypeName": "A healthy fasting glucose range is less than  126 mg/dl",
                          "feedbackTypeTypeCode": "Conclusion",
                          "feedbackTypeTypeKey": 1,
                          "feedbackTypeTypeName": "Feedback that is giving a conclusion"
                        ]
                      ],
                      "recommendations": [],
                      "attributeTypeCode": "FastingGlucose",
                      "attributeTypeKey": 8,
                      "attributeTypeName": "FastingGlucose",
                      "measuredOn": "2017-09-14",
                      "sortOrder": 1,
                      "unitofMeasure": "100122",
                      "value": "14.833333333333334"
                    ]
                  ],
                  "sections": [
                    [
                      "attributes": [
                        [
                          "event": [
                            "eventMetaDatas": [
                              [
                                "type": 166,
                                "value": "25.344352617079892"
                              ]
                            ],
                            "applicableTo": 14553157,
                            "dateLogged": "2017-11-07T09:30:22.259000000Z[UTC]",
                            "effectiveDateTime": "2017-11-07T09:30:20.582000000Z[Z]",
                            "eventId": 26507303,
                            "eventSourceTypeCode": "MobileApp",
                            "eventSourceTypeKey": 1,
                            "eventSourceTypeName": "MobileApp",
                            "reportedBy": 14553157,
                            "typeCode": "BMI",
                            "typeKey": 99,
                            "typeName": "BMI"
                          ],
                          "healthAttributeFeedbacks": [
                            [
                              "feedbackTypeCode": "BMIAbove",
                              "feedbackTypeKey": 1,
                              "feedbackTypeName": "A healthy BMI range is between 24.9 and 18.5",
                              "feedbackTypeTypeCode": "Observation",
                              "feedbackTypeTypeKey": 3,
                              "feedbackTypeTypeName": "Feedback that is giving an observation"
                            ]
                          ],
                          "attributeTypeCode": "BMI",
                          "attributeTypeKey": 37,
                          "attributeTypeName": "Body Mass Index",
                          "measuredOn": "2017-11-07",
                          "sortOrder": 1,
                          "unitofMeasure": "100164",
                          "value": "25.344352617079892"
                        ]
                      ],
                      "sections": [],
                      "sortOrder": 1,
                      "typeCode": "VAgeFocusImprovement",
                      "typeKey": 8,
                      "typeName": "Focus on improving these first"
                    ],
                    [
                      "attributes": [],
                      "sections": [],
                      "sortOrder": 2,
                      "typeCode": "VAgeMoreImprovements",
                      "typeKey": 9,
                      "typeName": "Have a go at these improvements"
                    ]
                  ],
                  "sortOrder": 1,
                  "typeCode": "VAgeResultBad",
                  "typeKey": 4,
                  "typeName": "What you need to improve"
                ],
                [
                  "attributes": [],
                  "sections": [
                    [
                      "attributes": [],
                      "sections": [],
                      "sortOrder": 1,
                      "typeCode": "VAgeAreasDoingWell",
                      "typeKey": 10,
                      "typeName": "You are doing well in these areas.  Keep it up!"
                    ],
                    [
                      "attributes": [],
                      "sections": [],
                      "sortOrder": 2,
                      "typeCode": "VAgeMoreDoingWell",
                      "typeKey": 11,
                      "typeName": "Good going! Here’s some more"
                    ]
                  ],
                  "sortOrder": 2,
                  "typeCode": "VAgeResultGood",
                  "typeKey": 5,
                  "typeName": "What you are doing well"
                ],
                [
                  "attributes": [
                    [
                      "healthAttributeFeedbacks": [],
                      "recommendations": [],
                      "attributeTypeCode": "BloodPressure",
                      "attributeTypeKey": 42,
                      "attributeTypeName": "Blood Pressure",
                      "sortOrder": 1
                    ]
                  ],
                  "sections": [
                    [
                      "attributes": [
                        [
                          "healthAttributeFeedbacks": [],
                          "attributeTypeCode": "AlcoholDrinksPerDay",
                          "attributeTypeKey": 44,
                          "attributeTypeName": "Alcohol Drinks Per Day",
                          "sortOrder": 1
                        ],
                        [
                          "healthAttributeFeedbacks": [],
                          "attributeTypeCode": "TobaccoUse",
                          "attributeTypeKey": 48,
                          "attributeTypeName": "Tobacco Use",
                          "sortOrder": 1
                        ],
                        [
                          "healthAttributeFeedbacks": [],
                          "attributeTypeCode": "SaltyFoodConsm",
                          "attributeTypeKey": 53,
                          "attributeTypeName": "Salty Food Consm",
                          "sortOrder": 1
                        ]
                      ],
                      "sections": [],
                      "sortOrder": 1,
                      "typeCode": "VAgeNeedToKnow",
                      "typeKey": 12,
                      "typeName": "We still need to know these things about you"
                    ],
                    [
                      "attributes": [],
                      "sections": [],
                      "sortOrder": 2,
                      "typeCode": "VAgeMoreNeedToKnow",
                      "typeKey": 13,
                      "typeName": "Have a look at these too"
                    ]
                  ],
                  "sortOrder": 3,
                  "typeCode": "VAgeResultUnknown",
                  "typeKey": 6,
                  "typeName": "What you have not provided"
                ]
              ],
              "sortOrder": 1,
              "typeCode": "VAgeSection",
              "typeKey": 1,
              "typeName": "VitalityAge"
            ],
            [
              "attributes": [],
              "sections": [],
              "sortOrder": 2,
              "typeCode": "NutritionSection",
              "typeKey": 2,
              "typeName": "Nutrition"
            ],
            [
              "attributes": [],
              "sections": [],
              "sortOrder": 3,
              "typeCode": "MWBSection",
              "typeKey": 3,
              "typeName": "MentalWellBeing"
            ]
          ]
        ]
}
