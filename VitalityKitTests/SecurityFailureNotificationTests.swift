////
////  File.swift
////  VitalityActive
////
////  Created by Simon Stewart on 3/14/17.
////  Copyright © 2017 Glucode. All rights reserved.
////
//
//
//import XCTest
//import Alamofire
//
//import VitalityKit
//
//class SecurityFailureNotificationTests: XCTestCase {
//    let securityFailureTenantId = 3
//    let goodTenantId = 2
//    let goodRoleTypeKey = 2
//    let goodPartyId = 100002
//    let securityFailurePartyId = 999
//    let effectiveDate = Date()
//}
//
//extension SecurityFailureNotificationTests {
//    override func setUp() {
//        
//        super.setUp()
//        
//        let expect = makeExpectation(#function)
//        
//        self.login() { (error) in
//            guard error == nil else {
//                XCTFail("login failed in setUp")
//                return expect.fulfill()
//            }
//            print("logged in")
//            return expect.fulfill()
//        }
//        
//        // Wait for the async request to complete
//        waitForExpectations(timeout: expectationTimeout, handler: nil)
//        
//    }
//    func testWithExpectedSecurityIssue() {
//        
//        let expect = makeExpectation(#function)
//        
//        NotificationCenter.default.addObserver(forName: .VIAServerSecurityException, object: nil, queue: nil) { (notification) in
//            return expect.fulfill() // this will stop execution, so the .getMemberProfile completion block *isn't* expected to run
//        }
//        Wire.Member.getMemberProfile(tenantId: securityFailureTenantId, roleTypeKey: goodRoleTypeKey, partyId: goodPartyId, effectiveDate: effectiveDate) { (response, error) in
//            guard error == nil else {
//                XCTFail("unexpected error of type: \(error)")
//                return expect.fulfill()
//            }
//        }
//        waitForExpectations(timeout: expectationTimeout) { error in
//            if let error = error {
//                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
//            }
//        }
//    }
//    func testWithNoExpectedSecurityIssue() {
//        
//        let expect = makeExpectation(#function)
//        
//        NotificationCenter.default.addObserver(forName: .VIAServerSecurityException, object: nil, queue: nil) { (notification) in
//            XCTFail("Observer has handled unexpectedly")
//            return expect.fulfill()
//        }
//        Wire.Member.getMemberProfile(tenantId: goodTenantId, roleTypeKey: goodRoleTypeKey, partyId: goodPartyId, effectiveDate: effectiveDate) { (response, error) in
//            guard error == nil else {
//                XCTFail("unexpected error of type: \(error)")
//                return expect.fulfill()
//            }
//            
//            return expect.fulfill()
//            
//        }
//        waitForExpectations(timeout: expectationTimeout) { error in
//            if let error = error {
//                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
//            }
//        }
//    }
//}
