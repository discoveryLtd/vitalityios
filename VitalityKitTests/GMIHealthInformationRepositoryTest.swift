import Foundation
import XCTest
import VitalityKit
import VIAUtilities

class GMIHealthInformationRepositoryTest: XCTestCase {
    
    func test_ItShouldSaveAndRetrieveAnItem() {
        let hi = GMIReportedHealthInformation.build(response: GMIGetHealthInformation(json: GMITestData.getHealthInformation))
        let repo = HealthInformationRepository()
        repo.save(healthInformationP: hi)
        _ = repo.getHealthInformation()
    }
    
    func test_ItShouldFindTheVitalityAgeSection() {
        let hi = GMIReportedHealthInformation.build(response: GMIGetHealthInformation(json: GMITestData.getHealthInformation))
        let repo = HealthInformationRepository()
        repo.save(healthInformationP: hi)
        if let section = repo.getSectionBy(sectionKey: SectionRef.VAgeSection) {
            XCTAssertEqual(SectionRef.VAgeSection, section.typeKey)
        } else {
            XCTFail("didn't get the expected section")
        }
    }
    
    func test_ItShouldReturnNil() {
        let hi = GMIReportedHealthInformation.build(response: GMIGetHealthInformation(json: GMITestData.getHealthInformation))
        let repo = HealthInformationRepository()
        repo.save(healthInformationP: hi)
        XCTAssertNil(repo.getSectionBy(sectionKey: SectionRef.Unknown))
    }
}
