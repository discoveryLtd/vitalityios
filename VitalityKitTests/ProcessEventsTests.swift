//
//  ProcessEventsTests.swift
//  VitalityActive
//
//  Created by Simon Stewart on 4/4/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation

import XCTest
import VitalityKit
import Alamofire
import VIAUtilities

class ProcessEventsTests: XCTestCase {
    
    let badTenantId = 123123
    let badPartyId = 123123
    var expectation: XCTestExpectation?
    
    override func setUp() {
        
        super.setUp()
        
        expectation = makeExpectation(#function)
    }
    
    func testUsingGoodTenantAsParam() {

        login { (error) in
        
            var associatedEvents = [ProcessEventsAssociatedEvent]()
            associatedEvents.append(ProcessEventsAssociatedEvent(eventOccuredOn: Date(), eventCapturedOn: Date(),
                                                                 eventMetaDataTypeKey: .ActiveEnergyExpend,
                                                                 eventMetaDataUnitOfMeasure: UnitOfMeasureRef.BMI,
                                                                 eventMetaDataValue: "1", associatedEventTypeKey: EventAssociationTypeRef.DocumentaryProof,
                                                                 eventSourceTypeKey: EventSourceRef.Benefit, partyId: self._goodPartyId, reportedBy: self._goodPartyId,
                                                                 eventId: 1, typeKey: EventTypeRef.ActivateVMembership))
            
            var events = [ProcessEventsEvent]()
            events.append(ProcessEventsEvent(date: Date(), eventTypeKey: EventTypeRef.ActivateVMembership, eventSourceTypeKey: EventSourceRef.Benefit, partyId: self._goodPartyId,
                                             reportedBy: self._goodPartyId, associatedEvents: associatedEvents, eventMetaDataTypeKey: EventMetaDataTypeRef.ActiveEnergyExpend,
                                             eventMetaDataUnitOfMeasure: UnitOfMeasureRef.FastingGlucoseMilligramsPerDeciLitre, eventMetaDataValues: nil))

            
            Wire.Events.processEvents(tenantId: self._goodTenantId, events: events) { (response, error) in
                guard error == nil else {
                    XCTFail("unexpected error of type: \(String(describing: error))")
                    return (self.expectation?.fulfill())!
                }
                
                (self.expectation?.fulfill())!
            }
        }
        
        waitForExpectations(timeout: expectationTimeout, handler: nil)
        
    }
    
    func testUsingBadTenantAsParam() {
        
        login { (error) in
            
            var associatedEvents = [ProcessEventsAssociatedEvent]()
            associatedEvents.append(ProcessEventsAssociatedEvent(eventOccuredOn: Date(), eventCapturedOn: Date(),
                                                                 eventMetaDataTypeKey: .ActiveEnergyExpend,
                                                                 eventMetaDataUnitOfMeasure: UnitOfMeasureRef.BMI,
                                                                 eventMetaDataValue: "1", associatedEventTypeKey: EventAssociationTypeRef.DocumentaryProof,
                                                                 eventSourceTypeKey: EventSourceRef.Benefit, partyId: self.badPartyId, reportedBy: self.badPartyId,
                                                                 eventId: 1, typeKey: EventTypeRef.ActivateVMembership))
            
            var events = [ProcessEventsEvent]()
            events.append(ProcessEventsEvent(date: Date(), eventTypeKey: EventTypeRef.ActivateVMembership, eventSourceTypeKey: EventSourceRef.Benefit, partyId: self.badPartyId,
                                             reportedBy: self.badPartyId, associatedEvents: associatedEvents, eventMetaDataTypeKey: EventMetaDataTypeRef.ActiveEnergyExpend,
                                             eventMetaDataUnitOfMeasure: UnitOfMeasureRef.FastingGlucoseMilligramsPerDeciLitre, eventMetaDataValues: nil))
            
            
            Wire.Events.processEvents(tenantId: self.badTenantId, events: events) { (response, error) in
                guard error != nil else {
                    XCTFail("unexpected error of type: \(String(describing: error))")
                    return (self.expectation?.fulfill())!
                }
                
                (self.expectation?.fulfill())!
            }
        }
        
        waitForExpectations(timeout: expectationTimeout, handler: nil)
        
    }
    
    func testUsingGoodTenantAndPartyAsParam() {
        
        login { (error) in
            
            let events = [EventTypeRef.VHRDataPrivacyAgree]

            Wire.Events.processEvents(tenantId: self._goodTenantId, partyId: self._goodPartyId, events: events) { ( error ) in
                guard error == nil else {
                    XCTFail("unexpected error of type: \(String(describing: error))")
                    return (self.expectation?.fulfill())!
                }
                
                (self.expectation?.fulfill())!
            }
        }
        
        waitForExpectations(timeout: expectationTimeout, handler: nil)
        
    }
    
    func testUsingBadTenantAndPartyAsParam() {
        
        login { (error) in
            
            let events = [EventTypeRef.VHRDataPrivacyAgree]
            
            Wire.Events.processEvents(tenantId: self.badTenantId, partyId: self.badPartyId, events: events) { ( error ) in
                guard error != nil else {
                    XCTFail("unexpected error of type: \(String(describing: error))")
                    return (self.expectation?.fulfill())!
                }
                
                (self.expectation?.fulfill())!
            }
        }
        
        waitForExpectations(timeout: expectationTimeout, handler: nil)
        
    }
}
