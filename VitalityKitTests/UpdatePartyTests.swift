//
//  UpdatePartyTests.swift
//  VitalityActive
//
//  Created by Simon Stewart on 2/23/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

import XCTest
import Alamofire

import VitalityKit

class UpdatePartyTests: XCTestCase {
    // TODO: WIP: params
    let goodPartyId = 100002
    
    var expectation: XCTestExpectation?
    
    override func setUp() {
        
        super.setUp()
        
        expectation = makeExpectation(#function)
    }
    
    func testUpdatePartyWithGoodParameters() {
        
        login { (error) in
            
            let request: UpdatePartyRequest = UpdatePartyRequest(partyId: self.goodPartyId)
            request.languagePreference = UpdatePartyLanguagePreference(ISOCode: "123", value: "a")
            
            Wire.Party.update(request: request) { error in
                guard error == nil else {
                    XCTFail("unexpected error of type: \(String(describing: error))")
                    return (self.expectation?.fulfill())!
                }
                
                (self.expectation?.fulfill())!
            }
        }
        
        waitForExpectations(timeout: expectationTimeout) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
        }
    }
}
