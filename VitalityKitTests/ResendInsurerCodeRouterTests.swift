//
//  ResendInsurerCodeRouterTests.swift
//  VitalityKitTests
//
//  Created by ted.philip.l.lat on 11/09/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import XCTest
import VitalityKit
import Alamofire
import VIAUtilities

class ResendInsurerCodeRouterTests: XCTestCase {
    
    var credentials = DataProvider.newRealm()
    var expectation: XCTestExpectation?
    var username = "ntest--Cameron_McDaniels@mailinator.com"
    
    override func setUp() {
        super.setUp()
        
        expectation = makeExpectation(#function)
    }
    
    func testResendInsurerCode() {
            
        let tenantId = AppSettings.getAppTenantId() ?? 2
        let trimmedEmail = Wire.updateEnvironmentAndTrim(email: self.username)
        let request = ResendInsurerCodeParam(eventSource: 10, username: trimmedEmail)
        
        Wire.Party.resendInsurerCode(tenantId: tenantId, request: request, completion: { error in
            guard error == nil else {
                XCTFail("unexpected error of type: \(String(describing: error))")
                return (self.expectation?.fulfill())!
            }
            
            (self.expectation?.fulfill())!
        })
        
        waitForExpectations(timeout: expectationTimeout) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
        }
    }
    
}
