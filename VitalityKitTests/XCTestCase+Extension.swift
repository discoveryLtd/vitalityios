//
//  XCTest+Extension.swift
//  VitalityActive
//
//  Created by Simon Stewart on 2/14/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

import XCTest
import RealmSwift
import VitalityKit


extension XCTestCase {

    var coreRealm: Realm {
        get {
            return DataProvider.newRealm()
        }
    }
    
    var _goodUserName: String {
        get {
            #if WIREMOCK
                return "test--wilmar@glucode.com"
            #else
                //return "ntest--Corrie_McBedford@mailinator.com"
            return "qa--Dionna_McBusch@mailinator.com"
                //return "amar@glucode.com"
            #endif
        }
    }
    
    var _goodTenantId: Int {
        get {
            return coreRealm.getTenantId()
        }
    }
    
    var _goodPartyId: Int {
        get{
            return coreRealm.getPartyId()
        }
    }
    
    var _goodMembershipId: Int {
        get{
            return coreRealm.getMembershipId()
        }
    }
    
    var _goodPassword: String {
        get {
            return "TestPass123"
            //return "iO$active13"
        }
    }

    var expectationTimeout: Double {
        get {
            return 70.0
        }
    }
    
    public func makeExpectation(_ callingFunctionName: String) -> XCTestExpectation {
        return expectation(description: "Expectation failure for \(callingFunctionName)")
    }
    
    public func login(callback: @escaping ((_: Error?) -> Void)) {
        let ex = makeExpectation(#function)
        Wire.Member.login(email: _goodUserName, password: _goodPassword, completion: { error in
            ex.fulfill()
            callback(error.0)
        })
    }
    
    open override func setUp() {
        self.continueAfterFailure = false
        #if !WIREMOCK
             //XCTFail("Don't run unit tests against LIVE")
        #endif
    }
    
    func createDate(value: String) -> Date {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd"
        return formatter.date(from: value)!
    }
    
    func jsonDictionary(url: URL) -> [String: Any]? {
        do {
            let data = try Data(contentsOf: url)
            return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
        } catch {
            print(error.localizedDescription)
        }
        return nil
    }

}
