//
//  CreateEventTests.swift
//  VitalityActive
//
//  Created by Simon Stewart on 2/9/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

import XCTest
import Alamofire
import VIAUtilities
import VitalityKit

class CreateEventTests: XCTestCase {
    let goodEvents = [EventTypeRef.LoginTandCAgree]
    
    var goodPartyId: Int = 0
    var goodTenantId = 0

    var expectation: XCTestExpectation?
    var credentials = DataProvider.newRealm()
    
    override func setUp() {
        
        super.setUp()
        
        expectation = makeExpectation(#function)
    }
    
    func testCreateEventsWithGoodParameters() {
        login { (error) in
        
        //gets the credentials of the account used in XCTestCase+Extension
        self.goodPartyId = self.credentials.getPartyId()
        self.goodTenantId = self.credentials.getTenantId()
        
        Wire.Events.createEvents(tenantId: self.goodTenantId, partyId: self.goodPartyId, events: self.goodEvents) { error in
            guard error == nil else {
                XCTFail("unexpected error of type: \(String(describing: error))")
                return (self.expectation?.fulfill())!
            }

            (self.expectation?.fulfill())!
        }
        }

        waitForExpectations(timeout: expectationTimeout) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
        }
    }
    func testCreateEventsWithBadParameters_ZeroEvents() {
        login { (error) in
        Wire.Events.createEvents(tenantId: self.goodTenantId, partyId: self.goodPartyId, events: []) { error in
            guard error != nil else {
                XCTFail("unexpected error of type: \(String(describing: error))")
                return (self.expectation?.fulfill())!
            }

            (self.expectation?.fulfill())!
        }
        }

        waitForExpectations(timeout: expectationTimeout) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
        }
    }
}
