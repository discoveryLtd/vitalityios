//
//  VDPLinkDevicesTests.swift
//  VitalityActive
//
//  Created by Simon Stewart on 6/7/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import XCTest
import VitalityKit

class VDPLinkDevicesTests: XCTestCase {
    
    var badTenantId = 123123
    var expectation: XCTestExpectation?
    
    override func setUp() {
        
        super.setUp()
        
        expectation = makeExpectation(#function)
    }
/*  Note: This functional testing is tested using badParameters only because there is a problem with the API. It always returns 302 response even in actual linking of the device. The comment below that says "fails" is from previous developer from glucode. */
    
    
    // fails
    func testUsingBadParameters() {
        
        login { (error) in
            
            let url = "https://app.validic.com/organizations/58c075c4ff9d93000c00000e/auth/fitbit?user_token={USER_ACCESS_TOKEN}&format_redirect=json&redirect_uri={URI}"
            
            let request = VDPLinkDeviceRequest(userEmail: "Dionna_McBusch@mailinator.com", userIdentifier: "1001212620", userIdentifierType: "PARTY_ID", partnerSystem: "Validic", partnerDevice: "Fitbit", partnerLinkUrl: url, partnerLinkMethod: "GET", partnerLinkedStatus: "UNLINKED", partnerLinkRedirectUrl: "vitalityapp://something/tenant/25", partnerRedirectUrl: "com.vitalityactivemobile.sumitomo://wellnessDevicesApps")
            
            
            Wire.Events.linkDevice(tenantId: self.badTenantId, request: request, completion:  { (redirectLocation, err) in
                guard err != nil else {
                    XCTFail("unexpected error of type: \(String(describing: redirectLocation))")
                    return (self.expectation?.fulfill())!
                }
                self.expectation?.fulfill()
            })
        }
        
        self.waitForExpectations(timeout: self.expectationTimeout) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
        }
    }
    
    func testSoftBankLinkUsingBadParameters() {
        
        login { (error) in
            let finish = false
            let token = "kaurhqvnwn1p23trkhjm9ngqkurbzc1zvwi04zw1ix5iojnliciaqviiybds7h3i"
            let refreshToken = "kaurhqvnwn1p23trkhjm9ngqkurbzc1zvwi04zw1ix5iojnliciaqviiybds7h3i"
            let userId = "kaurhqvnwn1p23trkhjm9ngqkurbzc1zvwi04zw1ix5iojnliciaqviiybds7h3i"
            
            let request = VDPLinkSoftBankDeviceRequest(refreshToken: refreshToken, token: token, userId: userId)
            
            Wire.Events.linkSoftBankDevice(tenantId: self.badTenantId, identifier: self._goodPartyId, partnerSystem: "Softbank", request: request) { [weak self] error in
                guard error != nil else {
                    XCTFail("Call failed with \(String(describing: error))")
                    return (self!.expectation!.fulfill())
                }
                (self!.expectation!.fulfill())
            }
        }
        
        self.waitForExpectations(timeout: expectationTimeout) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
        }
    }
}
