//
//  GetEventByPartyTests.swift
//  VitalityActive
//
//  Created by Simon Stewart on 8/8/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import XCTest
import VitalityKit
import VIAUtilities

class GetEventByPartyTests: XCTestCase {
    
    var expectation: XCTestExpectation?
    
    override func setUp() {
        
        super.setUp()
        
        expectation = makeExpectation(#function)
    }
    
    func testWithFilters() {
        
        login { (error) in
            
            let request = GetEventByPartyParameter(partyId: self._goodPartyId, effectiveFrom: Date(), effectiveTo: Date(), eventTypeFilterTypeKeys: [1])
            Wire.Events.getEventByParty(tenantId: self._goodTenantId, request: request) { (error, response) in
                guard error == nil else {
                    XCTFail("failed")
                    return (self.expectation?.fulfill())!
                }
                return (self.expectation?.fulfill())!
            }
        }
        
        // Wait for the async request to complete
        waitForExpectations(timeout: expectationTimeout, handler: nil)
        
    }
    
    func testWithTypes() {
        // eventTypesTypeKeys: [1] not getting any response with this, but no error
        
        login { (error) in
            
            let request = GetEventByPartyParameter(partyId: self._goodPartyId, effectiveFrom: Date(), effectiveTo: Date(), eventTypesTypeKeys: [.VHRAssmntCompleted])
            Wire.Events.getEventByParty(tenantId: self._goodTenantId, request: request) { (error, response) in
                guard error == nil else {
                    XCTFail("failed")
                    return (self.expectation?.fulfill())!
                }
                print("logged in")
                return (self.expectation?.fulfill())!
            }
        }
        
        // Wait for the async request to complete
        waitForExpectations(timeout: expectationTimeout, handler: nil)
        
    }
    func testWithFilterTypes() {
        
        login { (error) in
            
            let request = GetEventByPartyParameter(partyId: self._goodPartyId, effectiveFrom: Date(), effectiveTo: Date(), eventTypeFilterTypeKeys: [EventFilterRef.MobileEventsFeed.rawValue])
            Wire.Events.getEventByParty(tenantId: self._goodTenantId, request: request) { (error, response) in
                guard error == nil else {
                    XCTFail("failed")
                    return (self.expectation?.fulfill())!
                }
                print("logged in")
                return (self.expectation?.fulfill())!
            }
        }
        
        // Wait for the async request to complete
        waitForExpectations(timeout: expectationTimeout, handler: nil)
    }
}
