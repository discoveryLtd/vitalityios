//
//  AppConfigModelTests.swift
//  VitalityActive
//
//  Created by Marius on 2017/03/20.
//  Copyright © 2017 Glucode. All rights reserved.
//

import XCTest
import VitalityKit
import RealmSwift

class AppConfigModelTests: XCTestCase {
	var application: LoginApplication?
	override func setUp() {
		super.setUp()

//		Realm.Configuration.defaultConfiguration.inMemoryIdentifier = self.name
		let file = "LoginResponse"

		guard let pathString = Bundle(for: type(of: self)).path(forResource: file, ofType: "json") else {
			fatalError("UnitTestData.json not found")
		}

		guard let jsonString = try? NSString(contentsOfFile: pathString, encoding: String.Encoding.utf8.rawValue) else {
			fatalError("Unable to convert UnitTestData.json to String")
		}

		print("The JSON string is: \(jsonString)")

		guard let jsonData = jsonString.data(using: String.Encoding.utf8.rawValue) else {
			fatalError("Unable to convert UnitTestData.json to NSData")
		}

		do {
			guard let jsonDictionary = try JSONSerialization.jsonObject(with: jsonData, options: []) as? [String:Any] else {
				fatalError("Unable to convert UnitTestData.json to JSON dictionary")
			}
			self.application = LoginApplication.init(json: jsonDictionary)!
		} catch _ {

		}
	}

	override func tearDown() {
		let realm = DataProvider.newRealm()
		try! realm.write {
			realm.deleteAll()
		}

		// Put teardown code here. This method is called after the invocation of each test method in the class.
		super.tearDown()
	}

	func testApplicationParsing() {
		AppConfig.parseAndPersist(self.application!)
		let realm = DataProvider.newRealm()
		if let object = realm.objects(AppConfig.self).first {
			XCTAssertNotNil(object)
			XCTAssertEqual("Sumitomo Active", object.name)
			XCTAssertEqual("Sumitomo", object.typeCode)
			XCTAssertEqual(.Sumitomo, object.typeKey)
		} else {
			XCTAssertTrue(false)
		}
	}

	func testApplicationFeature() {
		AppConfig.parseAndPersist(self.application!)
		let realm = DataProvider.newRealm()
		let objects = realm.objects(AppConfigFeature.self)
		XCTAssertEqual(7, objects.count)

		let firstFeature = objects.first
		XCTAssertEqual(firstFeature?.toggle, true)
		XCTAssertEqual(firstFeature?.name, "Insurer Configuration")
		XCTAssertEqual(firstFeature?.typeCode, "InsConfig")
		XCTAssertEqual(firstFeature?.typeKey, .InsConfig)
		XCTAssertEqual(4, firstFeature?.featureParameters.count)
	}

	func testApplicationFeatureParameterParsing() {
		AppConfig.parseAndPersist(self.application!)
		let realm = DataProvider.newRealm()
		let objects = realm.objects(AppConfigFeatureParameter.self)
		XCTAssertEqual(10, objects.count)

		let firstAppConfigParameter = objects.first
		XCTAssertEqual("gradientColor1", firstAppConfigParameter?.name)
	}

	func testApplicationVersionParsing() {
		AppConfig.parseAndPersist(self.application!)
		let realm = DataProvider.newRealm()
		let objects = realm.objects(AppConfigVersion.self)
		XCTAssertEqual(1, objects.count)

		let appConfigVersion = objects.first
		XCTAssertEqual("1.0", appConfigVersion?.releaseVersion)
		XCTAssertEqual(7, appConfigVersion?.appFeature.count)
	}
}
