//
//  AppConfigModelNoOptionalTests.swift
//  VitalityActive
//
//  Created by Marius on 2017/03/22.
//  Copyright © 2017 Glucode. All rights reserved.
//

import XCTest
import VitalityKit
import RealmSwift

class AppConfigModelNoOptionalTests: XCTestCase {

	var application: LoginApplication?
	override func setUp() {
		super.setUp()

		//Realm.Configuration.defaultConfiguration.inMemoryIdentifier = self.name
	}

	override func tearDown() {
		let realm = DataProvider.newRealm()
		try! realm.write {
			realm.deleteAll()
		}
		self.application = nil
		// Put teardown code here. This method is called after the invocation of each test method in the class.
		super.tearDown()
	}

	func readFile(_ fullNoOptionals: Bool = false) {

		var file = "LoginResponseNoOptionals"
		if (fullNoOptionals) {
			file = "LoginResponseNoOptionalsComplete"
		}

		guard let pathString = Bundle(for: type(of: self)).path(forResource: file, ofType: "json") else {
			fatalError("UnitTestData.json not found")
		}

		guard let jsonString = try? NSString(contentsOfFile: pathString, encoding: String.Encoding.utf8.rawValue) else {
			fatalError("Unable to convert UnitTestData.json to String")
		}

		print("The JSON string is: \(jsonString)")

		guard let jsonData = jsonString.data(using: String.Encoding.utf8.rawValue) else {
			fatalError("Unable to convert UnitTestData.json to NSData")
		}

		do {
			guard let jsonDictionary = try JSONSerialization.jsonObject(with: jsonData, options: []) as? [String:Any] else {
				fatalError("Unable to convert UnitTestData.json to JSON dictionary")
			}
			self.application = LoginApplication.init(json: jsonDictionary)!
		} catch _ {

		}
	}

	func testApplicationParsing() {
		self.readFile()
		AppConfig.parseAndPersist(self.application!)
		let realm = DataProvider.newRealm()
		if let object = realm.objects(AppConfig.self).first {
			XCTAssertNotNil(object)
			XCTAssertEqual("Sumitomo Active", object.name)
			XCTAssertEqual(.Sumitomo, object.typeKey)
		} else {
			XCTAssertTrue(false)
		}
	}

	func testApplicationFeature() {
		self.readFile()
		AppConfig.parseAndPersist(self.application!)
		let realm = DataProvider.newRealm()
		let objects = realm.objects(AppConfigFeature.self)
		XCTAssertEqual(0, objects.count)

		let firstFeature = objects.first
		XCTAssertEqual(nil, firstFeature?.featureParameters.count)
	}

	func testApplicationFeatureParameterParsing() {
		self.readFile()
		AppConfig.parseAndPersist(self.application!)
		let realm = DataProvider.newRealm()
		let objects = realm.objects(AppConfigFeatureParameter.self)
		XCTAssertEqual(0, objects.count)

		let firstAppConfigParameter = objects.first
		XCTAssertEqual(nil, firstAppConfigParameter?.name)
	}

	func testApplicationVersionParsing() {
		self.readFile()
		AppConfig.parseAndPersist(self.application!)
		let realm = DataProvider.newRealm()
		let objects = realm.objects(AppConfigVersion.self)
		XCTAssertEqual(1, objects.count)

		let appConfigVersion = objects.first
		XCTAssertEqual("", appConfigVersion?.releaseVersion)
		XCTAssertEqual(0, appConfigVersion?.appFeature.count)
	}

	func testApplicationParsingFullOptionals() {
		self.readFile(true)
		AppConfig.parseAndPersist(self.application!)
		let realm = DataProvider.newRealm()
		if let object = realm.objects(AppConfig.self).first {
			XCTAssertNotNil(object)
			XCTAssertEqual("Sumitomo Active", object.name)
			XCTAssertEqual(.Sumitomo, object.typeKey)
		} else {
			XCTAssertTrue(false)
		}
	}

	func testApplicationFeatureFullOptionals() {
		self.readFile(true)
		AppConfig.parseAndPersist(self.application!)
		let realm = DataProvider.newRealm()
		let objects = realm.objects(AppConfigFeature.self)
		XCTAssertEqual(7, objects.count)

		let firstFeature = objects.first
		XCTAssertEqual(4, firstFeature?.featureParameters.count)
		XCTAssertEqual(false, firstFeature?.toggle)
		XCTAssertEqual("", firstFeature?.name)
	}

	func testApplicationFeatureParameterParsingFullOptionals() {
		self.readFile(true)
		AppConfig.parseAndPersist(self.application!)
		let realm = DataProvider.newRealm()
		let objects = realm.objects(AppConfigFeatureParameter.self)
		XCTAssertEqual(10, objects.count)

		let firstAppConfigParameter = objects[2]
		XCTAssertEqual(nil, firstAppConfigParameter.name)
	}

	func testApplicationVersionParsingFullOptionals() {
		self.readFile(true)
		AppConfig.parseAndPersist(self.application!)
		let realm = DataProvider.newRealm()
		let objects = realm.objects(AppConfigVersion.self)
		XCTAssertEqual(1, objects.count)

		let appConfigVersion = objects.first
		XCTAssertEqual("", appConfigVersion?.releaseVersion)
		XCTAssertEqual(7, appConfigVersion?.appFeature.count)
	}
}
