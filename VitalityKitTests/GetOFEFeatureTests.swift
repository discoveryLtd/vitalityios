//
//  GetOFEFeatureTests.swift
//  VitalityKitTests
//
//  Created by ted.philip.l.lat on 13/09/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import XCTest
import VitalityKit
import Alamofire
import VIAUtilities

class GetOFEFeatureTests: XCTestCase {
    var tenantId = 0
    var partyId = 0
    var vitalityMembershipId = 0
    var expectation: XCTestExpectation?
    var credentials = DataProvider.newRealm()
    
    override func setUp() {
        super.setUp()
        
        expectation = makeExpectation(#function)
    }
    
    func testExample() {
        
        login { (error) in
            
            self.tenantId = self.credentials.getTenantId()
            self.partyId = self.credentials.getPartyId()
            self.vitalityMembershipId = self.credentials.getMembershipId()
            
            Wire.Events.getOFEFeature(tenantId: self.tenantId, partyId: self.partyId, vitalityMembershipId: self.vitalityMembershipId) { (error) in
                
                guard error == nil else {
                    XCTFail("unexpected error of type: \(String(describing: error))")
                    return (self.expectation?.fulfill())!
                }
                
                (self.expectation?.fulfill())!
            }
            
        }
        
        waitForExpectations(timeout: expectationTimeout) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
        }
        
    }
    
}
