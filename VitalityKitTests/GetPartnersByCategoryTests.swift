//
//  GetPartnersByCategoryTests.swift
//  VitalityActive
//
//  Created by Simon Stewart on 11/1/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation

import XCTest
import Alamofire

import VitalityKit

class GetPartnersByCategoryTests: XCTestCase {
    let badTenantId = 123123
    let badPartyId = 123123
    let productFeatureCategoryTypeKey = 6 // Dependent on market
    var expectation: XCTestExpectation?
    
    override func setUp() {
        
        super.setUp()
        
        expectation = makeExpectation(#function)
    }
    
    func testGetPartnersByCategoryWithGoodParameters() {
        
        login { (error) in
            
            let request = GetPartnersByCategoryParameters(effectiveDate: Date(), productFeatureCategoryTypeKey: self.productFeatureCategoryTypeKey)
            
            Wire.Member.getPartnersByCategory(tenantId: self._goodTenantId, partyId: self._goodPartyId, request: request) { error, productFeaturePoints in
                guard error == nil else {
                    XCTFail("unexpected error of type: \(String(describing: error))")
                    return (self.expectation?.fulfill())!
                }
                
                (self.expectation?.fulfill())!
            }
        }
        
        waitForExpectations(timeout: expectationTimeout) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
        }
    }
    
    func testGetPartnersByCategoryWithBadParameters() {
        
        login { (error) in
            
            let request = GetPartnersByCategoryParameters(effectiveDate: Date(), productFeatureCategoryTypeKey: self.productFeatureCategoryTypeKey)
            
            Wire.Member.getPartnersByCategory(tenantId: self.badTenantId, partyId: self.badPartyId, request: request) { error, productFeaturePoints in
                guard error != nil else {
                    XCTFail("unexpected error of type: \(String(describing: error))")
                    return (self.expectation?.fulfill())!
                }
                
                (self.expectation?.fulfill())!
            }
        }
        
        waitForExpectations(timeout: expectationTimeout) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
        }
    }
}
