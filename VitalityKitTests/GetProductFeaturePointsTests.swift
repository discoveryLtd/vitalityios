//
//  GetProductFeaturePointsTests.swift
//  VitalityActive
//
//  Created by Simon Stewart on 10/11/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation

import XCTest
import Alamofire

import VitalityKit

class GetProductFeaturePointsTests: XCTestCase {

    let badTenantId = 123123
    let badPartyId = 123123
    var expectation: XCTestExpectation?
    
    override func setUp() {
        
        super.setUp()
        
        expectation = makeExpectation(#function)
    }
    
    func testGetProductFeaturePointsWithGoodParameters() {
        
        login { (error) in
            let request = ProductFeatureRequest(effectiveDate: Date(), productFeatureCategoryTypeKey: 7)
            Wire.Member.getProductFeaturePoints(tenantId: self._goodTenantId, partyId: self._goodPartyId, request: request) { error in
                guard error == nil else {
                    XCTFail("unexpected error of type: \(String(describing: error))")
                    return (self.expectation?.fulfill())!
                }
                
                (self.expectation?.fulfill())!
            }
        }
        
        waitForExpectations(timeout: expectationTimeout) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
        }
    }
    
    func testGetProductFeaturePointsWithBadParameters() {
        
        login { (error) in
            let request = ProductFeatureRequest(effectiveDate: Date(), productFeatureCategoryTypeKey: 7)
            Wire.Member.getProductFeaturePoints(tenantId: self.badTenantId, partyId: self.badPartyId, request: request) { error in
                guard error != nil else {
                    XCTFail("unexpected error of type: \(String(describing: error))")
                    return (self.expectation?.fulfill())!
                }
                
                (self.expectation?.fulfill())!
            }
        }
        
        waitForExpectations(timeout: expectationTimeout) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
        }
    }
}
