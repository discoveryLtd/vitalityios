//
//  GetEligibilityContentTests.swift
//  VitalityActive
//
//  Created by Simon Stewart on 11/1/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation

import XCTest
import Alamofire

import VitalityKit

class GetEligibilityContentTests: XCTestCase {
    let badTenantId = 123123
    let badPartyId = 123123
    let badMembershipId = 123123
    let goodProductFeatureKey = 91 // Dependent on market, FOR SLI 91 = Polar partner

    var expectation: XCTestExpectation?
    
    override func setUp() {
        
        super.setUp()
        
        expectation = makeExpectation(#function)
    }
    
    func testGetEligibilityContentTestsWithGoodParameters() {
        
        login { (error) in
            
            Wire.Member.getEligibilityContent(tenantId: self._goodTenantId, partyId: self._goodPartyId, membershipId: self._goodMembershipId, productFeatureKey: self.goodProductFeatureKey, completion: { error, urlString, content in
                guard error == nil else {
                    XCTFail("unexpected error of type: \(String(describing: error))")
                    return (self.expectation?.fulfill())!
                }
                
                (self.expectation?.fulfill())!
            })
        }
        
        waitForExpectations(timeout: expectationTimeout) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
        }
    }
    
    func testGetEligibilityContentTestsWithBadParameters() {
        
        login { (error) in
            
            Wire.Member.getEligibilityContent(tenantId: self.badTenantId, partyId: self.badPartyId, membershipId: self.badMembershipId, productFeatureKey: self.goodProductFeatureKey, completion: { error, urlString, content in
                guard error != nil else {
                    XCTFail("unexpected error of type: \(String(describing: error))")
                    return (self.expectation?.fulfill())!
                }
                
                (self.expectation?.fulfill())!
            })
        }
        
        waitForExpectations(timeout: expectationTimeout) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
        }
    }
}
