//
//  GetSAVPotentialPointsTests.swift
//  VitalityKitTests
//
//  Created by ted.philip.l.lat on 21/09/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import XCTest
import VitalityKit
import Alamofire
import VIAUtilities

class GetSAVPotentialPointsTests: XCTestCase {
    var expectation: XCTestExpectation?
    var credentials = DataProvider.newRealm()
    
    override func setUp() {
        super.setUp()
        
        expectation = makeExpectation(#function)
    }
   
    func testGetSAVPotentialPoints() {
        
        login { (error) in
            
            let partyId = self.credentials.getPartyId()
            let tenantId = self.credentials.getTenantId()
            let eventTypes = EventTypeRef.configuredTypesForSAV()
            let uniqueEventTypes = Array(Set(eventTypes))
            let membershipId = self.credentials.getMembershipId()
            
            Wire.Events.getSAVPotentialPointsAndEventsCompletedPoints(tenantId: tenantId, partyId: partyId, eventTypes: uniqueEventTypes, vitalityMembershipId: membershipId) { (error) in
                
                guard error == nil else {
                    XCTFail("unexpected error of type: \(String(describing: error))")
                    return (self.expectation?.fulfill())!
                }
                
                (self.expectation?.fulfill())!
            }
        }
        
        waitForExpectations(timeout: expectationTimeout) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
        }
    }
    
}
