//
//  ActivationBarcodeRouterTests.swift
//  VitalityKitTests
//
//  Created by Dexter Anthony Ambrad on 9/26/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import XCTest
import VitalityKit

class ActivationBarcodeRouterTests: XCTestCase {
    
    var expectation: XCTestExpectation?
    
    override func setUp() {
        
        super.setUp()
        
        expectation = makeExpectation(#function)
    }
    
    func testGenerateWithGoodParameters() {
        
        login { (error) in
            Wire.ActivationCode.generate(tenantId: self._goodTenantId, partyId: self._goodPartyId, useAcceptEncodingDeflateHeader: true, completion: { error, response in
                guard error == nil else {
                    XCTFail("unexpected error of type: \(String(describing: error))")
                    return (self.expectation?.fulfill())!
                }
                
                (self.expectation?.fulfill())!
            })
        }
        
        waitForExpectations(timeout: expectationTimeout) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
        }
    }
}
