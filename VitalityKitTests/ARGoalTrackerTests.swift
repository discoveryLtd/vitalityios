import XCTest

@testable import VitalityKit
@testable import VIAUtilities

class ARGoalTrackerTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        DataProvider.reset()
    }
    
    override func tearDown() {
        DataProvider.reset()
        super.tearDown()
    }
    
    func testCurrentGoalExists() {
        var dayComponent = DateComponents()
        dayComponent.day = -5;
        let theCalendar = NSCalendar.current
        let previousDate = theCalendar.date(byAdding: dayComponent, to: Date())
        setupActiveTrackerWithDates(start: previousDate!, end: Date())
        
        let arRealm = DataProvider.newARRealm()
        XCTAssertTrue(arRealm.currentGoalExists(for: Date()))
    }
    
    func testSameDayGoalTrackerComparison() {
        let startDateString = "2017-10-04"
        let endDateString = "2017-11-04"
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let startDate = formatter.date(from: startDateString)
        let endDate = formatter.date(from: endDateString)
        setupActiveTrackerWithDates(start: startDate!, end: endDate!)
        
        let arRealm = DataProvider.newARRealm()
        XCTAssertFalse(startDate!.compare(to: endDate!, granularity: .day) == endDate!.compare(to: startDate!, granularity: .day))
        XCTAssertTrue(arRealm.currentGoalExists(for: formatter.date(from: "2017-10-16")!))
        XCTAssertNotNil(arRealm.firstGoalTrackersForThisCycle(with: formatter.date(from: "2017-10-16")!))
    }
    
    func testFirstGoalTrackersForThisCycle() {
        var dayComponent = DateComponents()
        dayComponent.day = -5;
        let theCalendar = NSCalendar.current
        let previousDate = theCalendar.date(byAdding: dayComponent, to: Date())
        setupActiveTrackerWithDates(start: previousDate!, end: Date())
    }
    
    func setupActiveTrackerWithDates(start: Date, end: Date) {
        let goalTracker = ARGoalTracker()
        goalTracker.effectiveTo = end
        
       
        goalTracker.effectiveFrom = start
        goalTracker.goalKey = GoalRef.ActiveRewards
        goalTracker.goalName = "Dancing"
        goalTracker.goalTrackerStatus = GoalTrackerStatusRef.Achieved
        goalTracker.goalTrackerStatusCode = String(describing: GoalTrackerStatusRef.Achieved.rawValue)
        goalTracker.goalTrackerStatusName = "Achieved"
        goalTracker.totalObjectives = 0
        goalTracker.completedObjectives = 0
        goalTracker.id = 0
        goalTracker.percentageCompleted = 50
        goalTracker.statusChangedOn =  Date()
        goalTracker.monitorUntil = Date()
        
        
        do {
            let arRealm = DataProvider.newARRealm()
            try arRealm.write {
                arRealm.add(goalTracker)
            }
        } catch {
            XCTAssert(false, "Realm save failed")
        }
    }
}
