//
//  GetHealthInformationRouterTests.swift
//  VitalityKitTests
//
//  Created by ted.philip.l.lat on 20/09/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import XCTest
import VitalityKit
import Alamofire
import VIAUtilities

class GetHealthInformationRouterTests: XCTestCase {
    var tenantId = 0
    var partyId = 0
    var expectation: XCTestExpectation?
    var credentials = DataProvider.newRealm()
    var realm = DataProvider.newOFERealm()
    
    override func setUp() {
        super.setUp()
        
        expectation = makeExpectation(#function)
    }
    
    func testGetHealthInformation() {
        
        login { (error) in
            
            self.tenantId = self.credentials.getTenantId()
            self.partyId = self.credentials.getPartyId()
            
            Wire.MyHealth.shared().healthInformationFor(isSummary: false, tenantId: self.tenantId, partyId: self.partyId) { (response, error) in
                
                guard error == nil else {
                    XCTFail("unexpected error of type: \(String(describing: error))")
                    return (self.expectation?.fulfill())!
                }
                
                (self.expectation?.fulfill())!
                
            }
        }
        
        waitForExpectations(timeout: expectationTimeout) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
        }
    }
    
}
