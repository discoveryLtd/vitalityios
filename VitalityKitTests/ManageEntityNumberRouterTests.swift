//
//  ManageEntityNumberRouterTests.swift
//  VitalityKitTests
//
//  Created by Val Tomol on 26/09/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import XCTest
import Alamofire
import VitalityKit

class ManageEntityNumberRouterTests: XCTestCase {
    /*
     * Service only available on UKE market.
     * ntest--Carry_McColwell@mailinator.com / TestPass123 / Tenant 26
     */
    let goodPartyId = "7004629995"
    let goodDOB = "1991-10-22"
    let goodEntityNumber = "123456"
    
    var expectation: XCTestExpectation?
    
    override func setUp() {
        super.setUp()
        
        expectation = makeExpectation(#function)
    }
    
    func testAddEntityNumberRequest() {
        let request = ManageEntityNumberParams(entityNumber: goodEntityNumber,
                                               partyId: goodPartyId,
                                               dateOfBirth: goodDOB)
        
        Wire.Party.addEntityNumber(request: request) { error in
            // Note: Bypassed 201 success code since endpoint is producing an error.
            guard error != nil else {
                XCTFail("unexpected error of type: \(String(describing: error))")
                return (self.expectation?.fulfill())!
            }
            
            (self.expectation?.fulfill())!
        }
        
        waitForExpectations(timeout: expectationTimeout) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
        }
    }
    
    func testUpdateEntityNumberRequest() {
        let request = ManageEntityNumberParams(entityNumber: goodEntityNumber,
                                               partyId: goodPartyId,
                                               dateOfBirth: goodDOB)
        
        Wire.Party.updateEntityNumber(request: request) { error in
            // Note: Bypassed 200 success code since endpoint is producing an error.
            guard error != nil else {
                XCTFail("unexpected error of type: \(String(describing: error))")
                return (self.expectation?.fulfill())!
            }
            
            (self.expectation?.fulfill())!
        }
        
        waitForExpectations(timeout: expectationTimeout) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
        }
    }
    
    func testResetRealm() {
        Wire.Party.resetRealm(with: goodEntityNumber)
        
        (self.expectation?.fulfill())!
        
        waitForExpectations(timeout: expectationTimeout) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
        }
    }
}
