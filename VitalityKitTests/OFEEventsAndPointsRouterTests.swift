//
//  OFEEventsAndPointsRouterTests.swift
//  VitalityKitTests
//
//  Created by ted.philip.l.lat on 14/09/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import XCTest
import VitalityKit
import Alamofire
import VIAUtilities

class OFEEventsAndPointsRouterTests: XCTestCase {
    var tenantId = 0
    var partyId = 0
    var vitalityMembershipId = 0
    var expectation: XCTestExpectation?
    var credentials = DataProvider.newRealm()
    var realm = DataProvider.newOFERealm()
    
    override func setUp() {
        super.setUp()
        
        expectation = makeExpectation(#function)
    }
    
    func testExample() {
        login { (error) in
            
            self.tenantId = self.credentials.getTenantId()
            self.partyId = self.credentials.getPartyId()

            var eventTypes = [Int]()
            for event in self.realm.allOFEEventTypes() {
                eventTypes.append(event.eventTypeKey)
            }
            
            let effectiveTo = "9999-12-31"
            let effectiveFrom = "2018-04-23"
            
            Wire.Member.getEventsPoints(tenantId: self.tenantId, partyId: self.partyId, effectiveFrom: effectiveFrom, effectiveTo: effectiveTo, typeKeys: eventTypes) { [weak self] (result, error, useCutomError) -> Void in
                
                guard error == nil else {
                    XCTFail("unexpected error of type: \(String(describing: error))")
                    return (self!.expectation?.fulfill())!
                }
                
                (self?.expectation?.fulfill())!
            }
        }
        
        waitForExpectations(timeout: expectationTimeout) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
        }
    }
    
}
