//
//  VHRQuestionnaireProgressAndPointsTrackingTests.swift
//  VitalityActive
//
//  Created by Simon Stewart on 4/19/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import VIAUtilities
import XCTest
import Alamofire

import VitalityKit

class VHRQuestionnaireProgressAndPointsTrackingTests: XCTestCase {
    #if WIREMOCK
    let goodPartyId: Int = 100002
    let goodTenantId = 2
    let goodVitalityMembershipId: Int = 123456
    #else
    var goodPartyId: Int = 0
    var goodTenantId = 0
    var goodVitalityMembershipId: Int = 0
    
    #endif
    
    var credentials = DataProvider.newRealm()
    var expectation: XCTestExpectation?
    
    override func setUp() {
        
        super.setUp()
        
        expectation = makeExpectation(#function)
        
    }
    
    func GetParams() -> VHRQuestionnaireParameter {
        let _ : [Int] = [1]
        let param = VHRQuestionnaireParameter(setTypeKey: QuestionnaireSetRef.VHR, prePopulation: false)
        
        return param
    }
    // 1QuestionnaireProgressNPoint HealthAtri multiquestions VA-6155
    func testBasic1() {
        _testVHRQuestionnaireProgressAndPointsTracking(vitalityMembershipId: goodVitalityMembershipId)
    }
    
    func testBasic2() {
        _testVHRQuestionnaireProgressAndPointsTrackingMock(vitalityMembershipId: goodVitalityMembershipId)
    }
    
    func _testVHRQuestionnaireProgressAndPointsTracking(vitalityMembershipId: Int) {
        
        login { (error) in
            
            //gets the credentials of the account used in XCTestCase+Extension
            #if !WIREMOCK
            self.goodPartyId = self.credentials.getPartyId()
            self.goodTenantId = self.credentials.getTenantId()
            self.goodVitalityMembershipId = self.credentials.getMembershipId()
            #endif
            Wire.Member.VHRQuestionnaireProgressAndPointsTracking(tenantId: self.goodTenantId,
                                                                  partyId: self.goodPartyId,
                                                                  vitalityMembershipId: vitalityMembershipId,
                                                                  payload: self.GetParams()) { error in
                guard error == nil else {
                    XCTFail("unexpected error of type: \(String(describing: error))")
                    return (self.expectation?.fulfill())!
                }
                
                return (self.expectation?.fulfill())!
            }
        }
        
        waitForExpectations(timeout: expectationTimeout) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
        }
    }
    
    func _testVHRQuestionnaireProgressAndPointsTrackingMock(vitalityMembershipId: Int) {
        
        login { (error) in
            
            //gets the credentials of the account used in XCTestCase+Extension
            #if !WIREMOCK
            self.goodPartyId = self.credentials.getPartyId()
            self.goodTenantId = self.credentials.getTenantId()
            self.goodVitalityMembershipId = self.credentials.getMembershipId()
            #endif
            Wire.Member.VHRQuestionnaireProgressAndPointsTrackingMock(tenantId: self.goodTenantId,
                                                                  partyId: self.goodPartyId,
                                                                  vitalityMembershipId: vitalityMembershipId,
                                                                  payload: self.GetParams()) { error in
                                                                    guard error == nil else {
                                                                        XCTFail("unexpected error of type: \(String(describing: error))")
                                                                        return (self.expectation?.fulfill())!
                                                                    }
                                                                    
                                                                    return (self.expectation?.fulfill())!
            }
        }
        
        waitForExpectations(timeout: expectationTimeout) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
        }
    }
}
