//
//  LoginTests.swift
//  VitalityActive
//
//  Created by Simon Stewart on 1/25/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

import XCTest
import VitalityKit
import Alamofire
import VIAUtilities

class LoginTests: XCTestCase {
#if WIREMOCK
    let goodUserName = "ntest--Corrie_McBedford@mailinator.com"
    let goodUserNameForPassword = "TestPass123"
    let badUserName = "incorrect@email.com"
    let goodPassword = "Password1"
    let badPassword = "incorrectpassword"
#else
    let goodUserName = "test--Darline.Darling@mailinator.com"
    let goodUserNameForPassword = "ntest--Corrie_McBedford@mailinator.com"
    let badUserName = "test--incorrect@email.com"
    let goodPassword = "TestPass123"
    let badPassword = "incorrectpassword"
#endif
}

extension LoginTests {
    override func setUp() {
//        #if !WIREMOCK
//            XCTFail("Don't run unit tests against LIVE")
//        #endif
    }
    func testLoginWithPassingCredentials() {

        let expectation = XCTestExpectation(description: "Test Successfull Login")
        
        Wire.Member.login(email: goodUserName, password: goodPassword, completion: { error in

            guard error.0 == nil else {
                XCTFail("unexpected error of type: \(String(describing: error))")
                return expectation.fulfill()
            }

            return expectation.fulfill()
        })
        waitForExpectations(timeout: expectationTimeout) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
        }
    }

    func testLoginWithMissingCredentials() {

        let expect = makeExpectation(#function)

        Wire.Member.login(email: "", password: "", completion: { error in

            guard error.0 is LoginError, (error.0 as! LoginError) == .missingEmailOrPassword else { // error expected
                XCTFail("unexpected error of type: \(String(describing: error))")
                return expect.fulfill()
            }

            return expect.fulfill()

        })
        waitForExpectations(timeout: expectationTimeout) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
        }
    }
    func testLoginWithMissingUserNameCredential() {

        let expect = makeExpectation(#function)

        Wire.Member.login(email: "", password: goodPassword, completion: { error in

            guard error.0 is LoginError, (error.0 as! LoginError) == .missingEmailOrPassword else {
                XCTFail("unexpected error of type: \(String(describing: error))")
                return expect.fulfill()
            }

            return expect.fulfill()

        })
        waitForExpectations(timeout: expectationTimeout) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
        }
    }
    func testLoginWithMissingPasswordCredential() {

        let expect = makeExpectation(#function)

        Wire.Member.login(email: goodUserName, password: "", completion: { error in

            guard error.0 is LoginError, (error.0 as! LoginError) == .missingEmailOrPassword else {
                XCTFail("unexpected error of type: \(String(describing: error))")
                return expect.fulfill()
            }

            return expect.fulfill()

        })
        waitForExpectations(timeout: expectationTimeout) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
        }
    }
    func testLoginWithBadCredentials() {

        let expect = makeExpectation(#function)

        Wire.Member.login(email: badUserName, password: badPassword, completion: { error in

            guard error.0 is LoginError, (error.0 as! LoginError) == .incorrectEmailOrPassword else {
                XCTFail("unexpected error of type: \(String(describing: error))")
                return expect.fulfill()
            }

            return expect.fulfill()

        })
        waitForExpectations(timeout: expectationTimeout) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
        }

    }
    // TODO : fails
    func testLoginWithBadPasswordCredential() {

        let expect = makeExpectation(#function)

        Wire.Member.login(email: "santi@test.com", password: "incorrectpassword", completion: { error in
            
            guard error.0 is LoginError, (error.0 as! LoginError) == .incorrectEmailOrPassword else {
                XCTFail("unexpected error of type: \(String(describing: error))")
                return expect.fulfill()
            }

            return expect.fulfill()

        })
        waitForExpectations(timeout: expectationTimeout) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
        }
    }
}

// TODO: currently failing, but not being used.  Fix deferred until API is working
extension LoginTests {

    func testForgotPasswordWithGoodUserName() {

        let expect = makeExpectation(#function)

        Wire.Member.forgotPassword(email: goodUserNameForPassword, completion: { error in

            guard error == nil else {
                XCTFail("unexpected error of type: \(String(describing: error))")
                return expect.fulfill()
            }

            return expect.fulfill()

        })
        waitForExpectations(timeout: expectationTimeout) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
        }
    }
    func testForgotPasswordWithBadUserName() {

        let expect = makeExpectation(#function)

        Wire.Member.forgotPassword(email: badUserName, completion: { error in

            guard error != nil else {
                XCTFail("unexpected error of type: \(String(describing: error))")
                return expect.fulfill()
            }

            return expect.fulfill()

        })
        waitForExpectations(timeout: expectationTimeout) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
        }
    }
}
