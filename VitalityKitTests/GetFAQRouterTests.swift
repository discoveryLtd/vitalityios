//
//  GetFAQRouterTests.swift
//  VitalityKitTests
//
//  Created by ted.philip.l.lat on 14/09/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import XCTest
import VitalityKit
import Alamofire
import VIAUtilities

class GetFAQRouterTests: XCTestCase {
    
    var tenantId = 0
    var partyId = 0
    var vitalityMembershipId = 0
    var expectation: XCTestExpectation?
    var credentials = DataProvider.newRealm()
    
    override func setUp() {
        super.setUp()
        
        expectation = makeExpectation(#function)
    }
    
    
    func testGetFAQByTagName() {
        login { (error) in
            
            let tagName = "help"
            
            Wire.Content.getFAQByTagName(tagName: tagName) { error, data, result in
                guard error == nil else {
                    XCTFail("unexpected error of type: \(String(describing: error))")
                    return (self.expectation?.fulfill())!
                }
                
                (self.expectation?.fulfill())!
            }
            
        }
        
        waitForExpectations(timeout: expectationTimeout) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
        }
    }
    
    func testGetFAQByTagNameAndKeyword() {
        login { (error) in
            
            let tagName = "help"
            let keyword = "help"
            
            Wire.Content.getFAQByTagNameAndKeyword(tagName: tagName, keyword: keyword) { error, data, result in
                guard error == nil else {
                    XCTFail("unexpected error of type: \(String(describing: error))")
                    return (self.expectation?.fulfill())!
                }
                
                (self.expectation?.fulfill())!
            }
            
        }
        
        waitForExpectations(timeout: expectationTimeout) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
        }
    }
    
}
