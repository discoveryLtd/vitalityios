//
//  GetApplicationConfigurationTests.swift
//  VitalityActive
//
//  Created by Simon Stewart on 4/6/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

import XCTest
import VitalityKit
import Alamofire
import VIAUtilities

class GetApplicationConfigurationTests: XCTestCase {
    #if WIREMOCK
    let tenantId = 2
    #else
    var tenantId: Int = 0
    #endif

    var credentials = DataProvider.newRealm()
    var expectation: XCTestExpectation?
    
    override func setUp() {
        
        super.setUp()
        
        expectation = makeExpectation(#function)
    }
    
    func test1() {

        login { (error) in
        #if !WIREMOCK
        self.tenantId = self.credentials.getTenantId()
        #endif
        Wire.Member.getApplicationConfiguration(tenantId: self.tenantId, completion: { (error) in

            guard error == nil else {
                XCTFail("unexpected error of type: \(String(describing: error))")
                return (self.expectation?.fulfill())!
            }

            (self.expectation?.fulfill())!
        })
        }
        
        waitForExpectations(timeout: expectationTimeout) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
        }
    }
}
