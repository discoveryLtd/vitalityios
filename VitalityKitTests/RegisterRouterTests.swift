//
//  RegisterRouterTests.swift
//  VitalityActive
//
//  Created by Wilmar van Heerden on 2016/11/02.
//  Copyright © 2016 Glucode. All rights reserved.
//

import XCTest
@testable import VitalityKit

class RegisterRouterTests: XCTestCase {

    override func setUp() {
        super.setUp()
    }

    override func tearDown() {
        super.tearDown()
    }

    func testRegisterRouteIsCorrect() {
        let urlRequest = RegisterRouter.register(email: "", password: "", registrationCode: "").urlRequest
        guard (urlRequest?.url!) != nil else {
            XCTFail()
            return
        }
        let components = URLComponents(url: (urlRequest?.url)!, resolvingAgainstBaseURL: true)!
        let baseURL = try! Wire.default.baseURL()
        let expectedComponents = URLComponents(url: baseURL, resolvingAgainstBaseURL: true)
        XCTAssert(components.scheme == expectedComponents?.scheme, "Expected \(String(describing: expectedComponents?.scheme!)), got \(components.scheme!)")
        XCTAssert(components.host == expectedComponents?.host, "Expected \(String(describing: expectedComponents?.host!)), got \(components.host!)")
    }
}
