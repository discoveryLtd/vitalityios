//
//  CMSTests.swift
//  VitalityActive
//
//  Created by Simon Stewart on 2/9/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

import XCTest
import Alamofire

import VitalityKit

class CMSTests: XCTestCase {
    let goodFileName = "wda_activity_mapping.json"
    let goodGroupId = AppConfigFeature.insurerCMSGroupId() ?? ""
    let goodArticleId = "vhr-disclaimer"
    let goodReferenceId = 561548
    
    let badFileName = "badFileName"
    let badpartyId = 123123
    let badGroupId = "badGroupId"
    let badArticleId = "badArticleId"
    let badReferenceId = 123123
    let badMembershipNumber = "123123"
    
    var expectation: XCTestExpectation?
    
    override func setUp() {
        
        super.setUp()
        
        expectation = makeExpectation(#function)
    }
    
    func testGetArticleByURLTitleWithGoodParameters() {
        login { (error) in
            
            Wire.Content.getArticleByURLTitle(urlTitle: self.goodArticleId, groupId: self.goodGroupId) { (content, error) in
                guard error == nil else {
                    XCTFail("unexpected error of type: \(String(describing: error))")
                    return (self.expectation?.fulfill())!
                }
                
                (self.expectation?.fulfill())!
            }
        }
        
        waitForExpectations(timeout: expectationTimeout) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
        }
    }
    
    func testGetArticleByURLTitleWithBadParameters() {
        login { (error) in
            
            Wire.Content.getArticleByURLTitle(urlTitle: self.badArticleId, groupId: self.badGroupId) { (content, error) in
                // should fail
                guard error != nil else {
                    XCTFail("unexpected error of type: \(String(describing: error))")
                    return (self.expectation?.fulfill())!
                }
                
                (self.expectation?.fulfill())!
            }
        }
        
        waitForExpectations(timeout: expectationTimeout) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
        }
    }
    
    func testGetFileByGroupIdWithGoodParameters() {
        login { (error) in
        let modifiedSince: Date? = Date()
            
        Wire.Content.getFileByGroupId(fileName: self.goodFileName, groupId: self.goodGroupId, modifiedSince: modifiedSince) { (data, error) in
            guard error == nil else {
                XCTFail("unexpected error of type: \(String(describing: error))")
                return (self.expectation?.fulfill())!
            }

            (self.expectation?.fulfill())!
        }
        }

        waitForExpectations(timeout: expectationTimeout) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
        }
    }
    func testGetFileByGroupIdWithBadParameters() {
        login { (error) in
            let modifiedSince: Date? = Date()
            Wire.Content.getFileByGroupId(fileName: self.badFileName, groupId: self.badGroupId, modifiedSince: modifiedSince) { (data, error) in
                // should fail
                guard error != nil else {
                    XCTFail("unexpected error of type: \(String(describing: error))")
                    return (self.expectation?.fulfill())!
                }

                (self.expectation?.fulfill())!
            }
        }

        waitForExpectations(timeout: expectationTimeout) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
        }
    }
    
    func testGetFileByReferenceWithGoodParameters() {
        login { (error) in
            Wire.Content.getFileByReferenceId(referenceId: self.goodReferenceId)
            { (context, content, error) in
                guard error == nil else {
                    XCTFail("unexpected error of type: \(String(describing: error))")
                    return (self.expectation?.fulfill())!
                }
                
                (self.expectation?.fulfill())!
            }
        }
        
        waitForExpectations(timeout: expectationTimeout) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
        }
    }
    
    func testGetFileByReferenceWithBadParameters() {
        login { (error) in
            Wire.Content.getFileByReferenceId(referenceId: self.badReferenceId)
            { (context, content, error) in
                // should fail
                guard error != nil else {
                    XCTFail("unexpected error of type: \(String(describing: error))")
                    return (self.expectation?.fulfill())!
                }
                
                (self.expectation?.fulfill())!
            }
        }
        
        waitForExpectations(timeout: expectationTimeout) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
        }
    }
    
    func testPostFeedbackEmailServiceWithGoodParameters() {
        login { (error) in
            
            var feedbacks = [Feedback]()
            var datas = [Data]()
//            let feedback = Feedback(feedbackType: "Technical Assistance", feedbackTypeId: "TECHNICAL_ASSISTANCE", feedbackTimestamp: Date(), feedbackMessage: "Sample Feedback", memberPartyId: String(self.goodpartyId), memberFirstName: "Corrie", memberLastName: "McBedford", contactNumber: "", membershipNumber: "59049883", memberEmailAddress: "Corrie_McBedford@mailinator.com", channelName: "Mobile", deviceDetails: "Simulator", osDetails: "11.4", appVersion: "1.0.0")
            let data = "This is my string.This is my string.This is my string.This is my string.".data(using: .utf8)!
            datas.append(data)
            let feedback = Feedback(feedbackType: "Technical Assistance", feedbackTypeId: "TECHNICAL_ASSISTANCE", feedbackTimestamp: Date(), feedbackMessage: "Sample Feedback", memberPartyId: "1001212620", memberFirstName: "Dionna", memberLastName: "McBusch", contactNumber: "", membershipNumber: "1000174495", memberEmailAddress: "Dionna_McBusch@mailinator.com", channelName: "Mobile", deviceDetails: "Simulator", osDetails: "11.4", appVersion: "1.0.0")
            
            feedbacks.append(feedback)
            
            Wire.Content.postSendFeedbackEmail(feedback: feedbacks, imageData: datas) { (error) in
                guard error == nil else {
                    XCTFail("unexpected error of type: \(String(describing: error))")
                    return (self.expectation?.fulfill())!
                }
                
                (self.expectation?.fulfill())!
            }
        }
        
        waitForExpectations(timeout: expectationTimeout) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
        }
    }
    
    func testGetFileByPartyIdWithGoodParameters() {
        login { (error) in
            Wire.Content.getFileByPartyId(partyId: self._goodPartyId)
            { (content, error) in
                guard error == nil else {
                    XCTFail("unexpected error of type: \(String(describing: error))")
                    return (self.expectation?.fulfill())!
                }
                
                (self.expectation?.fulfill())!
            }
        }
        
        waitForExpectations(timeout: expectationTimeout) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
        }
    }
    
    func testGetFileByPartyIdWithBadParameters() {
        login { (error) in
            Wire.Content.getFileByPartyId(partyId: self.badpartyId)
            { (content, error) in
                // should fail
                guard error != nil else {
                    XCTFail("unexpected error of type: \(String(describing: error))")
                    return (self.expectation?.fulfill())!
                }
                
                (self.expectation?.fulfill())!
            }
        }
        
        waitForExpectations(timeout: expectationTimeout) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
        }
    }
}

extension CMSTests {
    func testUploadWithGoodParameters() {
        // https://gist.github.com/janporu-san/e832cdee51974fc55660

        login { (error) in

        let data = "This is my string.This is my string.This is my string.This is my string.".data(using: .utf8)!
        
        Wire.Content.uploadFile(partyId: self._goodPartyId, fileContents: data) { (fileUploadRef, error) in
            guard error == nil else {
                XCTFail("unexpected error of type: \(String(describing: error))")
                return (self.expectation?.fulfill())!
            }

            (self.expectation?.fulfill())!
        }
        }

        waitForExpectations(timeout: expectationTimeout) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
        }
    }

}
