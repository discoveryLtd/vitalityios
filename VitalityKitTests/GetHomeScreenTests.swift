//
//  GetHomeScreenTests.swift
//  VitalityActive
//
//  Created by Simon Stewart on 2/23/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation

import XCTest
import Alamofire

import VitalityKit

class GetHomeScreenTests: XCTestCase {
    #if WIREMOCK
    let goodPartyId: Int = 100002
    let goodTenantId = 2
    let goodVitalityMembershipId: Int = 123456
    #else
    var goodPartyId: Int = 0
    var goodTenantId: Int = 0
    var goodVitalityMembershipId: Int = 0
    #endif

    var expectation: XCTestExpectation?
    var credentials = DataProvider.newRealm()
    
    override func setUp() {
        
        super.setUp()
        
        expectation = makeExpectation(#function)
    }
    
    func testGetHomeScreenWithGoodParameters() {
        
        login { (error) in
            
        //gets the credentials of the account used in XCTestCase+Extension
        #if !WIREMOCK
        self.goodPartyId = self.credentials.getPartyId()
        self.goodTenantId = self.credentials.getTenantId()
        self.goodVitalityMembershipId = self.credentials.getMembershipId()
        #endif
        
        Wire.Content.getHomeScreen(tenantId: self.goodTenantId, partyId: self.goodPartyId, vitalityMembershipId: self.goodVitalityMembershipId) { error in
            guard error == nil else {
                XCTFail("unexpected error of type: \(String(describing: error))")
                return (self.expectation?.fulfill())!
            }

            (self.expectation?.fulfill())!
        }
        }

        waitForExpectations(timeout: expectationTimeout) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
        }
    }
}
