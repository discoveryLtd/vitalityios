import XCTest

import RealmSwift
@testable import VitalityKit

class VHCHealthAttributeGroupTests: XCTestCase {
    
    let realm = DataProvider.newVHCRealm()
    
    override func setUp() {
        super.setUp()

        try! realm.write {
            realm.deleteAll()
        }
    }

    override func tearDown() {
        super.tearDown()
    }

    // MARK: - Tests

    func testVHCHealthAttributeBMI() {
        let response = setupBMIResponse()

        VHCEvent.persistGetPotentialPoints(response: response)

        let object = realm.objects(VHCHealthAttributeGroup.self).filter({ $0.type == .VHCBMI })
        if object.count > 1 {
            XCTAssertTrue(false)
        }
        if let group = object.first {
            XCTAssertNotNil(group)
            XCTAssertEqual(3, group.healthAttributes.count)
            XCTAssertEqual(0, group.type.rawValue)
        } else {
            XCTAssertTrue(false)
        }
    }

    func testVHCHealthAttributeWaistCircumference() {
        let response = setupWaistCircumferenceResponse()

        VHCEvent.persistGetPotentialPoints(response: response)

        let object = realm.objects(VHCHealthAttributeGroup.self).filter({ $0.type == .VHCWaistCircum })
        if object.count > 1 {
            XCTAssertTrue(false)
        }
        if let group = object.first {
            XCTAssertNotNil(group)
            XCTAssertEqual(1, group.healthAttributes.count)
            XCTAssertEqual(1, group.type.rawValue)
        } else {
            XCTAssertTrue(false)
        }
    }

    func testVHCHealthAttributeBloodGlucose() {
        let response = setupBloodGlucoseResponse()

        VHCEvent.persistGetPotentialPoints(response: response)

        let object = realm.objects(VHCHealthAttributeGroup.self).filter({ $0.type == .VHCBloodGlucose })
        if object.count > 1 {
            XCTAssertTrue(false)
        }
        if let group = object.first {
            XCTAssertNotNil(group)
            XCTAssertEqual(2, group.healthAttributes.count)
            XCTAssertEqual(2, group.type.rawValue)
        } else {
            XCTAssertTrue(false)
        }
    }

    func testVHCHealthAttributeBloodPressure() {
        let response = setupBloodPressureResponse()

        VHCEvent.persistGetPotentialPoints(response: response)

        let object = realm.objects(VHCHealthAttributeGroup.self).filter({ $0.type == .VHCBloodPressure })
        if object.count > 1 {
            XCTAssertTrue(false)
        }
        if let group = object.first {
            XCTAssertNotNil(group)
            XCTAssertEqual(2, group.healthAttributes.count)
            XCTAssertEqual(3, group.type.rawValue)
        } else {
            XCTAssertTrue(false)
        }
    }

    func testVHCHealthAttributeCholesterol() {
        let response = setupCholesterolResponse()

        VHCEvent.persistGetPotentialPoints(response: response)

        let object = realm.objects(VHCHealthAttributeGroup.self).filter({ $0.type == .VHCCholesterol })
        if object.count > 1 {
            XCTAssertTrue(false)
        }
        if let group = object.first {
            XCTAssertNotNil(group)
            XCTAssertEqual(4, group.healthAttributes.count)
            XCTAssertEqual(4, group.type.rawValue)
        } else {
            XCTAssertTrue(false)
        }
    }

    func testVHCHealthHba1c() {
        let response = setupHba1cResponse()
        
        VHCEvent.persistGetPotentialPoints(response: response)
        
        let object = realm.objects(VHCHealthAttributeGroup.self).filter({ $0.type == .VHCHbA1c })
        if object.count > 1 {
            XCTAssertTrue(false)
        }
        if let group = object.first {
            XCTAssertNotNil(group)
            XCTAssertEqual(1, group.healthAttributes.count)
            XCTAssertEqual(5, group.type.rawValue)
        } else {
            XCTAssertTrue(false)
        }
    }

   // MARK: - Setup helpers

    func setupBMIResponse() -> GetVHCPotentialPointsResponse {
        let heathAttribute1 = GPPEHealthAttribute.init(healthAttributeTypeValidValueses: [], typeCode: "BMI", typeName: "The BMI measurement of the member", typekey: 1)
        let heathAttribute2 = GPPEHealthAttribute.init(healthAttributeTypeValidValueses: [], typeCode: "Height", typeName: "The height measurement of the member", typekey: 2)
        let heathAttribute3 = GPPEHealthAttribute.init(healthAttributeTypeValidValueses: [], typeCode: "Weight", typeName: "The weight measurement of the member", typekey: 3)

        
        let potentialPointsEventType = GPPEEventType.init(categoryCode: "Assessment", categoryKey: 1, categoryName: "Assessment", event: [],
                                                          healthAttribute: [heathAttribute1, heathAttribute2, heathAttribute3], reasonCode: "",
                                                          reasonKey: 0, reasonName: "", totalEarnedPoints: 100,
                                                          totalPotentialPoints: 200, typeCode: "WeightCaptured", typeKey: 88,
                                                          typeName: "Weight Captured")

        return GetVHCPotentialPointsResponse.init(eventType: [potentialPointsEventType], warnings: nil)
    }

    func setupWaistCircumferenceResponse() -> GetVHCPotentialPointsResponse {
        let heathAttribute1 = GPPEHealthAttribute.init(healthAttributeTypeValidValueses: [], typeCode: "WaistCircumerence", typeName: "The waist circumference measurement of the member", typekey: 4)
        let potentialPointsEventType = GPPEEventType.init(categoryCode: "Assessment",categoryKey: 1,categoryName: "Assessment", event: [],
                                                          healthAttribute: [heathAttribute1], reasonCode: "",
                                                          reasonKey: 0, reasonName: "", totalEarnedPoints: 100,
                                                          totalPotentialPoints: 200, typeCode: "WaistCircum", typeKey: 89,
                                                          typeName: "Waist Circumference")

        return GetVHCPotentialPointsResponse.init(eventType: [potentialPointsEventType], warnings: nil)
    }

    func setupBloodGlucoseResponse() -> GetVHCPotentialPointsResponse {
        let heathAttribute1 = GPPEHealthAttribute.init(healthAttributeTypeValidValueses: [], typeCode: "FastingGlucose", typeName: "The fasting glucose measurement of the member", typekey: 8)
        let heathAttribute2 = GPPEHealthAttribute.init(healthAttributeTypeValidValueses: [], typeCode: "RandomGlucose", typeName: "The random glucose measurement of the member", typekey: 9)
        let potentialPointsEventType = GPPEEventType.init(categoryCode: "Assessment", categoryKey: 1, categoryName: "Assessment", event: [],
                                                          healthAttribute: [heathAttribute1, heathAttribute2], reasonCode: "",
                                                          reasonKey: 0, reasonName: "", totalEarnedPoints: 100,
                                                          totalPotentialPoints: 200, typeCode: "FastingGlucose", typeKey: 90,
                                                          typeName: "Fasting Glucose")

        return GetVHCPotentialPointsResponse.init(eventType: [potentialPointsEventType], warnings: nil)
    }

    func setupBloodPressureResponse() -> GetVHCPotentialPointsResponse {
        let heathAttribute1 = GPPEHealthAttribute.init(healthAttributeTypeValidValueses: [], typeCode: "SystolicBP", typeName: "The systolic blood pressure measurement of the member", typekey: 5)
        let heathAttribute2 = GPPEHealthAttribute.init(healthAttributeTypeValidValueses: [], typeCode: "DiastolicBP", typeName: "The diastolic blood pressure measurement of the member", typekey: 6)
        let potentialPointsEventType = GPPEEventType.init(categoryCode: "Assessment", categoryKey: 1, categoryName: "Assessment", event: [],
                                                          healthAttribute: [heathAttribute1, heathAttribute2], reasonCode: "",
                                                          reasonKey: 0, reasonName: "", totalEarnedPoints: 100,
                                                          totalPotentialPoints: 200, typeCode: "SystolicBP", typeKey: 92,
                                                          typeName: "Systolic Blood Pressure")

        return GetVHCPotentialPointsResponse.init(eventType: [potentialPointsEventType], warnings: nil)
    }

    func setupCholesterolResponse() -> GetVHCPotentialPointsResponse {
        let heathAttribute1 = GPPEHealthAttribute.init(healthAttributeTypeValidValueses: [], typeCode: "TotalCholesterol", typeName: "The total cholesterol measurement of the member", typekey: 7)
        let heathAttribute2 = GPPEHealthAttribute.init(healthAttributeTypeValidValueses: [], typeCode: "HDLCholesterol", typeName: "The HDL cholesterol measurement of the member", typekey: 11)
        let heathAttribute3 = GPPEHealthAttribute.init(healthAttributeTypeValidValueses: [], typeCode: "LDLCholesterol", typeName: "The LDL cholesterol measurement of the member", typekey: 12)
        let heathAttribute4 = GPPEHealthAttribute.init(healthAttributeTypeValidValueses: [], typeCode: "Triglycerides", typeName: "The TriGLy cholesterol measurement of the member", typekey: 13)
        let potentialPointsEventType = GPPEEventType.init(categoryCode: "Assessment", categoryKey: 1, categoryName: "Assessment", event: [],
                                                          healthAttribute: [heathAttribute1, heathAttribute2, heathAttribute3, heathAttribute4], reasonCode: "",
                                                          reasonKey: 0, reasonName: "", totalEarnedPoints: 100,
                                                           totalPotentialPoints: 200, typeCode: "TotalCholesterol", typeKey: 94,
                                                          typeName: "Total Cholesterol")

        return GetVHCPotentialPointsResponse.init(eventType: [potentialPointsEventType], warnings: nil)
    }

    func setupHba1cResponse() -> GetVHCPotentialPointsResponse {
        let heathAttribute1 = GPPEHealthAttribute.init(healthAttributeTypeValidValueses: [], typeCode: "HbA1c", typeName: "The HbA1c measurement of the member", typekey: 10)
        
        let potentialPointsEventType = GPPEEventType.init(categoryCode: "Assessment", categoryKey: 1, categoryName: "Assessment",event: [],
                                                          healthAttribute: [heathAttribute1], reasonCode: "",
                                                          reasonKey: 0, reasonName: "", totalEarnedPoints: 100,
                                                          totalPotentialPoints: 200, typeCode: "HbA1c", typeKey: 98,
                                                          typeName: "HbA1c")
        
        return GetVHCPotentialPointsResponse.init(eventType: [potentialPointsEventType], warnings: nil)
    }

}
