import Foundation
import XCTest
import VitalityKit
import VIAUtilities

class HAReportedHealthAttributeTest: XCTestCase {
    
    func test_ItShouldReturnACollectionOfHAReportedHealthAttributesWhenGivenACaptureAssessmentResponse() {
        let healthAttribute = HAReportedHealthAttribute.buildHealthAttributes(response: CaptureAssessmentResponse(json: [
            "healthAttributeMetadatas" : [[
                "healthAttributeFeedbacks" : [[
                    "feedbackTypeCode" : "VAgeAbove",
                    "feedbackTypeKey" : 36,
                    "feedbackTypeName" : "Too High",
                    "feedbackTypeTypeCode" : "Observation",
                    "feedbackTypeTypeKey" : 3,
                    "feedbackTypeTypeName" : "Feedback that is giving an observation"
                ]],
                "measurementUnitId" : -1,
                "typeCode" : "VitalityAge",
                "typeKey" : PartyAttributeTypeRef.VitalityAge.rawValue,
                "typeName" : "VitalityAge",
                "value" : "29"
            ]]
        ]))!.first(where: { (healthAttribute) -> Bool in return healthAttribute.attributeType == PartyAttributeTypeRef.VitalityAge})

        XCTAssertEqual(PartyAttributeTypeRef.VitalityAge, healthAttribute!.attributeType)
        XCTAssertEqual("VitalityAge", healthAttribute!.typeCode)
        XCTAssertEqual("VitalityAge", healthAttribute!.typeName)
        XCTAssertEqual("29", healthAttribute!.value)
        XCTAssertEqual(-1, healthAttribute!.measurementUnitId)
    }
    
    func test_ItShouldReturnAnUpdatedCollection() {
        var json = [
            "healthAttributeMetadatas" : [[
                "healthAttributeFeedbacks" : [[
                    "feedbackTypeCode" : "VAgeAbove",
                    "feedbackTypeKey" : 36,
                    "feedbackTypeName" : "Too High",
                    "feedbackTypeTypeCode" : "Observation",
                    "feedbackTypeTypeKey" : 3,
                    "feedbackTypeTypeName" : "Feedback that is giving an observation"
                ]],
                "measurementUnitId" : -1,
                "typeCode" : "VitalityAge",
                "typeKey" : PartyAttributeTypeRef.VitalityAge.rawValue,
                "typeName" : "VitalityAge",
                "value" : "29"
            ]]
        ]
        _ = HAReportedHealthAttribute.buildHealthAttributes(response: CaptureAssessmentResponse(json: json))!.first(where: { (healthAttribute) -> Bool in return healthAttribute.attributeType == PartyAttributeTypeRef.VitalityAge})
        json["healthAttributeMetadatas"]![0]["value"] = "31"
        let healthAttribute = HAReportedHealthAttribute.buildHealthAttributes(response: CaptureAssessmentResponse(json: json))!.first(where: { (healthAttribute) -> Bool in return healthAttribute.attributeType == PartyAttributeTypeRef.VitalityAge})

        XCTAssertEqual(PartyAttributeTypeRef.VitalityAge, healthAttribute!.attributeType)
        XCTAssertEqual("VitalityAge", healthAttribute!.typeCode)
        XCTAssertEqual("VitalityAge", healthAttribute!.typeName)
        XCTAssertEqual("31", healthAttribute!.value)
        XCTAssertEqual(-1, healthAttribute!.measurementUnitId)
    }

    func test_buildHealthAttributes_shouldReturnACollectionOfAttributesWhenGivenAGetHealthAttributeFeedbackResponse() {
        let healthAttribute = HAReportedHealthAttribute.buildHealthAttributes(response: GetHealthAttributeFeedbackResponse(json: [
            "healthAttribute" : [[
                "attributeTypeCode" : "VitalityAge",
                "attributeTypeKey" : PartyAttributeTypeRef.VitalityAge.rawValue,
                "attributeTypeName" : "VitalityAge",
                "healthAttributeFeedbacks" : [[
                    "feedbackTypeCode" : "VAgeAbove",
                    "feedbackTypeKey" : 36,
                    "feedbackTypeName" : "Too High",
                    "feedbackTypeTypeCode" : "Observation",
                    "feedbackTypeTypeKey" : 3,
                    "feedbackTypeTypeName" : "Feedback that is giving an observation",
                ]],
                "sourceEventId" : -1,
                "measuredOn" : "2017-08-30",
                "unitofMeasure" : "-1",
                "value" : "29"
            ]]
        ]))!.first(where: { (healthAttribute) -> Bool in return healthAttribute.attributeType == PartyAttributeTypeRef.VitalityAge})

        XCTAssertEqual(PartyAttributeTypeRef.VitalityAge, healthAttribute!.attributeType)
        XCTAssertEqual("VitalityAge", healthAttribute!.typeCode)
        XCTAssertEqual("VitalityAge", healthAttribute!.typeName)
        XCTAssertEqual("29", healthAttribute!.value)
        XCTAssertEqual(-1, healthAttribute!.measurementUnitId)
    }
}
