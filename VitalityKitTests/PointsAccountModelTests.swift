//
//  PointsAccountModelTests.swift
//  VitalityActive
//
//  Created by Lionel Camacho on 2017/02/03.
//  Copyright © 2017 Glucode. All rights reserved.
//

import XCTest
import VitalityKit
import RealmSwift

class PointsAccountModelTests: XCTestCase {

    var metadata = [PointsEntryMetadata]()
    var reason = [PointsEntryReason]()
    var progressTrackerEvent = [ActiveRewardsProgressTrackerEvent]()
    var pointsEntry = PointsEntry()
    var anotherPointsEntry = PointsEntry()
    var otherPointsEntry = PointsEntry()
    var lastPointsEntry = PointsEntry()
    var pointsAccount = PointsAccount()

    let newServiceDateTimeFormatter = DateFormatter.apiManagerFormatter()

    override func setUp() {
        super.setUp()

        // Use an in-memory Realm identified by the name of the current test.
        // This ensures that each test can't accidentally access or modify the data
        // from other tests or the application itself, and because they're in-memory,
        // there's nothing that needs to be cleaned up.
        //Realm.Configuration.defaultConfiguration.inMemoryIdentifier = self.name

        metadata = [PointsEntryMetadata(value: ["typeCode": "MK123", "typeKey": 123, "unitOfMeasure": "CM", "value": "1200"])]
        reason = [PointsEntryReason(value: ["categoryCode": "FR274", "categoryKey": 274, "reason": "Awesome Reason"])]
        progressTrackerEvent = [ActiveRewardsProgressTrackerEvent(value: ["goalTypeCode": "GTC233", "goalTypeKey": 233, "points": 250])]

        pointsEntry = PointsEntry(value: ["category": "Vitality Active", "earnedValue": 100, "effectiveDate": Date(), "eventId": 123, "id": 1, "metadatas": metadata, "party": 456, "potentialValue": 150, "prelimitValue": 100, "progressTrackerEvents": progressTrackerEvent, "reasons": reason, "typeCode": "TC32", "typeKey": 32])
        pointsEntry.effectiveDate = newServiceDateTimeFormatter.date(from: "2014-12-30T01:00:00+02:00[Africa/Johannesburg]")!

        anotherPointsEntry = PointsEntry(value: ["category": "Vitality Active", "earnedValue": 100, "effectiveDate": Date(), "eventId": 123, "id": 1, "metadatas": metadata, "party": 456, "potentialValue": 150, "prelimitValue": 100, "progressTrackerEvents": progressTrackerEvent, "reasons": reason, "typeCode": "TC32", "typeKey": 32])
        anotherPointsEntry.effectiveDate = newServiceDateTimeFormatter.date(from: "2014-12-12T02:00:00+02:00[Africa/Johannesburg]")!

        otherPointsEntry = PointsEntry(value: ["category": "Mochachos", "earnedValue": 2500, "effectiveDate": Date(), "eventId": 125, "id": 2, "metadatas": metadata, "party": 234, "potentialValue": 2500, "prelimitValue": 2400, "progressTrackerEvents": progressTrackerEvent, "reasons": reason, "typeCode": "TC23", "typeKey": 23])
        otherPointsEntry.effectiveDate = newServiceDateTimeFormatter.date(from: "2017-12-30T11:00:00+02:00[Africa/Johannesburg]")!

        lastPointsEntry = PointsEntry(value: ["category": "Vitality Active", "earnedValue": 500, "effectiveDate": Date(), "eventId": 133, "id": 3, "metadatas": metadata, "party": 455, "potentialValue": 500, "prelimitValue": 500, "progressTrackerEvents": progressTrackerEvent, "reasons": reason, "typeCode": "TC6768", "typeKey": 6768])
        lastPointsEntry.effectiveDate = newServiceDateTimeFormatter.date(from: "2012-12-30T13:00:00+02:00[Africa/Johannesburg]")!

        pointsAccount = PointsAccount(value: ["carryOverPoints": 0, "effectiveFrom": Date(), "effectiveTo": Date(), "pointsTotal": 2500, "pointsEntries": [pointsEntry, anotherPointsEntry, otherPointsEntry, lastPointsEntry] ])
        pointsAccount.effectiveFrom = newServiceDateTimeFormatter.date(from: "2017-12-30T11:30:00+02:00[Africa/Johannesburg]")
        pointsAccount.effectiveTo = newServiceDateTimeFormatter.date(from: "2018-12-30T12:30:00+02:00[Africa/Johannesburg]")
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testPointsAccountFilterPointsEntriesByCategory() {
        XCTFail("Test does not compile / is outdated")
//        let realm = DataProvider.newInMemoryRealm()
//        try! realm.write {
//            realm.add(pointsAccount)
//        }
//        
//        let realmPointsAccount = realm.objects(PointsAccount.self).first!
//        let filtered = realmPointsAccount.filterPointsEntries(by: "Vitality Active")
//        var filteredResults: [String] = []
//        for result in filtered {
//            filteredResults.append(result.categoryCode)
//        }
//        
//        let expectedResults =  ["Vitality Active", "Vitality Active", "Vitality Active"]
//        
//        XCTAssertEqual(filteredResults, expectedResults, "Returned categories are not as expected")
    }

    func testPointsAccountSortByEffectiveDate() {
        XCTFail("Test does not compile / is outdated")
//        let realm = DataProvider.newInMemoryRealm()
//        
//        try! realm.write {
//            realm.add(pointsAccount)
//        }
//        
//        let realmPointsAccount = realm.objects(PointsAccount.self).first!
//        let sorted = realmPointsAccount.sortPointsEntriesByEffectiveDate()
//        var sortedResults: [String] = []
//        for result in sorted {
//            if let date = result.effectiveDate{
//                sortedResults.append("\(date)")
//            }
//        }
//        
//        let expectedResults =  ["2017-12-30 09:00:00 +0000", "2014-12-29 23:00:00 +0000",  "2014-12-12 00:00:00 +0000", "2012-12-30 11:00:00 +0000"]
//        
//        XCTAssertEqual(sortedResults, expectedResults, "Returned dates are not sorted correctly")

    }

    func testPointsAccountFilterPointsEntriesForMonthAndYear() {
        XCTFail("Test does not compile / is outdated")

//        let formatter = DateFormatter()
//        formatter.dateFormat = "yyyy/MM"
//        let monthAndYear = formatter.date(from: "2014/12")
//        
//        let realm = DataProvider.newInMemoryRealm()
//        
//        try! realm.write {
//            realm.add(pointsAccount)
//        }
//        
//        let realmPointsAccount = realm.objects(PointsAccount.self).first!
//        let filtered = realmPointsAccount.filteredPointsEntries(for: monthAndYear!)
//        var filteredResults: [String] = []
//        for result in filtered {
//            if let date = result.effectiveDate{
//                filteredResults.append("\(date)")
//            }
//        }
//        
//        let expectedResults =  ["2014-12-12 00:00:00 +0000", "2014-12-29 23:00:00 +0000"]
//        
//        XCTAssertEqual(filteredResults, expectedResults, "Returned dates are not filtered correctly for month and year")
    }

}
