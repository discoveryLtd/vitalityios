//
//  VDPUploadTests.swift
//  VitalityActive
//
//  Created by Simon Stewart on 7/4/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

// generic upload

import Foundation
import XCTest
@testable import VitalityKit

class VDPUploadTests: XCTestCase {
    
    var expectation: XCTestExpectation?
    
    override func setUp() {
        
        super.setUp()
        
        expectation = makeExpectation(#function)
    }
    
    func makeReading() -> VDPGenericUploadReadingViewModel {
        let reading = VDPGenericUploadReadingViewModel()
        reading.startTime = Date()
        reading.endTime = Date()
        reading.duration? = VDPGenericValueUnitofMeasurementViewModel(value: 650, unitOfMeasurement: "KILOCALORIES")
        reading.partnerCreateDate = Date()
        reading.description = "Samsung Watch workout"
        reading.notes = "notes"
        reading.manufacturer = "Samsung"
        reading.model = "iOS Health app"
        reading.partnerReadingId = "11023411785690"
        reading.readingType = "RUNNING"
        reading.dataCategory = "FITNESS"
        reading.integrity = "VERIFIED"
        reading.workout = VDPGenericUploadReadingWorkoutViewModel()
        reading.workout?.distance = VDPGenericValueUnitofMeasurementViewModel(value: 1500, unitOfMeasurement: "METERS")
        reading.workout?.energy = VDPGenericValueUnitofMeasurementViewModel(value: 650, unitOfMeasurement: "KILOCALORIES")
        reading.workout?.heartRate = VDPGenericUploadReadingWorkoutHeartRateViewModel(avgMinMax: VDPGenericUploadReadingWorkoutHeartRateAvgMinMaxViewModel(average: 160, maximum: 170, minimum: 70))
        
        return reading
    }
    
//    func testJSONParsing() {
//        let genericUpload = VDPGenericUploadViewModel()
//        genericUpload.device = VDPGenericUploadDeviceViewModel()
//        genericUpload.device?.deviceId = "1"
//        genericUpload.device?.make = "SAMSUNG"
//        genericUpload.readings = [VDPGenericUploadReadingViewModel]()
//        genericUpload.readings?.append(makeReading())
//        genericUpload.readings?.append(makeReading())
//        
//        let x = genericUpload.toJSON()
//        XCTAssertTrue(x == nil, "Expected JSON data, found none")
//    }
    
    func testWithValidPayload() {
        
        login { (error) in
            
            let genericUpload = VDPGenericUploadViewModel()
            genericUpload.header = VDPGenericUploadHeaderViewModel()
            genericUpload.header?.user = VDPGenericUploadHeaderUserViewModel()
            genericUpload.header?.user?.userIdentifier = "0307201715"
            genericUpload.header?.user?.identifierType = "PARTY_ID"
            genericUpload.header?.uploadDate = Date()
            genericUpload.header?.sessionId = "210116109"
            genericUpload.header?.partnerSystem = "Samsung"
            genericUpload.header?.verified = true
            genericUpload.header?.uploadCount = 1
            genericUpload.header?.tenantId = 1
            genericUpload.header?.processingType = "REALTIME"
            genericUpload.device = VDPGenericUploadDeviceViewModel()
            genericUpload.device?.deviceId = "151512"
            genericUpload.device?.manufacturer = "Samsung"
            genericUpload.device?.make = "Samsung"
            genericUpload.device?.model = "iOS Health app"
            genericUpload.readings = [VDPGenericUploadReadingViewModel]()
            genericUpload.readings?.append(self.makeReading())
            
            Wire.Events.upload(tenantId: 2, payload: genericUpload) { (error) in
                guard error == nil else {
                    XCTFail("failed")
                    return (self.expectation?.fulfill())!
                }
                print("logged in")
                return (self.expectation?.fulfill())!
            }
            
        }
        
        self.waitForExpectations(timeout: self.expectationTimeout) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
        }
        
    }
    func testNormal() {
        
        login { (error) in
            
            let genericUpload = VDPGenericUploadViewModel()
            genericUpload.header = VDPGenericUploadHeaderViewModel()
            genericUpload.header?.user = VDPGenericUploadHeaderUserViewModel()
            genericUpload.header?.user?.entityNo = "1229095268"
            genericUpload.header?.rawUploadData = "No Raw Data"
            genericUpload.header?.tenantId = 1
            genericUpload.header?.partnerSystem = "Apple"
            genericUpload.header?.sessionId = "1229095268_2016-04-22T6:26:17+0100"
            genericUpload.header?.verified = true
            genericUpload.header?.uploadDate = self.createDate(value: "2016/05/31")
            
            genericUpload.device = VDPGenericUploadDeviceViewModel()
            genericUpload.device?.manufacturer = "Apple"
            genericUpload.device?.make = "iPhone"
            genericUpload.device?.model = "6"
            
            genericUpload.readings = [VDPGenericUploadReadingViewModel]()
            let reading = VDPGenericUploadReadingViewModel()
            reading.manufacturer = "Apple"
            reading.endTime = self.createDate(value: "2016/04/22")
            reading.partnerCreateDate = self.createDate(value: "2016/05/31")
            reading.integrity = "VERIFIED"
            reading.partnerReadingId = "WALK_1229095268_2016-04-21_iPhone"
            reading.model = "6"
            reading.dataCategory = "ROUTINE"
            reading.readingType = "WALK"
            reading.startTime = self.createDate(value: "2016/04/21")
            
            reading.workout = VDPGenericUploadReadingWorkoutViewModel()
            reading.workout?.totalSteps = 8500
            genericUpload.readings?.append(reading)
            
            Wire.Events.upload(tenantId: 2, payload: genericUpload) { (error) in
                guard error == nil else {
                    XCTFail("failed")
                    return (self.expectation?.fulfill())!
                }
                print("logged in")
                return (self.expectation?.fulfill())!
            }
        }
        
        self.waitForExpectations(timeout: self.expectationTimeout) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
        }
        
    }
    func testMetaData() {
        
        login { (error) in
            
            let genericUpload = VDPGenericUploadViewModel()
            genericUpload.header = VDPGenericUploadHeaderViewModel()
            genericUpload.header?.user = VDPGenericUploadHeaderUserViewModel()
            genericUpload.header?.user?.userIdentifier = "1229095268"
            genericUpload.header?.user?.identifierType = "ENTITY_NUMBER"
            genericUpload.header?.rawUploadData = "No Raw Data"
            genericUpload.header?.partnerSystem = "Apple"
            genericUpload.header?.tenantId = 1
            genericUpload.header?.sessionId = "1229095268_2016-04-22T6:26:17+0100"
            genericUpload.header?.verified = true
            genericUpload.header?.uploadDate = self.createDate(value: "2016/05/31")
            
            genericUpload.device = VDPGenericUploadDeviceViewModel()
            genericUpload.device?.manufacturer = "Apple"
            genericUpload.device?.make = "iPhone"
            genericUpload.device?.model = "6"
            
            genericUpload.readings = [VDPGenericUploadReadingViewModel]()
            let reading = VDPGenericUploadReadingViewModel()
            reading.manufacturer = "Apple"
            reading.endTime = self.createDate(value: "2016/04/22")
            reading.partnerCreateDate = self.createDate(value: "2016/05/31")
            reading.integrity = "VERIFIED"
            reading.partnerReadingId = "WALK_1229095268_2016-04-21_iPhone"
            reading.model = "6"
            reading.dataCategory = "ROUTINE"
            reading.readingType = "WALK"
            reading.startTime = self.createDate(value: "2016/04/21")
            
            reading.metaDatas = [VDPGenericReadingMetaDataViewModel]()
            reading.metaDatas?.append(VDPGenericReadingMetaDataViewModel(name: "Colour", value: "true"))
            
            reading.workout = VDPGenericUploadReadingWorkoutViewModel()
            reading.workout?.totalSteps = 6587
            genericUpload.readings?.append(reading)
            
            Wire.Events.upload(tenantId: 2, payload: genericUpload) { (error) in
                guard error == nil else {
                    XCTFail("failed")
                    return (self.expectation?.fulfill())!
                }
                print("logged in")
                return (self.expectation?.fulfill())!
            }
        }
        
        self.waitForExpectations(timeout: self.expectationTimeout) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
        }
        
    }
    func testMultiple() {
        
        login { (error) in
            
            let genericUpload = VDPGenericUploadViewModel()
            genericUpload.header = VDPGenericUploadHeaderViewModel()
            genericUpload.header?.user = VDPGenericUploadHeaderUserViewModel()
            genericUpload.header?.user?.userIdentifier = "1229095268"
            genericUpload.header?.user?.identifierType = "ENTITY_NUMBER"
            genericUpload.header?.rawUploadData = "No Raw Data"
            genericUpload.header?.partnerSystem = "Apple"
            genericUpload.header?.tenantId = 1
            genericUpload.header?.sessionId = "1229095268_2016-04-22T6:26:17+0100"
            genericUpload.header?.verified = true
            genericUpload.header?.uploadDate = self.createDate(value: "2016/05/31")
            
            genericUpload.device = VDPGenericUploadDeviceViewModel()
            genericUpload.device?.manufacturer = "Apple"
            genericUpload.device?.make = "iPhone"
            genericUpload.device?.model = "6"
            
            genericUpload.readings = [VDPGenericUploadReadingViewModel]()
            
            // reading 1
            let reading1 = VDPGenericUploadReadingViewModel()
            reading1.manufacturer = "Apple"
            reading1.endTime = self.createDate(value: "2016/04/22")
            reading1.partnerCreateDate = self.createDate(value: "2016/05/31")
            reading1.integrity = "VERIFIED"
            reading1.partnerReadingId = "WALK_1229095268_2016-04-21_iPhone"
            reading1.model = "6"
            reading1.dataCategory = "ROUTINE"
            reading1.readingType = "WALK"
            reading1.startTime = self.createDate(value: "2016/04/21")
            
            reading1.metaDatas = [VDPGenericReadingMetaDataViewModel]()
            reading1.metaDatas?.append(VDPGenericReadingMetaDataViewModel(name: "Colour", value: "true"))
            reading1.metaDatas?.append(VDPGenericReadingMetaDataViewModel(name: "dailycalories", value: 230))
            
            reading1.workout = VDPGenericUploadReadingWorkoutViewModel()
            reading1.workout?.totalSteps = 6587
            genericUpload.readings?.append(reading1)
            
            // reading 2
            let reading2 = VDPGenericUploadReadingViewModel()
            reading2.manufacturer = "Apple"
            reading2.endTime = self.createDate(value: "2016/04/22")
            reading2.partnerCreateDate = self.createDate(value: "2016/05/31")
            reading2.integrity = "VERIFIED"
            reading2.partnerReadingId = "WALK_1229095268_2016-04-21_iPhone"
            reading2.model = "6"
            reading2.dataCategory = "ROUTINE"
            reading2.readingType = "WALK"
            reading2.startTime = self.createDate(value: "2016/04/21")
            reading1.workout = VDPGenericUploadReadingWorkoutViewModel()
            reading1.workout?.totalSteps = 7500
            genericUpload.readings?.append(reading2)
            
            Wire.Events.upload(tenantId: 2, payload: genericUpload) { (error) in
                guard error == nil else {
                    XCTFail("failed")
                    return (self.expectation?.fulfill())!
                }
                print("logged in")
                return (self.expectation?.fulfill())!
            }
        }
        
        self.waitForExpectations(timeout: self.expectationTimeout) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
        }
        
    }
}
