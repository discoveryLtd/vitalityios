//
//  VDPGetDevicesTests.swift
//  VitalityActive
//
//  Created by Simon Stewart on 6/1/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import XCTest
import VitalityKit

class VDPGetDevicesTests: XCTestCase {
    let goodPartyId = 100002
    let goodTenantId = 2
    
    var expectation: XCTestExpectation?
    
    override func setUp() {
        
        super.setUp()
        
        expectation = makeExpectation(#function)
    }
    
    func test1() {
        
        login { (error) in
            
            Wire.Events.getDevices(tenantId: self.goodTenantId, partyId: self.goodPartyId) { (response, error) in
                if(error != nil) {
                    XCTFail("Call failed with \(String(describing: error))")
                }
                return (self.expectation?.fulfill())!
            }
        }
        
        waitForExpectations(timeout: expectationTimeout) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
        }
    }
}
