//
//  ParticipatingPartnerCustomURLTests.swift
//  VitalityKitTests
//
//  Created by Dexter Anthony Ambrad on 9/26/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import XCTest
import VitalityKit

class ParticipatingPartnerCustomURLTests: XCTestCase {
    
    var expectation: XCTestExpectation?
    
    override func setUp() {
        
        super.setUp()
        
        expectation = makeExpectation(#function)
    }
    
    func testWithGoodParameters() {
        
        if let partnerUrl = URL(string: "https://www.qa.vitality.sumitomolife.co.jp/en/web/vitality-member-portal/know_your_health/vitality_health_check/softbank") {
            
//            Wire.Partner.participatingPartnerCustomURL(partnerUrl: partnerUrl){ error in
//                guard error == nil else {
//                    XCTFail("unexpected error of type: \(String(describing: error))")
//                    return (self.expectation?.fulfill())!
//                }
//                
//                (self.expectation?.fulfill())!
//            }
        }
        
        waitForExpectations(timeout: expectationTimeout) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
        }
    }
}
