//
//  WireDelegateTests.swift
//  VitalityActive
//
//  Created by Wilmar van Heerden on 2017/09/13.
//  Copyright © 2017 Glucode. All rights reserved.
//

import XCTest
@testable import VitalityKit

class WireDelegateTests: XCTestCase {
    
    let qaCentralAssetBaseURL = URL(string: "https://qa.vitalityservicing.com/v1/api")!
    
    let qaCentralAssetAPIManagerIdentifier = "WDVlODVDQ3RQTVFpOWpRZ2p4TERSTkxXSjV3YTpQSVZieHkyanRiOFMwRDc1NHJMa0FXWjR2aTBh"
    
    func testQACentralAssetURL() {
        let delegate = FakeWireDelegate()
        XCTAssertTrue(delegate.qaCentralAssetBaseURL == qaCentralAssetBaseURL, "QA Central Asset URL is incorrect")
    }
    
    func testQACentralAssetAPIManagerIdentifier() {
        let delegate = FakeWireDelegate()
        XCTAssertTrue(delegate.qaCentralAssetAPIManagerIdentifier == qaCentralAssetAPIManagerIdentifier, "QA Central Asset URL is incorrect")
    }
    
}

fileprivate class FakeWireDelegate: WireDelegate {
//    var qaFrankfurtURL: URL { return URL(string: "https://www.apple.com")! }
//    var devBaseURL: URL { return URL(string: "https://www.apple.com")! }
//    var testBaseURL: URL { return URL(string: "https://www.apple.com")! }
//    var qaBaseURL: URL { return URL(string: "https://www.apple.com")! }
//    var productionBaseURL: URL { return URL(string: "https://www.apple.com")! }
//    var eagleBaseURL: URL { return URL(string: "https://www.apple.com")! }
//    
//    var devAPIManagerIdentifier: String { return "123123123" }
//    var testAPIManagerIdentifier: String { return "123123123" }
//    var qaAPIManagerIdentifier: String { return "123123123" }
//    var productionAPIManagerIdentifier: String { return "123123123" }
//    var eagleAPIManagerIdentifier: String { return "123123123" }
//    var qaFrankfurtAPIManagerIdentifier: String { return "123123123" }
    
    var devBaseURL: URL { return URL(string: "https://www.apple.com")! }
    var testBaseURL: URL { return URL(string: "https://www.apple.com")! }
    var qaBaseURL: URL { return URL(string: "https://www.apple.com")! }
    var qaCentralAssetBaseURL: URL { return URL(string: "https://www.apple.com")! }
    var eagleBaseURL: URL { return URL(string: "https://www.apple.com")! }
    var qaFrankfurtURL: URL { return URL(string: "https://www.apple.com")! }
    var productionBaseURL: URL { return URL(string: "https://www.apple.com")! }
    
    var performanceBaseURL: URL { return URL(string: "https://www.apple.com")! }
    var test2BaseURL: URL { return URL(string: "https://www.apple.com")! }
    var qaSouthKoreaBaseURL: URL { return URL(string: "https://www.apple.com")! }
    
    var devAPIManagerIdentifier: String { return "123123123" }
    var testAPIManagerIdentifier: String { return "123123123" }
    var qaAPIManagerIdentifier: String { return "123123123" }
    var qaCentralAssetAPIManagerIdentifier: String { return "123123123" }
    var eagleAPIManagerIdentifier: String { return "123123123" }
    var qaFrankfurtAPIManagerIdentifier: String { return "123123123" }
    var productionAPIManagerIdentifier: String { return "123123123" }
    
    var test2APIManagerIdentifier: String { return "123123123" }
    var performanceAPIManagerIdentifier: String { return "123123123" }
    var qaSouthKoreaAPIManagerIdentifier: String { return "123123123" }
    init() {
        //
    }

}
