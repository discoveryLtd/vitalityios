//
//  ChangeUsernameRouterTests.swift
//  VitalityKitTests
//
//  Created by ted.philip.l.lat on 19/09/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import XCTest
import VitalityKit
import Alamofire
import VIAUtilities

class ChangeUsernameRouterTests: XCTestCase {
    
    var expectation: XCTestExpectation?
    
    override func setUp() {
        super.setUp()
     
        expectation = makeExpectation(#function)
    }
    
    func testChangeUsername() {
        login { (error) in
            
            //current username = Armandina_Daily@mailinator.com
            let existingUserName = "Armandina_Daily@mailinator.com"
            let newUsername = "Armandina_Daily1@mailinator.com"
            let password = "TestPass123"
            
            let request = ChangeUsernameParam(existingUserName: existingUserName,
                                              newUsername: newUsername,
                                              password: password)
            
            Wire.Party.changeUsername(request: request) { (error, data, newAccessToken) in
                
                guard error == nil else {
                    XCTFail("unexpected error of type: \(String(describing: error))")
                    return (self.expectation?.fulfill())!
                }
                
                (self.expectation?.fulfill())!
            }
        }
        
        waitForExpectations(timeout: expectationTimeout) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
        }
    }
    
}
