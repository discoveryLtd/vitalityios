//
//  GetAllPointsHistoryTests.swift
//  VitalityActive
//
//  Created by Simon Stewart on 1/31/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

import XCTest
import VitalityKit
import Alamofire

class GetAllPointsHistoryTests: XCTestCase {
    
    var expectation: XCTestExpectation?
    
    override func setUp() {
        
        super.setUp()
        
        expectation = makeExpectation(#function)
    }
    
    func testWithCurrentPeriod() {
        
        login { (error) in
            
            self._testForPeriod(period: .current, expect: self.expectation!)
        }
        
        waitForExpectations(timeout: expectationTimeout) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
        }
    }
    func testWithPreviousPeriod() {
        
        login { (error) in
            
            self._testForPeriod(period: .previous, expect: self.expectation!)
        }
        
        waitForExpectations(timeout: expectationTimeout) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
        }
    }
    func testWithCurrentAndPreviousPeriod() {
        
        login { (error) in
            
            self._testForPeriod(period: .currentAndPrevious, expect: self.expectation!)
        }
        
        waitForExpectations(timeout: expectationTimeout) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
        }
    }
    private func _testForPeriod(period: PointsPeriod, expect: XCTestExpectation) {
        Wire.Member.getAllPointsHistory(tenantId: self._goodTenantId, vitalityMembershipId: _goodMembershipId, pointsPeriod: period) {
            (response, error, succcess) in
            guard error == nil else {
                XCTFail("unexpected error of type: \(String(describing: error))")
                return (self.expectation?.fulfill())!
            }
            
            (self.expectation?.fulfill())!
        }
    }
}
