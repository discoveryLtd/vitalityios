//
//  GetAppUpdateRouterTests.swift
//  VitalityKitTests
//
//  Created by ted.philip.l.lat on 11/09/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import XCTest
import VitalityKit
import Alamofire
import VIAUtilities

class GetAppUpdateRouterTests: XCTestCase {
    
    var credentials = DataProvider.newRealm()
    var expectation: XCTestExpectation?
    var tenantId : Int = 0
    
    
    override func setUp() {
        super.setUp()
        
        expectation = makeExpectation(#function)
    }
    
    func test1() {
        
        login { (error) in
            
            self.tenantId = self.credentials.getTenantId()
            
            Wire.Update.getAppUpdate(tenantId: self.credentials.getTenantId()) {
                (error, upgradeUrl) in
                
                guard error == nil else {
                    XCTFail("unexpected error of type: \(String(describing: error))")
                    return (self.expectation?.fulfill())!
                }
                
                (self.expectation?.fulfill())!
            }
        }
        
        waitForExpectations(timeout: expectationTimeout) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
        }
        
    }
    
}
