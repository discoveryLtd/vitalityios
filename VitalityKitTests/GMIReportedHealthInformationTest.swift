import Foundation
import XCTest
import VitalityKit
import VIAUtilities

class GMIReportedHealthInformationTest: XCTestCase {
    
    func test_ItShouldValidateSuccessfulHealthInformationCreation() {
        let hi = GMIReportedHealthInformation.build(response: GMIGetHealthInformation(json: GMITestData.getHealthInformation))
        XCTAssertEqual(3, hi.sections.count)

        if let feedback = hi.sections.first(where: {(section) in return section.typeKey == SectionRef.VAgeSection })?.healthAttributes.first(where: {(a) in return 1 == 1})?.feedbacks.first(where: {(f) in 1 == 1}) {
            XCTAssertEqual(1, feedback.feedbackTips.count)
        } else {
                XCTFail("expected feedback but got none")
        }
    }
}
