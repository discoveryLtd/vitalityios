//
//  OFEAddWeblinkProofViewController.swift
//  VitalityActive
//
//  Created by Val Tomol on 12/04/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import VIAUIKit
import VitalityKit
import VIACommon

class OFEAddWeblinkProofViewModel: AnyObject {
    // MARK: Properties
    let ofeCapturedResult = DataProvider.newOFERealm().objects(OFECapturedResult.self).first
    
    var weblink: String? {
        didSet {
            self.isWeblinkDirty = !String.isNilOrEmpty(string: self.weblink)
        }
    }
    var isWeblinkDirty = false
    var isWeblinkValid: Bool {
        if self.weblink == nil {
            return false
        }
        return self.weblink!.isURLValid()
    }
    
    // MARK: Functions
    func addWeblinkProofViewTitle() -> String {
        return CommonStrings.Ofe.Common.WebLinkProof1381
    }
    
    func headerTitle() -> String {
        return CommonStrings.Ofe.Proof.Header1383
    }
    
    func headerMessage() -> String {
        return CommonStrings.Ofe.Proof.SectionDescription1382
    }
    
    func weblinkTextFieldHeader() -> String {
        return CommonStrings.Ofe.Proof.Title1388
    }
    
    func weblinkTextFieldPlaceholder() -> String {
        return CommonStrings.Ofe.Proof.Title1388
    }
    
    func weblinkErrorMessage() -> String {
        return CommonStrings.Ofe.EnterValidWeblink1381
    }
    
    func hasWeblinkProof() -> Bool {
        return ofeCapturedResult?.weblinkProof != nil
    }
}

class OFEAddWeblinkProofViewController: VIATableViewController {
    // MARK: Properties
    fileprivate let SEGUE_SUMMARY = "segueSummary"
    
    var viewModel = OFEAddWeblinkProofViewModel()
    var claimPointsViewModel = OFEClaimPointsViewModel()
    
    // MARK: View Lifecycles
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = viewModel.addWeblinkProofViewTitle()
        self.configureAppearance()
        self.configureTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.configureKeyboardAutoToolbar(VIAApplicableFeatures.default.enableKeyboardAutoToolbar ?? false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.hideBackButtonTitle()
        self.disableKeyboardAutoToolbar()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
}

// MARK: View Configuration
extension OFEAddWeblinkProofViewController {
    fileprivate func configureAppearance() {
        navigationController?.makeNavigationBarTransparent()
        self.addRightBarButtonItem(CommonStrings.NextButtonTitle84, target: self, selector: #selector(onNext))
        
        self.navigationItem.rightBarButtonItem?.isEnabled = false
    }
    
    func onNext(sender: UIBarButtonItem) {
        performSegue(withIdentifier: SEGUE_SUMMARY, sender: self)
    }
    
    fileprivate func configureTableView() {
        self.tableView.register(VIATextFieldTableViewCell.nib(), forCellReuseIdentifier: VIATextFieldTableViewCell.defaultReuseIdentifier)
        self.tableView.estimatedRowHeight = 100
        self.tableView.sectionHeaderHeight = UITableViewAutomaticDimension
        self.tableView.estimatedSectionHeaderHeight = 500
        self.tableView.backgroundColor = UIColor.white
        self.tableView.tableFooterView = UIView()
        
        self.tableView.contentInset = UIEdgeInsets(top: 100, left: 0, bottom: 0, right: 0)
        let view = AddWeblinkView.viewFromNib(owner: self)!
        // TODO: Take a look into this. Image asset is exported as 25x25 even though the asset on Zepplin is 50x50.
        view.setHeader(image: VIAOrganisedFitnessEventsAsset.weblinkProofBig.templateImage)
        view.setHeader(title: viewModel.headerTitle())
        view.setDetail(message: viewModel.headerMessage())
        tableView.tableHeaderView = view
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if !VIAApplicableFeatures.default.includeAllProofs() {
            guard let destinationVC = segue.destination as? OFESummaryViewController else { return }
            destinationVC.usePhotoProofOnSubmit = false
        }
    }
}

// MARK: UITableView DataSource and Delegates
extension OFEAddWeblinkProofViewController {
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return nil
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return nil
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return self.tableView.estimatedRowHeight
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: VIATextFieldTableViewCell.defaultReuseIdentifier, for: indexPath) as? VIATextFieldTableViewCell else {
            return tableView.defaultTableViewCell()
        }
        
        cell.accessoryType = .none
        cell.selectionStyle = .none
        
        cell.setHeadingLabelText(text: viewModel.weblinkTextFieldHeader())
        cell.setTextFieldPlaceholder(placeholder: viewModel.weblinkTextFieldPlaceholder())
        cell.hideCellImage()
        configureTextFieldListener(cell: cell, indexPath: indexPath)
        
        return cell
    }
}

// MARK: UITextField Delegates
extension OFEAddWeblinkProofViewController {
    func configureTextFieldListener(cell: VIATextFieldTableViewCell, indexPath: IndexPath) {
        cell.textFieldDidEndEditing = { [weak self] textField in
            self?.captureTextFieldResult(textField: textField)
            
            if (self?.viewModel.isWeblinkDirty)! && !(self?.viewModel.isWeblinkValid)! {
                cell.setErrorMessage(message: self?.viewModel.weblinkErrorMessage())
            } else {
                cell.setErrorMessage(message: nil)
            }
        }
        cell.textFieldShouldReturn = { [weak self] textField in
            self?.captureTextFieldResult(textField: textField)
            return true
        }
        cell.textFieldTextDidChange = { [weak self] textField in
            self?.viewModel.weblink = textField.text
            
            if !(textField.text?.isEmpty)! && (self?.viewModel.isWeblinkValid)! {
                return
            }
            
            self?.navigationItem.rightBarButtonItem?.isEnabled = false
        }
    }
    
    func captureTextFieldResult(textField: UITextField) {
        claimPointsViewModel.saveCapturedData(weblinkProof: !(textField.text?.isEmpty)! ? textField.text : nil)
        updateTableView()
    }
}

// MARK: Custom Methods
extension OFEAddWeblinkProofViewController {
    func updateTableView() {
        manipulateRightBarButton()
        tableView.beginUpdates()
        // No animation or other calls needed since we are just updating the content of the textfield.
        tableView.endUpdates()
    }
    
    func manipulateRightBarButton() {
        if viewModel.hasWeblinkProof() && viewModel.isWeblinkValid {
            self.navigationItem.rightBarButtonItem?.isEnabled = true
        }
    }
}
