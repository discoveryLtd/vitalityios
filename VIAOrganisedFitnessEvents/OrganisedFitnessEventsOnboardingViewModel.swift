//
//  OrganisedFitnessEventsOnboardingViewModel.swift
//  VitalityActive
//
//  Created by Erik John T. Alicaya (ADMIN) on 12/04/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import VitalityKit
import VIAUIKit
import VIAUtilities

public class OrganisedFitnessEventsOnBoardingViewModel: AnyObject, OnboardingViewModel {
    public var heading: String {
        return CommonStrings.Ofe.Heading.Title1335
    }
    
    public var contentItems: [OnboardingContentItem] {
        var items = [OnboardingContentItem]()
        items.append(OnboardingContentItem(heading: CommonStrings.Ofe.Onboarding.Header1338,
                                           content: CommonStrings.Ofe.Onboarding.Description1339,
                                           image: VIAOrganisedFitnessEventsAsset.manRunningSmall.image))
        items.append(OnboardingContentItem(heading: CommonStrings.Ofe.Onboarding.Heading1340,
                                           content: CommonStrings.Ofe.Onboarding.Description1341,
                                           image: VIAOrganisedFitnessEventsAsset.rewardSmall.image))
        return items
    }
    
    public var buttonTitle: String {
        return CommonStrings.Dc.Onboarding.GotIt131
    }
    
    public var alternateButtonTitle: String? {
        return CommonStrings.LearnMoreButtonTitle104
    }
    
    public var gradientColor: Color {
        return .blue
    }
    
    public var mainButtonHighlightedTextColor: UIColor? {
        return .improveYourHealthBlue()
    }
}
