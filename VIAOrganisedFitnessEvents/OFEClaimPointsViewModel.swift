//
//  OFEClaimPointsViewModel.swift
//  VitalityActive
//
//  Created by Val Tomol on 26/04/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import VIAUtilities
import VitalityKit

public class EventDetail {
    var text: String = ""
    var placeholder: String = ""
    var rowTypeRef: OFEFeatureTypeRef? = .Unknown
    var summaryHeader: String = ""
    
    init(text: String,
         placeholder: String,
         rowTypeRef: OFEFeatureTypeRef? = .Unknown,
         summaryHeader: String) {
        self.text = text
        self.placeholder = placeholder
        self.rowTypeRef = rowTypeRef
        self.summaryHeader = summaryHeader
    }
}

public class OFEClaimPointsViewModel: AnyObject {
    // MARK: Properties
    let ofeCapturedResult = DataProvider.newOFERealm().objects(OFECapturedResult.self).first
    internal let eventDetailSortOrder: [OFEFeatureTypeRef] = [.EventType, .OFEDistance, .FinishTime, .EventDate]
    internal let extraEventDetailSortOrder: [OFEFeatureTypeRef] = [.EventName, .RaceNumber, .Organiser]
    
    // MARK: Functions
    func claimPointsViewTitle() -> String {
        return CommonStrings.Ofe.Action.Title1337
    }
    
    func headerTitle() -> String {
        return CommonStrings.Ofe.ClaimPoints.Header1363
    }
    
    func contentTitle() -> String {
        return CommonStrings.Ofe.ClaimPoints.Description1364
    }
    
    func hasCapturedResults() -> Bool {
        return ofeCapturedResult != nil
    }
    
    func hasCompletedForm() -> Bool {        
        let requiredFieldsBool = ofeCapturedResult?.eventTypeCode != nil &&
            ofeCapturedResult?.eventType != nil &&
            ofeCapturedResult?.eventDate != nil &&
            ofeCapturedResult?.selectionOption != nil
        
        var applicableFieldsBool: Bool {
            for field in applicableEventDetails() {
                let fieldTypeRef: OFEFeatureTypeRef = field.rowTypeRef!
                if fieldTypeRef == .OFEDistance, ofeCapturedResult?.selectionOption == nil {
                    return false
                } else if fieldTypeRef == .FinishTime, ofeCapturedResult?.finishTime == nil {
                    return false
                } else if fieldTypeRef == .EventName, ofeCapturedResult?.eventName == nil {
                    return false
                } else if fieldTypeRef == .RaceNumber, ofeCapturedResult?.raceNumber == nil {
                    return false
                } else if fieldTypeRef == .Organiser, ofeCapturedResult?.organiser == nil {
                    return false
                }
            }
            return true
        }
        
        return requiredFieldsBool && applicableFieldsBool
    }
    
    func saveCapturedData(eventTypeCode: String? = nil,
                          eventType: OFEEventType? = nil,
                          selectionOption: OFESelectionOption? = nil,
                          finishTime: String? = nil,
                          eventDate: Date? = nil,
                          eventName: String? = nil,
                          raceNumber: String? = nil,
                          organiser: String? = nil,
                          weblinkProof: String? = nil) {
        var eventTypeCodeValue: String? {
            return (eventTypeCode != nil) ? eventTypeCode : ofeCapturedResult?.eventTypeCode
        }
        var eventTypeValue: OFEEventType? {
            return (eventType != nil) ? eventType : ofeCapturedResult?.eventType
        }
        var selectionOptionValue: OFESelectionOption? {
            return (selectionOption != nil) ? selectionOption : ofeCapturedResult?.selectionOption
        }
        var finishTimeValue: String? {
            return (finishTime != nil) ? finishTime : ofeCapturedResult?.finishTime
        }
        var eventDateValue: Date? {
            return (eventDate != nil) ? eventDate : ofeCapturedResult?.eventDate
        }
        var eventNameValue: String? {
            return (eventName != nil) ? eventName : ofeCapturedResult?.eventName
        }
        var raceNumberValue: String? {
            return (raceNumber != nil) ? raceNumber : ofeCapturedResult?.raceNumber
        }
        var organiserValue: String? {
            return (organiser != nil) ? organiser : ofeCapturedResult?.organiser
        }
        var weblinkProofValue: String? {
            return weblinkProof
        }
        
        let ofeRealm = DataProvider.newOFERealm()
        ofeRealm.capturedResult(eventTypeCode: eventTypeCodeValue,
                                eventType: eventTypeValue,
                                selectionOption: selectionOptionValue,
                                finishTime: finishTimeValue,
                                eventDate: eventDateValue,
                                eventName: eventNameValue,
                                raceNumber: raceNumberValue,
                                organiser: organiserValue,
                                weblinkProof: weblinkProofValue)
    }
    
    func applicableEventDetails() -> [EventDetail] {
        var details: [EventDetail] = []
        if let eventDetails = ofeCapturedResult?.eventType?.eventDetails {
            for eventDetail in eventDetails {
                var placeholderValue: String
                switch eventDetail.metadataKey {
                case .OFEDistance:
                    placeholderValue = CommonStrings.LabelSelect288
                case .RaceNumber:
                    placeholderValue = CommonStrings.Ofe.ClaimPoints.Placeholder1405
                case .FinishTime:
                    placeholderValue = "00:00:00"
                default:
                    placeholderValue = eventDetail.metadataName
                }
                
                let eventDetail = EventDetail(text: eventDetail.metadataName,
                                              placeholder: placeholderValue,
                                              rowTypeRef: eventDetail.metadataKey,
                                              summaryHeader: eventDetail.metadataName)
                details.append(eventDetail)
            }
        }
        return details
    }
    
    func eventDetailRows() -> [EventDetail] {
        var details: [EventDetail] = []
        // Force Event Type row.
        let eventTypeRow = EventDetail(text: CommonStrings.Ofe.ClaimPoints.Placeholder1366,
                                       placeholder: CommonStrings.LabelSelect288,
                                       rowTypeRef: .EventType,
                                       summaryHeader: CommonStrings.Ofe.ClaimPoints.Placeholder1366)
        details.append(eventTypeRow)
        
        // Insert applicaple event details.
        for detailOrder in eventDetailSortOrder {
            for applicableEventRow in applicableEventDetails() {
                let eventDetailRow = EventDetail(text: applicableEventRow.text,
                                                 placeholder: applicableEventRow.placeholder,
                                                 rowTypeRef: applicableEventRow.rowTypeRef,
                                                 summaryHeader: applicableEventRow.summaryHeader)
                if applicableEventRow.rowTypeRef == detailOrder {
                    details.append(eventDetailRow)
                }
            }
        }
        
        // Force Event Date row.
        let eventDateRow = EventDetail(text: CommonStrings.Ofe.ClaimPoints.EventDate1373,
                                       placeholder: CommonStrings.DateTitle264,
                                       rowTypeRef: .EventDate,
                                       summaryHeader: CommonStrings.Ofe.ClaimPoints.EventDate1373)
        details.append(eventDateRow)
        
        return details
    }
    
    func extraEventDetailRows() -> [EventDetail] {
        var extraDetails: [EventDetail] = []
        
        // Insert applicaple event details.
        for detailOrder in extraEventDetailSortOrder {
            for applicableEventRow in applicableEventDetails() {
                let eventDetailRow = EventDetail(text: applicableEventRow.text,
                                                 placeholder: applicableEventRow.placeholder,
                                                 rowTypeRef: applicableEventRow.rowTypeRef,
                                                 summaryHeader: applicableEventRow.summaryHeader)
                if applicableEventRow.rowTypeRef == detailOrder {
                    extraDetails.append(eventDetailRow)
                }
            }
        }
        
        return extraDetails
    }
}
