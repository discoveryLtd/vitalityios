//
//  EventsPointsDetailViewController.swift
//  VitalityActive
//
//  Created by Harry C. Lingad on 4/13/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import UIKit
import VIAUIKit
import VitalityKit
import VIAUtilities
import VIACommon
import RealmSwift

class EventsPointsDetailViewController: VIATableViewController {
    fileprivate let SEGUE_CLAIM_POINTS = "segueClaimPoints"
    
    lazy var dateFormatter = DateFormatter.fullDateFormatter()
    var reasonText: String?
    var pointsEntry: PointsEntry!
    var device: String?
    
    var eventType: OFEEventType?
    var eventTypeName: String?
    var eventsAndPointsDetail: [OFESelectionOption] = []
    
    var sections: [Sections] = [Sections.Points, Sections.ClaimPoints]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = eventTypeName ?? ""
        
        configureTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        configureAppearance()
    }
    
    func configureAppearance() {
        navigationController?.makeNavigationBarTransparent()
        self.hideBackButtonTitle()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - TableView Configuration
    func configureTableView() {
        self.tableView.register(OFEEventPointCell.nib(), forCellReuseIdentifier: OFEEventPointCell.defaultReuseIdentifier)
        self.tableView.register(VIALabelTableViewCell.nib(), forCellReuseIdentifier: VIALabelTableViewCell.defaultReuseIdentifier)
        self.tableView.register(VIATableViewSectionHeaderFooterView.nib(), forHeaderFooterViewReuseIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier)
        self.tableView.estimatedRowHeight = 100
        self.tableView.contentInset = UIEdgeInsets(top: 30, left: 0, bottom: 0, right: 0)
    }
    
    // MARK: UITableView datasource
    enum Sections: Int, EnumCollection {
        case Points = 0
        case ClaimPoints = 1
        
        func sectionHeaderTitle() -> String? {
            switch self {
            case .Points:
                return CommonStrings.Pm.ActivityDetailSectionHeading1560
            case .ClaimPoints:
                return nil
            }
        }
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch sections[section] {
        case .Points:
            return eventsAndPointsDetail.count
        case .ClaimPoints:
            if let hideHelpTab = VIAApplicableFeatures.default.hideHelpTab,
                hideHelpTab{
                return 0
            } else{
                return 1
            }
        }
    }
    
    // MARK: UITableView delegate
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let theSection = sections[indexPath.section]
        if theSection == .Points {
            return self.configurePointsCell(indexPath)
        } else if theSection == .ClaimPoints {
            return self.configureClaimPointsCell(indexPath)
        }
        
        return UITableViewCell(style: .default, reuseIdentifier: "cell")
        
    }
    
    func configurePointsCell(_ indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: OFEEventPointCell.defaultReuseIdentifier, for: indexPath) as? OFEEventPointCell else {
            return tableView.defaultTableViewCell()
        }
        
        cell.headingText = String(eventsAndPointsDetail[indexPath.row].potentialPoints)
        cell.contentText = eventsAndPointsDetail[indexPath.row].featureName
        cell.configureWithLargeHeading()
        
        cell.selectionStyle = .none
        
        return cell
    }
    
    func configureClaimPointsCell(_ indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: VIALabelTableViewCell.defaultReuseIdentifier, for: indexPath) as? VIALabelTableViewCell else {
            return tableView.defaultTableViewCell()
        }
        
        cell.labelText = CommonStrings.Ofe.Action.Title1337
        cell.configure(font: UIFont.cellStyleMenuItemTextLabel(), color: UIColor.improveYourHealthBlue())
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier) as? VIATableViewSectionHeaderFooterView else {
            return nil
        }
        
        if let title = sections[section].sectionHeaderTitle() {
            view.labelText = title.uppercased()
        }
        
        return view
    }
    
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return nil
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let section = sections[indexPath.section]
        
        if section == .ClaimPoints {
            let ofeRealm = DataProvider.newOFERealm()
            // Delete previous capture.
            try? ofeRealm.write {
                ofeRealm.delete(ofeRealm.allOFECapturedResults())
            }
            // Save current capture. Reset the rest of the fields.
            ofeRealm.capturedResult(eventTypeCode: eventType?.eventTypeCode,
                                    eventType: eventType,
                                    eventDate: Date())
            
            self.performSegue(withIdentifier: SEGUE_CLAIM_POINTS, sender: self)
        }
    }
}
