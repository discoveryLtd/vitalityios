//
//  OFESubmissionHelper.swift
//  VitalityActive
//
//  Created by Val Tomol on 20/04/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import VitalityKit
import VIAUtilities
import VIAImageManager

public protocol OFEPhotoUploadDelegate: class {
    func displayImageUploadErrorMessage(with completion: @escaping (_ resubmit: Bool) -> Void)
}

protocol OFEEventsProcessor {
    func processEvents(completion: @escaping (_ error: Error?) -> Void)
}

class OFESubmissionHelper: OFEEventsProcessor {
    internal let ofeCapturedResult = DataProvider.newOFERealm().objects(OFECapturedResult.self).first
    internal let ofeRealm = DataProvider.newOFERealm()
    internal weak var photoUploadDelegate: OFEPhotoUploadDelegate?
    internal let photoUploader = PhotoUploader()
    
    private var completion: ((Error?) -> (Void)) = { error in
        debugPrint("Complete")
    }
    
    func setupDelegate(_ delegate: OFEPhotoUploadDelegate) {
        photoUploadDelegate = delegate
    }
    
    func processEvents(completion: @escaping (_ error: Error?) -> Void) {
        self.completion = completion
        submitImagesRetreiveReferences()
    }
    
    internal func submitImagesRetreiveReferences() {
        guard let realmCapturedAssets = ofeRealm.allPhotos() else {
            debugPrint("No images to upload")
            return
        }
        
        photoUploader.delegate = self
        photoUploader.submitCapturedVHCImages(realmCapturedAssets)        
    }
    
    internal func submitEvents(with references: Array<String>?) {
        let coreRealm = DataProvider.newRealm()
        let tenantId = coreRealm.getTenantId()
        let partyId = coreRealm.getPartyId()
        
        // ProcessEventsV2 - PartyReferenceEvent
        let eventSourceTypeKey: EventSourceRef = .MobileApp
        let eventCapturedOn = Date()
        let eventOccuredOn = ofeCapturedResult?.eventDate
        let applicableTo = ProcessEventsV2ApplicableTo(referenceTypeKey: eventSourceTypeKey.rawValue,
                                                       value: String(partyId))
        let reportedBy = ProcessEventsV2PartyReferenceEventsReportedBy(referenceTypeKey: eventSourceTypeKey.rawValue,
                                                                       value: String(partyId))
        let typeKey = ofeCapturedResult?.eventType?.eventTypeKey
        var eventMetaDatas = [ProcessEventsV2EventMetaData]()
        
        // Attach event metadatas.
        for eventDetail in (ofeCapturedResult?.eventType?.eventDetails)! {
            // OFEDistance
            if eventDetail.metadataKey == .OFEDistance  {
                let selectedOptionFeatureKey = String((ofeCapturedResult?.selectionOption?.featureKey)!)
                // TODO: Temporary implementation of uom
                let uom = String(UnitOfMeasureRef.Kilometer.rawValue)
                let distanceMetaData = ProcessEventsV2EventMetaData(typeKey: eventDetail.metadataKey.rawValue,
                                                                    value: selectedOptionFeatureKey,
                                                                    unitOfMeasure: uom)
                eventMetaDatas.append(distanceMetaData)
            }
            
            // RaceNumber
            if eventDetail.metadataKey == .RaceNumber {
                let raceNumberMetaData = ProcessEventsV2EventMetaData(typeKey: eventDetail.metadataKey.rawValue,
                                                                      value: (ofeCapturedResult?.raceNumber)!,
                                                                      unitOfMeasure: nil)
                eventMetaDatas.append(raceNumberMetaData)
            }
            
            // EventName
            if eventDetail.metadataKey == .EventName {
                let eventNameMetaData = ProcessEventsV2EventMetaData(typeKey: eventDetail.metadataKey.rawValue,
                                                                     value: (ofeCapturedResult?.eventName)!,
                                                                     unitOfMeasure: nil)
                eventMetaDatas.append(eventNameMetaData)
            }
            
            // FinishTime
            if eventDetail.metadataKey == .FinishTime {
                let finishTimeMetaData = ProcessEventsV2EventMetaData(typeKey: eventDetail.metadataKey.rawValue,
                                                                      value: (ofeCapturedResult?.finishTime)!,
                                                                      unitOfMeasure: nil)
                eventMetaDatas.append(finishTimeMetaData)
            }
            
            // Organiser
            if eventDetail.metadataKey == .Organiser {
                let organiserMetaData = ProcessEventsV2EventMetaData(typeKey: eventDetail.metadataKey.rawValue,
                                                                     value: (ofeCapturedResult?.organiser)!,
                                                                     unitOfMeasure: nil)
                eventMetaDatas.append(organiserMetaData)
            }
        }
        
        // Weblink
        if let weblinkProof = ofeCapturedResult?.weblinkProof {
            let weblinkMetaData = ProcessEventsV2EventMetaData(typeKey: OFEFeatureTypeRef.WeblinkProof.rawValue,
                                                               value: weblinkProof,
                                                               unitOfMeasure: nil)
            eventMetaDatas.append(weblinkMetaData)
        }
        
        // Photo references
        if references?.count ?? 0 > 0 {
            for reference in references! {
                let imageProof = ProcessEventsV2EventMetaData(typeKey: EventMetaDataTypeRef.DocumentReference.rawValue,
                                                              value: reference,
                                                              unitOfMeasure: nil)
                eventMetaDatas.append(imageProof)
            }
        }
        
        let processEvent = ProcessEventsV2PartyReferenceEvent(applicableTo: applicableTo,
                                                              associatedEvents: [],
                                                              eventCapturedOn: eventCapturedOn,
                                                              eventExternalReference: nil,
                                                              eventId: nil,
                                                              eventMetaDatas: eventMetaDatas,
                                                              eventOccurredOn: eventOccuredOn!,
                                                              eventSourceTypeKey: eventSourceTypeKey,
                                                              reportedBy: reportedBy,
                                                              typeKey: typeKey!)
        
        var events = [ProcessEventsV2PartyReferenceEvent]()
        events.append(processEvent)
        
        Wire.Events.processEventsV2(tenantId: tenantId, events: events, completion: { [weak self] (response, error) in
            self?.completion(error)
        })
    }
}

extension OFESubmissionHelper: PhotoUploaderDelegate{
    
    func imagesUploaded(references: Array<String>) {
        submitEvents(with: references)
    }
    
    func onImageUploadFailed() {
        self.photoUploadDelegate?.displayImageUploadErrorMessage(with: { [weak self] tryAgain in
            if tryAgain {
                self?.photoUploader.reSubmitFailedImages()
            }
        })
    }
}
