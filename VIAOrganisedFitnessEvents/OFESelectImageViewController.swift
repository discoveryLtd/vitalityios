////
////  SelectImageViewController.swift
////  VitalityActive
////
////  Created by Erik John T. Alicaya (ADMIN) on 13/04/2018.
////  Copyright © 2018 Glucode. All rights reserved.
////
//
//import UIKit
//import VitalityKit
//import VIACommon
//import AVFoundation
//import Photos
//import VIAImageManager
//
//@objc protocol OptionalImagePickerDelegate: class {
//    @objc optional func dismissComplete()
//}
//
//protocol ImagePickerDelegate: class {
//    func pass(imageInfo: [String : Any])
//    func show(alert: UIAlertController)
//    func present(imagePicker: UIImagePickerController)
//}
//
//
//class OFESelectImageController: NSObject, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
//    private var imagePicker = UIImagePickerController()
//    
//    // TODO: Should this delegate be optional and then hold a weak reference?
//    weak var delegate: ImagePickerDelegate?
//    weak var optionalDelegate: OptionalImagePickerDelegate?
//    fileprivate var configuration: UIImagePickerController.ImageControllerConfiguration?
//    
//    init(pickerDelegate: ImagePickerDelegate, configuration: UIImagePickerController.ImageControllerConfiguration) {
//        self.delegate = pickerDelegate
//        self.configuration = configuration
//    }
//    
//    func selectImage(view: UIView? = nil) {
//        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
//        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
//            let cameraAction = UIAlertAction(title: VhcStrings.Proof.ActionTakePhotoAlertTitle167, style: .default) { (UIAlertAction) in
//                self.showCamera()
//            }
//            actionSheet.addAction(cameraAction)
//        }
//        
//        
//        let photoAction = UIAlertAction(title: VhcStrings.Proof.ActionChooseFromLibraryAlertTitle168,
//                                        style: UIAlertActionStyle.default) { (UIAlertAction) in
//                                            self.showImagePicker()
//        }
//        actionSheet.addAction(photoAction)
//        
//        if nil != view {
//            actionSheet.popoverPresentationController?.sourceView = view
//            actionSheet.popoverPresentationController?.sourceRect = view!.bounds
//        }
//        
//        let cancelAction = UIAlertAction(title: CommonStrings.CancelButtonTitle24, style: UIAlertActionStyle.cancel) { (UIAlertAction) in
//            actionSheet.dismiss(animated: false, completion: nil)
//        }
//        
//        actionSheet.addAction(cancelAction)
//        delegate?.show(alert: actionSheet)
//    }
//    
//    func showImageDirectly(){
//        self.showImagePicker()
//    }
//    
//    func dismissImagePicker() {
//        self.imagePicker.dismiss(animated: true) {
//            self.optionalDelegate?.dismissComplete!()
//        }
//    }
//    
//    private func showImagePicker() {
//        let status = PHPhotoLibrary.authorizationStatus()
//        if (status == PHAuthorizationStatus.authorized) {
//            self.presentPhotosImagePicker()
//        } else if (status == PHAuthorizationStatus.denied) {
//            self.showNoAccessToPhotosAction()
//        } else if (status == PHAuthorizationStatus.notDetermined) {
//            self.reqeustPhotosAccess {
//                let status = PHPhotoLibrary.authorizationStatus()
//                if (status == PHAuthorizationStatus.authorized) {
//                    self.presentPhotosImagePicker()
//                }
//            }
//        }
//    }
//    
//    private func presentPhotosImagePicker() {
//        imagePicker.delegate = self
//        imagePicker.sourceType = UIImagePickerControllerSourceType.savedPhotosAlbum
//        imagePicker.allowsEditing = false
//        
//        delegate?.present(imagePicker: imagePicker)
//    }
//    
//    private func showCamera() {
//        if AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeVideo) == AVAuthorizationStatus.denied {
//            self.showNoAccessToCameraAction()
//        } else if AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeVideo) == AVAuthorizationStatus.authorized {
//            self.presentCameraImagePicker()
//        } else if AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeVideo) == AVAuthorizationStatus.notDetermined {
//            self.reqeustCameraAccess {
//                self.presentCameraImagePicker()
//            }
//        }
//    }
//    
//    private func presentCameraImagePicker() {
//        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
//            imagePicker.allowsEditing = false
//            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
//            imagePicker.cameraCaptureMode = .photo
//            imagePicker.modalPresentationStyle = .fullScreen
//            imagePicker.delegate = self
//            delegate?.present(imagePicker: imagePicker)
//        }
//    }
//    
//    func showNoAccessToPhotosAction() {
//        self.showNoAccessAlertController(title: VhcStrings.Proof.CannotAccessLibraryAlertTitle172, message: VhcStrings.Proof.CannotAccessLibraryAlertMessage173)
//    }
//    
//    func showNoAccessToCameraAction() {
//        self.showNoAccessAlertController(title: VhcStrings.Proof.CannotAccessCameraAlertTitle175, message: VhcStrings.Proof.CannotAccessCameraAlertMessage176)
//    }
//    
//    func showNoAccessAlertController(title: String, message: String) {
//        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
//        let cancelAction = UIAlertAction(title: CommonStrings.CancelButtonTitle24, style: UIAlertActionStyle.cancel) { (UIAlertAction) in
//            alertController.dismiss(animated: true, completion: nil)
//        }
//        
//        let settingsAction = UIAlertAction(title: CommonStrings.GenericSettingsButtonTitle271, style: .default) { (_) -> Void in
//            let settingsUrl = URL(string: UIApplicationOpenSettingsURLString)
//            if let url = settingsUrl {
//                UIApplication.shared.open(url, completionHandler: nil)
//            }
//        }
//        alertController.addAction(settingsAction)
//        
//        alertController.addAction(cancelAction)
//        delegate?.show(alert: alertController)
//    }
//    
//    internal func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
//        
//        if let limitFileSizeForUpload = configuration?.shouldLimitFileSize, limitFileSizeForUpload {
//            
//            guard let url = info[UIImagePickerControllerReferenceURL] as? URL else { return }
//            let result = PHAsset.fetchAssets(withALAssetURLs: [url], options: nil)
//            guard let asset = result.firstObject else { return }
//            let manager = PHImageManager.default()
//            manager.requestImageData(for: asset, options: nil, resultHandler: { imageData, dataUTI, orientation, imageInfo in
//
//                guard let maxSizeValue = self.configuration?.maxFileSize else { return }
//                
//                //Get bytes size of image
//                if let imageSize = imageData?.count, imageSize <= maxSizeValue {
//                    self.delegate?.pass(imageInfo: info)
//                }else{
//                    self.dismissImagePicker()
//                    let controller = UIAlertController(title: CommonStrings.AlertErrorTitle268,
//                                                        message: CommonStrings.Common.DialogUploadLimitErrorMessage9999,
//                                                        preferredStyle: .alert)
//                        
//                    let ok = UIAlertAction(title: CommonStrings.OkButtonTitle40, style: .cancel) { action in
//                    }
//                    controller.addAction(ok)
//                    self.delegate?.show(alert: controller)
//                }
//            })
//        }else{
//            delegate?.pass(imageInfo: info)
//        }
//    }
//    
//    public func reqeustPhotosAccess(_ completion: @escaping () -> Void) {
//        PHPhotoLibrary.requestAuthorization { (status) in
//            switch (status) {
//            case .authorized:
//                completion()
//                break
//            case .denied:
//                break
//            case .restricted:
//                break
//            case .notDetermined:
//                break
//            }
//        }
//    }
//    
//    public func reqeustCameraAccess(_ completion: @escaping () -> Void) {
//        AVCaptureDevice.requestAccess(forMediaType: AVMediaTypeVideo) { (granted) in
//            if (granted == true) {
//                completion()
//            } else {
//                
//            }
//        }
//    }
//}
