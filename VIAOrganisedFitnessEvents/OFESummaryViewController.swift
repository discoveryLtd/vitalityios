//
//  OFESummaryViewController.swift
//  VitalityActive
//
//  Created by Val Tomol on 13/04/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import VIAUIKit
import VitalityKit
import VIAUtilities
import VIACommon

class OFESummaryViewModel: AnyObject {
    // MARK: Properties
    let ofeCapturedResult = DataProvider.newOFERealm().objects(OFECapturedResult.self).first
    let realm = DataProvider.newOFERealm()
    
    // MARK: Functions
    func summaryViewTitle() -> String {
        return CommonStrings.Ofe.Summary.Title1389
    }
    
    func summaryHeader() -> String {
        return CommonStrings.Ofe.Summary.Header1390
    }
    
    func summaryContent() -> String {
        return CommonStrings.Ofe.Summary.HeaderDescription1391
    }
    
    func pointsText(text: String) -> String {
        return CommonStrings.Status.PointsProgress838(text)
    }
    
    func submittedImages() -> Array<UIImage>? {
        var imagesArray: Array<UIImage> = Array<UIImage>()
        let images = realm.allPhotos()
        if let collectedImages = images {
            for collectedImage in collectedImages {
                if let assetURL = collectedImage.assetURL {
                    if let asset = OFEPhotoAssetHelper.photosAssetForFileURL(url: assetURL) {
                        imagesArray.append(OFEPhotoAssetHelper.getAssetThumbnail(asset: asset))
                    }
                } else {
                    if let imageData = collectedImage.assetData {
                        if let assetImage = UIImage(data: imageData) {
                            /*
                             Assets saved to realm as data using the UIImagePNGRepresentation method
                             If you save a UIImage as a JPEG, it will set the rotation flag.
                             PNGs do not support a rotation flag, so if you save a UIImage as a PNG,
                             it will be rotated incorrectly and not have a flag set to fix it.
                             So if you want PNGs you must rotate them yourself.
                             */
                            if let cgAsset = assetImage.cgImage {
                                let uprightImage = UIImage(cgImage: cgAsset, scale: 1, orientation: UIImageOrientation.right)
                                imagesArray.append(uprightImage)
                            } else {
                                imagesArray.append(assetImage)
                            }
                        }
                    }
                }
            }
        }
        return imagesArray
    }
}

class OFESummaryViewController: VIATableViewController {
    // MARK: Properties
    fileprivate let SEGUE_DATA_PRIVACY = "segueDataPrivacy"
    fileprivate let SEGUE_COMPLETION = "segueCompletion"
    public var usePhotoProofOnSubmit: Bool = true
    internal var heightForCell: CGFloat = 0.0
    
    var viewModel = OFESummaryViewModel()
    var claimPointsViewModel = OFEClaimPointsViewModel()
    
    lazy var dateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter.dayDateShortMonthyearFormatter()
        return dateFormatter
    }()
    
    lazy var timeFormatter: DateFormatter = {
        let dateFormatter = DateFormatter.hoursMinutesSecondsFormatter()
        return dateFormatter
    }()
    
    // MARK: View Lifecycles
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = viewModel.summaryViewTitle()
        self.configureAppearance()
        self.configureTableView()
        
        prepareCapturedResultForSubmission()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.navigationBar.isHidden = false
        navigationController?.makeNavigationBarTransparent()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return VIAApplicableFeatures.default.includeAllProofs() ? SectionsWithAllProofs.allValues.count : Sections.allValues.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if VIAApplicableFeatures.default.includeAllProofs() {
            let sectionValue = SectionsWithAllProofs(rawValue: section)
            
            if sectionValue == .eventDetails {
                return claimPointsViewModel.eventDetailRows().count
            } else if sectionValue == .extraEventDetails {
                return claimPointsViewModel.extraEventDetailRows().count
            } else if sectionValue == .imageProof {
                return 1
            } else {
                return WeblinkProofRow.allValues.count
            }
        } else {
            let sectionValue = Sections(rawValue: section)
            
            if sectionValue == .eventDetails {
                return claimPointsViewModel.eventDetailRows().count
            } else if sectionValue == .extraEventDetails {
                return claimPointsViewModel.extraEventDetailRows().count
            } else {
                return WeblinkProofRow.allValues.count
            }
        }
    }
}

// MARK: Section Enum
extension OFESummaryViewController {
    enum Sections: Int, EnumCollection {
        case eventDetails = 0
        case extraEventDetails = 1
        case proof = 2
        
        func sectionHeaderTitle() -> String {
            switch self {
            case .eventDetails:
                return CommonStrings.Ofe.ClaimPoints.Header1365.Other
            case .proof:
                return CommonStrings.Ofe.Summary.ProofTitle1392
            default:
                return ""
            }
        }
    }
    
    enum SectionsWithAllProofs: Int, EnumCollection {
        case eventDetails = 0
        case extraEventDetails = 1
        case imageProof = 2
        case weblinkProof = 3
        
        func sectionHeaderTitle() -> String {
            switch self {
            case .eventDetails:
                return CommonStrings.Ofe.ClaimPoints.Header1365.Other
            case .imageProof:
                return CommonStrings.Ofe.Proof.SectionHeader1379
            case .weblinkProof:
                return CommonStrings.Ofe.Common.WebLinkProof1381
            default:
                return ""
            }
        }
    }
    
    enum WeblinkProofRow: Int, EnumCollection {
        case link = 0 // Or photos. Since photo collection view only needs 1 row.
        
        func header() -> String {
            switch self {
            case .link:
                return CommonStrings.Ofe.Summary.LinkTitle1393
            }
        }
    }
}

// MARK: View Configuration
extension OFESummaryViewController {
    fileprivate func configureAppearance() {
        navigationController?.makeNavigationBarTransparent()
        self.addRightBarButtonItem(CommonStrings.ConfirmTitleButton182, target: self, selector: #selector(onConfirm))
    }
    
    func onConfirm(sender: UIBarButtonItem) {
        // TODO: Do 'if' here...
        performSegue(withIdentifier: SEGUE_DATA_PRIVACY, sender: self)
        //performSegue(withIdentifier: SEGUE_COMPLETION, sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Insert data manipulation here..
    }
    
    fileprivate func configureTableView() {
        self.tableView.register(VIATableViewSectionHeaderFooterView.nib(), forHeaderFooterViewReuseIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier)
        self.tableView.register(HeaderValueSubtitleTableViewCell.nib(), forCellReuseIdentifier: HeaderValueSubtitleTableViewCell.defaultReuseIdentifier)
        self.tableView.register(ImageCollectionTableViewCell.nib(), forCellReuseIdentifier: ImageCollectionTableViewCell.defaultReuseIdentifier)
        self.tableView.estimatedRowHeight = 100
        self.tableView.sectionHeaderHeight = UITableViewAutomaticDimension
        self.tableView.estimatedSectionHeaderHeight = 500
        
        self.tableView.contentInset = UIEdgeInsets(top: 10, left: 0, bottom: 0, right: 0)
        let view = VIASummaryHeaderView.viewFromNib(owner: self)!
        view.title = viewModel.summaryHeader()
        view.content = viewModel.summaryContent()
        tableView.tableHeaderView = view
        tableView.sizeHeaderViewToFit()
    }
}

// MARK: UITableView DataSource and Delegates
extension OFESummaryViewController {
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier) as? VIATableViewSectionHeaderFooterView else {
            return nil
        }
        
        if VIAApplicableFeatures.default.includeAllProofs() {
            if let sectionValue = SectionsWithAllProofs(rawValue: section) {
                if sectionValue == .extraEventDetails {
                    return nil
                }
                
                view.labelText = sectionValue.sectionHeaderTitle().uppercased()
            }
        } else {
            if let sectionValue = Sections(rawValue: section) {
                if sectionValue == .extraEventDetails {
                    return nil
                }
                
                view.labelText = sectionValue.sectionHeaderTitle().uppercased()
            }
        }
        
        return view
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return nil
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if VIAApplicableFeatures.default.includeAllProofs() {
            let sectionValue = SectionsWithAllProofs(rawValue: indexPath.section)
            
            if sectionValue == .imageProof {
                return imageCollectionCell()
            }
        } else {
            let sectionValue = Sections(rawValue: indexPath.section)
            
            if sectionValue == .proof {
                if usePhotoProofOnSubmit {
                    return imageCollectionCell()
                }
            }
        }
        
        return eventDetailCell(indexPath: indexPath)
    }
}

// MARK: UITableView Custom Methods
extension OFESummaryViewController {
    func eventDetailCell(indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: HeaderValueSubtitleTableViewCell.defaultReuseIdentifier, for: indexPath) as? HeaderValueSubtitleTableViewCell else {
            return tableView.defaultTableViewCell()
        }
        
        cell.accessoryType = .none
        cell.selectionStyle = .none
        cell.setCell(subTitle: "")
        
        let sectionValue = Sections(rawValue: indexPath.section)
        if sectionValue == .eventDetails {
            let eventDetailRow: EventDetail = claimPointsViewModel.eventDetailRows()[indexPath.row]
            let eventDetailRowTypeRef: OFEFeatureTypeRef = eventDetailRow.rowTypeRef!
            
            cell.setCell(title: eventDetailRow.summaryHeader)
            
            switch eventDetailRowTypeRef {
            case .EventType:
                cell.setCell(value: viewModel.ofeCapturedResult?.eventType?.eventTypeName ?? "")
            case .OFEDistance:
                cell.setCell(value: viewModel.ofeCapturedResult?.selectionOption?.featureName ?? "")
                cell.setCell(subTitle: viewModel.pointsText(text: String(viewModel.ofeCapturedResult?.selectionOption?.potentialPoints ?? 0)))
            case .FinishTime:
                cell.setCell(value: viewModel.ofeCapturedResult?.finishTime ?? "")
            case .EventDate:
                cell.setCell(value: dateFormatter.string(from: viewModel.ofeCapturedResult?.eventDate ?? Date()))
            default:
                break
            }
        } else if sectionValue == .extraEventDetails {
            let extraEventDetailRow: EventDetail = claimPointsViewModel.extraEventDetailRows()[indexPath.row]
            let extraEventDetailRowTypeRef: OFEFeatureTypeRef = extraEventDetailRow.rowTypeRef!
            
            cell.setCell(title: extraEventDetailRow.summaryHeader)
            
            switch extraEventDetailRowTypeRef {
            case .EventName:
                cell.setCell(value: viewModel.ofeCapturedResult?.eventName ?? "")
            case .RaceNumber:
                cell.setCell(value: viewModel.ofeCapturedResult?.raceNumber ?? "")
            case .Organiser:
                cell.setCell(value: viewModel.ofeCapturedResult?.organiser ?? "")
            default:
                break
            }
        } else {
            if let weblinkProofRow = WeblinkProofRow(rawValue: indexPath.row) {
                cell.setCell(title: weblinkProofRow.header())
                if let weblink = viewModel.ofeCapturedResult?.weblinkProof {
                    cell.setCell(value: weblink)
                } else {
                    cell.setCell(value: "")
                }
            }
        }
        
        return cell
    }
    
    func imageCollectionCell() -> UITableViewCell {
        if let imageCollectionCell = self.tableView.dequeueReusableCell(withIdentifier: ImageCollectionTableViewCell.defaultReuseIdentifier) {
            let collectionviewCell = self.setUpCaptureImages(cell: imageCollectionCell)
            self.heightForImageCollection(cell: collectionviewCell)
            return collectionviewCell
        }
        
        return UITableViewCell()
    }
    
    func setUpCaptureImages(cell: UITableViewCell) -> ImageCollectionTableViewCell {
        if let imageCollectionViewCell = cell as? ImageCollectionTableViewCell {
            if let capturedImages = viewModel.submittedImages() {
                imageCollectionViewCell.setImageCollection(images: capturedImages)
            }
            imageCollectionViewCell.imageCollectionView?.bounces = false
            imageCollectionViewCell.viewWidth = self.view.frame.width
            imageCollectionViewCell.setupCollectionFlowLayoutSize()
            return imageCollectionViewCell
        }
        return ImageCollectionTableViewCell()
    }
    
    func heightForImageCollection(cell: ImageCollectionTableViewCell) {
        let collectionViewFlowLayout = cell.imageCollectionView?.collectionViewLayout
        let cellPadding = collectionViewFlowLayout?.collectionViewContentSize
        let height = cellPadding?.height
        self.heightForCell = height ?? 0
        cell.updateImageCollectionViewSize(with:height)
    }
    
    func prepareCapturedResultForSubmission() {
        if !VIAApplicableFeatures.default.includeAllProofs() {
            if usePhotoProofOnSubmit {
                claimPointsViewModel.saveCapturedData(weblinkProof: nil)
            } else {
                let ofeRealm = DataProvider.newOFERealm()
                try? ofeRealm.write {
                    ofeRealm.delete(ofeRealm.allPhotosAssets())
                }
            }
        }
    }
}
