//
//  OFEClaimPointsViewController.swift
//  VitalityActive
//
//  Created by Val Tomol on 04/04/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import VIAUIKit
import VIAUtilities
import VitalityKit
import VIACommon
import VIAImageManager

class OFEClaimPointsViewController: VIATableViewController, ImproveYourHealthTintable {
    // MARK: Properties
    fileprivate let SEGUE_EVENT_TYPE = "segueEventType"
    fileprivate let SEGUE_DISTANCE = "segueDistance"
    fileprivate let SEGUE_SELECT_PROOF = "segueSelectProof"
    fileprivate let SEGUE_IMAGE_PROOF = "segueImageProof"
    
    var viewModel = OFEClaimPointsViewModel()
    
    lazy var dateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter.dayDateShortMonthyearFormatter()
        return dateFormatter
    }()
    
    lazy var timeFormatter: DateFormatter = {
        let dateFormatter = DateFormatter.hoursMinutesSecondsFormatter()
        return dateFormatter
    }()
    
    // MARK: View Lifecycles
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = viewModel.claimPointsViewTitle()
        self.configureAppearance()
        self.configureTableView()
        
        addNotificationWithCompletion { [weak self] (notification) in
            self?.navigationController?.dismiss(animated: true, completion: nil)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.hideBackButtonTitle()
        self.disableKeyboardAutoToolbar()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Update view model.
        viewModel = OFEClaimPointsViewModel()
        reloadTableView()
        
        self.configureKeyboardAutoToolbar(VIAApplicableFeatures.default.enableKeyboardAutoToolbar ?? false)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.hasCapturedResults() ? Sections.allValues.count : 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let sectionValue = Sections(rawValue: section)
        
        if sectionValue == .eventDetails {
            return viewModel.hasCapturedResults() ? viewModel.eventDetailRows().count : 1
        } else {
            return viewModel.extraEventDetailRows().count
        }
    }
}

// MARK: Section Enum
extension OFEClaimPointsViewController {
    enum Sections: Int, EnumCollection {
        case eventDetails = 0
        case extraEventDetails = 1
        
        func setionHeaderTitle() -> String {
            switch self {
            case .eventDetails:
                return CommonStrings.Ofe.ClaimPoints.Header1365.Other
            default:
                return ""
            }
        }
    }
}

// MARK: View Configuration
extension OFEClaimPointsViewController {
    fileprivate func configureAppearance() {
        navigationController?.makeNavigationBarTransparent()
        self.addLeftBarButtonItem(CommonStrings.CancelButtonTitle24, target: self, selector: #selector(onCancel))
        self.addRightBarButtonItem(CommonStrings.NextButtonTitle84, target: self, selector: #selector(onNext))
        
        self.navigationItem.rightBarButtonItem?.isEnabled = false
    }
    
    func onCancel(sender: UIBarButtonItem) {
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    
    func onNext(sender: UIBarButtonItem) {
        if VIAApplicableFeatures.default.includeAllProofs() {
            performSegue(withIdentifier: SEGUE_IMAGE_PROOF, sender: self)
        } else {
            performSegue(withIdentifier: SEGUE_SELECT_PROOF, sender: self)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Insert data manipulation here..
        if segue.identifier == SEGUE_IMAGE_PROOF, let destination = segue.destination as? SelectedPhotosCollectionViewController {
            destination.viewModel = OFESelectedPhotosCollectionViewModel()
            destination.delegate = self
            
        }
    }
    
    fileprivate func configureTableView() {
        self.tableView.register(TitleImageDescriptionHeaderView.nib(), forHeaderFooterViewReuseIdentifier: TitleImageDescriptionHeaderView.defaultReuseIdentifier)
        self.tableView.register(VIARightDetailTableViewCell.nib(), forCellReuseIdentifier: VIARightDetailTableViewCell.defaultReuseIdentifier)
        self.tableView.register(VIATextFieldTableViewCell.nib(), forCellReuseIdentifier: VIATextFieldTableViewCell.defaultReuseIdentifier)
        self.tableView.register(VIAEmailTextFieldTableViewCell.nib(), forCellReuseIdentifier: VIAEmailTextFieldTableViewCell.defaultReuseIdentifier)
        self.tableView.estimatedRowHeight = 100
        self.tableView.sectionHeaderHeight = UITableViewAutomaticDimension
        self.tableView.estimatedSectionHeaderHeight = 500
        
        self.tableView.contentInset = UIEdgeInsets(top: 10, left: 0, bottom: 0, right: 0)
        let view = TableViewHeaderView.viewFromNib(owner: self)!
        view.frame.size.height = 250
        view.header = viewModel.headerTitle()	
        view.content = viewModel.contentTitle()
        tableView.tableHeaderView = view
    }
}

// MARK: UITableView DataSource and Delegates
extension OFEClaimPointsViewController {
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: TitleImageDescriptionHeaderView.defaultReuseIdentifier) as? TitleImageDescriptionHeaderView else {
            return nil
        }
        
        if let sectionValue = Sections(rawValue: section) {
            if sectionValue == Sections.extraEventDetails {
                return nil
            }
            
            view.title = sectionValue.setionHeaderTitle()
            view.descriptionText = nil
        }
        
        return view
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return nil
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let sectionValue = Sections(rawValue: indexPath.section)
        
        if sectionValue == .eventDetails {
            return eventDetailCell(indexPath: indexPath)
        } else {
            return extraEventDetailCell(indexPath: indexPath)
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let sectionValue = Sections(rawValue: indexPath.section)
        
        if sectionValue == .eventDetails {
            if let rowTypeRef: OFEFeatureTypeRef = viewModel.eventDetailRows()[indexPath.row].rowTypeRef {
                switch rowTypeRef {
                case .EventType:
                    eventTypeTapped()
                case .OFEDistance:
                    distanceTapped()
                case .FinishTime:
                    showDatePicker(mode: .countDownTimer)
                case .EventDate:
                    showDatePicker(mode: .date)
                default:
                    return
                }
            }
        }
    }
}

// MARK: UITableView Custom Methods
extension OFEClaimPointsViewController {
    func eventDetailCell(indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: VIARightDetailTableViewCell.defaultReuseIdentifier, for: indexPath) as? VIARightDetailTableViewCell else {
            return tableView.defaultTableViewCell()
        }
        
        cell.accessoryType = .none
        
        let eventDetailRow: EventDetail = viewModel.eventDetailRows()[indexPath.row]
        let eventDetailRowTypeRef: OFEFeatureTypeRef = eventDetailRow.rowTypeRef!
        
        cell.setTitle(eventDetailRow.text)
        cell.setPlaceholder(eventDetailRow.placeholder)
        
        if eventDetailRowTypeRef == .EventType || eventDetailRowTypeRef == .OFEDistance {
            cell.accessoryType = .disclosureIndicator
        }
        
        let capturedResult = viewModel.ofeCapturedResult
        var finishTimeValue: String? {
            return capturedResult?.finishTime != nil ? capturedResult?.finishTime : ""
        }
        var eventDateValue: String? {
            if VIAApplicableFeatures.default.withDefaultStringValueForOFEEventDate() {
                let defaultValue = CommonStrings.Assessment.DateTested.DefaultValue2149
                return capturedResult?.eventDate != nil ? dateFormatter.string(from: (capturedResult?.eventDate)!) : defaultValue
            } else {
                return capturedResult?.eventDate != nil ? dateFormatter.string(from: (capturedResult?.eventDate)!) : ""
            }
        }
        
        switch eventDetailRowTypeRef {
        case .EventType:
            cell.setValue(capturedResult?.eventType?.eventTypeName)
        case .OFEDistance:
            cell.setValue(capturedResult?.selectionOption?.featureName)
        case .FinishTime:
            cell.setValue(finishTimeValue)
        case .EventDate:
            cell.setValue(eventDateValue)
        default:
            break
        }
        
        return cell
    }
    
    func extraEventDetailCell(indexPath: IndexPath) -> UITableViewCell {
        guard let cell = VIAApplicableFeatures.default.getTextFieldViewCell(tableView: tableView, indexPath: indexPath) as? VIATextFieldTableViewCell else {
            return tableView.defaultTableViewCell()
        }
        
        cell.accessoryType = .none
        cell.selectionStyle = .none
        
        let extraEventDetailRow: EventDetail = viewModel.extraEventDetailRows()[indexPath.row]
        let extraEventDetailRowTypeRef: OFEFeatureTypeRef = extraEventDetailRow.rowTypeRef!
        
        var textFieldText: String? = ""
        if extraEventDetailRowTypeRef == .EventName {
            textFieldText = viewModel.ofeCapturedResult?.eventName
        } else if extraEventDetailRowTypeRef == .RaceNumber {
            textFieldText = viewModel.ofeCapturedResult?.raceNumber
        }
        
        switch extraEventDetailRowTypeRef {
        case .EventName:
            textFieldText = viewModel.ofeCapturedResult?.eventName
        case .RaceNumber:
            textFieldText = viewModel.ofeCapturedResult?.raceNumber
        case .Organiser:
            textFieldText = viewModel.ofeCapturedResult?.organiser
        default:
            break
        }
        
        cell.setHeadingLabelText(text: extraEventDetailRow.text)
        cell.setTextFieldPlaceholder(placeholder: extraEventDetailRow.placeholder)
        cell.setTextFieldText(text: textFieldText)
        cell.textField.returnKeyType = .done
        cell.hideCellImage()
        configureTextFieldListener(cell: cell, indexPath: indexPath)
        
        return cell
    }
    
    func eventTypeTapped() {
        performSegue(withIdentifier: SEGUE_EVENT_TYPE, sender: self)
    }
    
    func distanceTapped() {
        performSegue(withIdentifier: SEGUE_DISTANCE, sender: self)
    }
    
    func showDatePicker(mode: UIDatePickerMode) {
        let datePicker: UIDatePicker = UIDatePicker()
        datePicker.timeZone = NSTimeZone.local
        datePicker.datePickerMode = mode
        datePicker.frame = CGRect(x: 0, y: 15, width: 270, height: 200)
        
        if datePicker.datePickerMode == .date {
            datePicker.maximumDate = Date()
            datePicker.minimumDate = VIAApplicableFeatures.default.setMinimumDate() ?? Date()
        }
        
        let alertController = UIAlertController(title: "\n\n\n\n\n\n\n\n", message: nil, preferredStyle: .alert)
        alertController.view.addSubview(datePicker)
        alertController.addAction(UIAlertAction(title: CommonStrings.CancelButtonTitle24, style: .cancel, handler: nil))
        alertController.addAction(UIAlertAction(title: CommonStrings.OkButtonTitle40, style: .default, handler: { _ in
            self.captureDatePickerResult(date: datePicker.date, mode: mode)
        }))
        
        self.present(alertController, animated: true, completion:nil)
    }
    
    func captureDatePickerResult(date: Date, mode: UIDatePickerMode) {
        var pickedTime: String? {
            return mode == .countDownTimer ? timeFormatter.string(from: date) : viewModel.ofeCapturedResult?.finishTime
        }
        var pickedDate: Date? {
            var dateValue: Date?
            if let tenantID = AppSettings.getAppTenant(), tenantID == .SLI {
                let calendar        = Calendar.current
                let currentDate     = Date()
                let selectedDate    = date
                
                var dateComponents      = DateComponents()
                dateComponents.year     = calendar.component(.year,     from: selectedDate)
                dateComponents.month    = calendar.component(.month,    from: selectedDate)
                dateComponents.day      = calendar.component(.day,      from: selectedDate)
                dateComponents.hour     = calendar.component(.hour,     from: currentDate)
                dateComponents.minute   = calendar.component(.minute,   from: currentDate)
                dateComponents.second   = calendar.component(.second,   from: currentDate)
                
                if let formattedDateTime = calendar.date(from: dateComponents){
                    dateValue = formattedDateTime
                }else{
                    dateValue = selectedDate
                }
            } else {
                dateValue = date
            }
            
            return mode == .date ? dateValue : viewModel.ofeCapturedResult?.eventDate
        }
        
        viewModel.saveCapturedData(finishTime: pickedTime,
                                   eventDate: pickedDate)
        reloadTableView()
    }
}

// MARK: UITextField Delegates
extension OFEClaimPointsViewController {
    func configureTextFieldListener(cell: VIATextFieldTableViewCell, indexPath: IndexPath) {
        let extraEventDetailRow: EventDetail = viewModel.extraEventDetailRows()[indexPath.row]
        let extraEventDetailRowTypeRef: OFEFeatureTypeRef = extraEventDetailRow.rowTypeRef!
        
        setTextFieldDelegates(cell: cell, rowTypeRef: extraEventDetailRowTypeRef)
    }
    
    func setTextFieldDelegates(cell: VIATextFieldTableViewCell, rowTypeRef: OFEFeatureTypeRef) {
        cell.textFieldDidEndEditing = { [weak self] textField in
            self?.textFieldDidEndEditingOn(rowTypeRef: rowTypeRef, textField: textField)
        }
        cell.textFieldShouldReturn = { [weak self] textField in
            (self?.textFieldShouldReturnOn(rowTypeRef: rowTypeRef, textField: textField))!
        }
        cell.textFieldTextDidChange = { [weak self] textField in
            if (textField.text?.isEmpty)! {
                self?.navigationItem.rightBarButtonItem?.isEnabled = false
            } else {
                return
            }
        }
    }
    
    func textFieldDidEndEditingOn(rowTypeRef: OFEFeatureTypeRef, textField: UITextField) {
        captureTextFieldResult(rowTypeRef: rowTypeRef, textField: textField)
    }
    
    func textFieldShouldReturnOn(rowTypeRef: OFEFeatureTypeRef, textField: UITextField) -> Bool {
        captureTextFieldResult(rowTypeRef: rowTypeRef, textField: textField)
        return true
    }
    
    func captureTextFieldResult(rowTypeRef: OFEFeatureTypeRef, textField: UITextField) {
        var text: String? {
            return !(textField.text?.isEmpty)! ? textField.text : nil
        }
        
        if rowTypeRef == .EventName {
            viewModel.saveCapturedData(eventName: text)
        } else if rowTypeRef == .RaceNumber {
            viewModel.saveCapturedData(raceNumber: text)
        } else if rowTypeRef == .Organiser {
            viewModel.saveCapturedData(organiser: text)
        }
        
        updateTableView()
    }
}

// MARK: Custom Methods
extension OFEClaimPointsViewController {
    func reloadTableView() {
        manipulateRightBarButton()
        tableView.reloadData()
    }
    
    func updateTableView() {
        manipulateRightBarButton()
        tableView.beginUpdates()
        // No animation or other calls needed since we are just updating the content of the textfield.
        tableView.endUpdates()
    }
    
    func manipulateRightBarButton() {
        self.navigationItem.rightBarButtonItem?.isEnabled = false
        
        if viewModel.hasCompletedForm() {
            self.navigationItem.rightBarButtonItem?.isEnabled = true
        }
    }
}

extension OFEClaimPointsViewController: SelectedPhotosCollectionViewDelegate{
    
    public func onPerformSegue(){
        self.performSegue(withIdentifier: "segueWeblinkProof", sender: self)
    }
}
