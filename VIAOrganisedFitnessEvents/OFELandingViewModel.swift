//
//  OFELandingViewModel.swift
//  VitalityActive
//
//  Created by Val Tomol on 16/04/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import VitalityKit
import VIAUIKit
import VIAUtilities
import RealmSwift

public class OFELandingViewModel: AnyObject {
    // TODO: Interim - Get last event workaround using history data
    // https://jira.vitalityservicing.com/browse/VA-32063
    let realm = DataProvider.newRealm()
    let ofeRealm = DataProvider.newOFERealm()
    var lastClaimedEvent = OFEEventsAndPoints()
    
    // MARK: Properties
    
    // MARK: Functions
    func landingViewTitle() -> String {
        return CommonStrings.Ofe.Heading.Title1335
    }
    
    func fetchOFEFeature(forceUpdate: Bool = false, completion: @escaping (Error?) -> Void) {
        // TODO: Implement OFECache
        if forceUpdate {
            let tenantId = DataProvider.newRealm().getTenantId()
            let partyId = DataProvider.newRealm().getPartyId()
            let vitalityMembershipId = DataProvider.newRealm().getMembershipId()
            
            Wire.Events.getOFEFeature(tenantId: tenantId, partyId: partyId, vitalityMembershipId: vitalityMembershipId) { (error) in
                guard error == nil else {
                    completion(error)
                    return
                }
                completion(nil)
            }
        } else {
            completion(nil)
        }
    }
}

// TODO: Interim - Get last event workaround using history data
// https://jira.vitalityservicing.com/browse/VA-32063
extension OFELandingViewModel {
    func fetchHistory(completion: @escaping () -> Void) {
        let partyId = realm.getPartyId()
        let tenantId = realm.getTenantId()
        let agreementId = realm.getMembershipId()
        let agreementTypeKey = 4
        let offsetForward = 0
        let offsetPast = 1
        
        Wire.Member.getAgreementPeriod(tenantId: tenantId, agreementId: agreementId, agreementTypeKey: agreementTypeKey, offsetForward: offsetForward, offsetPast: offsetPast) { (_, error, _) in
            guard error == nil else {
                completion()
                return
            }
            
            guard self.ofeRealm.allAgreementPeriod().count != 0 else {
                completion()
                return
            }
            
            for agreementPeriod in self.ofeRealm.allAgreementPeriod() {
                
                guard agreementPeriod.effectivePeriods.count != 0 else {
                    completion()
                    continue
                }
                
                for effectivePeriod in agreementPeriod.effectivePeriods {
                    let effectiveFrom = effectivePeriod.effectiveFrom
                    let effectiveTo = effectivePeriod.effectiveTo
                    
                    Wire.Member.getEventsPoints(tenantId: tenantId, partyId: partyId, effectiveFrom: effectiveFrom, effectiveTo: effectiveTo, typeKeys: self.retriveEventTypes()) { (_, error, _) in
                        guard error == nil else {
                            completion()
                            return
                        }
                        
                        if let lastEvent = self.ofeRealm.allEventsAndPoints().sorted(by: { $0.id < $1.id }).last {
                            self.lastClaimedEvent = lastEvent
                        }
                        
                        completion()
                    }
                }
            }
        }
    }
    
    func retriveEventTypes() -> [Int] {
        var eventTypes = [Int]()
        for event in ofeRealm.allOFEEventTypes() {
            eventTypes.append(event.eventTypeKey)
        }
        return eventTypes
    }
    
    func hasLastClaimedEvent() -> Bool {
        if !lastClaimedEvent.isInvalidated {
            return (lastClaimedEvent.id != 0) ? true : false
        } else {
            return false
        }
    }
    
    func getLastClaimedEventTypeName() -> String {
        return lastClaimedEvent.typeName
    }
    
    func getLastClaimedEventDistance() -> String {        
        for eventMetaData in lastClaimedEvent.eventMetadatas {
            if eventMetaData.typeKey == OFEFeatureTypeRef.OFEDistance.rawValue {
                for selectionOption in ofeRealm.allOFESelectionOptions() {
                    if selectionOption.featureKey == Int(eventMetaData.value) {
                        return selectionOption.featureName
                    }
                }
            }
        }
        
        return ""
    }
    
    func getLastClaimedEventSubmissionDate() -> String {
        let stringToDateFormatterWithMilliseconds: DateFormatter = {
            let dateFormatter = DateFormatter.apiManagerFormatterWithMilliseconds(localeIdentifier: DeviceLocale.toString())
            return dateFormatter
        }()
        
        let stringToDateFormatter: DateFormatter = {
            let dateFormatter = DateFormatter.apiManagerFormatter(localeIdentifier: DeviceLocale.toString())
            return dateFormatter
        }()
        
        let dateToStringFormatter: DateFormatter = {
            let dateFormatter = DateFormatter.dayDateShortMonthyearFormatter()
            return dateFormatter
        }()
        
        
        let stringDate = lastClaimedEvent.dateLogged

        if !stringDate.isEmpty {
            if let dateMill = stringToDateFormatterWithMilliseconds.date(from: stringDate) {
                return dateToStringFormatter.string(from: dateMill)
            } else if let date = stringToDateFormatter.date(from: stringDate) {
                return dateToStringFormatter.string(from: date)
            } else {
                return ""
            }
        }
        
        return ""
    }
}
