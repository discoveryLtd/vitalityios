//
//  OFEHistoryViewController.swift
//  VitalityActive
//
//  Created by OJ Garde on 4/13/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import VIAUIKit
import VIACommon
import VIAUtilities
import VitalityKit

import RealmSwift

public class OFEHistoryViewController: VIATableViewController {
    // MARK: Properties
    fileprivate let SEGUE_DETAILS = "segueDetails"
    
    let viewModel = OFEHistoryViewModel()
    
    lazy var dateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter.dayDateShortMonthyearFormatter()
        return dateFormatter
    }()
    
    lazy var timeFormatter: DateFormatter = {
        let dateFormatter = DateFormatter.hoursMinutesSecondsFormatter()
        return dateFormatter
    }()
    
    var getEventsPointsResponse: OFEGetEventsPointsResponse?
    var realm = DataProvider.newOFERealm()
    var eventsPoints = [EventsPoints]()
    var selectedEventsPoint: EventsPoints?
    var imageProofs: [UIImage] = []
    var detailTitle: String?
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        
        configureTable()
        performInitialRequest()
        
        // TO DO - Still need to follow to MVVM coding standard
    }
    
    override public func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.title = viewModel.historyViewTitle()
    }
    
    override public func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override public func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.hideBackButtonTitle()
    }
    
    
    public override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    public override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return eventsPoints.count
    }
}

extension OFEHistoryViewController {
    public override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    public override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return tableView.dequeueReusableHeaderFooterView(withIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier) ?? nil
    }
    
    public override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier) as? VIATableViewSectionHeaderFooterView{
            view.labelText = CommonStrings.Ofe.History.EventDetails1397.uppercased()
            return view
        }
        return nil
    }
    
    public override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    public override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    public override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: OFEHistoryEventCell.defaultReuseIdentifier, for: indexPath) as! OFEHistoryEventCell
        
        cell.labelText = VIAApplicableFeatures.default.getOFEFormattedDate(dateString: eventsPoints[indexPath.row].activityDate!)
        cell.accessoryType = .disclosureIndicator
        return cell
    }
    
    public override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedEventsPoint = eventsPoints[indexPath.row]
        self.viewDetails()
    }
    
    override public func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == SEGUE_DETAILS {
            guard let historyDetailsViewController = segue.destination as? OFEHistoryDetailsViewController else { return }
            historyDetailsViewController.title = self.detailTitle
            historyDetailsViewController.eventsPoint = self.selectedEventsPoint
            historyDetailsViewController.imageProofs = self.imageProofs
        }
    }
}

extension OFEHistoryViewController {
    // TODO add argument for the view model of the detail
    fileprivate func viewDetails() {
        showHUDOnView(view: self.view)
        viewModel.fetchUploadedImages(eventsPoint: self.selectedEventsPoint!) { [weak self] images in
            self?.hideHUDFromView(view: self?.view)
            self?.imageProofs = images
            self?.performSegue(withIdentifier: (self?.SEGUE_DETAILS)!, sender: self)
        }
    }
    
    func performInitialRequest() {
        self.showHUDOnView(view: self.view)
        startGetAgreementPeriodRequest { [weak self] in
            self?.retrieveAgreementPeriod()
        }
    }
    
    func startGetAgreementPeriodRequest(completion: (() -> Void)?) {
        let tenantId = DataProvider.newRealm().getTenantId()
        let agreementId = DataProvider.newRealm().getMembershipId()
        let agreementTypeKey = 4
        let offsetForward = 0
        let offsetPast = 1
        
        Wire.Member.getAgreementPeriod(tenantId: tenantId, agreementId: agreementId, agreementTypeKey: agreementTypeKey, offsetForward: offsetForward, offsetPast: offsetPast) { [weak self] (result, error, useCutomError) in
            if error != nil {
                if useCutomError! {
                    self?.showUnableToUpdateError()
                } else {
                    self?.handleErrorOccurred(error!, isCacheOutdated: false)
                }
                debugPrint("Points history parsing error encountered: \(String(describing: error))")
            } else {
                debugPrint("Points history parsing done")
            }
            
            if let completionToExecute = completion {
                completionToExecute()
            }
        }
    }
    
    func startEVENTPointsHistoryRequest(effectiveFrom: String, effectiveTo: String, completion: (() -> Void)?) {
        let partyId = DataProvider.newRealm().getPartyId()
        let tenantId = DataProvider.newRealm().getTenantId()
        
        Wire.Member.getEventsPoints(tenantId: tenantId, partyId: partyId, effectiveFrom: effectiveFrom, effectiveTo: effectiveTo, typeKeys: retriveEventTypes()) { [weak self] (result, error, useCutomError) in
            if error != nil {
                if useCutomError! {
                    self?.showUnableToUpdateError()
                } else {
                    self?.handleErrorOccurred(error!, isCacheOutdated: false)
                }
                debugPrint("Points history parsing error encountered: \(String(describing: error))")
            } else {
                debugPrint("Points history parsing done")
            }
            
            self?.eventsPoints.removeAll()
            self?.retrieveData()
            self?.tableView.reloadData()
            
            if self?.eventsPoints.count == 0 {
                self?.configureEmptyStatusView()
            } else {
                self?.removeEmptyStatusView()
            }
            
            if let completionToExecute = completion {
                completionToExecute()
            }
        }
    }
    
    func configureEmptyStatusView() {
        removeEmptyStatusView()
        let view = VIAStatusView.viewFromNib(owner: self)!
        
        view.heading = CommonStrings.Ofe.Common.NoHistory1361
        view.message = CommonStrings.Ofe.Common.NoHistoryMessage1362 
        self.view.addSubview(view)
        
        view.snp.makeConstraints { (make) in
            make.height.equalToSuperview()
            make.width.equalToSuperview()
            make.left.right.top.bottom.equalToSuperview()
        }
                return
        }


    func removeEmptyStatusView() {
        self.view.subviews.forEach { (view) in
            if view is VIAStatusView {
                view.removeFromSuperview()
            }
        }
    }
    
    
    
    
    func retrieveAgreementPeriod(){
        for agreementPeriod in realm.allAgreementPeriod() {
            for effectivePeriod in agreementPeriod.effectivePeriods {
                let effectiveFrom = effectivePeriod.effectiveFrom
                let effectiveTo = effectivePeriod.effectiveTo
                
                startEVENTPointsHistoryRequest(effectiveFrom: effectiveFrom, effectiveTo: effectiveTo) { [weak self] in
                    self?.hideHUDFromView(view: self?.view)
                }
            }
        }
    }
    
    func retriveEventTypes() -> [Int]{
        var eventTypes = [Int]()
        for event in realm.allOFEEventTypes() {
            eventTypes.append(event.eventTypeKey)
        }
        return eventTypes
    }
    
    func retrieveData() {
        for eventsPoint in realm.allEventsAndPoints() {
            var eventMetadatas = [EventMetadata]()
            for eventMetadata in eventsPoint.eventMetadatas {
                eventMetadatas.append(EventMetadata(typeCode: eventMetadata.typeCode, typeKey: eventMetadata.typeKey, typeName: eventMetadata.typeName, unitOfMeasure: eventMetadata.unitOfMeasure, value: eventMetadata.value))
            }

            eventsPoints.append(EventsPoints(id: eventsPoint.id,
                                             activityDate: eventsPoint.activityDate,
                                             dateLogged: eventsPoint.dateLogged,
                                             typeName: eventsPoint.typeName,
                                             typeCode: eventsPoint.typeCode,
                                             typeKey: eventsPoint.typeKey,
                                             eventMetadatas: eventMetadatas))
        }
    }
    
    func handleErrorOccurred(_ error: Error, isCacheOutdated: Bool?) {
        let backendError = error as? BackendError ?? BackendError.other
        if let outdated = isCacheOutdated, outdated == true {
            let statusView = self.handleBackendErrorWithStatusView(backendError, tryAgainAction: { [weak self] in
                self?.performInitialRequest()
            })
            configureStatusView(statusView)
        } else {
            self.handleBackendErrorWithAlert(backendError, tryAgainAction: { [weak self] in
                self?.performInitialRequest()
            })
        }
    }
    
    fileprivate func showUnableToUpdateError() {
        let title = CommonStrings.Pm.AlertUnableToUpdateTitle557
        let message = CommonStrings.Pm.AlertUnableToUpdateMessage558
        var actions:[UIAlertAction] = []
        actions.append(UIAlertAction(title: CommonStrings.CancelButtonTitle24, style: .cancel, handler: nil))
        actions.append(UIAlertAction(title: CommonStrings.TryAgainButtonTitle43, style: .default) { [weak self] (action) in
            self?.tableView.reloadData()
        })
        
        showAlertDialog(title: title, message: message, alertActions: actions)
    }
    
    fileprivate func showAlertDialog(title:String? = nil, message:String? = nil,
                                     alertActions:[UIAlertAction]){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        for alertAction in alertActions{
            alert.addAction(alertAction)
        }
        alert.view.tintColor = UIButton().tintColor
        self.present(alert, animated: true, completion: nil)
    }
}

extension OFEHistoryViewController {
    fileprivate func configureTable() {
        self.tableView.register(OFEHistoryEventCell.nib(), forCellReuseIdentifier: OFEHistoryEventCell.defaultReuseIdentifier)
        self.tableView.register(VIATableViewSectionHeaderFooterView.nib(), forHeaderFooterViewReuseIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier)
        
        self.tableView.separatorInset = UIEdgeInsets(top: 0, left: 55, bottom: 0, right: 0)
        
        self.tableView.refreshControl = UIRefreshControl()
        self.tableView.refreshControl?.tintColor = UIColor.cellSeperator()
        self.tableView.refreshControl?.addTarget(self, action: #selector(performRefresh), for: .valueChanged)
    }
    
    func performRefresh() {
        self.tableView.refreshControl?.endRefreshing()
    }
}


