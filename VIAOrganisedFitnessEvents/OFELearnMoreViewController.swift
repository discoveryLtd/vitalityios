//
//  OFELearnMoreViewController.swift
//  VitalityActive
//
//  Created by Harry C. Lingad on 4/11/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import VIAUIKit
import VitalityKit
import VIAUtilities
import VIACommon


public final class OFELearnMoreViewController: VIATableViewController, ImproveYourHealthTintable {
    
    let viewModel = OFELearnMoreViewModel()
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = viewModel.title
        self.configureAppearance()
        self.configureTableView()
    }
    
    override public func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.hideBackButtonTitle()
    }
    
    func configureAppearance() {
        navigationController?.makeNavigationBarTransparent()
        navigationController?.navigationBar.barTintColor = UIColor.white
    }
    
    func configureTableView() {
        self.tableView.register(VIAGenericContentCell.nib(), forCellReuseIdentifier: VIAGenericContentCell.defaultReuseIdentifier)
        self.tableView.register(VIATableViewCell.nib(), forCellReuseIdentifier: VIATableViewCell.defaultReuseIdentifier)
        self.tableView.estimatedRowHeight = 75
        self.tableView.sectionHeaderHeight = 0
        self.tableView.sectionFooterHeight = 0
        self.tableView.estimatedSectionHeaderHeight = 0
        self.tableView.contentInset = UIEdgeInsets(top: 25, left: 0, bottom: 0, right: 0)
        self.tableView.backgroundColor = .white
        self.tableView.tableFooterView = UIView()
        self.tableView.separatorStyle = UITableViewCellSeparatorStyle.none
        /* Set this to remove the preceeding empty rows in the table. */
    }
    
    enum Sections: Int, EnumCollection {
        case Content = 0
    }
    

    
    public override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {

        return UITableViewAutomaticDimension

    }
    
    
    override public func numberOfSections(in tableView: UITableView) -> Int {
        return Sections.allValues.count
    }
    
    override public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let section = Sections(rawValue: section)
        if section == .Content {
            return viewModel.contentItems.count
        }
        return 0
    }
    
    // MARK: UITableView delegate
    
    override public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let section = Sections(rawValue: indexPath.section)
        if section == .Content {
            return self.configureContentCell(indexPath)
        }
        
        return tableView.dequeueReusableCell(withIdentifier: VIATableViewCell.defaultReuseIdentifier, for: indexPath)
    }
    
    override public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // TO DO
    }
    
    func configureContentCell(_ indexPath: IndexPath) -> VIAGenericContentCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: VIAGenericContentCell.defaultReuseIdentifier, for: indexPath) as! VIAGenericContentCell
        cell.isUserInteractionEnabled = false
        
        let item = viewModel.contentItems[indexPath.row]
        
        if indexPath.row == 0 {
            cell.customHeaderFont = viewModel.headingTitleFont
            cell.customContentFont = viewModel.headingMessageFont
        } else {
            cell.customHeaderFont = viewModel.sectionTitleFont
            cell.customContentFont = viewModel.sectionMessageFont
        }
        
        cell.header = item.header
        cell.content = item.content
        cell.cellImage = item.image
        cell.layoutIfNeeded()
        return cell
    }
    

    
    func showDisclaimer(_ sender: Any) {
        self.performSegue(withIdentifier: "showDisclaimer", sender: self)
    }
    
    override public func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // TO DO
    }
    
}

