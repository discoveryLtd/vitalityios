// Generated using SwiftGen, by O.Halligon — https://github.com/SwiftGen/SwiftGen

#if os(OSX)
import AppKit.NSImage
public typealias VIAOrganisedFitnessEventsColor = NSColor
public typealias VIAOrganisedFitnessEventsImage = NSImage
#elseif os(iOS) || os(tvOS) || os(watchOS)
import UIKit.UIImage
public typealias VIAOrganisedFitnessEventsColor = UIColor
public typealias VIAOrganisedFitnessEventsImage = UIImage
#endif

// swiftlint:disable file_length

public typealias VIAOrganisedFitnessEventsAssetType = VIAOrganisedFitnessEventsImageAsset

public struct VIAOrganisedFitnessEventsImageAsset: Equatable {
	fileprivate var name: String

	public var templateImage: VIAOrganisedFitnessEventsImage {
		let bundle = Bundle(for: BundleToken.self)
		#if os(iOS) || os(tvOS)
		let image = VIAOrganisedFitnessEventsImage(named: name, in: bundle, compatibleWith: nil)?.withRenderingMode(.alwaysTemplate)
		#elseif os(OSX)
		let image = bundle.image(forResource: name).withRenderingMode(.alwaysTemplate)
		#elseif os(watchOS)
		let image = VIAOrganisedFitnessEventsImage(named: name).withRenderingMode(.alwaysTemplate)
		#endif
		guard let result = image else { fatalError("Unable to load template image named \(name).") }
		return result
	}

	public var image: VIAOrganisedFitnessEventsImage {
		let bundle = Bundle(for: BundleToken.self)
		#if os(iOS) || os(tvOS)
		let image = VIAOrganisedFitnessEventsImage(named: name, in: bundle, compatibleWith: nil)
		#elseif os(OSX)
		let image = bundle.image(forResource: name)
		#elseif os(watchOS)
		let image = VIAOrganisedFitnessEventsImage(named: name)
		#endif
		guard let result = image else { fatalError("Unable to load image named \(name).") }
		return result
	}

	public static func ==(lhs: VIAOrganisedFitnessEventsImageAsset, rhs: VIAOrganisedFitnessEventsImageAsset) -> Bool {
        return lhs.image == rhs.image
    }
}

public struct VIAOrganisedFitnessEventsColorAsset {
fileprivate var name: String

#if swift(>=3.2)
@available(iOS 11.0, tvOS 11.0, watchOS 4.0, OSX 10.13, *)
var color: VIAOrganisedFitnessEventsColor {
return VIAOrganisedFitnessEventsColor(asset: self)
}
#endif
}

// swiftlint:disable identifier_name line_length nesting type_body_length type_name
public enum VIAOrganisedFitnessEventsAsset {
  public static let weblinkProofBig = VIAOrganisedFitnessEventsImageAsset(name: "weblinkProofBig")
  public static let events = VIAOrganisedFitnessEventsImageAsset(name: "events")
  public static let history = VIAOrganisedFitnessEventsImageAsset(name: "history")
  public static let weblinkProof = VIAOrganisedFitnessEventsImageAsset(name: "weblinkProof")
  public static let rewardSmall = VIAOrganisedFitnessEventsImageAsset(name: "rewardSmall")
  public static let ofeCheckMark = VIAOrganisedFitnessEventsImageAsset(name: "ofeCheckMark")
  public static let questionMarkSmall = VIAOrganisedFitnessEventsImageAsset(name: "questionMarkSmall")
  public static let calendar50 = VIAOrganisedFitnessEventsImageAsset(name: "calendar50")
  public static let learnMoreSmall = VIAOrganisedFitnessEventsImageAsset(name: "learnMoreSmall")
  public static let manRunningSmall = VIAOrganisedFitnessEventsImageAsset(name: "manRunningSmall")
  public static let cameraLarge = VIAOrganisedFitnessEventsImageAsset(name: "cameraLarge")
  public static let fitnessEvents = VIAOrganisedFitnessEventsImageAsset(name: "fitnessEvents")
  public static let imageProof = VIAOrganisedFitnessEventsImageAsset(name: "imageProof")
  public static let rewards50 = VIAOrganisedFitnessEventsImageAsset(name: "rewards50")
  public static let imgTrailRunning = VIAOrganisedFitnessEventsImageAsset(name: "imgTrailRunning")
  public static let addSmall = VIAOrganisedFitnessEventsImageAsset(name: "addSmall")
  public static let trashSmall = VIAOrganisedFitnessEventsImageAsset(name: "trashSmall")

  // swiftlint:disable trailing_comma
  public static let allColors: [VIAOrganisedFitnessEventsColorAsset] = [
  ]
  public static let allImages: [VIAOrganisedFitnessEventsImageAsset] = [
    weblinkProofBig,
    events,
    history,
    weblinkProof,
    rewardSmall,
    ofeCheckMark,
    questionMarkSmall,
    calendar50,
    learnMoreSmall,
    manRunningSmall,
    cameraLarge,
    fitnessEvents,
    imageProof,
    rewards50,
    imgTrailRunning,
    addSmall,
    trashSmall,
  ]
  // swiftlint:enable trailing_comma
  @available(*, deprecated, renamed: "allImages")
  public static let allValues: [VIAOrganisedFitnessEventsAssetType] = allImages
}
// swiftlint:enable identifier_name line_length nesting type_body_length type_name

extension VIAOrganisedFitnessEventsImage {
@available(iOS 1.0, tvOS 1.0, watchOS 1.0, *)
@available(OSX, deprecated,
message: "This initializer is unsafe on macOS, please use the VIAOrganisedFitnessEventsImageAsset.image property")
convenience init!(asset: VIAOrganisedFitnessEventsAssetType) {
#if os(iOS) || os(tvOS)
let bundle = Bundle(for: BundleToken.self)
self.init(named: asset.name, in: bundle, compatibleWith: nil)
#elseif os(OSX) || os(watchOS)
self.init(named: asset.name)
#endif
}
}

extension VIAOrganisedFitnessEventsColor {
#if swift(>=3.2)
@available(iOS 11.0, tvOS 11.0, watchOS 4.0, OSX 10.13, *)
convenience init!(asset: VIAOrganisedFitnessEventsColorAsset) {
let bundle = Bundle(for: BundleToken.self)
#if os(iOS) || os(tvOS)
self.init(named: asset.name, in: bundle, compatibleWith: nil)
#elseif os(OSX)
self.init(named: asset.name, bundle: bundle)
#elseif os(watchOS)
self.init(named: asset.name)
#endif
}
#endif
}

private final class BundleToken {}
