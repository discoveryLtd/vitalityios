//
//  OFEDataPrivacyViewController.swift
//  VitalityActive
//
//  Created by OJ Garde on 4/13/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation

import VitalityKit
import VIAUtilities
import VIAUIKit
import VIACommon

public class OFEDataPrivacyViewController: CustomTermsConditionsViewController, ImproveYourHealthTintable {
    // MARK: Properties
    fileprivate let SEGUE_COMPLETION = "segueCompletion"
    
    let eventsProcessor = OFESubmissionHelper()
    
    // MARK: View Lifecycles
    override open func viewDidLoad() {
        super.viewDidLoad()
        
        // Get content model.
        self.viewModel = CustomTermsConditionsViewModel(articleId: AppConfigFeature.contentId(for: .VHCDSConsentContent))
        
        rightButtonTitle = VIAApplicableFeatures.default.getDataSharingRightBarButtonTitle()
        leftButtonTitle = VIAApplicableFeatures.default.getDataSharingLeftBarButtonTitle()
        
    }
    
    override public func configureAppearance() {
        super.configureAppearance()
        
        navigationController?.makeNavigationBarInvisible()
        navigationController?.navigationBar.isHidden = true
    }
    
    // MARK: Action Buttons
    override public func leftButtonTapped(_ sender: UIBarButtonItem?) {
        navigationController?.popViewController(animated: true)
    }
    
    override public func rightButtonTapped(_ sender: UIBarButtonItem?) {
        self.showHUDOnWindow()
        
        let events = [EventTypeRef.SVDataPrivacyAgree]
        viewModel?.agreeToTermsAndConditions(with: events, completion: { [weak self] (error) in
            self?.hideHUDFromWindow()
            guard error == nil else {
                self?.handleErrorOccurred(error)
                return
            }
            self?.submit()
        })
    }
    
    func submit() {
        self.showHUDOnWindow()
        eventsProcessor.setupDelegate(self)
        eventsProcessor.processEvents(completion: { [weak self] error in
            self?.hideHUDFromWindow()
            guard error == nil else {
                self?.handleErrorOccurred(error)
                return
            }
            self?.performSegue(withIdentifier: (self?.SEGUE_COMPLETION)!, sender: self)
        })
    }
    
    func handleErrorOccurred(_ error: Error?) {
        guard error == nil else {
            debugPrint(error as Any)
            switch error {
            case is BackendError:
                self.handleBackendErrorWithAlert(error as! BackendError, tryAgainAction: { [weak self] in
                    self?.rightButtonTapped(nil)
                })
            default:
                self.displayUnknownServiceErrorOccurredAlert()
            }
            self.hideHUDFromWindow()
            return
        }
    }
}

extension OFEDataPrivacyViewController: OFEPhotoUploadDelegate{
    
    public func displayImageUploadErrorMessage(with completion: @escaping (_ resubmit: Bool) -> Void){
        let cancelAction = UIAlertAction(title: CommonStrings.CancelButtonTitle24, style: UIAlertActionStyle.cancel) {
            [weak self] (alertAction) in
            completion(false)
            self?.hideHUDFromWindow()
        }
        let tryAgain = UIAlertAction(title: CommonStrings.TryAgainButtonTitle43, style: UIAlertActionStyle.default) { (alertAction) in
            completion(true)
        }
        
        let failedToSubmitVHCAlert = UIAlertController(title: CommonStrings.PrivacyPolicy.UnableToCompleteAlertTitle115, message: CommonStrings.Ofe.Summary.CompleteErrorMessage2310, preferredStyle: .alert)
        
        failedToSubmitVHCAlert.addAction(cancelAction)
        failedToSubmitVHCAlert.addAction(tryAgain)
        self.present(failedToSubmitVHCAlert, animated: true, completion: nil)
    }
}
