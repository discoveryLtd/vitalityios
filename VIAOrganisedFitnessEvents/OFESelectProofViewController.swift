//
//  OFESelectProofViewController.swift
//  VitalityActive
//
//  Created by Val Tomol on 12/04/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import VIAUIKit
import VIAUtilities
import VitalityKit
import VIACommon
import VIAImageManager

class OFESelectProofViewController: VIATableViewController {
    // MARK: Properties
    fileprivate let SEGUE_IMAGE_PROOF = "segueImageProof"
    fileprivate let SEGUE_WEBLINK_PROOF = "segueWeblinkProof"
    
    // MARK: View Lifecycles
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = CommonStrings.Ofe.Proof.Title1377
        self.configureAppearance()
        self.configureTableView()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.hideBackButtonTitle()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
}

// MARK: Section Enum
extension OFESelectProofViewController {
    enum SelectProofRows: Int, EnumCollection {
        case question = 0
        case imageProof = 1
        case weblinkProof = 2
        
        func headerTitle() -> String {
            switch self {
            case .question:
                return CommonStrings.Ofe.Proof.Header1378
            case .imageProof:
                return CommonStrings.Ofe.Proof.SectionHeader1379
            case .weblinkProof:
                return CommonStrings.Ofe.Common.WebLinkProof1381
            }
        }
        
        func content() -> String {
            switch self {
            case .imageProof:
                return CommonStrings.Ofe.Proof.SectionDescription1380
            case .weblinkProof:
                return CommonStrings.Ofe.Proof.SectionDescription1382
            default:
                return ""
            }
        }
        
        func image() -> UIImage {
            switch self {
            case .imageProof:
                return VIAOrganisedFitnessEventsAsset.imageProof.templateImage
            case .weblinkProof:
                return VIAOrganisedFitnessEventsAsset.weblinkProof.templateImage
            default:
                return UIImage()
            }
        }
    }
}

// MARK: View Configuration
extension OFESelectProofViewController {
    fileprivate func configureAppearance() {
        navigationController?.makeNavigationBarTransparent()
    }
    
    fileprivate func configureTableView() {
        self.tableView.register(VIALabelTableViewCell.nib(), forCellReuseIdentifier: VIALabelTableViewCell.defaultReuseIdentifier)
        self.tableView.register(VIAGenericContentCell.nib(), forCellReuseIdentifier: VIAGenericContentCell.defaultReuseIdentifier)
        self.tableView.estimatedRowHeight = 100
        self.tableView.sectionHeaderHeight = UITableViewAutomaticDimension
        self.tableView.estimatedSectionHeaderHeight = 500
        self.tableView.backgroundColor = UIColor.white
        self.tableView.tableFooterView = UIView()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Insert data manipulation here..
        if segue.identifier == SEGUE_IMAGE_PROOF, let destination = segue.destination as? SelectedPhotosCollectionViewController {
            destination.viewModel = OFESelectedPhotosCollectionViewModel()
            destination.delegate = self
            
            /* Let's get the global Image Configuration. */
            if let globalImageConfiguration = VIAApplicableFeatures.default.getImageControllerConfiguration() as? UIImagePickerController.ImageControllerConfiguration{                
                destination.defaultConfiguration =  globalImageConfiguration.initWith(allowCropping: false)
            }
        }
    }
}

// MARK: UITableView DataSource and Delegates
extension OFESelectProofViewController {
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return nil
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return nil
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let rowValue = SelectProofRows(rawValue: indexPath.row)
        
        if rowValue == .question {
            return questionCell(indexPath: indexPath)
        } else {
            return proofCell(indexPath: indexPath)
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let rowValue = SelectProofRows(rawValue: indexPath.row)
        
        if rowValue == .imageProof {
            imageProofTapped()
        } else if rowValue == .weblinkProof {
            weblinkProofTapped()
        }
    }
}
// MARK: UITableView Custom Methods
extension OFESelectProofViewController {
    func questionCell(indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: VIALabelTableViewCell.defaultReuseIdentifier, for: indexPath) as? VIALabelTableViewCell else {
            return tableView.defaultTableViewCell()
        }
        
        cell.accessoryType = .none
        cell.selectionStyle = .none
        
        cell.configure(font: UIFont.headlineFont(), color: UIColor.night())
        
        if let rowValue = SelectProofRows(rawValue: indexPath.row) {
            cell.labelText = rowValue.headerTitle()
        }
        
        return cell
    }
    
    func proofCell(indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: VIAGenericContentCell.defaultReuseIdentifier, for: indexPath) as? VIAGenericContentCell else {
            return tableView.defaultTableViewCell()
        }
        
        cell.accessoryType = .disclosureIndicator
        cell.selectionStyle = .none
        
        if let rowValue = SelectProofRows(rawValue: indexPath.row) {
            cell.header = rowValue.headerTitle()
            cell.content = rowValue.content()
            cell.cellImage = rowValue.image()
        }
        
        
        return cell
    }
    
    func imageProofTapped() {
        performSegue(withIdentifier: SEGUE_IMAGE_PROOF, sender: self)
    }
    
    func weblinkProofTapped() {
        performSegue(withIdentifier: SEGUE_WEBLINK_PROOF, sender: self)
    }
}

extension OFESelectProofViewController: SelectedPhotosCollectionViewDelegate{
    
    public func onPerformSegue(){
        if VIAApplicableFeatures.default.includeAllProofs(){
            self.performSegue(withIdentifier: "segueWeblinkProof", sender: self)
        }else{
            self.performSegue(withIdentifier: "segueSummary", sender: self)
        }
    }
}
