//
//  OFEOFESelectedPhotosCollectionViewModel.swift
//  VIAOrganisedFitnessEvents
//
//  Created by OJ Garde on 8/19/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation

import VitalityKit
import VIACommon
import VIAImageManager

public class OFESelectedPhotosCollectionViewModel : SelectedPhotosCollectionViewModel{
    
    public override init(){
        super.init()
        self.configuration = VIAApplicableFeatures.default.getImageControllerConfiguration() as? UIImagePickerController.ImageControllerConfiguration
    }
    
    open override func getSelectedPhotoViewTitle() -> String{
        return CommonStrings.Ofe.Proof.Title1379
    }
    
    open override func getHeaderText() -> String{
        return CommonStrings.Ofe.Proof.Header1383
    }
    
    open override func getMessageText() -> String{
        return CommonStrings.Ofe.Proof.Description1384
    }
    
    open override func getActionTitle() -> String{
        return CommonStrings.Ofe.Proof.AddTitle1385
    }
    
    open override func getRealmDomain() -> DataProvider.Domain{
        return DataProvider.Domain.ofe
    }
    
    open override func getFooterMessageWithLimit(totalImages: String, maxLimit: String) -> String{
        return CommonStrings.Proof.AttachmentsFootnoteMessage177(totalImages, maxLimit)
    }
    
    open override func getFooterMessage(totalImages: String, maxLimit: String) -> String{
        return CommonStrings.Proof.AttachmentsFootnoteMessage177(totalImages, maxLimit)
    }
}
