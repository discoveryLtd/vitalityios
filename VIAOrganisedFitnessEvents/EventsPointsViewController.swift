//
//  EventsPointsViewController.swift
//  VitalityActive
//
//  Created by OJ Garde on 4/13/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import UIKit
import VIAUIKit
import VitalityKit
import VIAUtilities
import VIACommon
import RealmSwift

class EventsPointsViewController: VIATableViewController {
    fileprivate let SEGUE_EVENTS_AND_POINTS = "segueEventsAndPoints"
    
    var details: [OFEEventType] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = CommonStrings.Ofe.Landing.ActionTitle1344
        
        configureTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        configureAppearance()
        
        details = DataProvider.newOFERealm().allOFEEventTypes().sorted { $0.eventTypeName < $1.eventTypeName }
    }
    
    func configureAppearance() {
        navigationController?.makeNavigationBarTransparent()
        self.hideBackButtonTitle()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - TableView Configuration
    func configureTableView() {
        self.tableView.register(OFEEventPointCell.nib(), forCellReuseIdentifier: OFEEventPointCell.defaultReuseIdentifier)
        self.tableView.estimatedRowHeight = 100
    }
    
    // MARK: - PointsEntry data processing
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return details.count
    }
    
    // MARK: UITableView delegate
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return nil
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return nil
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return self.configurePointsCell(indexPath)
    }
    
    func configurePointsCell(_ indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: OFEEventPointCell.defaultReuseIdentifier, for: indexPath) as? OFEEventPointCell else {
            return tableView.defaultTableViewCell()
        }
        
        cell.accessoryType = UITableViewCellAccessoryType.disclosureIndicator

        cell.headingText = details[indexPath.row].eventTypeName
        cell.contentText = CommonStrings.Status.PointsProgress838("\(details[indexPath.row].potentialPoints)")
        
        return cell
    }
    
    private var selectedEventType: OFEEventType?
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedEventType = details[indexPath.row]
        self.performSegue(withIdentifier: SEGUE_EVENTS_AND_POINTS, sender: self)
    }
    
    public override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == SEGUE_EVENTS_AND_POINTS {
            if let vc = segue.destination as? EventsPointsDetailViewController,
                let selectedEventType = self.selectedEventType {
                
                vc.eventType = selectedEventType
                vc.eventTypeName = selectedEventType.eventTypeName
                
                var selectionOption: [OFESelectionOption] = []
                outer: for eventDetail in selectedEventType.eventDetails {
                    if eventDetail.metadataKey == .OFEDistance {
                        for selectionGroup in eventDetail.selectionGroups {
                            if selectionGroup.categoryKey == .OFEKilometers {
                                selectionOption = Array(selectionGroup.selectionOptions)
                                break outer
                            }
                        }
                    }
                }
                
                vc.eventsAndPointsDetail = selectionOption
            }
        }
    }
}
