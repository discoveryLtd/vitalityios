//
//  OFEEventTypeViewController.swift
//  VitalityActive
//
//  Created by Val Tomol on 10/04/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import VIAUIKit
import VitalityKit
import VIACommon

class OFEEventTypeViewModel: AnyObject {
    // MARK: Properties
    let eventTypesSorted = DataProvider.newOFERealm().allOFEEventTypes().sorted { $0.eventTypeName < $1.eventTypeName }

    // MARK: Functions
    func eventTypeViewTitle() -> String {
        return CommonStrings.Ofe.ClaimPoints.Placeholder1366
    }
    
    func hasSearchResults(filteredEvents: [OFEEventType]) -> Bool {
        return filteredEvents.count > 0
    }
}

class OFEEventTypeViewController: VIATableViewController, UISearchBarDelegate, UISearchResultsUpdating {
    // MARK: Properties
    let viewModel = OFEEventTypeViewModel()
    let searchController = UISearchController(searchResultsController: nil)
    
    struct eventObjects {
        var sectionName: String
        var sectionObjects: [OFEEventType]
    }
    var eventObjectArray = [eventObjects]()
    var unfilteredEvents: [OFEEventType] = []
    var filteredEvents: [OFEEventType] = []
    
    // MARK: View Lifecycles
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = viewModel.eventTypeViewTitle()
        self.configureAppearance()
        self.configureTableView()

        setupViewData()
    }
    
    func setupViewData() {
        var curLetter: String = ""
        for eventType in viewModel.eventTypesSorted {
            let startIndex = eventType.eventTypeName.characters.startIndex
            let firstCharacter = String(eventType.eventTypeName.characters[startIndex])
            var eventTypeArray: [OFEEventType] = []
            
            for eType in viewModel.eventTypesSorted {
                if firstCharacter == String(eType.eventTypeName.characters[eType.eventTypeName.characters.startIndex]) {
                    eventTypeArray.append(eType)
                }
            }
            
            if curLetter != firstCharacter {
                eventObjectArray.append(eventObjects(sectionName: firstCharacter, sectionObjects: eventTypeArray))
                curLetter = firstCharacter
            }
        }
        
        for sectionEvents in eventObjectArray {
            for event in sectionEvents.sectionObjects {
                unfilteredEvents.append(event)
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        searchController.dismiss(animated: false, completion: nil)
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        //return viewModel.hasSearchResults(filteredEvents: filteredEvents) ? 1 : eventObjectArray.count
        if viewModel.hasSearchResults(filteredEvents: filteredEvents) {
            return 1
        } else {
            return searchBarIsEmpty() ? eventObjectArray.count : 0
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //return viewModel.hasSearchResults(filteredEvents: filteredEvents) ? filteredEvents.count : eventObjectArray[section].sectionObjects.count
        if viewModel.hasSearchResults(filteredEvents: filteredEvents) {
            return filteredEvents.count
        } else {
            return searchBarIsEmpty() ? eventObjectArray[section].sectionObjects.count : 0
        }
    }
}

// MARK: View Configuration
extension OFEEventTypeViewController {
    func configureAppearance() {
        navigationController?.makeNavigationBarTransparent()
    }
    
    func configureTableView() {
        self.tableView.register(VIATableViewSectionHeaderFooterView.nib(), forHeaderFooterViewReuseIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier)
        self.tableView.register(VIALabelTableViewCell.nib(), forCellReuseIdentifier: VIALabelTableViewCell.defaultReuseIdentifier)
        self.tableView.estimatedRowHeight = 100
        self.tableView.sectionHeaderHeight = UITableViewAutomaticDimension
        self.tableView.estimatedSectionHeaderHeight = 500
        
        tableView.tableHeaderView = configureSearchController().searchBar
    }
    
    func configureSearchController() -> UISearchController {
        searchController.searchResultsUpdater = self
        searchController.hidesNavigationBarDuringPresentation = true
        searchController.dimsBackgroundDuringPresentation = false
        searchController.searchBar.delegate = self
        
        return searchController
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Insert data manipulation here..
    }
}

// MARK: UITableView DataSource and Delegates
extension OFEEventTypeViewController {
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return viewModel.hasSearchResults(filteredEvents: filteredEvents) ? CGFloat.leastNormalMagnitude : UITableViewAutomaticDimension
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if viewModel.hasSearchResults(filteredEvents: filteredEvents) {
            return nil
        }
        
        guard let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier) as? VIATableViewSectionHeaderFooterView else {
            return nil
        }
        
        view.set(font: UIFont.headlineFont())
        view.set(textColor: UIColor.night())
        view.labelText = eventObjectArray[section].sectionName
        
        return view
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return eventCell(indexPath: indexPath)
    }
    
    override func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        if viewModel.hasSearchResults(filteredEvents: filteredEvents) {
            return nil
        } else {
            if searchBarIsEmpty() {
                var indexes: Array<String> = []
                for index in eventObjectArray {
                    indexes.append(index.sectionName)
                }
                return indexes
            } else {
                return nil
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        var eventType: OFEEventType?
        
        if viewModel.hasSearchResults(filteredEvents: filteredEvents) {
            eventType = filteredEvents[indexPath.row]
        } else {
            eventType = eventObjectArray[indexPath.section].sectionObjects[indexPath.row]
        }
        
        let ofeRealm = DataProvider.newOFERealm()
        // Delete previous capture.
        try? ofeRealm.write {
            ofeRealm.delete(ofeRealm.allOFECapturedResults())
        }
        // Save current capture. Reset the rest of the fields.
        if VIAApplicableFeatures.default.withDefaultStringValueForOFEEventDate() {
            ofeRealm.capturedResult(eventTypeCode: eventType?.eventTypeCode,
                                    eventType: eventType,
                                    eventDate: nil)
        } else {
            ofeRealm.capturedResult(eventTypeCode: eventType?.eventTypeCode,
                                    eventType: eventType,
                                    eventDate: Date())
        }
        
        self.searchController.isActive = false
        self.navigationController?.popViewController(animated: true)
    }
}

// MARK: UITableView Custom Methods
extension OFEEventTypeViewController {
    func eventCell(indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: VIALabelTableViewCell.defaultReuseIdentifier, for: indexPath) as? VIALabelTableViewCell else {
            return tableView.defaultTableViewCell()
        }
        
        cell.configure(font: UIFont.bodyFont(), color: UIColor.night())
        if viewModel.hasSearchResults(filteredEvents: filteredEvents) {
            cell.labelText = filteredEvents[indexPath.row].eventTypeName
        } else {
            cell.labelText = eventObjectArray[indexPath.section].sectionObjects[indexPath.row].eventTypeName
        }
        
        return cell
    }
}

// MARK: Search Controller Delegate
extension OFEEventTypeViewController {
    func updateSearchResults(for searchController: UISearchController) {
        if let searchText = searchController.searchBar.text, !searchText.isEmpty {
            filteredEvents = unfilteredEvents.filter { event in
                return event.eventTypeName.lowercased().contains(searchText.lowercased())
            }
            
            if viewModel.hasSearchResults(filteredEvents: filteredEvents) {
                hideNoResultsAlert()
            } else {
                showNoResultsAlertWithKeyword(searchText)
            }
        } else {
            filteredEvents = []
            hideNoResultsAlert()
        }
        
        tableView.reloadData()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        filteredEvents = []
        hideNoResultsAlert()
        
        tableView.reloadData()
    }
    
    func searchBarIsEmpty() -> Bool {
        return searchController.searchBar.text?.isEmpty ?? true
    }
}

// MARK: Custom Methods
extension OFEEventTypeViewController {
    func showNoResultsAlertWithKeyword(_ keyword: String) {
        self.hideNoResultsAlert()
        let superview = self.tableView
        let noResultsStackView = UIStackView()
        noResultsStackView.tag = 100
        noResultsStackView.axis = .vertical
        noResultsStackView.spacing = 5
        noResultsStackView.alignment = .center
        
        let titleLabel = UILabel()
        titleLabel.font = UIFont.headlineFont()
        titleLabel.textColor = UIColor.darkGray
        titleLabel.numberOfLines = 0
        titleLabel.textAlignment = .center
        titleLabel.text = CommonStrings.Ofe.Search.NoResults1368
        noResultsStackView.addArrangedSubview(titleLabel)
        
        let messageLabel = UILabel()
        messageLabel.font = UIFont.subheadlineFont()
        messageLabel.textColor = UIColor.darkGray
        messageLabel.numberOfLines = 0
        messageLabel.textAlignment = .center
        messageLabel.text = CommonStrings.Ofe.Search.NoResultDescription1369(keyword)
        noResultsStackView.addArrangedSubview(messageLabel)
        
        superview?.addSubview(noResultsStackView)
        noResultsStackView.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.centerY.equalToSuperview().multipliedBy(1.0)
            make.width.equalToSuperview().multipliedBy(0.75)
        }
    }
    
    func hideNoResultsAlert() {
        if let viewWithTag = self.tableView.viewWithTag(100) {
            viewWithTag.removeFromSuperview()
        }
    }
}
