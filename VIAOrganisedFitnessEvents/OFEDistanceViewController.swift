//
//  OFEDistanceViewController.swift
//  VitalityActive
//
//  Created by Val Tomol on 10/04/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import VIAUIKit
import VIAUtilities
import VitalityKit

class OFEDistanceViewModel: AnyObject {
    // MARK: Properties
    let ofeCapturedResult = DataProvider.newOFERealm().objects(OFECapturedResult.self).first
    
    // MARK: Functions
    func distanceViewTitle() -> String {
        return CommonStrings.Ofe.ClaimPoints.Distance1371
    }
    
    func questionText() -> String {
        return CommonStrings.Ofe.Header1406
    }
    
    func pointsText(text: String) -> String {
        return CommonStrings.Status.PointsProgress838(text)
    }
    
    func distanceUnitTypes() -> [OFESelectionGroup] {
        var unitTypes: [OFESelectionGroup] = []
        if let eventDetails = ofeCapturedResult?.eventType?.eventDetails {
            for eventDetail in eventDetails {
                if eventDetail.metadataKey == .OFEDistance {
                    for selectionGroup in eventDetail.selectionGroups {
                        unitTypes.append(selectionGroup)
                    }
                }
            }
        }
        return unitTypes
    }
    
    func hasOtherUnitTypes() -> Bool {
        return distanceUnitTypes().count > 1
    }
    
    func distanceOptions() -> [OFESelectionOption] {
        var options: [OFESelectionOption] = []
        if let eventDetails = ofeCapturedResult?.eventType?.eventDetails {
            for eventDetail in eventDetails {
                if eventDetail.metadataKey == .OFEDistance {
                    for selectionGroup in eventDetail.selectionGroups {
                        for selectionOption in selectionGroup.selectionOptions {
                            options.append(selectionOption)
                        }
                    }
                }
            }
        }
        return options.sorted { $0.0.potentialPoints < $0.1.potentialPoints }
    }
}

class OFEDistanceViewController: VIATableViewController {
    // MARK: Properties
    let viewModel = OFEDistanceViewModel()
    let claimPointsViewModel = OFEClaimPointsViewModel()
    
    // MARK: View Lifecycles
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = viewModel.distanceViewTitle()
        self.configureAppearance()
        self.configureTableView()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.hasOtherUnitTypes() ? Sections.allValues.count : 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let sectionValue = Sections(rawValue: section)
        
        if sectionValue == .distance {
            return viewModel.distanceOptions().count + 1 // +1 for the question row.
        } else {
            return 1
        }
    }
}

// MARK: Section Enum
extension OFEDistanceViewController {
    enum Sections: Int, EnumCollection {
        case distance = 0
        case unit = 1
        
        func sectionHeaderTitle() -> String {
            switch self {
            case .unit:
                return CommonStrings.Ofe.Common.Unit1407
            default:
                return ""
            }
        }
    }
}

// MARK: View Configuration
extension OFEDistanceViewController {
    fileprivate func configureAppearance() {
        navigationController?.makeNavigationBarTransparent()
    }
    
    fileprivate func configureTableView() {
        self.tableView.register(VIATableViewSectionHeaderFooterView.nib(), forHeaderFooterViewReuseIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier)
        self.tableView.register(ActivityDetailSubtitleCell.nib(), forCellReuseIdentifier: ActivityDetailSubtitleCell.defaultReuseIdentifier)
        self.tableView.register(VIALabelTableViewCell.nib(), forCellReuseIdentifier: VIALabelTableViewCell.defaultReuseIdentifier)
        self.tableView.estimatedRowHeight = 100
        self.tableView.sectionHeaderHeight = UITableViewAutomaticDimension
        self.tableView.estimatedSectionHeaderHeight = 500
        
        self.tableView.contentInset = UIEdgeInsets(top: 10, left: 0, bottom: 0, right: 0)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Insert data manipulation here..
    }
}

// MARK: UITableView DataSource and Delegates
extension OFEDistanceViewController {
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier) as? VIATableViewSectionHeaderFooterView else {
            return nil
        }
        
        if let sectionValue = Sections(rawValue: section) {
            if sectionValue == Sections.distance {
                return nil
            }
            
            view.labelText = sectionValue.sectionHeaderTitle().uppercased()
        }
        
        return view
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return nil
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let sectionValue = Sections(rawValue: indexPath.section)
        
        if sectionValue == .distance {
            return questionChoiceCell(indexPath: indexPath)
        } else {
            return unitCell(indexPath: indexPath)
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let sectionValue = Sections(rawValue: indexPath.section)
        
        if sectionValue == .distance {
            if indexPath.row != 0 {
                var selectionOption: OFESelectionOption
                selectionOption = viewModel.distanceOptions()[indexPath.row - 1]
                
                claimPointsViewModel.saveCapturedData(selectionOption: selectionOption)
                
                self.navigationController?.popViewController(animated: true)
            }
        } else {
            unitTapped()
        }
    }
}

// MARK: UITableView Custom Methods
extension OFEDistanceViewController {
    func questionChoiceCell(indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            return questionCell(indexPath: indexPath)
        } else {
            return choicesCell(indexPath: indexPath)
        }
    }
    
    func questionCell(indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: VIALabelTableViewCell.defaultReuseIdentifier, for: indexPath) as? VIALabelTableViewCell else {
            return tableView.defaultTableViewCell()
        }
        
        cell.accessoryType = .none
        cell.selectionStyle = .none
        
        cell.configure(font: UIFont.headlineFont(), color: UIColor.night())
        cell.labelText = viewModel.questionText()
        
        return cell
    }
    
    func choicesCell(indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ActivityDetailSubtitleCell.defaultReuseIdentifier, for: indexPath) as? ActivityDetailSubtitleCell else {
            return tableView.defaultTableViewCell()
        }
        
        cell.accessoryType = .none
        
        cell.headingText = viewModel.distanceOptions()[indexPath.row - 1].featureName
        cell.contentText = viewModel.pointsText(text: String(viewModel.distanceOptions()[indexPath.row - 1].potentialPoints))
        
        return cell
    }
    
    func unitCell(indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: VIALabelTableViewCell.defaultReuseIdentifier, for: indexPath) as? VIALabelTableViewCell else {
            return tableView.defaultTableViewCell()
        }
        
        cell.accessoryType = .disclosureIndicator
        
        cell.labelText = viewModel.distanceUnitTypes().last?.categoryName.lowercased()
        
        return cell
    }
    
    func unitTapped() {
        let unitsMenu = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        for unitOption in viewModel.distanceUnitTypes() {
            let unitAction = UIAlertAction(title: unitOption.categoryName.lowercased(), style: .default, handler: { _ in
                NSLog("DID SELECT \(unitOption)")
            })
            unitsMenu.addAction(unitAction)
        }
        
        let cancelAction = UIAlertAction(title: CommonStrings.CancelButtonTitle24, style: .cancel, handler: nil)
        unitsMenu.addAction(cancelAction)
        
        self.present(unitsMenu, animated: true, completion: nil)
    }
}
