// Generated using SwiftGen, by O.Halligon — https://github.com/AliSoftware/SwiftGen

internal class VIAOrganisedFitnessEventsImagesPlaceholder {
    // Placeholder class to reference lower down
}

#if os(iOS) || os(tvOS) || os(watchOS)
    import UIKit.UIImage
    typealias Image = UIImage
#elseif os(OSX)
    import AppKit.NSImage
    typealias Image = NSImage
#endif

// swiftlint:disable file_length
// swiftlint:disable line_length

// swiftlint:disable type_body_length
@available(*, deprecated, message: "Use VIAOrganisedFitnessEventsAsset.{image}.image or VIAOrganisedFitnessEventsAsset.{image}.templateImage instead")
public enum OrganisedFitnessEventsAsset: String {
    case addPhoto = "addSmall"
    case cameraLarge = "cameraLarge"
        
    var image: Image {
        return Image(asset: self)
    }
}
// swiftlint:enable type_body_length

public extension UIImage {
    @available(*, deprecated, message: "Use VIAOrganisedFitnessEventsAsset.{image}.image or VIAOrganisedFitnessEventsAsset.{image}.templateImage instead")
    class func OFEtemplateImage(asset: OrganisedFitnessEventsAsset) -> UIImage? {
        let bundle = Bundle(for: VIAOrganisedFitnessEventsImagesPlaceholder.self) // placeholder declared at top of file
        let image = UIImage(named: asset.rawValue, in: bundle, compatibleWith: nil)?.withRenderingMode(.alwaysTemplate)
        return image
    }
}

public extension Image {
    @available(*, deprecated, message: "Use VIAOrganisedFitnessEventsAsset.{image}.image or VIAOrganisedFitnessEventsAsset.{image}.templateImage instead")
    convenience init!(asset: OrganisedFitnessEventsAsset) {
        let bundle = Bundle(for: VIAOrganisedFitnessEventsImagesPlaceholder.self) // placeholder declared at top of file
        self.init(named: asset.rawValue, in: bundle, compatibleWith: nil)
    }
}
