//
//  OFEHistoryDetailsViewController.swift
//  VitalityActive
//
//  Created by OJ Garde on 4/13/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import VIACommon
import VIAUIKit
import VitalityKit
import VIAUtilities

public class OFEHistoryDetailsViewController: VIATableViewController{
    let viewModel = OFEHistoryViewModel()
    
    internal var heightForCell: CGFloat = 0.0
    
    var realm = DataProvider.newOFERealm()
    
    var eventsPoint: EventsPoints?
    var imageProofs: [UIImage] = []
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        configureTable()
        
        self.title = VIAApplicableFeatures.default.getOFEFormattedDate(dateString: (eventsPoint?.activityDate)!)
    }
    
    override public func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // TODO: Replaced with value from model
    }
    
    override public func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override public func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    
    public override func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    
    public override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let sectionValue = Sections(rawValue: sections.integer(at: section)) {
            if sectionValue == .eventDetails {
                return eventDetailRows.count
            } else if sectionValue == .extraEventDetails {
                return extraEventDetailRows.count
            } else {
                return sectionValue.rowCount()
            }
        }
        
        return 0
    }
}

extension OFEHistoryDetailsViewController {
    public override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    public override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    public override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    public override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier) as? VIATableViewSectionHeaderFooterView {
            if let sectionValue = Sections(rawValue: sections.integer(at: section)) {
                view.labelText = sectionValue.sectionHeaderTitle().uppercased()
            }
            
            return view
        }
        
        return nil
    }
    
    public override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    public override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return nil
    }
    
    public override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let sectionValue = Sections(rawValue: sections.integer(at: indexPath.section))
        
        if sectionValue == .proof1 {
            return imageCollectionCell()
        }
        
        return eventMetadataCell(indexPath: indexPath)
    }
}

extension OFEHistoryDetailsViewController {
    enum Sections: Int, EnumCollection {
        case eventDetails = 0
        case extraEventDetails = 1
        case proof1 = 2
        case proof2 = 3
        case submissionDate = 4
        
        func sectionHeaderTitle() -> String {
            switch self {
            case .eventDetails:
                return CommonStrings.Ofe.ClaimPoints.Header1365.Other
            case .proof1:
                return VIAApplicableFeatures.default.includeAllProofs() ? CommonStrings.Ofe.Proof.SectionHeader1379 : CommonStrings.Ofe.Summary.ProofTitle1392
            case .proof2:
                return CommonStrings.Ofe.Common.WebLinkProof1381
            case .submissionDate:
                return CommonStrings.Ofe.History.SubmissionDate1398
            default:
                return ""
            }
        }
        
        // Static row counts.
        func rowCount() -> Int {
            switch self {
            case .proof1:
                return 1
            case .proof2:
                return 1
            case .submissionDate:
                return 1
            default:
                return 1
            }
        }
    }
    
    var sections: NSMutableIndexSet {
        let sectionsIndexSet = NSMutableIndexSet()
        sectionsIndexSet.add(Sections.eventDetails.rawValue)
        sectionsIndexSet.add(Sections.extraEventDetails.rawValue)
        sectionsIndexSet.add(Sections.proof1.rawValue)
        if VIAApplicableFeatures.default.includeAllProofs() {
            sectionsIndexSet.add(Sections.proof2.rawValue)
        }
        sectionsIndexSet.add(Sections.submissionDate.rawValue)
        
        return sectionsIndexSet
    }
    
    enum EventDetailRows: Int, EnumCollection {
        case eventType = 0
        case distance = 1
        case finishTime = 2
        case eventDate = 3
        
        func eventTypeKey() -> Int {
            switch self {
            case .eventType:
                return OFEFeatureTypeRef.EventType.rawValue
            case .distance:
                return OFEFeatureTypeRef.OFEDistance.rawValue
            case .finishTime:
                return OFEFeatureTypeRef.FinishTime.rawValue
            case .eventDate:
                return OFEFeatureTypeRef.EventDate.rawValue
            }
        }
        
        func title() -> String {
            switch self {
            case .eventType:
                return CommonStrings.Ofe.ClaimPoints.Placeholder1366
            case .eventDate:
                return CommonStrings.Ofe.ClaimPoints.EventDate1373
            default:
                return ""
            }
        }
    }
    
    var eventDetailRows: NSMutableIndexSet {
        let eventDetailRowsIndexSet = NSMutableIndexSet()
        eventDetailRowsIndexSet.add(EventDetailRows.eventType.rawValue)
        if hasMedata(ofType: OFEFeatureTypeRef.OFEDistance.rawValue) {
            eventDetailRowsIndexSet.add(EventDetailRows.distance.rawValue)
        }
        if hasMedata(ofType: OFEFeatureTypeRef.FinishTime.rawValue) {
            eventDetailRowsIndexSet.add(EventDetailRows.finishTime.rawValue)
        }
        eventDetailRowsIndexSet.add(EventDetailRows.eventDate.rawValue)
        
        return eventDetailRowsIndexSet
    }
    
    enum ExtraEventDetailRows: Int, EnumCollection {
        case eventName = 0
        case organiser = 1
        case raceNumber = 2
        
        func eventTypeKey() -> Int {
            switch self {
            case .eventName:
                return OFEFeatureTypeRef.EventName.rawValue
            case .organiser:
                return OFEFeatureTypeRef.Organiser.rawValue
            case .raceNumber:
                return OFEFeatureTypeRef.RaceNumber.rawValue
            }
        }
    }
    
    var extraEventDetailRows: NSMutableIndexSet {
        let extraEventDetailRowsIndexSet = NSMutableIndexSet()
        if hasMedata(ofType: OFEFeatureTypeRef.EventName.rawValue) {
            extraEventDetailRowsIndexSet.add(ExtraEventDetailRows.eventName.rawValue)
        }
        if hasMedata(ofType: OFEFeatureTypeRef.Organiser.rawValue) {
            extraEventDetailRowsIndexSet.add(ExtraEventDetailRows.organiser.rawValue)
        }
        if hasMedata(ofType: OFEFeatureTypeRef.RaceNumber.rawValue) {
            extraEventDetailRowsIndexSet.add(ExtraEventDetailRows.raceNumber.rawValue)
        }
        
        return extraEventDetailRowsIndexSet
    }
    
    func eventMetadataCell(indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: HeaderValueSubtitleTableViewCell.defaultReuseIdentifier, for: indexPath) as? HeaderValueSubtitleTableViewCell else {
            return tableView.defaultTableViewCell()
        }
        
        cell.accessoryType = .none
        cell.selectionStyle = .none
        cell.setCell(subTitle: "")
        
        let sectionValue = Sections(rawValue: sections.integer(at: indexPath.section))
        if sectionValue == .eventDetails {
            let rowValue = EventDetailRows(rawValue: eventDetailRows.integer(at: indexPath.row))
            if rowValue == .eventType {
                cell.setCell(title: (rowValue?.title())!)
                cell.setCell(value: (eventsPoint?.typeName)!)
            } else if rowValue == .distance {
                cell.setCell(title: getEventTypeName(type: (rowValue?.eventTypeKey())!))
                cell.setCell(value: getDistance(type: Int(getEventValue(type: (rowValue?.eventTypeKey())!))!))
                cell.setCell(subTitle: CommonStrings.Status.PointsProgress838(getDistancePoints(type: Int(getEventValue(type: (rowValue?.eventTypeKey())!))!)))
            } else if rowValue == .finishTime {
                cell.setCell(title: getEventTypeName(type: (rowValue?.eventTypeKey())!))
                cell.setCell(value: getEventValue(type: (rowValue?.eventTypeKey())!))
            } else if rowValue == .eventDate {
                cell.setCell(title: (rowValue?.title())!)
                cell.setCell(value: VIAApplicableFeatures.default.getOFEFormattedDate(dateString: (eventsPoint?.activityDate)!))
            }
        } else if sectionValue == .extraEventDetails {
            let rowValue = ExtraEventDetailRows(rawValue: extraEventDetailRows.integer(at: indexPath.row))
            cell.setCell(title: getEventTypeName(type: (rowValue?.eventTypeKey())!))
            cell.setCell(value: getEventValue(type: (rowValue?.eventTypeKey())!))
        } else if sectionValue == .proof2 {
            cell.setCell(title: CommonStrings.Ofe.Summary.LinkTitle1393)
            cell.setCell(value: getEventValue(type: OFEFeatureTypeRef.WeblinkProof.rawValue))
        } else if sectionValue == .submissionDate {
            cell.setCell(title: CommonStrings.DateTitle264)
            cell.setCell(value: viewModel.prettifyDateFromString(dateString: (eventsPoint?.dateLogged)!))
        }
        
        return cell
    }
    
    func imageCollectionCell() -> UITableViewCell {
        if let imageCollectionCell = self.tableView.dequeueReusableCell(withIdentifier: ImageCollectionTableViewCell.defaultReuseIdentifier) {
            let collectionviewCell = self.uploadedProofsItemCell(cell: imageCollectionCell)
            self.heightForImageCollection(cell: collectionviewCell)
            return collectionviewCell
        }
        
        return UITableViewCell()
    }
}

extension OFEHistoryDetailsViewController {
    fileprivate func configureTable(){
        self.tableView.register(ImageCollectionTableViewCell.nib(), forCellReuseIdentifier: ImageCollectionTableViewCell.defaultReuseIdentifier)
        self.tableView.register(HeaderValueSubtitleTableViewCell.nib(), forCellReuseIdentifier: HeaderValueSubtitleTableViewCell.defaultReuseIdentifier)
        self.tableView.register(VIATableViewCell.nib(), forCellReuseIdentifier: VIATableViewCell.defaultReuseIdentifier)
        self.tableView.register(VIATableViewSectionHeaderFooterView.nib(), forHeaderFooterViewReuseIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier)
    }
    
    func hasMedata(ofType type: Int) -> Bool {
        for eventMetadata in (eventsPoint?.eventMetadatas)! {
            if eventMetadata.typeKey == type {
                return true
            }
        }
        
        return false
    }
    
    func getEventTypeName(type: Int) -> String {
        for eventMetadata in (eventsPoint?.eventMetadatas!)! {
            if eventMetadata.typeKey == type {
                return eventMetadata.typeName!
            }
        }
        
        return ""
    }
    
    func getEventValue(type: Int) -> String {
        for eventMetadata in (eventsPoint?.eventMetadatas!)! {
            if eventMetadata.typeKey == type {
                return eventMetadata.value!
            }
        }
        
        return ""
    }
    
    func getDistance(type: Int) -> String {
        for selectionOption in realm.allOFESelectionOptions() {
            if type == selectionOption.featureKey {
                return selectionOption.featureName
            }
        }
        
        return ""
    }
    
    func getDistancePoints(type: Int) -> String {
        for selectionOption in realm.allOFESelectionOptions() {
            if type == selectionOption.featureKey {
                return String(selectionOption.potentialPoints)
            }
        }
        
        return ""
    }
    
    func uploadedProofsItemCell(cell: UITableViewCell) -> ImageCollectionTableViewCell {
        if let imageCollectionViewCell = cell as? ImageCollectionTableViewCell {
            imageCollectionViewCell.setImageCollection(images: self.imageProofs)
            imageCollectionViewCell.imageCollectionView?.bounces = false
            imageCollectionViewCell.viewWidth = self.view.frame.width
            imageCollectionViewCell.setupCollectionFlowLayoutSize()
            self.hideHUDFromView(view: self.view)
            return imageCollectionViewCell
        }
        return ImageCollectionTableViewCell()
    }
    
    func heightForImageCollection(cell: ImageCollectionTableViewCell) {
        let collectionViewFlowLayout = cell.imageCollectionView?.collectionViewLayout
        let cellPadding = collectionViewFlowLayout?.collectionViewContentSize
        let height = cellPadding?.height
        self.heightForCell = height ?? 0
        cell.updateImageCollectionViewSize(with:height)
    }
}


