////
////  SelectedPhotosCollectionViewController.swift
////  VitalityActive
////
////  Created by Erik John T. Alicaya (ADMIN) on 13/04/2018.
////  Copyright © 2018 Glucode. All rights reserved.
////
//
//import UIKit
//import VIAUIKit
//import VitalityKit
//import VitalityKit
//import Photos
//import RealmSwift
//import VIACommon
//
//class OFESelectedPhotosCollectionViewModel: AnyObject {
//    // MARK: Properties
//
//    // MARK: Functions
//    func selectedPhotoViewTitle() -> String {
//        return OFEStrings.Ofe.Proof.Title1379
//    }
//
//    func headerText() -> String {
//        return OFEStrings.Ofe.Proof.Header1383
//    }
//
//    func messageText() -> String {
//        return OFEStrings.Ofe.Proof.Description1384
//    }
//
//    func actionTitle() -> String {
//        return OFEStrings.Ofe.Proof.AddTitle1385
//    }
//}
//
//class OFESelectedPhotosCollectionViewController: UICollectionViewController {
//    fileprivate let SEGUE_SUMMARY = "segueSummary"
//    fileprivate let SEGUE_WEBLINK_PROOF = "segueWeblinkProof"
//
//    var imagesInfo: Array<Dictionary<String, Any>> = []
//    var imagePicker: OFESelectImageController?
//    var emptyStateAddPhotoView: AddPhotoView?
//    var realm: Realm?
//    var viewCounter: Int = 0
//
//    var viewModel = OFESelectedPhotosCollectionViewModel()
//
//    @IBOutlet weak var selectedPhotosFlowLayout: UICollectionViewFlowLayout!
//    @IBAction func unwindToPhotosCollection(segue: UIStoryboardSegue) {
//        self.collectionView?.reloadData()
//    }
//
//    public func remove(imageInfo: [String: Any]) {
//        if let pickedImage = imageInfo[UIImagePickerControllerOriginalImage] as? UIImage {
//            var index = 0
//            for helpImageInfo in self.imagesInfo {
//                if let heldImage = helpImageInfo[UIImagePickerControllerOriginalImage] as? UIImage {
//                    if (pickedImage.isEqual(heldImage)) {
//                        self.imagesInfo.remove(at: index)
//                    }
//                }
//                index += 1
//            }
//        }
//    }
//
//    public func add(imageInfo: [String : Any]) {
//        self.imagesInfo.append(imageInfo)
//    }
//
//    deinit {
//    }
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//
//        self.collectionView?.alwaysBounceVertical = true
//        self.title = viewModel.selectedPhotoViewTitle()
//
//        guard let configuration = VIAApplicableFeatures.default.getImageControllerConfiguration()
//            as? UIImagePickerController.ImageControllerConfiguration else {return}
//
//        imagePicker = OFESelectImageController(pickerDelegate: self, configuration: configuration)
//        self.view.backgroundColor = UIColor.day()
//
//        self.setCollectionViewCellSize()
//        self.registerCollectionViewCells()
//        self.setupNavigationItem()
//
//        self.clearsSelectionOnViewWillAppear = false
//    }
//
//    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(animated)
//
//        self.hideBackButtonTitle()
//
//        if (isCollectionEmpty()) {
//            emptyStateAddPhotoView = AddPhotoView.viewFromNib(owner: self) as? AddPhotoView
//
//            emptyStateAddPhotoView?.setHeader(title: viewModel.headerText())
//            emptyStateAddPhotoView?.setDetail(message: viewModel.messageText())
//            emptyStateAddPhotoView?.setAction(title: viewModel.actionTitle())
//            emptyStateAddPhotoView?.setHeader(image: UIImage.OFEtemplateImage(asset: .cameraLarge))
//            emptyStateAddPhotoView?.set(photoViewDelegate: self)
//            emptyStateAddPhotoView?.frame = self.collectionView?.frame ?? CGRect(x: 0, y: 0, width: 0, height: 0)
//            emptyStateAddPhotoView?.tag = 1
//
//            if emptyStateAddPhotoView != nil {
//                emptyStateAddPhotoView?.tag = 1
//                self.viewCounter += 1
//                self.collectionView?.addSubview(emptyStateAddPhotoView!)
//                if (self.navigationItem.rightBarButtonItem?.isEnabled ?? false) {
//                    self.navigationItem.rightBarButtonItem?.isEnabled = false
//                }
//            }
//        } else {
//            while self.viewCounter != 1{
//                if let EmptyStateViewWithTag = self.collectionView?.viewWithTag(1) {
//                    EmptyStateViewWithTag.removeFromSuperview()
//                    self.collectionView?.reloadData()
//                    self.viewCounter -= 1
//                }
//            }
//        }
//    }
//
//    func isCollectionEmpty() -> Bool {
//        return self.imagesInfo.count == 0
//    }
//
//    func registerCollectionViewCells() {
//        self.collectionView!.register(ImageCollectionViewCell.nib(), forCellWithReuseIdentifier: ImageCollectionViewCell.defaultReuseIdentifier)
//        self.collectionView!.register(LabelSupplimentaryView.nib(), forSupplementaryViewOfKind: UICollectionElementKindSectionFooter, withReuseIdentifier: "Footer")
//    }
//
//    func setupNavigationItem() {
//        let nextBarButton = UIBarButtonItem(title: CommonStrings.NextButtonTitle84, style: .done, target: self, action: #selector(submitPhotos))
//        self.navigationItem.rightBarButtonItem = nextBarButton
//    }
//
//    func submitPhotos() {
//        self.showHUDOnWindow()
//        self.beginURLWrite(completion: {
//            self.hideHUDFromWindow()
//            if VIAApplicableFeatures.default.includeAllProofs() {
//                self.performSegue(withIdentifier: self.SEGUE_WEBLINK_PROOF, sender: self)
//            } else {
//                self.performSegue(withIdentifier: self.SEGUE_SUMMARY, sender: self)
//            }
//        })
//    }
//
//    func beginURLWrite(completion: @escaping () -> Void) {
//        DispatchQueue.global(qos: .background).async {
//            let realm = DataProvider.newOFERealm()
//            var realmAssets = Array<PhotoAsset>()
//
//            for image in self.imagesInfo {
//                let realmAsset = PhotoAsset()
//                if let uiImageURL = image[UIImagePickerControllerReferenceURL] as? URL {
//                    let photosAsset = PHAsset.fetchAssets(withALAssetURLs: [uiImageURL], options: nil).firstObject
//                    let localIdentifier = photosAsset?.localIdentifier
//                    realmAsset.assetURL = localIdentifier
//                } else {
//                    if let uiImage = image[UIImagePickerControllerOriginalImage] as? UIImage {
//                        if let imageData = PhotoAsset.compress(image:uiImage) {
//                            realmAsset.assetData = imageData
//                        }
//                    }
//                }
//                realmAssets.append(realmAsset)
//            }
//
//            try! realm.write {
//                realm.delete(realm.allPhotosAssets())
//                realm.add(realmAssets)
//            }
//
//            DispatchQueue.main.async {
//                completion()
//            }
//        }
//    }
//
//    func setCollectionViewCellSize() {
//        let leftAndRightSpaceingTotal: CGFloat = 15
//        let cellPaddingTotal: CGFloat = 15
//        let totalWidthPadding: CGFloat = leftAndRightSpaceingTotal + cellPaddingTotal
//        let itemWidth = (((self.collectionView?.frame.size.width) ?? totalWidthPadding) - totalWidthPadding) / 2
//        self.selectedPhotosFlowLayout.itemSize = CGSize(width: itemWidth,
//                                                      height: itemWidth)
//        self.selectedPhotosFlowLayout.minimumLineSpacing = 10
//    }
//
//    func dequeToCaptureResults() {
//        //self.performSegue(withIdentifier: "unwindToCaptureResults", sender: self)
//    }
//
//    // MARK: - Navigation
//
//    // In a storyboard-based application, you will often want to do a little preparation before navigation
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        if segue.identifier == self.SEGUE_SUMMARY{
//            if !VIAApplicableFeatures.default.includeAllProofs() {
//                guard let destinationVC = segue.destination as? OFESummaryViewController else { return }
//                destinationVC.usePhotoProofOnSubmit = true
//            }
//        }else{
//            guard let selectedImage = sender as? [String : Any] else {
//                return
//            }
//            guard let detailImageViewController = segue.destination as? OFEImageDetailViewController else {
//                return
//            }
//            detailImageViewController.set(selectedImage: selectedImage)
//
//        }    }
//
//    // MARK: UICollectionViewDataSource
//
//    override func numberOfSections(in collectionView: UICollectionView) -> Int {
//        return 1
//    }
//
//    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        let addMoreImagesCellCount = 1
//        var itemCount = self.imagesInfo.count
//        if (self.imagesInfo.count < 11) {
//            itemCount += addMoreImagesCellCount
//        }
//        return itemCount
//    }
//
//    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ImageCollectionViewCell.defaultReuseIdentifier, for: indexPath) as? ImageCollectionViewCell
//        cell?.imageView.image = nil
//        cell?.resetCell()
//
//        if (indexPath.item == self.imagesInfo.count) {
//            if let plusImage = UIImage.OFEtemplateImage(asset: .addPhoto) {
//                cell?.setSmall(image: plusImage)
//            }
//            cell?.selectionClosure = {
//                self.imagePicker?.selectImage()
//            }
//        } else {
//            if let pickedImage = self.imagesInfo[indexPath.row][UIImagePickerControllerOriginalImage] as? UIImage {
//                cell?.imageView.image = pickedImage
//            }
//        }
//        guard let returnCell = cell else { return UICollectionViewCell() }
//        return returnCell
//    }
//
//    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
//        var supplimentaryView: UICollectionReusableView = UICollectionReusableView()
//        if (kind == UICollectionElementKindSectionFooter) {
//            guard let footerView = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionFooter, withReuseIdentifier: "Footer", for: indexPath) as? LabelSupplimentaryView else {
//                return UICollectionReusableView()
//            }
//            footerView.setView(text: VhcStrings.Proof.AttachmentsFootnoteMessage177(String(self.imagesInfo.count), "11")) // TODO: If the 11 should come from a service
//            supplimentaryView = footerView
//        }
//        return supplimentaryView
//    }
//
//
//    // MARK: UICollectionViewDelegate
//    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        collectionView.deselectItem(at: indexPath, animated: false)
//
//        if (indexPath.item < self.imagesInfo.count) {
//            let pickedImage = self.imagesInfo[indexPath.row]
//            self.performSegue(withIdentifier: "showImageDetailView", sender: pickedImage)
//        } else {
//            if let cell = collectionView.cellForItem(at: indexPath) as? ImageCollectionViewCell {
//                cell.selectionClosure()
//            }
//        }
//    }
//}
//
//extension OFESelectedPhotosCollectionViewController: ImagePickerDelegate {
//    func pass(imageInfo: [String: Any]) {
//
//        self.emptyStateAddPhotoView?.performSelector(onMainThread: #selector(self.emptyStateAddPhotoView?.removeFromSuperview), with: nil, waitUntilDone: false)
//
//        if (self.navigationItem.rightBarButtonItem?.isEnabled == false) {
//            self.navigationItem.rightBarButtonItem?.isEnabled = true
//        }
//        self.imagesInfo.append(imageInfo)
//
//        self.collectionView?.reloadData()
//        self.imagePicker?.dismissImagePicker()
//    }
//
//    func show(alert: UIAlertController) {
//        if let popoverController = alert.popoverPresentationController {
//            popoverController.sourceView = emptyStateAddPhotoView!.getAddPhotoButton()
//            popoverController.sourceRect = emptyStateAddPhotoView!.getAddPhotoButton().bounds
//        }
//        self.present(alert, animated: true, completion: nil)
//    }
//
//    func present(imagePicker: UIImagePickerController) {
//        self.present(imagePicker, animated: true, completion: nil)
//    }
//}
//
//extension OFESelectedPhotosCollectionViewController: AddPhotoViewDelegate {
//    func showPhotoPicker() {
//        self.imagePicker?.selectImage()
//    }
//}
