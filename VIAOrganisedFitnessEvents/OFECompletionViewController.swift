//
//  OFECompletionViewController.swift
//  VitalityActive
//
//  Created by OJ Garde on 4/13/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import UIKit
import VIAUIKit
import VitalityKit
import VIAUtilities

public class OFECompletionViewController: VIACirclesViewController {
    override public func awakeFromNib() {
        super.awakeFromNib()
        
        self.viewModel = OFECompletionViewModel()
    }
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
        if let viewModel = self.viewModel as? OFECompletionViewModel {
            viewModel.finalizeSubmission()
        }
    }
    
    override public func buttonTapped(_ sender: UIButton) {
        let notificationName = Notification.Name(rawValue: ofeSubmissionCompleteNotificationKey)
        NotificationCenter.default.post(name: notificationName, object: nil)
    
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
}

public class OFECompletionViewModel: AnyObject, CirclesViewModel {
    public required init() {
        // Init
    }
    
    public var image: UIImage? {
        return VIAOrganisedFitnessEventsAsset.ofeCheckMark.image
    }
    
    public var heading: String? {
        return CommonStrings.Ofe.Completed.Header1394
    }
    
    public var message: String? {
        return CommonStrings.Ofe.Completed.Description1395
    }
    
    public var footnote: String? {
        return CommonStrings.Confirmation.CompletedFootnotePointsMessage119
    }
    
    public var buttonTitle: String? {
        return CommonStrings.Ofe.GreatButtonTitle120
    }
    
    public var buttonHighlightedTextColor: UIColor? {
        return UIColor.improveYourHealthBlue()
    }
    
    public var gradientColor: Color {
        return .blue
    }
    
    func finalizeSubmission() {
        // Delete captured results
        let ofeRealm = DataProvider.newOFERealm()
        try? ofeRealm.write {
            ofeRealm.delete(ofeRealm.allOFECapturedResults())
            ofeRealm.delete(ofeRealm.allPhotosAssets())
        }
    }
}
