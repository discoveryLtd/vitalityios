//
//  OFELearnMoreViewModel.swift
//  VitalityActive
//
//  Created by Harry C. Lingad on 4/11/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import VIAUIKit
import VitalityKit
import VIACommon

public class OFELearnMoreViewModel: AnyObject, VIALearnMoreViewModel {
    
    public var title: String {
        return CommonStrings.LearnMoreButtonTitle104
    }
    
    public var contentItems: [LearnMoreContentItem] {
        var items = [LearnMoreContentItem]()
    
        
        
        items.append(LearnMoreContentItem(
                        header: CommonStrings.Ofe.LearnMore.Header1348,
                        content: CommonStrings.Ofe.LearnMore.Description1349
                    ))
        
        
        items.append(LearnMoreContentItem(
            header: CommonStrings.Ofe.Onboarding.Heading1340,
            content: CommonStrings.Ofe.LearnMore.Description1350,
            image: VIAOrganisedFitnessEventsAsset.rewards50.templateImage
        ))
        
        items.append(LearnMoreContentItem(
            header: CommonStrings.Ofe.LearnMore.Header1351,
            content: CommonStrings.Ofe.LearnMore.Description1352,
            image: VIAOrganisedFitnessEventsAsset.calendar50.templateImage))
        

        
        return items
    }
    
    public var imageTint: UIColor {
        return .improveYourHealthBlue()
    }
}

