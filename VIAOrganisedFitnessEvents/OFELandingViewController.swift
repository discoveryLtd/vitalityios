//
//  OFELandingViewController.swift
//  VitalityActive
//
//  Created by Harry C. Lingad on 4/10/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import VIAUIKit
import VIACommon
import VIAUtilities
import VitalityKit

let ofeSubmissionCompleteNotificationKey = "ofeSubmissionCompleteNotificationKey"

class OFELandingViewController: VIATableViewController {
    var viewModel = OFELandingViewModel()
    let ofeSubmissionCompleteNotificationName = Notification.Name(rawValue: ofeSubmissionCompleteNotificationKey)
    
    var shouldFetchOFEFeatures: Bool = false
    var hasLastClaimedEvent: Bool = false
    
    // MARK: - Navigation
    fileprivate let SEGUE_LEARN_MORE        = "segueLearnMore"
    fileprivate let SEGUE_CLAIM_POINTS      = "segueClaimPoints"
    fileprivate let SEGUE_HISTORY           = "segueHistory"
    fileprivate let SEGUE_HELP              = "segueHelp"
    fileprivate let SEGUE_ONBOARDING        = "showOnboardingSegue"
    fileprivate let SEGUE_EVENTS            = "segueEvents"
    
    deinit {
        // Remove notifications.
        removeObservers()
    }
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        
        showOnboardingIfNeeded()
        self.title = viewModel.landingViewTitle()
        configureTableView()
        
        // Add notifications.
        createObservers()
        
        loadOFEFeatureData()
    }
    
    public override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // TODO: Dispose of any resources that can be recreated.
    }
    
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        configureAppearance()
        
        if shouldFetchOFEFeatures {
            shouldFetchOFEFeatures = false
            refreshOFEFeatureData()
        }
    }
    
    public override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    public override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.hideBackButtonTitle()
    }
    
    func createObservers() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(shouldFetch(notification:)),
                                               name: ofeSubmissionCompleteNotificationName, object: nil)
    }
    
    func removeObservers() {
        NotificationCenter.default.removeObserver(self)
    }
    
    func refreshOFEFeatureData() {
        // Refresh model.
        viewModel = OFELandingViewModel()
        hasLastClaimedEvent = viewModel.hasLastClaimedEvent()
        performRefresh()
        
        tableView.setContentOffset(.zero, animated:true)
    }
    
    func shouldFetch(notification: NSNotification) {
        if notification.name == ofeSubmissionCompleteNotificationName {
            shouldFetchOFEFeatures = true
        }
    }
    
    /*  API Call
     */
    func performRefresh() {
        loadOFEFeatureData()
        self.tableView.refreshControl?.endRefreshing()
    }
    
    func loadOFEFeatureData() {
        showHUDOnView(view: self.view)
        viewModel.fetchOFEFeature(forceUpdate: true) { [weak self] error in
            // self?.hideHUDFromView(view: self?.view)
            // self?.tableView.refreshControl?.endRefreshing()
            
            guard error == nil else {
                self?.hideHUDFromView(view: self?.view)
                self?.tableView.refreshControl?.endRefreshing()
                self?.handleErrorOccurred(error!, isCacheOutdated: false)
                return
            }
            
            // TODO: Interim - Get last event workaround using history data
            // https://jira.vitalityservicing.com/browse/VA-32063
            self?.viewModel.fetchHistory() {
                DispatchQueue.main.async{
                    self?.hasLastClaimedEvent = (self?.viewModel.hasLastClaimedEvent())!
                    self?.hideHUDFromView(view: self?.view)
                    self?.tableView.refreshControl?.endRefreshing()
                    self?.tableView.reloadData()
                }
            }
        }
    }
    
    func handleErrorOccurred(_ error: Error, isCacheOutdated: Bool?) {
        let backendError = error as? BackendError ?? BackendError.other
        if let outdated = isCacheOutdated, outdated == true {
            let statusView = self.handleBackendErrorWithStatusView(backendError, tryAgainAction: { [weak self] in
                self?.loadOFEFeatureData()
            })
            configureStatusView(statusView)
        } else {
            self.handleBackendErrorWithAlert(backendError, tryAgainAction: { [weak self] in
                self?.loadOFEFeatureData()
            })
        }
    }
    
    public override func numberOfSections(in tableView: UITableView) -> Int {
        return hasLastClaimedEvent ? SectionsWithLastClaimedEvent.allValues.count : Sections.allValues.count
    }
    
    public override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if hasLastClaimedEvent {
            return SectionsWithLastClaimedEvent(rawValue: section)?.rowCount() ?? 0
        } else {
            return Sections(rawValue: section)?.rowCount() ?? 0
        }
    }
}

// MARK: Custom UITableViewCell
extension OFELandingViewController{
    func headerViewCell(at indexPath: IndexPath) -> OFELandingHeaderViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: OFELandingHeaderViewCell.defaultReuseIdentifier, for: indexPath) as! OFELandingHeaderViewCell

        //cell.headerImage = VIAOrganisedFitnessEventsAsset.imgTrailRunning.image//OFEHeader.headerCell.headerImage()
        cell.headerImageView.image = VIAOrganisedFitnessEventsAsset.imgTrailRunning.image
        
        cell.title = OFEHeader.headerCell.text()
        
        cell.message = OFEHeader.headerMesssage.text()
        cell.button.setTitle(OFEHeader.buttonLabel.text(), for: .normal)
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        
        // MARK: Confirm and Submit button
        
        cell.didTapButton = { [weak self] in
            self?.captureResultsTapped()
        }
        
        return cell
    }
    
    func lastClaimedEventCell(at indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: OFELastClaimedEventCell.defaultReuseIdentifier, for: indexPath) as? OFELastClaimedEventCell else {
            return tableView.defaultTableViewCell()
        }
        
        cell.accessoryType = .none
        cell.selectionStyle = .none
        
        cell.setCell(title: viewModel.getLastClaimedEventTypeName())
        cell.setCell(value: viewModel.getLastClaimedEventDistance())
        cell.setCell(subTitle: viewModel.getLastClaimedEventSubmissionDate())
        
        return cell
    }
    
    func regularCell(at indexPath: IndexPath) -> VIATableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: VIATableViewCell.defaultReuseIdentifier, for: indexPath) as! VIATableViewCell
        
        if let menuItem = ActionMenu(rawValue: indexPath.row) {
            cell.labelText = menuItem.text()
            cell.cellImage = menuItem.imageIcon()
        }
        
        cell.cellImageView.tintColor = UIColor.currentGlobalTintColor()
        cell.accessoryType = .disclosureIndicator
        return cell
    }
}

// MARK: - UITableView DataSource + Delegate
extension OFELandingViewController{
    public override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    public override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        let sectionValue = SectionsWithLastClaimedEvent(rawValue: section)
        if sectionValue == .headerCell {
            return CGFloat.leastNonzeroMagnitude
        }
        
        return UITableViewAutomaticDimension
    }
    
    public override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier) as? VIATableViewSectionHeaderFooterView else {
            return nil
        }
        
        if hasLastClaimedEvent {
            if let sectionValue = SectionsWithLastClaimedEvent(rawValue: section) {
                if sectionValue != .lastClaimedEventCell {
                    return nil
                }
                
                view.labelText = CommonStrings.Ofe.Landing.SectionHeader1396.uppercased()
            }
        } else {
            return nil
        }
        
        return view
    }
    
    public override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    public override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return nil
    }
    
    public override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if hasLastClaimedEvent {
            let sectionValue = SectionsWithLastClaimedEvent(rawValue: indexPath.section)
            
            if sectionValue == .headerCell {
                return headerViewCell(at: indexPath)
            } else if sectionValue == .lastClaimedEventCell {
                return lastClaimedEventCell(at: indexPath)
            } else {
                return regularCell(at: indexPath)
            }
        } else {
            let sectionValue = Sections(rawValue: indexPath.section)
            
            if sectionValue == .headerCell {
                return headerViewCell(at: indexPath)
            } else {
                return regularCell(at: indexPath)
            }
        }
    }
    
    public override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if hasLastClaimedEvent {
            let sectionValue = SectionsWithLastClaimedEvent(rawValue: indexPath.section)
            
            if sectionValue == .menuCell {
                if let menuItem = ActionMenu(rawValue:  indexPath.row) {
                    menuItem.action(vc: self)
                }
            }
        } else {
            let sectionValue = Sections(rawValue: indexPath.section)
            
            if sectionValue == .menuCell {
                if let menuItem = ActionMenu(rawValue:  indexPath.row) {
                    menuItem.action(vc: self)
                }
            }
        }
    }
}

// MARK: Segue
extension OFELandingViewController {
    func captureResultsTapped() {
        self.performSegue(withIdentifier: SEGUE_CLAIM_POINTS, sender: self)
    }
    
    func learnMoreTapped() {
        self.performSegue(withIdentifier: SEGUE_LEARN_MORE, sender: self)
    }
    
    func eventsAndPointsTapped(){
        self.performSegue(withIdentifier: SEGUE_EVENTS, sender: self)
    }
    
    func historyTapped() {
        self.performSegue(withIdentifier: SEGUE_HISTORY, sender: self)
    }
    
    func helpTapped() {
        self.performSegue(withIdentifier: SEGUE_HELP, sender: self)
    }
    
    func showFirstTimeOnboarding() {
        // TODO: Create a preference for hasShownOFEOnboarding
        //if !AppSettings.hasShownSAVOnboarding() {
        //    self.performSegue(withIdentifier: SEGUE_ONBOARDING, sender: self)
        //}
    }
    
    /* Prepare everything before navigation to another ViewController */
    public override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }
}

// MARK: OFE Landing Configuration
extension OFELandingViewController{
    /* Action Menu */
    enum ActionMenu: Int, EnumCollection {
        case Events = 0
        case History = 1
        case LearnMore = 2
        case Help = 3
        
        func imageIcon() -> UIImage{
            switch self {
            case .Events:
                return VIAOrganisedFitnessEventsAsset.manRunningSmall.templateImage
            case .History:
                return VIAOrganisedFitnessEventsAsset.history.templateImage
            case .LearnMore:
                return VIAOrganisedFitnessEventsAsset.learnMoreSmall.templateImage
            case .Help:
                return VIAOrganisedFitnessEventsAsset.questionMarkSmall.templateImage
            }
        }
        
        func text() -> String? {
            switch self {
            case .Events:
                return CommonStrings.Ofe.Landing.ActionTitle1344
            case .History:
                return CommonStrings.HistoryButton140
            case .LearnMore:
                return CommonStrings.LearnMoreButtonTitle104
            case .Help:
                return CommonStrings.MenuHelpButton8
            }
        }
        
        func action(vc:OFELandingViewController) {
            switch self {
            case .Events:
                vc.eventsAndPointsTapped()
            case .History:
                vc.historyTapped()
            case .LearnMore:
                vc.learnMoreTapped()
            case .Help:
                vc.helpTapped()
            }
        }
    }
    
    /* Header */
    enum OFEHeader: Int, EnumCollection {
        case headerCell = 0
        case headerMesssage = 1
        case buttonLabel = 2
        
        func text() -> String? {
            switch self {
            case .headerCell:
                return CommonStrings.Ofe.Landing.Header1342
            case .headerMesssage:
                return CommonStrings.Ofe.Landing.Description1343
            case .buttonLabel:
                return CommonStrings.Ofe.Action.Title1337
            }
        }
        
        func headerImage() -> UIImage {
            return VIAOrganisedFitnessEventsAsset.manRunningSmall.templateImage
        }
    }
    
    /* Landing Page Section */
    enum SectionsWithLastClaimedEvent: Int, EnumCollection {
        case headerCell = 0
        case lastClaimedEventCell = 1
        case menuCell = 2
        
        func rowCount() -> Int {
            switch self {
            case .headerCell:
                return 1
            case .lastClaimedEventCell:
                return 1
            case .menuCell:
                return ActionMenu.allValues.count
            }
        }
    }
    
    enum Sections: Int, EnumCollection {
        case headerCell = 0
        case menuCell = 1
        
        func rowCount() -> Int {
            switch self {
            case .headerCell:
                return 1
            case .menuCell:
                return ActionMenu.allValues.count
            }
        }
    }
}

// MARK: UI Initialization
extension OFELandingViewController{
    // MARK: Setup
    func configureAppearance() {
        VIAAppearance.setGlobalTintColorToImproveYourHealth()
        navigationController?.makeNavigationBarTransparent()
    }
    
    // MARK: Show On Boarding once
    func showOnboardingIfNeeded() {
        if !AppSettings.hasShownOrganisedFitnessEventsOnboarding() {
            self.performSegue(withIdentifier: "showOnBoarding", sender: self)
        }
    }
    
    func configureTableView() {
        self.tableView.register(VIATableViewCell.nib(), forCellReuseIdentifier: VIATableViewCell.defaultReuseIdentifier)
        self.tableView.register(VIATableViewSectionHeaderFooterView.nib(), forHeaderFooterViewReuseIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier)
        self.tableView.register(OFELandingHeaderViewCell.nib(), forCellReuseIdentifier: OFELandingHeaderViewCell.defaultReuseIdentifier)
        self.tableView.register(OFELastClaimedEventCell.nib(), forCellReuseIdentifier: OFELastClaimedEventCell.defaultReuseIdentifier)
        self.tableView.estimatedRowHeight = 200
        self.tableView.separatorInset = UIEdgeInsets(top: 0, left: 55, bottom: 0, right: 0)
        
        self.tableView.refreshControl = UIRefreshControl()
        self.tableView.refreshControl?.tintColor = UIColor.cellSeperator()
        self.tableView.refreshControl?.addTarget(self, action: #selector(performRefresh), for: .valueChanged)
    }
    
    public override func configureStatusView(_ view: UIView?) {
        removeStatusView()
        
        guard let view = view else { return }
        
        self.view.addSubview(view)
        
        view.snp.makeConstraints { (make) in
            make.width.equalToSuperview()
            make.left.right.top.equalToSuperview()
            make.height.equalToSuperview().offset(self.tableView.contentOffset.y)
        }
    }
    
    public override func removeStatusView() {
        self.view.subviews.forEach { (view) in
            if view is VIAStatusView {
                view.removeFromSuperview()
            }
        }
    }
}
