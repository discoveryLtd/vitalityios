//
//  OFEHistoryViewModel.swift
//  VitalityActive
//
//  Created by Val Tomol on 04/05/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import VitalityKit
import VIAUIKit
import VIAUtilities
import RealmSwift

public class OFEHistoryViewModel: AnyObject {
    // MARK: Properties
    
    // MARK: Functions
    func historyViewTitle() -> String {
        return CommonStrings.HistoryButton140
    }
    
    func prettifyDateFromString(dateString: String) -> String {
        let stringToDateFormatterWithMilliseconds: DateFormatter = {
            let dateFormatter = DateFormatter.apiManagerFormatterWithMilliseconds(localeIdentifier: DeviceLocale.toString())
            return dateFormatter
        }()
        
        let stringToDateFormatter: DateFormatter = {
            let dateFormatter = DateFormatter.apiManagerFormatter(localeIdentifier: DeviceLocale.toString())
            return dateFormatter
        }()
        
        let dateToStringFormatter: DateFormatter = {
            let dateFormatter = DateFormatter.dayDateShortMonthyearFormatter()
            return dateFormatter
        }()
        
        if !dateString.isEmpty {
            if let dateMill = stringToDateFormatterWithMilliseconds.date(from: dateString) {
                return dateToStringFormatter.string(from: dateMill)
            } else if let date = stringToDateFormatter.date(from: dateString) {
                return dateToStringFormatter.string(from: date)
            } else {
                return ""
            }
        }
        
        return ""
    }
    
    func fetchUploadedImages(eventsPoint: EventsPoints, completion: @escaping ((_: [UIImage])) -> Void) {
        var images: [UIImage] = []
        var imageCount: Int = 0
        let totalImageCount: Int = getImageCount(eventsPoint: eventsPoint)
        
        if totalImageCount > 0 {
            for eventMetadata in eventsPoint.eventMetadatas! {
                if eventMetadata.typeKey == EventMetaDataTypeRef.DocumentReference.rawValue {
                    if let referenceId = eventMetadata.value {
                        getUploadedProof(refId: Int(referenceId)!) { image in
                            images.append(image)
                            imageCount += 1
                            
                            if imageCount == totalImageCount {
                                return completion(images)
                            }
                        }
                    }
                }
            }
        } else {
            return completion(images)
        }
    }
    
    func getImageCount(eventsPoint: EventsPoints) -> Int {
        var count: Int = 0
        for eventMetadata in eventsPoint.eventMetadatas! {
            if eventMetadata.typeKey == EventMetaDataTypeRef.DocumentReference.rawValue {
                count += 1
            }
        }
        
        return count
    }
}

extension OFEHistoryViewModel: CMSConsumer {
    func getUploadedProof(refId: Int, completion: @escaping ((_: UIImage) -> Void)) {
        self.downloadHistoryImage(referenceId: refId) { url, error in
            let defaultLogo = VIAOrganisedFitnessEventsAsset.questionMarkSmall.image
            guard url != nil, error == nil, let filePath = url?.path, FileManager.default.fileExists(atPath: filePath) else {
                debugPrint("Could not retrieve downloaded image from disk")
                return completion(defaultLogo)
            }
            
            if let img = UIImage(contentsOfFile: filePath) {
                debugPrint("Image retrieved successfully from service")
                return completion(img)
            }
            
            debugPrint("Falied to download image")
            return completion(defaultLogo)
        }
    }
}
