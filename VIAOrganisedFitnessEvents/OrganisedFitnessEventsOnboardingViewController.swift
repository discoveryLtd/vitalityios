//
//  OrganisedFitnessEventsOnboardingViewController.swift
//  VitalityActive
//
//  Created by Erik John T. Alicaya (ADMIN) on 12/04/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import VIACommon
import VitalityKit
import VIAUIKit

class OrganisedFitnessEventsOnBoardingViewController: VIAOnboardingViewController {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.viewModel = OrganisedFitnessEventsOnBoardingViewModel()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.sectionHeaderHeight = 0
        VIAAppearance.setGlobalTintColorToImproveYourHealth()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.navigationBar.isHidden = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.hideBackButtonTitle()
    }
    
    // MARK: - Navigation
    override func mainButtonTapped(_ sender: UIButton) {
        AppSettings.setHasShownOrganisedFitnessEventsOnboarding()
        self.dismiss(animated: true, completion: nil)
    }
    
    override func alternateButtonTapped(_ sender: UIButton) {
        self.performSegue(withIdentifier: "segueLearnMore", sender: self)
    }
}
