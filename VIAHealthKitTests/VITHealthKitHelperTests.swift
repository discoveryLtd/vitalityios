//
//  VITHealthKitHelperTests.swift
//  VitalityActive
//
//  Created by Wilmar van Heerden on 2017/06/30.
//  Copyright © 2017 Glucode. All rights reserved.
//

import XCTest
import VIAHealthKit

class VITHealthKitHelperTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testExample() {
        let helper = VITHealthKitHelper.shared()
        XCTAssertNotNil(helper, "Main VITHealthKitHelper is nil")
    }
    
}
