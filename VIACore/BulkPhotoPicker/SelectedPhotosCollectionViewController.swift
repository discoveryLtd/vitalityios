//
//  SelectedPhotosCollectionViewController.swift
//  VitalityActive
//
//  Created by Marius Janse van Vuuren on 3/27/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

import UIKit
import VIAUIKit
import VitalityKit
import VIACommon
import Photos
import RealmSwift

class SelectedPhotosCollectionViewController: UICollectionViewController {
    var imagesInfo: Array<Dictionary<String, Any>> = []
    var imagePicker: VHCSelectImageController?
    var emptyStateAddPhotoView: AddPhotoView?
    var realm: Realm?
    var viewCounter: Int = 0

    @IBOutlet weak var selectPhotosFlowLayout: UICollectionViewFlowLayout!
    @IBAction func unwindToPhotosCollection(segue: UIStoryboardSegue) {
        self.collectionView?.reloadData()
    }

    public func remove(imageInfo: [String: Any]) {
        if let pickedImage = imageInfo[UIImagePickerControllerOriginalImage] as? UIImage {
            var index = 0
            for helpImageInfo in self.imagesInfo {
                if let heldImage = helpImageInfo[UIImagePickerControllerOriginalImage] as? UIImage {
                    if (pickedImage.isEqual(heldImage)) {
                        self.imagesInfo.remove(at: index)
                    }
                }
                index += 1
            }
        }
    }

    public func add(imageInfo: [String : Any]) {
        self.imagesInfo.append(imageInfo)
    }

    deinit {
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        self.collectionView?.alwaysBounceVertical = true
        self.title = VhcStrings.Proof.AddProofScreenTitle163
        guard let configuration = VIAApplicableFeatures.default.getImageControllerConfiguration()
            as? UIImagePickerController.ImageControllerConfiguration else {return}
        
        imagePicker = VHCSelectImageController(pickerDelegate: self, configuration: configuration)
        self.view.backgroundColor = UIColor.day()

        self.setCollectionViewCellSize()
        self.registerCollectionViewCells()
        self.setupNavigationItem()

        self.clearsSelectionOnViewWillAppear = false
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        self.hideBackButtonTitle()

        if (isCollectionEmpty()) {
            emptyStateAddPhotoView = AddPhotoView.viewFromNib(owner: self) as? AddPhotoView

            emptyStateAddPhotoView?.setHeader(title: VhcStrings.Proof.AddProofEmptyTitle164)
            emptyStateAddPhotoView?.setDetail(message: VhcStrings.Proof.AddProofEmptyMessage165)
            emptyStateAddPhotoView?.setAction(title: VhcStrings.Proof.AddButton166)
            emptyStateAddPhotoView?.setHeader(image: UIImage.templateImage(asset: .cameraLarge))
            emptyStateAddPhotoView?.set(photoViewDelegate: self)
            emptyStateAddPhotoView?.frame = self.collectionView?.frame ?? CGRect(x: 0, y: 0, width: 0, height: 0)
            emptyStateAddPhotoView?.tag = 1
            
            if emptyStateAddPhotoView != nil {
                emptyStateAddPhotoView?.tag = 1
                self.viewCounter += 1
                self.collectionView?.addSubview(emptyStateAddPhotoView!)
                if (self.navigationItem.rightBarButtonItem?.isEnabled ?? false) {
                    self.navigationItem.rightBarButtonItem?.isEnabled = false
                }
            }
        } else {
            while self.viewCounter != 1{
                if let EmptyStateViewWithTag = self.collectionView?.viewWithTag(1) {
                    EmptyStateViewWithTag.removeFromSuperview()
                    self.collectionView?.reloadData()
                    self.viewCounter -= 1
                }
            }
        }
    }

    func isCollectionEmpty() -> Bool {
        return self.imagesInfo.count == 0
    }

    func registerCollectionViewCells() {
        self.collectionView!.register(ImageCollectionViewCell.nib(), forCellWithReuseIdentifier: ImageCollectionViewCell.defaultReuseIdentifier)
        self.collectionView!.register(LabelSupplimentaryView.nib(), forSupplementaryViewOfKind: UICollectionElementKindSectionFooter, withReuseIdentifier: "Footer")
    }

    func setupNavigationItem() {
        let nextBarButton = UIBarButtonItem(title: CommonStrings.NextButtonTitle84, style: .done, target: self, action: #selector(submitPhotos))
        self.navigationItem.rightBarButtonItem = nextBarButton
    }

    func submitPhotos() {
        self.showHUDOnWindow()
        self.beginURLWrite(completion: {
            self.hideHUDFromWindow()
            self.performSegue(withIdentifier: "showSummary", sender: self)
        })
    }

    func beginURLWrite(completion: @escaping () -> Void) {
        DispatchQueue.global(qos: .background).async {
            let realm = DataProvider.newVHCRealm()
            var realmAssets = Array<PhotoAsset>()

            for image in self.imagesInfo {
                let realmAsset = PhotoAsset()
                if let uiImageURL = image[UIImagePickerControllerReferenceURL] as? URL {
                    let photosAsset = PHAsset.fetchAssets(withALAssetURLs: [uiImageURL], options: nil).firstObject
                    let localIdentifier = photosAsset?.localIdentifier
                    realmAsset.assetURL = localIdentifier
                } else {
                    if let uiImage = image[UIImagePickerControllerOriginalImage] as? UIImage {
                        if let imageData = PhotoAsset.compress(image:uiImage) {
                            realmAsset.assetData = imageData
                        }
                    }
                }
                realmAssets.append(realmAsset)
            }

            try! realm.write {
                realm.delete(realm.allPhotosAssets())
                realm.add(realmAssets)
            }

            DispatchQueue.main.async {
                completion()
            }
        }
    }

    func setCollectionViewCellSize() {
        let leftAndRightSpaceingTotal: CGFloat = 15
        let cellPaddingTotal: CGFloat = 15
        let totalWidthPadding: CGFloat = leftAndRightSpaceingTotal + cellPaddingTotal
        let itemWidth = (((self.collectionView?.frame.size.width) ?? totalWidthPadding) - totalWidthPadding) / 2
        self.selectPhotosFlowLayout.itemSize = CGSize(width: itemWidth,
                                                      height: itemWidth)
        self.selectPhotosFlowLayout.minimumLineSpacing = 10
    }

    func dequeToCaptureResults() {
        self.performSegue(withIdentifier: "unwindToCaptureResults", sender: self)
    }

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let selectedImage = sender as? [String : Any] else {
            return
        }
        guard let detailImageViewController = segue.destination as? ImageDetailViewController else {
            return
        }

        detailImageViewController.set(selectedImage: selectedImage)
    }

    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let addMoreImagesCellCount = 1
        var itemCount = self.imagesInfo.count
        
        if VIAApplicableFeatures.default.shouldLimitNumberOfAttachProof!{
            if (self.imagesInfo.count < 11) {
                itemCount += addMoreImagesCellCount
            }
        } else {
            itemCount += addMoreImagesCellCount
        }
        
        return itemCount
        
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ImageCollectionViewCell.defaultReuseIdentifier, for: indexPath) as? ImageCollectionViewCell
        cell?.imageView.image = nil
        cell?.resetCell()

        if (indexPath.item == self.imagesInfo.count) {
            if let plusImage = UIImage.templateImage(asset: .addPhoto) {
                cell?.setSmall(image: plusImage)
            }
            cell?.selectionClosure = {
                self.imagePicker?.selectImage()
            }
        } else {
            if let pickedImage = self.imagesInfo[indexPath.row][UIImagePickerControllerOriginalImage] as? UIImage {
                cell?.imageView.image = pickedImage
            }
        }
        guard let returnCell = cell else { return UICollectionViewCell() }
        return returnCell
    }

    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        var supplimentaryView: UICollectionReusableView = UICollectionReusableView()
        if (kind == UICollectionElementKindSectionFooter) {
            guard let footerView = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionFooter, withReuseIdentifier: "Footer", for: indexPath) as? LabelSupplimentaryView else {
                return UICollectionReusableView()
            }
            
            let totalImages = String(self.imagesInfo.count)
            
            if VIAApplicableFeatures.default.shouldLimitNumberOfAttachProof!{
                footerView.setView(text: VhcStrings.Proof.AttachmentsFootnoteMessage177(totalImages, "11"))
                // TODO: If the 11 should come from a service
            } else {
                footerView.setView(text: VhcStrings.Proof.AttachmentsFootnoteMessage177(totalImages, totalImages))
            }
            
            supplimentaryView = footerView
        }
        return supplimentaryView
    }


    // MARK: UICollectionViewDelegate
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: false)

        if (indexPath.item < self.imagesInfo.count) {
            let pickedImage = self.imagesInfo[indexPath.row]
            self.performSegue(withIdentifier: "showImageDetailView", sender: pickedImage)
        } else {
            if let cell = collectionView.cellForItem(at: indexPath) as? ImageCollectionViewCell {
                cell.selectionClosure()
            }
        }
    }
}

extension SelectedPhotosCollectionViewController: VHCImagePickerDelegate {
    func pass(imageInfo: [String: Any]) {
        self.emptyStateAddPhotoView?.performSelector(onMainThread: #selector(self.emptyStateAddPhotoView?.removeFromSuperview), with: nil, waitUntilDone: false)

        if (self.navigationItem.rightBarButtonItem?.isEnabled == false) {
            self.navigationItem.rightBarButtonItem?.isEnabled = true
        }
        self.imagesInfo.append(imageInfo)

        self.collectionView?.reloadData()
        self.imagePicker?.dismissImagePicker()
    }

    func show(alert: UIAlertController) {
        if let popoverController = alert.popoverPresentationController {
            popoverController.sourceView = emptyStateAddPhotoView!.getAddPhotoButton()
            popoverController.sourceRect = emptyStateAddPhotoView!.getAddPhotoButton().bounds
        }
        self.present(alert, animated: true, completion: nil)
    }

    func present(imagePicker: UIImagePickerController) {
        self.present(imagePicker, animated: true, completion: nil)
    }
}

extension SelectedPhotosCollectionViewController: AddPhotoViewDelegate {
    func showPhotoPicker() {
        self.imagePicker?.selectImage()
    }
}
