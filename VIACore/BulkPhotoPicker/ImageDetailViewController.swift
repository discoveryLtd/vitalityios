//
//  ImageDetailViewController.swift
//  VitalityActive
//
//  Created by Marius Janse van Vuuren on 3/30/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

import UIKit
import VitalityKit

class ImageDetailViewController: UIViewController {

    @IBAction func imageTapped(_ sender: Any) {
        let navBarController = self.navigationController

        if (navBarController?.navigationBar.isHidden ?? true) {
            self.setNavigation(hidden : false)
            self.view.backgroundColor = UIColor.day()
        } else {
            self.setNavigation(hidden : true)
            self.view.backgroundColor = UIColor.night()
        }
    }

    func setNavigation(hidden: Bool) {
        let navBarController = self.navigationController
        navBarController?.setNavigationBarHidden(hidden, animated: true)
        self.navigationController?.setToolbarHidden(hidden, animated: true)
    }

    @IBOutlet weak var detailImageView: UIImageView!
    internal var detailImage: [String : Any]!
    override func viewDidLoad() {
        super.viewDidLoad()
        if let pickedImage = detailImage[UIImagePickerControllerOriginalImage] as? UIImage {
            self.detailImageView.image = pickedImage
        }

    }

    func set(selectedImage: [String : Any]) {
        self.detailImage = selectedImage
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setupToolBar()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }

    func setupToolBar() {
        var toolbarItemsArray = Array<UIBarButtonItem>()

        let flexibleSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        toolbarItemsArray.append(flexibleSpace)
        let deleteItem = UIBarButtonItem(image: UIImage.templateImage(asset: .trashSmall), style: .plain, target: self, action: #selector(deleteSelectedItem))
        toolbarItemsArray.append(deleteItem)

        self.setToolbarItems(toolbarItemsArray, animated: false)
        self.navigationController?.setToolbarHidden(false, animated: false)
    }

    override func viewWillDisappear(_ animated: Bool) {
        self.detailImageView.transform = CGAffineTransform(scaleX: 1, y: 1)
        self.navigationController?.setToolbarHidden(true, animated: false)
        super.viewWillDisappear(animated)
    }

    func deleteSelectedItem(sender: UIBarButtonItem) {
        let alertController = UIAlertController(title: "", message: VhcStrings.Proof.RemoveMessage178, preferredStyle: UIAlertControllerStyle.actionSheet)
        let cancelAction = UIAlertAction(title: CommonStrings.CancelButtonTitle24, style: .cancel) { (UIAlertAction) in
            alertController.dismiss(animated: true, completion: nil)
        }
        let removePhotoAction = UIAlertAction(title: VhcStrings.Proof.RemoveButton179, style: .destructive) { [weak self] (_) -> Void in
            self?.performSegue(withIdentifier: "unwindToPhotos", sender: self?.detailImage)
        }
        
        alertController.popoverPresentationController?.barButtonItem = sender

        alertController.addAction(removePhotoAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let destination = segue.destination as? SelectedPhotosCollectionViewController else {
            return
        }

        if let detailImage = sender as? [String: Any] {
            destination.remove(imageInfo: detailImage)
        }
    }

    // MARK: - Image zoom
    @IBOutlet var pinchGestureRecognizer: UIPinchGestureRecognizer!
    internal var scaleFactor: CGFloat = 1.0
    @IBAction func scaleImage(_ sender: UIPinchGestureRecognizer) {
        switch pinchGestureRecognizer.state {
        case .began :
            pinchGestureRecognizer.scale = scaleFactor
            break
        case .ended:
            let minimumScalefactor: CGFloat = 0.7
            if pinchGestureRecognizer.scale < minimumScalefactor {
                pinchGestureRecognizer.scale = minimumScalefactor
            }
            scaleFactor = pinchGestureRecognizer.scale
            break
        default:
            break
        }

        self.detailImageView.transform = CGAffineTransform(scaleX: pinchGestureRecognizer.scale, y: pinchGestureRecognizer.scale)
        pinchGestureRecognizer.scale = sender.scale
    }
}
