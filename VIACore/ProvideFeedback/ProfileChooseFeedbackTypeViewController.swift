//
//  ProfileChooseFeedbackTypeViewController.swift
//  VitalityActive
//
//  Created by Von Kervin R. Tuico on 15/03/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import VIAUIKit
import VitalityKit
import VIAUtilities
import VIACommon

protocol ProfileChooseFeedbackTypeViewControllerProtocol {
    func updateSelectedFeedbackType(feedbackTypeId: String, feedbackType: String, index: Int)
}

class ProfileChooseFeedbackTypeViewController: VIATableViewController {

    var delegate: ProfileChooseFeedbackTypeViewControllerProtocol!
    
    var selectedFeedbackType: String?
    var selectedFeedbackTypeIndex: Int?
    var selectedFeedbackTpyeId: String?
    
    override func viewWillAppear(_ animated: Bool) {
        if let isNotEmpty = self.selectedFeedbackType, !isNotEmpty.isEmpty {
            self.tableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = CommonStrings.Settings.FeedbackSubjectPlaceholder946
        setupNavBar()
        configureTableView()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if isMovingFromParentViewController {
            if let feedbackType = self.selectedFeedbackType, let index = self.selectedFeedbackTypeIndex,
                let feedbackTypeId = self.selectedFeedbackTpyeId {
                delegate.updateSelectedFeedbackType(feedbackTypeId: feedbackTypeId, feedbackType: feedbackType, index: index)
                self.navigationController?.setToolbarHidden(true, animated: false)
                delegate = nil
            }
        }
    }

    func configureTableView() {
        self.tableView.register(VIAProfileDetailCell.nib(), forCellReuseIdentifier: VIAProfileDetailCell.defaultReuseIdentifier)
        self.tableView.estimatedRowHeight = 96
        self.tableView.separatorInset = UIEdgeInsets(top: 0, left: 52, bottom: 0, right: 0)
        self.tableView.tableFooterView = UIView()
        self.tableView.backgroundColor = UIColor.tableViewBackground()
    }
    
    func setupNavBar() {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.barTintColor = .white
        self.navigationController?.makeNavigationBarTransparent()
        self.hideBackButtonTitle()
    }
    
    // MARK: TableView
    
    enum Sections: Int, EnumCollection, Equatable {
        case feedbackTypes = 0
        
        func title() -> String? {
            switch self {
            case .feedbackTypes:
                return nil
            }
        }
    }
    
    enum FeedbackTypeItems: Int, EnumCollection {
        case technicalAssistance = 0
        case generalAssistance  = 1
        case appExperienceUserFeedback = 2
        
        func title() -> String? {
            switch self {
            case .technicalAssistance:
                return CommonStrings.Settings.FeedbackType1Heading954
            case .generalAssistance:
                return CommonStrings.Settings.FeedbackType2Heading956
            case .appExperienceUserFeedback:
                return CommonStrings.Settings.FeedbackType3Heading958
            }
        }
        
        func description() -> String? {
            switch self {
            case .technicalAssistance:
                return CommonStrings.Settings.FeedbackType1Message955
            case .generalAssistance:
                return CommonStrings.Settings.FeedbackType2Message957
            case .appExperienceUserFeedback:
                return CommonStrings.Settings.FeedbackType3Message959
            }
        }
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return Sections.allValues.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let section = Sections(rawValue: section)
        
        if section == .feedbackTypes {
            return FeedbackTypeItems.allValues.count
        }
        return 0
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let index = self.selectedFeedbackTypeIndex, index == indexPath.row {
            self.tableView.selectRow(at: indexPath, animated: false, scrollPosition: .none)
            cell.accessoryType = .checkmark
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch Sections.allValues[indexPath.section] {
        case .feedbackTypes:
            return feedbackTypesCell(at: indexPath)
        }
    }
    
    func feedbackTypesCell(at indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: VIAProfileDetailCell.defaultReuseIdentifier, for: indexPath)
        let detailCell = cell as! VIAProfileDetailCell
        
        let rows = FeedbackTypeItems.allValues[indexPath.row]
        
        detailCell.labelText = rows.title()
        detailCell.valueText = rows.description()
        
        cell.selectionStyle = .none
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.cellForRow(at: indexPath)?.accessoryType = .checkmark
        
        let cell = tableView.cellForRow(at: indexPath) as! VIAProfileDetailCell
        self.selectedFeedbackType = cell.labelText
        self.selectedFeedbackTypeIndex = indexPath.row
        
        let rows = FeedbackTypeItems.allValues[indexPath.row]
        switch rows {
        case .technicalAssistance:
            self.selectedFeedbackTpyeId = "TECHNICAL_ASSISTANCE"
        case .generalAssistance:
            self.selectedFeedbackTpyeId = "GENERAL_ASSISTANCE"
        case .appExperienceUserFeedback:
            self.selectedFeedbackTpyeId = "APP_EXPERIENCE"
        }
    }
    
    override func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        self.tableView.cellForRow(at: indexPath)?.accessoryType = .none
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
 

}
