//
//  UIStoryboard+VIAAdditions.swift
//  VitalityActive
//
//  Created by Wilmar van Heerden on 2016/10/10.
//  Copyright © 2016 Glucode. All rights reserved.
//

import Foundation

public enum VIACoreStoryboardName: String {
    case loginRegistration = "LoginRegistration"
    case home = "Home"
    case activeRewards = "ActiveRewardsLanding"
    case rewards = "Rewards"
    case termsAndConditions = "TermsAndConditions"
    case nonSmokersDeclaration = "NonSmokersDeclaration"
    case contentDialog = "ContentDialog"
}

public extension UIStoryboard {

    public convenience init(coreStoryboard name: VIACoreStoryboardName) {
        // any class in VIACore.framework will suffice
        let bundle = Bundle(for: VIAFirstTimeOnboardingViewController.self)
        self.init(name: name.rawValue, bundle: bundle)
    }
}
