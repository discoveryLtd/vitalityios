//
//  HUDPresenter.swift
//  VitalityActive
//
//  Created by Wilmar van Heerden on 2016/11/04.
//  Copyright © 2016 Glucode. All rights reserved.
//

import Foundation
import MBProgressHUD

public protocol HUDPresenter {

    func showHUDOnWindow()

    func hideHUDFromWindow()

    func showHUDOnView(view: UIView?)

    func hideHUDFromView(view: UIView?)
}

public extension HUDPresenter {

    func showHUDOnWindow() {
        guard let window = UIApplication.shared.keyWindow else { return }
        MBProgressHUD.showAdded(to: window, animated: true)
    }

    func hideHUDFromWindow() {
        guard let window = UIApplication.shared.keyWindow else { return }
        MBProgressHUD.hide(for: window, animated: true)
    }

    func showHUDOnView(view: UIView?) {
        guard let view = view else { return }
        MBProgressHUD.showAdded(to: view, animated: true)
    }

    func hideHUDFromView(view: UIView?) {
        guard let view = view else { return }
        MBProgressHUD.hide(for: view, animated: true)
    }
}

extension UIViewController: HUDPresenter {}
