//
//  UIViewController+VIAAdditions.swift
//  VitalityActive
//
//  Created by Wilmar van Heerden on 2016/11/07.
//  Copyright © 2016 Glucode. All rights reserved.
//

import Foundation

public extension UIViewController {

    public func makeNavigationBarOpaque() {
        self.navigationController?.navigationBar.setBackgroundImage(nil, for: .default)
        self.navigationController?.navigationBar.shadowImage = nil
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = nil
        self.navigationController?.navigationBar.tintColor = UIApplication.shared.keyWindow?.tintColor
    }

    public func makeNavigationBarTransparent() {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
        self.navigationController?.navigationBar.tintColor = UIColor.day()
    }
}
