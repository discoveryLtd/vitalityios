//
//  BackendErrorHandler.swift
//  VitalityActive
//
//  Created by Wilmar van Heerden on 2016/12/14.
//  Copyright © 2016 Glucode. All rights reserved.
//

import Foundation
import VIAUIKit
import VitalityKit

public protocol BackendErrorHandler: class {

    func handleBackendError(_ error: BackendError, tryAgainAction: @escaping () -> Void)

    func displayGenericConnectivityErrorAlert(tryAgainAction: @escaping (() -> Void))

}

public extension BackendErrorHandler where Self: UIViewController {

    func handleBackendError(_ error: BackendError, tryAgainAction: @escaping () -> Void) {
        switch error {
        case .network:
            self.displayGenericConnectivityErrorAlert(tryAgainAction: tryAgainAction)
        default:
            self.displayUnknownServiceErrorOccurredAlert()
        }
    }

    func displayGenericConnectivityErrorAlert(tryAgainAction: @escaping (() -> Void)) {
        let controller = UIAlertController(title: commonStrings.GenericConnectivityErrorTitle,
                                           message: commonStrings.GenericConnectivityErrorMessage,
                                           preferredStyle: .alert)

        let tryAgain = UIAlertAction(title: commonStrings.GenericTryAgainButtonTitle, style: .default) { action in
            tryAgainAction()
        }
        controller.addAction(tryAgain)

        let cancel = UIAlertAction(title: commonStrings.GenericCancelButtonTitle, style: .cancel) { action in }
        controller.addAction(cancel)

        self.present(controller, animated: true, completion: nil)
    }

    func displayUnknownServiceErrorOccurredAlert() {
        let controller = UIAlertController(title: commonStrings.GenericServiceErrorTitle,
                                           message: commonStrings.GenericServiceErrorMessage,
                                           preferredStyle: .alert)

        let ok = UIAlertAction(title: commonStrings.GenericOkButtonTitle, style: .default) { action in }
        controller.addAction(ok)

        self.present(controller, animated: true, completion: nil)
    }

}

extension VIAViewController: BackendErrorHandler {}
extension VIATableViewController: BackendErrorHandler {}
