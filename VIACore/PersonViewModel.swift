//
//  PersonViewModel.swift
//  VitalityActive
//
//  Created by Joshua Ryan on 7/13/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation

class PersonViewModel {

	var title: String = ""
	var gender: String = ""
	var givenName: String = ""
	var familyName: String = ""
	var dob: Date?
	var preferredName: String = ""
	var suffix: String = ""
	var emailAddresses = [EmailViewModel]()
	var phoneNumbers = [PhoneNumberViewModel]()
	var avatarPath: String?
    var references = [ReferenceViewModel]()
}
