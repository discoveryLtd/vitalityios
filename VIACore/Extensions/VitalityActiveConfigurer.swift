import UIKit
import Foundation

import VIACommon
import VIAUIKit
import VitalityKit
import VIAUtilities

public protocol VitalityActiveConfigurer: class {
    var window: UIWindow? { get }
    func configureVitalityActiveDelegate()
    func configureWireDelegate()
    func setupInitialStory()
    func setupLoginStory()
    func setupSplashScreenStory()
    func securityException(_ notification: Notification)
}

public extension VitalityActiveConfigurer where Self: UIApplicationDelegate {

    func configureVitalityActiveDelegate() {
        // logging
        Logger.configureLogging()

        // register for session expiry
        NotificationCenter.default.addObserver(forName: .VIAServerSecurityException, object: nil, queue: nil) { notification in
            self.securityException(notification)
        }
        
        // register for app update
        NotificationCenter.default.addObserver(forName: .VIAAppUpdateException, object: nil, queue: nil) {
            [weak self] notification in
            self?.updateException(notification)
        }

        // configure
        configureWireDelegate()
        configureAppearanceDataSource()
        setupInitialStory()

        // dataprovider
        DataProvider.startup()
    }

    func configureWireDelegate() {
        if let delegate = self as? WireDelegate {
            Wire.default.delegate = delegate
        }
    }

    func configureAppearanceDataSource() {
        if let dataSource = self as? AppearanceColorDataSource {
            VIAAppearance.default.dataSource = dataSource
        }

        if let dataSource = self as? AppearanceIconographyDataSource {
            VIAIconography.default.dataSource = dataSource
        }
        
        if let dataSource = self as? ApplicableFeaturesDataSource {
            VIAApplicableFeatures.default.dataSource = dataSource
        }
        
        if let dataSource = self as? ViewControllerDelegateDataSource {
            VIAViewControllerFeatures.default.dataSource = dataSource
        }
    }

    func setupInitialStory() {
        let username = AppSettings.getLastLoginUsername()
    
        if let rememberMePreference = AppSettings.usernameForRememberMe(),
            rememberMePreference == username,
            DataProvider.currentVitalityPartyIsValid(shouldLogoutOnNilVitalityParty: false){
            setupSplashScreenStory()
        }else {
            self.setupLoginStory()
        }
    }

    func setupLoginStory() {
        let loginStoryboard = UIStoryboard(coreStoryboard: .loginRegistration)
        self.window?.rootViewController = loginStoryboard.instantiateInitialViewController()
        self.window?.makeKeyAndVisible()
    }

    func setupSplashScreenStory() {
        self.window?.makeKeyAndVisible()
        VIASplashScreenViewController.showInsurerSplashScreen(skipAppConfigRequest: false)
    }

    func securityException(_ notification: Notification) {
        DispatchQueue.main.async {
            VIALoginViewController.showLogin()
        }
    }
    
    func updateException(_ notification: Notification) {
        DispatchQueue.main.async {
            guard let upgradeUrl = notification.object as? String, !upgradeUrl.isEmpty else { return }
            VIALoginViewController.showLogin(upgradeUrl: upgradeUrl)
        }
    }

}
