//
//  UIStoryboard+VIAAdditions.swift
//  VitalityActive
//
//  Created by Wilmar van Heerden on 2016/10/10.
//  Copyright © 2016 Glucode. All rights reserved.
//

import Foundation

public enum VIACoreStoryboardName: String {
    case loginRegistration = "LoginRegistration"
    case loginRestriction = "LoginRestriction"
    case home = "Home"
    case rewards = "Rewards"
    case termsAndConditions = "TermsAndConditions"
    case nonSmokersDeclaration = "NonSmokersDeclaration"
    case pointsMonitor = "PointsMonitor"
    case FirstTimePreference = "FirstTimePreferences"
    case healthCheck = "VHC"
    case wellnessAndDevices = "WellnessDevicesApps"
}

public extension UIStoryboard {

    public convenience init(coreStoryboard name: VIACoreStoryboardName) {
        // any class in VIACore.framework will suffice
        let bundle = Bundle(for: VIAFirstTimeOnboardingViewController.self)
        self.init(name: name.rawValue, bundle: bundle)
    }
}
