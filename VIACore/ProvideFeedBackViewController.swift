//
//  ProvideFeedBackViewController.swift
//  VitalityActive
//
//  Created by Harry C. Lingad on 1/12/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import VitalityKit
import MessageUI

class ProvideFeedBackViewController: UIViewController, UITextViewDelegate, MFMailComposeViewControllerDelegate {
    
    @IBOutlet weak var titleL: UILabel!
    @IBOutlet weak var messageTV: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.title =  CommonStrings.Settings.FeedbackSectionTitle949
        
        messageTV.delegate = self
        
        let attributedString = NSMutableAttributedString(string: CommonStrings.Settings.FeedbackMessage1107)
        
        let boldFontAttribute = [NSFontAttributeName: UIFont.boldSystemFont(ofSize: 20.0)]
        
        attributedString.addAttributes(boldFontAttribute, range: NSRange(location: 0, length: attributedString.length))
        
        attributedString.addAttribute(NSLinkAttributeName, value: "", range: NSRange(location: 107, length: 26))
        
        messageTV.isEditable = false
        messageTV.isScrollEnabled = false
        messageTV.font = UIFont(name: "Arial-Bold", size: 100)
        messageTV.attributedText = attributedString
        
        titleL.text = CommonStrings.Settings.FeedbackTitle2444
        
        
    }
    
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        
        
        
        
        
        
        /*  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
         
         OPTION 1 - TEMPORARY IMPLEMENTATION JUST SHOW ALERTVIEW WHEN THE LINK IS CLICK
         
         
         
         let alertView = UIAlertController(title: "TEMPORARY IMPLEMENTATION", message: "This should take you to screen where you can compose your feedback email to the address feedback@essentials.co.uk", preferredStyle: .alert)
         let action = UIAlertAction(title: "OK", style: .default, handler: { (alert) in
         
         })
         alertView.addAction(action)
         self.present(alertView, animated: true, completion: nil)
         **/
        
        
        
        
        
        
        
        /*  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
         
         OPTION 2 - TO SEND EMAIL USING MAILTO
         
         **/
        
        UIApplication.shared.open(NSURL(string: "mailto:feedback@essentials.co.uk")! as URL, options: [:], completionHandler: nil)
        
        
        
        
        
        
        
        
        
        /* >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
         
         OPTION 3 - TO SEND EMAIL USING MFMailComposeViewController.canSendMail
         
         
         if MFMailComposeViewController.canSendMail() {
         let mail = MFMailComposeViewController()
         mail.mailComposeDelegate = self
         mail.setToRecipients(["mailto:feedback@essentials.co.uk"])
         mail.setMessageBody("<p>You're so awesome!</p>", isHTML: true)
         
         present(mail, animated: true)
         } else {
         // show failure alert
         }
         
         
         **/
        
        return false
    }
    
    
    
    /* >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
     
     OPTION 3 - TO SEND EMAIL USING MFMailComposeViewController
     uncomment this if you are using OPTION 3
     
     //MARK: MFMailComposeViewControllerDelegate Method
     func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
     controller.dismiss(animated: true, completion: nil)
     }
     
     **/
    
    
    
    
    
    
}
