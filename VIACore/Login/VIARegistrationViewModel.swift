//
//  VIARegistrationViewModel.swift
//  VitalityActive
//
//  Created by Wilmar van Heerden on 2016/09/26.
//  Copyright © 2016 Glucode. All rights reserved.
//

import Foundation
import VitalityKit
import VIACommon

public class VIARegistrationViewModel: AnyObject {

    public var registrationDetail: VIARegistrationDetail

    init() {
        self.registrationDetail = VIARegistrationDetail()
    }

    public func performRegistration(completion: @escaping ((_: Error?) -> Void)) {
        if self.registrationDetail.isValid {
            
            if let showDateOfBirthField = VIAApplicableFeatures.default.showDateOfBirthField, showDateOfBirthField{
                Wire.Member.registerWithDOB(email: self.registrationDetail.email!, password: self.registrationDetail.password!, registrationCode: self.registrationDetail.registrationCode!, bornOn: self.registrationDetail.bornOn!.replacingOccurrences(of: "/", with: "-"), completion: { error in
                    completion(error)
                })
            }else {
                Wire.Member.register(email: self.registrationDetail.email!, password: self.registrationDetail.password!, registrationCode: self.registrationDetail.registrationCode!, completion: { error in
                    completion(error)
                })
            }
            
        }
    }
}

// Mark - VIARegistrationDetail

public struct VIARegistrationDetail {

    public var isEmailDirty = false
    public var isPasswordDirty = false
    public var isConfirmPasswordDirty = false
    public var isRegistrationCodeDirty = false
    public var isBornOnDirty = false

    public var isValid: Bool {
        return self.emailValid && self.passwordValid && self.confirmPasswordValid && self.registrationCodeValid && self.bornOnValid
    }

    public var email: String? {
        didSet {
            self.isEmailDirty = !String.isNilOrEmpty(string: self.email)
        }
    }

    public var emailErrorMessage = CommonStrings.Registration.InvalidEmailFootnoteError35
    public var emailValid: Bool {
        if self.email == nil {
            return false
        }
        return self.email!.isValidEmail()
    }

    public var password: String? {
        didSet {
            self.isPasswordDirty = !String.isNilOrEmpty(string: self.password)
        }
    }

    public var passwordErrorMessage = CommonStrings.Registration.PasswordFieldFootnote29
    public var passwordValid: Bool {
        guard let pwd = self.password else {
            return false
        }
        
        if let tenantID = AppSettings.getAppTenant(), tenantID == .SLI {
            return pwd.isValidPassword()
                && !pwd.hasDoubleByte()
                && !pwd.stringContainsSpecialCharacters()
        } else {
            return pwd.isValidPassword()
        }
    }

    public var confirmPassword: String? {
        didSet {
            self.isConfirmPasswordDirty = !String.isNilOrEmpty(string: self.confirmPassword)
        }
    }

    public var confirmPasswordErrorMessage = CommonStrings.Registration.MismatchedPasswordFootnoteError36
    public var confirmPasswordValid: Bool {
        if self.confirmPassword == nil {
            return false
        }
        return !String.isNilOrEmpty(string: self.confirmPassword) && self.confirmPassword == self.password
    }

    public var registrationCode: String? {
        didSet {
            self.isRegistrationCodeDirty = !String.isNilOrEmpty(string: self.registrationCode)
        }
    }

    public var registrationCodeErrorMessage = CommonStrings.Registration.RegistrationCodeFieldPlaceholder33
    public var registrationCodeValid: Bool {
        if self.registrationCode == nil {
            return false
        }
        return !String.isNilOrEmpty(string: self.registrationCode)
    }
    
    public var bornOn: String? {
        didSet {
            self.isBornOnDirty = !String.isNilOrEmpty(string: self.bornOn)
        }
    }
    
    public var bornOnValid: Bool {
        if self.bornOn == nil {
            if let showDateOfBirthField = VIAApplicableFeatures.default.showDateOfBirthField,
                showDateOfBirthField{
                return false
            } else {
                return true
            }
        }
        
        return !String.isNilOrEmpty(string: self.bornOn) && dobFormatValid
    }
    
    public var dobFormatValid: Bool {
        let dateFormatter = DateFormatter.yearMonthDayFormatter()
        if let dobDateString = self.bornOn {
            let dobDate = dateFormatter.date(from: dobDateString)
            if dobDate != nil {
                // Invalidate if selectedDate is greater than presentDate.
                if let selectedDate = dobDate {
                    if selectedDate > Date() {
                        return false
                    }
                }
                return true
            }
        }
        return false
    }
}
