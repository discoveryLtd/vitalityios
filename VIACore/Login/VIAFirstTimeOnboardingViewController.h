//
//  VIAOnboardingViewController.h
//  VitalityActive
//
//  Created by Wilmar van Heerden on 2016/09/14.
//  Copyright © 2016 Glucode. All rights reserved.
//

#import <JazzHands/IFTTTJazzHands.h>

@interface VIAFirstTimeOnboardingViewController : IFTTTAnimatedPagingScrollViewController

@property (nonatomic, assign) BOOL displayLoginImmediately;

@end
