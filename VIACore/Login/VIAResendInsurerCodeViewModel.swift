//
//  VIAResendInsurerCodeViewModel.swift
//  VitalityActive
//
//  Created by Genie Mae Lorena Edera on 2/22/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import VIAUtilities
import VitalityKit

public class VIAResendInsurerCodeViewModel: AnyObject {
    
    public var success: (() -> Void)?
    public var failure: ((_ error: Error) -> Void)?
    public var isDirty = false
    
    public var email: String? {
        didSet {
            self.isDirty = !String.isNilOrEmpty(string: self.email)
        }
    }
    
    public var emailValid: Bool {
        if self.email == nil {
            return false
        }
        return self.email!.isValidEmail()
    }
    
    init() {}
    
    public func initiateInsurerCodeRequest(completion: @escaping ((_: Error?) -> Void)) {
        let tenantId = AppSettings.getAppTenantId() ?? 2  // 2 = VitalityActive
        
        let trimmedEmail = Wire.updateEnvironmentAndTrim(email: self.email!)
        let request = ResendInsurerCodeParam(eventSource: 10, username:trimmedEmail)
        
        Wire.Party.resendInsurerCode(tenantId: tenantId, request: request, completion: { error in
            completion(error)
        })
    }
}
