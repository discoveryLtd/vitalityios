//
//  LoginRestrictionViewController.swift
//  VitalityActive
//
//  Created by Genie Mae Lorena Edera on 3/9/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import UIKit
import VitalityKit
import VIACommon

class LoginRestrictionViewController: UIViewController {
    
    @IBOutlet weak var logo: UIImageView!
    @IBOutlet weak var generalErrorTitle: UILabel!
    @IBOutlet weak var generalErrorLabel: UILabel!
    @IBOutlet weak var logoutButton: UIButton!
    @IBOutlet weak var vitalityLink: UIButton!
    
    @IBAction func logout(_ sender: Any) {
        let controller = UIAlertController(title: CommonStrings.Settings.LogoutTitle908,
                                           message: CommonStrings.Settings.AlertLogoutMessage910,
                                           preferredStyle: .alert)
        let cancel = UIAlertAction(title: CommonStrings.CancelButtonTitle24, style: .cancel) { action in }
        controller.addAction(cancel)
        
        let ok = UIAlertAction(title: CommonStrings.Settings.LogoutTitle908, style: .default) { [weak self] action in
            self?.doLogOut()
        }
        controller.addAction(ok)
        
        present(controller, animated: true, completion: nil)
    }
    
    @IBAction func loadVitalitySite(_ sender: Any) {
        if let redirectURL = VIAApplicableFeatures.default.getLogoutRedirectURL(), let url = URL(string: redirectURL) {
            UIApplication.shared.open(url, options: [:])
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        self.configureScreen()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func configureScreen() {
        self.logo.image = VIACoreAsset.LoginRestriction.membershipInactive.image
        
        self.generalErrorTitle.text = CommonStrings.Login.LoginRestrictionGeneralErrorMessageTitle2263
        
        let start = VitalityPartyMembership.membershipStartDate()
        let realm = DataProvider.newRealm()
        let currentPerson = realm.objects(PartyPerson.self).last
        var startDateString = Localization.dayMonthAndYearFormatter.string(from: start)
        
        if NSLocale.preferredLanguages[0].range(of:"en") != nil {
            let formatter = DateFormatter()
            
            formatter.dateFormat = "yyyy/MM/dd"
            startDateString = formatter.string(from: start)
        }
        
        let errorMsg = CommonStrings.Login.LoginRestrictionGeneralErrorMessage2262(startDateString, (currentPerson?.givenName ?? "You")!)
        let range = (errorMsg as NSString).range(of: startDateString)
        let attribute = NSMutableAttributedString.init(string: errorMsg)
        let boldFontAttribute = [NSFontAttributeName: UIFont.boldSystemFont(ofSize: 15.0)]
        attribute.addAttributes(boldFontAttribute, range: range)
        
        self.generalErrorLabel.attributedText = attribute
        
        self.logoutButton.setTitle(CommonStrings.Settings.LogoutTitle908, for: .normal)
        self.logoutButton.setTitleColor(UIColor.crimsonRed(), for: .normal)
        
        self.vitalityLink.setTitle(CommonStrings.Login.LoginRestrictionLearnAboutVitalityLink2264, for: .normal)
        self.vitalityLink.setTitleColor(UIColor.primaryColor(), for: .normal)
    }
    
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        UIApplication.shared.open(URL, options: [:])
        return false
    }
    
    public class func show() {
        let storyboard = UIStoryboard(coreStoryboard: .loginRestriction)
        let viewController = storyboard.instantiateInitialViewController()
        UIApplication.shared.keyWindow?.rootViewController = viewController
    }
    
    func doLogOut() {
        Wire.cancelAllTasks()
        DataProvider.reset()
        VIALoginViewController.showLogin()
    }
}

extension NSMutableAttributedString {
    
    public func setAsLink(textToFind:String, linkURL:String) -> Bool {
        
        let foundRange = self.mutableString.range(of: textToFind)
        if foundRange.location != NSNotFound {
            self.addAttribute(NSLinkAttributeName, value: linkURL, range: foundRange)
            return true
        }
        return false
    }
}
