//
//  DCHomeScreenCardSubtitleWithoutMainImageCell.swift
//  VIACore
//
//  Created by Val Tomol on 27/02/2019.
//  Copyright © 2019 Glucode. All rights reserved.
//

import Foundation
import SnapKit

class DCHomeScreenCardSubtitleWithoutMainImageCell: HomeScreenCardBaseCell {
    static let spacing: CGFloat = 15
    
    // MARK: Properties
    
    public var title: String? {
        get {
            return self.titleLabel.text
        }
        set {
            self.titleLabel.text = newValue
        }
    }
    
    public var subtitle: String? {
        get {
            return self.subtitleLabel.text
        }
    }
    
    public var actionTitle: String? {
        get {
            return self.actionLabel.text
        }
    }
    
    // MARK: Views
    
    lazy private var stackView: UIStackView = {
        let view = UIStackView(frame: .zero)
        view.axis = .horizontal
        view.spacing = spacing
        return view
    }()
    
    lazy private var labelsStackView: UIStackView = {
        let view = UIStackView(frame: .zero)
        view.axis = .vertical
        return view
    }()
    
    lazy private var titleLabel: UILabel = {
        let label = UILabel(frame: .zero)
        label.numberOfLines = 3
        label.font = UIFont.systemFont(ofSize: 20, weight: UIFontWeightHeavy)
        label.textColor = UIColor.night()
        label.adjustsFontSizeToFitWidth = true
        label.minimumScaleFactor = 0.8
        return label
    }()
    
    lazy private var subtitleLabel: UILabel = {
        let label = UILabel(frame: .zero)
        label.numberOfLines = 1
        label.textColor = UIColor.mediumGrey()
        label.font = UIFont.systemFont(ofSize: 15)
        label.snp.makeConstraints({ (make) in
            make.height.equalTo(20)
        })
        return label
    }()
    
    lazy private var actionLabel: UILabel = {
        let label = UILabel(frame: .zero)
        label.numberOfLines = 1
        label.textColor = UIColor.mediumGrey()
        label.font = UIFont.systemFont(ofSize: 15)
        label.snp.makeConstraints({ (make) in
            make.height.equalTo(20)
        })
        return label
    }()
    
    // MARK: Setup
    
    override func setupCardContentView(_ contentView: UIView) {
        
        contentView.addSubview(stackView)
        stackView.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.top.left.bottom.right.equalToSuperview().inset(DCHomeScreenCardSubtitleCell.spacing)
        }
        
        let labelsContainer = UIView(frame: .zero)
        stackView.addArrangedSubview(labelsContainer)
        labelsContainer.addSubview(labelsStackView)
        labelsStackView.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.left.right.equalToSuperview()
        }
        
        labelsStackView.addArrangedSubview(titleLabel)
        labelsStackView.addArrangedSubview(subtitleLabel)
        labelsStackView.addArrangedSubview(actionLabel)
        
        // hidden by default, setters controls visibility
        subtitleLabel.isHidden = true
        actionLabel.isHidden = true
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        self.title = nil
        self.setSubtitle(nil)
        self.setActionTitle(nil, tintColor: nil)
    }
    
    // MARK: Setters
    
    public func setSubtitle(_ title: String?) {
        subtitleLabel.textColor = UIColor.mediumGrey()
        subtitleLabel.text = title
        subtitleLabel.isHidden = title == nil
        setNeedsLayout()
        layoutIfNeeded()
        setNeedsUpdateConstraints()
        updateConstraintsIfNeeded()
    }
    
    public func setSubtitle(_ title: String?, tintColor: UIColor?) {
        subtitleLabel.text = title
        subtitleLabel.textColor = tintColor
        subtitleLabel.isHidden = title == nil
        setNeedsLayout()
        layoutIfNeeded()
        setNeedsUpdateConstraints()
        updateConstraintsIfNeeded()
    }
    
    public func setActionTitle(_ title: String?, tintColor: UIColor?) {
        actionLabel.text = title
        actionLabel.textColor = tintColor
        actionLabel.isHidden = title == nil
        setNeedsLayout()
        layoutIfNeeded()
        setNeedsUpdateConstraints()
        updateConstraintsIfNeeded()
    }
}
