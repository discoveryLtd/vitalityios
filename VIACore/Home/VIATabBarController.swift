import Foundation

import VIAUIKit
import VIACommon
import VIAUtilities
import VIAPointsMonitor
import VIAPartners
import VIAMyHealth
import VitalityKit

class VIATabBarController: UITabBarController {

    fileprivate weak var customDelegate: VIATabbarControllerDelegate? {
        return _customDelegate
    }
    
    fileprivate lazy var _customDelegate: VIATabbarControllerDelegate = {
        return VIATabbarControllerDelegate()
    }()
    
    deinit {
        debugPrint("VIATabBarController deinit")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.delegate = self.customDelegate
        
        showHideTabBarItems()
    }
    
    func showHideTabBarItems(){
        
        /*
         UKE should display "Vitality Age",
         other markets should be "My Health"
        */
        //(VIAApplicableFeatures.default.shouldSkipMyHealthLandingScreen)! ? viewControllers?.remove(at: 2) : viewControllers?.remove(at: 3)
        let indexToRemove = (VIAApplicableFeatures.default.shouldSkipMyHealthLandingScreen()) ? 2 : 3
        viewControllers?.remove(at: indexToRemove)
        
        //ge20180110 : Hide Help
        if(VIAApplicableFeatures.default.hideHelpTab)! {
            viewControllers?.remove(at: 3)
        }
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        self.tabBar.backgroundColor = VIAAppearance.default.dataSource?.tabBarBackgroundColor
        self.tabBar.barTintColor = VIAAppearance.default.dataSource?.tabBarBarTintColor
        self.tabBar.tintColor = VIAAppearance.default.dataSource?.tabBarTintColor
        self.tabBar.unselectedItemTintColor = VIAAppearance.default.dataSource?.unselectedItemTintColor
    }

}

fileprivate class VIATabbarControllerDelegate: NSObject, UITabBarControllerDelegate {

    override init() {
        debugPrint("VIATabbarControllerDelegate")
    }

    deinit {
        debugPrint("deinit VIATabbarControllerDelegate")
    }

    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        guard let navController = viewController as? UINavigationController else { return true }

        if let controller = navController.topViewController as? PointsMonitorLandingViewController {
            controller.shouldPerformDataRequest = true
        } else if let controller = navController.topViewController as? VIAHomeViewController {
            controller.shouldPerformDataRequest = true
        } else if let controller = navController.topViewController as? PartnersListTableViewController {
            controller.viewModel = PartnersListTableViewModel(withPartnerType: ProductFeatureCategoryRef.WellnessPartners)
            controller.sourceFeatureType = .improveYourHealth
            controller.loadPartners()
        } else if let controller = navController.topViewController as? VIAVitalityHealthModalTableViewController {
            controller.showBarButton = false
            controller.cardIndex = 0
        } 
    
        return true
    }
    
    /* Added this method to properly identify the selected index of the tab. */
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        guard let navController = viewController as? UINavigationController else { return }
        
        /* FC-22986 : Generali Germany
         * App is Crashing when going back to MyHealth after editing VHR.
         */
        let tabBarIndex = tabBarController.selectedIndex
        /* Check if the tab selected is MyHealth */
        if tabBarIndex == 2 {
            /* Check if TenantID is 18 */
            if let tenantID = AppSettings.getAppTenant(), tenantID == .GI {
                /* Always load the MyHealth Landing Screen and pops all the views that has been opened. */
                navController.popToRootViewController(animated: true)
            }
        }
    }

}
