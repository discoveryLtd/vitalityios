import Foundation

import VitalityKit
import VIAUtilities
import VIAUIKit
import VIAHealthKit
import VIACommon

import RealmSwift
import SwiftDate

protocol VIAHomeViewModelDelegate: class {

    func viewModelDidLoadData()

    func viewModelDidUpdateData(immediately: Bool)

    func shouldEndRefreshing()
}

public class VIAHomeViewModel {

    // MARK: Delegate

    weak var delegate: VIAHomeViewModelDelegate?

    // MARK: Data

    var realm = DataProvider.newRealm()

    var notificationToken: NotificationToken?
    
    
    public lazy var homeScreenData: Results<HomeScreenData> = {
        return self.realm.allHomeScreenData()
    }()

    var _data: [HomeSectionValue]?
    var data: [HomeSectionValue] {
        if _data == nil {
            var sections: [HomeSectionValue] = []
            guard let data = latestHomeScreenData()?.sections.sorted(byKeyPath: #keyPath(HomeSection.type), ascending: true) else { return sections }

            for section in data {
                sections.append(HomeSectionValue(homeSection: section))
            }
            _data = sections
        }
        return _data!
    }
    

    // MARK: Lifecycle

    private init() {
        registerForNotifications()
        AppSettings.stopHomeScreenLoading()
    }

    deinit {
        NotificationCenter.default.removeObserver(self, name: .VIAIsAuthenticatedNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: .VIANonSmokersDeclarationCompletedNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: .VIAVHCCompletedNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: .VIAVHRQuestionnaireSubmittedNotification, object: nil)
    }

    public static var instance: VIAHomeViewModel?
    public static var sharedInstance: VIAHomeViewModel{
        get{
            if nil == instance{
                instance = VIAHomeViewModel()
            }
            return instance!
        }
    }
    
    // MARK: Action

    func registerForNotifications() {
        // Changes to home sections
        notificationToken = homeScreenData.addNotificationBlock { [weak self] (changes: RealmCollectionChange) in
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
                self?.realmDataDidChange(changes)
            })
        }
        
        /*
         * Task: Mobile Performance Enhancements
         * Disabled all reload calls from other Journeys.
         * Home screen card should only be refreshed on viewWillAppear and on pull-to-refresh
         */
//        /* Is authenticated to prefetch home screen response after login */
//        NotificationCenter.default.addObserver(forName: .VIAIsAuthenticatedNotification, object: nil, queue: nil) { (notification) in
//            self.loadData() { (error) in }
//        }
//
//        /* NSD Journey completion */
//        NotificationCenter.default.addObserver(forName: .VIANonSmokersDeclarationCompletedNotification, object: nil, queue: nil) { (notification) in
//                self.loadData() { (error) in }
//        }
//
//        /* VHC Journey completion */
//        NotificationCenter.default.addObserver(forName: .VIAVHCCompletedNotification, object: nil, queue: nil) { (notification) in
//            self.loadData() { (error) in }
//        }
//
//        /* VHR Journey completion */
//        NotificationCenter.default.addObserver(forName: .VIAVHRQuestionnaireSubmittedNotification, object: nil, queue: nil) { (notification) in
//            self.loadData() { (error) in }
//        }
    }
    
    func latestHomeScreenData() -> HomeScreenData? {
        return homeScreenData.first
    }

    func realmDataDidChange(_ changes: RealmCollectionChange<Results<HomeScreenData>>) {
        _data = nil

        if case .initial = changes {
            self.delegate?.viewModelDidLoadData()
        } else {
            self.delegate?.viewModelDidUpdateData(immediately: true) //TODO: should be false, only made true for demo, had racing issues
        }
    }

    func loadData(completion: @escaping (_ error: Error?) -> Void) {
        
        realm.refresh()
        let partyId = realm.getPartyId()
        let tenantId = realm.getTenantId()

        /**
         In IGI, we need to check if there is avaialable reward so we can show/hide "Earn Rewards" card, so we need to query the getRewardedByRewardId
         **/
        if let tenantIDFromInfoPlist = AppSettings.getAppTenant() {
            /* CA=2 | UKE=26 | IGI=28 | SLI=25 */
            
            if tenantIDFromInfoPlist == .IGI {
                Wire.Rewards.getAwardedRewardByPartyId(tenantId: tenantId, partyId: partyId, fromDate: Date(), toDate: Date(), linkedKeys: []) { [weak self] (rewardsRequestError, response) in
                }
            }
        }
        
        Wire.Content.getHomeScreen(tenantId: tenantId, partyId: partyId, vitalityMembershipId: realm.getMembershipId()) { [weak self] error in
            completion(error)
            
        }
        
        self.delegate?.shouldEndRefreshing()
        
        //HomeScreenDataController.loadData(realm: realm) {
        //    self.delegate?.shouldEndRefreshing()
        //}
    }
    
    // MARK: AR / VHR
    
    func activeRewardsIsAlreadyActivated() -> Bool? {
        if let card = realm.homeCard(by: .ActiveRewards) {
            return card.status != .NotStarted
        }
        debugPrint("No AR card found to determine status from")
        return nil
    }
    
    func vhrIsRequiredBeforeActiveRewardsActivation(completion: @escaping (Error?, Bool?) -> Void) {
        guard realm.feature(of: .ARVHRRequired) != nil else {
            return completion(nil, false)
        }
        
        let tenantId = realm.getTenantId()
        let partyId = realm.getPartyId()
        let parameter = GetEventByPartyParameter(partyId: partyId, eventTypesTypeKeys: [.VHRAssmntCompleted])
        Wire.Events.getEventByParty(tenantId: tenantId, request: parameter) { error, response in
            if let error = error {
                completion(error, nil)
            } else {
                completion(nil, (response?.event?.count ?? 0) == 0)
            }
        }
    }

    // MARK: Apple Watch
    func setUserEntityNumberForAppleWatch() {
        let partyId = "\(realm.getPartyId())"
        VITHealthKitHelper.shared().setUserEntityNumber(partyId)
    }
    
    func isDeviceLinked() -> Bool {
        switch  VITHealthKitHelper.shared().status {
        case .available:
            return true
        default:
            return false
        }
    }

}
