import VIACommon
import Foundation
import VIAUIKit
import UIKit
import IGListKit
import SnapKit
import VIAHealthKit
import VitalityKit
import VIAUtilities
import VIAAssessments
import VIAActiveRewards
import VIAStatus
import VIAPartners
import TTGSnackbar
import RealmSwift


public class VIAHomeViewController: VIAViewController, IGListAdapterDataSource, VIAHomeViewModelDelegate, PrimaryColorTintable {

    public static func getDefaultHomeScreenLeftContentInset() -> CGFloat{
        if let shouldResizeCard = VIAApplicableFeatures.default.resizeHomeCard,shouldResizeCard {
            return 10.0
        }
        return 20.0
    }
    
    public static func getDefaultHomeScreenRightContentInset() -> CGFloat{
        if let shouldResizeCard = VIAApplicableFeatures.default.resizeHomeCard,shouldResizeCard {
            return 30.0
        }
        return 20.0
    }
    
    public static func getHomeCardRightContentInset() -> CGFloat{
        if let shouldResizeCard = VIAApplicableFeatures.default.resizeHomeCard,shouldResizeCard {
            return 10.0
        }
        return 8.0
    }
    
    // MARK: Properties
    
    let viewModel: VIAHomeViewModel = VIAHomeViewModel.sharedInstance
    
    var performUpdatesOnViewDidAppear = false
    
    var shouldPerformDataRequest = true
    
    let collectionView = IGListCollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
    
    let statusNavView: VIAStatusProgressView = VIAStatusProgressView.viewFromNib(owner: self)!
    
    lazy var adapter: IGListAdapter = {
        return IGListAdapter(updater: IGListAdapterUpdater(), viewController: self, workingRangeSize: 0)
    }()
    
    
    // TODO: Remove me, only needed for Reward quick links
    var flowController: ARLandingNavDelegate = ARRewardsFlowCoordinator()
    
    fileprivate var snackbar:TTGSnackbar?
    
    // MARK: Lifecycle
    
    public override func awakeFromNib() {
        super.awakeFromNib()
        setupTabBarTitle()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupTabBarTitle()
    }
    
    public override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setupTabBarTitle()
    }
    
    func setupTabBarTitle() {
        navigationController?.tabBarItem.title = CommonStrings.MenuHomeButton5
    }
    
    // MARK: View lifecycle
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        
        title = CommonStrings.MenuHomeButton5
        
        // branded logo nav title
        let imageView = UIImageView(frame:CGRect(x: 0, y: 0, width: 54, height: 31))
        imageView.contentMode = .scaleAspectFit
        imageView.image = VIAIconography.default.homeLogo(for: Locale.current)
        navigationItem.titleView = imageView
        
        #if DEBUG
            self.navigationItem.setRightBarButtonItems([UIBarButtonItem(title: "Debug Mode", style: .plain, target: self, action: #selector(openCardsMenu(_:)))], animated: false)
        #endif
        
        viewModel.delegate = self
        performRefresh()
        
        configureAppearance()
        configureSubviews()
        configureAdapter()
        configureNotificationObservers()
        
        
        if let tabBarController = self.tabBarController {
            var indexToRemove = 4
            if(VIAApplicableFeatures.default.showPartnersTabItem)!{
                indexToRemove = 3
            }
            if indexToRemove < (tabBarController.viewControllers?.count)! {
                var viewControllers = tabBarController.viewControllers
                viewControllers?.remove(at: indexToRemove)
                tabBarController.viewControllers = viewControllers
            }
        }
        
        
    }
    
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        configureAppearance()
        resetBackButtonTitle()
        //if shouldPerformDataRequest {
        performRefresh()
        //}
        showStatusModalIfRequired()
        flowController = ARRewardsFlowCoordinator()
        self.viewModel.setUserEntityNumberForAppleWatch()
    }
    
    public override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        performUpdatesIfNeeded()
        
        checkUpdateForNewAppVersion()
    }
    
    public override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        hideBackButtonTitle()
    }
    
    func showStatusModalIfRequired() {
        if StatusHelper.statusDidIncrease() || StatusHelper.newMembershipYearEntered() {
            performSegue(withIdentifier: "showStatusIncreased", sender: nil)
        }
    }
    
    func navigateToScreenFromNotification(){
        
        let screenNumber = AppSettings.getSelectedScreenFromNotification()
        switch(screenNumber){
        case 4:
            self.navigationController?.popToRootViewController(animated: true)
            self.showARLanding(nil)
            self.showARAvailableRewards(nil)
            break
        case 5:
            self.navigationController?.popToRootViewController(animated: true)
            self.showARLanding(nil)
            self.showARLearnMore(nil)
            break
        case 6:
            self.navigationController?.popToRootViewController(animated: true)
            self.showActiveRewards(nil)
            break
        default:
            /* Do nothing */
            break
        }
    }
    
    deinit {
        debugPrint("VIAHomeViewController deinit")
    }
    
    // MARK: View config
    
    
    func configureAppearance() {
        navigationController?.navigationBar.isHidden = false
        navigationController?.makeNavigationBarTransparent()
        navigationController?.navigationBar.setBackgroundImage(VIACoreAsset.NavBar.transparentPixel.image , for: .default)
        navigationController?.navigationBar.shadowImage = VIACoreAsset.NavBar.transparentPixel.image
        view.backgroundColor = .day()
    }
    
    func configureSubviews() {
        automaticallyAdjustsScrollViewInsets = false
        
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.distribution = .fill
        stackView.alignment = .fill
        stackView.spacing = 1
        view.addSubview(stackView)
        
        stackView.snp.makeConstraints { (make) in
            make.leading.trailing.equalToSuperview()
            make.top.equalTo(topLayoutGuide.snp.bottom)
            make.bottom.equalTo(bottomLayoutGuide.snp.top)
        }
        
        stackView.addArrangedSubview(statusNavView)
        statusNavView.snp.makeConstraints { (make) in
            make.height.equalTo(75)
            make.width.equalToSuperview()
        }
        
        statusNavView.isHidden = true
        configureStatusNavView()
        
        stackView.addArrangedSubview(collectionView)
        configureCollectionView()
    }
    
    func configureCollectionView() {
        collectionView.backgroundColor = UIColor.tableViewBackground()
        collectionView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 15, right: 0)
        collectionView.refreshControl = UIRefreshControl()
        collectionView.refreshControl?.tintColor = UIColor.cellSeperator()
        collectionView.refreshControl?.addTarget(self, action: #selector(performRefresh), for: .valueChanged)
        collectionView.showsVerticalScrollIndicator = false
        collectionView.showsHorizontalScrollIndicator = false
    }
    
    func configureStatusNavView() {
        guard let progress = viewModel.latestHomeScreenData()?.calculateStatusProgress(),
            let pointsMessage = viewModel.latestHomeScreenData()?.configureStatusPointsMessage(),
            let icon = viewModel.latestHomeScreenData()?.statusIcon(),
            let title = viewModel.latestHomeScreenData()?.statusTitle()
            else {
                statusNavView.isHidden = true
                navigationController?.navigationBar.shadowImage = VIACoreAsset.NavBar.greyPixel.image
                return
        }
        statusNavView.delegate = self
        statusNavView.progress = progress
        statusNavView.message = pointsMessage
        statusNavView.image = icon
        statusNavView.title = title
        statusNavView.isHidden = false
        navigationController?.navigationBar.shadowImage = VIACoreAsset.NavBar.transparentPixel.image
        
    }
    
    func configureAdapter() {
        adapter.collectionView = collectionView
        adapter.dataSource = self
    }
    
    func configureNotificationObservers(){
        NotificationCenter.default.addObserver(forName: .VIAPushNotificationWillOpen, object: nil, queue: .main) { [weak self] notification in
            self?.navigateToScreenFromNotification()
        }
    }
    
    // MARK: IGListAdapterDataSource
    
    public func objects(for listAdapter: IGListAdapter) -> [IGListDiffable] {
        return viewModel.data
    }
    
    public func listAdapter(_ listAdapter: IGListAdapter, sectionControllerFor object: Any) -> IGListSectionController {
        return HomeScreenSectionController()
    }
    
    public func emptyView(for listAdapter: IGListAdapter) -> UIView? {
        return nil
    }
    
    // MARK: Navigation
    
    @IBAction func unwindToHomeViewController(segue: UIStoryboardSegue) {
        debugPrint("unwindToHomeViewController")
    }
    
    @IBAction func activeRewardsRequiresVHRBeforeActivation(segue: UIStoryboardSegue) {
        if let customSegue = segue as? UIStoryboardSegueWithCompletion {
            customSegue.completion = { [weak self] in
                self?.showVHR(nil)
            }
        }
    }
    
    func prepareActiveRewardsOnboarding(for viewController: VIAOnboardingViewController) {
        if VitalityProductFeature.isEnabled(.ActiveGoal) {
            viewController.viewModel = VIAAROnboardingGetStartedActiveGoalViewModel()
        } else if VitalityProductFeature.isEnabled(.ARChoice) {
            viewController.viewModel = VIAAROnboardingGetStartedARChoiceViewModel()
        } else if VitalityProductFeature.isEnabled(.ARProbabilistic) {
            viewController.viewModel = VIAAROnboardingGetStartedARProbabilisticViewModel()
        } else {
            // sensible default if AR isn't configured is to fallback to Active Goal
            viewController.viewModel = VIAAROnboardingGetStartedActiveGoalViewModel()
        }
    }
    
    // MARK: Perform updates / reload
    private func updateRefreshing(status: Bool){
        let isRefreshing:[String: Bool] = ["isRefreshing": status]
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "homeScreenDidRefresh"), object: nil, userInfo: isRefreshing)
    }
    
    func performRefresh() {
        if let replaceHUD = Bundle.main.object(forInfoDictionaryKey: "VIAReplaceHUDWithSnackbar") as? Bool, replaceHUD{
            showMessageOnSnackbar(message: CommonStrings.HomeCard.InformCardsUpdating2240)
        }else{
            showHUDOnView(view: self.view)
        }
        
        updateRefreshing(status: true)
        collectionView.refreshControl?.beginRefreshing()
        viewModel.loadData() { [weak self] (error) in
            // Reload StatusNavView after loading homecard data.
            self?.dismissSnackbar()
            self?.configureStatusNavView()
            
            guard error == nil else {
                self?.initErrorHandler(error!)
                return
            }
        }
    }
    
    func initErrorHandler(_ error: Error) {
        let backendError = error as? BackendError ?? BackendError.other
        self.handleBackendErrorWithAlert(backendError)
    }
    
    func shouldEndRefreshing() {
        collectionView.refreshControl?.endRefreshing()
        configureStatusNavView()
        
        updateRefreshing(status: false)
    }
    
    func performUpdatesIfNeeded() {
        if performUpdatesOnViewDidAppear {
            performUpdatesOnViewDidAppear = false
            performUpdates()
        }
    }
    
    func performUpdates() {
//        let appIdentifier = Bundle.main.object(forInfoDictionaryKey: "VAAppConfigIdentifier") as? String ?? ""
//        if appIdentifier == "IGIVitality"{
//            adapter.reloadData { [weak self] (finished) in
//                self?.hideHUDFromView(view: self?.view)
//                debugPrint("reloadData")
//                if let _ = self?.collectionView.refreshControl?.isRefreshing {
//                    self?.collectionView.refreshControl?.endRefreshing()
//                }
//            }
//        }else{
//            adapter.performUpdates(animated: true) { [weak self] (finished) in
//                self?.hideHUDFromView(view: self?.view)
//                debugPrint("performUpdates")
//                if let _ = self?.collectionView.refreshControl?.isRefreshing {
//                    self?.collectionView.refreshControl?.endRefreshing()
//                }
//            }
//        }
        
        adapter.performUpdates(animated: true) { [weak self] (finished) in
            self?.hideHUDFromView(view: self?.view)
            // debugPrint("performUpdates")
            if let _ = self?.collectionView.refreshControl?.isRefreshing {
                self?.collectionView.refreshControl?.endRefreshing()
            }
            
            self?.reloadData()
        }
    }
    
    func reloadData() {
        adapter.reloadData { [weak self] (finished) in
            // debugPrint("reloadData")
            if let _ = self?.collectionView.refreshControl?.isRefreshing {
                self?.collectionView.refreshControl?.endRefreshing()
            }
        }
    }
    
    // MARK: VIAHomeViewModelDelegate
    
    func viewModelDidLoadData() {
        reloadData()
    }
    
    func viewModelDidUpdateData(immediately: Bool) {
        if immediately {
            performUpdates()
        } else {
            performUpdatesOnViewDidAppear = true
        }
    }
    
}

extension VIAHomeViewController{
    
    public func checkUpdateForNewAppVersion(){
       VIAUpdaterUtil.checkUpdateForNewAppVersion()
    }
}

extension VIAHomeViewController{
    
    public func showMessageOnSnackbar(message: String){
        if snackbar == nil {
            snackbar = TTGSnackbar()
        }
        
        snackbar?.message = message
        snackbar?.duration =  .forever
        snackbar?.animationType = .slideFromBottomBackToBottom
        snackbar?.bottomMargin = 55.0
        
        snackbar?.backgroundColor = UIColor.white
        snackbar?.messageTextColor = UIColor.black
        snackbar?.messageTextAlign = .center
        
        snackbar?.show()
    }
    
    public func dismissSnackbar(){
        snackbar?.dismiss()
    }
}
