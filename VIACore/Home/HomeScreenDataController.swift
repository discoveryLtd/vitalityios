
import Foundation
import VitalityKit

import RealmSwift

public class HomeScreenDataController{
    
    class func loadData(realm: Realm, completion: (()->Void)?) {
        realm.refresh()
        let partyId = realm.getPartyId()
        let tenantId = realm.getTenantId()
        Wire.Content.getHomeScreen(tenantId: tenantId, partyId: partyId,
                                   vitalityMembershipId: realm.getMembershipId()) { error in
            completion?()
        }
    }
}
