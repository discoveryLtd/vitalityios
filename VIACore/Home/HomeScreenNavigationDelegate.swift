import Foundation

protocol HomeScreenNavigationDelegate: class {
    
    static func showHome()
    
    func showActiveRewards(_ sender: Any?)
    
    func showARSpinner(_ sender: Any?)
    
    func showARAvailableRewards(_ sender: Any?)
    
    func showARChooseRewards(_ sender: Any?)
    
    func showARVoucher(_ sender: Any?)
    
    func showARLearnMore(_ sender: Any?)
    
    func showARCurrentRewards(_ sender: Any?)
    
    func showNonSmokersDeclaration(_ sender: Any?)
    
    func showVHC(_ sender: Any?)
    
    func showSAV(_ sender: Any?)
    
    func showMWB(_ sender: Any?)
    
    func showVHR(_ sender: Any?)
    
    func showVNA(_ sender: Any?)
    
    func showOFE(_ sender: Any?)
    
    func showDeviceCashback(_ sender: Any?)
    
    func showPolicyCashback(_ sender: Any?)
    
    func showIntegratedBenefit(_ sender: Any?)
    
    func showAppleWatchCashback(_ sender: Any?)
    
    func showStatus(_ sender: Any?)
    
    func showWellnessDevicesAndApps(_ sender: Any?)
    
    func showRewardPartners(_ sender: Any?)
    
    func showHealthPartners(_ sender: Any?)
    
    func showWellnessPartners(_ sender: Any?)
    
    func showCorporateBenefitPartners(_ sender: Any?)
    
    func showNuffieldHealthServices(_ sender: Any?)
    
    func showEmployerReward(_ sender: Any?)
    
    func showActivationBarcode(_ sender: Any?)
    
    func rewardUKESpinNow(_ sender: Any?)
    
    func rewardUKEStarbucksViewReward(_ sender: Any?)
    
    func rewardUKEStarbucksUseReward(_ sender: Any?)
    
    func rewardUKECinemaUseReward(_ sender: Any?)
    
    func rewardUKECinemaRedeemReward(_ sender: Any?)
    
    func showAvailableRewards(_ sender: Any?)
    
    func unwindToHomeViewControllerFromNonSmokersDeclaration(segue: UIStoryboardSegue)
}
