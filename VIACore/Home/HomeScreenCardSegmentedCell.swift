import Foundation
import SnapKit
import VIAUIKit

class HomeScreenCardSegmentedCell: HomeScreenCardBaseCell {

    static let spacing: CGFloat = 15
    static let circleStackSpacing: CGFloat = 8

    // MARK: Properties

    public var title: String? {
        get {
            return self.titleLabel.text
        }
        set {
            self.titleLabel.text = newValue
        }
    }

    public var subtitle: String? {
        get {
            return self.subtitleLabel.text
        }
    }

    public var actionTitle: String? {
        get {
            return self.actionLabel.text
        }
    }

    // MARK: Views

    lazy private var stackView: UIStackView = {
        let view = UIStackView(frame: .zero)
        view.axis = .horizontal
        view.spacing = spacing
        return view
    }()

    lazy private var segmentedCircleView: VIASegmentedCircleView = VIASegmentedCircleView()

    lazy private var labelsStackView: UIStackView = {
        let view = UIStackView(frame: .zero)
        view.axis = .vertical
        return view
    }()

    lazy private var titleLabel: UILabel = {
        let label = UILabel(frame: .zero)
        label.numberOfLines = 2
        label.font = UIFont.title2Font()
        label.textColor = UIColor.night()
        return label
    }()

    lazy private var subtitleLabel: UILabel = {
        let label = UILabel(frame: .zero)
        label.numberOfLines = 1
        label.textColor = UIColor.mediumGrey()
        label.font = UIFont.subheadlineFont()
        label.snp.makeConstraints({ (make) in
            make.height.equalTo(20)
        })
        return label
    }()

    lazy private var actionLabel: UILabel = {
        let label = UILabel(frame: .zero)
        label.numberOfLines = 1
        label.textColor = UIColor.mediumGrey()
        label.font = UIFont.subheadlineFont()
        label.snp.makeConstraints({ (make) in
            make.height.equalTo(20)
        })
        return label
    }()

    // MARK: Setup

    override func setupCardContentView(_ contentView: UIView) {
        contentView.addSubview(stackView)
        stackView.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.top.left.bottom.right.equalToSuperview().inset(HomeScreenCardSegmentedCell.circleStackSpacing)
        }

        stackView.addArrangedSubview(segmentedCircleView)
        segmentedCircleView.snp.makeConstraints { (make) in
            make.height.equalToSuperview()
            make.width.equalTo(stackView.snp.height)
        }

        let labelsContainer = UIView(frame: .zero)
        stackView.addArrangedSubview(labelsContainer)
        labelsContainer.addSubview(labelsStackView)
        labelsStackView.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.left.right.equalToSuperview()
        }

        labelsStackView.addArrangedSubview(titleLabel)
        labelsStackView.addArrangedSubview(subtitleLabel)
        labelsStackView.addArrangedSubview(actionLabel)

        // hidden by default, setters controls visibility
        subtitleLabel.isHidden = true
        actionLabel.isHidden = true
    }

    override func prepareForReuse() {
        super.prepareForReuse()

        self.title = nil
        self.setSubtitle(nil)
        self.setActionTitle(nil, tintColor: nil)
    }

    // MARK: Setters

    public func setSegmentedCircleView(totalSections: Int, completedSections: Int, numberFormatter: NumberFormatter) {
        segmentedCircleView.setupSegmentedCircleForDisplay(totalSections: totalSections, completedSections: completedSections, numberFormatter: numberFormatter)
    }

    public func setSubtitle(_ title: String?) {
        subtitleLabel.text = title
        subtitleLabel.isHidden = title == nil
    }

    public func setActionTitle(_ title: String?, tintColor: UIColor?) {
        actionLabel.text = title
        actionLabel.textColor = tintColor
        actionLabel.isHidden = title == nil
    }

}
