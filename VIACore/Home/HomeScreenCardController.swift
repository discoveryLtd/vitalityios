import Foundation
import IGListKit
import VitalityKit
import VIAUIKit
import VIAUtilities
import VIAActiveRewards
import VIACommon

class HomeScreenCardController: IGListSectionController, IGListSectionType {

    var card: HomeCardValue!
    
    var isHomeCardRefreshing: Bool = false
    
    let integerFormatter = NumberFormatter.integerFormatter()

    var tintColor: UIColor = UIColor.day()

    convenience init(tintColor: UIColor) {
        self.init()

        self.tintColor = tintColor
        self.inset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: VIAHomeViewController.getHomeCardRightContentInset())
    }
    func didUpdate(to object: Any) {
        card = object as! HomeCardValue
    }
    func isHomeLoading(_ notification: NSNotification) {
        if let isRefreshing = notification.userInfo?["isRefreshing"] as? Bool {
            isHomeCardRefreshing = isRefreshing
        }
    }
    func numberOfItems() -> Int {
        return 1
    }

    func sizeForItem(at index: Int) -> CGSize {

        let height = collectionContext?.containerSize.height ?? 0
        let width = collectionContext?.containerSize.width ?? 0

        return CGSize(width: width, height: 155) 
    }

    func cellForItem(at index: Int) -> UICollectionViewCell {
        switch card.type {
        case .ActiveRewards:
            return configureActiveRewardsCard(at: index)
        case .AppleWatch:
            return configureAppleWatchCard(at: index)
        case .HealthHub:
            return configureHealthHubCard(at: index)
        case .MentalWellbeing:
            return configureMentalWellbeingCard(at: index)
        case .NonSmokersDecl:
            return configureNonSmokersDeclCard(at: index)
        case .Rewards:
            return configureRewardsCard(at: index)
        case .ScreenandVacc:
            return configureScreenandVaccCard(at: index)
        case .VitNutAssessment:
            return configureVitNutAssessmentCard(at: index)
        case .VitalityHealthCheck:
            return configureVitalityHealthCheckCard(at: index)
        case .VitalityHealthReview:
            return configureVitalityHealthReviewCard(at: index)
        case .Vouchers:
            return configureVouchersCard(at: index)
        case .WellDevandApps:
            return configureWellDevandAppsCard(at: index)
        case .HealthPartners,
             .WellnessPartners,
             .RewardPartners:
            return configurePartnerRewardsCard(at: index)
        case .OrganisedFitnessEvents:
            return configureOrganisedFitnessEventsCard(at: index)
        case .DeviceCashback:
            return configureDeviceCashbackCard(at: index)
        case .PolicyCashback:
            return configurePolicyCashbackCard(at: index)
        case .RewardSelection:
            return configureVouchersCard(at: index)
        case .ActivationBarcode:
            return configureActivationBarcodeCard(at: index)
		case .HealthServices:
			// TODO
			return configureHealthServicesCard(at: index)
        case .NuffieldHealthServices:
            return configureNuffieldHealthServicesCard(at: index)
        case .AvailableRewards:
            return configureAvailableRewardsCard(at: index)
        case .EmployerStatus:
            return configureEmployerStatusCard(at: index)
        case .UKEEmployerStatus:
            return configureEmployerStatusCard(at: index)
        case .Unknown:
            return defaultCell(at: index)
        
        }
    }
    
    func defaultCell(at index: Int) -> UICollectionViewCell {
        let cell: HomeScreenCardBaseCell = dequeueReusableCell(at: index)
        return cell
    }
    
    func dequeueReusableCell<T>(at index: Int) -> T where T: UICollectionViewCell {
        return collectionContext!.dequeueReusableCell(of: T.self, for: self, at: index) as! T
    }
    
    func configureActiveRewardsCard(at index: Int) -> UICollectionViewCell {
        if card.status == .NotStarted {
            let cell: HomeScreenCardSubtitleCell = dequeueReusableCell(at: index)
            cell.heading = card.heading()
            cell.setHeadingImage(with: card.headingImage(), tintColor: self.tintColor)
            cell.title = card.title()
            cell.setSubtitle(card.subtitle())
            cell.setMainImage(with: card.mainImageForCurrentStatus(), tintColor: self.tintColor)
            cell.setActionTitle(card.actionTitle(), tintColor: self.tintColor)
            return cell
            
        } else if card.status == .Activated {
            let cell: HomeScreenCardSubtitleCell = dequeueReusableCell(at: index)
            cell.heading = card.heading()
            cell.setHeadingImage(with: self.card.headingImage(), tintColor: self.tintColor)
            cell.title = card.title()
            if let date = card.goalStartDate {
                let dateString = Localization.dayDateShortMonthyearFormatter.string(from: date)
                let text = CommonStrings.Ar.HomeCard.ActivatedStartDate776(dateString)
                cell.setSubtitle(text)
            } else {
                cell.setSubtitle(card.title())
            }
            cell.setMainImage(with: card.mainImageForCurrentStatus(), tintColor: self.tintColor)
            return cell
            
        } else if card.status == .InProgress {
            let cell: ActiveRewardsHomeScreenCardCell = dequeueReusableCell(at: index)
            cell.heading = card.heading()
            cell.setHeadingImage(with: self.card.headingImage(), tintColor: self.tintColor)
            // points
            let amountCompletedString = Localization.integerFormatter.string(from: NSNumber(integerLiteral: card.amountCompleted)) ?? "\(card.amountCompleted)"
            let totalString = Localization.integerFormatter.string(from: NSNumber(integerLiteral: card.total)) ?? "\(card.total)"
            cell.title = CommonStrings.Ar.HomeCard.InProgressTitle773(amountCompletedString, totalString)
            // dates
            if let startDate = card.goalStartDate, let endDate = card.goalEndDate {
                let startDateString = Localization.dayMonthAndYearFormatter.string(from: startDate)
                let endDateString = Localization.dayMonthAndYearFormatter.string(from: endDate)
                let text = CommonStrings.Ar.HomeCard.InProgressDates774(startDateString, endDateString)
                cell.setSubtitle(text)
            } else {
                cell.setSubtitle(nil)
            }
            cell.setActionTitle(card.actionTitle(), tintColor: self.tintColor)
            
            let model = VIAGraphViewModel(total: card.total, startingValue: 0, endingValue: card.amountCompleted)
            if let graph = VIAGraphView.viewFromNib(owner: nil) {
                graph.progressLabel.isHidden = true
                graph.ringWidth = 7
                graph.spacing = 1
                graph.setCountingLabel(font: UIFont.title2Font())
                cell.setMainView(with: graph)
                graph.model = model
            }
            return cell
            
        } else if card.status == .Achieved {
            let cell: ActiveRewardsHomeScreenCardCell = dequeueReusableCell(at: index)
            cell.heading = card.heading()
            cell.setHeadingImage(with: self.card.headingImage(), tintColor: self.tintColor)
            cell.title = CommonStrings.Ar.LandingAchievedTitle768
            // points
            let amountCompletedString = Localization.integerFormatter.string(from: NSNumber(integerLiteral: card.amountCompleted)) ?? "\(card.amountCompleted)"
            let totalString = Localization.integerFormatter.string(from: NSNumber(integerLiteral: card.total)) ?? "\(card.total)"
            cell.setSubtitle(CommonStrings.Ar.HomeCard.InProgressTitle773(amountCompletedString, totalString))
            // action
            cell.setActionTitle(card.actionTitle(), tintColor: self.tintColor)
            
            let model = VIAGraphViewModel(total: card.total, startingValue: 0, endingValue: card.amountCompleted)
            if let graph = VIAGraphView.viewFromNib(owner: nil) {
                graph.progressLabel.isHidden = true
                graph.ringWidth = 7
                graph.spacing = 1
                graph.setCountingLabel(font: UIFont.title2Font())
                cell.setMainView(with: graph)
                graph.model = model
            }
            return cell
            
        }
        return defaultCell(at: index)
    }
    
    func configureAppleWatchCard(at index: Int) -> UICollectionViewCell {
        if card.status == .InProgress || card.status == .Achieved {
        //if card.status == .NotStarted {
            let cell: AWCHomeScreenCardCell = dequeueReusableCell(at: index)
            cell.heading = card.heading()
            cell.setHeadingImage(with: self.card.headingImage(), tintColor: self.tintColor)
            cell.setMainImage(with: self.card.mainImageForCurrentStatus())
            cell.title = card.title()
            cell.setSubtitle(card.subtitle())
            cell.setActionTitle(card.actionTitle(), tintColor: self.tintColor)
            cell.setImageLabel(card.imageLabel())
            return cell
        }else{
            let cell: HomeScreenCardSubtitleCell = dequeueReusableCell(at: index)
            cell.heading = card.heading()
            cell.setHeadingImage(with: self.card.headingImage(), tintColor: self.tintColor)
            cell.setMainImage(with: self.card.mainImageForCurrentStatus(), tintColor: self.tintColor)
            cell.title = card.title()
            cell.setSubtitle(card.subtitle())
            cell.setActionTitle(card.actionTitle(), tintColor: self.tintColor)
            return cell
        }
    }
    
    func configureHealthHubCard(at index: Int) -> UICollectionViewCell {
        return defaultCell(at: index)
    }
    
    func configureMentalWellbeingCard(at index: Int) -> UICollectionViewCell {
        if card.status == .Done {
            let cell: HomeScreenCardSubtitleCell = dequeueReusableCell(at: index)
            cell.heading = card.heading()
            cell.setHeadingImage(with: self.card.headingImage(), tintColor: self.tintColor)
            cell.setMainImage(with: self.card.mainImageForCurrentStatus(), tintColor: self.tintColor)
            cell.title = card.title()
            cell.setSubtitle(card.subtitle())
            cell.setActionTitle(card.actionTitle(), tintColor: self.tintColor)
            return cell
        } else {
            let cell: HomeScreenCardSegmentedCell = dequeueReusableCell(at: index)
            cell.heading = card.heading()
            cell.setHeadingImage(with: self.card.headingImage(), tintColor: self.tintColor)
            cell.setSegmentedCircleView(totalSections: card.total, completedSections: card.amountCompleted, numberFormatter: integerFormatter)
            cell.title = card.title()
            cell.setSubtitle(card.subtitle())
            cell.setActionTitle(card.actionTitle(), tintColor: self.tintColor)
            return cell
        }
    }
    
    func configureNonSmokersDeclCard(at index: Int) -> UICollectionViewCell {
//      This method will notify if the home screen is fetching data from the backend
        NotificationCenter.default.addObserver(self, selector: #selector(self.isHomeLoading(_:)), name: NSNotification.Name(rawValue: "homeScreenDidRefresh"), object: nil)

        if card.status == .Done ||
            card.type == .NonSmokersDecl ||
            card.type == .VitalityHealthCheck ||
            card.type == .ScreenandVacc ||
            card.type == .WellDevandApps ||
            (card.type == .ActiveRewards && card.status == .NotStarted) {
            
            let cell: HomeScreenCardSubtitleCell = dequeueReusableCell(at: index)
            cell.heading = card.heading()
            cell.setHeadingImage(with: self.card.headingImage(), tintColor: self.tintColor)
            cell.setMainImage(with: self.card.mainImageForCurrentStatus(), tintColor: self.tintColor)
            cell.title = card.title()
            cell.setSubtitle(card.subtitle())
            cell.setActionTitle(card.actionTitle(), tintColor: self.tintColor)
            return cell
        } else {
            let cell: HomeScreenCardSegmentedCell = dequeueReusableCell(at: index)
            cell.heading = card.heading()
            cell.setHeadingImage(with: self.card.headingImage(), tintColor: self.tintColor)
            cell.setSegmentedCircleView(totalSections: card.total, completedSections: card.amountCompleted, numberFormatter: integerFormatter)
            cell.title = card.title()
            cell.setSubtitle(card.subtitle())
            cell.setActionTitle(card.actionTitle(), tintColor: self.tintColor)
            return cell
        }
    }
    
    func configureRewardsCard(at index: Int) -> UICollectionViewCell {
        let cell: HomeScreenCardSubtitleCell = dequeueReusableCell(at: index)
        cell.heading = card.heading()
        cell.setHeadingImage(with: self.card.headingImage(), tintColor: self.tintColor)
        cell.setMainImage(with: self.card.mainImageForCurrentStatus(), renderAsTemplate: false, tintColor: nil)
        cell.title = card.title()
        cell.setSubtitle(card.subtitle())
        cell.setActionTitle(card.actionTitle(), tintColor: self.tintColor)
        return cell
    }
	
    func configureNuffieldHealthServicesCard(at index: Int) -> UICollectionViewCell {
        let cell: HomeScreenCardSubtitleCell = dequeueReusableCell(at: index)
        //cell.configureCellForNuffield()
        cell.heading = card.heading()
        cell.setHeadingImage(with: self.card.headingImage(), tintColor: nil)
        cell.setMainImage(with: self.card.mainImageForCurrentStatus(), renderAsTemplate: false, tintColor: nil)
        cell.title = card.title()
        cell.setSubtitle(card.subtitle())
        cell.setActionTitle(card.actionTitle(), tintColor: self.tintColor)
        return cell
    }
    
    func configureEmployerStatusCard(at index: Int) -> UICollectionViewCell {
        let cell: HomeScreenCardSubtitleCell = dequeueReusableCell(at: index)
        cell.configureCellForNuffield()
        cell.heading = card.heading()
        cell.setHeadingImage(with: self.card.headingImage(), tintColor: self.tintColor)
        cell.setMainImage(with: self.card.mainImageForCurrentStatus(), renderAsTemplate: false, tintColor: nil)
        cell.title = card.title()
        cell.setSubtitle(card.subtitle())
        cell.setActionTitle(card.actionTitle(), tintColor: self.tintColor)
        return cell
    }
    
	func configureHealthServicesCard(at index: Int) -> UICollectionViewCell {
		let cell: HomeScreenCardSubtitleCell = dequeueReusableCell(at: index)
		cell.heading = card.heading()
		cell.setHeadingImage(with: self.card.headingImage(), tintColor: self.tintColor)
		cell.setMainImage(with: self.card.mainImageForCurrentStatus(), renderAsTemplate: false, tintColor: nil)
		cell.title = card.title()
		cell.setSubtitle(card.subtitle())
		cell.setActionTitle(card.actionTitle(), tintColor: self.tintColor)
		return cell
	}
    
    func configureActivationBarcodeCard(at index: Int) -> UICollectionViewCell {
        let cell: HomeScreenCardSubtitleCell = dequeueReusableCell(at: index)
        cell.heading = card.heading()
        cell.setHeadingImage(with: self.card.headingImage(), tintColor: self.tintColor)
        cell.setMainImage(with: self.card.mainImageForCurrentStatus(), renderAsTemplate: false, tintColor: nil)
        cell.title = card.title()
        cell.setSubtitle(card.subtitle())
        cell.setActionTitle(card.actionTitle(), tintColor: self.tintColor)
        return cell
    }
	
    func configureScreenandVaccCard(at index: Int) -> UICollectionViewCell {
        let cell: HomeScreenCardSubtitleCell = dequeueReusableCell(at: index)
        cell.heading = card.heading()
        cell.setHeadingImage(with: self.card.headingImage(), tintColor: self.tintColor)
        cell.setMainImage(with: self.card.mainImageForCurrentStatus(), tintColor: self.tintColor)
        cell.title = card.title()
        cell.setSubtitle(card.subtitle())
        cell.setActionTitle(card.actionTitle(), tintColor: self.tintColor)
        return cell
    }
    
    func configureVitNutAssessmentCard(at index: Int) -> UICollectionViewCell {
        if card.status == .Done ||
            card.type == .NonSmokersDecl ||
            card.type == .VitalityHealthCheck ||
            card.type == .ScreenandVacc ||
            card.type == .WellDevandApps ||
            (card.type == .ActiveRewards && card.status == .NotStarted) {
            
            let cell: HomeScreenCardSubtitleCell = dequeueReusableCell(at: index)
            cell.heading = card.heading()
            cell.setHeadingImage(with: self.card.headingImage(), tintColor: self.tintColor)
            cell.setMainImage(with: self.card.mainImageForCurrentStatus(), tintColor: self.tintColor)
            cell.title = card.title()
            cell.setSubtitle(card.subtitle())
            cell.setActionTitle(card.actionTitle(), tintColor: self.tintColor)
            return cell
        } else {
            let cell: HomeScreenCardSegmentedCell = dequeueReusableCell(at: index)
            cell.heading = card.heading()
            cell.setHeadingImage(with: self.card.headingImage(), tintColor: self.tintColor)
            cell.setSegmentedCircleView(totalSections: card.total, completedSections: card.amountCompleted, numberFormatter: integerFormatter)
            cell.title = card.title()
            cell.setSubtitle(card.subtitle())
            cell.setActionTitle(card.actionTitle(), tintColor: self.tintColor)
            return cell
        }
    }
    
    func configureVitalityHealthCheckCard(at index: Int) -> UICollectionViewCell {
        if card.status == .Done ||
            card.type == .NonSmokersDecl ||
            card.type == .VitalityHealthCheck ||
            card.type == .ScreenandVacc ||
            card.type == .WellDevandApps ||
            (card.type == .ActiveRewards && card.status == .NotStarted) {
            
            let cell: HomeScreenCardSubtitleCell = dequeueReusableCell(at: index)
            cell.heading = card.heading()
            cell.setHeadingImage(with: self.card.headingImage(), tintColor: self.tintColor)
            cell.setMainImage(with: self.card.mainImageForCurrentStatus(), tintColor: self.tintColor)
            cell.title = card.title()
            cell.setSubtitle(card.subtitle())
            cell.setActionTitle(card.actionTitle(), tintColor: self.tintColor)
            return cell
        } else {
            let cell: HomeScreenCardSegmentedCell = dequeueReusableCell(at: index)
            cell.heading = card.heading()
            cell.setHeadingImage(with: self.card.headingImage(), tintColor: self.tintColor)
            cell.setSegmentedCircleView(totalSections: card.total, completedSections: card.amountCompleted, numberFormatter: integerFormatter)
            cell.title = card.title()
            cell.setSubtitle(card.subtitle())
            cell.setActionTitle(card.actionTitle(), tintColor: self.tintColor)
            return cell
        }
    }
    
    func configureVitalityHealthReviewCard(at index: Int) -> UICollectionViewCell {
        if card.status == .Done ||
            card.type == .NonSmokersDecl ||
            card.type == .VitalityHealthCheck ||
            card.type == .ScreenandVacc ||
            card.type == .WellDevandApps ||
            (card.type == .ActiveRewards && card.status == .NotStarted) {
            
            let cell: HomeScreenCardSubtitleCell = dequeueReusableCell(at: index)
            cell.heading = card.heading()
            cell.setHeadingImage(with: self.card.headingImage(), tintColor: self.tintColor)
            cell.setMainImage(with: self.card.mainImageForCurrentStatus(), tintColor: self.tintColor)
            cell.title = card.title()
            cell.setSubtitle(card.subtitle())
            cell.setActionTitle(card.actionTitle(), tintColor: self.tintColor)
            return cell
        } else {
            let cell: HomeScreenCardSegmentedCell = dequeueReusableCell(at: index)
            cell.heading = card.heading()
            cell.setHeadingImage(with: self.card.headingImage(), tintColor: self.tintColor)
            cell.setSegmentedCircleView(totalSections: card.total, completedSections: card.amountCompleted, numberFormatter: integerFormatter)
            cell.title = card.title()
            cell.setSubtitle(card.subtitle())
            cell.setActionTitle(card.actionTitle(), tintColor: self.tintColor)
            return cell
        }
    }
    
    func configureDeviceCashbackCard(at index: Int) -> UICollectionViewCell {
        if card.status == .InProgress ||
            card.status == .Achieved {
            
            let cell: DCHomeScreenCardSubtitleCell = dequeueReusableCell(at: index)
            cell.heading = card.heading()
            cell.setHeadingImage(with: self.card.headingImage(), tintColor: self.tintColor)
            cell.setMainImage(with: self.card.mainImageForCurrentStatus())
            cell.title = card.title()
            cell.setSubtitle(card.subtitle())
            cell.setActionTitle(card.actionTitle(), tintColor: self.tintColor)
            return cell
        } else {
            let cell: DCHomeScreenCardSubtitleWithoutMainImageCell = dequeueReusableCell(at: index)
            cell.heading = card.heading()
            cell.setHeadingImage(with: self.card.headingImage(), tintColor: self.tintColor)
            cell.title = card.title()
            cell.setSubtitle(card.subtitle())
            cell.setActionTitle(card.actionTitle(), tintColor: self.tintColor)
            return cell
        }
    }
    
    func configurePolicyCashbackCard(at index: Int) -> UICollectionViewCell {
        let cell: HomeScreenCardSubtitleCell = dequeueReusableCell(at: index)
        cell.heading = card.heading()
        cell.setHeadingImage(with: self.card.headingImage(), tintColor: self.tintColor)
        cell.setMainImage(with: self.card.mainImageForCurrentStatus())
        cell.title = card.title()
        cell.setSubtitle(card.subtitle(), tintColor: self.tintColor)
        cell.setActionTitle(card.actionTitle(), tintColor: self.tintColor)
        return cell
    }
    
    func configureOrganisedFitnessEventsCard(at index: Int) -> UICollectionViewCell {
        let cell: HomeScreenCardSubtitleCell = dequeueReusableCell(at: index)
        cell.heading = card.heading()
        cell.setHeadingImage(with: self.card.headingImage(), tintColor: self.tintColor)
        cell.setMainImage(with: self.card.mainImageForCurrentStatus(), tintColor: self.tintColor)
        cell.title = card.title()
        cell.setSubtitle(card.subtitle(), tintColor: self.tintColor)
        cell.setActionTitle(card.actionTitle(), tintColor: self.tintColor)
        return cell
    }
    
    func configureVouchersCard(at index: Int) -> UICollectionViewCell {
        if let rewardId = card.voucherRewardId() {
            
            /** FC-26793 : UKE : Change Request
             * Re-construct the if else to switch
             * Added a case for the new Rewards.
             */
            switch rewardId {
            case RewardReferences.StarbucksVoucher.rawValue:
                return configureStarbucksVouchersCard(at: index)
            case RewardReferences.Cineworld.rawValue:
                return configureCineworldVouchersCard(at: index)
            case RewardReferences.CineworldOrVue.rawValue:
                return configureCineworldOrVueVouchersCard(at: index)
            case RewardReferences.Vue.rawValue:
                return configureVueVouchersCard(at: index)
            case RewardReferences.EasyTickets.rawValue:
                return configureEasyTicketVouchersCard(at: index)
            case RewardReferences.FoodPanda.rawValue:
                return configureFoodPandaVouchersCard(at: index)
            case RewardReferences.Cinemark.rawValue:
                return configureCinemarkCard(at: index)
            case RewardReferences.JuanValdez.rawValue:
                return configureJuanValdezCard(at: index)
            case RewardReferences.AmazonSmileVoucher.rawValue,
                 RewardReferences.BookMyShow.rawValue,
                 RewardReferences.CafeCoffeeDay.rawValue,
                 RewardReferences.Spotify.rawValue:
                return configureUKEVoucherSolutionsCard(at: index)
            default: return configureGenericVouchersCard(at: index)
            }
        }
        return configureGenericVouchersCard(at: index) // fallback
    }
    
    func configureGenericVouchersCard(at index: Int) -> UICollectionViewCell {
        let cell: HomeScreenCardSubtitleCell = dequeueReusableCell(at: index)
        cell.heading = card.heading()
        cell.setHeadingImage(with: self.card.headingImage(), tintColor: self.tintColor)
        cell.setMainImage(with: self.card.mainImageForCurrentStatus(), tintColor: self.tintColor)
        cell.title = card.title()
        cell.setSubtitle(card.subtitle())
        cell.setActionTitle(card.actionTitle(), tintColor: self.tintColor)
        return cell
    }
    
    /* FC-26793 : UKE : Change Request */
    func configureUKEVoucherSolutionsCard(at index: Int) -> UICollectionViewCell {
        let cell: HomeScreenCardSubtitleCell = dequeueReusableCell(at: index)
        cell.heading = card.heading()
        cell.setHeadingImage(with: self.card.headingImage(), tintColor: self.tintColor)
        if let voucherRewardID = card.voucherRewardId() {
            cell.setMainImage(with: VIAARRewardHelper.partnerImage(for: RewardReferences(rawValue: voucherRewardID) ?? RewardReferences(rawValue: -1)!))
        }
        cell.title = card.title()
        cell.setSubtitle(card.subtitle())
        cell.setActionTitle(card.actionTitle(), tintColor: self.tintColor)
        return cell
    }
    
    func configureStarbucksVouchersCard(at index: Int) -> UICollectionViewCell {
        let cell: HomeScreenCardSubtitleCell = dequeueReusableCell(at: index)
        cell.heading = card.heading()
        cell.setHeadingImage(with: self.card.headingImage(), tintColor: self.tintColor)
        cell.setMainImage(with: VIAARRewardHelper.partnerImage(for: .StarbucksVoucher))
        cell.title = card.title()
        cell.setSubtitle(card.subtitle())
        cell.setActionTitle(card.actionTitle(), tintColor: self.tintColor)
        return cell
    }
    
    func configureCineworldVouchersCard(at index: Int) -> UICollectionViewCell {
        let cell: HomeScreenCardSubtitleCell = dequeueReusableCell(at: index)
        cell.heading = card.heading()
        cell.setHeadingImage(with: self.card.headingImage(), tintColor: self.tintColor)
        cell.setMainImage(with: VIAARRewardHelper.partnerImage(for: .Cineworld))
        cell.title = card.title()
        cell.setSubtitle(card.subtitle())
        cell.setActionTitle(card.actionTitle(), tintColor: self.tintColor)
        return cell
    }
    
    func configureCineworldOrVueVouchersCard(at index: Int) -> UICollectionViewCell {
        let cell: HomeScreenCardSubtitleCell = dequeueReusableCell(at: index)
        cell.heading = card.heading()
        cell.setHeadingImage(with: self.card.headingImage(), tintColor: self.tintColor)
        cell.setMainImage(with: VIAARRewardHelper.partnerImage(for: .CineworldOrVue))
        cell.title = card.title()
        cell.setSubtitle(card.subtitle())
        cell.setActionTitle(card.actionTitle(), tintColor: self.tintColor)
        return cell
    }
    
    func configureEasyTicketVouchersCard(at index: Int) -> UICollectionViewCell {
        let cell: HomeScreenCardSubtitleCell = dequeueReusableCell(at: index)
        cell.heading = card.heading()
        cell.setHeadingImage(with: self.card.headingImage(), tintColor: self.tintColor)
        cell.setMainImage(with: VIAARRewardHelper.partnerImage(for: .EasyTickets))
        cell.title = card.title()
        cell.setSubtitle(card.subtitle())
        cell.setActionTitle(card.actionTitle(), tintColor: self.tintColor)
        return cell
    }
    
    func configureFoodPandaVouchersCard(at index: Int) -> UICollectionViewCell {
        let cell: HomeScreenCardSubtitleCell = dequeueReusableCell(at: index)
        cell.heading = card.heading()
        cell.setHeadingImage(with: self.card.headingImage(), tintColor: self.tintColor)
        cell.setMainImage(with: VIAARRewardHelper.partnerImage(for: .FoodPanda))
        cell.title = card.title()
        cell.setSubtitle(card.subtitle())
        cell.setActionTitle(card.actionTitle(), tintColor: self.tintColor)
        return cell
    }
    
    func configureVueVouchersCard(at index: Int) -> UICollectionViewCell {
        let cell: HomeScreenCardSubtitleCell = dequeueReusableCell(at: index)
        cell.heading = card.heading()
        cell.setHeadingImage(with: self.card.headingImage(), tintColor: self.tintColor)
        cell.setMainImage(with: VIAARRewardHelper.partnerImage(for: .Vue))
        cell.title = card.title()
        cell.setSubtitle(card.subtitle())
        cell.setActionTitle(card.actionTitle(), tintColor: self.tintColor)
        return cell
    }
    
    func configureCinemarkCard(at index: Int) -> UICollectionViewCell {
        let cell: HomeScreenCardSubtitleCell = dequeueReusableCell(at: index)
        cell.heading = card.heading()
        cell.setHeadingImage(with: self.card.headingImage(), tintColor: self.tintColor)
        cell.setMainImage(with: VIAARRewardHelper.partnerImage(for: .Cinemark))
        cell.title = card.title()
        cell.setSubtitle(card.subtitle())
        cell.setActionTitle(card.actionTitle(), tintColor: self.tintColor)
        return cell
    }
    
    func configureJuanValdezCard(at index: Int) -> UICollectionViewCell {
        let cell: HomeScreenCardSubtitleCell = dequeueReusableCell(at: index)
        cell.heading = card.heading()
        cell.setHeadingImage(with: self.card.headingImage(), tintColor: self.tintColor)
        cell.setMainImage(with: VIAARRewardHelper.partnerImage(for: .JuanValdez))
        cell.title = card.title()
        cell.setSubtitle(card.subtitle())
        cell.setActionTitle(card.actionTitle(), tintColor: self.tintColor)
        return cell
    }
    
    func configureWellDevandAppsCard(at index: Int) -> UICollectionViewCell {
        if card.status == .Done ||
            card.type == .NonSmokersDecl ||
            card.type == .VitalityHealthCheck ||
            card.type == .ScreenandVacc ||
            card.type == .WellDevandApps ||
            (card.type == .ActiveRewards && card.status == .NotStarted) {
            
            let cell: HomeScreenCardSubtitleCell = dequeueReusableCell(at: index)
            cell.heading = card.heading()
            cell.setHeadingImage(with: self.card.headingImage(), tintColor: self.tintColor)
            cell.setMainImage(with: self.card.mainImageForCurrentStatus(), tintColor: self.tintColor)
            cell.title = card.title()
            cell.setSubtitle(card.subtitle())
            cell.setActionTitle(card.actionTitle(), tintColor: self.tintColor)
            return cell
        } else {
            let cell: HomeScreenCardSegmentedCell = dequeueReusableCell(at: index)
            cell.heading = card.heading()
            cell.setHeadingImage(with: self.card.headingImage(), tintColor: self.tintColor)
            cell.setSegmentedCircleView(totalSections: card.total, completedSections: card.amountCompleted, numberFormatter: integerFormatter)
            cell.title = card.title()
            cell.setSubtitle(card.subtitle())
            cell.setActionTitle(card.actionTitle(), tintColor: self.tintColor)
            return cell
        }
    }
    
    func configurePartnerRewardsCard(at index: Int) -> UICollectionViewCell {
        let cell: HomeScreenCardSubtitleCell = dequeueReusableCell(at: index)
        cell.heading = card.heading()
        cell.setHeadingImage(with: self.card.headingImage(), tintColor: self.tintColor)
        cell.setMainImage(with: self.card.mainImageForCurrentStatus(), renderAsTemplate: false, tintColor: self.tintColor)
        cell.title = card.title()
        cell.setSubtitle(card.subtitle())
        cell.setActionTitle(card.actionTitle(), tintColor: self.tintColor)
        return cell
    }
    
    func configureAvailableRewardsCard(at index: Int) -> UICollectionViewCell {
        let cell: HomeScreenCardSubtitleCell = dequeueReusableCell(at: index)
        cell.heading = card.heading()
        cell.setHeadingImage(with: self.card.headingImage(), tintColor: self.tintColor)
        cell.setMainImage(with: self.card.mainImageForCurrentStatus(), renderAsTemplate: false, tintColor: self.tintColor)
        cell.title = card.title()
        cell.setSubtitle(card.subtitle())
        cell.setActionTitle(card.actionTitle(), tintColor: self.tintColor)
        return cell
    }
    func didSelectItem(at index: Int) {
        guard let delegate = self.viewController as? HomeScreenNavigationDelegate else { return }

        switch card.type {
        case .NonSmokersDecl:
            if card.status != .Done && !isHomeCardRefreshing{
                delegate.showNonSmokersDeclaration(nil)
            }
        case .VitalityHealthCheck:
            delegate.showVHC(nil)
        case .ActiveRewards:
            delegate.showActiveRewards(nil)
        case .VitalityHealthReview:
            delegate.showVHR(nil)
        case .WellDevandApps:
            delegate.showWellnessDevicesAndApps(nil)
        case .VitNutAssessment:
            delegate.showVNA(nil)
        case .RewardPartners:
            delegate.showRewardPartners(nil)
        case .HealthPartners:
            delegate.showHealthPartners(nil)
        case .WellnessPartners:
            delegate.showWellnessPartners(nil)
        case .MentalWellbeing:
            delegate.showMWB(nil)
        case .ScreenandVacc:
            delegate.showSAV(nil)
        case .AppleWatch:
            delegate.showAppleWatchCashback(nil)
        case .HealthHub:
            fallthrough
        case .Rewards:
            if card.rewardsIsLearnMoreState() {
                delegate.showARLearnMore(nil)
            } else if card.rewardsHasSingleUnclaimedReward() {
				if let cardItem = card.unclaimedRewards.first {
					for metaDataDictionary in cardItem.metaData ?? [] {
						if let rewardId = metaDataDictionary.filter({
							$0.key == CardItemMetadataTypeRef.AwardedRewardId
						}).first?.value {
							if cardItem.type == CardItemTypeRef.WheelSpin {
                                if let sliActiveChallenge = VIAApplicableFeatures.default.showSwapRewardsOption, sliActiveChallenge {
                                    delegate.showARAvailableRewards(rewardId)
                                } else {
                                    delegate.showARSpinner(rewardId)
                                }
							} else {
								delegate.showARChooseRewards(rewardId)
							}
							return
						}
					}
				}
            } else if card.rewardsHasMultipleUnclaimedRewards() {
                delegate.showARCurrentRewards(nil)
            } else {
                fallthrough
            }
        case .Vouchers:
            for metaDataDictionary in card.metaData ?? [] {
                if let rewardId = metaDataDictionary.filter({
                    $0.key == CardMetadataTypeRef.AwardedRewardId
                }).first?.value {
                    delegate.showARVoucher(rewardId)
                    return
                }
            }
        case .OrganisedFitnessEvents:
            delegate.showOFE(nil)
        case .DeviceCashback:
            delegate.showDeviceCashback(nil)
        case .PolicyCashback:
            delegate.showPolicyCashback(nil)
        case .RewardSelection:
            for metaDataDictionary in card.metaData ?? [] {
                if let rewardId = metaDataDictionary.filter({
                    $0.key == CardMetadataTypeRef.AwardedRewardId
                }).first?.value {
                    delegate.showARVoucher(rewardId)
                    return
                }
            }
        case .ActivationBarcode:
            delegate.showActivationBarcode(nil)
		case .HealthServices:
			delegate.showCorporateBenefitPartners(nil)
        case .NuffieldHealthServices:
            delegate.showNuffieldHealthServices(nil)
        case .AvailableRewards:
            delegate.showAvailableRewards(nil)
        case .EmployerStatus:
            delegate.showEmployerReward(nil)
        case .UKEEmployerStatus:
            delegate.showEmployerReward(nil)
        case .Unknown:
            debugPrint("Unknown")
        
        }
    }

}
