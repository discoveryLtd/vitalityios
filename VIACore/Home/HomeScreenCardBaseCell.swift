import Foundation
import VIACommon

// This class is not to be altered. Each card's specific elements goes into
// the cardContentView property with the appropriate constraints and actions

class HomeScreenCardBaseCell: UICollectionViewCell {

    static let cornerRadius: CGFloat = 10

    public var heading: String? {
        get {
            return headingLabel.text
        }
        set {
            self.headingLabel.text = newValue
        }
    }
    
    
    lazy private var containerWithShadow: UIView = {
        let view = UIView(frame: .zero)
        view.backgroundColor = UIColor.clear
        view.layer.borderWidth = 0
        view.layer.shadowOffset = CGSize(width: 0, height: 3)
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOpacity = 0.1
        
        return view
    }()

    lazy private var container: UIView = {
        let view = UIView(frame: .zero)
        view.backgroundColor = UIColor.day()
        view.layer.borderColor = UIColor(white: 200 / 255.0, alpha: 1.0).cgColor
        view.layer.borderWidth = 1
        view.layer.cornerRadius = HomeScreenCardBaseCell.cornerRadius
        view.clipsToBounds = true
        
        return view
    }()

    lazy private var headingContainer: UIView = {
        let view = UIView(frame: .zero)
        view.backgroundColor = UIColor.homeScreenCardTitleBackground()
        return view
    }()

    lazy private var headingStackView: UIStackView = {
        let view = UIStackView(frame: .zero)
        view.axis = .horizontal
        view.spacing = 7
        return view
    }()

    lazy private var headingImageView: UIImageView = {
        let view = UIImageView(frame: .zero)
        view.contentMode = .center
        view.snp.makeConstraints({ (make) in
            make.width.height.equalTo(20)
        })
        return view
    }()

    lazy private var headingLabel: UILabel = {
        let view = UILabel(frame: .zero)
        view.textAlignment = .left
        view.textColor = UIColor.darkGrey()
        view.font = UIFont.systemFont(ofSize: 14)
        return view
    }()

    lazy public var cardContentView: UIView = {
        let view = UIView(frame: .zero)
        return view
    }()

    private func setupSubviews() {
        self.backgroundColor = .clear
        
        if let shouldResizeCard = VIAApplicableFeatures.default.resizeHomeCard,shouldResizeCard {
            containerWithShadow.addSubview(container)
            container.snp.makeConstraints({ (make) in
                make.top.left.right.bottom.equalToSuperview()
            })
            
            self.contentView.addSubview(containerWithShadow)
            containerWithShadow.snp.makeConstraints({ (make) in
                make.top.left.right.equalToSuperview()
                make.bottom.equalToSuperview().inset(10)
            })
        }else{
            self.contentView.addSubview(container)
            container.snp.makeConstraints({ (make) in
                make.top.left.right.bottom.equalToSuperview()
            })
        }

        setupHeadingViews()
        setupCardContentViewContainer()
    }

    func setupHeadingViews() {
        container.addSubview(headingContainer)
        headingContainer.snp.makeConstraints({ (make) in
            make.top.left.right.equalToSuperview()
            make.height.equalTo(35)
        })

        headingContainer.addSubview(headingStackView)
        headingStackView.snp.makeConstraints({ (make) in
            make.top.equalToSuperview().inset(5)
            make.left.right.equalToSuperview().inset(15)
            make.height.equalToSuperview()
        })

        headingStackView.addArrangedSubview(headingImageView)
        headingStackView.addArrangedSubview(headingLabel)
    }

    func setupCardContentViewContainer() {
        container.addSubview(cardContentView)
        cardContentView.snp.makeConstraints({ (make) in
            make.top.equalTo(headingContainer.snp.bottom)
            make.left.right.bottom.equalToSuperview()
        })

        setupCardContentView(cardContentView)
    }

    open func setupCardContentView(_ contentView: UIView) {
        debugPrint("To be implemented by subclass")
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

        setupSubviews()
    }

    override init(frame: CGRect) {
        super.init(frame: frame)

        setupSubviews()
    }

    public func setHeadingImage(with asset: VIACoreImageAsset, tintColor: UIColor?) {
        self.headingImageView.image = asset.templateImage
        self.headingImageView.tintColor = tintColor
    }

}
