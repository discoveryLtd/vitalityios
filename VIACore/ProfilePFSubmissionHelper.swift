//
//  ProfilePFSubmissionHelper.swift
//  VitalityActive
//
//  Created by Von Kervin R. Tuico on 27/03/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import UIKit
import Foundation
import Photos

import VIAUtilities
import VitalityKit
import VIAImageManager

import RealmSwift

public protocol PFPhotoUploadDelegate: class {
    func displayImageUploadErrorMessage(with completion: @escaping (_ resubmit: Bool) -> Void)
}

protocol PFEventsProcessor {
    func sendFeedbackEmail(completion: @escaping (_ error: Error?) -> Void)
}

class ProfilePFSubmissionHelper: PFEventsProcessor {
  
    
    internal weak var photoUploadDelegate: PFPhotoUploadDelegate?
    
    /* Initialize Realm */
    internal let imagesRealm = DataProvider.newProvideFeedbackRealm()
    internal let provideFeedbackRealm = DataProvider.newProvideFeedbackRealm()
    
    internal let photoUploader = PhotoUploader()
    private var completion: ((Error?) -> (Void)) = { error in
        debugPrint("Complete")
    }
    
    func setupDelegate(_ delegate: PFPhotoUploadDelegate) {
        photoUploadDelegate = delegate
    }
    
    /* Trigger to upload the selected images */
    func sendFeedbackEmail(completion: @escaping (_ error: Error?) -> Void) {
        self.completion = completion
        submitImagesRetreiveReferences()
    }
    
    /* Upload the selected images */
    internal func submitImagesRetreiveReferences() {
        guard let realmCapturedAssets = imagesRealm.allPhotos() else {
            debugPrint("No images to upload")
            return
        }
        
        photoUploader.delegate = self
        photoUploader.getAllUploadableImages(realmCapturedAssets)
        
        /* Parse all the Uploadable Images to get its Data and Append it to a new variable */
        let images = photoUploader.imagesToBeUploaded
        var imageDataList = Array<Data>()
        for image in images {
            imageDataList.append(image.data)
        }
        
        if !imageDataList.isEmpty {
            sendFeedback(imageData: imageDataList)
        }
    }
    
    internal func sendFeedback(imageData: [Data]) {
            var feedbacks = [Feedback]()
            var feedback: Feedback?
            let coreRealm = DataProvider.newRealm()
            let partyId = coreRealm.getPartyId()
            let membershipId = coreRealm.getMembershipId()
            let deviceDetails = "\(UIDevice.current.getDeviceType())"
            let iOSVersion = UIDevice.current.systemVersion as String? ?? "Unknown"
            let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
            
            for item in self.provideFeedbackRealm.allProvideFeedbackItems() {
                
                feedback = Feedback(feedbackType: item.selectedFeedbackType, feedbackTypeId: item.selectedFeedbackTypeId, feedbackTimestamp: Date(), feedbackMessage: item.userFeedback, memberPartyId: "\(partyId)", memberFirstName: item.memberFirstName, memberLastName: item.memberLastName, contactNumber: item.userContactNumber, membershipNumber: "\(membershipId)", memberEmailAddress: item.userEmailAddress, channelName: "Mobile", deviceDetails: deviceDetails, osDetails: iOSVersion, appVersion: appVersion!)
                
                feedbacks.append(feedback!)
            }
        
            if !feedbacks.isEmpty, !imageData.isEmpty {
                Wire.Content.postSendFeedbackEmail(feedback: feedbacks, imageData: imageData, completion: { (error) in
                    if (error != nil) {
                        self.completion(error)
                    } else {
                        self.completion(nil)
                    }
                })
            }
    }
}

extension ProfilePFSubmissionHelper: PhotoUploaderDelegate{
    
    /*  */
    func imagesUploaded(references: Array<String>) {
        
    }
    
    func onImageUploadFailed() {
        
    }
    
}
