//
//  ProfileSecurityViewController.swift
//  VitalityActive
//
//  Created by Steven Layug on 11/10/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

import VitalityKit
import VIAUIKit
import VIAUtilities
import VIAActiveRewards
import VIACommon

public class  ProfileSecurityViewController: VIATableViewController {
    
    @IBAction func unwindToProfileSecurityViewController(segue:UIStoryboardSegue) { }
    
    enum Rows: Int, EnumCollection {
        case TitleRow = 0
        case TouchIdRow = 1
        case RememberMeRow = 2
        case ChangePasswordRow = 3
    }
    
    var touchIdPref = TouchIdPreference()
    var rememberMePref = RememberMePreference()
    
    @IBAction func unwindFromCViewController(segue:UIStoryboardSegue) {
    }
    
    public override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        
        setupNavBar()
        configureTableView()
        touchIdPref.touchIdDelegate = self
        
    }
    
    public override func viewDidDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    func configureTableView() {
        self.tableView.register(SettingsHeader.nib(), forCellReuseIdentifier: "header")
        self.tableView.register(FirstTimePreferenceSwitchCell.nib(), forCellReuseIdentifier: "cell")
        self.tableView.estimatedRowHeight = 200
        self.tableView.separatorInset = UIEdgeInsets(top: 0, left: 52, bottom: 0, right: 0)
        self.tableView.backgroundColor = .white
        self.tableView.tableFooterView = UIView()
        self.removeFooterHeaderSpaces()
    }
    
    func setupNavBar() {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        
        self.hideBackButtonTitle()
    }
    
    func removeFooterHeaderSpaces() {
        self.tableView.estimatedSectionHeaderHeight = 0
        self.tableView.estimatedSectionFooterHeight = 0
    }
    
    public override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    public override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Rows.allValues.count
    }
    
    public override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        // Hide touch ID and Remember Me on UKE
        if let enableTouchID = VIAApplicableFeatures.default.enableTouchID,
            !enableTouchID,let enableChangePassword = VIAApplicableFeatures.default.enableChangePassword,!enableChangePassword {
            if (indexPath.row == 1 || indexPath.row == 3){
                return 0
            }
            return UITableViewAutomaticDimension
        } else {
            return UITableViewAutomaticDimension
        }
    }
    
    func configSwitchCell(_ switchCell: FirstTimePreferenceSwitchCell, dataObject: PreferenceDataObject, completion: @escaping (_:UISwitch)->Void) -> FirstTimePreferenceSwitchCell {
        switchCell.setupCell()
        switchCell.actionButton.isHidden = true
        if let image = dataObject.preferenceImage {
            switchCell.setImage(asset: image)
        }
        if let title = dataObject.preferenceTitle {
            switchCell.setTitle(title)
        }
        if let detail = dataObject.preferencesDetail {
            switchCell.setDetailText(detail)
        }
        
        switchCell.setActionForSwitch {(sender) -> (Void)?  in
            return completion(sender)
        }
        
        switchCell.selectionStyle = .none
        
        return switchCell
    }
    
    func setupHeader(_ cell: SettingsHeader) -> SettingsHeader {
        cell.setTitle(CommonStrings.UserPrefs.SecurityGroupHeaderTitle77)
        cell.setDetailText(CommonStrings.UserPrefs.SecurityGroupHeaderMessage78)
        
        // Remove separator
        cell.separatorInset = UIEdgeInsetsMake(0, 0, 0, tableView.bounds.width);
        
        cell.selectionStyle = .none
        return cell
    }
    public override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let row = Rows(rawValue: indexPath.row)
        
        if row == .TouchIdRow {
            var cell: UITableViewCell = UITableViewCell.init()
            
            if let enableTouchID = VIAApplicableFeatures.default.enableTouchID,
                enableTouchID{
                // touch ID
                guard var switchCell = self.tableView.dequeueReusableCell(withIdentifier: "cell") as? FirstTimePreferenceSwitchCell else {
                    return self.tableView.defaultTableViewCell()
                }
                
                switchCell = configSwitchCell(switchCell, dataObject: touchIdPref) {[weak self] (sender) in
                    self?.touchIdPref.toggleTouchId(sender: sender)
                }
                
                switchCell.switchAction.setOn(touchIdPref.preferenceInitialState(), animated: false)
                cell = switchCell
            } else {
                // Hide touch ID for UKE
                cell.separatorInset = UIEdgeInsetsMake(0, tableView.bounds.width, 0, 0)
                cell.selectionStyle = .none
            }
            return cell
        }
        else if row == .RememberMeRow {
            
            // Remember Me
            // Specific cell identifier used because of error encountered
            guard var switchCell = self.tableView.dequeueReusableCell(withIdentifier: "cell") as? FirstTimePreferenceSwitchCell else {
                return self.tableView.defaultTableViewCell()
            }
            
            switchCell = configSwitchCell(switchCell, dataObject: rememberMePref) {[weak self] (sender) in
                self?.rememberMePref.toggleRememberMe(sender: sender)
            }
            switchCell.switchAction.setOn(rememberMePref.preferenceInitialState(), animated: false)
            
            if !VIAApplicableFeatures.default.rememberMePreferenceDefault!{
                if (touchIdPref.preferenceInitialState()) {
                    switchCell.isUserInteractionEnabled = false
                    switchCell.switchAction.alpha = 0.5
                } else {
                    switchCell.isUserInteractionEnabled = true
                    switchCell.switchAction.alpha = 1
                }
            }
            
            if let enableTouchID = VIAApplicableFeatures.default.enableTouchID,
                !enableTouchID,let enableChangePassword = VIAApplicableFeatures.default.enableChangePassword,!enableChangePassword{
                // Remove separator for UKE
                switchCell.separatorInset = UIEdgeInsetsMake(0, tableView.bounds.width, 0, 0)
            }
            
            return switchCell
        }
        else if row == .ChangePasswordRow {
            // Change Password
            // Specific cell identifier used because of error encountered
            
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "changePasswordCell", for: indexPath)
            
            if let enableChangePassword = VIAApplicableFeatures.default.enableChangePassword,
                enableChangePassword{
                // Change Password for UKE
                cell.imageView?.tintColor = UIColor.currentGlobalTintColor()
                
                cell.textLabel?.text = CommonStrings.Settings.SecurityChangePasswordTitle827
                cell.imageView?.image = VIACoreAsset.Profile.profileSecurityChangePassword.templateImage
                cell.accessoryType = .disclosureIndicator
            } else {
                // Hide Change Password for UKE
                cell.separatorInset = UIEdgeInsetsMake(0, tableView.bounds.width, 0, 0)
                cell.selectionStyle = .none
            }
            return cell
            
        }
        else {
            // header
            guard var cell = self.tableView.dequeueReusableCell(withIdentifier: "header") as? SettingsHeader else {
                return self.tableView.defaultTableViewCell()
            }
            cell = setupHeader(cell)
            return cell
        }
    }
    
    override public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let row = Rows(rawValue: indexPath.row)
        
        if row == .ChangePasswordRow  {
            self.performSegue(withIdentifier: "showChangePassword", sender: self)
        }
    }
}

extension ProfileSecurityViewController: touchIdDelegate {
    func reloadScreen() {
        self.tableView.reloadData()
    }
    
    func reloadSecurity() {
        self.tableView.reloadData()
    }
    
    func promptInvalidSecurityDetails(errorMessage: String, attempts:Int) {
        let alert = UIAlertController(title: title, message: errorMessage, preferredStyle: .alert)
        
        if attempts == AppSettings.getMaxPasswordAttemptForTouchID() {
            alert.addAction(UIAlertAction(title: CommonStrings.OkButtonTitle40, style: .default,
                                          handler: { [weak self] (action) in
                VIALoginViewController.showLogin()
            }))
        } else {
            alert.addAction(UIAlertAction(title: CommonStrings.OkButtonTitle40, style: .cancel, handler: nil))
        }
        
        switch attempts {
        case 2...3:
            alert.addAction(UIAlertAction(title: CommonStrings.Login.ForgotPasswordButtonTitle22, style: .default,
                                          handler: { [weak self] (action) in
                                            self?.performSegue(withIdentifier: "showForgotPassword", sender: nil)
            }))
            break
        case AppSettings.getMaxPasswordAttemptForTouchID()...:
            alert.addAction(UIAlertAction(title: CommonStrings.Login.ResetPasswordButtonTitle22, style: .default,
                                          handler: { [weak self] (action) in
                                            self?.performSegue(withIdentifier: "showForgotPassword", sender: nil)
            }))
            break
        default: break
        }
        self.present(alert, animated: true, completion: nil)
    }
}
