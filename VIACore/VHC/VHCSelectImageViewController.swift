//
//  VHCSelectImageViewController.swift
//  VIACore
//
//  Created by Michelle R. Oratil on 11/08/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import UIKit
import AVFoundation
import Photos

import VitalityKit
import VIACommon

@objc protocol VHCOptionalImagePickerDelegate: class {
    @objc optional func dismissComplete()
}

protocol VHCImagePickerDelegate: class {
    func pass(imageInfo: [String : Any])
    func show(alert: UIAlertController)
    func present(imagePicker: UIImagePickerController)
}

class VHCSelectImageController: NSObject, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    private var imagePicker = UIImagePickerController()
    
    // TODO: Should this delegate be optional and then hold a weak reference?
    weak var delegate: VHCImagePickerDelegate?
    weak var optionalDelegate: VHCOptionalImagePickerDelegate?
    lazy var allowCropping: Bool = false
    fileprivate var configuration: UIImagePickerController.ImageControllerConfiguration?
    
    init(pickerDelegate: VHCImagePickerDelegate, configuration: UIImagePickerController.ImageControllerConfiguration) {
        self.delegate = pickerDelegate
        self.configuration = configuration
    }
    
    func selectImage(view: UIView? = nil) {
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
            let cameraAction = UIAlertAction(title: VhcStrings.Proof.ActionTakePhotoAlertTitle167, style: .default) { (UIAlertAction) in
                self.showCamera()
            }
            actionSheet.addAction(cameraAction)
        }
        
        
        let photoAction = UIAlertAction(title: VhcStrings.Proof.ActionChooseFromLibraryAlertTitle168,
                                        style: UIAlertActionStyle.default) { (UIAlertAction) in
                                            self.showImagePicker()
        }
        actionSheet.addAction(photoAction)
        
        if nil != view {
            actionSheet.popoverPresentationController?.sourceView = view
            actionSheet.popoverPresentationController?.sourceRect = view!.bounds
        }
        
        let cancelAction = UIAlertAction(title: CommonStrings.CancelButtonTitle24, style: UIAlertActionStyle.cancel) { (UIAlertAction) in
            actionSheet.dismiss(animated: false, completion: nil)
        }
        
        actionSheet.addAction(cancelAction)
        delegate?.show(alert: actionSheet)
    }
    
    func showImageDirectly(){
        self.showImagePicker()
    }
    
    func dismissImagePicker() {
        self.imagePicker.dismiss(animated: true) {
            self.optionalDelegate?.dismissComplete!()
        }
    }
    
    private func showImagePicker() {
        let status = PHPhotoLibrary.authorizationStatus()
        if (status == PHAuthorizationStatus.authorized) {
            self.presentPhotosImagePicker()
        } else if (status == PHAuthorizationStatus.denied) {
            self.showNoAccessToPhotosAction()
        } else if (status == PHAuthorizationStatus.notDetermined) {
            self.reqeustPhotosAccess {
                let status = PHPhotoLibrary.authorizationStatus()
                if (status == PHAuthorizationStatus.authorized) {
                    self.presentPhotosImagePicker()
                }
            }
        }
    }
    
    private func presentPhotosImagePicker() {
        imagePicker.delegate = self
        imagePicker.sourceType = UIImagePickerControllerSourceType.savedPhotosAlbum
        
        /* Toggle Crop View */
        if self.allowCropping {
            imagePicker.allowsEditing = true
        } else {
            imagePicker.allowsEditing = false
        }
        
        delegate?.present(imagePicker: imagePicker)
    }
    
    private func showCamera() {
        if AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeVideo) == AVAuthorizationStatus.denied {
            self.showNoAccessToCameraAction()
        } else if AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeVideo) == AVAuthorizationStatus.authorized {
            self.presentCameraImagePicker()
        } else if AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeVideo) == AVAuthorizationStatus.notDetermined {
            self.reqeustCameraAccess {
                self.presentCameraImagePicker()
            }
        }
    }
    
    private func presentCameraImagePicker() {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
            imagePicker.allowsEditing = true
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.cameraCaptureMode = .photo
            imagePicker.modalPresentationStyle = .fullScreen
            imagePicker.delegate = self
            delegate?.present(imagePicker: imagePicker)
        }
    }
    
    func showNoAccessToPhotosAction() {
        self.showNoAccessAlertController(title: VhcStrings.Proof.CannotAccessLibraryAlertTitle172, message: VhcStrings.Proof.CannotAccessLibraryAlertMessage173)
    }
    
    func showNoAccessToCameraAction() {
        self.showNoAccessAlertController(title: VhcStrings.Proof.CannotAccessCameraAlertTitle175, message: VhcStrings.Proof.CannotAccessCameraAlertMessage176)
    }
    
    func showNoAccessAlertController(title: String, message: String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        let cancelAction = UIAlertAction(title: CommonStrings.CancelButtonTitle24, style: UIAlertActionStyle.cancel) { (UIAlertAction) in
            alertController.dismiss(animated: true, completion: nil)
        }
        
        let settingsAction = UIAlertAction(title: CommonStrings.GenericSettingsButtonTitle271, style: .default) { (_) -> Void in
            let settingsUrl = URL(string: UIApplicationOpenSettingsURLString)
            if let url = settingsUrl {
                UIApplication.shared.open(url, completionHandler: nil)
            }
        }
        alertController.addAction(settingsAction)
        
        alertController.addAction(cancelAction)
        delegate?.show(alert: alertController)
    }
    
    internal func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        /* Let's check first if media URL is present. This means the media is from Library. Else, it is coming from the camera. */        
        guard let url = info[UIImagePickerControllerReferenceURL] as? URL else {
            checkImageFromCamera(info: info)
            return
        }
        checkImageFromLibrary(url: url, info: info)
    }
    
    private func checkImageFromCamera(info: [String : Any]){
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage{
            /* Convert image into Data */
            let imageData = UIImageJPEGRepresentation(image, 1.0)
            self.validateImage(imageData: imageData, info: info)
        }else{
            showUnknownErrorOccured()
        }
    }
    
    private func checkImageFromLibrary(url: URL, info: [String : Any]){
        let result = PHAsset.fetchAssets(withALAssetURLs: [url], options: nil)
        guard let asset = result.firstObject else {
            showUnknownErrorOccured()
            return
        }
        let manager = PHImageManager.default()
        manager.requestImageData(for: asset, options: nil, resultHandler: { [weak self]
            (imageData, dataUTI, orientation, imageInfo) in
            self?.validateImage(imageData: imageData, info: info)
        })
    }
    
    private func validateImage(imageData: Data?, info: [String : Any]){
        if let limitFileSizeForUpload = self.configuration?.shouldLimitFileSize, limitFileSizeForUpload{
            self.checkFileSize(imageData: imageData, info: info)
        }else{
            self.delegate?.pass(imageInfo: info)
        }
    }
    
    
    private func checkFileSize(imageData: Data?, info: [String : Any]){
        if isFileSizeValid(imageData){
            self.delegate?.pass(imageInfo: info)
        }else{
            self.dismissImagePicker()
            self.showUploadLimitError()
        }
    }
    
    private func isFileSizeValid(_ imageData: Data?) -> Bool{
        if let maxSizeValue = self.configuration?.maxFileSize, let imageSize = imageData?.count{ //Get bytes size of image
            return imageSize <= maxSizeValue
        }
        return false
    }
    
    private func showUnknownErrorOccured(){
        let alert = UIAlertController(title: CommonStrings.AlertUnknownMessage267,
                                      message: nil, preferredStyle: UIAlertControllerStyle.alert)
        self.dismissImagePicker()
        alert.addAction(UIAlertAction(title: CommonStrings.OkButtonTitle40,
                                      style: .default, handler: nil))
        self.delegate?.show(alert: alert)
    }
    
    private func showUploadLimitError(){
        let controller = UIAlertController(title: CommonStrings.AlertErrorTitle268,
                                           message: CommonStrings.Common.DialogUploadLimitErrorMessage9999,
                                           preferredStyle: .alert)
        controller.addAction(UIAlertAction(title: CommonStrings.OkButtonTitle40, style: .cancel, handler: nil))
        self.delegate?.show(alert: controller)
    }
    
    public func reqeustPhotosAccess(_ completion: @escaping () -> Void) {
        PHPhotoLibrary.requestAuthorization { (status) in
            switch (status) {
            case .authorized:
                completion()
                break
            case .denied:
                break
            case .restricted:
                break
            case .notDetermined:
                break
            }
        }
    }
    
    public func reqeustCameraAccess(_ completion: @escaping () -> Void) {
        AVCaptureDevice.requestAccess(forMediaType: AVMediaTypeVideo) { (granted) in
            if (granted == true) {
                completion()
            } else {
                
            }
        }
    }
}
