//
//  ImageCollection.swift
//  VitalityActive
//
//  Created by admin on 2017/05/16.
//  Copyright © 2017 Glucode. All rights reserved.
//

import UIKit
import VIAUIKit

class ImageCollection: UICollectionView, UICollectionViewDataSource, Nibloadable {
    public var imagesAssets: Array<UIImage>?

    override func awakeFromNib() {
        super.awakeFromNib()
        self.dataSource = self
    }

    public func setImageCollectionItemWidth(with viewWidth: CGFloat) {
        if let collectionFlowLayout = self.collectionViewLayout as? UICollectionViewFlowLayout {
            let leftAndRightSpacingTotal: CGFloat = 20
            let cellPaddingTotal: CGFloat = 10
            let totalWidthPadding: CGFloat = leftAndRightSpacingTotal + cellPaddingTotal
            let itemWidth = (viewWidth - totalWidthPadding) / 2
            collectionFlowLayout.itemSize = CGSize(width: itemWidth,
                                                   height: itemWidth)
            collectionFlowLayout.minimumLineSpacing = 10
        }
    }

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imagesAssets?.count ?? 0
    }

    // The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let dequeuedCell = self.dequeueReusableCell(withReuseIdentifier: ImageCollectionViewCell.defaultReuseIdentifier, for: indexPath)
        if let imageCell = dequeuedCell as? ImageCollectionViewCell {
            if ((self.imagesAssets?.count ?? 0) > indexPath.item) {
                if let imageAsset = self.imagesAssets?[indexPath.item] {
                    imageCell.imageView.image = imageAsset
                    imageCell.imageView.contentMode = .scaleAspectFill
                }
            }

            return imageCell
        }
        return dequeuedCell
    }
}
