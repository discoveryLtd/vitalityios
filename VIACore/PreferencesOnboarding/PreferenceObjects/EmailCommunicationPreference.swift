import VIAUtilities
import Foundation
import VitalityKit
import VitalityKit


open class EmailCommunicationPreference: PreferenceDataObject {
    weak var delegate: PreferenceObjectDelegate?
    let realm = DataProvider.newRealm()

    override init() {
        super.init()
        self.preferenceImage = UIImage.templateImage(asset: CoreAsset.setupEmail)
        self.preferenceTitle = CommonStrings.EmailFieldPlaceholder18
        let currentEmail = DataProvider.newRealm().currentEmailAddress()?.value ?? ""
        self.preferencesDetail = CommonStrings.Settings.EmailCommunication985(currentEmail)
        self.preferencesSwitchAction = self.toggleEmailPreference    }

    open func setDelegate(preferenceDelegate: PreferenceObjectDelegate) {
        self.delegate = preferenceDelegate
    }

    override func preferenceInitialState() -> Bool {
        let type = PreferenceTypeRef.EmailCommPref.rawValue
        if let emailPreference = realm.partyGeneralPreference(with: type) {
            if emailPreference.value == "true" {
                self.preferenceState = true
                return self.preferenceState
            } else if Int(emailPreference.value) == 0 {
                self.preferenceState = true
                return self.preferenceState
            } else {
                self.preferenceState = false
                return self.preferenceState
            }
        }
        self.preferenceState = false
        return self.preferenceState
    }

    
    func changeEmailPreference(sender: UISwitch?, completion: @escaping (Error?) -> Void) {

        var currentSwitchState = false
        if (sender!.isOn) {
            currentSwitchState = true
        } else if (sender!.isOn == false) {
            currentSwitchState = false
        }
        
        // TODO: should pass effectiveTo: "9999-12-31" since it will add 1 day when it reached the requestBody I decided to used 9998 as the year.
        // NOTE: This is just a temporary fix for now.
        
        //let recommendedFutureDate = Wire.default.yearMonthDayFormatter.date(from: "9999-12-31")
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        let recommendedFutureDate = dateFormatter.date(from: "9999-12-31")
        
        let request = UpdatePartyRequest(partyId: DataProvider.newRealm().getPartyId())
        request.generalPreference = UpdatePartyGeneralPreference(effectiveFrom: Date(), effectiveTo: recommendedFutureDate!, typeKey: PreferenceTypeRef.EmailCommPref, value: String(currentSwitchState))
        
        Wire.Party.update(request: request) { (error) in
            if (error != nil) {
                //Revert switch to previous state
                self.preferenceState = !currentSwitchState
                self.delegate?.setPreferenceState(active: self.preferenceState)
                completion(error)
            } else {
                let realm = DataProvider.newRealm()
                try! realm.write {
                    let type = PreferenceTypeRef.EmailCommPref.rawValue
                    if let emailPreferences = realm.partyGeneralPreference(with: type) {
                        realm.delete(emailPreferences)
                    }
                    
                    let emailGeneralPreference = PartyGeneralPreference()
                    emailGeneralPreference.type = type
                    if (currentSwitchState == true) {
                        emailGeneralPreference.value = "true"
                    } else {
                        emailGeneralPreference.value = "false"
                    }
                    realm.add(emailGeneralPreference)
                }
                self.preferenceState = currentSwitchState
                self.delegate?.setPreferenceState(active: self.preferenceState)
                completion(nil)
            }
        }

    }
    
  
    
    func toggleEmailPreference(sender: UISwitch?) {
        if (sender!.isOn) {
            self.setEmailCommunicationPreference(currentSwitchState: true)
        } else if (sender!.isOn == false) {
            self.setEmailCommunicationPreference(currentSwitchState: false)
        }
    }

    
    
    func setEmailCommunicationPreference(currentSwitchState: Bool) {

        let request = UpdatePartyRequest(partyId: DataProvider.newRealm().getPartyId())
        request.generalPreference = UpdatePartyGeneralPreference(effectiveFrom: Date(), effectiveTo: Date.distantFuture, typeKey: PreferenceTypeRef.EmailCommPref, value: String(currentSwitchState))

        Wire.Party.update(request: request) { (error) in
            if (error != nil) {
                //Revert switch to previous state
                self.preferenceState = !currentSwitchState
                self.delegate?.setPreferenceState(active: self.preferenceState)
            } else {
                let realm = DataProvider.newRealm()
                try! realm.write {
                    let type = PreferenceTypeRef.EmailCommPref.rawValue
                    if let emailPreferences = realm.partyGeneralPreference(with: type) {
                        realm.delete(emailPreferences)
                    }

                    let emailGeneralPreference = PartyGeneralPreference()
                    emailGeneralPreference.type = type
                    if (currentSwitchState == true) {
                       emailGeneralPreference.value = "true"
                    } else {
                        emailGeneralPreference.value = "false"
                    }
                    realm.add(emailGeneralPreference)
                }
                self.preferenceState = currentSwitchState
                self.delegate?.setPreferenceState(active: self.preferenceState)
            }
        }
    }
    
    
}
