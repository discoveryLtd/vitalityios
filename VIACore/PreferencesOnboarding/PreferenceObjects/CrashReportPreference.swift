//
//  CrashReportPreference.swift
//  VitalityActive
//
//  Created by Marius Janse van Vuuren on 2017/02/27.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import VitalityKit
import VIACommon
import VitalityKit
import RealmSwift

class CrashReportPreference: PreferenceDataObject {
    let realm = DataProvider.newRealm()
    
    override init() {
        super.init()
        self.preferenceImage = UIImage.templateImage(asset: CoreAsset.setupReports)
        self.preferenceTitle = CommonStrings.UserPrefs.CrashReportsToggleTitle75
        self.preferencesDetail = CommonStrings.UserPrefs.CrashReportsToggleMessage76            
        self.preferencesSwitchAction = self.toggleCrashReportPreference
    }

    override func preferenceInitialState() -> Bool {
        if (AppSettings.getCrashReportPreference() == nil) {
            AppSettings.addCrashReportPreference()
        }

        if let permissionAllowed = AppSettings.getCrashReportPreference() {
            self.preferenceState = permissionAllowed
            return preferenceState
        } else {
            self.preferenceState = false
            return self.preferenceState
        }
    }

    func toggleCrashReportPreference(sender: UISwitch) {
        if (sender.isOn) {
            preferenceState = true
            AppSettings.addCrashReportPreference()
        } else {
            preferenceState = false
            AppSettings.removedCrashReportPreference()
        }
    }
}
