//
//  AnalyticsPreference.swift
//  VitalityActive
//
//  Created by Marius Janse van Vuuren on 2017/02/27.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import VitalityKit
import VIACommon
import VitalityKit
import RealmSwift

class AnalyticsPreference: PreferenceDataObject {
    let realm = DataProvider.newRealm()

    override init() {
        super.init()
        self.preferenceImage = UIImage.templateImage(asset: CoreAsset.setupAnalytics)
        self.preferenceTitle = CommonStrings.UserPrefs.AnalyticsToggleTitle73
        self.preferencesDetail = CommonStrings.UserPrefs.AnalyticsToggleMessage74
        self.preferencesSwitchAction = self.toggleAnalyticsPreference
    }

    override func preferenceInitialState() -> Bool {
        if (AppSettings.getAnalyticsPreference() == nil) {
            AppSettings.addAnalyticsPreference()
        }

        if let permissionAllowed = AppSettings.getAnalyticsPreference() {
            self.preferenceState = permissionAllowed
            return self.preferenceState
        } else {
            self.preferenceState = false
            return self.preferenceState
        }
    }

    func toggleAnalyticsPreference(sender: UISwitch) {
        if (sender.isOn) {
            self.preferenceState = true
            AppSettings.addAnalyticsPreference()
        } else {
            self.preferenceState = false
            AppSettings.removedAnalyticsPreference()
        }
    }
}
