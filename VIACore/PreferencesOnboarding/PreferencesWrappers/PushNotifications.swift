//
//  PushNotifications.swift
//  VitalityActive
//
//  Created by Marius Janse van Vuuren on 2017/02/27.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import UserNotifications

class PushNotifications {
    public class func requestPushNotificationPermission(completion: @escaping (Bool) -> Void) {
        if #available(iOS 10.0, *) {
            let center = UNUserNotificationCenter.current()
            center.delegate = UIApplication.shared.delegate as? UNUserNotificationCenterDelegate
            center.requestAuthorization(options: [.sound, .alert, .badge]) { (granted, error) in
                if error == nil {
                    UIApplication.shared.registerForRemoteNotifications()
                } else {
                    debugPrint("Could not register for notifications")
                }
                completion(granted)
            }
        } else {
            UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.sound, .alert, .badge], categories: nil))
                UIApplication.shared.registerForRemoteNotifications()
        }
    }

}
