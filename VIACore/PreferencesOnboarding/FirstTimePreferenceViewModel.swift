//
//  FirstTimePreferenceViewModel.swift
//  VitalityActive
//
//  Created by Marius Janse van Vuuren on 2017/02/15.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation

open class FirstTimePreferenceViewModel {

    let onboardingDataSource: FirstTimePreferenceDataSource

    public required init() {
        self.onboardingDataSource = FirstTimePreferenceDataSource()
    }

    func getNumberOfCategories() -> Int {
        return self.onboardingDataSource.preferences.count
    }

    func getNumberOfPreferences(in category: Int) -> Int {
        return self.onboardingDataSource.preferences[category].count
    }

    func getPrefernceItemFromDataSource(in category: Int, preference: Int) -> Any? {
        if (self.onboardingDataSource.preferences.count >= category) {
            if (self.onboardingDataSource.preferences[category].count >= preference) {
                return self.onboardingDataSource.preferences[category][preference]
            }
        }

        return nil
    }

    func areAllPreferencesSwitchedOff() -> Bool {
        for category in self.onboardingDataSource.preferences {
            for preference in category {
                if (preference is PreferenceDataObject) {
                    if ((preference as! PreferenceDataObject).preferenceState == true) {
                        return false
                    }
                }
            }
        }
        return true
    }
    
    func areAllCommunicationPreferencesSwitchedOff() -> Bool {
        let category = self.onboardingDataSource.preferences.first
        for preference in category! {
            if (preference is PreferenceDataObject) {
                if ((preference as! PreferenceDataObject).preferenceState == true) {
                    return false
                }
            }
        }
        return true
    }
    
    open func setDetailText(detailText: String, detailLabel: UILabel, switchButton: UISwitch) {
        detailLabel.text = detailText
    }
}
