// Generated using SwiftGen, by O.Halligon — https://github.com/SwiftGen/SwiftGen

#if os(OSX)
import AppKit.NSImage
public typealias VIACoreColor = NSColor
public typealias VIACoreImage = NSImage
#elseif os(iOS) || os(tvOS) || os(watchOS)
import UIKit.UIImage
public typealias VIACoreColor = UIColor
public typealias VIACoreImage = UIImage
#endif

// swiftlint:disable file_length

public typealias VIACoreAssetType = VIACoreImageAsset

public struct VIACoreImageAsset: Equatable {
	fileprivate var name: String

	public var templateImage: VIACoreImage {
		let bundle = Bundle(for: BundleToken.self)
		#if os(iOS) || os(tvOS)
		let image = VIACoreImage(named: name, in: bundle, compatibleWith: nil)?.withRenderingMode(.alwaysTemplate)
		#elseif os(OSX)
		let image = bundle.image(forResource: name).withRenderingMode(.alwaysTemplate)
		#elseif os(watchOS)
		let image = VIACoreImage(named: name).withRenderingMode(.alwaysTemplate)
		#endif
		guard let result = image else { fatalError("Unable to load template image named \(name).") }
		return result
	}

	public var image: VIACoreImage {
		let bundle = Bundle(for: BundleToken.self)
		#if os(iOS) || os(tvOS)
		let image = VIACoreImage(named: name, in: bundle, compatibleWith: nil)
		#elseif os(OSX)
		let image = bundle.image(forResource: name)
		#elseif os(watchOS)
		let image = VIACoreImage(named: name)
		#endif
		guard let result = image else { fatalError("Unable to load image named \(name).") }
		return result
	}

	public static func ==(lhs: VIACoreImageAsset, rhs: VIACoreImageAsset) -> Bool {
        return lhs.image == rhs.image
    }
}

public struct VIACoreColorAsset {
fileprivate var name: String

#if swift(>=3.2)
@available(iOS 11.0, tvOS 11.0, watchOS 4.0, OSX 10.13, *)
var color: VIACoreColor {
return VIACoreColor(asset: self)
}
#endif
}

// swiftlint:disable identifier_name line_length nesting type_body_length type_name
public enum VIACoreAsset {
  public enum Privacy {
    public static let vitalityStatus = VIACoreImageAsset(name: "vitalityStatus")
  }
  public enum Sav {
    public static let savScreenings = VIACoreImageAsset(name: "savScreenings")
    public static let savHistory = VIACoreImageAsset(name: "savHistory")
    public static let savHealthcareBig = VIACoreImageAsset(name: "savHealthcareBig")
    public static let samplePartner1 = VIACoreImageAsset(name: "samplePartner1")
    public static let savLearnMore = VIACoreImageAsset(name: "savLearnMore")
    public static let savCompletedBig = VIACoreImageAsset(name: "savCompletedBig")
    public static let savHelp = VIACoreImageAsset(name: "savHelp")
    public static let savCholesterolSmall = VIACoreImageAsset(name: "savCholesterolSmall")
    public static let savVaccinationsLogo = VIACoreImageAsset(name: "savVaccinationsLogo")
    public static let savGeneralDocsSmall = VIACoreImageAsset(name: "savGeneralDocsSmall")
    public static let samplePartner3 = VIACoreImageAsset(name: "samplePartner3")
    public static let samplePartner2 = VIACoreImageAsset(name: "samplePartner2")
    public static let savEarnPointsBig = VIACoreImageAsset(name: "savEarnPointsBig")
    public static let checkMarkSmall = VIACoreImageAsset(name: "checkMarkSmall")
    public static let savPointsInfo = VIACoreImageAsset(name: "savPointsInfo")
    public static let savCheckMarkInactive = VIACoreImageAsset(name: "savCheckMarkInactive")
    public static let savVaccinations = VIACoreImageAsset(name: "savVaccinations")
    public static let pointsSmall = VIACoreImageAsset(name: "pointsSmall")
    public static let doctorSmall = VIACoreImageAsset(name: "doctorSmall")
  }
  public enum Goals {
    public static let statusInProgress = VIACoreImageAsset(name: "statusInProgress")
    public static let statusPending = VIACoreImageAsset(name: "statusPending")
    public static let statusNotAchieved = VIACoreImageAsset(name: "statusNotAchieved")
    public static let arrowDrill = VIACoreImageAsset(name: "arrowDrill")
    public static let statusAchieved = VIACoreImageAsset(name: "statusAchieved")
  }
  public enum HomeScreenCards {
    public static let hsCardPinkBlock = VIACoreImageAsset(name: "hsCardPinkBlock")
    public static let employerReward = VIACoreImageAsset(name: "employerReward")
    public static let hsVHCBig = VIACoreImageAsset(name: "hsVHCBig")
    public static let hsCardSAVBig = VIACoreImageAsset(name: "hsCardSAVBig")
    public static let hsCardExclamationBig = VIACoreImageAsset(name: "hsCardExclamationBig")
    public static let hsCardAwaitingShipmentBig = VIACoreImageAsset(name: "hsCardAwaitingShipmentBig")
    public static let hsNuffieldHealthLarge = VIACoreImageAsset(name: "hsNuffieldHealthLarge")
    public static let hsCardGreenBlock = VIACoreImageAsset(name: "hsCardGreenBlock")
    public static let hsCardLinkDevicesBig = VIACoreImageAsset(name: "hsCardLinkDevicesBig")
    public static let hsOFEBig = VIACoreImageAsset(name: "hsOFEBig")
    public static let hsCardCheckmarkBig = VIACoreImageAsset(name: "hsCardCheckmarkBig")
    public static let nuffieldMainLogo = VIACoreImageAsset(name: "nuffieldMainLogo")
    public static let hsCardLockedBig = VIACoreImageAsset(name: "hsCardLockedBig")
    public static let nuffieldNewMainLogo = VIACoreImageAsset(name: "nuffieldNewMainLogo")
    public static let hsPolicyCashbackBig = VIACoreImageAsset(name: "hsPolicyCashbackBig")
    public static let hsActivationBarcode = VIACoreImageAsset(name: "hsActivationBarcode")
    public static let hsCardOrangeBlock = VIACoreImageAsset(name: "hsCardOrangeBlock")
    public static let hsCardNonSmokersBig = VIACoreImageAsset(name: "hsCardNonSmokersBig")
    public static let hsCardHealthServicesBig = VIACoreImageAsset(name: "hsCardHealthServicesBig")
    public enum Headings {
      public static let policyCashback = VIACoreImageAsset(name: "policyCashback")
      public static let activeRewards = VIACoreImageAsset(name: "activeRewards")
      public static let rewardPartners = VIACoreImageAsset(name: "rewardPartners")
      public static let partnersRewards = VIACoreImageAsset(name: "partnersRewards")
      public static let mwb = VIACoreImageAsset(name: "mwb")
      public static let events = VIACoreImageAsset(name: "events")
      public static let nuffieldLogo = VIACoreImageAsset(name: "nuffieldLogo")
      public static let partnersWellness = VIACoreImageAsset(name: "partnersWellness")
      public static let employerServices = VIACoreImageAsset(name: "employerServices")
      public static let deviceCashback = VIACoreImageAsset(name: "deviceCashback")
      public static let vhc = VIACoreImageAsset(name: "vhc")
      public static let nutrition = VIACoreImageAsset(name: "nutrition")
      public static let linkDevices = VIACoreImageAsset(name: "linkDevices")
      public static let vhr = VIACoreImageAsset(name: "vhr")
      public static let rewards = VIACoreImageAsset(name: "rewards")
      public static let nonSmokers = VIACoreImageAsset(name: "nonSmokers")
      public static let partnersHealth = VIACoreImageAsset(name: "partnersHealth")
      public static let screeningsVac = VIACoreImageAsset(name: "screeningsVac")
      public static let appleWatch = VIACoreImageAsset(name: "appleWatch")
      public static let activationBarcode = VIACoreImageAsset(name: "activationBarcode")
    }
    public static let hsPartnerRewardsWellnessBig = VIACoreImageAsset(name: "hsPartnerRewardsWellnessBig")
    public static let hsPartnerRewardsGreatBig = VIACoreImageAsset(name: "hsPartnerRewardsGreatBig")
    public static let hsCardWatchBig = VIACoreImageAsset(name: "hsCardWatchBig")
    public static let hsCardBlueBlock = VIACoreImageAsset(name: "hsCardBlueBlock")
    public static let hsCardCalendarBig = VIACoreImageAsset(name: "hsCardCalendarBig")
    public static let hsCardGoalAchievedBig = VIACoreImageAsset(name: "hsCardGoalAchievedBig")
    public static let yourEmployerServices = VIACoreImageAsset(name: "yourEmployerServices")
    public static let hsCardRewardAchievedMain = VIACoreImageAsset(name: "hsCardRewardAchievedMain")
    public static let hsCardPaymentCompleteBig = VIACoreImageAsset(name: "hsCardPaymentCompleteBig")
    public static let homeCardAWCPending = VIACoreImageAsset(name: "homeCardAWCPending")
    public static let hsPartnerRewardsHealthBig = VIACoreImageAsset(name: "hsPartnerRewardsHealthBig")
    public static let hsCardRewardsPaceholderBig = VIACoreImageAsset(name: "hsCardRewardsPaceholderBig")
  }
  public enum LoginRestriction {
    public static let membershipInactive = VIACoreImageAsset(name: "membershipInactive")
  }
  public enum NonSmokersDeclaration {
    public static let nsdLearnMore3 = VIACoreImageAsset(name: "nsdLearnMore_3")
    public static let nsdLearnMore2 = VIACoreImageAsset(name: "nsdLearnMore_2")
    public static let cardActiveRewards = VIACoreImageAsset(name: "cardActiveRewards")
    public static let smokingIconOnboarding = VIACoreImageAsset(name: "smokingIconOnboarding")
    public static let nsdLearnMore1 = VIACoreImageAsset(name: "nsdLearnMore_1")
    public static let nonsmokersNoimagePartner = VIACoreImageAsset(name: "nonsmokersNoimagePartner")
    public static let pointsIconOnboarding = VIACoreImageAsset(name: "pointsIconOnboarding")
  }
  public enum TabBarIcons {
    public static let tabBarHealthInactive = VIACoreImageAsset(name: "tabBarHealthInactive")
    public static let tabBarProfileActive = VIACoreImageAsset(name: "tabBarProfileActive")
    public static let tabBarPointsInactive = VIACoreImageAsset(name: "tabBarPointsInactive")
    public static let tabBarHomeInactive = VIACoreImageAsset(name: "tabBarHomeInactive")
    public static let mybenefits = VIACoreImageAsset(name: "mybenefits")
    public static let tabBarHelpActive = VIACoreImageAsset(name: "tabBarHelpActive")
    public static let tabBarProfileInactive = VIACoreImageAsset(name: "tabBarProfileInactive")
    public static let tabBarPointsActive = VIACoreImageAsset(name: "tabBarPointsActive")
    public static let tabBarHomeActive = VIACoreImageAsset(name: "tabBarHomeActive")
    public static let mybenefitsInactive = VIACoreImageAsset(name: "mybenefitsInactive")
    public static let tabBarHealthActive = VIACoreImageAsset(name: "tabBarHealthActive")
    public static let tabBarHelpInactive = VIACoreImageAsset(name: "tabBarHelpInactive")
  }
  public enum LoginAndRegistration {
    public static let loginConfirmPassword = VIACoreImageAsset(name: "loginConfirmPassword")
    public static let loginEmail = VIACoreImageAsset(name: "loginEmail")
    public static let loginPassword = VIACoreImageAsset(name: "loginPassword")
    public static let loginRegistrationCode = VIACoreImageAsset(name: "loginRegistrationCode")
    public static let loginBirth = VIACoreImageAsset(name: "loginBirth")
  }
  public enum Vhr {
    public static let vhrOnboardingCompleteTime = VIACoreImageAsset(name: "vhrOnboardingCompleteTime")
    public static let vhrOnboardingVitalityAge = VIACoreImageAsset(name: "vhrOnboardingVitalityAge")
    public static let vhrCheckMarkActive = VIACoreImageAsset(name: "vhrCheckMarkActive")
    public static let vhrCheckMarkSingle = VIACoreImageAsset(name: "vhrCheckMarkSingle")
    public enum SectionIcons {
      public static let vhrSectionFamilyMedical = VIACoreImageAsset(name: "vhrSectionFamilyMedical")
      public static let vhrSectionSmoking = VIACoreImageAsset(name: "vhrSectionSmoking")
      public static let vhrSectionOverall = VIACoreImageAsset(name: "vhrSectionOverall")
      public static let vhrSectionPhysicalActivity = VIACoreImageAsset(name: "vhrSectionPhysicalActivity")
      public static let vhrSectionMedicalHistory = VIACoreImageAsset(name: "vhrSectionMedicalHistory")
      public static let vhrSectionAlcoholIntake = VIACoreImageAsset(name: "vhrSectionAlcoholIntake")
      public static let vhrSectionEatingHabits = VIACoreImageAsset(name: "vhrSectionEatingHabits")
      public static let vhrSectionSleepingPatterns = VIACoreImageAsset(name: "vhrSectionSleepingPatterns")
      public static let vhrSectionWellbeing = VIACoreImageAsset(name: "vhrSectionWellbeing")
    }
    public static let vhrLearnMorePoints = VIACoreImageAsset(name: "vhrLearnMorePoints")
    public static let vhrBackArrow = VIACoreImageAsset(name: "vhrBackArrow")
    public static let vhrLearnCompleteTime = VIACoreImageAsset(name: "vhrLearnCompleteTime")
    public static let vhrLearnVitalityAge = VIACoreImageAsset(name: "vhrLearnVitalityAge")
    public static let vhrGenericLearnMore = VIACoreImageAsset(name: "vhrGenericLearnMore")
    public static let vhrCheckMarkInactive = VIACoreImageAsset(name: "vhrCheckMarkInactive")
    public static let vhrOnboardingEarnPoints = VIACoreImageAsset(name: "vhrOnboardingEarnPoints")
  }
  public enum PreferencesOnboarding {
    public static let setupNotifications = VIACoreImageAsset(name: "setupNotifications")
    public static let setupEmail = VIACoreImageAsset(name: "setupEmail")
    public static let touchIdIcon = VIACoreImageAsset(name: "touchIdIcon")
    public static let setupRememberMe = VIACoreImageAsset(name: "setupRememberMe")
    public static let setupTouchID = VIACoreImageAsset(name: "setupTouchID")
    public static let setupAnalytics = VIACoreImageAsset(name: "setupAnalytics")
    public static let setupReports = VIACoreImageAsset(name: "setupReports")
  }
  public enum Feedback {
    public static let feedbackEmail = VIACoreImageAsset(name: "feedbackEmail")
    public static let feedbackSubmitted = VIACoreImageAsset(name: "feedbackSubmitted")
  }
  public enum Profile {
    public static let profileAnalyticsSmall = VIACoreImageAsset(name: "profileAnalyticsSmall")
    public static let profileSettings = VIACoreImageAsset(name: "profileSettings")
    public static let profileSecurityChangePassword = VIACoreImageAsset(name: "profileSecurityChangePassword")
    public static let profileSecurity = VIACoreImageAsset(name: "profileSecurity")
    public static let profileMembershipPass = VIACoreImageAsset(name: "profileMembershipPass")
    public static let entityNumberSmall = VIACoreImageAsset(name: "entityNumberSmall")
    public static let profileCommunicationPreferences = VIACoreImageAsset(name: "profileCommunicationPreferences")
    public static let profilePrivacy = VIACoreImageAsset(name: "profilePrivacy")
    public static let profileEventsFeed = VIACoreImageAsset(name: "profileEventsFeed")
    public static let profileHelp = VIACoreImageAsset(name: "profileHelp")
    public static let profileTermsAndCondition = VIACoreImageAsset(name: "profileTermsAndCondition")
    public static let profileMembershipLogo = VIACoreImageAsset(name: "profileMembershipLogo")
    public static let profileLogout = VIACoreImageAsset(name: "profileLogout")
    public static let profileRateUs = VIACoreImageAsset(name: "profileRateUs")
    public static let profileSetupReports = VIACoreImageAsset(name: "profileSetupReports")
    public static let profileUnitPreferences = VIACoreImageAsset(name: "profileUnitPreferences")
    public static let emailSmall = VIACoreImageAsset(name: "emailSmall")
    public static let profileProvideFeedback = VIACoreImageAsset(name: "profileProvideFeedback")
    public static let carrierBrandingAreaGray = VIACoreImageAsset(name: "carrierBrandingAreaGray")
  }
  public enum Partners {
    public static let partnersPlaceholder = VIACoreImageAsset(name: "partnersPlaceholder")
    public static let partnersGetStarted = VIACoreImageAsset(name: "partnersGetStarted")
  }
  public enum ActiveRewards {
    public static let activeRewardsTerms = VIACoreImageAsset(name: "ActiveRewardsTerms")
    public enum Rewards {
      public static let noActiveReward = VIACoreImageAsset(name: "noActiveReward")
      public static let rewardTrophyBig = VIACoreImageAsset(name: "rewardTrophyBig")
      public static let bookMyShow = VIACoreImageAsset(name: "book_my_show")
      public static let activeReward = VIACoreImageAsset(name: "activeReward")
      public static let spotify = VIACoreImageAsset(name: "spotify")
      public static let amazonUs = VIACoreImageAsset(name: "amazon_us")
      public static let amazonDe = VIACoreImageAsset(name: "amazon_de")
      public static let partnerStarbucks = VIACoreImageAsset(name: "partnerStarbucks")
      public static let amazonUk = VIACoreImageAsset(name: "amazon_uk")
    }
    public static let activeRewardsHelp = VIACoreImageAsset(name: "ActiveRewardsHelp")
    public static let activeRewardsRewards2 = VIACoreImageAsset(name: "ActiveRewardsRewards2")
    public static let activeRewardsLearnMore = VIACoreImageAsset(name: "ActiveRewardsLearnMore")
    public enum LearnMore {
      public static let arLearnMoreReward = VIACoreImageAsset(name: "ARLearnMoreReward")
      public static let arLearnMoreActivate = VIACoreImageAsset(name: "ARLearnMoreActivate")
      public static let arLearnMoreTrophy = VIACoreImageAsset(name: "ARLearnMoreTrophy")
    }
    public static let activeRewardsActivity = VIACoreImageAsset(name: "ActiveRewardsActivity")
    public enum HomeScreen {
      public static let activeRewardsHomeScreenCard = VIACoreImageAsset(name: "ActiveRewardsHomeScreenCard")
      public static let activeRewardsTrophy = VIACoreImageAsset(name: "ActiveRewardsTrophy")
    }
    public static let arRangeCalendar = VIACoreImageAsset(name: "ARRangeCalendar")
    public static let arvhrRequired = VIACoreImageAsset(name: "ARVHRRequired")
    public static let activeRewardsRewards = VIACoreImageAsset(name: "ActiveRewardsRewards")
    public static let arLandingCalendarBig = VIACoreImageAsset(name: "ARLandingCalendarBig")
    public enum Onboarding {
      public static let arOnboardingSpinner = VIACoreImageAsset(name: "AROnboardingSpinner")
      public static let arOnboardingCalendar = VIACoreImageAsset(name: "AROnboardingCalendar")
      public static let arOnboardingRings = VIACoreImageAsset(name: "AROnboardingRings")
      public static let arOnboardingReward = VIACoreImageAsset(name: "AROnboardingReward")
      public static let arOnboardingTrophy = VIACoreImageAsset(name: "AROnboardingTrophy")
      public static let arOnboardingActivated = VIACoreImageAsset(name: "AROnboardingActivated")
    }
  }
  public enum DeviceCashback {
    public static let homeCardOrangeCoin = VIACoreImageAsset(name: "homeCardOrangeCoin")
    public static let homeCardActivated = VIACoreImageAsset(name: "homeCardActivated")
    public static let homeCardAwaitingShipment = VIACoreImageAsset(name: "homeCardAwaitingShipment")
    public static let homeCardGetStarted = VIACoreImageAsset(name: "homeCardGetStarted")
    public static let homeCardBlueCoin = VIACoreImageAsset(name: "homeCardBlueCoin")
    public static let homeCardGreenCoin = VIACoreImageAsset(name: "homeCardGreenCoin")
    public static let homeCardPinkCoin = VIACoreImageAsset(name: "homeCardPinkCoin")
  }
  public enum Generic {
    public static let helpGeneric = VIACoreImageAsset(name: "helpGeneric")
  }
  public enum EventFeeds {
    public static let heathDataCategoryLogo = VIACoreImageAsset(name: "heathDataCategoryLogo")
    public static let screeningCategoryLogo = VIACoreImageAsset(name: "screeningCategoryLogo")
    public static let allCategoryLogo = VIACoreImageAsset(name: "allCategoryLogo")
    public static let newOtherCategoryLogo = VIACoreImageAsset(name: "newOtherCategoryLogo")
    public static let legalCategoryLogo = VIACoreImageAsset(name: "legalCategoryLogo")
    public static let deviceCategoryLogo = VIACoreImageAsset(name: "deviceCategoryLogo")
    public static let financialsCategoryLogo = VIACoreImageAsset(name: "financialsCategoryLogo")
    public static let nutritionCategoryLogo = VIACoreImageAsset(name: "nutritionCategoryLogo")
    public static let assessmentsCategoryLogo = VIACoreImageAsset(name: "assessmentsCategoryLogo")
    public static let statusCategoryLogo = VIACoreImageAsset(name: "statusCategoryLogo")
    public static let rewardCategoryLogo = VIACoreImageAsset(name: "rewardCategoryLogo")
    public static let otherCategoryLogo = VIACoreImageAsset(name: "otherCategoryLogo")
    public static let profileManagementCategoryLogo = VIACoreImageAsset(name: "profileManagementCategoryLogo")
    public static let activationCategoryLogo = VIACoreImageAsset(name: "activationCategoryLogo")
    public static let getActiveCategoryLogo = VIACoreImageAsset(name: "getActiveCategoryLogo")
  }
  public enum FirstTimeOnboardingUKE {
    public static let onboardingTrackActivity = VIACoreImageAsset(name: "onboardingTrackActivity")
    public static let onboardingExplore = VIACoreImageAsset(name: "onboardingExplore")
    public static let onboardingHeathReview = VIACoreImageAsset(name: "onboardingHeathReview")
    public static let onboardingRewards = VIACoreImageAsset(name: "onboardingRewards")
  }
  public enum NavBar {
    public static let greyPixel = VIACoreImageAsset(name: "GreyPixel")
    public static let transparentPixel = VIACoreImageAsset(name: "TransparentPixel")
    public static let pixel = VIACoreImageAsset(name: "Pixel")
  }
  public enum FirstTimeOnboarding {
    public static let firstTimeOnboarding01 = VIACoreImageAsset(name: "firstTimeOnboarding01")
    public static let firstTimeOnboarding02 = VIACoreImageAsset(name: "firstTimeOnboarding02")
    public static let firstTimeOnboarding03 = VIACoreImageAsset(name: "firstTimeOnboarding03")
  }
  public enum WellnessDevicesAndApps {
    public static let wellnessDevicesPoints = VIACoreImageAsset(name: "wellnessDevicesPoints")
    public static let wellnessDevicesHeartRate = VIACoreImageAsset(name: "wellnessDevicesHeartRate")
    public static let wellnessDevicesLearnMoreMed = VIACoreImageAsset(name: "wellnessDevicesLearnMoreMed")
    public static let wellnessDevicesLearnMore = VIACoreImageAsset(name: "wellnessDevicesLearnMore")
    public static let wellnessDevicesWeight = VIACoreImageAsset(name: "wellnessDevicesWeight")
    public static let wellnessDevicesHeight = VIACoreImageAsset(name: "wellnessDevicesHeight")
    public static let wellnessDevicesRings = VIACoreImageAsset(name: "wellnessDevicesRings")
    public static let wellnessDevicesSleep = VIACoreImageAsset(name: "wellnessDevicesSleep")
    public static let wellnessDevicesHelp = VIACoreImageAsset(name: "wellnessDevicesHelp")
    public static let wellnessDevicesDistance = VIACoreImageAsset(name: "wellnessDevicesDistance")
    public static let wellnessDevicesSpeed = VIACoreImageAsset(name: "wellnessDevicesSpeed")
    public static let wellnessDevicesSteps = VIACoreImageAsset(name: "wellnessDevicesSteps")
    public static let wellnessDevicesLearnMorePoints = VIACoreImageAsset(name: "wellnessDevicesLearnMorePoints")
    public static let wellnessDevicesCalories = VIACoreImageAsset(name: "wellnessDevicesCalories")
    public static let wellnessDevicesLink = VIACoreImageAsset(name: "wellnessDevicesLink")
    public static let healthKitOnboardingHealthAccess = VIACoreImageAsset(name: "healthKitOnboardingHealthAccess")
  }
  public enum BulkImageUploader {
    public static let addPhoto = VIACoreImageAsset(name: "addPhoto")
    public static let cameraLarge = VIACoreImageAsset(name: "cameraLarge")
    public static let trashSmall = VIACoreImageAsset(name: "trashSmall")
  }
  public enum Vhc {
    public static let vhcGenericInHealthyRangeSmall = VIACoreImageAsset(name: "vhcGenericInHealthyRangeSmall")
    public static let vhcGenericBloodGlucose = VIACoreImageAsset(name: "vhcGenericBloodGlucose")
    public static let vhcGenericWaistCircumference = VIACoreImageAsset(name: "vhcGenericWaistCircumference")
    public static let vhcGenericOutOfHealthyRangeLarge = VIACoreImageAsset(name: "vhcGenericOutOfHealthyRangeLarge")
    public static let vhcHelp = VIACoreImageAsset(name: "vhcHelp")
    public static let vhcGeneralDocsSmall = VIACoreImageAsset(name: "vhcGeneralDocsSmall")
    public static let vhcGenericPointsLarge = VIACoreImageAsset(name: "vhcGenericPointsLarge")
    public static let vhcIcnPointsSmall = VIACoreImageAsset(name: "vhcIcnPointsSmall")
    public static let vhcGenericUrineProtein = VIACoreImageAsset(name: "vhcGenericUrineProtein")
    public static let vhcGenericHealthcare = VIACoreImageAsset(name: "vhcGenericHealthcare")
    public static let vhcCheckMarkSingle = VIACoreImageAsset(name: "vhcCheckMarkSingle")
    public static let vhcGenericBloodPressure = VIACoreImageAsset(name: "vhcGenericBloodPressure")
    public static let vhcGenericHistory = VIACoreImageAsset(name: "vhcGenericHistory")
    public static let vhcGenericBMI = VIACoreImageAsset(name: "vhcGenericBMI")
    public static let vhcEarnPointsBig = VIACoreImageAsset(name: "vhcEarnPointsBig")
    public static let vhcPartnerSoftBank = VIACoreImageAsset(name: "vhcPartnerSoftBank")
    public static let vhcGenericCholesterol = VIACoreImageAsset(name: "vhcGenericCholesterol")
    public static let vhcGenericUrine = VIACoreImageAsset(name: "vhcGenericUrine")
    public static let vhcCompletedBig = VIACoreImageAsset(name: "vhcCompletedBig")
    public static let vhcGenericOutOfHealthyRangeSmall = VIACoreImageAsset(name: "vhcGenericOutOfHealthyRangeSmall")
    public static let vhcGenericHba1c = VIACoreImageAsset(name: "vhcGenericHba1c")
    public static let vhcGenericInHealthyRangeLarge = VIACoreImageAsset(name: "vhcGenericInHealthyRangeLarge")
    public static let vhcIcnTipsSmall = VIACoreImageAsset(name: "vhcIcnTipsSmall")
    public static let vhcGenericQuestionMark = VIACoreImageAsset(name: "vhcGenericQuestionMark")
    public static let vhcGenericLearnMore = VIACoreImageAsset(name: "vhcGenericLearnMore")
    public static let vhcHealthcareBig = VIACoreImageAsset(name: "vhcHealthcareBig")
    public static let vhcGenericMask = VIACoreImageAsset(name: "vhcGenericMask")
    public static let vhcGenericPointsSmall = VIACoreImageAsset(name: "vhcGenericPointsSmall")
  }
  public enum Vna {
    public enum SectionIcons {
      public static let vhrSectionMicronutrientsAndFiber = VIACoreImageAsset(name: "vhrSectionMicronutrientsAndFiber")
    }
  }

  // swiftlint:disable trailing_comma
  public static let allColors: [VIACoreColorAsset] = [
  ]
  public static let allImages: [VIACoreImageAsset] = [
    Privacy.vitalityStatus,
    Sav.savScreenings,
    Sav.savHistory,
    Sav.savHealthcareBig,
    Sav.samplePartner1,
    Sav.savLearnMore,
    Sav.savCompletedBig,
    Sav.savHelp,
    Sav.savCholesterolSmall,
    Sav.savVaccinationsLogo,
    Sav.savGeneralDocsSmall,
    Sav.samplePartner3,
    Sav.samplePartner2,
    Sav.savEarnPointsBig,
    Sav.checkMarkSmall,
    Sav.savPointsInfo,
    Sav.savCheckMarkInactive,
    Sav.savVaccinations,
    Sav.pointsSmall,
    Sav.doctorSmall,
    Goals.statusInProgress,
    Goals.statusPending,
    Goals.statusNotAchieved,
    Goals.arrowDrill,
    Goals.statusAchieved,
    HomeScreenCards.hsCardPinkBlock,
    HomeScreenCards.employerReward,
    HomeScreenCards.hsVHCBig,
    HomeScreenCards.hsCardSAVBig,
    HomeScreenCards.hsCardExclamationBig,
    HomeScreenCards.hsCardAwaitingShipmentBig,
    HomeScreenCards.hsNuffieldHealthLarge,
    HomeScreenCards.hsCardGreenBlock,
    HomeScreenCards.hsCardLinkDevicesBig,
    HomeScreenCards.hsOFEBig,
    HomeScreenCards.hsCardCheckmarkBig,
    HomeScreenCards.nuffieldMainLogo,
    HomeScreenCards.hsCardLockedBig,
    HomeScreenCards.nuffieldNewMainLogo,
    HomeScreenCards.hsPolicyCashbackBig,
    HomeScreenCards.hsActivationBarcode,
    HomeScreenCards.hsCardOrangeBlock,
    HomeScreenCards.hsCardNonSmokersBig,
    HomeScreenCards.hsCardHealthServicesBig,
    HomeScreenCards.Headings.policyCashback,
    HomeScreenCards.Headings.activeRewards,
    HomeScreenCards.Headings.rewardPartners,
    HomeScreenCards.Headings.partnersRewards,
    HomeScreenCards.Headings.mwb,
    HomeScreenCards.Headings.events,
    HomeScreenCards.Headings.nuffieldLogo,
    HomeScreenCards.Headings.partnersWellness,
    HomeScreenCards.Headings.employerServices,
    HomeScreenCards.Headings.deviceCashback,
    HomeScreenCards.Headings.vhc,
    HomeScreenCards.Headings.nutrition,
    HomeScreenCards.Headings.linkDevices,
    HomeScreenCards.Headings.vhr,
    HomeScreenCards.Headings.rewards,
    HomeScreenCards.Headings.nonSmokers,
    HomeScreenCards.Headings.partnersHealth,
    HomeScreenCards.Headings.screeningsVac,
    HomeScreenCards.Headings.appleWatch,
    HomeScreenCards.Headings.activationBarcode,
    HomeScreenCards.hsPartnerRewardsWellnessBig,
    HomeScreenCards.hsPartnerRewardsGreatBig,
    HomeScreenCards.hsCardWatchBig,
    HomeScreenCards.hsCardBlueBlock,
    HomeScreenCards.hsCardCalendarBig,
    HomeScreenCards.hsCardGoalAchievedBig,
    HomeScreenCards.yourEmployerServices,
    HomeScreenCards.hsCardRewardAchievedMain,
    HomeScreenCards.hsCardPaymentCompleteBig,
    HomeScreenCards.homeCardAWCPending,
    HomeScreenCards.hsPartnerRewardsHealthBig,
    HomeScreenCards.hsCardRewardsPaceholderBig,
    LoginRestriction.membershipInactive,
    NonSmokersDeclaration.nsdLearnMore3,
    NonSmokersDeclaration.nsdLearnMore2,
    NonSmokersDeclaration.cardActiveRewards,
    NonSmokersDeclaration.smokingIconOnboarding,
    NonSmokersDeclaration.nsdLearnMore1,
    NonSmokersDeclaration.nonsmokersNoimagePartner,
    NonSmokersDeclaration.pointsIconOnboarding,
    TabBarIcons.tabBarHealthInactive,
    TabBarIcons.tabBarProfileActive,
    TabBarIcons.tabBarPointsInactive,
    TabBarIcons.tabBarHomeInactive,
    TabBarIcons.mybenefits,
    TabBarIcons.tabBarHelpActive,
    TabBarIcons.tabBarProfileInactive,
    TabBarIcons.tabBarPointsActive,
    TabBarIcons.tabBarHomeActive,
    TabBarIcons.mybenefitsInactive,
    TabBarIcons.tabBarHealthActive,
    TabBarIcons.tabBarHelpInactive,
    LoginAndRegistration.loginConfirmPassword,
    LoginAndRegistration.loginEmail,
    LoginAndRegistration.loginPassword,
    LoginAndRegistration.loginRegistrationCode,
    LoginAndRegistration.loginBirth,
    Vhr.vhrOnboardingCompleteTime,
    Vhr.vhrOnboardingVitalityAge,
    Vhr.vhrCheckMarkActive,
    Vhr.vhrCheckMarkSingle,
    Vhr.SectionIcons.vhrSectionFamilyMedical,
    Vhr.SectionIcons.vhrSectionSmoking,
    Vhr.SectionIcons.vhrSectionOverall,
    Vhr.SectionIcons.vhrSectionPhysicalActivity,
    Vhr.SectionIcons.vhrSectionMedicalHistory,
    Vhr.SectionIcons.vhrSectionAlcoholIntake,
    Vhr.SectionIcons.vhrSectionEatingHabits,
    Vhr.SectionIcons.vhrSectionSleepingPatterns,
    Vhr.SectionIcons.vhrSectionWellbeing,
    Vhr.vhrLearnMorePoints,
    Vhr.vhrBackArrow,
    Vhr.vhrLearnCompleteTime,
    Vhr.vhrLearnVitalityAge,
    Vhr.vhrGenericLearnMore,
    Vhr.vhrCheckMarkInactive,
    Vhr.vhrOnboardingEarnPoints,
    PreferencesOnboarding.setupNotifications,
    PreferencesOnboarding.setupEmail,
    PreferencesOnboarding.touchIdIcon,
    PreferencesOnboarding.setupRememberMe,
    PreferencesOnboarding.setupTouchID,
    PreferencesOnboarding.setupAnalytics,
    PreferencesOnboarding.setupReports,
    Feedback.feedbackEmail,
    Feedback.feedbackSubmitted,
    Profile.profileAnalyticsSmall,
    Profile.profileSettings,
    Profile.profileSecurityChangePassword,
    Profile.profileSecurity,
    Profile.profileMembershipPass,
    Profile.entityNumberSmall,
    Profile.profileCommunicationPreferences,
    Profile.profilePrivacy,
    Profile.profileEventsFeed,
    Profile.profileHelp,
    Profile.profileTermsAndCondition,
    Profile.profileMembershipLogo,
    Profile.profileLogout,
    Profile.profileRateUs,
    Profile.profileSetupReports,
    Profile.profileUnitPreferences,
    Profile.emailSmall,
    Profile.profileProvideFeedback,
    Profile.carrierBrandingAreaGray,
    Partners.partnersPlaceholder,
    Partners.partnersGetStarted,
    ActiveRewards.activeRewardsTerms,
    ActiveRewards.Rewards.noActiveReward,
    ActiveRewards.Rewards.rewardTrophyBig,
    ActiveRewards.Rewards.bookMyShow,
    ActiveRewards.Rewards.activeReward,
    ActiveRewards.Rewards.spotify,
    ActiveRewards.Rewards.amazonUs,
    ActiveRewards.Rewards.amazonDe,
    ActiveRewards.Rewards.partnerStarbucks,
    ActiveRewards.Rewards.amazonUk,
    ActiveRewards.activeRewardsHelp,
    ActiveRewards.activeRewardsRewards2,
    ActiveRewards.activeRewardsLearnMore,
    ActiveRewards.LearnMore.arLearnMoreReward,
    ActiveRewards.LearnMore.arLearnMoreActivate,
    ActiveRewards.LearnMore.arLearnMoreTrophy,
    ActiveRewards.activeRewardsActivity,
    ActiveRewards.HomeScreen.activeRewardsHomeScreenCard,
    ActiveRewards.HomeScreen.activeRewardsTrophy,
    ActiveRewards.arRangeCalendar,
    ActiveRewards.arvhrRequired,
    ActiveRewards.activeRewardsRewards,
    ActiveRewards.arLandingCalendarBig,
    ActiveRewards.Onboarding.arOnboardingSpinner,
    ActiveRewards.Onboarding.arOnboardingCalendar,
    ActiveRewards.Onboarding.arOnboardingRings,
    ActiveRewards.Onboarding.arOnboardingReward,
    ActiveRewards.Onboarding.arOnboardingTrophy,
    ActiveRewards.Onboarding.arOnboardingActivated,
    DeviceCashback.homeCardOrangeCoin,
    DeviceCashback.homeCardActivated,
    DeviceCashback.homeCardAwaitingShipment,
    DeviceCashback.homeCardGetStarted,
    DeviceCashback.homeCardBlueCoin,
    DeviceCashback.homeCardGreenCoin,
    DeviceCashback.homeCardPinkCoin,
    Generic.helpGeneric,
    EventFeeds.heathDataCategoryLogo,
    EventFeeds.screeningCategoryLogo,
    EventFeeds.allCategoryLogo,
    EventFeeds.newOtherCategoryLogo,
    EventFeeds.legalCategoryLogo,
    EventFeeds.deviceCategoryLogo,
    EventFeeds.financialsCategoryLogo,
    EventFeeds.nutritionCategoryLogo,
    EventFeeds.assessmentsCategoryLogo,
    EventFeeds.statusCategoryLogo,
    EventFeeds.rewardCategoryLogo,
    EventFeeds.otherCategoryLogo,
    EventFeeds.profileManagementCategoryLogo,
    EventFeeds.activationCategoryLogo,
    EventFeeds.getActiveCategoryLogo,
    FirstTimeOnboardingUKE.onboardingTrackActivity,
    FirstTimeOnboardingUKE.onboardingExplore,
    FirstTimeOnboardingUKE.onboardingHeathReview,
    FirstTimeOnboardingUKE.onboardingRewards,
    NavBar.greyPixel,
    NavBar.transparentPixel,
    NavBar.pixel,
    FirstTimeOnboarding.firstTimeOnboarding01,
    FirstTimeOnboarding.firstTimeOnboarding02,
    FirstTimeOnboarding.firstTimeOnboarding03,
    WellnessDevicesAndApps.wellnessDevicesPoints,
    WellnessDevicesAndApps.wellnessDevicesHeartRate,
    WellnessDevicesAndApps.wellnessDevicesLearnMoreMed,
    WellnessDevicesAndApps.wellnessDevicesLearnMore,
    WellnessDevicesAndApps.wellnessDevicesWeight,
    WellnessDevicesAndApps.wellnessDevicesHeight,
    WellnessDevicesAndApps.wellnessDevicesRings,
    WellnessDevicesAndApps.wellnessDevicesSleep,
    WellnessDevicesAndApps.wellnessDevicesHelp,
    WellnessDevicesAndApps.wellnessDevicesDistance,
    WellnessDevicesAndApps.wellnessDevicesSpeed,
    WellnessDevicesAndApps.wellnessDevicesSteps,
    WellnessDevicesAndApps.wellnessDevicesLearnMorePoints,
    WellnessDevicesAndApps.wellnessDevicesCalories,
    WellnessDevicesAndApps.wellnessDevicesLink,
    WellnessDevicesAndApps.healthKitOnboardingHealthAccess,
    BulkImageUploader.addPhoto,
    BulkImageUploader.cameraLarge,
    BulkImageUploader.trashSmall,
    Vhc.vhcGenericInHealthyRangeSmall,
    Vhc.vhcGenericBloodGlucose,
    Vhc.vhcGenericWaistCircumference,
    Vhc.vhcGenericOutOfHealthyRangeLarge,
    Vhc.vhcHelp,
    Vhc.vhcGeneralDocsSmall,
    Vhc.vhcGenericPointsLarge,
    Vhc.vhcIcnPointsSmall,
    Vhc.vhcGenericUrineProtein,
    Vhc.vhcGenericHealthcare,
    Vhc.vhcCheckMarkSingle,
    Vhc.vhcGenericBloodPressure,
    Vhc.vhcGenericHistory,
    Vhc.vhcGenericBMI,
    Vhc.vhcEarnPointsBig,
    Vhc.vhcPartnerSoftBank,
    Vhc.vhcGenericCholesterol,
    Vhc.vhcGenericUrine,
    Vhc.vhcCompletedBig,
    Vhc.vhcGenericOutOfHealthyRangeSmall,
    Vhc.vhcGenericHba1c,
    Vhc.vhcGenericInHealthyRangeLarge,
    Vhc.vhcIcnTipsSmall,
    Vhc.vhcGenericQuestionMark,
    Vhc.vhcGenericLearnMore,
    Vhc.vhcHealthcareBig,
    Vhc.vhcGenericMask,
    Vhc.vhcGenericPointsSmall,
    Vna.SectionIcons.vhrSectionMicronutrientsAndFiber,
  ]
  // swiftlint:enable trailing_comma
  @available(*, deprecated, renamed: "allImages")
  public static let allValues: [VIACoreAssetType] = allImages
}
// swiftlint:enable identifier_name line_length nesting type_body_length type_name

extension VIACoreImage {
@available(iOS 1.0, tvOS 1.0, watchOS 1.0, *)
@available(OSX, deprecated,
message: "This initializer is unsafe on macOS, please use the VIACoreImageAsset.image property")
convenience init!(asset: VIACoreAssetType) {
#if os(iOS) || os(tvOS)
let bundle = Bundle(for: BundleToken.self)
self.init(named: asset.name, in: bundle, compatibleWith: nil)
#elseif os(OSX) || os(watchOS)
self.init(named: asset.name)
#endif
}
}

extension VIACoreColor {
#if swift(>=3.2)
@available(iOS 11.0, tvOS 11.0, watchOS 4.0, OSX 10.13, *)
convenience init!(asset: VIACoreColorAsset) {
let bundle = Bundle(for: BundleToken.self)
#if os(iOS) || os(tvOS)
self.init(named: asset.name, in: bundle, compatibleWith: nil)
#elseif os(OSX)
self.init(named: asset.name, bundle: bundle)
#elseif os(watchOS)
self.init(named: asset.name)
#endif
}
#endif
}

private final class BundleToken {}
