// Generated using SwiftGen, by O.Halligon — https://github.com/SwiftGen/SwiftGen

// swiftlint:disable sorted_imports
import Foundation
import UIKit

// swiftlint:disable file_length

protocol StoryboardType {
  static var storyboardName: String { get }
}

extension StoryboardType {
  static var storyboard: UIStoryboard {
    return UIStoryboard(name: self.storyboardName, bundle: Bundle(for: BundleToken.self))
  }
}

struct SceneType<T: Any> {
  let storyboard: StoryboardType.Type
  let identifier: String

  func instantiate() -> T {
    guard let controller = storyboard.storyboard.instantiateViewController(withIdentifier: identifier) as? T else {
      fatalError("ViewController '\(identifier)' is not of the expected class \(T.self).")
    }
    return controller
  }
}

struct InitialSceneType<T: Any> {
  let storyboard: StoryboardType.Type

  func instantiate() -> T {
    guard let controller = storyboard.storyboard.instantiateInitialViewController() as? T else {
      fatalError("ViewController is not of the expected class \(T.self).")
    }
    return controller
  }
}

protocol SegueType: RawRepresentable { }

extension UIViewController {
  func perform<S: SegueType>(segue: S, sender: Any? = nil) where S.RawValue == String {
    performSegue(withIdentifier: segue.rawValue, sender: sender)
  }
}

// swiftlint:disable explicit_type_interface identifier_name line_length type_body_length type_name
enum VIACoreStoryboard {
  enum EditEntityNumber: StoryboardType {
    static let storyboardName = "EditEntityNumber"

    static let initialScene = InitialSceneType<VIACore.EditEntityNumberViewController>(storyboard: EditEntityNumber.self)

    static let editEntityNumberViewController = SceneType<VIACore.EditEntityNumberViewController>(storyboard: EditEntityNumber.self, identifier: "EditEntityNumberViewController")
  }
  enum EditMail: StoryboardType {
    static let storyboardName = "EditMail"

    static let editEmailViewController = SceneType<VIACore.EditEmailViewController>(storyboard: EditMail.self, identifier: "EditEmailViewController")
  }
  enum EventFeed: StoryboardType {
    static let storyboardName = "EventFeed"

    static let eventsFeedCategoryFilterNavigationViewController = SceneType<UINavigationController>(storyboard: EventFeed.self, identifier: "EventsFeedCategoryFilterNavigationViewController")

    static let eventsFeedLandingViewController = SceneType<VIACore.EventsFeedLandingViewController>(storyboard: EventFeed.self, identifier: "EventsFeedLandingViewController")
  }
  enum FirstTimePreferences: StoryboardType {
    static let storyboardName = "FirstTimePreferences"

    static let initialScene = InitialSceneType<UINavigationController>(storyboard: FirstTimePreferences.self)
  }
  enum Home: StoryboardType {
    static let storyboardName = "Home"

    static let initialScene = InitialSceneType<VIACore.VIATabBarController>(storyboard: Home.self)

    static let viaHomeNavigationViewController = SceneType<VIACore.VIANavigationViewController>(storyboard: Home.self, identifier: "VIAHomeNavigationViewController")

    static let viaHomeViewController = SceneType<VIACore.VIAHomeViewController>(storyboard: Home.self, identifier: "VIAHomeViewController")
  }
  enum LoginRegistration: StoryboardType {
    static let storyboardName = "LoginRegistration"

    static let initialScene = InitialSceneType<VIAFirstTimeOnboardingViewController>(storyboard: LoginRegistration.self)

    static let loginViewController = SceneType<VIACore.VIALoginViewController>(storyboard: LoginRegistration.self, identifier: "LoginViewController")

    static let viaFirstTimeOnboardingViewController = SceneType<VIAFirstTimeOnboardingViewController>(storyboard: LoginRegistration.self, identifier: "VIAFirstTimeOnboardingViewController")

    static let viaForgotPasswordNavigationViewController = SceneType<VIACore.VIANavigationViewController>(storyboard: LoginRegistration.self, identifier: "VIAForgotPasswordNavigationViewController")

    static let viaForgotPasswordViewController = SceneType<VIACore.VIAForgotPasswordViewController>(storyboard: LoginRegistration.self, identifier: "VIAForgotPasswordViewController")

    static let viaRegistrationNavigationController = SceneType<VIACore.VIANavigationViewController>(storyboard: LoginRegistration.self, identifier: "VIARegistrationNavigationController")

    static let viaRegistrationViewController = SceneType<VIACore.VIARegistrationViewController>(storyboard: LoginRegistration.self, identifier: "VIARegistrationViewController")

    static let viaResendInsurerCodeViewController = SceneType<VIACore.VIAResendInsurerCodeViewController>(storyboard: LoginRegistration.self, identifier: "VIAResendInsurerCodeViewController")

    static let viaSplashScreenViewController = SceneType<VIACore.VIASplashScreenViewController>(storyboard: LoginRegistration.self, identifier: "VIASplashScreenViewController")
  }
  enum LoginRestriction: StoryboardType {
    static let storyboardName = "LoginRestriction"

    static let initialScene = InitialSceneType<UINavigationController>(storyboard: LoginRestriction.self)

    static let loginRestriction = SceneType<VIACore.LoginRestrictionViewController>(storyboard: LoginRestriction.self, identifier: "LoginRestriction")
  }
  enum ProfileSettings: StoryboardType {
    static let storyboardName = "ProfileSettings"

    static let initialScene = InitialSceneType<UINavigationController>(storyboard: ProfileSettings.self)

    static let profileFeedbackViewController = SceneType<VIACore.ProfileFeedbackViewController>(storyboard: ProfileSettings.self, identifier: "ProfileFeedbackViewController")

    static let profileForgotPasswordViewController = SceneType<VIACore.ProfileForgotPasswordViewController>(storyboard: ProfileSettings.self, identifier: "ProfileForgotPasswordViewController")
  }
  enum ProvideFeedback: StoryboardType {
    static let storyboardName = "ProvideFeedback"

    static let initialScene = InitialSceneType<UINavigationController>(storyboard: ProvideFeedback.self)

    static let profileFeedbackCompletionViewController = SceneType<VIACore.ProfileFeedbackCompletionViewController>(storyboard: ProvideFeedback.self, identifier: "ProfileFeedbackCompletionViewController")

    static let profileProvideFeedbackViewController = SceneType<VIACore.ProfileProvideFeedbackViewController>(storyboard: ProvideFeedback.self, identifier: "ProfileProvideFeedbackViewController")

    static let profileUploadAttachmentCollectionViewController = SceneType<VIACore.ProfileUploadAttachmentCollectionViewController>(storyboard: ProvideFeedback.self, identifier: "ProfileUploadAttachmentCollectionViewController")
  }
}

enum StoryboardSegue {
  enum EditMail: String, SegueType {
    case showForgotPassword
  }
  enum FirstTimePreferences: String, SegueType {
    case showForgotPassword
    case showPrivacyPolicy
    case showWhatIsVitalityStatus
  }
  enum Home: String, SegueType {
    case showARAvailableRewards
    case showARChooseRewards
    case showARCurrentRewards
    case showARLanding
    case showARLearnMore
    case showAROnboarding
    case showARSpinner
    case showARVoucher
    case showActivationBarcode
    case showAppleWatchCashback
    case showDeviceCashback
    case showIntegratedBenefit
    case showMWB
    case showNonSmokersDeclaration
    case showNuffield
    case showOFE
    case showPartnerJourney
    case showPolicyCashback
    case showSAV
    case showStatus
    case showStatusIncreased
    case showUKECineSpinner
    case showUKEStarbucksSpinner
    case showVHC
    case showVHR
    case showVHRRequiredForAR
    case showVNA
    case showVitalityStatus
    case showWebVoucher
    case showWellnessDevices
  }
  enum LoginRegistration: String, SegueType {
    case showFeedback
    case showForgotPassword
    case showRegistration
    case showResendCode
    case unwindToEditEmailViewController
    case unwindToLogin
    case unwindToLoginAfterSuccessfulRegistration
  }
  enum ProfileSettings: String, SegueType {
    case showChangePassword
    case showChangePasswordConfirmation
    case showCommunicationSettings
    case showEditEmail
    case showEditEntityNumber
    case showEditProfile
    case showEventsFeed
    case showFeedback
    case showForgotPassword
    case showHelp
    case showMembershipInfo
    case showMembershipPass
    case showPrivacySettings
    case showPrivacyStatement
    case showProvideFeedback
    case showProvideFeedbackCMS
    case showSecuritySettings
    case showSettings
    case showTermsAndConditions
    case showWhatIsVitalityStatus
    case unwindToLogin
    case unwindToProfileSecurityViewController
  }
  enum ProvideFeedback: String, SegueType {
    case showChooseFeedbackType
    case showEditProof
    case showFeedbackCompletion
    case showImageDetailView
    case showUploadAttachment
    case unwindToCaptureResults
    case unwindToPhotos
    case unwindToProfileSettingsViewController
  }
}
// swiftlint:enable explicit_type_interface identifier_name line_length type_body_length type_name

private final class BundleToken {}
