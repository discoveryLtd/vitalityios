import VitalityKit
import VIACommon
import VIAHealthKit

public protocol WellnessDevicesContentLookup: WDAFetchDevicesManager {
    func noLinkedDevices() -> Bool
}

class WellnessDevicesContentLookupViewModel: WellnessDevicesContentLookup {
    public func noLinkedDevices() -> Bool {
        let wdaRealm = DataProvider.newWDARealm()
        return wdaRealm.allLinkedWDAPartners().count == 0 && VITHealthKitHelper.shared().status == .notLinkedYet
    }
}
