import VIAUtilities

public struct PointsCondition {
    public var conditionString: String
    public let arrayOfConditions: Array<String>
}

protocol PotentialPointsCondition {
    init(pointsWithTiersCondition: Conditions)
    var unit: Unit? { get }
    var pointsWithTiersCondition: Conditions { set get }

    func asTextLessThan(_ lessThan: String) -> String
    func asTextGreaterThan(_ greaterThan: String) -> String
    func asTextGreaterOrEqualThan(_ greaterOrEqual: String) -> String
    func asTextLessOrEqualThan(_ lessOrEqualThan: String) -> String
    func asTextLessAndGreaterThan(_ lessThan: String, _ greaterThan: String) -> String
    func asTextLessAndGreaterOrEqualThan(_ lessThan: String, _ greaterOrEqualThan: String) -> String
    func asTextLessOrEqualAndGreaterThan(_ lessOrEqual: String, _ greaterThan: String) -> String
    func asTextLessOrEqualAndGreaterOrEqualThan(_ lessOrEqual: String, _ greaterOrEqualThan: String) -> String
}

extension PotentialPointsCondition {
    func measurementString(for value: Int?, with unit: Unit?) -> String? {
        guard let unitOfMeasure = unit else { return String(describing: value) }
        if let validValue = value {
            return MeasurementFormatter.minutesString(for: validValue, with: unitOfMeasure)
        }
        return nil
    }

    public func asText() -> PointsCondition {
        var requirement = ""
        let condition = self.pointsWithTiersCondition
        var conditionals = [String]()

        let lessThan = condition.lessThanCondition
        let greaterThan = condition.greaterThanCondition
        let greaterThanOrEqual = condition.greaterThanOrEqualCondition
        let lessThanOrEqual = condition.lessThanOrEqualCondition
        let lessThanString = measurementString(for: lessThan, with: self.unit)
        let greaterThanString = measurementString(for: greaterThan, with: self.unit)
        let greaterThanOrEqualString = measurementString(for: greaterThanOrEqual, with: self.unit)
        let lessThanOrEqualString = measurementString(for: lessThanOrEqual, with: self.unit)

        if let lessThanString = lessThanString, let greaterThanString = greaterThanString {
            conditionals.append(lessThanString)
            conditionals.append(greaterThanString)
            requirement.append(self.asTextLessAndGreaterThan(lessThanString, greaterThanString))
        } else if let lessThanOrEqualString = lessThanOrEqualString,
            let greaterThanOrEqualString = greaterThanOrEqualString {
            conditionals.append(lessThanOrEqualString)
            conditionals.append(greaterThanOrEqualString)
            requirement.append(self.asTextLessOrEqualAndGreaterOrEqualThan(lessThanOrEqualString, greaterThanOrEqualString))
        } else if let lessThanOrEqualString = lessThanOrEqualString,
            let greaterThanString = greaterThanString {
            conditionals.append(lessThanOrEqualString)
            conditionals.append(greaterThanString)
            requirement.append(self.asTextLessOrEqualAndGreaterThan(lessThanOrEqualString, greaterThanString))
        } else if let greaterThanOrEqualString = greaterThanOrEqualString,
            let lessThanString = lessThanString {
            conditionals.append(greaterThanOrEqualString)
            conditionals.append(lessThanString)
            requirement.append(self.asTextLessAndGreaterOrEqualThan(lessThanString, greaterThanOrEqualString))
        } else if let lessThanString = lessThanString {
            conditionals.append(lessThanString)
            requirement.append(self.asTextLessThan(lessThanString))
        } else if let greaterThanString = greaterThanString {
            conditionals.append(greaterThanString)
            requirement.append(self.asTextGreaterThan(greaterThanString))
        } else if let greaterThanOrEqualString = greaterThanOrEqualString {
            conditionals.append(greaterThanOrEqualString)
            requirement.append(self.asTextGreaterOrEqualThan(greaterThanOrEqualString))
        } else if let lessThanOrEqualString = lessThanOrEqualString {
            conditionals.append(lessThanOrEqualString)
            requirement.append(self.asTextLessOrEqualThan(lessThanOrEqualString))
        }

        let pointsCondition = PointsCondition(conditionString: requirement, arrayOfConditions: conditionals)
        return pointsCondition
    }
}
