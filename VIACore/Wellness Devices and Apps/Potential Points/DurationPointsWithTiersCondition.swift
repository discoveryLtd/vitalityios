import Foundation
import VIAUtilities

public struct DurationCondition: PotentialPointsCondition {
    var pointsWithTiersCondition: Conditions
    var unit: Unit? {
        get {
            return self.pointsWithTiersCondition.unitOfMeasure.unit()
        }
    }

    public init (pointsWithTiersCondition: Conditions) {
        self.pointsWithTiersCondition = pointsWithTiersCondition
    }

    internal func asTextGreaterThan(_ greaterThan: String) -> String {
        return CommonStrings.PotentialPoints.SpeedDurationUpperBound477(greaterThan)
    }

    func asTextLessThan(_ lessThan: String) -> String {
        return CommonStrings.PotentialPoints.SpeedDurationLowerBound479(lessThan)
    }

    func asTextLessAndGreaterThan(_ lessThan: String, _ greaterThan: String) -> String {
        return CommonStrings.PotentialPoints.SpeedDurationLowerAndUpperBound478(greaterThan, lessThan)
    }

    func asTextLessOrEqualThan(_ lessOrEqualThan: String) -> String {
        return CommonStrings.PotentialPoints.SpeedDurationLessOrEqualThanBound583(lessOrEqualThan)
    }

    func asTextLessOrEqualAndGreaterThan(_ lessOrEqual: String, _ greaterThan: String) -> String {
        return CommonStrings.PotentialPoints.SpeedDurationLessOrEqualAndGreaterOrEqualThanBound586(greaterThan, lessOrEqual)
    }

    func asTextGreaterOrEqualThan(_ greaterOrEqualThan: String) -> String {
        return CommonStrings.PotentialPoints.SpeedDurationGreaterOrEqualBound582(greaterOrEqualThan)
    }

    func asTextLessAndGreaterOrEqualThan(_ lessThan: String, _ greaterOrEqualThan: String) -> String {
        return CommonStrings.PotentialPoints.SpeedDurationLessAndGreaterOrEqualThanBound585(greaterOrEqualThan, lessThan)
    }

    func asTextLessOrEqualAndGreaterOrEqualThan(_ lessOrEqual: String, _ greaterOrEqualThan: String) -> String {
        return CommonStrings.PotentialPoints.SpeedDurationLessOrEqualAndGreaterOrEqualThanBound586(greaterOrEqualThan, lessOrEqual)
    }
}
