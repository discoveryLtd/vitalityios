import Foundation
import VIAUtilities

public struct HeartRateCondition: PotentialPointsCondition {
    var pointsWithTiersCondition: Conditions
    var unit: Unit? {
        get {
            return VIAUnits.percentage
        }
    }

    public init (pointsWithTiersCondition: Conditions) {
        self.pointsWithTiersCondition = pointsWithTiersCondition
    }

    internal func asTextGreaterThan(_ greaterThan: String) -> String {
        return CommonStrings.PotentialPoints.HeartrateLowerBound482(greaterThan)
    }

    internal func asTextLessThan(_ lessThan: String) -> String {
        return CommonStrings.PotentialPoints.HeartrateUpperBound480(lessThan)
    }

    internal func asTextLessAndGreaterThan(_ lessThan: String, _ greaterThan: String) -> String {
        return CommonStrings.PotentialPoints.HeartrateLowerBoundUpperBound481(greaterThan, lessThan)
    }

    internal func asTextLessOrEqualThan(_ lessOrEqualThan: String) -> String {
        return CommonStrings.PotentialPoints.HeartrateLessOrEqualThanBound587(lessOrEqualThan)
    }

    internal func asTextLessOrEqualAndGreaterThan(_ lessOrEqual: String, _ greaterThan: String) -> String {
        return CommonStrings.PotentialPoints.HeartrateLessAndGreaterThanBound591(greaterThan, lessOrEqual)
    }

    internal func asTextGreaterOrEqualThan(_ greaterOrEqualThan: String) -> String {
        return CommonStrings.PotentialPoints.HeartrateGreaterOrEqualThanBound588(greaterOrEqualThan)
    }

    internal func asTextLessAndGreaterOrEqualThan(_ lessThan: String, _ greaterOrEqualThan: String) -> String {
        return CommonStrings.PotentialPoints.HeartrateLessAndGreaterThanBound591(greaterOrEqualThan, lessThan)
    }

    internal func asTextLessOrEqualAndGreaterOrEqualThan(_ lessOrEqual: String, _ greaterOrEqualThan: String) -> String {
        return CommonStrings.PotentialPoints.HeartrateLessOrEqualAndGreaterOrEqualBound590(greaterOrEqualThan, lessOrEqual)
    }
}
