import Foundation
import VIAUtilities

public struct CalorieCondition: PotentialPointsCondition {
    var pointsWithTiersCondition: Conditions
    var unit: Unit? {
        get {
            return self.pointsWithTiersCondition.unitOfMeasure.unit()
        }
    }

    public init (pointsWithTiersCondition: Conditions) {
        self.pointsWithTiersCondition = pointsWithTiersCondition
    }

    internal func asTextGreaterThan(_ greaterThan: String) -> String {
        return CommonStrings.PotentialPoints.CalorieCountLowerBound490(greaterThan)
    }

    internal func asTextLessThan(_ lessThan: String) -> String {
        return CommonStrings.PotentialPoints.CalorieCountUpperBound492(lessThan)
    }

    internal func asTextLessAndGreaterThan(_ lessThan: String, _ greaterThan: String) -> String {
        return CommonStrings.PotentialPoints.CalorieCountLowerBoundUpperBound491(greaterThan, lessThan)
    }

    internal func asTextLessOrEqualThan(_ lessOrEqualThan: String) -> String {
        return CommonStrings.PotentialPoints.CalorieCountLessOrEqualThanBound592(lessOrEqualThan)
    }

    internal func asTextGreaterOrEqualThan(_ greaterOrEqualThan: String) -> String {
        return CommonStrings.PotentialPoints.CalorieCountGreaterOrEqualThanBound593(greaterOrEqualThan)
    }

    internal func asTextLessOrEqualAndGreaterOrEqualThan(_ lessOrEqual: String, _ greaterOrEqualThan: String) -> String {
        return CommonStrings.PotentialPoints.CalorieCountLowerBoundUpperBound491(greaterOrEqualThan, lessOrEqual)
    }

    internal func asTextLessOrEqualAndGreaterThan(_ lessOrEqual: String, _ greaterThan: String) -> String {
        return CommonStrings.PotentialPoints.CalorieCountLessOrEqualAndGreaterThanBound594(greaterThan, lessOrEqual)
    }

    internal func asTextLessAndGreaterOrEqualThan(_ lessThan: String, _ greaterOrEqualThan: String) -> String {
        return CommonStrings.PotentialPoints.CalorieCountLessAndGreaterThanBound596(greaterOrEqualThan, lessThan)
    }
}
