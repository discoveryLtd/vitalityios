//
//  ProfileCommunicationPreferencesViewController.swift
//  VitalityActive
//
//  Created by Von Kervin R. Tuico on 10/11/2017.
//  Copyright © 2017 Glucode. All rights reserved.
//

import UserNotifications

import VIAUIKit
import VitalityKit
import VIAUtilities
import VIACommon

class ProfileCommunicationPreferencesViewController: VIATableViewController {
    
    var emailPrefs = EmailCommunicationPreference()
    var pushNotificationPrefs = PushNotificationPreferenceObject()
    var pushNotifState = false
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        
        setupNavBar()
        configureTableView()
        
        /* Set initial state of push notificateion toggle from app settings. */
        self.pushNotifState = pushNotificationPrefs.preferenceInitialState()
    }
    
    public override func viewWillAppear(_ animated: Bool){
        super.viewWillAppear(animated)
        
        /* Check for push notification state override. */
        if let applyPushNotifToggle = VIAApplicableFeatures.default.applyPushNotifToggle, applyPushNotifToggle{
            let center = UNUserNotificationCenter.current()
            center.getNotificationSettings { (settings) in
                DispatchQueue.main.sync { [weak self] in
                    /* Assign the state of device's notification to pushNotifState. */
                    self?.pushNotifState = .authorized == settings.authorizationStatus
                    /* Reload table to refresh the state of push notification toggle. */
                    self?.tableView.reloadData()
                }
            }
        }
    }
    
    func configureTableView() {
        self.tableView.register(SettingsHeader.nib(), forCellReuseIdentifier: "header")
        self.tableView.register(FirstTimePreferenceSwitchCell.nib(), forCellReuseIdentifier: FirstTimePreferenceSwitchCell.defaultReuseIdentifier)
        self.tableView.estimatedRowHeight = 200
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.separatorInset = UIEdgeInsets(top: 0, left: 52, bottom: 0, right: 0)
        self.tableView.backgroundColor = .white
        self.tableView.tableFooterView = UIView()
        self.removeFooterHeaderSpaces()
    }
    
    func setupNavBar() {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        
        self.hideBackButtonTitle()
    }
    
    func removeFooterHeaderSpaces() {
        self.tableView.estimatedSectionHeaderHeight = 0
        self.tableView.estimatedSectionFooterHeight = 0
    }
    
    func showEmailCell() -> UITableViewCell {
        guard var switchCell = self.tableView.dequeueReusableCell(withIdentifier: FirstTimePreferenceSwitchCell.defaultReuseIdentifier) as? FirstTimePreferenceSwitchCell else {
            return self.tableView.defaultTableViewCell()
        }
        
        switchCell = configSwitchCell(switchCell, dataObject: emailPrefs) {[weak self] (sender) in

            if (self?.pushNotificationPrefs.preferenceInitialState() == false) && (self?.emailPrefs.preferenceInitialState() == true){
                self?.checkIfEmailAndPushDisabled(preferenceDataObject: (self?.emailPrefs)!, sender: sender)
            }else{
                self?.updateEmailPreference(sender: sender)
            }
        }
        
        switchCell.switchAction.setOn(emailPrefs.preferenceInitialState(), animated: false)
        
        return switchCell
    }
    
    func showPushNotificationsCell() -> UITableViewCell {
        guard var switchCell = self.tableView.dequeueReusableCell(withIdentifier: FirstTimePreferenceSwitchCell.defaultReuseIdentifier) as? FirstTimePreferenceSwitchCell else {
            return self.tableView.defaultTableViewCell()
        }
        
        switchCell = configSwitchCell(switchCell, dataObject: pushNotificationPrefs) {[weak self] (sender) in
            
            if VIAApplicableFeatures.default.enableNotificationDialogConfirmation{
                if (self?.pushNotificationPrefs.preferenceState == true) && (self?.emailPrefs.preferenceState == false){
                    self?.checkIfEmailAndPushDisabled(preferenceDataObject: (self?.pushNotificationPrefs)!, sender: sender)
                }else{
                    self?.pushNotificationPrefs.toggle(sender: sender)
                }
            }else{
                self?.pushNotificationPrefs.toggle(sender: sender)
            }
    }
        
        let center = UNUserNotificationCenter.current()
        center.getNotificationSettings { (settings) in
            DispatchQueue.main.sync {
                
                if VIAApplicableFeatures.default.enableNotificationPreference!{
                    switchCell.switchAction.setOn(.authorized == settings.authorizationStatus, animated: false)
                }
                UserDefaults.standard.set(.authorized == settings.authorizationStatus, forKey: "pushNotifState")
            }
        }
        
        switchCell.switchAction.setOn(self.pushNotifState, animated: false)
        switchCell.switchAction.isEnabled = false
        
        switchCell.actionButton.isHidden = false
        switchCell.actionButtonHeight.constant = 20
        switchCell.actionButtonSpaceConstraint.constant = 15
        switchCell.actionButton.setTitle(CommonStrings.UserPrefs.PushMessageToggleSettingsLinkButtonTitle69, for: .normal)
        switchCell.setActionForButton({ () -> (Void)? in
            if let appSettings = URL(string: UIApplicationOpenSettingsURLString) {
                UIApplication.shared.open(appSettings, options: [:], completionHandler: nil)
            }
            return nil
        })
        
        // remove separator
        switchCell.separatorInset = UIEdgeInsetsMake(0, 0, 0, tableView.bounds.width)
        
        return switchCell
    }
    
    
    func checkIfEmailAndPushDisabled(preferenceDataObject: PreferenceDataObject, sender: UISwitch?){
    
            
            let alert = UIAlertController(title: CommonStrings.UserPrefs.NoPreferencesSetAlertTitle85, message: CommonStrings.UserPrefs.NoPreferencesSetAlertMessage86, preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: CommonStrings.CancelButtonTitle24, style: .cancel){ (action) in
                sender?.setOn(true, animated: true)
                
                if preferenceDataObject is EmailCommunicationPreference{
                    
                    self.updateEmailPreference(sender: sender!)
                    
                    //self.emailPrefs.toggleEmailPreference(sender: sender)
                }else{
                    self.pushNotificationPrefs.toggle(sender: sender)
                }
                
            })
            
            alert.addAction(UIAlertAction(title: CommonStrings.ContinueButtonTitle87, style: .default) { (action) in
                
                if preferenceDataObject is EmailCommunicationPreference{
                    
                    self.updateEmailPreference(sender: sender!)
                    
                    //self.emailPrefs.toggleEmailPreference(sender: sender)
                }else{
                    self.pushNotificationPrefs.toggle(sender: sender)
                }
                
                sender?.setOn(false, animated: true)

            })
       
            alert.view.tintColor = UIButton().tintColor
            self.present(alert, animated: true, completion: nil)
        
    }
    
    func updateEmailPreference(sender: UISwitch){
        self.showHUDOnView(view: self.view)
        self.emailPrefs.changeEmailPreference(sender: sender){ [weak self] error in
            self?.hideHUDFromView(view: self?.view)
        }
    }
    
    func showHeaderCell() -> UITableViewCell {
        // header
        guard var cell = self.tableView.dequeueReusableCell(withIdentifier: "header") as? SettingsHeader else {
            return self.tableView.defaultTableViewCell()
        }
        cell = setupHeader(cell)
        return cell
    }
    
    // MARK: UITableView Data Source
    enum RowItems: Int, EnumCollection {
        case Header = 0
        case Email = 1
        case PushNotification = 2
    }
    
    public override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    public override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    public override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let webContentViewController = segue.destination as? VIAWebContentViewController,
            let articleId = AppConfigFeature.contentId(for: .userPreferencesPrivacyPolicy) {
            let webContentViewModel = VIAWebContentViewModel(articleId: articleId)
            webContentViewController.viewModel = webContentViewModel
            webContentViewController.isNavBarTransparent = true
        }
    }
    
    func configSwitchCell(_ switchCell: FirstTimePreferenceSwitchCell, dataObject: PreferenceDataObject, completion: @escaping (_:UISwitch)->Void) -> FirstTimePreferenceSwitchCell {
        switchCell.setupCell()
        switchCell.actionButton.isHidden = true
        if let image = dataObject.preferenceImage {
            switchCell.setImage(asset: image)
        }
        if let title = dataObject.preferenceTitle {
            switchCell.setTitle(title)
        }
        if let detail = dataObject.preferencesDetail {
            switchCell.setDetailText(detail)
        }
        
        
        switchCell.setActionForSwitch {(sender) -> (Void)?  in
            return completion(sender)
        }
        
        switchCell.selectionStyle = .none
        
        return switchCell
    }
    
    func setupHeader(_ cell: SettingsHeader) -> SettingsHeader {
        cell.setTitle(CommonStrings.Settings.CommunicationTitle902)
        cell.setDetailText(CommonStrings.UserPrefs.CommunicationGroupHeaderMessage65)
        cell.actionButton.isHidden = true
        
        // remove separator
        cell.separatorInset = UIEdgeInsetsMake(0, 0, 0, tableView.bounds.width);
        
        cell.selectionStyle = .none
        return cell
    }
    
    public override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: UITableViewCell = UITableViewCell.init()
        if let row = RowItems(rawValue: indexPath.row){
            switch(row){
            case .Email:
                cell = self.showEmailCell()
                break
            case .PushNotification:
                cell = self.showPushNotificationsCell()
                break
            case .Header:
                cell = self.showHeaderCell()
                break
            }
        }
        return cell
    }
}
