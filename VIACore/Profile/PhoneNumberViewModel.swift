//
//  PhoneNumberViewModel.swift
//  VitalityActive
//
//  Created by Joshua Ryan on 7/13/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation

class PhoneNumberViewModel: EditableProfileFieldModel {

	var countryDialCode: String = ""
	var areaDialCode: String = ""
	var contactNumber: String = ""
	var numberExtension: String = ""
	var typeKey: Int = 0
	var typeName: String = ""

	var existingValue: String {
		get {
			return "\(countryDialCode) \(areaDialCode)\(contactNumber) \(numberExtension)"
		}
	}
	var updatedValue: String = ""

	var type: EditProfileFieldType {
		get {
			return .Phone
		}
	}

	var display: String {
		get {
			return "\(countryDialCode) \(areaDialCode)\(contactNumber)"
		}
	}
}
