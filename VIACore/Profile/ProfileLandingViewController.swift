//
//  ProfileLandingViewController.swift
//  VitalityActive
//
//  Copyright © 2017 Glucode. All rights reserved.
//

import VIAUIKit
import VitalityKit
import VIAUtilities
import VIACommon

class ProfileLandingViewController: VIACoordinatedTableViewController, PrimaryColorTintable {

    // MARK: Private

    private var flowController = ProfileFlowCoordinator()

    // MARK: Public

    
    // Mark: Initializers
    var viewModel: ProfileViewModel {
        get {
            return flowController.viewModel
        }
    }

    required init?(coder aDecoder: NSCoder) {
        
        super.init(coder: aDecoder)
        self.setupTabBarTitle()
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        self.setupTabBarTitle()
    }
    
    // MARK: Lifecycle
    override public func viewDidLoad() {
        super.viewDidLoad()

        self.title = CommonStrings.Settings.LandingTitle901
        self.hideBackButtonTitle()

        flowController.configureController(vc: self)
        self.configureAppearance()
        self.configureTableView()
    }

    override public func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        self.tableView.reloadData()
    }
    
    func setupTabBarTitle() {
        
        self.title = CommonStrings.MenuProfileButton9
        self.tabBarItem.title = CommonStrings.MenuProfileButton9
        self.navigationController?.tabBarItem.title = CommonStrings.MenuProfileButton9
        
    }

    func configureAppearance() {
        tableView.tableFooterView = UIView()
    }

    func configureTableView() {
        self.tableView.register(VIAUserBasicInfoCell.nib(), forCellReuseIdentifier: VIAUserBasicInfoCell.defaultReuseIdentifier)
        self.tableView.register(VIATableViewCell.self, forCellReuseIdentifier: VIATableViewCell.defaultReuseIdentifier)
        self.tableView.register(VIATableViewSectionHeaderFooterView.nib(), forHeaderFooterViewReuseIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier)
        self.tableView.estimatedRowHeight = 75
    }

    // MARK: UITableView data source

    enum Sections: Int, EnumCollection {
        case UserInfo = 0
        case Menu = 1

        static func heightForHeader(section: Int) -> CGFloat {
            if Sections(rawValue: section) == .UserInfo {
                return CGFloat.leastNormalMagnitude
            }
            return UITableViewAutomaticDimension
        }
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return Sections.allValues.count
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let section = Sections(rawValue: section)
        if section == .Menu {
            if (VIAApplicableFeatures.default.changeMenuItemLocation)!{
                return MenuItem.list().count
            }else{
                return MenuItem.list().count - 5
            }
            
        } else {
            return 1
        }
    }

    // MARK: UITableView delegate

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let section = Sections(rawValue: indexPath.section)

        var cell = tableView.dequeueReusableCell(withIdentifier: VIATableViewCell.defaultReuseIdentifier, for: indexPath)

        if section == .Menu {
            cell = configureMenuCell(tableView, indexPath: indexPath)
        } else if section == .UserInfo {
            cell = configureUserInfoCell(tableView, indexPath: indexPath)
        }
        cell.accessoryType = .disclosureIndicator

        return cell
    }

    func configureUserInfoCell(_ tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: VIAUserBasicInfoCell.defaultReuseIdentifier, for: indexPath) as! VIAUserBasicInfoCell

        let person = viewModel.currentPerson
        cell.name = "\(person.givenName) \(person.familyName)"

        cell.email = AppSettings.getLastLoginUsername()

        ProfileImageHelper.getFile(partyId: DataProvider.newRealm().getPartyId()) { (image) in
            if nil != image{
                cell.photo = image
            }
        }        
        
        cell.accessoryType = .disclosureIndicator

        return cell
    }

    func configureMenuCell(_ tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: VIATableViewCell.defaultReuseIdentifier, for: indexPath)

        let row = indexPath.row
        cell.textLabel?.text = MenuItem.list()[row].title()
        cell.imageView?.image = MenuItem.list()[row].image()
        cell.imageView?.tintColor = UIColor.currentGlobalTintColor()
        cell.accessoryType = .disclosureIndicator

        return cell
    }

    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let section = Sections(rawValue: section)

        if section == .UserInfo {
            return nil
        }

        let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier) as! VIATableViewSectionHeaderFooterView
        view.labelText = " " // Is there a better way of keeping the header height currently?
        return view
    }

    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return Sections.heightForHeader(section: section)
    }

    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)

        let section = Sections(rawValue: indexPath.section)

        if section == .UserInfo {
            self.performSegue(withIdentifier: "showEditProfile", sender: nil)
        } else if section == .Menu {
            if indexPath.row == LogoutItem().index() {
                logout()
            }
            else if indexPath.row == RateUsItem().index() {
                let appId = Bundle.main.object(forInfoDictionaryKey: "VAiTunesIdentifier") as? String ?? "Unknown"
                doRateApp(appId: appId)
            }
            else{
                self.performSegue(withIdentifier: MenuItem.list()[indexPath.row].segueIdentifier(), sender: nil)
            }
            
        }
    }

    func logout() {

        let alert = UIAlertController(title: CommonStrings.Settings.AlertLogoutTitle909, message: CommonStrings.Settings.AlertLogoutMessage910, preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: CommonStrings.CancelButtonTitle24, style: UIAlertActionStyle.cancel, handler: { (UIAlertAction) in
            alert.dismiss(animated: false, completion: nil)
        })
        let continueAction = UIAlertAction(title: CommonStrings.Settings.LogoutTitle908, style: UIAlertActionStyle.default, handler: { (UIAlertAction) in
            alert.dismiss(animated: false, completion: nil)
            VIALoginViewController.showLogin()
        })
        alert.view.tintColor = UIButton().tintColor
        alert.addAction(cancelAction)
        alert.addAction(continueAction)
        self.present(alert, animated: true, completion: nil)

    }
    
    func doRateApp(appId: String) {
        
        if let url = URL(string: "https://itunes.apple.com/app/id\(appId)") {
            
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: { (success) in
                    print("User sent to rate \(url): with \(success)")
                })
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == TermsConditionsItem().segueIdentifier() {
                        self.setBackButtonTitle(CommonStrings.Settings.LandingTitle2448)
                        if let webContentViewController = segue.destination as? VIAWebContentViewController,
                            let articleId = AppConfigFeature.contentId(for: .TermsAndConditionsContent) {
            
                            let webContentViewModel = VIAWebContentViewModel(articleId: articleId)
                            webContentViewController.viewModel = webContentViewModel
                            webContentViewController.title = CommonStrings.Settings.TermsConditionsTitle905
            }
        } else if segue.identifier == FeedbackItem().segueIdentifier() {
            self.hideBackButtonTitle()
            let webContentViewController = segue.destination as? VIAWebContentViewController
            
            let articleId = "profile-settings-feedback"
            
            let webContentViewModel = VIAWebContentViewModel(articleId: articleId)
            webContentViewController?.viewModel = webContentViewModel
            webContentViewController?.title = CommonStrings.Settings.FeedbackSectionTitle949
        } else if segue.identifier == RateUsItem().segueIdentifier() {
            self.hideBackButtonTitle()
        } else {
        
        }
    }
}

extension ProfileLandingViewController{
    
    fileprivate func downloadProfileImage(){
        ProfileImageHelper.getFile(partyId: DataProvider.newRealm().getPartyId()) { [weak self] (image) in
            self?.tableView.reloadData()
        }
    }
}

class MenuItem{
    static func list() -> [MenuItemsProtocol]{
        if AppSettings.getAppTenant() == .CA{
            return [MembershipPassItem(),
                    EventsFeedItem(),
                    SettingsItem(),
                    FeedbackItem(),
                    HelpItem(),
                    RateUsItem(),
                    TermsConditionsItem(),
                    LogoutItem()
            ]
        }
        return [MembershipPassItem(),
                SettingsItem(),
                FeedbackItem(),
                HelpItem(),
                RateUsItem(),
                TermsConditionsItem(),
                LogoutItem()
        ]
    }
}

protocol MenuItemsProtocol{
    func index() -> Int
    func title() -> String
    func image() -> UIImage?
    func segueIdentifier() -> String
}

class MembershipPassItem: MenuItemsProtocol{
    
    func index() -> Int{
        return 0
    }
    
    func title() -> String{
        if let hideMembershipPassAndUpdateTitle = VIAApplicableFeatures.default.hideMembershipPassAndUpdateTitle,
            hideMembershipPassAndUpdateTitle{
            return CommonStrings.ProfileMembershipDetailsTitle
        }
        return CommonStrings.Settings.MembershipPass911
    }
    
    func image() -> UIImage?{
        return VIACoreAsset.Profile.profileMembershipPass.templateImage
    }
    
    func segueIdentifier() -> String{
        return "showMembershipPass"
    }
}

class EventsFeedItem: MenuItemsProtocol{
    
    func index() -> Int{
        return 1
    }
    
    func title() -> String{
        return CommonStrings.Settings.LandingEventsTitle900
    }
    
    func image() -> UIImage?{
        return VIACoreAsset.Profile.profileEventsFeed.templateImage
    }
    
    func segueIdentifier() -> String{
        return "showEventsFeed"
    }
}

class SettingsItem: MenuItemsProtocol{
    
    func index() -> Int{
        return 2
    }
    
    func title() -> String{
        return CommonStrings.Settings.LandingSettingsTitle899
    }
    
    func image() -> UIImage?{
        return VIACoreAsset.Profile.profileSettings.templateImage
    }
    
    func segueIdentifier() -> String{
        return "showSettings"
    }
}

class FeedbackItem: MenuItemsProtocol{
    
    func index() -> Int{
        return 3
    }
    
    func title() -> String{
        return CommonStrings.Settings.FeedbackTitle906
    }
    
    func image() -> UIImage?{
        return VIACoreAsset.Profile.profileProvideFeedback.templateImage
    }
    
    func segueIdentifier() -> String{
        if VIAApplicableFeatures.default.shouldPullProvideFeedbackContentFromCMS!{
            return "showProvideFeedbackCMS"
        }
        return "showProvideFeedback"
    }
}

class HelpItem: MenuItemsProtocol{
    
    func index() -> Int{
        return 4
    }
    
    func title() -> String{
        return CommonStrings.HelpButtonTitle141
    }
    
    func image() -> UIImage?{
        return VIACoreAsset.Generic.helpGeneric.templateImage
    }
    
    func segueIdentifier() -> String{
        return "showHelp"
    }
}

class RateUsItem: MenuItemsProtocol{
    
    func index() -> Int{
        if AppSettings.getAppTenant() == .UKE {
            return 4
        }
        return 5
    }
    
    func title() -> String{
        return CommonStrings.Settings.RateUsTitle907
    }
    
    func image() -> UIImage?{
        return VIACoreAsset.Profile.profileRateUs.templateImage
    }
    
    func segueIdentifier() -> String{
        return "showRate"
    }
}

class TermsConditionsItem: MenuItemsProtocol{
    
    func index() -> Int{
        return 6
    }
    
    func title() -> String{
        return CommonStrings.Settings.TermsConditionsTitle905
    }
    
    func image() -> UIImage?{
        return VIACoreAsset.Profile.profileTermsAndCondition.templateImage
    }
    
    func segueIdentifier() -> String{
        return "showTermsAndConditions"
    }
}

class LogoutItem: MenuItemsProtocol{
    
    func index() -> Int{
        if AppSettings.getAppTenant() == .UKE {
            return 6
        }
        return 7
    }
    
    func title() -> String{
        return CommonStrings.Settings.LogoutTitle908
    }
    
    func image() -> UIImage?{
        return VIACoreAsset.Profile.profileLogout.templateImage
    }
    
    func segueIdentifier() -> String{
        return ""
    }
}
