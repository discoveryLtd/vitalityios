//
//  ProfileFlowCoordinator.swift
//  VitalityActive
//
//  Created by Joshua Ryan on 7/13/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

import VitalityKit
import VIACommon

class ProfileFlowCoordinator: VIAFlowDelegate {

	// MARK: Public

	weak var settingsController: ProfileSettingsViewController?
	weak var editProfileController: EditProfileViewController?
	weak var landingController: ProfileLandingViewController?
    weak var membershipPassController: ProfileMembershipPassViewController?

	var viewModel: ProfileViewModel = ProfileViewModel()

	// MARK: Private

	// State related items to inject into VCs
	fileprivate var currentField: EditableProfileFieldModel?


	// MARK: View Configuration

	func configureController(vc: UIViewController) {
		if let vc = vc as? ProfileSettingsViewController {
			settingsController = vc
			vc.delegate = self
			vc.coordinator = self
		} else if let vc = vc as? EditProfileViewController {
			editProfileController = vc
			vc.coordinator = self
			vc.viewModel = viewModel
		} else if let vc = vc as? ProfileLandingViewController {
			landingController = vc
			vc.coordinator = self
        } else if let vc = vc as? ProfileMembershipPassViewController {
            membershipPassController = vc
            vc.coordinator = self
        } else if let nav = vc as? UINavigationController, let rootVc = nav.viewControllers.first {
			configureController(vc: rootVc)
		}
	}
}

// MARK: ProfileSettingsDelegate

extension ProfileFlowCoordinator: ProfileSettingsDelegate {

}
