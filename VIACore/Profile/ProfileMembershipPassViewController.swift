//
//  ProfileMembershipPassViewController.swift
//  VitalityActive
//
//  Created by Ma. Catherine Purganan on 10/11/2017.
//  Copyright © 2017 Glucode. All rights reserved.
//

import AVFoundation
import PassKit

import VIAUIKit
import VIAUtilities
import VIACommon
import VitalityKit
import VIAHelpFAQ

// MARK: Enum
private enum Sections: Int, EnumCollection {
    case Banner = 0
    case MembershipDetails = 1
    case DigitalPass = 2
    case Help = 3
}

private enum MembershipItems: Int, EnumCollection {
    case VitalityNumber = 0
    case MembershipNumber = 1
    case CustomerNumber = 2
    case VitalityStatus = 3
    case MembershipStartDate = 4
    case MembershipStatus = 5
    
    func title() -> String? {
        switch self {
        case .VitalityNumber:
            if VIAApplicableFeatures.default.showMembershipBanner!{
                return CommonStrings.Settings.MembershipVitalityNumberTitle966
            }else{
                return CommonStrings.Settings.MembershipPartyIdTitle1105
            }
        case .VitalityStatus:
            return CommonStrings.Settings.MembershipVitalityStatusTitle967
        case .MembershipStartDate:
            return CommonStrings.Settings.MembershipStartDateTitle968
        case .MembershipStatus:
            return CommonStrings.Settings.MembershipStatus969
        case .MembershipNumber:
            return CommonStrings.ProfileFieldMembershipNumberTitle1125
        case .CustomerNumber:
            return CommonStrings.ProfileFieldCustomerNumberTitle1124
        }
    }
}


class ProfileMembershipPassViewController: VIACoordinatedTableViewController, ImagePickerDelegate, OptionalImagePickerDelegate {
    
    private var selectedMembershipItemRow: Int?
    private var addPassController: PKAddPassesViewController? = nil
    private let homeViewModel: VIAHomeViewModel = VIAHomeViewModel.sharedInstance
    private let profileViewModel: ProfileViewModel = ProfileViewModel()
    private let showHelpInfoIdentifier = "showHelp"
    private let showMembershipInfoIdentifier = "showMembershipInfo"
    var imagePicker: SelectImageController?
    
    private var flowController = ProfileFlowCoordinator()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        flowController.configureController(vc: self)
        if let hideMembershipPassAndUpdateTitle = VIAApplicableFeatures.default.hideMembershipPassAndUpdateTitle,
            hideMembershipPassAndUpdateTitle{
            self.title = CommonStrings.ProfileMembershipDetailsTitle
        }else{
            self.title = CommonStrings.Settings.MembershipPass911
        }
        self.configureAppearance()
        self.configureTableView()
        imagePicker = SelectImageController(pickerDelegate: self)
        imagePicker?.optionalDelegate = self
        
        //self.initializeUserPass()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        //WORKAROUND for Dealloc issue.
    }
    
    override public func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == showMembershipInfoIdentifier {
            guard segue.destination is ProfileMembershipInfoViewController else {
                return
            }
            
            let membershipInfoController = segue.destination as! ProfileMembershipInfoViewController
            let row = MembershipItems(rawValue: selectedMembershipItemRow!)
            membershipInfoController.title = row?.title()
            switch row! {
            case .VitalityNumber:
                membershipInfoController.informationText = CommonStrings.ProfileFieldVitalityNumberInformation
                break
            case .MembershipNumber:
                membershipInfoController.informationText = CommonStrings.ProfileFieldVitalityMembershipNumber
            case .CustomerNumber:
                membershipInfoController.informationText = CommonStrings.ProfileFieldVitalityCustomerNumber
            default:
                membershipInfoController.informationText = ""
                break
            }
            
        } else if segue.identifier == showHelpInfoIdentifier {
            guard segue.destination is VIAHelpViewController else {
                return
            }
        }
    }
    
    func configureAppearance() {
        self.hideBackButtonTitle()
        self.tableView.backgroundColor = UIColor.tableViewBackground()
        self.navigationController?.navigationBar.tintColor = UIColor.currentGlobalTintColor()
    }
    
    func configureTableView() {
        self.tableView.register(VIATableViewCell.self, forCellReuseIdentifier: VIATableViewCell.defaultReuseIdentifier)
        self.tableView.register(VIAProfileMembershipPassBannerTableViewCell.nib(), forCellReuseIdentifier: VIAProfileMembershipPassBannerTableViewCell.defaultReuseIdentifier)
        self.tableView.register(VIAProfileDetailCell.nib(), forCellReuseIdentifier: VIAProfileDetailCell.defaultReuseIdentifier)
        self.tableView.register(VIAProfileDigitalPassTableViewCell.nib(), forCellReuseIdentifier: VIAProfileDigitalPassTableViewCell.defaultReuseIdentifier)
        self.tableView.register(VIATableViewSectionHeaderFooterView.nib(), forHeaderFooterViewReuseIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier)
        self.tableView.estimatedRowHeight = 75
        self.tableView.backgroundColor = UIColor.tableViewBackground()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: UITableview datasource
    override func numberOfSections(in tableView: UITableView) -> Int {
        return Sections.allValues.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let section = Sections(rawValue: section)
        if section == .MembershipDetails {
            guard !(VIAApplicableFeatures.default.userPartyIDAsVitalityNumber ?? true) && (VIAApplicableFeatures.default.showPartyIdDetail() ?? false) else {
                return MembershipItems.allValues.count - 2
            }
            return MembershipItems.allValues.count
        }else if section == .Help {
            if let hideHelpTab = VIAApplicableFeatures.default.hideHelpTab,
                hideHelpTab {
                return 0
            }
            return 1
        }else if VIAApplicableFeatures.default.showMembershipBanner!{
            return 1
        }else{
            return 0
        }
    }
    
    // MARK: UITableView delegate
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let section = Sections(rawValue: indexPath.section)
        
        switch section! {
        case .MembershipDetails:
            return configureDetailCell(tableView, cellForRowAt: indexPath)
        case .Banner:
            return configureBannerCell(tableView, cellForRowAt: indexPath)
        case .Help:
            return configureMenuCell(tableView, indexPath: indexPath)
        case .DigitalPass:
            if let hideMembershipPassAndUpdateTitle = VIAApplicableFeatures.default.hideMembershipPassAndUpdateTitle,
                hideMembershipPassAndUpdateTitle{
                return UITableViewCell()
            }else{
                return configureDigitalPassCell(tableView, indexPath: indexPath)
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let section = Sections(rawValue: section)
    
        let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier) as! VIATableViewSectionHeaderFooterView
        view.set(font: .title3Font())
        view.set(textColor: .night())

        if section == .MembershipDetails {
            view.configureWithExtraSpacing(labelText: CommonStrings.Settings.MembershipDetailsSectionHeading965, extraTopSpacing: 28, extraBottomSpacing: 0)
        } else if section == .DigitalPass {
            view.configureWithExtraSpacing(labelText: CommonStrings.Settings.MembershipDigitalPassSectionHeading970, extraTopSpacing: 28, extraBottomSpacing: 0)
        }
        return view
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier) as! VIATableViewSectionHeaderFooterView
        view.textLabel?.font = UIFont.title1Font()
        view.configureWithExtraSpacing(labelText: nil, extraTopSpacing: 0, extraBottomSpacing: 50)
        return view
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let section = Sections(rawValue: indexPath.section)
        //TODO update perform segue strings and store it somewhere within class
        switch section! {
        case .MembershipDetails:
            let cell: VIAProfileDetailCell = tableView.cellForRow(at: indexPath) as! VIAProfileDetailCell
            if cell.accessoryType != .none {
                selectedMembershipItemRow = indexPath.row
                self.performSegue(withIdentifier: showMembershipInfoIdentifier, sender: nil)
            }
            break
        case .Help:
            self.performSegue(withIdentifier: showHelpInfoIdentifier, sender: nil)
            break
        case .DigitalPass:
            #if DEBUG
                self.userDidSelectDigitalPassRow()
            #endif
            break
        case .Banner:
            break
        }
    }
    
    override func tableView(_ tableView: UITableView, accessoryButtonTappedForRowWith indexPath: IndexPath) {
        //TODO update perform segue strings and store it somewhere within class
        let section = Sections(rawValue: indexPath.section)
        //TODO update perform segue strings and store it somewhere within class
        switch section! {
        case .MembershipDetails:
            let cell: VIAProfileDetailCell = tableView.cellForRow(at: indexPath) as! VIAProfileDetailCell
            if cell.accessoryType != .none {
                selectedMembershipItemRow = indexPath.row
                self.performSegue(withIdentifier: showMembershipInfoIdentifier, sender: nil)
            }
            break
        case .Help:
            self.performSegue(withIdentifier: showHelpInfoIdentifier, sender: nil)
            break
        case .DigitalPass:
            self.userDidSelectDigitalPassRow()
            break
        case .Banner:
            break
        }
    }
    
    func userDidSelectDigitalPassRow() {
        if self.isDigitalPassInWallet() {
            showManagePassActionSheet()
        } else {
            if let avatarPath = profileViewModel.currentPerson.avatarPath, FileManager.default.fileExists(atPath: avatarPath) {
                //TODO: Get data of image and user for Digital Pass.
//                if let data = FileManager.default.contents(atPath: avatarPath) {
//                    cell.photo = UIImage(data: data)
//                }
                self.selectDigitalPassForUserWithExistingProfileImage()
            } else {
                self.selectDigitalPassForUserWithNoExistingProfileImage()
            }
            
        }
    }
    
    func initializeUserPass() {
        if PKPassLibrary.isPassLibraryAvailable() {
            debugPrint("This pass library is available")
            if let url = Bundle.main.url(forResource: "membershipPass", withExtension: "pkpass") {
                do {
                    let passData = try Data(contentsOf:url)
                    digitalPass = PKPass.init(data: passData, error: nil)
                    if passLibrary == nil {
                        passLibrary = PKPassLibrary.init()
                    }
                } catch {
                    debugPrint(error)
                }
            } else {
                debugPrint("Pass file can't be found")
            }
        } else {
            debugPrint("Pass library is currently not available.")
        }
    }
    
    func isDigitalPassInWallet() -> Bool {
        if passLibrary != nil {
            if digitalPass != nil {
                return (passLibrary?.containsPass(digitalPass!))!
            }
        }
        return false
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let section = Sections(rawValue: indexPath.section)
        
        switch section! {
        case .Banner:
            if VIAApplicableFeatures.default.showMembershipBanner!{
                return UITableViewAutomaticDimension
            }else{
                return 0
            }
        case .DigitalPass:
            if let hideMembershipPassAndUpdateTitle = VIAApplicableFeatures.default.hideMembershipPassAndUpdateTitle,
                hideMembershipPassAndUpdateTitle{
                    return 0
            }else{
                return self.isDigitalPassInWallet() ? UITableViewAutomaticDimension : 115
            }
        default:
            return UITableViewAutomaticDimension
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        let headerSection = Sections(rawValue: section)
        
        if headerSection == .Banner {
            return CGFloat.leastNormalMagnitude
        }else if headerSection == .DigitalPass{
            //check if its uke
            if !VIAApplicableFeatures.default.showMembershipBanner!{
                return 0
            }else if let hideMembershipPassAndUpdateTitle = VIAApplicableFeatures.default.hideMembershipPassAndUpdateTitle,
                hideMembershipPassAndUpdateTitle{
                return 0
            }
        }
        
        return UITableViewAutomaticDimension
    }

    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        let footerSection = Sections(rawValue: section)
        
        if footerSection == .Help {
            if let hideHelpTab = VIAApplicableFeatures.default.hideHelpTab,
                hideHelpTab {
                return CGFloat.leastNormalMagnitude
            }
            
            return UITableViewAutomaticDimension
        }
        
        return UITableViewAutomaticDimension //CGFloat.leastNormalMagnitude
    }
    
    //MARK: Apple Pass Methods
    func displayPassInWallet() {
        if self.isDigitalPassInWallet() {
            if UIApplication.shared.canOpenURL((digitalPass?.passURL!)!) {
                UIApplication.shared.open((digitalPass?.passURL!)!, options: [:], completionHandler: nil)
            }
        } else {
            initializeUserPass()
            addPassController = PKAddPassesViewController.init(pass: digitalPass!)
            addPassController?.delegate = self
            self.navigationController?.present(addPassController!, animated: true, completion: nil)
        }
    }
    
    //MARK: Custom Cells
    func configureBannerCell(_ tableview: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableview.dequeueReusableCell(withIdentifier: VIAProfileMembershipPassBannerTableViewCell.defaultReuseIdentifier, for: indexPath) as! VIAProfileMembershipPassBannerTableViewCell
        //cell.logoImage = VIACoreAsset.Profile.carrierBrandingAreaGray.image.overlayImage(color: UIColor.currentGlobalTintColor())
        cell.inAspectRatio = VIAApplicableFeatures.default.showBannerInAspectRatio ?? false
        cell.logoImage = VIAApplicableFeatures.default.getMembershipBannerImage()
        return cell
    }
    
    func configureDetailCell(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: VIAProfileDetailCell.defaultReuseIdentifier, for: indexPath) as! VIAProfileDetailCell
        let willDisplayMembership = !(VIAApplicableFeatures.default.userPartyIDAsVitalityNumber ?? true) && (VIAApplicableFeatures.default.showPartyIdDetail() ?? false)
        let enumIndex = !willDisplayMembership ? indexPath.row > 0 ? indexPath.row + 2 : indexPath.row : indexPath.row
        let row = MembershipItems(rawValue: enumIndex)
        
        cell.selectionStyle = .none
        let coreRealm = DataProvider.newRealm()
        if row == .VitalityNumber {
            if VIAApplicableFeatures.default.userPartyIDAsVitalityNumber ?? false{
                cell.valueText = String(coreRealm.getPartyId())//coreRealm.getPartyId().description
            }else{
                cell.valueText = String(coreRealm.getMembershipId())//coreRealm.getMembershipId().description
            }
            if VIAApplicableFeatures.default.showMembershipPassInfoButton!{
                cell.accessoryType = .detailButton
            }
        } else if row == .VitalityStatus {
            cell.valueText = homeViewModel.latestHomeScreenData()?.statusTitle()
        } else if row == .MembershipStartDate {
            cell.valueText = Localization.dayDateLongMonthyearFormatter.string(from: (VitalityPartyMembership.currentVitalityMembershipPeriod()?.effectiveFrom)!)
        } else if row == .MembershipStatus {
            cell.valueText = self.isOutOfCurrentMembershipPeriod(date: DataProvider.currentMembershipPeriodEndDate()!) ? CommonStrings.ProfileMembershipInactiveStatusDescription1127 : CommonStrings.ProfileMembershipActiveStatusDescription1126
        }
        else if row == .MembershipNumber {
            cell.valueText = String(coreRealm.getMembershipId())
            cell.accessoryType = .detailButton
        }
        else if row == .CustomerNumber {
            cell.valueText = String(coreRealm.getPartyId())
            cell.accessoryType = .detailButton
        }
        
        cell.labelText = row?.title()
        
        
        return cell
    }
    
    func isOutOfCurrentMembershipPeriod(date: Date) -> Bool {
        guard let membershipStartDate = DataProvider.currentMembershipPeriodStartDate() else { return true }
        guard let membershipEndDate = DataProvider.currentMembershipPeriodEndDate() else { return true }
        
        if date < membershipStartDate || date > membershipEndDate {
            return true
        }
        
        return false
    }
    
    func configureMenuCell(_ tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCell(withIdentifier: VIATableViewCell.defaultReuseIdentifier, for: indexPath)
        cell = VIATableViewCell.init(style: UITableViewCellStyle.default, reuseIdentifier: VIATableViewCell.defaultReuseIdentifier)
        cell.textLabel?.text = CommonStrings.ProfileHelpTitle
        cell.imageView?.image = VIACoreAsset.Profile.profileHelp.templateImage
        cell.imageView?.tintColor = UIColor.currentGlobalTintColor()
        cell.accessoryType = .disclosureIndicator
        
        return cell
    }
    
    func configureDigitalPassCell(_ tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        #if DEBUG
            if self.isDigitalPassInWallet() {
                var cell = tableView.dequeueReusableCell(withIdentifier: VIATableViewCell.defaultReuseIdentifier, for: indexPath)
                cell = VIATableViewCell.init(style: UITableViewCellStyle.subtitle, reuseIdentifier: VIATableViewCell.defaultReuseIdentifier)
                let person = profileViewModel.currentPerson
                let givenName = person.givenName
                
                cell.textLabel?.text = String(format: "%@'s Digital Pass", givenName)
                cell.textLabel?.tintColor = UIColor.currentGlobalTintColor()
                cell.detailTextLabel?.text = "Manage"
                cell.detailTextLabel?.font = UIFont.tableViewHeaderFooter()
                cell.detailTextLabel?.textColor = UIColor.currentGlobalTintColor()
                cell.imageView?.image = nil
                cell.accessoryType = .none
                return cell
            }
            // cell layout for users with no added digital pass.
            let cell = tableView.dequeueReusableCell(withIdentifier: VIAProfileDigitalPassTableViewCell.defaultReuseIdentifier, for: indexPath) as! VIAProfileDigitalPassTableViewCell
            cell.digitalPassDescription = CommonStrings.ProfileMembershipPassDescription
            cell.selectionStyle = .none
        #else
            let cell = tableView.dequeueReusableCell(withIdentifier: VIAProfileDigitalPassTableViewCell.defaultReuseIdentifier, for: indexPath) as! VIAProfileDigitalPassTableViewCell            
            cell.digitalPassDescription = CommonStrings.ProfileMembershipPassDescription
            cell.hidePKAddPassButton = true
            cell.selectionStyle = .none
        #endif
        
            
        
        
        return cell
        
    }
    
    //MARK: PKAddPassesViewControllerDelegate
    func addPassesViewControllerDidFinish(_ controller: PKAddPassesViewController) {
        self.dismiss(animated: false) {
            self.addPassController?.delegate = nil
            self.addPassController = nil
        }
        self.tableView.reloadData()
    }
    
    //MARK: ImagePickerDelegate
    func pass(imageInfo: [String : Any]) {
        //TODO: Come back to this once device is available to test if the use photo for camera is being shown since this is not present in the simulator.
        let fileName = UInt(Date().timeIntervalSince1970 * 1000)
        let image = imageInfo[UIImagePickerControllerOriginalImage] as! UIImage
        
        if var fileURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            fileURL.appendPathComponent("\(fileName)")
            
            do {
                if let pngImageData = UIImagePNGRepresentation(image) {
                    fileURL.appendPathExtension("png")
                    try pngImageData.write(to: fileURL, options: .atomic)
                    profileViewModel.saveAvatar(path: fileURL.path)
                }
                imagePicker?.dismissImagePicker()
                
            } catch {
                
                print("SaveFailed: \(error)")
                imagePicker?.dismissImagePicker()
            }
        }
    }
    
    func dismissComplete() {
        imagePicker?.delegate = nil
        imagePicker?.optionalDelegate = nil
        imagePicker = nil
        //TODO: Update this to show alert only for success.
        self.showDownloadingAlertController()
    }
    
    func show(imageSouceSelector: UIAlertController) {
        imageSouceSelector.view.tintColor = UIColor.genericErrorAlertButtonText()
        self.navigationController?.present(imageSouceSelector, animated: true, completion: nil)
    }
    
    func present(imagePicker: UIImagePickerController) {
        imagePicker.view.tintColor = UIColor.genericErrorAlertButtonText()
        self.navigationController?.present(imagePicker, animated: true, completion: nil)
    }
}



