import VitalityKit
import VIAUIKit
import VIAUtilities
import VIAActiveRewards
import VIACommon
protocol ProfileSettingsDelegate: VIAFlowDelegate {
    
    
}

class ProfileSettingsViewController: VIACoordinatedTableViewController, PrimaryColorTintable {
    
    // MARK: Public (internal)
    @IBAction func unwindToProfileSettingsViewController(segue:UIStoryboardSegue) { }
    
    weak var delegate: ProfileSettingsDelegate?
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = CommonStrings.Settings.LandingSettingsTitle2447
        
        self.configureAppearance()
        self.configureTableView()
    }
    
    func configureAppearance() {
        self.hideBackButtonTitle()
        self.tableView.backgroundColor = UIColor.tableViewBackground()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == SettingsMenuItems.Privacy.segueIdentifier() {
            self.hideBackButtonTitle()
        } else if segue.identifier == SettingsMenuItems.Terms.segueIdentifier() {
            self.setBackButtonTitle(CommonStrings.Settings.LandingTitle2448)
            if let webContentViewController = segue.destination as? VIAWebContentViewController,
                let articleId = AppConfigFeature.contentId(for: .TermsAndConditionsContent) {
                
                let webContentViewModel = VIAWebContentViewModel(articleId: articleId)
                webContentViewController.viewModel = webContentViewModel
                webContentViewController.title = CommonStrings.Settings.TermsConditionsTitle905
            }
        } else if segue.identifier == FeedbackMenuItems.Feedback.segueIdentifier() {
            self.hideBackButtonTitle()
            let webContentViewController = segue.destination as? VIAWebContentViewController
            
            let articleId = "profile-settings-feedback"
            
            let webContentViewModel = VIAWebContentViewModel(articleId: articleId)
            webContentViewController?.viewModel = webContentViewModel
            webContentViewController?.title = CommonStrings.Settings.FeedbackSectionTitle949
        } else {
            self.hideBackButtonTitle()
        }
    }
    
    func configureTableView() {
        self.tableView.register(VIATableViewCell.self, forCellReuseIdentifier: VIATableViewCell.defaultReuseIdentifier)
        self.tableView.register(VIATableViewSectionHeaderFooterView.nib(), forHeaderFooterViewReuseIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier)
        self.tableView.estimatedRowHeight = 75
        self.tableView.backgroundColor = UIColor.tableViewBackground()
        self.tableView.tableFooterView = UIView()
    }
    
    // MARK: UITableView data source
    
    enum Sections: Int, EnumCollection {
        case SettingsMenu = 0
        case FeedbackMenu = 1
        case Logout = 2
        
        public static func count() -> Int{
            return [Sections.SettingsMenu, Sections.FeedbackMenu, Sections.Logout].count
        }
    }
    
    enum SettingsMenuItems: Int, EnumCollection {
        case Communication = 0
        case Privacy = 1
        // Removed units as per user journey
        //case Units = 2
        case Security = 2
        case Terms = 3
        
        func title() -> String? {
            switch self {
            case .Communication:
                return CommonStrings.Settings.CommunicationTitle902
            case .Privacy:
                return CommonStrings.Settings.PrivacyTitle903
                // Removed units as per user journey
                //    case .Units:
            //    return CommonStrings.SettingsUnitsTitle
            case .Security:
                return CommonStrings.Settings.SecurityTitle904
            case .Terms:
                return CommonStrings.Settings.TermsConditionsTitle905
            }
        }
        
        func image() -> UIImage? {
            switch self {
            case .Communication:
                return UIImage.templateImage(asset: .profileCommunicationPreferences)
            case .Privacy:
                return UIImage.templateImage(asset: .profilePrivacy)
                // Removed units as per user journey
                // case .Units:
            //      return UIImage.templateImage(asset: .profileUnitPreferences)
            case .Security:
                return UIImage.templateImage(asset: .profileSecurity)
            case .Terms:
                return UIImage.templateImage(asset: .profileTermsAndCondition)
            }
        }
        
        func segueIdentifier() -> String {
            switch self {
            case .Communication:
                return "showCommunicationSettings"
            case .Privacy:
                return "showPrivacySettings"
                // Removed units as per user journey
                //  case .Units:
            //      return "showUnitsSettings"
            case .Security:
                return "showSecuritySettings"
            case .Terms:
                return "showTermsAndConditions"
            }
        }
    }
    
    enum FeedbackMenuItems: Int, EnumCollection {
        case Feedback = 0
        case Rate = 1
        
        func title() -> String? {
            switch self {
            case .Feedback:
                return CommonStrings.Settings.FeedbackTitle906
            case .Rate:
                return CommonStrings.Settings.RateUsTitle907
            }
        }
        
        func image() -> UIImage? {
            switch self {
            case .Feedback:
                return UIImage.templateImage(asset: .profileProvideFeedback)
            case .Rate:
                return UIImage.templateImage(asset: .profileRateUs)
            }
        }
        
        func segueIdentifier() -> String {
            switch self {
            case .Feedback:
                
                if VIAApplicableFeatures.default.shouldPullProvideFeedbackContentFromCMS!{
                    return "showProvideFeedbackCMS"
                }
                else{
                    return "showProvideFeedback"
                }
                
            case .Rate:
                return "showRate"
            }
        }
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return Sections.count()
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let section = Sections(rawValue: section)
        if section == .SettingsMenu {
            if (VIAApplicableFeatures.default.changeMenuItemLocation)!{
                return SettingsMenuItems.allValues.count - 1
            }
            return SettingsMenuItems.allValues.count
        } else if section == .FeedbackMenu {
            if (VIAApplicableFeatures.default.changeMenuItemLocation)!{
                return 0
            }
            return FeedbackMenuItems.allValues.count
        } else if section == .Logout {
            if (VIAApplicableFeatures.default.changeMenuItemLocation)!{
                return 0
            }
            return 1
        }
        return 0
    }
    
    // MARK: UITableView delegate
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let section = Sections(rawValue: indexPath.section)
        
        let cell = tableView.dequeueReusableCell(withIdentifier: VIATableViewCell.defaultReuseIdentifier, for: indexPath)
        cell.imageView?.tintColor = UIColor.currentGlobalTintColor()
        
        if section == .SettingsMenu {
            let menuItem = SettingsMenuItems(rawValue: indexPath.row)
            cell.textLabel?.text = menuItem?.title()
            cell.imageView?.image = menuItem?.image()
        } else if section == .FeedbackMenu {
            let menuItem = FeedbackMenuItems(rawValue: indexPath.row)
            cell.textLabel?.text = menuItem?.title()
            cell.imageView?.image = menuItem?.image()
        } else if section == .Logout {
            cell.textLabel?.text = CommonStrings.Settings.LogoutTitle908
            cell.imageView?.image = VIACoreAsset.Profile.profileLogout.templateImage
        }
        
        cell.accessoryType = .disclosureIndicator
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier) as! VIATableViewSectionHeaderFooterView
        view.labelText = " "
        return view
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let section = Sections(rawValue: indexPath.section)
        if section == .SettingsMenu {
            let menuItem = SettingsMenuItems(rawValue: indexPath.row)
            guard let segueIdentifier = menuItem?.segueIdentifier() else { return }
            self.performSegue(withIdentifier: segueIdentifier, sender: nil)
        } else if section == .FeedbackMenu {
            let menuItem = FeedbackMenuItems(rawValue: indexPath.row)
            
            if menuItem == .Rate {
                let appId = Bundle.main.object(forInfoDictionaryKey: "VAiTunesIdentifier") as? String ?? "Unknown"
                doRateApp(appId: appId)
            }else if menuItem == .Feedback {
                
                guard let feedBackSegueIdentifier = menuItem?.segueIdentifier() else { return }
                self.performSegue(withIdentifier: feedBackSegueIdentifier, sender: nil)
            } else {
                //                    guard let segueIdentifier = menuItem?.segueIdentifier() else { return }
                //                    self.performSegue(withIdentifier: segueIdentifier, sender: nil)
            }
        } else if section == .Logout {
            logout()
        }
    }
    
    func logout() {
        let alert = UIAlertController(title: CommonStrings.Settings.AlertLogoutTitle909, message: CommonStrings.Settings.AlertLogoutMessage910, preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: CommonStrings.CancelButtonTitle24, style: UIAlertActionStyle.cancel, handler: { (UIAlertAction) in
        })
        let continueAction = UIAlertAction(title: CommonStrings.Settings.LogoutTitle908, style: UIAlertActionStyle.default, handler: { [weak self] (UIAlertAction) in
            self?.doLogout()
        })
        alert.view.tintColor = UIButton().tintColor
        alert.addAction(cancelAction)
        alert.addAction(continueAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func doLogout() {
        VIAApplicableFeatures.default.navigateToLoginScreen()
    }
    
    func doRateApp(appId: String) {
        
        if let url = URL(string: "https://itunes.apple.com/app/id\(appId)") {
            
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: { (success) in
                    print("User sent to rate \(url): with \(success)")
                })
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
}
