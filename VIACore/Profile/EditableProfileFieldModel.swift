//
//  EditableProfileFieldModel.swift
//  VitalityActive
//
//  Created by Joshua Ryan on 7/19/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation

enum EditProfileFieldType: Int {

	case Email = 0
	case Phone = 1
}

protocol EditableProfileFieldModel {

	var existingValue: String { get }
	var type: EditProfileFieldType { get }

	var updatedValue: String { get set }

}
