//
//  ProfileMembershipPassController+Helpers.swift
//  VitalityActive
//
//  Created by Ma. Catherine Purganan on 23/11/2017.
//  Copyright © 2017 Glucode. All rights reserved.
//
import VitalityKit
import PassKit

var passLibrary: PKPassLibrary? = nil //This should be persistent so that the pass need not be added every time you go to the page.
var digitalPass: PKPass? = nil

extension ProfileMembershipPassViewController: PKAddPassesViewControllerDelegate {
    
    func createCancelAlertAction(for alert: UIAlertController) -> UIAlertAction {
        let cancelAction = UIAlertAction(title: CommonStrings.CancelButtonTitle24, style: UIAlertActionStyle.cancel, handler: nil)
        return cancelAction
    }
    
    func presentAlertWithTwoActions(for alert: UIAlertController, initialAction: UIAlertAction, finalAction: UIAlertAction) {
        alert.view.tintColor = UIColor.genericErrorAlertButtonText()
        alert.addAction(initialAction)
        alert.addAction(finalAction)
        self.navigationController?.present(alert, animated: true, completion: nil)
    }
    
    func presentAlertWithThreeActions(for alert: UIAlertController, firstAction: UIAlertAction, secondAction: UIAlertAction, finalAction: UIAlertAction) {
        alert.view.tintColor = UIColor.genericErrorAlertButtonText()
        alert.addAction(firstAction)
        alert.addAction(secondAction)
        alert.addAction(finalAction)
        self.navigationController?.present(alert, animated: true, completion: nil)
    }
    
    
    //MARK: Download Digital Pass Alerts
    func showDownloadDigitalPassErrorAlert() {
        let alertController = UIAlertController(title: CommonStrings.PrivacyPolicy.UnableToCompleteAlertTitle115, message: CommonStrings.ProfileDigitalPassAlertDescripion, preferredStyle: .alert)
        let tryAgainAction = UIAlertAction(title: CommonStrings.TryAgainButtonTitle43, style: UIAlertActionStyle.default) { (UIAlertAction) in
            alertController.dismiss(animated: false, completion: {
                //TODO: Insert try again here
            })
        }
        
        let cancelAction = createCancelAlertAction(for: alertController)
        self.presentAlertWithTwoActions(for: alertController, initialAction: cancelAction, finalAction: tryAgainAction)
    }
    
    func showDownloadingAlertController() {
        self.showHUDOnWindow(with: CommonStrings.Settings.DownloadingPassTitle980)
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + .seconds(4)) {
            self.hideHUDFromWindow()
//            self.showDownloadDigitalPassErrorAlert()
            self.displayPassInWallet()
        }
    }
    
    //MARK: Check User Pass Alerts
    func selectDigitalPassForUserWithNoExistingProfileImage() {
        let alertController = UIAlertController(title: CommonStrings.Settings.AlertProvideProfileImageTitle974, message: CommonStrings.Settings.AlertProvideProfileImageMessage975, preferredStyle: .alert)
        
        let addImageNowAction = UIAlertAction(title: CommonStrings.ProfileDigitalPassAddNowPrompt, style: UIAlertActionStyle.default) { (UIAlertAction) in
            alertController.dismiss(animated: false, completion: nil)
            self.imagePicker?.selectImage()
        }
        
        let addImageLaterAction = UIAlertAction(title: CommonStrings.ProfileDigitalPassAddLaterPrompt, style: UIAlertActionStyle.default) { (UIAlertAction) in
            alertController.dismiss(animated: false, completion: nil)
            self.showDownloadingAlertController()
        }
        
        let cancelAction = createCancelAlertAction(for: alertController)
        
        self.presentAlertWithThreeActions(for: alertController, firstAction: addImageNowAction, secondAction: addImageLaterAction, finalAction: cancelAction)
    }
    
    func selectDigitalPassForUserWithExistingProfileImage() {
        let alertController = UIAlertController(title: CommonStrings.Settings.AlertProvideProfileImageTitle974, message: CommonStrings.Settings.AlertProvideProfileImageMessage975, preferredStyle: .alert)
        let useExistingImageAction = UIAlertAction(title: CommonStrings.Settings.AlertUseExistingButton976, style: UIAlertActionStyle.default) { (UIAlertAction) in
            alertController.dismiss(animated: false, completion: nil)
            self.showDownloadingAlertController()
        }
        
        let changeImageAction = UIAlertAction(title: CommonStrings.Settings.AlertChangeImageButton977, style: UIAlertActionStyle.default) { (UIAlertAction) in
            alertController.dismiss(animated: false, completion: nil)
            self.imagePicker?.selectImage()
            
        }
        
        let cancelAction = createCancelAlertAction(for: alertController)
        self.presentAlertWithThreeActions(for: alertController, firstAction: useExistingImageAction, secondAction: changeImageAction, finalAction: cancelAction)
    }
    
    //MARK: Remove Pass Alerts
    func showRemoveConfirmationAlert() {
        let alertController = UIAlertController(title: CommonStrings.Settings.AlertRemovePassTitle990, message: CommonStrings.Settings.AlertRemovePassMessage991, preferredStyle: .alert)
        
        let cancelAction = self.createCancelAlertAction(for: alertController)
        
        let removePassAction = UIAlertAction(title: CommonStrings.Settings.AlertRemovePassButton992, style: UIAlertActionStyle.destructive) { (UIAlertAction) in
            alertController.dismiss(animated: false, completion: nil)
            passLibrary?.removePass(digitalPass!)
            digitalPass = nil
            self.tableView.reloadData()
        }
        
        self.presentAlertWithTwoActions(for: alertController, initialAction: cancelAction, finalAction: removePassAction)
    }
    
    func showManagePassActionSheet() {
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let removePassAction = UIAlertAction(title: CommonStrings.Settings.ActionRemovePassButton993, style: UIAlertActionStyle.destructive) { (UIAlertAction) in
            alertController.dismiss(animated: false, completion: nil)
            self.showRemoveConfirmationAlert()
        }
        
        let showPassAction = UIAlertAction(title: CommonStrings.Settings.ActionShowWalletButton994, style: UIAlertActionStyle.default) { (UIAlertAction) in
            alertController.dismiss(animated: false, completion: nil)
            self.displayPassInWallet()
        }
        let cancelAction = self.createCancelAlertAction(for: alertController)
        
        self.presentAlertWithThreeActions(for: alertController, firstAction: removePassAction, secondAction: showPassAction, finalAction: cancelAction)
    }
}
