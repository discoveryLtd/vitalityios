//
//  UploadProfileImageHelper.swift
//  VIACore
//
//  Created by OJ Garde on 8/14/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import VitalityKit
import Foundation
import Security

// Keychain Constant Identifiers
let profilePicAccount: NSString = "ProfilePicture"
let profilePicKey: NSString = "ReferenceId"

// Arguments for the keychain queries
let kSecClassValue = NSString(format: kSecClass)
let kSecAttrAccountValue = NSString(format: kSecAttrAccount)
let kSecValueDataValue = NSString(format: kSecValueData)
let kSecClassGenericPasswordValue = NSString(format: kSecClassGenericPassword)
let kSecAttrServiceValue = NSString(format: kSecAttrService)
let kSecMatchLimitValue = NSString(format: kSecMatchLimit)
let kSecReturnDataValue = NSString(format: kSecReturnData)
let kSecMatchLimitOneValue = NSString(format: kSecMatchLimitOne)

public class ProfileImageHelper{
    
    public class func getReferenceId(tenantId: Int, partyId: Int, completion: @escaping (_ success:Bool, _ referenceId:String?)->Void){
        Wire.Content.getPreferencesByPartyId(tenantId: tenantId, partyId: partyId, completion: { (error, referenceId) in
            completion(error == nil, referenceId)
        })
    }
    
    public class func delete(referenceId: String, completion: @escaping (_ success:Bool)->Void){
        Wire.Content.deleteFileByReferenceId(referenceId: referenceId, completion: { (error) in
            completion(error == nil)
        })
    }
    
    public class func upload(partyID: Int, data: Data, completion: @escaping (_ success:Bool, _ referenceId:String?)->Void){
        Wire.Content.uploadProfileImage(partyId: partyID, fileContents: data, completion: { (fileUploadReference, error) in
            completion(error == nil, fileUploadReference)
        })
    }
    
    public class func saveReferenceId(tenantId: Int, partyId: Int, value: String, completion: @escaping (_ success:Bool)->Void){
        
        let request = MaintainPartyPreferenceRequest(effectiveFrom: Date(), value: value)
        Wire.Content.maintainPartyPreference(tenantId: tenantId, partyId: partyId, request: request, completion: { (error) in
            completion(error == nil)
        })
    }
    
    public class func getFile(partyId: Int, completion: ((_: UIImage?) -> ())?){
        guard let imageURL = AppSettings.getProfileImageUrl() else {
            retrieveImage(partyId: partyId, completion: completion)
            return
        }
        guard let data = FileManager.default.contents(atPath: imageURL) else {
            retrieveImage(partyId: partyId, completion: completion)
            return
        }
        guard let image = UIImage(data: data) else {
            retrieveImage(partyId: partyId, completion: completion)
            return
        }
        debugPrint("Profile image retrieved from cache")
        completion?(image)
    }
    
    public class func retrieveImage(partyId: Int, completion: ((_: UIImage?) -> ())?){
        let fileName = "\(partyId)_profile.jpg"
        Wire.Content.getFileByPartyId(partyId: partyId) { (data, error) in
            if nil == error{
                saveImage(fileName: fileName, data: data, completion: completion)
            }else{
                completion?(nil)
            }
        }
    }
    
    private class func saveImage(fileName: String, data: Data?, completion: ((_:UIImage?) -> ())?){
        guard let imageData = data, let image = UIImage(data: imageData) else { return }
        if var fileURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            fileURL.appendPathComponent("\(fileName)")
            
            do {
                if let jpgImageData = UIImageJPEGRepresentation(image, 1.0) {
                    try jpgImageData.write(to: fileURL, options: .atomic)
                    AppSettings.setProfileImage(url: fileURL.path)
                    completion?(image)
                }else{
                    completion?(nil)
                }
            } catch {
                completion?(nil)
            }
        }
    }
}

// MARK: Keychain
extension ProfileImageHelper{
    
    /**
     * Exposed methods to perform save and load queries.
     */
    
    public class func storeToKeychain(referenceId: NSString) {
        self.save(referenceIdKey: profilePicKey, data: referenceId)
    }
    
    public class func loadFromKeychain() -> NSString? {
        return self.load(referenceIdKey: profilePicKey)
    }
    
    /**
     * Internal methods for querying the keychain.
     */
    
    private class func save(referenceIdKey: NSString, data: NSString) {
        
        let dataFromString: NSData = data.data(using: String.Encoding.utf8.rawValue, allowLossyConversion: false)! as NSData
        
        // Instantiate a new default keychain query
        let keychainQuery: NSMutableDictionary = NSMutableDictionary(objects: [kSecClassGenericPasswordValue, referenceIdKey, profilePicAccount, dataFromString], forKeys: [kSecClassValue, kSecAttrServiceValue, kSecAttrAccountValue, kSecValueDataValue])
        
        // Delete any existing items
        SecItemDelete(keychainQuery as CFDictionary)
        
        // Add the new keychain item
        SecItemAdd(keychainQuery as CFDictionary, nil)
        
    }
    
    private class func load(referenceIdKey: NSString) -> NSString? {
        // Instantiate a new default keychain query
        // Tell the query to return a result
        // Limit our results to one item
        let keychainQuery: NSMutableDictionary = NSMutableDictionary(objects: [kSecClassGenericPasswordValue, referenceIdKey, profilePicAccount, kCFBooleanTrue, kSecMatchLimitOneValue], forKeys: [kSecClassValue, kSecAttrServiceValue, kSecAttrAccountValue, kSecReturnDataValue, kSecMatchLimitValue])
        
        var dataTypeRef :AnyObject?
        
        // Search for the keychain items
        let status: OSStatus = SecItemCopyMatching(keychainQuery, &dataTypeRef)
        var contentsOfKeychain: NSString? = nil
        
        if status == errSecSuccess {
            if let retrievedData = dataTypeRef as? NSData {
                contentsOfKeychain = NSString(data: retrievedData as Data, encoding: String.Encoding.utf8.rawValue)
            }
        } else {
            print("Nothing was retrieved from the keychain. Status code \(status)")
        }
        
        return contentsOfKeychain
    }
    
}

