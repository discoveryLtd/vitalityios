//
//  EventsFeedMonthEventsCollectionViewCell.swift
//  VitalityActive
//
//  Created by OJ Garde on 10/11/2017.
//  Copyright © 2017 Glucode. All rights reserved.
//

import UIKit
import VIAUtilities

public class EventsFeedMonthEventsCollectionViewCell: UICollectionViewCell {
    
    // MARK: Lifecycle
    
    public override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    public override func prepareForReuse() {
        super.prepareForReuse()
        self.tableViewController.refreshControl?.endRefreshing()
    }
    
    // MARK: Properties
    
    public var didSelectItem: (Any?) -> Void = { item in
        debugPrint("EventsFeedMonthEventsCollectionViewCell didSelectItem \(String(describing: item))")
    }
    
    func didSelectThing(thing: Any?) {
        self.didSelectItem(thing)
    }
    
    public var date = Date() {
        didSet {
            self.tableViewController.date = self.date
        }
    }
    
    public var selectedCategories = [EventCategoryRef]() {
        didSet {
            self.tableViewController.selectedCategories = self.selectedCategories
        }
    }
    
    public weak var refreshDelegate: EventsFeedMonthRefreshDelegate? {
        didSet {
            self.tableViewController.refreshDelegate = self.refreshDelegate
        }
    }
    
    private lazy var tableViewController: EventsFeedMonthTableViewController! = {
        let controller = EventsFeedMonthTableViewController(style: .grouped)
        controller.didSelectItem = self.didSelectThing
        return controller
    }()
    
    // MARK: Init & view
    
    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) r not been implemented")
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.contentView.backgroundColor = .white
    }
    
    deinit{
        debugPrint("[EventsFeedMonthEventsCollectionViewCell] deinit")
        self.tableViewController = nil
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        
        setup()
    }
    
    func setup() {
        guard let tableViewController = self.tableViewController else {
            debugPrint("tableViewController doesn't exist yet")
            return
        }
        
        if !self.contentView.subviews.contains(tableViewController.view) {
            self.contentView.addSubview(tableViewController.view)
        }
        
        tableViewController.view.snp.removeConstraints()
        tableViewController.view.snp.makeConstraints({ (make) in
            make.width.height.equalToSuperview()
            make.left.top.right.bottom.equalToSuperview()
        })
    }
    
}
