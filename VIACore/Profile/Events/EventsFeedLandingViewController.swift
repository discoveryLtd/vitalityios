//
//  EventsFeedLandingViewController.swift
//  VitalityActive
//
//  Created by OJ Garde on 10/11/2017.
//  Copyright © 2017 Glucode. All rights reserved.
//

import UIKit
import VIAUIKit
import VitalityKit
import VIAUtilities
import VIACommon

import SnapKit
import RealmSwift

class EventsFeedLandingViewController: VIAViewController {
    
    let superwide = 10_000
    
    // MARK: Properties
    
    let filterButton = UIButton()
    
    var monthPickerView = PointsMonitorMonthPickerView.viewFromNib(owner: self)!
    
    let collectionView = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
    
    var didScrollToStartingPosition = false
    
    var activeIndexPath = IndexPath(item: 0, section: 0)
    
    var selectedCategories = [EventCategoryRef]()
    
    var shouldPerformDataRequest: Bool = true
    
    var realm = DataProvider.newRealm()
    
    
    // MARK: View lifecycle
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupTabBarTitle()
        resetBackButtonTitle()
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setupTabBarTitle()
        resetBackButtonTitle()
    }
    
    deinit{
        debugPrint("[EventsFeedLandingViewController] Deallocate")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = CommonStrings.MenuPointsButton6
        configureTitleView()
        configureSubviews()
        if AppSettings.getAppTenant() != .CA{
            EventsFeedCategoryUtil.addCategoriesToRealm()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.makeNavigationBarTransparent()
        
        if shouldPerformDataRequest {
            shouldPerformDataRequest = false
            performInitialRequest()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        debugPrint("[EventsFeedLandingViewController] viewWillDisappear")
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        self.dismiss(animated: false, completion: nil)
        //        dch_checkDeallocation(afterDelay: 30)
        debugPrint("[EventsFeedLandingViewController] viewDidDisappear")
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        scrollCollectionViewToStartingPositionIfNeeded()
    }
}


// MARK: Actions
extension EventsFeedLandingViewController{
    
    func reloadData() {
        didScrollToStartingPosition = false
        scrollCollectionViewToStartingPositionIfNeeded()
        collectionView.reloadItems(at: [activeIndexPath])
    }
    
    func monthPickerViewDidSelectDate(date: Date, direction: Direction) {
        if let currentVisibleIndexPath = collectionView.indexPathsForVisibleItems.first {
            let newVisibleIndexPathRow = max(currentVisibleIndexPath.row + direction.rawValue, 0)
            let newVisibleIndexPath = IndexPath(item: newVisibleIndexPathRow, section: currentVisibleIndexPath.section)
            
            activeIndexPath = newVisibleIndexPath
            collectionView.reloadItems(at: [newVisibleIndexPath])
            collectionView.scrollToItem(at: newVisibleIndexPath, at: .centeredHorizontally, animated: true)
        }
    }
    
    func showCategoryFilter(_ sender: Any) {
        if let navController = storyboard?.instantiateViewController(withIdentifier: EventsFeedCategoryFilterViewController.defaultReuseIdentifier) as? UINavigationController {
            navController.modalPresentationStyle = .popover
            let filterViewController = navController.topViewController as? EventsFeedCategoryFilterViewController
            filterViewController?.delegate = self
            filterViewController?.selectedCategories = Set(selectedCategories)
            let popoverController = navController.popoverPresentationController
            popoverController?.backgroundColor = .white
            popoverController?.permittedArrowDirections = [.up]
            popoverController?.sourceView = navigationItem.titleView
            let centerX = ((navigationItem.titleView?.frame.width) ?? 0) / 2
            let height = ((navigationItem.titleView?.frame.height) ?? 0)
            popoverController?.sourceRect = CGRect(x: centerX, y: height, width: 1, height: 1)
            popoverController?.delegate = self
            present(navController, animated: true, completion: nil)
        }
    }
}


// MARK: UICollectionViewDataSource
extension EventsFeedLandingViewController: UICollectionViewDataSource{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // we create a superwide set of collectionview cells (all will be empty luckily)
        // which will serve as placeholders for when a user navigates via the date month picker.
        // the date month picker behaviour is set to go only to the next or previous collectionview cell
        // regardless of how big the actual jump in date range is. this behaviour is similar to the Health app on iOS
        return superwide
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: EventsFeedMonthEventsCollectionViewCell.defaultReuseIdentifier, for: indexPath) as! EventsFeedMonthEventsCollectionViewCell
        cell.refreshDelegate = self
        
        if indexPath == activeIndexPath {
            cell.date = monthPickerView.selectedMonth
            cell.selectedCategories = self.selectedCategories
        }
        
        return cell
    }
}


// MARK: UICollectionViewDelegateFlowLayout
extension EventsFeedLandingViewController: UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.collectionView.frame.size.width, height: self.collectionView.frame.size.height)
    }
}


// MARK: UIPopoverPresentationControllerDelegate
extension EventsFeedLandingViewController: UIPopoverPresentationControllerDelegate{
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
}


// MARK: EventsFeedMonthRefreshDelegate
extension EventsFeedLandingViewController: EventsFeedMonthRefreshDelegate{
    
    func performRefresh(sender: EventsFeedMonthTableViewController?) {
        collectionView.isScrollEnabled = false
        
        startEventsRequest(completion: { [unowned self] in
            self.collectionView.isScrollEnabled = true
            self.reloadData()
        })
    }
}


// MARK: EventsFeedCategoryFilterDelegate
extension EventsFeedLandingViewController: EventsFeedCategoryFilterDelegate{
    
    func didSelectCategories(categories: [EventCategoryRef]) {
       
        selectedCategories = categories
        updateCategoryFilterText(category: categories)
        
        reloadData()
    }
    
    func updateCategoryFilterText(category: [EventCategoryRef]) {
        if selectedCategories.count == 1 {
            if let selectedCategory = selectedCategories.first {
                let name = DataProvider.newRealm().eventCategory(with: selectedCategory)?.name ?? CommonStrings.EventsAllEvents
                filterButton.setTitle(name, for: .normal)
            }
        } else if let tenantId = AppSettings.getAppTenant(), tenantId == .CA, selectedCategories.count > 1{
            filterButton.setTitle("Multiple", for: .normal)
        } else {
            filterButton.setTitle(CommonStrings.EventsAllEvents, for: .normal)
        }
    }
    
    func isAllOtherEventCategory() -> Bool{
        return DataProvider.newRealm().areSelectedCategories(ofType: "Other", selectedCategories: selectedCategories)
    }
}


// MARK: UI Initialization
extension EventsFeedLandingViewController{
    
    func setupTabBarTitle() {
        navigationController?.tabBarItem.title = CommonStrings.EventsAllEvents
    }
    
    func configureTitleView() {
        let containerView = UIView(frame: CGRect(x: 0, y: 0, width: 200, height: 40))
        navigationItem.titleView = containerView
        
        let stackView = UIStackView()
        containerView.addSubview(stackView)
        stackView.axis = .horizontal
        stackView.distribution = .fill
        stackView.contentMode = .scaleToFill
        stackView.snp.makeConstraints { (make) in
            make.width.height.equalToSuperview()
            make.left.top.right.bottom.equalToSuperview()
        }
        
        filterButton.setTitle(CommonStrings.EventsAllEvents, for: .normal)
        filterButton.titleLabel?.textAlignment = .center
        filterButton.setTitleColor(UIColor.primaryColor(), for: .normal)
        
        let image = UIImage.templateImage(asset: CoreAsset.arrowDownIcon)?.withRenderingMode(.alwaysTemplate)
        filterButton.setImage(image, for: .normal)
        filterButton.tintColor = UIColor.primaryColor()
        
        filterButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 0)
        filterButton.adjustsImageWhenHighlighted = false
        filterButton.semanticContentAttribute = .forceRightToLeft
        filterButton.addTarget(self, action: #selector(showCategoryFilter(_:)), for: .touchUpInside)
        stackView.addArrangedSubview(filterButton)
    }
    
    func configureSubviews() {
        automaticallyAdjustsScrollViewInsets = false
        
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.distribution = .fill
        stackView.alignment = .fill
        view.addSubview(stackView)
        
        stackView.snp.makeConstraints { (make) in
            make.leading.trailing.equalToSuperview()
            make.top.equalTo(topLayoutGuide.snp.bottom)
            make.bottom.equalTo(bottomLayoutGuide.snp.top)
        }
        
        stackView.addArrangedSubview(monthPickerView)
        configureMonthPickerView()
        monthPickerView.snp.makeConstraints { (make) in
            make.height.equalTo(58)
            make.width.equalToSuperview()
        }

        
        stackView.addArrangedSubview(collectionView)
        configureCollectionView()
    }
    
    func configureCollectionView() {
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.backgroundColor = UIColor.tableViewBackground()
        collectionView.bounces = false
        collectionView.isScrollEnabled = false
        collectionView.showsHorizontalScrollIndicator = false
        
        collectionView.register(EventsFeedMonthEventsCollectionViewCell.self, forCellWithReuseIdentifier: EventsFeedMonthEventsCollectionViewCell.defaultReuseIdentifier)
        collectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: UICollectionViewCell.defaultReuseIdentifier)
        if let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.minimumLineSpacing = 0
            layout.minimumInteritemSpacing = 0
            layout.scrollDirection = .horizontal
            collectionView.isPagingEnabled = true
            collectionView.collectionViewLayout = layout
        }
    }
    
    func scrollCollectionViewToStartingPositionIfNeeded() {
        if didScrollToStartingPosition {
            return
        }
        
        // scroll halfway into the collectionview's number of items
        // to allow for enough padding for the user to navigate horizontally
        // in conjunction with the month picker
        view.layoutIfNeeded()
        let startingIndexPath = IndexPath(item: superwide / 2, section: 0)
        activeIndexPath = startingIndexPath
        collectionView.scrollToItem(at: startingIndexPath, at: .centeredHorizontally, animated: false)
        didScrollToStartingPosition = true
    }
    
    func configureMonthPickerView() {
        // we create a superwide set of collectionview cells (see numberOfItemsInSection:)
        // which will serve as placeholders for when a user navigates via the date month picker.
        // the date month picker behaviour is set to go only to the next or previous collectionview cell
        // regardless of how big the actual jump in date range is. this behaviour is similar to the Health app on iOS
        monthPickerView.didSelectDate = monthPickerViewDidSelectDate
    }
    
    func performInitialRequest() {
        self.showHUDOnView(view: self.view)
        startEventsRequest { [unowned self] in
            self.hideHUDFromView(view: self.view)
        }
    }
    
    func startEventsRequest(completion: (() -> Void)?) {
        let tenantId = DataProvider.newRealm().getTenantId()
        let partyId = DataProvider.newRealm().getPartyId()
        let effectiveFrom = DataProvider.currentMembershipPeriodStartDate()?.addMonth(n: -12) ?? Date()
        let effectiveTo = Date()
        let request = GetEventByPartyParameter(partyId: partyId, effectiveFrom: effectiveFrom, effectiveTo: effectiveTo, eventTypeFilterTypeKeys: [EventFilterRef.MobileEventsFeed.rawValue])
        Wire.Events.getEventByParty(tenantId: tenantId, request: request) { (error, response) in
            if error != nil {
                debugPrint("Events parsing error encountered: \(String(describing: error))")
            } else {
                debugPrint("Events parsing done")
            }
            
            self.realm.refresh()
            self.reloadData()
            
            if AppSettings.getAppTenant() == .CA{
                EventsFeedCategoryUtil.addEventsToRealm()
            }
            
            if let completionToExecute = completion {
                completionToExecute()
            }
        }
    }
}
extension Date {
    func addMonth(n: Int) -> Date {
        let cal = NSCalendar.current
        return cal.date(byAdding: .month, value: n, to: self)!
    }
}
