//
//  ProfileViewModel.swift
//  VitalityActive
//
//  Created by Joshua Ryan on 7/13/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

import VitalityKit

class ProfileViewModel {

	// MARK: Private

	private let realm = DataProvider.newRealm()
	private var party: VitalityParty?

	// MARK: Public (internal)

	var currentPerson: PersonViewModel {
		get {
			let model = PersonViewModel()
			party = realm.objects(VitalityParty.self).last
			if let person = party?.person {

				if let givenName = person.givenName {
					model.givenName = givenName
				}

				if let familyName = person.familyName {
					model.familyName = familyName
				}

				if let dob = person.bornOn {
					model.dob = dob
				}

				if let avatarPath = person.avatarPath {
					model.avatarPath = avatarPath
				}
                
                if let gender = person.gender{
                    model.gender = gender
                }
			}

			if let addresses = party?.emailAddresses {
				for email in addresses {
					let emailModel = EmailViewModel()
					emailModel.address = email.value
					emailModel.typeName = "Personal"

					model.emailAddresses.append(emailModel)
				}
			}

			if let phoneNumbers = party?.telephones {

				for phone in phoneNumbers {
					let phoneModel = PhoneNumberViewModel()

					phoneModel.areaDialCode = phone.areaDialCode
					phoneModel.contactNumber = phone.contactNumber
					phoneModel.countryDialCode = phone.countryDialCode
					if let numberExtension = phone.numberExtension {
						phoneModel.numberExtension = numberExtension
					}
					phoneModel.typeKey = phone.typeKey
					phoneModel.typeName = "Home" // TODO: Find proper type key value names

					model.phoneNumbers.append(phoneModel)
				}
			}
            
            if let references = party?.references {
                for reference in references {
                    let referenceModel = ReferenceViewModel()
                    
                    referenceModel.effectiveFrom = reference.effectiveFrom
                    referenceModel.effectiveTo = reference.effectiveTo
                    referenceModel.comments = reference.comments
                    referenceModel.issuedBy = reference.issuedBy
                    referenceModel.type = reference.type
                    referenceModel.value = reference.value
                    
                    model.references.append(referenceModel)
                }
            }

			if let partyId = party?.partyId {
				self.membership = partyId
			}

			return model
		}
	}

	var membership: Int = 0


	internal func saveEmail(email: EmailViewModel) {

		try! realm.write {
			let newEmail = PartyEmailAddress()
			newEmail.value = email.address
			party?.emailAddresses.append(newEmail)
		}
	}

	internal func savePhone(phone: PhoneNumberViewModel) {

		try! realm.write {
			let newPhone = PartyTelephone()
			newPhone.countryDialCode = phone.countryDialCode
			newPhone.areaDialCode = phone.areaDialCode
			newPhone.contactNumber = phone.contactNumber
			newPhone.numberExtension = phone.numberExtension

			party?.telephones.append(newPhone)
		}
	}

	internal func saveAvatar(path: String) {

		try! realm.write {
			party?.person?.avatarPath = path
			// TODO: Trigger API call to save new avatar to CMS?
		}
	}
}
