//
//  EditEmailViewController.swift
//  VitalityActive
//
//  Created by OJ Garde on 14/11/2017.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import VIAUIKit
import VIACommon
import VitalityKit

struct DisplayHolder{
    var heading:String
    var content:String
    var placeHolder:String
    var footerValue:String
}

class EditEmailViewController: VIACoordinatedTableViewController, PrimaryColorTintable{
    
    @IBAction func unwindToEditEmailViewController(segue:UIStoryboardSegue) { }
    
    var currentEmail:String?
    var newEmail:String?
    
    fileprivate var displayValues:[DisplayHolder] = []
    fileprivate var footerValues:[String] = []
    fileprivate let MAX_CHARACTER_COUNT_FOR_EMAIL = 100
    
    fileprivate var enableNextButton:Bool = false{
        didSet{
            navigationItem.rightBarButtonItem?.isEnabled = enableNextButton
        }
    }
    
    fileprivate var inputAlert:UIAlertController!
    fileprivate var secureEntryButton:UIButton!
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        
        self.configureTableView()
        
        //Current email must be passed from previous view controller
        //Else we will throw an error and dismiss this view controller
        guard let curEmail = currentEmail else{
            self.navigationController?.popViewController(animated: true)
            return
        }
        
        populateData(withCurrentEmail: curEmail)
    }
    
    override public func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.title = CommonStrings.Settings.ProfileChangeEmailTitle921
        self.configureAppearance()
        self.configureKeyboardAutoToolbar(VIAApplicableFeatures.default.enableKeyboardAutoToolbar ?? false)
        
        self.tableView.reloadData()
    }
    
    override public func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.disableKeyboardAutoToolbar()
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return displayValues.count
    }
    
    
}


// MARK: Data
extension EditEmailViewController{
    
    fileprivate func populateData(withCurrentEmail email:String){
        displayValues.append(DisplayHolder(heading: CommonStrings.Settings.ProfileCurrentEmailTitle922,
                                           content: email,
                                           placeHolder: CommonStrings.ProfileChangeEmailEmailPlaceholder,
                                           footerValue: ""))
        displayValues.append(DisplayHolder(heading: CommonStrings.Settings.ProfileNewEmailTitle923,
                                           content: "",
                                           placeHolder: CommonStrings.ProfileChangeEmailEmailPlaceholder,
                                           footerValue: CommonStrings.Settings.ProfileEmailFootnote924))
    }
}

// MARK: UITableViewControllerDataSource
extension EditEmailViewController{
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = getVIATextFieldTableViewCell(indexPath)
        
        // Retrieve current value from our holder
        let value = displayValues[indexPath.section]
        
        // Initialize cell image configuration
        cell.cellImageConfig = getVIATextFieldCellImageConfig()
        
        //Set value for heading
        cell.setHeadingLabelText(text: value.heading)
        
        // Set initial value for the textfield
        cell.setTextFieldText(text: value.content)
        if value.content != "" {
            cell.setTextFieldEnabled(state: false)
        }
        
        // Set placeholder value for the field in case no input is available
        cell.setTextFieldPlaceholder(placeholder: value.placeHolder)
        
        // Configure UITextField listener for validating of email
        configureTextFieldListener(cell, indexPath: indexPath)
        
        // Set this to .none to disable selection color on the cell row
        cell.selectionStyle = .none
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        // Retrieve current value from our holder
        let value = displayValues[section]
        return value.footerValue.isEmpty ? nil : getFooterView(value.footerValue)
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier) as! VIATableViewSectionHeaderFooterView
        view.labelText = " "
        return view
    }
}

// MARK: Table Helper
extension EditEmailViewController{
    
    fileprivate func configureTextFieldListener(_ cell: VIATextFieldTableViewCell, indexPath: IndexPath){
        cell.textFieldTextDidChange = { [unowned self] textField in
            
            // Holder for our error message
            var errorMessage:String? = nil
            self.newEmail = nil
            self.enableNextButton = true
            
            // Create holder for the value of email
            if let email = textField.text{
                
                // Let's check if email is invalid
                if self.isValidEmail(email) && self.checkForDoubleByte(email) {
                    self.newEmail = email
                }else{
                    errorMessage = CommonStrings.Settings.ProfileEmailErrorMessage925
                }
                
                // Let's check for the character count
                // This is the same as calling
                // if let hasExceeded = self?.hasExceedCharactersLimit(email) where hasExceeded
                if self.hasExceedCharactersLimit(email){
                    //errorMessage = "Maximum \(self.MAX_CHARACTER_COUNT_FOR_EMAIL) character limit reached"
                    errorMessage = CommonStrings.Settings.ProfileEmailErrorCharacterLimit926(String(self.MAX_CHARACTER_COUNT_FOR_EMAIL))
                }
            }
            
            // Refresh the error message label in UI
            UIView.performWithoutAnimation {
                self.tableView.beginUpdates()
                cell.setErrorMessage(message: errorMessage)
                self.enableNextButton = errorMessage == nil
                self.tableView.endUpdates()
            }
        }
        
        //cell.textFieldDidEndEditing = { [unowned self] textField in
        //    self.tableView.reloadRows(at: [indexPath], with: .automatic)
        //}
    }
    
    fileprivate func getFooterView(_ content:String) -> UIView{
        let view = getHeaderFooterView().addPadding(labelText: content, top: 0)
        return view
    }
}


// MARK: CellView
extension EditEmailViewController{
    
    fileprivate func getHeaderFooterView() -> VIATableViewSectionHeaderFooterView{
        return tableView.dequeueReusableHeaderFooterView(withIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier) as! VIATableViewSectionHeaderFooterView
    }
    
    fileprivate func getVIATextFieldTableViewCell(_ indexPath: IndexPath) -> VIATextFieldTableViewCell{
        return tableView.dequeueReusableCell(withIdentifier: VIATextFieldTableViewCell.defaultReuseIdentifier, for: indexPath) as! VIATextFieldTableViewCell
    }
}


// MARK: VIATextFieldCellImageConfig
extension EditEmailViewController{
    
    fileprivate func getVIATextFieldCellImageConfig() -> VIATextFieldCellImageConfig{
        var config = VIATextFieldCellImageConfig()
        config.templateImage = VIACoreAsset.Profile.emailSmall.templateImage
        config.activeTintColor = UIColor.black
        config.inactiveTintColor = UIColor.night()
        return config
    }
}

// MARK: View Configuration
extension EditEmailViewController{
    
    func configureAppearance() {
        self.hideBackButtonTitle()
        self.tableView.backgroundColor = UIColor.tableViewBackground()
        
        // Set left bar button for Cancel
        self.addLeftBarButtonItem(CommonStrings.ProfileChangeEmailCancel, target: self, selector: #selector(onCancel))
        
        // Set right bar button for Next
        self.addRightBarButtonItem(CommonStrings.DoneButtonTitle53, target: self, selector: #selector(onNext))
        
        // This must be disabled after load to prevent user from continuing
        // to the next page with a blank new email.
        self.navigationItem.rightBarButtonItem?.isEnabled = false
    }
    
    // Selector for Cancel navigation button
    func onCancel(sender: UIBarButtonItem){
        self.navigationController?.popViewController(animated: true)
    }
    
    // Selector for Next navigation button
    func onNext(sender: UIBarButtonItem){
        
        //Show Progress Dialog
        self.showHUDOnView(view: self.view)
        
        if let email = self.newEmail{
            
            //Check email availability from server
            checkEmail(email: email, completion: { [weak self] (available, error) in
                
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + .seconds(2)) {
                    self?.hideHUDFromView(view: self?.view)
                    if available{
                        self?.displayEmailChangeConfirmationView()
                    }else{
                        self?.displayUnableToChangeEmailErrorView()
                    }
                }
            })
        }
    }
    
    func configureTableView() {
        self.tableView.register(VIATextFieldTableViewCell.nib(), forCellReuseIdentifier: VIATextFieldTableViewCell.defaultReuseIdentifier)
        self.tableView.register(VIATableViewSectionHeaderFooterView.nib(), forHeaderFooterViewReuseIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier)
        self.tableView.estimatedRowHeight = 75
        self.tableView.backgroundColor = UIColor.tableViewBackground()
        self.tableView.tableFooterView = UIView()
    }
    
    func onConfirmEmailChange(){
        // TODO do your actions when the user confirms the email change
        if let email = self.newEmail{
            displayInputPassword(newEmail: email)
        }
    }
    
    func onPasswordEntered(password:String){
        debugPrint(password)
        // TODO do your actions when the user confirms the email change
        self.showHUDOnView(view: self.view)
        
        //Valdiate if current email and new email was set
        guard let currentEmail = self.currentEmail,
            let newEmail = self.newEmail else{
                return
        }
        
        //Call webservice for change email
        changeEmail(existingUserName: currentEmail, newUsername: newEmail, password: password){
            [weak self] (success,error) in
            
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + .seconds(2)) {
                self?.hideHUDFromView(view: self?.view)
                
                if success{
                    AppSettings.setLastLoginUsername(url: newEmail)
                    self?.navigationController?.popViewController(animated: true)
                }else{
                    self?.displayIncorrectPassword()
                }
            }
        }
    }
    
    func onForgotPassword(){
        //Redirect to forgot password page
        self.performSegue(withIdentifier: "showForgotPassword", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showForgotPassword",
            let destination = segue.destination as? VIAForgotPasswordViewController {
            /* Get Current Email & Environment Prefix */
            let currentEnvironmentPrefix = Wire.default.currentEnvironment.prefix()
            let email = currentEnvironmentPrefix + AppSettings.getLastLoginUsername()
            
            destination.viewController = "EditEmailViewController"
            destination.viewModel.email = email
        }
    }
}


// MARK: Utilities
extension EditEmailViewController{
    
    func hasExceedCharactersLimit(_ token:String) -> Bool{
        return token.characters.count > MAX_CHARACTER_COUNT_FOR_EMAIL
    }
    
    func isValidEmail(_ token:String?) -> Bool {
        let pattern = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        //TO DO: ASK
        let predicate = NSPredicate(format:"SELF MATCHES %@", pattern)
        return predicate.evaluate(with: token)
    }
    
    func checkForDoubleByte(_ token:String) -> Bool{
        
        let buf = token.utf8.count
        if buf == token.characters.count {
            return true
        }
        
        return false
    }
}

// MARK: UIAlertController
extension EditEmailViewController: UITextFieldDelegate{
    
    fileprivate func displayIncorrectPassword(){
        let title = CommonStrings.Settings.AlertIncorrectPasswordTitle933
        let message = CommonStrings.Settings.AlertIncorrectPasswordMessage934
        
        var actions:[UIAlertAction] = []
        actions.append(UIAlertAction(title: CommonStrings.ProfileChangeEmailCancel, style: .cancel, handler: nil))
        actions.append(UIAlertAction(title: CommonStrings.Settings.AlertForgotPasswordButton935, style: .default) { (action) in
            self.onForgotPassword()
        })
        
        showAlertDialog(title: title, message: message, alertActions: actions)
    }
    
    
    fileprivate func displayInputPassword(newEmail:String){
        let title = CommonStrings.Settings.AlertPasswordRequiredTitle931
        let message = CommonStrings.Settings.AlertPasswordRequiredMessage932 + " \(newEmail)"
        
        var actions:[UIAlertAction] = []
        actions.append(UIAlertAction(title: CommonStrings.ProfileChangeEmailCancel, style: .cancel, handler: nil))
        let okAction = UIAlertAction(title: CommonStrings.ProfileChangeEmailOk, style: .default) { [weak self] (action) in
            if let inputAlert = self?.inputAlert,
                let field = inputAlert.textFields?.first,
                let fieldValue = field.text{
                self?.onPasswordEntered(password: fieldValue)
            }
        }
        
        okAction.isEnabled = false
        actions.append(okAction)
        
        showInputDialog(title: title, message: message, alertActions: actions)
    }
    
    fileprivate func displayUnableToChangeEmailErrorView(){
        let title = CommonStrings.Settings.AlertUnableToChangeTitle927
        let message = CommonStrings.Settings.AlertUnableToChangeMessage928
        
        showAlertDialog(title: title, message: message, alertActions: [UIAlertAction(title: CommonStrings.OkButtonTitle40, style: .default){ (action) in
            self.tableView.reloadRows(at: [IndexPath(row: 0, section: 1)], with: .automatic)
            }])
    }
    
    fileprivate func displayEmailChangeConfirmationView(){
        let title = CommonStrings.Settings.AlertChangeConfirmationTitle929
        let message = CommonStrings.Settings.AlertChangeConfirmationMessage930
        
        var actions:[UIAlertAction] = []
        actions.append(UIAlertAction(title: CommonStrings.CancelButtonTitle24, style: .cancel){ (action) in
            self.navigationController?.popViewController(animated: true)
        })
        actions.append(UIAlertAction(title: CommonStrings.ContinueButtonTitle87, style: .default) { (action) in
            self.onConfirmEmailChange()
        })
        
        showAlertDialog(title: title, message: message, alertActions: actions)
    }
    
    fileprivate func showAlertDialog(title:String? = nil, message:String? = nil,
                                     alertActions:[UIAlertAction]){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        
        for alertAction in alertActions{
            alert.addAction(alertAction)
        }
        alert.view.tintColor = UIButton().tintColor
        self.present(alert, animated: true, completion: nil)
    }
    
    fileprivate func showInputDialog(title:String? = nil, message:String? = nil,
                                     alertActions:[UIAlertAction]){
        inputAlert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        inputAlert.addTextField { [weak self] textField in
            textField.placeholder = CommonStrings.ProfileChangeEmailPasswordPlaceholder
            textField.isSecureTextEntry = true
            textField.delegate = self
            
            self?.secureEntryButton = UIButton(frame: CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(15), height: CGFloat(15)))
            self?.secureEntryButton.setImage(VIAUIKitAsset.eyeClosed.image, for: .normal)
            self?.secureEntryButton.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0)
            self?.secureEntryButton.addTarget(self, action: #selector(self?.toggleSecureEntry), for: .touchUpInside)
            textField.rightView = self?.secureEntryButton
            textField.rightViewMode = .always
        }
        
        for alertAction in alertActions{
            inputAlert.addAction(alertAction)
        }
        inputAlert.view.tintColor = UIButton().tintColor
        self.present(inputAlert, animated: true, completion: nil)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if (range.location == 0 && range.length == 1) {
            self.inputAlert.actions[1].isEnabled = false
        }else{
            self.inputAlert.actions[1].isEnabled = true
        }
        
        return true;
    }
    
    func toggleSecureEntry(sender:UIButton){
        if let textField = inputAlert.textFields?.first{
            textField.isSecureTextEntry = !textField.isSecureTextEntry
            secureEntryButton.setImage(textField.isSecureTextEntry ? VIAUIKitAsset.eyeClosed.image : VIAUIKitAsset.eyeOpen.image, for: .normal)
        }
    }
}

extension EditEmailViewController{
    
    fileprivate func checkEmail(email:String, completion:@escaping (_ available:Bool, _ error:String?) -> Void){
        //Get tenant ID
        let tenantId    = DataProvider.newRealm().getTenantId()
        
        //Form the request
        let request     = GetPartyByEmailParam(roleTypeKey: 1, value: email)
        
        //Call service to validate if new email do exist
        Wire.Party.validateIfEmailIsAvailable(tenantId: tenantId, request: request, completion: completion)
    }
    
    fileprivate func changeEmail(existingUserName:String, newUsername: String, password: String,
                                 completion:@escaping (_ success:Bool, _ error:String?) -> Void){
        //Call service to validate if new email do exist
        let request = ChangeUsernameParam(existingUserName: existingUserName,
                                          newUsername: newUsername,
                                          password: password)
        
        Wire.Party.changeUsername(request: request) { (error, data, newAccessToken) in
            debugPrint(newAccessToken ?? "Invalid accessToken")
            completion(nil != newAccessToken, error.debugDescription)
        }
    }
}
