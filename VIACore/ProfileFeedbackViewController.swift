//
//  ProfileFeedbackViewController.swift
//  VitalityActive
//
//  Created by Yuson, Alyosha M. on 25/05/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import UIKit
import VIAUIKit

public struct ProfileFeedbackImageDetail {
    public var templateImage: UIImage?
    public var tintColor: UIColor?
    
    public init(_ templateImage: UIImage?, _ tintColor: UIColor?) {
        self.templateImage = templateImage
        self.tintColor = tintColor
    }
}

public class ProfileFeedbackViewController: VIAViewController {
    
    // MARK: Outlets
    
    @IBOutlet weak var imageView: UIImageView?
    @IBOutlet weak var headingLabel: UILabel?
    @IBOutlet weak var bodyLabel: UILabel?
    @IBOutlet weak var button: VIAButton?
    
    @IBAction func buttonTappedAction(_ sender: Any) {
        VIALoginViewController.showLogin()
    }
    // MARK: Public properties
    
    public var headingText: String? {
        didSet {
            guard self.headingLabel != nil else { return }
            self.headingLabel!.text = self.headingText
        }
    }
    
    public var bodyText: String? {
        didSet {
            guard self.bodyLabel != nil else { return }
            self.bodyLabel!.text = self.bodyText
        }
    }
    
    public var imageDetail: ProfileFeedbackImageDetail? {
        didSet {
            guard self.imageDetail != nil else { return }
            guard self.imageView != nil else { return }
            self.imageView!.image = self.imageDetail!.templateImage
            self.imageView!.tintColor = self.imageDetail!.tintColor
        }
    }
    
    // MARK: Methods
    
    public override func awakeFromNib() {
        super.awakeFromNib()
        // for some reason cross-framework storyboard references don't instantiate outlets properly,
        // so we have to force it by accessing the View of the viewcontroller
        _ = self.view
    }
    
    func configureView() {
        guard let imageView = self.imageView else { return }
        imageView.contentMode = .center
        
        guard let headingLabel = self.headingLabel else { return }
        headingLabel.font = UIFont.feedbackViewHeadingLabel()
        headingLabel.textColor = UIColor.feedbackViewHeadingLabel()
        headingLabel.textAlignment = .center
        headingLabel.numberOfLines = 0
        headingLabel.text = nil
        
        guard let bodyLabel = self.bodyLabel else { return }
        bodyLabel.font = UIFont.feedbackViewBodyLabel()
        bodyLabel.textColor = UIColor.feedbackViewBodyLabel()
        bodyLabel.textAlignment = .left
        bodyLabel.numberOfLines = 0
        bodyLabel.text = nil
    }
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.feedbackViewBackground()
        self.configureView()
    }
    
    public func setButtonTitle(title: String?) {
        self.button?.setTitle(title, for: .normal)
    }
}
