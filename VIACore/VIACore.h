//
//  VIACore.h
//  VIACore
//
//  Created by Wilmar van Heerden on 2016/09/14.
//  Copyright © 2016 Glucode. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for VIACore.
FOUNDATION_EXPORT double VIACoreVersionNumber;

//! Project version string for VIACore.
FOUNDATION_EXPORT const unsigned char VIACoreVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <VIACore/PublicHeader.h>
#import "VIAFirstTimeOnboardingViewController.h"
