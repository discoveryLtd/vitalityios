
import UIKit
import WebKit
import SnapKit
import SafariServices

import VitalityKit
import VIAUIKit

public class SVLearnMoreWebViewController : VIAViewController, WKNavigationDelegate, SVLearnMoreViewModelDelegate, SafariViewControllerEscapable{
    
    /* External Properties */
    public var viewModel: SVLearnMoreViewModel?
    
    
    /* Internal Properties */
    private var webView: WKWebView = WKWebView(frame: .zero)
    private var hasLoadedContent = false
    private var toolbar = UIToolbar()
    
    // MARK: View life cycle
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        viewModel?.delegate = self
        configureAppearance()
        configureViews()
        configureToolbarBorder()
    }
    
    open override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        configureToolbarBorder()
        configureAppearance()
        
        loadWebContent()
    }
    
    func configureViews() {
        configureToolbar()
        configureWebView()
    }
    
    open func configureAppearance() {
        toolbar.isHidden = true
    }
    
    func configureWebView() {
        webView.navigationDelegate = self
        var navigationBarHeight: CGFloat = 0
        if let navController = navigationController {
            navigationBarHeight = navController.navigationBar.isHidden ? 0 : navController.navigationBar.frame.height
        }
        
        let statusBarHeight = UIApplication.shared.statusBarFrame.height
        view.addSubview(webView)
        webView.snp.makeConstraints({(make) -> Void in
            make.top.equalTo(view).offset(navigationBarHeight + statusBarHeight)
            make.left.equalToSuperview()
            make.right.equalToSuperview()
            make.bottom.equalTo(toolbar.snp.top)
        })
    }
    
    func configureToolbar() {
        self.view.addSubview(toolbar)
        toolbar.backgroundColor = UIColor.tableViewBackground()
        toolbar.snp.makeConstraints({(make) -> Void in
            make.left.equalToSuperview()
            make.right.equalToSuperview()
            // enable for Xcode 9
            //            if #available(iOS 11, *) {
            //                make.bottom.equalTo(self.view.safeAreaLayoutGuide.snp.bottomMargin)
            //            } else {
            make.bottom.equalToSuperview()
            //            }
        })
    }
    
    func configureToolbarBorder() {
        toolbar.layer.addBorder(edge: .top)
    }
    
    // MARK: Actions
    
    func loadWebContent() {
        if hasLoadedContent {
            return
        }
        
        showHUDOnView(view: self.view)
        viewModel?.loadArticleContent(completion: { [weak self] error, content in
            self?.hideHUDFromView(view: self?.view)
            if let error = error {
                self?.handle(error: error)
            }
            if let validContent = content {
                self?.webView.loadHTMLString(validContent, baseURL: nil)
            }
        })
    }
    
    public func handle(error: Error) {
        switch error {
        case is BackendError:
            if let backendError = error as? BackendError {
                self.handleBackendErrorWithAlert(backendError, tryAgainAction: { [weak self] in
                    self?.loadWebContent()
                })
            }
            break
        case is CMSError:
            displayErrorWithUnwindAction()
            break
        default:
            displayErrorWithUnwindAction()
            break
        }
    }
    
    func displayErrorWithUnwindAction() {
        displayUnknownServiceErrorOccurredAlert(okAction: { [weak self] in
            self?.returnToPreviousView()
        })
    }
    
    // MARK: WKNavigationDelegate
    
    public func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        webView.evaluateJavaScript("document.documentElement.outerHTML.toString()", completionHandler: { [weak self] html, error in
            self?.hasLoadedContent = html != nil
        })
    }
    
    public func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        escapeToSafariViewController(with: navigationAction, decisionHandler: decisionHandler)
    }
    
    open func returnToPreviousView() {
        debugPrint("CMS content error occurred")
    }
}
