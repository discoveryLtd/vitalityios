import VIAUtilities

public protocol SVLearnMoreViewModelDelegate: class {
    func handle(error: Error)
}

public class SVLearnMoreViewModel: CMSConsumer {
    public weak var delegate: SVLearnMoreViewModelDelegate?
    public var articleId: String?
    internal let coreRealm = DataProvider.newRealm()
    public init(articleId: String?) {
        self.articleId = articleId
    }
    
    public func loadArticleContent(completion: @escaping (_ error: Error?, _ content: String?) -> Void) {
        guard let articleId = self.articleId else {
            completion(CMSError.missingArticleId, nil)
            return
        }
        
        self.getCMSArticleContent(articleId: articleId, completion: completion)
    }
}
