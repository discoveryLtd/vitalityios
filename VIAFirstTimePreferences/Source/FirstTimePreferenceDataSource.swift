import Foundation

import VitalityKit
import VIAUtilities
import VIACommon
import VIAPreferences

public struct FirstTimePreferenceDataSource {
    var preferences:[[Any]] = []

    init() {
        var communicationPreferences:[Any] = []
        let communicationHeader = setupCommunicationHeader()
        communicationPreferences.append(communicationHeader)
        communicationPreferences.append(EmailCommunicationPreference())
        communicationPreferences.append(PushNotificationPreferenceObject())
        preferences.append(communicationPreferences)

        var privacyPreferences:[Any] = []
        let privacyHeader = setupPrivacyHeader()
        privacyPreferences.append(privacyHeader)
        // Add Share Vitality Status section for UKE
        if let showShareVitalityStatus = VIAApplicableFeatures.default.showShareVitalityStatus, showShareVitalityStatus{
            // Share Vitality Status
            privacyPreferences.append(ShareVitalityStatusPreference())
        }
        privacyPreferences.append(AnalyticsPreference())
        privacyPreferences.append(CrashReportPreference())
        preferences.append(privacyPreferences)

        var securityPreferences:[Any] = []
        let securityHeader = setupSecurityHeader()
        securityPreferences.append(securityHeader)
        
        if let enableTouchID = VIAApplicableFeatures.default.enableTouchID,
            enableTouchID{
            // touch ID
            securityPreferences.append(TouchIdPreference())
        }
        // Hide touch ID for UKE
        securityPreferences.append(RememberMePreference())
        preferences.append(securityPreferences)
    }

    // MARK: - Headers
    // MARK: - Communication category
    internal func setupCommunicationHeader() -> PreferenceHeader {
        let communicationHeader = PreferenceHeader()
        communicationHeader.preferenceTitle = CommonStrings.UserPrefs.CommunicationGroupHeaderTitle64
        communicationHeader.preferencesDetail = CommonStrings.UserPrefs.CommunicationGroupHeaderMessage65
        communicationHeader.type = UserPreferenceHeaderType.communication
        return communicationHeader
    }

    // MARK: - Privacy category
    internal func setupPrivacyHeader() -> PreferenceHeader {
        let privacyHeader = PreferenceHeader()
        privacyHeader.preferenceTitle = CommonStrings.UserPrefs.PrivacyGroupHeaderTitle70
        privacyHeader.preferencesDetail = CommonStrings.UserPrefs.PrivacyGroupHeaderMessage71

        privacyHeader.preferenceActionButton = PreferenceActionButton()
        privacyHeader.preferenceActionButton?.preferencesActionButtonTitle = CommonStrings.UserPrefs.PrivacyGroupHeaderLinkButtonTitle72

        privacyHeader.type = UserPreferenceHeaderType.privacy
        return privacyHeader
    }

    // MARK: - Security category
    internal func setupSecurityHeader() -> PreferenceHeader {
        let communicationHeader = PreferenceHeader()
        communicationHeader.preferenceTitle = CommonStrings.UserPrefs.SecurityGroupHeaderTitle77
        communicationHeader.preferencesDetail = CommonStrings.UserPrefs.SecurityGroupHeaderMessage78
        communicationHeader.type = UserPreferenceHeaderType.communication
        return communicationHeader
    }
}
