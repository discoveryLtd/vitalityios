import Foundation

import VIAPreferences

public class FirstTimePreferenceViewModel {

    let onboardingDataSource: FirstTimePreferenceDataSource

    init() {
        self.onboardingDataSource = FirstTimePreferenceDataSource()
    }

    func getNumberOfCategories() -> Int {
        return self.onboardingDataSource.preferences.count
    }

    func getNumberOfPreferences(in category: Int) -> Int {
        return self.onboardingDataSource.preferences[category].count
    }

    func getPrefernceItemFromDataSource(in category: Int, preference: Int) -> Any? {
        if (self.onboardingDataSource.preferences.count >= category) {
            if (self.onboardingDataSource.preferences[category].count >= preference) {
                return self.onboardingDataSource.preferences[category][preference]
            }
        }

        return nil
    }

    func areAllPreferencesSwitchedOff() -> Bool {
        for category in self.onboardingDataSource.preferences {
            for preference in category {
                if (preference is PreferenceDataObject) {
                    if ((preference as! PreferenceDataObject).preferenceState == true) {
                        return false
                    }
                }
            }
        }
        return true
    }
    
    func areAllCommunicationPreferencesSwitchedOff() -> Bool {
        let category = self.onboardingDataSource.preferences.first
        for preference in category! {
            if (preference is PreferenceDataObject) {
                if ((preference as! PreferenceDataObject).preferenceState == true) {
                    return false
                }
            }
        }
        return true
    }
}
