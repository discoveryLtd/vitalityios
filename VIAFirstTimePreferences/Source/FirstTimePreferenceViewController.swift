import Foundation
import UserNotifications
import VIAUIKit
import VitalityKit
import VIACommon
import VIAPreferences


public class FirstTimePreferenceViewController: VIATableViewController, PrimaryColorTintable {
    
    internal var viewModel: FirstTimePreferenceViewModel = FirstTimePreferenceViewModel()
    var touchIdPref = TouchIdPreference()
    var rememberMePref = RememberMePreference()
    var emailPrefs = EmailCommunicationPreference()
    var notificationPref = PushNotificationPreferenceObject()
    var shareVitalityPref = ShareVitalityStatusPreference()
    var pushNotifState: Bool?
    var refreshNotificationPref: Bool?
    public var segueToHomeCompletion:() -> Void = {}
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        setupNavBar()
        setUsernameForRememberMeIfNeeded()
        registerForNotificationsIfNeeded()
        configureTableView()
        touchIdPref.touchIdDelegate = self
        refreshNotificationPref = true
        NotificationCenter.default.addObserver(self, selector: #selector(self.applicationIsActive(notification:)), name: NSNotification.Name.UIApplicationDidBecomeActive, object: nil)
        
        self.pushNotifState = notificationPref.preferenceInitialState()
        
        if let applyPushNotifToggle = VIAApplicableFeatures.default.applyPushNotifToggle, applyPushNotifToggle{
            NotificationCenter.default.addObserver(forName: .VIAPushNotificationStatus, object: nil, queue: nil) { [weak self] (notification) in
                let center = UNUserNotificationCenter.current()
                center.getNotificationSettings { (settings) in
                    DispatchQueue.main.sync {
                        self?.pushNotifState = .authorized == settings.authorizationStatus
                    }
                }
                self?.tableView.reloadData()
            }
        }
    }
    
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func setupNavBar() {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isHidden = true
    }
    
    func applicationIsActive(notification: Notification) {
        self.resetTableViewContent()
    }
    
    func resetTableViewContent() {
        self.viewModel = FirstTimePreferenceViewModel()
        let contentOffset = self.tableView.contentOffset
        self.tableView.reloadData()
        self.tableView.contentOffset = contentOffset
    }
    
    func configureTableView() {
        self.tableView.register(FirstTimePreferenceSwitchCell.nib(), forCellReuseIdentifier: FirstTimePreferenceSwitchCell.defaultReuseIdentifier)
        self.tableView.register(PreferencesSectionCell.nib(), forCellReuseIdentifier: PreferencesSectionCell.defaultReuseIdentifier)
        self.tableView.register(PreferencesButtonCell.nib(), forCellReuseIdentifier: PreferencesButtonCell.defaultReuseIdentifier)
        self.tableView.estimatedRowHeight = 100
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.separatorInset = UIEdgeInsets(top: 0, left: 52, bottom: 0, right: 0)
        self.tableView.backgroundColor = .white
        self.removeFooterHeaderSpaces()
    }
    
    func removeFooterHeaderSpaces() {
        self.tableView.estimatedSectionHeaderHeight = 0
        self.tableView.estimatedSectionFooterHeight = 0
    }
    
    // MARK: Actions
    func setUsernameForRememberMeIfNeeded() {
        //Sets default Remember Me value to false for CA and SLI, true for UKE
        if let enableTouchID = VIAApplicableFeatures.default.enableTouchID, enableTouchID {
            
            let user = AppSettings.getLastLoginUsername()
            
            if !(user.isEmpty) {
                AppSettings.setUsernameForRememberMe(user)
            }
        } else {
            guard AppSettings.hasShownUserPreferences() else { return }
        }
    }
    
    // NOTE: Temporary implementation until PushWoosh is implemented to comply with the BRD requirements
    func registerForNotificationsIfNeeded() {
        if let ukeApp = VIAApplicableFeatures.default.showShareVitalityStatus, ukeApp {
            let realm = DataProvider.newRealm()
            if let user = realm.objects(User.self).first {
                AppSettings.setUsernameForNotifications(user.username)
            }
        }
    }
    
    func next() {
        //if (self.viewModel.areAllPreferencesSwitchedOff()) {
        if (self.viewModel.areAllCommunicationPreferencesSwitchedOff()) {
            let alert = UIAlertController(title: CommonStrings.UserPrefs.NoPreferencesSetAlertTitle85, message: CommonStrings.UserPrefs.NoPreferencesSetAlertMessage86, preferredStyle: .alert)
            let cancelAction = UIAlertAction(title: CommonStrings.CancelButtonTitle24, style: UIAlertActionStyle.cancel, handler: { (UIAlertAction) in
                alert.dismiss(animated: false, completion: nil)
            })
            let continueAction = UIAlertAction(title: CommonStrings.ContinueButtonTitle87, style: UIAlertActionStyle.default, handler: { (UIAlertAction) in
                alert.dismiss(animated: false, completion: nil)
                self.dismissUserPreferences()
            })
            alert.addAction(cancelAction)
            alert.addAction(continueAction)
            self.present(alert, animated: true, completion: nil)
        } else {
            dismissUserPreferences()
        }
        
    }
    
    
    
    func dismissUserPreferences() {
        AppSettings.setHasShownUserPreferences()
        segueToHomeCompletion()
//        if let showUKEOnboarding = Bundle.main.object(forInfoDictionaryKey: "VIAHideOnBoardingScreen") as? Bool, showUKEOnboarding {
//            UKEFirstTimeOnboardingViewController.showUKEFirstTimeOnboarding()
//        } else {
//            VIAHomeViewController.showHome()
//        }
    }
    
    // MARK: UITableView
    
    override public func numberOfSections(in tableView: UITableView) -> Int {
        let footerSectionCount = 1
        return self.viewModel.getNumberOfCategories() + footerSectionCount
    }
    
    override public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let footerSectionIndex = self.viewModel.getNumberOfCategories()
        if (section == footerSectionIndex) {
            let footerCellCount = 1
            return footerCellCount
        } else {
            return self.viewModel.getNumberOfPreferences(in: section)
        }
    }
    
    override public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let footerSectionIndex = self.viewModel.getNumberOfCategories()
        if (indexPath.section == footerSectionIndex) {
            return self.footer(cellAt:indexPath)
        } else {
            let preferenceItem = self.viewModel.getPrefernceItemFromDataSource(in: indexPath.section, preference: indexPath.row)
            if let headerItem = preferenceItem as? PreferenceHeader, let sectionHeaderCell = sectionHeader(cellAt: indexPath, with: headerItem) {
                return sectionHeaderCell
            } else if let dataObject = preferenceItem as? PreferenceDataObject, let preferencesItem = preference(cellAt: indexPath, with: dataObject) {
                // Casting of objects for verification does not seem to work, since casting was done multiple times. Preference titles were found to be the best solution to check what type of Preference Object was called to apply conditions
                let touchIdTitle = CommonStrings.UserPrefs.TouchidToggleTitle79
                let rememberMeTitle = CommonStrings.UserPrefs.RememberMeToggleTitle81
                if (dataObject.preferenceTitle == touchIdTitle){
                    let touchIdCell = preference(cellAt: indexPath, with: touchIdPref)
                    return touchIdCell!
                } else if (dataObject.preferenceTitle == rememberMeTitle){
                    let rememberMeCell = rememberMePreference(cellAt: indexPath, with: rememberMePref)
                    return rememberMeCell!
                } else {
                    return preferencesItem
                }
            }
        }
        
        let cell: UITableViewCell = UITableViewCell(style: .default, reuseIdentifier: "cell")
        return cell
    }
    
    func footer(cellAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: PreferencesButtonCell.defaultReuseIdentifier, for: indexPath) as! PreferencesButtonCell
        cell.setInformationLabel(text: CommonStrings.UserPrefs.UserPrefsScreenFootnoteMessage83)
        cell.setActionButton(title: CommonStrings.NextButtonTitle84)
        cell.setButtonPressed { [unowned self] in
            self.next()
        }
        cell.selectionStyle = .none
        cell.separatorInset = UIEdgeInsets(top: 0, left: cell.frame.width, bottom: 0, right: 0)
        return cell
    }
    
    func sectionHeader(cellAt indexPath: IndexPath, with preferenceHeader: PreferenceHeader) -> UITableViewCell? {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: PreferencesSectionCell.defaultReuseIdentifier, for: indexPath) as! PreferencesSectionCell
        cell.resetCell()
        
        cell.setTitle(preferenceHeader.preferenceTitle!)
        cell.setDetailText(preferenceHeader.preferencesDetail!)
        if preferenceHeader.preferenceActionButton != nil {
            if preferenceHeader.type == UserPreferenceHeaderType.privacy {
                cell.setAction({
                    self.showPrivacyPolicy()
                })
            }
            cell.setActionButton(title: (preferenceHeader.preferenceActionButton?.preferencesActionButtonTitle)!)
            
        }
        cell.selectionStyle = .none
        cell.separatorInset = UIEdgeInsets(top: 0, left: cell.frame.width, bottom: 0, right: 0)
        return cell
    }
    
    func preference(cellAt indexPath: IndexPath, with preferenceObject: PreferenceDataObject) -> UITableViewCell? {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: FirstTimePreferenceSwitchCell.defaultReuseIdentifier, for: indexPath)
        
        guard let switchCell = cell as? FirstTimePreferenceSwitchCell else {
            return cell
        }
        switchCell.setupCell()
        switchCell.actionButton.isHidden = true
        switchCell.setImage(asset: preferenceObject.preferenceImage!)
        switchCell.setTitle(preferenceObject.preferenceTitle!)
        switchCell.setDetailText((preferenceObject.preferencesDetail)!)
        switchCell.switchAction.setOn(preferenceObject.preferenceInitialState(), animated: false)
        
        if preferenceObject is PushNotificationPreferenceObject {
            notificationPref = (preferenceObject as? PushNotificationPreferenceObject)!
            notificationPref.delegate = switchCell
            notificationPref.preferencesSwitchCompletion = { [weak self] in
                self?.tableView.reloadRows(at: [indexPath], with: UITableViewRowAnimation.none)
            }
            switchCell.actionButton.isHidden = false
            switchCell.actionButtonHeight.constant = 20
            switchCell.actionButtonSpaceConstraint.constant = 15
            switchCell.actionButton.setTitle(CommonStrings.UserPrefs.PushMessageToggleSettingsLinkButtonTitle69, for: .normal)
            switchCell.setActionForButton({ () -> (Void)? in
                if let appSettings = URL(string: UIApplicationOpenSettingsURLString) {
                    UIApplication.shared.open(appSettings, options: [:], completionHandler: nil)
                }
                return nil
            })
            
            let center = UNUserNotificationCenter.current()
            center.getNotificationSettings { (settings) in
                DispatchQueue.main.sync {
                    if self.refreshNotificationPref! {
                        switchCell.switchAction.setOn(.authorized == settings.authorizationStatus, animated: false)
                        self.refreshNotificationPref = false
                    }
                }
            }
            
            if let applyPushNotifToggle = VIAApplicableFeatures.default.applyPushNotifToggle, applyPushNotifToggle{
                switchCell.switchAction.setOn(self.pushNotifState!, animated: false)
            }else{
                switchCell.switchAction.setOn(notificationPref.preferenceInitialState(), animated: false)
            }
            
        } else if preferenceObject is EmailCommunicationPreference {
            emailPrefs = (preferenceObject as? EmailCommunicationPreference)!
            emailPrefs.setDelegate(preferenceDelegate: switchCell)
        } else if preferenceObject is ShareVitalityStatusPreference {
            switchCell.actionButton.isHidden = false
            switchCell.actionButtonHeight.constant = 15
            switchCell.actionButtonSpaceConstraint.constant = 15
            switchCell.setActionButton(title: CommonStrings.CommunicationPrefButtonStatus377)
            switchCell.setActionForButton({
                self.showWhatIsVitalityStatus()
            })
        }
        
        
        switchCell.setActionForSwitch {(sender) -> (Void)?  in
            self.switchPreferenceAction(sender: sender, preferenceDataObject: preferenceObject, firstTimePreferenceSwitchCell: switchCell)
        }
        
        
        if (preferenceObject.preferenceActionButton != nil) {
            switchCell.setActionButton(title:preferenceObject.preferenceActionButton!.preferencesActionButtonTitle!)
            switchCell.setActionForButton(preferenceObject.preferenceActionButton!.preferencesButtonAction)
        }
        switchCell.selectionStyle = .none
        self.extendEdgeInsetIfLastPreference(cell: switchCell, index:indexPath)
        
        return switchCell
    }
    
    func switchPreferenceAction(sender: UISwitch, preferenceDataObject: PreferenceDataObject, firstTimePreferenceSwitchCell: FirstTimePreferenceSwitchCell){
        
        if preferenceDataObject is PushNotificationPreferenceObject{
            
            if (notificationPref.preferenceInitialState() == true) && (emailPrefs.preferenceInitialState() == false){
                self.checkIfEmailAndPushDisabled(preferenceDataObject: (self.notificationPref), sender: sender)
            }else{
                self.notificationPref.toggle(sender: sender)
            }
            
        }else if preferenceDataObject is EmailCommunicationPreference{
            
            if (notificationPref.preferenceInitialState() == false) && (emailPrefs.preferenceInitialState() == true){
                self.checkIfEmailAndPushDisabled(preferenceDataObject: (self.emailPrefs), sender: sender)
            }else{
                self.updateEmailPreference(sender: sender)
            }
            
        }else if preferenceDataObject is ShareVitalityStatusPreference{
            if let enabled = Bundle.main.object(forInfoDictionaryKey: "VIAEnableShareVitalityStatusPreference") as? Bool, enabled{
                self.shareVitalityPref.toggleVitalityStatusPref(sender: sender)
            }
        }else if preferenceDataObject is TouchIdPreference{
            self.touchIdPref.touchIdDelegate = self
            self.touchIdPref.toggleTouchId(sender: sender)
        }
        
    }
    
    func checkIfEmailAndPushDisabled(preferenceDataObject: PreferenceDataObject, sender: UISwitch?){
        
        
        let alert = UIAlertController(title: CommonStrings.UserPrefs.NoPreferencesSetAlertTitle85, message: CommonStrings.UserPrefs.NoPreferencesSetAlertMessage86, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: CommonStrings.CancelButtonTitle24, style: .cancel){ (action) in
            sender?.setOn(true, animated: true)
            
            if preferenceDataObject is EmailCommunicationPreference{
                self.updateEmailPreference(sender: sender!)
            }else{
                self.notificationPref.toggle(sender: sender)
            }
            
        })
        
        alert.addAction(UIAlertAction(title: CommonStrings.ContinueButtonTitle87, style: .default) { (action) in
            
            if preferenceDataObject is EmailCommunicationPreference{
                self.updateEmailPreference(sender: sender!)
            }else{
                self.notificationPref.toggle(sender: sender)
            }
            
            sender?.setOn(false, animated: true)
            
        })
        
        alert.view.tintColor = UIButton().tintColor
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func updateEmailPreference(sender: UISwitch){
        self.showHUDOnView(view: self.view)
        self.emailPrefs.changeEmailPreference(sender: sender){ [weak self] error in
            self?.hideHUDFromView(view: self?.view)
        }
    }
    
    func extendEdgeInsetIfLastPreference(cell: UITableViewCell, index: IndexPath) {
        let indexCorrector = 1
        let headerCount = 1
        cell.separatorInset = UIEdgeInsets(top: 0, left: 62, bottom: 0, right: 0)
        if (index.section == self.viewModel.getNumberOfCategories() - indexCorrector) {
            if (index.row == self.viewModel.getNumberOfPreferences(in: index.section) - headerCount) {
                cell.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            }
        }
    }
    
    func rememberMePreference(cellAt indexPath: IndexPath, with preferenceObject: PreferenceDataObject) -> UITableViewCell? {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: FirstTimePreferenceSwitchCell.defaultReuseIdentifier, for: indexPath)
        
        guard let switchCell = cell as? FirstTimePreferenceSwitchCell else {
            return cell
        }
        switchCell.setupCell()
        switchCell.actionButton.isHidden = true
        switchCell.setImage(asset: preferenceObject.preferenceImage!)
        switchCell.setTitle(preferenceObject.preferenceTitle!)
        switchCell.setDetailText((preferenceObject.preferencesDetail)!)
        switchCell.switchAction.setOn(preferenceObject.preferenceInitialState(), animated: false)
        
        if let notificationPref = preferenceObject as? PushNotificationPreferenceObject {
            notificationPref.delegate = switchCell
            notificationPref.preferencesSwitchCompletion = { [weak self] in
                self?.tableView.reloadRows(at: [indexPath], with: UITableViewRowAnimation.none)
            }
        } else if preferenceObject is EmailCommunicationPreference {
            (preferenceObject as? EmailCommunicationPreference)?.setDelegate(preferenceDelegate: switchCell)
        }
        
        switchCell.setActionForSwitch(closure: preferenceObject.preferencesSwitchAction)
        if (preferenceObject.preferenceActionButton != nil) {
            switchCell.setActionButton(title:preferenceObject.preferenceActionButton!.preferencesActionButtonTitle!)
            switchCell.setActionForButton(preferenceObject.preferenceActionButton!.preferencesButtonAction)
        }
        switchCell.selectionStyle = .none
        self.extendEdgeInsetIfLastPreference(cell: switchCell, index:indexPath)
        
        // Apply Remember Me disablement only when Touch ID is available
        if let enableTouchID = VIAApplicableFeatures.default.enableTouchID, enableTouchID {
            if (touchIdPref.preferenceInitialState()) {
                switchCell.isUserInteractionEnabled = false
                switchCell.switchAction.alpha = 0.5
            } else {
                switchCell.isUserInteractionEnabled = true
                switchCell.switchAction.alpha = 1
            }
        }
        
        return switchCell
    }
    
    override public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: false)
    }
    
    // MARK: Navigation
    @IBAction open func unwindToVIAOnboardingViewControllerFromWebContentViewControllerWithSegue(_ sender: UIStoryboardSegue) {
        debugPrint("unwindToVIAOnboardingViewControllerFromWebContentViewControllerWithSegue")
    }
    
    public class func showUserPreferences() {
//        if(!VitalityPartyMembership.startedMembership() && VIAApplicableFeatures.default.showLoginRestriction()){
//            LoginRestrictionViewController.show()
//        }else{
//            let storyboard = UIStoryboard(coreStoryboard: .FirstTimePreference)
//            let viewController = storyboard.instantiateInitialViewController()
//            UIApplication.shared.keyWindow?.rootViewController = viewController
//        }
    }
    
    func showPrivacyPolicy() {
        self.performSegue(withIdentifier: "showPrivacyPolicy", sender: nil)
    }
    
    func showWhatIsVitalityStatus() {
        self.performSegue(withIdentifier: "showWhatIsVitalityStatus", sender: nil)
    }
    
    public override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        self.hideBackButtonTitle()
        
        if (segue.identifier == "showPrivacyPolicy"){
            if let webContentViewController = segue.destination as? VIAWebContentViewController,
                let articleId = AppConfigFeature.contentId(for: .userPreferencesPrivacyPolicy) {
                let webContentViewModel = VIAWebContentViewModel(articleId: articleId)
                webContentViewController.viewModel = webContentViewModel
                // Temporary since token needs to be added on PhraseApp for UKE
                if let appIsUke = VIAApplicableFeatures.default.showShareVitalityStatus, appIsUke{
                    webContentViewController.title = "Privacy statement"
                } else {
                    webContentViewController.title = CommonStrings.UserPrefs.PrivacyGroupHeaderLinkButtonTitle72
                }
            }
        } else {
            if let webContentViewController = segue.destination as? VIAWebContentViewController,
                let articleId = AppConfigFeature.contentId(for: .vitalityStatusConsentContent) {
                let webContentViewModel = VIAWebContentViewModel(articleId: articleId)
                webContentViewController.viewModel = webContentViewModel
                webContentViewController.title = CommonStrings.CommunicationPrefButtonStatus377
            }
        }
    }
}

extension FirstTimePreferenceViewController: touchIdDelegate {
    public func reloadScreen() {
        self.tableView.reloadData()
    }
    
    public func reloadSecurity() {
    }
    
    public func promptInvalidSecurityDetails(errorMessage: String, attempts:Int) {
        let alert = UIAlertController(title: title, message: errorMessage, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: CommonStrings.OkButtonTitle40, style: .cancel, handler: nil))
        if attempts >= AppSettings.getMaxPasswordAttemptForTouchID(){
            alert.addAction(UIAlertAction(title: CommonStrings.Login.ForgotPasswordButtonTitle22, style: .default,
                                          handler: { [weak self] (action) in
                                            self?.performSegue(withIdentifier: "showForgotPassword", sender: nil)
            }))
        }
        self.present(alert, animated: true, completion: nil)
    }
}
