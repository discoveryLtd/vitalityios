import UIKit
import SnapKit

import VIAUIKit
import VIAPreferences

public protocol FirstTimePreferenceSwitchCellProtocol {
    func setImage(asset: UIImage)
    func setTitle(_ title: String)
    func setDetailText(_ detailText: String)
    func setActionButton(title: String)
    func setActionForButton(_ closure: @escaping () -> (Void)?)
    func setActionForSwitch(closure: @escaping ((_ sender: UISwitch) -> (Void)?))
}


public class FirstTimePreferenceSwitchCell: UITableViewCell, Nibloadable {
    var reload: (() -> (Void)) = {}
    @IBOutlet weak var onboardingCellImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var detailsLabel: UILabel!
    
    @IBOutlet public weak var switchAction: UISwitch!
    @IBOutlet public weak var actionButton: UIButton!
    @IBOutlet public weak var actionButtonHeight: NSLayoutConstraint!
    @IBOutlet public weak var actionButtonSpaceConstraint: NSLayoutConstraint!

    internal var didFlipSwitchAction:(_ sender: UISwitch) -> (Void)? = {_ in
        debugPrint("didFlipSwitch")
    }
    internal var didSelectSwitchCellButton:() -> (Void)? = {
        debugPrint("didSelectButton")
    }

    override public func awakeFromNib() {
        super.awakeFromNib()
        setupCell()
    }

    public override func prepareForReuse() {
        super.prepareForReuse()
    }

    public func setupCell() {
        self.setupCellView()
        self.setupActionButton()
        self.setupImageView()
        self.setupTitleLabel()
        self.setupDetailsLabel()
        self.setupButton()
        self.setupSwitch()
    }

    public func setupCellView() {
        self.backgroundColor = .clear
        self.backgroundView?.backgroundColor = .clear
        self.contentView.backgroundColor = .clear
    }

    //User interaction controllers
    public func setupActionButton() {
        self.actionButton.isHidden = true
        self.actionButtonHeight.constant = 0
        self.actionButtonSpaceConstraint.constant = 0
        self.actionButton.addTarget(self, action: #selector(executeClosure), for: .touchUpInside)
    }

    @IBAction func onboardingSwitchFlipped(_ sender: UISwitch) {
        self.didFlipSwitchAction(sender)
    }

    public func executeClosure() {
        self.didSelectSwitchCellButton()
    }

    // New/dequeued cell setup
    public func setupImageView() {
        self.onboardingCellImage.image = nil
    }

    public func setupTitleLabel() {
        self.titleLabel.text = nil
        self.titleLabel.font = UIFont.onboardingCellHeading()
        self.titleLabel.textColor = UIColor.night()
    }

    public func setupDetailsLabel() {
        self.detailsLabel.text = nil
        self.detailsLabel.font = UIFont.onboardingCellContent()
        self.detailsLabel.textColor = UIColor.darkGrey()
    }

    public func setupButton() {
        self.actionButton.setTitle(nil, for: .normal)
        self.actionButton.titleLabel?.font = UIFont.onboardingCellContent()
        self.actionButton.titleLabel?.textColor = UIColor.primaryColor()
    }

    public func setupSwitch() {
        self.switchAction.isEnabled = true
        self.switchAction.onTintColor = UIColor.primaryColor()
    }
}

extension FirstTimePreferenceSwitchCell: FirstTimePreferenceSwitchCellProtocol {
    public func setImage(asset: UIImage) {
        self.onboardingCellImage.image = asset
    }
    public func setImageTintColor(_ color: UIColor) {
        self.onboardingCellImage.tintColor = color
    }
    public func setTitle(_ title: String) {
        self.titleLabel.text = title
    }

    public func setDetailText(_ detailText: String) {
        self.detailsLabel.text = detailText
    }

    public func setActionButton(title: String) {
        self.bringSubview(toFront: self.actionButton)
        self.actionButton.setTitle(title, for: .normal)
    }

    public func setActionForButton(_ closure: @escaping () -> (Void)?) {
        self.didSelectSwitchCellButton = closure
    }

    public func setActionForSwitch(closure: @escaping ((_ sender: UISwitch) -> (Void)?)) {
        self.didFlipSwitchAction = closure
    }
}

extension FirstTimePreferenceSwitchCell: PreferenceObjectDelegate {
    public func setPreferenceState(active: Bool) {
        self.switchAction.setOn(active, animated: false)
    }

    public func setPreferenceEnabled(active: Bool) {
        DispatchQueue.main.async {[weak self] in
            self?.switchAction.isEnabled = active
        }
    }

    public func disableActionButton(active: Bool) {
        if active {
            self.actionButton.isHidden = true
            self.actionButtonHeight.constant = 0
            self.actionButtonSpaceConstraint.constant = 0
        } else {
            self.actionButton.isHidden = false
            self.actionButtonHeight.constant = 20
            self.actionButtonSpaceConstraint.constant = 15
        }
    }
}
