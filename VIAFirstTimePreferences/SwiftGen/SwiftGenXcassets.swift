// Generated using SwiftGen, by O.Halligon — https://github.com/SwiftGen/SwiftGen

#if os(OSX)
import AppKit.NSImage
public typealias VIAFirstTimePreferencesColor = NSColor
public typealias VIAFirstTimePreferencesImage = NSImage
#elseif os(iOS) || os(tvOS) || os(watchOS)
import UIKit.UIImage
public typealias VIAFirstTimePreferencesColor = UIColor
public typealias VIAFirstTimePreferencesImage = UIImage
#endif

// swiftlint:disable file_length

public typealias VIAFirstTimePreferencesAssetType = VIAFirstTimePreferencesImageAsset

public struct VIAFirstTimePreferencesImageAsset: Equatable {
	fileprivate var name: String

	public var templateImage: VIAFirstTimePreferencesImage {
		let bundle = Bundle(for: BundleToken.self)
		#if os(iOS) || os(tvOS)
		let image = VIAFirstTimePreferencesImage(named: name, in: bundle, compatibleWith: nil)?.withRenderingMode(.alwaysTemplate)
		#elseif os(OSX)
		let image = bundle.image(forResource: name).withRenderingMode(.alwaysTemplate)
		#elseif os(watchOS)
		let image = VIAFirstTimePreferencesImage(named: name).withRenderingMode(.alwaysTemplate)
		#endif
		guard let result = image else { fatalError("Unable to load template image named \(name).") }
		return result
	}

	public var image: VIAFirstTimePreferencesImage {
		let bundle = Bundle(for: BundleToken.self)
		#if os(iOS) || os(tvOS)
		let image = VIAFirstTimePreferencesImage(named: name, in: bundle, compatibleWith: nil)
		#elseif os(OSX)
		let image = bundle.image(forResource: name)
		#elseif os(watchOS)
		let image = VIAFirstTimePreferencesImage(named: name)
		#endif
		guard let result = image else { fatalError("Unable to load image named \(name).") }
		return result
	}

	public static func ==(lhs: VIAFirstTimePreferencesImageAsset, rhs: VIAFirstTimePreferencesImageAsset) -> Bool {
        return lhs.image == rhs.image
    }
}

public struct VIAFirstTimePreferencesColorAsset {
fileprivate var name: String

#if swift(>=3.2)
@available(iOS 11.0, tvOS 11.0, watchOS 4.0, OSX 10.13, *)
var color: VIAFirstTimePreferencesColor {
return VIAFirstTimePreferencesColor(asset: self)
}
#endif
}

// swiftlint:disable identifier_name line_length nesting type_body_length type_name
public enum VIAFirstTimePreferencesAsset {

  // swiftlint:disable trailing_comma
  public static let allColors: [VIAFirstTimePreferencesColorAsset] = [
  ]
  public static let allImages: [VIAFirstTimePreferencesImageAsset] = [
  ]
  // swiftlint:enable trailing_comma
  @available(*, deprecated, renamed: "allImages")
  public static let allValues: [VIAFirstTimePreferencesAssetType] = allImages
}
// swiftlint:enable identifier_name line_length nesting type_body_length type_name

extension VIAFirstTimePreferencesImage {
@available(iOS 1.0, tvOS 1.0, watchOS 1.0, *)
@available(OSX, deprecated,
message: "This initializer is unsafe on macOS, please use the VIAFirstTimePreferencesImageAsset.image property")
convenience init!(asset: VIAFirstTimePreferencesAssetType) {
#if os(iOS) || os(tvOS)
let bundle = Bundle(for: BundleToken.self)
self.init(named: asset.name, in: bundle, compatibleWith: nil)
#elseif os(OSX) || os(watchOS)
self.init(named: asset.name)
#endif
}
}

extension VIAFirstTimePreferencesColor {
#if swift(>=3.2)
@available(iOS 11.0, tvOS 11.0, watchOS 4.0, OSX 10.13, *)
convenience init!(asset: VIAFirstTimePreferencesColorAsset) {
let bundle = Bundle(for: BundleToken.self)
#if os(iOS) || os(tvOS)
self.init(named: asset.name, in: bundle, compatibleWith: nil)
#elseif os(OSX)
self.init(named: asset.name, bundle: bundle)
#elseif os(watchOS)
self.init(named: asset.name)
#endif
}
#endif
}

private final class BundleToken {}
