import XCTest

@testable import VIAActiveRewards
@testable import VIAUIKit
@testable import VitalityKit

class VIAARActivityHistoryViewModelTests: XCTestCase {
    var goalProgressDetailsResponse: GoalProgressDetailsResponse?
    var viewModel: VIAARActivityHistoryViewModel?
    
    func response(from file: String) -> GPDResponseGetGoalProgressAndDetails? {
        var response: GPDResponseGetGoalProgressAndDetails? = nil
        guard let pathString = Bundle(for: type(of: self)).path(forResource: file, ofType: "json") else {
            fatalError("\(file) not found")
        }
        
        guard let jsonString = try? NSString(contentsOfFile: pathString, encoding: String.Encoding.utf8.rawValue) else {
            fatalError("Unable to convert \(file) to String")
        }
        
        print("The JSON string is: \(jsonString)")
        
        guard let jsonData = jsonString.data(using: String.Encoding.utf8.rawValue) else {
            fatalError("Unable to convert \(file) to NSData")
        }
        
        do {
            guard let jsonDictionary = try JSONSerialization.jsonObject(with: jsonData, options: []) as? [String:Any] else {
                fatalError("Unable to convert \(file) to JSON dictionary")
            }
            response = GoalProgressDetailsResponse.init(json: jsonDictionary)!.getGoalProgressAndDetailsResponse
        } catch _ {
            
        }
        
        return response
    }
    
    override func setUp() {
        super.setUp()
        let realm = DataProvider.newARRealm()
        try! realm.write {
            realm.deleteAll()
        }
        viewModel = VIAARActivityHistoryViewModel()
    }
    
    override func tearDown() {
        viewModel = nil
        let realm = DataProvider.newARRealm()
        try! realm.write {
            realm.deleteAll()
        }
        super.tearDown()
    }
    
    func testViewModelProvidedWithInProgressGoalGeneratesExpectedDataForDisplay() {
        let file = "goal_in_progress_with_activity_response"
        guard let activityHistoryModel = viewModel
            else { fatalError("viewModel not initialized") }
        guard let inProgressWithActivityResponse = response(from: file)
            else { fatalError("Unable to generate response from file provided") }
        
        ARGoalTracker.parseAndPersistGoalProgressAndDetails(with: inProgressWithActivityResponse)
        
        activityHistoryModel.configureSectionsAndGoalItemData()
        
        
        XCTAssertEqual(activityHistoryModel.numberOfDateSections(), 1, "Section count does not match expected value")
        XCTAssertEqual(activityHistoryModel.dateTitle(for: 0), "OCTOBER 2017", "Date section title does not match expected value")
        XCTAssertEqual(activityHistoryModel.numberOfGoalItems(in: 0), 1, "Number of goal items in section does not match expected value")
        
        let indexPath = IndexPath(row: 0, section: 0)
        
        XCTAssertEqual(activityHistoryModel.goalStatusTitle(at: indexPath), "AR.goal_in_progress_title_726", "Goal status icon does not match expected value")
        XCTAssertEqual(activityHistoryModel.goalPeriod(at: indexPath), "17 Oct - 19 Oct", "Goal period does not match expected value")
        XCTAssertEqual(activityHistoryModel.goalPoints(at: indexPath), "points.x_of_y_points_title_193", "Goal points does not match expected value")
        XCTAssertEqual(activityHistoryModel.goalStatusIcon(at: indexPath), VIAActiveRewardsAsset.Goals.statusInProgress.image, "Goal status icon does not match expected value")
    }
    
    func testViewModelProvidedWithAchievedGoalGeneratesExpectedDataForDisplay() {
        let file = "goal_achieved_with_activity_response"
        guard let activityHistoryModel = viewModel
            else { fatalError("viewModel not initialized") }
        guard let achievedWithActivityResponse = response(from: file)
            else { fatalError("Unable to generate response from file provided") }
        
        ARGoalTracker.parseAndPersistGoalProgressAndDetails(with: achievedWithActivityResponse)
        
        activityHistoryModel.configureSectionsAndGoalItemData()
        
        
        XCTAssertEqual(activityHistoryModel.numberOfDateSections(), 1, "Section count does not match expected value")
        XCTAssertEqual(activityHistoryModel.dateTitle(for: 0), "OCTOBER 2017", "Date section title does not match expected value")
        XCTAssertEqual(activityHistoryModel.numberOfGoalItems(in: 0), 1, "Number of goal items in section does not match expected value")
        
        let indexPath = IndexPath(row: 0, section: 0)
        
        XCTAssertEqual(activityHistoryModel.goalStatusTitle(at: indexPath), "AR.Goal_History.goal_achieved_title_692", "Goal status icon does not match expected value")
        XCTAssertEqual(activityHistoryModel.goalPeriod(at: indexPath), "17 Oct - 19 Oct", "Goal period does not match expected value")
        XCTAssertEqual(activityHistoryModel.goalPoints(at: indexPath), "points.x_of_y_points_title_193", "Goal points does not match expected value")
        XCTAssertEqual(activityHistoryModel.goalStatusIcon(at: indexPath), VIAActiveRewardsAsset.Goals.statusAchieved.image, "Goal status icon does not match expected value")
    }
    
    func testViewModelProvidedWithNotAchievedGoalGeneratesExpectedDataForDisplay() {
        let file = "goal_not_achieved_with_activity_response"
        guard let activityHistoryModel = viewModel
            else { fatalError("viewModel not initialized") }
        guard let notAchievedWithActivityResponse = response(from: file)
            else { fatalError("Unable to generate response from file provided") }
        
        ARGoalTracker.parseAndPersistGoalProgressAndDetails(with: notAchievedWithActivityResponse)
        
        activityHistoryModel.configureSectionsAndGoalItemData()
        
        
        XCTAssertEqual(activityHistoryModel.numberOfDateSections(), 1, "Section count does not match expected value")
        XCTAssertEqual(activityHistoryModel.dateTitle(for: 0), "OCTOBER 2017", "Date section title does not match expected value")
        XCTAssertEqual(activityHistoryModel.numberOfGoalItems(in: 0), 1, "Number of goal items in section does not match expected value")
        
        let indexPath = IndexPath(row: 0, section: 0)
        
        XCTAssertEqual(activityHistoryModel.goalStatusTitle(at: indexPath), "AR.goal_history.not_achieved_title_709", "Goal status icon does not match expected value")
        XCTAssertEqual(activityHistoryModel.goalPeriod(at: indexPath), "17 Oct - 19 Oct", "Goal period does not match expected value")
        XCTAssertEqual(activityHistoryModel.goalPoints(at: indexPath), "points.x_of_y_points_title_193", "Goal points does not match expected value")
        XCTAssertEqual(activityHistoryModel.goalStatusIcon(at: indexPath), VIAActiveRewardsAsset.Goals.statusNotAchieved.image, "Goal status icon does not match expected value")
    }
    
    func testViewModelProvidedWithPendingGoalGeneratesExpectedDataForDisplay() {
        let file = "goal_pending_with_activity_response"
        guard let activityHistoryModel = viewModel
            else { fatalError("viewModel not initialized") }
        guard let pendingWithActivityResponse = response(from: file)
            else { fatalError("Unable to generate response from file provided") }
        
        ARGoalTracker.parseAndPersistGoalProgressAndDetails(with: pendingWithActivityResponse)
        
        activityHistoryModel.configureSectionsAndGoalItemData()
        
        
        XCTAssertEqual(activityHistoryModel.numberOfDateSections(), 1, "Section count does not match expected value")
        XCTAssertEqual(activityHistoryModel.dateTitle(for: 0), "OCTOBER 2017", "Date section title does not match expected value")
        XCTAssertEqual(activityHistoryModel.numberOfGoalItems(in: 0), 1, "Number of goal items in section does not match expected value")
        
        let indexPath = IndexPath(row: 0, section: 0)
        
        XCTAssertEqual(activityHistoryModel.goalStatusTitle(at: indexPath), "AR.goal_pending_title_693", "Goal status icon does not match expected value")
        XCTAssertEqual(activityHistoryModel.goalPeriod(at: indexPath), "17 Oct - 19 Oct", "Goal period does not match expected value")
        XCTAssertEqual(activityHistoryModel.goalPoints(at: indexPath), "points.x_of_y_points_title_193", "Goal points does not match expected value")
        XCTAssertEqual(activityHistoryModel.goalStatusIcon(at: indexPath), VIAActiveRewardsAsset.Goals.statusPending.image, "Goal status icon does not match expected value")
    }
    
}
