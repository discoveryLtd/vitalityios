import XCTest
import VitalityKit
import RealmSwift

@testable import VIAActiveRewards
@testable import VIAUIKit
@testable import VitalityKit

class VIAARActivityDetailViewModelTests: XCTestCase {
    var goalProgressDetailsResponse: GoalProgressDetailsResponse?
    var viewModel: VIAARActivityDetailViewModel?
    
    func response(from file: String) -> GPDResponseGetGoalProgressAndDetails? {
        var response: GPDResponseGetGoalProgressAndDetails? = nil
        guard let pathString = Bundle(for: type(of: self)).path(forResource: file, ofType: "json") else {
            fatalError("\(file) not found")
        }
        
        guard let jsonString = try? NSString(contentsOfFile: pathString, encoding: String.Encoding.utf8.rawValue) else {
            fatalError("Unable to convert \(file) to String")
        }
        
        print("The JSON string is: \(jsonString)")
        
        guard let jsonData = jsonString.data(using: String.Encoding.utf8.rawValue) else {
            fatalError("Unable to convert \(file) to NSData")
        }
        
        do {
            guard let jsonDictionary = try JSONSerialization.jsonObject(with: jsonData, options: []) as? [String:Any] else {
                fatalError("Unable to convert \(file) to JSON dictionary")
            }
            response = GoalProgressDetailsResponse.init(json: jsonDictionary)!.getGoalProgressAndDetailsResponse
        } catch _ {
            
        }
        
        return response
    }
    
    override func setUp() {
        super.setUp()
        let realm = DataProvider.newARRealm()
        try! realm.write {
            realm.deleteAll()
        }
        viewModel = VIAARActivityDetailViewModel()
    }
    
    override func tearDown() {
        viewModel = nil
        let realm = DataProvider.newARRealm()
        try! realm.write {
            realm.deleteAll()
        }
        super.tearDown()
    }
    
    func testViewModelProvidedWithInProgressSelectedGoalItemWithNoPointsActivityGeneratesExpectedDataForDisplay() {
        let file = "goal_in_progress_without_activity_response"
        guard let activityDetailViewModel = viewModel
            else { fatalError("viewModel not initialized") }
        guard let inProgressGoalResponse = response(from: file)
            else { fatalError("Unable to generate response from file provided") }
        
         ARGoalTracker.parseAndPersistGoalProgressAndDetails(with: inProgressGoalResponse)
        
        activityDetailViewModel.selectedGoalTrackerId = 456603
        activityDetailViewModel.configureSelectedGoalItemData()
        guard let selectedGoalItem = activityDetailViewModel.selectedGoalItem else {
            fatalError("failed to get selected goal item")
        }
        
        guard let statusTitle = activityDetailViewModel.selectedGoalItemStatusTitle() else {
            fatalError("expected status title for selected goal item")
        }

        XCTAssertEqual(selectedGoalItem.earnedPoints, 0, "Earned points does not match expected value")
        XCTAssertEqual(selectedGoalItem.potentialPoints, 140, "Potential points does not match expected value")
        XCTAssertEqual(statusTitle, "AR.goal_in_progress_title_726", "Goal title does not match expected value")
        XCTAssertEqual(activityDetailViewModel.selectedGoalItemHasEvents(), false, "Goal has events when it is expected not to")
        XCTAssertEqual(activityDetailViewModel.selectedGoalItemStatusDescription(), nil, "Goal item status description does not match expected value")
        XCTAssertEqual(activityDetailViewModel.selectedGoalItemDateRangeScreenTitle(), "17 Oct 2017 - 19 Oct 2017", "Screen date title does not match expected value")
        XCTAssertEqual(activityDetailViewModel.selectedGoalItemPointsMessage(), "points.x_of_y_points_title_193", "Points message string does not match expected value")
        XCTAssertEqual(activityDetailViewModel.selectedGoalItemStatusIcon(), VIAActiveRewardsAsset.Goals.statusInProgress.image, "Icon does not match expected value")
    }
    
    func testViewModelProvidedWithNotAchievedSelectedGoalItemWithNoPointsActivityGeneratesExpectedDataForDisplay() {
        let file = "goal_not_achieved_without_activity_response"
        guard let activityDetailViewModel = viewModel
            else { fatalError("viewModel not initialized") }
        guard let notAchievedGoalResponse = response(from: file)
            else { fatalError("Unable to generate response from file provided") }
        
        ARGoalTracker.parseAndPersistGoalProgressAndDetails(with: notAchievedGoalResponse)
        
        activityDetailViewModel.selectedGoalTrackerId = 456603
        activityDetailViewModel.configureSelectedGoalItemData()
        guard let selectedGoalItem = activityDetailViewModel.selectedGoalItem else {
            fatalError("failed to get selected goal item")
        }
        
        guard let statusTitle = activityDetailViewModel.selectedGoalItemStatusTitle() else {
            fatalError("expected status title for selected goal item")
        }
        
        XCTAssertEqual(selectedGoalItem.earnedPoints, 0, "Earned points does not match expected value")
        XCTAssertEqual(selectedGoalItem.potentialPoints, 140, "Potential points does not match expected value")
        XCTAssertEqual(statusTitle, "AR.goal_history.not_achieved_title_709", "Goal title does not match expected value")
        XCTAssertEqual(activityDetailViewModel.selectedGoalItemHasEvents(), false, "Goal has events when it is expected not to")
        XCTAssertEqual(activityDetailViewModel.selectedGoalItemStatusDescription(), "AR.goal_history.not_achieved_message_846", "Goal item status description does not match expected value")
        XCTAssertEqual(activityDetailViewModel.selectedGoalItemDateRangeScreenTitle(), "17 Oct 2017 - 19 Oct 2017", "Screen date title does not match expected value")
        XCTAssertEqual(activityDetailViewModel.selectedGoalItemPointsMessage(), "points.x_of_y_points_title_193", "Points message string does not match expected value")
        XCTAssertEqual(activityDetailViewModel.selectedGoalItemStatusIcon(), VIAActiveRewardsAsset.Goals.statusNotAchieved.image, "Icon does not match expected value")
    }
    
    func testViewModelProvidedWithPendingSelectedGoalItemWithNoPointsActivityGeneratesExpectedDataForDisplay() {
        let file = "goal_pending_without_activity_response"
        guard let activityDetailViewModel = viewModel
            else { fatalError("viewModel not initialized") }
        guard let pendingGoalResponse = response(from: file)
            else { fatalError("Unable to generate response from file provided") }
        
        ARGoalTracker.parseAndPersistGoalProgressAndDetails(with: pendingGoalResponse)
        
        activityDetailViewModel.selectedGoalTrackerId = 456603
        activityDetailViewModel.configureSelectedGoalItemData()
        guard let selectedGoalItem = activityDetailViewModel.selectedGoalItem else {
            fatalError("failed to get selected goal item")
        }
        
        guard let statusTitle = activityDetailViewModel.selectedGoalItemStatusTitle() else {
            fatalError("expected status title for selected goal item")
        }
        
        XCTAssertEqual(selectedGoalItem.earnedPoints, 140, "Earned points does not match expected value")
        XCTAssertEqual(selectedGoalItem.potentialPoints, 140, "Potential points does not match expected value")
        XCTAssertEqual(statusTitle, "AR.goal_pending_title_693", "Goal title does not match expected value")
        XCTAssertEqual(activityDetailViewModel.selectedGoalItemHasEvents(), false, "Goal has events when it is expected not to")
        XCTAssertEqual(activityDetailViewModel.selectedGoalItemStatusDescription(), nil, "Goal item status description does not match expected value")
        XCTAssertEqual(activityDetailViewModel.selectedGoalItemDateRangeScreenTitle(), "17 Oct 2017 - 19 Oct 2017", "Screen date title does not match expected value")
        XCTAssertEqual(activityDetailViewModel.selectedGoalItemPointsMessage(), "points.x_of_y_points_title_193", "Points message string does not match expected value")
        XCTAssertEqual(activityDetailViewModel.selectedGoalItemStatusIcon(), VIAActiveRewardsAsset.Goals.statusPending.image, "Icon does not match expected value")
    }
    
    func testViewModelProvidedWithAchievedSelectedGoalItemWithActivityGeneratesExpectedDataForDisplay() {
        let file = "goal_achieved_with_activity_response"
        guard let activityDetailViewModel = viewModel
            else { fatalError("viewModel not initialized") }
        guard let achievedGoalResponse = response(from: file)
            else { fatalError("Unable to generate response from file provided") }
        
        ARGoalTracker.parseAndPersistGoalProgressAndDetails(with: achievedGoalResponse)
        
        activityDetailViewModel.selectedGoalTrackerId = 456603
        activityDetailViewModel.configureSelectedGoalItemData()
        guard let selectedGoalItem = activityDetailViewModel.selectedGoalItem else {
            fatalError("failed to get selected goal item")
        }
        
        guard let statusTitle = activityDetailViewModel.selectedGoalItemStatusTitle() else {
            fatalError("expected status title for selected goal item")
        }
        
        XCTAssertEqual(selectedGoalItem.earnedPoints, 140, "Earned points does not match expected value")
        XCTAssertEqual(selectedGoalItem.potentialPoints, 140, "Potential points does not match expected value")
        XCTAssertEqual(statusTitle, "AR.Goal_History.goal_achieved_title_692", "Goal title does not match expected value")
        XCTAssertEqual(activityDetailViewModel.selectedGoalItemHasEvents(), true, "Goal has events when it is expected not to")
        XCTAssertEqual(activityDetailViewModel.numberOfItems(in: 1), 1, "Number of events in section does not match expected value")
        XCTAssertEqual(activityDetailViewModel.selectedGoalItemStatusDescription(), nil, "Goal item status description does not match expected value")
        XCTAssertEqual(activityDetailViewModel.selectedGoalItemDateRangeScreenTitle(), "17 Oct 2017 - 19 Oct 2017", "Screen date title does not match expected value")
        XCTAssertEqual(activityDetailViewModel.selectedGoalItemPointsMessage(), "points.x_of_y_points_title_193", "Points message string does not match expected value")
        XCTAssertEqual(activityDetailViewModel.selectedGoalItemStatusIcon(), VIAActiveRewardsAsset.Goals.statusAchieved.image, "Icon does not match expected value")
        XCTAssertEqual(activityDetailViewModel.descriptionForEvent(at: 0), "Steps", "Activity description does not match expected value")
        XCTAssertEqual(activityDetailViewModel.deviceForEvent(at: 0), "Apple", "Activity device does not match expected value")
        XCTAssertEqual(activityDetailViewModel.dateEarnedForEvent(at: 0) , "Tue, 17 Oct", "Activity date does not match expected value")
        XCTAssertEqual(activityDetailViewModel.pointsForEvent(at: 0) , "140", "Activity points does not match expected value")
    }
    
    func testViewModelProvidedWithInProgressSelectedGoalItemWithActivityGeneratesExpectedDataForDisplay() {
        let file = "goal_in_progress_with_activity_response"
        guard let activityDetailViewModel = viewModel
            else { fatalError("viewModel not initialized") }
        guard let inProgressResponse = response(from: file)
            else { fatalError("Unable to generate response from file provided") }
        
        ARGoalTracker.parseAndPersistGoalProgressAndDetails(with: inProgressResponse)
        
        activityDetailViewModel.selectedGoalTrackerId = 456603
        activityDetailViewModel.configureSelectedGoalItemData()
        guard let selectedGoalItem = activityDetailViewModel.selectedGoalItem else {
            fatalError("failed to get selected goal item")
        }
        
        guard let statusTitle = activityDetailViewModel.selectedGoalItemStatusTitle() else {
            fatalError("expected status title for selected goal item")
        }
        
        XCTAssertEqual(selectedGoalItem.earnedPoints, 100, "Earned points does not match expected value")
        XCTAssertEqual(selectedGoalItem.potentialPoints, 140, "Potential points does not match expected value")
        XCTAssertEqual(statusTitle, "AR.goal_in_progress_title_726", "Goal title does not match expected value")
        XCTAssertEqual(activityDetailViewModel.selectedGoalItemHasEvents(), true, "Goal does not have events when it is expected to")
        XCTAssertEqual(activityDetailViewModel.numberOfItems(in: 1), 1, "Number of events in section does not match expected value")
        XCTAssertEqual(activityDetailViewModel.selectedGoalItemStatusDescription(), nil, "Goal item status description does not match expected value")
        XCTAssertEqual(activityDetailViewModel.selectedGoalItemDateRangeScreenTitle(), "17 Oct 2017 - 19 Oct 2017", "Screen date title does not match expected value")
        XCTAssertEqual(activityDetailViewModel.selectedGoalItemPointsMessage(), "points.x_of_y_points_title_193", "Points message string does not match expected value")
        XCTAssertEqual(activityDetailViewModel.selectedGoalItemStatusIcon(), VIAActiveRewardsAsset.Goals.statusInProgress.image, "Icon does not match expected value")
        XCTAssertEqual(activityDetailViewModel.descriptionForEvent(at: 0), "Steps", "Activity description does not match expected value")
        XCTAssertNil(activityDetailViewModel.deviceForEvent(at: 0), "No device expected for this response")
        XCTAssertEqual(activityDetailViewModel.dateEarnedForEvent(at: 0) , "Tue, 17 Oct", "Activity date does not match expected value")
        XCTAssertEqual(activityDetailViewModel.pointsForEvent(at: 0) , "100", "Activity points does not match expected value")
    }
    
    func testViewModelProvidedWithNotAcievedSelectedGoalItemWithActivityGeneratesExpectedDataForDisplay() {
        let file = "goal_not_achieved_with_activity_response"
        guard let activityDetailViewModel = viewModel
            else { fatalError("viewModel not initialized") }
        guard let notAchivedResponse = response(from: file)
            else { fatalError("Unable to generate response from file provided") }
        
        ARGoalTracker.parseAndPersistGoalProgressAndDetails(with: notAchivedResponse)
        
        activityDetailViewModel.selectedGoalTrackerId = 456603
        activityDetailViewModel.configureSelectedGoalItemData()
        guard let selectedGoalItem = activityDetailViewModel.selectedGoalItem else {
            fatalError("failed to get selected goal item")
        }
        
        guard let statusTitle = activityDetailViewModel.selectedGoalItemStatusTitle() else {
            fatalError("expected status title for selected goal item")
        }
        
        XCTAssertEqual(selectedGoalItem.earnedPoints, 100, "Earned points does not match expected value")
        XCTAssertEqual(selectedGoalItem.potentialPoints, 140, "Potential points does not match expected value")
        XCTAssertEqual(statusTitle, "AR.goal_history.not_achieved_title_709", "Goal title does not match expected value")
        XCTAssertEqual(activityDetailViewModel.selectedGoalItemHasEvents(), true, "Goal does not have events when it is expected to")
        XCTAssertEqual(activityDetailViewModel.numberOfItems(in: 1), 1, "Number of events in section does not match expected value")
        XCTAssertEqual(activityDetailViewModel.selectedGoalItemStatusDescription(), "AR.goal_history.not_achieved_message_846", "Goal item status description does not match expected value")
        XCTAssertEqual(activityDetailViewModel.selectedGoalItemDateRangeScreenTitle(), "17 Oct 2017 - 19 Oct 2017", "Screen date title does not match expected value")
        XCTAssertEqual(activityDetailViewModel.selectedGoalItemPointsMessage(), "points.x_of_y_points_title_193", "Points message string does not match expected value")
        XCTAssertEqual(activityDetailViewModel.selectedGoalItemStatusIcon(), VIAActiveRewardsAsset.Goals.statusNotAchieved.image, "Icon does not match expected value")
        XCTAssertEqual(activityDetailViewModel.descriptionForEvent(at: 0), "Steps", "Activity description does not match expected value")
        XCTAssertEqual(activityDetailViewModel.deviceForEvent(at: 0), "Apple", "Activity device does not match expected value")
        XCTAssertEqual(activityDetailViewModel.dateEarnedForEvent(at: 0) , "Tue, 17 Oct", "Activity date does not match expected value")
        XCTAssertEqual(activityDetailViewModel.pointsForEvent(at: 0) , "100", "Activity points does not match expected value")
    }
    
    func testViewModelProvidedWithPendingSelectedGoalItemWithActivityGeneratesExpectedDataForDisplay() {
        let file = "goal_pending_with_activity_response"
        guard let activityDetailViewModel = viewModel
            else { fatalError("viewModel not initialized") }
        guard let pendingResponse = response(from: file)
            else { fatalError("Unable to generate response from file provided") }
        
        ARGoalTracker.parseAndPersistGoalProgressAndDetails(with: pendingResponse)
        
        activityDetailViewModel.selectedGoalTrackerId = 456603
        activityDetailViewModel.configureSelectedGoalItemData()
        guard let selectedGoalItem = activityDetailViewModel.selectedGoalItem else {
            fatalError("failed to get selected goal item")
        }
        
        guard let statusTitle = activityDetailViewModel.selectedGoalItemStatusTitle() else {
            fatalError("expected status title for selected goal item")
        }
        
        XCTAssertEqual(selectedGoalItem.earnedPoints, 140, "Earned points does not match expected value")
        XCTAssertEqual(selectedGoalItem.potentialPoints, 140, "Potential points does not match expected value")
        XCTAssertEqual(statusTitle, "AR.goal_pending_title_693", "Goal title does not match expected value")
        XCTAssertEqual(activityDetailViewModel.selectedGoalItemHasEvents(), true, "Goal doesn not have events when it is expected to")
        XCTAssertEqual(activityDetailViewModel.numberOfItems(in: 1), 1, "Number of events in section does not match expected value")
        XCTAssertEqual(activityDetailViewModel.selectedGoalItemStatusDescription(), nil, "Goal item status description does not match expected value")
        XCTAssertEqual(activityDetailViewModel.selectedGoalItemDateRangeScreenTitle(), "17 Oct 2017 - 19 Oct 2017", "Screen date title does not match expected value")
        XCTAssertEqual(activityDetailViewModel.selectedGoalItemPointsMessage(), "points.x_of_y_points_title_193", "Points message string does not match expected value")
        XCTAssertEqual(activityDetailViewModel.selectedGoalItemStatusIcon(), VIAActiveRewardsAsset.Goals.statusPending.image, "Icon does not match expected value")
        XCTAssertEqual(activityDetailViewModel.descriptionForEvent(at: 0), "Steps", "Activity description does not match expected value")
        XCTAssertEqual(activityDetailViewModel.deviceForEvent(at: 0), "Apple", "Activity device does not match expected value")
        XCTAssertEqual(activityDetailViewModel.dateEarnedForEvent(at: 0) , "Tue, 17 Oct", "Activity date does not match expected value")
        XCTAssertEqual(activityDetailViewModel.pointsForEvent(at: 0) , "140", "Activity points does not match expected value")
    }
    
    
}
