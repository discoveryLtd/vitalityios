import XCTest

@testable import VIAActiveRewards
@testable import VIAUIKit
@testable import VitalityKit

class VIAARActivityHistoryRequestDateRangesTests: XCTestCase {
    //MARK: Properties
    var dateFormatter = DateFormatter()
    let viewModel = VIAARActivityHistoryViewModel()
    var dateRanges: [VIAARActivityHistoryViewModel.RequestDateRange]?
    var dateTwoYearsAgo: Date?
    
    override func setUp() {
        super.setUp()
        dateFormatter.dateStyle = DateFormatter.Style.long
        dateFormatter.dateFormat = "dd-MM-yyyy"
    }
    
    override func tearDown() {
        super.tearDown()
        dateRanges = nil
        dateTwoYearsAgo = nil
    }
    
    func testDateRangesForPagedRequestsAreGeneratedCorrectlyByActivityHistoryViewModelForDateInBegginingOfTheMonth() {
        if let startDate = dateFormatter.date(from: "01-09-2017") {
            dateRanges = viewModel.configureRequestDatesInThreeMonthPeriods(currentDate: startDate)
            dateTwoYearsAgo = Calendar.current.date(byAdding: .month, value: -24, to: startDate)
        }
        
        guard let ranges = dateRanges else {
            XCTFail("Date Ranges array not set, value is nil")
            return
        }
        
        guard let twoYearsAgo = dateTwoYearsAgo else {
            XCTFail("Date two years ago not set, value is nil")
            return
        }
        
        XCTAssertEqual(9, ranges.count, "Number of range items generated not as expected for 2 year period")
        
        XCTAssertEqual("01-09-2017", dateFormatter.string(from: ranges[0].effectiveTo), "First date range effectiveTo value is incorrect")
        XCTAssertEqual("01-07-2017", dateFormatter.string(from: ranges[0].effectiveFrom), "First date range effectiveFrom value is incorrect")
        
        XCTAssertEqual("30-06-2017", dateFormatter.string(from: ranges[1].effectiveTo), "Second date range effectiveTo value is incorrect")
        XCTAssertEqual("01-04-2017", dateFormatter.string(from: ranges[1].effectiveFrom), "Second date range effectiveFrom value is incorrect")
        
        XCTAssertEqual("31-03-2017", dateFormatter.string(from: ranges[2].effectiveTo), "Third date range effectiveTo value is incorrect")
        XCTAssertEqual("01-01-2017", dateFormatter.string(from: ranges[2].effectiveFrom), "Third date range effectiveFrom value is incorrect")
        
        XCTAssertEqual("31-12-2016", dateFormatter.string(from: ranges[3].effectiveTo), "Fourth date range effectiveTo value is incorrect")
        XCTAssertEqual("01-10-2016", dateFormatter.string(from: ranges[3].effectiveFrom), "Foruth date range effectiveFrom value is incorrect")
        
        XCTAssertEqual("30-09-2016", dateFormatter.string(from: ranges[4].effectiveTo), "Fifth date range effectiveTo value is incorrect")
        XCTAssertEqual("01-07-2016", dateFormatter.string(from: ranges[4].effectiveFrom), "Fifth date range effectiveFrom value is incorrect")
        
        XCTAssertEqual("30-06-2016", dateFormatter.string(from: ranges[5].effectiveTo), "Sixth date range effectiveTo value is incorrect")
        XCTAssertEqual("01-04-2016", dateFormatter.string(from: ranges[5].effectiveFrom), "Sixth date range effectiveFrom value is incorrect")
        
        XCTAssertEqual("31-03-2016", dateFormatter.string(from: ranges[6].effectiveTo), "Seventh date range effectiveTo value is incorrect")
        XCTAssertEqual("01-01-2016", dateFormatter.string(from: ranges[6].effectiveFrom), "Seventh date range effectiveFrom value is incorrect")
        
        XCTAssertEqual("31-12-2015", dateFormatter.string(from: ranges[7].effectiveTo), "Eighth date range effectiveTo value is incorrect")
        XCTAssertEqual("01-10-2015", dateFormatter.string(from: ranges[7].effectiveFrom), "Eighth date range effectiveFrom value is incorrect")
        
        XCTAssertEqual("30-09-2015", dateFormatter.string(from: ranges[8].effectiveTo), "Ninth date range effectiveTo value is incorrect")
        XCTAssertEqual("01-07-2015", dateFormatter.string(from: ranges[8].effectiveFrom), "Ninth date range effectiveFrom value is incorrect")
        
        XCTAssertTrue(ranges[8].effectiveTo.isIn(date: twoYearsAgo, granularity: .year), "Final date range item is not configured correctly for a 2 year period")
        XCTAssertTrue(ranges[8].effectiveTo.isIn(date: twoYearsAgo, granularity: .month), "Final date range item is not configured correctly for a 2 year period")
    }
    
    func testDateRangesForPagedRequestsAreGeneratedCorrectlyByActivityHistoryViewModelForDateInMiddleOfTheMonth() {
        if let startDate = dateFormatter.date(from: "15-09-2017") {
            dateRanges = viewModel.configureRequestDatesInThreeMonthPeriods(currentDate: startDate)
            dateTwoYearsAgo = Calendar.current.date(byAdding: .month, value: -24, to: startDate)
        }
        
        guard let ranges = dateRanges else {
            XCTFail("Date Ranges array not set, value is nil")
            return
        }
        
        guard let twoYearsAgo = dateTwoYearsAgo else {
            XCTFail("Date two years ago not set, value is nil")
            return
        }
        
        XCTAssertEqual(9, ranges.count, "Number of range items generated not as expected for 2 year period")
        
        XCTAssertEqual("15-09-2017", dateFormatter.string(from: ranges[0].effectiveTo), "First date range effectiveTo value is incorrect")
        XCTAssertEqual("01-07-2017", dateFormatter.string(from: ranges[0].effectiveFrom), "First date range effectiveFrom value is incorrect")
        
        XCTAssertEqual("30-06-2017", dateFormatter.string(from: ranges[1].effectiveTo), "Second date range effectiveTo value is incorrect")
        XCTAssertEqual("01-04-2017", dateFormatter.string(from: ranges[1].effectiveFrom), "Second date range effectiveFrom value is incorrect")
        
        XCTAssertEqual("31-03-2017", dateFormatter.string(from: ranges[2].effectiveTo), "Third date range effectiveTo value is incorrect")
        XCTAssertEqual("01-01-2017", dateFormatter.string(from: ranges[2].effectiveFrom), "Third date range effectiveFrom value is incorrect")
        
        XCTAssertEqual("31-12-2016", dateFormatter.string(from: ranges[3].effectiveTo), "Fourth date range effectiveTo value is incorrect")
        XCTAssertEqual("01-10-2016", dateFormatter.string(from: ranges[3].effectiveFrom), "Foruth date range effectiveFrom value is incorrect")
        
        XCTAssertEqual("30-09-2016", dateFormatter.string(from: ranges[4].effectiveTo), "Fifth date range effectiveTo value is incorrect")
        XCTAssertEqual("01-07-2016", dateFormatter.string(from: ranges[4].effectiveFrom), "Fifth date range effectiveFrom value is incorrect")
        
        XCTAssertEqual("30-06-2016", dateFormatter.string(from: ranges[5].effectiveTo), "Sixth date range effectiveTo value is incorrect")
        XCTAssertEqual("01-04-2016", dateFormatter.string(from: ranges[5].effectiveFrom), "Sixth date range effectiveFrom value is incorrect")
        
        XCTAssertEqual("31-03-2016", dateFormatter.string(from: ranges[6].effectiveTo), "Seventh date range effectiveTo value is incorrect")
        XCTAssertEqual("01-01-2016", dateFormatter.string(from: ranges[6].effectiveFrom), "Seventh date range effectiveFrom value is incorrect")
        
        XCTAssertEqual("31-12-2015", dateFormatter.string(from: ranges[7].effectiveTo), "Eighth date range effectiveTo value is incorrect")
        XCTAssertEqual("01-10-2015", dateFormatter.string(from: ranges[7].effectiveFrom), "Eighth date range effectiveFrom value is incorrect")
        
        XCTAssertEqual("30-09-2015", dateFormatter.string(from: ranges[8].effectiveTo), "Ninth date range effectiveTo value is incorrect")
        XCTAssertEqual("01-07-2015", dateFormatter.string(from: ranges[8].effectiveFrom), "Ninth date range effectiveFrom value is incorrect")
        
        XCTAssertTrue(ranges[8].effectiveTo.isIn(date: twoYearsAgo, granularity: .year), "Final date range item is not configured correctly for a 2 year period")
        XCTAssertTrue(ranges[8].effectiveTo.isIn(date: twoYearsAgo, granularity: .month), "Final date range item is not configured correctly for a 2 year period")
    }
    
    func testDateRangesForPagedRequestsAreGeneratedCorrectlyByActivityHistoryViewModelForDateAtTheEndOfTheMonth() {
        if let startDate = dateFormatter.date(from: "30-09-2017") {
            dateRanges = viewModel.configureRequestDatesInThreeMonthPeriods(currentDate: startDate)
            dateTwoYearsAgo = Calendar.current.date(byAdding: .month, value: -24, to: startDate)
        }
        
        guard let ranges = dateRanges else {
            XCTFail("Date Ranges array not set, value is nil")
            return
        }
        
        guard let twoYearsAgo = dateTwoYearsAgo else {
            XCTFail("Date two years ago not set, value is nil")
            return
        }
        
        XCTAssertEqual(9, ranges.count, "Number of range items generated not as expected for 2 year period")
        
        XCTAssertEqual("30-09-2017", dateFormatter.string(from: ranges[0].effectiveTo), "First date range effectiveTo value is incorrect")
        XCTAssertEqual("01-07-2017", dateFormatter.string(from: ranges[0].effectiveFrom), "First date range effectiveFrom value is incorrect")
        
        XCTAssertEqual("30-06-2017", dateFormatter.string(from: ranges[1].effectiveTo), "Second date range effectiveTo value is incorrect")
        XCTAssertEqual("01-04-2017", dateFormatter.string(from: ranges[1].effectiveFrom), "Second date range effectiveFrom value is incorrect")
        
        XCTAssertEqual("31-03-2017", dateFormatter.string(from: ranges[2].effectiveTo), "Third date range effectiveTo value is incorrect")
        XCTAssertEqual("01-01-2017", dateFormatter.string(from: ranges[2].effectiveFrom), "Third date range effectiveFrom value is incorrect")
        
        XCTAssertEqual("31-12-2016", dateFormatter.string(from: ranges[3].effectiveTo), "Fourth date range effectiveTo value is incorrect")
        XCTAssertEqual("01-10-2016", dateFormatter.string(from: ranges[3].effectiveFrom), "Foruth date range effectiveFrom value is incorrect")
        
        XCTAssertEqual("30-09-2016", dateFormatter.string(from: ranges[4].effectiveTo), "Fifth date range effectiveTo value is incorrect")
        XCTAssertEqual("01-07-2016", dateFormatter.string(from: ranges[4].effectiveFrom), "Fifth date range effectiveFrom value is incorrect")
        
        XCTAssertEqual("30-06-2016", dateFormatter.string(from: ranges[5].effectiveTo), "Sixth date range effectiveTo value is incorrect")
        XCTAssertEqual("01-04-2016", dateFormatter.string(from: ranges[5].effectiveFrom), "Sixth date range effectiveFrom value is incorrect")
        
        XCTAssertEqual("31-03-2016", dateFormatter.string(from: ranges[6].effectiveTo), "Seventh date range effectiveTo value is incorrect")
        XCTAssertEqual("01-01-2016", dateFormatter.string(from: ranges[6].effectiveFrom), "Seventh date range effectiveFrom value is incorrect")
        
        XCTAssertEqual("31-12-2015", dateFormatter.string(from: ranges[7].effectiveTo), "Eighth date range effectiveTo value is incorrect")
        XCTAssertEqual("01-10-2015", dateFormatter.string(from: ranges[7].effectiveFrom), "Eighth date range effectiveFrom value is incorrect")
        
        XCTAssertEqual("30-09-2015", dateFormatter.string(from: ranges[8].effectiveTo), "Ninth date range effectiveTo value is incorrect")
        XCTAssertEqual("01-07-2015", dateFormatter.string(from: ranges[8].effectiveFrom), "Ninth date range effectiveFrom value is incorrect")
        
        XCTAssertTrue(ranges[8].effectiveTo.isIn(date: twoYearsAgo, granularity: .year), "Final date range item is not configured correctly for a 2 year period")
        XCTAssertTrue(ranges[8].effectiveTo.isIn(date: twoYearsAgo, granularity: .month), "Final date range item is not configured correctly for a 2 year period")
    }
    
}
