//
//  OnboardingGetStartedProbabilisticViewModelTest.swift
//  VitalityActive
//
//  Created by jacdevos on 2016/10/24.
//  Copyright © 2016 Glucode. All rights reserved.


import XCTest
import VIAActiveRewards

@testable import VIAActiveRewards
@testable import VIAUIKit
@testable import VitalityKit

class VIAAROnboardingModelTest: XCTestCase {

    func testPropertiesShouldMatchSpecifiedStringsForOnboardingGetStartedProbabilisticViewModel() {
        let viewModel = VIAAROnboardingGetStartedARProbabilisticViewModel()

        XCTAssertEqual(4, viewModel.contentItems.count, "Incorrect amount of contentItems")
        XCTAssertEqual(ActiveRewardsStrings.Ar.Onboarding.ProbabilisticItem4698, viewModel.contentItems[3].content, "contentItem incorrect at index \(3)")
    }

    func testPropertiesShouldMatchSpecifiedStringsForOnboardingGetStartedChoiceViewModel() {
        let viewModel = VIAAROnboardingGetStartedARChoiceViewModel()

        XCTAssertEqual(4, viewModel.contentItems.count, "Incorrect amount of contentItems")
        XCTAssertEqual(ActiveRewardsStrings.Ar.Onboarding.DefinedItem4732, viewModel.contentItems[3].content, "contentItem incorrect at index \(3)")
    }
}
