import XCTest
import RealmSwift

@testable import VitalityKit
@testable import VIAActiveRewards

class EffectiveDateFilterableActiveRewardsTests: XCTestCase {
    var goalProgressDetailsResponse: GoalProgressDetailsResponse?
    var viewModel: VIAARActivityHistoryViewModel?
    
    func response(from file: String) -> GPDResponseGetGoalProgressAndDetails? {
        var response: GPDResponseGetGoalProgressAndDetails? = nil
        guard let pathString = Bundle(for: type(of: self)).path(forResource: file, ofType: "json") else {
            fatalError("\(file) not found")
        }
        
        guard let jsonString = try? NSString(contentsOfFile: pathString, encoding: String.Encoding.utf8.rawValue) else {
            fatalError("Unable to convert \(file) to String")
        }
        
        print("The JSON string is: \(jsonString)")
        
        guard let jsonData = jsonString.data(using: String.Encoding.utf8.rawValue) else {
            fatalError("Unable to convert \(file) to NSData")
        }
        
        do {
            guard let jsonDictionary = try JSONSerialization.jsonObject(with: jsonData, options: []) as? [String:Any] else {
                fatalError("Unable to convert \(file) to JSON dictionary")
            }
            response = GoalProgressDetailsResponse.init(json: jsonDictionary)!.getGoalProgressAndDetailsResponse
        } catch _ {
            
        }
        
        return response
    }
    
    override func setUp() {
        super.setUp()
        let realm = DataProvider.newARRealm()
        try! realm.write {
            realm.deleteAll()
        }
        viewModel = VIAARActivityHistoryViewModel()
    }
    
    override func tearDown() {
        viewModel = nil
        let realm = DataProvider.newARRealm()
        try! realm.write {
            realm.deleteAll()
        }
        super.tearDown()
    }
    
    func testCallingFilterForElementsBetweenStartAndEndReturnsExpectedPeriodsForGoals() {
        let file = "get_goal_progress_and_details_seven_months_response"
        guard let goalHistoryViewModel = viewModel else {
            fatalError("viewModel not initialized") }
        guard let goalResponse = response(from: file) else {
            fatalError("Unable to generate response from file provided")
        }
        
        ARGoalTracker.parseAndPersistGoalProgressAndDetails(with: goalResponse)
        
        // configureSectionsAndGoalItemData() calls testfilterForElementsBetweenStartAndEnd()
        goalHistoryViewModel.configureSectionsAndGoalItemData()

        XCTAssertEqual(goalHistoryViewModel.numberOfDateSections(), 7, "Section count does not match expected value")
        
        XCTAssertEqual(goalHistoryViewModel.dateTitle(for: 0), "OCTOBER 2017", "Date section title does not match expected value")
        XCTAssertEqual(goalHistoryViewModel.dateTitle(for: 1), "SEPTEMBER 2017", "Date section title does not match expected value")
        XCTAssertEqual(goalHistoryViewModel.dateTitle(for: 2), "AUGUST 2017", "Date section title does not match expected value")
        XCTAssertEqual(goalHistoryViewModel.dateTitle(for: 3), "JULY 2017", "Date section title does not match expected value")
        XCTAssertEqual(goalHistoryViewModel.dateTitle(for: 4), "JUNE 2017", "Date section title does not match expected value")
        XCTAssertEqual(goalHistoryViewModel.dateTitle(for: 5), "MAY 2017", "Date section title does not match expected value")
         XCTAssertEqual(goalHistoryViewModel.dateTitle(for: 6), "APRIL 2017", "Date section title does not match expected value")
        
        XCTAssertEqual(goalHistoryViewModel.numberOfGoalItems(in: 0), 2, "Number of goal items in section does not match expected value")
        XCTAssertEqual(goalHistoryViewModel.numberOfGoalItems(in: 1), 2, "Number of goal items in section does not match expected value")
        XCTAssertEqual(goalHistoryViewModel.numberOfGoalItems(in: 2), 2, "Number of goal items in section does not match expected value")
        XCTAssertEqual(goalHistoryViewModel.numberOfGoalItems(in: 3), 2, "Number of goal items in section does not match expected value")
        XCTAssertEqual(goalHistoryViewModel.numberOfGoalItems(in: 4), 2, "Number of goal items in section does not match expected value")
        XCTAssertEqual(goalHistoryViewModel.numberOfGoalItems(in: 5), 2, "Number of goal items in section does not match expected value")
        XCTAssertEqual(goalHistoryViewModel.numberOfGoalItems(in: 6), 3, "Number of goal items in section does not match expected value")
    }
    
}
