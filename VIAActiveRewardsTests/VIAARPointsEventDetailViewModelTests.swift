import XCTest
import VitalityKit
import RealmSwift

@testable import VIAActiveRewards
@testable import VIAUIKit
@testable import VitalityKit

class VIAARPointsEventDetailViewModelTests: XCTestCase {
    var goalProgressDetailsResponse: GoalProgressDetailsResponse?
    var viewModel: VIAARPointsEventDetailViewModel?
    
    func response(from file: String) -> GPDResponseGetGoalProgressAndDetails? {
        var response: GPDResponseGetGoalProgressAndDetails? = nil
        guard let pathString = Bundle(for: type(of: self)).path(forResource: file, ofType: "json") else {
            fatalError("\(file) not found")
        }
        
        guard let jsonString = try? NSString(contentsOfFile: pathString, encoding: String.Encoding.utf8.rawValue) else {
            fatalError("Unable to convert \(file) to String")
        }
        
        print("The JSON string is: \(jsonString)")
        
        guard let jsonData = jsonString.data(using: String.Encoding.utf8.rawValue) else {
            fatalError("Unable to convert \(file) to NSData")
        }
        
        do {
            guard let jsonDictionary = try JSONSerialization.jsonObject(with: jsonData, options: []) as? [String:Any] else {
                fatalError("Unable to convert \(file) to JSON dictionary")
            }
            response = GoalProgressDetailsResponse.init(json: jsonDictionary)!.getGoalProgressAndDetailsResponse
        } catch _ {
            
        }
        
        return response
    }
    
    override func setUp() {
        super.setUp()
        let realm = DataProvider.newARRealm()
        try! realm.write {
            realm.deleteAll()
        }
        viewModel = VIAARPointsEventDetailViewModel()
    }
    
    override func tearDown() {
        viewModel = nil
        let realm = DataProvider.newARRealm()
        try! realm.write {
            realm.deleteAll()
        }
        super.tearDown()
    }
    
    func testViewModelProvidedWithEventWhichHasDeviceDetailsAndOtherMetadataItemsGeneratesExpectedDataForDisplay() {
        let file = "goal_achieved_with_activity_response"
        guard let eventDetailModel = viewModel
            else { fatalError("viewModel not initialized") }
        guard let achievedWithActivityResponse = response(from: file)
            else { fatalError("Unable to generate response from file provided") }
        
        ARGoalTracker.parseAndPersistGoalProgressAndDetails(with: achievedWithActivityResponse)
        
        eventDetailModel.selectedEventId = 21296927
        eventDetailModel.configureSelectedEventData()
        
        guard let selectedEvent = eventDetailModel.selectedEventDetailItem else {
            fatalError("failed to get selected goal item")
        }
        
        XCTAssertEqual(eventDetailModel.numberOfVisibleSections(), 3, "Number of visible sections does not match expected value")
        XCTAssertEqual(eventDetailModel.title(for: 0), "AR.points_event_detail.points_section_header_title_716", "Points section title does not match expected value")
        XCTAssertEqual(eventDetailModel.title(for: 1), "AR.points_event_detail.device_section_header_title_700", "Device section title does not match expected value")
        XCTAssertEqual(eventDetailModel.title(for: 2), "AR.points_event_detail.details_section_header_title_735", "Points section title does not match expected value")
        //Device should not appear in details section and we add Event and Date metadata so in total 10 items should appear in details section
        XCTAssertEqual(eventDetailModel.numberOfItems(in: 2) , 10, "Number of metadata items in details section does not match expected value")
        
        XCTAssertEqual(selectedEvent.earnedPoints, 140, "Earned Points does not match expected value")
        
        XCTAssertNotNil(selectedEvent.deviceName, "deviceName is expected to have a value for this response")
        XCTAssertEqual(selectedEvent.deviceName, "Apple", "Device name does not match expected value")
        //First item should always be Event and last item should always be Date
        XCTAssertEqual(selectedEvent.metadataItem(at: 0)?.title, "AR.points_event_detail.cell_title_684", "Event metadata item title does not match expected value")
        XCTAssertEqual(selectedEvent.metadataItem(at: 0)?.description, "Steps", "Event metadata description does not match expected value")
        XCTAssertEqual(selectedEvent.metadataItem(at: 9)?.title, "date_title_264", "Date metadata item title does not match expected value")
        XCTAssertEqual(selectedEvent.metadataItem(at: 9)?.description, "Tuesday, 17 October 2017", "Date metadata description does not match expected value")
        
        XCTAssertEqual(selectedEvent.metadataItem(at: 1)?.title, "Start Time", "Start Time metadata item title does not match expected value")
        XCTAssertEqual(selectedEvent.metadataItem(at: 1)?.description, "12:00 PM", "Start Time metadata description does not match expected value")
        XCTAssertEqual(selectedEvent.metadataItem(at: 2)?.title, "End Time", "End Time metadata item title does not match expected value")
        XCTAssertEqual(selectedEvent.metadataItem(at: 2)?.description, "12:38 PM", "End Time metadata description does not match expected value")
        XCTAssertEqual(selectedEvent.metadataItem(at: 3)?.title, "Total Steps", "Total Steps metadata item title does not match expected value")
        XCTAssertEqual(selectedEvent.metadataItem(at: 3)?.description, "15 200", "Total Steps metadata description does not match expected value")
        XCTAssertEqual(selectedEvent.metadataItem(at: 4)?.title, "Floors Ascended", "Floors Ascended metadata item title does not match expected value")
        XCTAssertEqual(selectedEvent.metadataItem(at: 4)?.description, "5", "Floors Ascended metadata description does not match expected value")
        XCTAssertEqual(selectedEvent.metadataItem(at: 5)?.title, "Distance", "Distance metadata item title does not match expected value")
        XCTAssertEqual(selectedEvent.metadataItem(at: 5)?.description, "3 934,56 ", "Distance metadata description does not match expected value")
        XCTAssertEqual(selectedEvent.metadataItem(at: 6)?.title, "Device Data Category", "Device Data Category metadata item title does not match expected value")
        XCTAssertEqual(selectedEvent.metadataItem(at: 6)?.description, "Fitness", "Device Data Category metadata description does not match expected value")
        XCTAssertEqual(selectedEvent.metadataItem(at: 7)?.title, "Total Energy Expenditure", "Total Energy Expenditure metadata item title does not match expected value")
        XCTAssertEqual(selectedEvent.metadataItem(at: 7)?.description, "1 840 ", "Total Energy Expenditure metadata description does not match expected value")
        XCTAssertEqual(selectedEvent.metadataItem(at: 8)?.title, "Reading Type", "Reading Type metadata item title does not match expected value")
        XCTAssertEqual(selectedEvent.metadataItem(at: 8)?.description, "walk", "Reading Type metadata description does not match expected value")
    }
    
    func testViewModelProvidedWithEventWhichDoesNotHaveDeviceDetailsAndHasOtherMetadataItemsGeneratesExpectedDataForDisplay() {
        let file = "goal_in_progress_with_activity_response"
        guard let eventDetailModel = viewModel
            else { fatalError("viewModel not initialized") }
        guard let inProgressWithActivityResponse = response(from: file)
            else { fatalError("Unable to generate response from file provided") }
        
        ARGoalTracker.parseAndPersistGoalProgressAndDetails(with: inProgressWithActivityResponse)
        
        eventDetailModel.selectedEventId = 21296927
        eventDetailModel.configureSelectedEventData()
        
        guard let selectedEvent = eventDetailModel.selectedEventDetailItem else {
            fatalError("failed to get selected goal item")
        }
        //Device data not returned in response, device section should not be visible so only 2 visible sections expected
        XCTAssertEqual(eventDetailModel.numberOfVisibleSections(), 2, "Number of visible sections does not match expected value")
        XCTAssertEqual(eventDetailModel.title(for: 0), "AR.points_event_detail.points_section_header_title_716", "Points section title does not match expected value")
        XCTAssertEqual(eventDetailModel.title(for: 1), "AR.points_event_detail.details_section_header_title_735", "Details section title does not match expected value")
        //Device should not appear in details section and we add Event and Date metadata so in total 10 items should appear in details section
        XCTAssertEqual(eventDetailModel.numberOfItems(in: 1) , 10, "Number of metadata items in details section does not match expected value")
        
        XCTAssertEqual(selectedEvent.earnedPoints, 100, "Earned Points does not match expected value")
        
        XCTAssertNil(selectedEvent.deviceName, "deviceName is not expected to have a value for this response")
        //First item should always be Event and last item should always be Date
        XCTAssertEqual(selectedEvent.metadataItem(at: 0)?.title, "AR.points_event_detail.cell_title_684", "Event metadata item title does not match expected value")
        XCTAssertEqual(selectedEvent.metadataItem(at: 0)?.description, "Steps", "Event metadata description does not match expected value")
        XCTAssertEqual(selectedEvent.metadataItem(at: 9)?.title, "date_title_264", "Date metadata item title does not match expected value")
        XCTAssertEqual(selectedEvent.metadataItem(at: 9)?.description, "Tuesday, 17 October 2017", "Date metadata description does not match expected value")
        
        XCTAssertEqual(selectedEvent.metadataItem(at: 1)?.title, "Start Time", "Start Time metadata item title does not match expected value")
        XCTAssertEqual(selectedEvent.metadataItem(at: 1)?.description, "12:00 PM", "Start Time metadata description does not match expected value")
        XCTAssertEqual(selectedEvent.metadataItem(at: 2)?.title, "End Time", "End Time metadata item title does not match expected value")
        XCTAssertEqual(selectedEvent.metadataItem(at: 2)?.description, "12:38 PM", "End Time metadata description does not match expected value")
        XCTAssertEqual(selectedEvent.metadataItem(at: 3)?.title, "Total Steps", "Total Steps metadata item title does not match expected value")
        XCTAssertEqual(selectedEvent.metadataItem(at: 3)?.description, "15 200", "Total Steps metadata description does not match expected value")
        XCTAssertEqual(selectedEvent.metadataItem(at: 4)?.title, "Floors Ascended", "Floors Ascended metadata item title does not match expected value")
        XCTAssertEqual(selectedEvent.metadataItem(at: 4)?.description, "5", "Floors Ascended metadata description does not match expected value")
        XCTAssertEqual(selectedEvent.metadataItem(at: 5)?.title, "Distance", "Distance metadata item title does not match expected value")
        XCTAssertEqual(selectedEvent.metadataItem(at: 5)?.description, "3 934,56 ", "Distance metadata description does not match expected value")
        XCTAssertEqual(selectedEvent.metadataItem(at: 6)?.title, "Device Data Category", "Device Data Category metadata item title does not match expected value")
        XCTAssertEqual(selectedEvent.metadataItem(at: 6)?.description, "Fitness", "Device Data Category metadata description does not match expected value")
        XCTAssertEqual(selectedEvent.metadataItem(at: 7)?.title, "Total Energy Expenditure", "Total Energy Expenditure metadata item title does not match expected value")
        XCTAssertEqual(selectedEvent.metadataItem(at: 7)?.description, "1 840 ", "Total Energy Expenditure metadata description does not match expected value")
        XCTAssertEqual(selectedEvent.metadataItem(at: 8)?.title, "Reading Type", "Reading Type metadata item title does not match expected value")
        XCTAssertEqual(selectedEvent.metadataItem(at: 8)?.description, "walk", "Reading Type metadata description does not match expected value")
    }
    
    func testViewModelProvidedWithEventWhichDoesNotHaveDeviceDetailsAndNoOtherMetadataItemsGeneratesExpectedDataForDisplay() {
        let file = "goal_achieved_with_activity_no_metadata_response"
        guard let eventDetailModel = viewModel
            else { fatalError("viewModel not initialized") }
        guard let inProgressWithActivityResponse = response(from: file)
            else { fatalError("Unable to generate response from file provided") }
        
        ARGoalTracker.parseAndPersistGoalProgressAndDetails(with: inProgressWithActivityResponse)
        
        eventDetailModel.selectedEventId = 21296927
        eventDetailModel.configureSelectedEventData()
        
        guard let selectedEvent = eventDetailModel.selectedEventDetailItem else {
            fatalError("failed to get selected goal item")
        }
        //Device data not returned in response, device section should not be visible so only 2 visible sections expected
        XCTAssertEqual(eventDetailModel.numberOfVisibleSections(), 2, "Number of visible sections does not match expected value")
        XCTAssertEqual(eventDetailModel.title(for: 0), "AR.points_event_detail.points_section_header_title_716", "Points section title does not match expected value")
        XCTAssertEqual(eventDetailModel.title(for: 1), "AR.points_event_detail.details_section_header_title_735", "Details section title does not match expected value")
        //Device should not appear in details section and we add Event and Date metadata even if no other metadata is returned in the response so in total 2 items should appear in details section
        XCTAssertEqual(eventDetailModel.numberOfItems(in: 1) , 2, "Number of metadata items in details section does not match expected value")
        
        XCTAssertEqual(selectedEvent.earnedPoints, 140, "Earned Points does not match expected value")
        
        XCTAssertNil(selectedEvent.deviceName, "deviceName is not expected to have a value for this response")
        //First item should always be Event and last item should always be Date
        XCTAssertEqual(selectedEvent.metadataItem(at: 0)?.title, "AR.points_event_detail.cell_title_684", "Event metadata item title does not match expected value")
        XCTAssertEqual(selectedEvent.metadataItem(at: 0)?.description, "Steps", "Event metadata description does not match expected value")
        XCTAssertEqual(selectedEvent.metadataItem(at: 1)?.title, "date_title_264", "Date metadata item title does not match expected value")
        XCTAssertEqual(selectedEvent.metadataItem(at: 1)?.description, "Tuesday, 17 October 2017", "Date metadata description does not match expected value")
    }
    
}
