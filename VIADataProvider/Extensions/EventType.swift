import Foundation
import VIAUtilities

extension EventTypeRef {
    public static func configuredTypesForVHC() -> [EventTypeRef] {
        let realm = DataProvider.newRealm()
        let featureTypes = VitalityProductFeature.allVHCFeatureTypes().flatMap({ $0.rawValue })
        let productFeatures = realm.allProductFeatures().filter("productFeatureType IN %@", featureTypes)
        let types = productFeatures.flatMap({ $0.eventType() }) as Array<EventTypeRef>
        return types
    }
}
