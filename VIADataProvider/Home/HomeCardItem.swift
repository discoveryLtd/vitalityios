//
//  CardItem.swift
//  VitalityActive
//
//  Created by Lionel Camacho on 2017/02/27.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import RealmSwift

public class HomeCardItem: Object {
    public dynamic var validFrom: Date?
    public dynamic var validTo: Date?
    public dynamic var statusTypeName: String = ""
    public dynamic var typeKey: Int = 0 //TODO: make enum
    public dynamic var statusTypeKey: Int = 0 //TODO: make enum
    public dynamic var typeName: String = ""
    public dynamic var typeCode: String = ""
    public dynamic var statusTypeCode: String = ""

    public let homeCardItemMetadatas = List<HomeCardItemMetadata>()
}

extension Realm {
    public func allCardItems() -> Results<HomeCardItem> {
        return self.objects(HomeCardItem.self)
    }
}
