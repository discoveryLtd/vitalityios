//
//  Card.swift
//  VitalityActive
//
//  Created by Lionel Camacho on 2017/02/27.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import RealmSwift
import VIAUtilities

public class HomeCard: Object {
    public dynamic var validFrom: Date?
    public dynamic var validTo: Date?
    public dynamic var amountCompleted: Int = 0
    public dynamic var status: CardStatusTypeRef = CardStatusTypeRef(rawValue: CardStatusTypeRef.Unknown.rawValue) ?? .Unknown
    public dynamic var unit: CardUnitTypeRef = CardUnitTypeRef(rawValue: CardUnitTypeRef.Unknown.rawValue) ?? .Unknown
    public dynamic var priortity: Int = 0
    public dynamic var total: Int = 0
    public dynamic var type: CardTypeRef = CardTypeRef(rawValue: CardTypeRef.Unknown.rawValue) ?? .Unknown
    public dynamic var metadataPotentialPoints: String = ""
    public dynamic var metadataRewardType: String = ""
    public dynamic var metadataEarnedPoints: String = ""
    public let cardItems = List<HomeCardItem>()

    override public static func primaryKey() -> String? {
        return "type"
    }
}

extension Realm {
    public func allHomeCards() -> Results<HomeCard> {
        return self.objects(HomeCard.self)
    }
}
