//
//  Status.swift
//  VitalityActive
//
//  Created by Lionel Camacho on 2017/02/27.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import RealmSwift

public class HomeVitalityStatus: Object {
    public dynamic var effectiveTo: Date?
    public dynamic var effectiveFrom: Date?
    public dynamic var statusKey: Int = 0
    public dynamic var categoryKey: Int = 0
    public dynamic var statusName: String = ""
    public dynamic var categoryCode: String = ""
    public dynamic var categoryName: String = ""
    public dynamic var statusCode: String = ""
}

extension Realm {
    public func HomeVitalityStatuses() -> Results<HomeVitalityStatus> {
        return self.objects(HomeVitalityStatus.self)
    }
}
