//
//  cardItemMetadata.swift
//  VitalityActive
//
//  Created by Lionel Camacho on 2017/02/27.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import RealmSwift

public class HomeCardItemMetadata: Object {

    public dynamic var typeKey: Int = 0 //TODO: make enum
    public dynamic var typeName: String = ""
    public dynamic var typeCode: String = ""
    public dynamic var value: String = ""

}

extension Realm {
    public func allHomeCardItemMetadatas() -> Results<HomeCardItemMetadata> {
        return self.objects(HomeCardItemMetadata.self)
    }
}
