//
//  Section.swift
//  VitalityActive
//
//  Created by Lionel Camacho on 2017/02/27.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import RealmSwift
import VIAUtilities

public class HomeSection: Object {
    public dynamic var type: SectionTypeRef = SectionTypeRef(rawValue: SectionTypeRef.Unknown.rawValue) ?? .Unknown
    public dynamic var priority: Bool = false

    public let cards = List<HomeCard>()

    override public static func primaryKey() -> String? {
        return "type"
    }
}

extension Realm {
    public func allHomeSections() -> Results<HomeSection> {
        return self.objects(HomeSection.self)
    }
}
