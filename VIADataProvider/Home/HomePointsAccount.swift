//
//  HomePointsAccount.swift
//  VitalityActive
//
//  Created by Lionel Camacho on 2017/02/27.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import RealmSwift

public class HomePointsAccount: Object {
    public dynamic var pointsTotal: Int = 0
}

extension Realm {
    public func HomePointsAccounts() -> Results<HomePointsAccount> {
        return self.objects(HomePointsAccount.self)
    }
}
