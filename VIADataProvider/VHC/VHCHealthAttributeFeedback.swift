//
//  VHCHealthAttributeFeedback.swift
//  VitalityActive
//
//  Created by Lionel Camacho on 2017/03/16.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import RealmSwift
import VIAUtilities

public class VHCHealthAttributeFeedback: Object {

    public dynamic var feedbackTypeName: String = ""
    public dynamic var feedbackCategoryName: String = ""

    public dynamic var type: PartyAttributeFeedbackRef = PartyAttributeFeedbackRef(rawValue: PartyAttributeFeedbackRef.Unknown.rawValue) ?? .Unknown
    public dynamic var category: PartyAttributeFeedbackTypeRef = PartyAttributeFeedbackTypeRef(rawValue: PartyAttributeFeedbackTypeRef.Unknown.rawValue) ?? .Unknown

}

extension Realm {
    public func allVHCHealthAttributeFeedback() -> Results<VHCHealthAttributeFeedback> {
        return self.objects(VHCHealthAttributeFeedback.self)
    }
}
