//
//  VHCEvent.swift
//  VitalityActive
//
//  Created by Lionel Camacho on 2017/03/15.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import RealmSwift

public class VHCEvent: Object {

    public dynamic var eventDateTime: Date?
    public dynamic var eventId: Int = 0
    public dynamic var eventSource: VHCEventSource?
    //We take the first points entry from PointsEntries and the first reason from that points entry
    public dynamic var reasonName: String = ""
    public dynamic var reasonKey: Int = 0
    public dynamic var reasonCode: String = ""
    public let eventMetaData = List<VHCEventMetaData>()
    public let healthAttributeReadings = List<VHCHealthAttributeReading>()
    public let pointsEntries = List<VHCEventPointsEntry>()
    public let eventType = LinkingObjects(fromType: VHCEventType.self, property: "events")

}

extension Realm {
    public func allVHCEvents() -> Results<VHCEvent> {
        return self.objects(VHCEvent.self)
    }
}
