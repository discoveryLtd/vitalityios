import Foundation
import RealmSwift

public class VHCCapturedResult: Object {
    public dynamic var isValid: Bool = false
    public dynamic var dateCaptured = Date()
    public dynamic var unitOfMeasureType: VHCHealthAttributeValidValues.UnitOfMeasureType = .Unknown
    public dynamic var rawInput: String?
    public dynamic var inputUnitSymbol: String?
    public dynamic var value = Double()
    public dynamic var healthAttributeType: PartyAttributeTypeRef = .Unknown

    override public class func primaryKey() -> String? {
        return "healthAttributeType"
    }

    public func measurement() -> Measurement<Unit>? {
        if let symbol = self.inputUnitSymbol {
            let unit = Unit(symbol: symbol)
            let measurement = Measurement(value: self.value, unit: unit)
            return measurement
        }
        return nil
    }

    public func eventType() -> EventTypeRef {
        let realm = DataProvider.newRealm()
        if let attribute = realm.vhcHealthAttribute(of: self.healthAttributeType) {
            return attribute.eventType.first?.type ?? .Unknown
        }
        return .Unknown
    }

    public static func deleteAll() {
        let realm = DataProvider.newRealm()
        try! realm.write {
            realm.delete(realm.allCapturedResults())
        }
    }
}

extension Realm {
    public func captureResult(type: PartyAttributeTypeRef,
                              date: Date = Date(),
                              unitOfMeasureType: VHCHealthAttributeValidValues.UnitOfMeasureType?,
                              value: Double,
                              isValid: Bool,
                              rawInput: String?,
                              inputUnitSymbol: String?) {
        let capturedResult = VHCCapturedResult()
        try! self.write {
            capturedResult.isValid = isValid
            capturedResult.dateCaptured = date
            capturedResult.unitOfMeasureType = unitOfMeasureType ?? .Unknown
            capturedResult.value = value
            capturedResult.healthAttributeType = type
            capturedResult.rawInput = rawInput
            capturedResult.inputUnitSymbol = inputUnitSymbol

            self.create(VHCCapturedResult.self, value: capturedResult, update: true)
            self.add(capturedResult, update: true)
        }
    }

    public func allCapturedResults() -> Results<VHCCapturedResult> {
        return self.objects(VHCCapturedResult.self)
    }

    public func vhcCapturedResult(of type: PartyAttributeTypeRef) -> VHCCapturedResult? {
        let attributes = self.allCapturedResults()
        let filtered = attributes.filter("healthAttributeType == %@", type.rawValue).first
        return filtered
    }
}
