import Foundation
import RealmSwift
import VIAUtilities

public class VHCPointsReason: Object {
    public dynamic var reasonKey: Int = 0
    public dynamic var reasonName: String = ""
    public dynamic var reasonCode: String = ""

    public dynamic var categoryKey: Int = 0
    public dynamic var categoryName: String = ""
    public dynamic var categoryCode: String = ""
}

extension Realm {
    public func allVHCPointsReasons() -> Results<VHCPointsReason> {
        return self.objects(VHCPointsReason.self)
    }
}
