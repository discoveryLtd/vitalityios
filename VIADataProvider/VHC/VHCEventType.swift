import Foundation
import RealmSwift
import VIAUtilities

public class VHCEventType: Object {
    public dynamic var totalEarnedPoints: Int = 0
    public dynamic var totalPotentialPoints: Int = 0
    public dynamic var typeName: String = ""
    public dynamic var type: EventTypeRef = EventTypeRef(rawValue: EventTypeRef.Unknown.rawValue) ?? .Unknown

    public let events = List<VHCEvent>()
    public let healthAttributes = List<VHCHealthAttribute>()

    override public static func primaryKey() -> String? {
        return "type"
    }
}

extension Realm {
    public func allVHCPotentialPointsEventTypes() -> Results<VHCEventType> {
        return self.objects(VHCEventType.self)
    }
}
