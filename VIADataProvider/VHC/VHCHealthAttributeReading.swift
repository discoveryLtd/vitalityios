//
//  VHCHealthAttributeReadings.swift
//  VitalityActive
//
//  Created by Lionel Camacho on 2017/03/15.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import RealmSwift

public class VHCHealthAttributeReading: Object {
    //TODO:- Use enums for type
    public dynamic var healthAttributeTypeCode: String = ""
    public dynamic var healthAttributeTypeName: String = ""
    public dynamic var healthAttributeTypeKey: Int = 0
    public dynamic var measuredOn: Date?
    public dynamic var unitOfMeasure: String = ""
    public let value = RealmOptional<Double>()

    public let healthAttributeFeedbacks = List<VHCHealthAttributeFeedback>()

    public let event = LinkingObjects(fromType: VHCEvent.self, property: "healthAttributeReadings")
}

extension Realm {
    public func allVHCHealthAttributeReadings() -> Results<VHCHealthAttributeReading> {
        return self.objects(VHCHealthAttributeReading.self)
    }

    public func getAllVHCHealthAttributeReadingsFor(healthAttributeKey: Int) -> Results<VHCHealthAttributeReading> {
        let readings = self.allVHCHealthAttributeReadings()
        let filtered = readings.filter("healthAttributeTypeKey == %@", healthAttributeKey).sorted(byKeyPath: "measuredOn")
        return filtered
    }

    public func getLatestVHCHealthAttributeReadingFor(healthAttributeKey: Int) -> VHCHealthAttributeReading? {
        let readings = self.allVHCHealthAttributeReadings()
        let filtered = readings.filter("healthAttributeTypeKey == %@", healthAttributeKey).sorted(byKeyPath: "measuredOn")
        return filtered.first
    }
}
