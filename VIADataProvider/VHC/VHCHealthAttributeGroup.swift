import Foundation
import RealmSwift
import VIAUtilities

public class VHCHealthAttributeGroup: Object {
    public dynamic var name: String = ""
    public dynamic var type: ProductFeatureTypeRef = ProductFeatureTypeRef(rawValue: ProductFeatureTypeRef.Unknown.rawValue) ?? .Unknown

    public let healthAttributes = List<VHCHealthAttribute>()

    override public static func primaryKey() -> String? {
        return "type"
    }

    public func containsBothBloodPressureMeasurables() -> Bool {
        let containsSystolic = self.healthAttributes.contains(where: { $0.type == .BloodPressureSystol })
        let containsDiastolic = self.healthAttributes.contains(where: { $0.type == .BloodPressureDiasto })
        return containsSystolic && containsDiastolic
    }

    public func healthAttribute(of type: PartyAttributeTypeRef) -> VHCHealthAttribute? {
        return self.healthAttributes.filter({ $0.type == type }).first
    }

}

extension Realm {
    public func allVHCHealthAttributeGroups() -> Results<VHCHealthAttributeGroup> {
        return self.objects(VHCHealthAttributeGroup.self).sorted(byKeyPath: "name", ascending: true)
    }

    public func getVHCHealthAttributeGroupsBy(type: ProductFeatureTypeRef) -> Results<VHCHealthAttributeGroup> {
        return self.objects(VHCHealthAttributeGroup.self).filter("type == \(type)")
    }

    public func getVHCHealthAttributeGroupBy(type: ProductFeatureTypeRef) -> VHCHealthAttributeGroup? {
        return self.objects(VHCHealthAttributeGroup.self).filter("type == \(type.rawValue)").first
    }

    public func getVHCHealthAttributeGroup(by healthAttributeType: PartyAttributeTypeRef) -> VHCHealthAttributeGroup? {
        let groups = self.allVHCHealthAttributeGroups()
        for group in groups {
            for healthAttribute in group.healthAttributes {
                if (healthAttribute.type.rawValue == healthAttributeType.rawValue) {
                    return group
                }
            }
        }
        return nil
    }
}
