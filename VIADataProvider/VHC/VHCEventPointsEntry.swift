import Foundation
import RealmSwift
import VIAUtilities

public class VHCEventPointsEntry: Object {
    public dynamic var typeKey: Int = 0
    public dynamic var typeName: String = ""
    public dynamic var typeCode: String = ""

    public dynamic var categoryKey: Int = 0
    public dynamic var categoryName: String = ""
    public dynamic var categoryCode: String = ""

    public dynamic var id: Int = 0
    public dynamic var preLimitValue: Int = 0
    public dynamic var potentialValue: Int = 0
    public dynamic var earnedValue: Int = 0

    public let reasons = List<VHCPointsReason>()
}

extension Realm {
    public func allVHCPointsEntries() -> Results<VHCEventPointsEntry> {
        return self.objects(VHCEventPointsEntry.self)
    }
}
