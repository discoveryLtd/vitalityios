//
//  VHCEventSource.swift
//  VitalityActive
//
//  Created by Lionel Camacho on 2017/03/16.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import RealmSwift
import  VIAUtilities

public class VHCEventSource: Object {
    public dynamic var eventSourceName: String = ""
    public dynamic var note: String = ""
    public dynamic var type: EventSourceRef = EventSourceRef(rawValue: EventSourceRef.Unknown.rawValue) ?? .Unknown
}

extension Realm {
    public func allVHCEventSources() -> Results<VHCEventSource> {
        return self.objects(VHCEventSource.self)
    }
}
