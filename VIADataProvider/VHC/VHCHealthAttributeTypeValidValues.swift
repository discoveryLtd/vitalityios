import Foundation
import RealmSwift
import VIAUtilities

public class VHCHealthAttributeValidValues: Object {
    public let lowerLimit = RealmOptional<Double>()
    public let maxValue = RealmOptional<Double>()
    public let minValue = RealmOptional<Double>()
    public let upperLimit = RealmOptional<Double>()
    public dynamic var typeTypeName: String  = ""
    public dynamic var unitOfMeasureType: UnitOfMeasureType = UnitOfMeasureType(rawValue: UnitOfMeasureType.Unknown.rawValue) ?? .Unknown
    public dynamic var type: Int = -1

    @objc public enum UnitOfMeasureType: Int, EnumCollection {
        case Unknown = -1
        case Gram = 100101
        case Kilogram = 100102
        case Meter = 100103
        case Kilometer = 100104
        case Inch = 100105
        case Foot = 100106
        case Mile = 100107
        case Ounce = 100108
        case Pound = 100109
        case Ton = 100110
        case Centimeter = 100111
        case MilimeterOfMercury = 100112
        case TotalCholestrolMillimolesPerLitre = 100113
        case Percentage = 100114
        case SystolicKiloPascal = 100115
        case Stone = 100116
        case FootInch = 100117
        case TotalCholestrolMilligramsPerDeciLitre = 100118
        case TriglycerideMilligramsPerDeciLitre = 100119
        case FastingGlucoseMilligramsPerDeciLitre = 100120
        case TriglycerideMillimolesPerLitre = 100121
        case FastingGlucoseMillimolesPerLitre = 100122
        case LDLCholestrolMilligramsPerDeciLitre = 100123
        case HDLCholestrolMilligramsPerDeciLitre = 100124
        case LDLCholestrolMillimolesPerLitre = 100125
        case HDLCholestrolMillimolesPerLitre = 100126
        case RandomGlucoseMillimolesPerLitre = 100127
        case RandomGlucoseMilligramsPerDeciLitre = 100128
        case SystolicMillimeterOfMercury = 100129
        case DiastolicKiloPascal = 100130
        case DiastolicMillimeterOfMercury = 100131

        public func unit() -> Unit {
            switch self {
            case .Gram:
                return  UnitMass.grams
            case .Kilogram:
                return UnitMass.kilograms
            case .Meter:
                return UnitLength.meters
            case .Kilometer:
                return UnitLength.kilometers
            case .Inch:
                return UnitLength.inches
            case .Foot:
                return UnitLength.feet
            case .Mile:
                return UnitLength.miles
            case .Ounce:
                return UnitMass.ounces
            case .Pound:
                return UnitMass.pounds
            case .Ton:
                return UnitMass.metricTons
            case .Centimeter:
                return UnitLength.centimeters
            case .MilimeterOfMercury,
                 .SystolicMillimeterOfMercury,
                 .DiastolicMillimeterOfMercury:
                return UnitPressure.millimetersOfMercury
            case .TotalCholestrolMillimolesPerLitre,
                 .TriglycerideMillimolesPerLitre,
                 .FastingGlucoseMillimolesPerLitre,
                 .LDLCholestrolMillimolesPerLitre,
                 .HDLCholestrolMillimolesPerLitre,
                 .RandomGlucoseMillimolesPerLitre:
                return UnitConcentrationMass.millimolesPerLiter(withGramsPerMole: 0.0)
            case .Percentage:
                return Unit(symbol: "%")
            case .SystolicKiloPascal,
                 .DiastolicKiloPascal:
                return UnitPressure.kilopascals
            case .Stone:
                return UnitMass.stones
            case .FootInch:
                return Unit(symbol: "ft in")
            case .TotalCholestrolMilligramsPerDeciLitre,
                 .TriglycerideMilligramsPerDeciLitre,
                 .FastingGlucoseMilligramsPerDeciLitre,
                 .LDLCholestrolMilligramsPerDeciLitre,
                 .HDLCholestrolMilligramsPerDeciLitre,
                 .RandomGlucoseMilligramsPerDeciLitre:
                return UnitConcentrationMass.milligramsPerDeciliter
            case .Unknown:
                return Unit()
            }
        }
    }
}

extension Realm {
    public func allVHCHealthAttributeValidValues() -> Results<VHCHealthAttributeValidValues> {
        return self.objects(VHCHealthAttributeValidValues.self)
    }
}
