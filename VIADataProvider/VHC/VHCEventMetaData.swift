//
//  VHCEventMetaData.swift
//  VitalityActive
//
//  Created by Lionel Camacho on 2017/03/15.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import RealmSwift

public class VHCEventMetaData: Object {
    public dynamic var code: String = ""
    public dynamic var key: Int = 0
    public dynamic var name: String = ""
    public dynamic var note: String = ""
    public dynamic var unitOfMeasure: String = ""
    public dynamic var value: String = ""
}

extension Realm {
    public func allVHCEventMetaData() -> Results<VHCEventMetaData> {
        return self.objects(VHCEventMetaData.self)
    }
}
