import Foundation
import RealmSwift
import VIAUtilities

public class VHCHealthAttribute: Object {
    public dynamic var typeName: String = ""
    public dynamic var type: PartyAttributeTypeRef = PartyAttributeTypeRef(rawValue: PartyAttributeTypeRef.Unknown.rawValue)!
    public let validValues = List<VHCHealthAttributeValidValues>()
    public let group = LinkingObjects(fromType: VHCHealthAttributeGroup.self, property: "healthAttributes")
    public let eventType = LinkingObjects(fromType: VHCEventType.self, property: "healthAttributes")

    override public static func primaryKey() -> String? {
        return "type"
    }

    public func allUnitsFromValidValues() -> [Unit] {
        return self.validValues.flatMap({ $0.unitOfMeasureType.unit() })
    }
}

extension VHCHealthAttribute {

    public func captureFootnote() -> String? {
        let contentProvider = CMSKeyValueContentProvider.for(.vhcResFiles)
        let footnote = contentProvider.value(for: "healthAttributeType\(self.type.rawValue)CaptureFootnote")
        if footnote.isEmpty {
            return nil
        }
        return footnote
    }

}

extension Realm {

    public func allVHCHealthAttributes() -> Results<VHCHealthAttribute> {
        return self.objects(VHCHealthAttribute.self)
    }

    public func vhcHealthAttribute(of type: PartyAttributeTypeRef) -> VHCHealthAttribute? {
        let attributes = self.allVHCHealthAttributes()
        let filtered = attributes.filter("type == %@", type.rawValue).first
        return filtered
    }

    public func vhcHealthAttributeTypeName(for attributeRawValue: Int) -> String? {
        if let attributeType = PartyAttributeTypeRef(rawValue: attributeRawValue) {
            if let vhcHealthAttribute: VHCHealthAttribute = self.vhcHealthAttribute(of: attributeType) {
                return vhcHealthAttribute.typeName
            }
        }
        return nil
    }
}
