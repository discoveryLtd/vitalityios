//
//  Metadata.swift
//  VitalityActive
//
//  Created by Lionel Camacho on 2017/01/31.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import RealmSwift

public class PointsEntryMetadata: Object {
    public dynamic var typeCode: String = ""
    public dynamic var TypeKey: Int = 0 //TODO: make enum
    public dynamic var unitOfMeasure: String = ""
    public dynamic var value: String = ""
    public let pointsEntry = LinkingObjects(fromType: PointsEntry.self, property: "metadatas")
}

extension Realm {
    public func allPointsEntryMetadatas() -> Results<PointsEntryMetadata> {
        return self.objects(PointsEntryMetadata.self)
    }
}
