//
//  categoryFilter.swift
//  VitalityActive
//
//  Created by Lionel Camacho on 2017/02/13.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import RealmSwift
import VIAUtilities

public class PointsMonitorCategory: Object {

    public dynamic var name: String = ""
    public dynamic var categoryId: PointsEntryCategoryRef = .Unknown
    public dynamic var index: Int = 0
    override public static func primaryKey() -> String? {
        return "index"
    }
}

extension Realm {
    public func allPointsMonitorCategories() -> Results<PointsMonitorCategory> {
        return self.objects(PointsMonitorCategory.self)
    }
}
