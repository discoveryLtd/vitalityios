import Foundation
import RealmSwift
import SwiftDate
import VIAUtilities

public class PointsEntry: Object, EffectiveDateFilterable {

    public dynamic var category: PointsMonitorCategory?
    public var categoryKey: PointsEntryCategoryRef = PointsEntryCategoryRef(rawValue: PointsEntryCategoryRef.Unknown.rawValue) ?? .Unknown
    public dynamic var categoryCode: String = ""
    public dynamic var categoryName: String = ""
    public dynamic var earnedValue: Int = 0
    public dynamic var effectiveDate: Date = Date.distantFuture
    public dynamic var eventId: Int = 0
    public dynamic var id: Int = 0
    public dynamic var party: Int = 0
    public dynamic var potentialValue: Int = 0
    public dynamic var prelimitValue: Int = 0
    public dynamic var typeKey: Int = 0 //TODO: populate with PointsEntryTypeRef
    public dynamic var typeCode: String = ""
    public dynamic var typeName: String = ""
    public let reasons = List<PointsEntryReason>()
    public let metadatas = List<PointsEntryMetadata>()
    public let activeRewardsProgressTrackerEvents = List<ActiveRewardsProgressTrackerEvent>()
    public let pointsAccount = LinkingObjects(fromType: PointsAccount.self, property: "pointsEntries")

    override public static func primaryKey() -> String? {
        return "id"
    }

    public func effectiveDateForFilter() -> Date {
        return self.effectiveDate
    }
}

public protocol EffectiveDateFilterable {
    func effectiveDateForFilter() -> Date
}

public extension Results where T: EffectiveDateFilterable {
    func filterForElementsBetweenStartAndEnd(month: Date) -> Results<T> {
        let start = (month - 1.month).endOf(component: .month)
        let end = (month + 1.month).startOf(component: .month)
        let filtered = self.filter("effectiveDate BETWEEN {%@, %@}", start, end)
        return filtered
    }

    func filterForElementsBetweenStartAndEnd(day: Date) -> Results<T> {
        let start = (day - 1.day).endOf(component: .day)
        let end = (day + 1.day).startOf(component: .day)
        let filtered = self.filter("effectiveDate BETWEEN {%@, %@}", start, end)
        return filtered
    }
}

extension Realm {
    public func allPointsEntries() -> Results<PointsEntry> {
        return self.objects(PointsEntry.self).sorted(byKeyPath: "effectiveDate", ascending: false)
    }

    public func pointsEntries(for monthAndYear: Date) -> Results<PointsEntry> {
        let filtered = self.allPointsEntries().filterForElementsBetweenStartAndEnd(month: monthAndYear)
        let sorted = filtered.sorted(byKeyPath: "effectiveDate", ascending: false)
        return sorted
    }

    public func pointsEntries(for categories: [String], during monthAndYear: Date) -> Results<PointsEntry> {

        let filtered = self.allPointsEntries().filterForElementsBetweenStartAndEnd(month: monthAndYear)
        let categoryFiltered = filtered.filter("categoryName IN %@", categories)
        return categoryFiltered
    }

}
