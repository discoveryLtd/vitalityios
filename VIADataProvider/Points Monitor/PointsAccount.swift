//
//  PointsAccount.swift
//  VitalityActive
//
//  Created by Lionel Camacho on 2017/01/31.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import RealmSwift
import SwiftDate

public class PointsAccount: Object {
    public dynamic var carryOverPoints: Int = 0
    public dynamic var effectiveFrom: Date?
    public dynamic var effectiveTo: Date?
    public dynamic var pointsTotal: Int = 0
    public let pointsEntries = List<PointsEntry>()

    public class func latest() -> PointsAccount? {
        let realm = DataProvider.newRealm()
        let account = realm.objects(PointsAccount.self).sorted(byKeyPath: "effectiveTo", ascending: false).first
        return account
    }

    public func filterPointsEntries(by category: String) -> Results<PointsEntry> {
        let filteredPointsEntries = pointsEntries.filter("category == '\(category)'")
        return filteredPointsEntries
    }

    public func sortPointsEntriesByEffectiveDate() -> Results<PointsEntry> {
        let sortedPointsEntries = pointsEntries.sorted(byKeyPath: "effectiveDate", ascending: false)
        return sortedPointsEntries
    }
}

extension Realm {
    public func allPointsAccounts() -> Results<PointsAccount> {
        return self.objects(PointsAccount.self)
    }
}
