import Foundation
import RealmSwift

public class PointsEntryReason: Object {
    public dynamic var categoryCode: String = ""
    public dynamic var categoryKey: Int = 0
    public dynamic var categoryName: String = ""

    public dynamic var key: Int = 0
    public dynamic var name: String = ""

    public let pointsEntry = LinkingObjects(fromType: PointsEntry.self, property: "reasons")
}

extension Realm {
    public func allReasons() -> Results<PointsEntryReason> {
        return self.objects(PointsEntryReason.self)
    }
}
