//
//  ProgressTrackerEvent.swift
//  VitalityActive
//
//  Created by Lionel Camacho on 2017/01/31.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import RealmSwift

public class ActiveRewardsProgressTrackerEvent: Object {
    public dynamic var goalTypeCode: String = ""
    public dynamic var goalTypeKey: Int = 0
    public dynamic var points: Int = 0
    public let pointsEntry = LinkingObjects(fromType: PointsEntry.self, property: "activeRewardsProgressTrackerEvents")
}

extension Realm {
    public func allActiveRewardsProgressTrackerEvents() -> Results<ActiveRewardsProgressTrackerEvent> {
        return self.objects(ActiveRewardsProgressTrackerEvent.self)
    }
}
