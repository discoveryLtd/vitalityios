//
//  VitalityMembershipCurrentPeriod.swift
//  VitalityActive
//
//  Created by Lionel Camacho on 2017/04/26.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import RealmSwift

public class VitalityMembershipCurrentPeriod: Object {
    public dynamic var effectiveTo: Date?
    public dynamic var effectiveFrom: Date?
}

extension Realm {
    public func getCurrentMembershipPeriodStartDate() -> Date? {
        return self.objects(VitalityMembershipCurrentPeriod.self).first?.effectiveFrom
    }

    public func getCurrentMembershipPeriodEndDate() -> Date? {
        return self.objects(VitalityMembershipCurrentPeriod.self).first?.effectiveTo
    }

}
