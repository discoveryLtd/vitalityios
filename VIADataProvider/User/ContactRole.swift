//
//  ContactRole.swift
//  VitalityActive
//
//  Created by Wilmar van Heerden on 2016/12/05.
//  Copyright © 2016 Glucode. All rights reserved.
//

import Foundation
import RealmSwift

public class ContactRole: Object {
    public dynamic var availabilityFrom: Date?
    public dynamic var availabilityTo: Date?
    public dynamic var rolePurposeType: String = ""
    public dynamic var roleType: String = ""
}
