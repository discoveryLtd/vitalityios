//
//  User.swift
//  VitalityActive
//
//  Created by Wilmar van Heerden on 2016/11/17.
//  Copyright © 2016 Glucode. All rights reserved.
//

import Foundation
import RealmSwift

public class User: Object {
//    public static let demoPartyId: Int = 100001
//    public static let vitalityMembershipId: Int = 100000

    //public dynamic var partyId: Int = 0
    public dynamic var username: String = ""
//    public dynamic var middleNames: String = ""
//    public dynamic var gender: String = ""
//    public dynamic var dateOfBirth: Date?
//    public dynamic var givenName: String = ""
//    public dynamic var familyName: String = ""
//    public dynamic var language: String = ""
//    public dynamic var preferredName: String = ""
//    public dynamic var title: String = ""
//    public dynamic var suffix: String = ""

    override public static func primaryKey() -> String? {
        return "username"
    }
}

extension Realm {
    public func getUsername() -> String? { //TODO: remove optional
        return self.objects(User.self).first?.username // TODO: wrong place for a default
    }
}
