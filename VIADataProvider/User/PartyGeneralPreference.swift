//
//  GeneralPreference.swift
//  VitalityActive
//
//  Created by Wilmar van Heerden on 2016/12/05.
//  Copyright © 2016 Glucode. All rights reserved.
//

import Foundation
import RealmSwift

public class PartyGeneralPreference: Object {
    public dynamic var effectiveFrom: Date?
    public dynamic var effectiveTo: Date?
    public dynamic var type: Int = 0
    public dynamic var value: String = ""
}
