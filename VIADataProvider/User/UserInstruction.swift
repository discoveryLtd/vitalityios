//
//  UserInstruction.swift
//  VitalityActive
//
//  Created by Wilmar van Heerden on 2017/02/16.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import RealmSwift

public class UserInstruction: Object {
    public static let defaultUserInstructionValue = -999

    public dynamic var typeKey: Int = UserInstruction.defaultUserInstructionValue //TODO: make enum
    public dynamic var id: Int = UserInstruction.defaultUserInstructionValue
}

extension Realm {

    public func userInstructionForLoginTermsAndConditions() -> UserInstruction? {
        let result = self.objects(UserInstruction.self).filter("typeKey == \(1)").first //TODO: change 1 to enum name instead of raw value (whenever enum has been defined by simon)
        return result
    }

    public func deleteUserInstructionForLoginTermsAndConditions() {
        if let result = self.objects(UserInstruction.self).filter("typeKey == \(1)").first { //TODO: change 1 to enum name instead of raw value (whenever enum has been defined by simon)
            try! self.write {
                self.delete(result)
                debugPrint("Deleted user instruction for LoginTermsAndConditions")
            }
        } else {
            debugPrint("No user instruction for terms and conditions to delete")
        }
    }

}
