//
//  EmailAddress.swift
//  VitalityActive
//
//  Created by Wilmar van Heerden on 2016/12/05.
//  Copyright © 2016 Glucode. All rights reserved.
//

import Foundation
import RealmSwift

public class PartyEmailAddress: Object {
    public dynamic var value: String = ""
    public let contactRoles = List<PartyContactRole>()
}
