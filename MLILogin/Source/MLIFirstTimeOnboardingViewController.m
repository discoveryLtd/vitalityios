//
//  MLIFirstTimeOnboardingViewController.m
//  MLILogin
//
//  Created by Michelle R. Oratil on 15/01/2019.
//  Copyright © 2019 Glucode. All rights reserved.
//

#import "MLIFirstTimeOnboardingViewController.h"
#import <ChameleonFramework/Chameleon.h>
#import <VIAUIKit/VIAUIKit-Swift.h>
#import <VitalityKit/VitalityKit-Swift.h>
#import <MLILogin/MLILogin-Swift.h>
#import "POP.h"

@interface MLIFirstTimeOnboardingViewController ()
@property (nonatomic, strong) UIView *circle0;
@property (nonatomic, strong) UIView *circle1;
@property (nonatomic, strong) UIView *circle2;
@property (nonatomic, strong) UIView *circle3;
@property (nonatomic, strong) UIImageView *imageView0;
@property (nonatomic, strong) UIImageView *imageView1;
@property (nonatomic, strong) UIImageView *imageView2;
@property (nonatomic, strong) UIStackView *stackView0;
@property (nonatomic, assign) BOOL hasAnimatedPage0;
@property (nonatomic, assign) BOOL hasAnimatedPage1;
@property (nonatomic, assign) BOOL hasAnimatedPage2;
@property (nonatomic, strong) NSMutableArray *circleViews;
@property (nonatomic, strong) UIButton *skipButton;
@property (nonatomic, strong) UIPageControl *pageControl;
@property (nonatomic, strong) MLILoginViewController *loginViewController;
@end

@implementation MLIFirstTimeOnboardingViewController

#pragma mark - Init

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    _loginViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"MLILoginViewController"];
    _loginViewController.parentController = self;
    BOOL hasLoggedInBefore = [AppSettings hasLoggedInAtLeastOnce];
    self.displayLoginImmediately = hasLoggedInBefore;
//    _loginViewController.segueToHomeCompletion = self.loginViewController.segueToHomeCompletion;
}

#pragma mark - View lifecycle

- (UIStatusBarStyle)preferredStatusBarStyle
{
    if ([self hidePreOnBoarding]) {
        return UIStatusBarStyleDefault;
    }
    
    return self.pageOffset >= 3 ? UIStatusBarStyleDefault : UIStatusBarStyleLightContent;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.circleViews = [NSMutableArray new];
    self.contentView.backgroundColor = [UIColor whiteColor];
    self.scrollView.backgroundColor = [UIColor whiteColor];
    self.scrollView.pagingEnabled = YES;
    self.scrollView.bounces = NO;
    
    if (self.displayLoginImmediately) {
        [self skipToLogin];
    }
    
    [self configureOnboardingViews];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self applyRoundingToCircleViews];
    
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self applyRoundingToCircleViews];
    [self animateForCurrentPage];
}

- (void)applyRoundingToCircleViews
{
    for (UIView *circle in self.circleViews) {
        circle.layer.cornerRadius = circle.frame.size.width / 2;
    }
}

- (void)dealloc
{
    
}

#pragma mark - Getters

- (UIButton *)skipButton
{
    if (_skipButton) {
        return _skipButton;
    }
    _skipButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _skipButton.translatesAutoresizingMaskIntoConstraints = NO;
    _skipButton.titleLabel.textAlignment = NSTextAlignmentRight;
    _skipButton.titleLabel.font = [UIFont onboardingButton];
    _skipButton.alpha = 0.0f;
    [_skipButton setTitle:[MLIFirstTimeOnboardingViewController skipButtonTitle] forState:UIControlStateNormal];
    [_skipButton setTitleColor:[UIColor onboardingText] forState:UIControlStateNormal];
    [_skipButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateSelected];
    [_skipButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
    [_skipButton addTarget:self action:@selector(skip:) forControlEvents:UIControlEventTouchUpInside];
    return _skipButton;
}

- (UIPageControl *)pageControl
{
    if (_pageControl) {
        return _pageControl;
    }
    _pageControl = [[UIPageControl alloc] initWithFrame:CGRectZero];
    _pageControl.translatesAutoresizingMaskIntoConstraints = NO;
    _pageControl.numberOfPages = 4;
    _pageControl.pageIndicatorTintColor = [UIColor colorWithWhite:1.0f alpha:0.2f];
    _pageControl.currentPageIndicatorTintColor = [UIColor onboardingText];
    _pageControl.alpha = 0.0f;
    return _pageControl;
}

+ (CGFloat)statusBarHeight
{
    CGSize statusBarSize = [[UIApplication sharedApplication] statusBarFrame].size;
    CGFloat statusBarHeight = MIN(statusBarSize.width, statusBarSize.height);
    return statusBarHeight;
}

#pragma mark - Actions

- (void)skip:(UIButton *)sender
{
    [self scrollToLogin:YES];
}

- (void)scrollToLogin:(BOOL)animated
{
    CGRect rect = CGRectMake(self.contentView.frame.size.width - 1, 0, self.contentView.frame.size.width, self.contentView.frame.size.height);
    [self.scrollView scrollRectToVisible:rect animated:animated];
}

- (void)skipToLogin
{
    self.pageOffset = self.numberOfPages - 1;
    [self scrollToLogin:NO];
}

#pragma mark - Configure onboarding views

- (void)configureOnboardingViews
{
    UIView *contentView = self.contentView;
    
    [self addBackgroundViewsToContentView:contentView];
    [self addCircleViewsToContentView:contentView];
    
    self.stackView0 = [self addLabelsWithHeadingText:[MLIFirstTimeOnboardingViewController heading1] subtitleText:[MLIFirstTimeOnboardingViewController description1] forPage:0 toContentView:contentView];
    self.stackView0.alpha = 0.0f;
    [self addLabelsWithHeadingText:[MLIFirstTimeOnboardingViewController heading2] subtitleText:[MLIFirstTimeOnboardingViewController description2] forPage:1 toContentView:contentView];
    [self addLabelsWithHeadingText:[MLIFirstTimeOnboardingViewController heading3] subtitleText:[MLIFirstTimeOnboardingViewController description3] forPage:2 toContentView:contentView];
    [self addImageViewsToContentView:contentView];
    [self addLoginViewToContentView:contentView];
    if ([self hidePreOnBoarding]) {
        // Do not show Pre Onboarding Screen on first time login.
    } else {
        [self addSkipButtonToContentView:self.view];
        [self addPageControlToContentView:self.view];
    }
    [self animateCurrentFrame];
}

- (void)addLoginViewToContentView:(UIView *)contentView
{
    [self addChildViewController:self.loginViewController];
    
    UIView *loginView = self.loginViewController.view;
    [contentView addSubview:loginView];
    
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    [contentView addConstraint:[NSLayoutConstraint constraintWithItem:loginView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:contentView attribute:NSLayoutAttributeHeight multiplier:1.0f constant:0.0f]];
    [contentView addConstraint:[NSLayoutConstraint constraintWithItem:loginView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0f constant:screenWidth]];
    [contentView addConstraint:[NSLayoutConstraint constraintWithItem:loginView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:contentView attribute:NSLayoutAttributeTop multiplier:1.0f constant:0.0f]];
    [self keepView:loginView onPage:self.numberOfPages-1];
    [self.loginViewController didMoveToParentViewController:self];
}

- (void)addPageControlToContentView:(UIView *)contentView
{
    [contentView addSubview:self.pageControl];
    [contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.pageControl attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:contentView attribute:NSLayoutAttributeCenterX multiplier:1.0f constant:0.0f]];
    [contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.pageControl attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:contentView attribute:NSLayoutAttributeBottomMargin multiplier:1.0f constant:0.0f]];
}

- (void)addSkipButtonToContentView:(UIView *)contentView
{
    [contentView addSubview:self.skipButton];
    
    CGFloat statusBarHeight = [MLIFirstTimeOnboardingViewController statusBarHeight];
    
    [contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.skipButton attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:contentView attribute:NSLayoutAttributeTopMargin multiplier:1.0f constant:statusBarHeight]];
    [contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.skipButton attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:contentView attribute:NSLayoutAttributeRightMargin multiplier:1.0f constant:0.0f]];
    [contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.skipButton attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationGreaterThanOrEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0f constant:44.0f]];
}

#pragma mark - Gradient colors

+ (UIColor *)greenGradientForFrame:(CGRect)frame applyAODAColor:(BOOL)green
{
    return [UIColor onboardingGreenGradientWithFrame:frame applyAODAColor:green];
}

+ (UIColor *)blueGradientForFrame:(CGRect)frame applyAODAColor:(BOOL)blue
{
    return [UIColor onboardingBlueGradientWithFrame:frame applyAODAColor:blue];
}

+ (UIColor *)pinkGradientForFrame:(CGRect)frame
{
    return [UIColor onboardingPinkGradientWithFrame:frame];
}

#pragma mark - IFTTTAnimatedPagingScrollViewController specific

- (NSUInteger)numberOfPages
{
    if ([self hidePreOnBoarding]){
        return 1;
    }
    
    return 4;
}

#pragma mark - BackgroundViews

- (void)addBackgroundViewsToContentView:(UIView *)contentView
{
    CGRect frame = self.view.frame;
    
    NSArray *pages = @[@0, @1, @2, @3];
    
    UIView *pink = [MLIFirstTimeOnboardingViewController newBackgroundViewWithBackgroundColor:[MLIFirstTimeOnboardingViewController pinkGradientForFrame:frame]];
    [contentView addSubview:pink];
    [MLIFirstTimeOnboardingViewController alignBackgroundView:pink toContentView:contentView];
    [self keepView:pink onPages:pages];
    
    UIView *blue = [MLIFirstTimeOnboardingViewController newBackgroundViewWithBackgroundColor:[MLIFirstTimeOnboardingViewController blueGradientForFrame:frame applyAODAColor:false]];
    [contentView addSubview:blue];
    [MLIFirstTimeOnboardingViewController alignBackgroundView:blue toContentView:contentView];
    [self keepView:blue onPages:pages];
    
    UIView *green = [MLIFirstTimeOnboardingViewController newBackgroundViewWithBackgroundColor:[MLIFirstTimeOnboardingViewController greenGradientForFrame:frame applyAODAColor:false]];
    [contentView addSubview:green];
    [MLIFirstTimeOnboardingViewController alignBackgroundView:green toContentView:contentView];
    [self keepView:green onPages:pages];
    
    IFTTTAlphaAnimation *greenFade = [[IFTTTAlphaAnimation alloc] initWithView:green];
    [greenFade addKeyframeForTime:0.0f alpha:1.0f];
    [greenFade addKeyframeForTime:0.3f alpha:0.6f];
    [greenFade addKeyframeForTime:0.5f alpha:0.5f];
    [greenFade addKeyframeForTime:0.6f alpha:0.3f];
    [greenFade addKeyframeForTime:1.0f alpha:0.0f];
    [self.animator addAnimation:greenFade];
    
    IFTTTAlphaAnimation *blueFade = [[IFTTTAlphaAnimation alloc] initWithView:blue];
    [blueFade addKeyframeForTime:1.0f alpha:1.0f];
    [blueFade addKeyframeForTime:1.3f alpha:0.6f];
    [blueFade addKeyframeForTime:1.5f alpha:0.5f];
    [blueFade addKeyframeForTime:1.6f alpha:0.3f];
    [blueFade addKeyframeForTime:2.0f alpha:0.0f];
    [self.animator addAnimation:blueFade];
}

+ (UIView *)newBackgroundViewWithBackgroundColor:(UIColor *)backgroundColor
{
    UIView *view = [UIView new];
    view.translatesAutoresizingMaskIntoConstraints = NO;
    view.backgroundColor = backgroundColor;
    return view;
}

+ (void)alignBackgroundView:(UIView *)backgroundView toContentView:(UIView *)contentView
{
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    
    [contentView addConstraint:[NSLayoutConstraint constraintWithItem:backgroundView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:contentView attribute:NSLayoutAttributeHeight multiplier:1.0f constant:0.0f]];
    [contentView addConstraint:[NSLayoutConstraint constraintWithItem:backgroundView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0f constant:screenWidth]];
    [contentView addConstraint:[NSLayoutConstraint constraintWithItem:backgroundView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:contentView attribute:NSLayoutAttributeTop multiplier:1.0f constant:0.0f]];
}

#pragma mark - Circle views

- (void)addCircleViewsToContentView:(UIView *)contentView
{
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    CGFloat statusBarHeight = [MLIFirstTimeOnboardingViewController statusBarHeight];
    UIColor *white10 = [UIColor colorWithWhite:1.0f alpha:0.1f];
    UIColor *white20 = [UIColor colorWithWhite:1.0f alpha:0.2f];
    
    NSArray *pages = @[@0, @1, @2];
    
    UIView *circle1 = [MLIFirstTimeOnboardingViewController newCircleViewWithBackgroundColor:white10];
    self.circle0 = circle1;
    [contentView addSubview:circle1];
    [contentView addConstraint:[NSLayoutConstraint constraintWithItem:circle1 attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0f constant:screenWidth]];
    [contentView addConstraint:[NSLayoutConstraint constraintWithItem:circle1 attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:circle1 attribute:NSLayoutAttributeHeight multiplier:1.0f constant:0.0f]];
    [contentView addConstraint:[NSLayoutConstraint constraintWithItem:circle1 attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:contentView attribute:NSLayoutAttributeTop multiplier:1.0f constant:statusBarHeight]];
    [self keepView:circle1 onPages:pages];
    
    UIView *circle2 = [MLIFirstTimeOnboardingViewController newCircleViewWithBackgroundColor:white10];
    self.circle1 = circle2;
    [contentView addSubview:circle2];
    [MLIFirstTimeOnboardingViewController alignCircle:circle2 toOtherView:circle1 withScale:0.73f inContentView:contentView];
    [self keepView:circle2 onPages:pages];
    
    UIView *circle3 = [MLIFirstTimeOnboardingViewController newCircleViewWithBackgroundColor:white20];
    self.circle2 = circle3;
    [contentView addSubview:circle3];
    [MLIFirstTimeOnboardingViewController alignCircle:circle3 toOtherView:circle1 withScale:0.45f inContentView:contentView];
    [self keepView:circle3 onPages:pages];
    
    UIView *circle4 = [MLIFirstTimeOnboardingViewController newCircleViewWithBackgroundColor:white20];
    self.circle3 = circle4;
    [contentView addSubview:circle4];
    [MLIFirstTimeOnboardingViewController alignCircle:circle4 toOtherView:circle1 withScale:0.25f inContentView:contentView];
    [self keepView:circle4 onPages:pages];
    
    [self.circleViews addObject:circle1];
    [self.circleViews addObject:circle2];
    [self.circleViews addObject:circle3];
    [self.circleViews addObject:circle4];
}

+ (UIView *)newCircleViewWithBackgroundColor:(UIColor *)backgroundColor
{
    UIView *circle = [UIView new];
    circle.translatesAutoresizingMaskIntoConstraints = NO;
    circle.backgroundColor = backgroundColor;
    return circle;
}

+ (void)alignCircle:(UIView *)circle toOtherView:(UIView *)otherView withScale:(CGFloat)scale inContentView:(UIView *)contentView
{
    [contentView addConstraint:[NSLayoutConstraint constraintWithItem:circle attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:otherView attribute:NSLayoutAttributeCenterY multiplier:1.0f constant:0.0f]];
    [contentView addConstraint:[NSLayoutConstraint constraintWithItem:circle attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:otherView attribute:NSLayoutAttributeWidth multiplier:scale constant:0.0f]];
    [contentView addConstraint:[NSLayoutConstraint constraintWithItem:circle attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:otherView attribute:NSLayoutAttributeHeight multiplier:scale constant:0.0f]];
}

#pragma mark - Labels

- (UIStackView *)addLabelsWithHeadingText:(NSString *)headingText subtitleText:(NSString *)subtitleText forPage:(CGFloat)page toContentView:(UIView *)contentView
{
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    
    UILabel *headingLabel = [MLIFirstTimeOnboardingViewController newLabelWithText:headingText];
    headingLabel.font = [UIFont onboardingHeading];
    UILabel *subtitleLabel = [MLIFirstTimeOnboardingViewController newLabelWithText:subtitleText];
    subtitleLabel.font = [UIFont onboardingSubtitle];
    
    UIStackView *stackView = [[UIStackView alloc] initWithArrangedSubviews:@[headingLabel, subtitleLabel]];
    stackView.axis = UILayoutConstraintAxisVertical;
    stackView.alignment = UIStackViewAlignmentCenter;
    stackView.spacing = 20.0f;
    [contentView addSubview:stackView];
    [self keepView:stackView onPage:page];
    
    [contentView addConstraint:[NSLayoutConstraint constraintWithItem:stackView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.circle1 attribute:NSLayoutAttributeBottom multiplier:1.0f constant:0.0f]];
    [headingLabel addConstraint:[NSLayoutConstraint constraintWithItem:headingLabel attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationLessThanOrEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0f constant:screenWidth * 0.666f]];
    [subtitleLabel addConstraint:[NSLayoutConstraint constraintWithItem:subtitleLabel attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationLessThanOrEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0f constant:screenWidth * 0.8f]];
    
    // scale animations
    IFTTTScaleAnimation *headingScaleAnimation = [[IFTTTScaleAnimation alloc] initWithView:headingLabel];
    [headingScaleAnimation addKeyframeForTime:(page - 0.5f) scale:0.5f];
    [headingScaleAnimation addKeyframeForTime:page scale:1.0f];
    [self.animator addAnimation:headingScaleAnimation];
    IFTTTScaleAnimation *subtitleScaleAnimation = [[IFTTTScaleAnimation alloc] initWithView:subtitleLabel];
    [subtitleScaleAnimation addKeyframeForTime:(page - 0.5f) scale:0.5f];
    [subtitleScaleAnimation addKeyframeForTime:page scale:1.0f];
    [self.animator addAnimation:subtitleScaleAnimation];
    // fade animations
    IFTTTAlphaAnimation *headingFadeAnimation = [[IFTTTAlphaAnimation alloc] initWithView:headingLabel];
    [headingFadeAnimation addKeyframeForTime:(page - 0.5f) alpha:0.0f];
    [headingFadeAnimation addKeyframeForTime:page alpha:1.0f];
    [headingFadeAnimation addKeyframeForTime:(page + 0.5f) alpha:0.0f];
    [self.animator addAnimation:headingFadeAnimation];
    IFTTTAlphaAnimation *subtitleFadeAnimation = [[IFTTTAlphaAnimation alloc] initWithView:subtitleLabel];
    [subtitleFadeAnimation addKeyframeForTime:(page - 0.5f) alpha:0.0f];
    [subtitleFadeAnimation addKeyframeForTime:page alpha:1.0f];
    [subtitleFadeAnimation addKeyframeForTime:(page + 0.5f) alpha:0.0f];
    [self.animator addAnimation:subtitleFadeAnimation];
    
    return stackView;
}

+ (UILabel *)newLabelWithText:(NSString *)text
{
    UILabel *label = [UILabel new];
    label.translatesAutoresizingMaskIntoConstraints = NO;
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor onboardingText];
    label.numberOfLines = 0;
    label.text = text;
    return label;
}

#pragma mark - Images

- (void)addImageViewsToContentView:(UIView *)contentView
{
    UIView *otherView = self.circle0;
    
    UIImageView *imageView0 = [MLIFirstTimeOnboardingViewController newImageViewWithNamed:@"firstTimeOnboarding01"];
    imageView0.alpha = 0.0f;
    [contentView addSubview:imageView0];
    [MLIFirstTimeOnboardingViewController alignImageView:imageView0 toOtherView:otherView inContentView:contentView];
    [self keepView:imageView0 onPage:0];
    self.imageView0 = imageView0;
    
    UIImageView *imageView1 = [MLIFirstTimeOnboardingViewController newImageViewWithNamed:@"firstTimeOnboarding02"];
    imageView1.alpha = 0.0f;
    [contentView addSubview:imageView1];
    [MLIFirstTimeOnboardingViewController alignImageView:imageView1 toOtherView:otherView inContentView:contentView];
    [self keepView:imageView1 onPage:1];
    self.imageView1 = imageView1;
    
    UIImageView *imageView2 = [MLIFirstTimeOnboardingViewController newImageViewWithNamed:@"firstTimeOnboarding03"];
    imageView2.alpha = 0.0f;
    [contentView addSubview:imageView2];
    [MLIFirstTimeOnboardingViewController alignImageView:imageView2 toOtherView:otherView inContentView:contentView];
    [self keepView:imageView2 onPage:2];
    self.imageView2 = imageView2;
}

- (void)addScaleAndFadeAnimationsToView:(UIView *)view onPage:(CGFloat)page
{
    // scale animation
    IFTTTScaleAnimation *scaleAnimation = [[IFTTTScaleAnimation alloc] initWithView:view];
    [scaleAnimation addKeyframeForTime:(page - 0.5f) scale:0.5f];
    [scaleAnimation addKeyframeForTime:page scale:1.0f];
    [self.animator addAnimation:scaleAnimation];
    // fade animation
    IFTTTAlphaAnimation *fadeAnimation = [[IFTTTAlphaAnimation alloc] initWithView:view];
    [fadeAnimation addKeyframeForTime:(page - 0.5f) alpha:0.0f];
    [fadeAnimation addKeyframeForTime:page alpha:1.0f];
    [fadeAnimation addKeyframeForTime:(page + 0.5f) alpha:0.0f];
    [self.animator addAnimation:fadeAnimation];
}

+ (UIImageView *)newImageViewWithNamed:(NSString *)imageName
{
    NSBundle *currentBundle = [NSBundle bundleForClass:[MLIFirstTimeOnboardingViewController class]];
    UIImage *image = [UIImage imageNamed:imageName inBundle:currentBundle compatibleWithTraitCollection:nil];
    UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
    imageView.translatesAutoresizingMaskIntoConstraints = NO;
    imageView.contentMode = UIViewContentModeCenter;
    return imageView;
}

+ (void)alignImageView:(UIImageView *)imageView toOtherView:(UIView *)otherView inContentView:(UIView *)contentView
{
    [contentView addConstraint:[NSLayoutConstraint constraintWithItem:imageView attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:otherView attribute:NSLayoutAttributeCenterY multiplier:1.0f constant:0.0f]];
}

#pragma mark - UIScrollView

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [super scrollViewDidScroll:scrollView];
    
    [self.view endEditing:YES];
    [self animateForCurrentPage];
    [self updatePageControlForCurrentPage];
    [self updateControlsVisibilityBasedOnCurrentVisiblePage];
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView
{
    [self setNeedsStatusBarAppearanceUpdate];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    [self setNeedsStatusBarAppearanceUpdate];
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    [self setNeedsStatusBarAppearanceUpdate];
}

#pragma mark - Animations

+ (void)addBounceAnimationToView:(UIView *)view delay:(NSTimeInterval)delay
{
    POPSpringAnimation *animation = [POPSpringAnimation animationWithPropertyNamed:kPOPLayerScaleXY];
    animation.velocity = [NSValue valueWithCGSize:CGSizeMake(3.0f, 3.0f)];
    animation.toValue = [NSValue valueWithCGSize:CGSizeMake(1.0f, 1.0f)];
    animation.dynamicsMass = 5.0f;
    animation.dynamicsFriction = 50.0f;
    animation.beginTime = CACurrentMediaTime() + delay;
    [view.layer pop_addAnimation:animation forKey:@"layerScaleSpringAnimation"];
}

+ (void)addFadeInAnimationToView:(UIView *)view delay:(NSTimeInterval)delay
{
    POPBasicAnimation *animation = [POPBasicAnimation animationWithPropertyNamed:kPOPViewAlpha];
    animation.toValue = @1;
    animation.beginTime = CACurrentMediaTime() + delay;
    [view pop_addAnimation:animation forKey:@"layerFadeAnimation"];
}

- (void)animateForCurrentPage
{
    void (^pulsingCirclesAnimationBlock)(CGFloat) = ^(CGFloat delay) {
        [MLIFirstTimeOnboardingViewController addBounceAnimationToView:self.circle0 delay:delay + 0.1f];
        [MLIFirstTimeOnboardingViewController addBounceAnimationToView:self.circle1 delay:delay + 0.2f];
        [MLIFirstTimeOnboardingViewController addBounceAnimationToView:self.circle2 delay:delay + 0.3f];
        [MLIFirstTimeOnboardingViewController addBounceAnimationToView:self.circle3 delay:delay + 0.4f];
    };
    
    CGFloat delay = 0.5f;
    CGFloat currentPage = roundf(self.pageOffset);
    if (currentPage == 0 && self.pageOffset == 0 && !self.hasAnimatedPage0) {
        CGFloat startupDelay = 0.75f;
        pulsingCirclesAnimationBlock(startupDelay);
        [MLIFirstTimeOnboardingViewController addFadeInAnimationToView:self.imageView0 delay:startupDelay + delay];
        [MLIFirstTimeOnboardingViewController addBounceAnimationToView:self.imageView0 delay:startupDelay + delay];
        [self addScaleAndFadeAnimationsToView:self.imageView0 onPage:currentPage];
        
        [MLIFirstTimeOnboardingViewController addFadeInAnimationToView:self.stackView0 delay:startupDelay + delay];
        [self addScaleAndFadeAnimationsToView:self.stackView0 onPage:currentPage];
        
        [MLIFirstTimeOnboardingViewController addFadeInAnimationToView:self.skipButton delay:startupDelay + delay];
        [MLIFirstTimeOnboardingViewController addFadeInAnimationToView:self.pageControl delay:startupDelay + delay];
        
        self.hasAnimatedPage0 = YES;
    }
    else if (currentPage == 1 && self.pageOffset == 1 && !self.hasAnimatedPage1) {
        pulsingCirclesAnimationBlock(0.0f);
        [MLIFirstTimeOnboardingViewController addFadeInAnimationToView:self.imageView1 delay:delay];
        [MLIFirstTimeOnboardingViewController addBounceAnimationToView:self.imageView1 delay:delay];
        [self addScaleAndFadeAnimationsToView:self.imageView1 onPage:currentPage];
        self.hasAnimatedPage1 = YES;
    }
    else if (currentPage == 2 && self.pageOffset == 2 && !self.hasAnimatedPage2) {
        pulsingCirclesAnimationBlock(0.0f);
        [MLIFirstTimeOnboardingViewController addFadeInAnimationToView:self.imageView2 delay:delay];
        [MLIFirstTimeOnboardingViewController addBounceAnimationToView:self.imageView2 delay:delay];
        [self addScaleAndFadeAnimationsToView:self.imageView2 onPage:currentPage];
        self.hasAnimatedPage2 = YES;
    }
}

#pragma mark - Controls

- (void)updateControlsVisibilityBasedOnCurrentVisiblePage
{
    if (self.pageOffset < 2.5) {
        self.skipButton.alpha = 1.0f;
    }
    else {
        self.skipButton.alpha = 0.0f;
        self.pageControl.alpha = 1.0f;
    }
}

- (void)updatePageControlForCurrentPage
{
    self.pageControl.currentPage = roundf(self.pageOffset);
    
    if (self.pageControl.currentPage > 2) {
        self.pageControl.pageIndicatorTintColor = [UIColor colorWithWhite:0.0f alpha:0.2f];
        self.pageControl.currentPageIndicatorTintColor = [UIColor primaryColor];
    }
    else {
        self.pageControl.pageIndicatorTintColor = [UIColor colorWithWhite:1.0f alpha:0.2f];
        self.pageControl.currentPageIndicatorTintColor = [UIColor onboardingText];
    }
}

#pragma mark - Others

- (BOOL)hidePreOnBoarding
{
    return [[[NSBundle mainBundle] objectForInfoDictionaryKey: @"VIAHideOnBoardingScreen"] boolValue];
}

@end
