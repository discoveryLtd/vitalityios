//
//  MLISplashScreenViewModel.swift
//  MLILogin
//
//  Created by Michelle R. Oratil on 15/01/2019.
//  Copyright © 2019 Glucode. All rights reserved.
//

import Foundation
import VitalityKit
import UIKit
import RealmSwift
import VIACommon
import VIAHealthKit
import HealthKit
import VIAWellnessDevices

protocol SplashScreenViewModel : class {
    var logo: UIImage? { get }
    func loadLogo(completion: @escaping ((_: UIImage?) -> Void))
}

protocol SplashScreenViewModelDelegate: class {
    func willFetchAppConfigAndUpdateContent()
    func didDownloadLogo(image: UIImage?)
    func navigateToNextStep()
    func serviceErrorOccured(with error: Error)
}

class MLISplashScreenViewModel: SplashScreenViewModel, CMSConsumer, VIAAppleHealthHandler {
    
    weak var delegate: SplashScreenViewModelDelegate?
    
    var logo: UIImage?
    
    init(delegate: SplashScreenViewModelDelegate?, skipAppConfigRequest: Bool) {
        self.delegate = delegate
        
        if !skipAppConfigRequest {
            fetchAppConfigIfNeeded()
        } else {
            updateContent()
        }
        
        // for Apple Health device auto-sync
        configureAutoSync()
    }
    
    func fetchAppConfigIfNeeded() {
        self.delegate?.willFetchAppConfigAndUpdateContent()
        let realm: Realm = DataProvider.newRealm()
        
        Wire.Member.getApplicationConfiguration(tenantId: realm.getTenantId()) { [weak self] error in
            
            if error == .securityException {
                // don't do anything special, the app delegate
                // handles securityExceptions from notification
                return
            } else if let error = error {
                self?.delegate?.serviceErrorOccured(with: error)
            } else {
                /*
                 * Check app update in a separate thread.
                 */
                self?.checkUpdateForNewAppVersion()
                /*
                 * Proceed to login
                 */
                self?.updateContent()
            }
        }
    }
    
    func checkUpdateForNewAppVersion(){
        let realm: Realm = DataProvider.newRealm()
        Wire.Update.getAppUpdate(tenantId: realm.getTenantId()) {
            (error, upgradeUrl) in
            
            /*
             * If Appstore URL is valid and not empty. Notify user to download the latest app.
             */
            if let url = upgradeUrl{
                NotificationCenter.default.post(name: .VIAAppUpdateException, object: url)
            }
        }
    }
    
    func updateContent() {
        DispatchQueue.global(qos: .userInitiated).async {
            let group = DispatchGroup()
            
            let completion = {
                group.leave()
            }
            
            group.enter()
            self.loadLogoIfNeeded(completion: completion)
            group.enter()
            self.downloadResourceFiles(completion: completion)
            _ = group.wait(timeout: .distantFuture)
            //group.enter()
            DispatchQueue.main.async {
                //completion()
                self.delegate?.navigateToNextStep()
            }
        }
    }
    
    func loadLogoIfNeeded(completion: @escaping () -> Void) {
        
        var delay = 0.0
        
        if let enableDelay = VIAApplicableFeatures.default.enableDelayInSplashScreenDisplay, enableDelay {
            delay = 1.0
        }
        
        self.loadLogo(completion: { image in
            DispatchQueue.main.async {
                self.delegate?.didDownloadLogo(image: image)
                
                DispatchQueue.main.asyncAfter(deadline: .now() + delay) {
                    completion()
                }
                
            }
        })
    }
    //
    func loadLogo(completion: @escaping ((_: UIImage?) -> Void)) {
        self.logo = VIAIconography.default.splashLogo()
        completion(self.logo)
    }
    //
    func downloadResourceFiles(completion: @escaping () -> Void) {
        //TODO: what is the condition under which we download or don't download
        // resource files. do we check the version in the file? or do we assume
        // that if it exists we don't download it? do we delete all files upon
        // parsing a new version of app config?
        let downloader = ResourceFileDownloader()
        downloader.downloadResourceFilesWithRetries {
            // TODO: what action to take if error.Count != 0?
            completion()
        }
    }
}
