//
//  MLILoginViewModel.swift
//  MLILogin
//
//  Created by Michelle R. Oratil on 15/01/2019.
//  Copyright © 2019 Glucode. All rights reserved.
//

import Foundation
import VitalityKit
import VitalityKit
import SwiftKeychainWrapper
import RNCryptor


protocol LoginViewModel {
    var loginComplete: ((_: Error?) -> Void)? { get }
    var email: String? { get }
    var password: String? { get }
    
    func performLogin()
}

public class MLILoginViewModel: LoginViewModel {
    
    public var loginComplete: ((_: Error?) -> Void)?
    public var isEmailDirty = false
    public var isPasswordDirty = false
    public var isUserNotYou = false
    
    public var isFieldEmpty: Bool {
        guard let email = email, let password = password else {
            return true
        }
        
        if password.isEmpty || email.isEmpty {
            return true
        }
        
        return false
    }
    
    public var isValid: Bool {
        return self.emailValid && self.passwordValid
    }
    
    public var email: String? {
        didSet {
            self.isEmailDirty = !String.isNilOrEmpty(string: self.email)
        }
    }
    
    public var emailErrorMessage = CommonStrings.Registration.InvalidEmailFootnoteError35
    public var emailValid: Bool {
        if self.email == nil {
            return false
        }
        return self.email!.isValidEmail()
    }
    
    public var password: String? {
        didSet {
            self.isPasswordDirty = !String.isNilOrEmpty(string: self.password)
        }
    }
    
    public var passwordErrorMessage = CommonStrings.Registration.PasswordFieldFootnote29
    public var passwordValid: Bool {
        
        guard let pwd = self.password else {
            return false
        }
        return pwd.isValidPassword()
    }
    
    public func performLogin() {
        
        if self.email == nil || self.password == nil {
            self.loginComplete(error: LoginError.missingEmailOrPassword)
            return
        }
        
        //TODO: temporary workaround to clear cache between users
        DataProvider.reset()
        Wire.Member.login(email: self.email!, password: self.password!, completion: { [weak self] (error, upgradeUrl) in
            /*
             * If Appstore URL is valid and not empty. Notify user to download the latest app.
             */
            if let stringURL = upgradeUrl, let url = URL(string: stringURL){
                NotificationCenter.default.post(name: .VIAAppUpdateException, object: stringURL)
            }
            self?.loginComplete(error: error)
            
        })
        
    }
    
    func loginComplete(error: Error?) {
        guard self.loginComplete != nil else {
            return
        }
        
        //Store Encryption Key
        KeychainWrapper.standard.set(self.generateEncryptionKey()!, forKey: "encryptionKey")
        
        //Convert User Password to Base64 and convert to Data
        let passwordToBase64 = NSData(base64Encoded: (password?.toBase64())!, options: NSData.Base64DecodingOptions.ignoreUnknownCharacters)
        
        //Encrypt User Password
        let encryptedPassword = RNCryptor.encrypt(data: passwordToBase64! as Data, withPassword: KeychainWrapper.standard.string(forKey: "encryptionKey")!)
        
        //Store Encrypted Password
        KeychainWrapper.standard.set(encryptedPassword, forKey: "encryptedPassword")
        
        self.loginComplete!(error)
    }
    
    func generateEncryptionKey() -> String? {
        
        var keyData = Data(count: 32)
        let result = keyData.withUnsafeMutableBytes {
            (mutableBytes: UnsafeMutablePointer<UInt8>) -> Int32 in
            SecRandomCopyBytes(kSecRandomDefault, keyData.count, mutableBytes)
        }
        if result == errSecSuccess {
            return keyData.base64EncodedString()
        } else {
            print("Problem generating encryption key")
            return nil
        }
    }
}
