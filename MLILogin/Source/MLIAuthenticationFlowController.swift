//
//  MLIAuthenticationFlowController.swift
//  MLILogin
//
//  Created by Michelle R. Oratil on 21/01/2019.
//  Copyright © 2019 Glucode. All rights reserved.
//

import UIKit
import VitalityKit
import VIAUtilities
import SafariServices

let tokenKey = "id_token"

protocol MLILoginDelegate: class {
    // FLOW ELEMENT: Web Auth Screen
    func presentWebView(_ view: UIViewController)
    
    // FLOW ELEMENT: Web Auth Screen > Successful? > N
    func returnToWelcomeScreenBecauseOf(error: Error)
    func loginFailedBecauseOf(error: Error)
    
    // FLOW ELEMENT: Web Auth Screen > Successful? > Y
    func webAuthenticationSucceeded()
    
    // FLOW ELEMENT: Web Auth Screen > Successful? > Y > UKE Registration/Login Service > Successful? > Y
    // FLOW ELEMENT: UKE Registration Screen > UKE Registration/Login Service > Successful? > Y
    func presentNextStep()
    
    // FLOW ELEMENT: Web Auth Screen > Successful? > Y > UKE Registration/Login Service > Successful? > N
    func presentMLIRegistrationScreen()
}

//protocol UKERegistrationDelegate: class {
//    // FLOW ELEMENT: UKE Registration Screen > UKE Registration/Login Service > Successful? > N
//    func authorisationCodeValidationFailed()
//    func dateOfBirthValidationFailed()
//
//    // FLOW ELEMENT: UKE Registration Screen > UKE Registration/Login Service > Entity # Provided? Y > Valid? > N
//    func entityNumberValidationFailed()
//
//    func unknownErrorOccured()
//}

class MLIAuthenticationFlowController: NSObject, SFSafariViewControllerDelegate {
    weak var loginDelegate: MLILoginDelegate?
    //weak var registrationDelegate: UKERegistrationDelegate?
    var webAuthToken: String?
    var awaitingCallback: Bool?
    
    // MARK: - Get Started -> Web Authentication
    
    // FLOW ELEMENT: Web Auth Screen
    func startWebAuthentication(withDelegate delegate: MLILoginDelegate?) {
        loginDelegate = delegate
        awaitingCallback = true
        
        guard let url = webAuthenticationURL(for: Wire.default.currentEnvironment) else { return }
        let webAuthViewController = SFSafariViewController(url: url)
        webAuthViewController.delegate = self
        NotificationCenter.default.addObserver(forName: .VIAApplicationDidOpenURL, object: nil, queue: .main, using: webAuthenticationCompleted(withNotification:))
        
        loginDelegate?.presentWebView(webAuthViewController)
    }
    
    func webAuthenticationURL(for environment: Wire.Environment) -> URL? {
        let urlString = webAuthenticationURLString(for: environment)
        return URL(string: urlString)
    }
    
    func webAuthenticationURLString(for environment: Wire.Environment) -> String {
        return "https://portal.manulife.ca/manulifevitality"
    }
    
    // FLOW ELEMENT: Web Auth Screen > Successful?
    func webAuthenticationCompleted(withNotification notification: Notification) {
        guard let awaitingCallback = awaitingCallback, awaitingCallback else { return }
        NotificationCenter.default.removeObserver(self, name: .VIAApplicationDidOpenURL, object: nil)
        self.awaitingCallback = false
        guard let info = notification.object as? NotificationInfo,
            let urlComponents = info.urlComponents,
            let token = urlComponents.queryItems?.first(where: {$0.name == tokenKey})?.value
            else {
                // Successful? -> No
                webAuthenticationFailed()
                return
        }
        
        // Successful? -> Yes
        webAuthenticationSucceeded(with: token)
        
    }
    
    // FLOW ELEMENT: Web Auth Screen > Successful? > N
    func webAuthenticationFailed() {
        // This should display some kind of error message
        // It's incredibly unlikely that we'd actually encounter this:
        // A URL callback after a failed login? The web page would likely deal with this
        loginDelegate?.returnToWelcomeScreenBecauseOf(error: MLIAuthenticationError.webAuthFailed)
    }
    
    // FLOW ELEMENT: Web Auth Screen > Successful? > Y
    func webAuthenticationSucceeded(with token: String) {
        loginDelegate?.webAuthenticationSucceeded()
        startMLILogin(with: token)
    }
    
    // FLOW ELEMENT: Web Auth Screen > Dismissed
    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        awaitingCallback = false
    }
    
    // MARK: - Web Authentication Successful -> MLI Login
    
    // FLOW ELEMENT: Web Auth Screen > Successful? > Y > MLI Login Service
    func startMLILogin(with token: String) {
        webAuthToken = token
        // Service Call -> MLI Login
        Wire.Member.mliLogin(token: token) { [weak self] (username, password, error) in
            
            // FAILURE - Handle error
            if let error = error {
                if let error = error as? MLIAuthenticationError {
                    switch error {
                    case .registrationRequired:
                        self?.ukeRegistrationFailed()
                    default:
                        self?.loginDelegate?.loginFailedBecauseOf(error: error)
                        break
                    }
                } else {
                    self?.loginDelegate?.loginFailedBecauseOf(error: error)
                }
                
                return
            }
            
            // SUCCESS - Login
            guard let username = username else { return }
            guard let password = password else { return }
            self?.ukeRegistrationSucceeded(withEmail: username, password: password)
        }
        
    }
    
    // FLOW ELEMENT: Web Auth Screen > Successful? > Y > UKE Login Service > Successful? > Y
    // FLOW ELEMENT: UKE Registration Screen > UKE Registration Service > Successful? > Y
    func ukeRegistrationSucceeded(withEmail email: String, password: String) {
        let prefixedEmail = ensureEnvironmentPrefixIsPresent(for: email)
        startVALogin(withEmail: prefixedEmail, password: password)
    }
    
    func ensureEnvironmentPrefixIsPresent(for email: String) -> String {
        let prefix = Wire.default.currentEnvironment.prefix()
        guard !(email.contains(prefix)) else { return email }
        return prefix + email
    }
    
    // FLOW ELEMENT: Web Auth Screen > Successful? > Y > UKE Login Service > Successful? > N
    // FLOW ELEMENT: UKE Registration Screen > UKE Registration Service > Successful? > N
    func ukeRegistrationFailed() {
        // present Authentication Code & DOB screen
        loginDelegate?.presentMLIRegistrationScreen()
    }
    
    // FLOW ELEMENT: Web Auth Screen > Successful? > Y > UKE Login Service > Successful? > N > UKE Registration Screen > UKE Registration Service
//    func validateUKERegistrationInfo(withAuthenticationCode authenticationCode: String, dateOfBirth: Date, entityNumber: String?, delegate authDelegate: UKERegistrationDelegate?) {
//        registrationDelegate = authDelegate
//        
//        guard let token = webAuthToken else { return }
//        
//        // Service Call -> UKE Registration
//        Wire.Member.ukeRegister(token: token, authorizationCode: authenticationCode, dateOfBirth: dateOfBirth, entityNumber: entityNumber) { [weak self] (username, password, error) in
//            if let error = error {
//                // FAILURE
//                if let error = error as? UKEAuthenticationError {
//                    switch error {
//                    case .invalidDateOfBirthV1, .invalidDateOfBirthVHS:
//                        self?.registrationDelegate?.dateOfBirthValidationFailed()
//                    case .invalidEntityNumber:
//                        self?.registrationDelegate?.entityNumberValidationFailed()
//                    case .invalidAuthorizationCode, .unableToDecryptAuthorizationCode:
//                        self?.registrationDelegate?.authorisationCodeValidationFailed()
//                    default:
//                        self?.registrationDelegate?.unknownErrorOccured()
//                    }
//                    return
//                }
//                
//                self?.registrationDelegate?.unknownErrorOccured()
//                return
//            }
//            
//            // SUCCESS
//            guard let username = username else { return }
//            guard let password = password else { return }
//            self?.ukeRegistrationSucceeded(withEmail: username, password: password)
//        }
//    }
    
    // MARK: - UKE Registration/Login Successful -> VA Login
    
    // FLOW ELEMENT: Web Auth Screen > Successful? > Y > UKE Login Service > Successful? > Y > VA Login
    // FLOW ELEMENT: UKE Registration Screen > UKE Registration Service > Successful? > Y > VA Login
    func startVALogin(withEmail email: String, password: String) {
        Wire.Member.login(email: email, password: password, completion: { [weak self] (error, upgradeUrl) in
            
            /*
             * If Appstore URL is valid and not empty. Notify user to download the latest app.
             */
            if let url = upgradeUrl{
                NotificationCenter.default.post(name: .VIAAppUpdateException, object: url)
            }
            if let error = error {
                self?.vaAuthenticationFailed(error: error)
            } else {
                self?.vaAuthenticationSucceeded()
            }
        })
    }
    
    // FLOW ELEMENT: VA Login > Successful? > N
    func vaAuthenticationFailed(error: Error) {
        loginDelegate?.loginFailedBecauseOf(error: error)
    }
    
    // FLOW ELEMENT: VA Login > Successful? > Y
    func vaAuthenticationSucceeded() {
        loginDelegate?.presentNextStep()
    }
}
