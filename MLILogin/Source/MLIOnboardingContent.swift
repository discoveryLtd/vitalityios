//
//  MLIOnboardingContent.swift
//  MLILogin
//
//  Created by Michelle R. Oratil on 15/01/2019.
//  Copyright © 2019 Glucode. All rights reserved.
//


import Foundation
import VitalityKit

public extension MLIFirstTimeOnboardingViewController {
    class func skipButtonTitle() -> String {
        return CommonStrings.SkipButtonTitle11
    }
    
    class func heading1() -> String {
        return CommonStrings.Onboarding.KnowYourHealthTitle12
    }
    
    class func heading2() -> String {
        return CommonStrings.Onboarding.ImproveYourHealthTitle14
    }
    
    class func heading3() -> String {
        return CommonStrings.Onboarding.EnjoyWeeklyRewardsTitle16
    }
    
    class func description1() -> String {
        return CommonStrings.Onboarding.KnowYourHealthDescription13
    }
    
    class func description2() -> String {
        return CommonStrings.Onboarding.ImproveYourHealthDescription15
    }
    
    class func description3() -> String {
        return CommonStrings.Onboarding.EnjoyWeeklyRewardsDescription17
    }
}
