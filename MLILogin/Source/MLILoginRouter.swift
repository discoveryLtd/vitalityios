//
//  MLILoginRouter.swift
//  MLILogin
//
//  Created by Michelle R. Oratil on 21/01/2019.
//  Copyright © 2019 Glucode. All rights reserved.
//
import Foundation
import Alamofire
import VitalityKit

// MARK: Errors

public enum MLIAuthenticationError: Int, Error {
    case unknown = 0
    case tokenRequired = 6
    case tokenInvalid = 3
    case registrationRequired = 1
    case invalidDateOfBirthV1 = 13
    case invalidDateOfBirthVHS = 15
    case invalidEntityNumber = 4
    case invalidAuthorizationCode = 2
    case unableToDecryptAuthorizationCode = 5
    case webAuthFailed = 700
    case invalidResponse = 900
}

// MARK: Router


enum MLILoginRouter: URLRequestConvertible {
    
    case login(token: String)
    //case register(token: String, authorizationCode: String, dateOfBirth: Date, entityNumber: String?)
    
    var basePath: String {
        switch self {
        case .login:
            return "tstc-integration-platform-services-service-ex"
        }
    }
    
    var method: HTTPMethod {
        switch self {
        case .login:
            return .post
        }
    }
    
    var route: String {
        switch self {
        case .login:
            return "register"
        }
    }
    
    var parameters: Parameters? {
        switch self {
            case .login(let token):
            let body = MLILoginParameters(token: token)
            return body.toJSON()
        }
    }
    
    // MARK: URLRequestConvertible
    
    func asURLRequest() throws -> URLRequest {
        let url = try Wire.urlForPath(path: self.basePath).appendingPathComponent(self.route)
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = self.method.rawValue
        
        switch self {
        case .login:
            return try JSONEncoding.default.encode(urlRequest, with: self.parameters)
        }
    }
}

// MARK: Networking

extension Wire.Member {
    
    public static func mliLogin(token: String, completion: @escaping ((_ username: String?, _ password: String?, _: Error?) -> Void)) {
        let loginRequest = MLILoginRouter.login(token: token)
        Wire.Member.mliAuthenticate(token: token, request: loginRequest, completion: completion)
    }
    
//    public static func ukeRegister(token: String, authorizationCode: String, dateOfBirth: Date, entityNumber: String?, completion: @escaping ((_ username: String?, _ password: String?, _: Error?) -> Void)) {
//        let registerRequest = MLILoginRouter.register(token: token, authorizationCode: authorizationCode, dateOfBirth: dateOfBirth, entityNumber: entityNumber)
//        Wire.Member.mliAuthenticate(token: token, request: registerRequest, completion: completion)
//    }
    
    internal static func mliAuthenticate(token: String, request: URLRequestConvertible, completion: @escaping ((_ username: String?, _ password: String?, _: Error?) -> Void)) {
        guard !token.isEmpty else {
            return completion(nil, nil, MLIAuthenticationError.tokenRequired)
        }
        Wire.unauthorisedSessionManager.request(request)
            .log()
            .validate(statusCode: 200 ..< 300)
            .apiManagerResponse { response in
                switch response.httpStatusCode {
                case 200 ..< 300:
                    Wire.Member.parseSuccessfulMLIAuthenticationResponse(response, completion: completion)
                case 400:
                    Wire.Member.parseFailedMLIAuthenticationResponse(response, completion: completion)
                default:
                    completion(nil, nil, response.result.error ?? MLIAuthenticationError.unknown)
                }
        }
    }
    
    internal static func parseSuccessfulMLIAuthenticationResponse(_ response: DataResponse<APIManagerResponse>, completion: @escaping ((_ username: String?, _ password: String?, _: Error?) -> Void)) {
        let responseInfo = response.jsonDictionary
        guard
            let username = responseInfo["username"] as? String,
            let password = responseInfo["password"] as? String else {
                completion(nil, nil, MLIAuthenticationError.invalidResponse)
                return
        }
        
        completion(username, password, nil)
    }
    
    internal static func parseFailedMLIAuthenticationResponse(_ response: DataResponse<APIManagerResponse>, completion: @escaping ((_ username: String?, _ password: String?, _: Error?) -> Void)) {
        guard let responseData = response.data,
            let jsonResponse = JSONSerialization.jsonObject(data: responseData),
            let errors = jsonResponse["errors"] as? [[String : Any]],
            let codeString = errors[0]["code"] as? String,
            let code = Int(codeString)
            else { return completion(nil, nil, MLIAuthenticationError.unknown) }
        let error = MLIAuthenticationError(rawValue: code) ?? MLIAuthenticationError.unknown
        completion(nil, nil, error)
    }
}

// MARK: Structs

public struct MLILoginParameters {
    public var token: String
    
    public init(token: String) {
        self.token = token
    }
    
    public func toJSON() -> Parameters {
        return [ "token": self.token ]
    }
}

//public struct UKERegistrationParameters {
//    public var token: String
//    public var authorizationCode: String
//    public var dateOfBirth: Date
//    public var entityNumber: String?
//
//    public init(token: String, authorizationCode: String, dateOfBirth: Date, entityNumber: String?) {
//        self.token = token
//        self.authorizationCode = authorizationCode
//        self.dateOfBirth = dateOfBirth
//        self.entityNumber = entityNumber
//    }
//
//    public func toJSON() -> Parameters {
//        let dateFormatter = DateFormatter()
//        dateFormatter.dateFormat = "yyyy-MM-dd"
//        let dateOfBirthValidation: Parameters = [
//            "typeKey": 1,
//            "typeCode": "DOB",
//            "value": dateFormatter.string(from: self.dateOfBirth)
//        ]
//
//        var validations: [Parameters] = [dateOfBirthValidation]
//
//        if let entityNumber = self.entityNumber {
//            let entityNumberValidation: Parameters = [
//                "typeKey": 2,
//                "typeCode": "DEN",
//                "value": entityNumber
//            ]
//            validations.append(entityNumberValidation)
//        }
//        return [
//            "token": self.token,
//            "authorizationCode": self.authorizationCode,
//            "validation": validations
//        ]
//    }
//}
