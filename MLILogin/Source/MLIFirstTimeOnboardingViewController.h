//
//  MLIFirstTimeOnboardingViewController.h
//  MLILogin
//
//  Created by Michelle R. Oratil on 15/01/2019.
//  Copyright © 2019 Glucode. All rights reserved.
//

#import <JazzHands/IFTTTJazzHands.h>

@interface MLIFirstTimeOnboardingViewController : IFTTTAnimatedPagingScrollViewController

@property (nonatomic, assign) BOOL displayLoginImmediately;

@end
