//
//  MLILoginLearnMoreViewController.swift
//  MLILogin
//
//  Created by Michelle R. Oratil on 23/01/2019.
//  Copyright © 2019 Glucode. All rights reserved.
//

import UIKit
import VIAUIKit
import VIACommon
import VIAUtilities
import VitalityKit

class MLILoginLearnMoreViewController: VIATableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Activate"
        configureAppearance()
        configureTableView()
    }

    func configureAppearance() {
        navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    func configureTableView(){
        self.tableView.separatorInset = UIEdgeInsets(top: 0, left: 38, bottom: 0, right: 0)
        self.tableView.separatorColor = .clear
        self.tableView.backgroundColor = UIColor.day()
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 75
        self.tableView.register(MLILearnMoreTableViewCell.nib(), forCellReuseIdentifier: MLILearnMoreTableViewCell.defaultReuseIdentifier)
    }
    
    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 1
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: MLILearnMoreTableViewCell.defaultReuseIdentifier, for: indexPath)
        let learnMore = cell as! MLILearnMoreTableViewCell
        
        learnMore.headerTitle = CommonStrings.Login.LearnMoreTitle2284
        // OJ: Replace this with PhraseApp token 
        learnMore.detailTitle = "To enjoy the benefits of the Manulife Vitality Program you must first activate it on the Manulife website, visit www.manulife.ca/vitality on your laptop or desktop machine to do so."

        // Configure the cell...

        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
}
