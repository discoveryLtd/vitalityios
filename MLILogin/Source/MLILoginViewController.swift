//
//  MLILoginViewController.swift
//  MLILogin
//
//  Created by Michelle R. Oratil on 15/01/2019.
//  Copyright © 2019 Glucode. All rights reserved.
//

import VIACommon
import UIKit
import VIAUIKit
import VitalityKit
import TTTAttributedLabel
import LocalAuthentication
import VIAHealthKit
import HealthKit
//import VIAAssessments
import VIAWellnessDevices
import VIAUtilities
import SafariServices

let loginRegistration = "Login"

protocol LoginViewController {
    var viewModel: MLILoginViewModel { get set }
    var loginHasFailedAtLeastOnce: Bool { get set }
}

public class MLINavigationController: UINavigationController{
    
}

public class MLILoginViewController: VIAViewController, LoginViewController, SFSafariViewControllerDelegate, MLILoginDelegate, UITableViewDelegate, UITableViewDataSource, PrimaryColorTintable, VIAAppleHealthHandler {
    
    // MARK: Properties
    @IBOutlet var logoImageView: UIImageView!
    @IBOutlet var tableView: UITableView!
    var defaultCellSeparatorColor: UIColor?
    var viewModel: MLILoginViewModel = MLILoginViewModel()
    var loginHasFailedAtLeastOnce = false
    var loginButton: VIAButton = VIAButton(title: "I have activated the Manulife Vitality Program")
    var loggingInSpinner: UIActivityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
    public var parentController: UIViewController?
    
    let authFlowController: MLIAuthenticationFlowController = MLIAuthenticationFlowController()
    // MARK: View lifecycle
    
    public override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        
        self.logoImageView.image = VIAIconography.default.loginLogo()
        self.view.backgroundColor = UIColor.day()
        self.configureTableView()
        //self.populateUsernameIfSaved()
        self.tableView.reloadData()
    }
    
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        UIApplication.shared.statusBarStyle = .default
        self.configureKeyboardAutoToolbar(VIAApplicableFeatures.default.enableKeyboardAutoToolbar ?? false)
        
        self.navigationController?.navigationBar.isHidden = true
    }
    
    public override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.disableKeyboardAutoToolbar()
        self.navigationController?.navigationBar.isHidden = false
        self.hideBackButtonTitle(parentController)
    }
    
    public override func viewDidAppear(_ animated: Bool) {
        self.debugLogin()
    }
    
    func configureTableView() {
        self.defaultCellSeparatorColor = self.tableView.separatorColor
        
        let deviceType = UIDevice.current.getDeviceType()
        switch deviceType {
        case .AppleIphone6S, .AppleIphone7:
            if #available(iOS 8.0, *) {
                if((UIScreen.main.bounds.size.height == 667.0 || UIScreen.main.bounds.size.height == 568.0) && UIScreen.main.nativeScale < UIScreen.main.scale) {
                    enablePageScrollingOnLoginTableView()
                }
            }
            break
        case .AppleIphone5C, .AppleIphone5, .AppleIphone5S, .AppleIphoneSE, .Simulator:
            enablePageScrollingOnLoginTableView()
            break
        default:
            self.tableView.isScrollEnabled = false
        }
        self.tableView.separatorInset = UIEdgeInsets(top: 0, left: 38, bottom: 0, right: 0)
        self.tableView.separatorColor = .clear
        self.tableView.backgroundColor = UIColor.day()
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 75
//        self.tableView.register(VIALabelTableViewCell.nib(), forCellReuseIdentifier: VIALabelTableViewCell.defaultReuseIdentifier)
//        self.tableView.register(VIAEmailTextFieldTableViewCell.nib(), forCellReuseIdentifier: VIAEmailTextFieldTableViewCell.defaultReuseIdentifier)
        self.tableView.register(MLILoginTableViewCell.nib(), forCellReuseIdentifier: MLILoginTableViewCell.defaultReuseIdentifier)
        self.configureTableViewFooterView()
    }
    
    func configureTableViewFooterView() {
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: self.tableView.frame.size.width, height: 0))
        
        // login button
        loginButton.isEnabled = true
        self.loginButton.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        self.loginButton.translatesAutoresizingMaskIntoConstraints = false
        
        loggingInSpinner.frame = CGRect(x: 70.0, y: 17.0, width: 10.0, height: 10.0)
        loggingInSpinner.startAnimating()
        loggingInSpinner.alpha = 0.0
        loginButton.addSubview(loggingInSpinner)
        
        self.loginButton.addTarget(self, action: #selector(performLogin(_:)), for: .touchUpInside)
        footerView.addSubview(self.loginButton)
        
        // forgot password
        let learnMore = VIAButton(title: CommonStrings.LearnMoreTitle1082)
        learnMore.hidesBorder = true
        learnMore.highlightedTextColor = learnMore.tintColor
        learnMore.titleLabel?.font = UIFont.tableViewFooterLabelButton()
        learnMore.addTarget(self, action: #selector(showLearnMore(_:)), for: .touchUpInside)
        footerView.addSubview(learnMore)
        
        // constraints
        let views = ["learnMore": learnMore, "footer": footerView, "login": self.loginButton]
        footerView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[login(44)]-30-[learnMore(22)]|", options: [], metrics: nil, views: views))
        footerView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[login]|", options: [], metrics: nil, views: views))
        footerView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-[learnMore]-|", options: [], metrics: nil, views: views))
        
        // fix frame
        var frame = footerView.frame
        let newHeight = footerView.systemLayoutSizeFitting(UILayoutFittingCompressedSize).height
        frame.size.height = newHeight
        footerView.frame = frame
        
        self.tableView.tableFooterView = footerView
    }
    
    
    // MARK: MLILoginDelegate
    func presentWebView(_ view: UIViewController) {
        present(view, animated: true, completion: nil)
    }
    
    func returnToWelcomeScreenBecauseOf(error: Error) {
        dismiss(animated: true, completion: nil)
        showUnknownError()
    }
    
    func loginFailedBecauseOf(error: Error) {
        hideHUDFromWindow()
        if let error = error as? BackendError {
            handleBackendErrorWithAlert(error)
        } else if let error = error as? LoginError {
            var dialogTitle = ""
            var dialogMessage = ""
            
            if error == .accountLockedOut {
                dialogTitle = CommonStrings.Login.AccountLockedAlertTitle737
                dialogMessage = CommonStrings.Login.AccountLockedErrorAlertMessage738
            } else {
                dialogTitle = CommonStrings.Login.IncorrectEmailPasswordErrorTitle47
                dialogMessage = CommonStrings.Login.IncorrectEmailPasswordErrorMessage48
            }
            
            let controller = UIAlertController(title: dialogTitle, message: dialogMessage, preferredStyle: .alert)
            
            
            let ok = UIAlertAction(title: CommonStrings.OkButtonTitle40, style: .cancel, handler: nil)
            controller.addAction(ok)
            
            self.present(controller, animated: true, completion: nil)
        } else {
            showUnknownError()
        }
    }
    
    func showUnknownError() {
        let alert = UIAlertController(title: CommonStrings.GenericUnkownErrorTitle266, message: CommonStrings.GenericUnkownErrorMessage267, preferredStyle: .alert)
        let okAction = UIAlertAction(title: CommonStrings.OkButtonTitle40, style: .default, handler: nil)
        alert.addAction(okAction)
        present(alert, animated: true, completion: nil)
    }
    
    func webAuthenticationSucceeded() {
        dismiss(animated: true, completion: nil)
        showHUDOnWindow()
    }
    
    func presentNextStep() {
        
        hideHUDFromWindow()
        AppSettings.updateLastLoginDate()
        
        // for Apple Health device auto-sync
        configureAutoSync()
        
        MLISplashScreenViewController.showInsurerSplashScreen(skipAppConfigRequest: true, segueToHomeCompletion: {
            VIAApplicableFeatures.default.navigateToHomeScreen()
        })
        
    }
    
    func presentMLIRegistrationScreen() {
//        hideHUDFromWindow()
//        dismiss(animated: true, completion: nil)
//        performSegue(withIdentifier: "showAuthenticationScreen", sender: self)
    }
    
    // MARK: UITableView datasource
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: MLILoginTableViewCell.defaultReuseIdentifier, for: indexPath)
        let loginCell = cell as! MLILoginTableViewCell
        
        loginCell.headerTitle = "Welcome"
        loginCell.headerDescription = "To enjoy the Manulife Vitality Program you first need to activate it on the Manulife website"
        
        return cell
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return UITableViewAutomaticDimension
        }
        return UITableViewAutomaticDimension
    }
    
    // MARK: UITableView delegate
    
    public func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    public func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
    
    // MARK: Actions
//    func addGestureRecogniser() {
//        let hold = UILongPressGestureRecognizer(target: self, action: #selector(environmentSelectionGestureRecognized(_:)))
//        hold.numberOfTapsRequired = 2
//        hold.numberOfTouchesRequired = 2
//        hold.minimumPressDuration = 3
//        view.addGestureRecognizer(hold)
//    }
    
    
    func dismissKeyboard(_ gesture: UITapGestureRecognizer?) {
        self.tableView.endEditing(true)
    }
    
    func performLogin(_ sender: UIButton?) {
        #if WIREMOCK
            debugLogin()
        #else
            authFlowController.startWebAuthentication(withDelegate: self)
        #endif
    }
    
    func debugLogin() {
        let loginAlert = UIAlertController(title: "Test Manulife Vitality Grp Benefits Login", message: "Complete Manulife Login Details", preferredStyle: .alert)
        loginAlert.addTextField(configurationHandler: { field in
            field.placeholder = "Email"
            field.autocapitalizationType = .none
            field.autocorrectionType = .no
        })
        loginAlert.addTextField(configurationHandler: { field in
            field.placeholder = "Password"
            field.isSecureTextEntry = true
            field.autocapitalizationType = .none
            field.autocorrectionType = .no
        })
        let okAction = UIAlertAction(title: "OK", style: .default, handler: { [unowned self] action in
            guard
                let email = loginAlert.textFields?.first?.text,
                let password = loginAlert.textFields?.last?.text,
                !email.isEmpty,
                !password.isEmpty
                else { return }
            
            self.showHUDOnWindow()
            self.authFlowController.loginDelegate = self
            self.authFlowController.startVALogin(withEmail: email, password: password)
        })
        loginAlert.addAction(okAction)
        
        #if DEBUG
        loginAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        #endif
        
        present(loginAlert, animated: true, completion: nil)
    }
    
    func loginComplete(_ error: Error?) {
        loggingInSpinner.alpha = 0.0
        resetLoginButton()
        
        guard error == nil else {
            switch error {
            case is LoginError:
                self.handleLoginError(error as! LoginError)
            case is RegistrationError:
                self.handleLoginError(error as! LoginError)
            case is BackendError:
                self.handleBackendErrorWithAlert(error as! BackendError, tryAgainAction: { [weak self] in
                    self?.performLogin(nil)
                })
            default:
                self.displayUnknownServiceErrorOccurredAlert()
            }
            self.hideHUDFromWindow()
            return
        }
        
        if DataProvider.currentVitalityPartyIsValid(){
            
            AppSettings.updateLastLoginDate()
            //HomeScreenDataController.loadData(realm: DataProvider.newRealm(), completion: nil)
            
            // for Apple Health device auto-sync
            configureAutoSync()
            
            MLISplashScreenViewController.showInsurerSplashScreen(skipAppConfigRequest: true, segueToHomeCompletion: {})
        }else{
            debugPrint("[DebugMode] current Vitality Party is INVALID. User should be logged out.")
            NotificationCenter.default.post(name: .VIAServerSecurityException, object: nil)
        }
    }
    
    func handleLoginError(_ error: LoginError) {
        self.displayLoginErrorMessage(error: error )
        resetLoginButton()
    }
    
    func resetLoginButton() {
        self.loginButton.setTitle(CommonStrings.Login.LoginButtonTitle20, for: .normal)
    }
    
    func displayLoginErrorMessage(error: LoginError) {
        
        var dialogTitle = ""
        var dialogMessage = ""
        
        if error == .accountLockedOut {
            dialogTitle = CommonStrings.Login.IncorrectEmailPasswordErrorTitle47
            dialogMessage = CommonStrings.Login.AccountLockedErrorAlertMessage738
        }
        else {
            dialogTitle = CommonStrings.Login.IncorrectEmailPasswordErrorTitle47
            dialogMessage = CommonStrings.Login.IncorrectEmailPasswordErrorMessage48
        }
        
        let controller = UIAlertController(title: dialogTitle,
                                           message: dialogMessage,
                                           preferredStyle: .alert)
        
        if error == .accountLockedOut {
            let resetPassword = UIAlertAction(title: CommonStrings.Login.ResetPasswordButtonTitle22, style: .default) { action in
                //self.showForgotPassword(nil)
            }
            controller.addAction(resetPassword)
        }
        else if self.loginHasFailedAtLeastOnce {
            let forgotPassword = UIAlertAction(title: CommonStrings.Login.ForgotPasswordButtonTitle22, style: .default) { action in
                //self.showForgotPassword(nil)
            }
            controller.addAction(forgotPassword)
        }
        
        let ok = UIAlertAction(title: CommonStrings.OkButtonTitle40, style: .cancel) { action in
        }
        controller.addAction(ok)
        
        self.loginHasFailedAtLeastOnce = true
        self.present(controller, animated: true, completion: nil)
    }
    
    func toggleLoginButtonIsEnabled() {
        if self.viewModel.isFieldEmpty || !viewModel.emailValid {
            //self.loginButton.isEnabled = false
            self.loginButton.alpha = 0.5
            
        } else {
            //self.loginButton.isEnabled = true
            self.loginButton.alpha = 1.0
        }
    }
    
    
    func displayMessageForDomainStateChanged() {
        
        let alert = UIAlertController(title: CommonStrings.Login.Touchid.FingerprintChangeTitle1140, message: CommonStrings.Login.Touchid.FingerprintChangeMessage1141, preferredStyle: .alert)
        
        let yes = UIAlertAction(title: "Yes", style: .default) { action in
            
            alert.dismiss(animated: true, completion: nil)
            
            //self.viewModel.updateTouchIdPreference()
        }
        
        let no = UIAlertAction(title: "No", style: .cancel) { action in
            
            alert.dismiss(animated: true, completion: nil)
            
            self.displayErrorAlertController(title: CommonStrings.Login.Touchid.TouchIdDisabledTitle1142, message:  CommonStrings.Login.Touchid.TouchIdDisabledMessage1143)
        }
        
        alert.addAction(yes)
        alert.addAction(no)
        self.present(alert, animated: true, completion: nil)
    }
    
    func displayErrorAlertController(title : String, message : String) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let ok = UIAlertAction(title: CommonStrings.OkButtonTitle40, style: .cancel) { action in
            alert.dismiss(animated: true, completion: nil)
            
            //self.viewModel.turnOffLoginWithTouchId()
        }
        
        alert.addAction(ok)
        self.present(alert, animated: true, completion: nil)
    }
    
    // MARK: Navigation
    
    public static func showLogin(upgradeUrl: String? = nil) {
        let storyboardBundle = Bundle(for: MLIFirstTimeOnboardingViewController.self)
        let viewController = UIStoryboard(name: "Login", bundle: storyboardBundle).instantiateViewController(withIdentifier: "MLIFirstTimeOnboardingViewController") as! MLIFirstTimeOnboardingViewController
        viewController.displayLoginImmediately = true
        UIApplication.shared.keyWindow?.rootViewController = viewController
        
        logOut()
        
        if let enableOTAUpdate = Bundle.main.object(forInfoDictionaryKey: "VIAEnableOTAUpdate") as? Bool, enableOTAUpdate{
            guard let appURL = upgradeUrl, let url = URL(string: appURL) else { return }
            let alert = UIAlertController(title: CommonStrings.AppUpdate.HeaderTitle2126,
                                          message: CommonStrings.AppUpdate.Message2127, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: CommonStrings.AppUpdate.UpdateButton2128, style: .default, handler: { (action) in
                if UIApplication.shared.canOpenURL(url){
                    UIApplication.shared.open(url)
                }
            }))
            viewController.present(alert, animated: true, completion: nil)
        }
    }
    
    private static func logOut() {
        Wire.cancelAllTasks()
        DataProvider.reset()
        
//        NotificationCenter.default.removeObserver(AssessmentLandingViewController.self, name: .VIAVHRQuestionnaireSubmittedNotification, object: nil)
//        NotificationCenter.default.removeObserver(AssessmentLandingViewController.self, name: .VIAVHRQuestionnaireStarted, object: nil)
    }
    
    public override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        if segue.identifier == "showLearnMore" {
//            let navController = segue.destination as? UINavigationController
//            let viewController = navController?.topViewController as? MLILoginLearnMoreViewController
//            //UIApplication.shared.keyWindow?.rootViewController = viewController
//            //viewController?.viewModel.email = self.viewModel.email
//        }
    }
    
    @IBAction public func unwindToLogin(segue: UIStoryboardSegue) {
        // typically called when register / forgot password was cancelled
    }

    func showRegistrationForNotYou(_ sender: UIButton?) {
        
        viewModel.isUserNotYou = false
        
        sender?.setTitle(CommonStrings.Login.RegisterButtonTitle23, for: .normal)
        sender?.removeTarget(self, action: #selector(showRegistrationForNotYou(_:)), for: .touchUpInside)
        sender?.addTarget(self, action: #selector(showRegistration(_:)), for: .touchUpInside)
        
        AppSettings.removeUsernameForRememberMe()
        viewModel.email = ""
        //viewModel.turnOffLoginWithTouchId()
        
        self.tableView.reloadData()
    }
    
    func showRegistration(_ sender: UIButton?) {
        self.performSegue(withIdentifier: "showRegistration", sender: nil)
    }
    
    func showLearnMore(_ sender: UIButton?) {
        parentController?.performSegue(withIdentifier: "showLearnMore", sender: nil)
    }
}

extension MLILoginViewController {
    /* Enables table view scrool bar to fix the UI issue on the following iPhone devices:
     
     - iPhone5C
     - iPhone5
     - iPhone5S
     - iPhoneSE
     - iPhone6S  : Zoomed view
     - iPhone7   : Zoomed view
     
     */
    func enablePageScrollingOnLoginTableView() {
        self.tableView.isScrollEnabled = true
        self.tableView.showsVerticalScrollIndicator = false
        self.tableView.showsHorizontalScrollIndicator = false
        
    }
}
