//
//  MLISplashScreenViewController.swift
//  MLILogin
//
//  Created by Michelle R. Oratil on 15/01/2019.
//  Copyright © 2019 Glucode. All rights reserved.
//

import UIKit
import VIAUIKit
import VitalityKit
import VIAHelpFAQ
import VIAHealthKit
import VIAUtilities
import VIAFirstTimePreferences
import VIACommon
import ChameleonFramework

public class MLISplashScreenViewController: VIAViewController, SplashScreenViewModelDelegate {
    
    // MARK: Properties
    
    @IBOutlet private var imageView: UIImageView!
    
    @IBOutlet weak var MLIimageView: UIImageView!
    // MARK: Getters/setters
    
    var shouldDisplayHUD: Bool = true
    
    var viewModel: MLISplashScreenViewModel?
    
    public var segueToHomeCompletion:() -> Void = {}
    
    // MARK: View Lifecycle
    
    override public var preferredStatusBarStyle: UIStatusBarStyle {
        // https://stackoverflow.com/a/2509596/149591
        // ((Red value * 299) + (Green value * 587) + (Blue value * 114)) / 1000
        if let components = self.gradientTop().rgb() {
            let result = ((components.red * 299) + (components.green * 587) + (components.blue * 114)) / 1000
            return result > 125 ? UIStatusBarStyle.default : UIStatusBarStyle.lightContent
        }
        return .lightContent
    }
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        
        self.MLIimageView.backgroundColor = .clear
//        if let tenant = AppSettings.getAppTenant(){
//            // Implemented EC this way because of the way the splash screen asset was given.
//            // Image asset contains 3-stack images and is in 'portrait', as opposed to the default 'landscape'.
//            if tenant == .EC {
//                self.imageView.contentMode = .scaleAspectFill
//            }
//        }
        self.view.backgroundColor = UIColor(gradientStyle: .topToBottom,
                                            withFrame: self.view.bounds,
                                            andColors: [self.gradientTop() as Any, self.gradientBottom()])
    }
    
    override public func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        updatePrimaryColor()
    }
    
    // MARK: Colors
    
    func gradientTop() -> UIColor {
        guard let gradientTop = VIAAppearance.default.dataSource?.getSplashScreenGradientTop() else { return UIColor.white }
        return gradientTop
    }
    
    func gradientBottom() -> UIColor {
        guard let gradientBottom = VIAAppearance.default.dataSource?.getSplashScreenGradientBottom() else { return UIColor.white }
        return gradientBottom
    }
    
    // MARK: Actions
    
    func updatePrimaryColor() {
        if let colorString = AppConfigFeature.insurerGlobalTintColor() {
            VIAAppearance.default.primaryColorFromServer = UIColor(hexString: colorString)
            UIApplication.shared.keyWindow?.tintColor = UIColor.primaryColor()
            let windows = UIApplication.shared.windows
            for window in windows {
                for view in window.subviews {
                    view.removeFromSuperview()
                    window.addSubview(view)
                }
            }
        }
    }
    
    // MARK: Navigation
    
    public static func showInsurerSplashScreen(skipAppConfigRequest: Bool, segueToHomeCompletion: @escaping () -> Void) {
        
        /* If VIADisableSplashScreen is not configured in the Info.plist, make it enabled. */
        let disableSplashScreen = Bundle.main.object(forInfoDictionaryKey: "VIADisableSplashScreen") as? Bool ?? false
        
        let storyboard = UIStoryboard(name: "Login", bundle: nil)
        let viewcontroller = storyboard.instantiateViewController(withIdentifier: "MLISplashScreenViewController") as! MLISplashScreenViewController
        
        viewcontroller.segueToHomeCompletion = segueToHomeCompletion
        
        //self.present(viewcontroller, animated: true, completion: nil)
        
        if disableSplashScreen{
            /* If disabled, navigate to next step and skip splash screen */
            viewcontroller.navigateToNextStep()
        }else{
            /* Else, show the splash screen */
            let viewModel = MLISplashScreenViewModel(delegate: viewcontroller, skipAppConfigRequest: skipAppConfigRequest)
            viewcontroller.viewModel = viewModel
            
            let window = UIApplication.shared.keyWindow
            window?.rootViewController = viewcontroller
        }
    }
    
    // MARK: ViewModelDelegate
    
    func willFetchAppConfigAndUpdateContent() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            if self.shouldDisplayHUD {
                self.showHUDOnView(view: self.view)
            }
        }
    }
    
    func didDownloadLogo(image: UIImage?) {
        self.shouldDisplayHUD = false
        if image != nil {
            self.hideHUDFromView(view: self.view)
            if AppSettings.getAppTenant() == .MLI{
                self.MLIimageView.alpha = 0
                self.MLIimageView.image = image
                UIView.animate(withDuration: 1, animations: {
                    self.MLIimageView.alpha = 1
                })
            }else{
                self.imageView.alpha = 0
                self.imageView.image = image
                UIView.animate(withDuration: 1, animations: {
                    self.imageView.alpha = 1
                })
            }
        }
    }
    
    func navigateToNextStep() {
        if let _ = DataProvider.currentUserInstructionForLoginTermsAndConditions() {
            VIATermsConditionsViewController.showMainTermsAndConditions(
                onDisagree: {
                    /* when user disagrees */
                    MLILoginViewController.showLogin()
            }, onAgree: {
                /* when user agrees */
                if AppSettings.hasShownUserPreferences() {
                    /* navigate to home screen */
                    VIAApplicableFeatures.default.navigateToHomeScreen()
                } else {
                    /* navigate to First time onboarding screen */
                    self.navigateToFirstTimePref()
                }
            })
        } else if !AppSettings.hasShownUserPreferences() {
            //FirstTimePreferenceViewController.showUserPreferences()
            navigateToFirstTimePref()
        } else {
            VIAApplicableFeatures.default.navigateToHomeScreen()
        }
    }
    
    func navigateToFirstTimePref(){
        let storyboard = UIStoryboard(name: "FirstTimePreferences", bundle: nil)
        let viewController = (storyboard.instantiateInitialViewController() as! UINavigationController).topViewController as? FirstTimePreferenceViewController
        UIApplication.shared.keyWindow?.rootViewController = viewController
        
        viewController?.segueToHomeCompletion = segueToHomeCompletion
    }
    
    func serviceErrorOccured(with error: Error) {
        DispatchQueue.main.async {
            self.handleBackendErrorWithAlert((error as? BackendError) ?? BackendError.other)
            MLILoginViewController.showLogin()
        }
    }
}
