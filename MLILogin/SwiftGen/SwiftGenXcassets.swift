// Generated using SwiftGen, by O.Halligon — https://github.com/SwiftGen/SwiftGen

#if os(OSX)
import AppKit.NSImage
public typealias MLILoginColor = NSColor
public typealias MLILoginImage = NSImage
#elseif os(iOS) || os(tvOS) || os(watchOS)
import UIKit.UIImage
public typealias MLILoginColor = UIColor
public typealias MLILoginImage = UIImage
#endif

// swiftlint:disable file_length

public typealias MLILoginAssetType = MLILoginImageAsset

public struct MLILoginImageAsset: Equatable {
	fileprivate var name: String

	public var templateImage: MLILoginImage {
		let bundle = Bundle(for: BundleToken.self)
		#if os(iOS) || os(tvOS)
		let image = MLILoginImage(named: name, in: bundle, compatibleWith: nil)?.withRenderingMode(.alwaysTemplate)
		#elseif os(OSX)
		let image = bundle.image(forResource: name).withRenderingMode(.alwaysTemplate)
		#elseif os(watchOS)
		let image = MLILoginImage(named: name).withRenderingMode(.alwaysTemplate)
		#endif
		guard let result = image else { fatalError("Unable to load template image named \(name).") }
		return result
	}

	public var image: MLILoginImage {
		let bundle = Bundle(for: BundleToken.self)
		#if os(iOS) || os(tvOS)
		let image = MLILoginImage(named: name, in: bundle, compatibleWith: nil)
		#elseif os(OSX)
		let image = bundle.image(forResource: name)
		#elseif os(watchOS)
		let image = MLILoginImage(named: name)
		#endif
		guard let result = image else { fatalError("Unable to load image named \(name).") }
		return result
	}

	public static func ==(lhs: MLILoginImageAsset, rhs: MLILoginImageAsset) -> Bool {
        return lhs.image == rhs.image
    }
}

public struct MLILoginColorAsset {
fileprivate var name: String

#if swift(>=3.2)
@available(iOS 11.0, tvOS 11.0, watchOS 4.0, OSX 10.13, *)
var color: MLILoginColor {
return MLILoginColor(asset: self)
}
#endif
}

// swiftlint:disable identifier_name line_length nesting type_body_length type_name
public enum MLILoginAsset {
  public static let manulifeSplashLogo = MLILoginImageAsset(name: "manulifeSplashLogo")
  public static let manulifeLoginLogo = MLILoginImageAsset(name: "manulifeLoginLogo")

  // swiftlint:disable trailing_comma
  public static let allColors: [MLILoginColorAsset] = [
  ]
  public static let allImages: [MLILoginImageAsset] = [
    manulifeSplashLogo,
    manulifeLoginLogo,
  ]
  // swiftlint:enable trailing_comma
  @available(*, deprecated, renamed: "allImages")
  public static let allValues: [MLILoginAssetType] = allImages
}
// swiftlint:enable identifier_name line_length nesting type_body_length type_name

extension MLILoginImage {
@available(iOS 1.0, tvOS 1.0, watchOS 1.0, *)
@available(OSX, deprecated,
message: "This initializer is unsafe on macOS, please use the MLILoginImageAsset.image property")
convenience init!(asset: MLILoginAssetType) {
#if os(iOS) || os(tvOS)
let bundle = Bundle(for: BundleToken.self)
self.init(named: asset.name, in: bundle, compatibleWith: nil)
#elseif os(OSX) || os(watchOS)
self.init(named: asset.name)
#endif
}
}

extension MLILoginColor {
#if swift(>=3.2)
@available(iOS 11.0, tvOS 11.0, watchOS 4.0, OSX 10.13, *)
convenience init!(asset: MLILoginColorAsset) {
let bundle = Bundle(for: BundleToken.self)
#if os(iOS) || os(tvOS)
self.init(named: asset.name, in: bundle, compatibleWith: nil)
#elseif os(OSX)
self.init(named: asset.name, bundle: bundle)
#elseif os(watchOS)
self.init(named: asset.name)
#endif
}
#endif
}

private final class BundleToken {}
