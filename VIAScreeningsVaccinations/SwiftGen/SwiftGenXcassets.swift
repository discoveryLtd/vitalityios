// Generated using SwiftGen, by O.Halligon — https://github.com/SwiftGen/SwiftGen

#if os(OSX)
import AppKit.NSImage
public typealias VIAScreeningsVaccinationsColor = NSColor
public typealias VIAScreeningsVaccinationsImage = NSImage
#elseif os(iOS) || os(tvOS) || os(watchOS)
import UIKit.UIImage
public typealias VIAScreeningsVaccinationsColor = UIColor
public typealias VIAScreeningsVaccinationsImage = UIImage
#endif

// swiftlint:disable file_length

public typealias VIAScreeningsVaccinationsAssetType = VIAScreeningsVaccinationsImageAsset

public struct VIAScreeningsVaccinationsImageAsset: Equatable {
	fileprivate var name: String

	public var templateImage: VIAScreeningsVaccinationsImage {
		let bundle = Bundle(for: BundleToken.self)
		#if os(iOS) || os(tvOS)
		let image = VIAScreeningsVaccinationsImage(named: name, in: bundle, compatibleWith: nil)?.withRenderingMode(.alwaysTemplate)
		#elseif os(OSX)
		let image = bundle.image(forResource: name).withRenderingMode(.alwaysTemplate)
		#elseif os(watchOS)
		let image = VIAScreeningsVaccinationsImage(named: name).withRenderingMode(.alwaysTemplate)
		#endif
		guard let result = image else { fatalError("Unable to load template image named \(name).") }
		return result
	}

	public var image: VIAScreeningsVaccinationsImage {
		let bundle = Bundle(for: BundleToken.self)
		#if os(iOS) || os(tvOS)
		let image = VIAScreeningsVaccinationsImage(named: name, in: bundle, compatibleWith: nil)
		#elseif os(OSX)
		let image = bundle.image(forResource: name)
		#elseif os(watchOS)
		let image = VIAScreeningsVaccinationsImage(named: name)
		#endif
		guard let result = image else { fatalError("Unable to load image named \(name).") }
		return result
	}

	public static func ==(lhs: VIAScreeningsVaccinationsImageAsset, rhs: VIAScreeningsVaccinationsImageAsset) -> Bool {
        return lhs.image == rhs.image
    }
}

public struct VIAScreeningsVaccinationsColorAsset {
fileprivate var name: String

#if swift(>=3.2)
@available(iOS 11.0, tvOS 11.0, watchOS 4.0, OSX 10.13, *)
var color: VIAScreeningsVaccinationsColor {
return VIAScreeningsVaccinationsColor(asset: self)
}
#endif
}

// swiftlint:disable identifier_name line_length nesting type_body_length type_name
public enum VIAScreeningsVaccinationsAsset {
  public static let savScreenings = VIAScreeningsVaccinationsImageAsset(name: "savScreenings")
  public static let savHistory = VIAScreeningsVaccinationsImageAsset(name: "savHistory")
  public static let savHealthcareBig = VIAScreeningsVaccinationsImageAsset(name: "savHealthcareBig")
  public static let samplePartner1 = VIAScreeningsVaccinationsImageAsset(name: "samplePartner1")
  public static let savLearnMore = VIAScreeningsVaccinationsImageAsset(name: "savLearnMore")
  public static let savCompletedBig = VIAScreeningsVaccinationsImageAsset(name: "savCompletedBig")
  public static let addPhoto = VIAScreeningsVaccinationsImageAsset(name: "addPhoto")
  public static let savHelp = VIAScreeningsVaccinationsImageAsset(name: "savHelp")
  public static let savCholesterolSmall = VIAScreeningsVaccinationsImageAsset(name: "savCholesterolSmall")
  public static let savVaccinationsLogo = VIAScreeningsVaccinationsImageAsset(name: "savVaccinationsLogo")
  public static let savGeneralDocsSmall = VIAScreeningsVaccinationsImageAsset(name: "savGeneralDocsSmall")
  public static let samplePartner3 = VIAScreeningsVaccinationsImageAsset(name: "samplePartner3")
  public static let samplePartner2 = VIAScreeningsVaccinationsImageAsset(name: "samplePartner2")
  public static let statusAchieved = VIAScreeningsVaccinationsImageAsset(name: "statusAchieved")
  public static let savEarnPointsBig = VIAScreeningsVaccinationsImageAsset(name: "savEarnPointsBig")
  public static let checkMarkSmall = VIAScreeningsVaccinationsImageAsset(name: "checkMarkSmall")
  public static let savPointsInfo = VIAScreeningsVaccinationsImageAsset(name: "savPointsInfo")
  public static let vhcCompletedBig = VIAScreeningsVaccinationsImageAsset(name: "vhcCompletedBig")
  public static let savCheckMarkInactive = VIAScreeningsVaccinationsImageAsset(name: "savCheckMarkInactive")
  public static let savVaccinations = VIAScreeningsVaccinationsImageAsset(name: "savVaccinations")
  public static let pointsSmall = VIAScreeningsVaccinationsImageAsset(name: "pointsSmall")
  public static let cameraLarge = VIAScreeningsVaccinationsImageAsset(name: "cameraLarge")
  public static let doctorSmall = VIAScreeningsVaccinationsImageAsset(name: "doctorSmall")
  public static let trashSmall = VIAScreeningsVaccinationsImageAsset(name: "trashSmall")

  // swiftlint:disable trailing_comma
  public static let allColors: [VIAScreeningsVaccinationsColorAsset] = [
  ]
  public static let allImages: [VIAScreeningsVaccinationsImageAsset] = [
    savScreenings,
    savHistory,
    savHealthcareBig,
    samplePartner1,
    savLearnMore,
    savCompletedBig,
    addPhoto,
    savHelp,
    savCholesterolSmall,
    savVaccinationsLogo,
    savGeneralDocsSmall,
    samplePartner3,
    samplePartner2,
    statusAchieved,
    savEarnPointsBig,
    checkMarkSmall,
    savPointsInfo,
    vhcCompletedBig,
    savCheckMarkInactive,
    savVaccinations,
    pointsSmall,
    cameraLarge,
    doctorSmall,
    trashSmall,
  ]
  // swiftlint:enable trailing_comma
  @available(*, deprecated, renamed: "allImages")
  public static let allValues: [VIAScreeningsVaccinationsAssetType] = allImages
}
// swiftlint:enable identifier_name line_length nesting type_body_length type_name

extension VIAScreeningsVaccinationsImage {
@available(iOS 1.0, tvOS 1.0, watchOS 1.0, *)
@available(OSX, deprecated,
message: "This initializer is unsafe on macOS, please use the VIAScreeningsVaccinationsImageAsset.image property")
convenience init!(asset: VIAScreeningsVaccinationsAssetType) {
#if os(iOS) || os(tvOS)
let bundle = Bundle(for: BundleToken.self)
self.init(named: asset.name, in: bundle, compatibleWith: nil)
#elseif os(OSX) || os(watchOS)
self.init(named: asset.name)
#endif
}
}

extension VIAScreeningsVaccinationsColor {
#if swift(>=3.2)
@available(iOS 11.0, tvOS 11.0, watchOS 4.0, OSX 10.13, *)
convenience init!(asset: VIAScreeningsVaccinationsColorAsset) {
let bundle = Bundle(for: BundleToken.self)
#if os(iOS) || os(tvOS)
self.init(named: asset.name, in: bundle, compatibleWith: nil)
#elseif os(OSX)
self.init(named: asset.name, bundle: bundle)
#elseif os(watchOS)
self.init(named: asset.name)
#endif
}
#endif
}

private final class BundleToken {}
