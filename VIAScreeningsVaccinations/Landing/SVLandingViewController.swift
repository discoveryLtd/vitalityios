//
//  SAVLandingViewController.swift
//  VitalityActive
//
//  Created by Michelle Oratil on 24/11/2017.
//  Copyright © 2017 Glucode. All rights reserved.
//

import VIAUIKit
import VitalityKit
import VIACommon
import SnapKit
import RealmSwift
import VIAUtilities

public class SVLandingViewController: VIATableViewController {
    
    public var viewModel = SAVLandingViewModel()
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        self.title = CommonStrings.HomeCard.CardSectionTitle365
        configureTableView()
        loadData()
        // Do any additional setup after loading the view.
    }
    
    public override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        configureAppearance()
    }
    
    public override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        //self.showVitalityAgeIfRequired()
    }
    
    public override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.hideBackButtonTitle()
    }
    
    // MARK: Setup
    func configureAppearance() {
        VIAAppearance.setGlobalTintColorToKnowYourHealth()
        navigationController?.makeNavigationBarTransparent()
    }
    
    // MARK: Show On Boarding once
    func showOnboardingIfNeeded() {
        if viewModel.shouldShowOnboarding() {
            showFirstTimeOnboarding(self)
        }
    }
    
    func configureTableView() {
        self.tableView.register(SAVHealthActionViewCell.nib(), forCellReuseIdentifier: SAVHealthActionViewCell.defaultReuseIdentifier) // Health Action ViewCell
        self.tableView.register(VIATableViewCell.nib(), forCellReuseIdentifier: VIATableViewCell.defaultReuseIdentifier)
        self.tableView.register(VIATableViewSectionHeaderFooterView.nib(), forHeaderFooterViewReuseIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier)
        self.tableView.register(SAVLandingHeaderViewCell.nib(), forCellReuseIdentifier: SAVLandingHeaderViewCell.defaultReuseIdentifier) // SAVHeader ViewCell
        self.tableView.estimatedRowHeight = 200
        self.tableView.separatorInset = UIEdgeInsets(top: 0, left: 55, bottom: 0, right: 0)
        
        self.tableView.refreshControl = UIRefreshControl()
        self.tableView.refreshControl?.tintColor = UIColor.cellSeperator()
        self.tableView.refreshControl?.addTarget(self, action: #selector(performRefresh), for: .valueChanged)
    }
    
    /*  List of Table Sections & Rows Data
     */
    
    /* Bottom Menu */
    enum Menu: Int {
        case healthcarePdf = 0
        case history = 1
        case learnMore = 2
        case help = 3
    }
    
    var menuRows: NSMutableIndexSet {
        let indexSet = NSMutableIndexSet()
        if VIAApplicableFeatures.default.showSVHistoryMenu(){
            indexSet.add(Menu.history.rawValue)
        }
        
        if VIAApplicableFeatures.default.checkForHealthcarePDFDisplay() {
            if (VitalityProductFeature.isEnabled(.AdvScreening)) || (VitalityProductFeature.isEnabled(.Screening)) {
                indexSet.add(Menu.healthcarePdf.rawValue)
            }
        }
        
        indexSet.add(Menu.learnMore.rawValue)
        
        if !VIAApplicableFeatures.default.hideHelpTab! {
            indexSet.add(Menu.help.rawValue)
        }
        
        return indexSet
    }
    
    /* Upper Menu */
//    enum HealthActionMenu: Int {
//        case screenings = 0
//        case vaccinations = 1
//
//        func text() -> String? {
//            switch self {
//            case .screenings:
//                return CommonStrings.Sv.ScreeningsTitle1012
//            case .vaccinations:
//                return CommonStrings.Sv.VaccinationsTitle1013
//            }
//        }
//    }
//
//    enum PointsValue: Int {
//        case screenings = 0
//        case vaccinations = 1
//
//        func value(_ vc: SVLandingViewController) -> String? {
//            switch self {
//            case .screenings:
//                let earnedPoints    = formatted(number: vc.getScreeningsTotalEarnedPoints())
//                let potentialPoints = formatted(number: vc.getScreeningsTotalPotentialPoints())
//                if vc.getScreeningsTotalEarnedPoints() > 0{
//                    return CommonStrings.HomeCard.PointsEarnedMessage252(earnedPoints, potentialPoints)
//                }
//                return CommonStrings.HomeCard.CardEarnUpToPointsMessage124(potentialPoints)
//            case .vaccinations:
//                let earnedPoints    = formatted(number: vc.getVaccinationsTotalEarnedPoints())
//                let potentialPoints = formatted(number: vc.getVaccinationsTotalPotentialPoints())
//                if vc.getVaccinationsTotalEarnedPoints() > 0{
//                    return CommonStrings.HomeCard.PointsEarnedMessage252(earnedPoints, potentialPoints)
//                }
//                return CommonStrings.HomeCard.CardEarnUpToPointsMessage124(potentialPoints)
//            }
//        }
//
//        private func formatted(number: Int) -> String{
//            return Localization.decimalFormatter.number(from: String(number))?.stringValue ?? ""
//        }
//    }
    
    /* Sections */
    enum SAVText: Int, EnumCollection {
        case headerCell = 0
        case headerMesssage = 1
        case healthActionCell = 2
        
        
        func text() -> String? {
            switch self {
            case .headerCell:
                return CommonStrings.Sv.LandingHealthActionTitle1008
            case .headerMesssage:
                return VIAApplicableFeatures.default.getSVLandingHeaderMessage()
            case .healthActionCell:
                return CommonStrings.Sv.LandingSectionHeading1011.localizedUppercase
            }
        }
    }
    
    enum Sections: Int {
        case headerCell = 0
        case healthActionCell = 1
        case menuCell = 2
    }    
    
    /*  API Call
     */
    
    func performRefresh() {
        loadData()
        self.tableView.refreshControl?.endRefreshing()
    }
    
    func loadData(forceUpdate: Bool = false) {
        showHUDOnView(view: self.view)
        viewModel.fetchScreeningAndVaccinationAttributes(forceUpdate: forceUpdate) { [weak self] error, isCacheOutdated in
            self?.showOnboardingIfNeeded()
            self?.hideHUDFromView(view: self?.view)
            self?.tableView.refreshControl?.endRefreshing()
            guard error == nil else {
                self?.handleErrorOccurred(error!, isCacheOutdated: isCacheOutdated)
                return
            }
            self?.removeStatusView()
            self?.tableView.reloadData()
        }
    }
    
    func handleErrorOccurred(_ error: Error, isCacheOutdated: Bool?) {
        let backendError = error as? BackendError ?? BackendError.other
        if let outdated = isCacheOutdated, outdated == true {
            let statusView = self.handleBackendErrorWithStatusView(backendError, tryAgainAction: { [weak self] in
                self?.loadData(forceUpdate: true)
            })
            configureStatusView(statusView)
        } else {
            self.handleBackendErrorWithAlert(backendError, tryAgainAction: { [weak self] in
                self?.loadData(forceUpdate: true)
            })
        }
    }
    
    public override func configureStatusView(_ view: UIView?) {
        removeStatusView()
        
        guard let view = view else { return }
        
        self.view.addSubview(view)
        
        view.snp.makeConstraints { (make) in
            make.width.equalToSuperview()
            make.left.right.top.equalToSuperview()
            make.height.equalToSuperview().offset(self.tableView.contentOffset.y)
        }
    }
    
    public override func removeStatusView() {
        self.view.subviews.forEach { (view) in
            if view is VIAStatusView {
                view.removeFromSuperview()
            }
        }
    }
    
    // MARK: - UITableView DataSource + Delegate
    
    public override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let sectionValue = Sections(rawValue: indexPath.section)
        if sectionValue == .healthActionCell {
            if let healthActionRow = HealthActionMenu(rawValue: indexPath.row){
                /**
                 * if row == Screenings and Screenings Total Potentials Points, collapse the row
                 */
                if healthActionRow == HealthActionMenu.screenings
                && getScreeningsTotalPotentialPoints() <= 0{
                    return 0
                }
                /**
                 * if row == Vaccinations and Vaccinations Total Potentials Points, collapse the row
                 */
                if healthActionRow == HealthActionMenu.vaccinations
                && getVaccinationsTotalPotentialPoints() <= 0{
                    return 0
                }
            }
        }
        return UITableViewAutomaticDimension
    }
    
    public override func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    public override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let sectionValue = Sections(rawValue: section)
        if sectionValue == .headerCell {
            return 1
        } else if sectionValue == .healthActionCell {
            if viewModel.landingCellsGroupData.count > 0 {
                return 2
            } else {
                return 0
            }
        } else {
            return menuRows.count
        }
    }
    
    public override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let sectionValue = Sections(rawValue: section)
        if sectionValue == .headerCell && viewModel.landingCellsGroupData.count > 0 {
            let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier) as? VIATableViewSectionHeaderFooterView
            return view
        } else {
            return nil
        }
    }
    
    public override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let sectionValue = Sections(rawValue: section)
        if sectionValue == .healthActionCell && viewModel.landingCellsGroupData.count > 0 {
            let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier) as? VIATableViewSectionHeaderFooterView
            view?.labelText = SAVText.healthActionCell.text()
            return view
        } else {
            return nil
        }
    }
    
    public override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        let sectionValue = Sections(rawValue: section)
        if sectionValue == .headerCell && viewModel.landingCellsGroupData.count > 0 {
            return 20.0
        }
        return UITableViewAutomaticDimension
    }
    
    public override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        let sectionValue = Sections(rawValue: section)
        if sectionValue == .headerCell {
            return CGFloat.leastNonzeroMagnitude
        }
        return UITableViewAutomaticDimension
    }
    
    public override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let sectionValue = Sections(rawValue: indexPath.section)
        
        if sectionValue == .headerCell {
            return headerViewCell(at: indexPath)
        } else if sectionValue == .healthActionCell {
            return healthActionCell(at: indexPath)
        } else {
            return regularCell(at: indexPath)
        }
    }
    
    public override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let sectionValue = Sections(rawValue: indexPath.section)
        
        if sectionValue == .healthActionCell {
            measurementTapped()
        } else if sectionValue == .menuCell {
            if let menuItem = Menu(rawValue: menuRows.integer(at: indexPath.row)) {
                switch menuItem {
                case .history:
                    historyTapped()
                case .learnMore:
                    learnMoreTapped()
                case .help:
                    helpTapped()
                case .healthcarePdf:
                    pdfTapped()
                }
            }
        }
    }
    
    func headerViewCell(at indexPath: IndexPath) -> SAVLandingHeaderViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: SAVLandingHeaderViewCell.defaultReuseIdentifier, for: indexPath) as! SAVLandingHeaderViewCell
        cell.title = SAVText.headerCell.text()
        cell.message = SAVText.headerMesssage.text()
        cell.button.setTitle(CommonStrings.Sv.LandingSubmitButton1010, for: .normal)
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        
        // MARK: Confirm and Submit button
        
        cell.didTapButton = { [weak self] in
            self?.captureResultsTapped()
        }
        
        
        return cell
    }
    
    func healthActionCell(at indexPath: IndexPath) -> SAVHealthActionViewCell {
        return VIAApplicableFeatures.default.getSVHealthActionCell(at: indexPath, tableView: tableView,
                                                                   screeningsTotalPotentialPoints: getScreeningsTotalPotentialPoints(),
                                                                   screeningsTotalEarnedPoints: getScreeningsTotalEarnedPoints(),
                                                                   vaccinationsTotalPotentialPoints: getVaccinationsTotalPotentialPoints(),
                                                                   vaccinationsTotalEarnedPoints: getVaccinationsTotalEarnedPoints())
    }
    
    func regularCell(at indexPath: IndexPath) -> VIATableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: VIATableViewCell.defaultReuseIdentifier, for: indexPath) as! VIATableViewCell
        
        if let menuItem = Menu(rawValue: menuRows.integer(at: indexPath.row)) {
            switch menuItem {
            case .history:
                cell.labelText = CommonStrings.HistoryButton140
                cell.cellImage = VIAScreeningsVaccinationsAsset.savHistory.templateImage
            case .learnMore:
                cell.labelText = CommonStrings.LearnMoreButtonTitle104
                cell.cellImage = VIAScreeningsVaccinationsAsset.savLearnMore.templateImage
            case .help:
                cell.labelText = CommonStrings.HelpButtonTitle141
                cell.cellImage = VIAScreeningsVaccinationsAsset.savHelp.templateImage
            case .healthcarePdf:
                cell.labelText = CommonStrings.Sv.LandingHealthcarePdf1040
                cell.cellImage = VIAScreeningsVaccinationsAsset.savGeneralDocsSmall.templateImage
            }
        }
        
        cell.cellImageView.tintColor = UIColor.currentGlobalTintColor()
        cell.accessoryType = .disclosureIndicator
        return cell
    }
    
    // MARK: - Navigation
    fileprivate let SEGUE_LEARN_MORE        = "segueLearnMore"
    fileprivate let SEGUE_CAPTURE_RESULTS   = "segueCaptureResults"
    fileprivate let SEGUE_HISTORY           = "segueHistory"
    fileprivate let SEGUE_HELP              = "segueHelp"
    fileprivate let SEGUE_LIST              = "showList"
    fileprivate let SEGUE_ONBOARDING        = "showOnboardingSegue"
    fileprivate let SEGUE_PDF               = "segueHealthcarePdf"
    
    func captureResultsTapped() {
        self.performSegue(withIdentifier: SEGUE_CAPTURE_RESULTS, sender: self)
    }
    
    func learnMoreTapped() {
        self.performSegue(withIdentifier: SEGUE_LEARN_MORE, sender: self)
    }
    
    func historyTapped() {
        self.performSegue(withIdentifier: SEGUE_HISTORY, sender: self)
    }
    
    func helpTapped() {
        self.performSegue(withIdentifier: SEGUE_HELP, sender: self)
    }
    
    func measurementTapped() {
        self.performSegue(withIdentifier: SEGUE_LIST, sender: self)
    }
    
    func pdfTapped() {
        self.performSegue(withIdentifier: SEGUE_PDF, sender: self)
    }
    
    public override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == SEGUE_LEARN_MORE {
            if let vc = segue.destination as? SVLearnMoreViewController {
                vc.landingCellsGroupData = self.viewModel.landingCellsGroupData
            }
        } else if segue.identifier == SEGUE_LIST {
            if let row = self.tableView.indexPathForSelectedRow?.row,
                let rowValue = HealthActionMenu(rawValue: row) {
                
                if let vc = segue.destination as? SVListViewController {
                    
                    if rowValue == .screenings {
                        vc.landingCellsGroupData    = getScreenings()
                        vc.title                    = CommonStrings.Sv.ScreeningsTitle1012
                    } else if rowValue == .vaccinations {
                        vc.landingCellsGroupData    = getVaccinations()
                        vc.title                    = CommonStrings.Sv.VaccinationsTitle1013
                    }
                }
            }
        } else if segue.identifier == SEGUE_CAPTURE_RESULTS {
            if let nc = segue.destination as? UINavigationController,
                let vc = nc.topViewController as? SVHealthActionViewController {
                vc.screeningsVaccinationsActions = getDataForCapture()
            }
        } else if segue.identifier == SEGUE_PDF, let webContentViewController = segue.destination as? VIAWebContentViewController {
            webContentViewController.showNavBar = true
            webContentViewController.title = CommonStrings.LandingScreen.HealthcarePdfLabel248
            let pdfFilename = AppConfigFeature.getFileName(type: VIAApplicableFeatures.default.getHealthCarePDFTypeKey())
            let webContenViewModel = VIAWebContentViewModel(pdfFilename: pdfFilename)
            webContentViewController.viewModel = webContenViewModel
        }
    }
    
    
    @IBAction func showFirstTimeOnboarding(_ sender: Any) {
        self.performSegue(withIdentifier: SEGUE_ONBOARDING, sender: self)
    }
    
    @IBAction func unwindToSAVLanding(segue: UIStoryboardSegue) {
        debugPrint("unwindToSAVLanding")
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

