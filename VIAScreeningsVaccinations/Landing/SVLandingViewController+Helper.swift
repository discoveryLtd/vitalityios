//
//  SVLandingViewController+Helper.swift
//  VitalityActive
//
//  Created by OJ Garde on 2/15/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import VIAUtilities
import VitalityKit

// MARK: Total Potential Points
extension SVLandingViewController{
    
    /**
     * getScreeningsTotalPotentialPoints
     *
     * returns Total Potential Points for Screenings
     */
    public func getScreeningsTotalPotentialPoints() -> Int{
        return getPotentialPoints(for: EventCategoryRef.Screenings)
    }
    
    /**
     * getVaccinationsTotalPotentialPoints
     *
     * returns Total Potential Points for Vaccinations
     */
    public func getVaccinationsTotalPotentialPoints() -> Int{
        return getPotentialPoints(for: EventCategoryRef.Vaccinations)
    }
    
    private func getPotentialPoints(for categoryType: EventCategoryRef) -> Int{
        /**
         * Initial and Default value for points is 0
         */
        var points = 0
        for data in viewModel.landingCellsGroupData{
            /**
             * If Event Potential Points is greater than 0 and event type is valid,
             * add the potential points to our holder.
             */
            if data.potentialPoints > 0 && data.categoryType == categoryType {
                /** FC-24405 : Generali Germany
                 * Control the Potential Points that is being displayed
                 */
                if let tenantID = AppSettings.getAppTenant(), tenantID == .GI {
                    /* Limit the maximum Potential Points to 2000 */
                    if (points + data.potentialPoints) < 2000 {
                        points += data.potentialPoints
                    } else {
                        points = 2000
                    }
                } else {
                    points += data.potentialPoints
                }
            }
        }
        
        return points
    }        
}


// MARK: Total Earned Points
extension SVLandingViewController{
    
    /**
     * getScreeningsTotalEarnedPoints
     *
     * returns Total Earned Points for Screenings
     */
    public func getScreeningsTotalEarnedPoints() -> Int{
        return getEarnPoints(for: EventCategoryRef.Screenings)
    }
    
    /**
     * getVaccinationsTotalEarnedPoints
     *
     * returns Total Potential Points for Vaccinations
     */
    public func getVaccinationsTotalEarnedPoints() -> Int{
        return getEarnPoints(for: EventCategoryRef.Vaccinations)
    }

    private func getEarnPoints(for categoryType: EventCategoryRef) -> Int{
        /**
         * Initial and Default value for points is 0
         */
        var points = 0
        for data in viewModel.landingCellsGroupData{
            /**
             * If Event Earned Points is greater than 0 and event type is valid,
             * add the earned points to our holder.
             */
            if data.earnedPoints > 0 && data.categoryType == categoryType {
                /** FC-24405 : Generali Germany
                 * Control the Earn Points that is being displayed
                 */
                if let tenantID = AppSettings.getAppTenant(), tenantID == .GI {
                    /* Limit the maximum Earn Points to 2000 */
                    if (points + data.earnedPoints) < 2000 {
                        points += data.earnedPoints
                    } else {
                        points = 2000
                    }
                } else {
                    points += data.earnedPoints
                }
            }
        }
        
        return points
    }
}

// MARK: Landing Page
extension SVLandingViewController{
    
    public func getScreenings() -> [SAVLandingCellData]{
        return getList(for: EventCategoryRef.Screenings)
    }
    
    public func getVaccinations() -> [SAVLandingCellData]{
        return getList(for: EventCategoryRef.Vaccinations)
    }
    
    private func getList(for categoryType:EventCategoryRef) -> [SAVLandingCellData]{
        
        var withPoints      = [SAVLandingCellData]()
        var withoutPoints   = [SAVLandingCellData]()
        
        for data in viewModel.landingCellsGroupData{
            /**
             * If Event Potential Points is greater than 0 and event type is valid,
             * add the potential points to our holder.
             */
            if data.potentialPoints > 0 && data.categoryType == categoryType{
                /* Separate containers for with Points and without Points */
                /* All items without points will go first in alphabetical order */
                /* Followed by items with points in alphabetical order */
                if data.earnedPoints > 0{
                    withPoints.append(data)
                }else{
                    withoutPoints.append(data)
                }
            }
        }
        
        /* Sort in localized alphabetical order */
        withPoints      = withPoints.sorted { $0.groupName.localizedCompare($1.groupName) == .orderedAscending }
        
        /* Sort in localized alphabetical order */
        withoutPoints   = withoutPoints.sorted { $0.groupName.localizedCompare($1.groupName) == .orderedAscending }
        
        /* Concat 2 containers and return */
        return withoutPoints + withPoints
    }
    
}

// MARK: Capture Results
extension SVLandingViewController{
    
    public func getDataForCapture() -> [HealthActionsHolder]{
        var screenings      = getScreeningsForCapture().sorted { $0.typeName.localizedCompare($1.typeName) == .orderedAscending }
        if screenings.count > 0{
            screenings[0].displayHeader = true
        }
        
        var vaccinations    = getVaccinationsForCapture().sorted { $0.typeName.localizedCompare($1.typeName) == .orderedAscending }
        if vaccinations.count > 0{
            vaccinations[0].displayHeader = true
        }
        
        return screenings + vaccinations
    }
    
    private func getScreeningsForCapture() -> [HealthActionsHolder]{
        return getListForCapture(for: EventTypeRef.configuredTypesforSAVScreenings(), featureType: .Screenings)
    }
    
    private func getVaccinationsForCapture() -> [HealthActionsHolder]{
        return getListForCapture(for: EventTypeRef.configuredTypesforSAVVaccinations(), featureType: .Vaccinations)
    }
    
    private func getListForCapture(for events:[EventTypeRef], featureType: ProductFeatureTypeRef) -> [HealthActionsHolder]{
        /**
         * Initial and Default value for points is 0
         */
        var temp = [HealthActionsHolder]()
        for data in viewModel.landingCellsGroupData{
            /**
             * If Event Potential Points is greater than 0 and event type is valid,
             * add the potential points to our holder.
             */
            if data.potentialPoints > 0 && events.contains(data.eventType){
                
                temp.append(HealthActionsHolder(typeName:       data.groupName,
                                                eventType:      data.eventType,
                                                featureType:    featureType,
                                                testedDate:     Date(),
                                                selected:       false,
                                                displayHeader:  false,
                                                showDatePicker: false))
            }
        }
        
        return temp
    }
}
