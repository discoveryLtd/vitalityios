//
//  SAVLandingViewModel.swift
//  VitalityActive
//
//  Created by Michelle Oratil on 01/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.
//

import VitalityKit
import VIAUIKit
import VIAUtilities
import RealmSwift

public class SAVLandingViewModel {
    
    public init(){}
    
    // MARK: Properties
    let dataController = SAVLandingDataController()
    public var landingCellsGroupData = [SAVLandingCellData]()
    
    // MARK: Functions
    
    public func shouldShowOnboarding() -> Bool {
        return !AppSettings.hasShownSAVOnboarding()
    }
    
    public func getCellDataFromDataController() {
        landingCellsGroupData.removeAll()
        landingCellsGroupData = dataController.dataForGroupsFromRealm()
        
//      Sort data in alphabetical order
        sortlandingCellsGroupDataAlphabetical()
    }
    
    public func sortlandingCellsGroupDataAlphabetical(){
        landingCellsGroupData = landingCellsGroupData.sorted { $0.groupName.localizedCompare($1.groupName) == .orderedAscending }
    }
    
    public func fetchScreeningAndVaccinationAttributes(forceUpdate: Bool = false, completion: @escaping (Error?, Bool?) -> Void) {
        if SAVCache.isSAVDataOutdated() || forceUpdate == true {
            
            let partyId = DataProvider.newRealm().getPartyId()
            let tenantId = DataProvider.newRealm().getTenantId()
            let eventTypes = EventTypeRef.configuredTypesForSAV()
            let uniqueEventTypes = Array(Set(eventTypes))
            let membershipId = DataProvider.newRealm().getMembershipId()
            Wire.Events.getSAVPotentialPointsAndEventsCompletedPoints(tenantId: tenantId, partyId: partyId, eventTypes: uniqueEventTypes, vitalityMembershipId: membershipId) { (error) in
                guard error == nil else {
                    completion(error, nil)
                    return
                }
                self.getCellDataFromDataController()
                completion(nil, nil)
            }
        } else {
            getCellDataFromDataController()
            completion(nil, nil)
        }
    }
    
    
    // MARK: Networking - Screening
    public func fetchScreeningsAttributes(forceUpdate: Bool = false, completion: @escaping (Error?, Bool?) -> Void) {
        if SAVCache.isSAVDataOutdated() || forceUpdate == true {
            let partyId = DataProvider.newRealm().getPartyId()
            let tenantId = DataProvider.newRealm().getTenantId()
            let eventTypes = EventTypeRef.configuredTypesforSAVScreenings()
            let membershipId = DataProvider.newRealm().getMembershipId()
            Wire.Events.getSAVPotentialPointsAndEventsCompletedPoints(tenantId: tenantId, partyId: partyId, eventTypes: eventTypes, vitalityMembershipId: membershipId) { (error) in
                guard error == nil else {
                    completion(error, nil)
                    return
                }
                self.getCellDataFromDataController()
                completion(nil, nil)
            }
        } else {
            getCellDataFromDataController()
            completion(nil, nil)
        }
    }
    
    // MARK: Networking - Vaccinations
    public func fetchVaccinationsAttributes(forceUpdate: Bool = false, completion: @escaping (Error?, Bool?) -> Void) {
        if SAVCache.isSAVDataOutdated() || forceUpdate == true {
            let partyId = DataProvider.newRealm().getPartyId()
            let tenantId = DataProvider.newRealm().getTenantId()
            let eventTypes = EventTypeRef.configuredTypesforSAVVaccinations()
            let membershipId = DataProvider.newRealm().getMembershipId()
            Wire.Events.getSAVPotentialPointsAndEventsCompletedPoints(tenantId: tenantId, partyId: partyId, eventTypes: eventTypes, vitalityMembershipId: membershipId) { (error) in
                guard error == nil else {
                    completion(error, nil)
                    return
                }
                self.getCellDataFromDataController()
                completion(nil, nil)
            }
        } else {
            getCellDataFromDataController()
            completion(nil, nil)
        }
    }
    
    
}
