//
//  SAVLandingDataController.swift
//  VitalityActive
//
//  Created by Michelle Oratil on 01/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import RealmSwift
import VitalityKit
import VIAUIKit
import VIAUtilities

public struct SAVLandingCellData {
    var groupName: String
    var message: String?
    var potentialPoints: Int = 0
    var earnedPoints: Int = 0
    var eventType: EventTypeRef = .Unknown
    var categoryType: EventCategoryRef = .Unknown
    var hasPointsEvents: Bool
}

public class SAVLandingDataController {
    var savRealm = DataProvider.newSAVRealm()
    var coreRealm = DataProvider.newRealm()
    
    public func dataForGroupsFromRealm() -> [SAVLandingCellData] {
        savRealm.refresh()
        
        
        var allGroupsData = [SAVLandingCellData]()
        let savPoints = savRealm.allSAVPotentialPointsEventTypes()
//        let productFeatures = coreRealm.allProductFeatures()
        for points in savPoints{
            
            if points.totalPotentialPoints != 0 {
                var message = CommonStrings.Status.PointsIndicationMessage829(String(points.totalPotentialPoints))
                var hasPointEvents = false
                if points.totalEarnedPoints > 0{
                    message = CommonStrings.Status.PointsEarnedIndicationMessage896(String(points.totalPotentialPoints))
                } else {
                    if !points.events.isEmpty {
                        message = CommonStrings.Points.NoPointsEarnedTitle194
                        hasPointEvents = !points.events.isEmpty
                    }
                }
//                for productFeature in productFeatures {
//                    if productFeature.typeCode == points.typeName {
                        let dataCell = SAVLandingCellData(groupName: points.typeName,
                                                          message: message,
                                                          potentialPoints: points.totalPotentialPoints,
                                                          earnedPoints: points.totalEarnedPoints,
                                                          eventType: points.type,
                                                          categoryType: points.categoryType, hasPointsEvents: hasPointEvents)
                        allGroupsData.append(dataCell)
//                    }
//                }
            }
        }
        return allGroupsData
    }
    
    public func allScreeningsAndVaccinations() -> [SAVLandingCellData] {
        savRealm.refresh()
        
        
        var allGroupsData = [SAVLandingCellData]()
        let productFeatures = coreRealm.allProductFeatures()
        for feature in productFeatures{
            if let eventType = feature.eventType(){
                if EventTypeRef.configuredTypesforSAVScreenings().contains(eventType)
                    || EventTypeRef.configuredTypesforSAVVaccinations().contains(eventType){
                    let dataCell = SAVLandingCellData(groupName: feature.typeName,
                                                                message: "",
                                                                  potentialPoints: 0,
                                                                  earnedPoints: 0,
                                                                  eventType: eventType,
                                                                  categoryType: feature.eventCategoryType(), hasPointsEvents: false)
                                allGroupsData.append(dataCell)
                }
            }
        }
        
        return allGroupsData
    }
}
