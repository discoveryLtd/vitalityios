//
//  SVListViewController.swift
//  VitalityActive
//
//  Created by OJ Garde on 2/15/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import VIAUIKit
import VitalityKit
import VIACommon
import VIAUtilities

import SnapKit
import RealmSwift

class SVListViewController: VIATableViewController {
    
    public var landingCellsGroupData = [SAVLandingCellData]()
    
    override func viewDidLoad() {
        super.viewDidLoad()        
        
        self.configureTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.configureAppearance()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.tableView.reloadData()
    }
    
    // MARK: Setup
    func configureAppearance() {
        
        navigationController?.makeNavigationBarTransparent()
    }
    
    func configureTableView() {
        self.tableView.register(SAVHealthActionViewCell.nib(), forCellReuseIdentifier: SAVHealthActionViewCell.defaultReuseIdentifier)
        self.tableView.register(VIATableViewCell.nib(), forCellReuseIdentifier: VIATableViewCell.defaultReuseIdentifier)
        self.tableView.register(VIATableViewSectionHeaderFooterView.nib(), forHeaderFooterViewReuseIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier)
        self.tableView.estimatedRowHeight = 75
    }
    
    // MARK: - UITableView DataSource + Delegate
    
    override public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return landingCellsGroupData.count
    }
    
    override public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier) as? VIATableViewSectionHeaderFooterView
        view?.labelText = "\n" + CommonStrings.Sv.ScreeningsDetailMessage1014 + "\n"
        return view
    }
    
    public override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    override public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return healthActionCell(at: indexPath)
    }
    
    func healthActionCell(at indexPath: IndexPath) -> SAVHealthActionViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: SAVHealthActionViewCell.defaultReuseIdentifier, for: indexPath) as! SAVHealthActionViewCell
        
        cell.accessoryType = .none
        cell.selectionStyle = .none
        
        let data        = landingCellsGroupData[indexPath.row]
        let title       = data.groupName
        let message     = data.message
        let completed   = data.earnedPoints > 0
        let hasPointsEvents = data.hasPointsEvents
        
        cell.isUserInteractionEnabled = true
        cell.configureSAVHealthActionMainCell(image: nil,
                                              measurableTitle: title,
                                              messageIcon: VIAScreeningsVaccinationsAsset.savPointsInfo.image,
                                              message: message,
                                              footerMessage: nil,
                                              isDisclosureIndicatorHidden: true,
                                              completed: completed,
                                              hasPointsEvents: hasPointsEvents)
        cell.layoutIfNeeded()
        return cell
        
    }
    
    override func configureStatusView(_ view: UIView?) {
        removeStatusView()
        
        guard let view = view else { return }
        
        self.view.addSubview(view)
        
        view.snp.makeConstraints { (make) in
            make.width.equalToSuperview()
            make.left.right.top.equalToSuperview()
            make.height.equalToSuperview().offset(self.tableView.contentOffset.y)
        }
    }
    
    override func removeStatusView() {
        self.view.subviews.forEach { (view) in
            if view is VIAStatusView {
                view.removeFromSuperview()
            }
        }
    }
}

