//
//  SVSubmissionHelper.swift
//  VitalityActive
//
//  Created by Val Tomol on 11/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.
//

import UIKit
import Foundation
import VIAUtilities
import VitalityKit
import Photos
import VIAImageManager

public protocol SVPhotoUploadDelegate: class {
    func displayImageUploadErrorMessage(with completion: @escaping (_ resubmit: Bool) -> Void)
}

protocol SVEventsProcessor {
    func processEvents(completion: @escaping (_ error: Error?) -> Void)
}

class SVSubmissionHelper: SVEventsProcessor {
    internal weak var photoUploadDelegate: SVPhotoUploadDelegate?
    internal let imagesRealm = DataProvider.newSAVRealm()
    internal let savRealm = DataProvider.newSAVRealm()
    internal let photoUploader = PhotoUploader()
    private var completion: ((Error?) -> (Void)) = { error in
        debugPrint("Complete")
    }
    
    func setupDelegate(_ delegate: SVPhotoUploadDelegate) {
        photoUploadDelegate = delegate
    }
    
    func processEvents(completion: @escaping (_ error: Error?) -> Void) {
        self.completion = completion
        submitImagesRetreiveReferences()
    }
    
    internal func submitImagesRetreiveReferences() {
        guard let realmCapturedAssets = imagesRealm.allPhotos() else {
            debugPrint("No images to upload")
            return
        }
        
        photoUploader.delegate = self
        photoUploader.submitCapturedVHCImages(realmCapturedAssets)
    }
    
    internal func submitEvents(with associateEventReferences: Array<String>?) {
        let coreRealm = DataProvider.newRealm()
        let tenantID = coreRealm.getTenantId()
        let partyID = coreRealm.getPartyId()
        let submissionDate = Date()
        var associatedEvents = [ProcessEventsAssociatedEvent]()
        
        
        for capturedResult in savRealm.allSAVItems() {
            
            let associatedEvent = ProcessEventsAssociatedEvent(eventOccuredOn: capturedResult.testedDate!,
                                                                           eventCapturedOn: submissionDate,
                                                                           eventMetaDataTypeKey: EventMetaDataTypeRef.Unknown,
                                                                           eventMetaDataUnitOfMeasure:UnitOfMeasureRef.Unknown,
                                                                           eventMetaDataValue: "",
                                                                           associatedEventTypeKey: .DocumentaryProof,
                                                                           eventSourceTypeKey: .MobileApp,
                                                                           partyId: partyID,
                                                                           reportedBy: partyID,
                                                                           eventId: nil,
                                                                           typeKey: capturedResult.eventType)
            associatedEvents.append(associatedEvent)
        }
        
        let processEvent = ProcessEventsEvent(date: submissionDate,
                                              eventTypeKey: .DocumentUploadsSV,
                                              eventSourceTypeKey: .MobileApp,
                                              partyId: partyID,
                                              reportedBy: partyID,
                                              associatedEvents: associatedEvents,
                                              eventMetaDataTypeKey: EventMetaDataTypeRef.DocumentReference,
                                              eventMetaDataUnitOfMeasure: nil,
                                              eventMetaDataValues: associateEventReferences)
        
        var events = [ProcessEventsEvent]()
        events.append(processEvent)
        
        Wire.Events.processEvents(tenantId: tenantID, events: events) { [weak self] (response, error) in
                self?.completion(error)
        }
    }
}

extension SVSubmissionHelper: PhotoUploaderDelegate{
    
    func imagesUploaded(references: Array<String>) {
        submitEvents(with: references)
    }
    
    func onImageUploadFailed() {
        self.photoUploadDelegate?.displayImageUploadErrorMessage(with: { [weak self] tryAgain in
            if tryAgain {
                self?.photoUploader.reSubmitFailedImages()
            }
        })
    }
}


