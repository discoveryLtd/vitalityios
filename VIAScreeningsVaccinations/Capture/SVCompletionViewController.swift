//
//  SVCompletionViewController.swift
//  VitalityActive
//
//  Created by Val Tomol on 05/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import UIKit
import VIAUIKit
import VitalityKit
import VIAUtilities
import VIACommon
import RealmSwift

class SVCompletionViewController: VIACirclesViewController {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.viewModel = VIAApplicableFeatures.default.getSVCompletionViewModel() as? CirclesViewModel
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
        if let svViewModel = self.viewModel as? SVGenericCompletionViewModel {
            svViewModel.finaliseSubmission()
        }
    }
    
    override func buttonTapped(_ sender: UIButton) {
        // TODO: Place IBAction function to landing page.
        //self.performSegue(withIdentifier: "unwindToSVLanding", sender: nil)
        /*
        @IBAction public func unwindToSVLanding(segue: UIStoryboardSegue) {
            debugPrint("unwindToSVLanding")
        }
        */
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
}
