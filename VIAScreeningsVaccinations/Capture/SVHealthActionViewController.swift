import VIAUIKit
import VIAUtilities
import VitalityKit
import VIACommon


public struct HealthActionsHolder{
    var typeName: String = ""
    var eventType: EventTypeRef = EventTypeRef.Unknown
    var featureType: ProductFeatureTypeRef = ProductFeatureTypeRef.Unknown
    var testedDate: Date? = nil
    var selected:Bool = false
    var displayHeader: Bool = false
    var showDatePicker: Bool = false
}

class SVHealthActionViewController: VIATableViewController, KnowYourHealthTintable, SVSelectedPhotosCollectionViewControllerProtocol, DatePickerDelegate {
    // MARK: Properties
    
    //Holder of add Proof Images
    var imagesInfo: Array<Dictionary<String, Any>> = []
    
    public var screeningsVaccinationsActions: [HealthActionsHolder] = []
    
    lazy var formatter: DateFormatter = {
        let dateFormatter = DateFormatter.dayDateShortMonthyearFormatter ()
        return dateFormatter
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureTableView()
        
        self.title = CommonStrings.Sv.LandingSubmitButton1010
        
        configureAppearance()
        
        tableView.sizeHeaderViewToFit()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.tableView.reloadData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        self.hideBackButtonTitle()
        
    }
    
    // Update imagesInfo variable based on the changes in SVImageDetailViewController, for persistent
    func updateAddProofImagesInfo(imagesInfo: Array<Dictionary<String, Any>>){
        self.imagesInfo = imagesInfo
    }
    
    func sortHealthActionsHolderAlphabetical(healthActionHolder: [HealthActionsHolder]) -> [HealthActionsHolder]{
        return healthActionHolder.sorted { $0.typeName.localizedCompare($1.typeName) == .orderedAscending }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // Mark: UITableViewDataSource
    // We cannot move this to extension due to Xcode restriction
    override func numberOfSections(in tableView: UITableView) -> Int {
        return screeningsVaccinationsActions.count
        
    }
    
    // Mark: UITableViewDataSource
    // We cannot move this to extension due to Xcode restriction
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
}

// MARK: Section enum
extension SVHealthActionViewController{
    
    enum Sections: Int, EnumCollection {
        case unknown = -1
        case screenings = 0
        case vaccinations = 1
        
        func sectionHeaderTitle() -> String? {
            switch self {
            case .screenings:
                return CommonStrings.Sv.ScreeningsTitle1012
            case .vaccinations:
                return CommonStrings.Sv.VaccinationsTitle1013
            case .unknown:
                return ""
            }
        }
        
        func sectionHeaderDescription() -> String? {
            switch self {
            case .screenings:
                return ""
            case .vaccinations:
                return CommonStrings.Sv.ConfirmSubmitVaccinationsMessage1017
            case .unknown:
                return ""
            }
        }
        
    }
}


// MARK: View Configuration
extension SVHealthActionViewController{
    
    fileprivate func configureAppearance() {
        navigationController?.makeNavigationBarTransparent()
        self.addLeftBarButtonItem(CommonStrings.CancelButtonTitle24, target: self, selector: #selector(onCancel))
        self.addRightBarButtonItem(CommonStrings.NextButtonTitle84, target: self, selector: #selector(onNext))
        
        self.navigationItem.rightBarButtonItem?.isEnabled = false
    }
    
    func onCancel(sender:UIBarButtonItem){
        //self.navigationController?.popViewController(animated: true)
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    
    func onNext(sender:UIBarButtonItem){
        //Save only the selected screenings and vaccinations
        
        let realmList = DataProvider.newSAVRealm()
        
        try! realmList.write {
            realmList.delete(realmList.allSAVItems())
            
            for action in screeningsVaccinationsActions{
                if action.selected && action.testedDate != nil{
                    
                    // Map action values to SAVSelectedItem
                    let sav = SAVSelectedItem()
                    sav.eventType   = action.eventType
                    sav.featureType = action.featureType
                    sav.testedDate  = action.testedDate
                    sav.typeName    = action.typeName

                    // Add sav to local database
                    realmList.add(sav)
                }
            }
        }
        
        self.performSegue(withIdentifier: "showAddProof", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Pass the updated imagesInfo to the SelectedPhotosCollectionViewController, for persistence
        let svSelectedPhotosCollectionViewController = segue.destination as! SVSelectedPhotosCollectionViewController
        svSelectedPhotosCollectionViewController.delegate = self
        svSelectedPhotosCollectionViewController.imagesInfo = self.imagesInfo
    }
    
    fileprivate func configureTableView() {
        self.tableView.register(VIALeftDetailTableViewCell.nib(),
                                forCellReuseIdentifier: VIALeftDetailTableViewCell.defaultReuseIdentifier)
        self.tableView.register(SVRadioButtonCell.nib(),
                                forCellReuseIdentifier: SVRadioButtonCell.defaultReuseIdentifier)
        self.tableView.register(SVDatePickerCell.nib(),
                                forCellReuseIdentifier: SVDatePickerCell.defaultReuseIdentifier)
        self.tableView.register(VIATableViewCell.nib(),
                                forCellReuseIdentifier: VIATableViewCell.defaultReuseIdentifier)
        self.tableView.register(TitleImageDescriptionHeaderView.nib(),
                                forHeaderFooterViewReuseIdentifier: TitleImageDescriptionHeaderView.defaultReuseIdentifier)
        self.tableView.register(VIADatePickerTableViewCell.nib(),
                                forCellReuseIdentifier: VIADatePickerTableViewCell.defaultReuseIdentifier)
        self.tableView.estimatedRowHeight = 100
        self.tableView.sectionHeaderHeight = UITableViewAutomaticDimension
        self.tableView.estimatedSectionHeaderHeight = 500
        
        // add extra inset for the first cell's header
        self.tableView.contentInset = UIEdgeInsets(top: 10, left: 0, bottom: 0, right: 0)
        let view = TableViewHeaderView.viewFromNib(owner: self)!
        view.header = CommonStrings.Sv.ConfirmSubmitIntroTitle1015
        view.content = CommonStrings.Sv.ConfirmSubmitIntroMessage1016
        tableView.tableHeaderView = view
    }
}


// MARK: UITableViewDataSource
extension SVHealthActionViewController {
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            let data = screeningsVaccinationsActions[indexPath.section]
            
            if !data.selected {
                if VIAApplicableFeatures.default.withDefaultStringValueForSVTestedDate() {
                    screeningsVaccinationsActions[indexPath.section].testedDate = NSDate.distantPast
                } else {
                    screeningsVaccinationsActions[indexPath.section].testedDate = Date()
                }
            } else {
                screeningsVaccinationsActions[indexPath.section].showDatePicker = false
            }
    
            screeningsVaccinationsActions[indexPath.section].selected = !data.selected
            manipulateRightBarButton()
        } else if indexPath.row == 1 {
            let data = screeningsVaccinationsActions[indexPath.section]
            
            if VIAApplicableFeatures.default.withDefaultStringValueForSVTestedDate() && data.testedDate == NSDate.distantPast {
                    screeningsVaccinationsActions[indexPath.section].testedDate = Date()
                    manipulateRightBarButton()
            }
            
            for itemIndex in 0 ... screeningsVaccinationsActions.count - 1 {
                if indexPath.section != itemIndex {
                    screeningsVaccinationsActions[itemIndex].showDatePicker = false
                }
            }
            
            screeningsVaccinationsActions[indexPath.section].showDatePicker = !data.showDatePicker
        }
        
        let contentOffset = tableView.contentOffset
        tableView.reloadData()
        tableView.layoutIfNeeded()
        tableView.setContentOffset(contentOffset, animated: false)
        
        self.tableView.deselectRow(at: indexPath, animated: false)
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            return getScreeningVaccinationCell(indexPath)
        } else if indexPath.row == 1 {
            return getScreeningVaccinationDateCell(indexPath)
        } else {
            return getDatePickerCell(indexPath)
        }
    }
    
    func getScreeningVaccinationCell(_ indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: SVRadioButtonCell.defaultReuseIdentifier,
                                                       for: indexPath) as? SVRadioButtonCell else {
            return tableView.defaultTableViewCell()
        }
        
        let data = screeningsVaccinationsActions[indexPath.section]
        
        cell.actionName = screeningsVaccinationsActions[indexPath.section].typeName
        
        if data.selected {
            cell.actionSelectionIndicatorIcon = VIAScreeningsVaccinationsAsset.checkMarkSmall.image.withRenderingMode(.alwaysTemplate)
            cell.tintColor = UIColor.flatGreen()
        } else {
            cell.actionSelectionIndicatorIcon = VIAScreeningsVaccinationsAsset.savCheckMarkInactive.image.withRenderingMode(.alwaysTemplate)
            cell.tintColor = UIColor.gray
        }
        
        return cell
    }
    
    func getScreeningVaccinationDateCell(_ indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: SVDatePickerCell.defaultReuseIdentifier,
                                                       for: indexPath) as? SVDatePickerCell else {
            return tableView.defaultTableViewCell()
        }
        
        let data = screeningsVaccinationsActions[indexPath.section]
        cell.dateLabel.text = CommonStrings.Sv.ConfirmSubmitTestedDate2403

        if VIAApplicableFeatures.default.withDefaultStringValueForSVTestedDate() {
            if data.testedDate != NSDate.distantPast{
                cell.testedDate = formatter.string(from: data.testedDate!)
            } else {
                cell.testedDate = CommonStrings.Assessment.DateTested.DefaultValue2149
            }
        } else {
            if let testedDate = data.testedDate{
                cell.testedDate = formatter.string(from: testedDate)
            }
        }
        
        return cell
    }

    func getDatePickerCell(_ indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: VIADatePickerTableViewCell.defaultReuseIdentifier, for: indexPath) as? VIADatePickerTableViewCell else {
            return tableView.defaultTableViewCell()
        }
        
        let data = screeningsVaccinationsActions[indexPath.section]
        
        cell.selectedDate = data.testedDate ?? Date()
        cell.maximumDate = Date()
        cell.minimumDate = VIAApplicableFeatures.default.setMinimumDate() ?? Date()
        
        cell.delegate = self

        return cell
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return getHeaderInSection(tableView, section: section)
    }
    
    private func getHeaderInSection(_ tableView: UITableView, section:Int) -> UIView?{
        guard let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: TitleImageDescriptionHeaderView.defaultReuseIdentifier) as? TitleImageDescriptionHeaderView else {
            return nil
        }
        
        let data = screeningsVaccinationsActions[section]
        if data.featureType == .Screenings && data.displayHeader{
            view.title = Sections.screenings.sectionHeaderTitle() ?? ""
            view.descriptionText = Sections.screenings.sectionHeaderDescription() ?? ""
            view.image = VIAScreeningsVaccinationsAsset.savScreenings.image
            
            return view
        }else if data.featureType == .Vaccinations && data.displayHeader{
            view.title = Sections.vaccinations.sectionHeaderTitle() ?? ""
            view.descriptionText = Sections.vaccinations.sectionHeaderDescription() ?? ""
            view.image = VIAScreeningsVaccinationsAsset.savVaccinations.image.withRenderingMode(.alwaysTemplate)
            
            return view
        }
        
        return nil
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        //ge20180216 : Adjust row height of date cell depending on selection of index 0
        if indexPath.row == 1 {
            let data = screeningsVaccinationsActions[indexPath.section]
            return data.selected ? 60 : 0
        } else if indexPath.row == 2 {
            let data = screeningsVaccinationsActions[indexPath.section]
            return data.showDatePicker ? UITableViewAutomaticDimension : 0
        } else {
            return 60
        }
    }
}

extension SVHealthActionViewController {
    // MARK: DatePickerDelegate
    func didPickDate(_ date: Date) {
        if AppSettings.getAppTenant() == .SLI {
            let calendar        = Calendar.current
            let currentDate     = Date()
            let selectedDate    = date
            
            var dateComponents      = DateComponents()
            dateComponents.year     = calendar.component(.year,     from: selectedDate)
            dateComponents.month    = calendar.component(.month,    from: selectedDate)
            dateComponents.day      = calendar.component(.day,      from: selectedDate)
            dateComponents.hour     = calendar.component(.hour,     from: currentDate)
            dateComponents.minute   = calendar.component(.minute,   from: currentDate)
            dateComponents.second   = calendar.component(.second,   from: currentDate)
            
            if let formattedDateTime = calendar.date(from: dateComponents){
                //self.didPickDate(formattedDateTime)
                print("--> FormattedDate:\(formattedDateTime)")
                 self.reloadTableFromActiveDatePicker(withDate: formattedDateTime)
            }else{
                //self.didPickDate(selectedDate)
                print("--> Selected:\(selectedDate)")
                self.reloadTableFromActiveDatePicker(withDate: selectedDate)
            }
        } else {
            self.reloadTableFromActiveDatePicker(withDate: date)
        }
    }
    
    fileprivate func showDatePicker(indexPath: IndexPath) {
        let data = screeningsVaccinationsActions[indexPath.section]
        let datePicker: UIDatePicker = UIDatePicker()
        datePicker.timeZone = NSTimeZone.local
        datePicker.datePickerMode = .date
        datePicker.maximumDate = Date()
        datePicker.minimumDate = DataProvider.newRealm().getRenewalPeriods().first?.effectiveFrom?.setMinimumDate()
        datePicker.frame = CGRect(x: 0, y: 15, width: 270, height: 200)
        datePicker.date = data.testedDate ?? Date()
        
        let alertController = UIAlertController(title: "\n\n\n\n\n\n\n\n", message: nil, preferredStyle: .alert)
        alertController.view.addSubview(datePicker)
        
        
        alertController.addAction(UIAlertAction(title: CommonStrings.CancelButtonTitle24,
                                                style: .cancel, handler: nil))
        alertController.addAction(UIAlertAction(title: CommonStrings.OkButtonTitle40,
                                                style: .default, handler: { [weak self] (action) in
                                                    self?.reloadTable(withDate: datePicker.date, indexPath: indexPath)
        }))
        
        self.present(alertController, animated: true, completion:{})
    }
    
    func manipulateRightBarButton() {
        
        //Check if there is an item selected
        var watcher = false
        for healthActionHolder in screeningsVaccinationsActions{
            if healthActionHolder.selected {
                if VIAApplicableFeatures.default.withDefaultStringValueForSVTestedDate() {
                    if let testedDate = healthActionHolder.testedDate, testedDate != NSDate.distantPast {
                        watcher = true
                    } else {
                        watcher = false
                        break
                    }
                } else {
                    watcher = true
                    break
                }
            }
        }
        self.navigationItem.rightBarButtonItem?.isEnabled = watcher ? true : false
    }
    
    private func reloadTable(withDate date:Date, indexPath:IndexPath) {
        screeningsVaccinationsActions[indexPath.section].testedDate = date
        
        UIView.setAnimationsEnabled(false)
        tableView.reloadRows(at: [indexPath], with: UITableViewRowAnimation.none)
        
        manipulateRightBarButton()
    }
    
    private func reloadTableFromActiveDatePicker(withDate date:Date) {
        var itemCount: Int = 0
        for item in screeningsVaccinationsActions {
            if item.showDatePicker == true {
                screeningsVaccinationsActions[itemCount].testedDate = date
            }
            itemCount += 1
        }
        
        tableView.reloadData()
        
        manipulateRightBarButton()
    }
}
