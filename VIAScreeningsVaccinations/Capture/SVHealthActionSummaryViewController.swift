//
//  SVHealthActionSummaryViewController.swift
//  VitalityActive
//
//  Created by Val Tomol on 13/11/2017.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation

import VitalityKit
import VIAUIKit
import VIAUtilities
import VIACommon

public protocol HealthActionSummaryViewModel {
    func submittedImages() -> Array<UIImage>
    func submitEvents(completion: @escaping (_ error: Error?) -> Void)
    func summaryViewTitle() -> String
    func summaryHeaderTitle() -> String
    func summaryHeaderMessage() -> String
}

public class SVHealthActionSummaryViewController: VIATableViewController, KnowYourHealthTintable {
    // MARK: Variables
    internal var viewModel: HealthActionSummaryViewModel?
    internal var heightForCell: CGFloat = 0.0
    
    let imagesRealm = DataProvider.newSAVRealm()
    let savListRealm = DataProvider.newSAVRealm()
    var screeningList = Array<SAVSelectedItem>()
    var vaccinationList = Array<SAVSelectedItem>()
    var sections = Array<Any>()
    
    // MARK: View Lifecycles
    override public func viewDidLoad() {
        super.viewDidLoad()
      
        self.viewModel = SVHealthActionSummaryViewModel()
        self.title = self.viewModel?.summaryViewTitle()
        self.setupTableView()
        
        self.addBarButtonItems()
        
        // Fetch Realm Screenings and Vaccination Image Data
        let images = imagesRealm.objects(PhotoAsset.self)
        print("-----> images [item count: \(images.count)]: \(images)")
        
        // Fetch Realm Screenings and Vaccination List
        let savList = savListRealm.objects(SAVSelectedItem.self)
        print("-----> savlist [item count: \(savList.count)]: \(savList)")
        
        if savList.count > 0 {
            for index in 0...(savList.count - 1) {
                if savList[index].featureType == ProductFeatureTypeRef.Screenings {
                    self.screeningList.append(savList[index])
                } else if savList[index].featureType == ProductFeatureTypeRef.Vaccinations {
                    self.vaccinationList.append(savList[index])
                }
            }
        }
    }
    
    override public func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.navigationBar.isHidden = false
        navigationController?.makeNavigationBarTransparent()
        self.navigationController?.setToolbarHidden(true, animated: false)
    }
    
    func setupTableView() {
        self.registerReuasbleView()
        self.tableView.estimatedRowHeight = 44
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.setupTableViewHeader()
    }
    
    func registerReuasbleView() {
        self.tableView.register(ImageCollectionTableViewCell.nib(), forCellReuseIdentifier: ImageCollectionTableViewCell.defaultReuseIdentifier)
        self.tableView.register(HeaderValueSubtitleTableViewCell.nib(), forCellReuseIdentifier: HeaderValueSubtitleTableViewCell.defaultReuseIdentifier)
        self.tableView.register(VIATableViewSectionHeaderFooterView.nib(), forHeaderFooterViewReuseIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier)
    }
    
    func setupTableViewHeader() {
        let header = TitleMessageHeaderFooterView.viewFromNib(owner: self) as! TitleMessageHeaderFooterView
        header.title = self.viewModel?.summaryHeaderTitle()
        header.content = self.viewModel?.summaryHeaderMessage()
        self.tableView.tableHeaderView = header
        self.tableView.sizeHeaderViewToFit()
    }
    
    // MARK: TableView Delegate
    public override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if let header = self.tableView.dequeueReusableHeaderFooterView(withIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier) as? VIATableViewSectionHeaderFooterView {
            self.setup(header: header, for: section)
            return header
        }
        return nil
    }
    
    func setup(header: VIATableViewSectionHeaderFooterView, for group: Int) {
        header.labelText = ""
        header.set(font: UIFont.subheadlineFont())
        header.set(textColor: UIColor.tableViewSectionHeaderFooter())
        
        if self.screeningList.count != 0 && self.vaccinationList.count != 0 {
            switch group {
            case 0:
                header.labelText = CommonStrings.Sv.ScreeningsTitle1012
            case 1:
                header.labelText = CommonStrings.Sv.VaccinationsTitle1013
            case 2:
                header.labelText = CommonStrings.SummaryScreen.UploadedProofTitle186
            default:
                header.labelText = ""
            }
        } else if self.screeningList.count != 0 || self.vaccinationList.count != 0 {
            if self.screeningList.count == 0 {
                switch group {
                case 0:
                    header.labelText = CommonStrings.Sv.VaccinationsTitle1013
                case 1:
                    header.labelText = CommonStrings.SummaryScreen.UploadedProofTitle186
                default:
                    header.labelText = ""
                }
            } else {
                switch group {
                case 0:
                    header.labelText = CommonStrings.Sv.ScreeningsTitle1012
                case 1:
                    header.labelText = CommonStrings.SummaryScreen.UploadedProofTitle186
                default:
                    header.labelText = ""
                }
            }
        } else {
            switch group {
            case 0:
                header.labelText = CommonStrings.SummaryScreen.UploadedProofTitle186
            default:
                header.labelText = ""
            }
        }
    }
    
    public override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    public override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: false)
    }
    
    // MARK: TableView DataSource
    public override func numberOfSections(in tableView: UITableView) -> Int {
        if self.screeningList.count != 0 && self.vaccinationList.count != 0 {
            return 3
        } else if self.screeningList.count != 0 || self.vaccinationList.count != 0 {
            return 2
        } else {
            return 1
        }
    }
    
    public override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let imagesRowCount = 1
        
        if self.screeningList.count != 0 && self.vaccinationList.count != 0 {
            switch section {
            case 0:
                return self.screeningList.count
            case 1:
                return self.vaccinationList.count
            case 2:
                return imagesRowCount
            default:
                return 0
            }
        } else if self.screeningList.count != 0 || self.vaccinationList.count != 0 {
            if self.screeningList.count == 0 {
                switch section {
                case 0:
                    return self.vaccinationList.count
                case 1:
                    return imagesRowCount
                default:
                    return 0
                }
            } else {
                switch section {
                case 0:
                    return self.screeningList.count
                case 1:
                    return imagesRowCount
                default:
                    return 0
                }
            }
        } else {
            switch section {
            case 0:
                return imagesRowCount
            default:
                return 0
            }
        }
    }
    
    public override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = UITableViewCell()
        if self.screeningList.count != 0 && self.vaccinationList.count != 0 {
            switch indexPath.section {
            case 0:
                cell = showResultsForRow(index: indexPath)
            case 1:
                cell = showResultsForRow(index: indexPath)
            case 2:
                cell = showImageCollectionForRow()
            default:
                cell  = UITableViewCell()
            }
        } else if self.screeningList.count != 0 || self.vaccinationList.count != 0 {
            if self.screeningList.count == 0 {
                switch indexPath.section {
                case 0:
                    cell = showResultsForRow(index: indexPath)
                case 1:
                    cell = showImageCollectionForRow()
                default:
                    cell  = UITableViewCell()
                }
            } else {
                switch indexPath.section {
                case 0:
                    cell = showResultsForRow(index: indexPath)
                case 1:
                    cell = showImageCollectionForRow()
                default:
                    cell  = UITableViewCell()
                }
            }
        } else {
            switch indexPath.section {
            default:
                cell  = UITableViewCell()
            }
        }
        
        return cell
    }
    
    func showResultsForRow(index: IndexPath) -> UITableViewCell {
        if let resultsCell = tableView.dequeueReusableCell(withIdentifier: HeaderValueSubtitleTableViewCell.defaultReuseIdentifier) {
            let collectionViewCell = self.setUpCapturedResults(cell: resultsCell, at: index)
            return collectionViewCell
        }
        return UITableViewCell()
    }
    
    func showImageCollectionForRow() -> UITableViewCell {
        if let imageCollectionCell = self.tableView.dequeueReusableCell(withIdentifier: ImageCollectionTableViewCell.defaultReuseIdentifier) {
            let collectionviewCell = self.setUpCaptureImages(cell: imageCollectionCell)
            self.heightForImageCollection(cell: collectionviewCell)
            return collectionviewCell
        }
        return UITableViewCell()
    }
    
    func setUpCapturedResults(cell: UITableViewCell, at index: IndexPath) -> UITableViewCell {
        if let capturedResultsCell = cell as? HeaderValueSubtitleTableViewCell {
            var valueText = String()
            var rawDate = Date()
            if self.screeningList.count != 0 && self.vaccinationList.count != 0 {
                switch index.section {
                case 0:
                    valueText = self.screeningList[index.row].typeName
                    rawDate = self.screeningList[index.row].testedDate!
                case 1:
                    valueText = self.vaccinationList[index.row].typeName
                    rawDate = self.vaccinationList[index.row].testedDate!
                default:
                    valueText = ""
                }
            } else if self.screeningList.count != 0 || self.vaccinationList.count != 0 {
                if self.screeningList.count == 0 {
                    switch index.section {
                    case 0:
                        valueText = self.vaccinationList[index.row].typeName
                        rawDate = self.vaccinationList[index.row].testedDate!
                    default:
                        valueText = ""
                    }
                } else {
                    switch index.section {
                    case 0:
                        valueText = self.screeningList[index.row].typeName
                        rawDate = self.screeningList[index.row].testedDate!
                    default:
                        valueText = ""
                    }
                }
            } else {
                switch index.section {
                default:
                    valueText = ""
                }
            }
            let formattedDate = Localization.dayDateShortMonthyearFormatter.string(from: rawDate)
            capturedResultsCell.selectionStyle = .none
            capturedResultsCell.setCell(title: "")
            capturedResultsCell.setCell(subTitle: CommonStrings.SummaryScreen.DateTestedTitle185(formattedDate))
            capturedResultsCell.setCell(value: valueText)
            return capturedResultsCell
        }
        
        return UITableViewCell()
    }
    
    func setUpCaptureImages(cell: UITableViewCell) -> ImageCollectionTableViewCell {
        if let imageCollectionViewCell = cell as? ImageCollectionTableViewCell {
            if let capturedImages = self.viewModel?.submittedImages() {
                imageCollectionViewCell.setImageCollection(images: capturedImages)
            }
            imageCollectionViewCell.imageCollectionView?.bounces = false
            imageCollectionViewCell.viewWidth = self.view.frame.width
            imageCollectionViewCell.setupCollectionFlowLayoutSize()
            return imageCollectionViewCell
        }
        return ImageCollectionTableViewCell()
    }
    
    func heightForImageCollection(cell: ImageCollectionTableViewCell) {
        let collectionViewFlowLayout = cell.imageCollectionView?.collectionViewLayout
        let cellPadding = collectionViewFlowLayout?.collectionViewContentSize
        let height = cellPadding?.height
        self.heightForCell = height ?? 0
        cell.updateImageCollectionViewSize(with:height)
    }
    
    // MARK: Navigation
    func addBarButtonItems() {
        let next = UIBarButtonItem(title: CommonStrings.ConfirmTitleButton182, style: .plain, target: self, action: #selector(next(_:)))
        self.navigationItem.rightBarButtonItem = next
    }
    
    func next(_ sender: Any) {
        if VitalityProductFeature.isEnabled(.SVDSConsent) {
            self.performSegue(withIdentifier: "showDataAndPrivacyPolicy", sender: nil)
        } else {
            submitSV()
        }
    }
    
    func submitSV() {
        self.showHUDOnWindow()
        self.viewModel?.submitEvents(completion: { [weak self] error in
            DispatchQueue.main.async {
                self?.hideHUDFromWindow()
                if error == nil {
                    self?.performSegue(withIdentifier: "showCompletion", sender: nil)
                } else {
                    debugPrint("Error")
                    self?.displaySVSubmissionFailure{ [weak self] (resubmit) in
                        if resubmit{
                            self?.submitSV()
                        }
                    }
                }
            }
        })
    }
    
    public func displaySVSubmissionFailure(completion: @escaping (_ resubmit: Bool) -> Void) {
        let cancelAction = UIAlertAction(title: CommonStrings.CancelButtonTitle24, style: UIAlertActionStyle.cancel) { (alertAction) in
            completion(false)
        }
        let tryAgain = UIAlertAction(title: CommonStrings.TryAgainButtonTitle43, style: UIAlertActionStyle.default) { (alertAction) in
            completion(true)
        }
        
        let failedToSubmitVHCAlert = UIAlertController(title: CommonStrings.PrivacyPolicy.UnableToCompleteAlertTitle115, message: CommonStrings.SummaryScreen.VhcCompleteErrorMessage187, preferredStyle: .alert)
        
        failedToSubmitVHCAlert.addAction(cancelAction)
        failedToSubmitVHCAlert.addAction(tryAgain)
        self.present(failedToSubmitVHCAlert, animated: true, completion: nil)
    }
    
    @IBAction public func unwindFromSVDataAndPrivacyPolicyAgreementToSummary(segue: UIStoryboardSegue) {
        debugPrint("unwindFromSVDataAndPrivacyPolicyAgreementToSummary")
    }
}
