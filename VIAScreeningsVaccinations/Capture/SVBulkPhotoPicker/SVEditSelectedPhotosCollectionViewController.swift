//
//  SVEditSelectedPhotosCollectionViewController.swift
//  VitalityActive
//
//  Created by Harry C. Lingad on 2/28/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import UIKit
import Photos

import VIAUIKit
import VitalityKit
import VitalityKit
import VIACommon
import VIAImageManager

import RealmSwift

// This is used to update the imagesInfo variable of SVHealthActionViewController, for persistent
public protocol SVEditSelectedPhotosCollectionViewControllerProtocol{
    func updateEditProofImagesInfo(imagesInfo: Array<Dictionary<String, Any>>)
}

class SVEditSelectedPhotosCollectionViewController: UICollectionViewController {
    
    var delegate:SVEditSelectedPhotosCollectionViewControllerProtocol!
    
    let savListRealm = DataProvider.newSAVRealm()
    var imagesInfo: Array<Dictionary<String, Any>> = []
    var imagesInformation: Array<ImageInformation> = []
    var selectedImageIndexes = [Int]()
    var imagePicker: VIASelectImageController?
    var emptyStateAddPhotoView: AddPhotoView?
    var realm: Realm?
    
    @IBOutlet weak var selectPhotosFlowLayout: UICollectionViewFlowLayout!
    @IBAction func unwindToPhotosCollection(segue: UIStoryboardSegue) {
        self.collectionView?.reloadData()
    }
    
    public func remove(imageInfo: [String: Any]) {
        if let pickedImage = imageInfo[UIImagePickerControllerOriginalImage] as? UIImage {
            var index = 0
            for helpImageInfo in self.imagesInfo {
                if let heldImage = helpImageInfo[UIImagePickerControllerOriginalImage] as? UIImage {
                    if (pickedImage.isEqual(heldImage)) {
                        self.imagesInfo.remove(at: index)
                    }
                }
                index += 1
            }
        }
        manipulateRightBarButton()
    }
    
    public func add(imageInfo: [String : Any]) {
        self.imagesInfo.append(imageInfo)
        manipulateRightBarButton()
    }
    
    deinit {
    }
    // This method will also update the imagesInfo in SVHealthActionViewController
    override func viewWillDisappear(_ animated: Bool) {
        if isMovingFromParentViewController {
            
            imagesInfo.removeAll()
            for imageInformation in imagesInformation{
                imagesInfo.append(imageInformation.imageInfo!)
            }
            self.navigationController?.setToolbarHidden(true, animated: false)
            delegate.updateEditProofImagesInfo(imagesInfo: self.imagesInfo)
            delegate = nil
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.collectionView?.alwaysBounceVertical = true
        self.title = CommonStrings.Proof.AddProofScreenTitle163
        imagePicker = VIASelectImageController(pickerDelegate: self, configuration: getImageConfiguration())
        self.view.backgroundColor = UIColor.day()
        
        self.setCollectionViewCellSize()
        self.registerCollectionViewCells()
        self.setupToolBar()
        self.addImageInfo(imagesInfo: imagesInfo)
        
        self.clearsSelectionOnViewWillAppear = false
        
    }
    
    private func getImageConfiguration() -> UIImagePickerController.ImageControllerConfiguration{
        return UIImagePickerController.ImageControllerConfiguration(shouldLimitFileSize: false, maxFileSize: 0, shouldLimitNumberOfAttachProof: false, maxAttachmentCount: 0, includeAllProofs: true)
    }
    
    func addImageInfo(imagesInfo: Array<Dictionary<String, Any>>){
        for imageInfo in imagesInfo{
            let newImageInfo = ImageInformation(imageInfo: imageInfo, status: false)
            imagesInformation.append(newImageInfo)
            
        }
    }
    
    
    func editSelectedProofs(sender: UIBarButtonItem) {
        //self.performSegue(withIdentifier: "showEditProof", sender: self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.hideBackButtonTitle()
        
        if (isCollectionEmpty()) {
            emptyStateAddPhotoView = AddPhotoView.viewFromNib(owner: self) as? AddPhotoView
            
            emptyStateAddPhotoView?.setHeader(title: CommonStrings.Sv.AddProofEmptyTitle1018)
            emptyStateAddPhotoView?.setDetail(message: CommonStrings.Sv.AddProofEmptyMessage1019)
            emptyStateAddPhotoView?.setAction(title: CommonStrings.Proof.AddButton166)
            emptyStateAddPhotoView?.setHeader(image: VIAScreeningsVaccinationsAsset.cameraLarge.templateImage)
            emptyStateAddPhotoView?.set(photoViewDelegate: self)
            emptyStateAddPhotoView?.frame = self.collectionView?.frame ?? CGRect(x: 0, y: 0, width: 0, height: 0)
            
            if emptyStateAddPhotoView != nil {
                self.collectionView?.addSubview(emptyStateAddPhotoView!)
            }
        }
        self.setupNavigationItem()
        manipulateRightBarButton()
    }
    
    func isCollectionEmpty() -> Bool {
        return self.imagesInfo.count == 0
    }
    
    func registerCollectionViewCells() {
        self.collectionView!.register(ImageCollectionViewCell.nib(), forCellWithReuseIdentifier: ImageCollectionViewCell.defaultReuseIdentifier)
        self.collectionView!.register(LabelSupplimentaryView.nib(), forSupplementaryViewOfKind: UICollectionElementKindSectionFooter, withReuseIdentifier: "Footer")
    }
    
    func setupNavigationItem() {
        let cancelBarButton = UIBarButtonItem(title: CommonStrings.CancelButtonTitle24 , style: .done, target: self, action: #selector(onCancel))
        
        let selectAllBarButton = UIBarButtonItem(title: CommonStrings.Settings.FeedbackSelectAll2093 , style: .done, target: self, action: #selector(onSelectAll))
        
        self.navigationItem.rightBarButtonItem = cancelBarButton
        self.navigationItem.leftBarButtonItem = selectAllBarButton
        //self.navigationItem.rightBarButtonItem?.isEnabled = false
    }
    
    func setupToolBar() {
        var toolbarItemsArray = Array<UIBarButtonItem>()
        
        let flexibleSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        toolbarItemsArray.append(flexibleSpace)
        let deleteItem = UIBarButtonItem(image: VIAScreeningsVaccinationsAsset.trashSmall.templateImage, style: .plain, target: self, action: #selector(deleteSelectedItem))
        toolbarItemsArray.append(deleteItem)
        
        self.setToolbarItems(toolbarItemsArray, animated: false)
        self.navigationController?.setToolbarHidden(false, animated: false)
    }
    
    func onCancel() {
        _ = navigationController?.popViewController(animated: true)
    }
    
    func onSelectAll(){
        
        if imagesInformation.count != 0{
            for index in 0...(imagesInformation.count - 1) {
                imagesInformation[index].status = true
            }
            self.collectionView?.reloadData()
        }
    }
    
    func deleteSelectedItem(sender: UIBarButtonItem) {
        
        var noItemToBeDeleted = 0
        
        for imageInformation in imagesInformation{
            if imageInformation.status == true{
                noItemToBeDeleted += 1
            }
            
        }
        
        let alertController = UIAlertController(title: "", message: CommonStrings.Proof.RemoveMessage178, preferredStyle: UIAlertControllerStyle.actionSheet)
        let cancelAction = UIAlertAction(title: CommonStrings.CancelButtonTitle24, style: .cancel) { (UIAlertAction) in
            alertController.dismiss(animated: true, completion: nil)
        }
        
        let removePhotoAction = UIAlertAction(title: VIAApplicableFeatures.default.getSVRemoveButtonLabel(numberOfPhotos: noItemToBeDeleted), style: .destructive) { [weak self] (_) -> Void in
            self?.proceedInDeleting()
        }
        
        alertController.popoverPresentationController?.barButtonItem = sender
        
        alertController.addAction(removePhotoAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    
    func proceedInDeleting(){
        var tempImagesInformation: Array<ImageInformation> = []
        
        if imagesInformation.count != 0{
            for index in 0...(imagesInformation.count - 1) {
                if imagesInformation[index].status == false{
                    tempImagesInformation.append(imagesInformation[index])
                }
            }
            
            imagesInformation.removeAll()
            imagesInformation = tempImagesInformation
            
            if imagesInformation.count == 0{
                _ = navigationController?.popViewController(animated: true)
            }else{
                self.collectionView?.reloadData()
            }
        }
        
    }
    
    
    
    
    
    
    func beginURLWrite(completion: @escaping () -> Void) {
        DispatchQueue.global(qos: .background).async {
            let realm = DataProvider.newSAVRealm()
            var realmAssets = Array<PhotoAsset>()
            
            for image in self.imagesInfo {
                let realmAsset = PhotoAsset()
                if let uiImageURL = image[UIImagePickerControllerReferenceURL] as? URL {
                    let photosAsset = PHAsset.fetchAssets(withALAssetURLs: [uiImageURL], options: nil).firstObject
                    let localIdentifier = photosAsset?.localIdentifier
                    realmAsset.assetURL = localIdentifier
                } else {
                    if let uiImage = image[UIImagePickerControllerOriginalImage] as? UIImage {
                        if let imageData = PhotoAsset.compress(image:uiImage) {
                            realmAsset.assetData = imageData
                        }
                    }
                }
                realmAssets.append(realmAsset)
            }
            
            try! realm.write {
                realm.delete(realm.allPhotosAssets())
                realm.add(realmAssets)
            }
            
            DispatchQueue.main.async {
                completion()
            }
        }
    }
    
    func setCollectionViewCellSize() {
        let leftAndRightSpaceingTotal: CGFloat = 15
        let cellPaddingTotal: CGFloat = 15
        let totalWidthPadding: CGFloat = leftAndRightSpaceingTotal + cellPaddingTotal
        let itemWidth = (((self.collectionView?.frame.size.width) ?? totalWidthPadding) - totalWidthPadding) / 2
        self.selectPhotosFlowLayout.itemSize = CGSize(width: itemWidth,
                                                      height: itemWidth)
        self.selectPhotosFlowLayout.minimumLineSpacing = 10
    }
    
    func dequeToCaptureResults() {
        self.performSegue(withIdentifier: "unwindToCaptureResults", sender: self)
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let selectedImage = sender as? [String : Any] else {
            return
        }
        guard let detailImageViewController = segue.destination as? SVImageDetailViewController else {
            return
        }
        
        detailImageViewController.set(selectedImage: selectedImage)
    }
    
    // MARK: UICollectionViewDataSource
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.imagesInformation.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ImageCollectionViewCell.defaultReuseIdentifier, for: indexPath) as? ImageCollectionViewCell
        cell?.imageView.image = nil
        cell?.resetCell()
        
        
        
        if let pickedImage = self.imagesInformation[indexPath.row].imageInfo?[UIImagePickerControllerOriginalImage] as? UIImage {
            cell?.imageView.image = pickedImage
            if self.imagesInformation[indexPath.row].status!{
                cell?.setStatusIndicatorImage(statusImage: VIAScreeningsVaccinationsAsset.statusAchieved.templateImage)
            }
            
        }
        
        
        
        guard let returnCell = cell else { return UICollectionViewCell() }
        return returnCell
    }
    
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        var supplimentaryView: UICollectionReusableView = UICollectionReusableView()
        if (kind == UICollectionElementKindSectionFooter) {
            guard let footerView = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionFooter, withReuseIdentifier: "Footer", for: indexPath) as? LabelSupplimentaryView else {
                return UICollectionReusableView()
            }
            
            footerView.setView(text: "")
            
            supplimentaryView = footerView
        }
        return supplimentaryView
    }
    
    func checkIfImageExist(imageIndex: Int) -> Bool{
        
        for selectedImageIndex in selectedImageIndexes{
            if selectedImageIndex == imageIndex{
                return true
            }
        }
        return false
    }
    
    
    // MARK: UICollectionViewDelegate
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: false)
        
        if (indexPath.item < self.imagesInformation.count) {
            if imagesInformation[indexPath.row].status!{
                imagesInformation[indexPath.row].status = false
            }else{
                imagesInformation[indexPath.row].status = true
            }
            
            self.collectionView?.reloadData()
        } else {
            if let cell = collectionView.cellForItem(at: indexPath) as? ImageCollectionViewCell {
                cell.selectionClosure()
            }
        }
    }
    
    
    
    
}

extension SVEditSelectedPhotosCollectionViewController: VIAImagePickerDelegate {
    func pass(imageInfo: [String: Any]) {
        
        self.emptyStateAddPhotoView?.performSelector(onMainThread: #selector(self.emptyStateAddPhotoView?.removeFromSuperview), with: nil, waitUntilDone: false)
        self.imagesInfo.append(imageInfo)
        
        self.collectionView?.reloadData()
        self.imagePicker?.dismissImagePicker()
        
        manipulateRightBarButton()
    }
    func retrieveNumberOfSelectedItems() -> Int{
        
        return savListRealm.allSAVItems().count
    }
    func manipulateRightBarButton() {
        if (self.imagesInfo.count > 0) {
            
            self.navigationItem.rightBarButtonItem?.isEnabled = true
        }
    }
    func show(alert imageSouceSelector: UIAlertController) {
        self.present(imageSouceSelector, animated: true, completion: nil)
    }
    
    func present(imagePicker: UIImagePickerController) {
        self.present(imagePicker, animated: true, completion: nil)
    }
}

extension SVEditSelectedPhotosCollectionViewController: AddPhotoViewDelegate {
    func showPhotoPicker() {
        self.imagePicker?.selectImage(view: emptyStateAddPhotoView?.getAddPhotoButton())
    }
}


