//
//  SVSelectedPhotosCollectionViewController.swift
//  VitalityActive
//
//  Created by Dexter Anthony Ambrad on 12/4/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

import UIKit
import Photos

import VIAUIKit
import VitalityKit
import VitalityKit
import VIACommon
import VIAImageManager

import RealmSwift

// This is used to update the imagesInfo variable of SVHealthActionViewController, for persistent
protocol SVSelectedPhotosCollectionViewControllerProtocol{
    func updateAddProofImagesInfo(imagesInfo: Array<Dictionary<String, Any>>)
}

class SVSelectedPhotosCollectionViewController: UICollectionViewController, SVEditSelectedPhotosCollectionViewControllerProtocol {
    
    var delegate:SVSelectedPhotosCollectionViewControllerProtocol!
    
    let savListRealm = DataProvider.newSAVRealm()
    var imagesInfo: Array<Dictionary<String, Any>> = []
    var imagePicker: VIASelectImageController?
    var emptyStateAddPhotoView: AddPhotoView?
    var editProofs: UIBarButtonItem?
    var realm: Realm?
    var maxLimit: Int = 11
    var shouldLimitNumberOfAttachProof: Bool = true
    
    @IBOutlet weak var selectPhotosFlowLayout: UICollectionViewFlowLayout!
    @IBAction func unwindToPhotosCollection(segue: UIStoryboardSegue) {
        self.collectionView?.reloadData()
    }
    
    public func remove(imageInfo: [String: Any]) {
        if let pickedImage = imageInfo[UIImagePickerControllerOriginalImage] as? UIImage {
            var index = 0
            for helpImageInfo in self.imagesInfo {
                if let heldImage = helpImageInfo[UIImagePickerControllerOriginalImage] as? UIImage {
                    if (pickedImage.isEqual(heldImage)) {
                        self.imagesInfo.remove(at: index)
                    }
                }
                index += 1
            }
        }
        manipulateRightBarButton()
    }
    
    public func add(imageInfo: [String : Any]) {
        self.imagesInfo.append(imageInfo)
        manipulateRightBarButton()
    }
    
    deinit {
    }
    // This method will also update the imagesInfo in SVHealthActionViewController
    override func viewWillDisappear(_ animated: Bool) {
        if isMovingFromParentViewController {
            delegate.updateAddProofImagesInfo(imagesInfo: self.imagesInfo)
            self.navigationController?.setToolbarHidden(true, animated: false)
            delegate = nil
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.collectionView?.alwaysBounceVertical = true
        self.title = CommonStrings.Proof.AddProofScreenTitle163
        guard let configuration = VIAApplicableFeatures.default.getImageControllerConfiguration()
            as? UIImagePickerController.ImageControllerConfiguration else {return}
        
        
        self.maxLimit = configuration.maxAttachmentCount
        self.shouldLimitNumberOfAttachProof = configuration.shouldLimitNumberOfAttachProof
        
        imagePicker = VIASelectImageController(pickerDelegate: self, configuration: configuration)
        self.view.backgroundColor = UIColor.day()
        
        self.setCollectionViewCellSize()
        self.registerCollectionViewCells()
        setupToolBar()
        
        
        self.clearsSelectionOnViewWillAppear = false
        
    }
    
    private func getImageConfiguration() -> UIImagePickerController.ImageControllerConfiguration{
        return UIImagePickerController.ImageControllerConfiguration(shouldLimitFileSize: false, maxFileSize: 0, shouldLimitNumberOfAttachProof: self.shouldLimitNumberOfAttachProof, maxAttachmentCount: self.maxLimit, includeAllProofs: true)
    }
    
    func setupToolBar() {
        var toolbarItemsArray = Array<UIBarButtonItem>()
        
        editProofs = UIBarButtonItem(barButtonSystemItem: .edit, target: self, action: #selector(editSelectedProofs))
        toolbarItemsArray.append(editProofs!)
        updateEditButtonStatus()
        
        self.setToolbarItems(toolbarItemsArray, animated: false)
        self.navigationController?.setToolbarHidden(false, animated: false)
    }
    
    func updateEditButtonStatus(){
        if imagesInfo.count == 0{
            editProofs?.isEnabled = false
        }else{
            editProofs?.isEnabled = true
        }
    }
    
    func editSelectedProofs(sender: UIBarButtonItem) {
        self.performSegue(withIdentifier: "showEditProof", sender: self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.hideBackButtonTitle()
        
        if (isCollectionEmpty()) {
            emptyStateAddPhotoView = AddPhotoView.viewFromNib(owner: self) as? AddPhotoView
            
            emptyStateAddPhotoView?.setHeader(title: CommonStrings.Sv.AddProofEmptyTitle1018)
            emptyStateAddPhotoView?.setDetail(message: CommonStrings.Sv.AddProofEmptyMessage1019)
            emptyStateAddPhotoView?.setAction(title: CommonStrings.Proof.AddButton166)
            emptyStateAddPhotoView?.setHeader(image: VIAScreeningsVaccinationsAsset.cameraLarge.templateImage)
            emptyStateAddPhotoView?.set(photoViewDelegate: self)
            emptyStateAddPhotoView?.frame = self.collectionView?.frame ?? CGRect(x: 0, y: 0, width: 0, height: 0)
            
            if emptyStateAddPhotoView != nil {
                self.collectionView?.addSubview(emptyStateAddPhotoView!)
            }
            emptyStateAddPhotoView?.tag = 1
            
            if emptyStateAddPhotoView != nil {
                if let EmptyStateViewWithTag = self.collectionView?.viewWithTag(1) {
                    EmptyStateViewWithTag.removeFromSuperview()
                    self.collectionView?.reloadData()
                    
                    self.collectionView?.addSubview(emptyStateAddPhotoView!)
                    if (self.navigationItem.rightBarButtonItem?.isEnabled ?? false) {
                        self.navigationItem.rightBarButtonItem?.isEnabled = false
                    }
                }else{
                    self.collectionView?.addSubview(emptyStateAddPhotoView!)
                    if (self.navigationItem.rightBarButtonItem?.isEnabled ?? false) {
                        self.navigationItem.rightBarButtonItem?.isEnabled = false
                    }
                }
            }
        } else {
            if let EmptyStateViewWithTag = self.collectionView?.viewWithTag(1) {
                EmptyStateViewWithTag.removeFromSuperview()
                self.collectionView?.reloadData()
            }
        }
        self.navigationController?.setToolbarHidden(false, animated: false)
        self.setupNavigationItem()
        manipulateRightBarButton()
    }
    
    func isCollectionEmpty() -> Bool {
        return self.imagesInfo.count == 0
    }
    
    func registerCollectionViewCells() {
        self.collectionView!.register(ImageCollectionViewCell.nib(), forCellWithReuseIdentifier: ImageCollectionViewCell.defaultReuseIdentifier)
        self.collectionView!.register(LabelSupplimentaryView.nib(), forSupplementaryViewOfKind: UICollectionElementKindSectionFooter, withReuseIdentifier: "Footer")
    }
    
    func setupNavigationItem() {
        let nextBarButton = UIBarButtonItem(title: CommonStrings.NextButtonTitle84 , style: .done, target: self, action: #selector(submitPhotos))
        self.navigationItem.rightBarButtonItem = nextBarButton
        self.navigationItem.rightBarButtonItem?.isEnabled = false
    }
    
    func submitPhotos() {
        self.showHUDOnWindow()
        self.beginURLWrite(completion: {
            self.hideHUDFromWindow()
            self.performSegue(withIdentifier: "showSummary", sender: self)
        })
    }
    
    func beginURLWrite(completion: @escaping () -> Void) {
        DispatchQueue.global(qos: .background).async {
            let realm = DataProvider.newSAVRealm()
            var realmAssets = Array<PhotoAsset>()
            
            for image in self.imagesInfo {
                let realmAsset = PhotoAsset()
                if let uiImageURL = image[UIImagePickerControllerReferenceURL] as? URL {
                    let photosAsset = PHAsset.fetchAssets(withALAssetURLs: [uiImageURL], options: nil).firstObject
                    let localIdentifier = photosAsset?.localIdentifier
                    realmAsset.assetURL = localIdentifier
                } else {
                    if let uiImage = image[UIImagePickerControllerOriginalImage] as? UIImage {
                        if let imageData = PhotoAsset.compress(image:uiImage) {
                            realmAsset.assetData = imageData
                        }
                    }
                }
                realmAssets.append(realmAsset)
            }
            
            try! realm.write {
                realm.delete(realm.allPhotosAssets())
                realm.add(realmAssets)
            }
            
            DispatchQueue.main.async {
                completion()
            }
        }
    }
    
    func setCollectionViewCellSize() {
        let leftAndRightSpaceingTotal: CGFloat = 15
        let cellPaddingTotal: CGFloat = 15
        let totalWidthPadding: CGFloat = leftAndRightSpaceingTotal + cellPaddingTotal
        let itemWidth = (((self.collectionView?.frame.size.width) ?? totalWidthPadding) - totalWidthPadding) / 2
        self.selectPhotosFlowLayout.itemSize = CGSize(width: itemWidth,
                                                      height: itemWidth)
        self.selectPhotosFlowLayout.minimumLineSpacing = 10
    }
    
    func dequeToCaptureResults() {
        self.performSegue(withIdentifier: "unwindToCaptureResults", sender: self)
    }
    
    func updateEditProofImagesInfo(imagesInfo: Array<Dictionary<String, Any>>){
        self.imagesInfo.removeAll()
        self.imagesInfo = imagesInfo
        updateEditButtonStatus()
        self.collectionView?.reloadData()
        
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "showEditProof" {
            // Pass the updated imagesInfo to the SelectedPhotosCollectionViewController, for persistence
            let svEditSelectedPhotosCollectionViewController = segue.destination as! SVEditSelectedPhotosCollectionViewController
            svEditSelectedPhotosCollectionViewController.delegate = self
            svEditSelectedPhotosCollectionViewController.imagesInfo = self.imagesInfo
            
        }
        guard let selectedImage = sender as? [String : Any] else {
            return
        }
        guard let detailImageViewController = segue.destination as? SVImageDetailViewController else {
            return
        }
        
        detailImageViewController.set(selectedImage: selectedImage)
    }
    
    // MARK: UICollectionViewDataSource
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let addMoreImagesCellCount = 1
        var itemCount = self.imagesInfo.count
        
        if VIAApplicableFeatures.default.shouldAllowProofGreaterThanSelection(){
            itemCount += addMoreImagesCellCount
        }else{
            if (self.imagesInfo.count < retrieveNumberOfSelectedItems()) {
                itemCount += addMoreImagesCellCount
            }
        }
        return itemCount
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ImageCollectionViewCell.defaultReuseIdentifier, for: indexPath) as? ImageCollectionViewCell
        cell?.imageView.image = nil
        cell?.resetCell()
        
        if (indexPath.item == self.imagesInfo.count) {
            if self.imagesInfo.count < self.maxLimit {
                cell?.setSmall(image: VIAScreeningsVaccinationsAsset.addPhoto.templateImage)
                cell?.selectionClosure = {
                    self.imagePicker?.selectImage()
                }
            }
        } else {
            if let pickedImage = self.imagesInfo[indexPath.row][UIImagePickerControllerOriginalImage] as? UIImage {
                cell?.imageView.image = pickedImage
            }
        }
        guard let returnCell = cell else { return UICollectionViewCell() }
        return returnCell
    }
    
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        var supplimentaryView: UICollectionReusableView = UICollectionReusableView()
        if (kind == UICollectionElementKindSectionFooter) {
            guard let footerView = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionFooter, withReuseIdentifier: "Footer", for: indexPath) as? LabelSupplimentaryView else {
                return UICollectionReusableView()
            }
            
            if VIAApplicableFeatures.default.shouldAllowProofGreaterThanSelection(){
                footerView.setView(text: CommonStrings.Sv.AttachmentsFootnoteMessage1042(String(self.imagesInfo.count), String(maxLimit)))
            }else{
                footerView.setView(text: CommonStrings.Sv.AttachmentsFootnoteMessage1042(String(self.imagesInfo.count),
                                                                                      String(retrieveNumberOfSelectedItems())))
            }
            
            supplimentaryView = footerView
        }
        return supplimentaryView
    }
    
    
    // MARK: UICollectionViewDelegate
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: false)
        
        if (indexPath.item < self.imagesInfo.count) {
            //            let pickedImage = self.imagesInfo[indexPath.row]
            //            self.performSegue(withIdentifier: "showImageDetailView", sender: pickedImage)
        } else {
            if let cell = collectionView.cellForItem(at: indexPath) as? ImageCollectionViewCell {
                cell.selectionClosure()
            }
        }
    }
}

extension SVSelectedPhotosCollectionViewController: VIAImagePickerDelegate {
    func pass(imageInfo: [String: Any]) {
        if self.imagesInfo.count < self.maxLimit {

            self.emptyStateAddPhotoView?.performSelector(onMainThread: #selector(self.emptyStateAddPhotoView?.removeFromSuperview), with: nil, waitUntilDone: false)
        
        //        if (self.navigationItem.rightBarButtonItem?.isEnabled == false) {
        //            self.navigationItem.rightBarButtonItem?.isEnabled = true
        //        }
            self.imagesInfo.append(imageInfo)
        
            self.collectionView?.reloadData()
            self.imagePicker?.dismissImagePicker()
        
            manipulateRightBarButton()
            updateEditButtonStatus()
        }
    }
    func retrieveNumberOfSelectedItems() -> Int{
        
        return savListRealm.allSAVItems().count
    }
    func manipulateRightBarButton() {
        
        print("--> Savlist: \(retrieveNumberOfSelectedItems()) && ImagesInfo:\(self.imagesInfo.count)")
        //if (retrieveNumberOfSelectedItems() == self.imagesInfo.count) {
        if (self.imagesInfo.count > 0) {
            
            self.navigationItem.rightBarButtonItem?.isEnabled = true
        }
    }
    func show(alert imageSouceSelector: UIAlertController) {
        self.present(imageSouceSelector, animated: true, completion: nil)
    }
    
    func present(imagePicker: UIImagePickerController) {
        if self.imagesInfo.count < self.maxLimit {
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
}

extension SVSelectedPhotosCollectionViewController: AddPhotoViewDelegate {
    func showPhotoPicker() {
        self.imagePicker?.selectImage(view: emptyStateAddPhotoView?.getAddPhotoButton())
    }
}

