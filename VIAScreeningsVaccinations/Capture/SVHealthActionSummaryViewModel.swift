//
//  SVHealthActionSummaryViewModel.swift
//  VitalityActive
//
//  Created by Val Tomol on 04/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.
//

import UIKit
import VIAUIKit
import VitalityKit
import VitalityKit
import VIAUtilities
import VIAImageManager

public protocol SVHealthActionSummaryViewModelDelegate: class {
    func displaySVSubmissionFailure(completion: @escaping (_ resubmit: Bool) -> Void)
}

class SVHealthActionSummaryViewModel: NSObject, HealthActionSummaryViewModel {
    // MARK: Variables
    let networking: SVEventsProcessor = SVSubmissionHelper()
    internal weak var delegate: SVHealthActionSummaryViewModelDelegate?
    let imagesRealm = DataProvider.newSAVRealm()
    
    func submitEvents(completion: @escaping (_ error: Error?) -> Void) {
        networking.processEvents(completion: completion)
    }
    
    func submittedImages() -> Array<UIImage> {
        var imagesArray: Array<UIImage> = Array<UIImage>()
        let images = imagesRealm.allPhotos()
        if let collectedImages = images {
            for collectedImage in collectedImages {
                if let assetURL = collectedImage.assetURL {
                    if let asset = PhotoAssetHelper.PhotosAssetForFileURL(url: assetURL) {
                        imagesArray.append(PhotoAssetHelper.getAssetThumbnail(asset: asset))
                    }
                } else {
                    if let imageData = collectedImage.assetData {
                        if let assetImage = UIImage(data: imageData) {
                            /*
                             Assets saved to realm as data using the UIImagePNGRepresentation method
                             If you save a UIImage as a JPEG, it will set the rotation flag.
                             PNGs do not support a rotation flag, so if you save a UIImage as a PNG,
                             it will be rotated incorrectly and not have a flag set to fix it.
                             So if you want PNGs you must rotate them yourself.
                             */
                            if let cgAsset = assetImage.cgImage {
                                let uprightImage = UIImage(cgImage: cgAsset, scale: 1, orientation: UIImageOrientation.right)
                                imagesArray.append(uprightImage)
                            } else {
                                imagesArray.append(assetImage)
                            }
                        }
                    }
                }
            }
        }
        return imagesArray
    }
    
    func summaryViewTitle() -> String {
        return CommonStrings.SummaryScreen.SummaryTitle181
    }
    
    func summaryHeaderTitle() -> String {
        return CommonStrings.Sv.ConfirmTitle1041
    }
    
    func summaryHeaderMessage() -> String {
        return CommonStrings.Sv.ConfirmMessage1024
    }
    
    // MARK: Photo Upload Delegate
    func displayImageUploadErrorMessage(with completion: @escaping (_ resubmit: Bool) -> Void) {
        self.delegate?.displaySVSubmissionFailure(completion: { resubmit in
            completion(resubmit)
        })
    }
}
