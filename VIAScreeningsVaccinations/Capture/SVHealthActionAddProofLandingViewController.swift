
//
//  SVHealthActionAddProofLandingViewController.swift
//  VitalityActive
//
//  Created by OJ Garde on 12/11/2017.
//  Copyright © 2017 Glucode. All rights reserved.
//

import UIKit
import VIAUIKit
import VitalityKit
import VIAImageManager

class SVHealthActionAddProofLandingViewController: VIAViewController, KnowYourHealthTintable{
    
    fileprivate var imagePicker: VIASelectImageController?
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        self.hideBackButtonTitle()
        configureView()
    }
    
    override public func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // TODO replace this static label
        self.title = CommonStrings.Proof.AddProofScreenTitle163
        
        configureAppearance()
    }
    
    fileprivate func configureAppearance() {
        navigationController?.makeNavigationBarTransparent()
        self.addRightBarButtonItem(CommonStrings.NextButtonTitle84, target: self, selector: #selector(onNext))
    }
    
    func onNext(sender:UIBarButtonItem){
        performSegue(withIdentifier: "showSummary", sender: self)
    }
    
    fileprivate func configureView(){
        if let addPhotoView = AddPhotoView.viewFromNib(owner: self){
            // TODO replace this CoreAsset
            addPhotoView.setHeader(image: VIAScreeningsVaccinationsAsset.cameraLarge.image.maskWithColor(UIColor.jungleGreen()))
            
            // TODO replace this static strings
            addPhotoView.setAction(title: CommonStrings.Proof.AddButton166)
            addPhotoView.setHeader(title: CommonStrings.Sv.AddProofEmptyTitle1018)
            addPhotoView.setDetail(message: CommonStrings.Sv.AddProofEmptyMessage1019)
            
            addPhotoView.set(photoViewDelegate: self)
            self.view.addSubview(addPhotoView)
            
            imagePicker = VIASelectImageController(pickerDelegate: self, configuration: getImageConfiguration())
        }
    }
    
    private func getImageConfiguration() -> UIImagePickerController.ImageControllerConfiguration{
        return UIImagePickerController.ImageControllerConfiguration(shouldLimitFileSize: false, maxFileSize: 0, shouldLimitNumberOfAttachProof: false, maxAttachmentCount: 0, includeAllProofs: true)
    }
}


// MARK: AddPhotoViewDelegate
extension SVHealthActionAddProofLandingViewController: AddPhotoViewDelegate{
    
    func showPhotoPicker(){
        imagePicker?.selectImage()
    }
}


// MARK: ImagePickerDelegate
extension SVHealthActionAddProofLandingViewController: VIAImagePickerDelegate{
    
    func pass(imageInfo: [String : Any]) {
        let fileName = UInt(Date().timeIntervalSince1970 * 1000)
        let image = imageInfo[UIImagePickerControllerOriginalImage] as! UIImage
        
        if var fileURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            fileURL.appendPathComponent("\(fileName)")
            
            do {
                if let pngImageData = UIImagePNGRepresentation(image) {
                    fileURL.appendPathExtension("png")
                    try pngImageData.write(to: fileURL, options: .atomic)
//                    viewModel?.saveAvatar(path: fileURL.path)
                    self.performSegue(withIdentifier: "showAttachments", sender: self)
                }
                
                self.imagePicker?.dismissImagePicker()
            } catch {
                
                print("SaveFailed: \(error)")
                self.imagePicker?.dismissImagePicker()
            }
        }
    }
    
    func show(alert imageSouceSelector: UIAlertController) {
        imageSouceSelector.view.tintColor = UIColor.genericErrorAlertButtonText()
        self.present(imageSouceSelector, animated: true, completion: nil)
    }
    
    func present(imagePicker: UIImagePickerController) {
        imagePicker.view.tintColor = UIColor.genericErrorAlertButtonText()
        self.present(imagePicker, animated: true, completion: nil)
    }
}
