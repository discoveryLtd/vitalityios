//
//  SVDataAndPrivacyPolicyAgreementViewController.swift
//  VitalityActive
//
//  Created by Val Tomol on 01/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import VitalityKit
import VIAUtilities
import VIAUIKit
import VIACommon

class SVDataAndPrivacyPolicyViewController: CustomTermsConditionsViewController, KnowYourHealthTintable {
     let eventsProcessor = SVSubmissionHelper()
    
    // MARK: View Lifecycles
    override open func viewDidLoad() {
        super.viewDidLoad()
        
        // Get content model.
        self.viewModel = CustomTermsConditionsViewModel(articleId: AppConfigFeature.contentId(for: .SVDSConsentContent))
    }
    
    override func configureAppearance() {
        super.configureAppearance()
        
        navigationController?.makeNavigationBarInvisible()
        navigationController?.navigationBar.isHidden = true
    }
    
    // MARK: Action Buttons
    override func leftButtonTapped(_ sender: UIBarButtonItem?) {
        self.performSegue(withIdentifier: "unwindFromSVDataAndPrivacyPolicyAgreementToSummary", sender: nil)
    }
    
    override func rightButtonTapped(_ sender: UIBarButtonItem?) {
        self.showHUDOnWindow()
        
        // TODO: Question on what to use.
        // SVDataPrivacyAgree = 123
        // ScreenVaccsDSAgree = 211
        let events = [EventTypeRef.SVDataPrivacyAgree]
        viewModel?.agreeToTermsAndConditions(with: events, completion: { [weak self] (error) in
            self?.hideHUDFromWindow()
            guard error == nil else {
                self?.handleErrorOccurred(error)
                return
            }
            self?.submitSV()
        })
    }
    
    func submitSV() {
        self.showHUDOnWindow()
        
        eventsProcessor.setupDelegate(self)
        eventsProcessor.processEvents(completion: { [weak self] error in
            self?.hideHUDFromWindow()
            guard error == nil else {
                self?.handleErrorOccurred(error)
                return
            }
            self?.performSegue(withIdentifier: "showCompletion", sender: nil)
        })
    }
    
    func handleErrorOccurred(_ error: Error?) {
        guard error == nil else {
            debugPrint(error as Any)
            switch error {
            case is BackendError:
                self.handleBackendErrorWithAlert(error as! BackendError, tryAgainAction: { [weak self] in
                    self?.rightButtonTapped(nil)
                })
            default:
                self.displayUnknownServiceErrorOccurredAlert()
            }
            self.hideHUDFromWindow()
            return
        }
    }
}

extension SVDataAndPrivacyPolicyViewController: SVPhotoUploadDelegate{
    
    public func displayImageUploadErrorMessage(with completion: @escaping (_ resubmit: Bool) -> Void){
        let cancelAction = UIAlertAction(title: CommonStrings.CancelButtonTitle24, style: UIAlertActionStyle.cancel) {
            [weak self] (alertAction) in
            completion(false)
            self?.hideHUDFromWindow()
        }
        let tryAgain = UIAlertAction(title: CommonStrings.TryAgainButtonTitle43, style: UIAlertActionStyle.default) { (alertAction) in
            completion(true)
        }
        
        let failedToSubmitVHCAlert = UIAlertController(title: CommonStrings.PrivacyPolicy.UnableToCompleteAlertTitle115, message: CommonStrings.Sv.AlertUnableToCompleteMessage1036, preferredStyle: .alert)
        
        failedToSubmitVHCAlert.addAction(cancelAction)
        failedToSubmitVHCAlert.addAction(tryAgain)
        self.present(failedToSubmitVHCAlert, animated: true, completion: nil)
    }
}
