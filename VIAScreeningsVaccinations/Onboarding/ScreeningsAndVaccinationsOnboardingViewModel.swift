import VitalityKit
import VitalityKit
import VIAUIKit

public class ScreeningsAndVaccinationsOnboardingViewModel: AnyObject, OnboardingViewModel {
    public var heading: String {
        return CommonStrings.HomeCard.CardSectionTitle365
    }
    
    public var contentItems: [OnboardingContentItem] {
        var items = [OnboardingContentItem]()

        items.append(OnboardingContentItem(heading: CommonStrings.Sv.OnboardingSection1Title1002,
                                           content: CommonStrings.Sv.OnboardingSection1Message1003,
                                           image: VIAScreeningsVaccinationsAsset.doctorSmall.image))
        items.append(OnboardingContentItem(heading: CommonStrings.Sv.OnboardingSection2Title1004,
                                           content: CommonStrings.Sv.OnboardingSection2Message1005,
                                           image: VIAScreeningsVaccinationsAsset.checkMarkSmall.image))
        items.append(OnboardingContentItem(heading: CommonStrings.Sv.OnboardingSection3Title1006,
                                           content: CommonStrings.Sv.OnboardingSection3Message1007,
                                           image: VIAScreeningsVaccinationsAsset.pointsSmall.image))
        return items
    }
    
    public var buttonTitle: String {
        return CommonStrings.Sv.Onboarding.GetStartedButton131
    }
    
    public var alternateButtonTitle: String? {
        return CommonStrings.LearnMoreButtonTitle104
    }
    
    public var gradientColor: Color {
        return .green
    }
    
    public var mainButtonHighlightedTextColor: UIColor? {
        return .knowYourHealthGreen()
    }
}
