import VitalityKit
import VIAUIKit
import VIAUtilities

class ScreeningsAndVaccinationsOnboardingViewController: VIAOnboardingViewController {
    
    var landingCellsGroupData = SAVLandingDataController().dataForGroupsFromRealm()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.viewModel = ScreeningsAndVaccinationsOnboardingViewModel()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        VIAAppearance.setGlobalTintColorToKnowYourHealth()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.navigationBar.isHidden = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.hideBackButtonTitle()
    }
    
    override func mainButtonTapped(_ sender: UIButton) {
        AppSettings.setHasShownSAVOnboarding()
        self.dismiss(animated: true, completion: nil)
    }

    // MARK: Learn More button
    
    override func alternateButtonTapped(_ sender: UIButton) {
        self.performSegue(withIdentifier: "learnMoreSegue", sender: self)
    }
    
    
    public override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? SVLearnMoreViewController{
            vc.fromOnboarding           = true
            vc.landingCellsGroupData    = landingCellsGroupData.sorted {
                $0.groupName.localizedCompare($1.groupName) == .orderedAscending
            }
        }
    }
}
