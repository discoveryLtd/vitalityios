//
//  SVLandingViewController+Helper.swift
//  VitalityActive
//
//  Created by OJ Garde on 2/15/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import VIAUtilities

extension SVLearnMoreViewController{
    
    /**
     * getScreeningsTotalPotentialPoints
     *
     * returns Total Potential Points for Screenings
     */
    public func getScreeningsTotalPotentialPoints() -> Int{
        return getPotentialPoints(for: EventTypeRef.configuredTypesforSAVScreenings())
    }
    
    /**
     * getVaccinationsTotalPotentialPoints
     *
     * returns Total Potential Points for Vaccinations
     */
    public func getVaccinationsTotalPotentialPoints() -> Int{
        return getPotentialPoints(for: EventTypeRef.configuredTypesforSAVVaccinations())
    }
    
    private func getPotentialPoints(for events:[EventTypeRef]) -> Int{
        /**
         * Initial and Default value for points is 0
         */
        var points = 0
        for data in landingCellsGroupData{
            /**
             * If Event Potential Points is greater than 0 and event type is valid,
             * add the potential points to our holder.
             */
            if data.potentialPoints > 0 && events.contains(data.eventType){
                points += data.potentialPoints
            }
        }
        
        return points
    }
}

extension SVLearnMoreViewController{
    
    /**
     * getScreeningsTotalEarnedPoints
     *
     * returns Total Earned Points for Screenings
     */
    public func getScreeningsTotalEarnedPoints() -> Int{
        return getEarnPoints(for: EventTypeRef.configuredTypesforSAVScreenings())
    }
    
    /**
     * getVaccinationsTotalEarnedPoints
     *
     * returns Total Potential Points for Vaccinations
     */
    public func getVaccinationsTotalEarnedPoints() -> Int{
        return getEarnPoints(for: EventTypeRef.configuredTypesforSAVVaccinations())
    }
    
    private func getEarnPoints(for events:[EventTypeRef]) -> Int{
        /**
         * Initial and Default value for points is 0
         */
        var points = 0
        for data in landingCellsGroupData{
            /**
             * If Event Earned Points is greater than 0 and event type is valid,
             * add the earned points to our holder.
             */
            if data.earnedPoints > 0 && events.contains(data.eventType){
                points += data.earnedPoints
            }
        }
        
        return points
    }
}


extension SVLearnMoreViewController{
    
    public func getScreenings(_ fromOnboarding: Bool = false) -> [SVLearnMoreListDataModel]{
        return getList(for: EventTypeRef.configuredTypesforSAVScreenings(), fromOnboarding: fromOnboarding)
    }
    
    public func getVaccinations(_ fromOnboarding: Bool = false) -> [SVLearnMoreListDataModel]{
        return getList(for: EventTypeRef.configuredTypesforSAVVaccinations(), fromOnboarding: fromOnboarding)
    }
    
    private func getList(for events:[EventTypeRef], fromOnboarding: Bool = false) -> [SVLearnMoreListDataModel]{
        /**
         * Initial and Default value for points is 0
         */
        var temp = [SVLearnMoreListDataModel]()
        for data in landingCellsGroupData{
            
            /**
             * If from on boarding screen, don't check the potential points.
             * add the potential points to our holder.
             */
            if fromOnboarding{
                if events.contains(data.eventType){
                    temp.append(SVLearnMoreListDataModel(groupName: data.groupName,
                                                         typeKey:   data.eventType,
                                                         contentId: SVLearnMoreDataController(data.eventType).contentId))
                }
            }
                /**
                 * If Event Potential Points is greater than 0 and event type is valid,
                 * add the potential points to our holder.
                 */
            else{
                if data.potentialPoints > 0 && events.contains(data.eventType){
                    temp.append(SVLearnMoreListDataModel(groupName: data.groupName,
                                                         typeKey:   data.eventType,
                                                         contentId: SVLearnMoreDataController(data.eventType).contentId))
                }
            }
        }
        
        return temp
    }
    
}
