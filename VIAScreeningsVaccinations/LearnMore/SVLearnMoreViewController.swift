//
//  SAVLearnMoreViewController.swift
//  VitalityActive
//
//  Created by Von Kervin R. Tuico on 13/11/2017.
//  Copyright © 2017 Glucode. All rights reserved.
//

import VIAUIKit
import VitalityKit
import VIAUtilities
import VIACommon

class SVLearnMoreViewController: VIATableViewController {
    
    /* External Properties */
    public var landingCellsGroupData    = [SAVLandingCellData]()
    public var fromOnboarding           = false
    
    
    /* Internal Properties */
    fileprivate let SEGUE_LIST                              = "segueLearnMoreList"
    fileprivate var viewModel: [SVLearnMoreListDataModel]?  = []
    fileprivate var selectedType: ProductFeatureTypeRef?    = .Unknown
    fileprivate var hasScreenings:Bool                      = false
    fileprivate var hasVaccinations:Bool                    = false
    fileprivate var hasLoadedContent                        = false
    fileprivate var htmlContentHeight: CGFloat              = 0.0
    fileprivate var validContent:String                     = ""
    
    let landingViewModel = SVLearnMoreLandingViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = landingViewModel.title
        setupNavBar()
        configureAppearance()
        configureTableView()
        
        self.hasScreenings      = fromOnboarding ?  true : getScreeningsTotalPotentialPoints() > 0
        self.hasVaccinations    = fromOnboarding ?  true : getVaccinationsTotalPotentialPoints() > 0
    }
    
    override public func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override public func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.hideBackButtonTitle()
    }
    
    func configureAppearance() {
        VIAAppearance.setGlobalTintColorToKnowYourHealth()
        navigationController?.makeNavigationBarTransparent()
        navigationController?.navigationBar.barTintColor = UIColor.white
    }
    
    func setupNavBar() {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        
        self.hideBackButtonTitle()
    }
    
    func configureTableView() {
        self.tableView.register(VIAGenericContentCell.nib(), forCellReuseIdentifier: VIAGenericContentCell.defaultReuseIdentifier)
        self.tableView.register(VIATableViewCell.nib(), forCellReuseIdentifier: VIATableViewCell.defaultReuseIdentifier)
        self.tableView.estimatedRowHeight   = 75
        self.tableView.sectionHeaderHeight  = 0
        self.tableView.sectionFooterHeight  = 0
        self.tableView.estimatedSectionHeaderHeight = 0
        self.tableView.contentInset     = UIEdgeInsets(top: 25, left: 0, bottom: 0, right: 0)
        self.tableView.backgroundColor  = .white
        self.tableView.tableFooterView  = UIView() /* Set this to remove the preceeding empty rows in the table. */
    }
    
    /*
     *   Tableview Delegates and DataSource.
     */
    
    enum Sections: Int, EnumCollection {
        case Content = 0
        case Measurables = 1
        
        func numberOfRows() -> Int {
            switch self {
            case .Content:
                return 1
            case .Measurables:
                return MeasurableItems.allValues.count
            }
        }
    }
    
    enum MeasurableItems: Int, EnumCollection {
        case Screenings
        case Vaccinations
        
        func title() -> String? {
            switch self {
            case .Screenings:
                return CommonStrings.Sv.ScreeningsTitle1012
            case .Vaccinations:
                return CommonStrings.Sv.VaccinationsTitle1013
            }
        }
        
        func iconName() -> UIImage {
            switch self {
            case .Screenings:
                return VIAScreeningsVaccinationsAsset.savCholesterolSmall.image
            case .Vaccinations:
                return VIAScreeningsVaccinationsAsset.savVaccinationsLogo.image
            }
        }
    }
    
    /* Number Sections in the table. */
    override public func numberOfSections(in tableView: UITableView) -> Int {
        return Sections.allValues.count
    }
    
    /* Number of Rows per section in the table. */
    override public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let section = Sections(rawValue: section)
        if section == .Content{
            return landingViewModel.contentItems.count
        }else if section == .Measurables{
            return hasScreenings && hasVaccinations ? 2 : 1
        }
        return 0
    }
    
    /* Check and collapse Screenings or Vaccinations if not applicable */
    override public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let section = Sections(rawValue: indexPath.section)
        let row = MeasurableItems(rawValue: indexPath.row)
        
        if indexPath.row == 2 {
            return VIAApplicableFeatures.default.participatingPartnersLinkHeight()
        }
        
        if section == .Content {
            return (hasLoadedContent && htmlContentHeight != 0) ? htmlContentHeight: UITableViewAutomaticDimension
        }else if section == .Measurables{
            return 50
        }else{
            return 0
        }
    }
    
    override public func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let section = Sections(rawValue: indexPath.section) {
            if section == .Content && indexPath.row != 4 {
                cell.separatorInset = UIEdgeInsetsMake(0, max(UIScreen.main.bounds.size.width, UIScreen.main.bounds.size.height), 0, 0)
            }
        }
    }
    
    /* Display table contents */
    public override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let section = Sections(rawValue: indexPath.section)
        if section == .Content {
            return self.configureContentCell(at: indexPath)
        } else if section == .Measurables {
            return self.configureMeasurableItemCell(indexPath)
        }
        
        return tableView.dequeueReusableCell(withIdentifier: VIATableViewCell.defaultReuseIdentifier, for: indexPath)
    }
    
    override public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let section = Sections(rawValue: indexPath.section)
        if section == .Measurables {
            
            switch indexPath.row {
            case 0:
                onScreeningsSelected(self)
                break
            case 1:
                onVaccinationsSelected(self)
                break
            default:
                break
            }
        }
    }
    
    func configureItemFormatOnCell (_ cell: UITableViewCell, _ header: UIFont, _ content: UIFont, _ icon: UIImage? = nil) {
        let type = cell as! VIAGenericContentCell
        
        type.cellImage          = icon
        type.customHeaderFont   = header
        type.customContentFont  = content
    }
    
    func configureMeasurableItemCell(_ indexPath: IndexPath) -> VIATableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: VIATableViewCell.defaultReuseIdentifier, for: indexPath) as! VIATableViewCell
        let content = MeasurableItems.allValues[indexPath.row]
        
        cell.textLabel?.text        = content.title()
        cell.imageView?.image       = content.iconName().templatedImage
        cell.imageView?.tintColor   = UIColor.currentGlobalTintColor()
        
        /* Only show the disclosure indicator if has valid row/s */
        if let section = Sections(rawValue: indexPath.section), let row = MeasurableItems(rawValue: indexPath.row){
            if section == .Measurables && row == .Screenings && hasScreenings
                || section == .Measurables && row == .Vaccinations && hasVaccinations{
                cell.accessoryType = .disclosureIndicator
            }
        }
        return cell
    }
    
    func configureContentCell(at indexPath: IndexPath) -> VIAGenericContentCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: VIAGenericContentCell.defaultReuseIdentifier, for: indexPath) as! VIAGenericContentCell
        
        cell.isUserInteractionEnabled = false
        
        let item = landingViewModel.contentItems[indexPath.row]
        
        if indexPath.row == 0 {
            cell.customHeaderFont = landingViewModel.headingTitleFont
            cell.customContentFont = landingViewModel.headingMessageFont
        } else if indexPath.row == 2{
            let customView = UIView(frame: CGRect(x: 0, y: 0, width: cell.bounds.size.width, height: 50))
            let button : UIButton = UIButton(type: .custom)
            button.frame = CGRect(x: 0, y: 0, width: cell.bounds.size.width, height: 50)
            button.setTitle(CommonStrings.Sv.PartnerTitle1029, for: .normal)
            button.setTitleColor(UIColor.currentGlobalTintColor(), for: .normal)
            button.contentHorizontalAlignment = UIControlContentHorizontalAlignment.left
            button.titleEdgeInsets.left = 76
            button.titleEdgeInsets.bottom = 10
            button.addTarget(self, action: #selector(participatingPartnersClick(_ :) ), for: .touchUpInside)
            customView.addSubview(button)
            cell.addSubview(customView)
            cell.isUserInteractionEnabled = true
        } else {
            cell.customHeaderFont = landingViewModel.sectionTitleFont
            cell.customContentFont = landingViewModel.sectionMessageFont
        }
        cell.header = item.header
        cell.content = item.content
        cell.cellImage = item.image
        cell.layoutIfNeeded()
        return cell
    }
    
    // MARK: - Navigation
    
    public override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? SVLearnMoreListViewController{
            if segue.identifier == SEGUE_LIST{
                vc.type = self.selectedType
                vc.data = self.viewModel
            }
        }
    }
    
    func participatingPartnersClick(_ sender: Any) {
        self.performSegue(withIdentifier: "segueParticipatingPartners", sender: self)
    }
    
    func onScreeningsSelected(_ sender: Any) {
        self.viewModel     =  getScreenings(fromOnboarding)
        self.selectedType  = .Screenings
        self.performSegue(withIdentifier: SEGUE_LIST, sender: self)
    }
    
    func onVaccinationsSelected(_ sender: Any) {
        self.viewModel     =  getVaccinations(fromOnboarding)
        self.selectedType  = .Vaccinations
        self.performSegue(withIdentifier: SEGUE_LIST, sender: self)
    }
}

// MARK: Data + Networking
extension SVLearnMoreViewController : CMSConsumer {
    
    public func loadArticleContent() {
        if !hasLoadedContent {
            showHUDOnView(view: self.view)
            
            getCMSArticleContent(articleId: "sv-learn-more", completion: { [weak self] error, content in
                self?.hideHUDFromView(view: self?.view)
                if let error = error {
                    self?.handle(error: error)
                }
                if let validContent = content {
                    self?.validContent = validContent
                    self?.tableView.reloadData()
                }
            })
        }
    }
    
    private func handle(error: Error) {
        switch error {
        case is BackendError:
            if let backendError = error as? BackendError {
                self.handleBackendErrorWithAlert(backendError, tryAgainAction: { [weak self] in
                    // TODO: Handle fallback
                })
            }
            break
        case is CMSError:
            displayErrorWithUnwindAction()
            break
        default:
            displayErrorWithUnwindAction()
            break
        }
    }
    
    private func displayErrorWithUnwindAction() {
        displayUnknownServiceErrorOccurredAlert(okAction: { [weak self] in
            // TODO: Handle fallback
        })
    }
}

// MARK: Web View Delegate
extension SVLearnMoreViewController: UIWebViewDelegate{
    public func webViewDidStartLoad(_ webView: UIWebView) {
        /* Disable user interaction */
        webView.stringByEvaluatingJavaScript(from: "document.documentElement.style.webkitUserSelect='none'")
        webView.stringByEvaluatingJavaScript(from: "document.documentElement.style.webkitTouchCallout='none'")
    }
    
    public func webViewDidFinishLoad(_ webView: UIWebView) {
        htmlContentHeight = webView.scrollView.contentSize.height
        
        self.tableView.reloadData()
    }
}
