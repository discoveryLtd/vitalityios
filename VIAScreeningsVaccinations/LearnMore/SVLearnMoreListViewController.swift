//
//  SVLearnMoreListViewController.swift
//  VitalityActive
//
//  Created by OJ Garde on 2/14/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import VIAUIKit
import VIACommon
import VitalityKit
import VIAUtilities


public struct SVLearnMoreListDataModel{
    var groupName: String
    var typeKey:EventTypeRef
    var contentId: String
}

public class SVLearnMoreListViewController: VIATableViewController{
    /* External Properties */
    /* Must be supplied by the sender */
    public var data: [SVLearnMoreListDataModel]?
    public var type: ProductFeatureTypeRef?
    
    /* Internal Properties */
    fileprivate var viewModel = [SVLearnMoreListDataModel]()
    fileprivate var productFeatureType: ProductFeatureTypeRef = .Unknown
    fileprivate var SEGUE_DETAILS = "showLearnMoreDetail"
    
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        
        
        /* Validate properties before proceeding */
        guard let data = self.data, let type = self.type else{
            return
        }
        self.viewModel = data
        self.productFeatureType = type
        
        self.configureAppearance()
        self.configureTableView()
    }
    
    override public func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        /* Always load the data in view will appear or in view did appear */
        self.tableView.reloadData()
    }
    
    // MARK: - UITableView datasource
    
    override public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if !viewModel.isEmpty {
            return viewModel.count
        }
        return 0
    }
    
    // MARK: UITableView delegate
    
    override public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        detailsClick(self)
    }
    
    override public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return self.configureContentCell(indexPath)
    }
    // MARK: - Navigation
    
    public override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == SEGUE_DETAILS,
            let vc = segue.destination as? SVLearnMoreWebViewController,
            let indexPath = self.tableView.indexPathForSelectedRow {
            
            let contentId   = self.viewModel[indexPath.row].contentId
            vc.viewModel    = SVLearnMoreViewModel(articleId: contentId)
            vc.title        = self.viewModel[indexPath.row].groupName
        }
    }
    
    @IBAction func detailsClick(_ sender: Any) {
        self.performSegue(withIdentifier: SEGUE_DETAILS, sender: self)
    }
}

extension SVLearnMoreListViewController{
    
    
    func configureContentCell(_ indexPath: IndexPath) -> VIATableViewCell {
        let cellLayout = self.tableView.dequeueReusableCell(withIdentifier: VIATableViewCell.defaultReuseIdentifier, for: indexPath)
        let cell = cellLayout as! VIATableViewCell
        let items = self.viewModel[indexPath.row]
        
        cell.labelText = items.groupName
        cell.accessoryType = .disclosureIndicator
        
        return cell
    }
    
    override public func configureStatusView(_ view: UIView?) {
        removeStatusView()
        
        guard let view = view else { return }
        
        self.view.addSubview(view)
        
        view.snp.makeConstraints { (make) in
            make.width.equalToSuperview()
            make.left.right.top.equalToSuperview()
            make.height.equalToSuperview().offset(self.tableView.contentOffset.y)
        }
    }
    
    override public func removeStatusView() {
        self.view.subviews.forEach { (view) in
            if view is VIAStatusView {
                view.removeFromSuperview()
            }
        }
    }
    
}

extension SVLearnMoreListViewController{
    
    func configureAppearance() {
        VIAAppearance.setGlobalTintColorToPrimaryColor()
        navigationController?.makeNavigationBarTransparent()
        tableView.tableFooterView = UIView()
        navigationController?.navigationBar.barTintColor = UIColor.white
        self.hideBackButtonTitle()
        
        if productFeatureType == .Screenings{
            self.title = CommonStrings.Sv.ScreeningsTitle1012
        }else if productFeatureType == .Vaccinations{
            self.title = CommonStrings.Sv.VaccinationsTitle1013
        }else{
            self.title = ""
        }
    }
    
    func configureTableView() {
        self.tableView.register(VIATableViewCell.nib(), forCellReuseIdentifier: VIATableViewCell.defaultReuseIdentifier)
        self.tableView.estimatedRowHeight = 75
        self.tableView.sectionHeaderHeight = 0
        self.tableView.sectionFooterHeight = 0
        self.tableView.estimatedSectionHeaderHeight = 0
        self.tableView.estimatedSectionFooterHeight = 0
        self.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
}
