//
//  SVLearnMoreViewModel.swift
//  VitalityActive
//
//  Created by OJ Garde on 2/14/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import VIAUtilities

public class SVLearnMoreDataController{
    private var type:EventTypeRef? = .Unknown
    
    required public init(_ type: EventTypeRef){
        self.type = type
    }
    
    public var contentId:String{
        get{
            /* If dictionary contains key = type, return the value else return empty string */
            if let type = self.type{
                return learnMoreContentIds()[type] ?? ""
            }
            return ""
        }
    }
    
    private func learnMoreContentIds() -> Dictionary<EventTypeRef, String> {
        let dict:Dictionary = [
            EventTypeRef.GastricCancer  : "sv-learn-more-gastric-cancer",
            EventTypeRef.Glaucoma       : "sv-learn-more-glaucoma",
            EventTypeRef.HIV            : "sv-learn-more-hiv",
            EventTypeRef.FOBT           : "sv-learn-more-fobt",
            EventTypeRef.Colonoscopy    : "sv-learn-more-colonoscopy",
            EventTypeRef.SkinCancer     : "sv-learn-more-skin-cancer",
            EventTypeRef.LungCancer     : "sv-learn-more-lung-cancer",
            EventTypeRef.Cardiovascular : "sv-learn-more-cardiovascular",
            EventTypeRef.OvarianCancer  : "sv-learn-more-ovarian-cancer",
            EventTypeRef.LiverCancer    : "sv-learn-more-liver-cancer",
            EventTypeRef.Flu            : "sv-learn-more-flu",
            EventTypeRef.Childhood      : "sv-learn-more-childhood",
            EventTypeRef.HPV            : "sv-learn-more-hpv",
            EventTypeRef.Pneumococcal   : "sv-pneumococcal-vaccination",
            EventTypeRef.Zoster         : "sv-learn-more-zoster",
            EventTypeRef.HepatitisB     : "sv-learn-more-hepatitis-b",
            EventTypeRef.MeningCococcal : "sv-learn-more-meningcococcal",
            EventTypeRef.PapSmear       : "sv-pap-smear",
            EventTypeRef.Mammogram      : "sv-mammogram",
            EventTypeRef.DentalCheckups : "sv-dental-check-up",
            EventTypeRef.EyeTest        : "sv-eye-test",
            EventTypeRef.CotinineTest   : "sv-cotinine-test",
            EventTypeRef.AbdomAorticUS  : "sv-abdominal-aortic-ultrasound"
        ]
        
        return dict
    }
}
