//
//  SAVParticipatingPartnersViewModel.swift
//  VitalityActive
//
//  Created by Steven Layug on 11/17/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation

enum SAVParticipatingPartnersViewModel {
    
    case samplePartner1
    case samplePartner2
    case samplePartner3
    
    var label: String {
        switch self {
        case .samplePartner1:
            return "Clicks"
        case .samplePartner2:
            return "Dis-Chem"
        case .samplePartner3:
            return "Easy Way"
        }
    }
    
    var image: UIImage {
        switch self {
        case .samplePartner1:
            return VIAScreeningsVaccinationsAsset.samplePartner1.image
        case .samplePartner2:
            return VIAScreeningsVaccinationsAsset.samplePartner2.image
        case .samplePartner3:
            return VIAScreeningsVaccinationsAsset.samplePartner3.image
        }
    }
    
    var Description: String {
        switch self {
        case .samplePartner1:
            return "You will get one free, gender, age, and life-specific Screenings and Vaccinations per year fron your Screening Benefit as this partner is participatiing on the Vitality Wellness Network.\n\nIf you don't have all the screening tests and vaccinations on the same day, you will have to pay for the test and screenings alter from you Medical Savings Account of your pocket"
        case .samplePartner2:
            return "You will get one free, gender, age, and life-specific Screenings and Vaccinations per year fron your Screening Benefit as this partner is participatiing on the Vitality Wellness Network.\n\nIf you don't have all the screening tests and vaccinations on the same day, you will have to pay for the test and screenings alter from you Medical Savings Account of your pocket"
        case .samplePartner3:
            return "You will get one free, gender, age, and life-specific Screenings and Vaccinations per year fron your Screening Benefit as this partner is participatiing on the Vitality Wellness Network.\n\nIf you don't have all the screening tests and vaccinations on the same day, you will have to pay for the test and screenings alter from you Medical Savings Account of your pocket"
        }
    }
    
}
