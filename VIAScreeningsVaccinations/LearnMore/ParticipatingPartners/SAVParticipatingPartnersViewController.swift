//
//  SAVParticipatingPartnersViewController.swift
//  VitalityActive
//
//  Created by Steven Layug on 11/16/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

import VIAUIKit
import VitalityKit
import VIAUtilities

class SAVParticipatingPartnersViewController: VIATableViewController {

    var tableViewItems = [SAVParticipatingPartnersViewModel.samplePartner1, SAVParticipatingPartnersViewModel.samplePartner2, SAVParticipatingPartnersViewModel.samplePartner3]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Participating Partners"
        
        setupNavBar()
        configureAppearance()
        setupTableViewHeader()
        configureTableView()
        
    }
    
    override public func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.hideBackButtonTitle()
    }
    
    func configureAppearance() {
        VIAAppearance.setGlobalTintColorToKnowYourHealth()
        navigationController?.makeNavigationBarTransparent()
        navigationController?.navigationBar.barTintColor = UIColor.white
    }
    
    func setupNavBar() {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        
        self.hideBackButtonTitle()
    }
    
    func setupTableViewHeader() {
        let header = ParticipatingPartnersListHeaderView.viewFromNib(owner: self)!
        header.contentLabel.lineBreakMode = .byWordWrapping
        header.title = CommonStrings.Sv.VisitPartnerTitle1034
        header.content = CommonStrings.Sv.VisitPartnerMessage1035
        self.tableView.tableHeaderView = header
        self.tableView.tableHeaderView?.backgroundColor = UIColor.tableViewBackground()
        self.tableView.sizeHeaderViewToFit()
    }
    
    func configureTableView() {
        self.tableView.register(SAVParticipatingPartnerCell.nib(), forCellReuseIdentifier: SAVParticipatingPartnerCell.defaultReuseIdentifier)
        self.tableView.register(VIATableViewCell.nib(), forCellReuseIdentifier: VIATableViewCell.defaultReuseIdentifier)
        self.tableView.estimatedRowHeight = 75
        self.tableView.sectionHeaderHeight = 0
        self.tableView.sectionFooterHeight = 0
        self.tableView.estimatedSectionHeaderHeight = 0
        self.tableView.estimatedSectionFooterHeight = 0
        self.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        self.tableView.separatorInset = UIEdgeInsets(top: 0, left: 80, bottom: 0, right: 0)
        self.tableView.backgroundColor = UIColor.tableViewBackground()
        self.tableView.tableFooterView = UIView()
    }
    
    override public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.tableViewItems.count
    }
        
    public override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return self.configureContentCell(indexPath)
    }
    
    override public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.detailsClick(self)
    }
    
    func configureContentCell(_ indexPath: IndexPath) -> SAVParticipatingPartnerCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: SAVParticipatingPartnerCell.defaultReuseIdentifier, for: indexPath) as! SAVParticipatingPartnerCell
        let content = self.tableViewItems[indexPath.row]
        cell.header = content.label
        cell.cellImage = content.image
        cell.imageView?.tintColor = UIColor.currentGlobalTintColor()
        cell.customHeaderFont = UIFont.onboardingCellContent()
        cell.accessoryType = .disclosureIndicator
        return cell
    }
    
    public override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "seguePartnerDetails",
            let nextScene = segue.destination as? SAVParticipatingPartnerDetailsViewController,
            let indexPath = self.tableView.indexPathForSelectedRow {
            
            let selectedPartner = self.tableViewItems[indexPath.row]
            nextScene.partnerDetails = selectedPartner
        }
    }
    
    @IBAction func detailsClick(_ sender: Any) {
        self.performSegue(withIdentifier: "seguePartnerDetails", sender: self)
    }

}
