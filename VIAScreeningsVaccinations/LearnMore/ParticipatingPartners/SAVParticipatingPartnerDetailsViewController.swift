//
//  SAVParticipatingPartnerDetailsViewController.swift
//  VitalityActive
//
//  Created by Steven Layug on 11/20/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

import VIAUIKit
import VitalityKit
import VIAUtilities

class SAVParticipatingPartnerDetailsViewController: VIATableViewController {

    var partnerDetails: SAVParticipatingPartnersViewModel?
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        self.title = partnerDetails?.label
        self.setupNavBar()
        self.configureAppearance()
        self.configureTableView()
    }
    
    override public func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.hideBackButtonTitle()
    }
    
    func setupNavBar() {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        
        self.hideBackButtonTitle()
    }
    
    func configureAppearance() {
        VIAAppearance.setGlobalTintColorToKnowYourHealth()
        navigationController?.makeNavigationBarTransparent()
        tableView.tableFooterView = UIView()
        navigationController?.navigationBar.barTintColor = UIColor.white
    }
    
    func configureTableView() {
        self.tableView.register(SAVPartnerImageCell.nib(), forCellReuseIdentifier: SAVPartnerImageCell.defaultReuseIdentifier)
        self.tableView.estimatedRowHeight = 75
        self.tableView.sectionHeaderHeight = 0
        self.tableView.sectionFooterHeight = 0
        self.tableView.estimatedSectionHeaderHeight = 0
        self.tableView.estimatedSectionFooterHeight = 0
        self.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        self.tableView.backgroundColor = .white
    }
    
    // MARK: - UITableView datasource
    
    override public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    // MARK: UITableView delegate
    
    override public func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.separatorInset = UIEdgeInsetsMake(0, max(UIScreen.main.bounds.size.width, UIScreen.main.bounds.size.height), 0, 0)
    }
    
    override public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return self.configureContentCell(indexPath)
    }
    
    func configureContentCell(_ indexPath: IndexPath) -> SAVPartnerImageCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: SAVPartnerImageCell.defaultReuseIdentifier, for: indexPath) as! SAVPartnerImageCell
        let header: String = (partnerDetails?.Description)!
        let icon: UIImage = (partnerDetails?.image)!
        cell.isUserInteractionEnabled = false
        cell.cellImageView.tintColor = UIColor.currentGlobalTintColor()
        cell.customHeaderFont = UIFont.bodyFont()
        cell.headerLabel.text = header
        cell.cellImage = icon
        cell.layoutIfNeeded()
        return cell
    }

}
