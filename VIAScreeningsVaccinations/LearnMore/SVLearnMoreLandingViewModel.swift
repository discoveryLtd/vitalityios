//
//  SVLearnMoreLandingViewModel.swift
//  VitalityActive
//
//  Created by Michelle R. Oratil on 23/04/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import VIAUIKit
import VitalityKit
import VIACommon

public class SVLearnMoreLandingViewModel: AnyObject, VIALearnMoreViewModel {
    
    public var title: String {
        return CommonStrings.Sv.LearnMoreTitle1082
    }
    
    public var contentItems: [LearnMoreContentItem] {
        var items = [LearnMoreContentItem]()
        
        items.append(LearnMoreContentItem(
            header: CommonStrings.Sv.LearnMoreMainTitle1025,
            content: CommonStrings.Sv.LearnMoreMainMessage1026
        ))
        
        items.append(LearnMoreContentItem(
            header: CommonStrings.Sv.LearnMoreSection1Title1027,
            content: CommonStrings.Sv.LearnMoreSection1Message1028,
            image: VIAScreeningsVaccinationsAsset.savHealthcareBig.image
        ))
        
        items.append(LearnMoreContentItem(
            header: "",
            content: ""
        ))

        items.append(LearnMoreContentItem(
            header: CommonStrings.Sv.LearnMoreSection2Title1030,
            content: CommonStrings.V.LearnMoreSection2Message1031,
            image: VIAScreeningsVaccinationsAsset.savCompletedBig.image
        ))

        items.append(LearnMoreContentItem(
            header: CommonStrings.Sv.LearnMoreSection3Title1032,
            content: CommonStrings.Sv.LearnMoreSection3Message1033,
            image: VIAScreeningsVaccinationsAsset.savEarnPointsBig.image
        ))
        
        return items
    }
    
    public var imageTint: UIColor {
        return .knowYourHealthGreen()
    }
}
