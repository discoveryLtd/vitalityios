//
//  ViewHistoryDataController.swift
//  VitalityActive
//
//  Created by Von Kervin R. Tuico on 06/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.
//

import RealmSwift

import VitalityKit
import VIAUIKit
import VIAUtilities

public struct SAVViewHistoryCellData {
    var savHistoryEventList: [SAVHistoryEvent]
    var historyEventModel: [SAVHistoryEvent]
    var eventDate: String
}

public class SAVViewHistoryDataController {
    var savViewHistoryRealm = DataProvider.newSAVRealm()
    class func sortedGroups(from realm: Realm) -> Array<VHCHealthAttributeGroup> {
        let results = Array(realm.allVHCHealthAttributeGroups())
        return results.sorted { ($0 as VHCHealthAttributeGroup).groupName() < ($1 as VHCHealthAttributeGroup).groupName() }
    }
    
    public func dataForGroupsFromRealm() -> [SAVViewHistoryCellData] {
        savViewHistoryRealm.refresh()
        
        var allGroupsData = [SAVViewHistoryCellData]()
        let savEvents = savViewHistoryRealm.allSAVHistoryEvents()
        
        /* Sort Events in Descending Order */
        let sortedSAVEvents = savEvents.sorted{ ($0 as SAVHistoryEvent).eventDateTime! > ($1 as SAVHistoryEvent).eventDateTime! }
        
        for event in sortedSAVEvents {
            
            if let date = event.eventDateTime {
                
                var savHistoryEventList = [SAVHistoryEvent]()
                savHistoryEventList.append(contentsOf: savEvents)
                var historyEventModel = [SAVHistoryEvent]()
                historyEventModel.append(event)

                let convertedEventDate = convertAPIDateToString(apiDate: date)
                debugPrint("Converted Date: \(convertedEventDate)")
                
//                let retainedDate = String(convertedEventDate.characters.prefix(10))
//                debugPrint("Retained Date: \(retainedDate)")
                if let dateFormat = stringToDate(stringDate: convertedEventDate) {
                    debugPrint("Date Format: \(dateFormat)")
                    let toHumanReadableFormat = dateToString(date: dateFormat)
                    debugPrint("Human Readable Format: \(toHumanReadableFormat)")
                    let isEventDateExist = allGroupsData.contains(where: { (data) -> Bool in
                        return ((data.eventDate) == toHumanReadableFormat)
                    })
                    
                    if isEventDateExist {
                        if isEventDateExist {
                            if var groupData = allGroupsData.filter( { ($0 as SAVViewHistoryCellData).eventDate == toHumanReadableFormat}).first {
                                allGroupsData.removeAll(where: { (data) -> Bool in
                                    return ((data.eventDate) == toHumanReadableFormat)
                                })
                                groupData.historyEventModel.append(event)
                                allGroupsData.append(groupData)
                            }
                        }
                    }
                    else {
                        let cellData = SAVViewHistoryCellData(savHistoryEventList: savHistoryEventList, historyEventModel: historyEventModel, eventDate: toHumanReadableFormat)
                        allGroupsData.append(cellData)
                    }
                }
            }
            
        }
        return allGroupsData
    }
    
    func convertAPIDateToString(apiDate: Date) -> String {
        let dateFormatter = DateFormatter.apiManagerFormatterWithMilliseconds()
        let date = dateFormatter.string(from: apiDate)
        
        if String.isNilOrEmpty(string: date) {
            let secondsDateFormatter = DateFormatter.apiManagerFormatter()
            
            return secondsDateFormatter.string(from: apiDate)
        }

        return date
    }
    
    func stringToDate(stringDate: String) -> Date? {
        let dateFormatter = DateFormatter.apiManagerFormatterWithMilliseconds()
        
        guard let date = dateFormatter.date(from: stringDate) else {
            let secondsDateFormatter = DateFormatter.apiManagerFormatter()
            
            return secondsDateFormatter.date(from: stringDate)
        }

        return date
    }
    
    func dateToString(date: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "E, dd MMM yyyy" //Your New Date format as per requirement change it own
        let stringDate = dateFormatter.string(from: date)
        return stringDate
    }
}
