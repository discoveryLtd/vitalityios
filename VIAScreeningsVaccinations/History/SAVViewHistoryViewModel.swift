//
//  ViewHistoryViewModel.swift
//  VitalityActive
//
//  Created by Von Kervin R. Tuico on 06/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.
//

import VitalityKit
import VIAUIKit
import VIAUtilities
import RealmSwift

public class SAVViewHistoryViewModel: AnyObject {
    
    // MARK: Properties
    let dataController = SAVViewHistoryDataController()
    var cellsGroupData = [SAVViewHistoryCellData]()
    
    // MARK: Functions
    
    func getCellDataFromDataController() {
        cellsGroupData.removeAll()
        cellsGroupData = dataController.dataForGroupsFromRealm()
    }
    
    // MARK: Networking - Screening
    func fetchHistoryAttributes(forceUpdate: Bool = false, completion: @escaping (Error?, Bool?) -> Void) {
        if SAVCache.isSAVDataOutdated() || forceUpdate == true {
            let partyId = DataProvider.newRealm().getPartyId()
            let tenantId = DataProvider.newRealm().getTenantId()
            //            let eventTypeGroups = DataProvider.newSAVRealm().allSAVEventTypeGroups()
            
            var eventTypes = [EventTypeRef]()
            //            for type in eventTypeGroups {
            //
            //                for points in type.eventTypes {
            //                    if points.totalPotentialPoints != 0 {
            //                        eventTypes.append(points.type)
            //                    }
            //                }
            //            }
            eventTypes.append(.DocumentUploadsSV)
            
            guard let effectiveFromDate = DataProvider.currentMembershipPeriodStartDate() else {
                return
            }
            
            guard let effectiveToDate = DataProvider.currentMembershipPeriodEndDate() else {
                return
            }
            
            let parameter = GetSAVEventByPartyParameter(partyId: partyId, effectiveFrom: effectiveFromDate, effectiveTo: effectiveToDate, eventTypesTypeKeys: eventTypes)
            
            Wire.Events.getEventByParty(tenantId: tenantId, request: parameter,
                                        completion: { error, response in
                                            if let error = error {
                                                completion(error, nil)
                                            } else {
                                                self.getCellDataFromDataController()
                                                completion(nil, nil)
                                            }
            })
            
        } else {
            getCellDataFromDataController()
            completion(nil, nil)
        }
    }
}

extension SAVViewHistoryViewModel: CMSConsumer {
    
    func getUploadedProof(refId: Int, completion: @escaping ((_: UIImage) -> Void)){
        
        self.downloadHistoryImage(referenceId: refId) { url, error in
            let defaultLogo = VIAScreeningsVaccinationsAsset.vhcCompletedBig.image
            guard url != nil, error == nil, let filePath = url?.path, FileManager.default.fileExists(atPath: filePath) else {
                debugPrint("Could not retrieve downloaded partner image from disk")
                return completion(defaultLogo)
            }

            if let img = UIImage(contentsOfFile: filePath) {
                debugPrint("Partner image retrieved successfully from service")
                return completion(img)
            }

            debugPrint("Falied to download partner image")
            return completion(defaultLogo)
        }
        
    }
    
}
