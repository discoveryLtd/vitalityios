//
//  SAVHistoryDetailsViewController.swift
//  VitalityActive
//
//  Created by Von Kervin R. Tuico on 13/11/2017.
//  Copyright © 2017 Glucode. All rights reserved.
//

import VIAUIKit
import VIACommon
import VIAUtilities
import VitalityKit

class SAVViewHistoryDetailsViewController: VIATableViewController {
    
    let viewModel = SAVViewHistoryViewModel()
    
    internal var heightForCell: CGFloat = 0.0
    
    /* Screenings & Vaccinations Container Variable */
    var screenings = [SAVAssociatedEvents]()
    var vaccinations = [SAVAssociatedEvents]()
    
    /* Image & PDF Container Variable */
    var pdfFile = UIImage()
    var logoImage: [UIImage] = []
    
    /* Data */
    var data: [SAVHistoryEvent] = []
    var savHistoryEventList = [SAVHistoryEvent]()
    var eventDate: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = eventDate
        self.navigationController?.navigationBar.backgroundImage(for: .default)
        
        setupNavBar()
        configureAppearance()
        configureTableView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        loadDetails()
    }
    
    func loadDetails() {
        screenings = [SAVAssociatedEvents]()
        vaccinations = [SAVAssociatedEvents]()
        
        /* Check if Data is Empty of Not */
        if data.count > 0 {
            var rawScreenings = [SAVAssociatedEvents]()
            var rawVaccinations = [SAVAssociatedEvents]()
            
            for aData in data {
                /* Check if AssociatedEvents is Empty or Not and Unwrapped its Contents */
                let associatedEvents = aData.associatedEvents.sorted(byKeyPath: "eventTypeName", ascending: true)
                if associatedEvents.count > 0 {
                    debugPrint("Associated Event: \(associatedEvents)")
                    /* Filter the Associated Events List */
                    for event in associatedEvents {
                        /* Sort the results into Screenings and Vaccinations */
                        if let categoryName = event.categoryName, categoryName == CommonStrings.Sv.ScreeningsTitle1012 {
                            debugPrint("Screening Event: \(event)")
                            rawScreenings.append(event)
                        } else {
                            debugPrint("Vaccination Event: \(event)")
                            rawVaccinations.append(event)
                        }
                    }
                }
            }
            
            screenings = rawScreenings.sorted(by: { $0.eventTypeName!.localizedCaseInsensitiveCompare($1.eventTypeName!) == ComparisonResult.orderedAscending })
            vaccinations = rawVaccinations.sorted(by: { $0.eventTypeName!.localizedCaseInsensitiveCompare($1.eventTypeName!) == ComparisonResult.orderedAscending})
            getImages()
        }
    }
    
    func getImages() {
        
        showHUDOnView(view: self.view)
        
        /* Check if Data is Empty of Not */
        if data.count > 0 {
            for aSAVHistory in data {
                /* Check if EventMetaDataOuts is Empty or Not and Unwrapped its Contents */
                let eventMetaDataOuts = aSAVHistory.eventMetaDataOuts
                if eventMetaDataOuts.count > 0 {
                    
                    for data in eventMetaDataOuts {
                        
                        if let referenceId = Int(data.value ?? "") {
                            viewModel.getUploadedProof(refId: referenceId) { img in
                                self.hideHUDFromView(view: self.view)
                                self.pdfFile = img
                                self.logoImage.append(self.pdfFile)
                                self.tableView.reloadData()
                            }
                        }
                    }
                }
            }
        }
    }
    
    func configureAppearance() {
        VIAAppearance.setGlobalTintColorToKnowYourHealth()
        navigationController?.makeNavigationBarTransparent()
        navigationController?.navigationBar.barTintColor = UIColor.white
    }
    
    func setupNavBar() {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        
        self.hideBackButtonTitle()
    }
    
    
    func configureTableView() {
        self.tableView.register(VIASubtitleTableViewCell.nib(), forCellReuseIdentifier: VIASubtitleTableViewCell.defaultReuseIdentifier)
        self.tableView.register(ImageCollectionTableViewCell.nib(), forCellReuseIdentifier: ImageCollectionTableViewCell.defaultReuseIdentifier)
        self.tableView.register(VIATableViewSectionHeaderFooterView.nib(), forHeaderFooterViewReuseIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier)
        self.tableView.estimatedRowHeight = 75
        self.tableView.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        self.tableView.tableFooterView = UIView()
    }
    
    /*
     *   Tableview Delegates and DataSource.
     */
    
    enum Sections: Int, EnumCollection {
        case Screenings = 0
        case Vaccinations = 1
        case Proofs = 2
    }
    
    public override func numberOfSections(in tableView: UITableView) -> Int {
        return Sections.allValues.count
    }
    
    public override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let section = Sections(rawValue: section)
        
        if section == .Screenings {
            if !screenings.isEmpty {
                return screenings.count
            }
            return 0
        }
        
        if section == .Vaccinations {
            if !vaccinations.isEmpty {
                return vaccinations.count
            }
        }
        
        if section == .Proofs {
            if !logoImage.isEmpty {
                return 1
            }
        }
        
        return 0
    }
    
    public override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let section = Sections(rawValue: indexPath.section)
        
        if section == .Screenings {
            return screeningsItemCell(indexPath)
        } else if section == .Vaccinations {
            return vacciantionsItemCell(indexPath)
        } else if section == .Proofs {
            if let imageCollectionCell = self.tableView.dequeueReusableCell(withIdentifier: ImageCollectionTableViewCell.defaultReuseIdentifier) {
                let collectionviewCell = self.uploadedProofsItemCell(cell: imageCollectionCell)
                self.heightForImageCollection(cell: collectionviewCell)
                return collectionviewCell
            }
        }
        
        return self.tableView.defaultTableViewCell()
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let section = Sections(rawValue: section)
        
        let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier)
        let footer = view as! VIATableViewSectionHeaderFooterView
        
        if section == .Screenings {
            if !screenings.isEmpty {
                footer.labelText = "\n\(CommonStrings.Sv.ScreeningsTitle1012.uppercased())"
                return view
            }
        } else if section == .Vaccinations {
            if !vaccinations.isEmpty {
                footer.labelText = "\n\(CommonStrings.Sv.VaccinationsTitle1013.uppercased())"
                return view
            }
        } else if section == .Proofs {
            if !logoImage.isEmpty {
                footer.labelText = "\n\(CommonStrings.SummaryScreen.UploadedProofTitle186.uppercased())"
                return view
            }
        }
        
        return nil
    }
    
    override public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let section = Sections(rawValue: indexPath.section)
        
        if section == .Proofs {
            
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    public override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let section = Sections(rawValue: indexPath.section)
        
        if section == .Proofs {
            return self.heightForCell
        }
        return UITableViewAutomaticDimension
    }
    
    func screeningsItemCell(_ indexPath: IndexPath) -> VIASubtitleTableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: VIASubtitleTableViewCell.defaultReuseIdentifier, for: indexPath) as! VIASubtitleTableViewCell
        let content = self.screenings[indexPath.row]
        
        if let eDate = self.stringToDate(stringDate: content.eventDateTime ?? "") {
            cell.contentText = CommonStrings.SummaryScreen.DateTestedTitle185(self.dateToString(date: eDate))
        }
        
        cell.headingText = content.eventTypeName
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        return cell
    }
    
    func vacciantionsItemCell(_ indexPath: IndexPath) -> VIASubtitleTableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: VIASubtitleTableViewCell.defaultReuseIdentifier, for: indexPath) as! VIASubtitleTableViewCell
        let content = self.vaccinations[indexPath.row]
        
        if let eDate = self.stringToDate(stringDate: content.eventDateTime ?? "")
        {
            cell.contentText = CommonStrings.SummaryScreen.DateTestedTitle185(self.dateToString(date: eDate))
        }
        
        cell.headingText = content.eventTypeName
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        return cell
    }
    
    func uploadedProofsItemCell(cell: UITableViewCell) -> ImageCollectionTableViewCell {
        if let imageCollectionViewCell = cell as? ImageCollectionTableViewCell {
            imageCollectionViewCell.setImageCollection(images: self.logoImage)
            imageCollectionViewCell.imageCollectionView?.bounces = false
            imageCollectionViewCell.viewWidth = self.view.frame.width
            imageCollectionViewCell.setupCollectionFlowLayoutSize()
            self.hideHUDFromView(view: self.view)
            return imageCollectionViewCell
        }
        return ImageCollectionTableViewCell()
    }
    
    func heightForImageCollection(cell: ImageCollectionTableViewCell) {
        let collectionViewFlowLayout = cell.imageCollectionView?.collectionViewLayout
        let cellPadding = collectionViewFlowLayout?.collectionViewContentSize
        let height = cellPadding?.height
        self.heightForCell = height ?? 0
        cell.updateImageCollectionViewSize(with:height)
    }
    
    func stringToDate(stringDate: String) -> Date? {
        let dateFormatter = DateFormatter.apiManagerFormatterWithMilliseconds()
        
        guard let date = dateFormatter.date(from: stringDate) else {
            let secondsDateFormatter = DateFormatter.apiManagerFormatter()
            
            return secondsDateFormatter.date(from: stringDate)
        }
        
        return date
    }
    
    func dateToString(date: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "E, dd MMM yyyy" //Your New Date format as per requirement change it own
        let stringDate = dateFormatter.string(from: date) //pass Date here
        return stringDate
    }
}
