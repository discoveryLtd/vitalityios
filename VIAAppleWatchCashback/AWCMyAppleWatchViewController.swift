//
//  AWCMyAppleWatchViewController.swift
//  VitalityActive
//
//  Created by Michelle R. Oratil on 07/05/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import VIAUIKit
import VIAUtilities
import VitalityKit
import VIACommon

class AWCMyAppleWatchViewController: VIATableViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "My Apple Watch" //CommonStrings.Dc.Landing.DeviceMenuItem1520
        initUI()
    }
    
    func initUI() {
        initNavigationBar()
        initTableView()
    }
    
    func initNavigationBar() {
        navigationController?.makeNavigationBarTransparent()
        navigationController?.navigationBar.barTintColor = UIColor.white
        
        self.hideBackButtonTitle()
    }
    
    func initTableView() {
        self.tableView.register(VIAInfoDetailCell.nib(), forCellReuseIdentifier: VIAInfoDetailCell.defaultReuseIdentifier)

        self.tableView.estimatedRowHeight = 96
        self.tableView.separatorInset = UIEdgeInsets(top: 0, left: 52, bottom: 0, right: 0)
        self.tableView.backgroundColor = UIColor.tableViewBackground()
    }
    
    enum Sections: Int, EnumCollection {
        case appleWatchDevice = 0
    }
    
    enum appleWatchDeviceFields: Int, EnumCollection, Equatable {
        case model = 0
        case invoiceNumber = 1
        case watchSerialNumber = 2
        case purchaseDate = 3
        
        func title() -> String? {
            switch self {
            case .model:
                return CommonStrings.Dc.LandingActivationPurchaseDetailModelField1490
            case .invoiceNumber:
                return CommonStrings.Dc.LandingActivationPurchaseDetailInvoiceNumberField1491
            case .watchSerialNumber:
                return "Watch Serial Number" //CommonStrings.Dc.LandingActivationPurchaseDetailDevSerialNumberField1493
            case .purchaseDate:
                return "Purchase Date" //CommonStrings.Dc.LandingActivationPurchaseDetailPurchaseAmtField1495
            }
        }
        
        // set the value of the fields here temporarily
        func values() -> String? {
            switch self {
            case .model:
                return "Apple Watch Series 1, 38mm Gold Aluminum Case"
            case .invoiceNumber:
                return "25G51FQ31" //CommonStrings.Dc.LandingActivationPurchaseDetailInvoiceNumberHint1492
            case .watchSerialNumber:
                return "G1415FG414V" //CommonStrings.Dc.LandingActivationPurchaseDetailDevSerialNumberHint1494
            case .purchaseDate:
                return "Wed, 1 June 2017"
            }
        }
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    
    
}

extension AWCMyAppleWatchViewController {
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return Sections.allValues.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return appleWatchDeviceFields.allValues.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return appleWatchDetailsCell(at: indexPath)
    }
    
    func appleWatchDetailsCell(at indexPath: IndexPath) -> UITableViewCell {
        
        let labels = appleWatchDeviceFields.allValues[indexPath.row]
        
        let cell = self.tableView.dequeueReusableCell(withIdentifier: VIAInfoDetailCell.defaultReuseIdentifier, for: indexPath)
        let inputFieldCell = cell as! VIAInfoDetailCell
        inputFieldCell.labelText = labels.title()
        inputFieldCell.valueText = labels.values()
        inputFieldCell.selectionStyle = .none
        return cell
    }
}
