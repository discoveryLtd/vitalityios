//
//  AWCDeviceActivatedViewController.swift
//  VitalityActive
//
//  Created by Michelle R. Oratil on 09/05/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import UIKit
import VIAUIKit
import VitalityKit
import VIAUtilities

import RealmSwift

class AWCDeviceActivatedViewController: VIACirclesViewController {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.viewModel = AWCDeviceActivatedViewModel()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func buttonTapped(_ sender: UIButton) {
        AppSettings.setHasLinkedAppleHealthDevice()
        self.performSegue(withIdentifier: "unwindToAWCLandingViewController", sender: self)
    }
}

public class AWCDeviceActivatedViewModel: AnyObject, CirclesViewModel {
    
    public var image: UIImage? {
        return VIAAppleWatchCashbackAsset.LinkAppleHealth.onboardingActivated.image
    }
    
    public var heading: String? {
        return CommonStrings.Dc.Success.ActivatedTitle1523
    }
    
    public var message: String? {
        return CommonStrings.Awc.Onboarding.SectionMessage2021
    }
    
    public var footnote: String? {
        return ""
    }
    
    public var buttonTitle: String? {
        return CommonStrings.GreatButtonTitle120
    }
    
    public var buttonHighlightedTextColor: UIColor? {
        return UIColor.improveYourHealthBlue()
    }
    
    public var gradientColor: Color {
        return .blue
    }
}
