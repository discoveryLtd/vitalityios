//
//  AWCHeaderStatusCell.swift
//  VitalityActive
//
//  Created by Steven Layug on 5/7/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import VIAUIKit

public class AWCHeaderStatusCell: UITableViewCell, Nibloadable {
    
    @IBOutlet weak var dateLabel: UILabel!
    public var dateLabelText: String?{
        set{
            self.dateLabel.text = newValue
        }
        get{
            return self.dateLabel.text
        }
    }
    
    @IBOutlet weak var monthLabel: UILabel!
    public var monthLabelText: String?{
        set{
            self.monthLabel.text = newValue
        }
        get{
            return self.monthLabel.text
        }
    }
    
    @IBOutlet weak var pointsTitleLabel: UILabel!
    public var pointsTitleLabelText: String?{
        set{
            self.pointsTitleLabel.text = newValue
        }
        get{
            return self.pointsTitleLabel.text
        }
    }
    
    @IBOutlet weak var pointsLabel: UILabel!
    public var pointsLabelText: String?{
        set{
            self.pointsLabel.text = newValue
        }
        get{
            return self.pointsLabel.text
        }
    }
    
    @IBOutlet weak var rewardsLabel: UILabel!
    public var rewardsLabelText: String?{
        set{
            self.rewardsLabel.text = newValue
        }
        get{
            return self.rewardsLabel.text
        }
    }
}

