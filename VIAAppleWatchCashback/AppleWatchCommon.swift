//
//  AppleWatch.swift
//  VIACommon
//
//  Created by OJ Garde on 7/10/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import VitalityKit
import VIAUtilities

public class AppleWatch{
    public class HowToTrackVitalityCoins{
        public enum PointsEarningActivities: Int, EnumCollection, Equatable {
            case heartRate = 0
            case steps = 1
            case gym = 2
            
            public func title() -> String? {
                switch self {
                case .heartRate:
                    return CommonStrings.PhysicalActivity.ItemHeartRateText439
                case .steps:
                    return CommonStrings.PhysicalActivity.ItemStepsText442
                case .gym:
                    return CommonStrings.Awc.TrackCoins.ActionMenuTitle2012
                }
            }
            
            public func image() -> UIImage?{
                switch self{
                case .heartRate:
                    return VIAAppleWatchCashbackAsset.Landing.HowToTrackVitalityCoin.heartRate.image
                case .steps:
                    return VIAAppleWatchCashbackAsset.Landing.HowToTrackVitalityCoin.steps.image
                case .gym:
                    return VIAAppleWatchCashbackAsset.Landing.HowToTrackVitalityCoin.gym
                        .image
                }
            }
            
            public func typeKey() -> PointsEntryTypeRef?{
                switch self{
                case .heartRate:
                    return PointsEntryTypeRef.Heartrate
                case .steps:
                    return PointsEntryTypeRef.Steps
                case .gym:
                    return PointsEntryTypeRef.GymVisit
                }
            }
        }
    }
}
