//
//  AWCHowToTrackVitalityCoinViewController.swift
//  VitalityActive
//
//  Created by Michelle R. Oratil on 09/05/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import VIAUIKit
import VitalityKit
import VIAUtilities
import VIACommon

class AWCHowToTrackVitalityCoinViewController: VIATableViewController {
    
    var viewModel: AWCHowToTrackVitalityCoinViewModel = AWCHowToTrackVitalityCoinViewModel()
    
    var imageBlocks: [UIImage]? = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureAppearance()
        configureTableView()
        fetchDeviceActivityMappingAndPointsWithTiers()
    }
    
    func configureAppearance() {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.makeNavigationBarTransparent()
        
        self.hideBackButtonTitle()
    }
    
    func configureTableView() {
        self.tableView.register(AWCHeaderBlockStatusCell.nib(), forCellReuseIdentifier: AWCHeaderBlockStatusCell.defaultReuseIdentifier)
        self.tableView.register(VIAGenericContentCell.nib(), forCellReuseIdentifier: VIAGenericContentCell.defaultReuseIdentifier)
        self.tableView.register(VIATableViewCell.nib(), forCellReuseIdentifier: VIATableViewCell.defaultReuseIdentifier)
        self.tableView.estimatedRowHeight = 96
        self.tableView.separatorInset = UIEdgeInsets(top: 0, left: 52, bottom: 0, right: 0)
        self.tableView.backgroundColor = UIColor.white
        self.tableView.separatorColor = UIColor.white.withAlphaComponent(0.5)
    }
    
    enum Sections: Int, EnumCollection, Equatable {
        case header = 0
        case pointsBlocks = 1
        case pointsEarningActivities = 2
        case accessRequirements = 3
        
        func title() -> String? {
            switch self {
            case .pointsEarningActivities:
                return CommonStrings.Dc.TrackCashbacks.SectionTitle1484
            case .accessRequirements:
                return CommonStrings.Dc.TrackCashbacks.SectionTitle1485
            default: return nil
            }
        }
    }
    
    enum pointsLevel: Int, EnumCollection, Equatable {
        case first = 0
        case second = 1
        case third = 2
        case fourth = 3
        
        func blocks() -> UIImage? {
            switch self {
            case .first:
                return VIAAppleWatchCashbackAsset.Landing.pinkBlock.image
            case .second:
                return VIAAppleWatchCashbackAsset.Landing.orangeBlock.image
            case .third:
                return VIAAppleWatchCashbackAsset.Landing.blueBlock.image
            case .fourth:
                return VIAAppleWatchCashbackAsset.Landing.greenBlock.image
            }
        }
        
        func activeBlocks() -> UIImage? {
            switch self {
            case .first:
                return VIAAppleWatchCashbackAsset.Landing.pinkBlockActive.image
            case .second:
                return VIAAppleWatchCashbackAsset.Landing.orangeBlockActive.image
            case .third:
                return VIAAppleWatchCashbackAsset.Landing.blueBlockActive.image
            case .fourth:
                return VIAAppleWatchCashbackAsset.Landing.greenBlockActive.image
            }
        }
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == "showActivities" {
            guard let destination = segue.destination as? VIAPointsEarningPotentialWithTiersTableViewController else {
                print("Incorrect view controller as destination")
                return
            }
            
            guard let selection = sender as? AppleWatch.HowToTrackVitalityCoins.PointsEarningActivities else { return }
            
            let pointsActivityTypeKey = selection.typeKey() ?? .Unknown
            destination.title = selection.title()
            destination.activityImage = selection.image()
            destination.pointsActivityTypeKey = pointsActivityTypeKey
            destination.viewModel = VIAWellnessDevicesPointsEarningPotentialViewModel(earningActivityKey: pointsActivityTypeKey.rawValue)
            
            if selection == .gym{
                destination.numberOfSections = 1
            }
            
            destination.viewController = "AWC"
        }
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let sections = Sections(rawValue: section)
        if sections == .pointsEarningActivities {
            let view = self.tableView.dequeueReusableCell(withIdentifier: VIATableViewCell.defaultReuseIdentifier)
            let accessRequirementsView = view as! VIATableViewCell
            accessRequirementsView.labelText = sections?.title()
            accessRequirementsView.label.font = UIFont.title2Font()
            return accessRequirementsView.contentView
        } else if sections == .accessRequirements {
            return accessRequirementsView()
        }
        return nil
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return Sections.allValues.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch Sections.allValues[section] {
        case .header:
            return 1
        case .pointsBlocks:
            return 1
        case .pointsEarningActivities:
            return AppleWatch.HowToTrackVitalityCoins.PointsEarningActivities.allValues.count
        case .accessRequirements:
            return 0
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch Sections.allValues[indexPath.section] {
        case .header:
            return headerCell(at: indexPath)
        case .pointsBlocks:
            return pointsHeader(at: indexPath)
        case .pointsEarningActivities:
            return pointsEarningActivitiesCell(at: indexPath)
        case .accessRequirements:
            break
        }
        return self.tableView.defaultTableViewCell()
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let section = Sections(rawValue: indexPath.section)
        if section == .header && indexPath.row != 0 {
            cell.separatorInset = UIEdgeInsetsMake(0, 0, 0, tableView.bounds.width)
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        let section = Sections(rawValue: section)
        if section == .header {
            return 0
        }
        return UITableViewAutomaticDimension
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        let section = Sections(rawValue: section)
        if section == .header {
            return 0
        }
        return UITableViewAutomaticDimension
    }
    
    func headerCell(at indexPath: IndexPath) -> UITableViewCell {
        var cell: UITableViewCell?
        
        if indexPath.row == 0 {
            cell = self.tableView.dequeueReusableCell(withIdentifier: VIAGenericContentCell.defaultReuseIdentifier, for: indexPath)
            let headerView = cell as! VIAGenericContentCell
            
            headerView.header = CommonStrings.Awc.TrackCoins.HeaderTitle2006
            headerView.content = CommonStrings.Awc.TrackCoins.HeaderMessage2007
            headerView.customHeaderFont = UIFont.title3Font()
            headerView.customContentFont = UIFont.subheadlineFont()
            
            cell?.separatorInset = UIEdgeInsets(top: 0, left: 52, bottom: 0, right: 0)
        }
        cell?.selectionStyle = .none
        return cell!
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let row = AppleWatch.HowToTrackVitalityCoins.PointsEarningActivities.allValues[indexPath.row]
        self.performSegue(withIdentifier: "showActivities", sender: row)
    }
    
    func pointsEarningActivitiesCell(at indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: VIATableViewCell.defaultReuseIdentifier, for: indexPath)
        let otherCell = cell as! VIATableViewCell
        let rowValues = AppleWatch.HowToTrackVitalityCoins.PointsEarningActivities.allValues[indexPath.row]
        
        otherCell.labelText = rowValues.title()
        otherCell.cellImage = rowValues.image()
        
        cell.accessoryType = .disclosureIndicator
        return cell
    }
    
    func pointsHeader(at indexPath: IndexPath) -> UITableViewCell {
        
        let cell = self.tableView.dequeueReusableCell(withIdentifier: AWCHeaderBlockStatusCell.defaultReuseIdentifier, for: indexPath)
        let pointsCell = cell as! AWCHeaderBlockStatusCell
        let levels = pointsLevel.allValues
        for level in levels {
            self.imageBlocks?.append(level.blocks()!)
        }

        pointsCell.firstBlockViewImage = self.imageBlocks?[0]
        pointsCell.secondBlockViewImage = self.imageBlocks?[1]
        pointsCell.thirdBlockViewImage = self.imageBlocks?[2]
        pointsCell.fourthBlockViewImage = self.imageBlocks?[3]
    
        pointsCell.firstBlockViewLabelText = "0 \(CommonStrings.Awc.TrackCoins.CoinsTitle2008)"
        pointsCell.secondBlockViewLabelText = "500 \(CommonStrings.Awc.TrackCoins.CoinsTitle2008)"
        pointsCell.thirdBlockViewLabelText = "750 \(CommonStrings.Awc.TrackCoins.CoinsTitle2008)"
        pointsCell.fourthBlockViewLabelText = "1000 \(CommonStrings.Awc.TrackCoins.CoinsTitle2008)"
        
        pointsCell.firstPointLabelText = CommonStrings.Dc.TrackCashbacks.PointsSubtitle1482("0")
        
        return cell
    }
    
    func accessRequirementsView() -> UIView? {
        let view = self.tableView.dequeueReusableCell(withIdentifier: VIAGenericContentCell.defaultReuseIdentifier)
        let accessRequirementsView = view as! VIAGenericContentCell
        
        accessRequirementsView.header = CommonStrings.Awc.TrackCoins.SectionTitle2013
        accessRequirementsView.content = CommonStrings.Awc.TrackCoins.SectionContent2014
        accessRequirementsView.customHeaderFont = UIFont.title1Font()
        accessRequirementsView.customContentFont = UIFont.subheadlineFont()
        return accessRequirementsView.contentView
    }
}

/* API Call Functions */
extension AWCHowToTrackVitalityCoinViewController {
    
    /* If needed, fetch the PotentialPointsWithTiers */
    func fetchDeviceActivityMappingAndPointsWithTiers() {
        guard self.viewModel.hasDeviceMappingData() == false else { return }
        guard WDACache.wdaDataIsOutdated() == true else { return }
        
        showHUDOnView(view: view)
        self.viewModel.performFetchDeviceActivityMappings(completion: { [weak self] error, didRecieveData in
            guard error == nil else {
                self?.hideHUDFromView(view: self?.view)
                self?.handleErrorOccurred(error!, isRealmEmpty: false)
                return
            }
            guard didRecieveData == true, self?.viewModel.hasPointsWithTiersData() == false else {
                self?.hideHUDFromView(view: self?.view)
                return
            }
            self?.fetchPotentialPointsWithTiers()
        })
    }
    
    /* GetPotentialPointsByEventType */
    func fetchPotentialPointsWithTiers() {
        viewModel.requestPotentialPointsWithTiers(completion: { [weak self] error in
            self?.hideHUDFromView(view: self?.view)
            guard error == nil else {
                self?.handleErrorOccurred(error!, isRealmEmpty: false)
                return
            }
        })
    }
    
    func handleErrorOccurred(_ error: Error, isRealmEmpty: Bool?) {
        let backendError = error as? BackendError ?? BackendError.other
        if let realmIsEmpty = isRealmEmpty, realmIsEmpty == true {
            _ = self.handleBackendErrorWithAlert(backendError)
        } else {
            removeStatusView()
            tableView.reloadData()
            _ = self.handleBackendErrorWithAlert(backendError)
        }
    }
}
