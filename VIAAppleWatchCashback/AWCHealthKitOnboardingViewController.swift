//
//  AWCHealthKitOnboardingViewController.swift
//  VitalityActive
//
//  Created by Michelle R. Oratil on 17/05/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import UIKit
import VIAUIKit
import VitalityKit

class AWCHealthKitOnboardingViewController: VIACirclesViewController {
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.viewModel = AWCHealthKitOnboardingViewModel()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.isHidden = false
        navigationController?.makeNavigationBarInvisible()
        button.isHidden = true
        addLinkButton()
        addLinkLabel()
    }
    
    public lazy var linkLabel: UIButton = {
        let view = VIAButton(title: CommonStrings.Dc.Attention.LinkDeviceLaterButton1515)
        view.tintColor = UIColor.day()
        view.hidesBorder = true
        if let highlightedTextColor = self.viewModel?.buttonHighlightedTextColor {
            view.highlightedTextColor = highlightedTextColor
        }
        return view
    }()
    
    public lazy var linkButton: UIButton = {
        let view = VIAButton(title: CommonStrings.Dc.Attention.LinkDeviceNowButton1514)
        view.tintColor = UIColor.day()
        if let highlightedTextColor = self.viewModel?.buttonHighlightedTextColor {
            view.highlightedTextColor = highlightedTextColor
        }
        return view
    }()
    
    func addLinkButton() {
        gradientView.addSubview(linkButton)
        linkButton.addTarget(self, action: #selector(linkButtonTapped(_:)), for: .touchUpInside)
        linkButton.snp.makeConstraints { make in
            make.width.equalTo(gradientView.snp.width).multipliedBy(0.5)
            make.height.equalTo(44)
            make.bottom.equalTo(gradientView.snp.bottom).offset(-85)
            make.centerX.equalTo(gradientView.snp.centerX)
        }
    }

    func addLinkLabel() {
        gradientView.addSubview(linkLabel)
        linkLabel.addTarget(self, action: #selector(linkLabelTapped(_:)), for: .touchUpInside)
        linkLabel.snp.makeConstraints { make in
            make.width.equalTo(gradientView.snp.width).multipliedBy(0.5)
            make.height.equalTo(44)
            make.bottom.equalTo(gradientView.snp.bottom).offset(-35)
            make.centerX.equalTo(gradientView.snp.centerX)
        }
        
    }
    
    open func linkButtonTapped(_ sender: UIButton) {
        self.performSegue(withIdentifier: "unwindToHealthKitDeviceDetail", sender: self)
    }
    
    open func linkLabelTapped(_ sender: UIButton) {
        self.performSegue(withIdentifier: "unwindToAWCLandingViewController", sender: self)
    }
    
}
