//
//  AWCHowToTrackVitalityCoinViewModel.swift
//  VIAAppleWatchCashback
//
//  Created by Von Kervin R. Tuico on 04/06/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import VitalityKit
import VIAHealthKit
import VIAUtilities
import VIACommon

class AWCHowToTrackVitalityCoinViewModel: DeviceActivityMappingHelper {
    var wdaRealm = DataProvider.newWDARealm()
    var coreRealm = DataProvider.newRealm()
    
    public func performFetchDeviceActivityMappings(completion: @escaping (Error?, Bool) -> Void) {
        self.fetchAndProcessDeviceActivityMappings { error, recievedData in
            completion(error, recievedData)
        }
    }
    
    public func requestPotentialPointsWithTiers(completion: @escaping (Error?) -> Void) {
        let backgroundCoreRealm = DataProvider.newRealm()
        var eventTypeKeys: Array<Int> = Array<Int>()
        eventTypeKeys.append(EventTypeRef.DeviceDataUpload.rawValue)
        eventTypeKeys.append(EventTypeRef.GymWorkout.rawValue)
        let partyId = backgroundCoreRealm.getPartyId()
        let tenantId = backgroundCoreRealm.getTenantId()
        let request = PotentialPointsWithTiersRequest(eventTypeKeys:eventTypeKeys)
        Wire.Events.getPotentialPointsWithTiers(tenantId: tenantId, partyId: partyId, request: request) { (error) in
            completion(error)
        }
    }
    
    func hasDeviceMappingData() -> Bool {
        wdaRealm.refresh()
        let devicesWithActivities = wdaRealm.allWDAAvailableDeviceActivitiesWithTypeRefs()
        guard devicesWithActivities.first != nil else {return false}
        return true
    }
    
    func hasPointsWithTiersData() -> Bool {
        coreRealm.refresh()
        let pointsWithTiersData = coreRealm.getAllPotentialPointsWithTiersEventType()
        guard pointsWithTiersData.first != nil else {return false}
        return true
    }
}

