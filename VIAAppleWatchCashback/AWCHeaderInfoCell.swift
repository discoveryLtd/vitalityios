//
//  AWCHeaderInfoCell.swift
//  VitalityActive
//
//  Created by Michelle R. Oratil on 08/05/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import VIAUIKit

class AWCHeaderInfoCell: UITableViewCell, Nibloadable {

    
    @IBOutlet weak var headerIcon: UIImageView!
    
    @IBOutlet weak var infoLabel: UILabel!
    public var infoText: String?{
        set{
            self.infoLabel.text = newValue
        }
        get{
            return self.infoLabel.text
        }
    }
    
    @IBOutlet weak var infoButton: VIAButton!
    public var infoButtonText: String?{
        set {
            self.infoButton.setTitle(newValue, for: .normal)
        }
        get {
            return self.infoButton.currentTitle
        }
    }
    
    override public func awakeFromNib() {
        infoLabel.text = nil
        infoLabel.font = UIFont.bodyFont()
        infoLabel.textColor = UIColor.darkGray
        
        //infoButton.tintColor = UIColor.currentGlobalTintColor()
        infoButton.setTitle(nil, for: .normal)
        infoButton.titleLabel?.font = UIFont.bodyFont()
    }

}
