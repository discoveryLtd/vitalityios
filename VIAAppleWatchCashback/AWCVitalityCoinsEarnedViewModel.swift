//
//  AWCVitalityCoinsEarnedViewModel.swift
//  VitalityActive
//
//  Created by Steven Layug on 5/14/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import VIAUIKit
import VIAUtilities
import VitalityKit
import VIACommon

class AWCVitalityCoinsEarnedViewModel {
    let realm = DataProvider.newGDCRealm()
    let globalRealm = DataProvider.newRealm()
    var rewardValues: [Int]? = []
    var pointsAchieved: [Int]? = []
    var validFrom: [String]? = []
    var highestObjective: [String]? = []
    var vitalityCoinStatus: [String]? = []
    var headerTexts: [String]? = []
    
    enum Objectives: String, EnumCollection, Equatable {
        case pinkIcon = "0"
        case orangeIcon = "1"
        case blueIcon = "2"
        case greenIcon = "3"
    }
    
    func getDeviceBenefitId() -> Int {
        guard let benefitId = self.realm.allGDCBenefit().first?.benefitId else {
            return 0
        }
        return benefitId
    }
    
    func headerCoinsEarnedValue () -> String {
        if let benefit = realm.allGDCBGRBenefits().first {
            return "\(benefit.totalGoalsRewardQuantity)"
        }
        return ""
    }
    
    func headerDateRange() -> String {
        let goalTrackers = realm.allGDCGoalTrackers()
        return self.dateFormatter(startDate: goalTrackers.first?.validFrom, endDate: goalTrackers.last?.validFrom)
    }
    
    func dateFormatter (startDate: String?, endDate: String?) -> String{
        let dateResponseFormatter = DateFormatter()
        dateResponseFormatter.dateFormat = "yyyy-MM-dd"
        
        guard let firstStartDate = startDate, let lastStartDate = endDate else {
            return ""
        }
        let fromDate: Date? = dateResponseFormatter.date(from: (firstStartDate))
        let toDate: Date? = dateResponseFormatter.date(from: (lastStartDate))
        
        return CommonStrings.Dc.CashbackEarned.PeriodHeader1537(Localization.fullMonthAndYearFormatter.string(from: fromDate!), Localization.fullMonthAndYearFormatter.string(from: toDate!))
    }
    
    func headerGoalsRemaining() -> String {
        if let benefit = realm.allGDCBGRBenefits().first {
            return CommonStrings.Dc.CashbackEarned.PeriodRemainingDesc1538("\(benefit.goalsRemaining)")
        }
        return ""
    }
    
    func goalTrackersCount() -> Int {
        return realm.allGDCGoalTrackers().count
    }
    
    func coinsEarnedValue(index: Int) -> String{

        let reward = (realm.allGDCGoalTrackers()[index])
        
        if reward.rewardValues.first?.totalRewardValueAmount != "0" {
            return "\(reward.rewardValues.first?.totalRewardValueAmount ?? "")"
        } else {
            return "\(reward.rewardValues.first?.totalRewardValueQuantity ?? 0)"
        }
        
    }
    
    func validFromDate(index: Int) -> String {
        let validFrom = (realm.allGDCGoalTrackers()[index].validFrom)
        
        let dateResponseFormatter = DateFormatter()
        dateResponseFormatter.dateFormat = "yyyy-MM-dd"
        
        guard let stringToDate = dateResponseFormatter.date(from: "\(validFrom)") else {
            return ""
        }
        
        return Localization.shortMonthAndYearFormatter.string(from: stringToDate)
    }
    
    func getGoalTrackerDate(index: Int) -> String? {
        var validFrom: [String] = []
        for goals in realm.allGDCGoalTrackers() {
            validFrom.append(goals.validFrom)
        }
        return validFrom[index]
    }
    
    func pointsValue(index: Int) -> String {
        let pointsAchieved = (realm.allGDCGoalTrackers()[index].pointsAchievedTowardsGoal)
        return CommonStrings.Dc.TrackCashbacks.PointsSubtitle1482("\(pointsAchieved)")
    }
    
    func getVitalityCoinStatus(index: Int) -> String {
        vitalityCoinStatus = []
        let goalTrackers = realm.allGDCGoalTrackers()
        for goals in  goalTrackers{
            let monitorUtil = goals.monitorUntil
            if !monitorUtil.isEmpty {
                /* Compare Dates */
                let formatter = DateFormatter()
                formatter.dateFormat = "yyyy-MM-dd"
                //FC-23864 - Use device timezone instead of UTC
                formatter.locale = Locale(identifier: DeviceLocale.toString())
                if let monitorUtilDate = formatter.date(from: monitorUtil){
                    let stringDate = formatter.string(from: Date())
                    
                    if let currentDate = formatter.date(from: stringDate){
                        
                        if monitorUtilDate < currentDate {
                            vitalityCoinStatus?.append(CommonStrings.Awc.Landing.ActionMenuTitle1999)
                        } else if monitorUtilDate >= currentDate {
                            vitalityCoinStatus?.append(CommonStrings.Awc.CoinsEarned.CoinsPending2030)
                        }
                    }
                }
            }
        }
        return vitalityCoinStatus![index]
    }
    
    func getHeaderText(index: Int) -> String {
        for goals in realm.allGDCGoalTrackers() {
            let monitorUtil = goals.monitorUntil
            if !monitorUtil.isEmpty {
                /* Compare Dates */
                let formatter = DateFormatter()
                formatter.dateFormat = "yyyy-MM-dd"
                let monitorUtilDate = formatter.date(from: monitorUtil)
                let currentDate = Date()
                
                //TODO: Tokens should be created for these
                if monitorUtilDate! <= currentDate {
                    headerTexts?.append(CommonStrings.Awc.CoinsEarned.MonthMessage2031)
                } else if monitorUtilDate! > currentDate {
                    headerTexts?.append(CommonStrings.Awc.CoinsEarned.MonthCoinsPendingMessage2032("6"))
                }
            }
        }
        return headerTexts![index]
    }
    
    func cellImage(index: Int) -> UIImage {
        for goal in realm.allGDCGoalTrackers() {
            highestObjective?.append(goal.highestObjectiveAchievedName)
        }
        
        if (highestObjective?[index].range(of: Objectives.pinkIcon.rawValue) != nil){
            return VIAAppleWatchCashbackAsset.Landing.pinkBlockActive.image
        } else if (highestObjective?[index].range(of: Objectives.orangeIcon.rawValue) != nil){
            return VIAAppleWatchCashbackAsset.Landing.orangeBlockActive.image
        } else if (highestObjective?[index].range(of: Objectives.blueIcon.rawValue) != nil){
            return VIAAppleWatchCashbackAsset.Landing.blueBlockActive.image
        } else if (highestObjective?[index].range(of: Objectives.greenIcon.rawValue) != nil){
            return VIAAppleWatchCashbackAsset.Landing.greenBlockActive.image
        }
        return VIAAppleWatchCashbackAsset.Landing.pinkBlockActive.image
    }
    
    // API Call
    func loadData(forceUpdate: Bool = false, completion: @escaping (_ error: Error?) -> Void)  {
        
        let partyId = globalRealm.getPartyId()
        let productKey = 2
        let tenantId = globalRealm.getTenantId()
        
        guard let benefitId = realm.allGDCBenefit().first?.benefitId, realm.allGDCBenefit().first?.benefitId != 0 else {
            completion(BackendError.other)
            return
        }
        //        TO DO: Left for debugging purposes
        //        let jsonParameters = GetBenefitGoalsAndRewardsParameters(benefitId: 58268707, partyId: 22250574, productKey: productKey)
        let jsonParameters = GetBenefitGoalsAndRewardsParameters(benefitId: benefitId, partyId: partyId, productKey: productKey)
        Wire.Events.getBenefitGoalsAndRewardsFeature(tenantId: tenantId, request: jsonParameters) { (error) in
            completion(error)
            debugPrint("\(self.realm.allGDCGoalTrackers())")
        }
    }
}
