//
//  AWCVitalityCoinsEarnedViewController.swift
//  VitalityActive
//
//  Created by Michelle R. Oratil on 07/05/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import VIAUIKit
import VIAUtilities
import VitalityKit
import VIACommon

class AWCVitalityCoinsEarnedViewController: VIATableViewController {
    
    let vitalityCoinsEarnedViewModel = AWCVitalityCoinsEarnedViewModel()
    
    // TO DO: To be populated for next details screen
    var headerImage: UIImage?
    var headerImageText: String?
    var progressText: String?
    var pointsProgressText: String?
    var dateTitle: String?
    var selectedIndexPath: Int?
    
    // TO DO: Temporary value since cycle completed scenario is out of scope
    let showList: Bool? = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureTableView()
        // TO DO: Token to be created
        self.title = CommonStrings.Awc.Landing.ActionMenuTitle1999
    }
    
    enum Sections: Int, EnumCollection, Equatable {
        case header = 0
        case details = 1
        case vitalityCoinsDetails = 2
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.initApi()
    }
    
    override public func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.hideBackButtonTitle()
    }
    
    func configureEmptyStatusView() {
        let view = VIAStatusView.viewFromNib(owner: self)!
        
        // TO DO: Tokens to be created
        view.heading = CommonStrings.Awc.CoinsEarned.NoCoinsEarnedTitle2015
        view.message = CommonStrings.Awc.CoinsEarned.NoCoinsEarnedDescription2016
        
        self.view.addSubview(view)
        self.view.bringSubview(toFront: view)
        
        view.snp.makeConstraints { (make) in
            make.width.equalToSuperview()
            make.left.right.top.equalToSuperview()
            make.height.equalToSuperview().offset(-62)
            make.bottom.equalToSuperview().offset(-62)
        }
    }

    func configureTableView() {
        self.tableView.register(VIATableViewSectionHeaderFooterView.nib(), forHeaderFooterViewReuseIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier)
        self.tableView.register(VIATableViewSectionHeaderFooterView.nib(), forHeaderFooterViewReuseIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier)
        self.tableView.register(VIATableViewCell.nib(), forCellReuseIdentifier: VIATableViewCell.defaultReuseIdentifier)
        self.tableView.register(AWCVitalityCoinsHeaderCell.nib(), forCellReuseIdentifier: AWCVitalityCoinsHeaderCell.defaultReuseIdentifier)
        self.tableView.register(AWCActivityCell.nib(), forCellReuseIdentifier: AWCActivityCell.defaultReuseIdentifier)
        self.tableView.register(AWCVitalityCoinsEarnedCell.nib(), forCellReuseIdentifier: AWCVitalityCoinsEarnedCell.defaultReuseIdentifier)
        
        self.tableView.register(VIATableViewCell.nib(), forCellReuseIdentifier: VIATableViewCell.defaultReuseIdentifier)
        self.tableView.estimatedRowHeight = 75
        self.tableView.backgroundColor = UIColor.tableViewBackground()
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.makeNavigationBarTransparent()
        self.navigationController?.hideBackButtonTitle()
        tableView.contentInset = UIEdgeInsets(top: -1, left: 0, bottom: 0, right: 0)
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        let section = Sections(rawValue: section)
        if section == .header {
            return 0
        }
        return UITableViewAutomaticDimension
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let sections = Sections(rawValue: section)
        if showList! {
            if sections == .vitalityCoinsDetails{
                return vitalityCoinsEarnedViewModel.goalTrackersCount()
            } else {
                return 0
            }
        } else if vitalityCoinsEarnedViewModel.goalTrackersCount() != 0 {
            if sections == .header{
                return 1
            } else if sections == .details{
                return vitalityCoinsEarnedViewModel.goalTrackersCount()
            }
            return 0
        }
        return 0
    }
    
    // MARK: UITableView delegate
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return Sections.allValues.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch Sections.allValues[indexPath.section] {
        case .header:
            return headerCell(at: indexPath)
        case .details:
            return detailCell(at: indexPath)
        case.vitalityCoinsDetails:
            return vitalityCoinsEarnedCell(at: indexPath)
        }
    }
    
    func headerCell(at indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: AWCVitalityCoinsHeaderCell.defaultReuseIdentifier, for: indexPath)
        let headerView = cell as! AWCVitalityCoinsHeaderCell
        
        headerView.coinsEarnedText = vitalityCoinsEarnedViewModel.headerCoinsEarnedValue()
        headerView.monthsText = vitalityCoinsEarnedViewModel.headerGoalsRemaining()
        headerView.dateRangeText = vitalityCoinsEarnedViewModel.headerDateRange()
        
        headerView.bannerImage.image = UIImage(asset: VIAAppleWatchCashbackAsset.Landing.cashbackEarnedGraphic)
        
        self.headerImage = UIImage(asset: VIAAppleWatchCashbackAsset.Landing.cashbackEarnedGraphic)
        
        return headerView
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let sections = Sections(rawValue: section)
        if vitalityCoinsEarnedViewModel.goalTrackersCount() != 0 {
            if showList! {
                if sections == .vitalityCoinsDetails{
                    return nil
                }
            } else{
                if sections == .details {
                    let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier) as! VIATableViewSectionHeaderFooterView
                    // TO DO: Token to be created
                    view.labelText = CommonStrings.Awc.Landing.ActionMenuTitle1999
                    view.backgroundColor = UIColor.tableViewBackground()
                    return view
                }
            }
        }
        return nil
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let sections = Sections(rawValue: indexPath.section) {
            switch sections {
            case .details:
                self.selectedIndexPath = indexPath.row
                self.performSegue(withIdentifier: "showActivityDetail", sender: self)
            default: break
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    
    func detailCell(at indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: AWCActivityCell.defaultReuseIdentifier, for: indexPath)
        let detailCell = cell as! AWCActivityCell
        
        // TO DO: Token to be created
        detailCell.statusLabelText = vitalityCoinsEarnedViewModel.getVitalityCoinStatus(index: indexPath.row)
        detailCell.cellImageView.contentMode = .scaleAspectFit
        detailCell.coinsValueLabelText = vitalityCoinsEarnedViewModel.coinsEarnedValue(index: indexPath.row)
        detailCell.dateLabelText = vitalityCoinsEarnedViewModel.validFromDate(index: indexPath.row)
        detailCell.pointsLabelText = vitalityCoinsEarnedViewModel.pointsValue(index: indexPath.row)
        detailCell.cellImageView.image = vitalityCoinsEarnedViewModel.cellImage(index: indexPath.row)
        
        return detailCell
    }
    
    // TO DO: This is out of scope, template will be used in the future
    func vitalityCoinsEarnedCell(at indexPath: IndexPath) -> UITableViewCell {
        
        let cell = self.tableView.dequeueReusableCell(withIdentifier: AWCVitalityCoinsEarnedCell.defaultReuseIdentifier, for: indexPath)
        let inputFieldCell = cell as! AWCVitalityCoinsEarnedCell
        inputFieldCell.deviceText = "Apple Watch Series 1"
        inputFieldCell.pointsText = "24 000 Vitality Coins Earned"
        inputFieldCell.dateText = "From August 2017 to January 2019"
        inputFieldCell.selectionStyle = .none
        return cell
    }
    
    func initApi() {
        self.showHUDOnWindow()
        vitalityCoinsEarnedViewModel.loadData(forceUpdate: true) {[weak self] (error) in
            guard error == nil else {
                self?.hideHUDFromWindow()
                guard error?.localizedDescription != BackendError.other.localizedDescription else {
                     self?.configureEmptyStatusView()
                    return
                }
                debugPrint("Error Message: \(error!)")
                self?.initErrorHandler(error!)
                return
            }
            DispatchQueue.main.async {
            if self?.vitalityCoinsEarnedViewModel.goalTrackersCount() == 0 {
                self?.configureEmptyStatusView()
            } else {
                self?.tableView.reloadData()
            }
            self?.hideHUDFromWindow()
        }
        }
    }
    
    func initErrorHandler(_ error: Error) {
        let backendError = error as? BackendError ?? BackendError.other
        self.handleBackendErrorWithAlert(backendError)
    }
}

extension AWCVitalityCoinsEarnedViewController {
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showActivityDetail" {
            if let AWCActivityDetails = segue.destination as? AWCActivityDetailsViewController {
                AWCActivityDetails.headerImage = vitalityCoinsEarnedViewModel.cellImage(index: selectedIndexPath!)
                AWCActivityDetails.headerImageText = "\(vitalityCoinsEarnedViewModel.coinsEarnedValue(index: selectedIndexPath!)) \(CommonStrings.Awc.TrackCoins.CoinsTitle2008)"
                AWCActivityDetails.pointsProgressText = vitalityCoinsEarnedViewModel.pointsValue(index: selectedIndexPath!)
                AWCActivityDetails.progressText = vitalityCoinsEarnedViewModel.getVitalityCoinStatus(index: selectedIndexPath!)
                AWCActivityDetails.headerMessage = vitalityCoinsEarnedViewModel.getHeaderText(index: selectedIndexPath!)
                AWCActivityDetails.dateTitle = vitalityCoinsEarnedViewModel.getGoalTrackerDate(index: selectedIndexPath!)
            }
        }
    }
    
}
