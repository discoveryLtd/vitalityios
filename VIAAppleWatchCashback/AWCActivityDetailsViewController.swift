//
//  AWCActivityDetailsViewController.swift
//  VitalityActive
//
//  Created by Steven Layug on 5/6/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import VIAUIKit
import VitalityKit
import VIAUtilities

public protocol AWCActivityDetailsViewModelDelegate {
    func getBenefitID() -> Int?
    func getGoalTrackerOuts(benefitID: Int, date: String) -> GDCGoalTrackerOuts?
    func getObjectivePointsEntries(goalTrackerOut: GDCGoalTrackerOuts) -> [GDCObjectivePointsEntries]?
    func getGoalProgressAndDetails(completion: @escaping (_ error: Error?) -> Void)
    func formattedDateFromString(dateString: String, withFormat format: String) -> String?
    func monthYearFormatter (date: String?) -> String
}

class AWCActivityDetailsViewController: VIATableViewController {
    
    internal var viewModel: AWCActivityDetailsViewModelDelegate?
    internal var benefitID: Int?
    internal var goalTrackerOut: GDCGoalTrackerOuts?
    internal var objectivePointsEntries: [GDCObjectivePointsEntries]? = []
    internal var hasActivities = false
    
    public var headerImage: UIImage?
    public var headerImageText: String?
    public var progressText: String?
    public var pointsProgressText: String?
    public var dateTitle: String?
    public var headerMessage: String?
    
    enum Sections: Int, EnumCollection, Equatable {
        case header = 0
        case activities = 1
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        /* Initialize ViewModel */
        self.viewModel = AWCActivityDetailsViewModel()
        
        initUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        /* Call GetGoalProgressAndDetails API */
        initAPI()
    }
    
    func initUI() {
        /* Set TableView Hidden while */
        self.tableView.isHidden = true
        initNavigationBar()
        configureTableView()
    }
    
    func configureTableView() {
        self.tableView.register(VIATableViewSectionHeaderFooterView.nib(), forHeaderFooterViewReuseIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier)
        self.tableView.register(AWCActivityDetailsHeaderCell.nib(), forCellReuseIdentifier: AWCActivityDetailsHeaderCell.defaultReuseIdentifier)
        self.tableView.register(AWCDetailsActivityCell.nib(), forCellReuseIdentifier: AWCDetailsActivityCell.defaultReuseIdentifier)
        self.tableView.register(VIATableViewCell.nib(), forCellReuseIdentifier: VIATableViewCell.defaultReuseIdentifier)
        self.tableView.estimatedRowHeight = 75
        self.tableView.estimatedSectionHeaderHeight = 40
        self.tableView.backgroundColor = UIColor.tableViewBackground()
        self.tableView.delegate = self
    }
    
    func initNavigationBar() {
        self.title = self.viewModel?.monthYearFormatter(date: self.dateTitle)
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.makeNavigationBarTransparent()
        self.hideBackButtonTitle()
    }
    
    func initAPI() {
        self.showHUDOnWindow()
        self.viewModel?.getGoalProgressAndDetails(completion: { [weak self] (error) in
            DispatchQueue.main.async {
                self?.hideHUDFromWindow()
                guard self?.viewModel?.getBenefitID() != nil else {
                    self?.tableView.isHidden = false
                    self?.tableView.reloadData()
                    return
                }
                guard error == nil else {
                    debugPrint("Error Message: \(error!)")
                    self?.initErrorHandler(error!)
                    return
                }
                self?.hasActivities = true
                /* Get all required data to display in the UI */
                self?.initRequiredDetails()
                self?.tableView.isHidden = false
                self?.tableView.reloadData()
            }
        })
    }
    
    func initRequiredDetails() {
        self.benefitID = self.viewModel?.getBenefitID()
        
        /* Check if Selected Month is Empty or Not */
        guard let selectedMonth = self.dateTitle else { return }
        self.goalTrackerOut = self.viewModel?.getGoalTrackerOuts(benefitID: self.benefitID!, date: selectedMonth)
        
        /* Check if fetched Goal Tracker Out is Empty or Not */
        guard goalTrackerOut != nil else { return }
        self.objectivePointsEntries = self.viewModel?.getObjectivePointsEntries(goalTrackerOut: goalTrackerOut!)
    }
    
    func initErrorHandler(_ error: Error) {
        let backendError = error as? BackendError ?? BackendError.other
        self.handleBackendErrorWithAlert(backendError)
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == Sections.activities.rawValue{
            if let pointsEntries = self.objectivePointsEntries, !pointsEntries.isEmpty {
                return pointsEntries.count
            }
            return 1
        } else {
            return 1
        }
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return Sections.allValues.count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if self.hasActivities {
            return UITableViewAutomaticDimension
        } else {
            let section = Sections(rawValue: indexPath.section)
            if section == .activities {
                return 50
            }
        }
        return UITableViewAutomaticDimension
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch Sections.allValues[section] {
        case .header:
            return 0
        default:
            return UITableViewAutomaticDimension
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch Sections.allValues[indexPath.section] {
        case .header:
            return headerCell(at: indexPath)
        case .activities:
            if self.hasActivities {
                return activityCell(at: indexPath)
            }
            return noActivityCell(at: indexPath)
        }
    }
    
    func headerCell(at indexPath: IndexPath) -> UITableViewCell {
        let view = self.tableView.dequeueReusableCell(withIdentifier: AWCActivityDetailsHeaderCell.defaultReuseIdentifier, for: indexPath)
        let headerView = view as! AWCActivityDetailsHeaderCell
        
        if self.hasActivities {
            headerView.setHeaderImageText = self.headerImageText
            headerView.setHeaderText = self.progressText
            headerView.setHeaderSubText = self.pointsProgressText
            headerView.headerUIImageView.image = self.headerImage
        } else {
            // TODO: Should be from AWCLandingVC, will update once AWCLanding is configured
            headerView.setHeaderImageText = "0 \(CommonStrings.Awc.TrackCoins.CoinsTitle2008)"
            headerView.setHeaderText = self.progressText
            // TODO: Should be token 1482, but current value is "%1$@ Points", cannot be updated since it is used on other screens. Needs own token.
            headerView.setHeaderSubText = CommonStrings.Dc.TrackCashbacks.PointsSubtitle1482("0")
            headerView.headerUIImageView.image = self.headerImage
        }

        return view
    }
    
    func activityCell(at indexPath: IndexPath) -> UITableViewCell {
        if self.hasActivities {
            let cell = self.tableView.dequeueReusableCell(withIdentifier: AWCDetailsActivityCell.defaultReuseIdentifier, for: indexPath)
            let activityCell = cell as! AWCDetailsActivityCell
            
            if let pointsEntries = self.objectivePointsEntries, !pointsEntries.isEmpty {
                
                let rowDetails = pointsEntries[indexPath.row]
                activityCell.pointsLabelText = "\(rowDetails.pointsContributed)"
                
                /* Convert Date Format */
                var date: String?
                if DeviceLocale.toString() == "ja_JP" {
                    date = viewModel?.formattedDateFromString(dateString: rowDetails.effectiveDate!, withFormat: "MMM dd (E)")
                } else {
                    date = viewModel?.formattedDateFromString(dateString: rowDetails.effectiveDate!, withFormat: "E, MMM dd")
                }
                activityCell.dateLabelText = date
                
                if rowDetails.typeKey == .GymVisit {
                    activityCell.stepsLabelText = rowDetails.typeName
                    activityCell.deviceLabelText = ""
                } else {
                    let pointsEntryMetadatas = rowDetails.pointsEntryMetadatas
                    for pointsEntryMetadata in pointsEntryMetadatas {
                        switch EventMetaDataTypeRef(rawValue: pointsEntryMetadata.typeKey.rawValue) {
                        case .Manufacturer?:
                            activityCell.deviceLabelText = pointsEntryMetadata.value
                            break
                        case .TotalSteps?:
                            let totalSteps = pointsEntryMetadata.value ?? "0"
                            activityCell.stepsLabelText = CommonStrings.Awc.MonthSteps2187(totalSteps)
                            break
                        case .AverageHeartRate?:
                            activityCell.stepsLabelText = rowDetails.typeName
                            break
                        default:
                            break
                        }
                    }
                }
                return activityCell
            }
        }
        
        let cell = self.tableView.dequeueReusableCell(withIdentifier: VIATableViewCell.defaultReuseIdentifier, for: indexPath)
        cell.textLabel?.text = CommonStrings.Awc.MonthActivityNoActivities2028
        return cell
    }
    
    func noActivityCell(at indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: VIATableViewCell.defaultReuseIdentifier, for: indexPath)
        cell.textLabel?.text = CommonStrings.Awc.MonthActivityNoActivities2028
        return cell
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let sections = Sections(rawValue: section)
        if sections == .activities {
            let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier) as! VIATableViewSectionHeaderFooterView
            view.labelText = CommonStrings.Dc.CashbackEarned.ActivityHeader1543
            view.set(font: UIFont.boldSystemFont(ofSize: 18))
            view.set(textColor: UIColor.black)
            view.backgroundColor = UIColor.tableViewBackground()
            
            return view
        }
        return nil
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let sections = Sections(rawValue: section)
        if sections == .header {
            let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier) as! VIATableViewSectionHeaderFooterView
            view.labelText = self.headerMessage
            view.backgroundColor = UIColor.tableViewBackground()
            return view
        } else if sections == .activities {
            let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier) as! VIATableViewSectionHeaderFooterView
            view.labelText = CommonStrings.Awc.MonthActivityMessage2029
            view.backgroundColor = UIColor.tableViewBackground()
            return view
        }
        return nil
    }
}
