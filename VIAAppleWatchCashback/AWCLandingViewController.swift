//
//  AWCLandingViewController.swift
//  VitalityActive
//
//  Created by Michelle R. Oratil on 04/05/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import VIAUIKit
import VIAUtilities
import VitalityKit
import SafariServices

class AWCLandingViewController: VIATableViewController, ImproveYourHealthTintable, SFSafariViewControllerDelegate, SafariViewControllerPresenter {
    
    @IBAction func unwindToAWCLandingViewController(segue:UIStoryboardSegue) { }
    
    @IBAction public func dismissedHealthKitOnboarding(segue: UIStoryboardSegue) {
        NotificationCenter.default.post(name: .DidDismissHealthKitOnboardingViewController, object: nil)
    }
    
    let viewModel = AWCLandingViewModel()
    var levels: GDCLevels?
    var amountAwarded: String?
    var pointsAwarded: String?
    var dateValue: String?
    var progressText: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = CommonStrings.HomeCard.CardSectionTitle364
        initUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tableView.reloadData()
        VIAAppearance.setGlobalTintColorToImproveYourHealth()
        
        viewModel.setHeaderButtonInfo = { [weak self] (token) in
            if self?.viewModel.cardStatus == .ContinueActivation {
                self?.viewModel.performLink()
                
                /* Identify if Health App is Linked or Not */
                guard let isDeviceLinked = self?.viewModel.isDeviceLinked(), isDeviceLinked else {
                    self?.performSegue(withIdentifier: token, sender: self)
                    return
                }
                
                self?.initLinkHealthApp(identifier: token)
            } else {
                self?.performSegue(withIdentifier: token, sender: self)
            }
        }
        
        if AppSettings.hasShownAppleWatchCashbackOnboarding(){
            self.initApi()
        }
        
        showOnboardingIfNeeded()
    }
    
    func initApi() {
        viewModel.loadData(onStart: { [weak self] in
            self?.showFullScreenHUD()
        }) { [weak self] (error)  in
            DispatchQueue.main.async {
                self?.hideFullScreenHUD()
                self?.viewModel.userHasPoints = self?.viewModel.showPointsHeader()
                guard error == nil else {
                    debugPrint("Error Message: \(error!)")
                    self?.initErrorHandler(error!)
                    return
                }
                
                self?.levels = self?.viewModel.getDeviceBenefitGoalValues()
                self?.tableView.reloadData()
            }
        }
    }
    
    func initErrorHandler(_ error: Error) {
        let backendError = error as? BackendError ?? BackendError.other
        self.handleBackendErrorWithAlert(backendError)
    }
    
    func initLinkHealthApp(identifier: String) {
        /* Call ProcessEvents to Link Apple Health */
        self.viewModel.linkAppleHealthProcessEvents(onStart: { [weak self] in
            self?.showFullScreenHUD()
            }, completion: { (error) in
                DispatchQueue.main.async {
                    self.hideFullScreenHUD()
                    guard error == nil else {
                        self.initErrorHandler(error!)
                        return
                    }
                    self.performSegue(withIdentifier: identifier, sender: self)
                }
        })
    }
    
    override public func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.hideBackButtonTitle()
    }
    
    func initUI() {
        initNavigationBar()
        initTableView()
    }
    
    func initNavigationBar() {
        navigationController?.makeNavigationBarTransparent()
    }
    
    func initTableView() {
        self.tableView.register(AWCHeaderTableViewCell.nib(), forCellReuseIdentifier: AWCHeaderTableViewCell.defaultReuseIdentifier)
        self.tableView.register(AWCHeaderInfoCell.nib(), forCellReuseIdentifier: AWCHeaderInfoCell.defaultReuseIdentifier)
        self.tableView.register(VIATableViewCell.nib(), forCellReuseIdentifier: VIATableViewCell.defaultReuseIdentifier)
        self.tableView.register(AWCHeaderStatusCell.nib(), forCellReuseIdentifier: AWCHeaderStatusCell.defaultReuseIdentifier)
        self.tableView.register(AWCActivityCell.nib(), forCellReuseIdentifier: AWCActivityCell.defaultReuseIdentifier)
        self.tableView.register(AWCHeaderBlockStatusCell.nib(), forCellReuseIdentifier: AWCHeaderBlockStatusCell.defaultReuseIdentifier)
        self.tableView.register(VIATableViewSectionHeaderFooterView.nib(), forHeaderFooterViewReuseIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier)
        self.tableView.estimatedRowHeight = 96
        self.tableView.separatorInset = UIEdgeInsets(top: 0, left: 52, bottom: 0, right: 0)
        self.tableView.tableFooterView = UIView()
        self.tableView.backgroundColor = UIColor.tableViewBackground()
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        guard let hasPoints = viewModel.userHasPoints else {
            return UITableViewAutomaticDimension
        }
        if !hasPoints {
            let section = Sections(rawValue: section)
            if (section == .headerInfo && viewModel.cardStatus != .Activated)
                || section == .others{
                return 0
            }
            return UITableViewAutomaticDimension
        } else {
            switch Sections.allValues[section] {
            case .headerInfo, .header:
                return 0
            default:
                return UITableViewAutomaticDimension
            }
        }
        
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        let section = Sections(rawValue: section)
        
        guard let hasPoints = viewModel.userHasPoints else {
            return UITableViewAutomaticDimension
        }
        
        if section == .headerInfo || (!hasPoints && section == .header){
            return 0
        }
        
        return UITableViewAutomaticDimension
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return Sections.allValues.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let hasPoints = viewModel.userHasPoints else{
            return 0
        }
        switch Sections.allValues[section] {
        case .header:
            /* If user has points,return 2. Else, return 0.*/
            return hasPoints ? 2 : 0
            
        case .activity:
            /* If user has points,return 1. Else, return 0.*/
            return hasPoints ? 1 : 0
            
        case .others:
            let menuItems = Others.allValues.count
            return menuItems
            
            /* Fallback to 0 */
        default: return 0
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let hasPoints = viewModel.userHasPoints else {
            return self.tableView.defaultTableViewCell()
        }
        if !hasPoints {
            switch Sections.allValues[indexPath.section] {
            case .header:
                return pointsHeader(at: indexPath)!
            case .others:
                return otherCell(at: indexPath)
            default:
                break
            }
        } else {
            switch Sections.allValues[indexPath.section] {
            case .header:
                return pointsHeader(at: indexPath)!
            case .activity:
                return activityCell(at: indexPath)!
            case .others:
                return otherCell(at: indexPath)
            default:
                break
            }
        }
        return self.tableView.defaultTableViewCell()
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let section = Sections(rawValue: indexPath.section) {
            switch section {
            case .activity:
                self.performSegue(withIdentifier: section.getSegueIdentifier(), sender: nil)
            case .others:
                if let segueIdentifier = Others(rawValue: indexPath.row)?.getSegueIdentifier(){
                    self.performSegue(withIdentifier: segueIdentifier, sender: nil)
                }
            default:
                break
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let section = Sections(rawValue: section)
        guard let hasPoints = viewModel.userHasPoints else {
            return nil
        }
        if !hasPoints {
            if section == .headerInfo{
                return viewModel.setHeaderInfoView(tableView: self.tableView)
            } else if section == .header {
                return viewModel.setHeaderView(tableView: self.tableView)
            }
        } else {
            if section == .activity {
                let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier) as!
                VIATableViewSectionHeaderFooterView
                view.labelText = CommonStrings.DcLandingActivityTitle1533
                view.backgroundColor = UIColor.tableViewBackground()
                return view
            }
        }
        return nil
    }
    
    func otherCell(at indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: VIATableViewCell.defaultReuseIdentifier, for: indexPath)
        let otherCell = cell as! VIATableViewCell
        
        let defaultMenuValue = Others.allValues[indexPath.row]
        otherCell.labelText = defaultMenuValue.title()
        otherCell.cellImage = defaultMenuValue.image()?.templatedImage
        cell.accessoryType = .disclosureIndicator
        return cell
    }
    
    func activityCell(at indexPath: IndexPath) -> UITableViewCell? {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: AWCActivityCell.defaultReuseIdentifier, for: indexPath) as? AWCActivityCell
        
        if let levelDetails = self.levels {
            if let rewardValue = levelDetails.reward?.accumulatedQuantity {
                // TODO: Tokens to be created
                cell?.coinsValueLabelText = "\(rewardValue)"
                cell?.pointsLabelText = CommonStrings.Awc.PotentialPoints.SectionHeader495("\(levelDetails.pointsAchieved)")
                cell?.dateLabelText = self.viewModel.getGoalTrackerValues()
                self.dateValue = self.viewModel.getGoalTrackerDate()
                
                //TODO: Needs to clarify if the status should always be in progress, no other scenarios as found on Journey and API mapping doc
                cell?.statusLabelText = CommonStrings.Awc.LandingActivitySubTitle2026
                self.progressText = cell?.statusLabelText
                
                self.amountAwarded = "\(rewardValue) \(CommonStrings.Awc.TrackCoins.CoinsTitle2008)"
                self.pointsAwarded = CommonStrings.Awc.PotentialPoints.SectionHeader495("\(levelDetails.pointsAchieved)")
                
                switch rewardValue{
                case 0:
                    cell?.cellImage = VIAAppleWatchCashbackAsset.Landing.pinkBlockActive.image
                case 500:
                    cell?.cellImage = VIAAppleWatchCashbackAsset.Landing.orangeBlockActive.image
                case 750:
                    cell?.cellImage = VIAAppleWatchCashbackAsset.Landing.blueBlockActive.image
                case 1000:
                    cell?.cellImage = VIAAppleWatchCashbackAsset.Landing.greenBlockActive.image
                default:
                    break
                }
            }
        }
        
        viewModel.statusImage = cell?.cellImage
        
        return cell
    }
    
    func pointsHeader(at indexPath: IndexPath) -> UITableViewCell? {
        if let rows = HeaderRows(rawValue: indexPath.row) {
            switch rows {
            case .status:
                let cell = self.tableView.dequeueReusableCell(withIdentifier: AWCHeaderStatusCell.defaultReuseIdentifier, for: indexPath) as? AWCHeaderStatusCell
                
                let (currentBenefitPeriod, terms) = self.viewModel.getBenefitMonth()
                cell?.dateLabelText = Localization.shortMonthAndYearFormatter.string(from: Date())
                cell?.monthLabelText = CommonStrings.DcLandingProgressMonth1531(currentBenefitPeriod, terms)
                cell?.pointsTitleLabelText = CommonStrings.Awc.LandingProgressMonthCurrentCoins2024
                cell?.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: (cell?.frame.width)!)
                
                if let levelDetails = self.levels {
                    if let rewardValue = levelDetails.reward?.accumulatedQuantity {
                        cell?.pointsLabelText = CommonStrings.Awc.LandingProgressMonthCurrentPoints2025("\(levelDetails.pointsAchieved)")
                        cell?.rewardsLabelText = "\(rewardValue)"
                        
                        switch rewardValue{
                        case 0:
                            cell?.rewardsLabel.textColor = self.viewModel.tierColor(tier: 0)
                        case 500:
                            cell?.rewardsLabel.textColor = self.viewModel.tierColor(tier: 1)
                        case 750:
                            cell?.rewardsLabel.textColor = self.viewModel.tierColor(tier: 2)
                        case 1000:
                            cell?.rewardsLabel.textColor = self.viewModel.tierColor(tier: 3)
                        default:
                            break
                        }
                    }
                }
                
                return cell
            case .blocks:
                let cell = self.tableView.dequeueReusableCell(withIdentifier: AWCHeaderBlockStatusCell.defaultReuseIdentifier, for: indexPath) as? AWCHeaderBlockStatusCell
                let levels = PointsLevel.allValues
                
                for level in levels {
                    guard let block = level.blocks(), let activeBLock = level.activeBlocks() else {
                        break
                    }
                    viewModel.imageBlocks.append(block)
                    viewModel.imageBlocksActive.append(activeBLock)
                }
                if let levelDetails = self.levels {
                    if let rewardValue = levelDetails.reward?.accumulatedQuantity {
                        
                        if rewardValue == 0 { cell?.firstBlockViewLabelText = "\(rewardValue) \(CommonStrings.Awc.TrackCoins.CoinsTitle2008)" }
                        if rewardValue == 500 { cell?.secondBlockViewLabelText = "\(rewardValue) \(CommonStrings.Awc.TrackCoins.CoinsTitle2008)" }
                        if rewardValue == 750 { cell?.thirdBlockViewLabelText = "\(rewardValue) \(CommonStrings.Awc.TrackCoins.CoinsTitle2008)" }
                        if rewardValue == 1000 { cell?.fourthBlockViewLabelText = "\(rewardValue) \(CommonStrings.Awc.TrackCoins.CoinsTitle2008)" }
                        
                        switch rewardValue{
                        case 0:
                            cell?.firstBlockViewImage = viewModel.imageBlocksActive[0]
                            cell?.secondBlockViewImage = viewModel.imageBlocks[1]
                            cell?.thirdBlockViewImage = viewModel.imageBlocks[2]
                            cell?.fourthBlockViewImage = viewModel.imageBlocks[3]
                        case 500:
                            cell?.firstBlockViewImage = viewModel.imageBlocks[0]
                            cell?.secondBlockViewImage = viewModel.imageBlocksActive[1]
                            cell?.thirdBlockViewImage = viewModel.imageBlocks[2]
                            cell?.fourthBlockViewImage = viewModel.imageBlocks[3]
                        case 750:
                            cell?.firstBlockViewImage = viewModel.imageBlocks[0]
                            cell?.secondBlockViewImage = viewModel.imageBlocks[1]
                            cell?.thirdBlockViewImage = viewModel.imageBlocksActive[2]
                            cell?.fourthBlockViewImage = viewModel.imageBlocks[3]
                        case 1000:
                            cell?.firstBlockViewImage = viewModel.imageBlocks[0]
                            cell?.secondBlockViewImage = viewModel.imageBlocks[1]
                            cell?.thirdBlockViewImage = viewModel.imageBlocks[2]
                            cell?.fourthBlockViewImage = viewModel.imageBlocksActive[3]
                        default:
                            break
                        }
                    }
                }
                return cell
            }
        }
        return self.tableView.defaultTableViewCell()
    }
}

extension AWCLandingViewController{
    
    // MARK: Show On Boarding once
    func showOnboardingIfNeeded() {
        if !AppSettings.hasShownAppleWatchCashbackOnboarding() {
            self.performSegue(withIdentifier: "showOnBoarding", sender: self)
        }
    }
}

extension AWCLandingViewController{
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showActivityDetail" {
            if let AWCActivityDetails = segue.destination as? AWCActivityDetailsViewController {
                AWCActivityDetails.headerImage = viewModel.statusImage
                AWCActivityDetails.headerImageText = self.amountAwarded
                AWCActivityDetails.pointsProgressText = self.pointsAwarded
                AWCActivityDetails.progressText = self.progressText
                AWCActivityDetails.dateTitle = self.dateValue
            }
        }
        
        if segue.identifier == "showVMP"{
            guard let appWatchURL = viewModel.getAppleWatchUrl() else { return }
            if let vc = segue.destination as? AWCVMPLinkViewController {
                vc.url = appWatchURL
            }
        }
    }
}



extension AWCLandingViewController{
    
    public func displayErrorAlertController(title : String, message : String, completion: (()->Void)? = nil) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: CommonStrings.ContinueButtonTitle87, style: .cancel) { action in
            completion?()
        })
        self.present(alert, animated: true, completion: nil)
    }
    
}
