//
//  AWCLandingViewController+Config.swift
//  VitalityActive
//
//  Created by OJ Garde on 5/14/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import VitalityKit
import VIAUtilities

extension AWCLandingViewController{
    
    enum Sections: Int, EnumCollection, Equatable {
        case headerInfo = 0
        case header = 1
        case activity = 2
        case others = 3
        
        public func getSegueIdentifier() -> String{
            switch self{
            case .activity:
                return "showActivityDetail"
            default:
                return ""
            }
        }
    }
    
    enum HeaderRows: Int, EnumCollection, Equatable {
        case status = 0
        case blocks = 1
    }
    
    enum PointsLevel: Int, EnumCollection, Equatable {
        case first = 0
        case second = 1
        case third = 2
        case fourth = 3
        
        func blocks() -> UIImage? {
            switch self {
            case .first:
                return VIAAppleWatchCashbackAsset.Landing.pinkBlock.image
            case .second:
                return VIAAppleWatchCashbackAsset.Landing.orangeBlock.image
            case .third:
                return VIAAppleWatchCashbackAsset.Landing.blueBlock.image
            case .fourth:
                return VIAAppleWatchCashbackAsset.Landing.greenBlock.image
            }
        }
        
        func activeBlocks() -> UIImage? {
            switch self {
            case .first:
                return VIAAppleWatchCashbackAsset.Landing.pinkBlockActive.image
            case .second:
                return VIAAppleWatchCashbackAsset.Landing.orangeBlockActive.image
            case .third:
                return VIAAppleWatchCashbackAsset.Landing.blueBlockActive.image
            case .fourth:
                return VIAAppleWatchCashbackAsset.Landing.greenBlockActive.image
            }
        }
    }
    
    enum Others: Int, EnumCollection, Equatable {
        case cashbacksEarned = 0
        case howToTrackCashbacks = 1
        case learnMore = 2
        case help = 3
        
        func title() -> String? {
            switch self {
            case .cashbacksEarned:
                return CommonStrings.Awc.Landing.ActionMenuTitle1999
            case .howToTrackCashbacks:
                return CommonStrings.Awc.Landing.ActionMenuTitle2000
            case .learnMore:
                return CommonStrings.LearnMoreButtonTitle104
            case .help:
                return CommonStrings.HelpButtonTitle141
            }
        }
        
        func image() -> UIImage? {
            switch self {
            case .cashbacksEarned:
                return VIAAppleWatchCashbackAsset.Landing.coinsEarned.image
            case .howToTrackCashbacks:
                return VIAAppleWatchCashbackAsset.Landing.howToTrack.image
            case .learnMore:
                return VIAAppleWatchCashbackAsset.Landing.learnMoreSmall.image
            case .help:
                return VIAAppleWatchCashbackAsset.Landing.helpSmall.image
            }
        }
        
        func getSegueIdentifier() -> String{
            switch self{
            case .cashbacksEarned: return "showVitalityCoinsEarned"
            case .howToTrackCashbacks: return "showHowToTrack"
            case .learnMore: return "showLearnMore"
            case .help: return "showHelp"
            }
        }
    }
}
