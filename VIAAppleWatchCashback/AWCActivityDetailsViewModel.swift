//
//  AWCActivityDetailsViewModel.swift
//  VitalityActive
//
//  Created by Von Kervin R. Tuico on 12/05/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import VitalityKit
import RealmSwift

class AWCActivityDetailsViewModel: AWCActivityDetailsViewModelDelegate {
    let realm = DataProvider.newRealm()
    internal let cashbackRealm = DataProvider.newGDCRealm()
    
    /* Get Benefit ID from GetDeviceBenefitResponse.BenefitOut.Id */
    func getBenefitID() -> Int? {
        /* Get Benefits value from Realm */
        let benefits = cashbackRealm.allGDCBenefit()
        return benefits.first?.benefitId
    }
    
    func getGoalTrackerOuts(benefitID: Int, date: String) -> GDCGoalTrackerOuts? {
        /* Get GoalProgressAndDetails value from Realm */
        let allGetGoalProgressAndDetails = cashbackRealm.allGDCGetGoalProgressAndDetails()
        
        /* Get Goal Tracker Outs inside GetProgressAndDetails Realm */
        var goalTrackerOuts: List<GDCGoalTrackerOuts> = List<GDCGoalTrackerOuts>()
        for details in allGetGoalProgressAndDetails {
            goalTrackerOuts = details.goalTrackerOuts
            break
        }
        
        /* Get the corresponding GoalTrackerOuts value from matching the Agreement ID and Benefit ID */
        var goalTrackerOutValue: GDCGoalTrackerOuts?
        for goalTrackerOut in goalTrackerOuts {
            if goalTrackerOut.agreementId == benefitID && goalTrackerOut.effectiveFrom == date {
                goalTrackerOutValue = goalTrackerOut
                break
            }
        }
        
        return goalTrackerOutValue
    }
    
    func getObjectivePointsEntries(goalTrackerOut: GDCGoalTrackerOuts) -> [GDCObjectivePointsEntries]? {
        /* Get the List of Objective Trackers */
        var objectiveTrackerValue: [GDCObjectiveTrackers]? = []
        for objectiveTracker in goalTrackerOut.objectiveTrackers {
            objectiveTrackerValue?.append(objectiveTracker)
        }

        let sortedObjectiveTrackerValue = objectiveTrackerValue?.sorted(by: { $0.objectiveCode?.compare($1.objectiveCode!) == .orderedDescending})

        /* Get the List of Points Entries */
        var objectivePointsEntriesValue: [GDCObjectivePointsEntries]? = []
        if let objectivePointsEntries = sortedObjectiveTrackerValue?.first?.objectivePointsEntries {
            for objectivePointsEntry in objectivePointsEntries {
                objectivePointsEntriesValue?.append(objectivePointsEntry)
            }
        }
        return objectivePointsEntriesValue?.sorted(by: { $0.effectiveDate?.compare($1.effectiveDate!) == .orderedDescending}) ?? []
    }
    
    func getGoalProgressAndDetails(completion: @escaping (_ error: Error?) -> Void) {
        let tenantID = realm.getTenantId()
        let partyID = realm.getPartyId()
        let benefits = cashbackRealm.allGDCBenefit()
        let goals = cashbackRealm.allGDCGoal()
        
        /* Get Effective Dates from GetDeviceBenefitResponse.Benefit */
        var effectiveFrom = ""
        var effectiveTo = ""
        for benefit in benefits {
            let state = benefit.state
            
            guard let dateFrom = state?.effectiveFrom else { return }
            guard let dateTo = state?.effectiveTo else { return }
            
            effectiveFrom = dateFrom
            effectiveTo = dateTo
        }
        
        /* Convertion of dates fetched from string type to date type */
        let effectiveFromDateType = DateFormatter.yearMonthDayFormatter().date(from: effectiveFrom)
        let effectiveToDateType = DateFormatter.yearMonthDayFormatter().date(from: effectiveTo)
        
        /* Get Goal Keys from GetDeviceBenefitResponse.Goal */
        var goalKeys: [Int] = []
        for goal in goals {
            goalKeys.append(goal.key)
        }
        
        let request = GetGoalProgressAndDetailsParameters(effectiveDateFrom: effectiveFromDateType, effectiveDateTo: effectiveToDateType, goalKeys: goalKeys, goalStatusTypeKeys: [], partyId: partyID)
        
        Wire.Events.getGDCGoalProgressAndDetails(tenantId: tenantID, request: request) { (error, response) in
            completion(error)
        }
    }
    
    func formattedDateFromString(dateString: String, withFormat format: String) -> String? {
        
        let inputFormatter = DateFormatter()
        inputFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZZZ'['VV']'"
        
        if let date = Date.parseStringToDate(dateString) {

            let outputFormatter = DateFormatter()
            outputFormatter.setLocalizedDateFormatFromTemplate(format)
            
            return outputFormatter.string(from: date)
        }
        
        return nil
    }
    
    func monthYearFormatter (date: String?) -> String {
        let dateResponseFormatter = DateFormatter()
        dateResponseFormatter.dateFormat = "yyyy-MM-dd"
        let dateStringFormatter = DateFormatter()
        
        if DeviceLocale.toString() == "ja_JP" {
            dateStringFormatter.setLocalizedDateFormatFromTemplate("yyyy MMMM")
        } else {
            dateStringFormatter.dateFormat = "MMMM yyyy"
        }
        
        guard let dateValue = date else {
            return ""
        }
        let monthDate: Date? = dateResponseFormatter.date(from: (dateValue))
        
        return dateStringFormatter .string(from: monthDate!)
    }
}
