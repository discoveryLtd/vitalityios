//
//  AWCOnBoardingViewModel.swift
//  VitalityActive
//
//  Created by Michelle R. Oratil on 04/05/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//


import VitalityKit
import VIAUIKit
import VIAUtilities

public class AWCOnBoardingViewModel: AnyObject, OnboardingViewModel {
    
    public var heading: String {
        return CommonStrings.Awc.HomeCard.Title1991
    }
    
    public var contentItems: [OnboardingContentItem] {
        var items = [OnboardingContentItem]()
        items.append(OnboardingContentItem(heading: CommonStrings.Awc.HomeCard.Message1992,
                                           content: CommonStrings.Awc.Onboarding.SectionMessage1994,
                                           image: VIAAppleWatchCashbackAsset.OnBoarding.appleWatchSmall.image))
        items.append(OnboardingContentItem(heading: CommonStrings.Dc.Onboarding.SectionTitle1460,
                                           content: CommonStrings.Awc.Onboarding.SectionMessage1995,
                                           image: VIAAppleWatchCashbackAsset.OnBoarding.getActive.image))
        items.append(OnboardingContentItem(heading: CommonStrings.SectionTitleGetRewarded278,
                                           content: CommonStrings.Awc.Onboarding.SectionMessage1996,
                                           image: VIAAppleWatchCashbackAsset.OnBoarding.getRewarded.image))
        return items
    }
    
    public var buttonTitle: String {
        return CommonStrings.Dc.Onboarding.GotIt131
    }
    
    public var alternateButtonTitle: String? {
        return CommonStrings.LearnMoreButtonTitle104
    }
    
    public var gradientColor: Color {
        return .blue
    }
    
    public var mainButtonHighlightedTextColor: UIColor? {
        return .improveYourHealthBlue()
    }
}
