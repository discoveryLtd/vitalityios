//
//  AWCVitalityCoinsEarnedCell.swift
//  VitalityActive
//
//  Created by Michelle R. Oratil on 05/05/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import VIAUIKit

class AWCVitalityCoinsEarnedCell: UITableViewCell, Nibloadable {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    @IBOutlet weak var deviceLabel: UILabel!
    public var deviceText: String? {
        set {
            self.deviceLabel.text = newValue
        }
        get {
            return self.deviceLabel.text
        }
    }
    
    @IBOutlet weak var pointsLabel: UILabel!
    public var pointsText: String? {
        set {
            self.pointsLabel.text = newValue
        }
        get {
            return self.pointsLabel.text
        }
    }
 
    
    @IBOutlet weak var dateLabel: UILabel!
    public var dateText: String? {
        set {
            self.dateLabel.text = newValue
        }
        get {
            return self.dateLabel.text
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}

