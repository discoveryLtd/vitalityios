// Generated using SwiftGen, by O.Halligon — https://github.com/SwiftGen/SwiftGen

#if os(OSX)
import AppKit.NSImage
public typealias VIAAppleWatchCashbackColor = NSColor
public typealias VIAAppleWatchCashbackImage = NSImage
#elseif os(iOS) || os(tvOS) || os(watchOS)
import UIKit.UIImage
public typealias VIAAppleWatchCashbackColor = UIColor
public typealias VIAAppleWatchCashbackImage = UIImage
#endif

// swiftlint:disable file_length

public typealias VIAAppleWatchCashbackAssetType = VIAAppleWatchCashbackImageAsset

public struct VIAAppleWatchCashbackImageAsset: Equatable {
	fileprivate var name: String

	public var templateImage: VIAAppleWatchCashbackImage {
		let bundle = Bundle(for: BundleToken.self)
		#if os(iOS) || os(tvOS)
		let image = VIAAppleWatchCashbackImage(named: name, in: bundle, compatibleWith: nil)?.withRenderingMode(.alwaysTemplate)
		#elseif os(OSX)
		let image = bundle.image(forResource: name).withRenderingMode(.alwaysTemplate)
		#elseif os(watchOS)
		let image = VIAAppleWatchCashbackImage(named: name).withRenderingMode(.alwaysTemplate)
		#endif
		guard let result = image else { fatalError("Unable to load template image named \(name).") }
		return result
	}

	public var image: VIAAppleWatchCashbackImage {
		let bundle = Bundle(for: BundleToken.self)
		#if os(iOS) || os(tvOS)
		let image = VIAAppleWatchCashbackImage(named: name, in: bundle, compatibleWith: nil)
		#elseif os(OSX)
		let image = bundle.image(forResource: name)
		#elseif os(watchOS)
		let image = VIAAppleWatchCashbackImage(named: name)
		#endif
		guard let result = image else { fatalError("Unable to load image named \(name).") }
		return result
	}

	public static func ==(lhs: VIAAppleWatchCashbackImageAsset, rhs: VIAAppleWatchCashbackImageAsset) -> Bool {
        return lhs.image == rhs.image
    }
}

public struct VIAAppleWatchCashbackColorAsset {
fileprivate var name: String

#if swift(>=3.2)
@available(iOS 11.0, tvOS 11.0, watchOS 4.0, OSX 10.13, *)
var color: VIAAppleWatchCashbackColor {
return VIAAppleWatchCashbackColor(asset: self)
}
#endif
}

// swiftlint:disable identifier_name line_length nesting type_body_length type_name
public enum VIAAppleWatchCashbackAsset {
  public enum Landing {
    public static let coinsEarned = VIAAppleWatchCashbackImageAsset(name: "coinsEarned")
    public static let greenBlockActive = VIAAppleWatchCashbackImageAsset(name: "greenBlockActive")
    public enum HowToTrackVitalityCoin {
      public static let steps = VIAAppleWatchCashbackImageAsset(name: "steps")
      public static let gym = VIAAppleWatchCashbackImageAsset(name: "gym")
      public static let heartRate = VIAAppleWatchCashbackImageAsset(name: "heartRate")
    }
    public static let arrowDrill = VIAAppleWatchCashbackImageAsset(name: "arrowDrill")
    public static let blueBlock = VIAAppleWatchCashbackImageAsset(name: "blueBlock")
    public static let calendar = VIAAppleWatchCashbackImageAsset(name: "calendar")
    public static let howToTrack = VIAAppleWatchCashbackImageAsset(name: "howToTrack")
    public static let cashbackEarnedGraphic = VIAAppleWatchCashbackImageAsset(name: "cashbackEarnedGraphic")
    public static let orangeBlock = VIAAppleWatchCashbackImageAsset(name: "orangeBlock")
    public static let orangeBlockActive = VIAAppleWatchCashbackImageAsset(name: "orangeBlockActive")
    public static let learnMoreSmall = VIAAppleWatchCashbackImageAsset(name: "learnMoreSmall")
    public static let cashbackCoinGraphic = VIAAppleWatchCashbackImageAsset(name: "cashbackCoinGraphic")
    public static let appleWatchSmall = VIAAppleWatchCashbackImageAsset(name: "appleWatchSmall")
    public static let illustrationRunning = VIAAppleWatchCashbackImageAsset(name: "illustrationRunning")
    public static let manRunningSmall = VIAAppleWatchCashbackImageAsset(name: "manRunningSmall")
    public static let blueBlockActive = VIAAppleWatchCashbackImageAsset(name: "blueBlockActive")
    public static let helpSmall = VIAAppleWatchCashbackImageAsset(name: "helpSmall")
    public static let illustrationDelivery = VIAAppleWatchCashbackImageAsset(name: "illustrationDelivery")
    public static let pinkBlockActive = VIAAppleWatchCashbackImageAsset(name: "pinkBlockActive")
    public static let greenBlock = VIAAppleWatchCashbackImageAsset(name: "greenBlock")
    public static let pinkBlock = VIAAppleWatchCashbackImageAsset(name: "pinkBlock")
  }
  public enum HomeCard {
    public static let addDeviceXsmall = VIAAppleWatchCashbackImageAsset(name: "addDeviceXsmall")
  }
  public enum LinkAppleHealth {
    public static let onboardingHealthAccess = VIAAppleWatchCashbackImageAsset(name: "onboardingHealthAccess")
    public static let onboardingActivated = VIAAppleWatchCashbackImageAsset(name: "onboardingActivated")
  }
  public enum LearnMore {
    public static let learnMoreGetAppleWatch = VIAAppleWatchCashbackImageAsset(name: "learnMoreGetAppleWatch")
    public static let learnMoreGetRewarded = VIAAppleWatchCashbackImageAsset(name: "learnMoreGetRewarded")
    public static let learnMoreGetActive = VIAAppleWatchCashbackImageAsset(name: "learnMoreGetActive")
  }
  public enum OnBoarding {
    public static let getRewarded = VIAAppleWatchCashbackImageAsset(name: "getRewarded")
    public static let appleWatchSmall = VIAAppleWatchCashbackImageAsset(name: "appleWatchSmall")
    public static let getActive = VIAAppleWatchCashbackImageAsset(name: "getActive")
  }

  // swiftlint:disable trailing_comma
  public static let allColors: [VIAAppleWatchCashbackColorAsset] = [
  ]
  public static let allImages: [VIAAppleWatchCashbackImageAsset] = [
    Landing.coinsEarned,
    Landing.greenBlockActive,
    Landing.HowToTrackVitalityCoin.steps,
    Landing.HowToTrackVitalityCoin.gym,
    Landing.HowToTrackVitalityCoin.heartRate,
    Landing.arrowDrill,
    Landing.blueBlock,
    Landing.calendar,
    Landing.howToTrack,
    Landing.cashbackEarnedGraphic,
    Landing.orangeBlock,
    Landing.orangeBlockActive,
    Landing.learnMoreSmall,
    Landing.cashbackCoinGraphic,
    Landing.appleWatchSmall,
    Landing.illustrationRunning,
    Landing.manRunningSmall,
    Landing.blueBlockActive,
    Landing.helpSmall,
    Landing.illustrationDelivery,
    Landing.pinkBlockActive,
    Landing.greenBlock,
    Landing.pinkBlock,
    HomeCard.addDeviceXsmall,
    LinkAppleHealth.onboardingHealthAccess,
    LinkAppleHealth.onboardingActivated,
    LearnMore.learnMoreGetAppleWatch,
    LearnMore.learnMoreGetRewarded,
    LearnMore.learnMoreGetActive,
    OnBoarding.getRewarded,
    OnBoarding.appleWatchSmall,
    OnBoarding.getActive,
  ]
  // swiftlint:enable trailing_comma
  @available(*, deprecated, renamed: "allImages")
  public static let allValues: [VIAAppleWatchCashbackAssetType] = allImages
}
// swiftlint:enable identifier_name line_length nesting type_body_length type_name

extension VIAAppleWatchCashbackImage {
@available(iOS 1.0, tvOS 1.0, watchOS 1.0, *)
@available(OSX, deprecated,
message: "This initializer is unsafe on macOS, please use the VIAAppleWatchCashbackImageAsset.image property")
convenience init!(asset: VIAAppleWatchCashbackAssetType) {
#if os(iOS) || os(tvOS)
let bundle = Bundle(for: BundleToken.self)
self.init(named: asset.name, in: bundle, compatibleWith: nil)
#elseif os(OSX) || os(watchOS)
self.init(named: asset.name)
#endif
}
}

extension VIAAppleWatchCashbackColor {
#if swift(>=3.2)
@available(iOS 11.0, tvOS 11.0, watchOS 4.0, OSX 10.13, *)
convenience init!(asset: VIAAppleWatchCashbackColorAsset) {
let bundle = Bundle(for: BundleToken.self)
#if os(iOS) || os(tvOS)
self.init(named: asset.name, in: bundle, compatibleWith: nil)
#elseif os(OSX)
self.init(named: asset.name, bundle: bundle)
#elseif os(watchOS)
self.init(named: asset.name)
#endif
}
#endif
}

private final class BundleToken {}
