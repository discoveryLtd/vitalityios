//
//  AWCOnBoardingViewController.swift
//  VitalityActive
//
//  Created by Michelle R. Oratil on 04/05/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import VitalityKit
import VIAUIKit

class AWCOnBoardingViewController: VIAOnboardingViewController {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.viewModel = AWCOnBoardingViewModel()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initNavigationBar()
        VIAAppearance.setGlobalTintColorToImproveYourHealth()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.navigationBar.isHidden = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.hideBackButtonTitle()
    }
    
    override func mainButtonTapped(_ sender: UIButton) {
        AppSettings.setHasShownAppleWatchCashbackOnboarding()
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: Learn More button
    
    override func alternateButtonTapped(_ sender: UIButton) {
        self.performSegue(withIdentifier: "showLearnMore", sender: self)
    }
    
    func initNavigationBar() {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.hideBackButtonTitle()
        self.navigationItem.setHidesBackButton(true, animated:true);
    }
    
    public override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        //        if let vc = segue.destination as? SVLearnMoreViewController{
        //            vc.fromOnboarding           = true
        //            vc.landingCellsGroupData    = SAVLandingDataController().allScreeningsAndVaccinations()
        //        }
    }
    
}
