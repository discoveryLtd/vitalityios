//
//  AWCHeaderBlockStatusCell.swift
//  VitalityActive
//
//  Created by Steven Layug on 5/7/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import VIAUIKit

public class AWCHeaderBlockStatusCell: UITableViewCell, Nibloadable {
    
    
    @IBOutlet weak var firstBlockView: UIImageView!
    public var firstBlockViewImage: UIImage?{
        set{
            self.firstBlockView.image = newValue
        }
        get{
            return self.firstBlockView.image
        }
    }
    
    @IBOutlet weak var firstBlockViewLabel: UILabel!
    public var firstBlockViewLabelText: String?{
        set{
            self.firstBlockViewLabel.text = newValue
        }
        get{
            return self.firstBlockViewLabel.text
        }
    }
    
    @IBOutlet weak var secondBlockView: UIImageView!
    public var secondBlockViewImage: UIImage?{
        set{
            self.secondBlockView.image = newValue
        }
        get{
            return self.secondBlockView.image
        }
    }
    
    @IBOutlet weak var secondBlockViewLabel: UILabel!
    public var secondBlockViewLabelText: String?{
        set{
            self.secondBlockViewLabel.text = newValue
        }
        get{
            return self.secondBlockViewLabel.text
        }
    }
    
    @IBOutlet weak var thirdBlockView: UIImageView!
    public var thirdBlockViewImage: UIImage?{
        set{
            self.thirdBlockView.image = newValue
        }
        get{
            return self.thirdBlockView.image
        }
    }
    
    @IBOutlet weak var thirdBlockViewLabel: UILabel!
    public var thirdBlockViewLabelText: String?{
        set{
            self.thirdBlockViewLabel.text = newValue
        }
        get{
            return self.thirdBlockViewLabel.text
        }
    }
    
    @IBOutlet weak var fourthBlockView: UIImageView!
    public var fourthBlockViewImage: UIImage?{
        set{
            self.fourthBlockView.image = newValue
        }
        get{
            return self.fourthBlockView.image
        }
    }
    
    @IBOutlet weak var fourthBlockViewLabel: UILabel!
    public var fourthBlockViewLabelText: String?{
        set{
            self.fourthBlockViewLabel.text = newValue
        }
        get{
            return self.fourthBlockViewLabel.text
        }
    }
    
    @IBOutlet weak var firstPointLabel: UILabel!
    public var firstPointLabelText: String?{
        set{
            self.firstPointLabel.text = newValue
        }
        get{
            return self.firstPointLabel.text
        }
    }
    
    @IBOutlet weak var secondPointLabel: UILabel!
    public var secondPointLabelText: String?{
        set{
            self.secondPointLabel.text = newValue
        }
        get{
            return self.secondPointLabel.text
        }
    }
    
    @IBOutlet weak var thirdPointLabel: UILabel!
    public var thirdPointLabelText: String?{
        set{
            self.thirdPointLabel.text = newValue
        }
        get{
            return self.thirdPointLabel.text
        }
    }
    
    @IBOutlet weak var fourthPointLabel: UILabel!
    public var fourthPointLabelText: String?{
        set{
            self.fourthPointLabel.text = newValue
        }
        get{
            return self.fourthPointLabel.text
        }
    }
}

