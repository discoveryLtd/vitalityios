//
//  AWCLearnMoreViewModel.swift
//  VitalityActive
//
//  Created by Michelle R. Oratil on 07/05/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import VIAUIKit
import VitalityKit
import VIACommon

class AWCLearnMoreViewModel: AnyObject, VIALearnMoreViewModel {
    
    public var title: String {
        return CommonStrings.LearnMoreButtonTitle104
    }
    
    public var contentItems: [LearnMoreContentItem] {
        var items = [LearnMoreContentItem]()
        
        items.append(LearnMoreContentItem(
            header: CommonStrings.Awc.Learnmore.SectionHeaderTitle2001,
            content: CommonStrings.Awc.Learnmore.SectionHeaderDescription2002
        ))
        
        items.append(LearnMoreContentItem(
            header: CommonStrings.Awc.HomeCard.Title1991,
            content: CommonStrings.Awc.Learnmore.SectionBodyTitle2003,
            image: UIImage(asset: VIAAppleWatchCashbackAsset.LearnMore.learnMoreGetAppleWatch)
        ))
        
        items.append(LearnMoreContentItem(
            header: CommonStrings.Dc.Onboarding.SectionTitle1460,
            content: CommonStrings.Awc.Onboarding.SectionMessage1995,
            image: UIImage(asset: VIAAppleWatchCashbackAsset.LearnMore.learnMoreGetActive)
        ))
        
        items.append(LearnMoreContentItem(
            header: CommonStrings.SectionTitleGetRewarded278,
            content: CommonStrings.Awc.Onboarding.SectionMessage1996,
            image: UIImage(asset: VIAAppleWatchCashbackAsset.LearnMore.learnMoreGetRewarded)
        ))
        
        return items
    }
    
    public var imageTint: UIColor {
        return .improveYourHealthBlue()
    }
}
