//
//  AWCHeaderTableViewCell.swift
//  VitalityActive
//
//  Created by Michelle R. Oratil on 04/05/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import VIAUIKit

public class AWCHeaderTableViewCell: UITableViewCell, Nibloadable {
    
    
    @IBOutlet weak var headerUIImageView: UIImageView!
    @IBOutlet weak var fullWidthHeaderUIImageView: UIImageView!
    
    @IBOutlet weak var headerTitleLabel: UILabel!
    public var headerTitle: String?{
        set{
            self.headerTitleLabel.text = newValue
        }
        get{
            return self.headerTitleLabel.text
        }
    }
    
    @IBOutlet weak var headerDescriptionLabel: UILabel!
    public var headerDescription: String? {
        set {
            self.headerDescriptionLabel.text = newValue
        }
        get {
            return self.headerDescriptionLabel.text
        }
    }
    
    @IBOutlet weak var headerButton: VIAButton!
    public var headerButtonTitle: String? {
        set {
            self.headerButton.setTitle(newValue, for: .normal)
        }
        get {
            return self.headerButton.currentTitle
        }
    }
    
    @IBOutlet weak var headerLinkButton: VIAButton!
    public var headerLinkTitle: String? {
        set {
            self.headerLinkButton.setTitle(newValue, for: .normal)
        }
        get {
            return self.headerLinkButton.currentTitle
        }
        
    }
    
    override public func awakeFromNib() {
        headerTitleLabel.text = nil
        headerTitleLabel.font = UIFont.title2Font()
        headerTitleLabel.textColor = UIColor.night()
        
        headerDescriptionLabel.text = nil
        headerDescriptionLabel.font = UIFont.bodyFont()
        headerDescriptionLabel.textColor = UIColor.darkGrey()
        
        headerButton.tintColor = UIColor.currentGlobalTintColor()
        headerButton.setTitle(nil, for: .normal)
        headerButton.titleLabel?.font = UIFont.bodyFont()
        headerButton.isHidden = true
        
        headerLinkButton.tintColor = UIColor.currentGlobalTintColor()
        headerLinkButton.setTitle(nil, for: .normal)
        headerLinkButton.titleLabel?.font = UIFont.bodyFont()
        headerLinkButton.isHidden = true
    }
    
    override public func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
}
