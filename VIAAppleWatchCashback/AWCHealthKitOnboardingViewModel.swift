//
//  AWCHealthKitOnboardingViewModel.swift
//  VitalityActive
//
//  Created by Michelle R. Oratil on 17/05/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import VIAUIKit
import VitalityKit

class AWCHealthKitOnboardingViewModel: AnyObject, CirclesViewModel {
    
    public required init() {
    }
    
    public var image: UIImage? {
        return VIAAppleWatchCashbackAsset.LinkAppleHealth.onboardingHealthAccess.image
    }
    
    public var heading: String? {
        return CommonStrings.HealthKit.OnboardingHeaders451
    }
    
    public var message: String? {
        return CommonStrings.Awc.Onboarding.SectionMessage2020
    }
    
    public var buttonTitle: String? {
        return ""
    }
    
    public var footnote: String? {
        return ""
    }
    
    public var gradientColor: Color {
        return .blue
    }
}
