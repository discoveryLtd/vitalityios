//
//  AWCVitalityCoinsHeaderCell.swift
//  VitalityActive
//
//  Created by Michelle R. Oratil on 08/05/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import VIAUIKit

class AWCVitalityCoinsHeaderCell: UITableViewCell, Nibloadable {

    
    @IBOutlet weak var bannerImage: UIImageView!
    @IBOutlet weak var coinsEarnedLabel: UILabel!
    public var coinsEarnedText: String? {
        set {
            self.coinsEarnedLabel.text = newValue
        }
        get {
            return self.coinsEarnedLabel.text
        }
    }
    
    @IBOutlet weak var dateRangeLabel: UILabel!
    public var dateRangeText: String? {
        set {
            self.dateRangeLabel.text = newValue
        }
        get {
            return self.dateRangeLabel.text
        }
    }
    
    @IBOutlet weak var monthsLabel: UILabel!
    public var monthsText: String? {
        set {
            self.monthsLabel.text = newValue
        }
        get {
            return self.monthsLabel.text
        }
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
}
