//
//  AWCLandingViewModel.swift
//  VitalityActive
//
//  Created by Michelle R. Oratil on 04/05/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import VitalityKit
import VIAUtilities
import VIAUIKit
import VIAHealthKit
import RealmSwift
import VIACommon

protocol AWCLandingViewModelDelegate: class {
    func setHeaderInfoView(tableView: UITableView) -> UIView
    func setHeaderView(tableView: UITableView) -> UIView
    func setDeviceLinkStatus()
}

class AWCLandingViewModel: AWCLandingViewModelDelegate {

    var userHasPoints: Bool? = false
    
    var imageBlocks: [UIImage] = []
    var imageBlocksActive: [UIImage] = []
    var statusImage: UIImage?

    var isDeviceActivated: Bool? = false
    lazy var cardStatus = DataProvider.newRealm().getCardStatus(ofType: CardTypeRef.AppleWatch)
    
    var setHeaderButtonInfo: ((_ token: String) -> Void)?
    var headerIdentifier: String?
    var setLinkButtonInfo: ((_ token: String) -> Void)?
    var linkIdentifier: String?
    weak var delegate: AWCLandingViewModelDelegate?
    let currentDate = Date()
    let activeStatus = "ACTIVE"
    let pendingStatus = "PENDING"
    
    let coreRealm = DataProvider.newRealm()
    let realm = DataProvider.newGDCRealm()
    
    
    // MARK: VIAHealthKit - Link Apple Health App
    
    init() {
        NotificationCenter.default.addObserver(self, selector: #selector(setDeviceLinkStatus), name: .ShouldDisplayHealthKitOnboardingViewController, object: nil)
        coreRealm.refresh()
        let partyId = "\(coreRealm.getPartyId())"
        VITHealthKitHelper.shared().setUserEntityNumber(partyId)
        isDeviceActivated = isDeviceLinked()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: .ShouldDisplayHealthKitOnboardingViewController, object: nil)
    }
    
    func performLink() {
        VITHealthKitHelper.shared().request(.fitness) { [weak self] granted, error in
            self?.didFinishHealthKitLinking(granted: granted, error: error)
        }
    }
    
    func didFinishHealthKitLinking(granted: Bool, error: Error?) {
        if !AppSettings.hasLinkedAppleHealthDevice(){
            setHeaderButtonInfo!("showActivatedScreen")
            isDeviceActivated = isDeviceLinked()
        }
    }
    
    @objc func setDeviceLinkStatus() {
        isDeviceActivated = isDeviceLinked()
    }
    
    func isDeviceLinked() -> Bool {
        switch  VITHealthKitHelper.shared().status {
        case .available:
            return true
        default:
            return false
        }
    }
    
    func getAppleWatchUrl() -> URL? {
        guard let vmpURL = VIAApplicableFeatures.default.getVMPURL() else { return URL(string: "")! }
        return vmpURL
    }
    
    func getDeviceBenefits() -> GDCBenefit? {
        
        let benefitData = self.realm.allGDCBenefit()
        var benefits: GDCBenefit?
        for data in benefitData{
            benefits = data
        }
        return benefits
    }
    
    func getBenefitMonth() -> (String, String) {
        let deviceBenefit = getDeviceBenefits() ?? nil
        var terms = ""
        if let deviceBenefit = deviceBenefit {
            for productAttribute in (deviceBenefit.productAttributes) {
                if productAttribute.typeCode == "BenefitTermMonths" {
                    terms = productAttribute.value ?? ""
                }
            }
        }
        
        return ("\(deviceBenefit?.currentBenefitPeriod ?? 0)", terms)
    }
    
    func getDeviceBenefitGoalValues() -> GDCLevels? {
        
        let goalData = realm.allGDCLevels().filter { $0.isHighestLevelAchieved == true }
        var levels: GDCLevels?
        for data in goalData{
            levels = data
        }
        return levels
    }
    
    func getGoalTrackerValues() -> String? {
        return self.dateFormatter(date: realm.allGDCGoalTracker().first?.effectiveFrom)
    }
    
    func getGoalTrackerDate() -> String? {
        return realm.allGDCGoalTracker().first?.effectiveFrom
    }
    
    // MARK: HeaderViews for Landing page
    
    func setHeaderInfoView(tableView: UITableView) -> UIView {
        if showActivedLanding(){
            return headerInfoView(tableView: tableView)!
        }
        return UIView()
    }
    
    func showActivedLanding() -> Bool{
        if let statusTypeCode = self.realm.allGDCState().first?.statusTypeCode, statusTypeCode == activeStatus {
            if let effectiveFrom = self.realm.allGDCGoalTracker().first?.effectiveFrom {
                if let effectiveFromValue = DateFormatter.yearMonthDayFormatter().date(from: effectiveFrom),
                    effectiveFromValue > currentDate{
                    isDeviceActivated = true
                    return true
                }
            }
        }
        return false
    }
    
    func showPointsHeader() -> Bool{
        if let statusTypeCode = self.realm.allGDCState().first?.statusTypeCode, statusTypeCode == activeStatus {
            if let effectiveFrom = self.realm.allGDCGoalTracker().first?.effectiveFrom {
                if let effectiveFromValue = DateFormatter.yearMonthDayFormatter().date(from: effectiveFrom),
                    effectiveFromValue <= currentDate{
                    userHasPoints = true
                    return true
                }
            }
        }
        return false
    }
    
    func setHeaderView(tableView: UITableView) -> UIView {
        
        let viewDetails = AWCCardStatus.self
        
        if let benefitId = self.realm.allGDCBenefit().first?.benefitId {
            if let deliveryDate = self.realm.allGDCPurchase().first?.deliveryDate {
                if let dateDateValue = DateFormatter.yearMonthDayFormatter().date(from: deliveryDate),
                    currentDate <= dateDateValue {
                    
                    return headerView(tableView: tableView, details: viewDetails.awaitingShipment)!
                    
                }else if let effectiveFrom = self.realm.allGDCState().first?.effectiveFrom, let effectiveTo = self.realm.allGDCState().first?.effectiveTo {
                    
                    let effectiveFromValue = DateFormatter.yearMonthDayFormatter().date(from: effectiveFrom)
                    let effectiveToValue = DateFormatter.yearMonthDayFormatter().date(from: effectiveTo)
                    
                    if currentDate >= effectiveFromValue! && currentDate <= effectiveToValue!{
                        
                        guard let statusTypeCode = self.realm.allGDCState().first?.statusTypeCode else {
                            return headerView(tableView: tableView, details: viewDetails.getAppleWatch)!
                        }
                        
                        if statusTypeCode == pendingStatus {
                            if let linkedValue = self.realm.allGDCQualifyingDeviceses().first?.linked {
                                /* Handler to show SyncPending screen after linking */
                                return linkedValue || isDeviceActivated! ? headerView(tableView: tableView, details: viewDetails.deviceSyncPending)! : headerView(tableView: tableView, details: viewDetails.startEarningVitalityCoins)!
                            }
                        } else if statusTypeCode == activeStatus {
                            if let effectiveFrom = self.realm.allGDCGoalTracker().first?.effectiveFrom {
                                if let effectiveFromValue = DateFormatter.yearMonthDayFormatter().date(from: effectiveFrom),
                                    effectiveFromValue > currentDate {
                                    isDeviceActivated = true
                                    return headerView(tableView: tableView, details: viewDetails.activated)!
                                }
                            }
                        }
                        
                    }
                }
            }
        }
        
        return headerView(tableView: tableView, details: viewDetails.getAppleWatch)!
    }
    
    func headerInfoView(tableView: UITableView) -> UIView? {
        let view = tableView.dequeueReusableCell(withIdentifier: AWCHeaderInfoCell.defaultReuseIdentifier)
        let headerView = view as! AWCHeaderInfoCell
        
        // TO DO: Tokens to be created
        headerView.infoText = CommonStrings.Awc.Landing.MonthlyTargetReminder2022
        headerView.infoButtonText = CommonStrings.Dc.Landing.MonthlyTargetHelpButtonTitle1553
        headerView.infoButton.isHidden = false
        headerView.headerIcon.image = VIAAppleWatchCashbackAsset.Landing.manRunningSmall.image
        
        return view
    }
    
    func headerView(tableView: UITableView, details: AWCCardStatus) -> UIView? {
        let view = tableView.dequeueReusableCell(withIdentifier: AWCHeaderTableViewCell.defaultReuseIdentifier)
        let headerView = view as! AWCHeaderTableViewCell
        
        headerView.headerTitle = details.headerTitle()
        headerView.headerDescription = details.headerDescription()
        headerView.headerUIImageView.image = details.headerBanner()
        headerView.fullWidthHeaderUIImageView.image = details.fullWidthHeaderBanner()
        
        if let title = details.headerButtonTitle(), !title.isEmpty {
            headerView.headerButton.isHidden = false
            headerView.headerButtonTitle = title
        } else {
            headerView.headerButton.isHidden = true
        }
        
        headerIdentifier = details.headerIdentifier()
        headerView.headerButton.addTarget(self, action: #selector(headerButtonAction), for: .touchUpInside)
        
        return view
    }
    
    @objc func headerButtonAction() {
        if let token = headerIdentifier, let setHeaderButtonInfo = setHeaderButtonInfo {
            setHeaderButtonInfo(token)
        }
    }
    
    func tierColor(tier: Int) -> UIColor {
        switch tier {
        case 0:
            return UIColor(red: 212 / 255, green: 7 / 255, blue: 110 / 255, alpha: 1)
        case 1:
            return UIColor(red: 249 / 255, green: 149 / 255, blue: 51 / 255, alpha: 1)
        case 2:
            return UIColor(red: 23 / 255, green: 170 / 255, blue: 214 / 255, alpha: 1)
        case 3:
            return UIColor(red: 133 / 255, green: 209 / 255, blue: 23 / 255, alpha: 1)
        default:
            break
        }
        return UIColor(red: 0 / 255, green: 0 / 255, blue: 0 / 255, alpha: 1)
    }
    
    func dateFormatter (date: String?) -> String {
        let dateResponseFormatter = DateFormatter()
        dateResponseFormatter.dateFormat = "yyyy-MM-dd"
        let dateStringFormatter = DateFormatter()
        dateStringFormatter.dateFormat = "MMMM"
        
        guard let dateValue = date else {
            return ""
        }
        let monthDate: Date? = dateResponseFormatter.date(from: (dateValue))
        
        return dateStringFormatter .string(from: monthDate!)
    }
}

extension AWCLandingViewModel {
    
    enum AWCCardStatus {
        
        case getAppleWatch
        case awaitingShipment
        case startEarningVitalityCoins
        case deviceSyncPending
        case activated
        
        func headerTitle() -> String? {
            switch self{
            case .getAppleWatch:
                return CommonStrings.Awc.Landing.HeaderTitle1997
            case .awaitingShipment:
                // TO DO: Tokens to be created
                return self.dateOfShipment()
            case .startEarningVitalityCoins:
                return CommonStrings.Awc.HomeCard.EarningCoins.Title2017
            case .activated:
                // TO DO: Tokens to be created
                guard let date = monthlyTarget() else { return "" }
                return CommonStrings.Dc.Landing.MonthlyTargetTitle1525(" \(date)")
            case .deviceSyncPending:
                return CommonStrings.Dc.Landing.ActivitySyncPendingTitle1521
            }
        }
        
        func headerDescription() -> String? {
            switch self{
            case .getAppleWatch:
                return CommonStrings.Awc.Landing.HeaderTitle1998
            case .awaitingShipment:
                // TO DO: Tokens to be created
                return CommonStrings.Awc.Landing.ShippedDeviceDescription2005
            case .startEarningVitalityCoins:
                return CommonStrings.Awc.Landing.EarningCoins.Description2019
            case .activated:
                // TO DO: Tokens to be created
                return CommonStrings.Dc.Landing.MonthlyTargetDescription2023
            case .deviceSyncPending:
                return CommonStrings.Dc.Landing.ActivitySyncPendingDescription1522("Apple Watch")
            }
        }
        
        func headerButtonTitle() -> String? {
            switch self{
            case .getAppleWatch:
                return CommonStrings.Awc.HomeCard.Message1992
            case .awaitingShipment:
                return ""
            case .startEarningVitalityCoins:
                return CommonStrings.Awc.HomeCard.EarningCoins.Footer2018
            case .activated:
                // TO DO: Tokens to be created
                return CommonStrings.Awc.Landing.ActionMenuTitle2000
            default: break
            }
            return ""
        }
        
        func headerIdentifier() -> String? {
            switch self{
            case .getAppleWatch:
                return "showVMP"
            case .awaitingShipment:
                return ""
            case .startEarningVitalityCoins:
                return "showLinkAppleHealth"
            case .activated:
                return "showHowToTrack"
            default: break
            }
            return ""
        }
        
        func headerBanner() -> UIImage? {
            switch self{
            case .awaitingShipment:
                return VIAAppleWatchCashbackAsset.Landing.illustrationDelivery.image
            case .activated:
                return VIAAppleWatchCashbackAsset.Landing.calendar.image
            case .deviceSyncPending:
                return VIAAppleWatchCashbackAsset.Landing.illustrationRunning.image
            default: break
            }
            return nil
        }
        
        func fullWidthHeaderBanner() -> UIImage? {
            switch self{
            case .getAppleWatch:
                return VIAAppleWatchCashbackAsset.Landing.cashbackCoinGraphic.image
            case .startEarningVitalityCoins:
                return VIAAppleWatchCashbackAsset.Landing.cashbackCoinGraphic.image
            default: break
            }
            return nil
        }
        
        func dateOfShipment() -> String {
            let realm = DataProvider.newGDCRealm()
            
            guard let date = realm.allGDCPurchase().first?.deliveryDate else {
                return ""
            }
            
            let dateFromResponse = Localization.yearMonthDayFormatter.date(from: date)
            let deliveryDate = Localization.dayDateShortMonthyearFormatter.string(from: dateFromResponse!)
            return CommonStrings.Awc.Landing.ShippedDeviceTitle2004(deliveryDate)
        }
        
        func monthlyTarget() -> String? {
            let realm = DataProvider.newGDCRealm()
            
            guard let date = realm.allGDCGoalTracker().first?.effectiveFrom else {
                return ""
            }
            
            let dateFromResponse = Localization.yearMonthDayFormatter.date(from: date)
            let monthlyTarget = Localization.dayDateMonthYearFormatter.string(from: dateFromResponse!)
            return monthlyTarget
        }
        
    }
}

extension AWCLandingViewModel{
    
    public func loadData(onStart: (()->Void)?, completion: ((_ error:Error?)->Void)?){
        let realm       = DataProvider.newRealm()
        let tenantID    = realm.getTenantId()
        let partyID     = realm.getPartyId()
        
        //TODO:
        let productFeatureKey = getCardMetaData(realm: realm) ?? "223"
        let getDeviceBenefit = GetDeviceBenefit(productFeatureKey: Int(productFeatureKey)!, swap: false, partyId: partyID)
        let getDeviceBenefitRequest = GetDeviceBenefitParameters(getDeviceBenefit)
        
        /* Trigger on start to kickoff the progressView */
        onStart?()
        
        Wire.Events.getDeviceBenefit(tenantId: tenantID, request: getDeviceBenefitRequest) { (error) in
            completion?(error)
        }
    }
    
    private func getCardMetaData(realm: Realm) -> String?{
        /* Get Device Cashback Home Card Details */
        let deviceCashbackCard = realm.homeCard(by: CardTypeRef.AppleWatch)
        
        /* Get "productFeatureKey" value */
        if let cardMetaDatas = deviceCashbackCard?.cardMetadatas {
            for cardMetaData in cardMetaDatas {
                if cardMetaData.type.rawValue == CardMetadataTypeRef.CardFeatureDeviceCashback.rawValue {
                    return cardMetaData.value
                }
            }
        }
        return nil
    }
    
    /* API to Link Apple Health */
    public func linkAppleHealthProcessEvents(onStart: (()->Void)?, completion: ((_ error:Error?)-> Void)?) {
        let coreRealm = DataProvider.newRealm()
        let tenantID = coreRealm.getTenantId()
        let partyID = coreRealm.getPartyId()
        let events: [EventTypeRef] = [EventTypeRef.DeviceLinking]
        let eventMetaDataValues: Array<String>? = ["Apple"]
    
        var processEvents = [ProcessEventsEvent]()
        for event in events {
            let processEvent = ProcessEventsEvent(date: Date(),
                                                  eventTypeKey: event,
                                                  eventSourceTypeKey: .MobileApp,
                                                  partyId: partyID,
                                                  reportedBy: partyID,
                                                  associatedEvents: [ProcessEventsAssociatedEvent](),
                                                  eventMetaDataTypeKey: EventMetaDataTypeRef(rawValue: EventMetaDataTypeRef.Manufacturer.rawValue),
                                                  eventMetaDataUnitOfMeasure: nil,
                                                  eventMetaDataValues: eventMetaDataValues)
            processEvents.append(processEvent)
        }
        
        Wire.Events.processEvents(tenantId: tenantID, events: processEvents) { [weak self] (response, error) in
            completion!(error)
        }
    }
}
