// Generated using SwiftGen, by O.Halligon — https://github.com/SwiftGen/SwiftGen

#if os(OSX)
import AppKit.NSImage
public typealias VIANuffieldServicesColor = NSColor
public typealias VIANuffieldServicesImage = NSImage
#elseif os(iOS) || os(tvOS) || os(watchOS)
import UIKit.UIImage
public typealias VIANuffieldServicesColor = UIColor
public typealias VIANuffieldServicesImage = UIImage
#endif

// swiftlint:disable file_length

public typealias VIANuffieldServicesAssetType = VIANuffieldServicesImageAsset

public struct VIANuffieldServicesImageAsset: Equatable {
	fileprivate var name: String

	public var templateImage: VIANuffieldServicesImage {
		let bundle = Bundle(for: BundleToken.self)
		#if os(iOS) || os(tvOS)
		let image = VIANuffieldServicesImage(named: name, in: bundle, compatibleWith: nil)?.withRenderingMode(.alwaysTemplate)
		#elseif os(OSX)
		let image = bundle.image(forResource: name).withRenderingMode(.alwaysTemplate)
		#elseif os(watchOS)
		let image = VIANuffieldServicesImage(named: name).withRenderingMode(.alwaysTemplate)
		#endif
		guard let result = image else { fatalError("Unable to load template image named \(name).") }
		return result
	}

	public var image: VIANuffieldServicesImage {
		let bundle = Bundle(for: BundleToken.self)
		#if os(iOS) || os(tvOS)
		let image = VIANuffieldServicesImage(named: name, in: bundle, compatibleWith: nil)
		#elseif os(OSX)
		let image = bundle.image(forResource: name)
		#elseif os(watchOS)
		let image = VIANuffieldServicesImage(named: name)
		#endif
		guard let result = image else { fatalError("Unable to load image named \(name).") }
		return result
	}

	public static func ==(lhs: VIANuffieldServicesImageAsset, rhs: VIANuffieldServicesImageAsset) -> Bool {
        return lhs.image == rhs.image
    }
}

public struct VIANuffieldServicesColorAsset {
fileprivate var name: String

#if swift(>=3.2)
@available(iOS 11.0, tvOS 11.0, watchOS 4.0, OSX 10.13, *)
var color: VIANuffieldServicesColor {
return VIANuffieldServicesColor(asset: self)
}
#endif
}

// swiftlint:disable identifier_name line_length nesting type_body_length type_name
public enum VIANuffieldServicesAsset {
  public static let partnersPlaceholder = VIANuffieldServicesImageAsset(name: "partnersPlaceholder")
  public static let nuffieldPlaceholder = VIANuffieldServicesImageAsset(name: "nuffieldPlaceholder")

  // swiftlint:disable trailing_comma
  public static let allColors: [VIANuffieldServicesColorAsset] = [
  ]
  public static let allImages: [VIANuffieldServicesImageAsset] = [
    partnersPlaceholder,
    nuffieldPlaceholder,
  ]
  // swiftlint:enable trailing_comma
  @available(*, deprecated, renamed: "allImages")
  public static let allValues: [VIANuffieldServicesAssetType] = allImages
}
// swiftlint:enable identifier_name line_length nesting type_body_length type_name

extension VIANuffieldServicesImage {
@available(iOS 1.0, tvOS 1.0, watchOS 1.0, *)
@available(OSX, deprecated,
message: "This initializer is unsafe on macOS, please use the VIANuffieldServicesImageAsset.image property")
convenience init!(asset: VIANuffieldServicesAssetType) {
#if os(iOS) || os(tvOS)
let bundle = Bundle(for: BundleToken.self)
self.init(named: asset.name, in: bundle, compatibleWith: nil)
#elseif os(OSX) || os(watchOS)
self.init(named: asset.name)
#endif
}
}

extension VIANuffieldServicesColor {
#if swift(>=3.2)
@available(iOS 11.0, tvOS 11.0, watchOS 4.0, OSX 10.13, *)
convenience init!(asset: VIANuffieldServicesColorAsset) {
let bundle = Bundle(for: BundleToken.self)
#if os(iOS) || os(tvOS)
self.init(named: asset.name, in: bundle, compatibleWith: nil)
#elseif os(OSX)
self.init(named: asset.name, bundle: bundle)
#elseif os(watchOS)
self.init(named: asset.name)
#endif
}
#endif
}

private final class BundleToken {}
