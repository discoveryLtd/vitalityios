//
//  NuffieldDetailsViewController.swift
//  VitalityActive
//
//  Created by Harry C. Lingad on 5/16/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import UIKit
import VIAUIKit
import VitalityKit
import SafariServices
import VIAPartners

// TODO: Remove after IGI Partner server content works.
import VIACommon

public class NuffieldDetailsViewController: VIATableViewController, PartnersDetailTableViewCellDelegate, SafariViewControllerPresenter, Tintable, UIWebViewDelegate, SFSafariViewControllerDelegate {
    
    public var viewModel: NuffieldDetailsViewModel?
    public var sourceFeatureType: FeatureType?
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        setGlobalTintColor()
        setupTableView()
        title = viewModel?.partner.name
    }
    
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        hideBackButtonTitle()
        if viewModel?.partner.learnMoreUrl == nil && viewModel?.partner.webViewContent == nil {
            showFullScreenHUD()
        }
    }
    
    public override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        // TODO: Remove this ugliness after IGI partner info is implemented server side.
        if viewModel?.partner.name == "foodpanda" || viewModel?.partner.name == "EasyTickets" { return }
        
        if viewModel?.partner.logoFileName == "none" { return }
        
        retrievePartnerDetails()
    }
    
    func setupTableView() {
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: UITableViewCell.defaultReuseIdentifier)
        tableView.register(PartnersDetailTableViewCell.self, forCellReuseIdentifier: PartnersDetailTableViewCell.defaultReuseIdentifier)
        tableView.tableFooterView = UIView()
        tableView.separatorInset = .zero
    }
    
    public func setGlobalTintColor() {
        guard let sourceFeature = sourceFeatureType else {
            return
        }
        switch sourceFeature {
        case .knowYourHealth:
            VIAAppearance.setGlobalTintColorToKnowYourHealth()
        case .improveYourHealth:
            VIAAppearance.setGlobalTintColorToImproveYourHealth()
        case .getRewarded:
            VIAAppearance.setGlobalTintColorToGetRewarded()
        }
    }
    
    public func retrievePartnerDetails() {
        viewModel?.requestEligibilityContent(completion: { [weak self] (error) in
            
            guard error == nil else {
                self?.handleError(error)
                return
            }
            
            self?.tableView.reloadData()
            self?.removeStatusView()
        })
    }
    
    func handleError(_ error: Error?) {
        hideFullScreenHUD()
        let statusView = handleBackendErrorWithStatusView((error as? BackendError) ?? .other, tryAgainAction: { [weak self] in
            self?.showFullScreenHUD()
            self?.retrievePartnerDetails()
        })
        configureStatusView(statusView)
    }
    
    // MARK: - Table view data source
    
    public override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    public override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (VIAApplicableFeatures.default.showPartnerGetStartedLink)!{
            return 2
        }else{
            return 1
        }
    }
    
    public override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: PartnersDetailTableViewCell.defaultReuseIdentifier) as? PartnersDetailTableViewCell else { return UITableViewCell() }
            cell.delegate = self
            
            // TODO: Remove this ugliness after IGI partner info is implemented server side.
            if viewModel?.partner.name == "foodpanda" {
                cell.content = RewardPartnerWebContentProvider.foodpandaHtml
            } else if viewModel?.partner.name == "EasyTickets" {
                cell.content = RewardPartnerWebContentProvider.easyticketsHtml
            } else if viewModel?.partner.logoFileName == "none" {
                cell.content = "\r\n<div class=main-container>\r\n<div class=content-container>\r\n\r\n\t\t\t<p>\(String(describing: (viewModel?.partner.description)!))</p>\r\n</div></div>"
            }
                
            else {
                cell.content = viewModel?.partner.webViewContent
            }
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: UITableViewCell.defaultReuseIdentifier, for: indexPath)
            
            cell.imageView?.image = VIAApplicableFeatures.default.partnerDetailViewControllerActionImage
            cell.imageView?.tintColor = sourceFeatureType?.color
            cell.textLabel?.text = VIAApplicableFeatures.default.getPartnerDetailViewControllerActionTitle()
            cell.accessoryType = .disclosureIndicator
            
            return cell
        }
    }
    
    public override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    
    public override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    public override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 1 {
            tableView.deselectRow(at: indexPath, animated: true)
            removeStatusView()
            
            self.performSegue(withIdentifier: "showLearnMore", sender: nil)
        }
        
    }
    
    public override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return .leastNonzeroMagnitude
    }
    
    public override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return .leastNonzeroMagnitude
    }
    
    public override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showPartnerTermsAndConditions" {
            if let termsAndConditionsVC = segue.destination as? PartnerTermsAndConditionsViewController {
                termsAndConditionsVC.articleId = sender as? String
                let termsViewModel = PartnerTermsAndConditionsViewModel(articleId: termsAndConditionsVC.articleId)
                termsAndConditionsVC.viewModel = termsViewModel
            }
        }else if segue.identifier == "showLearnMore"{
            guard let learnMoreURL = self.viewModel?.partner.learnMoreUrl else { return }
            if let vc = segue.destination as? PartnersWKWebViewController {
                vc.url = learnMoreURL
            }
        }
    }
    
    // MARK: - Detail TVC Delegate
    public func webViewHeightChanged() {
        tableView.reloadData()
        hideFullScreenHUD()
    }
    
    public func userTappedPhoneNumber(number: String) {
        if let phoneURL = URL(string: "tel://\(number)") {
            let application:UIApplication = UIApplication.shared
            if (application.canOpenURL(phoneURL)) {
                application.open(phoneURL, options: [:], completionHandler: nil)
            }
        }
    }
    
    public func showGenericError() {
        
        let controller = UIAlertController(title: CommonStrings.GenericServiceErrorTitle268,
                                           message: CommonStrings.GenericServiceErrorMessage269,
                                           preferredStyle: .alert)
        let ok = UIAlertAction(title: CommonStrings.OkButtonTitle40, style: .default) { action in }
        controller.addAction(ok)
        present(controller, animated: true, completion: nil)
    }
    
    public func userTappedWebContent(link: URL) {
        presentModally(url: link, parentViewController: self, delegate: self)
    }
    
    public func userTappedTermsAndConditions(sender: String) {
        self.performSegue(withIdentifier: "showPartnerTermsAndConditions", sender: sender)
    }
}

