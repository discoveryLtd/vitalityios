//
//  NuffieldLandingViewModel.swift
//  VitalityActive
//
//  Created by Harry C. Lingad on 5/15/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import UIKit

import VitalityKit
import VIAUtilities

import RealmSwift

public class NuffieldLandingViewModel: CMSConsumer {
    
    public init(withPartnerType partnerCategoryType: ProductFeatureCategoryRef) {
        self.partnerCategoryType = partnerCategoryType
        title = CommonStrings.MyHealth.CardNuffieldTitle2077
    }
    
    //MARK: Properties
    
    public struct NuffieldPartnerData {
        public var name: String
        public var longDescription: String
        public var description: String
        public var logoFileName: String
        public var typeKey: Int
        public var learnMoreUrl: URL?
        public var webViewContent: String?
    }
    
    public struct NuffieldGroupData {
        public var name: String?
        public var partners: [NuffieldPartnerData]
    }
    
    let coreRealm = DataProvider.newPartnerRealm()
    
    public var partnerCategoryType: ProductFeatureCategoryRef
    public var partnerGroupsData = [NuffieldGroupData]()
    
    public var title: String?
    //MARK: Configuration
    
    func configurePartnerData() {
        coreRealm.refresh()
        
        guard let partnerCategory = coreRealm.partnerCategory(with: partnerCategoryType.rawValue) else {
            return
        }
        var groupsData = [NuffieldGroupData]()
        let groups = partnerCategory.partnerGroups
        for group in groups {
            var groupData = NuffieldGroupData(name: group.name, partners:[NuffieldPartnerData]())
            for partner in group.partners {
                let partnerData = NuffieldPartnerData(name: partner.name, longDescription: partner.longDescription, description: partner.shortDescription, logoFileName: partner.logoFileName, typeKey: partner.typeKey, learnMoreUrl: nil, webViewContent: nil)
                groupData.partners.append(partnerData)
            }
            groupsData.append(groupData)
        }
        partnerGroupsData = groupsData
    }
    
    //MARK: Partner data functions
    
    public var headerContent: String? {
        return CommonStrings.Partners.LandingHeader607
    }
    
    public func partnerCount(in section: Int) -> Int? {
        return partnerGroupsData[section].partners.count
    }
    
    public func groupCount() -> Int? {
        return partnerGroupsData.count
    }
    
    public func partnerDetails(at indexPath: IndexPath) -> NuffieldPartnerData? {
        return partnerGroupsData[indexPath.section].partners[indexPath.row]
    }
    
    public func sectionTitle(for section: Int) -> String? {
        return partnerGroupsData[section].name
    }
    
    //MARK: Networking
    
    public func requestPartnersByCategory(completion: @escaping (Error?) -> Void) {
        let requestParameters = configureGetPartnersByCategoryRequestParameters()
        let tenantId = DataProvider.newRealm().getTenantId()
        let partyId = DataProvider.newRealm().getPartyId()
        Wire.Member.getPartnersByCategory(tenantId: tenantId, partyId: partyId, request: requestParameters) { error, result in
            guard error == nil else {
                completion(error)
                return
            }
            completion(nil)
        }
    }
    
    public func configureGetPartnersByCategoryRequestParameters() -> GetPartnersByCategoryParameters {
        let effectiveDate = Date()
        let productFeatureCategoryTypeKey = partnerCategoryType.rawValue
        let requestParameters = GetPartnersByCategoryParameters(effectiveDate: effectiveDate, productFeatureCategoryTypeKey: productFeatureCategoryTypeKey)
        
        return requestParameters
    }
    
    public func partnerLogo(partner: NuffieldPartnerData, completion: @escaping ((_: UIImage, _: NuffieldPartnerData) -> Void)) {
        //Case where logo already exists on disk
        let documentsURL = FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask).first!
        let cachedLogoURL = documentsURL.appendingPathComponent(partner.logoFileName)
        if let cachedLogo = UIImage(contentsOfFile: cachedLogoURL.path) {
            debugPrint("Partner image retrieved successfully from disk")
            return completion(cachedLogo, partner)
        }
        
        //Otherwise dowload logo from liferay
        self.downloadCMSImageToCache(file: partner.logoFileName) { url, error in
            let defaultLogo = VIANuffieldServicesAsset.partnersPlaceholder.image
            let nuffieldPartnerFallbackLogo = VIANuffieldServicesAsset.nuffieldPlaceholder.image
            guard url != nil, error == nil, let filePath = url?.path, FileManager.default.fileExists(atPath: filePath) else {
                debugPrint("Could not retrieve downloaded partner image from disk")
                if AppSettings.getAppTenant() == .UKE && self.partnerCategoryType.rawValue == ProductFeatureCategoryRef.NuffieldHealthServices.rawValue{
                    return completion(nuffieldPartnerFallbackLogo, partner)
                }
                return completion(defaultLogo, partner)
            }

            if let logo = UIImage(contentsOfFile: filePath) {
                debugPrint("Partner image retrieved successfully from service")
                return completion(logo, partner)
            }

            if AppSettings.getAppTenant() == .UKE && self.partnerCategoryType.rawValue == ProductFeatureCategoryRef.NuffieldHealthServices.rawValue{
                return completion(nuffieldPartnerFallbackLogo, partner)
            }
            debugPrint("Falied to download partner image")
            return completion(defaultLogo, partner)
        }
    }
    
}

