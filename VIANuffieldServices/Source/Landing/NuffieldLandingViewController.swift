//
//  NuffieldTableViewController.swift
//  VitalityActive
//
//  Created by Harry C. Lingad on 5/15/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import UIKit

import VIAUIKit
import VIACommon
import VitalityKit
import VIAUtilities
import VIAActiveRewards
import VIAPartners

public class NuffieldLandingViewController: VIATableViewController, Tintable {
    
    var viewModel: NuffieldLandingViewModel?
    var sourceFeatureType: FeatureType?
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        
        if viewModel == nil {
            setupModel()
        }
        
        title = viewModel?.title
        configureAppearance()
        setupTableView()
        
        loadPartners()
    }
    
    // MARK: Configuration
    
    func setupTableView() {
        tableView.register(PartnersListHeaderView.nib(), forHeaderFooterViewReuseIdentifier: PartnersListHeaderView.defaultReuseIdentifier)
        tableView.register(VIATableViewSectionHeaderFooterView.nib(), forHeaderFooterViewReuseIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier)
        tableView.register(PartnersListPartnerCell.nib(), forCellReuseIdentifier: PartnersListPartnerCell.defaultReuseIdentifier)
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 70
        tableView.sectionHeaderHeight = UITableViewAutomaticDimension
        tableView.estimatedSectionHeaderHeight = 100
        tableView.separatorInset = UIEdgeInsetsMake(0, 110, 0, 0)
        tableView.estimatedSectionFooterHeight = 30
        tableView.sectionFooterHeight = 30
    }
    
    public func setGlobalTintColor() {
        guard let sourceFeature = sourceFeatureType else {
            return
        }
        switch sourceFeature {
        case .knowYourHealth:
            VIAAppearance.setGlobalTintColorToKnowYourHealth()
        case .improveYourHealth:
            VIAAppearance.setGlobalTintColorToImproveYourHealth()
        case .getRewarded:
            VIAAppearance.setGlobalTintColorToGetRewarded()
        }
    }
    
    func configureAppearance() {
        navigationController?.makeNavigationBarTransparent()
        hideBackButtonTitle()
        setGlobalTintColor()
    }
    
    // MARK: - Table view data source
    
    public override func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel?.groupCount() ?? 1
        
    }
    
    public override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel?.partnerCount(in: 0) ?? 0
    }
    
    public override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let tableViewCell = self.tableView.dequeueReusableCell(withIdentifier: PartnersListPartnerCell.defaultReuseIdentifier, for: indexPath) as? PartnersListPartnerCell else { return UITableViewCell() }
        
        guard let partnerDetails = viewModel?.partnerDetails(at: indexPath) else { return UITableViewCell() }
        
        tableViewCell.title = partnerDetails.name
        tableViewCell.content = partnerDetails.description
        
        // TODO: Remove this once server can return IGI partner data.
        if partnerDetails.name == "EasyTickets" {
            tableViewCell.partnerImage = VIAActiveRewardsAsset.ActiveRewards.Rewards.IGIPartners.easyticketsSmall.image
        } else if partnerDetails.name == "foodpanda" {
            tableViewCell.partnerImage = VIAActiveRewardsAsset.ActiveRewards.Rewards.IGIPartners.foodpandaSmall.image
        } else if partnerDetails.logoFileName == "none" {
            tableViewCell.partnerImage = nil
        }
            
        else {
            DispatchQueue.global(qos: .background).async { [weak self] in
                self?.viewModel?.partnerLogo(partner: partnerDetails) { logo, partner in
                    DispatchQueue.main.async {
                        if tableViewCell.title == partner.name && tableViewCell.content == partner.description {
                            tableViewCell.partnerImage = logo
                        }
                    }
                }
            }
        }
        
        tableViewCell.accessoryType = .disclosureIndicator
        return tableViewCell
    }
    
    public override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "showPartnerDetails", sender: self)
    }
    
    public override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 0 {
            guard let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: PartnersListHeaderView.defaultReuseIdentifier) as? PartnersListHeaderView else {
                return nil
            }
            guard let headerContent = viewModel?.headerContent else {
                return nil
            }
            view.configurePartnersListHeader(contentText: headerContent, sectionTitleText: viewModel?.sectionTitle(for: section))
            return view
        } else {
            guard let view =  tableView.dequeueReusableHeaderFooterView(withIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier) as? VIATableViewSectionHeaderFooterView else {
                return nil
            }
            view.configureWithExtraSpacing(labelText: viewModel?.sectionTitle(for: section), extraTopSpacing: 20, extraBottomSpacing: 0)
            view.set(textColor: .night())
            view.set(font: .title2Font())
            
            return view
        }
    }
    
    // MARK: - Navigation
    public override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showPartnerDetails" {
            guard let indexPath = tableView.indexPathForSelectedRow else { return }
            guard let partnerDetails = viewModel?.partnerDetails(at: indexPath) else { return }
            guard let detailVC = segue.destination as? NuffieldDetailsViewController else { return }
            let detailViewModel = NuffieldDetailsViewModel(for: partnerDetails)
            detailVC.viewModel = detailViewModel
            detailVC.sourceFeatureType = sourceFeatureType
        }
    }
    
    //MARK: Networking
    
    public func loadPartners() {
        showFullScreenHUD()
        viewModel?.requestPartnersByCategory(completion: { [weak self] (error) in
            self?.hideFullScreenHUD()
            
            guard error == nil else {
                self?.handleError(error)
                return
            }
            self?.viewModel?.configurePartnerData()
            self?.tableView.reloadData()
        })
    }
    
    func handleError(_ error: Error?) {
        let statusView = handleBackendErrorWithStatusView((error as? BackendError) ?? .other, tryAgainAction: { [weak self] in
            self?.loadPartners()
        })
        configureStatusView(statusView)
    }
    
    //MARK: Private
    
    func setupModel() {
        // Currenlty naviely defaults to reward partners
        let type = ProductFeatureCategoryRef.NuffieldHealthServices
        self.viewModel = NuffieldLandingViewModel(withPartnerType: type)
        self.sourceFeatureType = .getRewarded
    }
}

