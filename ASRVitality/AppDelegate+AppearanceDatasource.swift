import VitalityKit
import VIACommon
import VIAUIKit
import VIACore
import UIKit

extension AppDelegate: AppearanceColorDataSource {
    class func asrColor() -> UIColor {
        return UIColor(red: 255.0 / 255.0, green: 185.0 / 255.0, blue: 0.0 / 255.0, alpha: 1.0) // hex - #FFB900
    }
    
    // MARK: AppearanceDataSource
    
    public var primaryColor: UIColor {
        return AppDelegate.asrColor()
    }
    
    public var tabBarBackgroundColor: UIColor {
        return .white
    }
    
    public var tabBarBarTintColor: UIColor {
        return .white
    }
    
    public var tabBarTintColor: UIColor? {
        return UIColor.primaryColor()
    }
    
    public var unselectedItemTintColor: UIColor? {
        return nil
    }
    
    public var getInsurerGlobalTint: UIColor? {
        // return VIAAppearance.default.primaryColorFromServer
        
        return VIAAppearance.default.dataSource?.primaryColor
    }
    
    public func getSplashScreenGradientTop() -> UIColor {
        // return UIColor(hexString: AppConfigFeature.insurerGradientColor1Hex())
        
        return UIColor.white
    }
    
    public func getSplashScreenGradientBottom() -> UIColor {
        // return UIColor(hexString: AppConfigFeature.insurerGradientColor2Hex())
        
        return UIColor.white
    }
}

extension AppDelegate: AppearanceIconographyDataSource {
    
    public func homeLogo(for locale: Locale) -> UIImage {
        return ASRVitalityAsset.homeLogo.image
    }
    
    public func loginLogo(for locale: Locale) -> UIImage {
        return ASRVitalityAsset.loginLogo.image
    }
    
    public func splashLogo(for locale: Locale = Locale.current) -> UIImage {
        return ASRVitalityAsset.splashLogo.image
    }
}
