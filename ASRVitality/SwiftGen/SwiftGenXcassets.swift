// Generated using SwiftGen, by O.Halligon — https://github.com/SwiftGen/SwiftGen

#if os(OSX)
import AppKit.NSImage
public typealias ASRVitalityColor = NSColor
public typealias ASRVitalityImage = NSImage
#elseif os(iOS) || os(tvOS) || os(watchOS)
import UIKit.UIImage
public typealias ASRVitalityColor = UIColor
public typealias ASRVitalityImage = UIImage
#endif

// swiftlint:disable file_length

public typealias ASRVitalityAssetType = ASRVitalityImageAsset

public struct ASRVitalityImageAsset: Equatable {
	fileprivate var name: String

	public var templateImage: ASRVitalityImage {
		let bundle = Bundle(for: BundleToken.self)
		#if os(iOS) || os(tvOS)
		let image = ASRVitalityImage(named: name, in: bundle, compatibleWith: nil)?.withRenderingMode(.alwaysTemplate)
		#elseif os(OSX)
		let image = bundle.image(forResource: name).withRenderingMode(.alwaysTemplate)
		#elseif os(watchOS)
		let image = ASRVitalityImage(named: name).withRenderingMode(.alwaysTemplate)
		#endif
		guard let result = image else { fatalError("Unable to load template image named \(name).") }
		return result
	}

	public var image: ASRVitalityImage {
		let bundle = Bundle(for: BundleToken.self)
		#if os(iOS) || os(tvOS)
		let image = ASRVitalityImage(named: name, in: bundle, compatibleWith: nil)
		#elseif os(OSX)
		let image = bundle.image(forResource: name)
		#elseif os(watchOS)
		let image = ASRVitalityImage(named: name)
		#endif
		guard let result = image else { fatalError("Unable to load image named \(name).") }
		return result
	}

	public static func ==(lhs: ASRVitalityImageAsset, rhs: ASRVitalityImageAsset) -> Bool {
        return lhs.image == rhs.image
    }
}

public struct ASRVitalityColorAsset {
fileprivate var name: String

#if swift(>=3.2)
@available(iOS 11.0, tvOS 11.0, watchOS 4.0, OSX 10.13, *)
var color: ASRVitalityColor {
return ASRVitalityColor(asset: self)
}
#endif
}

// swiftlint:disable identifier_name line_length nesting type_body_length type_name
public enum ASRVitalityAsset {
  public static let splashLogo = ASRVitalityImageAsset(name: "splashLogo")
  public enum Rewards {
    public static let cinemarkAppIcon = ASRVitalityImageAsset(name: "cinemarkAppIcon")
    public static let juanvaldezAppIcon = ASRVitalityImageAsset(name: "juanvaldezAppIcon")
    public static let juanvaldez = ASRVitalityImageAsset(name: "juanvaldez")
    public static let sampleQrcode = ASRVitalityImageAsset(name: "sample-qrcode")
    public static let cinemark = ASRVitalityImageAsset(name: "cinemark")
  }
  public static let membershipBanner = ASRVitalityImageAsset(name: "membershipBanner")
  public static let loginLogo = ASRVitalityImageAsset(name: "loginLogo")
  public static let homeLogo = ASRVitalityImageAsset(name: "homeLogo")

  // swiftlint:disable trailing_comma
  public static let allColors: [ASRVitalityColorAsset] = [
  ]
  public static let allImages: [ASRVitalityImageAsset] = [
    splashLogo,
    Rewards.cinemarkAppIcon,
    Rewards.juanvaldezAppIcon,
    Rewards.juanvaldez,
    Rewards.sampleQrcode,
    Rewards.cinemark,
    membershipBanner,
    loginLogo,
    homeLogo,
  ]
  // swiftlint:enable trailing_comma
  @available(*, deprecated, renamed: "allImages")
  public static let allValues: [ASRVitalityAssetType] = allImages
}
// swiftlint:enable identifier_name line_length nesting type_body_length type_name

extension ASRVitalityImage {
@available(iOS 1.0, tvOS 1.0, watchOS 1.0, *)
@available(OSX, deprecated,
message: "This initializer is unsafe on macOS, please use the ASRVitalityImageAsset.image property")
convenience init!(asset: ASRVitalityAssetType) {
#if os(iOS) || os(tvOS)
let bundle = Bundle(for: BundleToken.self)
self.init(named: asset.name, in: bundle, compatibleWith: nil)
#elseif os(OSX) || os(watchOS)
self.init(named: asset.name)
#endif
}
}

extension ASRVitalityColor {
#if swift(>=3.2)
@available(iOS 11.0, tvOS 11.0, watchOS 4.0, OSX 10.13, *)
convenience init!(asset: ASRVitalityColorAsset) {
let bundle = Bundle(for: BundleToken.self)
#if os(iOS) || os(tvOS)
self.init(named: asset.name, in: bundle, compatibleWith: nil)
#elseif os(OSX)
self.init(named: asset.name, bundle: bundle)
#elseif os(watchOS)
self.init(named: asset.name)
#endif
}
#endif
}

private final class BundleToken {}
