import Foundation
import VitalityKit

extension AppDelegate: WireDelegate {
    
    public var qaFrankfurtURL: URL {
        return URL(string: "")!
    }
    
//    public var devBaseURL: URL {
//        return URL(string: "https://dev.vitalityservicing.com/v1/api")!
//    }
    
    public var testBaseURL: URL {
        return URL(string: "https://test.vitalityservicing.com/v1/api")!
    }
    
    public var qaBaseURL: URL {
        return URL(string: "https://qa.vitalityactive.com/api")!
    }
    
    public var productionBaseURL: URL {
        //        return URL(string: "https://vitalityservicing.com/v1/api")!
        return URL(string: "https://vitality.aia.co.kr/api")!
    }
    
    public var performanceBaseURL: URL {
        return URL(string: "https://qa.uke.vitalityservicing.com/api")!
    }
    
    public var qaSouthKoreaBaseURL: URL{
        return URL(string: "https://www.uat.kr.vg-svc.com/api")!
    }
}

extension AppDelegate{
    
//    public var devAPIManagerIdentifier: String {
//        return "bkQ4cElmMmZndm5WN0kzcU9MTGVpY3BDaFhZYTpCX3djVzNIVDZ5UTA5emdKSDVOa2tOMmtsOTRh"
//    }
    
    public var testAPIManagerIdentifier: String {
        return "WDVlODVDQ3RQTVFpOWpRZ2p4TERSTkxXSjV3YTpQSVZieHkyanRiOFMwRDc1NHJMa0FXWjR2aTBh"
    }
    
    public var qaAPIManagerIdentifier: String {
        return "bnlSWG9nWFJfWjdCY2Z2YWtwa2pyNnVzSWtJYTp6RHZPQllWZzJvc0poZ1hLc0tGNW1YRmpMX3Nh"
    }
    
    public var productionAPIManagerIdentifier: String {
        /* SK PROD: UDB5MTc0cFJ6QVZCOF9sZjFVTGw5cEFwRkVrYTo5cjhNNlRYeDJBMlk1UTlkeEo5a2FuNDZKQzRh*/
        return "UDB5MTc0cFJ6QVZCOF9sZjFVTGw5cEFwRkVrYTo5cjhNNlRYeDJBMlk1UTlkeEo5a2FuNDZKQzRh"
    }
    
    public var qaFrankfurtAPIManagerIdentifier: String {
        return "0000000000000000000000000000000000000000000000000000000000000000000000000000"
    }
    
    public var performanceAPIManagerIdentifier: String {
        return "QWJCRUJxQkI5SWcxbExGVFdhZ2ZmemJqN2Z3YTpmWkhHQmZPbk15SW1FTU5zX2VUdDVqczFjVThh"
    }
    
    public var qaSouthKoreaAPIManagerIdentifier: String {
        return "THZGcXI4aV84YmxDYVcxVFJfWVdXMDQ2ZjV3YTpmMkRxZjEyaEx5OTFjNlJqa3dKbHhnaUdfZmth"
    }
}
