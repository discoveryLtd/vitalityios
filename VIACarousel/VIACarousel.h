//
//  VIACarousel.h
//  VIACarousel
//
//  Created by Joshua Ryan on 7/7/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for VIACarousel.
FOUNDATION_EXPORT double VIACarouselVersionNumber;

//! Project version string for VIACarousel.
FOUNDATION_EXPORT const unsigned char VIACarouselVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <VIACarousel/PublicHeader.h>

#import "iCarousel.h"
