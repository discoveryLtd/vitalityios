//
//  GetPartyByEmailRouter.swift
//  VitalityActive
//
//  Created by OJ Garde on 08/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import Alamofire
import VIAUtilities


enum GetPartyByEmailRouter: URLRequestConvertible {
    static let basePath = "vitality-manage-party-information-service-1"
    
    case getPartyByEmail(tenantId: Int, request: GetPartyByEmailParam)
    
    var method: HTTPMethod {
        switch self {
        case .getPartyByEmail:
            return .post
        }
    }
    
    var route: String {
        switch self {
        case .getPartyByEmail(let tenantId, _):
            return ("svc/\(tenantId)/getPartyByEmail")
        }
    }
    
    var parameters: Parameters? {
        switch self {
        case .getPartyByEmail(_, let params):
            return params.toJSON()
        }
    }
    
    func asURLRequest() throws -> URLRequest {
        let url = try Wire.urlForPath(path: GetPartyByEmailRouter.basePath).appendingPathComponent(self.route)
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = self.method.rawValue
        switch self {
        case .getPartyByEmail:
            return try JSONEncoding.default.encode(urlRequest, with: self.parameters)
        }
    }
}

extension Wire.Party {    
    public static func getPartyByEmail(tenantId: Int, request: GetPartyByEmailParam, completion: @escaping (_ error: Error?, _ data: Data?, _ result: GetPartyByEmailOutBoundPayload?) -> Void) {
        
        let url = GetPartyByEmailRouter.getPartyByEmail(tenantId: tenantId, request: request)
        Wire.sessionManager.request(url)
            .log()
            .validate(statusCode: 200 ..< 300)
            .apiManagerResponse { response in
                switch response.httpStatusCode {
                case 200 ..< 300:
                    guard let result = GetPartyByEmailOutBoundPayload(json: response.jsonDictionary) else {
                        return completion(WireError.parsing, response.data, nil)
                    }
                    completion(nil, response.data, result)
                default:
                    debugPrint("\(String(describing: response.result.error))")
                    completion(response.result.error ?? BackendError.other, response.data, nil)
                }
        }
    }
    
    public static func validateIfEmailIsAvailable(tenantId: Int, request: GetPartyByEmailParam,
                                                  completion: @escaping (_ available:Bool, _ error:String?) -> Void){
        let SOMETHING_WENT_WRONG = "Something went wrong while parsing json."
        let EMAIL_ALREADY_EXIST = "Email already exist."
        getPartyByEmail(tenantId: tenantId, request: request) { (error, data, payload) in
            if nil == payload{
                do{
                    if let data = data,
                        let json = try JSONSerialization.jsonObject(with: data) as? [String: Any],
                        let responseError = GetPartyByEmailErrorResponse(json: json)?.errors?.first,
                        responseError.code == 30163 || responseError.code == 30164{
                        debugPrint("code: \(responseError.code) <<<<<<<<<")
                        completion(true, nil)
                    }else{
                        completion(false, SOMETHING_WENT_WRONG)
                    }
                }catch{
                    debugPrint(SOMETHING_WENT_WRONG)
                    completion(false, SOMETHING_WENT_WRONG)
                }
            }else{
                debugPrint(EMAIL_ALREADY_EXIST)
                completion(false, EMAIL_ALREADY_EXIST)
            }
        }
    }
    
}

public struct GetPartyByEmailParam {
    var getPartyByEmailRequest:GetPartyByEmailRequest
    
    public init(roleTypeKey: Int, value:String) {
        self.getPartyByEmailRequest = GetPartyByEmailRequest(roleTypeKey: roleTypeKey, value: value)
    }
    
    public func toJSON() -> Parameters {
        let result = ["getPartyByEmailRequest" : getPartyByEmailRequest.toJSON()]
        debugPrint(result)
        return result
    }
    
    struct GetPartyByEmailRequest{
        public var roleTypeKey:Int
        public var value:String
        
        public init(roleTypeKey: Int, value:String) {
            self.roleTypeKey = roleTypeKey
            self.value = value
        }
        
        public func toJSON() -> Parameters {
            return ["roleTypeKey"   : roleTypeKey,
                    "value"         : value]
        }
    }
}

