//
//  MaintainPartyPreference.swift
//  VitalityKit
//
//  Created by wenilyn.a.teorica on 22/01/2019.
//  Copyright © 2019 Glucode. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireActivityLogger
import VIAUtilities

// MARK: Router

enum MaintainPartyPreferenceRouter: URLRequestConvertible {
    static let basePath = "vitality-manage-party-core-service-1"
    
    case maintainPartyPreference(tenantId: Int, partyId: Int, request: MaintainPartyPreferenceRequest)
    
    var method: HTTPMethod {
        switch self {
        case .maintainPartyPreference:
            return .post
        }
    }
    
    var route: String {
        switch self {
        case .maintainPartyPreference(let tenantId, let partyId, _):
            return ("svc/\(tenantId)/maintainPartyPreference/\(partyId)")
        }
    }
    
    var parameters: Parameters {
        switch self {
        case .maintainPartyPreference(_, _, let request):
            return request.toJSON()
        }
    }
    
    func asURLRequest() throws -> URLRequest {
        let url = try Wire.urlForPath(path: MaintainPartyPreferenceRouter.basePath).appendingPathComponent(self.route)
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = self.method.rawValue
        switch self {
        case .maintainPartyPreference(_, _, _):
            return try JSONEncoding.default.encode(urlRequest, with: self.parameters)
        }
    }
}

extension Wire.Content {
    public static func maintainPartyPreference(tenantId: Int, partyId: Int, request: MaintainPartyPreferenceRequest, completion: @escaping (_ error: Error?) -> Void) {
        let url = MaintainPartyPreferenceRouter.maintainPartyPreference(tenantId: tenantId, partyId: partyId, request: request)
        
        Wire.sessionManager.request(url)
            .log()
            .validate(statusCode: 200 ..< 300)
            .apiManagerResponse { response in
                switch response.httpStatusCode {
                case 200 ..< 300:
                    completion(nil)
                default:
                    completion(response.error ?? BackendError.other)
                }
        }
    }
}

public struct MaintainPartyPreferenceRequest {
    
    private var effectiveFrom: String
    private var effectiveTo: String
    private var typeKey: Int
    private var value: String
    
    public init(effectiveFrom: Date, value: String) {
        self.effectiveFrom = Wire.default.yearMonthDayFormatter.string(from: effectiveFrom)
        self.effectiveTo = "9999-12-31"
        self.typeKey = PreferenceTypeRef.ProfilePicRef.rawValue
        self.value = value
        
    }
    
    public func toJSON() -> Parameters {
        var preferencesArray = [Dictionary<String, Any>]()
        preferencesArray.append(["effectiveFrom": effectiveFrom, "effectiveTo": effectiveTo, "typeKey": typeKey, "value": value])
        
        return ["generalPreferences": preferencesArray]
    }
    
}

