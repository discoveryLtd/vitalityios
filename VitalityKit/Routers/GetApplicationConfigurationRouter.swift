import Foundation
import Alamofire

// MARK: Router

enum GetApplicationConfigurationRouter: URLRequestConvertible {

    case get(tenantId: Int)

    var basePath: String {
        switch self {
        case .get:
            return "vitality-application-configuration-service-1/"
        }
    }

    var method: HTTPMethod {
        switch self {
        case .get:
            return .get
        }
    }

    var route: String {
        switch self {
        case .get(let tenantId):
            return ("svc/\(tenantId)/getApplicationConfiguration")
        }
    }

    // MARK: URLRequestConvertible

    func asURLRequest() throws -> URLRequest {
        let url = try Wire.urlForPath(path: self.basePath).appendingPathComponent(self.route)
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = self.method.rawValue
        switch self {
        case .get:
            return try URLEncoding.default.encode(urlRequest, with: nil)
        }
    }
}

// MARK: Networking

extension Wire.Member {
    public static func getApplicationConfiguration(tenantId: Int, completion: @escaping ((_: WireError?) -> Void)) {
        // since AppConfig is the first service to be called once the app
        // resumes after a successful login, we have to set the auth token
        // to be able to access the rest of the API
        Wire.setAuthorisedAPIManagerToken(token: VitalityParty.accessTokenForCurrentParty())

        let url = GetApplicationConfigurationRouter.get(tenantId: tenantId)
        Wire.sessionManager.request(url)
            .log()
            .validate(statusCode: 200 ..< 300)
            .apiManagerResponse { response in
                switch response.httpStatusCode {
                case 200 ..< 300:

                    // app config is special, if the body is empty it is because
                    // we have the most current app config and nothing was returned
                    // for us. so first check if we have an empty body, and return
                    // successfully. if there is a body, we try to parse it.
                    if response.jsonDictionary.isEmpty {
                        return completion(nil)
                    }

                    // received new version of app config
                    guard let result = ApplicationConfigurationResponse(json: response.jsonDictionary) else {
                        return completion(WireError.parsing)
                    }

                    do {
                        try Parser.Login.parseVitalityAppConfig(result.application, completion: {
                            return completion(nil)
                        })
                    } catch _ {
                        return completion(WireError.parsing)
                    }
                default:
                    return completion(WireError.unknown)
                }
        }
    }

}
