//
//  GetSAVEventByPartyRouter.swift
//  VitalityActive
//
//  Created by Von Kervin R. Tuico on 14/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import Alamofire
import VIAUtilities

// MARK: Router
enum GetSAVEventByPartyRouter: URLRequestConvertible {
    
    case getEventByParty(tenantId: Int, request: GetSAVEventByPartyParameter)
    
    static let basePath = "vitality-manage-events-service-v1"
    
    var method: HTTPMethod {
        switch self {
        case .getEventByParty(_, _):
            return .post
        }
    }
    
    var route: String {
        switch self {
        case .getEventByParty(let tenantId, let params):
            return ("svc/\(tenantId)/getEventByParty/\(params.partyId)")
            
        }
    }
    
    var parameters: Parameters? {
        switch self {
        case .getEventByParty(_, let params):
            return params.toJSON()
        }
    }
    
    // MARK: URLRequestConvertible
    
    func asURLRequest() throws -> URLRequest {
        let url = try Wire.urlForPath(path: GetSAVEventByPartyRouter.basePath).appendingPathComponent(self.route)
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = self.method.rawValue
        
        switch self {
        case .getEventByParty:
            return try JSONEncoding.default.encode(urlRequest, with: self.parameters)
        }
    }
}

// MARK: Networking

extension Wire.Events {
    public static func getEventByParty(tenantId: Int, request: GetSAVEventByPartyParameter, completion: @escaping (( _: Error?, _: GetEventByPartyResponse?) -> Void)) {
        Wire.sessionManager.request(GetSAVEventByPartyRouter.getEventByParty(tenantId: tenantId, request: request))
            .log()
            .validate(statusCode: 200 ..< 300)
            .apiManagerResponse { response in
                switch response.httpStatusCode {
                case 200 ..< 300:
                    guard let result = GetEventByPartyResponse(json: response.jsonDictionary) else {
                        return completion(WireError.parsing, nil)
                    }
                    
                    Parser.SAVHistory.parseGetEventByParty(response: result, deleteOldData: true, completion: { (error) in
                        return completion(error, nil)
                    })

                case 400:
                    return completion(response.result.error, nil)
                default:
                    return completion(response.result.error, nil)
                }
        }
    }
}

// MARK: Structs

public struct GetSAVEventByPartyParameter {
    
    public var partyId: Int
    public var effectiveFrom: Date?
    public var effectiveTo: Date?
    public var eventTypeFilterTypeKeys: [Int]?
    public var eventTypesTypeKeys: [EventTypeRef]?
    public init(partyId: Int, effectiveFrom: Date? = nil, effectiveTo: Date? = nil, eventTypeFilterTypeKeys: [Int]) {
        self.partyId = partyId
        self.effectiveFrom = effectiveFrom
        self.effectiveTo = effectiveTo
        self.eventTypeFilterTypeKeys = eventTypeFilterTypeKeys
    }
    public init(partyId: Int, effectiveFrom: Date? = nil, effectiveTo: Date? = nil, eventTypesTypeKeys: [EventTypeRef]) {
        self.partyId = partyId
        self.effectiveFrom = effectiveFrom
        self.effectiveTo = effectiveTo
        self.eventTypesTypeKeys = eventTypesTypeKeys
    }
    public func toJSON() -> Parameters {
        
        var typeFilterKeys = [Parameters]()
        for key in self.eventTypeFilterTypeKeys ?? [] {
            typeFilterKeys.append(["typeKey": key])
        }
        var typesTypeKeys = [Parameters]()
        for key in self.eventTypesTypeKeys ?? [] {
            typesTypeKeys.append(["typeKey": key.rawValue])
        }
        
        var result: Parameters = [:]
        if let effectiveTo = effectiveTo, let effectiveFrom = effectiveFrom {
            result = ["effectivePeriod": [
                "effectiveTo": Wire.default.simpleDateFormatter.string(from:effectiveTo),
                "effectiveFrom": Wire.default.simpleDateFormatter.string(from:effectiveFrom)
                ]
            ]
        }
        
        if typeFilterKeys.count > 0 {
            result["eventTypeFilters"] = typeFilterKeys
        }
        if typesTypeKeys.count > 0 {
            result["eventTypes"] = typesTypeKeys
        }
        
        return result
    }
}
