import Foundation
import Alamofire

// MARK: Router

public enum CMSRouterError: Error {
    case noDataReturned
}

enum CMSRouter: URLRequestConvertible {
    static let basePath = "liferay-content-management"
    
    case getArticleByURLTitle(String, String)
    case getFileByGroupId(String, String, Date?)
    case fileUploadByUser(Int, String, Data)
    case getFileByReferenceId(Int)
    case postFeedbackEmailService()
    case getFileByPartyId(Int)
    case deleteFileByReferenceId(String)
    
    // MARK:
    
    var method: HTTPMethod {
        switch self {
        case .getArticleByURLTitle:
            return .get
        case .getFileByGroupId:
            return .get
        case .fileUploadByUser:
            return .post
        case .getFileByReferenceId:
            return .get
        case .postFeedbackEmailService:
            return .post
        case .getFileByPartyId:
            return .get
        case .deleteFileByReferenceId:
            return .get
        }
    }
    
    var route: String {
        switch self {
        case .getArticleByURLTitle(let urlTitle, let groupId):
            return "content-service/get-article-by-url-title/groupId/\(groupId)/urlTitle/\(urlTitle)"
        case .getFileByGroupId(let fileName, let groupId, _):
            return "document-service/get-file/\(fileName)/groupId/\(groupId)"
        case .fileUploadByUser(let partyId, let fileName, _):
            return "document-service/upload-file/\(fileName)/partyId/\(partyId)"
        case .getFileByReferenceId(let referenceId):
            return "document-service/get-file/referenceId/\(referenceId)"
        case .postFeedbackEmailService():
            return "feedback-email-service/send-feedback-email"
        case .getFileByPartyId(let partyId):
            return "document-service/get-file/\(partyId)_profile.jpg/partyId/\(partyId)"
        case .deleteFileByReferenceId(let referenceId):
            return "document-service/delete-file/referenceId/\(referenceId)"
        }
    }
    
    // MARK: URLRequestConvertible
    
    func asURLRequest() throws -> URLRequest {
        let url = try Wire.urlForPath(path: CMSRouter.basePath).appendingPathComponent(self.route)
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = self.method.rawValue
        
        switch self {
        case .getFileByGroupId(_, _, let modifiedSince):
            if modifiedSince != nil {
                let gmtDate = Wire.default.gmtDateFormatter.string(from: modifiedSince!)
                // If-Modified-Since date must be based on GMT
                // eg: Wed, 21 Oct 2015 07:28:00 GMT
                urlRequest.addValue("\(gmtDate) GMT", forHTTPHeaderField: "If-Modified-Since")
            }
            
    case .getArticleByURLTitle, .fileUploadByUser, .getFileByReferenceId, .postFeedbackEmailService, .getFileByPartyId, .deleteFileByReferenceId:
            break
        }
        
        switch self {
        case .getArticleByURLTitle:
            return try JSONEncoding.default.encode(urlRequest, with: nil)
        case .getFileByGroupId:
            return try JSONEncoding.default.encode(urlRequest, with: nil)
        case .fileUploadByUser(_, _, let data):
            urlRequest.setValue(String(data.count), forHTTPHeaderField: "Content-Length")
            return try JSONEncoding.default.encode(urlRequest, with: nil)
        case .getFileByReferenceId:
            return try JSONEncoding.default.encode(urlRequest, with: nil)
        case .postFeedbackEmailService():
            urlRequest.addValue(AppConfigFeature.insurerCMSGroupId()!, forHTTPHeaderField: "groupId")
            return try JSONEncoding.default.encode(urlRequest, with: nil)
        case .getFileByPartyId:
            urlRequest.setValue("application/octet-stream", forHTTPHeaderField: "Accept")
            return try JSONEncoding.default.encode(urlRequest, with: nil)
        case .deleteFileByReferenceId(_):
            return try JSONEncoding.default.encode(urlRequest, with: nil)

        }
    }
}

enum CMSUploadRouter: URLConvertible {
    
    case fileUploadByUser(Int, String, Data)
    case profileImageUploadByUser(Int, String, Data)
    
    // MARK:
    
    var method: HTTPMethod {
        switch self {
        case .fileUploadByUser:
            return .post
        case .profileImageUploadByUser:
            return .put
        }
    }
    
    var route: String {
        switch self {
        case .fileUploadByUser(let partyId, let fileName, _):
            return "document-service/upload-file/\(fileName)/partyId/\(partyId)"
        case .profileImageUploadByUser(let partyId, let fileName, _):
            return "document-service/upload-file/\(fileName)/partyId/\(partyId)"
        }
    }
    
    func asURL() throws -> URL {
        let url = try Wire.urlForPath(path: CMSRouter.basePath).appendingPathComponent(self.route)
        return url
    }
}

// MARK: Networking

extension Wire.Content {
    public static func getArticleByURLTitle(urlTitle: String, groupId: String, completion: @escaping ((_: String?, _: Error?) -> Void)) {
        Wire.sessionManager.request(CMSRouter.getArticleByURLTitle(urlTitle, groupId))
            .log()
            .validate(statusCode: 200 ..< 300)
            .apiManagerResponse { response in
                switch response.httpStatusCode {
                case 200 ..< 300:
                    completion(response.result.value?.responseString, nil)
                default:
                    completion(nil, response.result.error)
                }
        }
    }
    
    public static func getFileByGroupId(fileName: String, groupId: String, modifiedSince: Date?, completion: @escaping ((_: Data?, _: Error?) -> Void)) {
        Wire.sessionManager.request(CMSRouter.getFileByGroupId(fileName, groupId, modifiedSince))
            .log()
            .validate(statusCode: 200 ..< 300)
            .apiManagerResponse { response in
                switch response.httpStatusCode {
                case 200 ..< 300:
                    guard let data = response.data else {
                        completion(nil, CMSRouterError.noDataReturned)
                        return
                    }
                    completion(data, nil)
                default:
                    completion(nil, response.result.error ?? BackendError.other)
                }
        }
    }
    
    public static func getFileByReferenceId(referenceId: Int, completion: @escaping ((_: String?, _: Data?, _: Error?) -> Void)) {
        Wire.sessionManager.request(CMSRouter.getFileByReferenceId(referenceId))
            .log()
            .validate(statusCode: 200 ..< 300)
            .apiManagerResponse { response in
                switch response.httpStatusCode {
                case 200 ..< 300:
                    let headers = response.response?.allHeaderFields["Content-Disposition"] as? String
                    guard let data = response.data else {
                        completion(nil, nil, CMSRouterError.noDataReturned)
                        return
                    }
                    completion(headers, data, nil)
                    
                //NSLog(data.base64EncodedString())
                default:
                    completion(nil, nil, response.result.error ?? BackendError.other)
                }
        }
    }
    
    public typealias FileUploadReference = String?
    
    public static func uploadFile(partyId: Int, fileContents: Data, completion: @escaping ((_: FileUploadReference, _: Error?) -> Void)) {
        let fileName = UUID().uuidString
        
        Wire.sessionManager.upload(multipartFormData: { (multipartFormData) in
            multipartFormData.append(fileContents, withName: "file", fileName: "\(fileName).jpeg", mimeType: "image/jpeg")
        }, to: CMSUploadRouter.fileUploadByUser(partyId, fileName, fileContents), encodingCompletion: { (result) in
            switch result {
            case .success(let uploadRequest, _, _):
                uploadRequest.log()
                    .validate(statusCode: 200 ..< 300)
                    .apiManagerResponse { response in
                        switch response.httpStatusCode {
                        case 200 ..< 300:
                            let referenceId = Wire.Content.parseUploadReferenceId(from: response.result)
                            completion(referenceId, nil)
                        default:
                            completion(nil, response.result.error ?? BackendError.other)
                        }
                }
                break
            case .failure(let error):
                debugPrint(error)
                break
            }
        })
    }
    
    public static func uploadProfileImage(partyId: Int, fileContents: Data, completion: @escaping ((_: FileUploadReference, _: Error?) -> Void)) {
        let fileName = "\(partyId)_profile.jpg"
        
        Wire.sessionManager.upload(multipartFormData: { (multipartFormData) in
            multipartFormData.append(fileContents, withName: "file", fileName: fileName, mimeType: "image/jpeg")
        }, to: CMSUploadRouter.profileImageUploadByUser(partyId, fileName, fileContents), encodingCompletion: { (result) in
            switch result {
            case .success(let uploadRequest, _, _):
                uploadRequest.log()
                    .validate(statusCode: 200 ..< 300)
                    .apiManagerResponse { response in
                        switch response.httpStatusCode {
                        case 200 ..< 300:
                            let referenceId = Wire.Content.parseUploadReferenceId(from: response.result)
                            completion(referenceId, nil)
                        default:
                            completion(nil, response.result.error ?? BackendError.other)
                        }
                }
                break
            case .failure(let error):
                debugPrint(error)
                break
            }
        })
    }
    
    private static func parseUploadReferenceId(from result: Result<APIManagerResponse>) -> FileUploadReference {
        switch result {
        case .success(let response):
            if let success = response.json["success"] as? [String: Any] {
                return success["referenceId"] as! FileUploadReference
            }
        default:
            return nil
        }
        return nil
    }
    
    public static func postSendFeedbackEmail(feedback: [Feedback], imageData: [Data]? = nil, completion: @escaping ((_: Error?) -> Void)) {
        let url = CMSRouter.postFeedbackEmailService()
        
        if let data = imageData, !data.isEmpty {
            let fileName = UUID().uuidString
            
            Wire.sessionManager.upload(multipartFormData: { (multipartFormData) in
                for imageData in data {
                    multipartFormData.append(imageData, withName: "file", fileName: "\(fileName).jpeg", mimeType: "image/jpeg")
                }
                
                for value in feedback {
                    let body = FeedbackParameters(value)
                    
                    for (key, value) in body.toFormData() {
                        multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
                    }
                }
            }, with: url, encodingCompletion: { (result) in
                switch result {
                case .success(let uploadRequest, _, _):
                    uploadRequest.log()
                        .validate(statusCode: 200 ..< 300)
                        .apiManagerResponse { response in
                            switch response.httpStatusCode {
                            case 200 ..< 300:
                                completion(nil)
                            default:
                                completion(response.result.error ?? BackendError.other)
                            }
                    }
                    break
                case .failure(let error):
                    debugPrint(error)
                    break
                }
            })
        }
    }
    
    
    public static func getFileByPartyId(partyId: Int, completion: @escaping ((_: Data?, _: Error?) -> Void)) {
        Wire.sessionManager.request(CMSRouter.getFileByPartyId(partyId))
            .log()
            .validate(statusCode: 200 ..< 300)
            .apiManagerResponse { response in
                switch response.httpStatusCode {
                case 200 ..< 300:
                    guard let data = response.data else {
                        completion(nil, CMSRouterError.noDataReturned)
                        return
                    }
                    completion(data, nil)
                default:
                    completion(nil, response.result.error ?? BackendError.other)
                }
        }
    }
    
    public static func deleteFileByReferenceId(referenceId: String, completion: @escaping ((_: Error?) -> Void)) {
        Wire.sessionManager.request(CMSRouter.deleteFileByReferenceId(referenceId))
            . log()
            .validate(statusCode: 200 ..< 300)
            .apiManagerResponse { response in
                switch response.httpStatusCode {
                case 200 ..< 300:
                    completion(nil)
                default:
                    completion(response.result.error ?? BackendError.other)
                }
        }
    }
}

public struct Feedback {
    var feedbackType: String
    var feedbackTypeId: String
    var feedbackTimestamp: Date
    var feedbackMessage: String
    var memberPartyId: String
    var memberFirstName: String
    var memberLastName: String
    var contactNumber: String
    var membershipNumber: String
    var memberEmailAddress: String
    var channelName: String
    var deviceDetails: String
    var osDetails: String
    var appVersion: String
    
    public init(feedbackType: String, feedbackTypeId: String, feedbackTimestamp: Date,
                feedbackMessage: String, memberPartyId: String, memberFirstName: String,
                memberLastName: String, contactNumber: String, membershipNumber: String,
                memberEmailAddress: String, channelName: String, deviceDetails: String,
                osDetails: String, appVersion: String) {
        self.feedbackType = feedbackType
        self.feedbackTypeId = feedbackTypeId
        self.feedbackTimestamp = feedbackTimestamp
        self.feedbackMessage = feedbackMessage
        self.memberPartyId = memberPartyId
        self.memberFirstName = memberFirstName
        self.memberLastName = memberLastName
        self.contactNumber = contactNumber
        self.membershipNumber = membershipNumber
        self.memberEmailAddress = memberEmailAddress
        self.channelName = channelName
        self.deviceDetails = deviceDetails
        self.osDetails = osDetails
        self.appVersion = appVersion
    }
}


public struct FeedbackParameters {
    var feedback: Feedback
    
    public init(_ feedback: Feedback) {
        self.feedback = feedback
    }
    
    private func createFeedback(_ feedback: Feedback) -> Parameters {
        let formattedDate = DateFormatter.apiManagerFormatterWithMilliseconds().string(from: feedback.feedbackTimestamp)
        return [
            "feedbackType": feedback.feedbackType,
            "feedbackTypeId": feedback.feedbackTypeId,
            "feedbackTimestamp": formattedDate,
            "feedbackMessage": feedback.feedbackMessage,
            "memberPartyId": feedback.memberPartyId,
            "memberFirstName": feedback.memberFirstName,
            "memberLastName": feedback.memberLastName,
            "contactNumber": feedback.contactNumber,
            "membershipNumber": feedback.membershipNumber,
            "memberEmailAddress": feedback.memberEmailAddress,
            "channelName": feedback.channelName,
            "deviceDetails": feedback.deviceDetails,
            "osDetails": feedback.osDetails,
            "appVersion": feedback.appVersion
        ]
    }
    
    public func toFormData() -> Parameters {
        return createFeedback(feedback)
    }
}
