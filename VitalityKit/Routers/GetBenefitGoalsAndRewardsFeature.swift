//
//  GetBenefitGoalsAndRewardsFeature.swift
//  VitalityActive
//
//  Created by Steven Layug on 4/20/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import Alamofire
import VIAUtilities

enum GetBenefitGoalsAndRewardsFeatureRouter: URLRequestConvertible {
    case getBenefitGoalsAndRewardsFeature(tenantId: Int, request: GetBenefitGoalsAndRewardsParameters)
    
    var basePath: String {
        switch self {
        case .getBenefitGoalsAndRewardsFeature(_, _):
            return "vitality-purchase-orchestration-service-1"
        }
    }
    
    var method: HTTPMethod {
        switch self {
        case .getBenefitGoalsAndRewardsFeature(_, _):
            return .post
        }
    }

    var route: String {
        switch self {
        case .getBenefitGoalsAndRewardsFeature(let tenantId, _):
            return ("svc/\(tenantId)/getBenefitGoalsAndRewards")
        }
    }
    
    var parameters: Parameters {
        switch self {
        case .getBenefitGoalsAndRewardsFeature(_, let request):
            return request.toJSON()
        }
    }
        
    func asURLRequest() throws -> URLRequest {
        let url = try Wire.urlForPath(path: self.basePath).appendingPathComponent(self.route)
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = self.method.rawValue
        switch self {
        case .getBenefitGoalsAndRewardsFeature:
            return try JSONEncoding.default.encode(urlRequest, with: self.parameters)
        }
    }
}

extension Wire.Events {
    public static func getBenefitGoalsAndRewardsFeature(tenantId: Int, request: GetBenefitGoalsAndRewardsParameters, completion: @escaping (( _: Error?) -> Void)) {
        let url = GetBenefitGoalsAndRewardsFeatureRouter.getBenefitGoalsAndRewardsFeature(tenantId: tenantId, request: request)
        Wire.sessionManager.request(url)
            .log()
            .validate(statusCode: 200 ..< 300)
            .apiManagerResponse { response in
                switch response.httpStatusCode {
                case 200 ..< 300:
                    guard let result = GetBenefitGoalsAndRewardsFeatureResponse(json: response.jsonDictionary) else {
                        completion(WireError.parsing)
                        return
                    }
                    
                    // TODO: Force to delete old data?
                    Parser.BenefitGoalsAndRewards.parseGDCBenefit(response: result, deleteOldData: true, completion: completion)
                default:
                    completion(response.result.error ?? BackendError.other)
                }
        }
    }
}

public struct GetBenefitGoalsAndRewardsParameters {
    
    private var benefitId: Int
    private var partyId: Int
    private var productKey: Int
    
    public init(benefitId: Int, partyId: Int, productKey: Int) {
        self.benefitId = benefitId
        self.partyId = partyId
        self.productKey = productKey
    }
    
    public func toJSON() -> Parameters {
        return [
            "benefits": [[
                "benefitId": self.benefitId,
                ]],
            "partyId": self.partyId,
            "products:": [[
                "productKey": self.productKey
                ]]
        ]
    }
}
