////
////  GetUserInstructionRouter.swift
////  VitalityActive
////
////  Created by Simon Stewart on 2/9/17.
////  Copyright © 2017 Glucode. All rights reserved.
////
//
//import Foundation
//import Alamofire
//import AlamofireActivityLogger
//
//
//// MARK: Router
//
//enum GetUserInstructionRouter: URLRequestConvertible {
//    static let basePath = "vitality-manage-user-service-v1"
//    
//    case getUserInstruction(tenantId: Int, userId: String)
//    
//    var method: HTTPMethod {
//        switch self {
//        case .getUserInstruction:
//            return .post
//        }
//    }
//    
//    var route: String {
//        switch self {
//        case .getUserInstruction(let tenantId, _):
//            return ("\(tenantId)/v1/getUserInstruction")
//        }
//    }
//    
//    var parameters: Parameters {
//        switch self {
//        case .getUserInstruction( _, let userId):
//            let body = GetUserInstructionParameters(userId: userId)
//            return body.toJSON()
//        }
//    }
//    
//    func asURLRequest() throws -> URLRequest {
//        let url = try Wire.urlForPath(path: GetUserInstructionRouter.basePath).appendingPathComponent(self.route)
//        var urlRequest = URLRequest(url: url)
//        urlRequest.httpMethod = self.method.rawValue
//        switch self {
//        case .getUserInstruction:
//            return try JSONEncoding.default.encode(urlRequest, with: self.parameters)
//        }
//    }
//}
//
//extension Wire.Member {
//    public static func getUserInstruction(tenantId: Int, userId: String, completion: @escaping (_ result: Any?, _ error: Error?) -> Void) {
//        let url = GetUserInstructionRouter.getUserInstruction(tenantId: tenantId, userId: userId)
//        Wire.sessionManager.request(url)
//            .log()
//            .validate(statusCode: 200 ..< 300)
//            .apiManagerResponse  { response in
//                switch response.httpStatusCode {
//                case 200 ..< 300:
//                    guard let result = GetUserInstructionResponse(json: response.jsonDictionary) else {
//                        completion(nil, ParserError.unknown)
//                        return
//                    }
//                    completion(result, nil)
//                default:
//                    debugPrint("\(response.result.error)")
//                    completion(nil, response.result.error)
//                }
//        }
//    }
//}
//
//public struct GetUserInstructionParameters {
//    
//    private var userId: String
//    
//    public init(userId: String) {
//        self.userId = userId
//    }
//    
//    public func toJSON() -> Parameters {
//        return [ "userId" : userId]
//    }
//}
