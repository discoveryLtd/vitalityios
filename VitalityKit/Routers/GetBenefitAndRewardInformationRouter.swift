//
//  GetBenefitAndRewardInformationRouter.swift
//  VitalityActive
//
//  Created by Simon Stewart on 10/10/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireActivityLogger


// MARK: Router

enum GetBenefitAndRewardInformationRouter: URLRequestConvertible {
    static let basePath = "vitality-manage-vitality-products-service-1"
    
    case getBenefitAndRewardInfo(tenantId: Int, partyId: Int, vitalityMembershipId: Int)
    
    var method: HTTPMethod {
        switch self {
        case .getBenefitAndRewardInfo:
            return .post
        }
    }
    
    var route: String {
        switch self {
        case .getBenefitAndRewardInfo(let tenantId, let partyId, let vitalityMembershipId):
            return ("svc/\(tenantId)/getBenefitAndRewardInformation/\(partyId)/\(vitalityMembershipId)")
        }
    }
    
    func asURLRequest() throws -> URLRequest {
        let url = try Wire.urlForPath(path: GetBenefitAndRewardInformationRouter.basePath).appendingPathComponent(self.route)
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = self.method.rawValue
        
        switch self {
        case .getBenefitAndRewardInfo:
            return try JSONEncoding.default.encode(urlRequest, with: [:])
        }
    }
}

extension Wire.Member {
    public static func getBenefitAndRewardInfo(tenantId: Int, partyId: Int, vitalityMembershipId: Int, completion: @escaping (_ error: Error?, _ result: BenefitAndRewardInformationResponse?) -> Void) {
        let url = GetBenefitAndRewardInformationRouter.getBenefitAndRewardInfo(tenantId: tenantId, partyId: partyId, vitalityMembershipId: vitalityMembershipId)
        Wire.sessionManager.request(url)
            .log()
            .validate(statusCode: 200 ... 200)
            .apiManagerResponse { response in
                switch response.httpStatusCode {
                case 200 ... 200:
                    guard let result = BenefitAndRewardInformationResponse(json: response.jsonDictionary) else {
                        completion(WireError.parsing, nil)
                        return
                    }
                    return completion(nil, result)
                default:
                    debugPrint("\(String(describing: response.result.error))")
                    completion(response.result.error ?? BackendError.other, nil)
                }
        }
    }
}
