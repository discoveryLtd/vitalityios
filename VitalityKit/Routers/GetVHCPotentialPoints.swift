import Foundation
import Alamofire
import VIAUtilities

// MARK: Errors
public enum GetVHCPotentialPointsError: Error {
    case incorrectParameters
}

// MARK: Router

enum GetVHCPotentialPointsRouter: URLRequestConvertible {

    case getPotentialPoints(tenantId: Int, partyId: Int, eventTypes: [EventTypeRef])

    var basePath: String {
        switch self {
        case .getPotentialPoints(_, _, _):
            return "vitality-event-points-services-service-1"
        }
    }

    var method: HTTPMethod {
        switch self {
        case .getPotentialPoints(_, _, _):
            return .post
        }
    }

    var route: String {
        switch self {
        case .getPotentialPoints(let tenantId, let partyId, _):
            return ("svc/\(tenantId)/getPotentialPointsEventsCompletedAndHealthyRanges/\(partyId)")

        }
    }

    var parameters: Parameters? {
        switch self {
        case .getPotentialPoints(_, _, let eventTypes):
            let body = GetVHCPotentialPointsParameters(eventTypes: eventTypes)
            return body.toJSON()
        }
    }

    // MARK: URLRequestConvertible

    func asURLRequest() throws -> URLRequest {
        let url = try Wire.urlForPath(path: self.basePath).appendingPathComponent(self.route)
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = self.method.rawValue

        switch self {
        case .getPotentialPoints(_, _, _):
            return try JSONEncoding.default.encode(urlRequest, with: self.parameters)
        }
    }
}

// MARK: Networking

extension Wire.Events {
    public static func getVHCPotentialPointsEventsAndHealthyRanges(tenantId: Int, partyId: Int, eventTypes: [EventTypeRef], completion: @escaping (( _: Error?) -> Void)) {

        guard partyId != 0 else {
            return completion(GetVHCPotentialPointsError.incorrectParameters)
        }

        Wire.sessionManager.request(GetVHCPotentialPointsRouter.getPotentialPoints(tenantId: tenantId, partyId: partyId, eventTypes: eventTypes))
            .log()
            .validate(statusCode: 200 ..< 300)
            .apiManagerResponse { response in
                switch response.httpStatusCode {
                case 200 ..< 300:

                    guard let result = GetVHCPotentialPointsResponse(json: response.jsonDictionary) else {
                        completion(WireError.parsing)
                        return
                    }
                    Parser.VHC.parseGetPotentialPoints(response: result, completion: completion)

                case 400:
                    completion(response.result.error ?? BackendError.other)
                default:
                    completion(response.result.error ?? BackendError.other) // TODO: sort out proper error handling
                }
        }
    }
    
//    public static func getVHCPotentialPointsEventsAndHealthyRanges(tenantId: Int, partyId: Int, eventTypes: [EventTypeRef], completion: @escaping (( _: Error?) -> Void)) {
//        if let path = Bundle.main.url(forResource: "getPotentialPoints", withExtension: "json"){
//            if let stubResponse = parseJsonFile(url: path){
//                guard let result = GetVHCPotentialPointsResponse(json: stubResponse) else {
//                    return completion(WireError.parsing)
//                }
//                Parser.VHC.parseGetPotentialPoints(response: result, completion: completion)
//            }
//        }
//        return completion(nil)
//    }
}

// MARK: Structs

public struct GetVHCPotentialPointsParameters {
    public var eventTypes: [EventTypeRef]

    public init(eventTypes: [EventTypeRef]) {
        self.eventTypes = eventTypes
    }

    public func toJSON() -> Parameters {
        var events = [[String: Int]]()
        for key in eventTypes {
            events.append(["typeKey": key.rawValue])
        }
        let parameters = [
            "events": events,
            ]
        return parameters
    }
}
