////
////  GetMemberProfileRouter.swift
////  VitalityActive
////
////  Created by Simon Stewart on 2/6/17.
////  Copyright © 2017 Glucode. All rights reserved.
////
//
//import Foundation
//import Alamofire
//import AlamofireActivityLogger
//
//
//// MARK: Errors
//
//public enum GetMemberProfileError: Error {
//    case connectivityError
//}
//
//// MARK: Router
//
//enum GetMemberProfileRouter: URLRequestConvertible {
//    static let basePath = "vitality-party-membership-services-service-v1"
//    
//    case getMemberProfile(tenantId: Int, roleTypeKey: Int, partyId: Int, effectiveDate: Date)
//    
//    var method: HTTPMethod {
//        switch self {
//        case .getMemberProfile( _, _, _, _):
//            return .get
//        }
//    }
//    
//    var route: String {
//        switch self {
//        case .getMemberProfile(let tenantId, _, let partyId, _):
//            //return ("\(tenantId)/v1/getMemberProfile")
//            return ("svc/\(tenantId)/getMemberProfile/\(partyId)")
//        }
//    }
//    
//    var parameters: Parameters {
//        switch self {
//        case .getMemberProfile( _, let roleTypeKey, let partyId, let effectiveDate):
//            let body = GetMemberProfileParameters(roleTypeKey: roleTypeKey, partyId: partyId, effectiveDate: effectiveDate)
//            return body.toJSON()
//        }
//    }
//    
//    func asURLRequest() throws -> URLRequest {
//        let url = try Wire.urlForPath(path: GetMemberProfileRouter.basePath).appendingPathComponent(self.route)
//        var urlRequest = URLRequest(url: url)
//        urlRequest.httpMethod = self.method.rawValue
//        switch self {
//        case .getMemberProfile:
//            return try JSONEncoding.default.encode(urlRequest, with: self.parameters)
//        }
//    }
//}
//
//extension Wire.Member {
//    public static func getMemberProfile(tenantId: Int, roleTypeKey: Int, partyId: Int, effectiveDate: Date, completion: @escaping (_ result: GetMemberProfileResponse?, _ error: Error?) -> Void) {
//        let url = GetMemberProfileRouter.getMemberProfile(tenantId: tenantId, roleTypeKey: roleTypeKey, partyId: partyId, effectiveDate: effectiveDate)
//        Wire.sessionManager.request(url)
//            .log()
//            .validate(statusCode: 200 ..< 300)
//            .apiManagerResponse  { response in
//                switch response.httpStatusCode {
//                case 200 ..< 300:
//                    guard let result = GetMemberProfileResponse(json: response.jsonDictionary) else {
//                        completion(nil, ParserError.unknown)
//                        return
//                    }
//                    completion(result, nil)
//                    
//                default:
//                    debugPrint("\(response.result.error)")
//                    completion(nil, response.result.error)
//                }
//        }
//    }
//    
//}
//
//public struct GetMemberProfileParameters {
//    private var roleTypeKey: Int
//    private var partyId: Int
//    private var effectiveDate: Date
//    
//    public init(roleTypeKey: Int, partyId: Int, effectiveDate: Date) {
//        self.roleTypeKey = roleTypeKey
//        self.partyId = partyId
//        self.effectiveDate = effectiveDate
//    }
//    
//    public func toJSON() -> Parameters {
//        let effectiveDateString = Wire.default.yearMonthDayFormatter.string(from: self.effectiveDate)
//        return [ "partyRole" : [ "roleTypeKey" : roleTypeKey, "partyId" : partyId, "effectiveDate" : effectiveDateString ]]
//    }
//}
