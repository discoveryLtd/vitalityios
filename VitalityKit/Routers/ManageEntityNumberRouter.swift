//
//  ManageEntityNumberRouter.swift
//  VitalityActive
//
//  Created by Val Tomol on 08/01/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import VIAUtilities
import Alamofire
import RealmSwift

enum ManageEntityNumberRouter: URLRequestConvertible {
    static let basePath = "legacy/entitymaintenance"
    
    case addEntityNumber(request: ManageEntityNumberParams)
    case updateEntityNumber(request: ManageEntityNumberParams)
    
    var method: HTTPMethod {
        switch self {
        case .addEntityNumber:
            return .post
        case .updateEntityNumber:
            return .put
        }
    }
    
    var route: String {
        switch self {
        case .addEntityNumber(_):
            return ("entitynumber")
        case .updateEntityNumber(_):
            return ("entitynumber")
        }
    }
    
    var parameters: Parameters? {
        switch self {
        case .addEntityNumber(let params):
            return params.toJSON()
        case .updateEntityNumber(let params):
            return params.toJSON()
        }
    }
    
    func asURLRequest() throws -> URLRequest {
        let url = try Wire.urlForPath(path: ManageEntityNumberRouter.basePath).appendingPathComponent(self.route)
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = self.method.rawValue
        switch self {
        case .addEntityNumber:
            return try JSONEncoding.default.encode(urlRequest, with: self.parameters)
        case .updateEntityNumber:
            return try JSONEncoding.default.encode(urlRequest, with: self.parameters)
        }
    }
}

extension Wire.Party {
    public static func addEntityNumber(request: ManageEntityNumberParams, completion: @escaping (_ success: Bool) -> Void) {
        let url = ManageEntityNumberRouter.addEntityNumber(request: request)
        Wire.sessionManager.request(url)
            .log()
            .validate(statusCode: 200 ..< 300)
            .apiManagerResponse { response in
                switch response.httpStatusCode {
                case 201: // Status 201 - Created
                    completion(true)
                    resetRealm(with: request.entityNumber)
                default:
                    completion(false)
                }
        }
    }
    
    public static func updateEntityNumber(request: ManageEntityNumberParams, completion: @escaping (_ success: Bool) -> Void) {
        let url = ManageEntityNumberRouter.updateEntityNumber(request: request)
        Wire.sessionManager.request(url)
            .log()
            .validate(statusCode: 200 ..< 300)
            .apiManagerResponse { response in
                switch response.httpStatusCode {
                case 200: // Status 200 - Ok
                    completion(true)
                    resetRealm(with: request.entityNumber)
                default:
                    completion(false)
                }
        }
    }
    
    public static func resetRealm(with entityNumber: String) {
        var realm = DataProvider.newRealm()
        
        // Retrieve current party details from realm.
        let currentPartyDetails = realm.objects(VitalityParty.self)
        
        // Temporary holder for new set of references.
        let references:List<PartyReference> = List<PartyReference>()
        
        // Create new instance of realm for writing of updates.
        realm = DataProvider.newRealm()
        try! realm.write {
            for detail in currentPartyDetails {
                for reference in detail.references {
                    if reference.type == String(PartyReferenceTypeRef.EntityNumber.rawValue) {
                        let editedReference = reference
                        editedReference.value = entityNumber
                        editedReference.type = String(PartyReferenceTypeRef.EntityNumber.rawValue)
                        references.append(editedReference)
                    }
                }
            }
        }
    }
}

public struct ManageEntityNumberParams {
    public var entityNumber: String
    public var partyId: String
    public var dateOfBirth: String
    
    public init (entityNumber: String, partyId: String, dateOfBirth: String) {
        self.entityNumber = entityNumber
        self.partyId = partyId
        self.dateOfBirth = dateOfBirth
    }
    
    public func toJSON() -> Parameters {
        let result = ["entityNumber" : entityNumber,
                      "partyId" : partyId,
                      "dateOfBirth" : dateOfBirth]
        debugPrint(result)
        return result
    }
}
