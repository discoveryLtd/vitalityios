//
//  GetPartyByIdRouter.swift
//  VitalityActive
//
//  Created by Simon Stewart on 11/29/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import Alamofire
import VIAUtilities


enum GetPartyByIdRouter: URLRequestConvertible {
    static let basePath = "vitality-manage-party-information-service-1"
    
    case getPartyById(tenantId: Int, partyId: Int)
    
    var method: HTTPMethod {
        switch self {
        case .getPartyById:
            return .get
        }
    }
    
    var route: String {
        switch self {
        case .getPartyById(let tenantId, let partyId):
            return ("svc/\(tenantId)/getPartyById/\(partyId)")
        }
    }
    
    
    func asURLRequest() throws -> URLRequest {
        let url = try Wire.urlForPath(path: GetPartyByIdRouter.basePath).appendingPathComponent(self.route)
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = self.method.rawValue
        switch self {
        case .getPartyById:
            return try JSONEncoding.default.encode(urlRequest, with: nil)
        }
    }
}

extension Wire.Rewards {
    
    public static func getPartyById(tenantId: Int, partyId: Int, completion: @escaping (_ error: Error?, _ result: GetPartyByIdResponse?) -> Void) {
    
        let url = GetPartyByIdRouter.getPartyById(tenantId: tenantId, partyId: partyId)
        Wire.sessionManager.request(url)
            .log()
            .validate(statusCode: 200 ..< 300)
            .apiManagerResponse { response in
                switch response.httpStatusCode {
                case 200 ..< 300:
                    guard let result = GetPartyByIdResponse(json: response.jsonDictionary) else {
                        return completion(WireError.parsing, nil)
                    }
                    completion(nil, result)
                default:
                    debugPrint("\(String(describing: response.result.error))")
                    completion(response.result.error ?? BackendError.other, nil)
                }
        }
    }
    
}

