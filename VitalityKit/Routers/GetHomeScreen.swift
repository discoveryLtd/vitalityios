//
//  GetHomeScreen.swift
//  VitalityActive
//
//  Created by Simon Stewart on 2/23/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireActivityLogger


// MARK: Router

enum GetHomeScreenRouter: URLRequestConvertible {
    static let basePath = "vitality-home-screen-points-agreement-service-1/"

    case getHomeScreen(tenantId: Int, partyId: Int, vitalityMembershipId: Int)

    var method: HTTPMethod {
        switch self {
        case .getHomeScreen:
            return .get
        }
    }

    var route: String {
        switch self {
        case .getHomeScreen(let tenantId, let partyId, let vitalityMembershipId):
            return ("svc/\(tenantId)/getHomeScreenGetPointsGetStatus/\(partyId)/\(vitalityMembershipId)")
        }
    }

    func asURLRequest() throws -> URLRequest {
        let url = try Wire.urlForPath(path: GetHomeScreenRouter.basePath).appendingPathComponent(self.route)
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = self.method.rawValue
        switch self {
        case .getHomeScreen(_, _, _):
            return try URLEncoding.default.encode(urlRequest, with: nil)
        }
    }
}

extension Wire.Content {
    public static func getHomeScreen(tenantId: Int, partyId: Int, vitalityMembershipId: Int, completion: @escaping (_ error: Error?) -> Void) {
        let url = GetHomeScreenRouter.getHomeScreen(tenantId: tenantId, partyId: partyId, vitalityMembershipId: vitalityMembershipId)
        Wire.sessionManager.request(url)
            .log()
            .validate(statusCode: 200 ..< 300)
            .apiManagerResponse { response in
                switch response.httpStatusCode {
                case 200 ..< 300:
                    guard let result = GetHomeScreenResponse(json: response.jsonDictionary) else {
                        return completion(WireError.parsing)
                    }
                    Parser.HomeScreen.parseGetHomeScreen(response: result, completion: completion)
                default:
                    return completion(response.result.error ?? BackendError.other)
                }
        }
    }
}
