import Foundation
import Alamofire

enum ActivationBarcodeRouter: URLRequestConvertible {
    static let baseActivationPath =     "activation-barcode-service/"
    static let baseMediaObjectPath =    "vitality-manage-media-object-service/"
    
    case generate(tenantId: Int, partyId: Int, useAcceptEncodingDeflateHeader: Bool)
    case download(tenantId: Int, key: Int)
    
    var method: HTTPMethod {
        switch self {
        case .generate:
            return .get
        case .download:
            return .get
        }
    }
    
    var route: String {
        switch self {
        case .generate(let tenantId, let partyId, _):
            return ("activationBarcode?tenantId=\(tenantId)&partyId=\(partyId)")
        case .download(let tenantId, let key):
            return ("svc/\(tenantId)/getGeneratedMediaObjectByKey/\(key)")
        }
    }
    
    func asURLRequest() throws -> URLRequest {
        switch self {
        case .generate(_, _, let useAcceptEncodingDeflateHeader):
            let url = try Wire.urlForPath(path: ActivationBarcodeRouter.baseActivationPath).appendingPathComponent(self.route)
            var urlRequest = URLRequest(url: url)
            urlRequest.httpMethod = self.method.rawValue
            if useAcceptEncodingDeflateHeader {
                urlRequest.setValue("deflate", forHTTPHeaderField: "Accept-Encoding")
            }
            return try JSONEncoding.default.encode(urlRequest, with: nil)
        case .download(_, _):
            let url = try Wire.urlForPath(path: ActivationBarcodeRouter.baseMediaObjectPath).appendingPathComponent(self.route)
            var urlRequest = URLRequest(url: url)
            urlRequest.httpMethod = self.method.rawValue
            urlRequest.setValue("application/pdf", forHTTPHeaderField: "Content-Type")
            return try JSONEncoding.default.encode(urlRequest, with: nil)
        }
    }
}

extension Wire.ActivationCode {
    public static func generate(tenantId: Int, partyId: Int, useAcceptEncodingDeflateHeader: Bool, completion: @escaping (_ error: Error?, _ response: Data?) -> Void) {
        let url = ActivationBarcodeRouter.generate(tenantId: tenantId, partyId: partyId, useAcceptEncodingDeflateHeader: useAcceptEncodingDeflateHeader)
        Wire.sessionManager.request(url)
            .log()
            .validate(statusCode: 200 ..< 300)
            .apiManagerResponse { response in
                switch response.httpStatusCode {
                case 200 ..< 300:
                    
                    var valueKey = 0
                    
                    for (_, value) in response.jsonDictionary {
                        for (jsonkey, jsonValue) in (value as? [String: Any])! {
                            if jsonkey == "key" {
                                valueKey = jsonValue as! Int
                            }
                        }
                    }
                    
                    if valueKey > 0 {
                        Wire.ActivationCode.download(tenantId: tenantId, key: valueKey, completion: completion)
                    } else {
                        completion(BackendError.other, nil)
                    }
                    
                    
                default:
                    completion(response.result.error, nil)
                }
        }
    }
    
    public static func download(tenantId: Int, key: Int, completion: @escaping (_ error: Error?, _ response: Data?) -> Void) {
        let url = ActivationBarcodeRouter.download(tenantId: tenantId, key: key)
        
        Wire.sessionManager.request(url)
            .log()
            .validate(statusCode: 200 ..< 300)
            .apiManagerResponse { response in
                switch response.httpStatusCode {
                case 200 ..< 300:
            
                    completion(nil, response.data)
                    
                default:
                    completion(response.result.error, nil)
                }
        }
    }
}

// MARK: Structs
//public class ActivationBarcodeRequest {
//    public var tenantId: Int
//    public var partyId: Int
//
//    public init(tenantId: Int, partyId: Int) {
//        self.tenantId = tenantId
//        self.partyId = partyId
//    }
//
//    public func toJSON() -> Parameters {
//        return [
//            "tenantId": tenantId,
//            "partyId": partyId
//        ]
//    }
//}
