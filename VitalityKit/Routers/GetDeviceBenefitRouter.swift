//
//  GetDeviceBenefitRouter.swift
//  VitalityActive
//
//  Created by Von Kervin R. Tuico on 24/04/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import Alamofire

// MARK: Router
enum GetDeviceBenefitRouter: URLRequestConvertible {
    
    case getDeviceBenefit(tenantId: Int, request: GetDeviceBenefitParameters)
    
    static let basePath = "vitality-manage-goals-service-1"
    
    var method: HTTPMethod {
        switch self {
        case .getDeviceBenefit(_, _):
            return .post
        }
    }
    
    var route: String {
        switch self {
        case .getDeviceBenefit(let tenantId, _):
            return ("svc/\(tenantId)/getDeviceBenefit")
        }
    }
    
    var parameters: Parameters? {
        switch self {
        case .getDeviceBenefit(_, let params):
            return params.toJSON()
        }
    }
    
    // MARK: URLRequestConvertible
    
    func asURLRequest() throws -> URLRequest {
        let url = try Wire.urlForPath(path: GetDeviceBenefitRouter.basePath).appendingPathComponent(self.route)
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = self.method.rawValue
        
        switch self {
        case .getDeviceBenefit:
            return try JSONEncoding.default.encode(urlRequest, with: self.parameters)
        }
    }
}

// MARK: Networking

extension Wire.Events {
    public static func getDeviceBenefit(tenantId: Int, request: GetDeviceBenefitParameters, completion: @escaping (( _: Error?) -> Void)) {
        Wire.sessionManager.request(GetDeviceBenefitRouter.getDeviceBenefit(tenantId: tenantId, request: request))
            .log()
            .validate(statusCode: 200 ..< 300)
            .apiManagerResponse { response in
                switch response.httpStatusCode {
                case 200 ..< 300:
                    
                    guard let result = GetDeviceBenefitResponse(json: response.jsonDictionary) else {
                        return completion(WireError.parsing)
                    }
                    Parser.GDC.parseGDCDeviceBenefit(response: result, completion: completion)
                default:
                    return completion(response.result.error ?? BackendError.other)
                }
        }
    }
}

// MARK: Structs

public struct GetDeviceBenefit {
    public var productFeatureKey: Int
    public var swap: Bool
    public var partyId: Int
    
    public init(productFeatureKey: Int, swap: Bool, partyId: Int) {
        self.productFeatureKey = productFeatureKey
        self.swap = swap
        self.partyId = partyId
    }
}

public struct GetDeviceBenefitParameters {
    var deviceBenefit: GetDeviceBenefit
    
    public init(_ deviceBenefit: GetDeviceBenefit) {
        self.deviceBenefit = deviceBenefit
    }
    
    private func createDeviceBenefit(_ deviceBenefit: GetDeviceBenefit) -> Parameters {
        return [
            "productFeatureKey": deviceBenefit.productFeatureKey,
            "swap": deviceBenefit.swap,
            "partyId": deviceBenefit.partyId
        ]
    }
    
    public func toJSON() -> Parameters {
        return createDeviceBenefit(deviceBenefit)
    }
}
