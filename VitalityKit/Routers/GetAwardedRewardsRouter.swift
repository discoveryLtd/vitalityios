import Foundation
import Alamofire
import AlamofireActivityLogger


// MARK: Router

enum GetAwardedRewardRouter: URLRequestConvertible {
    static let basePath = "vitality-manage-rewards-service"
    
    case getAwardedRewardByPartyId(tenantId: Int, partyId: Int, parameters: GetAwardedRewardByPartyIdParameters)
    case getAwardedRewardById(tenantId: Int, awardedRewardId: Int, parameters: GetAwardedRewardByIdParameters)
    
    var method: HTTPMethod {
        switch self {
        case .getAwardedRewardByPartyId,
             .getAwardedRewardById:
            return .post
        }
    }
    
    var route: String {
        switch self {
        case .getAwardedRewardByPartyId(let tenantId, _, _):
            return ("svc/\(tenantId)/getAwardedRewardByPartyId")
        case .getAwardedRewardById(let tenantId, _, _):
            return ("svc/\(tenantId)/getAwardedRewardByID")
        }
    }
    
    var parameters: Parameters {
        switch self {
        case .getAwardedRewardByPartyId(_, _, let params):
            return params.toJSON()
        case .getAwardedRewardById(_, _, let params):
            return params.toJSON()
        }
    }
    
    func asURLRequest() throws -> URLRequest {
        let url = try Wire.urlForPath(path: GetAwardedRewardRouter.basePath).appendingPathComponent(self.route)
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = self.method.rawValue
        switch self {
        case .getAwardedRewardByPartyId,
             .getAwardedRewardById:
            return try JSONEncoding.default.encode(urlRequest, with: self.parameters)
        }
    }
}

extension Wire.Rewards {
    
    /* Added "LinkedKeys" for handling the SLI Vitality Coins appearing as a reward on Rewards screen */
    public static func getAwardedRewardByPartyId(tenantId: Int, partyId: Int, fromDate: Date,
                                                 toDate: Date, linkedKeys: [Int]? = nil,
                                                 completion: @escaping (_ error: Error?, _ result: GetAwardedRewardResponse?) -> Void) {
        let params = GetAwardedRewardByPartyIdParameters(fromDate: fromDate, toDate: toDate,
                                                         partyId: partyId, linkedKeys: linkedKeys)
        let url = GetAwardedRewardRouter.getAwardedRewardByPartyId(tenantId: tenantId, partyId: partyId, parameters: params)
        Wire.sessionManager.request(url)
            .log()
            .validate(statusCode: 200 ..< 300)
            .apiManagerResponse { response in
                switch response.httpStatusCode {
                case 200 ..< 300:
                    guard let result = GetAwardedRewardResponse(json: response.jsonDictionary) else {
                        return completion(WireError.parsing, nil)
                    }
                    Parser.rewards.parseProductFeaturesPoints(response: result, resetRewards: true, completion: { (error) in
                        return completion(error, result)
                    })
                case 400:
                    // TODO: Remove when server doesn't send errors for users who have not yet earned their first reward.
                    if let jsonDictionary = JSONSerialization.jsonObject(data: response.data!), let errors = jsonDictionary["errors"] as? [[String:Any]], let code = errors.first?["code"] as? Int, code == 30214 {
                        return completion(nil, nil)
                    }
                    
                    debugPrint("\(String(describing: response.result.error))")
                    return completion(response.result.error ?? BackendError.other, nil)
                default:
                    debugPrint("\(String(describing: response.result.error))")
                    return completion(response.result.error ?? BackendError.other, nil)
                }
        }
    }
    
    public static func getAwardedRewardById(tenantId: Int, awardedRewardId: Int, completion: @escaping (_ error: Error?) -> Void) {
        let params = GetAwardedRewardByIdParameters(awardedRewardId: awardedRewardId)
        let url = GetAwardedRewardRouter.getAwardedRewardById(tenantId: tenantId, awardedRewardId: awardedRewardId, parameters: params)
        Wire.sessionManager.request(url)
            .log()
            .validate(statusCode: 200 ..< 300)
            .apiManagerResponse { response in
                switch response.httpStatusCode {
                case 200 ..< 300:
                    guard let awardedRewardJson = response.jsonDictionary["awardedReward"] as? [String: Any] else {
                        return completion(WireError.parsing)
                    }
                    
                    guard  let result = GEARAwardedRewards(json: awardedRewardJson) else {
                        return completion(WireError.parsing)
                    }

                    Parser.rewards.parseAwardedReward(response: result, resetRewards: false, completion: { (error) in
                        return completion(error)
                    })
                default:
                    debugPrint("\(String(describing: response.result.error))")
                    completion(response.result.error ?? BackendError.other)
                }
        }
    }
}

public struct GetAwardedRewardByPartyIdParameters {
    
    private var effectiveFromDate: Date
    private var effectiveToDate: Date
    private var partyId: Int
    private var linkedKeys: [Int]?
    
    public init(fromDate: Date, toDate: Date, partyId: Int, linkedKeys: [Int]?) {
        self.effectiveFromDate = fromDate
        self.effectiveToDate = toDate
        self.partyId = partyId
        self.linkedKeys = linkedKeys
    }
    
    public func toJSON() -> Parameters {
        
        var keys = [Parameters]()
        if let linkedKeys = linkedKeys {
            for linkedKey in linkedKeys {
                keys.append(["id": linkedKey])
            }
        }
        
        if keys.count > 0 {
            return [
                "effectivePeriod": [
                    "effectiveFrom": Wire.default.simpleDateFormatter.string(from: effectiveFromDate),
                    "effectiveTo": Wire.default.simpleDateFormatter.string(from: effectiveToDate),
                ],
                "party": [
                    "id": partyId,
                ],
                "reward": keys
            ]
        } else {
            return [
                "effectivePeriod": [
                    "effectiveFrom": Wire.default.simpleDateFormatter.string(from: effectiveFromDate),
                    "effectiveTo": Wire.default.simpleDateFormatter.string(from: effectiveToDate),
                ],
                "party": [
                    "id": partyId,
                ]
            ]
        }
    }
}

public struct GetAwardedRewardByIdParameters {
    
    private var awardedRewardId: Int
    
    public init(awardedRewardId: Int) {
        self.awardedRewardId = awardedRewardId
    }
    
    public func toJSON() -> Parameters {
        return [
            "reward": [
                "id": awardedRewardId,
            ]
        ]
    }
}
