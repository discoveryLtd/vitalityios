//
//  GetPartnersByCategoryRouter.swift
//  VitalityActive
//
//  Created by Simon Stewart on 11/1/17.
//  Copyright © 2017 Glucode. All rights reserved.
//


import Foundation
import Alamofire
import AlamofireActivityLogger


// MARK: Router

enum GetPartnersByCategoryRouter: URLRequestConvertible {
    static let basePath = "vitality-manage-vitality-products-service-1"
    
    case getPartnersByCategory(tenantId: Int, partyId: Int, request: GetPartnersByCategoryParameters)
    
    var method: HTTPMethod {
        switch self {
        case .getPartnersByCategory:
            return .post
        }
    }
    
    var route: String {
        switch self {
        case .getPartnersByCategory(let tenantId, let partyId, _):
            return ("svc/\(tenantId)/getPartnersByCategory/\(partyId)")
        }
    }
    
    var parameters: Parameters {
        switch self {
        case .getPartnersByCategory(_, _, let params):
            return params.toJSON()
        }
    }
    
    func asURLRequest() throws -> URLRequest {
        let url = try Wire.urlForPath(path: GetPartnersByCategoryRouter.basePath).appendingPathComponent(self.route)
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = self.method.rawValue
        switch self {
        case .getPartnersByCategory:
            return try JSONEncoding.default.encode(urlRequest, with: self.parameters)
        }
    }
}

extension Wire.Member {
    public static func getPartnersByCategory(tenantId: Int, partyId: Int, request: GetPartnersByCategoryParameters, completion: @escaping (_ error: Error?, _ result: GetPartnersByCategoryResponse?) -> Void) {
        let url = GetPartnersByCategoryRouter.getPartnersByCategory(tenantId: tenantId, partyId: partyId, request: request)
        Wire.sessionManager.request(url)
            .log()
            .validate(statusCode: 200 ..< 300)
            .apiManagerResponse { response in
                switch response.httpStatusCode {
                case 200 ..< 300:
                    guard let result = GetPartnersByCategoryResponse(json: response.jsonDictionary) else {
                        return completion(WireError.parsing, nil)
                    }
                    
                    Parser.Partners.parseGetPartnersByCategory(response: result, completion: { error in
                        completion(error, result)
                    })
                    
                default:
					
					
					// TODO: Remove all of this after IGI getPartnersByCategory call works.
					
					if let file = Bundle.main.url(forResource: "igiPartners", withExtension: "json") {
						
						do {
							let data = try Data(contentsOf: file)
							let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
							guard let result = GetPartnersByCategoryResponse(json: json!) else {
								return completion(WireError.parsing, nil)
							}
								
							Parser.Partners.parseGetPartnersByCategory(response: result, completion: { error in
								completion(error, result)
							})
						} catch { debugPrint("IGI Partner hack failed") }
					} else {

                    debugPrint("\(String(describing: response.result.error))")
                    completion(response.result.error ?? BackendError.other, nil)
					}
				}
        }
    }
}

public struct GetPartnersByCategoryParameters {
    
    private var effectiveDate: Date
    private var productFeatureCategoryTypeKey: Int
    
    public init(effectiveDate: Date, productFeatureCategoryTypeKey: Int) {
        self.effectiveDate = effectiveDate
        self.productFeatureCategoryTypeKey = productFeatureCategoryTypeKey
    }
    
    public func toJSON() -> Parameters {
        return [
            "effectiveDate": Wire.default.simpleDateFormatter.string(from:effectiveDate),
            "productFeatureCategoryTypeKey": self.productFeatureCategoryTypeKey
        ]
    }
}
