//
//  GetPreferencesByPartyId.swift
//  VitalityKit
//
//  Created by wenilyn.a.teorica on 22/01/2019.
//  Copyright © 2019 Glucode. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireActivityLogger
import VIAUtilities

// MARK: Router

enum GetPreferencesByPartyIdRouter: URLRequestConvertible {
    static let basePath = "vitality-manage-party-core-service-1"
    
    case getPreferencesByPartyId(tenantId: Int, partyId: Int)
    
    var method: HTTPMethod {
        switch self {
        case .getPreferencesByPartyId:
            return .get
        }
    }
    
    var route: String {
        switch self {
        case .getPreferencesByPartyId(let tenantId, let partyId):
            return ("svc/\(tenantId)/getPreferencesByPartyId/\(partyId)")
        }
    }
    
    func asURLRequest() throws -> URLRequest {
        let url = try Wire.urlForPath(path: GetPreferencesByPartyIdRouter.basePath).appendingPathComponent(self.route)
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = self.method.rawValue
        switch self {
        case .getPreferencesByPartyId(_, _):
            return try URLEncoding.default.encode(urlRequest, with: nil)
        }
    }
}

extension Wire.Content {
    public static func getPreferencesByPartyId(tenantId: Int, partyId: Int, completion: @escaping (_ error: Error?, _ referenceId: String?) -> Void) {
        let url = GetPreferencesByPartyIdRouter.getPreferencesByPartyId(tenantId: tenantId, partyId: partyId)
        Wire.sessionManager.request(url)
            .log()
            .validate(statusCode: 200 ..< 300)
            .apiManagerResponse { response in
                switch response.httpStatusCode {
                case 200 ..< 300:
                    guard let result = parseResponse(json: response.jsonDictionary as AnyObject) else {
                        return completion(WireError.parsing, nil)
                    }
                    
                    let referenceId = getReferenceId(result: result)
                    completion(nil, referenceId)
                default:
                    return completion(response.result.error ?? BackendError.other, nil)
                }
        }
    }
    
    private static func parseResponse(json: AnyObject) -> Array<Any>? {
        return json["generalPreference"] as? Array
    }
    
    private static func getReferenceId(result: Array<Any>) -> String? {
        if let preferenceArray: [Dictionary<String, Any>] = result as? [Dictionary<String, Any>] {
            
            var preferenceListArray = [GetPreferencesByPartyIdResponse]()
            
            for preference in preferenceArray {
                if let typeKey = preference["typeKey"] as? Int, typeKey == PreferenceTypeRef.ProfilePicRef.rawValue {
                    
                    let effectiveFrom: String = preference["effectiveFrom"] as? String ?? "9999-12-31"
                    let value: String = preference["value"] as? String ?? ""
                preferenceListArray.append(GetPreferencesByPartyIdResponse.init(effectiveFrom: effectiveFrom, typeKey: typeKey, value: value))
                }
            }
            
            if preferenceListArray.count != 0 {
                let latestPreferenceArray = preferenceListArray.sorted(by: {$0.effectiveFrom > $1.effectiveFrom}).first
                
                if let referenceId = latestPreferenceArray?.value {
                    return referenceId
                }
            }
        }
        
        return nil
    }

    public struct GetPreferencesByPartyIdResponse   {
        
        var effectiveFrom: Date
        var typeKey: Int
        var value: String
        
        public init(effectiveFrom: String, typeKey: Int, value: String) {
            if let effectiveFromDate = Wire.default.yearMonthDayFormatter.date(from: effectiveFrom) {
                self.effectiveFrom = effectiveFromDate
            } else {
                self.effectiveFrom = Date.distantPast
            }
            self.typeKey = typeKey
            self.value = value
            
        }
    }
}

