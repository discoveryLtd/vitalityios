//
//  VDPLinkDevice.swift
//  VitalityActive
//
//  Created by Simon Stewart on 6/1/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireActivityLogger


// MARK: Router

public enum VDPLinkDeviceError: Error {
    case unknown
    case parser
}

enum VDPLinkDeviceRouter: URLRequestConvertible {
    static let basePath = "vdp-api"

    case post(tenantId: Int, request: VDPLinkDeviceRequest)
    case postSoftBankDevice(tenantId: Int, identifier: Int, partnerSystem: String, request: VDPLinkSoftBankDeviceRequest)

    var method: HTTPMethod {
        switch self {
        case .post:
            return .post
        case .postSoftBankDevice:
            return .post
        }
    }

    var route: String {
        switch self {
        case .post(let tenantId, _):
            return ("/devices/tenant/\(tenantId)")
        case .postSoftBankDevice(let tenantId, let identifier, let partnerSystem, _):
            return ("/devices/tenant/\(tenantId)/identifier/\(identifier)/partnerSystem/\(partnerSystem)")
        }
    }

    var parameters: Parameters {
        switch self {
        case .post(_, let request):
            return request.toJSON()
        case .postSoftBankDevice(_, _, _, let request):
            return request.toJSON()
        }
    }

    func asURLRequest() throws -> URLRequest {
        let url = try Wire.urlForPath(path: VDPLinkDeviceRouter.basePath).appendingPathComponent(self.route)
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = self.method.rawValue
        switch self {
        case .post:
            return try JSONEncoding.default.encode(urlRequest, with: self.parameters)
        case .postSoftBankDevice:
            return try JSONEncoding.default.encode(urlRequest, with: self.parameters)
        }
    }
}

public struct VDPLinkSoftBankDeviceRequest {
    var refreshToken: String
    var token: String
    var userId: String
    
    public init(refreshToken: String, token: String, userId: String) {
        self.refreshToken = refreshToken
        self.token = token
        self.userId = userId
    }
    
    public func toJSON() -> Parameters {
        var json = Parameters()
        json = [
            "refreshToken": self.refreshToken,
            "token": self.token,
            "userId": self.userId
        ]
        return json
    }
}

public class VDPLinkDeviceRequest {
    var userEmail: String
    var userIdentifier: String
    var userIdentifierType: String
    var partnerSystem: String
    var partnerDevice: String
    var partnerLinkUrl: String
    var partnerLinkMethod: String
    var partnerLinkedStatus: String
    var partnerLinkRedirectUrl: String
    var partnerRedirectUrl: String

    public init(userEmail: String, userIdentifier: String, userIdentifierType: String, partnerSystem: String,
                partnerDevice: String, partnerLinkUrl: String, partnerLinkMethod: String, partnerLinkedStatus: String, partnerLinkRedirectUrl: String, partnerRedirectUrl: String) {
        self.userEmail = userEmail
        self.userIdentifier = userIdentifier
        self.userIdentifierType = userIdentifierType
        self.partnerSystem = partnerSystem
        self.partnerDevice = partnerDevice
        self.partnerLinkUrl = partnerLinkUrl
        self.partnerLinkMethod = partnerLinkMethod
        self.partnerLinkedStatus = partnerLinkedStatus
        self.partnerLinkRedirectUrl = partnerLinkRedirectUrl
        self.partnerRedirectUrl = partnerRedirectUrl
    }

    public func toJSON() -> Parameters {
        var json = Parameters()
        json = [
            "partner": [
                "partnerSystem": self.partnerSystem,
                "device": self.partnerDevice,
                "redirectUrl": self.partnerRedirectUrl,
                "partnerLink": [
                    "url": self.partnerLinkUrl,
                    "method": self.partnerLinkMethod,
                    "redirectUrl": self.partnerLinkRedirectUrl
                ],
                "partnerLinkedStatus": self.partnerLinkedStatus,
            ],
            "user": [
                "email": self.userEmail,
                "identifierType": self.userIdentifierType,
                "userIdentifier": self.userIdentifier
            ],
        ]

        return json
    }
}
extension Wire.Events {
    
    public static func linkSoftBankDevice(tenantId: Int, identifier: Int, partnerSystem: String, request: VDPLinkSoftBankDeviceRequest, completion: @escaping (_ error: Error?) -> Void) {
        let url = VDPLinkDeviceRouter.postSoftBankDevice(tenantId: tenantId, identifier: identifier, partnerSystem: partnerSystem, request: request)
        
        Wire.sessionManager.request(url)
            .log()
            .validate(statusCode: 200 ..< 300)
            .apiManagerResponse { response in
                switch response.httpStatusCode {
                case 200 ..< 300:
                    completion(nil)
                default:
                    completion(response.result.error ?? BackendError.other)
                }
        }
    }
    
    public static func linkDevice(tenantId: Int, request: VDPLinkDeviceRequest, completion: @escaping (_ redirectLocation: String?, _ error: Error?) -> Void) {
        let url = VDPLinkDeviceRouter.post(tenantId: tenantId, request: request)
        /*
            1. Suggested way to use "redirectLocation" is to set up a URL scheme in the app and make redirectUrl something like vitalityapp://
            2. use:
                let safariVC = SFSafariViewController(url: URL(string: url)!)
                self.present(safariVC, animated: true, completion: nil)
            3. in AppDelegate do:
         
                func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool { // no equiv. notification. return NO if the application can't open for some reason
                    NotificationCenter.default.post(name: .test, object: url)
                    return true
                }
            4. raise appropriate notification and handle it in the Wire.Events.linkDevice calling code
            5. dismiss the SFSafariViewController
        */
        Wire.sessionManager.delegate.taskWillPerformHTTPRedirection = { session, task, response, request in
            let location = response.allHeaderFields["Location"] as? String
            Wire.sessionManager.delegate.taskWillPerformHTTPRedirection = nil // restore
            completion(location, nil)
            return nil
        }

        Wire.sessionManager.request(url)
            .log()
            .validate(statusCode: 200 ..< 300)
            .apiManagerResponse { response in
                switch response.httpStatusCode {
                default:
                    debugPrint("\(String(describing: response.result.error))")
                    completion(nil, response.result.error ?? BackendError.other)
                }
        }
    }
}
