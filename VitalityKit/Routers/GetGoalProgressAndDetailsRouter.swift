import Foundation
import Alamofire

// MARK: Router
enum GetGoalProgressAndDetailsRouter: URLRequestConvertible {

    case getGoalProgressAndDetails(tenantId: Int, request: GetGoalProgressAndDetailsParameters)

    static let basePath = "vitality-manage-goals-service-1"

    var method: HTTPMethod {
        switch self {
        case .getGoalProgressAndDetails(_, _):
            return .post
        }
    }

    var route: String {
        switch self {
        case .getGoalProgressAndDetails(let tenantId, _):
            return ("svc/\(tenantId)/getGoalProgressAndDetails")

        }
    }

    var parameters: Parameters? {
        switch self {
        case .getGoalProgressAndDetails(_, let params):
            return params.toJSON()
        }
    }

    // MARK: URLRequestConvertible

    func asURLRequest() throws -> URLRequest {
        let url = try Wire.urlForPath(path: GetGoalProgressAndDetailsRouter.basePath).appendingPathComponent(self.route)
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = self.method.rawValue

        switch self {
        case .getGoalProgressAndDetails:
            return try JSONEncoding.default.encode(urlRequest, with: self.parameters)
        }
    }
}

// MARK: Networking

extension Wire.Events {
    public static func getGoalProgressAndDetails(tenantId: Int, request: GetGoalProgressAndDetailsParameters, completion: @escaping (( _: Error?, _: GoalProgressDetailsResponse?) -> Void)) {
        Wire.sessionManager.request(GetGoalProgressAndDetailsRouter.getGoalProgressAndDetails(tenantId: tenantId, request: request))
            .log()
            .validate(statusCode: 200 ..< 300)
            .apiManagerResponse { response in
                switch response.httpStatusCode {
                case 200 ..< 300:

                    guard let result = GoalProgressDetailsResponse(json: response.jsonDictionary) else {
                        return completion(WireError.parsing, nil)
                    }

                    Parser.ARGoalProgressAndDetails.parseGoalProgressAndDetails(response: result, completion: { error in
                        completion(error, result)
                    })

                case 400:
                    return completion(response.result.error ?? BackendError.other, nil)
                default:
                    return completion(response.result.error ?? BackendError.other, nil) // TODO: sort out proper error handling
                }
        }
    }
    
    public static func getGDCGoalProgressAndDetails(tenantId: Int, request: GetGoalProgressAndDetailsParameters, completion: @escaping (( _: Error?, _: GetGoalProgressDetailsResponse?) -> Void)) {
        Wire.sessionManager.request(GetGoalProgressAndDetailsRouter.getGoalProgressAndDetails(tenantId: tenantId, request: request))
            .log()
            .validate(statusCode: 200 ..< 300)
            .apiManagerResponse { response in
                switch response.httpStatusCode {
                case 200 ..< 300:
                    
                    guard let result = GetGoalProgressDetailsResponse(json: response.jsonDictionary) else {
                        return completion(WireError.parsing, nil)
                    }
                    
                    Parser.GDCGetGoalProgressAndDetails.parseGoalProgressAndDetails(response: result, completion: { (error) in
                        completion(error, result)
                    })
                case 400:
                    return completion(response.result.error ?? BackendError.other, nil)
                default:
                    return completion(response.result.error ?? BackendError.other, nil) // TODO: sort out proper error handling
                }
        }
    }

}

// MARK: Structs

public struct GetGoalProgressAndDetailsParameters {

    public var effectiveDateFrom: Date?
    public var effectiveDateTo: Date?
    public var goalKeys: [Int]
    public var goalStatusTypeKeys: [Int]?
    public var partyId: Int

    public init(effectiveDateFrom: Date?, effectiveDateTo: Date?, goalKeys: [Int], goalStatusTypeKeys: [Int]?, partyId: Int) {
        self.effectiveDateFrom = effectiveDateFrom
        self.effectiveDateTo = effectiveDateTo
        self.goalKeys = goalKeys
        self.goalStatusTypeKeys = goalStatusTypeKeys
        self.partyId = partyId
    }
    public func toJSON() -> Parameters {

        var keys = [Parameters]()
        for goalKey in goalKeys {
            keys.append(["key": goalKey])
        }
        var typeKeys = [Parameters]()
        if let goalStatusKeys = goalStatusTypeKeys {
            for goalStatusKey in goalStatusKeys {
                typeKeys.append(["typeKey": goalStatusKey])
            }
        }

        if let effectiveFrom = effectiveDateFrom, let effectiveTo = effectiveDateTo {
            return [
                "getGoalProgressAndDetailsRequest": [
                    "effectiveDate": [
                        "effectiveFrom": Wire.default.simpleDateFormatter.string(from:effectiveFrom),
                        "effectiveTo": Wire.default.simpleDateFormatter.string(from:effectiveTo)
                    ],
                    "goal": keys,
                    "goalStatuses": typeKeys,
                    "partyId": partyId
                ]
            ]
        }

        return [
            "getGoalProgressAndDetailsRequest": [
                "goal": keys,
                "goalStatuses": typeKeys,
                "partyId": partyId
            ]
        ]

    }
}
