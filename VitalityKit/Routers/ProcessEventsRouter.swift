import Foundation
import Alamofire
import AlamofireActivityLogger
import VIAUtilities

// MARK: Router

enum ProcessEventsRouter: URLRequestConvertible {
    static let basePath = "vitality-manage-events-service-v1"

    case processEvents(tenantId: Int, events: [ProcessEventsEvent])

    var method: HTTPMethod {
        switch self {
        case .processEvents:
            return .post
        }
    }

    var route: String {
        switch self {
        case .processEvents(let tenantId, _):
            return ("svc/\(tenantId)/processEvents")
        }
    }

    var parameters: Parameters {
        switch self {
        case .processEvents(_, let events):
            let body = ProcessEventsParameters(events)
            return body.toJSON()
        }
    }

    func asURLRequest() throws -> URLRequest {
        let url = try Wire.urlForPath(path: ProcessEventsRouter.basePath).appendingPathComponent(self.route)
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = self.method.rawValue
        switch self {
        case .processEvents:
            return try JSONEncoding.default.encode(urlRequest, with: self.parameters)
        }
    }
}

extension Wire.Events {    
    public static func processEvents(tenantId: Int, events: [ProcessEventsEvent], completion: @escaping (_ response: ProcessEventsRootResponse?, _ error: Error?) -> Void) {
        let url = ProcessEventsRouter.processEvents(tenantId: tenantId, events: events)
        Wire.sessionManager.request(url)
            .log()
            .validate(statusCode: 200 ... 200)
            .apiManagerResponse { response in
                switch response.httpStatusCode {
                case 200:
                    guard let result = ProcessEventsRootResponse(json: response.jsonDictionary) else {
                        completion(nil, WireError.parsing)
                        return
                    }
                    completion(result, nil)
                default:
                    debugPrint("\(String(describing: response.result.error))")
                    completion(nil, response.result.error ?? BackendError.other)
                }
        }
    }
    public static func processEvents(tenantId: Int, partyId: Int, events: [EventTypeRef], completion: @escaping (_ error: Error?) -> Void) {

        var processEvents = [ProcessEventsEvent]()
        for event in events {
            let processEvent = ProcessEventsEvent(date: Date(),
                                                  eventTypeKey: event,
                                                  eventSourceTypeKey: .MobileApp,
                                                  partyId: partyId,
                                                  reportedBy: partyId,
                                                  associatedEvents: [ProcessEventsAssociatedEvent](),
                                                  eventMetaDataTypeKey: nil,
                                                  eventMetaDataUnitOfMeasure: nil,
                                                  eventMetaDataValues: nil)


            processEvents.append(processEvent)
        }

        Wire.Events.processEvents(tenantId: tenantId, events: processEvents) { (response, error) in
            completion(error)
        }


    }

}

public struct ProcessEventsAssociatedEvent {
    var eventOccuredOn: Date
    var eventCapturedOn: Date
    var associatedEventTypeKey: EventAssociationTypeRef
    var eventMetaDataUnitOfMeasure: UnitOfMeasureRef?
    var associatedEventMetadatasTypeKey: EventMetaDataTypeRef
    var associatedEventMetadatasValue: String
    var eventSourceTypeKey: EventSourceRef
    var partyId: Int
    var reportedBy: Int
    var eventId: Int?
    var typeKey: EventTypeRef
    public init(eventOccuredOn: Date,
                eventCapturedOn: Date,
                eventMetaDataTypeKey: EventMetaDataTypeRef,
                eventMetaDataUnitOfMeasure: UnitOfMeasureRef? = nil,
                eventMetaDataValue: String,
                associatedEventTypeKey: EventAssociationTypeRef,
                eventSourceTypeKey: EventSourceRef,
                partyId: Int,
                reportedBy: Int,
                eventId: Int?,
                typeKey: EventTypeRef) {
        self.eventOccuredOn = eventOccuredOn
        self.eventCapturedOn = eventCapturedOn
        self.associatedEventTypeKey = associatedEventTypeKey
        self.eventMetaDataUnitOfMeasure = eventMetaDataUnitOfMeasure
        self.associatedEventMetadatasTypeKey = eventMetaDataTypeKey
        self.eventSourceTypeKey = eventSourceTypeKey
        self.associatedEventMetadatasValue = eventMetaDataValue
        self.partyId = partyId
        self.reportedBy = reportedBy
        self.eventId = eventId
        self.typeKey = typeKey
    }

}

public struct ProcessEventsEvent {
    var date: Date
    var eventMetaDataTypeKey: EventMetaDataTypeRef?
    var eventMetaDataUnitOfMeasure: UnitOfMeasureRef?
    var eventMetaDataValues: Array<String>?
    var eventTypeKey: EventTypeRef
    var eventSourceTypeKey: EventSourceRef
    var partyId: Int
    var reportedBy: Int

    var associatedEvents: [ProcessEventsAssociatedEvent]

    public init(date: Date,
                eventTypeKey: EventTypeRef,
                eventSourceTypeKey: EventSourceRef, partyId: Int,
                reportedBy: Int, associatedEvents: [ProcessEventsAssociatedEvent], eventMetaDataTypeKey: EventMetaDataTypeRef?, eventMetaDataUnitOfMeasure: UnitOfMeasureRef?, eventMetaDataValues: Array<String>?) {
        self.date = date
        self.eventMetaDataTypeKey = eventMetaDataTypeKey
        self.eventMetaDataUnitOfMeasure = eventMetaDataUnitOfMeasure
        self.eventMetaDataValues = eventMetaDataValues
        self.eventTypeKey = eventTypeKey
        self.eventSourceTypeKey = eventSourceTypeKey
        self.partyId = partyId
        self.reportedBy = reportedBy
        self.associatedEvents = associatedEvents
    }
}

public struct ProcessEventsParameters {

    var events: [ProcessEventsEvent]
    public init(_ events: [ProcessEventsEvent]) {
        self.events = events
    }

    private func createAssociatedEvent(_ associatedEvent: ProcessEventsAssociatedEvent) -> Parameters {

        var formattedOccuredOnDate = Wire.getFormattedDateWithTimezone(date: associatedEvent.eventOccuredOn)
        var formattedCapturedOnDate = Wire.getFormattedDateWithTimezone(date: associatedEvent.eventCapturedOn)
        
        var associatedEventMetadatas = [
            [
                "typeKey": associatedEvent.associatedEventMetadatasTypeKey.rawValue,
                "value": associatedEvent.associatedEventMetadatasValue
            ]
        ]
        // if there is a UOM, assign it and redo the array of metadatas
        if let uom = associatedEvent.eventMetaDataUnitOfMeasure?.rawValue, uom != UnitOfMeasureRef.Unknown.rawValue, var first = associatedEventMetadatas.first {
            first["unitOfMeasure"] = "\(uom)"
            associatedEventMetadatas = [first]
        }
        var associatedEventParameters = [
            "eventOccuredOn": formattedOccuredOnDate,
            "eventAssociationTypeKey": associatedEvent.associatedEventTypeKey.rawValue,
            "typeKey": associatedEvent.typeKey.rawValue,
            "eventSourceTypeKey": associatedEvent.eventSourceTypeKey.rawValue,
            "eventCapturedOn": formattedCapturedOnDate,
            "applicableTo": associatedEvent.partyId,
            "reportedBy": associatedEvent.reportedBy
            ] as [String : Any]
        
        
        if .Unknown != associatedEvent.associatedEventMetadatasTypeKey{
            associatedEventParameters["associatedEventMetadatas"] = associatedEventMetadatas
        }
        
        
        if (associatedEvent.eventId != nil) {
            associatedEventParameters["eventId"] = associatedEvent.eventId
        }

        return associatedEventParameters
    }

    private func createEvent(_ event: ProcessEventsEvent) -> Parameters {

        let formattedDate = Wire.getFormattedDateWithTimezone(date: event.date)

        var associatedEvents = [Parameters]()
        for associatedEvent in event.associatedEvents {
            associatedEvents.append(createAssociatedEvent(associatedEvent))
        }
        var eventMetaDatas: Array<[String : Any]> = Array<[String : Any]>()
        if let eventValues = event.eventMetaDataValues {
            for eventMetaDataValue in eventValues {
                let eventMetaData: [String : Any] = [
                    "typeKey": event.eventMetaDataTypeKey?.rawValue ?? EventMetaDataTypeRef.DocumentReference.rawValue,
                    "value": eventMetaDataValue
                ]
                eventMetaDatas.append(eventMetaData)
            }
        }
        return [
            "eventOccuredOn": formattedDate,
            "eventMetaData": eventMetaDatas,
            "typeKey": event.eventTypeKey.rawValue,
            "eventSourceTypeKey": event.eventSourceTypeKey.rawValue,
            "eventCapturedOn": formattedDate,
            "applicableTo": event.partyId,
            "reportedBy": event.reportedBy,
            "associatedEvent": associatedEvents
        ]
    }


    public func toJSON() -> Parameters {

        var events = [Parameters]()
        for event in self.events {
            events.append(createEvent(event))
        }
//        print("----> Events:\(events)")
        return [
            "processEventsRequest": [
                "eventIn": events
            ]
        ]
    }
}
