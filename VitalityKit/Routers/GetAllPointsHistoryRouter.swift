//
//  GetAllPointsHistoryRouter.swift
//  VitalityActive
//
//  Created by Simon Stewart on 1/31/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireActivityLogger


public enum PointsPeriod: Int {
    case current = 0
    case previous = -1
    case currentAndPrevious = 2
}

// MARK: Router

enum GetAllPointsHistoryRouter: URLRequestConvertible {
    static let basePath = "vitality-goals-points-services-service-v1"

    case getAllPointsHistory(tenantId: Int, vitalityMembershipId: Int, pointsPeriod: PointsPeriod)

    var method: HTTPMethod {
        switch self {
        case .getAllPointsHistory:
            return .post
        }
    }

    var route: String {
        switch self {
        case .getAllPointsHistory(let tenantId, let vitalityMembershipId, _):
            return ("svc/\(tenantId)/getAllPointsHistory/\(vitalityMembershipId)")
        }
    }

    var parameters: Parameters {
        switch self {
        case .getAllPointsHistory(_, _, let pointsPeriod):
            let body = GetAllPointsHistoryParameters(pointsPeriod: pointsPeriod)
            return body.toJSON()
        }
    }

    func asURLRequest() throws -> URLRequest {
        let url = try Wire.urlForPath(path: GetAllPointsHistoryRouter.basePath).appendingPathComponent(self.route)
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = self.method.rawValue
        switch self {
        case .getAllPointsHistory:
            return try JSONEncoding.default.encode(urlRequest, with: self.parameters)
        }
    }
}

extension Wire.Member {
    public static func getAllPointsHistory(tenantId: Int, vitalityMembershipId: Int, pointsPeriod: PointsPeriod, completion: @escaping (_ result: Any?, _ error: Error?, _ useCustomError: Bool?) -> Void) {
        let url = GetAllPointsHistoryRouter.getAllPointsHistory(tenantId: tenantId, vitalityMembershipId: vitalityMembershipId, pointsPeriod: pointsPeriod)
        Wire.sessionManager.request(url)
            .log()
            .validate(statusCode: 200 ..< 300)
            .apiManagerResponse { response in
                switch response.httpStatusCode {
                case 200 ..< 300:
                    guard let result = GetAllPointsHistoryResponse(json: response.jsonDictionary) else {
                        completion(nil, WireError.parsing, nil)
                        return
                    }
                    Parser.PointsMonitor.parsePointsHistory(response: result, completion: completion)
                case -99999:
                    completion(nil, response.result.error ?? BackendError.other, false)
                default:
                    debugPrint("\(String(describing: response.result.error))")
                    completion(nil, response.result.error ?? BackendError.other, true)
                }
        }
    }
}

public struct GetAllPointsHistoryParameters {

    private var pointsPeriod: PointsPeriod

    public init(pointsPeriod: PointsPeriod) {
        self.pointsPeriod = pointsPeriod
    }

    public func toJSON() -> Parameters {

        var periodOffsetSubDoc: Any
        switch self.pointsPeriod {
        case .current, .previous:
            periodOffsetSubDoc = [[ "value": self.pointsPeriod.rawValue ]]
        case .currentAndPrevious:
            periodOffsetSubDoc = [["value": PointsPeriod.current.rawValue], ["value": PointsPeriod.previous.rawValue]]
        }

        // TODO: Return the value
        return [ "getAllPointsHistoryRequest": [ "pointsPeriodOffsets": periodOffsetSubDoc ] ]
    }
}
