//
//  GetAppUpdateRouter.swift
//  VitalityKit
//
//  Created by OJ Garde on 8/9/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import Alamofire
import VIAUtilities


enum GetAppUpdateRouter: URLRequestConvertible {
    static let basePath = "vitality-application-configuration-service-1/"
    
    case getAppUpdate(tenantId: Int)
    
    var method: HTTPMethod {
        switch self {
        case .getAppUpdate:
            return .get
        }
    }
    
    var route: String {
        switch self {
        case .getAppUpdate(let tenantId):
            return ("svc/\(tenantId)/checkAppVersion")
        }
    }
    
    
    func asURLRequest() throws -> URLRequest {
        let url = try Wire.urlForPath(path: GetAppUpdateRouter.basePath).appendingPathComponent(self.route)
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = self.method.rawValue
        switch self {
        case .getAppUpdate:
            return try JSONEncoding.default.encode(urlRequest, with: nil)
        }
    }
}

extension Wire.Update {
    
    public static func getAppUpdate(tenantId: Int, completion: @escaping (_ error: Error?, _ upgradeUrl: String?) -> Void) {
        
        let url = GetAppUpdateRouter.getAppUpdate(tenantId: tenantId)
        Wire.sessionManager.request(url)
            .log()
            .validate(statusCode: 200 ..< 300)
            .apiManagerResponse { response in
                switch response.httpStatusCode {
                case 200 ..< 300:
                    guard let result = GetAppUpdateResponse(json: response.jsonDictionary) else {
                        return completion(WireError.parsing, nil)
                    }
                    completion(nil, result.checkAppVersionResponse?.upgradeUrl)
                default:
                    debugPrint("\(String(describing: response.result.error))")
                    completion(response.result.error ?? BackendError.other, nil)
                }
        }
    }
    
}
