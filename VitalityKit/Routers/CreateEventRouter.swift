import Foundation
import Alamofire

import VIAUtilities

// MARK: Errors

public enum CreateEventError: Error {
    case parameters
}

extension Wire.Events {
    public static func createEvents(tenantId: Int, partyId: Int, events: [EventTypeRef], completion: @escaping ((_: Error?) -> Void)) {

        // validate for at least 1 event
        guard events.count > 0 else {
            return completion(CreateEventError.parameters)
        }

        Wire.Events.processEvents(tenantId: tenantId, partyId: partyId, events: events) { (error) in
            completion(error)
        }

    }
}
