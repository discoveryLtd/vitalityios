//
//  PartnerRedirectRouter.swift
//  VitalityKit
//
//  Created by Val Tomol on 21/03/2019.
//  Copyright © 2019 Glucode. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireActivityLogger

// MARK: Router

enum PartnerRedirectRouter: URLRequestConvertible {
    static let basePath = "tstc-integration-platform-services-service-v1"

    case post(tenantId: Int, request: PartnerRedirectParameters)
    
    var method: HTTPMethod {
        switch self {
        case .post:
            return .post
        }
    }
    
    var route: String {
        switch self {
        case .post(let tenantId, _):
            return ("\(tenantId)/partnerRedirect")
        }
    }
    
    var parameters: Parameters {
        switch self {
        case .post(_, let request):
            return request.toJSON()
        }
    }
    
    func asURLRequest() throws -> URLRequest {
        let url = try Wire.urlForPath(path: PartnerRedirectRouter.basePath).appendingPathComponent(self.route)
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = self.method.rawValue
        switch self {
        case .post:
            return try JSONEncoding.default.encode(urlRequest, with: self.parameters)
        }
    }
}

public class PartnerRedirectParameters {
    var partnerRedirectUrl: String
    var partyReferenceKey: String
    var partyReferenceValue: String
    
    public init(partnerRedirectUrl: String,
                partyReferenceKey: String,
                partyReferenceValue: String) {
        self.partnerRedirectUrl = partnerRedirectUrl
        self.partyReferenceKey = partyReferenceKey
        self.partyReferenceValue = partyReferenceValue
    }
    
    public func toJSON() -> Parameters {
        var json = Parameters()
        json = [
            "partnerRedirectURL": self.partnerRedirectUrl,
            "references": [
                [
                    "key": self.partyReferenceKey,
                    "value": self.partyReferenceValue
                ]
            ]
        ]
        
        return json
    }
}

extension Wire.Party {
    public static func partnerRedirect(tenantId: Int, request: PartnerRedirectParameters, completion: @escaping (_ redirectLocation: String?, _ commonAuthId: String?, _ error: Error?) -> Void) {
        let url = PartnerRedirectRouter.post(tenantId: tenantId, request: request)
        
        Wire.sessionManager.delegate.taskWillPerformHTTPRedirection = { session, task, response, request in
            let location = response.allHeaderFields["Location"] as? String
            var commonAuthId: String = ""
            
            for cookie in HTTPCookieStorage.shared.cookies! {
                if cookie.name == "commonAuthId" {
                    commonAuthId = "\(cookie.name)=\(cookie.value)"
                }
            }
            
            Wire.sessionManager.delegate.taskWillPerformHTTPRedirection = nil // Restore
            completion(location, commonAuthId, nil)
            return nil
        }
        
        Wire.sessionManager.request(url)
            .log()
            .validate(statusCode: 200 ..< 300)
            .apiManagerResponse { response in
                switch response.httpStatusCode {
                default:
                    debugPrint("\(String(describing: response.result.error))")
                    completion(nil, nil, response.result.error ?? BackendError.other)
                }
        }
    }
}
