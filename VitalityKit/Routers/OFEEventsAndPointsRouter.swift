//
//  OFEGetEventPointsRouter.swift
//  VitalityActive
//
//  Created by Harry C. Lingad on 4/16/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireActivityLogger


// MARK: Router

enum OFEEventsAndPointsRouter: URLRequestConvertible {
    static let basePath = "vitality-event-points-services-service-1"
    
    case getEventPointsHistory(tenantId: Int, partyId: Int, effectiveFrom: String, effectiveTo: String, typeKeys: [Int])
    
    var method: HTTPMethod {
        switch self {
        case .getEventPointsHistory:
            return .post
        }
    }
    
    var route: String {
        switch self {
        case .getEventPointsHistory(let tenantId, let partyId, _, _, _):
            return ("svc/\(tenantId)/getEventPoints/\(partyId)")
        }
    }
    
    var parameters: Parameters {
        switch self {
        case .getEventPointsHistory(_, _, let effectiveFrom, let effectiveTo, let typeKeys):
            let body = OFEHistoryGetEventPointsParameters(effectiveFrom: effectiveFrom, effectiveTo: effectiveTo, typeKeys: typeKeys)
            return body.toJSON()
        }
    }
    
    func asURLRequest() throws -> URLRequest {
        let url = try Wire.urlForPath(path: OFEEventsAndPointsRouter.basePath).appendingPathComponent(self.route)
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = self.method.rawValue
        switch self {
        case .getEventPointsHistory:
            return try JSONEncoding.default.encode(urlRequest, with: self.parameters)
        }
    }
}

extension Wire.Member {
    
    // API
    public static func getEventsPoints(tenantId: Int, partyId: Int, effectiveFrom: String, effectiveTo: String, typeKeys: [Int], completion: @escaping (_ result: Any?, _ error: Error?, _ useCustomError: Bool?) -> Void){
        let url = OFEEventsAndPointsRouter.getEventPointsHistory(tenantId: tenantId, partyId: partyId, effectiveFrom: effectiveFrom, effectiveTo: effectiveTo, typeKeys: typeKeys)
        
        Wire.sessionManager.request(url)
            .log()
            .validate(statusCode: 200 ..< 300)
            .apiManagerResponse { response in
                switch response.httpStatusCode {
                case 200 ..< 300:
                    
                    guard let result = OFEGetEventsPointsResponse(json: response.jsonDictionary) else {
                        print("Error")
                        completion(nil, WireError.parsing, nil)
                        return
                    }
                    
                    Parser.EventsAndPoints.parseEventsPointsHistory(response: result, completion: completion)
                    
                case -99999:
                    completion(nil, response.result.error ?? BackendError.other, false)
                default:
                    debugPrint("\(String(describing: response.result.error))")
                    completion(nil, response.result.error ?? BackendError.other, true)
                }
        }
        
        
    }

    
//    public static func getEventPointsHistoryMock(tenantId: Int, partyId: Int, effectiveFrom: String, effectiveTo: String, completion: @escaping (_ result: Any?, _ error: Error?, _ useCustomError: Bool?) -> Void) {
//        
//                if let path = Bundle.main.url(forResource: "pointsEvents", withExtension: "json"){
//                    if let response = parseJsonFile(url: path){
//                        print(response)
//                        guard let result = OFEGetAgreementPeriodResponse(json: response) else {
//                            return completion(WireError.parsing, nil, false)
//                        }
//                        print(result)
//                        
//                        //Parser.EventsPoints.parseEventsPointsHistory(response: result, completion: completion)
//                    }
//                }else{
//                    print("no file")
//                }
//                return completion(nil, nil, false)
//    }
    
    
    
    
}



//------------------------------------------

public struct OFEHistoryGetEventPointsParameters {
    
    private var effectiveFrom: String
    private var effectiveTo: String
    private var typeKeys = [Int]()
    
    public init(effectiveFrom: String, effectiveTo: String, typeKeys: [Int]) {
        self.effectiveFrom = effectiveFrom
        self.effectiveTo = effectiveTo
        self.typeKeys = typeKeys
    }
    
    public func toJSON() -> Parameters {
        
        var eventTypeKeys = [Any]()
        
        for typeKey in typeKeys{
            eventTypeKeys.append([ "typeKey": typeKey])
        }
        
        return [ "effectivePeriod": [ "effectiveFrom": effectiveFrom, "effectiveTo": effectiveTo ],
                 "event": eventTypeKeys]
    }
    
    
    
}
