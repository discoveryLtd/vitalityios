// THIS IS NOT CURRENTLY NEEDED IN THE APP
////
////  GetUserInstructionRouter.swift
////  VitalityActive
////
////  Created by Simon Stewart on 2/9/17.
////  Copyright © 2017 Glucode. All rights reserved.
////
//
//import Foundation
//import Alamofire
//import AlamofireActivityLogger
//
//
//// MARK: Router
//
//enum CreateUserInstructionRouter: URLRequestConvertible {
//    static let basePath = "vitality-manage-user-service-v1"
//    
//    case createUserInstruction(userId: String, effectiveFrom: String, type: String)
//    
//    var method: HTTPMethod {
//        switch self {
//        case .createUserInstruction:
//            return .post
//        }
//    }
//    
//    var route: String {
//        switch self {
//        case .createUserInstruction:
//            let tenantId = InsurerConfiguration.TEMPORARY_TENANTID_PLEASE_REMOVE_BEFORE_ENTERING_ANYTHING_RESEMBLING_PRODUCTION
//            return ("\(tenantId)/v1/createUserInstruction")
//        }
//    }
//    
//    var parameters: Parameters {
//        switch self {
//        case .createUserInstruction(let userId, let effectiveFrom, let type):
//            let body = CreateUserInstructionParameters(userId: userId, effectiveFrom: effectiveFrom, type: type)
//            return body.toJSON()
//        }
//    }
//    
//    func asURLRequest() throws -> URLRequest {
//        let url = try Wire.urlForPath(path: CreateUserInstructionRouter.basePath, includeTenantId: false).appendingPathComponent(self.route)
//        var urlRequest = URLRequest(url: url)
//        urlRequest.httpMethod = self.method.rawValue
//        switch self {
//        case .createUserInstruction:
//            return try JSONEncoding.default.encode(urlRequest, with: self.parameters)
//        }
//    }
//}
//
//extension Wire.Member {
//    public static func createUserInstruction(userId: String, effectiveFrom: String, type: String, completion: @escaping (_ result: Any?, _ error: Error?) -> Void) {
//        let url = CreateUserInstructionRouter.createUserInstruction(userId: userId, effectiveFrom: effectiveFrom, type: type)
//        Wire.sessionManager.request(url)
//            .log()
//            .validate(statusCode: 201 ... 201)
//            .apiManagerResponse  { response in
//                switch response.httpStatusCode {
//                case 200 ... 201:
//                    completion(nil, nil) // no response given
//                default:
//                    debugPrint("\(response.result.error)")
//                    completion(nil, response.result.error)
//                }
//        }
//    }
//}
//
//public struct CreateUserInstructionParameters {
//    
//    private var userId: String
//    private var effectiveFrom: String
//    private var type: String
//    
//    public init(userId: String, effectiveFrom: String, type: String) {
//        self.userId = userId
//        self.effectiveFrom = effectiveFrom
//        self.type = type
//    }
//    
//    public func toJSON() -> Parameters {
//        return [ "instructions" :
//            [
//                ["effectiveFrom": effectiveFrom, "type": type]
//            ],
//            "userId" : userId
//        ]
//    }
//}
