//
//  VDPGetDevices.swift
//  VitalityActive
//
//  Created by Simon Stewart on 6/1/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireActivityLogger


// MARK: Router

enum VDPGetDevicesRouter: URLRequestConvertible {
    static let basePath = "vdp-api"

    case get(tenantId: Int, partyId: Int)

    var method: HTTPMethod {
        switch self {
        case .get:
            return .get
        }
    }

    var route: String {
        switch self {
        case .get(let tenantId, let partyId):
            return ("/devices/tenant/\(tenantId)/identifier/\(partyId)")
        }
    }

    func asURLRequest() throws -> URLRequest {
        let url = try Wire.urlForPath(path: VDPGetDevicesRouter.basePath).appendingPathComponent(self.route)
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = self.method.rawValue
        switch self {
        case .get:
            return try JSONEncoding.default.encode(urlRequest, with: nil)
        }
    }
}

extension Wire.Events {
    public static func getDevices(tenantId: Int, partyId: Int, completion: @escaping (_ response: VDPGetDevicesResponse?, _ error: Error?) -> Void) {
        let url = VDPGetDevicesRouter.get(tenantId: tenantId, partyId: partyId)
        Wire.sessionManager.request(url)
            .log()
            .validate(statusCode: 200 ..< 300)
            .apiManagerResponse { response in
                switch response.httpStatusCode {
                case 200 ..< 300:
                    guard let result = VDPGetDevicesResponse(json: response.jsonDictionary) else {
                        return completion(nil, WireError.parsing)
                    }
                    Parser.WellnessDevicesAndApps.parseGetDevices(response: result, completion: completion)
                    return
                default:
                    debugPrint("\(String(describing: response.result.error))")
                    completion(nil, response.result.error ?? BackendError.other)
                    return
                }
        }
    }
}
