//
//  CaptureAssessment.swift
//  VitalityActive
//
//  Created by Simon Stewart on 6/1/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireActivityLogger
import RealmSwift


// MARK: Router

enum CaptureAssessmentRouter: URLRequestConvertible {
    static let basePath = "vitality-manage-assessments-service-1"

    case capture(tenantId: Int, request: CaptureAssessmentRequest)

    var method: HTTPMethod {
        switch self {
        case .capture:
            return .post
        }
    }

    var route: String {
        switch self {
        case .capture(let tenantId, let request):
            return ("svc/\(tenantId)/captureAssessment/\(request.partyId)/\(request.questionnaireSetTypeKey)")
        }
    }

    var parameters: Parameters {
        switch self {
        case .capture(_, let request):
            let body = CaptureAssessmentParameters(request)
            return body.toJSON()
        }
    }

    func asURLRequest() throws -> URLRequest {
        let url = try Wire.urlForPath(path: CaptureAssessmentRouter.basePath).appendingPathComponent(self.route)
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = self.method.rawValue
        switch self {
        case .capture:
            return try JSONEncoding.default.encode(urlRequest, with: self.parameters)
        }
    }
}

extension Wire.Events {
    public static func captureAssessment(tenantId: Int, request: CaptureAssessmentRequest, completion: @escaping (_ response: GMIReportedHealthInformation?, _ error: Error?) -> Void) {
        let url = CaptureAssessmentRouter.capture(tenantId: tenantId, request: request)
        Wire.sessionManager.request(url)
            .log()
            .validate(statusCode: 200 ..< 300)
            .apiManagerResponse { response in
                switch response.httpStatusCode {
                case 200 ..< 300:
                    guard let result = CaptureAssessmentResponse(json: response.jsonDictionary) else {
                        completion(nil, WireError.parsing)
                        return
                    }
                    Wire.MyHealth.shared().healthInformationFor(isSummary: false, tenantId: tenantId, partyId: request.partyId, completion: completion)
                    //completion(HAReportedHealthAttribute.buildHealthAttributes(response: result), nil)
                default:
                    debugPrint("\(String(describing: response.result.error))")
                    completion(nil, response.result.error ?? BackendError.other)
                }
        }
    }
}

public class CaptureAssessmentParameters {
    var request: CaptureAssessmentRequest
    public init(_ request: CaptureAssessmentRequest) {
        self.request = request
    }

    public func toJSON() -> Parameters {
        return self.request.toJSON()
    }
}
public class CaptureAssessmentRequest {
    var assessment: [Assessment]
    var partyId: Int
    var questionnaireSetTypeKey: Int

    public init(assessment: [Assessment], partyId: Int, questionnaireSetTypeKey: Int) {
        self.assessment = assessment
        self.partyId = partyId
        self.questionnaireSetTypeKey = questionnaireSetTypeKey
    }
    public func toJSON() -> Parameters {

        var assessments = [Parameters]()
        for val in self.assessment {
            assessments.append(val.toJSON())
        }
        return [
            "assessment": assessments
        ]
    }
}
public class Assessment {
    var assessmentAnswer: [AssessmentAnswer]
    var statusTypeKey: Int // This key represents a New capture assessment request
    var typeKey: Int // The key of the questionnaire

    public init(assessmentAnswer: [AssessmentAnswer], statusTypeKey: Int, typeKey: Int) {
        self.assessmentAnswer = assessmentAnswer
        self.statusTypeKey = statusTypeKey
        self.typeKey = typeKey
    }

    public func toJSON() -> Parameters {

        var answers = [Parameters]()
        for val in self.assessmentAnswer {
            answers.append(val.toJSON())
        }
        return [
            "assessmentAnswer": answers,
            "statusTypeKey": self.statusTypeKey,
            "typeKey": self.typeKey
        ]
    }
}

public class AssessmentAnswer {
    var answerStatusTypeKey: Int // This key represents a Answered status
    public private(set) var questionTypeKey: Int // The question key
    var selectedValues: [SelectedValues]

    public init(answerStatusTypeKey: Int, questionTypeKey: Int, selectedValues: [SelectedValues]) {
        self.answerStatusTypeKey = answerStatusTypeKey
        self.questionTypeKey = questionTypeKey
        self.selectedValues = selectedValues
    }

    public func toJSON() -> Parameters {

        var selVals = [Parameters]()
        for val in self.selectedValues {
            selVals.append(val.toJSON())
        }
        return [
            "answerStatusTypeKey": self.answerStatusTypeKey,
            "selectedValues": selVals,
            "questionTypeKey": self.questionTypeKey
        ]
    }
}

public class SelectedValues {
    var value: String // The users answer
    var valueType: Int // The question type key
    var fromValue: String? // This field is optional, would only be populated if the question required a range to be provided
    var toValue: String? // This field is optional, would only be populated if the question required a range to be provided
    var unitOfMeasure: String? // This field is optional, only to be populated with the UOD id if the question specified it

    public init(value: String, valueType: Int, fromValue: String?, toValue: String?, unitOfMeasure: String?) {
        self.value = value
        self.valueType = valueType
        self.fromValue = fromValue
        self.toValue = toValue
        self.unitOfMeasure = unitOfMeasure
    }
    public func toJSON() -> Parameters {
        var json = [String: Any]()

        json["value"] = self.value
        json["valueType"] = self.valueType

        if let fromValue = self.fromValue {
            json["fromValue"] = fromValue
        }
        if let toValue = self.toValue {
            json["toValue"] = toValue
        }
        if self.unitOfMeasure != nil {
            json["unitOfMeasure"] = self.unitOfMeasure
        }
        return json
    }
}
