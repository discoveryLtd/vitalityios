//
//  UpdatePartyRouter.swift
//  VitalityActive
//
//  Created by Simon Stewart on 2/23/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireActivityLogger
import VIAUtilities

// MARK: Router

enum UpdatePartyRouter: URLRequestConvertible {
    static let basePath = "vitality-party-party-information-services-service-1"

    case updatePartyRouter(request: UpdatePartyRequest)

    var method: HTTPMethod {
        switch self {
        case .updatePartyRouter(_):
            return .post
        }
    }

    var route: String {
        switch self {
        case .updatePartyRouter(let request):
            let realm = DataProvider.newRealm()
            let tenantId = realm.getTenantId()
            return ("svc/\(tenantId)/updateParty/\(request.PartyId)") // TODO: partyId can also come out of Realm, but reduces testability
        }
    }

    var parameters: Parameters {
        switch self {
        case .updatePartyRouter(let request):
            let body = UpdatePartyParameters()
            return body.toJSON(request: request)
        }
    }

    func asURLRequest() throws -> URLRequest {
        let url = try Wire.urlForPath(path: UpdatePartyRouter.basePath).appendingPathComponent(self.route)
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = self.method.rawValue
        switch self {
        case .updatePartyRouter(_):
            return try JSONEncoding.default.encode(urlRequest, with: self.parameters)
        }
    }
}

extension Wire.Party {
    public static func update(request: UpdatePartyRequest, completion: @escaping (_ error: Error?) -> Void) {
        let url = UpdatePartyRouter.updatePartyRouter(request: request)
        Wire.sessionManager.request(url)
            .log()
            .validate(statusCode: 200 ..< 300)
            .apiManagerResponse { response in
                switch response.httpStatusCode {
                case 200 ..< 300:
                    completion(nil)
                default:
                    debugPrint("\(response.result)")
                    completion(response.result.error ?? BackendError.other)
                }
        }
    }
}

public class UpdatePartyRequest {
    var PartyId: Int
    public var webAddress: UpdatePartyWebAddress?
    public var geographicalAreaPreference: UpdatePartyGeographicalAreaPreference?
    public var languagePreference: UpdatePartyLanguagePreference?
    public var emailAddress: UpdatePartyEmailAddress?
    public var reference: UpdatePartyReference?
    public var generalPreference: UpdatePartyGeneralPreference?
    public var person: UpdatePartyPerson?
    public var measurementSystemPreference: UpdatePartyMeasurementSystemPreference?
    public var telephone: UpdatePartyTelephone?
    public var organisation: UpdatePartyOrganisation?
    public var physicalAddress: UpdatePartyPhysicalAddress?
    public var timeZonePreference: UpdatePartyTimeZonePreference?
    public init(partyId: Int) {
        self.PartyId = partyId
    }
    public init(partyId: Int, webAddress: UpdatePartyWebAddress?, geographicalAreaPreference: UpdatePartyGeographicalAreaPreference?, languagePreference: UpdatePartyLanguagePreference?, emailAddress: UpdatePartyEmailAddress?, reference: UpdatePartyReference?, generalPreference: UpdatePartyGeneralPreference?, person: UpdatePartyPerson?, measurementSystemPreference: UpdatePartyMeasurementSystemPreference?, telephone: UpdatePartyTelephone?, organisation: UpdatePartyOrganisation, physicalAddress: UpdatePartyPhysicalAddress?, timeZonePreference: UpdatePartyTimeZonePreference?) {
        self.PartyId = partyId
        self.webAddress = webAddress
        self.geographicalAreaPreference = geographicalAreaPreference
        self.languagePreference = languagePreference
        self.emailAddress = emailAddress
        self.reference = reference
        self.generalPreference = generalPreference
        self.person = person
        self.measurementSystemPreference = measurementSystemPreference
        self.telephone = telephone
        self.organisation = organisation
        self.physicalAddress = physicalAddress
        self.timeZonePreference = timeZonePreference
    }
}

public class UpdatePartyContactRole {
    var availabilityFrom: String // TODO: possibly date
    var availabilityTo: String // TODO: possibly date
    var effectiveFrom: Date
    var effectiveTo: Date
    var rolePurposeTypeKey: Int
    var roleTypeKey: Int

    public init(effectiveFrom: Date, effectiveTo: Date, availabilityFrom: String, availabilityTo: String, rolePurposeTypeKey: Int, roleTypeKey: Int) {
        self.effectiveFrom = effectiveFrom
        self.effectiveTo = effectiveTo
        self.availabilityFrom = availabilityFrom
        self.availabilityTo = availabilityTo
        self.rolePurposeTypeKey = rolePurposeTypeKey
        self.roleTypeKey = roleTypeKey
    }
}
public class UpdatePartyGeographicalAreaPreference {
    var effectiveFrom: Date
    var effectiveTo: Date
    var typeKey: Int
    var value: String

    public init(effectiveFrom: Date, effectiveTo: Date, typeKey: Int, value: String) {
        self.effectiveFrom = effectiveFrom
        self.effectiveTo = effectiveTo
        self.typeKey = typeKey
        self.value = value
    }
}
public class UpdatePartyReference {
    var effectiveFrom: Date
    var effectiveTo: Date
    var typeKey: Int
    var value: String
    var comments: String
    var issuedBy: Int

    public init(effectiveFrom: Date, effectiveTo: Date, typeKey: Int, value: String, comments: String, issuedBy: Int) {
        self.effectiveFrom = effectiveFrom
        self.effectiveTo = effectiveTo
        self.typeKey = typeKey
        self.value = value
        self.comments = comments
        self.issuedBy = issuedBy
    }
}
public class UpdatePartyGeneralPreference {
    var effectiveFrom: Date
    var effectiveTo: Date
    var typeKey: PreferenceTypeRef
    var value: String

    public init(effectiveFrom: Date, effectiveTo: Date, typeKey: PreferenceTypeRef, value: String) {
        self.effectiveFrom = effectiveFrom
        self.effectiveTo = effectiveTo
        self.typeKey = typeKey
        self.value = value
    }
}
public class UpdatePartyPerson {
    var middleNames: String
    var bornOn: Date
    var givenName: String
    var familyName: String
    var language: String
    var genderKey: Int
    var preferredName: String
    var suffix: String
    var titleKey: Int

    public init(middleNames: String, bornOn: Date, givenName: String, familyName: String, language: String, genderKey: Int, preferredName: String, suffix: String, titleKey: Int) {
        self.middleNames = middleNames
        self.bornOn = bornOn
        self.givenName = givenName
        self.familyName = familyName
        self.language = language
        self.genderKey = genderKey
        self.preferredName = preferredName
        self.suffix = suffix
        self.titleKey = titleKey
    }

}
public class UpdatePartyLanguagePreference {
    var ISOCode: String
    var value: String

    public init(ISOCode: String, value: String) {
        self.ISOCode = ISOCode
        self.value = value
    }
}
public class UpdatePartyMeasurementSystemPreference {
    var typeKey: Int

    public init(typeKey: Int) {
        self.typeKey = typeKey
    }
}
public class UpdatePartyOrganisation {
    var typeKey: Int
    var registeredDate: Date
    var tradingAs: String
    var name: String
    var shortName: String
    var suffix: String

    public init(typeKey: Int, registeredDate: Date, tradingAs: String, name: String, shortName: String, suffix: String) {
        self.typeKey = typeKey
        self.registeredDate = registeredDate
        self.tradingAs = tradingAs
        self.name = name
        self.shortName = shortName
        self.suffix = suffix

    }
}
public class UpdatePartyWebAddress {
    var url: String
    var contactRoles: UpdatePartyContactRole
    public init(url: String, contactRoles: UpdatePartyContactRole) {
        self.url = url
        self.contactRoles = contactRoles
    }
}
public class UpdatePartyEmailAddress {
    var value: String
    var contactRoles: UpdatePartyContactRole
    public init(value: String, contactRoles: UpdatePartyContactRole) {
        self.value = value
        self.contactRoles = contactRoles
    }
}
public class UpdatePartyPhysicalAddress {
    var country: String
    var POBox: Bool
    var complex: String
    var postalCode: Int // API uses int, but really should be string
    var streetAddress1: String
    var unitNumber: Int // API uses int, but really should be string
    var streetAddress2: String
    var streetAddress3: String
    var place: String
    var contactRoles: UpdatePartyContactRole
    public init(country: String, POBox: Bool, complex: String, postalCode: Int, streetAddress1: String, unitNumber: Int, streetAddress2: String, streetAddress3: String, place: String, contactRoles: UpdatePartyContactRole) {
        self.country = country
        self.POBox = POBox
        self.complex = complex
        self.postalCode = postalCode
        self.streetAddress1 = streetAddress1
        self.unitNumber = unitNumber
        self.streetAddress2 = streetAddress2
        self.streetAddress3 = streetAddress3
        self.place = place

        self.contactRoles = contactRoles
    }
}
public class UpdatePartyTelephone {
    var _extension: String
    var areaDialName: String
    var typeKey: Int
    var contactNumber: String
    var countryDialCode: String
    var areaDialCode: String

    var contactRoles: UpdatePartyContactRole
    public init(_extension: String, areaDialName: String, typeKey: Int, contactNumber: String, countryDialCode: String, areaDialCode: String, contactRoles: UpdatePartyContactRole) {
        self._extension = _extension
        self.areaDialName = areaDialName
        self.typeKey = typeKey
        self.contactNumber = contactNumber
        self.countryDialCode = countryDialCode
        self.areaDialCode = areaDialCode
        self.contactRoles = contactRoles
    }
}
public class UpdatePartyTimeZonePreference {
    var code: String
    var daylightSavings: Int
    var value: String

    public init(code: String, daylightSavings: Int, value: String) {
        self.code = code
        self.daylightSavings = daylightSavings
        self.value = value
    }
}
public struct UpdatePartyParameters {
    public func toJSON(request: UpdatePartyRequest) -> Parameters {

        var updatePartyRequest = Dictionary<String, Any>()

        if let item = request.webAddress {
            updatePartyRequest["webAddress"] = [UpdatePartyHelper.createWebAddress(item: item)]
        }
        if let item = request.geographicalAreaPreference {
            updatePartyRequest["geographicalAreaPreference"] = UpdatePartyHelper.createGeographicalPreferences(item: item)
        }
        if let item = request.languagePreference {
            updatePartyRequest["languagePreference"] = UpdatePartyHelper.createLanguagePreference(item: item)
        }
        if let item = request.emailAddress {
            updatePartyRequest["emailAddresses"] = [UpdatePartyHelper.createEmailAddress(item: item)]
        }
        if let item = request.reference {
            updatePartyRequest["references"] = [UpdatePartyHelper.createReference(item: item)]
        }
        if let item = request.generalPreference {
            updatePartyRequest["generalPreferences"] = [UpdatePartyHelper.createGeneralPreferences(item: item)]
        }
        if let item = request.person {
            updatePartyRequest["person"] = UpdatePartyHelper.createPerson(item: item)
        }
        if let item = request.measurementSystemPreference {
            updatePartyRequest["measurementSystemPreference"] = UpdatePartyHelper.createMeasurementSystemPreference(item: item)
        }
        if let item = request.telephone {
            updatePartyRequest["telephones"] = [UpdatePartyHelper.createTelephone(item: item)]
        }
        if let item = request.organisation {
            updatePartyRequest["organisation"] = UpdatePartyHelper.createOrganisation(item: item)
        }
        if let item = request.physicalAddress {
            updatePartyRequest["physicalAddresses"] = [UpdatePartyHelper.createPhysicalAddress(item: item)]
        }
        if let item = request.timeZonePreference {
            updatePartyRequest["timeZonePreference"] = UpdatePartyHelper.createTimeZonePreference(item: item)
        }

        var parameters = Dictionary<String, Any>()
        parameters["updatePartyRequest"] = updatePartyRequest
        return parameters

    }
}

public class UpdatePartyHelper {
    public static func createWebAddress(item: UpdatePartyWebAddress) -> [String:Any] {
        var webAddresses: [String:Any] = [:]
        webAddresses = [
            "uRL": item.url,
            "contactRoles": [
                [
                    "effectiveTo": Wire.default.yearMonthDayFormatter.string(from:  item.contactRoles.effectiveTo),
                    "rolePurposeTypeKey": item.contactRoles.rolePurposeTypeKey,
                    "availabilityFrom": item.contactRoles.availabilityFrom,
                    "availabilityTo": item.contactRoles.availabilityTo,
                    "roleTypeKey": item.contactRoles.roleTypeKey,
                    "effectiveFrom": Wire.default.yearMonthDayFormatter.string(from:  item.contactRoles.effectiveFrom)
                ]
            ]
        ]
        return webAddresses
    }
    public static func createGeographicalPreferences(item: UpdatePartyGeographicalAreaPreference) -> [String: Any] {
        var geographicalAreaPreference: [String:Any] = [:]
        geographicalAreaPreference = [
            "effectiveTo": Wire.default.yearMonthDayFormatter.string(from:  item.effectiveTo),
            "typeKey": item.typeKey,
            "value": "\(item.value)",
            "effectiveFrom": Wire.default.yearMonthDayFormatter.string(from:  item.effectiveFrom)
        ]
        return geographicalAreaPreference
    }
    public static func createLanguagePreference (item: UpdatePartyLanguagePreference) -> [String: Any] {
        var languagePreference: [String:Any] = [:]
        languagePreference = [
            "iSOCode": "\(item.ISOCode)",
            "value": "\(item.value)"
        ]
        return languagePreference
    }
    public static func createEmailAddress(item: UpdatePartyEmailAddress) -> [String: Any] {
        var emailAddresses: [String:Any] = [:]
        emailAddresses = [
            "value": "string",
            "contactRoles": [
                [
                    "effectiveTo": "\(item.contactRoles.effectiveTo)",
                    "rolePurposeTypeKey": item.contactRoles.rolePurposeTypeKey,
                    "availabilityFrom": "\(item.contactRoles.availabilityFrom)",
                    "availabilityTo": "\(item.contactRoles.availabilityTo)",
                    "roleTypeKey": item.contactRoles.roleTypeKey,
                    "effectiveFrom": "\(item.contactRoles.effectiveFrom)"
                ]
            ]
        ]
        return emailAddresses
    }
    public static func createReference(item: UpdatePartyReference) -> [String: Any] {
        var references: [String:Any] = [:]
        references = [
            "effectiveTo": Wire.default.yearMonthDayFormatter.string(from:  item.effectiveTo),
            "typeKey": item.typeKey,
            "comments": item.comments,
            "issuedBy": item.issuedBy,
            "value": item.value,
            "effectiveFrom": Wire.default.yearMonthDayFormatter.string(from:  item.effectiveFrom)
        ]
        return references
    }
    public static func createGeneralPreferences(item: UpdatePartyGeneralPreference) -> [String: Any] {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        
        var generalPreferences: [String:Any] = [:]
        generalPreferences = [
            "typeKey": item.typeKey.rawValue,
            "value": item.value,
            //"effectiveTo": Wire.default.yearMonthDayFormatter.string(from: item.effectiveTo),
            "effectiveTo": dateFormatter.string(from: item.effectiveTo),
            "effectiveFrom": Wire.default.yearMonthDayFormatter.string(from: item.effectiveFrom)
        ]

        return generalPreferences
    }
    public static func createPerson(item: UpdatePartyPerson) -> [String: Any] {
        var person: [String:Any] = [:]
        person = [
            "middleNames": item.middleNames,
            "bornOn": Wire.default.yearMonthDayFormatter.string(from:  item.bornOn),
            "givenName": item.givenName,
            "familyName": item.familyName,
            "language": item.language,
            "genderKey": item.genderKey,
            "preferredName": item.preferredName,
            "suffix": item.suffix,
            "titleKey": item.titleKey
        ]
        return person
    }
    public static func createMeasurementSystemPreference(item: UpdatePartyMeasurementSystemPreference) -> [String:Any] {
        var measurementSystemPreference: [String:Any] = [:]
        measurementSystemPreference = [
            "typeKey": item.typeKey
        ]
        return measurementSystemPreference
    }
    public static func createTelephone (item: UpdatePartyTelephone) -> [String: Any] {
        var telephones: [String:Any] = [:]
        telephones = [
            "extension": item._extension,
            "areaDialName": item.areaDialName,
            "typeKey": item.typeKey,
            "contactNumber": item.contactNumber,
            "countryDialCode": item.countryDialCode,
            "areaDialCode": item.areaDialCode,
            "contactRoles": [
                [
                    "effectiveTo": Wire.default.yearMonthDayFormatter.string(from:  item.contactRoles.effectiveTo),
                    "rolePurposeTypeKey": item.contactRoles.rolePurposeTypeKey,
                    "availabilityFrom": item.contactRoles.availabilityFrom,
                    "availabilityTo": item.contactRoles.availabilityTo,
                    "roleTypeKey": item.contactRoles.roleTypeKey,
                    "effectiveFrom": Wire.default.yearMonthDayFormatter.string(from:  item.contactRoles.effectiveFrom)
                ]
            ]
        ]
        return telephones
    }
    public static func createOrganisation(item: UpdatePartyOrganisation) -> [String:Any] {
        var organisation: [String:Any] = [:]
        organisation = [
            "typeKey": item.typeKey,
            "registeredDate": "\(item.registeredDate)",
            "tradingAs": item.tradingAs,
            "name": item.name,
            "shortName": item.shortName,
            "suffix": item.suffix
        ]
        return organisation
    }
    public static func createPhysicalAddress(item: UpdatePartyPhysicalAddress) -> [String:Any] {
        var physicalAddresses: [String:Any] = [:]
        physicalAddresses = [
            "country": item.country,
            "pOBox": item.POBox,
            "complex": item.complex,
            "postalCode": item.postalCode,
            "streetAddress1": item.streetAddress1,
            "unitNumber": item.unitNumber,
            "streetAddress2": item.streetAddress2,
            "streetAddress3": item.streetAddress3,
            "place": item.place,
            "contactRoles": [
                [
                    "effectiveTo": Wire.default.yearMonthDayFormatter.string(from:  item.contactRoles.effectiveTo),
                    "rolePurposeTypeKey": item.contactRoles.rolePurposeTypeKey ,
                    "availabilityFrom": item.contactRoles.availabilityFrom,
                    "availabilityTo": item.contactRoles.availabilityTo,
                    "roleTypeKey": item.contactRoles.roleTypeKey,
                    "effectiveFrom": Wire.default.yearMonthDayFormatter.string(from:  item.contactRoles.effectiveFrom)
                ]
            ]
        ]
        return physicalAddresses
    }
    public static func createTimeZonePreference(item: UpdatePartyTimeZonePreference) -> [String:Any] {
        var timeZonePreference: [String:Any] = [:]
        timeZonePreference = [
            "code": item.code,
            "daylightSavings": item.daylightSavings,
            "value": item.value
        ]
        return timeZonePreference
    }

}
