//
//  UpdatedRewardStatusRouter.swift
//  VitalityActive
//
//  Created by Wilmar van Heerden on 2017/11/22.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireActivityLogger
import VIAUtilities


// MARK: Router

enum UpdateRewardStatusRouter: URLRequestConvertible {
    static let basePath = "vitality-manage-rewards-service"
    
    case updateRewardStatus(tenantId: Int, parameters: UpdateRewardStatusParameters)
    case updateRewardAcknowledgedStatus(tenantId: Int, parameters: UpdateRewardAcknowledgedStatusParameters)
    
    var method: HTTPMethod {
        switch self {
        case .updateRewardStatus:
            return .post
            
        case .updateRewardAcknowledgedStatus:
            return .post
        }
    }
    
    var route: String {
        switch self {
        case .updateRewardStatus(let tenantId, _):
            return ("svc/\(tenantId)/updateRewardStatus")
        case .updateRewardAcknowledgedStatus(let tenantId, _):
            return ("svc/\(tenantId)/updateRewardStatus")
        }
    }
    
    var parameters: Parameters {
        switch self {
        case .updateRewardStatus(_, let params):
            return params.toJSON()
        case .updateRewardAcknowledgedStatus(_, let params):
            return params.toJSON()
        }
    }
    
    func asURLRequest() throws -> URLRequest {
        let url = try Wire.urlForPath(path: UpdateRewardStatusRouter.basePath).appendingPathComponent(self.route)
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = self.method.rawValue
        switch self {
        case .updateRewardStatus:
            return try JSONEncoding.default.encode(urlRequest, with: self.parameters)
        case .updateRewardAcknowledgedStatus:
            return try JSONEncoding.default.encode(urlRequest, with: self.parameters)
        }
    }
}

extension Wire.Rewards {
    
    public static func updateRewardStatus(tenantId: Int, partyId: Int, awardedRewardId: Int, status: AwardedRewardStatusRef, completion: @escaping (_ error: Error?, _ result: UpdateRewardStatusResponse?) -> Void) {
        let params = UpdateRewardStatusParameters(awardedRewardId: awardedRewardId, status: status, partyId: partyId)
        let url = UpdateRewardStatusRouter.updateRewardStatus(tenantId: tenantId, parameters: params)
        Wire.sessionManager.request(url)
            .log()
            .validate(statusCode: 200 ..< 300)
            .apiManagerResponse { response in
                switch response.httpStatusCode {
                case 200 ..< 300:
                    guard let result = UpdateRewardStatusResponse(json: response.jsonDictionary) else {
                        return completion(WireError.parsing, nil)
                    }
                    return completion(nil, result)
                default:
                    debugPrint("\(String(describing: response.result.error))")
                    completion(response.result.error ?? BackendError.other, nil)
                }
        }
    }
    
    public static func updateRewardAcknowledgedStatus(tenantId: Int, partyId: Int, awardedRewardId: Int, status: AwardedRewardStatusRef, completion: @escaping (_ error: Error?, _ result: UpdateRewardStatusResponse?) -> Void) {
        let params = UpdateRewardAcknowledgedStatusParameters(awardedRewardId: awardedRewardId, status: status, partyId: partyId)
        let url = UpdateRewardStatusRouter.updateRewardAcknowledgedStatus(tenantId: tenantId, parameters: params)
        Wire.sessionManager.request(url)
            .log()
            .validate(statusCode: 200 ..< 300)
            .apiManagerResponse { response in
                switch response.httpStatusCode {
                case 200 ..< 300:
                    guard let result = UpdateRewardStatusResponse(json: response.jsonDictionary) else {
                        return completion(WireError.parsing, nil)
                    }
                    return completion(nil, result)
                default:
                    debugPrint("\(String(describing: response.result.error))")
                    completion(response.result.error ?? BackendError.other, nil)
                }
        }
    }
}

public struct UpdateRewardAcknowledgedStatusParameters {
    
    private var awardedRewardId: Int
    private var partyId: Int
    private var status: AwardedRewardStatusRef
    
    public init(awardedRewardId: Int, status: AwardedRewardStatusRef, partyId: Int) {
        self.awardedRewardId = awardedRewardId
        self.partyId = partyId
        self.status = status
    }
    
    public func toJSON() -> Parameters {
        return [
            
            "awardedRewardId": awardedRewardId,
            "awardedRewardStatusKey": status.rawValue,
            "changedBy": [
                "referenceTypeKey": ResolveReferenceTypeRef.PartyID.rawValue,
                "referenceValue": partyId
            ],
        ]
        
        /*"awardedRewardId": ,
         "awardedRewardStatusKey": ,
         "effectiveOn": ,
         "rewardExpiryDate":
         "changedBy": [
         "referenceTypeKey": ResolveReferenceTypeRef.PartyID.rawValue,
         "referenceValue": "\(partyId)"
         
         
         ],*/
    }
}

public struct UpdateRewardStatusParameters {
    
    private var awardedRewardId: Int
    private var partyId: Int
    private var status: AwardedRewardStatusRef
    
    public init(awardedRewardId: Int, status: AwardedRewardStatusRef, partyId: Int) {
        self.awardedRewardId = awardedRewardId
        self.partyId = partyId
        self.status = status
    }
    
    public func toJSON() -> Parameters {
        return [
            "awardedRewardId": awardedRewardId,
            "awardedRewardStatusKey": status.rawValue,
            "effectiveOn": Wire.default.simpleDateFormatter.string(from: Date()),
            "changedBy": [
                "referenceTypeKey": ResolveReferenceTypeRef.PartyID.rawValue,
                "referenceValue": "\(partyId)"
            ],
        ]
    }
}
