//
//  GetHealthAttributeFeedbackRouter.swift
//  VitalityActive
//
//  Created by Chris on 2017/08/31.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireActivityLogger
import RealmSwift
import VIAUtilities


// MARK: Router

enum GetHealthAttributeFeedbackRouter: URLRequestConvertible {
    static let basePath = "vitality-manage-health-service-v1"

    case get(tenantId: Int, request: GetHealthAttributeFeedbackRequest)

    var method: HTTPMethod {
        switch self {
        case .get:
            return .post
        }
    }

    var route: String {
        switch self {
        case .get(let tenantId, let request):
            return ("svc/\(tenantId)/getHealthAttributeFeedback/\(request.partyId)")
        }
    }

    var parameters: Parameters {
        switch self {
        case .get(_, let request):
            let body = GetHealthAttributeFeedbackParameters(request)
            return body.toJSON()
        }
    }

    func asURLRequest() throws -> URLRequest {
        let url = try Wire.urlForPath(path: GetHealthAttributeFeedbackRouter.basePath).appendingPathComponent(self.route)
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = self.method.rawValue
        switch self {
        case .get:
            return try JSONEncoding.default.encode(urlRequest, with: self.parameters)
        }
    }
}

extension Wire.Events {

    public static func healthAttributeFeedbackFor(tenantId: Int, partyId: Int, completion: @escaping (_ response: Results<HAReportedHealthAttribute>?, _ error: Error?) -> Void) {

        Wire.Events.healthAttributeFeedbackFor(
            tenantId: tenantId,
            request: GetHealthAttributeFeedbackRequest(

                healthAttributeTypes: PartyAttributeTypeRef.allRawValues().map({ (i) in return HealthAttributeType(typeKey: i) }),
                partyId: partyId
                ),
            completion: completion)
    }

    public static func healthAttributeFeedbackFor(tenantId: Int, request: GetHealthAttributeFeedbackRequest, completion: @escaping (_ response: Results<HAReportedHealthAttribute>?, _ error: Error?) -> Void) {
        let url = GetHealthAttributeFeedbackRouter.get(tenantId: tenantId, request: request)

        Wire.sessionManager.request(url)
            .log()
            .validate(statusCode: 200 ..< 300)
            .apiManagerResponse { response in
                switch response.httpStatusCode {
                case 200 ..< 300:
                    guard let result = GetHealthAttributeFeedbackResponse(json: response.jsonDictionary) else {
                        completion(nil, WireError.parsing)
                        return
                    }
                    completion(HAReportedHealthAttribute.buildHealthAttributes(response: result), nil)
                default:
                    debugPrint("\(String(describing: response.result.error))")
                    completion(nil, response.result.error ?? BackendError.other)
                }
        }
    }
}

/*NB! The extension below is to facilitate iterating over the PartyAttributeTypeRef enum
  -  This is a VERY bad solution, but the best I could find for now.
  -  The solution ASSUMES a contigious sequence of integers as raw values.
  - The implication is that should this convention be broken the iterator
  - will no longer work as expected.
  - Furthermore, adding and removing from the enum will cause a unit test to break.
  - This is deliberate to ensure that someone verifies that the iterator continues working
  - after changes to the enum
*/
extension PartyAttributeTypeRef {
    public static func allRawValues() -> [Int] {
        return [
            31, 28, 37, 14, 42, 3, 2, 16, 23, 22, 8, 18,
            29, 6, 10, 12, 20, 21, 5, 32, 41, 33, 19, 26,
            9, 34, 24, 27, 36, 15, 4, 17, 35, 13, 40, 25, 1, 11, 30
        ]
    }
}

public class GetHealthAttributeFeedbackParameters {
    var request: GetHealthAttributeFeedbackRequest
    public init(_ request: GetHealthAttributeFeedbackRequest) {
        self.request = request
    }

    public func toJSON() -> Parameters {
        return self.request.toJSON()
    }
}
public class GetHealthAttributeFeedbackRequest {
    var healthAttributeTypes: [HealthAttributeType]
    var partyId: Int

    public init(healthAttributeTypes: [HealthAttributeType], partyId: Int) {
        self.healthAttributeTypes = healthAttributeTypes
        self.partyId = partyId
    }

    public func toJSON() -> Parameters {
        var attributes = [Parameters]()
        for val in self.healthAttributeTypes {
            attributes.append(val.toJSON())
        }
        return [
            "healthAttributeTypes": attributes
        ]
    }

    /*public static func buildRequestForAllHealthAttributes(partyId: Int) -> GetHealthAttributeFeedbackRequest {
        return (GetHealthAttributeFeedbackRequest(
            healthAttributeTypes: PartyAttributeTypeRef.all().map({ (attributeType) in return HealthAttributeType(typeKey: attributeType.rawValue) }),
            partyId: partyId
            ))
    }*/
}

public class HealthAttributeType {
    var typeKey: Int

    public init(typeKey: Int) {
        self.typeKey = typeKey
    }

    public func toJSON() -> Parameters {
        var json = [String: Any]()
        json["typeKey"] = self.typeKey
        return json
    }
}
