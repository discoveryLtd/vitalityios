import Foundation
import Alamofire
import RealmSwift
import VIAUtilities

// MARK: Errors

public enum LoginError: Error {
    case incorrectEmailOrPassword
    case missingEmailOrPassword
    case accountLockedOut
}

public enum ForgotPasswordError: Error {
    case notRegistered
}

// MARK: Router

enum LoginRouter: URLRequestConvertible {

    case login(email: String, password: String)
    case forgotPassword(email: String)

    var basePath: String {
        switch self {
        case .login,
             .forgotPassword:
            return "tstc-integration-platform-services-service-v1"
        }
    }

    var method: HTTPMethod {
        switch self {
        case .login,
             .forgotPassword:
            return .post
        }
    }

    var route: String {
        switch self {
        case .login:
            return "login"
        case .forgotPassword:
            return "forgotPassword"
        }
    }

    var parameters: Parameters? {
        switch self {
        case .login(let email, let password):
            let body = LoginParameters(email: email, password: password)
            return body.toJSON()
        case .forgotPassword(let email):
            let body = ForgotPasswordParameters(email: email)
            return body.toJSON()
        }
    }

    // MARK: URLRequestConvertible

    func asURLRequest() throws -> URLRequest {
        let url = try Wire.urlForPath(path: self.basePath).appendingPathComponent(self.route)
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = self.method.rawValue

        switch self {
        case .login,
             .forgotPassword:
            return try JSONEncoding.default.encode(urlRequest, with: self.parameters)
        }
    }
}

// MARK: Networking

extension Wire.Member {

    public static func login(email: String, password: String, completion: @escaping ((_: Error?, _ upgradeUrl: String? ) -> Void)) {

        guard !email.isEmpty && !password.isEmpty else {
            return completion(LoginError.missingEmailOrPassword, nil)
        }
        
        let trimmedEmail = getTrimmedEmail(email)

        // perform login request
        Wire.unauthorisedSessionManager.request(LoginRouter.login(email: trimmedEmail, password: password))
            .log()
            .validate(statusCode: 200 ..< 300)
            .apiManagerResponse { response in
                
                enum errorCodes: Int, EnumCollection {
                    case failError = 400
                    case firstLockoutError  = 40000
                    case secondLockoutError = 423
                }
                
//                let errorCode = errorCodes(rawValue: )
                
                switch response.httpStatusCode {
                case 200 ..< 300:

                    guard let result = LoginResponse(json: response.jsonDictionary) else {
                        completion(WireError.parsing, nil)
                        return
                    }
                    
                    resetRealm(withNew: trimmedEmail)

                    Parser.Login.parse(response: result, completion: { error in
                        completion(error, result.application?.upgradeUrl)
                    })
                case errorCodes.failError.rawValue:
                    if let data = response.data{
                        if let json = JSONSerialization.jsonObject(data: data) {
                            let responseError = APIManagerResponseErrors.init(json: json)?.errors.first
                            if responseError?.code == String(errorCodes.firstLockoutError.rawValue) {
                                completion(LoginError.accountLockedOut, nil)
                            } else {
                                completion(LoginError.incorrectEmailOrPassword, nil)
                            }
                        } else {
                            completion(LoginError.incorrectEmailOrPassword, nil)
                        }
                    } else {
                        completion(LoginError.incorrectEmailOrPassword, nil)
                    }
                case errorCodes.secondLockoutError.rawValue:
                    completion(LoginError.accountLockedOut, nil)
                default:
                    completion(response.result.error ?? BackendError.other, nil)
                }
        }
    }

    public static func forgotPassword(email: String, completion: @escaping ((_: Error?) -> Void)) {

        // trim email and extract environment
        let trimmedEmail = Wire.updateEnvironmentAndTrim(email: email)

        // perform request
        Wire.unauthorisedSessionManager.request(LoginRouter.forgotPassword(email: trimmedEmail))
            .log()
            .validate(statusCode: 200 ..< 300)
            .apiManagerResponse { response in
                switch response.httpStatusCode {
                case 200 ..< 300:
                    completion(nil)
                case 400:
                    completion(ForgotPasswordError.notRegistered)
                default:
                    completion(response.result.error)
                }
        }
    }

}

extension Wire.Member{
    
    fileprivate static func getTrimmedEmail(_ email: String) -> String{
        /*
         * Trim email and extract environment
         */
        var trimmedEmail = email
        if Bundle.main.object(forInfoDictionaryKey: "VIAEnablePrefix") as? Bool ?? true{
            trimmedEmail = Wire.updateEnvironmentAndTrim(email: email)
        }
        
        /*
         * Set default server
         */
        if let environment = Bundle.main.object(forInfoDictionaryKey: "VIADefaultEnvironment") as? NSNumber,
            environment.intValue >= 0{
            Wire.default.currentEnvironment = Wire.Environment(rawValue: environment.intValue) ?? .production
        }
        
        return trimmedEmail
    }
    
    fileprivate static func handleAccountLockError(response: DataResponse<APIManagerResponse>, completion: @escaping ((_: Error?) -> Void)){
        /* Parse error code from response body */
        if let code = getErrorCode(response: response){
            switch code{
            case 40000:
                /* Handle account locked error */
                completion(LoginError.accountLockedOut)
            default:
                /* Normal handling of error */
                completion(LoginError.incorrectEmailOrPassword)
            }
        }else{
            /* Normal handling of error */
            completion(LoginError.incorrectEmailOrPassword)
        }
    }
    
    fileprivate static func getErrorCode(response: DataResponse<APIManagerResponse>) -> Int?{
        guard let responseData = response.data,
            let jsonResponse = JSONSerialization.jsonObject(data: responseData),
            let errors = jsonResponse["errors"] as? [[String: Any]],
            let codeString = errors[0]["code"] as? String,
            let code = Int(codeString) else { return nil }
        return code
    }
}

extension Wire.Member{
    
    fileprivate static func resetRealm(withNew email: String) {
        let username = AppSettings.getLastLoginUsername()
        
        /** If user is not the same with previous user, delete profile photo */
        checkProfilePhoto(username: username, email: email)
        
        /** override existing username */
        AppSettings.setLastLoginUsername(url: email)
    }
    
    fileprivate static func checkProfilePhoto(username: String, email: String){
        
        /** If user is not the same with previous user, delete profile photo */
        if username != email{
            AppSettings.setProfileImage(url: "")
        }
    }
}

// MARK: Structs

public struct LoginParameters {
    public var email: String
    public var password: String

    public init(email: String, password: String) {
        self.email = email
        self.password = password
    }

    public func toJSON() -> Parameters {
        return [
            "loginRequest": [
                "username": self.email,
                "password": self.password,
            ],
        ]
    }
}
public struct ForgotPasswordParameters {
    public var email: String

    public init(email: String) {
        self.email = email
    }

    public func toJSON() -> Parameters {
        return [ "userName": self.email ]
    }
}
