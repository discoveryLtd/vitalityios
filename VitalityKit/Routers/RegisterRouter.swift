//
//  Register.swift
//  VitalityActive
//
//  Created by Wilmar van Heerden on 2016/09/20.
//  Copyright © 2016 Glucode. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireActivityLogger
import VIAUtilities

// MARK: Errors

public enum RegistrationError: Error {
    case invalidEmail
    case invalidEmailregistrationCodeCombination
    case connectivityError
    case userAlreadyExists
    case invalidDateOfBirth
    case emptyDateOfBirth
}

// MARK: Router

enum RegisterRouter: URLRequestConvertible {
    static let basePath = "tstc-integration-platform-services-service-v1"

    case register(email: String, password: String, registrationCode: String)
    case registerWithDOB(email: String, password: String, registrationCode: String, bornOn: String)
    case delete(userId: String)

    // MARK: 

    var method: HTTPMethod {
        switch self {
        case .register:
            return .post
        case .registerWithDOB:
            return .post
        case .delete:
            return .delete
        }
    }

    var route: String {
        switch self {
        case .register:
            return ("register")
        case .registerWithDOB:
            return ("register")
        case .delete:
            return ("register")
        }

    }

    var parameters: Parameters {
        switch self {
        case .register(let email, let password, let registrationCode):
            let body = RegisterParameters(email: email, password: password, registrationCode: registrationCode)
            return body.toJSON()
        case .registerWithDOB(let email, let password, let registrationCode, let bornOn):
            let body = RegisterWithDOBParameters(email: email, password: password, registrationCode: registrationCode, bornOn: bornOn)
            return body.toJSON()
        case .delete(let userId):
            return ["userId": userId]
        }
    }

    // MARK: URLRequestConvertible

    func asURLRequest() throws -> URLRequest {
        let url = try Wire.urlForPath(path: RegisterRouter.basePath).appendingPathComponent(self.route)
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = self.method.rawValue

        switch self {
        case .register:
            return try JSONEncoding.default.encode(urlRequest, with: self.parameters)
        case .registerWithDOB:
            return try JSONEncoding.default.encode(urlRequest, with: self.parameters)
        case .delete:
            return try URLEncoding.default.encode(urlRequest, with: self.parameters)
        }
    }
}

// MARK: Active Rewards Networking

extension Wire.Member {
    public static func register(email: String, password: String, registrationCode: String, completion: @escaping ((_: Error?) -> Void)) {

        guard email.isValidEmail() else {
            return completion(RegistrationError.invalidEmailregistrationCodeCombination)
        }

        // trim email and extract environment
        let trimmedEmail = Wire.updateEnvironmentAndTrim(email: email)

        Wire.unauthorisedSessionManager.request(RegisterRouter.register(email: trimmedEmail, password: password, registrationCode: registrationCode))
            .log()
            .validate(statusCode: 200 ..< 300)
            .apiManagerResponse { response in
                // this request sends nothing back for us to consume, it only says yay 200
                // so there won't be any JSON to serialize
                switch response.httpStatusCode {
                case 200 ..< 300:
                    completion(nil)
                case 404: // TODO: API consistency here.  Swagger = 404, but API = 400 for user not found
                    completion(RegistrationError.invalidEmailregistrationCodeCombination)
                case 409:
                    completion(RegistrationError.userAlreadyExists)
                case 499: // TODO: Change this once error code from BE is defined
                    completion(RegistrationError.invalidDateOfBirth)
                default:
                    completion(response.result.error ?? BackendError.other)
                }
        }
    }
    
    public static func registerWithDOB(email: String, password: String, registrationCode: String, bornOn: String, completion: @escaping ((_: Error?) -> Void)) {
        
        guard email.isValidEmail() else {
            return completion(RegistrationError.invalidEmailregistrationCodeCombination)
        }
        
        // trim email and extract environment
        let trimmedEmail = Wire.updateEnvironmentAndTrim(email: email)
        
        Wire.unauthorisedSessionManager.request(RegisterRouter.registerWithDOB(email: trimmedEmail, password: password, registrationCode: registrationCode, bornOn: bornOn))
            .log()
            .validate(statusCode: 200 ..< 300)
            .apiManagerResponse { response in
                switch response.httpStatusCode {
                case 200 ..< 300:
                    completion(nil)
                case 400:
                    if let data = response.data{
                        let json = JSONSerialization.jsonObject(data: data)
                        let responseError = APIManagerResponseErrors.init(json: json!)?.errors.first
                        if responseError?.code == "40010" {
                            completion(RegistrationError.invalidDateOfBirth)
                        } else if responseError?.code == "40011" {
                            completion(RegistrationError.emptyDateOfBirth)
                        }
                    }
                case 404: // TODO: API consistency here.  Swagger = 404, but API = 400 for user not found
                    completion(RegistrationError.invalidEmailregistrationCodeCombination)
                case 409:
                    completion(RegistrationError.userAlreadyExists)
                default:
                    completion(response.result.error ?? BackendError.other)
                }
        }
    }
    
    public static func delete(userId: String, completion: @escaping ((_: Error?) -> Void)) {
        Wire.sessionManager.request(RegisterRouter.delete(userId: userId))
            .log()
            .validate(statusCode: 200 ..< 300)
            .apiManagerResponse { response in
                switch response.httpStatusCode {
                case 200 ..< 300:
                    completion(nil)
                default:
                    completion(response.result.error ?? BackendError.other)
                }
        }
    }
}

// MARK: Structs

public struct RegisterParameters {
    public var email: String
    public var password: String
    public var registrationCode: String

    public init(email: String, password: String, registrationCode: String) {
        self.email = email
        self.password = password
        self.registrationCode = registrationCode
    }

    public func toJSON() -> Parameters {
        return [
            "password": self.password,
            "userName": self.email,
            "registrationCode": self.registrationCode,
        ]
    }
}

public struct RegisterWithDOBParameters {
    public var email: String
    public var password: String
    public var registrationCode: String
    public var bornOn: String
    
    public init(email: String, password: String, registrationCode: String, bornOn: String) {
        self.email = email
        self.password = password
        self.registrationCode = registrationCode
        self.bornOn = bornOn
    }
    
    public func toJSON() -> Parameters {
        return [
            "password": self.password,
            "userName": self.email,
            "registrationCode": self.registrationCode,
            "bornOn": self.bornOn,
        ]
    }
}
