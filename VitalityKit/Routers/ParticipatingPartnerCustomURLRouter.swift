//
//  ParticipatingPartnerCustomURL.swift
//  VitalityActive
//
//  Created by Dexter Anthony Ambrad on 5/3/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import Alamofire
import VIAUtilities

enum ParticipatingPartnerCustomURLRouter: URLRequestConvertible {
    
    case participatingPartnerCustomURL(partnerURL: URL)
    
    var method: HTTPMethod {
        switch self {
        case .participatingPartnerCustomURL:
            return .get
        }
    }
    
    var url: URL {
        switch self {
        case .participatingPartnerCustomURL(let partnerURL):
            return partnerURL
        }
    }
    func asURLRequest() throws -> URLRequest {
        var urlRequest = URLRequest(url: self.url)
        
        urlRequest.httpMethod = self.method.rawValue
        switch self {
        case .participatingPartnerCustomURL:
            return try JSONEncoding.default.encode(urlRequest, with: nil)
        }
    }
}
