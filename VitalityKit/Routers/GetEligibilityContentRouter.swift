//
//  GetEligibilityContentRouter.swift
//  VitalityActive
//
//  Created by Simon Stewart on 11/1/17.
//  Copyright © 2017 Glucode. All rights reserved.
//


import Foundation
import Alamofire
import AlamofireActivityLogger


// MARK: Router

enum GetEligibilityContentRouter: URLRequestConvertible {
    static let basePath = "vitality-manage-vitality-products-service-1"
    
    case getEligibilityContent(tenantId: Int, partyId: Int, membershipId: Int, productFeatureKey: Int)
    
    var method: HTTPMethod {
        switch self {
        case .getEligibilityContent:
            return .post
        }
    }
    
    var route: String {
        switch self {
        case .getEligibilityContent(let tenantId, let partyId, let membershipId, _):
            return ("svc/\(tenantId)/getEligibilityContent/\(partyId)/\(membershipId)")
        }
    }
    
    var parameters: Parameters {
        switch self {
        case .getEligibilityContent(_, _, _, let productFeatureKey):
            var json = Parameters()
            json = [
                "productEligibilityContentIn": [
                    "productFeatureKey": productFeatureKey
                ]
            ]
            return json
        }
    }
    
    func asURLRequest() throws -> URLRequest {
        let url = try Wire.urlForPath(path: GetEligibilityContentRouter.basePath).appendingPathComponent(self.route)
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = self.method.rawValue
        switch self {
        case .getEligibilityContent:
            return try JSONEncoding.default.encode(urlRequest, with: self.parameters)
        }
    }
}

extension Wire.Member {
    public static func getEligibilityContent(tenantId: Int, partyId: Int, membershipId: Int, productFeatureKey: Int, completion: @escaping (_ error: Error?, _ urlString: String?, _ content: String?) -> Void) {
        let url = GetEligibilityContentRouter.getEligibilityContent(tenantId: tenantId, partyId: partyId, membershipId: membershipId, productFeatureKey: productFeatureKey)
        Wire.sessionManager.request(url)
            .log()
            .validate(statusCode: 200 ..< 300)
            .apiManagerResponse { response in
                switch response.httpStatusCode {
                case 200 ..< 300:
                    guard let result = GetEligibilityContentResponse(json: response.jsonDictionary) else {
                        return completion(WireError.parsing, nil, nil)
                    }
                    return completion(nil, result.productFeatureEligibilityContent?.partnerURL, result.productFeatureEligibilityContent?.content)
                default:
                    debugPrint("\(String(describing: response.result.error))")
                    completion(response.result.error ?? BackendError.other, nil, nil)
                }
        }
    }
}
