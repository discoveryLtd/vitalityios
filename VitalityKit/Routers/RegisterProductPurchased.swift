//
//  RegisterProductPurchased.swift
//  VitalityActive
//
//  Created by Von Kervin R. Tuico on 07/05/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import Alamofire

// MARK: Router
enum RegisterProductPurchasedRouter: URLRequestConvertible {
    
    case registerProductPurchased(tenantId: Int, request: RegisterProductPurchasedParameters)
    
    static let basePath = "vitality-purchase-orchestration-service-1"
    
    var method: HTTPMethod {
        switch self {
        case .registerProductPurchased(_, _):
            return .post
        }
    }
    
    var route: String {
        switch self {
        case .registerProductPurchased(let tenantId, _):
            return ("/svc/\(tenantId)/registerProductPurchased")
        }
    }
    
    var parameters: Parameters? {
        switch self {
        case .registerProductPurchased(_, let params):
            return params.toJSON()
        }
    }
    
    // MARK: URLRequestConvertible
    
    func asURLRequest() throws -> URLRequest {
        let url = try Wire.urlForPath(path: RegisterProductPurchasedRouter.basePath).appendingPathComponent(self.route)
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = self.method.rawValue
        
        switch self {
        case .registerProductPurchased:
            return try JSONEncoding.default.encode(urlRequest, with: self.parameters)
        }
    }
}

// MARK: Networking

extension Wire.Events {
    /* TODO: Add Response */
    public static func registerProductPurchased(tenantId: Int, request: RegisterProductPurchasedParameters,
                                                completion: @escaping (_: Error?) -> Void) {
        let url = RegisterProductPurchasedRouter.registerProductPurchased(tenantId: tenantId, request: request)
        Wire.sessionManager.request(url)
            .log()
            .validate(statusCode: 200 ..< 300)
            .apiManagerResponse { (response) in
                switch response.httpStatusCode {
                case 200 ..< 300:
                    completion(nil)
                case 400:
                    return completion(response.result.error ?? BackendError.other)
                default:
                    return completion(response.result.error ?? BackendError.other)
                }
        }
    }
}

// MARK: Structs

public struct RegisterProductPurchasedParameters {
    var request: RegisterProductPurchasedRequest
    
    public init(_ request: RegisterProductPurchasedRequest) {
        self.request = request
    }
    
    public func toJSON() -> Parameters {
        return self.request.toJSON()
    }
}

public struct RegisterProductPurchasedRequest {
    public var actionOn: String
    public var instructionReference: Int
    public var instructionTypeKey: Int
    public var purchaseDetails: PurchaseDetails
    
    public init(actionOn: String, instructionReference: Int, instructionTypeKey: Int, purchaseDetails: PurchaseDetails) {
        self.actionOn = actionOn
        self.instructionReference = instructionReference
        self.instructionTypeKey = instructionTypeKey
        self.purchaseDetails = purchaseDetails
    }
    
    public func toJSON() -> Parameters {
        
        return [
            "actionOn": self.actionOn,
            "instructionReference": self.instructionReference,
            "instructionTypeKey": self.instructionTypeKey,
            "purchaseDetails": self.purchaseDetails.toJSON()
        ]
    }
}

public struct PurchaseDetails {
    
    public var productKey: Int
    public var purchaseId: Int
    public var amounts: [Amounts]
    public var parties: [Parties]
    public var purchaseItems: [PurchaseItems]
    public var purchaseReferences: [PurchaseReferences]
    
    public init(amounts: [Amounts], parties: [Parties], productKey: Int, purchaseId: Int,
                purchaseItems: [PurchaseItems], purchaseReferences: [PurchaseReferences]) {
        self.amounts = amounts
        self.parties = parties
        self.productKey = productKey
        self.purchaseId = purchaseId
        self.purchaseItems = purchaseItems
        self.purchaseReferences = purchaseReferences
    }
    
    public func toJSON() -> Parameters {
        
        var amounts = [Parameters]()
        for value in self.amounts {
            amounts.append(value.toJSON())
        }
        
        var parties = [Parameters]()
        for value in self.parties {
            parties.append(value.toJSON())
        }
        
        var purchaseItems = [Parameters]()
        for value in self.purchaseItems {
            purchaseItems.append(value.toJSON())
        }
        
        var purchaseReferences = [Parameters]()
        for value in self.purchaseReferences {
            purchaseReferences.append(value.toJSON())
        }
        
        return [
            "amounts": amounts,
            "parties": parties,
            "productKey" : self.productKey,
//            "purchaseId" : self.purchaseId,
            "purchaseItems": purchaseItems,
            "purchaseReferences": purchaseReferences
        ]
    }
}

public struct Amounts {
    
    public var typeCode: String
    public var typeKey: Int
    public var typeName: String
    public var value: String
    
    public init(typeCode: String, typeKey: Int,
                typeName: String, value: String) {
        self.typeCode = typeCode
        self.typeKey = typeKey
        self.typeName = typeName
        self.value = value
    }
    
    public func toJSON() -> Parameters {
        return [
            "typeCode" : self.typeCode,
            "typeKey" : self.typeKey,
//            "typeName" : self.typeName,
            "value" : self.value
        ]
    }
}

public struct Parties {
    
    public var partyRoleTypeKey: Int
    public var referenceTypeKey: Int
    public var referenceValue: String
    
    public init(partyRoleTypeKey: Int, referenceTypeKey: Int, referenceValue: String) {
        self.partyRoleTypeKey = partyRoleTypeKey
        self.referenceTypeKey = referenceTypeKey
        self.referenceValue = referenceValue
    }
    
    public func toJSON() -> Parameters {
        
        return [
            "partyRoleTypeKey" : self.partyRoleTypeKey,
            "referenceTypeKey" : self.referenceTypeKey,
            "referenceValue" : self.referenceValue
        ]
    }
}

public struct PurchaseItems {
    
    public var actionOn: String
    public var amounts: [PIAmounts]
    public var categoryKey: Int
    public var description: String
    public var instructionReference: Int
    public var instructionTypeKey: Int
    public var parties: [PIParties]
    public var productKey: Int
    public var purchaseItemDates: [PIPurchaseItemDates]
    public var purchaseItemDiscounts: [PIPurchaseItemDiscounts]
    public var purchaseItemId: Int
    public var purchaseItemMetaDatas: [PIPurchaseItemMetaDatas]
    public var purchaseItemReferences: [PIPurchaseItemReferences]
    public var qualifiesForDiscount: Bool
    public var quantity: Int
    
    public init(actionOn: String, amounts: [PIAmounts], categoryKey: Int, description: String,
                instructionReference: Int, instructionTypeKey: Int, parties: [PIParties], productKey: Int,
                purchaseItemDates: [PIPurchaseItemDates], purchaseItemDiscounts: [PIPurchaseItemDiscounts],
                purchaseItemId: Int, purchaseItemMetaDatas: [PIPurchaseItemMetaDatas],
                purchaseItemReferences: [PIPurchaseItemReferences], qualifiesForDiscount: Bool, quantity: Int) {
        self.actionOn = actionOn
        self.amounts = amounts
        self.categoryKey = categoryKey
        self.description = description
        self.instructionReference = instructionReference
        self.instructionTypeKey = instructionTypeKey
        self.parties = parties
        self.productKey = productKey
        self.purchaseItemDates = purchaseItemDates
        self.purchaseItemDiscounts = purchaseItemDiscounts
        self.purchaseItemId = purchaseItemId
        self.purchaseItemMetaDatas = purchaseItemMetaDatas
        self.purchaseItemReferences = purchaseItemReferences
        self.qualifiesForDiscount = qualifiesForDiscount
        self.quantity = quantity
    }
    
    public func toJSON() -> Parameters {
        
        var amounts = [Parameters]()
        for value in self.amounts {
            amounts.append(value.toJSON())
        }
        
        var parties = [Parameters]()
        for value in self.parties {
            parties.append(value.toJSON())
        }
        
        var purchaseItemDates = [Parameters]()
        for value in self.purchaseItemDates {
            purchaseItemDates.append(value.toJSON())
        }
        
        var purchaseItemDiscounts = [Parameters]()
        for value in self.purchaseItemDiscounts {
            purchaseItemDiscounts.append(value.toJSON())
        }
        
        var purchaseItemMetaDatas = [Parameters]()
        for value in self.purchaseItemMetaDatas {
            purchaseItemMetaDatas.append(value.toJSON())
        }
        
        var purchaseItemReferences = [Parameters]()
        for value in self.purchaseItemReferences {
            purchaseItemReferences.append(value.toJSON())
        }
        
        return [
            "actionOn" : self.actionOn,
            "amounts" : amounts,
            "categoryKey" : self.categoryKey,
            "description" : self.description,
            "instructionReference" : self.instructionReference,
            "instructionTypeKey" : self.instructionTypeKey,
            "parties" : parties,
            "productKey" : self.productKey,
            "purchaseItemDates" : purchaseItemDates,
            "purchaseItemDiscounts" : purchaseItemDiscounts,
            "purchaseItemId" : self.purchaseItemId,
            "purchaseItemMetaDatas" : purchaseItemMetaDatas,
            "purchaseItemReferences" : purchaseItemReferences,
            "qualifiesForDiscount" : self.qualifiesForDiscount,
            "quantity" : self.quantity
        ]
    }
}

public struct PIAmounts {
    
    public var typeCode: String
    public var typeKey: Int
    public var typeName: String
    public var value: String
    
    public init(typeCode: String, typeKey: Int,
                typeName: String, value: String) {
        self.typeCode = typeCode
        self.typeKey = typeKey
        self.typeName = typeName
        self.value = value
    }
    
    public func toJSON() -> Parameters {
        return [
            "typeCode" : self.typeCode,
            "typeKey" : self.typeKey,
            "typeName" : self.typeName,
            "value" : self.value
        ]
    }
}

public struct PIParties {
    
    public var partyRoleTypeKey: Int
    public var referenceTypeKey: Int
    public var referenceValue: String
    
    public init(partyRoleTypeKey: Int, referenceTypeKey: Int, referenceValue: String) {
        self.partyRoleTypeKey = partyRoleTypeKey
        self.referenceTypeKey = referenceTypeKey
        self.referenceValue = referenceValue
    }
    
    public func toJSON() -> Parameters {
        
        return [
            "partyRoleTypeKey" : self.partyRoleTypeKey,
            "referenceTypeKey" : self.referenceTypeKey,
            "referenceValue" : self.referenceValue
        ]
    }
}

public struct PIPurchaseItemDates {
    
    public var actionOn: String
    public var typeKey: Int
    
    public init(actionOn: String, typeKey: Int) {
        self.actionOn = actionOn
        self.typeKey = typeKey
    }
    
    public func toJSON() -> Parameters {
        
        return [
            "actionOn" : self.actionOn,
            "typeKey" : self.typeKey
        ]
    }
}

public struct PIPurchaseItemDiscounts {
    
    public var pricingCategoryTypeKey: Int
    public var typeKey: Int
    public var value: Int
    
    public init(pricingCategoryTypeKey: Int, typeKey: Int, value: Int) {
        self.pricingCategoryTypeKey = pricingCategoryTypeKey
        self.typeKey = typeKey
        self.value = value
    }
    
    public func toJSON() -> Parameters {
        
        return [
            "pricingCategoryTypeKey" : self.pricingCategoryTypeKey,
            "typeKey" : self.typeKey,
            "value" : self.value
        ]
    }
}

public struct PIPurchaseItemMetaDatas {
    
    public var metadataValue: String
    public var typeKey: Int
    
    public init(metadataValue: String, typeKey: Int) {
        self.metadataValue = metadataValue
        self.typeKey = typeKey
    }
    
    public func toJSON() -> Parameters {
        
        return [
            "metadataValue" : self.metadataValue,
            "typeKey" : self.typeKey
        ]
    }
}

public struct PIPurchaseItemReferences {
    
    public var purchaseItemReference: String
    public var typeKey: Int
    
    public init(purchaseItemReference: String, typeKey: Int) {
        self.purchaseItemReference = purchaseItemReference
        self.typeKey = typeKey
    }
    
    public func toJSON() -> Parameters {
        
        return [
            "purchaseItemReference" : self.purchaseItemReference,
            "typeKey" : self.typeKey
        ]
    }
}

public struct PurchaseReferences {
    
    public var purchaseReference: String
    public var purchaseReferenceTypeKey: Int
    
    public init(purchaseReference: String, purchaseReferenceTypeKey: Int) {
        self.purchaseReference = purchaseReference
        self.purchaseReferenceTypeKey = purchaseReferenceTypeKey
    }
    
    public func toJSON() -> Parameters {
        
        return [
            "purchaseReference" : self.purchaseReference,
            "purchaseReferenceTypeKey" : self.purchaseReferenceTypeKey
        ]
    }
}
