//
//  ProcessEventsV2Router.swift
//  VitalityActive
//
//  Created by Simon Stewart on 12/1/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireActivityLogger
import VIAUtilities


enum ProcessEventsV2Router: URLRequestConvertible {
    static let basePath = "vitality-manage-events-service-v1"
    
    case processEventsV2(tenantId: Int, events: [ProcessEventsV2PartyReferenceEvent])
    
    var method: HTTPMethod {
        switch self {
        case .processEventsV2:
            return .post
        }
    }
    
    var route: String {
        switch self {
        case .processEventsV2(let tenantId, _):
            return ("svc/\(tenantId)/processEventsV2")
        }
    }
    
    var parameters: Parameters {
        switch self {
        case .processEventsV2(_, let events):
            let body = ProcessEventsV2Parameters(events)
            return body.toJSON()
        }
    }
    
    func asURLRequest() throws -> URLRequest {
        let url = try Wire.urlForPath(path: ProcessEventsV2Router.basePath).appendingPathComponent(self.route)
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = self.method.rawValue
        switch self {
        case .processEventsV2:
            return try JSONEncoding.default.encode(urlRequest, with: self.parameters)
        }
    }
}

extension Wire.Events {
    public static func processEventsV2(tenantId: Int, events: [ProcessEventsV2PartyReferenceEvent], completion: @escaping (_ response: ProcessEventsV2Response?, _ error: Error?) -> Void) {
        let url = ProcessEventsV2Router.processEventsV2(tenantId: tenantId, events: events)
        Wire.sessionManager.request(url)
            .log()
            .validate(statusCode: 200 ... 200)
            .apiManagerResponse { response in
                switch response.httpStatusCode {
                case 200:
                    guard let result = ProcessEventsV2Response(json: response.jsonDictionary) else {
                        completion(nil, WireError.parsing)
                        return
                    }
                    completion(result, nil)
                default:
                    debugPrint("\(String(describing: response.result.error))")
                    completion(nil, response.result.error ?? BackendError.other)
                }
        }
    }
    
}

public struct ProcessEventsV2AssociatedEvent {
    
    var applicableTo: ProcessEventsV2ApplicableTo
    
    var associatedEventExternalReference: [ProcessEventsV2AssociatedEventExternalReference]?
    var associatedEventMetadatas: [ProcessEventsV2AssociatedEventMetaDatas]?
    var associatedEventReportedBy:ProcessEventsV2AssociatedEventMetaDatas
    
    var eventAssociationTypeKey: EventAssociationTypeRef
    var eventCapturedOn: Date
    var eventId: Int?
    var eventOccuredOn: Date
    var eventSourceTypeKey: EventSourceRef
    
    var typeKey: EventTypeRef
    
    init(applicableTo: ProcessEventsV2ApplicableTo, associatedEventExternalReference: [ProcessEventsV2AssociatedEventExternalReference]?,
         associatedEventMetadatas: [ProcessEventsV2AssociatedEventMetaDatas]?, associatedEventReportedBy:ProcessEventsV2AssociatedEventMetaDatas,
         eventAssociationTypeKey: EventAssociationTypeRef, eventCapturedOn: Date, eventId: Int?, eventOccuredOn: Date,
         eventSourceTypeKey: EventSourceRef, typeKey: EventTypeRef) {
        
        self.applicableTo = applicableTo
        
        self.associatedEventExternalReference = associatedEventExternalReference
        self.associatedEventMetadatas = associatedEventMetadatas
        self.associatedEventReportedBy = associatedEventReportedBy
        
        self.eventAssociationTypeKey = eventAssociationTypeKey
        self.eventCapturedOn = eventCapturedOn
        self.eventId = eventId
        self.eventOccuredOn = eventOccuredOn
        self.eventSourceTypeKey = eventSourceTypeKey
        
        self.typeKey = typeKey
    }
}


public struct ProcessEventsV2PartyReferenceEvent {
    
    var applicableTo: ProcessEventsV2ApplicableTo
    
    var associatedEvents: [ProcessEventsV2AssociatedEvent]
    
    var eventCapturedOn: Date
    var eventExternalReference: [ProcessEventsV2EventExternalReference]?
    var eventId: Int?
    
    var eventMetaDatas: [ProcessEventsV2EventMetaData]?
    var eventOccurredOn: Date
    var eventSourceTypeKey: EventSourceRef
    
    var reportedBy: ProcessEventsV2PartyReferenceEventsReportedBy
    var typeKey: Int
    
    public init(applicableTo: ProcessEventsV2ApplicableTo,
                associatedEvents: [ProcessEventsV2AssociatedEvent],
                eventCapturedOn: Date,
                eventExternalReference: [ProcessEventsV2EventExternalReference]?,
                eventId: Int?,
                eventMetaDatas: [ProcessEventsV2EventMetaData]?,
                eventOccurredOn: Date,
                eventSourceTypeKey: EventSourceRef,
                reportedBy: ProcessEventsV2PartyReferenceEventsReportedBy,
                typeKey: Int) {
        self.applicableTo = applicableTo
        
        self.associatedEvents = associatedEvents
        
        self.eventCapturedOn = eventCapturedOn
        self.eventExternalReference = eventExternalReference
        self.eventId = eventId
        
        self.eventMetaDatas = eventMetaDatas
        self.eventOccurredOn = eventOccurredOn
        self.eventSourceTypeKey = eventSourceTypeKey
        
        self.reportedBy = reportedBy
        self.typeKey = typeKey
    }
}

public struct ProcessEventsV2EventExternalReference {
    var typeKey: Int
    var value: String
    
    public init(typeKey: Int, value: String) {
        self.typeKey = typeKey
        self.value = value
    }
}
public struct ProcessEventsV2ApplicableTo {
    var referenceTypeKey: Int
    var value: String
    
    public init(referenceTypeKey: Int, value: String) {
        self.referenceTypeKey = referenceTypeKey
        self.value = value
    }
}
public struct ProcessEventsV2AssociatedEventExternalReference {
    var typeKey: Int
    var value: String
    
    public init(typeKey: Int, value: String) {
        self.typeKey = typeKey
        self.value = value
    }
}
public struct ProcessEventsV2AssociatedEventReportedBy {
    var referenceTypeKey: Int
    var value: String
    
    public init(referenceTypeKey: Int, value: String) {
        self.referenceTypeKey = referenceTypeKey
        self.value = value
    }
}
public struct ProcessEventsV2PartyReferenceEventsReportedBy {
    var referenceTypeKey: Int
    var value: String
    
    public init(referenceTypeKey: Int, value: String) {
        self.referenceTypeKey = referenceTypeKey
        self.value = value
    }
}
public struct ProcessEventsV2AssociatedEventMetaDatas {
    var typeKey: Int
    var value: String
    var unitOfMeasure: String?
    
    public init(typeKey: Int, value: String, unitOfMeasure: String?) {
        self.typeKey = typeKey
        self.value = value
        self.unitOfMeasure = unitOfMeasure
    }
}
public struct ProcessEventsV2EventMetaData {
    var typeKey: Int
    var value: String
    var unitOfMeasure: String?
    
    public init(typeKey: Int, value: String, unitOfMeasure: String?) {
        self.typeKey = typeKey
        self.value = value
        self.unitOfMeasure = unitOfMeasure
    }
    
}
public struct ProcessEventsV2Parameters {
    
    var events: [ProcessEventsV2PartyReferenceEvent]
    public init(_ events: [ProcessEventsV2PartyReferenceEvent]) {
        self.events = events
    }
    
    private func createAssociatedEvent(_ associatedEvent: ProcessEventsV2AssociatedEvent) -> Parameters {
        
        
        let associatedEventApplicableTo = [
            [
                "typeKey": associatedEvent.applicableTo.referenceTypeKey,
                "value": associatedEvent.applicableTo.value
            ]
        ]
        var associatedEventExternalReferencesResult: Array<[String : Any]> = Array<[String : Any]>()
        if let associatedEventExternalReferences = associatedEvent.associatedEventExternalReference {
            for associatedEventExternalReference in associatedEventExternalReferences {
                let extRef: [String : Any] = [
                    "typeKey": associatedEventExternalReference.typeKey,
                    "value": associatedEventExternalReference.value
                ]
                associatedEventExternalReferencesResult.append(extRef)
            }
        }
        var associatedEventMetaDatasResult: Array<[String : Any]> = Array<[String : Any]>()
        if let metaDatas = associatedEvent.associatedEventMetadatas {
            for metaData in metaDatas {
                let extRef: [String : Any] = [
                    "typeKey": metaData.typeKey,
                    "unitOfMeasure": metaData.unitOfMeasure ?? "",
                    "value": metaData.value
                ]
                associatedEventMetaDatasResult.append(extRef)
            }
        }
        
        
        var associatedEventParameters = [
            "associatedEventApplicableTo": associatedEventApplicableTo,
            "associatedEventExternalReference": associatedEventExternalReferencesResult,
            "associatedEventMetadatas": associatedEventMetaDatasResult,
            "eventAssociationTypeKey": associatedEvent.eventAssociationTypeKey.rawValue,
            "eventCapturedOn": Wire.getFormattedDateWithTimezone(date: associatedEvent.eventCapturedOn),
            "eventSourceTypeKey": associatedEvent.eventSourceTypeKey.rawValue,
            "eventOccuredOn": Wire.getFormattedDateWithTimezone(date: associatedEvent.eventOccuredOn),
            "eventSourceTypeKey": associatedEvent.eventSourceTypeKey.rawValue,
            "typeKey": associatedEvent.typeKey.rawValue
            ] as [String : Any]
        if (associatedEvent.eventId != nil) {
            associatedEventParameters["eventId"] = associatedEvent.eventId
        }
        
        return associatedEventParameters
    }
    
    private func createEvent(_ event: ProcessEventsV2PartyReferenceEvent) -> Parameters {
        
        var associatedEvents = [Parameters]()
        for associatedEvent in event.associatedEvents {
            associatedEvents.append(createAssociatedEvent(associatedEvent))
        }
        
        var eventExtRefsResult: Array<[String : Any]> = Array<[String : Any]>()
        if let extRefs = event.eventExternalReference {
            for extRef in extRefs {
                let eventMetaData: [String : Any] = [
                    "typeKey": extRef.typeKey,
                    "value": extRef.value
                ]
                eventExtRefsResult.append(eventMetaData)
            }
        }
        
        var eventMetaDatas: Array<[String : Any]> = Array<[String : Any]>()
        if let eventValues = event.eventMetaDatas {
            for eventMetaDataValue in eventValues {
                var eventMetaData: [String : Any] = [
                    "typeKey": eventMetaDataValue.typeKey,
                    "value": eventMetaDataValue.value
                ]
                if let unitOfMeasure = eventMetaDataValue.unitOfMeasure {
                    eventMetaData["unitOfMeasure"] = unitOfMeasure
                }
                eventMetaDatas.append(eventMetaData)
            }
        }
        var result: Parameters = [
            "applicableTo":[
                "referenceTypeKey" : event.applicableTo.referenceTypeKey,
                "value": event.applicableTo.value
            ],
            "associatedEvent": associatedEvents,
            "eventCapturedOn" : Wire.getFormattedDateWithTimezone(date: event.eventCapturedOn),
            "eventOccuredOn" : Wire.getFormattedDateWithTimezone(date: event.eventOccurredOn),
            "eventSourceTypeKey": event.eventSourceTypeKey.rawValue,
            "reportedBy":[
                "referenceTypeKey" : event.reportedBy.referenceTypeKey,
                "value": event.reportedBy.value
            ],
            "typeKey":event.typeKey
        ]
        if eventExtRefsResult.count > 0 {
            result["eventExternalReference"] = eventExtRefsResult
        }
        if eventMetaDatas.count > 0 {
            result["eventMetaData"] = eventMetaDatas
        }
        if event.eventId != nil {
            result["eventId"] = event.eventId
        }
        
        return result
    }
    
    
    public func toJSON() -> Parameters {
        
        var events = [Parameters]()
        for event in self.events {
            events.append(createEvent(event))
        }
        
        return [
            "processEventV2Request": [
                "partyReferenceEvents": events
            ]
        ]
    }
}
