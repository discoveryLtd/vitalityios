import Foundation
import Alamofire
import VIAUtilities

// MARK: Router
enum GetEventByPartyRouter: URLRequestConvertible {

    case getEventByParty(tenantId: Int, request: GetEventByPartyParameter)

    static let basePath = "vitality-manage-events-service-v1"

    var method: HTTPMethod {
        switch self {
        case .getEventByParty(_, _):
            return .post
        }
    }

    var route: String {
        switch self {
        case .getEventByParty(let tenantId, let params):
            return ("svc/\(tenantId)/getEventByParty/\(params.partyId)")

        }
    }

    var parameters: Parameters? {
        switch self {
        case .getEventByParty(_, let params):
            return params.toJSON()
        }
    }

    // MARK: URLRequestConvertible

    func asURLRequest() throws -> URLRequest {
        let url = try Wire.urlForPath(path: GetEventByPartyRouter.basePath).appendingPathComponent(self.route)
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = self.method.rawValue

        switch self {
        case .getEventByParty:
            return try JSONEncoding.default.encode(urlRequest, with: self.parameters)
        }
    }
}

// MARK: Networking

extension Wire.Events {
//    public static func getEventByPartyUsingForcedSession(tenantId: Int, request: GetEventByPartyParameter, completion: @escaping (( _: Error?, _: GetEventByPartyResponse?) -> Void)) {
//        Wire.forcedSessionManager.request(GetEventByPartyRouter.getEventByParty(tenantId: tenantId, request: request))
//            .log()
//            .validate(statusCode: 200 ..< 300)
//            .apiManagerResponse { response in
//                switch response.httpStatusCode {
//                case 200 ..< 300:
//                    guard let result = GetEventByPartyResponse(json: response.jsonDictionary) else {
//                        return completion(WireError.parsing, nil)
//                    }
//                    Parser.Events.parseEvents(response: result, completion: { (error, eventRO) in
//                        return completion(nil, result)
//                    })
//                case 400:
//                    return completion(response.result.error, nil)
//                default:
//                    return completion(response.result.error, nil)
//                }
//        }
//    }
    public static func getEventByParty(tenantId: Int, request: GetEventByPartyParameter, completion: @escaping (( _: Error?, _: GetEventByPartyResponse?) -> Void)) {
        Wire.sessionManager.request(GetEventByPartyRouter.getEventByParty(tenantId: tenantId, request: request))
            .log()
            .validate(statusCode: 200 ..< 300)
            .apiManagerResponse { response in
                switch response.httpStatusCode {
                case 200 ..< 300:
                    guard let result = GetEventByPartyResponse(json: response.jsonDictionary) else {
                        return completion(WireError.parsing, nil)
                    }
                    Parser.Events.parseEvents(response: result, completion: { (error, eventRO) in
                        return completion(nil, result)
                    })
                case 400:
                    return completion(response.result.error, nil)
                default:
                    return completion(response.result.error, nil)
                }
        }
    }
    
//    public static func getEventByParty(tenantId: Int, request: GetEventByPartyParameter, completion: @escaping (( _: Error?, _: GetEventByPartyResponse?) -> Void)) {
//        if let path = Bundle.main.url(forResource: "allevents", withExtension: "json"){
//            if let stubResponse = parseJsonFile(url: path){
//                guard let result = GetEventByPartyResponse(json: stubResponse) else {
//                    return completion(WireError.parsing, nil)
//                }
//                Parser.Events.parseEvents(response: result, completion: { (error, eventRO) in
//                    return completion(nil, result)
//                })
//            }
//        }
//        return completion(nil, nil)
//    }
    
    static func parseJsonFile(url: URL) -> [String : AnyObject]?{
        do {
            let data:Data = try Data(contentsOf: url)
            return try JSONSerialization.jsonObject(with: data, options: []) as? [String : AnyObject]
        }
        catch {
            return nil
        }
    }
    
//    public static func getEventByParty(tenantId: Int, request: GetEventByPartyParameter, completion: @escaping (( _: Error?, _: GetEventByPartyResponse?) -> Void)) {
//        if let path = Bundle.main.url(forResource: "allevents", withExtension: "json"){
//            if let stubResponse = parseJsonFile(url: path){
//                guard let result = GetEventByPartyResponse(json: stubResponse) else {
//                    return completion(WireError.parsing, nil)
//                }
//                Parser.Events.parseEvents(response: result, completion: { (error, eventRO) in
//                    return completion(nil, result)
//                })
//            }
//        }
//        return completion(nil, nil)
//    }
    
//    static func parseJsonFile(url: URL) -> [String : AnyObject]?{
//        do {
//            let data:Data = try Data(contentsOf: url)
//            return try JSONSerialization.jsonObject(with: data, options: []) as! [String : AnyObject]
//        }
//        catch {
//            return nil
//        }
//    }
}

// MARK: Structs

public struct GetEventByPartyParameter {

    public var partyId: Int
    public var effectiveFrom: Date?
    public var effectiveTo: Date?
    public var eventTypeFilterTypeKeys: [Int]?
    public var eventTypesTypeKeys: [EventTypeRef]?
    public init(partyId: Int, effectiveFrom: Date?, effectiveTo: Date?, eventTypeFilterTypeKeys: [Int]) {
        self.partyId = partyId
        self.effectiveFrom = effectiveFrom
        self.effectiveTo = effectiveTo
        self.eventTypeFilterTypeKeys = eventTypeFilterTypeKeys
        print("EVENT \(eventTypeFilterTypeKeys) dates received \(String(describing: effectiveFrom)) ---> \(String(describing: effectiveTo))")
    }
    public init(partyId: Int, effectiveFrom: Date? = nil, effectiveTo: Date? = nil, eventTypesTypeKeys: [EventTypeRef]) {
        self.partyId = partyId
        self.effectiveFrom = effectiveFrom
        self.effectiveTo = effectiveTo
        self.eventTypesTypeKeys = eventTypesTypeKeys
    }
    public func toJSON() -> Parameters {

        var typeFilterKeys = [Parameters]()
        for key in self.eventTypeFilterTypeKeys ?? [] {
            typeFilterKeys.append(["typeKey": key])
        }
        var typesTypeKeys = [Parameters]()
        for key in self.eventTypesTypeKeys ?? [] {
            typesTypeKeys.append(["typeKey": key.rawValue])
        }

        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        var result: Parameters = [:]
        if let effectiveTo = effectiveTo, let effectiveFrom = effectiveFrom {
            result = ["effectivePeriod": [
                "effectiveTo": dateFormatter.string(from: effectiveTo),
                "effectiveFrom": dateFormatter.string(from: effectiveFrom)
                ]
            ]
        }

        if typeFilterKeys.count > 0 {
            result["eventTypeFilters"] = typeFilterKeys
        }
        if typesTypeKeys.count > 0 {
            result["eventTypes"] = typesTypeKeys
        }

        return result
    }
}

