//
//  CreateUserInstructionRouter.swift
//  VitalityActive
//
//  Created by Simon Stewart on 2/9/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireActivityLogger


// MARK: Router

enum CaptureLoginEventsRouter: URLRequestConvertible {
    static let basePath = "vitality-manage-user-service-v1"

    case captureLoginEvents(tenantId: Int, partyId: Int, loginSuccess: Bool, userInstructionResponseId: String, userInstructionResponseType: String)

    var method: HTTPMethod {
        switch self {
        case .captureLoginEvents:
            return .post
        }
    }

    var route: String {
        switch self {
        case .captureLoginEvents(let tenantId, let partyId, _, _, _):
            return ("svc/\(tenantId)/captureLoginEvents/\(partyId)")
        }
    }

    var parameters: Parameters {
        switch self {
        case .captureLoginEvents( _, _, let loginSuccess, let userInstructionResponseId, let userInstructionResponseType):
            let body = CaptureLoginEventsParameters(loginSuccess: loginSuccess, userInstructionResponseId: userInstructionResponseId, userInstructionResponseType: userInstructionResponseType)
            return body.toJSON()
        }
    }

    func asURLRequest() throws -> URLRequest {
        let url = try Wire.urlForPath(path: CaptureLoginEventsRouter.basePath).appendingPathComponent(self.route)
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = self.method.rawValue
        switch self {
        case .captureLoginEvents:
            return try JSONEncoding.default.encode(urlRequest, with: self.parameters)
        }
    }
}

extension Wire.Events {
    public static func captureLoginEvents(tenantId: Int, partyId: Int, loginSuccess: Bool, userInstructionResponseId: String, userInstructionResponseType: String, completion: @escaping (_ error: Error?) -> Void) {
        let url = CaptureLoginEventsRouter.captureLoginEvents(tenantId: tenantId, partyId: partyId, loginSuccess: loginSuccess, userInstructionResponseId: userInstructionResponseId, userInstructionResponseType: userInstructionResponseType)
        Wire.sessionManager.request(url)
            .log()
            .validate(statusCode: 201 ... 201)
            .apiManagerResponse { response in
                switch response.httpStatusCode {
                case 201 ... 201:
                    completion(nil) // no response given
                default:
                    debugPrint("\(String(describing: response.result.error))")
                    completion(response.result.error ?? BackendError.other)
                }
        }
    }
}

public struct CaptureLoginEventsParameters {

    private var loginSuccess: Bool
    private var userInstructionResponseId: String
    private var userInstructionResponseType: String

    public init(loginSuccess: Bool, userInstructionResponseId: String, userInstructionResponseType: String) {
        self.loginSuccess = loginSuccess
        self.userInstructionResponseId = userInstructionResponseId
        self.userInstructionResponseType = userInstructionResponseType
    }

    public func toJSON() -> Parameters {
        let dateString = Wire.getFormattedDateWithTimezone(date: Date())
        return [
            "loginDate": dateString,
            "loginSuccess": self.loginSuccess ? "true" : "false",
            "userInstructionResponses": [
                [
                    "id": self.userInstructionResponseId,
                    "typeKey": self.userInstructionResponseType,
                    "value": self.loginSuccess
                ]
            ]
        ]
    }
}
