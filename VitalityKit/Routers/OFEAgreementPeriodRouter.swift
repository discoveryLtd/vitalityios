//
//  OFEGetEventPointsRouter.swift
//  VitalityActive
//
//  Created by Harry C. Lingad on 4/16/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireActivityLogger


// MARK: Router

enum OFEAgreementPeriodRouter: URLRequestConvertible {
    static let basePath = "vitality-agreement-utility-service-1"

    case getAgreementPeriodHistory(tenantId: Int, agreementId: Int, agreementTypeKey: Int, offsetForward: Int, offsetPast: Int)
    
    var method: HTTPMethod {
        switch self {
        case .getAgreementPeriodHistory:
            return .post
        }
    }
    
    var route: String {
        switch self {
        case .getAgreementPeriodHistory(let tenantId, _, _, _, _):
            return ("svc/\(tenantId)/getAgreementPeriod")
        }
    }
    
    var parameters: Parameters {
        switch self {
        case .getAgreementPeriodHistory(_, let agreementId, let agreementTypeKey, let offsetForward, let offsetPast):
            let body = OFEAgreementPeriodParameters(agreementId: agreementId, agreementTypeKey: agreementTypeKey, offsetForward: offsetForward, offsetPast: offsetPast)
            return body.toJSON()
        }
    }
    
    func asURLRequest() throws -> URLRequest {
        let url = try Wire.urlForPath(path: OFEAgreementPeriodRouter.basePath).appendingPathComponent(self.route)
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = self.method.rawValue
        switch self {
        case .getAgreementPeriodHistory:
            return try JSONEncoding.default.encode(urlRequest, with: self.parameters)
        }
    }
}

extension Wire.Member {
    
    // API
    public static func getAgreementPeriod(tenantId: Int, agreementId: Int, agreementTypeKey: Int, offsetForward: Int, offsetPast: Int, completion: @escaping (_ result: Any?, _ error: Error?, _ useCustomError: Bool?) -> Void){
        let url = OFEAgreementPeriodRouter.getAgreementPeriodHistory(tenantId: tenantId, agreementId: agreementId, agreementTypeKey: agreementTypeKey, offsetForward: offsetForward, offsetPast: offsetPast)
        
        Wire.sessionManager.request(url)
            .log()
            .validate(statusCode: 200 ..< 300)
            .apiManagerResponse { response in
                switch response.httpStatusCode {
                case 200 ..< 300:
                    
                    guard let result = OFEGetAgreementPeriodResponse(json: response.jsonDictionary) else {
                        print("Error")
                        completion(nil, WireError.parsing, nil)
                        return
                    }
                    
                    Parser.AgreementPeriod.parseAgreementPeriodHistory(response: result, completion: completion)
                    
                case -99999:
                    completion(nil, response.result.error ?? BackendError.other, false)
                default:
                    debugPrint("\(String(describing: response.result.error))")
                    completion(nil, response.result.error ?? BackendError.other, true)
                }
        }
        
        
    }
    
    
    //    public static func getEventPointsHistoryMock(tenantId: Int, partyId: Int, effectiveFrom: String, effectiveTo: String, completion: @escaping (_ result: Any?, _ error: Error?, _ useCustomError: Bool?) -> Void) {
    //
    //        if let path = Bundle.main.url(forResource: "pointsEvents", withExtension: "json"){
    //            if let response = parseJsonFile(url: path){
    //                print(response)
    //                guard let result = OFEHistoryResponse(json: response) else {
    //                    return completion(WireError.parsing, nil, false)
    //                }
    //                print(result)
    //
    //                //Parser.EventsPoints.parseEventsPointsHistory(response: result, completion: completion)
    //            }
    //        }else{
    //            print("no file")
    //        }
    //        return completion(nil, nil, false)
    //    }
    
    
    
    
}



//------------------------------------------

public struct OFEAgreementPeriodParameters {
    
    private var agreementId: Int
    private var agreementTypeKey: Int
    private var offsetForward: Int
    private var offsetPast: Int
    
    
    public init(agreementId: Int, agreementTypeKey: Int, offsetForward: Int, offsetPast: Int) {
        self.agreementId = agreementId
        self.agreementTypeKey = agreementTypeKey
        self.offsetForward = offsetForward
        self.offsetPast = offsetPast
        
    }
    
    public func toJSON() -> Parameters {
        
        return [  "getAgreementPeriodRequest": ["agreementId":"\(agreementId)", "agreementTypeKey": "\(agreementTypeKey)", "effectiveDate": [ "periodOffset": ["offsetForward": "\(offsetForward)", "offsetPast": "\(offsetPast)"]]
            ]
        ]
        
        
    }
}

