import Foundation
import Alamofire
import AlamofireActivityLogger
import VIAUtilities
import RealmSwift

// MARK: Router

enum ExchangeRewardRouter: URLRequestConvertible {
    static let basePath = "vitality-manage-rewards-service"
    
    case exchangeReward(tenantId: Int, awardedRewardId: Int, rewardValueLinkId: Int, type: ExchangeTypeRef)
    
    var method: HTTPMethod {
        switch self {
        case .exchangeReward:
            return .post
        }
    }
    
    var route: String {
        switch self {
        case .exchangeReward(let tenantId, let awardedRewardId, let rewardValueLinkId, _):
            return ("svc/\(tenantId)/exchangeReward/\(awardedRewardId)/\(rewardValueLinkId)")
        }
    }
    
    var queryItems: [URLQueryItem] {
        switch self {
        case .exchangeReward(_, _, _, let type):
            let typeItem = URLQueryItem(name: "exchangeTypeKey", value: "\(type.rawValue)")
            return [typeItem]
        }
    }
    
    func asURLRequest() throws -> URLRequest {
        var url = try Wire.urlForPath(path: ExchangeRewardRouter.basePath).appendingPathComponent(self.route)
        
        if var compoments = URLComponents(url: url, resolvingAgainstBaseURL: false) {
            compoments.queryItems = self.queryItems
            url = compoments.url ?? url
        }
        
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = self.method.rawValue
        
        switch self {
        case .exchangeReward:
            return try JSONEncoding.default.encode(urlRequest, with: [:])
        }
    }
}

extension Wire.Rewards {
    /** FC-26793 : UKE : Change Request
     * Added VoucherNumber to the Completion Block.
     * VoucherNumber is required to show the Voucher Code.
     */
    public static func exchangeReward(tenantId: Int, awardedRewardId: Int, rewardValueLinkId: Int, type: ExchangeTypeRef, completion: @escaping (_ error: Error?, _ newRewardId: Int?, _ voucherNumber: String?) -> Void) {
        let url = ExchangeRewardRouter.exchangeReward(tenantId: tenantId, awardedRewardId: awardedRewardId, rewardValueLinkId: rewardValueLinkId, type: type)
        Wire.sessionManager.request(url)
            .log()
            .validate(statusCode: 200 ..< 300)
            .apiManagerResponse { response in
                switch response.httpStatusCode {
                case 200 ..< 300:
                    guard let result = ExchangeRewardResponse(json: response.jsonDictionary) else {
                        return completion(WireError.parsing, nil, nil)
                    }
                    cleanupOldRewardData(with: awardedRewardId)
                    return completion(nil, result.exchangeRewardResponse?.exchangedRewardId, result.exchangeRewardResponse?.awardedRewardReferences?.first?.value)
                default:
                    debugPrint("\(String(describing: response.result.error))")
                    completion(response.result.error ?? BackendError.other, nil, nil)
                }
        }
    }
    
    static private func cleanupOldRewardData(with oldRewardId: Int) {
        do {
            let arRealm = DataProvider.newARRealm()
            arRealm.refresh()
            let oldUnclaimedRewards = arRealm.unclaimedRewards(with: oldRewardId)
            let oldSelections = arRealm.arRewardSelections(with: oldRewardId)
            let oldVouchers = arRealm.rewardVouchers(with: oldRewardId)
            var voucherCodes: [Results<ARVoucherCode>]?
            
            for voucher in oldVouchers {
                for voucherCodeValue in voucher.voucherCodes() {
                    voucherCodes?.append(arRealm.voucherCode(with: voucherCodeValue))
                }
            }
            
            try arRealm.write {
                arRealm.delete(oldUnclaimedRewards)
                arRealm.delete(oldSelections)
                arRealm.delete(oldVouchers)
                
                guard let codes = voucherCodes else {
                    return
                }
                
                for code in codes {
                    arRealm.delete(code)
                }
            }
        } catch {
            print("Unable to delete old data")
        }
    }
}
