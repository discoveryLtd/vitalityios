import Foundation
import Alamofire
import AlamofireActivityLogger

// MARK: Router

enum VDPUploadRouter: URLRequestConvertible {
    static let basePath = "vdp-api"

    case post(tenantId: Int, payload: VDPGenericUploadViewModel)

    var method: HTTPMethod {
        switch self {
        case .post:
            return .post
        }
    }

    var route: String {
        switch self {
        case .post(let tenantId, _):
            return ("/upload/tenant/\(tenantId)")
        }
    }

    func asURLRequest() throws -> URLRequest {
        let url = try Wire.urlForPath(path: VDPGetDevicesRouter.basePath).appendingPathComponent(self.route)
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = self.method.rawValue
        switch self {
        case .post(_, let payload):
            // set header fields
            urlRequest.setValue("application/json",
                                forHTTPHeaderField: "Content-Type")
            urlRequest.httpBody = payload.toJSON() //payload.data(using: .utf8, allowLossyConversion: false)
        }
        return urlRequest

    }
}
extension Wire.Events {
    public static func upload(tenantId: Int, payload: VDPGenericUploadViewModel, completion: @escaping (_ error: Error?) -> Void) {
        let url = VDPUploadRouter.post(tenantId: tenantId, payload: payload)

        Wire.sessionManager.request(url)
            .log()
            .validate(statusCode: 200 ... 200)
            .apiManagerResponse { response in
                switch response.httpStatusCode {
                case 200:
                    completion(nil)
                default:
                    completion(response.error ?? BackendError.other)
                }
        }
    }
}
