//
//  GetSAVPotentialPoints.swift
//  VitalityActive
//
//  Created by Michelle Oratil on 25/11/2017.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import Alamofire
import VIAUtilities

// MARK: Errors
public enum GetSAVPotentialPointsError: Error {
    case incorrectParameters
}

// MARK: Router

enum GetSAVPotentialPointsRouter: URLRequestConvertible {
    
    case getPotentialPoints(tenantId: Int, partyId: Int, eventTypes: [EventTypeRef], vitalityMembershipId: Int)
    
    var basePath: String {
        switch self {
        case .getPotentialPoints(_, _, _, _):
            return "vitality-event-points-services-service-1"
        }
    }
    
    var method: HTTPMethod {
        switch self {
        case .getPotentialPoints(_, _, _, _):
            return .post
        }
    }
    
    var route: String {
        switch self {
        case .getPotentialPoints(let tenantId, let partyId, _, _):
            return ("svc/\(tenantId)/getPotentialPointsAndEventsCompletedPoints/\(partyId)")
            
        }
    }
    
    var parameters: Parameters? {
        switch self {
        case .getPotentialPoints(_, _, let eventTypes, let vitalityMembershipId):
            let body = GetSAVPotentialPointsParameters(eventTypes: eventTypes, vitalityMembershipId: vitalityMembershipId)
            return body.toJSON()
        }
    }
    
    // MARK: URLRequestConvertible
    
    func asURLRequest() throws -> URLRequest {
        let url = try Wire.urlForPath(path: self.basePath).appendingPathComponent(self.route)
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = self.method.rawValue
        
        switch self {
        case .getPotentialPoints(_, _, _, _):
            return try JSONEncoding.default.encode(urlRequest, with: self.parameters)
        }
    }
}

// MARK: Networking

extension Wire.Events {
    public static func getSAVPotentialPointsAndEventsCompletedPoints(tenantId: Int, partyId: Int, deleteOldData:Bool = true,eventTypes: [EventTypeRef], vitalityMembershipId: Int, completion: @escaping (( _: Error?) -> Void)) {
        
        guard partyId != 0 else {
            return completion(GetSAVPotentialPointsError.incorrectParameters)
        }
        
        Wire.sessionManager.request(GetSAVPotentialPointsRouter.getPotentialPoints(tenantId: tenantId, partyId: partyId, eventTypes: eventTypes, vitalityMembershipId: vitalityMembershipId))
            .log()
            .validate(statusCode: 200 ..< 300)
            .apiManagerResponse { response in
                switch response.httpStatusCode {
                case 200 ..< 300:
                    
                    guard let result = GetSAVPotentialPointsResponse(json: response.jsonDictionary) else {
                        completion(WireError.parsing)
                        return
                    }
                    Parser.SAV.parseGetPotentialPoints(response: result, deleteOldData: deleteOldData, completion: completion)
                    
                case 400:
                    completion(response.result.error ?? BackendError.other)
                default:
                    completion(response.result.error ?? BackendError.other) // TODO: sort out proper error handling
                }
        }
    }
}

// MARK: Structs

public struct GetSAVPotentialPointsParameters {
    public var eventTypes: [EventTypeRef]
    public var vitalityMembershipId: Int
    
    public init(eventTypes: [EventTypeRef], vitalityMembershipId: Int) {
        self.eventTypes = eventTypes
        self.vitalityMembershipId = vitalityMembershipId
    }
    
    public func toJSON() -> Parameters {
        var events = [[String: Int]]()
        for key in eventTypes {
            events.append(["typeKey": key.rawValue])
        }
        
        var parameters: Parameters = [:]
        parameters["eventTypeses"] = events
        parameters["vitalityMembershipId"] = vitalityMembershipId
        return parameters
    }
}

