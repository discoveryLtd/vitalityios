//
//  GetPotentialPointsWithTiers.swift
//  VitalityActive
//
//  Created by Simon Stewart on 6/21/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Alamofire
import AlamofireActivityLogger

// MARK: Router

public enum GetPotentialPointsWithTiersError: Error {
    case unknown
    case parser
}

enum PotentialPointsWithTiersRouter: URLRequestConvertible {
    static let basePath = "vitality-manage-vitality-points-service-1"

    case post(tenantId: Int, partyId: Int, request: PotentialPointsWithTiersRequest)

    var method: HTTPMethod {
        switch self {
        case .post:
            return .post
        }
    }

    var route: String {
        switch self {
        case .post(let tenantId, let partyId, _):
            return ("svc/\(tenantId)/getPotentialPointsByEventType/\(partyId)")
        }
    }

    var parameters: Parameters {
        switch self {
        case .post(_, _, let request):
            return request.toJSON()
        }
    }

    func asURLRequest() throws -> URLRequest {
        let url = try Wire.urlForPath(path: PotentialPointsWithTiersRouter.basePath).appendingPathComponent(self.route)
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = self.method.rawValue
        switch self {
        case .post:
            return try JSONEncoding.default.encode(urlRequest, with: self.parameters)
        }
    }
}

public class PotentialPointsWithTiersRequest {
    var eventTypeKeys: [Int]
    public init(eventTypeKeys: [Int]) {
        self.eventTypeKeys = eventTypeKeys
    }

    public func toJSON() -> Parameters {
        var eventTypes = [[String: Any]]()
        for typeKey in self.eventTypeKeys {
            eventTypes.append(["typeKey": typeKey])
        }
        return [ "eventType": eventTypes ]
    }
}
extension Wire.Events {
    // service: vitality-manage-vitality-points-service-1
    // API:     getPotentialPointsByEventType
    public static func getPotentialPointsWithTiers(tenantId: Int, partyId: Int,
                                                   request: PotentialPointsWithTiersRequest,
                                                   completion: @escaping (_ error: Error?) -> Void) {

        let url = PotentialPointsWithTiersRouter.post(tenantId: tenantId, partyId: partyId, request: request)
        Wire.sessionManager.request(url)
            .log()
            .validate(statusCode: 200 ..< 300)
            .apiManagerResponse { response in
                switch response.httpStatusCode {
                case 200 ..< 300:
                    guard let result = PotentialPointsByEventType(json: response.jsonDictionary) else {
                        completion(GetPotentialPointsWithTiersError.parser)
                        return
                    }
                    Parser.PotentialPointsWithTiers.parsePotentialPointsWithTiers(response: result, completion: { (error) in
                        return completion(error)
                    })
                default:
                    debugPrint("\(String(describing: response.result.error))")
                    completion(response.result.error ?? BackendError.other)
                }
        }

    }
}
