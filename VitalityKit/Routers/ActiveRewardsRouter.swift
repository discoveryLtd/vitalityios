//
//  ActiveRewards.swift
//  VitalityActive
//
//  Created by Wilmar van Heerden on 2016/10/25.
//  Copyright © 2016 Glucode. All rights reserved.
//

import Foundation
import Alamofire

// MARK: Router

enum ActiveRewardsRouter: URLRequestConvertible {
    static let basePath = "vitality-agreement-goals-services-service-1/"

    case activate(tenantId: Int, partyId: Int, request: ActivateActiveRewardsRequest)

    var method: HTTPMethod {
        switch self {
        case .activate:
            return .post
        }
    }

    var route: String {
        switch self {
        case .activate(let tenantId, let partyId, _):
            return ("svc/\(tenantId)/activateActiveRewards/\(partyId)")
        }
    }
    var parameters: Parameters {
        switch self {
        case .activate(_, _, let request):
            return request.toJSON()
        }
    }
    func asURLRequest() throws -> URLRequest {
        let url = try Wire.urlForPath(path: ActiveRewardsRouter.basePath).appendingPathComponent(self.route)
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = self.method.rawValue
        switch self {
        case .activate(_, _, _):
            return try JSONEncoding.default.encode(urlRequest, with: self.parameters)
        }
    }
}

extension Wire.Rewards {
    public static func activate(tenantId: Int, partyId: Int, request: ActivateActiveRewardsRequest, completion: @escaping (_ error: Error?) -> Void) {
        let url = ActiveRewardsRouter.activate(tenantId: tenantId, partyId: partyId, request: request)
        Wire.sessionManager.request(url)
            .log()
            .validate(statusCode: 200 ..< 300)
            .apiManagerResponse { response in
                switch response.httpStatusCode {
                case 200 ..< 300:
                    guard let _ = ActivateActiveRewardsResponse(json: response.jsonDictionary) else {
                        return completion(WireError.parsing)
                    }
                    completion(nil)
                default:
                    completion(response.result.error)
                }
        }
    }
}


// MARK: Structs
public class ActivateActiveRewardsRequest {
    public var effectiveFrom: Date
    public var effectiveTo: Date

    public init(effectiveFrom: Date = Date(), effectiveTo: Date = Date.distantFuture) {
        self.effectiveFrom = effectiveFrom
        self.effectiveTo = effectiveTo
    }

    public func toJSON() -> Parameters {
        let fromString = Wire.default.simpleDateFormatter.string(from: self.effectiveFrom)
        let toString = Wire.default.simpleDateFormatter.string(from: self.effectiveTo)
        return [
            "effectivePeriod": [
                "effectiveFrom": fromString,
                "effectiveTo": toString
            ]
        ]
    }
}
