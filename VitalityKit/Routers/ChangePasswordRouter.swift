//
//  ChangePasswordRouter.swift
//  VitalityActive
//
//  Created by OJ Garde on 09/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import Alamofire
import VIAUtilities


enum ChangePasswordRouter: URLRequestConvertible {
    static let basePath = "tstc-integration-platform-services-service-v1"
    
    case changePassword(request:ChangePasswordParam)
    
    var method: HTTPMethod {
        switch self {
        case .changePassword:
            return .post
        }
    }
    
    var route: String {
        switch self {
        case .changePassword(_):
            return ("changePassword")
        }
    }
    
    var parameters: Parameters? {
        switch self {
        case .changePassword(let params):
            return params.toJSON()
        }
    }
    
    func asURLRequest() throws -> URLRequest {
        let url = try Wire.urlForPath(path: ChangePasswordRouter.basePath).appendingPathComponent(self.route)
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = self.method.rawValue
        urlRequest.setValue("application/json",
                            forHTTPHeaderField: "Content-Type")
        urlRequest.setValue("application/json",
                            forHTTPHeaderField: "Accept")
        switch self {
        case .changePassword:
            return try JSONEncoding.default.encode(urlRequest, with: parameters)
        }
    }
}

extension Wire.Party {
    
    public static func changePassword(request: ChangePasswordParam, completion: @escaping (_ success: Bool, _ responseCode: Int) -> Void) {
        enum responseCodes: Int, EnumCollection {
            case succcessCode = 200
            case genericErrorCode = 400
            case lockoutError = 40000
        }
        
        let url = ChangePasswordRouter.changePassword(request: request)
        Wire.unauthorisedSessionManager.request(url)
            .log()
            .validate(statusCode: 200 ..< 300)
            .apiManagerResponse { response in
                
                let responseCode = responseCodes(rawValue: response.httpStatusCode)
                
                switch responseCode {
                case .succcessCode?:
                    guard let newAccessToken = parseResponse(json: response.jsonDictionary as AnyObject) else{
                        return completion(false, response.httpStatusCode)
                    }
                    updateAccessToken(newAccessToken: newAccessToken)
                    completion(true, response.httpStatusCode)
                case .genericErrorCode?:
                    if let data = response.data{
                        if let json = JSONSerialization.jsonObject(data: data) {
                            let responseError = APIManagerResponseErrors.init(json: json)?.errors.first
                            if responseError?.code == String(responseCodes.lockoutError.rawValue) {
                                completion(false, responseCodes.lockoutError.rawValue)
                            } else {
                                completion(false, response.httpStatusCode)
                            }
                        } else {
                            completion(false, response.httpStatusCode)
                        }
                    } else {
                        completion(false, response.httpStatusCode)
                    }
                default:
                    completion(false, response.httpStatusCode)
                }
        }
    }
    private static func parseResponse(json: AnyObject) -> String?{
        return json["newAccessToken"] as? String
    }
    private static func updateAccessToken(newAccessToken: String){
        debugPrint("newAccesssToken>>>>\(newAccessToken)")
        Wire.setAuthorisedAPIManagerToken(token: newAccessToken)
    }
}

public struct ChangePasswordParam {
    
    public var existingPassword:String
    public var newPassword:String
    public var userName:String
    
    public init(existingPassword: String, newPassword: String, userName: String) {
        self.existingPassword   = existingPassword
        self.newPassword        = newPassword
        self.userName           = userName
    }
    
    public func toJSON() -> Parameters {
        let param = ["existingPassword"  : existingPassword,
         "newPassword"       : newPassword,
         "userName"          : userName]
        debugPrint(param)
        return param
    }
}



