//
//  ChangeUsernameRouter.swift
//  VitalityActive
//
//  Created by OJ Garde on 09/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import VIAUtilities

import Alamofire
import RealmSwift


enum ChangeUsernameRouter: URLRequestConvertible {
    static let basePath = "tstc-integration-platform-services-service-v1"
    
    case changeUsername(request: ChangeUsernameParam)
    
    var method: HTTPMethod {
        switch self {
        case .changeUsername:
            return .post
        }
    }
    
    var route: String {
        switch self {
        case .changeUsername(_):
            return ("changeUsername")
        }
    }
    
    var parameters: Parameters? {
        switch self {
        case .changeUsername(let params):
            return params.toJSON()
        }
    }
    
    func asURLRequest() throws -> URLRequest {
        let url = try Wire.urlForPath(path: ChangeUsernameRouter.basePath).appendingPathComponent(self.route)
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = self.method.rawValue
        switch self {
        case .changeUsername:
            return try JSONEncoding.default.encode(urlRequest, with: self.parameters)
        }
    }
}

extension Wire.Party {
    
    public static func changeUsername(request: ChangeUsernameParam, completion: @escaping (_ error: Error?, _ data: Data?, _ result: String?) -> Void) {
        
        let url = ChangeUsernameRouter.changeUsername(request: request)
        Wire.unauthorisedSessionManager.request(url)
            .log()
            .validate(statusCode: 200 ..< 300)
            .apiManagerResponse { response in
                debugPrint(response.httpStatusCode)
                switch response.httpStatusCode {
                case 200 ..< 300:
                    guard let newAccessToken = parseResponse(json: response.jsonDictionary as AnyObject) else{
                        return completion(WireError.parsing, response.data, nil)
                    }
                    completion(nil, response.data, newAccessToken)
                    
                    postChangeUsernameActions(newEmail: request.newUsername,
                                              oldEmail: request.existingUserName,
                                              newAccessToken: newAccessToken)
                default:
                    debugPrint("\(String(describing: response.result.error))")
                    completion(response.result.error ?? BackendError.other, response.data, nil)
                }
        }
    }
    
    private static func parseResponse(json: AnyObject) -> String?{
        return json["newAccessToken"] as? String
    }
    
    private static func postChangeUsernameActions(newEmail: String, oldEmail: String, newAccessToken: String){
        debugPrint("newAccesssToken>>>>\(newAccessToken)")
        Wire.setAuthorisedAPIManagerToken(token: newAccessToken)
        resetRealm(withNew: newEmail, oldEmail: oldEmail)
        resetCurrentUsername(withNew: newEmail)
    }
    
    private static func resetRealm(withNew newEmail: String, oldEmail: String) {
        var realm = DataProvider.newRealm()
        
        //Retrieve current party details from realm
        let currentPartyDetails = realm.objects(VitalityParty.self)
        
        //Temporary holder for new set of email addresses
        let emailAddresses:List<PartyEmailAddress> = List<PartyEmailAddress>()
        
        //Create new instance of realm for writing of updates
        realm = DataProvider.newRealm()
        try! realm.write {
            for detail in currentPartyDetails{
                for email in detail.emailAddresses{
                    if email.value == oldEmail{
                        let editedEmail = email
                        editedEmail.value = newEmail
                        emailAddresses.append(editedEmail)
                    }else{
                        emailAddresses.append(email)
                    }
                }
            }
        }
    }
    
    private static func resetCurrentUsername(withNew email: String) {
        let realm = DataProvider.newRealm()
        let users = realm.objects(User.self)
        
        // delete existing user data
        try! realm.write {
            realm.delete(users)
        }
        let user = User()
        user.username = email
        // save new user
        try! realm.write {
            realm.add(user)
        }
    }

}

public struct ChangeUsernameParam {
    public var existingUserName: String
    public var newUsername: String
    public var password: String
    
    public init(existingUserName: String, newUsername: String, password: String) {
        self.existingUserName   = existingUserName
        self.newUsername        = newUsername
        self.password           = password
    }
    
    public func toJSON() -> Parameters {
        let result = ["existingUserName"    : existingUserName,
                      "newUsername"         : newUsername,
                      "password"            : password]
        debugPrint(result)
        return result
    }
}

