//
//  GetOFEFeature.swift
//  VitalityActive
//
//  Created by Val Tomol on 16/04/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import Alamofire
import VIAUtilities

enum GetOFEFeatureRouter: URLRequestConvertible {
    case getOFEFeature(tenantId: Int, partyId: Int, vitalityMembershipId: Int)
    
    var basePath: String {
        switch self {
        case .getOFEFeature(_, _, _):
            return "vitality-manage-vitality-products-service-1"
        }
    }
    
    var method: HTTPMethod {
        switch self {
        case .getOFEFeature(_, _, _):
            return .post
        }
    }
    
    var route: String {
        switch self {
        case .getOFEFeature(let tenantId, let partyId, let vitalityMembershipId):
            return ("svc/\(tenantId)/getOFEFeature/\(partyId)/\(vitalityMembershipId)")
        }
    }
    
    func asURLRequest() throws -> URLRequest {
        let url = try Wire.urlForPath(path: self.basePath).appendingPathComponent(self.route)
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = self.method.rawValue
        urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        urlRequest.setValue("application/json", forHTTPHeaderField: "Accept")
        
        switch self {
        case .getOFEFeature(_, _, _):
            return try JSONEncoding.default.encode(urlRequest, with: nil)
        }
    }
}

extension Wire.Events {
    public static func getOFEFeature(tenantId: Int, partyId: Int, vitalityMembershipId: Int, completion: @escaping (( _: Error?) -> Void)) {
        let url = GetOFEFeatureRouter.getOFEFeature(tenantId: tenantId, partyId: partyId, vitalityMembershipId: vitalityMembershipId)
        Wire.sessionManager.request(url)
            .log()
            .validate(statusCode: 200 ..< 300)
            .apiManagerResponse { response in
                switch response.httpStatusCode {
                case 200 ..< 300:
                    guard let result = GetOFEFeatureResponse(json: response.jsonDictionary) else {
                        completion(WireError.parsing)
                        return
                    }
                    
                    // TODO: Force to delete old data?
                    Parser.OFE.parseOFEFeature(response: result, deleteOldData: true, completion: completion)
                default:
                    completion(response.result.error ?? BackendError.other)
                }
        }
    }
}
