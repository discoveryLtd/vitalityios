//
//  GetProductFeaturePointsRouter.swift
//  VitalityActive
//
//  Created by Simon Stewart on 10/10/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireActivityLogger


// MARK: Router

enum GetProductFeaturePointsRouter: URLRequestConvertible {
    static let basePath = "vitality-manage-vitality-products-service-1"
    
    case getProductFeaturePoints(tenantId: Int, partyId: Int, request: ProductFeatureRequest)
    
    var method: HTTPMethod {
        switch self {
        case .getProductFeaturePoints:
            return .post
        }
    }
    
    var route: String {
        switch self {
        case .getProductFeaturePoints(let tenantId, let partyId, _):
            return ("/svc/\(tenantId)/getProductFeaturePointsByProductFeatureCategoryType/\(partyId)")
        }
    }
    
    func asURLRequest() throws -> URLRequest {
        let url = try Wire.urlForPath(path: GetProductFeaturePointsRouter.basePath).appendingPathComponent(self.route)
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = self.method.rawValue

        switch self {
        case .getProductFeaturePoints( _, _, let request):
            let json = request.toJSON()
            return try JSONEncoding.default.encode(urlRequest, with: json)
        }
    }
}

extension Wire.Member {
    public static func getProductFeaturePoints(tenantId: Int, partyId: Int, request: ProductFeatureRequest, completion: @escaping (_ error: Error?) -> Void) {
        let url = GetProductFeaturePointsRouter.getProductFeaturePoints(tenantId: tenantId, partyId: partyId, request: request)
        Wire.sessionManager.request(url)
            .log()
            .validate(statusCode: 200 ... 200)
            .apiManagerResponse { response in
                switch response.httpStatusCode {
                case 200 ... 200:
                    guard let result = GetProductFeaturePointsResponse(json: response.jsonDictionary) else {
                        completion(WireError.parsing)
                        return
                    }
                    Parser.ProductFeaturesPoints.parseProductFeaturesPoints(response: result, completion: { error in
                        completion(error)
                    })
                    return
                default:
                    debugPrint("\(String(describing: response.result.error))")
                    completion(response.result.error ?? BackendError.other)
                }
        }
    }
}

public class ProductFeatureRequest{
    private var effectiveDate: Date
    private var productFeatureCategoryTypeKey: Int
    public init(effectiveDate: Date, productFeatureCategoryTypeKey: Int) {
        self.effectiveDate = effectiveDate
        self.productFeatureCategoryTypeKey = productFeatureCategoryTypeKey
    }
    public func toJSON() -> Parameters {
        return [
                "effectiveDate": Wire.default.simpleDateFormatter.string(from:effectiveDate),
                "productFeatureCategoryTypeKey": self.productFeatureCategoryTypeKey
            ]
    }
}
