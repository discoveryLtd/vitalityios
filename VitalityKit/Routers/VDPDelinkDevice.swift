//
//  VDPDelinkDevice.swift
//  VitalityActive
//
//  Created by Simon Stewart on 6/1/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireActivityLogger


// MARK: Router

enum VDPDelinkDevicesRouter: URLRequestConvertible {
    static let basePath = "vdp-api"

    case delete(tenantId: Int, identifier: Int, partnerSystem: String, delinkUrl: String, delinkMethod: String)

    var method: HTTPMethod {
        switch self {
        case .delete:
            return .delete
        }
    }

    var route: String {
        switch self {
        case .delete(let tenantId, let identifier, let partnerSystem, _, _):
            return ("/devices/tenant/\(tenantId)/identifier/\(identifier)/partnerSystem/\(partnerSystem)")
        }
    }
    
    var queryItems: [URLQueryItem] {
        switch self {
        case .delete(_, _, _, let delinkUrl, let delinkMethod):
            let delinkUrl = URLQueryItem(name: "delinkUrl", value: delinkUrl)
            let delinkMethod = URLQueryItem(name: "delinkMethod", value: delinkMethod)
            return [delinkUrl, delinkMethod]
        }
    }

    func asURLRequest() throws -> URLRequest {
        var url = try Wire.urlForPath(path: VDPDelinkDevicesRouter.basePath).appendingPathComponent(self.route)
        if var compoments = URLComponents(url: url, resolvingAgainstBaseURL: false) {
            compoments.queryItems = self.queryItems
            url = compoments.url ?? url
        }
        
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = self.method.rawValue
        switch self {
        case .delete:
            return try URLEncoding.default.encode(urlRequest, with: nil)
        }
    }
}

extension Wire.Events {
    public static func delinkDevice(tenantId: Int, identifier: Int, partnerSystem: String, delinkUrl: String, delinkMethod: String, completion: @escaping (_ error: Error?) -> Void) {
        let url = VDPDelinkDevicesRouter.delete(tenantId: tenantId, identifier: identifier, partnerSystem: partnerSystem, delinkUrl: delinkUrl, delinkMethod: delinkMethod)
        Wire.sessionManager.request(url)
            .log()
            .validate(statusCode: 200 ..< 300)
            .apiManagerResponse { response in
                switch response.httpStatusCode {
                case 200 ..< 300:
                    return completion(nil)
                default:
                    debugPrint("\(String(describing: response.error))")
                    completion(response.error ?? BackendError.other)
                }
        }
    }
}
