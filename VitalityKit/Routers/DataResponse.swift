//
//  DataResponse.swift
//  VitalityActive
//
//  Created by Simon Stewart on 2/14/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Alamofire

public extension DataResponse {
    public var httpStatusCode: Int {
        if let httpStatusCode = self.response?.statusCode {
            return httpStatusCode
        }
        return -99999
    }

    public var jsonDictionary: [String: Any] {
        if let response = self.result.value as? APIManagerResponse {
            return response.json
        }
        return [ : ]
    }

    public func networkingErrorOccurred() -> Bool {
        if self.result.error is BackendError, case .network(let backendError) = self.result.error as! BackendError {
            debugPrint("Backend error occurred: \(backendError)")
            return true
        }
        return false
    }
}
