//
//  ResendInsurerCodeRouter.swift
//  VitalityActive
//
//  Created by Dexter Anthony Ambrad on 3/22/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import Alamofire
import VIAUtilities

public enum ResendInsurerCodeErrorStatus: Error {
    case userAlreadyRegistered
    case userNotFound
}

enum ResendInsurerCodeRouter: URLRequestConvertible {
    static let basePath = "tstc-integration-platform-services-service-v1"
    
    case resendInsurerCode(tenantId: Int, request: ResendInsurerCodeParam)
    
    var method: HTTPMethod {
        switch self {
        case .resendInsurerCode:
            return .post
        }
    }
    
    var route: String {
        switch self {
        case .resendInsurerCode(let tenantId, _):
            return ("\(tenantId)/resendInsurerCode")
        }
    }
    
    var parameters: Parameters? {
        switch self {
        case .resendInsurerCode(_, let params):
            return params.toJSON()
        }
    }
    
    func asURLRequest() throws -> URLRequest {
        let url = try Wire.urlForPath(path: ResendInsurerCodeRouter.basePath).appendingPathComponent(self.route)
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = self.method.rawValue
        switch self {
        case .resendInsurerCode:
            return try JSONEncoding.default.encode(urlRequest, with: self.parameters)
        }
    }
}
extension Wire.Party {
    public static func resendInsurerCode(tenantId: Int, request: ResendInsurerCodeParam, completion: @escaping (_ error: Error?) -> Void) {
        let url = ResendInsurerCodeRouter.resendInsurerCode(tenantId: tenantId, request: request)
        Wire.unauthorisedSessionManager.request(url)
            .log()
            .validate(statusCode: 200 ..< 300)
            .apiManagerResponse { response in
                switch response.httpStatusCode {
                case 200 ..< 300:
                    completion(nil)
                case 400:
                    completion(ResendInsurerCodeErrorStatus.userNotFound)
                case 404:
                    completion(ResendInsurerCodeErrorStatus.userAlreadyRegistered)
                default:
                    completion(response.result.error ?? BackendError.other)
                }
        }
    }
}
public struct ResendInsurerCodeParam {
    var resendInsurerCodeRequest:ResendInsurerCodeRequest
    
    public init(eventSource: Int, username:String) {
        self.resendInsurerCodeRequest = ResendInsurerCodeRequest(eventSource: eventSource, username: username)
    }
    
    public func toJSON() -> Parameters {
        let result = ["resendInsurerCodeRequest" : resendInsurerCodeRequest.toJSON()]
        debugPrint(result)
        return result
    }
    
    struct ResendInsurerCodeRequest{
        public var eventSource:Int
        public var username:String
        
        public init(eventSource: Int, username:String) {
            self.eventSource = eventSource
            self.username = username
        }
        
        public func toJSON() -> Parameters {
            return ["eventSource"   : eventSource,
                    "username"      : username]
        }
    }
}

