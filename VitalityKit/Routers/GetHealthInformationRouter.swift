//
//  GetHealthInformationRouter.swift
//  VitalityActive
//
//  Created by Chris on 2017/08/31.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireActivityLogger
import RealmSwift
import VIAUtilities


// MARK: Router

enum GetHealthInformationRouter: URLRequestConvertible {
    static let basePath = "vitality-manage-health-service-v1"

    case get(tenantId: Int, request: GetHealthInformationRequest)

    var method: HTTPMethod {
        switch self {
        case .get:
            return .post
        }
    }

    var route: String {
        switch self {
        case .get(let tenantId, let request):
            return ("svc/\(tenantId)/getHealthInformation/\(request.partyId)")
        }
    }

    var parameters: Parameters {
        switch self {
        case .get(_, let request):
            let body = GetHealthInformationParameters(request)
            return body.toJSON()
        }
    }

    func asURLRequest() throws -> URLRequest {
        let url = try Wire.urlForPath(path: GetHealthInformationRouter.basePath).appendingPathComponent(self.route)
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = self.method.rawValue
        switch self {
        case .get:
            return try JSONEncoding.default.encode(urlRequest, with: self.parameters)
        }
    }
}

extension Wire.MyHealth {

    public func healthInformationFor(isSummary: Bool, tenantId: Int, partyId: Int, completion: @escaping (_ response: GMIReportedHealthInformation?, _ error: Error?) -> Void) {

        Wire.MyHealth.shared().healthInformationFor(
            tenantId: tenantId,
            request: GetHealthInformationRequest(
                sectionKeys: SectionRef.allValues.map({ (i) in return SectionType(sectionKey: i) }),
                isSummary: isSummary,
                partyId: partyId
                ),
            completion: completion)
    }

    public func healthInformationFor(tenantId: Int, request: GetHealthInformationRequest, completion: @escaping (_ response: GMIReportedHealthInformation?, _ error: Error?) -> Void) {

        if isWorking(){
            debugPrint("Cancelling currently running task and start a new one.")
            dataRequest?.cancel()        
        }
        
        let url = GetHealthInformationRouter.get(tenantId: tenantId, request: request)
        
        startWorking()
        dataRequest = Wire.sessionManager.request(url)
            .log()
            .validate(statusCode: 200 ..< 300)
            .apiManagerResponse { [weak self] response in
                self?.stopWorking()
                switch response.httpStatusCode {
                case 200 ..< 300:
                    guard let result = GMIGetHealthInformation(json: response.jsonDictionary) else {
                        completion(nil, WireError.parsing)
                        return
                    }
                    completion(HealthInformationRepository().save(healthInformationP: GMIReportedHealthInformation.build(response: result)), nil)
                default:
                    debugPrint("\(String(describing: response.result.error))")
                    completion(nil, response.result.error ?? BackendError.other)
                    // completion(nil, nil)
                }
        }
        dataRequest?.resume()
    }
}

public class GetHealthInformationParameters {
    var request: GetHealthInformationRequest
    public init(_ request: GetHealthInformationRequest) {
        self.request = request
    }

    public func toJSON() -> Parameters {
        return self.request.toJSON()
    }
}
public class GetHealthInformationRequest {
    var sectionKeys: [SectionType]
    var isSummary : Bool
    var partyId: Int

    public init(sectionKeys: [SectionType], isSummary: Bool, partyId: Int) {
        self.sectionKeys = sectionKeys
        self.isSummary = isSummary
        self.partyId = partyId
    }

    public func toJSON() -> Parameters {
        var sections : [Int] = []
        for val in self.sectionKeys {
            if val.sectionKey != SectionRef.Unknown {
                sections.append(val.sectionKey.rawValue)
            }
        }
        return [
            "sectionKeys": sections,
            "summary": self.isSummary
        ]
    }

}

public class SectionType {
    public var sectionKey: SectionRef

    public init(sectionKey: SectionRef) {
        self.sectionKey = sectionKey
    }

    public func toJSON() -> Parameters {
        var json = [String: Any]()
        json["sectionKey"] = self.sectionKey.rawValue
        return json
    }
}
