//
//  GetFAQRouter.swift
//  VitalityActive
//
//  Created by Val Tomol on 07/02/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireActivityLogger

enum GetFAQRouter: URLRequestConvertible {
    static let basePath = "liferay-content-management"
    
    case getFAQByTagName(tagName: String)
    case getFAQByTagNameAndKeyword(tagName: String, keyword: String)
    
    var method: HTTPMethod {
        switch self {
        case .getFAQByTagName:
            return .get
        case .getFAQByTagNameAndKeyword:
            return .get
        }
    }
    
    var route: String {
        switch self {
        case .getFAQByTagName(let tagName):
            // GET /faq-service/get-faqbytag/{tagName}
            return ("faq-service/get-faqbytag/\(tagName)")
        case .getFAQByTagNameAndKeyword(let tagName, let keyword):
            // GET /faq-service/get-faqbykey/{tagkey}/tagName/{tagName}
            return ("faq-service/get-faqbykey/\(keyword)/tagName/\(tagName)")
        }
    }
    
    func asURLRequest() throws -> URLRequest {
        let url = try Wire.urlForPath(path: GetFAQRouter.basePath).appendingPathComponent(self.route)
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = self.method.rawValue
        urlRequest.addValue(AppConfigFeature.insurerCMSGroupId()!, forHTTPHeaderField: "groupId")
        switch self {
        case .getFAQByTagName(_):
            return try URLEncoding.default.encode(urlRequest, with: nil)
        case .getFAQByTagNameAndKeyword(_):
            return try URLEncoding.default.encode(urlRequest, with: nil)
        }
    }
}

extension Wire.Content {
    public static func getFAQByTagName(tagName: String, completion: @escaping (_ error: Error?, _ data: Data?, _ result: Array<Any>?) -> Void) {
        let url = GetFAQRouter.getFAQByTagName(tagName: tagName)
        Wire.sessionManager.request(url)
            .log()
            .validate(statusCode: 200 ..< 300)
            .apiManagerResponse { response in
                switch response.httpStatusCode {
                case 200 ..< 300:
                    guard let faqArray = parseResponse(json: response.jsonDictionary as AnyObject) else {
                        return completion(WireError.parsing, response.data, nil)
                    }
                    completion(nil, response.data, faqArray)
                default:
                    completion(response.result.error ?? BackendError.other, response.data, nil)
                }
                
        }
    }
    
    public static func getFAQByTagNameAndKeyword(tagName: String, keyword: String, completion: @escaping (_ error: Error?, _ data: Data?, _ result: Array<Any>?) -> Void) {
        let url = GetFAQRouter.getFAQByTagNameAndKeyword(tagName: tagName, keyword: keyword)
        Wire.sessionManager.request(url)
            .log()
            .validate(statusCode: 200 ..< 300)
            .apiManagerResponse { response in
                switch response.httpStatusCode {
                case 200 ..< 300:
                    guard let faqArray = parseResponse(json: response.jsonDictionary as AnyObject) else {
                        return completion(WireError.parsing, response.data, nil)
                    }
                    completion(nil, response.data, faqArray)
                default:
                    completion(response.result.error ?? BackendError.other, response.data, nil)
                }
                
        }
    }
    
    private static func parseResponse(json: AnyObject) -> Array<Any>? {
        return json["FAQ"] as? Array
    }
}
