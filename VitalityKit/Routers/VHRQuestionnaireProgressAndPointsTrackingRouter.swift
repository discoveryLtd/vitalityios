import Foundation
import Alamofire
import VIAUtilities

// MARK: Router

enum VHRQuestionnaireProgressAndPointsTrackingRouter: URLRequestConvertible {

    case post(tenantId: Int, partyId: Int, VitalityMembershipId: Int, payload: VHRQuestionnaireParameter)

    var basePath: String {
        switch self {
        case .post:
            return "vitality-assessment-agreement-events-points-services-service-1/"
        }
    }

    var method: HTTPMethod {
        switch self {
        case .post:
            return .post
        }
    }

    var route: String {
        switch self {
        case .post(let tenantId, let partyId, let vitalityMembershipId, _):
            return ("svc/\(tenantId)/questionnaireProgressAndPointsTracking/\(partyId)/\(vitalityMembershipId)")
        }
    }

    var parameters: Parameters {
        switch self {
        case .post(_, _, _, let payload):
            let body = VHRQuestionnaireProgressAndPointsTrackingParameters(questionnaire: payload)
            return body.toJSON()
        }
    }

    // MARK: URLRequestConvertible

    func asURLRequest() throws -> URLRequest {
        let url = try Wire.urlForPath(path: self.basePath).appendingPathComponent(self.route)
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = self.method.rawValue
        switch self {
        case .post:
            return try JSONEncoding.default.encode(urlRequest, with: self.parameters)
        }
    }
}

// MARK: Networking

extension Wire.Member {
    public static func VHRQuestionnaireProgressAndPointsTracking(tenantId: Int, partyId: Int, vitalityMembershipId: Int, payload: VHRQuestionnaireParameter, completion: @escaping ((_: Error?) -> Void)) {
//        print("ge-->VHR: API Call")
        let url = VHRQuestionnaireProgressAndPointsTrackingRouter.post(tenantId: tenantId, partyId: partyId, VitalityMembershipId: vitalityMembershipId, payload: payload)
        Wire.sessionManager.request(url)
            .log()
            .validate(statusCode: 200 ..< 300)
            .apiManagerResponse { response in
                switch response.httpStatusCode {
                case 200 ..< 300:

                    // parse into view model
                    guard let result = QuestionnaireProgressAndPointsTracking(json: response.jsonDictionary) else {
                        return completion(WireError.parsing)
                    }
                    Parser.VHR.parseQuestionnaires(response: result, completion: completion)
                default:
                    return completion(response.result.error ?? BackendError.other)
                }
        }
    }

    //------------
    //ge20180113 : MOCK
    public static func VHRQuestionnaireProgressAndPointsTrackingMock(tenantId: Int, partyId: Int, vitalityMembershipId: Int, payload: VHRQuestionnaireParameter, completion: @escaping ((_: Error?) -> Void)) {
        print("------------\nge-->VHR: API Call MOCK")
        if let path = Bundle.main.url(forResource: "questionnaireProgressAndPointsTracking", withExtension: "json"){
            if let response = parseJsonFile(url: path){
                guard let result = QuestionnaireProgressAndPointsTracking(json: response) else {
                    return completion(WireError.parsing)
                }
                Parser.VHR.parseQuestionnaires(response: result, completion: completion)
            }
        }else{
            print("no file")
        }
        return completion(nil)
    }
    
    static func parseJsonFile(url: URL) -> [String : AnyObject]?{
        do {
            let data:Data = try Data(contentsOf: url)
            return try JSONSerialization.jsonObject(with: data, options: []) as? [String : AnyObject]
        }
        catch {
            return nil
        }
    }
    //------------

}

public struct VHRQuestionnaireParameter {
    var setTypeKey: QuestionnaireSetRef
    var prePopulation: Bool
    public init(setTypeKey: QuestionnaireSetRef, prePopulation: Bool) {
        self.setTypeKey = setTypeKey
        self.prePopulation = prePopulation
    }
}

public struct VHRQuestionnaireProgressAndPointsTrackingParameters {

    var questionnaire: VHRQuestionnaireParameter

    init(questionnaire: VHRQuestionnaireParameter) {
        self.questionnaire = questionnaire
    }

    public func toJSON() -> Parameters {

        let params: [String: Any] = [
            "questionnaireChannelTypeKey": QuestionnaireChannelRef.Mobile.rawValue,
            "questionnaireSetTypeKey": self.questionnaire.setTypeKey.rawValue,
            "prePopulation": self.questionnaire.prePopulation
        ]

        // removed the explicit definition of questionnaire sections
        // due to the fact that the product should dynamically be able
        // to add new sections into a questionnaire

        //if let questionnaireTypes = self.questionnaire.types {
        //    var types = [Parameters]()
        //    for item in questionnaireTypes {
        //        types.append(["typeKey": item.rawValue])
        //    }
        //    params["questionnaires"] = types
        //}

        return params
    }
}
