import Foundation
import UIKit


class UserAgent {

    class func toString(_ appConfigIdentifier: String = UserAgent.appConfigIdentifier()) -> String {
//        let appConfigIdentifier = UserAgent.appConfigIdentifier()
        let shortVersionString = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString")
        let bundleVersionString = Bundle.main.object(forInfoDictionaryKey: kCFBundleVersionKey as String)
        let appVersion = "\(shortVersionString!).\(bundleVersionString!)" as String
        let deviceModel = UserAgent.deviceCode() as String? ?? "Unknown"
        let OSName = UIDevice.current.systemName as String? ?? "Unknown"
        let iOSVersion = UIDevice.current.systemVersion as String? ?? "Unknown"
        let appConfigVersion = AppConfigVersion.currentAppConfigReleaseVersion()
        let userAgent = "\(appConfigIdentifier)/\(appVersion)/\(appConfigVersion) (\(deviceModel); \(OSName) \(iOSVersion))"
        return userAgent
    }

    class func appConfigIdentifier() -> String {
        let identifier = Bundle.main.object(forInfoDictionaryKey: "VAAppConfigIdentifier") as? String ?? "Unknown"
        return identifier
    }

    private static func deviceCode() -> String? {
        var name: [Int32] = [CTL_HW, HW_MACHINE]
        var size: Int = 2
        sysctl(&name, 2, nil, &size, &name, 0)
        var hw_machine = [CChar](repeating: 0, count: Int(size))
        sysctl(&name, 2, &hw_machine, &size, &name, 0)
        return String(cString: hw_machine)
    }
}
