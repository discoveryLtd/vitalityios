//
//  ActivateActiveRewardsResponse.swift
//  VitalityActive
//
//  Created by Swagger Codegen on 07/19/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file

import Foundation

public struct ActivateActiveRewardsResponse: CreatableFromJSON {
    let goalTracker: AARGoalTracker?
    init(goalTracker: AARGoalTracker?) {
        self.goalTracker = goalTracker
    }
    public init?(json: [String: Any]) {
        let goalTracker = AARGoalTracker(json: json, key: "goalTracker")
        self.init(goalTracker: goalTracker)
    }
}

public struct AARGoalTracker: CreatableFromJSON {
    let completedObjectives: Int
    let goalTypeCode: String
    let goalTypeKey: Int
    let goalTypeName: String
    let monitorUntil: String
    let objectiveTrackers: [AARObjectiveTracker]? // The recording of the goal objectives
    let percentageCompleted: Int?
    let statusDate: String
    let statusTypeCode: String
    let statusTypeKey: Int
    let statusTypeName: String
    let totalObjectives: Int
    let validFrom: String
    let validTo: String
    init(completedObjectives: Int, goalTypeCode: String, goalTypeKey: Int, goalTypeName: String, monitorUntil: String, objectiveTrackers: [AARObjectiveTracker]?, percentageCompleted: Int?, statusDate: String, statusTypeCode: String, statusTypeKey: Int, statusTypeName: String, totalObjectives: Int, validFrom: String, validTo: String) {
        self.completedObjectives = completedObjectives
        self.goalTypeCode = goalTypeCode
        self.goalTypeKey = goalTypeKey
        self.goalTypeName = goalTypeName
        self.monitorUntil = monitorUntil
        self.objectiveTrackers = objectiveTrackers
        self.percentageCompleted = percentageCompleted
        self.statusDate = statusDate
        self.statusTypeCode = statusTypeCode
        self.statusTypeKey = statusTypeKey
        self.statusTypeName = statusTypeName
        self.totalObjectives = totalObjectives
        self.validFrom = validFrom
        self.validTo = validTo
    }
    public init?(json: [String: Any]) {
        guard let completedObjectives = json["completedObjectives"] as? Int else { debugPrint("Expected non-optional property [completedObjectives] of type [Int] on object [AARGoalTracker] but did not find");return nil; }
        guard let goalTypeCode = json["goalTypeCode"] as? String else { debugPrint("Expected non-optional property [goalTypeCode] of type [String] on object [AARGoalTracker] but did not find");return nil; }
        guard let goalTypeKey = json["goalTypeKey"] as? Int else { debugPrint("Expected non-optional property [goalTypeKey] of type [Int] on object [AARGoalTracker] but did not find");return nil; }
        guard let goalTypeName = json["goalTypeName"] as? String else { debugPrint("Expected non-optional property [goalTypeName] of type [String] on object [AARGoalTracker] but did not find");return nil; }
        guard let monitorUntil = json["monitorUntil"] as? String else { debugPrint("Expected non-optional property [monitorUntil] of type [String] on object [AARGoalTracker] but did not find");return nil; }
        let objectiveTrackers = AARObjectiveTracker.createRequiredInstances(from: json, arrayKey: "objectiveTrackers")
        let percentageCompleted = json["percentageCompleted"] as? Int
        guard let statusDate = json["statusDate"] as? String else { debugPrint("Expected non-optional property [statusDate] of type [String] on object [AARGoalTracker] but did not find");return nil; }
        guard let statusTypeCode = json["statusTypeCode"] as? String else { debugPrint("Expected non-optional property [statusTypeCode] of type [String] on object [AARGoalTracker] but did not find");return nil; }
        guard let statusTypeKey = json["statusTypeKey"] as? Int else { debugPrint("Expected non-optional property [statusTypeKey] of type [Int] on object [AARGoalTracker] but did not find");return nil; }
        guard let statusTypeName = json["statusTypeName"] as? String else { debugPrint("Expected non-optional property [statusTypeName] of type [String] on object [AARGoalTracker] but did not find");return nil; }
        guard let totalObjectives = json["totalObjectives"] as? Int else { debugPrint("Expected non-optional property [totalObjectives] of type [Int] on object [AARGoalTracker] but did not find");return nil; }
        guard let validFrom = json["validFrom"] as? String else { debugPrint("Expected non-optional property [validFrom] of type [String] on object [AARGoalTracker] but did not find");return nil; }
        guard let validTo = json["validTo"] as? String else { debugPrint("Expected non-optional property [validTo] of type [String] on object [AARGoalTracker] but did not find");return nil; }
        self.init(completedObjectives: completedObjectives, goalTypeCode: goalTypeCode, goalTypeKey: goalTypeKey, goalTypeName: goalTypeName, monitorUntil: monitorUntil, objectiveTrackers: objectiveTrackers, percentageCompleted: percentageCompleted, statusDate: statusDate, statusTypeCode: statusTypeCode, statusTypeKey: statusTypeKey, statusTypeName: statusTypeName, totalObjectives: totalObjectives, validFrom: validFrom, validTo: validTo)
    }
}

public struct AARObjectiveTracker: CreatableFromJSON {
    let effectiveFrom: String
    let effectiveTo: String
    let eventCountAchieved: Int? // The count of events achieved

    let eventCountTarget: Int? // The number of events of the specific type required to achieve the objective

    let eventOutcomeAchieved: Bool? // Indicator if the event outcome has been achieved

    let eventOutcomeTarget: Int? // The target for the event outcome

    let monitorUntil: String
    let objectiveTypeCode: String
    let objectiveTypeKey: Int
    let objectiveTypeName: String
    let percentageCompleted: Int? // Percentage completion of the objective

    let pointsAchieved: Int? // The number of points achieved

    let pointsTarget: Int? // The points target

    let statusCode: String
    let statusDate: String
    let statusKey: Int
    let statusName: String
    init(effectiveFrom: String, effectiveTo: String, eventCountAchieved: Int?, eventCountTarget: Int?, eventOutcomeAchieved: Bool?, eventOutcomeTarget: Int?, monitorUntil: String, objectiveTypeCode: String, objectiveTypeKey: Int, objectiveTypeName: String, percentageCompleted: Int?, pointsAchieved: Int?, pointsTarget: Int?, statusCode: String, statusDate: String, statusKey: Int, statusName: String) {
        self.effectiveFrom = effectiveFrom
        self.effectiveTo = effectiveTo
        self.eventCountAchieved = eventCountAchieved
        self.eventCountTarget = eventCountTarget
        self.eventOutcomeAchieved = eventOutcomeAchieved
        self.eventOutcomeTarget = eventOutcomeTarget
        self.monitorUntil = monitorUntil
        self.objectiveTypeCode = objectiveTypeCode
        self.objectiveTypeKey = objectiveTypeKey
        self.objectiveTypeName = objectiveTypeName
        self.percentageCompleted = percentageCompleted
        self.pointsAchieved = pointsAchieved
        self.pointsTarget = pointsTarget
        self.statusCode = statusCode
        self.statusDate = statusDate
        self.statusKey = statusKey
        self.statusName = statusName
    }
    public init?(json: [String: Any]) {
        guard let effectiveFrom = json["effectiveFrom"] as? String else { debugPrint("Expected non-optional property [effectiveFrom] of type [String] on object [AARObjectiveTrackers] but did not find");return nil; }
        guard let effectiveTo = json["effectiveTo"] as? String else { debugPrint("Expected non-optional property [effectiveTo] of type [String] on object [AARObjectiveTrackers] but did not find");return nil; }
        let eventCountAchieved = json["eventCountAchieved"] as? Int
        let eventCountTarget = json["eventCountTarget"] as? Int
        let eventOutcomeAchieved = json["eventOutcomeAchieved"] as? Bool
        let eventOutcomeTarget = json["eventOutcomeTarget"] as? Int
        guard let monitorUntil = json["monitorUntil"] as? String else { debugPrint("Expected non-optional property [monitorUntil] of type [String] on object [AARObjectiveTrackers] but did not find");return nil; }
        guard let objectiveTypeCode = json["objectiveTypeCode"] as? String else { debugPrint("Expected non-optional property [objectiveTypeCode] of type [String] on object [AARObjectiveTrackers] but did not find");return nil; }
        guard let objectiveTypeKey = json["objectiveTypeKey"] as? Int else { debugPrint("Expected non-optional property [objectiveTypeKey] of type [Int] on object [AARObjectiveTrackers] but did not find");return nil; }
        guard let objectiveTypeName = json["objectiveTypeName"] as? String else { debugPrint("Expected non-optional property [objectiveTypeName] of type [String] on object [AARObjectiveTrackers] but did not find");return nil; }
        let percentageCompleted = json["percentageCompleted"] as? Int
        let pointsAchieved = json["pointsAchieved"] as? Int
        let pointsTarget = json["pointsTarget"] as? Int
        guard let statusCode = json["statusCode"] as? String else { debugPrint("Expected non-optional property [statusCode] of type [String] on object [AARObjectiveTrackers] but did not find");return nil; }
        guard let statusDate = json["statusDate"] as? String else { debugPrint("Expected non-optional property [statusDate] of type [String] on object [AARObjectiveTrackers] but did not find");return nil; }
        guard let statusKey = json["statusKey"] as? Int else { debugPrint("Expected non-optional property [statusKey] of type [Int] on object [AARObjectiveTrackers] but did not find");return nil; }
        guard let statusName = json["statusName"] as? String else { debugPrint("Expected non-optional property [statusName] of type [String] on object [AARObjectiveTrackers] but did not find");return nil; }
        self.init(effectiveFrom: effectiveFrom, effectiveTo: effectiveTo, eventCountAchieved: eventCountAchieved, eventCountTarget: eventCountTarget, eventOutcomeAchieved: eventOutcomeAchieved, eventOutcomeTarget: eventOutcomeTarget, monitorUntil: monitorUntil, objectiveTypeCode: objectiveTypeCode, objectiveTypeKey: objectiveTypeKey, objectiveTypeName: objectiveTypeName, percentageCompleted: percentageCompleted, pointsAchieved: pointsAchieved, pointsTarget: pointsTarget, statusCode: statusCode, statusDate: statusDate, statusKey: statusKey, statusName: statusName)
    }
}
