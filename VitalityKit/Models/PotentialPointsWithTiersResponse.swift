// manual changes have been made to this file to get around a recursive Swagger issue 
//     from the API team

//
//  PotentialPointsByEventType.swift
//  VitalityActive
//
//  Created by Swagger Codegen on 11/01/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file

import Foundation

public struct PotentialPointsByEventType: CreatableFromJSON {
    let eventType: [PPETEventType]? // Reason for awarding/not awarding the points
    init(eventType: [PPETEventType]?) {
        self.eventType = eventType
    }
    public init?(json: [String: Any]) {
        let eventType = PPETEventType.createRequiredInstances(from: json, arrayKey: "eventType")
        self.init(eventType: eventType)
    }
}

public struct PPETEventType: CreatableFromJSON {
    let potentialPointsEntries: [PPETPotentialPointsEntry]? // Reason for awarding/not awarding the points
    let totalPotentialPoints: Int? // The total potential points for the event type
    let typeCode: String? // A short name describing the; reference, association type associated to a specific key. It's purpose is to identify a unique key, in order to facilitate easy reference and understanding. The short name is used for easy identification for debugging purposes but does not necessarily exclude the option of using it as a reference in itself. For example it could be used to populate a drop down box.
    let typeKey: Int
    let typeName: String? // A detailed description of the; reference, association type associated to a specific key. Its purpose is to describe the context of the unique key in more detail, in order to facilitate easy understanding of the use of the key it describes.
    init(potentialPointsEntries: [PPETPotentialPointsEntry]?, totalPotentialPoints: Int?, typeCode: String?, typeKey: Int, typeName: String?) {
        self.potentialPointsEntries = potentialPointsEntries
        self.totalPotentialPoints = totalPotentialPoints
        self.typeCode = typeCode
        self.typeKey = typeKey
        self.typeName = typeName
    }
    public init?(json: [String: Any]) {
        let potentialPointsEntries = PPETPotentialPointsEntry.createRequiredInstances(from: json, arrayKey: "eventType")
        let totalPotentialPoints = json["totalPotentialPoints"] as? Int
        let typeCode = json["typeCode"] as? String
        guard let typeKey = json["typeKey"] as? Int else { debugPrint("Expected non-optional property [typeKey] of type [Int] on object [PPETEventType] but did not find");return nil; }
        let typeName = json["typeName"] as? String
        self.init(potentialPointsEntries: potentialPointsEntries, totalPotentialPoints: totalPotentialPoints, typeCode: typeCode, typeKey: typeKey, typeName: typeName)
    }
}

public struct PPETPotentialPointsEntry: CreatableFromJSON {
    let categoryCode: String? // A short name describing the; reference, association type associated to a specific key. It's purpose is to identify a unique key, in order to facilitate easy reference and understanding. The short name is used for easy identification for debugging purposes but does not necessarily exclude the option of using it as a reference in itself. For example it could be used to populate a drop down box.
    let categoryKey: Int
    let categoryLimit: Int?
    let categoryName: String? // A detailed description of the; reference, association type associated to a specific key. Its purpose is to describe the context of the unique key in more detail, in order to facilitate easy understanding of the use of the key it describes.
    let frequencyLimit: Int
    let maximumMembershipPeriodPoints: Int
    let pointsEntryDetails: [PPETPointsEntryDetails]? // the potential points for a specific event and points entry type
    let pointsEntryLimit: Int?
    let potentialPointsValue: Int
    let typeCode: String? // A short name describing the; reference, association type associated to a specific key. It's purpose is to identify a unique key, in order to facilitate easy reference and understanding. The short name is used for easy identification for debugging purposes but does not necessarily exclude the option of using it as a reference in itself. For example it could be used to populate a drop down box.
    let typeKey: Int
    let typeName: String? // A detailed description of the; reference, association type associated to a specific key. Its purpose is to describe the context of the unique key in more detail, in order to facilitate easy understanding of the use of the key it describes.
    init(categoryCode: String?, categoryKey: Int, categoryLimit: Int?, categoryName: String?, frequencyLimit: Int, maximumMembershipPeriodPoints: Int, pointsEntryDetails: [PPETPointsEntryDetails]?, pointsEntryLimit: Int?, potentialPointsValue: Int, typeCode: String?, typeKey: Int, typeName: String?) {
        self.categoryCode = categoryCode
        self.categoryKey = categoryKey
        self.categoryLimit = categoryLimit
        self.categoryName = categoryName
        self.frequencyLimit = frequencyLimit
        self.maximumMembershipPeriodPoints = maximumMembershipPeriodPoints
        self.pointsEntryDetails = pointsEntryDetails
        self.pointsEntryLimit = pointsEntryLimit
        self.potentialPointsValue = potentialPointsValue
        self.typeCode = typeCode
        self.typeKey = typeKey
        self.typeName = typeName
    }
    public init?(json: [String: Any]) {
        let categoryCode = json["categoryCode"] as? String
        guard let categoryKey = json["categoryKey"] as? Int else { debugPrint("Expected non-optional property [categoryKey] of type [Int] on object [PPETPotentialPointsEntry] but did not find");return nil; }
        let categoryLimit = json["categoryLimit"] as? Int
        let categoryName = json["categoryName"] as? String
        guard let frequencyLimit = json["frequencyLimit"] as? Int else { debugPrint("Expected non-optional property [frequencyLimit] of type [Int] on object [PPETPotentialPointsEntry] but did not find");return nil; }
        guard let maximumMembershipPeriodPoints = json["maximumMembershipPeriodPoints"] as? Int else { debugPrint("Expected non-optional property [maximumMembershipPeriodPoints] of type [Int] on object [PPETPotentialPointsEntry] but did not find");return nil; }
        let pointsEntryDetails = PPETPointsEntryDetails.createRequiredInstances(from: json, arrayKey: "pointsEntryDetails")
        let pointsEntryLimit = json["pointsEntryLimit"] as? Int
        guard let potentialPointsValue = json["potentialPointsValue"] as? Int else { debugPrint("Expected non-optional property [potentialPointsValue] of type [Int] on object [PPETPotentialPointsEntry] but did not find");return nil; }
        let typeCode = json["typeCode"] as? String
        guard let typeKey = json["typeKey"] as? Int else { debugPrint("Expected non-optional property [typeKey] of type [Int] on object [PPETPotentialPointsEntry] but did not find");return nil; }
        let typeName = json["typeName"] as? String
        self.init(categoryCode: categoryCode, categoryKey: categoryKey, categoryLimit: categoryLimit, categoryName: categoryName, frequencyLimit: frequencyLimit, maximumMembershipPeriodPoints: maximumMembershipPeriodPoints, pointsEntryDetails: pointsEntryDetails, pointsEntryLimit: pointsEntryLimit, potentialPointsValue: potentialPointsValue, typeCode: typeCode, typeKey: typeKey, typeName: typeName)
    }
}

public struct PPETPointsEntryDetails: CreatableFromJSON {
    let conditions: [PPETConditions]?
    let potentialPoints: Int
    init(conditions: [PPETConditions]?, potentialPoints: Int) {
        self.conditions = conditions
        self.potentialPoints = potentialPoints
    }
    public init?(json: [String: Any]) {
        let conditions = PPETConditions.createRequiredInstances(from: json, arrayKey: "conditions")
        guard let potentialPoints = json["potentialPoints"] as? Int else { debugPrint("Expected non-optional property [potentialPoints] of type [Int] on object [PPETPointsEntryDetails] but did not find");return nil; }
        self.init(conditions: conditions, potentialPoints: potentialPoints)
    }
}

public struct PPETConditions: CreatableFromJSON {
    let greaterThan: Int?
    let greaterThanOrEqualTo: Int?
    let lessThan: Int?
    let lessThanOrEqualTo: Int?
    let metadataTypeCode: String? // A short name describing the; reference, association type associated to a specific key. It's purpose is to identify a unique key, in order to facilitate easy reference and understanding. The short name is used for easy identification for debugging purposes but does not necessarily exclude the option of using it as a reference in itself. For example it could be used to populate a drop down box.
    let metadataTypeKey: Int
    let metadataTypeName: String? // A detailed description of the; reference, association type associated to a specific key. Its purpose is to describe the context of the unique key in more detail, in order to facilitate easy understanding of the use of the key it describes.
    let unitOfMeasure: String?
    init(greaterThan: Int?, greaterThanOrEqualTo: Int?, lessThan: Int?, lessThanOrEqualTo: Int?, metadataTypeCode: String?, metadataTypeKey: Int, metadataTypeName: String?, unitOfMeasure: String?) {
        self.greaterThan = greaterThan
        self.greaterThanOrEqualTo = greaterThanOrEqualTo
        self.lessThan = lessThan
        self.lessThanOrEqualTo = lessThanOrEqualTo
        self.metadataTypeCode = metadataTypeCode
        self.metadataTypeKey = metadataTypeKey
        self.metadataTypeName = metadataTypeName
        self.unitOfMeasure = unitOfMeasure
    }
    public init?(json: [String: Any]) {
        let greaterThan = json["greaterThan"] as? Int
        let greaterThanOrEqualTo = json["greaterThanOrEqualTo"] as? Int
        let lessThan = json["lessThan"] as? Int
        let lessThanOrEqualTo = json["lessThanOrEqualTo"] as? Int
        let metadataTypeCode = json["metadataTypeCode"] as? String
        guard let metadataTypeKey = json["metadataTypeKey"] as? Int else { debugPrint("Expected non-optional property [metadataTypeKey] of type [Int] on object [PPETConditions] but did not find");return nil; }
        let metadataTypeName = json["metadataTypeName"] as? String
        let unitOfMeasure = json["unitOfMeasure"] as? String
        self.init(greaterThan: greaterThan, greaterThanOrEqualTo: greaterThanOrEqualTo, lessThan: lessThan, lessThanOrEqualTo: lessThanOrEqualTo, metadataTypeCode: metadataTypeCode, metadataTypeKey: metadataTypeKey, metadataTypeName: metadataTypeName, unitOfMeasure: unitOfMeasure)
    }
}
