//
//  GoalProgressDetailsResponse.swift
//  VitalityActive
//
//  Created by Swagger Codegen on 09/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file

import Foundation

public struct GoalProgressDetailsResponse: CreatableFromJSON {
    let getGoalProgressAndDetailsResponse: GPDResponseGetGoalProgressAndDetails?
    init(getGoalProgressAndDetailsResponse: GPDResponseGetGoalProgressAndDetails?) {
        self.getGoalProgressAndDetailsResponse = getGoalProgressAndDetailsResponse
    }
    public init?(json: [String: Any]) {
        let getGoalProgressAndDetailsResponse = GPDResponseGetGoalProgressAndDetails(json: json, key: "getGoalProgressAndDetailsResponse")
        self.init(getGoalProgressAndDetailsResponse: getGoalProgressAndDetailsResponse)
    }
}

public struct GPDResponseGetGoalProgressAndDetails: CreatableFromJSON {
    let goalTrackerOuts: [GPDGoalTrackerOuts]? // Goal is the aim towards which an effort is directed.
    init(goalTrackerOuts: [GPDGoalTrackerOuts]?) {
        self.goalTrackerOuts = goalTrackerOuts
    }
    public init?(json: [String: Any]) {
        let goalTrackerOuts = GPDGoalTrackerOuts.createRequiredInstances(from: json, arrayKey: "goalTrackerOuts")
        self.init(goalTrackerOuts: goalTrackerOuts)
    }
}

public struct GPDGoalTrackerOuts: CreatableFromJSON {
    let completedObjectives: Int
    let effectiveFrom: String
    let effectiveTo: String
    let goalCode: String? // A short name describing the; reference, association type associated to a specific key. It's purpose is to identify a unique key, in order to facilitate easy reference and understanding. The short name is used for easy identification for debugging purposes but does not necessarily exclude the option of using it as a reference in itself. For example it could be used to populate a drop down box.
    let goalKey: Int
    let goalName: String? // A detailed description of the; reference, association type associated to a specific key. Its purpose is to describe the context of the unique key in more detail, in order to facilitate easy understanding of the use of the key it describes.
    let goalTrackerStatusCode: String? // A short name describing the; reference, association type associated to a specific key. It's purpose is to identify a unique key, in order to facilitate easy reference and understanding. The short name is used for easy identification for debugging purposes but does not necessarily exclude the option of using it as a reference in itself. For example it could be used to populate a drop down box.
    let goalTrackerStatusKey: Int
    let goalTrackerStatusName: String? // A detailed description of the; reference, association type associated to a specific key. Its purpose is to describe the context of the unique key in more detail, in order to facilitate easy understanding of the use of the key it describes.
    let id: Int
    let monitorUntil: String
    let objectiveTrackers: [GPDObjectiveTrackers]? // Goal is the aim towards which an effort is directed.
    let partyId: Int
    let percentageCompleted: Int
    let statusChangedOn: String
    let totalObjectives: Int
    init(completedObjectives: Int, effectiveFrom: String, effectiveTo: String, goalCode: String?, goalKey: Int, goalName: String?, goalTrackerStatusCode: String?, goalTrackerStatusKey: Int, goalTrackerStatusName: String?, id: Int, monitorUntil: String, objectiveTrackers: [GPDObjectiveTrackers]?, partyId: Int, percentageCompleted: Int, statusChangedOn: String, totalObjectives: Int) {
        self.completedObjectives = completedObjectives
        self.effectiveFrom = effectiveFrom
        self.effectiveTo = effectiveTo
        self.goalCode = goalCode
        self.goalKey = goalKey
        self.goalName = goalName
        self.goalTrackerStatusCode = goalTrackerStatusCode
        self.goalTrackerStatusKey = goalTrackerStatusKey
        self.goalTrackerStatusName = goalTrackerStatusName
        self.id = id
        self.monitorUntil = monitorUntil
        self.objectiveTrackers = objectiveTrackers
        self.partyId = partyId
        self.percentageCompleted = percentageCompleted
        self.statusChangedOn = statusChangedOn
        self.totalObjectives = totalObjectives
    }
    public init?(json: [String: Any]) {
        guard let completedObjectives = json["completedObjectives"] as? Int else { debugPrint("Expected non-optional property [completedObjectives] of type [Int] on object [GPDGoalTrackerOuts] but did not find");return nil; }
        guard let effectiveFrom = json["effectiveFrom"] as? String else { debugPrint("Expected non-optional property [effectiveFrom] of type [String] on object [GPDGoalTrackerOuts] but did not find");return nil; }
        guard let effectiveTo = json["effectiveTo"] as? String else { debugPrint("Expected non-optional property [effectiveTo] of type [String] on object [GPDGoalTrackerOuts] but did not find");return nil; }
        let goalCode = json["goalCode"] as? String
        guard let goalKey = json["goalKey"] as? Int else { debugPrint("Expected non-optional property [goalKey] of type [Int] on object [GPDGoalTrackerOuts] but did not find");return nil; }
        let goalName = json["goalName"] as? String
        let goalTrackerStatusCode = json["goalTrackerStatusCode"] as? String
        guard let goalTrackerStatusKey = json["goalTrackerStatusKey"] as? Int else { debugPrint("Expected non-optional property [goalTrackerStatusKey] of type [Int] on object [GPDGoalTrackerOuts] but did not find");return nil; }
        let goalTrackerStatusName = json["goalTrackerStatusName"] as? String
        guard let id = json["id"] as? Int else { debugPrint("Expected non-optional property [id] of type [Int] on object [GPDGoalTrackerOuts] but did not find");return nil; }
        guard let monitorUntil = json["monitorUntil"] as? String else { debugPrint("Expected non-optional property [monitorUntil] of type [String] on object [GPDGoalTrackerOuts] but did not find");return nil; }
        let objectiveTrackers = GPDObjectiveTrackers.createRequiredInstances(from: json, arrayKey: "objectiveTrackers")
        guard let partyId = json["partyId"] as? Int else { debugPrint("Expected non-optional property [partyId] of type [Int] on object [GPDGoalTrackerOuts] but did not find");return nil; }
        guard let percentageCompleted = json["percentageCompleted"] as? Int else { debugPrint("Expected non-optional property [percentageCompleted] of type [Int] on object [GPDGoalTrackerOuts] but did not find");return nil; }
        guard let statusChangedOn = json["statusChangedOn"] as? String else { debugPrint("Expected non-optional property [statusChangedOn] of type [String] on object [GPDGoalTrackerOuts] but did not find");return nil; }
        guard let totalObjectives = json["totalObjectives"] as? Int else { debugPrint("Expected non-optional property [totalObjectives] of type [Int] on object [GPDGoalTrackerOuts] but did not find");return nil; }
        self.init(completedObjectives: completedObjectives, effectiveFrom: effectiveFrom, effectiveTo: effectiveTo, goalCode: goalCode, goalKey: goalKey, goalName: goalName, goalTrackerStatusCode: goalTrackerStatusCode, goalTrackerStatusKey: goalTrackerStatusKey, goalTrackerStatusName: goalTrackerStatusName, id: id, monitorUntil: monitorUntil, objectiveTrackers: objectiveTrackers, partyId: partyId, percentageCompleted: percentageCompleted, statusChangedOn: statusChangedOn, totalObjectives: totalObjectives)
    }
}

public struct GPDObjectiveTrackers: CreatableFromJSON {
    let effectiveFrom: String
    let effectiveTo: String
    let eventCountAchieved: Int? // The count of events achieved
    
    let eventCountTarget: Int? // The number of events of the specific type required to achieve the objective
    
    let eventOutcomeAchieved: Bool? // Indicator if the event outcome has been achieved
    
    let eventOutcomeTarget: String? // v
    let events: [GPDEvents]? // An event details something that has happened or been done. Events happen at a point in time and this can be different to the point in time it became known. In the request we will need to know who reported the event, who is this applicable to and the date.
    let monitorUntil: String
    let objectiveCode: String? // A short name describing the; reference, association type associated to a specific key. It's purpose is to identify a unique key, in order to facilitate easy reference and understanding. The short name is used for easy identification for debugging purposes but does not necessarily exclude the option of using it as a reference in itself. For example it could be used to populate a drop down box.
    let objectiveKey: Int
    let objectiveName: String? // A detailed description of the; reference, association type associated to a specific key. Its purpose is to describe the context of the unique key in more detail, in order to facilitate easy understanding of the use of the key it describes.
    let objectivePointsEntries: [GPDObjectivePointsEntry]? // A points entry details how many points are earned for an event based on the evaluation of the rules configured for that event type
    let percentageCompleted: Int? // Percentage completion of the objective
    
    let pointsAchieved: Int? // The number of points achieved
    
    let pointsTarget: Int? // The points target
    
    let statusChangedOn: String
    let statusCode: String? // A short name describing the; reference, association type associated to a specific key. It's purpose is to identify a unique key, in order to facilitate easy reference and understanding. The short name is used for easy identification for debugging purposes but does not necessarily exclude the option of using it as a reference in itself. For example it could be used to populate a drop down box.
    let statusKey: Int
    let statusName: String? // A detailed description of the; reference, association type associated to a specific key. Its purpose is to describe the context of the unique key in more detail, in order to facilitate easy understanding of the use of the key it describes.
    init(effectiveFrom: String, effectiveTo: String, eventCountAchieved: Int?, eventCountTarget: Int?, eventOutcomeAchieved: Bool?, eventOutcomeTarget: String?, events: [GPDEvents]?, monitorUntil: String, objectiveCode: String?, objectiveKey: Int, objectiveName: String?, objectivePointsEntries: [GPDObjectivePointsEntry]?, percentageCompleted: Int?, pointsAchieved: Int?, pointsTarget: Int?, statusChangedOn: String, statusCode: String?, statusKey: Int, statusName: String?) {
        self.effectiveFrom = effectiveFrom
        self.effectiveTo = effectiveTo
        self.eventCountAchieved = eventCountAchieved
        self.eventCountTarget = eventCountTarget
        self.eventOutcomeAchieved = eventOutcomeAchieved
        self.eventOutcomeTarget = eventOutcomeTarget
        self.events = events
        self.monitorUntil = monitorUntil
        self.objectiveCode = objectiveCode
        self.objectiveKey = objectiveKey
        self.objectiveName = objectiveName
        self.objectivePointsEntries = objectivePointsEntries
        self.percentageCompleted = percentageCompleted
        self.pointsAchieved = pointsAchieved
        self.pointsTarget = pointsTarget
        self.statusChangedOn = statusChangedOn
        self.statusCode = statusCode
        self.statusKey = statusKey
        self.statusName = statusName
    }
    public init?(json: [String: Any]) {
        guard let effectiveFrom = json["effectiveFrom"] as? String else { debugPrint("Expected non-optional property [effectiveFrom] of type [String] on object [GPDObjectiveTrackers] but did not find");return nil; }
        guard let effectiveTo = json["effectiveTo"] as? String else { debugPrint("Expected non-optional property [effectiveTo] of type [String] on object [GPDObjectiveTrackers] but did not find");return nil; }
        let eventCountAchieved = json["eventCountAchieved"] as? Int
        let eventCountTarget = json["eventCountTarget"] as? Int
        let eventOutcomeAchieved = json["eventOutcomeAchieved"] as? Bool
        let eventOutcomeTarget = json["eventOutcomeTarget"] as? String
        let events = GPDEvents.createRequiredInstances(from: json, arrayKey: "events")
        guard let monitorUntil = json["monitorUntil"] as? String else { debugPrint("Expected non-optional property [monitorUntil] of type [String] on object [GPDObjectiveTrackers] but did not find");return nil; }
        let objectiveCode = json["objectiveCode"] as? String
        guard let objectiveKey = json["objectiveKey"] as? Int else { debugPrint("Expected non-optional property [objectiveKey] of type [Int] on object [GPDObjectiveTrackers] but did not find");return nil; }
        let objectiveName = json["objectiveName"] as? String
        let objectivePointsEntries = GPDObjectivePointsEntry.createRequiredInstances(from: json, arrayKey: "objectivePointsEntries")
        let percentageCompleted = json["percentageCompleted"] as? Int
        let pointsAchieved = json["pointsAchieved"] as? Int
        let pointsTarget = json["pointsTarget"] as? Int
        guard let statusChangedOn = json["statusChangedOn"] as? String else { debugPrint("Expected non-optional property [statusChangedOn] of type [String] on object [GPDObjectiveTrackers] but did not find");return nil; }
        let statusCode = json["statusCode"] as? String
        guard let statusKey = json["statusKey"] as? Int else { debugPrint("Expected non-optional property [statusKey] of type [Int] on object [GPDObjectiveTrackers] but did not find");return nil; }
        let statusName = json["statusName"] as? String
        self.init(effectiveFrom: effectiveFrom, effectiveTo: effectiveTo, eventCountAchieved: eventCountAchieved, eventCountTarget: eventCountTarget, eventOutcomeAchieved: eventOutcomeAchieved, eventOutcomeTarget: eventOutcomeTarget, events: events, monitorUntil: monitorUntil, objectiveCode: objectiveCode, objectiveKey: objectiveKey, objectiveName: objectiveName, objectivePointsEntries: objectivePointsEntries, percentageCompleted: percentageCompleted, pointsAchieved: pointsAchieved, pointsTarget: pointsTarget, statusChangedOn: statusChangedOn, statusCode: statusCode, statusKey: statusKey, statusName: statusName)
    }
}

public struct GPDEvents: CreatableFromJSON {
    let dateLogged: String?
    let eventDateTime: String?
    let eventMetaDatas: [GPDEventMetaDatas]? // Additional optional descriptive attributes for the event.
    let eventSourceCode: String? // A short name describing the; reference, association type associated to a specific key. It's purpose is to identify a unique key, in order to facilitate easy reference and understanding. The short name is used for easy identification for debugging purposes but does not necessarily exclude the option of using it as a reference in itself. For example it could be used to populate a drop down box.
    let eventSourceKey: Int
    let eventSourceName: String? // A detailed description of the; reference, association type associated to a specific key. Its purpose is to describe the context of the unique key in more detail, in order to facilitate easy understanding of the use of the key it describes.
    let id: Int? // A unique identifier within the system of record that describes a specific instance of an entity or attribute.
    let partyId: Int? // A unique identifier within the system of record that describes a specific instance of an entity or attribute.
    let reportedBy: Int? // This is the partyID of the party that logged the event.  This is always a party (This could be the source system of the event or the party who reported the event). For instance when a servicing agent (ReportedBy) logs an event for a party (ApplicableTo).
    
    let typeCode: String? // A short name describing the; reference, association type associated to a specific key. It's purpose is to identify a unique key, in order to facilitate easy reference and understanding. The short name is used for easy identification for debugging purposes but does not necessarily exclude the option of using it as a reference in itself. For example it could be used to populate a drop down box.
    let typeKey: Int
    let typeName: String? // A detailed description of the; reference, association type associated to a specific key. Its purpose is to describe the context of the unique key in more detail, in order to facilitate easy understanding of the use of the key it describes.
    init(dateLogged: String?, eventDateTime: String?, eventMetaDatas: [GPDEventMetaDatas]?, eventSourceCode: String?, eventSourceKey: Int, eventSourceName: String?, id: Int?, partyId: Int?, reportedBy: Int?, typeCode: String?, typeKey: Int, typeName: String?) {
        self.dateLogged = dateLogged
        self.eventDateTime = eventDateTime
        self.eventMetaDatas = eventMetaDatas
        self.eventSourceCode = eventSourceCode
        self.eventSourceKey = eventSourceKey
        self.eventSourceName = eventSourceName
        self.id = id
        self.partyId = partyId
        self.reportedBy = reportedBy
        self.typeCode = typeCode
        self.typeKey = typeKey
        self.typeName = typeName
    }
    public init?(json: [String: Any]) {
        let dateLogged = json["dateLogged"] as? String
        let eventDateTime = json["eventDateTime"] as? String
        let eventMetaDatas = GPDEventMetaDatas.createRequiredInstances(from: json, arrayKey: "eventMetaDatas")
        let eventSourceCode = json["eventSourceCode"] as? String
        guard let eventSourceKey = json["eventSourceKey"] as? Int else { debugPrint("Expected non-optional property [eventSourceKey] of type [Int] on object [GPDEvents] but did not find");return nil; }
        let eventSourceName = json["eventSourceName"] as? String
        let id = json["id"] as? Int
        let partyId = json["partyId"] as? Int
        let reportedBy = json["reportedBy"] as? Int
        let typeCode = json["typeCode"] as? String
        guard let typeKey = json["typeKey"] as? Int else { debugPrint("Expected non-optional property [typeKey] of type [Int] on object [GPDEvents] but did not find");return nil; }
        let typeName = json["typeName"] as? String
        self.init(dateLogged: dateLogged, eventDateTime: eventDateTime, eventMetaDatas: eventMetaDatas, eventSourceCode: eventSourceCode, eventSourceKey: eventSourceKey, eventSourceName: eventSourceName, id: id, partyId: partyId, reportedBy: reportedBy, typeCode: typeCode, typeKey: typeKey, typeName: typeName)
    }
}

public struct GPDEventMetaDatas: CreatableFromJSON {
    let typeCode: String? // A short name describing the; reference, association type associated to a specific key. It's purpose is to identify a unique key, in order to facilitate easy reference and understanding. The short name is used for easy identification for debugging purposes but does not necessarily exclude the option of using it as a reference in itself. For example it could be used to populate a drop down box.
    let typeKey: Int
    let typeName: String? // A detailed description of the; reference, association type associated to a specific key. Its purpose is to describe the context of the unique key in more detail, in order to facilitate easy understanding of the use of the key it describes.
    let unitOfMeasure: String? // Unit of measure of the metadata.  Not all metadata has a unit of measure
    
    let value: String
    init(typeCode: String?, typeKey: Int, typeName: String?, unitOfMeasure: String?, value: String) {
        self.typeCode = typeCode
        self.typeKey = typeKey
        self.typeName = typeName
        self.unitOfMeasure = unitOfMeasure
        self.value = value
    }
    public init?(json: [String: Any]) {
        let typeCode = json["typeCode"] as? String
        guard let typeKey = json["typeKey"] as? Int else { debugPrint("Expected non-optional property [typeKey] of type [Int] on object [GPDEventMetaDatas] but did not find");return nil; }
        let typeName = json["typeName"] as? String
        let unitOfMeasure = json["unitOfMeasure"] as? String
        guard let value = json["value"] as? String else { debugPrint("Expected non-optional property [value] of type [String] on object [GPDEventMetaDatas] but did not find");return nil; }
        self.init(typeCode: typeCode, typeKey: typeKey, typeName: typeName, unitOfMeasure: unitOfMeasure, value: value)
    }
}

public struct GPDObjectivePointsEntry: CreatableFromJSON {
    let categoryCode: String? // A short name describing the; reference, association type associated to a specific key. It's purpose is to identify a unique key, in order to facilitate easy reference and understanding.  The short name is used for easy identification for debugging purposes but does not necessarily exclude the option of using it as a reference in itself. For example it could be used to populate a drop down box.
    let categoryKey: Int
    let categoryName: String? // A detailed description of the; reference, association type associated to a specific key. Its purpose is to describe the context of the unique key in more detail, in order to facilitate easy understanding of the use of the key it describes.
    let earnedValue: Int
    let effectiveDate: String
    let eventId: Int
    let id: Int
    let partyId: Int? // Party that the points entry belongs to
    let pointsContributed: Int
    let pointsEntryMetadatas: [GPDPointsEntryMetadatas]? // Metadata provided data information about other data. In the points context this details describing the event details etc.
    let potentialValue: Int
    let prelimitValue: Int
    let reason: [GPDReason]? // Reason for awarding/not awarding the points
    let statusChangeDate: String
    let statusTypeCode: String? // A short name describing the; reference, association type associated to a specific key. It's purpose is to identify a unique key, in order to facilitate easy reference and understanding. The short name is used for easy identification for debugging purposes but does not necessarily exclude the option of using it as a reference in itself. For example it could be used to populate a drop down box.
    let statusTypeKey: Int
    let statusTypeName: String? // A detailed description of the; reference, association type associated to a specific key. Its purpose is to describe the context of the unique key in more detail, in order to facilitate easy understanding of the use of the key it describes.
    let systemAwareOn: String
    let typeCode: String? // A short name describing the; reference, association type associated to a specific key. It's purpose is to identify a unique key, in order to facilitate easy reference and understanding.  The short name is used for easy identification for debugging purposes but does not necessarily exclude the option of using it as a reference in itself. For example it could be used to populate a drop down box.
    let typeKey: Int
    let typeName: String? // A detailed description of the; reference, association type associated to a specific key. Its purpose is to describe the context of the unique key in more detail, in order to facilitate easy understanding of the use of the key it describes.
    init(categoryCode: String?, categoryKey: Int, categoryName: String?, earnedValue: Int, effectiveDate: String, eventId: Int, id: Int, partyId: Int?, pointsContributed: Int, pointsEntryMetadatas: [GPDPointsEntryMetadatas]?, potentialValue: Int, prelimitValue: Int, reason: [GPDReason]?, statusChangeDate: String, statusTypeCode: String?, statusTypeKey: Int, statusTypeName: String?, systemAwareOn: String, typeCode: String?, typeKey: Int, typeName: String?) {
        self.categoryCode = categoryCode
        self.categoryKey = categoryKey
        self.categoryName = categoryName
        self.earnedValue = earnedValue
        self.effectiveDate = effectiveDate
        self.eventId = eventId
        self.id = id
        self.partyId = partyId
        self.pointsContributed = pointsContributed
        self.pointsEntryMetadatas = pointsEntryMetadatas
        self.potentialValue = potentialValue
        self.prelimitValue = prelimitValue
        self.reason = reason
        self.statusChangeDate = statusChangeDate
        self.statusTypeCode = statusTypeCode
        self.statusTypeKey = statusTypeKey
        self.statusTypeName = statusTypeName
        self.systemAwareOn = systemAwareOn
        self.typeCode = typeCode
        self.typeKey = typeKey
        self.typeName = typeName
    }
    public init?(json: [String: Any]) {
        let categoryCode = json["categoryCode"] as? String
        guard let categoryKey = json["categoryKey"] as? Int else { debugPrint("Expected non-optional property [categoryKey] of type [Int] on object [GPDObjectivePointsEntry] but did not find");return nil; }
        let categoryName = json["categoryName"] as? String
        guard let earnedValue = json["earnedValue"] as? Int else { debugPrint("Expected non-optional property [earnedValue] of type [Int] on object [GPDObjectivePointsEntry] but did not find");return nil; }
        guard let effectiveDate = json["effectiveDate"] as? String else { debugPrint("Expected non-optional property [effectiveDate] of type [String] on object [GPDObjectivePointsEntry] but did not find");return nil; }
        guard let eventId = json["eventId"] as? Int else { debugPrint("Expected non-optional property [eventId] of type [Int] on object [GPDObjectivePointsEntry] but did not find");return nil; }
        guard let id = json["id"] as? Int else { debugPrint("Expected non-optional property [id] of type [Int] on object [GPDObjectivePointsEntry] but did not find");return nil; }
        let partyId = json["partyId"] as? Int
        guard let pointsContributed = json["pointsContributed"] as? Int else { debugPrint("Expected non-optional property [pointsContributed] of type [Int] on object [GPDObjectivePointsEntry] but did not find");return nil; }
        let pointsEntryMetadatas = GPDPointsEntryMetadatas.createRequiredInstances(from: json, arrayKey: "pointsEntryMetadatas")
        guard let potentialValue = json["potentialValue"] as? Int else { debugPrint("Expected non-optional property [potentialValue] of type [Int] on object [GPDObjectivePointsEntry] but did not find");return nil; }
        guard let prelimitValue = json["prelimitValue"] as? Int else { debugPrint("Expected non-optional property [prelimitValue] of type [Int] on object [GPDObjectivePointsEntry] but did not find");return nil; }
        let reason = GPDReason.createRequiredInstances(from: json, arrayKey: "reason")
        guard let statusChangeDate = json["statusChangeDate"] as? String else { debugPrint("Expected non-optional property [statusChangeDate] of type [String] on object [GPDObjectivePointsEntry] but did not find");return nil; }
        let statusTypeCode = json["statusTypeCode"] as? String
        guard let statusTypeKey = json["statusTypeKey"] as? Int else { debugPrint("Expected non-optional property [statusTypeKey] of type [Int] on object [GPDObjectivePointsEntry] but did not find");return nil; }
        let statusTypeName = json["statusTypeName"] as? String
        guard let systemAwareOn = json["systemAwareOn"] as? String else { debugPrint("Expected non-optional property [systemAwareOn] of type [String] on object [GPDObjectivePointsEntry] but did not find");return nil; }
        let typeCode = json["typeCode"] as? String
        guard let typeKey = json["typeKey"] as? Int else { debugPrint("Expected non-optional property [typeKey] of type [Int] on object [GPDObjectivePointsEntry] but did not find");return nil; }
        let typeName = json["typeName"] as? String
        self.init(categoryCode: categoryCode, categoryKey: categoryKey, categoryName: categoryName, earnedValue: earnedValue, effectiveDate: effectiveDate, eventId: eventId, id: id, partyId: partyId, pointsContributed: pointsContributed, pointsEntryMetadatas: pointsEntryMetadatas, potentialValue: potentialValue, prelimitValue: prelimitValue, reason: reason, statusChangeDate: statusChangeDate, statusTypeCode: statusTypeCode, statusTypeKey: statusTypeKey, statusTypeName: statusTypeName, systemAwareOn: systemAwareOn, typeCode: typeCode, typeKey: typeKey, typeName: typeName)
    }
}

public struct GPDPointsEntryMetadatas: CreatableFromJSON {
    let typeCode: String
    let typeKey: Int
    let typeName: String
    let unitOfMeasure: String?
    let value: String
    init(typeCode: String, typeKey: Int, typeName: String, unitOfMeasure: String?, value: String) {
        self.typeCode = typeCode
        self.typeKey = typeKey
        self.typeName = typeName
        self.unitOfMeasure = unitOfMeasure
        self.value = value
    }
    public init?(json: [String: Any]) {
        guard let typeCode = json["typeCode"] as? String else { debugPrint("Expected non-optional property [typeCode] of type [String] on object [GPDPointsEntryMetadatas] but did not find");return nil; }
        guard let typeKey = json["typeKey"] as? Int else { debugPrint("Expected non-optional property [typeKey] of type [Int] on object [GPDPointsEntryMetadatas] but did not find");return nil; }
        guard let typeName = json["typeName"] as? String else { debugPrint("Expected non-optional property [typeName] of type [String] on object [GPDPointsEntryMetadatas] but did not find");return nil; }
        let unitOfMeasure = json["unitOfMeasure"] as? String
        guard let value = json["value"] as? String else { debugPrint("Expected non-optional property [value] of type [String] on object [GPDPointsEntryMetadatas] but did not find");return nil; }
        self.init(typeCode: typeCode, typeKey: typeKey, typeName: typeName, unitOfMeasure: unitOfMeasure, value: value)
    }
}

public struct GPDReason: CreatableFromJSON {
    let categoryCode: String? // Identifies the reason for awarding/not awarding the points - Optional Code attribute for that CategoryKey
    
    let categoryKey: Int
    let categoryName: String? // Identifies the reason for awarding/not awarding the points - Optional Name attribute for that CategoryKey
    
    let reasonCode: String? // Identifies the reason for awarding/not awarding the points - Optional Code attribute for that ReasonKey
    
    let reasonKey: Int
    let reasonName: String? // Identifies the reason for awarding/not awarding the points - Optional Name attribute for that ReasonKey
    init(categoryCode: String?, categoryKey: Int, categoryName: String?, reasonCode: String?, reasonKey: Int, reasonName: String?) {
        self.categoryCode = categoryCode
        self.categoryKey = categoryKey
        self.categoryName = categoryName
        self.reasonCode = reasonCode
        self.reasonKey = reasonKey
        self.reasonName = reasonName
    }
    public init?(json: [String: Any]) {
        let categoryCode = json["categoryCode"] as? String
        guard let categoryKey = json["categoryKey"] as? Int else { debugPrint("Expected non-optional property [categoryKey] of type [Int] on object [GPDReason] but did not find");return nil; }
        let categoryName = json["categoryName"] as? String
        let reasonCode = json["reasonCode"] as? String
        guard let reasonKey = json["reasonKey"] as? Int else { debugPrint("Expected non-optional property [reasonKey] of type [Int] on object [GPDReason] but did not find");return nil; }
        let reasonName = json["reasonName"] as? String
        self.init(categoryCode: categoryCode, categoryKey: categoryKey, categoryName: categoryName, reasonCode: reasonCode, reasonKey: reasonKey, reasonName: reasonName)
    }
}
