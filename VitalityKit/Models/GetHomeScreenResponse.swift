//
//  GetHomeScreenResponse.swift
//  VitalityActive
//
//  Created by Swagger Codegen on 10/18/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file

import Foundation

public struct GetHomeScreenResponse: CreatableFromJSON {
    let currentStatus: GHSCurrentStatus?
    let daysLeftInMembershipPeriod: GHSDaysLeftInMembershipPeriod?
    let pointsToNextStatuses: [GHSPointsToNextStatus]? // The full list of status (Lower, current and higher) including the points threshold for each status and the points needed to reach the status
    let sections: [GHSSections]? // A section contains a group of cards on the Home Screen eg: Know Your Health
    init(currentStatus: GHSCurrentStatus?, daysLeftInMembershipPeriod: GHSDaysLeftInMembershipPeriod?, pointsToNextStatuses: [GHSPointsToNextStatus]?, sections: [GHSSections]?) {
        self.currentStatus = currentStatus
        self.daysLeftInMembershipPeriod = daysLeftInMembershipPeriod
        self.pointsToNextStatuses = pointsToNextStatuses
        self.sections = sections
    }
    public init?(json: [String: Any]) {
        let currentStatus = GHSCurrentStatus(json: json, key: "currentStatus")
        let daysLeftInMembershipPeriod = GHSDaysLeftInMembershipPeriod(json: json, key: "daysLeftInMembershipPeriod")
        let pointsToNextStatuses = GHSPointsToNextStatus.createRequiredInstances(from: json, arrayKey: "pointsToNextStatuses")
        let sections = GHSSections.createRequiredInstances(from: json, arrayKey: "sections")
        self.init(currentStatus: currentStatus, daysLeftInMembershipPeriod: daysLeftInMembershipPeriod, pointsToNextStatuses: pointsToNextStatuses, sections: sections)
    }
}

public struct GHSCurrentStatus: CreatableFromJSON {
    let carryOverStatusCode: String? // Code (my example = actual code in English = “Female” vs. actual code in French = “Femme”) A short name describing the; reference, association type associated to a specific key. It's purpose is to identify a unique key, in order to facilitate easy reference and understanding. The short name is used for easy identification for debugging purposes but does not necessarily exclude the option of using it as a reference in itself. For example it could be used to populate a drop down box.
    let carryOverStatusKey: Int? // Key (my example = “Female” = “Femme” = 1) A unique global ID that identifies a type of; reference, association, source, across the Vitality system regardless of the market the system is deployed in. Its purpose is to convey a particular meaning consistently across all market implementations, regardless of language used In other words, the use of a unique global id permits the ubiquitous use of a single piece of text describing the same meaning in the system but referenced in various languages of a specific market implementation. Example A Key numbered as "1" can be referenced as “Female” in English and “Femme” in French, but  it is applied the same within the system
    let carryOverStatusName: String? // Name (my example = “English Female” vs. “French Femme”) A detailed description of the; reference, association type associated to a specific key. Its purpose is to describe the context of the unique key in more detail, in order to facilitate easy understanding of the use of the key it describes. Value The value that applies to the reference, association, metatdata, or source
    let highestVitalityStatusCode: String? // The Highest Vitality status based on the status points thresholds E.g. Platinum
    let highestVitalityStatusKey: Int
    let highestVitalityStatusName: String? // The Highest Vitality status based on the status points thresholds E.g. Platinum
    let lowestVitalityStatusCode: String? // The Lowest Vitality Status based on the status points thresholds E.g. Bronze
    let lowestVitalityStatusKey: Int
    let lowestVitalityStatusName: String? // The Lowest Vitality Status based on the status points thresholds E.g. Bronze
    let nextVitalityStatusCode: String? // The next Vitality status after the members current Vitality Status. E.g. Member is on Bronze status. Next vitality status is Silver If the member is already on the highest Vitality status then there will not be a Next Vitality Status
    let nextVitalityStatusKey: Int? // The next Vitality status after the members current Vitality Status. E.g. Member is on Bronze status. Next vitality status is Silver If the member is already on the highest Vitality status then there will not be a Next Vitality Status
    let nextVitalityStatusName: String? // The next Vitality status after the members current Vitality Status. E.g. Member is on Bronze status. Next vitality status is Silver If the member is already on the highest Vitality status then there will not be a Next Vitality Status
    let overallVitalityStatusCode: String? // This is the member's overall Vitality Status
    let overallVitalityStatusKey: Int
    let overallVitalityStatusName: String? // This is the member's overall Vitality Status
    let pointsStatusCode: String? // The member's points status
    let pointsStatusKey: Int
    let pointsStatusName: String? // The member's points status
    let pointsToMaintainStatus: Int? // This is the points the member needs to stay in the current status
    let totalPoints: Int
    init(carryOverStatusCode: String?, carryOverStatusKey: Int?, carryOverStatusName: String?, highestVitalityStatusCode: String?, highestVitalityStatusKey: Int, highestVitalityStatusName: String?, lowestVitalityStatusCode: String?, lowestVitalityStatusKey: Int, lowestVitalityStatusName: String?, nextVitalityStatusCode: String?, nextVitalityStatusKey: Int?, nextVitalityStatusName: String?, overallVitalityStatusCode: String?, overallVitalityStatusKey: Int, overallVitalityStatusName: String?, pointsStatusCode: String?, pointsStatusKey: Int, pointsStatusName: String?, pointsToMaintainStatus: Int?, totalPoints: Int) {
        self.carryOverStatusCode = carryOverStatusCode
        self.carryOverStatusKey = carryOverStatusKey
        self.carryOverStatusName = carryOverStatusName
        self.highestVitalityStatusCode = highestVitalityStatusCode
        self.highestVitalityStatusKey = highestVitalityStatusKey
        self.highestVitalityStatusName = highestVitalityStatusName
        self.lowestVitalityStatusCode = lowestVitalityStatusCode
        self.lowestVitalityStatusKey = lowestVitalityStatusKey
        self.lowestVitalityStatusName = lowestVitalityStatusName
        self.nextVitalityStatusCode = nextVitalityStatusCode
        self.nextVitalityStatusKey = nextVitalityStatusKey
        self.nextVitalityStatusName = nextVitalityStatusName
        self.overallVitalityStatusCode = overallVitalityStatusCode
        self.overallVitalityStatusKey = overallVitalityStatusKey
        self.overallVitalityStatusName = overallVitalityStatusName
        self.pointsStatusCode = pointsStatusCode
        self.pointsStatusKey = pointsStatusKey
        self.pointsStatusName = pointsStatusName
        self.pointsToMaintainStatus = pointsToMaintainStatus
        self.totalPoints = totalPoints
    }
    public init?(json: [String: Any]) {
        let carryOverStatusCode = json["carryOverStatusCode"] as? String
        let carryOverStatusKey = json["carryOverStatusKey"] as? Int
        let carryOverStatusName = json["carryOverStatusName"] as? String
        let highestVitalityStatusCode = json["highestVitalityStatusCode"] as? String
        guard let highestVitalityStatusKey = json["highestVitalityStatusKey"] as? Int else { debugPrint("Expected non-optional property [highestVitalityStatusKey] of type [Int] on object [GHSCurrentStatus] but did not find");return nil; }
        let highestVitalityStatusName = json["highestVitalityStatusName"] as? String
        let lowestVitalityStatusCode = json["lowestVitalityStatusCode"] as? String
        guard let lowestVitalityStatusKey = json["lowestVitalityStatusKey"] as? Int else { debugPrint("Expected non-optional property [lowestVitalityStatusKey] of type [Int] on object [GHSCurrentStatus] but did not find");return nil; }
        let lowestVitalityStatusName = json["lowestVitalityStatusName"] as? String
        let nextVitalityStatusCode = json["nextVitalityStatusCode"] as? String
        let nextVitalityStatusKey = json["nextVitalityStatusKey"] as? Int
        let nextVitalityStatusName = json["nextVitalityStatusName"] as? String
        let overallVitalityStatusCode = json["overallVitalityStatusCode"] as? String
        guard let overallVitalityStatusKey = json["overallVitalityStatusKey"] as? Int else { debugPrint("Expected non-optional property [overallVitalityStatusKey] of type [Int] on object [GHSCurrentStatus] but did not find");return nil; }
        let overallVitalityStatusName = json["overallVitalityStatusName"] as? String
        let pointsStatusCode = json["pointsStatusCode"] as? String
        guard let pointsStatusKey = json["pointsStatusKey"] as? Int else { debugPrint("Expected non-optional property [pointsStatusKey] of type [Int] on object [GHSCurrentStatus] but did not find");return nil; }
        let pointsStatusName = json["pointsStatusName"] as? String
        let pointsToMaintainStatus = json["pointsToMaintainStatus"] as? Int
        guard let totalPoints = json["totalPoints"] as? Int else { debugPrint("Expected non-optional property [totalPoints] of type [Int] on object [GHSCurrentStatus] but did not find");return nil; }
        self.init(carryOverStatusCode: carryOverStatusCode, carryOverStatusKey: carryOverStatusKey, carryOverStatusName: carryOverStatusName, highestVitalityStatusCode: highestVitalityStatusCode, highestVitalityStatusKey: highestVitalityStatusKey, highestVitalityStatusName: highestVitalityStatusName, lowestVitalityStatusCode: lowestVitalityStatusCode, lowestVitalityStatusKey: lowestVitalityStatusKey, lowestVitalityStatusName: lowestVitalityStatusName, nextVitalityStatusCode: nextVitalityStatusCode, nextVitalityStatusKey: nextVitalityStatusKey, nextVitalityStatusName: nextVitalityStatusName, overallVitalityStatusCode: overallVitalityStatusCode, overallVitalityStatusKey: overallVitalityStatusKey, overallVitalityStatusName: overallVitalityStatusName, pointsStatusCode: pointsStatusCode, pointsStatusKey: pointsStatusKey, pointsStatusName: pointsStatusName, pointsToMaintainStatus: pointsToMaintainStatus, totalPoints: totalPoints)
    }
}

public struct GHSDaysLeftInMembershipPeriod: CreatableFromJSON {
    let daysRemaining: Int
    init(daysRemaining: Int) {
        self.daysRemaining = daysRemaining
    }
    public init?(json: [String: Any]) {
        guard let daysRemaining = json["daysRemaining"] as? Int else { debugPrint("Expected non-optional property [daysRemaining] of type [Int] on object [GHSDaysLeftInMembershipPeriod] but did not find");return nil; }
        self.init(daysRemaining: daysRemaining)
    }
}

public struct GHSPointsToNextStatus: CreatableFromJSON {
    let pointsNeeded: Int
    let sortOrder: Int
    let statusCode: String
    let statusKey: Int
    let statusName: String
    let statusPoints: Int
    init(pointsNeeded: Int, sortOrder: Int, statusCode: String, statusKey: Int, statusName: String, statusPoints: Int) {
        self.pointsNeeded = pointsNeeded
        self.sortOrder = sortOrder
        self.statusCode = statusCode
        self.statusKey = statusKey
        self.statusName = statusName
        self.statusPoints = statusPoints
    }
    public init?(json: [String: Any]) {
        guard let pointsNeeded = json["pointsNeeded"] as? Int else { debugPrint("Expected non-optional property [pointsNeeded] of type [Int] on object [GHSPointsToNextStatus] but did not find");return nil; }
        guard let sortOrder = json["sortOrder"] as? Int else { debugPrint("Expected non-optional property [sortOrder] of type [Int] on object [GHSPointsToNextStatus] but did not find");return nil; }
        guard let statusCode = json["statusCode"] as? String else { debugPrint("Expected non-optional property [statusCode] of type [String] on object [GHSPointsToNextStatus] but did not find");return nil; }
        guard let statusKey = json["statusKey"] as? Int else { debugPrint("Expected non-optional property [statusKey] of type [Int] on object [GHSPointsToNextStatus] but did not find");return nil; }
        guard let statusName = json["statusName"] as? String else { debugPrint("Expected non-optional property [statusName] of type [String] on object [GHSPointsToNextStatus] but did not find");return nil; }
        guard let statusPoints = json["statusPoints"] as? Int else { debugPrint("Expected non-optional property [statusPoints] of type [Int] on object [GHSPointsToNextStatus] but did not find");return nil; }
        self.init(pointsNeeded: pointsNeeded, sortOrder: sortOrder, statusCode: statusCode, statusKey: statusKey, statusName: statusName, statusPoints: statusPoints)
    }
}

public struct GHSSections: CreatableFromJSON {
    let cards: [GHSCards]? // Represents a card that is shown on a members home screen. Cards are used to inform the member about his/her progress on a member journey and to prompt the member to perform certain actions
    let priorityFlag: Bool
    let typeCode: String? // A short name describing the; reference, association type associated to a specific key. It's purpose is to identify a unique key, in order to facilitate easy reference and understanding. The short name is used for easy identification for debugging purposes but does not necessarily exclude the option of using it as a reference in itself. For example it could be used to populate a drop down box.
    let typeKey: Int
    let typeName: String? // A detailed description of the; reference, association type associated to a specific key. Its purpose is to describe the context of the unique key in more detail, in order to facilitate easy understanding of the use of the key it describes.
    init(cards: [GHSCards]?, priorityFlag: Bool, typeCode: String?, typeKey: Int, typeName: String?) {
        self.cards = cards
        self.priorityFlag = priorityFlag
        self.typeCode = typeCode
        self.typeKey = typeKey
        self.typeName = typeName
    }
    public init?(json: [String: Any]) {
        let cards = GHSCards.createRequiredInstances(from: json, arrayKey: "cards")
        guard let priorityFlag = json["priorityFlag"] as? Bool else { debugPrint("Expected non-optional property [priorityFlag] of type [Bool] on object [GHSSections] but did not find");return nil; }
        let typeCode = json["typeCode"] as? String
        guard let typeKey = json["typeKey"] as? Int else { debugPrint("Expected non-optional property [typeKey] of type [Int] on object [GHSSections] but did not find");return nil; }
        let typeName = json["typeName"] as? String
        self.init(cards: cards, priorityFlag: priorityFlag, typeCode: typeCode, typeKey: typeKey, typeName: typeName)
    }
}

public struct GHSCards: CreatableFromJSON {
    let amountCompleted: Int
    let cardItems: [GHSCardItems]? // Items is the information that appears on the card
    let cardMetadatas: [GHSCardMetadatas]? // Metadata that is associated with a card
    let priority: Int
    let statusTypeCode: String
    let statusTypeKey: Int
    let statusTypeName: String
    let total: Int
    let typeCode: String? // A short name describing the; reference, association type associated to a specific key. It's purpose is to identify a unique key, in order to facilitate easy reference and understanding. The short name is used for easy identification for debugging purposes but does not necessarily exclude the option of using it as a reference in itself. For example it could be used to populate a drop down box.
    let typeKey: Int
    let typeName: String? // A detailed description of the; reference, association type associated to a specific key. Its purpose is to describe the context of the unique key in more detail, in order to facilitate easy understanding of the use of the key it describes.
    let unitTypeCode: String? // A short name describing the; reference, association type associated to a specific key. It's purpose is to identify a unique key, in order to facilitate easy reference and understanding. The short name is used for easy identification for debugging purposes but does not necessarily exclude the option of using it as a reference in itself. For example it could be used to populate a drop down box.
    let unitTypeKey: Int
    let unitTypeName: String
    let validFrom: String
    let validTo: String
    init(amountCompleted: Int, cardItems: [GHSCardItems]?, cardMetadatas: [GHSCardMetadatas]?, priority: Int, statusTypeCode: String, statusTypeKey: Int, statusTypeName: String, total: Int, typeCode: String?, typeKey: Int, typeName: String?, unitTypeCode: String?, unitTypeKey: Int, unitTypeName: String, validFrom: String, validTo: String) {
        self.amountCompleted = amountCompleted
        self.cardItems = cardItems
        self.cardMetadatas = cardMetadatas
        self.priority = priority
        self.statusTypeCode = statusTypeCode
        self.statusTypeKey = statusTypeKey
        self.statusTypeName = statusTypeName
        self.total = total
        self.typeCode = typeCode
        self.typeKey = typeKey
        self.typeName = typeName
        self.unitTypeCode = unitTypeCode
        self.unitTypeKey = unitTypeKey
        self.unitTypeName = unitTypeName
        self.validFrom = validFrom
        self.validTo = validTo
    }
    public init?(json: [String: Any]) {
        let amountCompleted = json["amountCompleted"] as? Int ?? 0
        let cardItems = GHSCardItems.createRequiredInstances(from: json, arrayKey: "cardItems")
        let cardMetadatas = GHSCardMetadatas.createRequiredInstances(from: json, arrayKey: "cardMetadatas")
        guard let priority = json["priority"] as? Int else { debugPrint("Expected non-optional property [priority] of type [Int] on object [GHSCards] but did not find");return nil; }
        guard let statusTypeCode = json["statusTypeCode"] as? String else { debugPrint("Expected non-optional property [statusTypeCode] of type [String] on object [GHSCards] but did not find");return nil; }
        guard let statusTypeKey = json["statusTypeKey"] as? Int else { debugPrint("Expected non-optional property [statusTypeKey] of type [Int] on object [GHSCards] but did not find");return nil; }
        guard let statusTypeName = json["statusTypeName"] as? String else { debugPrint("Expected non-optional property [statusTypeName] of type [String] on object [GHSCards] but did not find");return nil; }
        let total = json["total"] as? Int ?? 0
        let typeCode = json["typeCode"] as? String
        guard let typeKey = json["typeKey"] as? Int else { debugPrint("Expected non-optional property [typeKey] of type [Int] on object [GHSCards] but did not find");return nil; }
        let typeName = json["typeName"] as? String
        let unitTypeCode = json["unitTypeCode"] as? String
        guard let unitTypeKey = json["unitTypeKey"] as? Int else { debugPrint("Expected non-optional property [unitTypeKey] of type [Int] on object [GHSCards] but did not find");return nil; }
        guard let unitTypeName = json["unitTypeName"] as? String else { debugPrint("Expected non-optional property [unitTypeName] of type [String] on object [GHSCards] but did not find");return nil; }
        guard let validFrom = json["validFrom"] as? String else { debugPrint("Expected non-optional property [validFrom] of type [String] on object [GHSCards] but did not find");return nil; }
        guard let validTo = json["validTo"] as? String else { debugPrint("Expected non-optional property [validTo] of type [String] on object [GHSCards] but did not find");return nil; }
        self.init(amountCompleted: amountCompleted, cardItems: cardItems, cardMetadatas: cardMetadatas, priority: priority, statusTypeCode: statusTypeCode, statusTypeKey: statusTypeKey, statusTypeName: statusTypeName, total: total, typeCode: typeCode, typeKey: typeKey, typeName: typeName, unitTypeCode: unitTypeCode, unitTypeKey: unitTypeKey, unitTypeName: unitTypeName, validFrom: validFrom, validTo: validTo)
    }
}

public struct GHSCardItems: CreatableFromJSON {
    let cardItemMetadatas: [GHSCardItemMetadatas]? // Metadata that is associated with an Item
    let statusTypeCode: String? // A short name describing the; reference, association type associated to a specific key. It's purpose is to identify a unique key, in order to facilitate easy reference and understanding. The short name is used for easy identification for debugging purposes but does not necessarily exclude the option of using it as a reference in itself. For example it could be used to populate a drop down box.
    let statusTypeKey: Int
    let statusTypeName: String? // A detailed description of the; reference, association type associated to a specific key. Its purpose is to describe the context of the unique key in more detail, in order to facilitate easy understanding of the use of the key it describes.
    let typeCode: String? // A short name describing the; reference, association type associated to a specific key. It's purpose is to identify a unique key, in order to facilitate easy reference and understanding. The short name is used for easy identification for debugging purposes but does not necessarily exclude the option of using it as a reference in itself. For example it could be used to populate a drop down box.
    let typeKey: Int
    let typeName: String? // A detailed description of the; reference, association type associated to a specific key. Its purpose is to describe the context of the unique key in more detail, in order to facilitate easy understanding of the use of the key it describes.
    let validFrom: String
    let validTo: String
    init(cardItemMetadatas: [GHSCardItemMetadatas]?, statusTypeCode: String?, statusTypeKey: Int, statusTypeName: String?, typeCode: String?, typeKey: Int, typeName: String?, validFrom: String, validTo: String) {
        self.cardItemMetadatas = cardItemMetadatas
        self.statusTypeCode = statusTypeCode
        self.statusTypeKey = statusTypeKey
        self.statusTypeName = statusTypeName
        self.typeCode = typeCode
        self.typeKey = typeKey
        self.typeName = typeName
        self.validFrom = validFrom
        self.validTo = validTo
    }
    public init?(json: [String: Any]) {
        let cardItemMetadatas = GHSCardItemMetadatas.createRequiredInstances(from: json, arrayKey: "cardItemMetadatas")
        let statusTypeCode = json["statusTypeCode"] as? String
        guard let statusTypeKey = json["statusTypeKey"] as? Int else { debugPrint("Expected non-optional property [statusTypeKey] of type [Int] on object [GHSCardItems] but did not find");return nil; }
        let statusTypeName = json["statusTypeName"] as? String
        let typeCode = json["typeCode"] as? String
        guard let typeKey = json["typeKey"] as? Int else { debugPrint("Expected non-optional property [typeKey] of type [Int] on object [GHSCardItems] but did not find");return nil; }
        let typeName = json["typeName"] as? String
        guard let validFrom = json["validFrom"] as? String else { debugPrint("Expected non-optional property [validFrom] of type [String] on object [GHSCardItems] but did not find");return nil; }
        guard let validTo = json["validTo"] as? String else { debugPrint("Expected non-optional property [validTo] of type [String] on object [GHSCardItems] but did not find");return nil; }
        self.init(cardItemMetadatas: cardItemMetadatas, statusTypeCode: statusTypeCode, statusTypeKey: statusTypeKey, statusTypeName: statusTypeName, typeCode: typeCode, typeKey: typeKey, typeName: typeName, validFrom: validFrom, validTo: validTo)
    }
}

public struct GHSCardItemMetadatas: CreatableFromJSON {
    let typeCode: String? // A short name describing the; reference, association type associated to a specific key. It's purpose is to identify a unique key, in order to facilitate easy reference and understanding. The short name is used for easy identification for debugging purposes but does not necessarily exclude the option of using it as a reference in itself. For example it could be used to populate a drop down box.
    let typeKey: Int
    let typeName: String? // A detailed description of the; reference, association type associated to a specific key. Its purpose is to describe the context of the unique key in more detail, in order to facilitate easy understanding of the use of the key it describes.
    let value: String
    init(typeCode: String?, typeKey: Int, typeName: String?, value: String) {
        self.typeCode = typeCode
        self.typeKey = typeKey
        self.typeName = typeName
        self.value = value
    }
    public init?(json: [String: Any]) {
        let typeCode = json["typeCode"] as? String
        guard let typeKey = json["typeKey"] as? Int else { debugPrint("Expected non-optional property [typeKey] of type [Int] on object [GHSCardItemMetadatas] but did not find");return nil; }
        let typeName = json["typeName"] as? String
        guard let value = json["value"] as? String else { debugPrint("Expected non-optional property [value] of type [String] on object [GHSCardItemMetadatas] but did not find");return nil; }
        self.init(typeCode: typeCode, typeKey: typeKey, typeName: typeName, value: value)
    }
}

public struct GHSCardMetadatas: CreatableFromJSON {
    let typeCode: String? // A short name describing the; reference, association type associated to a specific key. It's purpose is to identify a unique key, in order to facilitate easy reference and understanding. The short name is used for easy identification for debugging purposes but does not necessarily exclude the option of using it as a reference in itself. For example it could be used to populate a drop down box.
    let typeKey: Int
    let typeName: String? // A detailed description of the; reference, association type associated to a specific key. Its purpose is to describe the context of the unique key in more detail, in order to facilitate easy understanding of the use of the key it describes.
    let value: String
    init(typeCode: String?, typeKey: Int, typeName: String?, value: String) {
        self.typeCode = typeCode
        self.typeKey = typeKey
        self.typeName = typeName
        self.value = value
    }
    public init?(json: [String: Any]) {
        let typeCode = json["typeCode"] as? String
        guard let typeKey = json["typeKey"] as? Int else { debugPrint("Expected non-optional property [typeKey] of type [Int] on object [GHSCardMetadatas] but did not find");return nil; }
        let typeName = json["typeName"] as? String
        //TODO:
        //guard let value = json["value"] as? String else { debugPrint("Expected non-optional property [value] of type [String] on object [GHSCardMetadatas] but did not find");return nil; }
        let value = json["value"] as? String ?? ""
        self.init(typeCode: typeCode, typeKey: typeKey, typeName: typeName, value: value)
    }
}
