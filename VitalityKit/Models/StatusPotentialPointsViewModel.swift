//
//  StatusPotentialPointsViewModel.swift
//  VitalityActive
//
//  Created by admin on 2017/11/06.
//  Copyright © 2017 Glucode. All rights reserved.
//

import UIKit

public class StatusPotentialPointsViewModel: VIAWellnessDevicesPointsEarningPotentialViewModel {
    let statusRealm = DataProvider.newStatusRealm()
    internal var statusEventType: StatusPointsEntry? {
        get {
            if let eventsTypeEntry = statusRealm.statusPointsEntry(with: earningActivityKey).first {
                return eventsTypeEntry
            }
            return nil
        }
    }
    
    public func statusItem(at index: Int) -> StatusPotentialPointsWithTiersEntryDetails? {
        if index < self.statusEventType?.potentialPointsEntries.count ?? 0 {
            return self.statusEventType?.potentialPointsEntries[index]
        }
        return nil
    }
    
    public override func numberOfPotentialPointsItems() -> Int {
        return self.statusEventType?.potentialPointsEntries.count ?? 1
    }
    
    public override func pointValueForItem(at index: Int) -> String {
        guard let pointValue = statusItem(at: index)?.potentialPoints.value else {
            return ""
        }
        return String(describing: pointValue)
    }
    
    public override func pointEarningPotentialItemPoints(for index: Int) -> String {
        if let potentialPoints = statusItem(at:index)?.potentialPoints.value {
            return string(for: potentialPoints)
        } else if let totalEventPoints = self.eventType?.potentialPointsValue.value {
            return string(for: totalEventPoints)
        }
        return ""
    }
    
    override func string(for potentialPoints: Int) -> String {
        var pointValue = ""
        pointValue = String(describing: potentialPoints)
        pointValue.append(" ")
        pointValue.append(CommonStrings.PotentialPoints.SectionHeader495)
        return pointValue
    }
    
    public override func conditions(for index: Int) -> Array<Conditions> {
        guard let conditions = statusItem(at:index)?.conditions else {
            return []
        }
        return Array(conditions)
    }
}
