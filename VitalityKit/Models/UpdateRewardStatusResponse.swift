//
//  UpdateRewardStatusResponse.swift
//  VitalityActive
//
//  Created by Swagger Codegen on 11/22/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file

import Foundation

public struct UpdateRewardStatusResponse: CreatableFromJSON {
    let awardedRewardId: Int
    init(awardedRewardId: Int) {
        self.awardedRewardId = awardedRewardId
    }
    public init?(json: [String: Any]) {
        guard let awardedRewardId = json["awardedRewardId"] as? Int else { debugPrint("Expected non-optional property [awardedRewardId] of type [Int] on object [UpdateRewardStatusResponse] but did not find");return nil; }
        self.init(awardedRewardId: awardedRewardId)
    }
}
