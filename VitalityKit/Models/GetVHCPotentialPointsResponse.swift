//
//  GetVHCPotentialPointsResponse.swift
//  VitalityActive
//
//  Created by Swagger Codegen on 08/22/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file

import Foundation

public struct GetVHCPotentialPointsResponse: CreatableFromJSON {
    let eventType: [GPPEEventType]? // The requested event type
    let warnings: [GPPEWarnings]? // A list of warnings that were generated in the process Warnings are only generated when a downstream service fails, but the response for the entire service is successful
    init(eventType: [GPPEEventType]?, warnings: [GPPEWarnings]?) {
        self.eventType = eventType
        self.warnings = warnings
    }
    public init?(json: [String: Any]) {
        let eventType = GPPEEventType.createRequiredInstances(from: json, arrayKey: "eventType")
        let warnings = GPPEWarnings.createRequiredInstances(from: json, arrayKey: "warnings")
        self.init(eventType: eventType, warnings: warnings)
    }
}

public struct GPPEEventType: CreatableFromJSON {
    let categoryCode: String? // A short name describing the; reference, association type associated to a specific key. It's purpose is to identify a unique key, in order to facilitate easy reference and understanding. The short name is used for easy identification for debugging purposes but does not necessarily exclude the option of using it as a reference in itself. For example it could be used to populate a drop down box.
    let categoryKey: Int
    let categoryName: String? // A detailed description of the; reference, association type associated to a specific key. Its purpose is to describe the context of the unique key in more detail, in order to facilitate easy understanding of the use of the key it describes.
    let event: [GPPEEvent]? // The requested event
    let healthAttribute: [GPPEHealthAttribute]? // The valid value ranges for the health attribute type
    let reasonCode: String? // Net reason for points awarded or not awarded for the event.

    let reasonKey: Int?
    let reasonName: String? // Net reason for points awarded or not awarded for the event.

    let totalEarnedPoints: Int?
    let totalPotentialPoints: Int? // The total potential points that can be earned for completing the event type

    let typeCode: String? // A short name describing the; reference, association type associated to a specific key. It's purpose is to identify a unique key, in order to facilitate easy reference and understanding. The short name is used for easy identification for debugging purposes but does not necessarily exclude the option of using it as a reference in itself. For example it could be used to populate a drop down box.
    let typeKey: Int
    let typeName: String? // A detailed description of the; reference, association type associated to a specific key. Its purpose is to describe the context of the unique key in more detail, in order to facilitate easy understanding of the use of the key it describes.
    init(categoryCode: String?, categoryKey: Int, categoryName: String?, event: [GPPEEvent]?, healthAttribute: [GPPEHealthAttribute]?, reasonCode: String?, reasonKey: Int?, reasonName: String?, totalEarnedPoints: Int?, totalPotentialPoints: Int?, typeCode: String?, typeKey: Int, typeName: String?) {
        self.categoryCode = categoryCode
        self.categoryKey = categoryKey
        self.categoryName = categoryName
        self.event = event
        self.healthAttribute = healthAttribute
        self.reasonCode = reasonCode
        self.reasonKey = reasonKey
        self.reasonName = reasonName
        self.totalEarnedPoints = totalEarnedPoints
        self.totalPotentialPoints = totalPotentialPoints
        self.typeCode = typeCode
        self.typeKey = typeKey
        self.typeName = typeName
    }
    public init?(json: [String: Any]) {
        let categoryCode = json["categoryCode"] as? String
        guard let categoryKey = json["categoryKey"] as? Int else { debugPrint("Expected non-optional property [categoryKey] of type [Int] on object [GPPEEventType] but did not find");return nil; }
        let categoryName = json["categoryName"] as? String
        let event = GPPEEvent.createRequiredInstances(from: json, arrayKey: "event")
        let healthAttribute = GPPEHealthAttribute.createRequiredInstances(from: json, arrayKey: "healthAttribute")
        let reasonCode = json["reasonCode"] as? String
        let reasonKey = json["reasonKey"] as? Int
        let reasonName = json["reasonName"] as? String
        let totalEarnedPoints = json["totalEarnedPoints"] as? Int
        let totalPotentialPoints = json["totalPotentialPoints"] as? Int
        let typeCode = json["typeCode"] as? String
        guard let typeKey = json["typeKey"] as? Int else { debugPrint("Expected non-optional property [typeKey] of type [Int] on object [GPPEEventType] but did not find");return nil; }
        let typeName = json["typeName"] as? String
        self.init(categoryCode: categoryCode, categoryKey: categoryKey, categoryName: categoryName, event: event, healthAttribute: healthAttribute, reasonCode: reasonCode, reasonKey: reasonKey, reasonName: reasonName, totalEarnedPoints: totalEarnedPoints, totalPotentialPoints: totalPotentialPoints, typeCode: typeCode, typeKey: typeKey, typeName: typeName)
    }
}

public struct GPPEEvent: CreatableFromJSON {
    let eventDateTime: String? // The date and time of the when the event was occured.


    let eventId: Int? // The unique system generated ID for events.

    let eventMetaDataType: [GPPEEventMetaDataType]? // The type for event meta data
    let eventSource: GPPEEventSource?
    let healthAttributeReadings: [GPPEHealthAttributeReadings]? // An instance of a Health Attribute details the value of a specific attribute type for a Party. Health Attributes should only be used to information about specific attribute values which Vitality is interested in. Health Attributes are separate from Assessment answers. Attributes can be supplied direct to Health Attributes, assessments are only 1 of the mechanisms to obtain Health attribute data
    let pointsEntries: [GPPEPointsEntry]? // A points entry details how many points are earned for an event based on the evaluation of the rules configured for that event type
    init(eventDateTime: String?, eventId: Int?, eventMetaDataType: [GPPEEventMetaDataType]?, eventSource: GPPEEventSource?, healthAttributeReadings: [GPPEHealthAttributeReadings]?, pointsEntries: [GPPEPointsEntry]?) {
        self.eventDateTime = eventDateTime
        self.eventId = eventId
        self.eventMetaDataType = eventMetaDataType
        self.eventSource = eventSource
        self.healthAttributeReadings = healthAttributeReadings
        self.pointsEntries = pointsEntries
    }
    public init?(json: [String: Any]) {
        let eventDateTime = json["eventDateTime"] as? String
        let eventId = json["eventId"] as? Int
        let eventMetaDataType = GPPEEventMetaDataType.createRequiredInstances(from: json, arrayKey: "eventMetaDataType")
        let eventSource = GPPEEventSource(json: json, key: "eventSource")
        let healthAttributeReadings = GPPEHealthAttributeReadings.createRequiredInstances(from: json, arrayKey: "healthAttributeReadings")
        let pointsEntries = GPPEPointsEntry.createRequiredInstances(from: json, arrayKey: "pointsEntries")
        self.init(eventDateTime: eventDateTime, eventId: eventId, eventMetaDataType: eventMetaDataType, eventSource: eventSource, healthAttributeReadings: healthAttributeReadings, pointsEntries: pointsEntries)
    }
}

public struct GPPEEventMetaDataType: CreatableFromJSON {
    let code: String? // The name of the event metadata type

    let key: Int? // A code that uniquely identifies the event metadata type

    let name: String? // The name of the event metadata type

    let note: String? // A note of additional information applicable to the specific event metadata type

    let unitOfMeasure: String? // Unit of measure of the metadata type

    let value: String? // The value of the metadata

    init(code: String?, key: Int?, name: String?, note: String?, unitOfMeasure: String?, value: String?) {
        self.code = code
        self.key = key
        self.name = name
        self.note = note
        self.unitOfMeasure = unitOfMeasure
        self.value = value
    }
    public init?(json: [String: Any]) {
        let code = json["code"] as? String
        let key = json["key"] as? Int
        let name = json["name"] as? String
        let note = json["note"] as? String
        let unitOfMeasure = json["unitOfMeasure"] as? String
        let value = json["value"] as? String
        self.init(code: code, key: key, name: name, note: note, unitOfMeasure: unitOfMeasure, value: value)
    }
}

public struct GPPEEventSource: CreatableFromJSON {
    let eventSourceCode: String? // Description/name of the source

    let eventSourceKey: Int
    let eventSourceName: String? // Description/name of the source

    let note: String? // A note of additional information applicable to the specific event source

    init(eventSourceCode: String?, eventSourceKey: Int, eventSourceName: String?, note: String?) {
        self.eventSourceCode = eventSourceCode
        self.eventSourceKey = eventSourceKey
        self.eventSourceName = eventSourceName
        self.note = note
    }
    public init?(json: [String: Any]) {
        let eventSourceCode = json["eventSourceCode"] as? String
        guard let eventSourceKey = json["eventSourceKey"] as? Int else { debugPrint("Expected non-optional property [eventSourceKey] of type [Int] on object [GPPEEventSource] but did not find");return nil; }
        let eventSourceName = json["eventSourceName"] as? String
        let note = json["note"] as? String
        self.init(eventSourceCode: eventSourceCode, eventSourceKey: eventSourceKey, eventSourceName: eventSourceName, note: note)
    }
}

public struct GPPEHealthAttributeReadings: CreatableFromJSON {
    let healthAttributeFeedbacks: [GPPEHealthAttributeFeedbacks]? // An instance of a Health Attribute details the value of a specific attribute type for a Party. Health Attributes should only be used to information about specific attribute values which Vitality is interested in. Health Attributes are separate from Assessment answers. Attributes can be supplied direct to Health Attributes, assessments are only 1 of the mechanisms to obtain Health attribute data
    let healthAttributeTypeCode: String? // A short name describing the; reference, association type associated to a specific key. It's purpose is to identify a unique key, in order to facilitate easy reference and understanding. The short name is used for easy identification for debugging purposes but does not necessarily exclude the option of using it as a reference in itself. For example it could be used to populate a drop down box.
    let healthAttributeTypeKey: Int
    let healthAttributeTypeName: String? // A detailed description of the; reference, association type associated to a specific key. Its purpose is to describe the context of the unique key in more detail, in order to facilitate easy understanding of the use of the key it describes.
    let measuredOn: String
    let unitOfMeasure: String? // Unit of Measure of the Health Attribute
    let value: String
    init(healthAttributeFeedbacks: [GPPEHealthAttributeFeedbacks]?, healthAttributeTypeCode: String?, healthAttributeTypeKey: Int, healthAttributeTypeName: String?, measuredOn: String, unitOfMeasure: String?, value: String) {
        self.healthAttributeFeedbacks = healthAttributeFeedbacks
        self.healthAttributeTypeCode = healthAttributeTypeCode
        self.healthAttributeTypeKey = healthAttributeTypeKey
        self.healthAttributeTypeName = healthAttributeTypeName
        self.measuredOn = measuredOn
        self.unitOfMeasure = unitOfMeasure
        self.value = value
    }
    public init?(json: [String: Any]) {
        let healthAttributeFeedbacks = GPPEHealthAttributeFeedbacks.createRequiredInstances(from: json, arrayKey: "healthAttributeFeedbacks")
        let healthAttributeTypeCode = json["healthAttributeTypeCode"] as? String
        guard let healthAttributeTypeKey = json["healthAttributeTypeKey"] as? Int else { debugPrint("Expected non-optional property [healthAttributeTypeKey] of type [Int] on object [GPPEHealthAttributeReadings] but did not find");return nil; }
        let healthAttributeTypeName = json["healthAttributeTypeName"] as? String
        guard let measuredOn = json["measuredOn"] as? String else { debugPrint("Expected non-optional property [measuredOn] of type [String] on object [GPPEHealthAttributeReadings] but did not find");return nil; }
        let unitOfMeasure = json["unitOfMeasure"] as? String
        guard let value = json["value"] as? String else { debugPrint("Expected non-optional property [value] of type [String] on object [GPPEHealthAttributeReadings] but did not find");return nil; }
        self.init(healthAttributeFeedbacks: healthAttributeFeedbacks, healthAttributeTypeCode: healthAttributeTypeCode, healthAttributeTypeKey: healthAttributeTypeKey, healthAttributeTypeName: healthAttributeTypeName, measuredOn: measuredOn, unitOfMeasure: unitOfMeasure, value: value)
    }
}

public struct GPPEHealthAttributeFeedbacks: CreatableFromJSON {
    let feedbackTypeCode: String? // A short name describing the; reference, association type associated to a specific key. It's purpose is to identify a unique key, in order to facilitate easy reference and understanding. The short name is used for easy identification for debugging purposes but does not necessarily exclude the option of using it as a reference in itself. For example it could be used to populate a drop down box.
    let feedbackTypeKey: Int
    let feedbackTypeName: String? // A detailed description of the; reference, association type associated to a specific key. Its purpose is to describe the context of the unique key in more detail, in order to facilitate easy understanding of the use of the key it describes.
    let feedbackTypeTypeCode: String? // A short name describing the; reference, association type associated to a specific key. It's purpose is to identify a unique key, in order to facilitate easy reference and understanding. The short name is used for easy identification for debugging purposes but does not necessarily exclude the option of using it as a reference in itself. For example it could be used to populate a drop down box.
    let feedbackTypeTypeKey: Int
    let feedbackTypeTypeName: String? // A detailed description of the; reference, association type associated to a specific key. Its purpose is to describe the context of the unique key in more detail, in order to facilitate easy understanding of the use of the key it describes.
    init(feedbackTypeCode: String?, feedbackTypeKey: Int, feedbackTypeName: String?, feedbackTypeTypeCode: String?, feedbackTypeTypeKey: Int, feedbackTypeTypeName: String?) {
        self.feedbackTypeCode = feedbackTypeCode
        self.feedbackTypeKey = feedbackTypeKey
        self.feedbackTypeName = feedbackTypeName
        self.feedbackTypeTypeCode = feedbackTypeTypeCode
        self.feedbackTypeTypeKey = feedbackTypeTypeKey
        self.feedbackTypeTypeName = feedbackTypeTypeName
    }
    public init?(json: [String: Any]) {
        let feedbackTypeCode = json["feedbackTypeCode"] as? String
        guard let feedbackTypeKey = json["feedbackTypeKey"] as? Int else { debugPrint("Expected non-optional property [feedbackTypeKey] of type [Int] on object [GPPEHealthAttributeFeedbacks] but did not find");return nil; }
        let feedbackTypeName = json["feedbackTypeName"] as? String
        let feedbackTypeTypeCode = json["feedbackTypeTypeCode"] as? String
        guard let feedbackTypeTypeKey = json["feedbackTypeTypeKey"] as? Int else { debugPrint("Expected non-optional property [feedbackTypeTypeKey] of type [Int] on object [GPPEHealthAttributeFeedbacks] but did not find");return nil; }
        let feedbackTypeTypeName = json["feedbackTypeTypeName"] as? String
        self.init(feedbackTypeCode: feedbackTypeCode, feedbackTypeKey: feedbackTypeKey, feedbackTypeName: feedbackTypeName, feedbackTypeTypeCode: feedbackTypeTypeCode, feedbackTypeTypeKey: feedbackTypeTypeKey, feedbackTypeTypeName: feedbackTypeTypeName)
    }
}

public struct GPPEPointsEntry: CreatableFromJSON {
    let categoryCode: String? // This is the human readable text related to the typeKey specified in the payload.. Essentially may be used by requesting parties but in general is to facilitate the payload context. It is in essence the short text description of the Type attribute specified in the functional requirements and will be specific to the language chosen by application for the carrier
    let categoryKey: Int? // Category of the points entry type E.g Fitness A unique key that identifies the correct type entry to use for a specific environment. Used to facilitate  the correct use of language. It is in essence the numerical depiction of the Type attribute specified in the functional requirements
    let categoryName: String? // A detailed description of the; reference, association type associated to a specific key. Its purpose is to describe the context of the unique key in more detail, in order to facilitate easy understanding of the use of the key it describes.
    let earnedValue: Int? // The actual number of points that a member earns towards his/her points status after frequency and limit rules are applied

    let id: Int? // Id of the points entry

    let potentialValue: Int? // The number of points for the points entry without taking other events into consideration i.e. before frequency and limit checks but includes using age, gender (and in future chronic conditions) to determine this value

    let preLimitValue: Int? // The number of points for the points entry  taking frequency checks into consideration, but excluding limit checks

    let reason: [GPPEReason]? // Reason for awarding/not awarding the points
    let statusChangedDate: String? // The date on which the status for the points entry last changed.
    let statusTypeCode: String? // This is the human readable text related to the typeKey specified in the payload.. Essentially may be used by requesting parties but in general is to facilitate the payload context. It is in essence the short text description of the Type attribute specified in the functional requirements and will be specific to the language chosen by application for the carrier

    let statusTypeKey: Int
    let statusTypeName: String? // This is the human readable text related to the typeKey specified in the payload.. Essentially may be used by requesting parties but in general is to facilitate the payload context. It is in essence the short text description of the Type attribute specified in the functional requirements and will be specific to the language chosen by application for the carrier

    let typeCode: String? // This is the human readable text related to the typeKey specified in the payload.. Essentially may be used by requesting parties but in general is to facilitate the payload context. It is in essence the short text description of the Type attribute specified in the functional requirements and will be specific to the language chosen by application for the carrier

    let typeKey: Int
    let typeName: String? // This is the human readable text related to the typeKey specified in the payload.. Essentially may be used by requesting parties but in general is to facilitate the payload context. It is in essence the short text description of the Type attribute specified in the functional requirements and will be specific to the language chosen by application for the carrier

    init(categoryCode: String?, categoryKey: Int?, categoryName: String?, earnedValue: Int?, id: Int?, potentialValue: Int?, preLimitValue: Int?, reason: [GPPEReason]?, statusChangedDate: String?, statusTypeCode: String?, statusTypeKey: Int, statusTypeName: String?, typeCode: String?, typeKey: Int, typeName: String?) {
        self.categoryCode = categoryCode
        self.categoryKey = categoryKey
        self.categoryName = categoryName
        self.earnedValue = earnedValue
        self.id = id
        self.potentialValue = potentialValue
        self.preLimitValue = preLimitValue
        self.reason = reason
        self.statusChangedDate = statusChangedDate
        self.statusTypeCode = statusTypeCode
        self.statusTypeKey = statusTypeKey
        self.statusTypeName = statusTypeName
        self.typeCode = typeCode
        self.typeKey = typeKey
        self.typeName = typeName
    }
    public init?(json: [String: Any]) {
        let categoryCode = json["categoryCode"] as? String
        let categoryKey = json["categoryKey"] as? Int
        let categoryName = json["categoryName"] as? String
        let earnedValue = json["earnedValue"] as? Int
        let id = json["id"] as? Int
        let potentialValue = json["potentialValue"] as? Int
        let preLimitValue = json["preLimitValue"] as? Int
        let reason = GPPEReason.createRequiredInstances(from: json, arrayKey: "reason")
        let statusChangedDate = json["statusChangedDate"] as? String
        let statusTypeCode = json["statusTypeCode"] as? String
        guard let statusTypeKey = json["statusTypeKey"] as? Int else { debugPrint("Expected non-optional property [statusTypeKey] of type [Int] on object [GPPEPointsEntry] but did not find");return nil; }
        let statusTypeName = json["statusTypeName"] as? String
        let typeCode = json["typeCode"] as? String
        guard let typeKey = json["typeKey"] as? Int else { debugPrint("Expected non-optional property [typeKey] of type [Int] on object [GPPEPointsEntry] but did not find");return nil; }
        let typeName = json["typeName"] as? String
        self.init(categoryCode: categoryCode, categoryKey: categoryKey, categoryName: categoryName, earnedValue: earnedValue, id: id, potentialValue: potentialValue, preLimitValue: preLimitValue, reason: reason, statusChangedDate: statusChangedDate, statusTypeCode: statusTypeCode, statusTypeKey: statusTypeKey, statusTypeName: statusTypeName, typeCode: typeCode, typeKey: typeKey, typeName: typeName)
    }
}

public struct GPPEReason: CreatableFromJSON {
    let categoryCode: String? // A short name describing the; reference, association type associated to a specific key. It's purpose is to identify a unique key, in order to facilitate easy reference and understanding. The short name is used for easy identification for debugging purposes but does not necessarily exclude the option of using it as a reference in itself. For example it could be used to populate a drop down box.
    let categoryKey: Int
    let categoryName: String? // A detailed description of the; reference, association type associated to a specific key. Its purpose is to describe the context of the unique key in more detail, in order to facilitate easy understanding of the use of the key it describes.
    let reasonCode: String? // A short name describing the; reference, association type associated to a specific key. It's purpose is to identify a unique key, in order to facilitate easy reference and understanding. The short name is used for easy identification for debugging purposes but does not necessarily exclude the option of using it as a reference in itself. For example it could be used to populate a drop down box.
    let reasonKey: Int
    let reasonName: String? // A detailed description of the; reference, association type associated to a specific key. Its purpose is to describe the context of the unique key in more detail, in order to facilitate easy understanding of the use of the key it describes.
    init(categoryCode: String?, categoryKey: Int, categoryName: String?, reasonCode: String?, reasonKey: Int, reasonName: String?) {
        self.categoryCode = categoryCode
        self.categoryKey = categoryKey
        self.categoryName = categoryName
        self.reasonCode = reasonCode
        self.reasonKey = reasonKey
        self.reasonName = reasonName
    }
    public init?(json: [String: Any]) {
        let categoryCode = json["categoryCode"] as? String
        guard let categoryKey = json["categoryKey"] as? Int else { debugPrint("Expected non-optional property [categoryKey] of type [Int] on object [GPPEReason] but did not find");return nil; }
        let categoryName = json["categoryName"] as? String
        let reasonCode = json["reasonCode"] as? String
        guard let reasonKey = json["reasonKey"] as? Int else { debugPrint("Expected non-optional property [reasonKey] of type [Int] on object [GPPEReason] but did not find");return nil; }
        let reasonName = json["reasonName"] as? String
        self.init(categoryCode: categoryCode, categoryKey: categoryKey, categoryName: categoryName, reasonCode: reasonCode, reasonKey: reasonKey, reasonName: reasonName)
    }
}

public struct GPPEHealthAttribute: CreatableFromJSON {
    let healthAttributeTypeValidValueses: [GPPEHealthAttributeTypeValidValues]?
    let typeCode: String? // The code of the HealthAttributeType
    let typeName: String? // A detailed description of the; reference, association type associated to a specific key. Its purpose is to describe the context of the unique key in more detail, in order to facilitate easy understanding of the use of the key it describes.
    let typekey: Int
    init(healthAttributeTypeValidValueses: [GPPEHealthAttributeTypeValidValues]?, typeCode: String?, typeName: String?, typekey: Int) {
        self.healthAttributeTypeValidValueses = healthAttributeTypeValidValueses
        self.typeCode = typeCode
        self.typeName = typeName
        self.typekey = typekey
    }
    public init?(json: [String: Any]) {
        let healthAttributeTypeValidValueses = GPPEHealthAttributeTypeValidValues.createRequiredInstances(from: json, arrayKey: "healthAttributeTypeValidValueses")
        let typeCode = json["typeCode"] as? String
        let typeName = json["typeName"] as? String
        guard let typekey = json["typekey"] as? Int else { debugPrint("Expected non-optional property [typekey] of type [Int] on object [GPPEHealthAttribute] but did not find");return nil; }
        self.init(healthAttributeTypeValidValueses: healthAttributeTypeValidValueses, typeCode: typeCode, typeName: typeName, typekey: typekey)
    }
}

public struct GPPEHealthAttributeTypeValidValues: CreatableFromJSON {
    let lowerLimit: String?
    let maxValue: String? // The value that applies to the reference, association, metatdata, or source.
    let minValue: String? // The value that applies to the reference, association, metatdata, or source.
    let typeTypeCode: String? // The Data Type of the Health Attribute Type.
    let typeTypeKey: Int
    let typeTypeName: String? // The Data Type of the Health Attribute Type.
    let unitofMeasure: String? // The ID of the Unit of measure
    let upperLimit: String?
    let validValuesList: [String]?
    init(lowerLimit: String?, maxValue: String?, minValue: String?, typeTypeCode: String?, typeTypeKey: Int, typeTypeName: String?, unitofMeasure: String?, upperLimit: String?, validValuesList: [String]?) {
        self.lowerLimit = lowerLimit
        self.maxValue = maxValue
        self.minValue = minValue
        self.typeTypeCode = typeTypeCode
        self.typeTypeKey = typeTypeKey
        self.typeTypeName = typeTypeName
        self.unitofMeasure = unitofMeasure
        self.upperLimit = upperLimit
        self.validValuesList = validValuesList
    }
    public init?(json: [String: Any]) {
        let lowerLimit = json["lowerLimit"] as? String
        let maxValue = json["maxValue"] as? String
        let minValue = json["minValue"] as? String
        let typeTypeCode = json["typeTypeCode"] as? String
        guard let typeTypeKey = json["typeTypeKey"] as? Int else { debugPrint("Expected non-optional property [typeTypeKey] of type [Int] on object [GPPEHealthAttributeTypeValidValues] but did not find");return nil; }
        let typeTypeName = json["typeTypeName"] as? String
        let unitofMeasure = json["unitofMeasure"] as? String
        let upperLimit = json["upperLimit"] as? String
        let validValuesList = json["validValuesList"] as? [String]
        self.init(lowerLimit: lowerLimit, maxValue: maxValue, minValue: minValue, typeTypeCode: typeTypeCode, typeTypeKey: typeTypeKey, typeTypeName: typeTypeName, unitofMeasure: unitofMeasure, upperLimit: upperLimit, validValuesList: validValuesList)
    }
}

public struct GPPEWarnings: CreatableFromJSON {
    let code: String? // A short name describing the; reference, association type associated to a specific key. It's purpose is to identify a unique key, in order to facilitate easy reference and understanding. The short name is used for easy identification for debugging purposes but does not necessarily exclude the option of using it as a reference in itself. For example it could be used to populate a drop down box.
    let context: String
    let key: Int
    let name: String? // A detailed description of the; reference, association type associated to a specific key. Its purpose is to describe the context of the unique key in more detail, in order to facilitate easy understanding of the use of the key it describes.
    init(code: String?, context: String, key: Int, name: String?) {
        self.code = code
        self.context = context
        self.key = key
        self.name = name
    }
    public init?(json: [String: Any]) {
        let code = json["code"] as? String
        guard let context = json["context"] as? String else { debugPrint("Expected non-optional property [context] of type [String] on object [GPPEWarnings] but did not find");return nil; }
        guard let key = json["key"] as? Int else { debugPrint("Expected non-optional property [key] of type [Int] on object [GPPEWarnings] but did not find");return nil; }
        let name = json["name"] as? String
        self.init(code: code, context: context, key: key, name: name)
    }
}
