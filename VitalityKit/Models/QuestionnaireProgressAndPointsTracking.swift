//
//  QuestionnaireProgressAndPointsTracking.swift
//  VitalityActive
//
//  Created by Swagger Codegen on 06/23/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file

import Foundation

public struct QuestionnaireProgressAndPointsTracking: CreatableFromJSON {
    let questionnaire: [QPPTQuestionnaire]?
    let questionnaireSetCompleted: Bool
    let questionnaireSetTypeCode: String? // The short name describing the associated "type key" reference. It's purpose is to describe the unique key to facilitate easy reference. The short name is used for easy context recognition for debugging but could also be used as a reference able item in itself
    let questionnaireSetTypeKey: Int
    let questionnaireSetTypeName: String? // The extended name describing the associated "type key" reference. It's purpose is to describe the "type key" in detail to facilitate better   understanding
    let setText: String?
    let setTextDescription: String?
    let setTextNote: String?
    let totalEarnedPoints: Int
    let totalPotentialPoints: Int
    let totalQuestionnaireCompleted: Int
    let totalQuestionnaires: Int
    init(questionnaire: [QPPTQuestionnaire]?, questionnaireSetCompleted: Bool, questionnaireSetTypeCode: String?, questionnaireSetTypeKey: Int, questionnaireSetTypeName: String?, setText: String?, setTextDescription: String?, setTextNote: String?, totalEarnedPoints: Int, totalPotentialPoints: Int, totalQuestionnaireCompleted: Int, totalQuestionnaires: Int) {
        self.questionnaire = questionnaire
        self.questionnaireSetCompleted = questionnaireSetCompleted
        self.questionnaireSetTypeCode = questionnaireSetTypeCode
        self.questionnaireSetTypeKey = questionnaireSetTypeKey
        self.questionnaireSetTypeName = questionnaireSetTypeName
        self.setText = setText
        self.setTextDescription = setTextDescription
        self.setTextNote = setTextNote
        self.totalEarnedPoints = totalEarnedPoints
        self.totalPotentialPoints = totalPotentialPoints
        self.totalQuestionnaireCompleted = totalQuestionnaireCompleted
        self.totalQuestionnaires = totalQuestionnaires
    }
    public init?(json: [String: Any]) {
        let questionnaire = QPPTQuestionnaire.createRequiredInstances(from: json, arrayKey: "questionnaire")
        guard let questionnaireSetCompleted = json["questionnaireSetCompleted"] as? Bool else { debugPrint("Expected non-optional property [questionnaireSetCompleted] of type [Bool] on object [QuestionnaireProgressAndPointsTracking] but did not find");return nil; }
        let questionnaireSetTypeCode = json["questionnaireSetTypeCode"] as? String
        guard let questionnaireSetTypeKey = json["questionnaireSetTypeKey"] as? Int else { debugPrint("Expected non-optional property [questionnaireSetTypeKey] of type [Int] on object [QuestionnaireProgressAndPointsTracking] but did not find");return nil; }
        let questionnaireSetTypeName = json["questionnaireSetTypeName"] as? String
        let setText = json["setText"] as? String
        let setTextDescription = json["setTextDescription"] as? String
        let setTextNote = json["setTextNote"] as? String
        guard let totalEarnedPoints = json["totalEarnedPoints"] as? Int else { debugPrint("Expected non-optional property [totalEarnedPoints] of type [Int] on object [QuestionnaireProgressAndPointsTracking] but did not find");return nil; }
        guard let totalPotentialPoints = json["totalPotentialPoints"] as? Int else { debugPrint("Expected non-optional property [totalPotentialPoints] of type [Int] on object [QuestionnaireProgressAndPointsTracking] but did not find");return nil; }
        guard let totalQuestionnaireCompleted = json["totalQuestionnaireCompleted"] as? Int else { debugPrint("Expected non-optional property [totalQuestionnaireCompleted] of type [Int] on object [QuestionnaireProgressAndPointsTracking] but did not find");return nil; }
        guard let totalQuestionnaires = json["totalQuestionnaires"] as? Int else { debugPrint("Expected non-optional property [totalQuestionnaires] of type [Int] on object [QuestionnaireProgressAndPointsTracking] but did not find");return nil; }
        self.init(questionnaire: questionnaire, questionnaireSetCompleted: questionnaireSetCompleted, questionnaireSetTypeCode: questionnaireSetTypeCode, questionnaireSetTypeKey: questionnaireSetTypeKey, questionnaireSetTypeName: questionnaireSetTypeName, setText: setText, setTextDescription: setTextDescription, setTextNote: setTextNote, totalEarnedPoints: totalEarnedPoints, totalPotentialPoints: totalPotentialPoints, totalQuestionnaireCompleted: totalQuestionnaireCompleted, totalQuestionnaires: totalQuestionnaires)
    }
}

public struct QPPTQuestionnaire: CreatableFromJSON {
    let completedOn: String? // Date the questionnaire was completed
    let completionFlag: Bool
    let questionnaireSections: [QPPTQuestionnaireSections]? // This is the section/s that a questionnaire may be broken down into. Some questionnaires may not have sections.
    let sortOrderIndex: Int
    let text: String?
    let textDescription: String?
    let textNote: String?
    let typeCode: String
    let typeKey: Int
    let typeName: String? // The extended name describing the associated "type key" reference. It's purpose is to describe the "type key" in detail to facilitate better   understanding
    init(completedOn: String?, completionFlag: Bool, questionnaireSections: [QPPTQuestionnaireSections]?, sortOrderIndex: Int, text: String?, textDescription: String?, textNote: String?, typeCode: String, typeKey: Int, typeName: String?) {
        self.completedOn = completedOn
        self.completionFlag = completionFlag
        self.questionnaireSections = questionnaireSections
        self.sortOrderIndex = sortOrderIndex
        self.text = text
        self.textDescription = textDescription
        self.textNote = textNote
        self.typeCode = typeCode
        self.typeKey = typeKey
        self.typeName = typeName
    }
    public init?(json: [String: Any]) {
        let completedOn = json["completedOn"] as? String
        guard let completionFlag = json["completionFlag"] as? Bool else { debugPrint("Expected non-optional property [completionFlag] of type [Bool] on object [QPPTQuestionnaire] but did not find");return nil; }
        let questionnaireSections = QPPTQuestionnaireSections.createRequiredInstances(from: json, arrayKey: "questionnaireSections")
        guard let sortOrderIndex = json["sortOrderIndex"] as? Int else { debugPrint("Expected non-optional property [sortOrderIndex] of type [Int] on object [QPPTQuestionnaire] but did not find");return nil; }
        let text = json["text"] as? String
        let textDescription = json["textDescription"] as? String
        let textNote = json["textNote"] as? String
        guard let typeCode = json["typeCode"] as? String else { debugPrint("Expected non-optional property [typeCode] of type [String] on object [QPPTQuestionnaire] but did not find");return nil; }
        guard let typeKey = json["typeKey"] as? Int else { debugPrint("Expected non-optional property [typeKey] of type [Int] on object [QPPTQuestionnaire] but did not find");return nil; }
        let typeName = json["typeName"] as? String
        self.init(completedOn: completedOn, completionFlag: completionFlag, questionnaireSections: questionnaireSections, sortOrderIndex: sortOrderIndex, text: text, textDescription: textDescription, textNote: textNote, typeCode: typeCode, typeKey: typeKey, typeName: typeName)
    }
}

public struct QPPTQuestionnaireSections: CreatableFromJSON {
    let isVisible: Bool
    let questions: [QPPTQuestions]?
    let sortOrderIndex: Int
    let text: String?
    let textDescription: String?
    let textNote: String?
    let typeCode: String? // The short name describing the associated "type key" reference. It's purpose is to describe the unique key to facilitate easy reference. The short name is used for easy context recognition for debugging but could also be used as a reference able item in itself
    let typeKey: Int
    let typeName: String? // The extended name describing the associated "type key" reference. It's purpose is to describe the "type key" in detail to facilitate better   understanding
    let visibilityTagName: String
    init(isVisible: Bool, questions: [QPPTQuestions]?, sortOrderIndex: Int, text: String?, textDescription: String?, textNote: String?, typeCode: String?, typeKey: Int, typeName: String?, visibilityTagName: String) {
        self.isVisible = isVisible
        self.questions = questions
        self.sortOrderIndex = sortOrderIndex
        self.text = text
        self.textDescription = textDescription
        self.textNote = textNote
        self.typeCode = typeCode
        self.typeKey = typeKey
        self.typeName = typeName
        self.visibilityTagName = visibilityTagName
    }
    public init?(json: [String: Any]) {
        guard let isVisible = json["isVisible"] as? Bool else { debugPrint("Expected non-optional property [isVisible] of type [Bool] on object [QPPTQuestionnaireSections] but did not find");return nil; }
        let questions = QPPTQuestions.createRequiredInstances(from: json, arrayKey: "questions")
        guard let sortOrderIndex = json["sortOrderIndex"] as? Int else { debugPrint("Expected non-optional property [sortOrderIndex] of type [Int] on object [QPPTQuestionnaireSections] but did not find");return nil; }
        let text = json["text"] as? String
        let textDescription = json["textDescription"] as? String
        let textNote = json["textNote"] as? String
        let typeCode = json["typeCode"] as? String
        guard let typeKey = json["typeKey"] as? Int else { debugPrint("Expected non-optional property [typeKey] of type [Int] on object [QPPTQuestionnaireSections] but did not find");return nil; }
        let typeName = json["typeName"] as? String
        guard let visibilityTagName = json["visibilityTagName"] as? String else { debugPrint("Expected non-optional property [visibilityTagName] of type [String] on object [QPPTQuestionnaireSections] but did not find");return nil; }
        self.init(isVisible: isVisible, questions: questions, sortOrderIndex: sortOrderIndex, text: text, textDescription: textDescription, textNote: textNote, typeCode: typeCode, typeKey: typeKey, typeName: typeName, visibilityTagName: visibilityTagName)
    }
}

public struct QPPTQuestions: CreatableFromJSON {
    let format: String? // The expected format of the input value
    let length: Int? // The length of the expected input value
    let populationValues: [QPPTPopulationValues]?
    let questionAssociations: [QPPTQuestionAssociations]?
    let questionDecorator: QPPTQuestionDecorator?
    let questionTypeCode: String? // The short name describing the associated "type key" reference. It's purpose is to describe the unique key to facilitate easy reference. The short name is used for easy context recognition for debugging but could also be used as a reference able item in itself
    let questionTypeKey: Int
    let questionTypeName: String? // The extended name describing the associated "type key" reference. It's purpose is to describe the "type key" in detail to facilitate better   understanding
    let sortOrderIndex: Int
    let text: String?
    let textDescription: String?
    let textNote: String?
    let typeCode: String? // The short name describing the associated "type key" reference. It's purpose is to describe the unique key to facilitate easy reference. The short name is used for easy context recognition for debugging but could also be used as a reference able item in itself
    let typeKey: Int
    let typeName: String? // The extended name describing the associated "type key" reference. It's purpose is to describe the "type key" in detail to facilitate better understanding
    let unitOfMeasures: [QPPTUnitOfMeasures]?
    let validValues: [QPPTValidValues]?
    let visibilityTagName: String? // A value which describes what criteria needs to be complied with in order to unlock a question or questionnaire section
    init(format: String?, length: Int?, populationValues: [QPPTPopulationValues]?, questionAssociations: [QPPTQuestionAssociations]?, questionDecorator: QPPTQuestionDecorator?, questionTypeCode: String?, questionTypeKey: Int, questionTypeName: String?, sortOrderIndex: Int, text: String?, textDescription: String?, textNote: String?, typeCode: String?, typeKey: Int, typeName: String?, unitOfMeasures: [QPPTUnitOfMeasures]?, validValues: [QPPTValidValues]?, visibilityTagName: String?) {
        self.format = format
        self.length = length
        self.populationValues = populationValues
        self.questionAssociations = questionAssociations
        self.questionDecorator = questionDecorator
        self.questionTypeCode = questionTypeCode
        self.questionTypeKey = questionTypeKey
        self.questionTypeName = questionTypeName
        self.sortOrderIndex = sortOrderIndex
        self.text = text
        self.textDescription = textDescription
        self.textNote = textNote
        self.typeCode = typeCode
        self.typeKey = typeKey
        self.typeName = typeName
        self.unitOfMeasures = unitOfMeasures
        self.validValues = validValues
        self.visibilityTagName = visibilityTagName
    }
    public init?(json: [String: Any]) {
        let format = json["format"] as? String
        let length = json["length"] as? Int
        let populationValues = QPPTPopulationValues.createRequiredInstances(from: json, arrayKey: "populationValues")
        let questionAssociations = QPPTQuestionAssociations.createRequiredInstances(from: json, arrayKey: "questionAssociations")
        let questionDecorator = QPPTQuestionDecorator(json: json, key: "questionDecorator")
        let questionTypeCode = json["questionTypeCode"] as? String
        guard let questionTypeKey = json["questionTypeKey"] as? Int else { debugPrint("Expected non-optional property [questionTypeKey] of type [Int] on object [QPPTQuestions] but did not find");return nil; }
        let questionTypeName = json["questionTypeName"] as? String
        guard let sortOrderIndex = json["sortOrderIndex"] as? Int else { debugPrint("Expected non-optional property [sortOrderIndex] of type [Int] on object [QPPTQuestions] but did not find");return nil; }
        let text = json["text"] as? String
        let textDescription = json["textDescription"] as? String
        let textNote = json["textNote"] as? String
        let typeCode = json["typeCode"] as? String
        guard let typeKey = json["typeKey"] as? Int else { debugPrint("Expected non-optional property [typeKey] of type [Int] on object [QPPTQuestions] but did not find");return nil; }
        let typeName = json["typeName"] as? String
        let unitOfMeasures = QPPTUnitOfMeasures.createRequiredInstances(from: json, arrayKey: "unitOfMeasures")
        let validValues = QPPTValidValues.createRequiredInstances(from: json, arrayKey: "validValues")
        let visibilityTagName = json["visibilityTagName"] as? String
        self.init(format: format, length: length, populationValues: populationValues, questionAssociations: questionAssociations, questionDecorator: questionDecorator, questionTypeCode: questionTypeCode, questionTypeKey: questionTypeKey, questionTypeName: questionTypeName, sortOrderIndex: sortOrderIndex, text: text, textDescription: textDescription, textNote: textNote, typeCode: typeCode, typeKey: typeKey, typeName: typeName, unitOfMeasures: unitOfMeasures, validValues: validValues, visibilityTagName: visibilityTagName)
    }
}

public struct QPPTPopulationValues: CreatableFromJSON {
    let eventCode: String?
    let eventDate: String?
    let eventKey: Int?
    let eventName: String?
    let fromValue: String? // The from value for an answer that is associated with a range
    let toValue: String? // The to value for an answer that is associated with a range
    let unitOfMeasure: String? // Unit of Measure associated with value returned
    let value: String? // The value for an answer that is associated with a single number or character set
    let valueType: String? // The type of the value returned <b><i>Example</i></b> Date String Long Double
    init(eventCode: String?, eventDate: String?, eventKey: Int?, eventName: String?, fromValue: String?, toValue: String?, unitOfMeasure: String?, value: String?, valueType: String?) {
        self.eventCode = eventCode
        self.eventDate = eventDate
        self.eventKey = eventKey
        self.eventName = eventName
        self.fromValue = fromValue
        self.toValue = toValue
        self.unitOfMeasure = unitOfMeasure
        self.value = value
        self.valueType = valueType
    }
    public init?(json: [String: Any]) {
        let eventCode = json["eventCode"] as? String
        let eventDate = json["eventDate"] as? String
        let eventKey = json["eventKey"] as? Int
        let eventName = json["eventName"] as? String
        let fromValue = json["fromValue"] as? String
        let toValue = json["toValue"] as? String
        let unitOfMeasure = json["unitOfMeasure"] as? String
        let value = json["value"] as? String
        let valueType = json["valueType"] as? String
        self.init(eventCode: eventCode, eventDate: eventDate, eventKey: eventKey, eventName: eventName, fromValue: fromValue, toValue: toValue, unitOfMeasure: unitOfMeasure, value: value, valueType: valueType)
    }
}

public struct QPPTQuestionAssociations: CreatableFromJSON {
    let childQuestionKey: Int
    let sortIndex: Int
    init(childQuestionKey: Int, sortIndex: Int) {
        self.childQuestionKey = childQuestionKey
        self.sortIndex = sortIndex
    }
    public init?(json: [String: Any]) {
        guard let childQuestionKey = json["childQuestionKey"] as? Int else { debugPrint("Expected non-optional property [childQuestionKey] of type [Int] on object [QPPTQuestionAssociations] but did not find");return nil; }
        guard let sortIndex = json["sortIndex"] as? Int else { debugPrint("Expected non-optional property [sortIndex] of type [Int] on object [QPPTQuestionAssociations] but did not find");return nil; }
        self.init(childQuestionKey: childQuestionKey, sortIndex: sortIndex)
    }
}

public struct QPPTQuestionDecorator: CreatableFromJSON {
    let channelTypeCode: String? // The short name describing the associated "type key" reference. It's purpose is to describe the unique key to facilitate easy reference. The short name is used for easy context recognition for debugging but could also be used as a reference able item in itself
    let channelTypeKey: Int
    let channelTypeName: String? // The extended name describing the associated "type key" reference. It's purpose is to describe the "type key" in detail to facilitate better understanding
    let typeCode: String? // The short name describing the associated "type key" reference. It's purpose is to describe the unique key to facilitate easy reference. The short name is used for easy context recognition for debugging but could also be used as a reference able item in itself
    let typeKey: Int
    let typeName: String? // The extended name describing the associated "type key" reference. It's purpose is to describe the "type key" in detail to facilitate better understanding
    let unitOfMeasures: [QPPTUnitOfMeasures]?
    init(channelTypeCode: String?, channelTypeKey: Int, channelTypeName: String?, typeCode: String?, typeKey: Int, typeName: String?, unitOfMeasures: [QPPTUnitOfMeasures]?) {
        self.channelTypeCode = channelTypeCode
        self.channelTypeKey = channelTypeKey
        self.channelTypeName = channelTypeName
        self.typeCode = typeCode
        self.typeKey = typeKey
        self.typeName = typeName
        self.unitOfMeasures = unitOfMeasures
    }
    public init?(json: [String: Any]) {
        let channelTypeCode = json["channelTypeCode"] as? String
        guard let channelTypeKey = json["channelTypeKey"] as? Int else { debugPrint("Expected non-optional property [channelTypeKey] of type [Int] on object [QPPTQuestionDecorator] but did not find");return nil; }
        let channelTypeName = json["channelTypeName"] as? String
        let typeCode = json["typeCode"] as? String
        guard let typeKey = json["typeKey"] as? Int else { debugPrint("Expected non-optional property [typeKey] of type [Int] on object [QPPTQuestionDecorator] but did not find");return nil; }
        let typeName = json["typeName"] as? String
        let unitOfMeasures = QPPTUnitOfMeasures.createRequiredInstances(from: json, arrayKey: "unitOfMeasures")
        self.init(channelTypeCode: channelTypeCode, channelTypeKey: channelTypeKey, channelTypeName: channelTypeName, typeCode: typeCode, typeKey: typeKey, typeName: typeName, unitOfMeasures: unitOfMeasures)
    }
}

public struct QPPTUnitOfMeasures: CreatableFromJSON {
    let value: String
    init(value: String) {
        self.value = value
    }
    public init?(json: [String: Any]) {
        guard let value = json["value"] as? String else { debugPrint("Expected non-optional property [value] of type [String] on object [QPPTUnitOfMeasures] but did not find");return nil; }
        self.init(value: value)
    }
}

public struct QPPTValidValues: CreatableFromJSON {
    let description: String?
    let name: String
    let note: String?
    let unitOfMeasure: QPPTUnitOfMeasure?
    let validValueTypes: [QPPTValidValueTypes]?
    let value: String
    init(description: String?, name: String, note: String?, unitOfMeasure: QPPTUnitOfMeasure?, validValueTypes: [QPPTValidValueTypes]?, value: String) {
        self.description = description
        self.name = name
        self.note = note
        self.unitOfMeasure = unitOfMeasure
        self.validValueTypes = validValueTypes
        self.value = value
    }
    public init?(json: [String: Any]) {
        let description = json["description"] as? String
        guard let name = json["name"] as? String else { debugPrint("Expected non-optional property [name] of type [String] on object [QPPTValidValues] but did not find");return nil; }
        let note = json["note"] as? String
        let unitOfMeasure = QPPTUnitOfMeasure(json: json, key: "unitOfMeasure")
        let validValueTypes = QPPTValidValueTypes.createRequiredInstances(from: json, arrayKey: "validValueTypes")
        guard let value = json["value"] as? String else { debugPrint("Expected non-optional property [value] of type [String] on object [QPPTValidValues] but did not find");return nil; }
        self.init(description: description, name: name, note: note, unitOfMeasure: unitOfMeasure, validValueTypes: validValueTypes, value: value)
    }
}

public struct QPPTUnitOfMeasure: CreatableFromJSON {
    let value: String
    init(value: String) {
        self.value = value
    }
    public init?(json: [String: Any]) {
        guard let value = json["value"] as? String else { debugPrint("Expected non-optional property [value] of type [String] on object [QPPTUnitOfMeasure] but did not find");return nil; }
        self.init(value: value)
    }
}

public struct QPPTValidValueTypes: CreatableFromJSON {
    let typeCode: String? // The short name related to the "type key" referenced. It describes the unique key for easy reference purposes. The short name is used for easy   context recognition. assists in debugging but could also be used as a reference when applicable
    let typeKey: Int
    let typeName: String? // The extended name related to the "type Code". The name is used to describe the key's function in more detail
    init(typeCode: String?, typeKey: Int, typeName: String?) {
        self.typeCode = typeCode
        self.typeKey = typeKey
        self.typeName = typeName
    }
    public init?(json: [String: Any]) {
        let typeCode = json["typeCode"] as? String
        guard let typeKey = json["typeKey"] as? Int else { debugPrint("Expected non-optional property [typeKey] of type [Int] on object [QPPTValidValueTypes] but did not find");return nil; }
        let typeName = json["typeName"] as? String
        self.init(typeCode: typeCode, typeKey: typeKey, typeName: typeName)
    }
}
