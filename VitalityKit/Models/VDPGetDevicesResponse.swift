//
//  VDPGetDevicesResponse.swift
//  VitalityActive
//
//  Created by Simon Stewart on 6/1/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation

public struct VDPGetDevicesResponse: CreatableFromJSON {
    let markets: [VDPMarkets]
    init(markets: [VDPMarkets]) {
        self.markets = markets
    }
    public init?(json: [String: Any]) {
        guard let markets = VDPMarkets.createRequiredInstances(from: json, arrayKey: "markets") else { debugPrint("Expected non-optional property [markets] but did not find");return nil; }
        self.init(markets: markets)
    }
}

public struct VDPMarkets: CreatableFromJSON {
    let partner: VDPPartner
    let assets: VDPAssets
    init(partner: VDPPartner, assets: VDPAssets) {
        self.partner = partner
        self.assets = assets
    }
    public init?(json: [String: Any]) {
        guard let partner = VDPPartner(json: json, key: "partner") else { debugPrint("Expected. non-optional property [partner] but did not find");return nil; }
        guard let assets = VDPAssets(json: json, key: "assets") else { debugPrint("Expected non-optional property [assets] but did not find");return nil; }
        self.init(partner: partner, assets: assets)
    }
}
public struct VDPWarnings: CreatableFromJSON {
    let code: String
    let message: String
    init(code: String, message: String) {
        self.code = code
        self.message = message
    }
    public init?(json: [String: Any]) {
        guard let code = json["code"] as? String else { debugPrint("Expected non-optional property [code] but did not find");return nil; }
        guard let message = json["message"] as? String else { debugPrint("Expected non-optional property [message] but did not find");return nil; }
        self.init(code: code, message: message)
    }
}
public struct VDPPartnerDetail: CreatableFromJSON {
    let url: String
    let method: String
    init(url: String, method: String) {
        self.url = url
        self.method = method
    }
    public init?(json: [String: Any]) {
        guard let url = json["url"] as? String else { debugPrint("Expected non-optional property [url] of type [String] on object [CAQuestionnaireFeedback_isRequired_] but did not find");return nil; }
        guard let method = json["method"] as? String else { debugPrint("Expected non-optional property [method] of type [String] on object [CAQuestionnaireFeedback_isRequired_] but did not find");return nil; }
        self.init(url: url, method: method)
    }
}
public struct VDPAssets: CreatableFromJSON {
    let partnerLogoUrl: String
    let partnerDescription: String
    let partnerWebsiteUrl: String?
    let aboutPartnerContentId: String?
    let stepsToLinkContentId: String?
    init(partnerLogoUrl: String, partnerDescription: String, partnerWebsiteUrl: String?, aboutPartnerContentId: String?, stepsToLinkContentId: String?) {
        self.partnerLogoUrl = partnerLogoUrl
        self.partnerDescription = partnerDescription
        self.aboutPartnerContentId = aboutPartnerContentId
        self.partnerWebsiteUrl = partnerWebsiteUrl
        self.stepsToLinkContentId = stepsToLinkContentId
    }
    public init?(json: [String: Any]) {
        guard let partnerLogoUrl = json["partnerLogoUrl"] as? String else { debugPrint("Expected non-optional property [partnerLogoUrl] of type [String] on object [CAQuestionnaireFeedback_isRequired_] but did not find");return nil; }
        guard let partnerDescription = json["partnerDescription"] as? String else { debugPrint("Expected non-optional property [partnerDescription] of type [String] on object [CAQuestionnaireFeedback_isRequired_] but did not find");return nil; }
        let partnerWebsiteUrl = json["partnerWebsiteUrl"] as? String
        let aboutPartnerContentId = json["aboutPartnerContentId"] as? String
        let stepsToLinkContentId = json["stepsToLinkContentId"] as? String

        self.init(partnerLogoUrl: partnerLogoUrl, partnerDescription: partnerDescription, partnerWebsiteUrl: partnerWebsiteUrl, aboutPartnerContentId: aboutPartnerContentId, stepsToLinkContentId: stepsToLinkContentId)
    }
}

public struct VDPPartner: CreatableFromJSON {
    let partnerSystem: String
    let device: String
    let partnerLink: VDPPartnerDetail?
    let partnerDelink: VDPPartnerDetail?
    let partnerSync: VDPPartnerDetail?
    let partnerLinkedStatus: String
    let partnerLastSync: String
    let partnerLastWorkout: String?
    init(partnerSystem: String, device: String, partnerLink: VDPPartnerDetail?, partnerDelink: VDPPartnerDetail?, partnerSync: VDPPartnerDetail?, partnerLinkedStatus: String, partnerLastSync: String, partnerLastWorkout: String?) {
        self.partnerSystem = partnerSystem
        self.device = device
        self.partnerLink = partnerLink
        self.partnerDelink = partnerDelink
        self.partnerSync = partnerSync
        self.partnerLinkedStatus = partnerLinkedStatus
        self.partnerLastSync = partnerLastSync
        self.partnerLastWorkout = partnerLastWorkout
    }
    public init?(json: [String: Any]) {
        guard let partnerSystem = json["partnerSystem"] as? String else { debugPrint("Expected non-optional property [partnerSystem] but did not find");return nil; }
        guard let device = json["device"] as? String else { debugPrint("Expected non-optional property [device] but did not find");return nil; }
        let partnerLink = VDPPartnerDetail(json: json, key: "partnerLink")
        let partnerDelink = VDPPartnerDetail(json: json, key: "partnerDelink")
        let partnerSync = VDPPartnerDetail(json: json, key: "partnerSync")
        guard let partnerLinkedStatus = json["partnerLinkedStatus"] as? String else { debugPrint("Expected non-optional property [partnerLinkedStatus] but did not find");return nil; }
        guard let partnerLastSync = json["partnerLastSync"] as? String else { debugPrint("Expected non-optional property [partnerLastSync] but did not find");return nil; }
        let partnerLastWorkout = json["partnerLastWorkout"] as? String
        self.init(partnerSystem: partnerSystem, device: device, partnerLink: partnerLink, partnerDelink: partnerDelink, partnerSync: partnerSync, partnerLinkedStatus: partnerLinkedStatus, partnerLastSync: partnerLastSync, partnerLastWorkout: partnerLastWorkout)
    }
}
