//
//  OFEGetEventsPointsResponse.swift
//  VitalityActive
//
//  Created by Harry C. Lingad on 4/24/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation

public struct OFEGetEventsPointsResponse: CreatableFromJSON {
    let eventsPoints: [EventsPoints]? // The points account. This covers the points earned and the period in which the points were earned
    init(eventsPoints: [EventsPoints]?) {
        self.eventsPoints = eventsPoints
    }
    public init?(json: [String: Any]) {
        let eventsPoints = EventsPoints.createRequiredInstances(from: json, arrayKey: "pointsEvents")
        self.init(eventsPoints: eventsPoints)
    }
}

public struct EventsPoints: CreatableFromJSON {
    
    public let id: Int
    public let activityDate: String?
    public let dateLogged: String?
    public let typeName: String?
    public let typeCode: String?
    public let typeKey: Int
    public let eventMetadatas: [EventMetadata]?
    
    public init(id: Int,
                activityDate:  String?,
                dateLogged: String?,
                typeName: String?,
                typeCode: String?,
                typeKey: Int,
                eventMetadatas: [EventMetadata]?) {
        
        self.id = id
        self.activityDate = activityDate
        self.dateLogged = dateLogged
        self.typeName = typeName
        self.typeCode = typeCode
        self.typeKey = typeKey
        self.eventMetadatas = eventMetadatas
    }
    
    public init?(json: [String: Any]) {
        let id =  json["id"] as? Int
        let activityDate = json["activityDate"] as? String
        let dateLogged = json["dateLogged"] as? String
        let typeName = json["typeName"] as? String
        let typeCode = json["typeCode"] as? String
        let typeKey =  json["typeKey"] as? Int
        let eventMetadatas = EventMetadata.createRequiredInstances(from: json, arrayKey: "eventMetadatas")
        
        self.init(id: id!,
                  activityDate: activityDate,
                  dateLogged: dateLogged,
                  typeName: typeName,
                  typeCode: typeCode,
                  typeKey: typeKey!,
                  eventMetadatas: eventMetadatas)
    }
    
}



//------------------------------------------------------

public struct AssociatedEvent: CreatableFromJSON {
    
    let activityDate: String?
    let associationTypeCode: String?
    let associationTypeKey: Int
    let associationTypeName: String?
    let dateLogged: String?
    let dateTimeAssociated: String?
    let eventId: Int
    let eventSourceCode: String?
    let eventSourceKey: Int
    let eventSourceName: String?
    let typeCode: String?
    let typeKey: Int
    let typeName: String?
    
    init(activityDate: String?, associationTypeCode: String?, associationTypeKey: Int, associationTypeName: String?, dateLogged: String?, dateTimeAssociated: String?, eventId: Int, eventSourceCode: String?, eventSourceKey: Int, eventSourceName: String?, typeCode: String?, typeKey: Int, typeName: String?){
        
        self.activityDate = activityDate
        self.associationTypeCode = associationTypeCode
        self.associationTypeKey = associationTypeKey
        self.associationTypeName = associationTypeName
        self.dateLogged = dateLogged
        self.dateTimeAssociated = dateTimeAssociated
        self.eventId = eventId
        self.eventSourceCode = eventSourceCode
        self.eventSourceKey = eventSourceKey
        self.eventSourceName = eventSourceName
        self.typeCode = typeCode
        self.typeKey = typeKey
        self.typeName = typeName
        
    }
    
    
    public init?(json: [String: Any]) {
        let activityDate = json["activityDate"] as? String
        let associationTypeCode = json["associationTypeCode"] as?  String
        let associationTypeKey = json["associationTypeKey"] as?  Int
        let associationTypeName = json["associationTypeName"] as?  String
        let dateLogged = json["dateLogged"] as?  String?
        let dateTimeAssociated = json["dateTimeAssociated"] as?  String
        let eventId = json["eventId"] as?  Int
        let eventSourceCode = json["eventSourceCode"] as?  String
        let eventSourceKey = json["eventSourceKey"] as?  Int
        let eventSourceName = json["eventSourceName"] as?  String
        let typeCode = json["typeCode"] as?  String
        let typeKey = json["typeKey"] as?  Int
        let typeName = json["typeName"] as?  String
        
        self.init(activityDate: activityDate, associationTypeCode: associationTypeCode, associationTypeKey: associationTypeKey!, associationTypeName: associationTypeName, dateLogged: dateLogged!, dateTimeAssociated: dateTimeAssociated, eventId: eventId!, eventSourceCode: eventSourceCode!, eventSourceKey: eventSourceKey!, eventSourceName: eventSourceName!, typeCode: typeCode!, typeKey: typeKey!, typeName: typeName!)
        
    }
    
}
//------------------------------------------------------


//------------------------------------------------------

//public struct EventsExternalReference: CreatableFromJSON {
//
//    let typeCode: String?
//    let typeKey: Int
//    let typeName: String?
//    let value: String?
//
//
//    init(typeCode: String?, typeKey: Int, typeName: String?, value: String?){
//
//        self.typeCode = typeCode
//        self.typeKey = typeKey
//        self.typeName = typeName
//        self.value = value
//    }
//
//    public init?(json: [String: Any]) {
//
//        let activityDate = json["activityDate"] as? String
//        let typeKey = json["typeKey"] as?  Int
//        let typeName = json["typeName"] as? String
//        let value = json["value"] as?  String
//
//        self.init(typeCode: activityDate, typeKey: typeKey!, typeName: typeName, value: value)
//
//    }
//
//}



public struct EventMetadata: CreatableFromJSON {
    
    public let typeCode: String?
    public let typeKey: Int
    public let typeName: String?
    public let unitOfMeasure: String?
    public let value: String?
    
    
    public init(typeCode: String?, typeKey: Int, typeName: String?, unitOfMeasure: String?, value: String?){
        
        self.typeCode = typeCode
        self.typeKey = typeKey
        self.typeName = typeName
        self.unitOfMeasure = unitOfMeasure
        self.value = value
    }
    
    public init?(json: [String: Any]) {
        
        let typeCode = json["typeCode"] as? String
        let typeKey = json["typeKey"] as?  Int
        let typeName = json["typeName"] as? String
        
        var unitOfMeasure = json["unitOfMeasure"] as? String
        if unitOfMeasure == nil{
            unitOfMeasure = "No Value"
        }
        
        var value = json["value"] as?  String
        if value == nil{
            value = "No Value"
        }
        
        self.init(typeCode: typeCode, typeKey: typeKey!, typeName: typeName, unitOfMeasure: unitOfMeasure, value: value)
        
    }
    
}

//------------------------------------------------------


public struct EventSource: CreatableFromJSON {
    
    let eventSourceCode: String?
    let eventSourceKey: Int
    let eventSourceName: String?
    let note: String?
    
    
    init(eventSourceCode: String?, eventSourceKey: Int, eventSourceName: String?, note: String?){
        
        self.eventSourceCode = eventSourceCode
        self.eventSourceKey = eventSourceKey
        self.eventSourceName = eventSourceName
        self.note = note
    }
    
    public init?(json: [String: Any]) {
        
        let eventSourceCode = json["eventSourceCode"] as? String
        let eventSourceKey = json["eventSourceKey"] as? Int
        let eventSourceName = json["eventSourceName"] as? String
        let note = json["note"] as? String
        
        self.init(eventSourceCode: eventSourceCode, eventSourceKey: eventSourceKey!, eventSourceName: eventSourceName, note: note)
    }
}

//------------------------------------------------------


public struct OFEPointsEntry: CreatableFromJSON {
    
    let categoryCode: String?
    let categoryKey: Int
    let categoryName: String?
    let earnedValue: Int
    
    let id: Int
    let metadatas: [OFEEventMetadataModel]?
    
    let potentialValue: Int
    let preLimitValue: Int
    let reason: [OFEPointsEntryReason]?
    let statusChangedDate: String?
    let statusTypeCode: String?
    let statusTypeKey: Int
    
    let statusTypeName: String?
    let typeCode: String?
    let typeKey: Int
    let typeName: String?
    
    
    init(categoryCode: String?, categoryKey: Int, categoryName: String?, earnedValue: Int, id: Int, metadatas: [OFEEventMetadataModel]?, potentialValue: Int, preLimitValue: Int, reason: [OFEPointsEntryReason]?, statusChangedDate: String?, statusTypeCode: String?, statusTypeKey: Int, statusTypeName: String?, typeCode: String?, typeKey: Int, typeName: String?){
        
        self.categoryCode = categoryCode
        self.categoryKey = categoryKey
        self.categoryName = categoryName
        self.earnedValue = earnedValue
        self.id = id
        self.metadatas = metadatas
        
        self.potentialValue = potentialValue
        self.preLimitValue = preLimitValue
        self.reason = reason
        self.statusChangedDate = statusChangedDate
        self.statusTypeCode = statusTypeCode
        self.statusTypeKey = statusTypeKey
        
        self.statusTypeName = statusTypeName
        self.typeCode = typeCode
        self.typeKey = typeKey
        self.typeName = typeName
        
    }
    
    public init?(json: [String: Any]) {
        
        let categoryCode = json["categoryCode"] as? String
        let categoryKey = json["categoryKey"] as? Int
        let categoryName = json["categoryName"] as? String
        let earnedValue = json["earnedValue"] as? Int
        let id = json["id"] as? Int
        
        let metadatas = OFEEventMetadataModel.createRequiredInstances(from: json, arrayKey: "metadatas")
        
        let potentialValue = json["potentialValue"] as? Int
        let preLimitValue = json["preLimitValue"] as? Int
        let reason = OFEPointsEntryReason.createRequiredInstances(from: json, arrayKey: "reason")
        let statusChangedDate = json["statusChangedDate"] as? String
        let statusTypeCode = json["statusTypeCode"] as? String
        let statusTypeKey = json["statusTypeKey"] as? Int
        
        let statusTypeName = json["statusTypeName"] as? String
        let typeCode = json["typeCode"] as? String
        let typeKey = json["typeKey"] as? Int
        let typeName = json["typeName"] as? String
        
        self.init(categoryCode: categoryCode, categoryKey: categoryKey!, categoryName: categoryName, earnedValue: earnedValue!, id: id!, metadatas: metadatas, potentialValue: potentialValue!, preLimitValue: preLimitValue!, reason: reason, statusChangedDate: statusChangedDate, statusTypeCode: statusTypeCode, statusTypeKey: statusTypeKey!, statusTypeName: statusTypeName, typeCode: typeCode, typeKey: typeKey!, typeName: typeName)
    }
}

//------------------------------------------------------

public struct OFEPointsEntryReason: CreatableFromJSON {
    
    let categoryCode: String?
    let categoryKey: Int
    let categoryName: String?
    let reasonCode: String?
    let reasonKey: Int
    let reasonName: String?
    
    init(categoryCode: String?, categoryKey: Int, categoryName: String?, reasonCode: String?, reasonKey: Int, reasonName: String?){
        
        self.categoryCode = categoryCode
        self.categoryKey = categoryKey
        self.categoryName = categoryName
        self.reasonCode = reasonCode
        self.reasonKey = reasonKey
        self.reasonName = reasonName
    }
    
    
    public init?(json: [String: Any]) {
        
        let categoryCode = json["categoryCode"] as? String
        let categoryKey = json["categoryKey"] as? Int
        let categoryName = json["categoryName"] as? String
        let reasonCode = json["reasonCode"] as? String
        let reasonKey = json["reasonKey"] as? Int
        let reasonName = json["reasonName"] as? String
        
        self.init(categoryCode: categoryCode, categoryKey: categoryKey!, categoryName: categoryName, reasonCode: reasonCode, reasonKey: reasonKey!, reasonName: reasonName)
        
    }
}


//------------------------------------------------------

public struct OFEEventMetadataModel: CreatableFromJSON {
    
    
    let typeCode: String?
    let typeKey: Int
    let typeName: String?
    let unitOfMeasure: String?
    let value: String?
    
    init(typeCode: String?, typeKey: Int, typeName: String?, unitOfMeasure: String?, value: String?){
        self.typeCode = typeCode
        self.typeKey = typeKey
        self.typeName = typeName
        self.unitOfMeasure = unitOfMeasure
        self.value = value
        
    }
    
    
    public init?(json: [String: Any]) {
        
        let typeCode = json["typeCode"] as? String
        let typeKey = json["typeKey"] as? Int
        let typeName = json["typeName"] as? String
        let unitOfMeasure = json["unitOfMeasure"] as? String
        let value = json["value"] as? String
        
        self.init(typeCode: typeCode, typeKey: typeKey!, typeName: typeName, unitOfMeasure: unitOfMeasure, value: value)
        
    }
    
    
    
    
}







