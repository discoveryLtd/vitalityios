//
//  VDPGenericUploadViewModel.swift
//  VitalityActive
//
//  Created by Simon Stewart on 7/13/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import VIAUtilities

public class VDPGenericUploadHeaderViewModel {
    public lazy var user: VDPGenericUploadHeaderUserViewModel? = {
        return VDPGenericUploadHeaderUserViewModel()
    }()
    public var partyId: Int?
    public var uploadDate: Date?
    public var sessionId: String?
    public var partnerSystem: String?
    public var partnerSystemSource: String?
    public var verified: Bool?
    public var uploadCount: Int?
    public var tenantId: Int?
    public var processingType: String?
    public var rawUploadData: String?

    public init() {}
}

public class VDPGenericUploadHeaderUserViewModel {
    public var userIdentifier: String?
    public var identifierType: String?
    public var entityNo: String?

    public init() {}
}

public class VDPGenericUploadDeviceViewModel {
    public var deviceId: String?
    public var manufacturer: String?
    public var make: String?
    public var model: String?

    public init() {}
}

public class VDPGenericUploadReadingViewModel {
    public var startTime: Date?
    public var endTime: Date?
    var duration: VDPGenericValueUnitofMeasurementViewModel?
    public var partnerCreateDate: Date?
    public var description: String?
    public var notes: String?
    public var manufacturer: String?
    public var model: String?
    public var activeDuration: Int?
    public var partnerReadingId: String?
    public var readingType: String?
    public var dataCategory: String?
    public var integrity: String?
    var workout: VDPGenericUploadReadingWorkoutViewModel?
    public var workoutIsPopulated: Bool {
        return workout != nil
    }
    public var metaDatas: [VDPGenericReadingMetaDataViewModel]?
    public func addToWorkout() -> VDPGenericUploadReadingWorkoutViewModel? {
        if workout == nil {
            workout = VDPGenericUploadReadingWorkoutViewModel()
        }
        return workout
    }

    public func addDuration(_ model: VDPGenericValueUnitofMeasurementViewModel) {
        self.duration = model
    }

    public init() {}
}

public class VDPGenericUploadReadingWorkoutViewModel {
    public var totalSteps: Int?
    var distance: VDPGenericValueUnitofMeasurementViewModel?
    var energy: VDPGenericValueUnitofMeasurementViewModel?
    var heartRate: VDPGenericUploadReadingWorkoutHeartRateViewModel?

    public func addDistance(_ model: VDPGenericValueUnitofMeasurementViewModel) {
        self.distance = model
    }

    public func addEnergy(_ model: VDPGenericValueUnitofMeasurementViewModel) {
        self.energy = model
    }

    public func addHeartRate(_ model: VDPGenericUploadReadingWorkoutHeartRateViewModel) {
        self.heartRate = model
    }

    public init() {}
}

public struct VDPGenericValueUnitofMeasurementViewModel {
    var value: Int
    var unitOfMeasurement: String
    public init(value: Int, unitOfMeasurement: String) {
        self.value = value
        self.unitOfMeasurement = unitOfMeasurement
    }
}
public struct VDPGenericReadingMetaDataViewModel {
    var name: String
    var value: Any
    public init(name: String, value: Any) {
        self.name = name
        self.value = value
    }
}
public struct VDPGenericUploadReadingWorkoutHeartRateViewModel {
    var avgMinMax: VDPGenericUploadReadingWorkoutHeartRateAvgMinMaxViewModel
    public init(avgMinMax: VDPGenericUploadReadingWorkoutHeartRateAvgMinMaxViewModel) {
        self.avgMinMax = avgMinMax
    }
}

public struct VDPGenericUploadReadingWorkoutHeartRateAvgMinMaxViewModel {
    var average: Int
    var maximum: Int
    var minimum: Int
    public init(average: Int, maximum: Int, minimum: Int) {
        self.average = average
        self.maximum = maximum
        self.minimum = minimum
    }
}

public typealias jsonElement = [String: Any]

public class VDPGenericUploadViewModel {

    public lazy var header: VDPGenericUploadHeaderViewModel? = {
        return VDPGenericUploadHeaderViewModel()
    }()
    public lazy var device: VDPGenericUploadDeviceViewModel? = {
        return VDPGenericUploadDeviceViewModel()
    }()
    public var readings: [VDPGenericUploadReadingViewModel]? = [VDPGenericUploadReadingViewModel]()

    public init() {}

    public func toJSON() -> Data {
        var json = jsonElement()

        // header
        if let header = self.header {
            var jsonHeader = jsonElement()
            // header.user
            if let user = header.user {
                var jsonUser = jsonElement()
                if let identifier = user.userIdentifier {
                    jsonUser["userIdentifier"] = identifier
                }
                if let identifierType = user.identifierType {
                    jsonUser["identifierType"] = identifierType
                }
                if let entityNo = user.entityNo {
                    jsonUser["entityNo"] = entityNo
                }
                jsonHeader["user"] = jsonUser
            }
            if let uploadDate = header.uploadDate {
                if shouldUseCustomFormatter() {
                    //PROD ISSUE 03/29
//                    jsonHeader["uploadDate"] = formatDate(dateValue: uploadDate)
                    jsonHeader["uploadDate"] = formatDate(dateValue: uploadDate, appendTimeZone: true)
                    //PROD ISSUE 03/29
                }else{
                    jsonHeader["uploadDate"] = Wire.default.apiManagerFormatterWithDateTime.string(from: uploadDate)
                }
            }
            if let sessionId = header.sessionId {
                if shouldUseCustomFormatter(), let uploadDate = header.uploadDate, let partyId = header.partyId {
                    jsonHeader["sessionId"] = "\(partyId)-\(formatDate(dateValue: uploadDate, appendTimeZone: true))"
                }else{
                    jsonHeader["sessionId"] = sessionId
                }
            }
            if let partnerSystem = header.partnerSystem {
                jsonHeader["partnerSystem"] = partnerSystem
            }
            if let partnerSystemSource = header.partnerSystemSource {
                jsonHeader["partnerSystemSource"] = partnerSystemSource
            }
            if let verified = header.verified {
                jsonHeader["verified"] = verified ? "true" : "false"
            }
            if let uploadCount = header.uploadCount {
                jsonHeader["uploadCount"] = "\(uploadCount)"
            }
            if let tenantId = header.tenantId {
                jsonHeader["tenantId"] = "\(tenantId)"
            }
            if let processingType = header.processingType {
                jsonHeader["processingType"] = processingType
            }
            if let rawUploadData = header.rawUploadData {
                jsonHeader["rawUploadData"] = rawUploadData
            }
            json["header"] = jsonHeader
        }

        // device
        if let device = self.device {
            var jsonDevice = jsonElement()
            if let deviceId = device.deviceId {
                jsonDevice["deviceId"] = deviceId
            }
            if let manufacturer = device.manufacturer {
                jsonDevice["manufacturer"] = manufacturer
            }
            if let make = device.make {
                jsonDevice["make"] = make
            }
            if let model = device.model {
                jsonDevice["model"] = model
            }
            json["device"] = jsonDevice
        }

        // readings
        if let readings = self.readings {
            var jsonReadings = [jsonElement]()

            for reading in readings {
                var jsonReading = jsonElement()

                if let startTime = reading.startTime {
                    if shouldUseCustomFormatter() {
                        //PROD ISSUE 03/29
//                        jsonReading["startTime"] = formatDate(dateValue: startTime)
                        jsonReading["startTime"] = formatDate(dateValue: startTime, appendTimeZone: true)
                        //PROD ISSUE 03/29
                    }else{
                        jsonReading["startTime"] = Wire.default.apiManagerFormatterWithDateTimeTimeZone.string(from: startTime)
                    }
                }
                if let endTime = reading.endTime {
                    if shouldUseCustomFormatter() {
                        //PROD ISSUE 03/29
//                        jsonReading["endTime"] = formatDate(dateValue: endTime)
                        if let dataCategory = reading.dataCategory, dataCategory == "ROUTINE" {
                            jsonReading["endTime"] = formatDate(dateValue: setTimeReading(dateTime: endTime), appendTimeZone: true)
                        } else {
                            jsonReading["endTime"] = formatDate(dateValue: endTime, appendTimeZone: true)
                        }
                        //PROD ISSUE 03/29
                    }else{
                        jsonReading["endTime"] = Wire.default.apiManagerFormatterWithDateTimeTimeZone.string(from: endTime)
                    }
                }
                if let duration = reading.duration {
                    var jsonDuration = jsonElement()
                    jsonDuration["value"] = String(describing: duration.value)
                    jsonDuration["unit"] = duration.unitOfMeasurement
                    jsonReading["duration"] = jsonDuration
                }
                if let partnerCreateDate = reading.partnerCreateDate {
                    jsonReading["partnerCreateDate"] = Wire.default.apiManagerFormatterWithDateTimeTimeZone.string(from: partnerCreateDate)
                }

                if let metaDatas = reading.metaDatas {
                    var jsonMetaData = [jsonElement]()
                    for metaData in metaDatas {
                        jsonMetaData.append(["name": metaData.name, "value": metaData.value])
                    }
                    jsonReading["readingMetaData"] = jsonMetaData
                }

                if let description = reading.description {
                    jsonReading["description"] = description
                }
                if let notes = reading.notes {
                    jsonReading["notes"] = notes
                }
                if let manufacturer = reading.manufacturer {
                    jsonReading["manufacturer"] = manufacturer
                }
                if let model = reading.model {
                    jsonReading["model"] = model
                }
                if let partnerReadingId = reading.partnerReadingId {
                    jsonReading["partnerReadingId"] = partnerReadingId
                }
                if let readingType = reading.readingType {
                    jsonReading["readingType"] = readingType
                }
                if let dataCategory = reading.dataCategory {
                    jsonReading["dataCategory"] = dataCategory
                }
                if let integrity = reading.integrity {
                    jsonReading["integrity"] = integrity
                }

                // workout
                if let workout = reading.workout {
                    var jsonWorkout = jsonElement()

                    // steps
                    if let totalSteps = workout.totalSteps {
                        jsonWorkout["totalSteps"] = totalSteps
                    }

                    // distance
                    if let distance = workout.distance {
                        var jsonDistance = jsonElement()
                        jsonDistance["value"] = "\(distance.value)" //TODO: format with number formatter
                        jsonDistance["unitOfMeasurement"] = distance.unitOfMeasurement
                        jsonWorkout["distance"] = jsonDistance
                    }
                    // energy expenditure
                    if let energy = workout.energy {
                        var jsonEnergy = jsonElement()
                        jsonEnergy["value"] = "\(energy.value)"
                        jsonEnergy["unitOfMeasurement"] = energy.unitOfMeasurement
                        jsonWorkout["energyExpenditure"] = jsonEnergy
                    }
                    // heart rate
                    if let heartRate = workout.heartRate {
                        var jsonHeartRate = jsonElement()
                        var jsonAvgMinMax = jsonElement()
                        jsonAvgMinMax["average"] = "\(heartRate.avgMinMax.average)"
                        jsonAvgMinMax["maximum"] = "\(heartRate.avgMinMax.maximum)"
                        jsonAvgMinMax["minimum"] = "\(heartRate.avgMinMax.minimum)"
                        jsonHeartRate["avgMinMax"] = jsonAvgMinMax
                        jsonWorkout["heartRate"] = jsonHeartRate
                    }
                    jsonReading["workout"] = jsonWorkout
                }
                jsonReadings.append(jsonReading)
            }
            json["readings"] = jsonReadings
        }

        return try! JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
    }

    func formatDate(dateValue: Date, appendTimeZone: Bool = false) -> String {
        let formatter = DateFormatter()
        
        if appendTimeZone {
            formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"
        }else{
            formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        }
        
        formatter.locale = Locale(identifier: DeviceLocale.toString())
        
        if let timezone = Bundle.main.object(forInfoDictionaryKey: "VIATimezone") as? String{
            formatter.timeZone = TimeZone(identifier: timezone)
        }
        
        return formatter.string(from: dateValue)
    }
    
    func shouldUseCustomFormatter() -> Bool{
        if let tenantID = AppSettings.getAppTenant() {
            return tenantID == .SLI || tenantID == .UKE
        }        
        return false
    }
    
    func setTimeReading(dateTime: Date?) -> Date {
        guard let dateTimeValue = dateTime else { return Date() }
        var components = DateComponents()
        components.second = -1
        return Calendar.current.date(byAdding: components, to: dateTimeValue)!
    }
}
