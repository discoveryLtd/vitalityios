//
//  ProcessEventsV2Response.swift
//  VitalityActive
//
//  Created by Swagger Codegen on 12/01/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file

import Foundation

public struct ProcessEventsV2Response: CreatableFromJSON {
    let processEventsResponse: PEV2ProcessEventsResponse?
    init(processEventsResponse: PEV2ProcessEventsResponse?) {
        self.processEventsResponse = processEventsResponse
    }
    public init?(json: [String: Any]) {
        let processEventsResponse = PEV2ProcessEventsResponse(json: json, key: "processEventsResponse")
        self.init(processEventsResponse: processEventsResponse)
    }
}

public struct PEV2ProcessEventsResponse: CreatableFromJSON {
    let ids: [PEV2Ids]?
    init(ids: [PEV2Ids]?) {
        self.ids = ids
    }
    public init?(json: [String: Any]) {
        let ids = PEV2Ids.createRequiredInstances(from: json, arrayKey: "ids")
        self.init(ids: ids)
    }
}

public struct PEV2Ids: CreatableFromJSON {
    let eventExternalReference: [PEV2EventExternalReference]? // This is a class to facilitate the language requirements of renaming type attribute to type key and adding typeCode attribute. This is for use in Outbound payloads
    let eventId: Int
    init(eventExternalReference: [PEV2EventExternalReference]?, eventId: Int) {
        self.eventExternalReference = eventExternalReference
        self.eventId = eventId
    }
    public init?(json: [String: Any]) {
        let eventExternalReference = PEV2EventExternalReference.createRequiredInstances(from: json, arrayKey: "eventExternalReference")
        guard let eventId = json["eventId"] as? Int else { debugPrint("Expected non-optional property [eventId] of type [Int] on object [PEV2Ids] but did not find");return nil; }
        self.init(eventExternalReference: eventExternalReference, eventId: eventId)
    }
}

public struct PEV2EventExternalReference: CreatableFromJSON {
    let typeCode: String? // A short name describing the; reference, association type associated to a specific key. It's purpose is to identify a unique key, in order to facilitate easy reference and understanding. The short name is used for easy identification for debugging purposes but does not necessarily exclude the option of using it as a reference in itself. For example it could be used to populate a drop down box.
    let typeKey: Int
    let typeName: String? // A detailed description of the; reference, association type associated to a specific key. Its purpose is to describe the context of the unique key in more detail, in order to facilitate easy understanding of the use of the key it describes.
    let value: String
    init(typeCode: String?, typeKey: Int, typeName: String?, value: String) {
        self.typeCode = typeCode
        self.typeKey = typeKey
        self.typeName = typeName
        self.value = value
    }
    public init?(json: [String: Any]) {
        let typeCode = json["typeCode"] as? String
        guard let typeKey = json["typeKey"] as? Int else { debugPrint("Expected non-optional property [typeKey] of type [Int] on object [PEV2EventExternalReference] but did not find");return nil; }
        let typeName = json["typeName"] as? String
        guard let value = json["value"] as? String else { debugPrint("Expected non-optional property [value] of type [String] on object [PEV2EventExternalReference] but did not find");return nil; }
        self.init(typeCode: typeCode, typeKey: typeKey, typeName: typeName, value: value)
    }
}
