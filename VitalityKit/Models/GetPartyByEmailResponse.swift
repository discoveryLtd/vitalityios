//
//  GetPartyByEmailResponse.swift
//  VitalityActive
//
//  Created by OJ Garde on 08/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.
//

public struct GetPartyByEmailOutBoundPayload: CreatableFromJSON {
    let getPartyByEmailResponse: GetPartyByEmailResponse?
    
    public init(getPartyByEmailResponse: GetPartyByEmailResponse?) {
        self.getPartyByEmailResponse = getPartyByEmailResponse
    }
    public init?(json: [String: Any]) {
        let getPartyByEmailResponse = GetPartyByEmailResponse(json: json, key: "getPartyByEmailResponse")
        self.init(getPartyByEmailResponse: getPartyByEmailResponse)
    }
    public func getPartyId() -> Int{
        return (getPartyByEmailResponse?.parties?.first?.partyId)!
    }
    public func getEffectiveFrom() -> String{
        return (getPartyByEmailResponse?.parties?.first?.generalPreferences!.first?.effectiveFrom)!
    }
    public func getEffectiveTo() -> String{
        return (getPartyByEmailResponse?.parties?.first?.generalPreferences!.first?.effectiveTo)!
    }
}

public struct GetPartyByEmailResponse: CreatableFromJSON {
    let parties: [GPBIDParty]?
    
    init(parties: [GPBIDParty]?) {
        self.parties = parties
    }
    public init?(json: [String: Any]) {
        let parties = GPBIDParty.createRequiredInstances(from: json, arrayKey: "parties")
        self.init(parties: parties)
    }
}

