//
//  GetOFEFeatureResponse.swift
//  VitalityActive
//
//  Created by Val Tomol on 16/04/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation

public struct GetOFEFeatureResponse: CreatableFromJSON {
    let getOfeFeatureResponse: OFEFeatureEvent?
    init(getOfeFeatureResponse: OFEFeatureEvent?) {
        self.getOfeFeatureResponse = getOfeFeatureResponse
    }
    public init?(json: [String : Any]) {
        let getOfeFeatureResponse = OFEFeatureEvent(json: json, key: "getOfeFeatureResponse")
        self.init(getOfeFeatureResponse: getOfeFeatureResponse)
    }
}

public struct OFEFeatureEvent: CreatableFromJSON {
    let ofeEventTypeses: [OFEFeatureEventType]?
    init(ofeEventTypeses: [OFEFeatureEventType]?) {
        self.ofeEventTypeses = ofeEventTypeses
    }
    public init?(json: [String : Any]) {
        let ofeEventTypeses = OFEFeatureEventType.createRequiredInstances(from: json, arrayKey: "ofeEventTypeses")
        self.init(ofeEventTypeses: ofeEventTypeses)
    }
}

public struct OFEFeatureEventType: CreatableFromJSON {
    let eventDetails: [OFEFeatureEventDetail]?
    let eventTypeCode: String?
    let eventTypeKey: Int?
    let eventTypeName: String?
    let featureCode: String?
    let featureKey: Int?
    let featureName: String?
    let potentialPoints: Int?
    init(eventDetails: [OFEFeatureEventDetail]?, eventTypeCode: String?, eventTypeKey: Int?, eventTypeName: String?, featureCode: String?, featureKey: Int?, featureName: String?, potentialPoints: Int?) {
        self.eventDetails = eventDetails
        self.eventTypeCode = eventTypeCode
        self.eventTypeKey = eventTypeKey
        self.eventTypeName = eventTypeName
        self.featureCode = featureCode
        self.featureKey = featureKey
        self.featureName = featureName
        self.potentialPoints = potentialPoints
    }
    public init?(json: [String : Any]) {
        let eventDetails = OFEFeatureEventDetail.createRequiredInstances(from: json, arrayKey: "eventDetail")
        let eventTypeCode = json["eventTypeCode"] as? String
        let eventTypeKey = json["eventTypeKey"] as? Int
        let eventTypeName = json["eventTypeName"] as? String
        let featureCode = json["featureCode"] as? String
        let featureKey = json["featureKey"] as? Int
        let featureName = json["featureName"] as? String
        let potentialPoints = json["potentialPoints"] as? Int
        self.init(eventDetails: eventDetails, eventTypeCode: eventTypeCode, eventTypeKey: eventTypeKey, eventTypeName: eventTypeName, featureCode: featureCode, featureKey: featureKey, featureName: featureName, potentialPoints: potentialPoints)
    }
}

public struct OFEFeatureEventDetail: CreatableFromJSON {
    let selectionGroups: [OFEFeatureSelectionGroup]?
    let metadataCode: String?
    let metadataKey: Int?
    let metadataName: String?
    init(selectionGroups: [OFEFeatureSelectionGroup]?, metadataCode: String?, metadataKey: Int?, metadataName: String?) {
        self.selectionGroups = selectionGroups
        self.metadataCode = metadataCode
        self.metadataKey = metadataKey
        self.metadataName = metadataName
    }
    public init?(json: [String : Any]) {
        let selectionGroups = OFEFeatureSelectionGroup.createRequiredInstances(from: json, arrayKey: "selectionGroups")
        let metadataCode = json["metadataCode"] as? String
        let metadataKey = json["metadataKey"] as? Int
        let metadataName = json["metadataName"] as? String
        self.init(selectionGroups: selectionGroups, metadataCode: metadataCode, metadataKey: metadataKey, metadataName: metadataName)
    }
}

public struct OFEFeatureSelectionGroup: CreatableFromJSON {
    let selectionOptions: [OFEFeatureSelectionOption]?
    let categoryCode: String?
    let categoryKey: Int?
    let categoryName: String?
    let featureCode: String?
    let featureKey: Int?
    let featureName: String?
    init(selectionOptions: [OFEFeatureSelectionOption]?, categoryCode: String?, categoryKey: Int?, categoryName: String?, featureCode: String?, featureKey: Int?, featureName: String?) {
        self.selectionOptions = selectionOptions
        self.categoryCode = categoryCode
        self.categoryKey = categoryKey
        self.categoryName = categoryName
        self.featureCode = featureCode
        self.featureKey = featureKey
        self.featureName = featureName
    }
    public init?(json: [String : Any]) {
        let selectionOptions = OFEFeatureSelectionOption.createRequiredInstances(from: json, arrayKey: "selectionOptions")
        let categoryCode = json["categoryCode"] as? String
        let categoryKey = json["categoryKey"] as? Int
        let categoryName = json["categoryName"] as? String
        let featureCode = json["featureCode"] as? String
        let featureKey = json["featureKey"] as? Int
        let featureName = json["featureName"] as? String
        self.init(selectionOptions: selectionOptions, categoryCode: categoryCode, categoryKey: categoryKey, categoryName: categoryName, featureCode: featureCode, featureKey: featureKey, featureName: featureName)
    }
}

public struct OFEFeatureSelectionOption: CreatableFromJSON {
    let featureCode: String?
    let featureKey: Int?
    let featureName: String?
    let potentialPoints: Int?
    init(featureCode: String?, featureKey: Int?, featureName: String?, potentialPoints: Int?) {
        self.featureCode = featureCode
        self.featureKey = featureKey
        self.featureName = featureName
        self.potentialPoints = potentialPoints
    }
    public init?(json: [String : Any]) {
        let featureCode = json["featureCode"] as? String
        let featureKey = json["featureKey"] as? Int
        let featureName = json["featureName"] as? String
        let potentialPoints = json["potentialPoints"] as? Int
        self.init(featureCode: featureCode, featureKey: featureKey, featureName: featureName, potentialPoints: potentialPoints)
    }
}
