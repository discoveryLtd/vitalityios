//
//  GetEligibilityContentResponse.swift
//  VitalityActive
//
//  Created by Swagger Codegen on 11/01/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file

import Foundation

public struct GetEligibilityContentResponse: CreatableFromJSON {
    let productFeatureEligibilityContent: GECProductFeatureEligibilityContent?
    init(productFeatureEligibilityContent: GECProductFeatureEligibilityContent?) {
        self.productFeatureEligibilityContent = productFeatureEligibilityContent
    }
    public init?(json: [String: Any]) {
        let productFeatureEligibilityContent = GECProductFeatureEligibilityContent(json: json, key: "productFeatureEligibilityContent")
        self.init(productFeatureEligibilityContent: productFeatureEligibilityContent)
    }
}

public struct GECProductFeatureEligibilityContent: CreatableFromJSON {
    let content: String
    let partnerURL: String
    init(content: String, partnerURL: String) {
        self.content = content
        self.partnerURL = partnerURL
    }
    public init?(json: [String: Any]) {
        guard let content = json["content"] as? String else { debugPrint("Expected non-optional property [content] of type [String] on object [GECProductFeatureEligibilityContent] but did not find");return nil; }
        guard let partnerURL = json["partnerURL"] as? String else { debugPrint("Expected non-optional property [partnerURL] of type [String] on object [GECProductFeatureEligibilityContent] but did not find");return nil; }
        self.init(content: content, partnerURL: partnerURL)
    }
}
