//
//  GetPartyByIdResponse.swift
//  VitalityActive
//
//  Created by Swagger Codegen on 11/29/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file

import Foundation

public struct GetPartyByIdResponse: CreatableFromJSON {
    let getPartyResponse: GPBIDGetPartyResponse?
    init(getPartyResponse: GPBIDGetPartyResponse?) {
        self.getPartyResponse = getPartyResponse
    }
    public init?(json: [String: Any]) {
        let getPartyResponse = GPBIDGetPartyResponse(json: json, key: "getPartyResponse")
        self.init(getPartyResponse: getPartyResponse)
    }
}

public struct GPBIDGetPartyResponse: CreatableFromJSON {
    let party: GPBIDParty?
    init(party: GPBIDParty?) {
        self.party = party
    }
    public init?(json: [String: Any]) {
        let party = GPBIDParty(json: json, key: "party")
        self.init(party: party)
    }
}

public struct GPBIDParty: CreatableFromJSON {
    let emailAddressOuts: [GPBIDEmailAddressOuts]?
    let generalPreferences: [GPBIDGeneralPreferences]?
    let geographicalAreaPreference: GPBIDGeographicalAreaPreference?
    let languagePreference: GPBIDLanguagePreference?
    let measurementSystemPreference: GPBIDMeasurementSystemPreference?
    let organisation: GPBIDOrganisation?
    let partyId: Int
    let person: GPBIDPerson?
    let physicalAddressOuts: [GPBIDPhysicalAddressOuts]?
    let references: [GPBIDReferences]?
    let telephoneOuts: [GPBIDTelephoneOuts]?
    let timeZonePreference: GPBIDTimeZonePreference?
    let webAddressOuts: [GPBIDWebAddressOuts]?
    init(emailAddressOuts: [GPBIDEmailAddressOuts]?, generalPreferences: [GPBIDGeneralPreferences]?, geographicalAreaPreference: GPBIDGeographicalAreaPreference?, languagePreference: GPBIDLanguagePreference?, measurementSystemPreference: GPBIDMeasurementSystemPreference?, organisation: GPBIDOrganisation?, partyId: Int, person: GPBIDPerson?, physicalAddressOuts: [GPBIDPhysicalAddressOuts]?, references: [GPBIDReferences]?, telephoneOuts: [GPBIDTelephoneOuts]?, timeZonePreference: GPBIDTimeZonePreference?, webAddressOuts: [GPBIDWebAddressOuts]?) {
        self.emailAddressOuts = emailAddressOuts
        self.generalPreferences = generalPreferences
        self.geographicalAreaPreference = geographicalAreaPreference
        self.languagePreference = languagePreference
        self.measurementSystemPreference = measurementSystemPreference
        self.organisation = organisation
        self.partyId = partyId
        self.person = person
        self.physicalAddressOuts = physicalAddressOuts
        self.references = references
        self.telephoneOuts = telephoneOuts
        self.timeZonePreference = timeZonePreference
        self.webAddressOuts = webAddressOuts
    }
    public init?(json: [String: Any]) {
        let emailAddressOuts = GPBIDEmailAddressOuts.createRequiredInstances(from: json, arrayKey: "emailAddressOuts")
        let generalPreferences = GPBIDGeneralPreferences.createRequiredInstances(from: json, arrayKey: "generalPreferences")
        let geographicalAreaPreference = GPBIDGeographicalAreaPreference(json: json, key: "geographicalAreaPreference")
        let languagePreference = GPBIDLanguagePreference(json: json, key: "languagePreference")
        let measurementSystemPreference = GPBIDMeasurementSystemPreference(json: json, key: "measurementSystemPreference")
        let organisation = GPBIDOrganisation(json: json, key: "organisation")
        guard let partyId = json["partyId"] as? Int else { debugPrint("Expected non-optional property [partyId] of type [Int] on object [GPBIDParty] but did not find");return nil; }
        let person = GPBIDPerson(json: json, key: "person")
        let physicalAddressOuts = GPBIDPhysicalAddressOuts.createRequiredInstances(from: json, arrayKey: "physicalAddressOuts")
        let references = GPBIDReferences.createRequiredInstances(from: json, arrayKey: "references")
        let telephoneOuts = GPBIDTelephoneOuts.createRequiredInstances(from: json, arrayKey: "telephoneOuts")
        let timeZonePreference = GPBIDTimeZonePreference(json: json, key: "timeZonePreference")
        let webAddressOuts = GPBIDWebAddressOuts.createRequiredInstances(from: json, arrayKey: "webAddressOuts")
        self.init(emailAddressOuts: emailAddressOuts, generalPreferences: generalPreferences, geographicalAreaPreference: geographicalAreaPreference, languagePreference: languagePreference, measurementSystemPreference: measurementSystemPreference, organisation: organisation, partyId: partyId, person: person, physicalAddressOuts: physicalAddressOuts, references: references, telephoneOuts: telephoneOuts, timeZonePreference: timeZonePreference, webAddressOuts: webAddressOuts)
    }
}

public struct GPBIDEmailAddressOuts: CreatableFromJSON {
    let contactRoleOuts: [GPBIDContactRoleOuts]?
    let id: Int
    let value: String
    init(contactRoleOuts: [GPBIDContactRoleOuts]?, id: Int, value: String) {
        self.contactRoleOuts = contactRoleOuts
        self.id = id
        self.value = value
    }
    public init?(json: [String: Any]) {
        let contactRoleOuts = GPBIDContactRoleOuts.createRequiredInstances(from: json, arrayKey: "contactRoleOuts")
        guard let id = json["id"] as? Int else { debugPrint("Expected non-optional property [id] of type [Int] on object [GPBIDEmailAddressOuts] but did not find");return nil; }
        guard let value = json["value"] as? String else { debugPrint("Expected non-optional property [value] of type [String] on object [GPBIDEmailAddressOuts] but did not find");return nil; }
        self.init(contactRoleOuts: contactRoleOuts, id: id, value: value)
    }
}

public struct GPBIDContactRoleOuts: CreatableFromJSON {
    let availabilityFrom: String? // The start time from which the specific contact mechanism may be used  for contact purposes in relation to the contact mechanism role type it is associated with
    let availabilityTo: String? // The end time from which the specific contact mechanism may not be used for contact purposes in relation to the contact mechanism role type it is associated with
    let contactPurposeOuts: [GPBIDContactPurposeOuts]?
    let effectiveFrom: String
    let effectiveTo: String
    let roleId: Int
    let typeCode: String? // The short name related to the type key referenced in the payload that describes the unique key for identification purposes. The short name is used for easy human context recognition
    let typeKey: Int
    let typeName: String? // The extended name of the code related to the type key referenced in the payload. The name is used to describer the key's function in more detail
    init(availabilityFrom: String?, availabilityTo: String?, contactPurposeOuts: [GPBIDContactPurposeOuts]?, effectiveFrom: String, effectiveTo: String, roleId: Int, typeCode: String?, typeKey: Int, typeName: String?) {
        self.availabilityFrom = availabilityFrom
        self.availabilityTo = availabilityTo
        self.contactPurposeOuts = contactPurposeOuts
        self.effectiveFrom = effectiveFrom
        self.effectiveTo = effectiveTo
        self.roleId = roleId
        self.typeCode = typeCode
        self.typeKey = typeKey
        self.typeName = typeName
    }
    public init?(json: [String: Any]) {
        let availabilityFrom = json["availabilityFrom"] as? String
        let availabilityTo = json["availabilityTo"] as? String
        let contactPurposeOuts = GPBIDContactPurposeOuts.createRequiredInstances(from: json, arrayKey: "contactPurposeOuts")
        guard let effectiveFrom = json["effectiveFrom"] as? String else { debugPrint("Expected non-optional property [effectiveFrom] of type [String] on object [GPBIDContactRoleOuts] but did not find");return nil; }
        guard let effectiveTo = json["effectiveTo"] as? String else { debugPrint("Expected non-optional property [effectiveTo] of type [String] on object [GPBIDContactRoleOuts] but did not find");return nil; }
        guard let roleId = json["roleId"] as? Int else { debugPrint("Expected non-optional property [roleId] of type [Int] on object [GPBIDContactRoleOuts] but did not find");return nil; }
        let typeCode = json["typeCode"] as? String
        guard let typeKey = json["typeKey"] as? Int else { debugPrint("Expected non-optional property [typeKey] of type [Int] on object [GPBIDContactRoleOuts] but did not find");return nil; }
        let typeName = json["typeName"] as? String
        self.init(availabilityFrom: availabilityFrom, availabilityTo: availabilityTo, contactPurposeOuts: contactPurposeOuts, effectiveFrom: effectiveFrom, effectiveTo: effectiveTo, roleId: roleId, typeCode: typeCode, typeKey: typeKey, typeName: typeName)
    }
}

public struct GPBIDContactPurposeOuts: CreatableFromJSON {
    let typeCode: String? // The short name related to the type key referenced in the payload that describes the unique key for identification purposes. The short name is used for easy human context  recognition
    let typeKey: Int? // The unique global key that represents a type of role purpose in terms of the following contact mechanisms
    
    let typeName: String? // The extended name of the code related to the type key referenced in the payload. The name is used to describer the key's function in more detail
    init(typeCode: String?, typeKey: Int?, typeName: String?) {
        self.typeCode = typeCode
        self.typeKey = typeKey
        self.typeName = typeName
    }
    public init?(json: [String: Any]) {
        let typeCode = json["typeCode"] as? String
        let typeKey = json["typeKey"] as? Int
        let typeName = json["typeName"] as? String
        self.init(typeCode: typeCode, typeKey: typeKey, typeName: typeName)
    }
}

public struct GPBIDGeneralPreferences: CreatableFromJSON {
    let effectiveFrom: String? // The date from which the item is effective.
    let effectiveTo: String? // The date to which the item is effective.
    let typeCode: String? // A short name describing the; reference, association type associated to a specific key. It's purpose is to identify a unique key, in order to facilitate easy reference and understanding. The short name is used for easy identification for debugging purposes but does not necessarily exclude the option of using it as a reference in itself. For example it could be used to populate a drop down box.
    let typeKey: Int
    let typeName: String? // A detailed description of the; reference, association type associated to a specific key. Its purpose is to describe the context of the unique key in more detail, in order to facilitate easy understanding of the use of the key it describes.
    let value: String? // The value that applies to the reference, association, metatdata, or source.
    init(effectiveFrom: String?, effectiveTo: String?, typeCode: String?, typeKey: Int, typeName: String?, value: String?) {
        self.effectiveFrom = effectiveFrom
        self.effectiveTo = effectiveTo
        self.typeCode = typeCode
        self.typeKey = typeKey
        self.typeName = typeName
        self.value = value
    }
    public init?(json: [String: Any]) {
        let effectiveFrom = json["effectiveFrom"] as? String
        let effectiveTo = json["effectiveTo"] as? String
        let typeCode = json["typeCode"] as? String
        guard let typeKey = json["typeKey"] as? Int else { debugPrint("Expected non-optional property [typeKey] of type [Int] on object [GPBIDGeneralPreferences] but did not find");return nil; }
        let typeName = json["typeName"] as? String
        let value = json["value"] as? String
        self.init(effectiveFrom: effectiveFrom, effectiveTo: effectiveTo, typeCode: typeCode, typeKey: typeKey, typeName: typeName, value: value)
    }
}

public struct GPBIDGeographicalAreaPreference: CreatableFromJSON {
    let effectiveFrom: String
    let effectiveTo: String
    let typeCode: String? // A short name describing the; reference, association type associated to a specific key. It's purpose is to identify a unique key, in order to facilitate easy reference and understanding. The short name is used for easy identification for debugging purposes but does not necessarily exclude the option of using it as a reference in itself. For example it could be used to populate a drop down box.
    let typeKey: Int
    let typeName: String? // A detailed description of the; reference, association type associated to a specific key. Its purpose is to describe the context of the unique key in more detail, in order to facilitate easy understanding of the use of the key it describes.
    let value: String? // The value that applies to the reference, association, metatdata, or source.
    init(effectiveFrom: String, effectiveTo: String, typeCode: String?, typeKey: Int, typeName: String?, value: String?) {
        self.effectiveFrom = effectiveFrom
        self.effectiveTo = effectiveTo
        self.typeCode = typeCode
        self.typeKey = typeKey
        self.typeName = typeName
        self.value = value
    }
    public init?(json: [String: Any]) {
        guard let effectiveFrom = json["effectiveFrom"] as? String else { debugPrint("Expected non-optional property [effectiveFrom] of type [String] on object [GPBIDGeographicalAreaPreference] but did not find");return nil; }
        guard let effectiveTo = json["effectiveTo"] as? String else { debugPrint("Expected non-optional property [effectiveTo] of type [String] on object [GPBIDGeographicalAreaPreference] but did not find");return nil; }
        let typeCode = json["typeCode"] as? String
        guard let typeKey = json["typeKey"] as? Int else { debugPrint("Expected non-optional property [typeKey] of type [Int] on object [GPBIDGeographicalAreaPreference] but did not find");return nil; }
        let typeName = json["typeName"] as? String
        let value = json["value"] as? String
        self.init(effectiveFrom: effectiveFrom, effectiveTo: effectiveTo, typeCode: typeCode, typeKey: typeKey, typeName: typeName, value: value)
    }
}

public struct GPBIDLanguagePreference: CreatableFromJSON {
    let iSOCode: String? // A short name describing the; reference, association type associated to a specific key. It's purpose is to identify a unique key, in order to facilitate easy reference and understanding. The short name is used for easy identification for debugging purposes but does not necessarily exclude the option of using it as a reference in itself. For example it could be used to populate a drop down box.
    let value: String? // The value that applies to the reference, association, metatdata, or source.
    init(iSOCode: String?, value: String?) {
        self.iSOCode = iSOCode
        self.value = value
    }
    public init?(json: [String: Any]) {
        let iSOCode = json["iSOCode"] as? String
        let value = json["value"] as? String
        self.init(iSOCode: iSOCode, value: value)
    }
}

public struct GPBIDMeasurementSystemPreference: CreatableFromJSON {
    let typeCode: String? // A short name describing the; reference, association type associated to a specific key. It's purpose is to identify a unique key, in order to facilitate easy reference and understanding. The short name is used for easy identification for debugging purposes but does not necessarily exclude the option of using it as a reference in itself. For example it could be used to populate a drop down box.
    let typeKey: Int? // A unique global ID that identifies a type of; reference, association, source, across the Vitality system regardless of the market the system is deployed in. Its purpose is to convey a particular meaning consistently across all market implementations, regardless of language used. In other words, the use of a unique global id permits the ubiquitous use of a single piece of text describing the same meaning in the system but referenced in various languages of a specific market implementation.
    let typeName: String? // A detailed description of the; reference, association type associated to a specific key. Its purpose is to describe the context of the unique key in more detail, in order to facilitate easy understanding of the use of the key it describes.
    init(typeCode: String?, typeKey: Int?, typeName: String?) {
        self.typeCode = typeCode
        self.typeKey = typeKey
        self.typeName = typeName
    }
    public init?(json: [String: Any]) {
        let typeCode = json["typeCode"] as? String
        let typeKey = json["typeKey"] as? Int
        let typeName = json["typeName"] as? String
        self.init(typeCode: typeCode, typeKey: typeKey, typeName: typeName)
    }
}

public struct GPBIDOrganisation: CreatableFromJSON {
    let category: String?
    let categoryName: String? // A detailed description of the; reference, association type associated to a specific key. Its purpose is to describe the context of the unique key in more detail, in order to facilitate easy understanding of the use of the key it describes.
    let name: String
    let orgainsationTypeTypeCode: String? // The short name related to the type key referenced in the payload that describes the unique key for identification purposes. The short name is used for easy human context  recognition
    let orgainsationTypeTypeKey: Int
    let orgainsationTypeTypeName: String? // The extended name of the code related to the type key referenced in the payload. The name is used to describer the key's function in more detail
    let registeredDate: String
    let shortName: String? // The text value identifying the specific Organisation
    let statusTypeCode: String? // A short name describing the; reference, association type associated to a specific key. It's purpose is to identify a unique key, in order to facilitate easy reference and understanding. The short name is used for easy identification for debugging purposes but does not necessarily exclude the option of using it as a reference in itself. For example it could be used to populate a drop down box.
    let statusTypeKey: Int
    let statusTypeName: String? // A detailed description of the; reference, association type associated to a specific key. Its purpose is to describe the context of the unique key in more detail, in order to facilitate easy understanding of the use of the key it describes.
    let suffix: String? // Any text representing additional information related to the name of the Organisation e.g. PTY LTD
    let tradingAs: String? // An additional name should the Organisation be trading under a different name
    init(category: String?, categoryName: String?, name: String, orgainsationTypeTypeCode: String?, orgainsationTypeTypeKey: Int, orgainsationTypeTypeName: String?, registeredDate: String, shortName: String?, statusTypeCode: String?, statusTypeKey: Int, statusTypeName: String?, suffix: String?, tradingAs: String?) {
        self.category = category
        self.categoryName = categoryName
        self.name = name
        self.orgainsationTypeTypeCode = orgainsationTypeTypeCode
        self.orgainsationTypeTypeKey = orgainsationTypeTypeKey
        self.orgainsationTypeTypeName = orgainsationTypeTypeName
        self.registeredDate = registeredDate
        self.shortName = shortName
        self.statusTypeCode = statusTypeCode
        self.statusTypeKey = statusTypeKey
        self.statusTypeName = statusTypeName
        self.suffix = suffix
        self.tradingAs = tradingAs
    }
    public init?(json: [String: Any]) {
        let category = json["category"] as? String
        let categoryName = json["categoryName"] as? String
        guard let name = json["name"] as? String else { debugPrint("Expected non-optional property [name] of type [String] on object [GPBIDOrganisation] but did not find");return nil; }
        let orgainsationTypeTypeCode = json["orgainsationTypeTypeCode"] as? String
        guard let orgainsationTypeTypeKey = json["orgainsationTypeTypeKey"] as? Int else { debugPrint("Expected non-optional property [orgainsationTypeTypeKey] of type [Int] on object [GPBIDOrganisation] but did not find");return nil; }
        let orgainsationTypeTypeName = json["orgainsationTypeTypeName"] as? String
        guard let registeredDate = json["registeredDate"] as? String else { debugPrint("Expected non-optional property [registeredDate] of type [String] on object [GPBIDOrganisation] but did not find");return nil; }
        let shortName = json["shortName"] as? String
        let statusTypeCode = json["statusTypeCode"] as? String
        guard let statusTypeKey = json["statusTypeKey"] as? Int else { debugPrint("Expected non-optional property [statusTypeKey] of type [Int] on object [GPBIDOrganisation] but did not find");return nil; }
        let statusTypeName = json["statusTypeName"] as? String
        let suffix = json["suffix"] as? String
        let tradingAs = json["tradingAs"] as? String
        self.init(category: category, categoryName: categoryName, name: name, orgainsationTypeTypeCode: orgainsationTypeTypeCode, orgainsationTypeTypeKey: orgainsationTypeTypeKey, orgainsationTypeTypeName: orgainsationTypeTypeName, registeredDate: registeredDate, shortName: shortName, statusTypeCode: statusTypeCode, statusTypeKey: statusTypeKey, statusTypeName: statusTypeName, suffix: suffix, tradingAs: tradingAs)
    }
}

public struct GPBIDPerson: CreatableFromJSON {
    let bornOn: String? // The date and year when the party was born.
    
    let familyName: String? // A detailed description of the; reference, association type associated to a specific key. Its purpose is to describe the context of the unique key in more detail, in order to facilitate easy understanding of the use of the key it describes.
    let genderTypeCode: String? // A short name describing the; reference, association type associated to a specific key. It's purpose is to identify a unique key, in order to facilitate easy reference and understanding. The short name is used for easy identification for debugging purposes but does not necessarily exclude the option of using it as a reference in itself. For example it could be used to populate a drop down box.
    let genderTypeKey: Int
    let genderTypeName: String? // A detailed description of the; reference, association type associated to a specific key. Its purpose is to describe the context of the unique key in more detail, in order to facilitate easy understanding of the use of the key it describes.
    let givenName: String? // A detailed description of the; reference, association type associated to a specific key. Its purpose is to describe the context of the unique key in more detail, in order to facilitate easy understanding of the use of the key it describes.
    let language: String?
    let middleNames: String?
    let preferredName: String? // A detailed description of the; reference, association type associated to a specific key. Its purpose is to describe the context of the unique key in more detail, in order to facilitate easy understanding of the use of the key it describes.
    let suffix: String?
    let titleTypeCode: String? // A short name describing the; reference, association type associated to a specific key. It's purpose is to identify a unique key, in order to facilitate easy reference and understanding. The short name is used for easy identification for debugging purposes but does not necessarily exclude the option of using it as a reference in itself. For example it could be used to populate a drop down box.
    let titleTypeKey: Int? // A unique global ID that identifies a type of; reference, association, source, across the Vitality system regardless of the market the system is deployed in. Its purpose is to convey a particular meaning consistently across all market implementations, regardless of language used. In other words, the use of a unique global id permits the ubiquitous use of a single piece of text describing the same meaning in the system but referenced in various languages of a specific market implementation.
    let titleTypeName: String? // A detailed description of the; reference, association type associated to a specific key. Its purpose is to describe the context of the unique key in more detail, in order to facilitate easy understanding of the use of the key it describes.
    init(bornOn: String?, familyName: String?, genderTypeCode: String?, genderTypeKey: Int, genderTypeName: String?, givenName: String?, language: String?, middleNames: String?, preferredName: String?, suffix: String?, titleTypeCode: String?, titleTypeKey: Int?, titleTypeName: String?) {
        self.bornOn = bornOn
        self.familyName = familyName
        self.genderTypeCode = genderTypeCode
        self.genderTypeKey = genderTypeKey
        self.genderTypeName = genderTypeName
        self.givenName = givenName
        self.language = language
        self.middleNames = middleNames
        self.preferredName = preferredName
        self.suffix = suffix
        self.titleTypeCode = titleTypeCode
        self.titleTypeKey = titleTypeKey
        self.titleTypeName = titleTypeName
    }
    public init?(json: [String: Any]) {
        let bornOn = json["bornOn"] as? String
        let familyName = json["familyName"] as? String
        let genderTypeCode = json["genderTypeCode"] as? String
        guard let genderTypeKey = json["genderTypeKey"] as? Int else { debugPrint("Expected non-optional property [genderTypeKey] of type [Int] on object [GPBIDPerson] but did not find");return nil; }
        let genderTypeName = json["genderTypeName"] as? String
        let givenName = json["givenName"] as? String
        let language = json["language"] as? String
        let middleNames = json["middleNames"] as? String
        let preferredName = json["preferredName"] as? String
        let suffix = json["suffix"] as? String
        let titleTypeCode = json["titleTypeCode"] as? String
        let titleTypeKey = json["titleTypeKey"] as? Int
        let titleTypeName = json["titleTypeName"] as? String
        self.init(bornOn: bornOn, familyName: familyName, genderTypeCode: genderTypeCode, genderTypeKey: genderTypeKey, genderTypeName: genderTypeName, givenName: givenName, language: language, middleNames: middleNames, preferredName: preferredName, suffix: suffix, titleTypeCode: titleTypeCode, titleTypeKey: titleTypeKey, titleTypeName: titleTypeName)
    }
}

public struct GPBIDPhysicalAddressOuts: CreatableFromJSON {
    let complex: String? // The value of the party contact. E.g. If the Type is Email then the value is the actual email address
    let contactRoleOuts: [GPBIDContactRoleOuts]?
    let country: String?
    let id: Int
    let pOBox: Bool? // when enabled disenable complex field
    let place: String?
    let postalCode: Int? // A short name describing the; reference, association type associated to a specific key. It's purpose is to identify a unique key, in order to facilitate easy reference and understanding. The short name is used for easy identification for debugging purposes but does not necessarily exclude the option of using it as a reference in itself. For example it could be used to populate a drop down box.
    let streetAddress1: String?
    let streetAddress2: String?
    let streetAddress3: String?
    let unitNumber: Int? // Type of the Party Contact Mecahnism E.g. Email, MobileNumber
    init(complex: String?, contactRoleOuts: [GPBIDContactRoleOuts]?, country: String?, id: Int, pOBox: Bool?, place: String?, postalCode: Int?, streetAddress1: String?, streetAddress2: String?, streetAddress3: String?, unitNumber: Int?) {
        self.complex = complex
        self.contactRoleOuts = contactRoleOuts
        self.country = country
        self.id = id
        self.pOBox = pOBox
        self.place = place
        self.postalCode = postalCode
        self.streetAddress1 = streetAddress1
        self.streetAddress2 = streetAddress2
        self.streetAddress3 = streetAddress3
        self.unitNumber = unitNumber
    }
    public init?(json: [String: Any]) {
        let complex = json["complex"] as? String
        let contactRoleOuts = GPBIDContactRoleOuts.createRequiredInstances(from: json, arrayKey: "contactRoleOuts")
        let country = json["country"] as? String
        guard let id = json["id"] as? Int else { debugPrint("Expected non-optional property [id] of type [Int] on object [GPBIDPhysicalAddressOuts] but did not find");return nil; }
        let pOBox = json["pOBox"] as? Bool
        let place = json["place"] as? String
        let postalCode = json["postalCode"] as? Int
        let streetAddress1 = json["streetAddress1"] as? String
        let streetAddress2 = json["streetAddress2"] as? String
        let streetAddress3 = json["streetAddress3"] as? String
        let unitNumber = json["unitNumber"] as? Int
        self.init(complex: complex, contactRoleOuts: contactRoleOuts, country: country, id: id, pOBox: pOBox, place: place, postalCode: postalCode, streetAddress1: streetAddress1, streetAddress2: streetAddress2, streetAddress3: streetAddress3, unitNumber: unitNumber)
    }
}

public struct GPBIDReferences: CreatableFromJSON {
    let comments: String?
    let effectiveFrom: String? // The start date of the validity of the reference.
    let effectiveTo: String? // The end date of the validity of the reference.
    
    let issuedBy: Int? // The origin of the reference.
    let typeCode: String? // A short name describing the; reference, association type associated to a specific key. It's purpose is to identify a unique key, in order to facilitate easy reference and understanding. The short name is used for easy identification for debugging purposes but does not necessarily exclude the option of using it as a reference in itself. For example it could be used to populate a drop down box.
    let typeKey: Int
    let typeName: String? // A detailed description of the; reference, association type associated to a specific key. Its purpose is to describe the context of the unique key in more detail, in order to facilitate easy understanding of the use of the key it describes.
    let value: String? // The actual value being submitted for PartyReference.
    
    init(comments: String?, effectiveFrom: String?, effectiveTo: String?, issuedBy: Int?, typeCode: String?, typeKey: Int, typeName: String?, value: String?) {
        self.comments = comments
        self.effectiveFrom = effectiveFrom
        self.effectiveTo = effectiveTo
        self.issuedBy = issuedBy
        self.typeCode = typeCode
        self.typeKey = typeKey
        self.typeName = typeName
        self.value = value
    }
    public init?(json: [String: Any]) {
        let comments = json["comments"] as? String
        let effectiveFrom = json["effectiveFrom"] as? String
        let effectiveTo = json["effectiveTo"] as? String
        let issuedBy = json["issuedBy"] as? Int
        let typeCode = json["typeCode"] as? String
        guard let typeKey = json["typeKey"] as? Int else { debugPrint("Expected non-optional property [typeKey] of type [Int] on object [GPBIDReferences] but did not find");return nil; }
        let typeName = json["typeName"] as? String
        let value = json["value"] as? String
        self.init(comments: comments, effectiveFrom: effectiveFrom, effectiveTo: effectiveTo, issuedBy: issuedBy, typeCode: typeCode, typeKey: typeKey, typeName: typeName, value: value)
    }
}

public struct GPBIDTelephoneOuts: CreatableFromJSON {
    let areaDialCode: String
    let contactNumber: String
    let contactRoleOuts: [GPBIDContactRoleOuts]?
    let countryDialCode: String
    let `extension`: String?
    let id: Int
    let typeCode: String
    let typeKey: Int
    let typeName: String
    init(areaDialCode: String, contactNumber: String, contactRoleOuts: [GPBIDContactRoleOuts]?, countryDialCode: String, `extension`: String?, id: Int, typeCode: String, typeKey: Int, typeName: String) {
        self.areaDialCode = areaDialCode
        self.contactNumber = contactNumber
        self.contactRoleOuts = contactRoleOuts
        self.countryDialCode = countryDialCode
        self.`extension` = `extension`
        self.id = id
        self.typeCode = typeCode
        self.typeKey = typeKey
        self.typeName = typeName
    }
    public init?(json: [String: Any]) {
        guard let areaDialCode = json["areaDialCode"] as? String else { debugPrint("Expected non-optional property [areaDialCode] of type [String] on object [GPBIDTelephoneOuts] but did not find");return nil; }
        guard let contactNumber = json["contactNumber"] as? String else { debugPrint("Expected non-optional property [contactNumber] of type [String] on object [GPBIDTelephoneOuts] but did not find");return nil; }
        let contactRoleOuts = GPBIDContactRoleOuts.createRequiredInstances(from: json, arrayKey: "contactRoleOuts")
        guard let countryDialCode = json["countryDialCode"] as? String else { debugPrint("Expected non-optional property [countryDialCode] of type [String] on object [GPBIDTelephoneOuts] but did not find");return nil; }
        let `extension` = json["`extension`"] as? String
        guard let id = json["id"] as? Int else { debugPrint("Expected non-optional property [id] of type [Int] on object [GPBIDTelephoneOuts] but did not find");return nil; }
        guard let typeCode = json["typeCode"] as? String else { debugPrint("Expected non-optional property [typeCode] of type [String] on object [GPBIDTelephoneOuts] but did not find");return nil; }
        guard let typeKey = json["typeKey"] as? Int else { debugPrint("Expected non-optional property [typeKey] of type [Int] on object [GPBIDTelephoneOuts] but did not find");return nil; }
        guard let typeName = json["typeName"] as? String else { debugPrint("Expected non-optional property [typeName] of type [String] on object [GPBIDTelephoneOuts] but did not find");return nil; }
        self.init(areaDialCode: areaDialCode, contactNumber: contactNumber, contactRoleOuts: contactRoleOuts, countryDialCode: countryDialCode, `extension`: `extension`, id: id, typeCode: typeCode, typeKey: typeKey, typeName: typeName)
    }
}

public struct GPBIDTimeZonePreference: CreatableFromJSON {
    let code: String
    let daylightSavings: Bool
    let hourOffset: Int?
    let minuteOffset: Int?
    let value: String
    init(code: String, daylightSavings: Bool, hourOffset: Int?, minuteOffset: Int?, value: String) {
        self.code = code
        self.daylightSavings = daylightSavings
        self.hourOffset = hourOffset
        self.minuteOffset = minuteOffset
        self.value = value
    }
    public init?(json: [String: Any]) {
        guard let code = json["code"] as? String else { debugPrint("Expected non-optional property [code] of type [String] on object [GPBIDTimeZonePreference] but did not find");return nil; }
        guard let daylightSavings = json["daylightSavings"] as? Bool else { debugPrint("Expected non-optional property [daylightSavings] of type [Bool] on object [GPBIDTimeZonePreference] but did not find");return nil; }
        let hourOffset = json["hourOffset"] as? Int
        let minuteOffset = json["minuteOffset"] as? Int
        guard let value = json["value"] as? String else { debugPrint("Expected non-optional property [value] of type [String] on object [GPBIDTimeZonePreference] but did not find");return nil; }
        self.init(code: code, daylightSavings: daylightSavings, hourOffset: hourOffset, minuteOffset: minuteOffset, value: value)
    }
}

public struct GPBIDWebAddressOuts: CreatableFromJSON {
    let contactRoleOuts: [GPBIDContactRoleOuts]?
    let id: Int
    let uRL: String
    init(contactRoleOuts: [GPBIDContactRoleOuts]?, id: Int, uRL: String) {
        self.contactRoleOuts = contactRoleOuts
        self.id = id
        self.uRL = uRL
    }
    public init?(json: [String: Any]) {
        let contactRoleOuts = GPBIDContactRoleOuts.createRequiredInstances(from: json, arrayKey: "contactRoleOuts")
        guard let id = json["id"] as? Int else { debugPrint("Expected non-optional property [id] of type [Int] on object [GPBIDWebAddressOuts] but did not find");return nil; }
        guard let uRL = json["uRL"] as? String else { debugPrint("Expected non-optional property [uRL] of type [String] on object [GPBIDWebAddressOuts] but did not find");return nil; }
        self.init(contactRoleOuts: contactRoleOuts, id: id, uRL: uRL)
    }
}
