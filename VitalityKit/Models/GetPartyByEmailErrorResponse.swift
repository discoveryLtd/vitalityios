//
//  GetPartyByEmailErrorResponse.swift
//  VitalityActive
//
//  Created by OJ Garde on 08/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.
//

public struct GetPartyByEmailErrorResponse: CreatableFromJSON {
    let errors: [GetPartyByEmailError]?
    
    public init(errors: [GetPartyByEmailError]?) {
        self.errors = errors
    }
    public init?(json: [String: Any]) {
        let errors = GetPartyByEmailError.createRequiredInstances(from: json, arrayKey: "errors")
        self.init(errors: errors)
    }
    public func getErrorCode() -> Int {
        return self.errors!.first!.code
    }
}

public struct GetPartyByEmailError: CreatableFromJSON{
    let code: Int
    let message: String
    let type: String
    let context: String
    let service: String
    
    public init(code:Int, message:String, type:String, context:String, service:String) {
        self.code       = code
        self.message    = message
        self.type       = type
        self.context    = context
        self.service    = service
    }
    
    public init?(json: [String: Any]) {
        self.code       = json["code"] as? Int ?? 0
        self.message    = json["message"] as? String ?? ""
        self.type       = json["type"] as? String ?? ""
        self.context    = json["context"] as? String ?? ""
        self.service    = json["service"] as? String ?? ""
    }
}
