//
//  ResendInsurerCodeResponse.swift
//  VitalityActive
//
//  Created by Dexter Anthony Ambrad on 3/22/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

public struct ResendInsurerCodeOutBoundPayload: CreatableFromJSON {
    let resendInsurerCodeResponse: ResendInsurerCodeResponse?
    
    public init(resendInsurerCodeResponse: ResendInsurerCodeResponse?) {
        self.resendInsurerCodeResponse = resendInsurerCodeResponse
    }
    public init?(json: [String: Any]) {
        // TODO: Change key depending on the response
        let resendInsurerCodeResponse = ResendInsurerCodeResponse(json: json, key: "resendInsurerCodeResponse") // TODO: Change depending the response
        self.init(resendInsurerCodeResponse: resendInsurerCodeResponse)
    }
}

public struct ResendInsurerCodeResponse: CreatableFromJSON {
    
    public init?(json: [String: Any]) {
        
    }
}
