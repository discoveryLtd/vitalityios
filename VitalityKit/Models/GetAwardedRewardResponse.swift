//
//  GetAwardedRewardResponse.swift
//  VitalityActive
//
//  Created by Swagger Codegen on 11/22/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file

import Foundation

public struct GetAwardedRewardResponse: CreatableFromJSON {
    let awardedRewards: [GEARAwardedRewards]? // The details of a reward that has been awarded to a member
    init(awardedRewards: [GEARAwardedRewards]?) {
        self.awardedRewards = awardedRewards
    }
    public init?(json: [String: Any]) {
        let awardedRewards = GEARAwardedRewards.createRequiredInstances(from: json, arrayKey: "awardedRewards")
        self.init(awardedRewards: awardedRewards)
    }
}

public struct GEARAwardedRewards: CreatableFromJSON {
    let agreementPartyId: Int
    let awardedOn: String
    let awardedRewardReferences: [GEARAwardedRewardReferences]? // A reference related to the entity that provided the reward.
    let awardedRewardStatuses: [GEARAwardedRewardStatus]? // The status of the awarded reward with a status change history
    let effectiveFrom: String
    let effectiveTo: String
    let eventId: Int?
    let exchange: GEARExchange? 
    let id: Int
    let inboundFinancialInstructionId: Int? // Id of inbound financial instruction that was created as a result of the awarded rewardOnly applicable if the reward is a financial reward
    let outcomeRewardValueLinkId: Int? // The ID of the Reward Value Link which has been determined to be the outcome of the probabilistic reward E.g. For a WheelSpin the determined outcome could have been a StarBucks $5 voucher
    let partyId: Int
    let reward: GEARReward
    let rewardProviderPartyId: Int
    let rewardSelectionType: GEARRewardSelectionType? 
    let rewardValue: GEARRewardValue
    init(agreementPartyId: Int, awardedOn: String, awardedRewardReferences: [GEARAwardedRewardReferences]?, awardedRewardStatuses: [GEARAwardedRewardStatus]?, effectiveFrom: String, effectiveTo: String, eventId: Int?, exchange: GEARExchange?, id: Int, inboundFinancialInstructionId: Int?, outcomeRewardValueLinkId: Int?, partyId: Int, reward: GEARReward, rewardProviderPartyId: Int, rewardSelectionType: GEARRewardSelectionType?, rewardValue: GEARRewardValue) {
        self.agreementPartyId = agreementPartyId
        self.awardedOn = awardedOn
        self.awardedRewardReferences = awardedRewardReferences
        self.awardedRewardStatuses = awardedRewardStatuses
        self.effectiveFrom = effectiveFrom
        self.effectiveTo = effectiveTo
        self.eventId = eventId
        self.exchange = exchange
        self.id = id
        self.inboundFinancialInstructionId = inboundFinancialInstructionId
        self.outcomeRewardValueLinkId = outcomeRewardValueLinkId
        self.partyId = partyId
        self.reward = reward
        self.rewardProviderPartyId = rewardProviderPartyId
        self.rewardSelectionType = rewardSelectionType
        self.rewardValue = rewardValue
    }
    public init?(json: [String: Any]) {
        guard let agreementPartyId = json["agreementPartyId"] as? Int else { debugPrint("Expected non-optional property [agreementPartyId] of type [Int] on object [GEARAwardedRewards] but did not find");return nil; }
        guard let awardedOn = json["awardedOn"] as? String else { debugPrint("Expected non-optional property [awardedOn] of type [String] on object [GEARAwardedRewards] but did not find");return nil; }
        let awardedRewardReferences = GEARAwardedRewardReferences.createRequiredInstances(from: json, arrayKey: "awardedRewardReferences")
        let awardedRewardStatuses = GEARAwardedRewardStatus.createRequiredInstances(from: json, arrayKey: "awardedRewardStatuses")
        guard let effectiveFrom = json["effectiveFrom"] as? String else { debugPrint("Expected non-optional property [effectiveFrom] of type [String] on object [GEARAwardedRewards] but did not find");return nil; }
        guard let effectiveTo = json["effectiveTo"] as? String else { debugPrint("Expected non-optional property [effectiveTo] of type [String] on object [GEARAwardedRewards] but did not find");return nil; }
        let eventId = json["eventId"] as? Int
        let exchange = GEARExchange(json: json, key: "exchange")
        guard let id = json["id"] as? Int else { debugPrint("Expected non-optional property [id] of type [Int] on object [GEARAwardedRewards] but did not find");return nil; }
        let inboundFinancialInstructionId = json["inboundFinancialInstructionId"] as? Int
        let outcomeRewardValueLinkId = json["outcomeRewardValueLinkId"] as? Int
        guard let partyId = json["partyId"] as? Int else { debugPrint("Expected non-optional property [partyId] of type [Int] on object [GEARAwardedRewards] but did not find");return nil; }
        guard let reward = GEARReward(json: json, key: "reward") else { debugPrint("Expected non-optional property [reward] of type [GEARReward] on object [GEARAwardedRewards] but did not find");return nil; }
        guard let rewardProviderPartyId = json["rewardProviderPartyId"] as? Int else { debugPrint("Expected non-optional property [rewardProviderPartyId] of type [Int] on object [GEARAwardedRewards] but did not find");return nil; }
        let rewardSelectionType = GEARRewardSelectionType(json: json, key: "rewardSelectionType")
        guard let rewardValue = GEARRewardValue(json: json, key: "rewardValue") else { debugPrint("Expected non-optional property [rewardValue] of type [GEARRewardValue] on object [GEARAwardedRewards] but did not find");return nil; }
        self.init(agreementPartyId: agreementPartyId, awardedOn: awardedOn, awardedRewardReferences: awardedRewardReferences, awardedRewardStatuses: awardedRewardStatuses, effectiveFrom: effectiveFrom, effectiveTo: effectiveTo, eventId: eventId, exchange: exchange, id: id, inboundFinancialInstructionId: inboundFinancialInstructionId, outcomeRewardValueLinkId: outcomeRewardValueLinkId, partyId: partyId, reward: reward, rewardProviderPartyId: rewardProviderPartyId, rewardSelectionType: rewardSelectionType, rewardValue: rewardValue)
    }
}

public struct GEARAwardedRewardReferences: CreatableFromJSON {
    let typeCode: String? 
    let typeKey: Int
    let typeName: String? 
    let value: String
    init(typeCode: String?, typeKey: Int, typeName: String?, value: String) {
        self.typeCode = typeCode
        self.typeKey = typeKey
        self.typeName = typeName
        self.value = value
    }
    public init?(json: [String: Any]) {
        let typeCode = json["typeCode"] as? String
        guard let typeKey = json["typeKey"] as? Int else { debugPrint("Expected non-optional property [typeKey] of type [Int] on object [GEARAwardedRewardReferences] but did not find");return nil; }
        let typeName = json["typeName"] as? String
        guard let value = json["value"] as? String else { debugPrint("Expected non-optional property [value] of type [String] on object [GEARAwardedRewardReferences] but did not find");return nil; }
        self.init(typeCode: typeCode, typeKey: typeKey, typeName: typeName, value: value)
    }
}

public struct GEARAwardedRewardStatus: CreatableFromJSON {
    let code: String? 
    let effectiveOn: String
    let key: Int
    let name: String? 
    let partyId: Int
    init(code: String?, effectiveOn: String, key: Int, name: String?, partyId: Int) {
        self.code = code
        self.effectiveOn = effectiveOn
        self.key = key
        self.name = name
        self.partyId = partyId
    }
    public init?(json: [String: Any]) {
        let code = json["code"] as? String
        guard let effectiveOn = json["effectiveOn"] as? String else { debugPrint("Expected non-optional property [effectiveOn] of type [String] on object [GEARAwardedRewardStatus] but did not find");return nil; }
        guard let key = json["key"] as? Int else { debugPrint("Expected non-optional property [key] of type [Int] on object [GEARAwardedRewardStatus] but did not find");return nil; }
        let name = json["name"] as? String
        guard let partyId = json["partyId"] as? Int else { debugPrint("Expected non-optional property [partyId] of type [Int] on object [GEARAwardedRewardStatus] but did not find");return nil; }
        self.init(code: code, effectiveOn: effectiveOn, key: key, name: name, partyId: partyId)
    }
}

public struct GEARExchange: CreatableFromJSON {
    let exchangeOn: String
    let fromRewardId: Int
    let toRewardId: Int
    let typeCode: String? 
    let typeKey: Int
    let typeName: String? 
    init(exchangeOn: String, fromRewardId: Int, toRewardId: Int, typeCode: String?, typeKey: Int, typeName: String?) {
        self.exchangeOn = exchangeOn
        self.fromRewardId = fromRewardId
        self.toRewardId = toRewardId
        self.typeCode = typeCode
        self.typeKey = typeKey
        self.typeName = typeName
    }
    public init?(json: [String: Any]) {
        guard let exchangeOn = json["exchangeOn"] as? String else { debugPrint("Expected non-optional property [exchangeOn] of type [String] on object [GEARExchange] but did not find");return nil; }
        guard let fromRewardId = json["fromRewardId"] as? Int else { debugPrint("Expected non-optional property [fromRewardId] of type [Int] on object [GEARExchange] but did not find");return nil; }
        guard let toRewardId = json["toRewardId"] as? Int else { debugPrint("Expected non-optional property [toRewardId] of type [Int] on object [GEARExchange] but did not find");return nil; }
        let typeCode = json["typeCode"] as? String
        guard let typeKey = json["typeKey"] as? Int else { debugPrint("Expected non-optional property [typeKey] of type [Int] on object [GEARExchange] but did not find");return nil; }
        let typeName = json["typeName"] as? String
        self.init(exchangeOn: exchangeOn, fromRewardId: fromRewardId, toRewardId: toRewardId, typeCode: typeCode, typeKey: typeKey, typeName: typeName)
    }
}

public struct GEARReward: CreatableFromJSON {
    let categoryCode: String?
    let categoryKey: Int
    let categoryName: String? 
    let id: Int
    let key: Int
    let name: String
    let optionTypeCode: String? 
    let optionTypeKey: Int
    let optionTypeName: String
    let providedByPartyId: Int
    let typeCategoryCode: String? 
    let typeCategoryKey: Int
    let typeCategoryName: String? 
    let typeCode: String? 
    let typeKey: Int
    let typeName: String? 
    init(categoryCode: String?, categoryKey: Int, categoryName: String?, id: Int, key: Int, name: String, optionTypeCode: String?, optionTypeKey: Int, optionTypeName: String, providedByPartyId: Int, typeCategoryCode: String?, typeCategoryKey: Int, typeCategoryName: String?, typeCode: String?, typeKey: Int, typeName: String?) {
        self.categoryCode = categoryCode
        self.categoryKey = categoryKey
        self.categoryName = categoryName
        self.id = id
        self.key = key
        self.name = name
        self.optionTypeCode = optionTypeCode
        self.optionTypeKey = optionTypeKey
        self.optionTypeName = optionTypeName
        self.providedByPartyId = providedByPartyId
        self.typeCategoryCode = typeCategoryCode
        self.typeCategoryKey = typeCategoryKey
        self.typeCategoryName = typeCategoryName
        self.typeCode = typeCode
        self.typeKey = typeKey
        self.typeName = typeName
    }
    public init?(json: [String: Any]) {
        let categoryCode = json["categoryCode"] as? String
        guard let categoryKey = json["categoryKey"] as? Int else { debugPrint("Expected non-optional property [categoryKey] of type [Int] on object [GEARReward] but did not find");return nil; }
        let categoryName = json["categoryName"] as? String
        guard let id = json["id"] as? Int else { debugPrint("Expected non-optional property [id] of type [Int] on object [GEARReward] but did not find");return nil; }
        guard let key = json["key"] as? Int else { debugPrint("Expected non-optional property [key] of type [Int] on object [GEARReward] but did not find");return nil; }
        guard let name = json["name"] as? String else { debugPrint("Expected non-optional property [name] of type [String] on object [GEARReward] but did not find");return nil; }
        let optionTypeCode = json["optionTypeCode"] as? String
        guard let optionTypeKey = json["optionTypeKey"] as? Int else { debugPrint("Expected non-optional property [optionTypeKey] of type [Int] on object [GEARReward] but did not find");return nil; }
        guard let optionTypeName = json["optionTypeName"] as? String else { debugPrint("Expected non-optional property [optionTypeName] of type [String] on object [GEARReward] but did not find");return nil; }
        guard let providedByPartyId = json["providedByPartyId"] as? Int else { debugPrint("Expected non-optional property [providedByPartyId] of type [Int] on object [GEARReward] but did not find");return nil; }
        let typeCategoryCode = json["typeCategoryCode"] as? String
        guard let typeCategoryKey = json["typeCategoryKey"] as? Int else { debugPrint("Expected non-optional property [typeCategoryKey] of type [Int] on object [GEARReward] but did not find");return nil; }
        let typeCategoryName = json["typeCategoryName"] as? String
        let typeCode = json["typeCode"] as? String
        guard let typeKey = json["typeKey"] as? Int else { debugPrint("Expected non-optional property [typeKey] of type [Int] on object [GEARReward] but did not find");return nil; }
        let typeName = json["typeName"] as? String
        self.init(categoryCode: categoryCode, categoryKey: categoryKey, categoryName: categoryName, id: id, key: key, name: name, optionTypeCode: optionTypeCode, optionTypeKey: optionTypeKey, optionTypeName: optionTypeName, providedByPartyId: providedByPartyId, typeCategoryCode: typeCategoryCode, typeCategoryKey: typeCategoryKey, typeCategoryName: typeCategoryName, typeCode: typeCode, typeKey: typeKey, typeName: typeName)
    }
}

public struct GEARRewardSelectionType: CreatableFromJSON {
    let code: String? // Reward Selection Type Code. eg Wheelspin, Choice
    let key: Int
    let name: String? // Reward Selection Type Name eg Wheelspin, Choice
    let rewardSelections: [GEARRewardSelections]? /* This class defines :

<u>a) For Probabilistic Reward Option:</u>
which reward values comprise the selection for a probabilistic reward and the Period/Duration when Reward Substitution is allowed. 

eg. for a WheelSpin,contains the selection of Rewards that will be part of the spin options.

<u>b) For Choice Reward Option :</u>
b) which reward values can be substituted (swapped) with other reward values and the Period/Duration when Reward Substitution is allowed. 

eg. if the member wins a Starbucks $5 voucher and decides to swap it, then this class will contain all the eligible rewards that the Starbucks $5 voucher can be swapped with.
 */
    init(code: String?, key: Int, name: String?, rewardSelections: [GEARRewardSelections]?) {
        self.code = code
        self.key = key
        self.name = name
        self.rewardSelections = rewardSelections
    }
    public init?(json: [String: Any]) {
        let code = json["code"] as? String
        guard let key = json["key"] as? Int else { debugPrint("Expected non-optional property [key] of type [Int] on object [GEARRewardSelectionType] but did not find");return nil; }
        let name = json["name"] as? String
        let rewardSelections = GEARRewardSelections.createRequiredInstances(from: json, arrayKey: "rewardSelections")
        self.init(code: code, key: key, name: name, rewardSelections: rewardSelections)
    }
}

public struct GEARRewardSelections: CreatableFromJSON {
    let reward: GEARReward
    let rewardValueAmount: String? // Monetary amount that the person could redeeme.g.$ 25
    let rewardValueItemDescription: String? // Defines a specific reward that the member could get E.g. Cup of coffee
    let rewardValueLinkId: Int
    let rewardValuePercent: Int? // Percentage value that needs to be applied
    let rewardValueQuantity: Int? // Quantity relates to the item description.E.g. 2 Cups coffee
    let rewardValueTypeCode: String? 
    let rewardValueTypeKey: Int
    let rewardValueTypeName: String
    let sortOrder: Int
    init(reward: GEARReward, rewardValueAmount: String?, rewardValueItemDescription: String?, rewardValueLinkId: Int, rewardValuePercent: Int?, rewardValueQuantity: Int?, rewardValueTypeCode: String?, rewardValueTypeKey: Int, rewardValueTypeName: String, sortOrder: Int) {
        self.reward = reward
        self.rewardValueAmount = rewardValueAmount
        self.rewardValueItemDescription = rewardValueItemDescription
        self.rewardValueLinkId = rewardValueLinkId
        self.rewardValuePercent = rewardValuePercent
        self.rewardValueQuantity = rewardValueQuantity
        self.rewardValueTypeCode = rewardValueTypeCode
        self.rewardValueTypeKey = rewardValueTypeKey
        self.rewardValueTypeName = rewardValueTypeName
        self.sortOrder = sortOrder
    }
    public init?(json: [String: Any]) {
        guard let reward = GEARReward(json: json, key: "reward") else { debugPrint("Expected non-optional property [reward] of type [GEARReward] on object [GEARRewardSelections] but did not find");return nil; }
        let rewardValueAmount = json["rewardValueAmount"] as? String
        let rewardValueItemDescription = json["rewardValueItemDescription"] as? String
        guard let rewardValueLinkId = json["rewardValueLinkId"] as? Int else { debugPrint("Expected non-optional property [rewardValueLinkId] of type [Int] on object [GEARRewardSelections] but did not find");return nil; }
        let rewardValuePercent = json["rewardValuePercent"] as? Int
        let rewardValueQuantity = json["rewardValueQuantity"] as? Int
        let rewardValueTypeCode = json["rewardValueTypeCode"] as? String
        guard let rewardValueTypeKey = json["rewardValueTypeKey"] as? Int else { debugPrint("Expected non-optional property [rewardValueTypeKey] of type [Int] on object [GEARRewardSelections] but did not find");return nil; }
        guard let rewardValueTypeName = json["rewardValueTypeName"] as? String else { debugPrint("Expected non-optional property [rewardValueTypeName] of type [String] on object [GEARRewardSelections] but did not find");return nil; }
        guard let sortOrder = json["sortOrder"] as? Int else { debugPrint("Expected non-optional property [sortOrder] of type [Int] on object [GEARRewardSelections] but did not find");return nil; }
        self.init(reward: reward, rewardValueAmount: rewardValueAmount, rewardValueItemDescription: rewardValueItemDescription, rewardValueLinkId: rewardValueLinkId, rewardValuePercent: rewardValuePercent, rewardValueQuantity: rewardValueQuantity, rewardValueTypeCode: rewardValueTypeCode, rewardValueTypeKey: rewardValueTypeKey, rewardValueTypeName: rewardValueTypeName, sortOrder: sortOrder)
    }
}

public struct GEARRewardValue: CreatableFromJSON {
    let amount: String? // Monetary amount that the person could redeeme.g.$ 25
    let itemDescription: String? // Defines a specific reward that the member could get E.g. Cup of coffee
    let linkId: Int
    let percent: Int? // Percentage value that needs to be applied
    let providerItems: [GEARProviderItems]? // Provider Item related to the Reward ValueThe functional requirement is to be able to call the voucher proxy and be able to pass in the reference of the reward. Hence, this class will store the value of the voucher provider item.e.g: VendorCardTypeId
    let quantity: Int? // Quantity relates to the item description.E.g. 2 Cups coffee
    let typeCode: String? 
    let typeKey: Int
    let typeName: String? 
    init(amount: String?, itemDescription: String?, linkId: Int, percent: Int?, providerItems: [GEARProviderItems]?, quantity: Int?, typeCode: String?, typeKey: Int, typeName: String?) {
        self.amount = amount
        self.itemDescription = itemDescription
        self.linkId = linkId
        self.percent = percent
        self.providerItems = providerItems
        self.quantity = quantity
        self.typeCode = typeCode
        self.typeKey = typeKey
        self.typeName = typeName
    }
    public init?(json: [String: Any]) {
        let amount = json["amount"] as? String
        let itemDescription = json["itemDescription"] as? String
        guard let linkId = json["linkId"] as? Int else { debugPrint("Expected non-optional property [linkId] of type [Int] on object [GEARRewardValue] but did not find");return nil; }
        let percent = json["percent"] as? Int
        let providerItems = GEARProviderItems.createRequiredInstances(from: json, arrayKey: "providerItems")
        let quantity = json["quantity"] as? Int
        let typeCode = json["typeCode"] as? String
        guard let typeKey = json["typeKey"] as? Int else { debugPrint("Expected non-optional property [typeKey] of type [Int] on object [GEARRewardValue] but did not find");return nil; }
        let typeName = json["typeName"] as? String
        self.init(amount: amount, itemDescription: itemDescription, linkId: linkId, percent: percent, providerItems: providerItems, quantity: quantity, typeCode: typeCode, typeKey: typeKey, typeName: typeName)
    }
}

public struct GEARProviderItems: CreatableFromJSON {
    let typeCode: String? /* Code (my example = actual code in English = “Female” vs. actual code in French = “Femme”)
A short name describing the; reference, association type associated to a specific key. It's purpose is to identify a unique key, in order to facilitate easy reference and understanding.
The short name is used for easy identification for debugging purposes but does not necessarily exclude the option of using it as a reference in itself. For example it could be used to populate a drop down box.
*/
    let typeKey: Int
    let typeName: String? /* Name (my example = “English Female” vs. “French Femme”)
A detailed description of the; reference, association type associated to a specific key. Its purpose is to describe the context of the unique key in more detail, in order to facilitate easy understanding of the use of the key it describes.
Value
The value that applies to the reference, association, metatdata, or source
 */
    let value: String
    init(typeCode: String?, typeKey: Int, typeName: String?, value: String) {
        self.typeCode = typeCode
        self.typeKey = typeKey
        self.typeName = typeName
        self.value = value
    }
    public init?(json: [String: Any]) {
        let typeCode = json["typeCode"] as? String
        guard let typeKey = json["typeKey"] as? Int else { debugPrint("Expected non-optional property [typeKey] of type [Int] on object [GEARProviderItems] but did not find");return nil; }
        let typeName = json["typeName"] as? String
        guard let value = json["value"] as? String else { debugPrint("Expected non-optional property [value] of type [String] on object [GEARProviderItems] but did not find");return nil; }
        self.init(typeCode: typeCode, typeKey: typeKey, typeName: typeName, value: value)
    }
}
