//
//  GetHealthAttributeFeedbackResponse.swift
//  VitalityActive
//
//  Created by Swagger Codegen on 08/31/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file

import Foundation

public struct GetHealthAttributeFeedbackResponse: CreatableFromJSON {
    let healthAttribute: [HAFHealthAttribute]? // An instance of a Health Attribute details the value of a specific attribute type for a Party. Health Attributes should only be used to information about specific attribute values which Vitality is interested in. Health Attributes are separate from Assessment answers. Attributes can be supplied direct to Health Attributes, assessments are only 1 of the mechanisms to obtain Health attribute data
    init(healthAttribute: [HAFHealthAttribute]?) {
        self.healthAttribute = healthAttribute
    }
    public init?(json: [String: Any]) {
        let healthAttribute = HAFHealthAttribute.createRequiredInstances(from: json, arrayKey: "healthAttribute")
        self.init(healthAttribute: healthAttribute)
    }
}

public struct HAFHealthAttribute: CreatableFromJSON {
    let attributeTypeCode: String?
    let attributeTypeKey: Int
    let attributeTypeName: String?
    let healthAttributeFeedbacks: [HAFHealthAttributeFeedbacks]? // An instance of a Health Attribute details the value of a specific attribute type for a Party. Health Attributes should only be used to information about specific attribute values which Vitality is interested in. Health Attributes are separate from Assessment answers. Attributes can be supplied direct to Health Attributes, assessments are only 1 of the mechanisms to obtain Health attribute data
    let healthAttributeMetadatas: [HAFHealthAttributeMetadatas]? // A Health Attribute can have metadata associated with it. This metadata may include information such as: fasting / non fasting test
    let measuredOn: String
    let sourceEventId: Int? // This is the source event of the attribute (Not EventSource Reference)
    let unitofMeasure: String? // A meaningful unique identifier related to the unit of measure for the Health Attribute e.g. mg/dL

    let value: String
    init(attributeTypeCode: String?, attributeTypeKey: Int, attributeTypeName: String?, healthAttributeFeedbacks: [HAFHealthAttributeFeedbacks]?, healthAttributeMetadatas: [HAFHealthAttributeMetadatas]?, measuredOn: String, sourceEventId: Int?, unitofMeasure: String?, value: String) {
        self.attributeTypeCode = attributeTypeCode
        self.attributeTypeKey = attributeTypeKey
        self.attributeTypeName = attributeTypeName
        self.healthAttributeFeedbacks = healthAttributeFeedbacks
        self.healthAttributeMetadatas = healthAttributeMetadatas
        self.measuredOn = measuredOn
        self.sourceEventId = sourceEventId
        self.unitofMeasure = unitofMeasure
        self.value = value
    }
    public init?(json: [String: Any]) {
        let attributeTypeCode = json["attributeTypeCode"] as? String
        guard let attributeTypeKey = json["attributeTypeKey"] as? Int else { debugPrint("Expected non-optional property [attributeTypeKey] of type [Int] on object [HAFHealthAttribute] but did not find");return nil; }
        let attributeTypeName = json["attributeTypeName"] as? String
        let healthAttributeFeedbacks = HAFHealthAttributeFeedbacks.createRequiredInstances(from: json, arrayKey: "healthAttributeFeedbacks")
        let healthAttributeMetadatas = HAFHealthAttributeMetadatas.createRequiredInstances(from: json, arrayKey: "healthAttributeMetadatas")
        guard let measuredOn = json["measuredOn"] as? String else { debugPrint("Expected non-optional property [measuredOn] of type [String] on object [HAFHealthAttribute] but did not find");return nil; }
        let sourceEventId = json["sourceEventId"] as? Int
        let unitofMeasure = json["unitofMeasure"] as? String
        guard let value = json["value"] as? String else { debugPrint("Expected non-optional property [value] of type [String] on object [HAFHealthAttribute] but did not find");return nil; }
        self.init(attributeTypeCode: attributeTypeCode, attributeTypeKey: attributeTypeKey, attributeTypeName: attributeTypeName, healthAttributeFeedbacks: healthAttributeFeedbacks, healthAttributeMetadatas: healthAttributeMetadatas, measuredOn: measuredOn, sourceEventId: sourceEventId, unitofMeasure: unitofMeasure, value: value)
    }
}

public struct HAFHealthAttributeFeedbacks: CreatableFromJSON {
    let feedbackTypeCode: String?
    let feedbackTypeKey: Int
    let feedbackTypeName: String
    let feedbackTypeTypeCode: String?
    let feedbackTypeTypeKey: Int
    let feedbackTypeTypeName: String
    init(feedbackTypeCode: String?, feedbackTypeKey: Int, feedbackTypeName: String, feedbackTypeTypeCode: String?, feedbackTypeTypeKey: Int, feedbackTypeTypeName: String) {
        self.feedbackTypeCode = feedbackTypeCode
        self.feedbackTypeKey = feedbackTypeKey
        self.feedbackTypeName = feedbackTypeName
        self.feedbackTypeTypeCode = feedbackTypeTypeCode
        self.feedbackTypeTypeKey = feedbackTypeTypeKey
        self.feedbackTypeTypeName = feedbackTypeTypeName
    }
    public init?(json: [String: Any]) {
        let feedbackTypeCode = json["feedbackTypeCode"] as? String
        guard let feedbackTypeKey = json["feedbackTypeKey"] as? Int else { debugPrint("Expected non-optional property [feedbackTypeKey] of type [Int] on object [HAFHealthAttributeFeedbacks] but did not find");return nil; }
        guard let feedbackTypeName = json["feedbackTypeName"] as? String else { debugPrint("Expected non-optional property [feedbackTypeName] of type [String] on object [HAFHealthAttributeFeedbacks] but did not find");return nil; }
        let feedbackTypeTypeCode = json["feedbackTypeTypeCode"] as? String
        guard let feedbackTypeTypeKey = json["feedbackTypeTypeKey"] as? Int else { debugPrint("Expected non-optional property [feedbackTypeTypeKey] of type [Int] on object [HAFHealthAttributeFeedbacks] but did not find");return nil; }
        guard let feedbackTypeTypeName = json["feedbackTypeTypeName"] as? String else { debugPrint("Expected non-optional property [feedbackTypeTypeName] of type [String] on object [HAFHealthAttributeFeedbacks] but did not find");return nil; }
        self.init(feedbackTypeCode: feedbackTypeCode, feedbackTypeKey: feedbackTypeKey, feedbackTypeName: feedbackTypeName, feedbackTypeTypeCode: feedbackTypeTypeCode, feedbackTypeTypeKey: feedbackTypeTypeKey, feedbackTypeTypeName: feedbackTypeTypeName)
    }
}

public struct HAFHealthAttributeMetadatas: CreatableFromJSON {
    let typeCode: String?
    let typeKey: Int
    let typeName: String?
    let value: String
    init(typeCode: String?, typeKey: Int, typeName: String?, value: String) {
        self.typeCode = typeCode
        self.typeKey = typeKey
        self.typeName = typeName
        self.value = value
    }
    public init?(json: [String: Any]) {
        let typeCode = json["typeCode"] as? String
        guard let typeKey = json["typeKey"] as? Int else { debugPrint("Expected non-optional property [typeKey] of type [Int] on object [HAFHealthAttributeMetadatas] but did not find");return nil; }
        let typeName = json["typeName"] as? String
        guard let value = json["value"] as? String else { debugPrint("Expected non-optional property [value] of type [String] on object [HAFHealthAttributeMetadatas] but did not find");return nil; }
        self.init(typeCode: typeCode, typeKey: typeKey, typeName: typeName, value: value)
    }
}
