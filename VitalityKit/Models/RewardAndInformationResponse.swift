//
//  BenefitAndRewardInformationResponse.swift
//  VitalityActive
//
//  Created by Swagger Codegen on 10/10/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file

import Foundation

public struct BenefitAndRewardInformationResponse: CreatableFromJSON {
    let getBenefitAndRewardInformationResponse: GBARIGetBenefitAndRewardInformationResponse?
    init(getBenefitAndRewardInformationResponse: GBARIGetBenefitAndRewardInformationResponse?) {
        self.getBenefitAndRewardInformationResponse = getBenefitAndRewardInformationResponse
    }
    public init?(json: [String: Any]) {
        let getBenefitAndRewardInformationResponse = GBARIGetBenefitAndRewardInformationResponse(json: json, key: "getBenefitAndRewardInformationResponse")
        self.init(getBenefitAndRewardInformationResponse: getBenefitAndRewardInformationResponse)
    }
}

public struct GBARIGetBenefitAndRewardInformationResponse: CreatableFromJSON {
    let productFeature: [GBARIProductFeature]? // Vitality Membership Product Feature
    let warnings: [GBARIWarnings]? // A list of warnings that were generated in the process Warnings are only generated when a downstream service fails, but the response for the entire service is successful
    init(productFeature: [GBARIProductFeature]?, warnings: [GBARIWarnings]?) {
        self.productFeature = productFeature
        self.warnings = warnings
    }
    public init?(json: [String: Any]) {
        let productFeature = GBARIProductFeature.createRequiredInstances(from: json, arrayKey: "productFeature")
        let warnings = GBARIWarnings.createRequiredInstances(from: json, arrayKey: "warnings")
        self.init(productFeature: productFeature, warnings: warnings)
    }
}

public struct GBARIProductFeature: CreatableFromJSON {
    let code: String? // Product Feature Code Vitality Membership Product Feature
    
    let key: Int
    let name: String?
    let product: [GBARIProduct]? // Lists the Products and the names thereof. Each product has a unique code that identifies the product.
    let productFeatureClassifications: [GBARIProductFeatureClassifications]? // The product feature classification describes how products features can be categorized or grouped in a different ways.  E.g. Know your health, Improve your health, Get rewarded
    let typeCode: String? // Product Feature Type Code Code for the product feature e.g. GymBenefit
    let typeKey: Int
    let typeName: String? // Product Feature Type Name of product feature E.g. Gym Benefit
    
    init(code: String?, key: Int, name: String?, product: [GBARIProduct]?, productFeatureClassifications: [GBARIProductFeatureClassifications]?, typeCode: String?, typeKey: Int, typeName: String?) {
        self.code = code
        self.key = key
        self.name = name
        self.product = product
        self.productFeatureClassifications = productFeatureClassifications
        self.typeCode = typeCode
        self.typeKey = typeKey
        self.typeName = typeName
    }
    public init?(json: [String: Any]) {
        let code = json["code"] as? String
        guard let key = json["key"] as? Int else { debugPrint("Expected non-optional property [key] of type [Int] on object [GBARIProductFeature] but did not find");return nil; }
        let name = json["name"] as? String
        let product = GBARIProduct.createRequiredInstances(from: json, arrayKey: "product")
        let productFeatureClassifications = GBARIProductFeatureClassifications.createRequiredInstances(from: json, arrayKey: "productFeatureClassifications")
        let typeCode = json["typeCode"] as? String
        guard let typeKey = json["typeKey"] as? Int else { debugPrint("Expected non-optional property [typeKey] of type [Int] on object [GBARIProductFeature] but did not find");return nil; }
        let typeName = json["typeName"] as? String
        self.init(code: code, key: key, name: name, product: product, productFeatureClassifications: productFeatureClassifications, typeCode: typeCode, typeKey: typeKey, typeName: typeName)
    }
}

public struct GBARIProduct: CreatableFromJSON {
    let code: String? // Code that uniquely identifies the product
    
    let eligibility: GBARIEligibility
    let key: Int
    let memberPricing: GBARIMemberPricing?
    let name: String? // Descriptive name of the product
    
    let productClassification: [GBARIProductClassification]? // The product classification describes how products can be categorised or grouped in a different ways.
    let productFeatureApplicabilities: [GBARIProductFeatureApplicability]? // Defines the product features that are applicable to the product on the effective date of the service request
    let productPricings: [GBARIProductPricings]? // A list of product pricing applicable for the product. This is a list of all possible pricing that is applicable independent of what the member has done. It can be used to present a list of possible pricing/discount levels to a member E.g. a list of pricing for a product could be: GarminBronzePrice 10% GarminSilverPrice 20% GarminGoldPrice 30%
    let supplier: GBARISupplier
    let typeCode: String? // Product type
    
    let typeKey: Int
    let typeName: String? // Product type
    
    let utilisations: [GBARIUtilisations]? // Utilisation for the product that is applicable based on the effective date from the service request A product can have multiple Utilisations (E.g. GarminUtilisation and SharedDeviceUtilisation)
    init(code: String?, eligibility: GBARIEligibility, key: Int, memberPricing: GBARIMemberPricing?, name: String?, productClassification: [GBARIProductClassification]?, productFeatureApplicabilities: [GBARIProductFeatureApplicability]?, productPricings: [GBARIProductPricings]?, supplier: GBARISupplier, typeCode: String?, typeKey: Int, typeName: String?, utilisations: [GBARIUtilisations]?) {
        self.code = code
        self.eligibility = eligibility
        self.key = key
        self.memberPricing = memberPricing
        self.name = name
        self.productClassification = productClassification
        self.productFeatureApplicabilities = productFeatureApplicabilities
        self.productPricings = productPricings
        self.supplier = supplier
        self.typeCode = typeCode
        self.typeKey = typeKey
        self.typeName = typeName
        self.utilisations = utilisations
    }
    public init?(json: [String: Any]) {
        let code = json["code"] as? String
        guard let eligibility = GBARIEligibility(json: json, key: "eligibility") else { debugPrint("Expected non-optional property [eligibility] of type [GBARIEligibility_isRequired_] on object [GBARIProduct] but did not find");return nil; }
        guard let key = json["key"] as? Int else { debugPrint("Expected non-optional property [key] of type [Int] on object [GBARIProduct] but did not find");return nil; }
        let memberPricing = GBARIMemberPricing(json: json, key: "memberPricing")
        let name = json["name"] as? String
        let productClassification = GBARIProductClassification.createRequiredInstances(from: json, arrayKey: "productClassification")
        let productFeatureApplicabilities = GBARIProductFeatureApplicability.createRequiredInstances(from: json, arrayKey: "productFeatureApplicabilities")
        let productPricings = GBARIProductPricings.createRequiredInstances(from: json, arrayKey: "productPricings")
        guard let supplier = GBARISupplier(json: json, key: "supplier") else { debugPrint("Expected non-optional property [supplier] of type [GBARISupplier_isRequired_] on object [GBARIProduct] but did not find");return nil; }
        let typeCode = json["typeCode"] as? String
        guard let typeKey = json["typeKey"] as? Int else { debugPrint("Expected non-optional property [typeKey] of type [Int] on object [GBARIProduct] but did not find");return nil; }
        let typeName = json["typeName"] as? String
        let utilisations = GBARIUtilisations.createRequiredInstances(from: json, arrayKey: "utilisations")
        self.init(code: code, eligibility: eligibility, key: key, memberPricing: memberPricing, name: name, productClassification: productClassification, productFeatureApplicabilities: productFeatureApplicabilities, productPricings: productPricings, supplier: supplier, typeCode: typeCode, typeKey: typeKey, typeName: typeName, utilisations: utilisations)
    }
}

public struct GBARIEligibility: CreatableFromJSON {
    let allowed: Bool
    let allowedReasons: [GBARIAllowedReasons]? // <font color="#505150">A reason description associated with the value of <b>Allowed</b></font>
    let eligibilityReasons: [GBARIEligibilityReason]?
    let statusCode: String? // A short name describing the; reference, association type associated to a specific key. It's purpose is to identify a unique key, in order to facilitate easy reference and understanding. The short name is used for easy identification for debugging purposes but does not necessarily exclude the option of using it as a reference in itself. For example it could be used to populate a drop down box.
    let statusKey: Int
    let statusName: String? // A detailed description of the; reference, association type associated to a specific key. Its purpose is to describe the context of the unique key in more detail, in order to facilitate easy understanding of the use of the key it describes.
    init(allowed: Bool, allowedReasons: [GBARIAllowedReasons]?, eligibilityReasons: [GBARIEligibilityReason]?, statusCode: String?, statusKey: Int, statusName: String?) {
        self.allowed = allowed
        self.allowedReasons = allowedReasons
        self.eligibilityReasons = eligibilityReasons
        self.statusCode = statusCode
        self.statusKey = statusKey
        self.statusName = statusName
    }
    public init?(json: [String: Any]) {
        guard let allowed = json["allowed"] as? Bool else { debugPrint("Expected non-optional property [allowed] of type [Bool] on object [GBARIEligibility_isRequired_] but did not find");return nil; }
        let allowedReasons = GBARIAllowedReasons.createRequiredInstances(from: json, arrayKey: "allowedReasons")
        let eligibilityReasons = GBARIEligibilityReason.createRequiredInstances(from: json, arrayKey: "eligibilityReasons")
        let statusCode = json["statusCode"] as? String
        guard let statusKey = json["statusKey"] as? Int else { debugPrint("Expected non-optional property [statusKey] of type [Int] on object [GBARIEligibility_isRequired_] but did not find");return nil; }
        let statusName = json["statusName"] as? String
        self.init(allowed: allowed, allowedReasons: allowedReasons, eligibilityReasons: eligibilityReasons, statusCode: statusCode, statusKey: statusKey, statusName: statusName)
    }
}

public struct GBARIAllowedReasons: CreatableFromJSON {
    let code: String? // <font color="#505150">A reason code associated with the value of <b>Allowed</b></font>
    
    let key: Int
    let name: String? // <font color="#505150">A reason name associated with the value of <b>Allowed</b></font>
    
    init(code: String?, key: Int, name: String?) {
        self.code = code
        self.key = key
        self.name = name
    }
    public init?(json: [String: Any]) {
        let code = json["code"] as? String
        guard let key = json["key"] as? Int else { debugPrint("Expected non-optional property [key] of type [Int] on object [GBARIAllowedReasons] but did not find");return nil; }
        let name = json["name"] as? String
        self.init(code: code, key: key, name: name)
    }
}

public struct GBARIEligibilityReason: CreatableFromJSON {
    let code: String
    let key: Int
    let name: String
    init(code: String, key: Int, name: String) {
        self.code = code
        self.key = key
        self.name = name
    }
    public init?(json: [String: Any]) {
        guard let code = json["code"] as? String else { debugPrint("Expected non-optional property [code] of type [String] on object [GBARIEligibilityReasons] but did not find");return nil; }
        guard let key = json["key"] as? Int else { debugPrint("Expected non-optional property [key] of type [Int] on object [GBARIEligibilityReasons] but did not find");return nil; }
        guard let name = json["name"] as? String else { debugPrint("Expected non-optional property [name] of type [String] on object [GBARIEligibilityReasons] but did not find");return nil; }
        self.init(code: code, key: key, name: name)
    }
}

public struct GBARIMemberPricing: CreatableFromJSON {
    let amounts: [GBARIAmounts]?
    let applicableVitalityStatuses: [GBARIApplicableVitalityStatus]?
    let discounts: [GBARIDiscounts]?
    let productPriceCategoryCode: String? // The category type  code of the pricing. The pricing category gives context to the amounts and discounts that are applicable. E.g. Category could be PolarSilverStatus pricing
    
    let productPriceCategoryKey: Int
    let productPriceCategoryName: String? // The category type name of the pricing. The pricing category gives context to the amounts and discounts that are applicable. E.g. Category could be PolarSilverStatus pricing
    
    init(amounts: [GBARIAmounts]?, applicableVitalityStatuses: [GBARIApplicableVitalityStatus]?, discounts: [GBARIDiscounts]?, productPriceCategoryCode: String?, productPriceCategoryKey: Int, productPriceCategoryName: String?) {
        self.amounts = amounts
        self.applicableVitalityStatuses = applicableVitalityStatuses
        self.discounts = discounts
        self.productPriceCategoryCode = productPriceCategoryCode
        self.productPriceCategoryKey = productPriceCategoryKey
        self.productPriceCategoryName = productPriceCategoryName
    }
    public init?(json: [String: Any]) {
        let amounts = GBARIAmounts.createRequiredInstances(from: json, arrayKey: "amounts")
        let applicableVitalityStatuses = GBARIApplicableVitalityStatus.createRequiredInstances(from: json, arrayKey: "applicableVitalityStatuses")
        let discounts = GBARIDiscounts.createRequiredInstances(from: json, arrayKey: "discounts")
        let productPriceCategoryCode = json["productPriceCategoryCode"] as? String
        guard let productPriceCategoryKey = json["productPriceCategoryKey"] as? Int else { debugPrint("Expected non-optional property [productPriceCategoryKey] of type [Int] on object [GBARIMemberPricing] but did not find");return nil; }
        let productPriceCategoryName = json["productPriceCategoryName"] as? String
        self.init(amounts: amounts, applicableVitalityStatuses: applicableVitalityStatuses, discounts: discounts, productPriceCategoryCode: productPriceCategoryCode, productPriceCategoryKey: productPriceCategoryKey, productPriceCategoryName: productPriceCategoryName)
    }
}

public struct GBARIAmounts: CreatableFromJSON {
    let typeCode: String? // A short name describing the; reference, association type associated to a specific key. It's purpose is to identify a unique key, in order to facilitate easy reference and understanding. The short name is used for easy identification for debugging purposes but does not necessarily exclude the option of using it as a reference in itself. For example it could be used to populate a drop down box.
    let typeKey: Int
    let typeName: String? // A detailed description of the; reference, association type associated to a specific key. Its purpose is to describe the context of the unique key in more detail, in order to facilitate easy understanding of the use of the key it describes.
    let value: String
    init(typeCode: String?, typeKey: Int, typeName: String?, value: String) {
        self.typeCode = typeCode
        self.typeKey = typeKey
        self.typeName = typeName
        self.value = value
    }
    public init?(json: [String: Any]) {
        let typeCode = json["typeCode"] as? String
        guard let typeKey = json["typeKey"] as? Int else { debugPrint("Expected non-optional property [typeKey] of type [Int] on object [GBARIAmounts] but did not find");return nil; }
        let typeName = json["typeName"] as? String
        guard let value = json["value"] as? String else { debugPrint("Expected non-optional property [value] of type [String] on object [GBARIAmounts] but did not find");return nil; }
        self.init(typeCode: typeCode, typeKey: typeKey, typeName: typeName, value: value)
    }
}

public struct GBARIApplicableVitalityStatus: CreatableFromJSON {
    let code: String?
    let key: Int
    let name: String?
    init(code: String?, key: Int, name: String?) {
        self.code = code
        self.key = key
        self.name = name
    }
    public init?(json: [String: Any]) {
        let code = json["code"] as? String
        guard let key = json["key"] as? Int else { debugPrint("Expected non-optional property [key] of type [Int] on object [GBARIApplicableVitalityStatus] but did not find");return nil; }
        let name = json["name"] as? String
        self.init(code: code, key: key, name: name)
    }
}

public struct GBARIDiscounts: CreatableFromJSON {
    let typeCode: String? // A short name describing the; reference, association type associated to a specific key. It's purpose is to identify a unique key, in order to facilitate easy reference and understanding. The short name is used for easy identification for debugging purposes but does not necessarily exclude the option of using it as a reference in itself. For example it could be used to populate a drop down box.
    let typeKey: Int
    let typeName: String? // A detailed description of the; reference, association type associated to a specific key. Its purpose is to describe the context of the unique key in more detail, in order to facilitate easy understanding of the use of the key it describes.
    let value: Int
    init(typeCode: String?, typeKey: Int, typeName: String?, value: Int) {
        self.typeCode = typeCode
        self.typeKey = typeKey
        self.typeName = typeName
        self.value = value
    }
    public init?(json: [String: Any]) {
        let typeCode = json["typeCode"] as? String
        guard let typeKey = json["typeKey"] as? Int else { debugPrint("Expected non-optional property [typeKey] of type [Int] on object [GBARIDiscounts] but did not find");return nil; }
        let typeName = json["typeName"] as? String
        guard let value = json["value"] as? Int else { debugPrint("Expected non-optional property [value] of type [Int] on object [GBARIDiscounts] but did not find");return nil; }
        self.init(typeCode: typeCode, typeKey: typeKey, typeName: typeName, value: value)
    }
}

public struct GBARIProductClassification: CreatableFromJSON {
    let productCategoryCode: String? // The product category code
    let productCategoryKey: Int
    let productCategoryName: String? // The product category name
    
    init(productCategoryCode: String?, productCategoryKey: Int, productCategoryName: String?) {
        self.productCategoryCode = productCategoryCode
        self.productCategoryKey = productCategoryKey
        self.productCategoryName = productCategoryName
    }
    public init?(json: [String: Any]) {
        let productCategoryCode = json["productCategoryCode"] as? String
        guard let productCategoryKey = json["productCategoryKey"] as? Int else { debugPrint("Expected non-optional property [productCategoryKey] of type [Int] on object [GBARIProductClassification] but did not find");return nil; }
        let productCategoryName = json["productCategoryName"] as? String
        self.init(productCategoryCode: productCategoryCode, productCategoryKey: productCategoryKey, productCategoryName: productCategoryName)
    }
}

public struct GBARIProductFeatureApplicability: CreatableFromJSON {
    let featureLinks: [GBARIFeatureLinks]? // Defines the link between a feature and a type in another bounded context. E.g. The gym events that are applicable for a gym product.
    let productFeatureCode: String?
    let productFeatureKey: Int
    let productFeatureName: String?
    let productFeatureTypeCode: String? // Code of Product Feature.
    let productFeatureTypeKey: Int
    let productFeatureTypeName: String? // Name of Product Feature.
    init(featureLinks: [GBARIFeatureLinks]?, productFeatureCode: String?, productFeatureKey: Int, productFeatureName: String?, productFeatureTypeCode: String?, productFeatureTypeKey: Int, productFeatureTypeName: String?) {
        self.featureLinks = featureLinks
        self.productFeatureCode = productFeatureCode
        self.productFeatureKey = productFeatureKey
        self.productFeatureName = productFeatureName
        self.productFeatureTypeCode = productFeatureTypeCode
        self.productFeatureTypeKey = productFeatureTypeKey
        self.productFeatureTypeName = productFeatureTypeName
    }
    public init?(json: [String: Any]) {
        let featureLinks = GBARIFeatureLinks.createRequiredInstances(from: json, arrayKey: "featureLinks")
        let productFeatureCode = json["productFeatureCode"] as? String
        guard let productFeatureKey = json["productFeatureKey"] as? Int else { debugPrint("Expected non-optional property [productFeatureKey] of type [Int] on object [GBARIProductFeatureApplicability] but did not find");return nil; }
        let productFeatureName = json["productFeatureName"] as? String
        let productFeatureTypeCode = json["productFeatureTypeCode"] as? String
        guard let productFeatureTypeKey = json["productFeatureTypeKey"] as? Int else { debugPrint("Expected non-optional property [productFeatureTypeKey] of type [Int] on object [GBARIProductFeatureApplicability] but did not find");return nil; }
        let productFeatureTypeName = json["productFeatureTypeName"] as? String
        self.init(featureLinks: featureLinks, productFeatureCode: productFeatureCode, productFeatureKey: productFeatureKey, productFeatureName: productFeatureName, productFeatureTypeCode: productFeatureTypeCode, productFeatureTypeKey: productFeatureTypeKey, productFeatureTypeName: productFeatureTypeName)
    }
}

public struct GBARIFeatureLinks: CreatableFromJSON {
    let linkedKey: Int
    let typeCode: String? // The code of the link type Indicates the type of the link to which the feature links e.g. Event type, Points Category Type, Health Attribute Type, Product
    let typeKey: Int
    let typeName: String? // The name of the link type Indicates the type of the link to which the feature links e.g. Event type, Points Category Type, Health Attribute Type, Product
    
    init(linkedKey: Int, typeCode: String?, typeKey: Int, typeName: String?) {
        self.linkedKey = linkedKey
        self.typeCode = typeCode
        self.typeKey = typeKey
        self.typeName = typeName
    }
    public init?(json: [String: Any]) {
        guard let linkedKey = json["linkedKey"] as? Int else { debugPrint("Expected non-optional property [linkedKey] of type [Int] on object [GBARIFeatureLinks] but did not find");return nil; }
        let typeCode = json["typeCode"] as? String
        guard let typeKey = json["typeKey"] as? Int else { debugPrint("Expected non-optional property [typeKey] of type [Int] on object [GBARIFeatureLinks] but did not find");return nil; }
        let typeName = json["typeName"] as? String
        self.init(linkedKey: linkedKey, typeCode: typeCode, typeKey: typeKey, typeName: typeName)
    }
}

public struct GBARIProductPricings: CreatableFromJSON {
    let amounts: [GBARIAmounts]?
    let applicableVitalityStatuses: [GBARIApplicableVitalityStatus]?
    let discounts: [GBARIDiscounts]?
    let productPriceCategoryCode: String? // The category type code of the pricing. The pricing category gives context to the amounts and discounts that are applicable. E.g. Category could be PolarSilverStatus pricing
    
    
    let productPriceCategoryKey: Int
    let productPriceCategoryName: String? // The category type name of the pricing. The pricing category gives context to the amounts and discounts that are applicable. E.g. Category could be PolarSilverStatus pricing
    
    
    
    init(amounts: [GBARIAmounts]?, applicableVitalityStatuses: [GBARIApplicableVitalityStatus]?, discounts: [GBARIDiscounts]?, productPriceCategoryCode: String?, productPriceCategoryKey: Int, productPriceCategoryName: String?) {
        self.amounts = amounts
        self.applicableVitalityStatuses = applicableVitalityStatuses
        self.discounts = discounts
        self.productPriceCategoryCode = productPriceCategoryCode
        self.productPriceCategoryKey = productPriceCategoryKey
        self.productPriceCategoryName = productPriceCategoryName
    }
    public init?(json: [String: Any]) {
        let amounts = GBARIAmounts.createRequiredInstances(from: json, arrayKey: "amounts")
        let applicableVitalityStatuses = GBARIApplicableVitalityStatus.createRequiredInstances(from: json, arrayKey: "applicableVitalityStatuses")
        let discounts = GBARIDiscounts.createRequiredInstances(from: json, arrayKey: "discounts")
        let productPriceCategoryCode = json["productPriceCategoryCode"] as? String
        guard let productPriceCategoryKey = json["productPriceCategoryKey"] as? Int else { debugPrint("Expected non-optional property [productPriceCategoryKey] of type [Int] on object [GBARIProductPricings] but did not find");return nil; }
        let productPriceCategoryName = json["productPriceCategoryName"] as? String
        self.init(amounts: amounts, applicableVitalityStatuses: applicableVitalityStatuses, discounts: discounts, productPriceCategoryCode: productPriceCategoryCode, productPriceCategoryKey: productPriceCategoryKey, productPriceCategoryName: productPriceCategoryName)
    }
}

public struct GBARISupplier: CreatableFromJSON {
    let name: String
    let partyId: Int
    let references: [GBARIReferences]? // References related to the product supplier
    let shortName: String
    init(name: String, partyId: Int, references: [GBARIReferences]?, shortName: String) {
        self.name = name
        self.partyId = partyId
        self.references = references
        self.shortName = shortName
    }
    public init?(json: [String: Any]) {
        guard let name = json["name"] as? String else { debugPrint("Expected non-optional property [name] of type [String] on object [GBARISupplier_isRequired_] but did not find");return nil; }
        guard let partyId = json["partyId"] as? Int else { debugPrint("Expected non-optional property [partyId] of type [Int] on object [GBARISupplier_isRequired_] but did not find");return nil; }
        let references = GBARIReferences.createRequiredInstances(from: json, arrayKey: "references")
        guard let shortName = json["shortName"] as? String else { debugPrint("Expected non-optional property [shortName] of type [String] on object [GBARISupplier_isRequired_] but did not find");return nil; }
        self.init(name: name, partyId: partyId, references: references, shortName: shortName)
    }
}

public struct GBARIReferences: CreatableFromJSON {
    let typeCode: String? // The code for type of reference
    
    let typeKey: Int
    let typeName: String? // The name for type of reference
    
    let value: String
    init(typeCode: String?, typeKey: Int, typeName: String?, value: String) {
        self.typeCode = typeCode
        self.typeKey = typeKey
        self.typeName = typeName
        self.value = value
    }
    public init?(json: [String: Any]) {
        let typeCode = json["typeCode"] as? String
        guard let typeKey = json["typeKey"] as? Int else { debugPrint("Expected non-optional property [typeKey] of type [Int] on object [GBARIReferences] but did not find");return nil; }
        let typeName = json["typeName"] as? String
        guard let value = json["value"] as? String else { debugPrint("Expected non-optional property [value] of type [String] on object [GBARIReferences] but did not find");return nil; }
        self.init(typeCode: typeCode, typeKey: typeKey, typeName: typeName, value: value)
    }
}

public struct GBARIUtilisations: CreatableFromJSON {
    let applicabilityTypeCode: String? // Indicates the applicability of the Utilisation code I.e VitalityMembership, or  Member
    
    
    let applicabilityTypeKey: Int
    let applicabilityTypeName: String? // Indicates the applicability of the Utilisation I.e Vitality Membership, or Member
    
    
    let effectiveFrom: String
    let effectiveTo: String
    let limitAmount: String? // The monetary utilisation limits  The utilisation tracker either tracks monetary amounts or counts of items
    
    let limitCount: Int? // The utilisation limits for item counts The utilisation tracker either tracks monetary amounts or counts of items
    
    let limitValueTypeCode: String? // Indicates the type of limit that is applicable to the utilisation code. E.g. Count or Amount
    
    
    let limitValueTypeKey: Int
    let limitValueTypeName: String? // Indicates the type of limit that is applicable to the utilisation name. E.g. Count or Amount
    
    let remainingAmount: String? // The remaining amount comes from the "Utilisation Tracker" class The remaining monetary utilisation limits after the utilisation has been reduced from the limits The utilisation tracker either tracks monetary amounts or counts of items
    
    let remainingCount: Int? // The remaining count comes from the "Utilisation Tracker" class The remaining utilisation limits for items after the utilisation has been reduced from the limits The utilisation tracker either tracks monetary amounts or counts of items
    
    let statusChangedOn: String? // Status Changed On comes from the Utilisation Tracker Status Changed On is the Date when the utilisation status changed
    
    let statusTypeCode: String? // Code of Status associated with the utilisation
    
    let statusTypeKey: Int
    let statusTypeName: String? // Name of Status associated with the utilisation
    
    let typeCode: String? // The code of Utilisation E.g. GarminUtilisation, DeviceUtilisation
    
    let typeKey: Int
    let typeName: String? // The name of Utilisation E.g. GarminUtilisation, DeviceUtilisation
    
    let utilisationAmount: String? // Monetary utilisation amount The utilisation tracker either tracks monetary amounts or counts of items
    
    let utitlisationCount: Int? // Count of items related to utilisation  The utilisation tracker either tracks monetary amounts or counts of items
    
    init(applicabilityTypeCode: String?, applicabilityTypeKey: Int, applicabilityTypeName: String?, effectiveFrom: String, effectiveTo: String, limitAmount: String?, limitCount: Int?, limitValueTypeCode: String?, limitValueTypeKey: Int, limitValueTypeName: String?, remainingAmount: String?, remainingCount: Int?, statusChangedOn: String?, statusTypeCode: String?, statusTypeKey: Int, statusTypeName: String?, typeCode: String?, typeKey: Int, typeName: String?, utilisationAmount: String?, utitlisationCount: Int?) {
        self.applicabilityTypeCode = applicabilityTypeCode
        self.applicabilityTypeKey = applicabilityTypeKey
        self.applicabilityTypeName = applicabilityTypeName
        self.effectiveFrom = effectiveFrom
        self.effectiveTo = effectiveTo
        self.limitAmount = limitAmount
        self.limitCount = limitCount
        self.limitValueTypeCode = limitValueTypeCode
        self.limitValueTypeKey = limitValueTypeKey
        self.limitValueTypeName = limitValueTypeName
        self.remainingAmount = remainingAmount
        self.remainingCount = remainingCount
        self.statusChangedOn = statusChangedOn
        self.statusTypeCode = statusTypeCode
        self.statusTypeKey = statusTypeKey
        self.statusTypeName = statusTypeName
        self.typeCode = typeCode
        self.typeKey = typeKey
        self.typeName = typeName
        self.utilisationAmount = utilisationAmount
        self.utitlisationCount = utitlisationCount
    }
    public init?(json: [String: Any]) {
        let applicabilityTypeCode = json["applicabilityTypeCode"] as? String
        guard let applicabilityTypeKey = json["applicabilityTypeKey"] as? Int else { debugPrint("Expected non-optional property [applicabilityTypeKey] of type [Int] on object [GBARIUtilisations] but did not find");return nil; }
        let applicabilityTypeName = json["applicabilityTypeName"] as? String
        guard let effectiveFrom = json["effectiveFrom"] as? String else { debugPrint("Expected non-optional property [effectiveFrom] of type [String] on object [GBARIUtilisations] but did not find");return nil; }
        guard let effectiveTo = json["effectiveTo"] as? String else { debugPrint("Expected non-optional property [effectiveTo] of type [String] on object [GBARIUtilisations] but did not find");return nil; }
        let limitAmount = json["limitAmount"] as? String
        let limitCount = json["limitCount"] as? Int
        let limitValueTypeCode = json["limitValueTypeCode"] as? String
        guard let limitValueTypeKey = json["limitValueTypeKey"] as? Int else { debugPrint("Expected non-optional property [limitValueTypeKey] of type [Int] on object [GBARIUtilisations] but did not find");return nil; }
        let limitValueTypeName = json["limitValueTypeName"] as? String
        let remainingAmount = json["remainingAmount"] as? String
        let remainingCount = json["remainingCount"] as? Int
        let statusChangedOn = json["statusChangedOn"] as? String
        let statusTypeCode = json["statusTypeCode"] as? String
        guard let statusTypeKey = json["statusTypeKey"] as? Int else { debugPrint("Expected non-optional property [statusTypeKey] of type [Int] on object [GBARIUtilisations] but did not find");return nil; }
        let statusTypeName = json["statusTypeName"] as? String
        let typeCode = json["typeCode"] as? String
        guard let typeKey = json["typeKey"] as? Int else { debugPrint("Expected non-optional property [typeKey] of type [Int] on object [GBARIUtilisations] but did not find");return nil; }
        let typeName = json["typeName"] as? String
        let utilisationAmount = json["utilisationAmount"] as? String
        let utitlisationCount = json["utitlisationCount"] as? Int
        self.init(applicabilityTypeCode: applicabilityTypeCode, applicabilityTypeKey: applicabilityTypeKey, applicabilityTypeName: applicabilityTypeName, effectiveFrom: effectiveFrom, effectiveTo: effectiveTo, limitAmount: limitAmount, limitCount: limitCount, limitValueTypeCode: limitValueTypeCode, limitValueTypeKey: limitValueTypeKey, limitValueTypeName: limitValueTypeName, remainingAmount: remainingAmount, remainingCount: remainingCount, statusChangedOn: statusChangedOn, statusTypeCode: statusTypeCode, statusTypeKey: statusTypeKey, statusTypeName: statusTypeName, typeCode: typeCode, typeKey: typeKey, typeName: typeName, utilisationAmount: utilisationAmount, utitlisationCount: utitlisationCount)
    }
}

public struct GBARIProductFeatureClassifications: CreatableFromJSON {
    let productFeatureCategoryCode: String? // The product feature category code
    
    let productFeatureCategoryKey: Int
    let productFeatureCategoryName: String? // The product feature category name
    
    let productFeatureCategoryTypeCode: String? // The product feature category type code
    
    let productFeatureCategoryTypeKey: Int
    let productFeatureCategoryTypeName: String? // The product feature category type name
    
    let sortOrder: Int
    init(productFeatureCategoryCode: String?, productFeatureCategoryKey: Int, productFeatureCategoryName: String?, productFeatureCategoryTypeCode: String?, productFeatureCategoryTypeKey: Int, productFeatureCategoryTypeName: String?, sortOrder: Int) {
        self.productFeatureCategoryCode = productFeatureCategoryCode
        self.productFeatureCategoryKey = productFeatureCategoryKey
        self.productFeatureCategoryName = productFeatureCategoryName
        self.productFeatureCategoryTypeCode = productFeatureCategoryTypeCode
        self.productFeatureCategoryTypeKey = productFeatureCategoryTypeKey
        self.productFeatureCategoryTypeName = productFeatureCategoryTypeName
        self.sortOrder = sortOrder
    }
    public init?(json: [String: Any]) {
        let productFeatureCategoryCode = json["productFeatureCategoryCode"] as? String
        guard let productFeatureCategoryKey = json["productFeatureCategoryKey"] as? Int else { debugPrint("Expected non-optional property [productFeatureCategoryKey] of type [Int] on object [GBARIProductFeatureClassifications] but did not find");return nil; }
        let productFeatureCategoryName = json["productFeatureCategoryName"] as? String
        let productFeatureCategoryTypeCode = json["productFeatureCategoryTypeCode"] as? String
        guard let productFeatureCategoryTypeKey = json["productFeatureCategoryTypeKey"] as? Int else { debugPrint("Expected non-optional property [productFeatureCategoryTypeKey] of type [Int] on object [GBARIProductFeatureClassifications] but did not find");return nil; }
        let productFeatureCategoryTypeName = json["productFeatureCategoryTypeName"] as? String
        guard let sortOrder = json["sortOrder"] as? Int else { debugPrint("Expected non-optional property [sortOrder] of type [Int] on object [GBARIProductFeatureClassifications] but did not find");return nil; }
        self.init(productFeatureCategoryCode: productFeatureCategoryCode, productFeatureCategoryKey: productFeatureCategoryKey, productFeatureCategoryName: productFeatureCategoryName, productFeatureCategoryTypeCode: productFeatureCategoryTypeCode, productFeatureCategoryTypeKey: productFeatureCategoryTypeKey, productFeatureCategoryTypeName: productFeatureCategoryTypeName, sortOrder: sortOrder)
    }
}

public struct GBARIWarnings: CreatableFromJSON {
    let code: String?
    let context: String
    let key: Int
    let name: String?
    init(code: String?, context: String, key: Int, name: String?) {
        self.code = code
        self.context = context
        self.key = key
        self.name = name
    }
    public init?(json: [String: Any]) {
        let code = json["code"] as? String
        guard let context = json["context"] as? String else { debugPrint("Expected non-optional property [context] of type [String] on object [GBARIWarnings] but did not find");return nil; }
        guard let key = json["key"] as? Int else { debugPrint("Expected non-optional property [key] of type [Int] on object [GBARIWarnings] but did not find");return nil; }
        let name = json["name"] as? String
        self.init(code: code, context: context, key: key, name: name)
    }
}
