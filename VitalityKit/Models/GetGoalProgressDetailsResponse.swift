//
//  GetGoalProgressDetailsResponse.swift
//  VitalityActive
//
//  Created by Von Kervin R. Tuico on 05/05/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation

public struct GetGoalProgressDetailsResponse: CreatableFromJSON {
    
    let getGoalProgressAndDetailsResponse: GDCGetGoalProgressAndDetailsResponse?
    
    init(getGoalProgressAndDetailsResponse: GDCGetGoalProgressAndDetailsResponse?) {
        self.getGoalProgressAndDetailsResponse = getGoalProgressAndDetailsResponse
    }
    
    public init?(json: [String: Any]) {
        let getGoalProgressAndDetailsResponse = GDCGetGoalProgressAndDetailsResponse(json: json, key: "getGoalProgressAndDetailsResponse")
        self.init(getGoalProgressAndDetailsResponse: getGoalProgressAndDetailsResponse)
    }
}

public struct GDCGetGoalProgressAndDetailsResponse: CreatableFromJSON {
    
    let goalTrackerOuts: [GDCGoalTrackerOutsResponse]?
    
    init(goalTrackerOuts: [GDCGoalTrackerOutsResponse]?) {
        self.goalTrackerOuts = goalTrackerOuts
    }
    
    public init?(json: [String : Any]) {
        let goalTrackerOuts = GDCGoalTrackerOutsResponse.createRequiredInstances(from: json, arrayKey: "goalTrackerOuts")
        self.init(goalTrackerOuts: goalTrackerOuts)
    }
}

public struct GDCGoalTrackerOutsResponse: CreatableFromJSON {
    
    let agreementId: Int?
    let completedObjectives: Int?
    let effectiveFrom: String?
    let effectiveTo: String?
    let goalCode: String?
    let goalKey: Int
    let goalName: String?
    let goalResultantEvents: [GDCGoalResultantEventsResponse]?
    let goalTrackerStatusCode: String?
    let goalTrackerStatusKey: Int
    let goalTrackerStatusName: String?
    let id: Int?
    let monitorUntil: String?
    let objectiveTrackers: [GDCObjectiveTrackersResponse]?
    let partyId: Int?
    let percentageCompleted: Int?
    let statusChangedOn: String?
    let totalObjectives: Int?
    
    init(agreementId: Int?, completedObjectives: Int?, effectiveFrom: String?, effectiveTo: String?,
         goalCode: String?, goalKey: Int, goalName: String?, goalResultantEvents: [GDCGoalResultantEventsResponse]?,
         goalTrackerStatusCode: String?, goalTrackerStatusKey: Int, goalTrackerStatusName: String?,
         id: Int?, monitorUntil: String?, objectiveTrackers: [GDCObjectiveTrackersResponse]?, partyId: Int?,
         percentageCompleted: Int?, statusChangedOn: String?, totalObjectives: Int?) {
        self.agreementId = agreementId
        self.completedObjectives = completedObjectives
        self.effectiveFrom = effectiveFrom
        self.effectiveTo = effectiveTo
        self.goalCode = goalCode
        self.goalKey = goalKey
        self.goalName = goalName
        self.goalResultantEvents = goalResultantEvents
        self.goalTrackerStatusCode = goalTrackerStatusCode
        self.goalTrackerStatusKey = goalTrackerStatusKey
        self.goalTrackerStatusName = goalTrackerStatusName
        self.id = id
        self.monitorUntil = monitorUntil
        self.objectiveTrackers = objectiveTrackers
        self.partyId = partyId
        self.percentageCompleted = percentageCompleted
        self.statusChangedOn = statusChangedOn
        self.totalObjectives = totalObjectives
    }
    
    public init?(json: [String : Any]) {
        let agreementId = json["agreementId"] as? Int
        let completedObjectives = json["completedObjectives"] as? Int
        let effectiveFrom = json["effectiveFrom"] as? String
        let effectiveTo = json["effectiveTo"] as? String
        let goalCode = json["goalCode"] as? String
        guard let goalKey = json["goalKey"] as? Int else { return nil }
        let goalName = json["goalName"] as? String
        let goalResultantEvents = GDCGoalResultantEventsResponse.createRequiredInstances(from: json, arrayKey: "goalResultantEvents")
        let goalTrackerStatusCode = json["goalTrackerStatusCode"] as? String
        guard let goalTrackerStatusKey = json["goalTrackerStatusKey"] as? Int else { return nil }
        let goalTrackerStatusName = json["goalTrackerStatusName"] as? String
        let id = json["id"] as? Int
        let monitorUntil = json["monitorUntil"] as? String
        let objectiveTrackers = GDCObjectiveTrackersResponse.createRequiredInstances(from: json, arrayKey: "objectiveTrackers")
        let partyId = json["partyId"] as? Int
        let percentageCompleted = json["percentageCompleted"] as? Int
        let statusChangedOn = json["statusChangedOn"] as? String
        let totalObjectives = json["totalObjectives"] as? Int
        self.init(agreementId: agreementId, completedObjectives: completedObjectives, effectiveFrom: effectiveFrom, effectiveTo: effectiveTo, goalCode: goalCode, goalKey: goalKey, goalName: goalName, goalResultantEvents: goalResultantEvents, goalTrackerStatusCode: goalTrackerStatusCode, goalTrackerStatusKey: goalTrackerStatusKey, goalTrackerStatusName: goalTrackerStatusName, id: id, monitorUntil: monitorUntil, objectiveTrackers: objectiveTrackers, partyId: partyId, percentageCompleted: percentageCompleted, statusChangedOn: statusChangedOn, totalObjectives: totalObjectives)
    }
}

public struct GDCGoalResultantEventsResponse: CreatableFromJSON {
    
    let id: Int?
    
    init(id: Int?) {
        self.id = id
    }
    
    public init?(json: [String : Any]) {
        let id = json["id"] as? Int
        self.init(id: id)
    }
    
}

public struct GDCObjectiveTrackersResponse: CreatableFromJSON {
    
    let effectiveFrom: String?
    let effectiveTo: String?
    let eventCountAchieved: Int?
    let eventCountTarget: Int?
    let eventOutcomeAchieved: Bool?
    let eventOutcomeTarget: String?
    let events: [GDCEventsResponse]?
    let monitorUntil: String?
    let objectiveCode: String?
    let objectiveKey: Int
    let objectiveName: String?
    let objectivePointsEntries: [GDCObjectivePointsEntriesResponse]?
    let percentageCompleted: Int?
    let pointsAchieved: Int?
    let pointsTarget: Int?
    let statusChangedOn: String?
    let statusCode: String?
    let statusKey: Int
    let statusName: String?
    
    init(effectiveFrom: String?, effectiveTo: String?, eventCountAchieved: Int?, eventCountTarget: Int?,
         eventOutcomeAchieved: Bool?, eventOutcomeTarget: String?, events: [GDCEventsResponse]?, monitorUntil: String?,
         objectiveCode: String?, objectiveKey: Int, objectiveName: String?,
         objectivePointsEntries: [GDCObjectivePointsEntriesResponse]?, percentageCompleted: Int?, pointsAchieved: Int?,
         pointsTarget: Int?, statusChangedOn: String?, statusCode: String?, statusKey: Int, statusName: String?) {
        self.effectiveFrom = effectiveFrom
        self.effectiveTo = effectiveTo
        self.eventCountAchieved = eventCountAchieved
        self.eventCountTarget = eventCountTarget
        self.eventOutcomeAchieved = eventOutcomeAchieved
        self.eventOutcomeTarget = eventOutcomeTarget
        self.events = events
        self.monitorUntil = monitorUntil
        self.objectiveCode = objectiveCode
        self.objectiveKey = objectiveKey
        self.objectiveName = objectiveName
        self.objectivePointsEntries = objectivePointsEntries
        self.percentageCompleted = percentageCompleted
        self.pointsAchieved = pointsAchieved
        self.pointsTarget = pointsTarget
        self.statusChangedOn = statusChangedOn
        self.statusCode = statusCode
        self.statusKey = statusKey
        self.statusName = statusName
    }
    
    public init?(json: [String : Any]) {
        let effectiveFrom = json["effectiveFrom"] as? String
        let effectiveTo = json["effectiveTo"] as? String
        let eventCountAchieved = json["eventCountAchieved"] as? Int
        let eventCountTarget = json["eventCountTarget"] as? Int
        let eventOutcomeAchieved = json["eventOutcomeAchieved"] as? Bool
        let eventOutcomeTarget = json["eventOutcomeTarget"] as? String
        let events = GDCEventsResponse.createRequiredInstances(from: json, arrayKey: "events")
        let monitorUntil = json["monitorUntil"] as? String
        let objectiveCode = json["objectiveCode"] as? String
        guard let objectiveKey = json["objectiveKey"] as? Int else { return nil }
        let objectiveName = json["objectiveName"] as? String
        let objectivePointsEntries = GDCObjectivePointsEntriesResponse.createRequiredInstances(from: json, arrayKey: "objectivePointsEntries")
        let percentageCompleted = json["percentageCompleted"] as? Int
        let pointsAchieved = json["pointsAchieved"] as? Int
        let pointsTarget = json["pointsTarget"] as? Int
        let statusChangedOn = json["statusChangedOn"] as? String
        let statusCode = json["statusCode"] as? String
        guard let statusKey = json["statusKey"] as? Int else { return nil }
        let statusName = json["statusName"] as? String
        self.init(effectiveFrom: effectiveFrom, effectiveTo: effectiveTo, eventCountAchieved: eventCountAchieved, eventCountTarget: eventCountTarget, eventOutcomeAchieved: eventOutcomeAchieved, eventOutcomeTarget: eventOutcomeTarget, events: events, monitorUntil: monitorUntil, objectiveCode: objectiveCode, objectiveKey: objectiveKey, objectiveName: objectiveName, objectivePointsEntries: objectivePointsEntries, percentageCompleted: percentageCompleted, pointsAchieved: pointsAchieved, pointsTarget: pointsTarget, statusChangedOn: statusChangedOn, statusCode: statusCode, statusKey: statusKey, statusName: statusName)
    }
}

public struct GDCEventsResponse: CreatableFromJSON {
    
    let dateLogged: String?
    let eventDateTime: String?
    let eventMetaDatas: [GDCEventMetaDatasResponse]?
    let eventSourceCode: String?
    let eventSourceKey: Int
    let eventSourceName: String?
    let id: Int?
    let partyId: Int?
    let reportedBy: Int?
    let typeCode: String?
    let typeKey: Int
    let typeName: String?
    
    init(dateLogged: String?, eventDateTime: String?, eventMetaDatas: [GDCEventMetaDatasResponse]?,
         eventSourceCode: String?, eventSourceKey: Int, eventSourceName: String?, id: Int?,
         partyId: Int?, reportedBy: Int?, typeCode: String?, typeKey: Int, typeName: String?) {
        self.dateLogged = dateLogged
        self.eventDateTime = eventDateTime
        self.eventMetaDatas = eventMetaDatas
        self.eventSourceCode = eventSourceCode
        self.eventSourceKey = eventSourceKey
        self.eventSourceName = eventSourceName
        self.id = id
        self.partyId = partyId
        self.reportedBy = reportedBy
        self.typeCode = typeCode
        self.typeKey = typeKey
        self.typeName = typeName
    }
    
    public init?(json: [String : Any]) {
        let dateLogged = json["dateLogged"] as? String
        let eventDateTime = json["eventDateTime"] as? String
        let eventMetaDatas = GDCEventMetaDatasResponse.createRequiredInstances(from: json, arrayKey: "eventMetaDatas")
        let eventSourceCode = json["eventSourceCode"] as? String
        guard let eventSourceKey = json["eventSourceKey"] as? Int else { return nil }
        let eventSourceName = json["eventSourceName"] as? String
        let id = json["id"] as? Int
        let partyId = json["partyId"] as? Int
        let reportedBy = json["reportedBy"] as? Int
        let typeCode = json["typeCode"] as? String
        guard let typeKey = json["typeKey"] as? Int else { return nil }
        let typeName = json["typeName"] as? String
        self.init(dateLogged: dateLogged, eventDateTime: eventDateTime, eventMetaDatas: eventMetaDatas, eventSourceCode: eventSourceCode, eventSourceKey: eventSourceKey, eventSourceName: eventSourceName, id: id, partyId: partyId, reportedBy: reportedBy, typeCode: typeCode, typeKey: typeKey, typeName: typeName)
    }
}

public struct GDCEventMetaDatasResponse: CreatableFromJSON {
    
    let typeCode: String?
    let typeKey: Int
    let typeName: String?
    let unitOfMeasure: String?
    let value: String?
    
    init(typeCode: String?, typeKey: Int, typeName: String?, unitOfMeasure: String?, value: String?) {
        self.typeCode = typeCode
        self.typeKey = typeKey
        self.typeName = typeName
        self.unitOfMeasure = unitOfMeasure
        self.value = value
    }
    
    public init?(json: [String : Any]) {
        let typeCode = json["typeCode"] as? String
        guard let typeKey = json["typeKey"] as? Int else { return nil }
        let typeName = json["typeName"] as? String
        let unitOfMeasure = json["unitOfMeasure"] as? String
        let value = json["value"] as? String
        self.init(typeCode: typeCode, typeKey: typeKey, typeName: typeName, unitOfMeasure: unitOfMeasure, value: value)
    }
}

public struct GDCObjectivePointsEntriesResponse: CreatableFromJSON {
    
    let categoryCode: String?
    let categoryKey: Int
    let categoryName: String?
    let contents: [GDCContentsResponse]?
    let earnedValue: Int?
    let effectiveDate: String?
    let eventId: Int?
    let id: Int?
    let partyId: Int?
    let pointsContributed: Int?
    let pointsEntryMetadatas: [GDCCPointsEntryMetadatasResponse]?
    let potentialValue: Int?
    let prelimitValue: Int?
    let reason: [GDCReasonResponse]?
    let statusChangeDate: String?
    let statusTypeCode: String?
    let statusTypeKey: Int
    let statusTypeName: String?
    let systemAwareOn: String?
    let typeCode: String?
    let typeKey: Int
    let typeName: String?
    
    init(categoryCode: String?, categoryKey: Int, categoryName: String?, contents: [GDCContentsResponse]?,
         earnedValue: Int?, effectiveDate: String?, eventId: Int?, id: Int?, partyId: Int?,
         pointsContributed: Int?, pointsEntryMetadatas: [GDCCPointsEntryMetadatasResponse]?,
         potentialValue: Int?, prelimitValue: Int?, reason: [GDCReasonResponse]?, statusChangeDate: String?,
         statusTypeCode: String?, statusTypeKey: Int, statusTypeName: String?, systemAwareOn: String?,
         typeCode: String?, typeKey: Int, typeName: String?) {
        self.categoryCode = categoryCode
        self.categoryKey = categoryKey
        self.categoryName = categoryName
        self.contents = contents
        self.earnedValue = earnedValue
        self.effectiveDate = effectiveDate
        self.eventId = eventId
        self.id = id
        self.partyId = partyId
        self.pointsContributed = pointsContributed
        self.pointsEntryMetadatas = pointsEntryMetadatas
        self.potentialValue = potentialValue
        self.prelimitValue = prelimitValue
        self.reason = reason
        self.statusChangeDate = statusChangeDate
        self.statusTypeCode = statusTypeCode
        self.statusTypeKey = statusTypeKey
        self.statusTypeName = statusTypeName
        self.systemAwareOn = systemAwareOn
        self.typeCode = typeCode
        self.typeKey = typeKey
        self.typeName = typeName
    }
    
    public init?(json: [String : Any]) {
        let categoryCode = json["categoryCode"] as? String
        guard let categoryKey = json["categoryKey"] as? Int else { return nil }
        let categoryName = json["categoryName"] as? String
        let contents = GDCContentsResponse.createRequiredInstances(from: json, arrayKey: "contents")
        let earnedValue = json["earnedValue"] as? Int
        let effectiveDate = json["effectiveDate"] as? String
        let eventId = json["eventId"] as? Int
        let id = json["id"] as? Int
        let partyId = json["partyId"] as? Int
        let pointsContributed = json["pointsContributed"] as? Int
        let pointsEntryMetadatas = GDCCPointsEntryMetadatasResponse.createRequiredInstances(from: json, arrayKey: "pointsEntryMetadatas")
        let potentialValue = json["potentialValue"] as? Int
        let prelimitValue = json["prelimitValue"] as? Int
        let reason = GDCReasonResponse.createRequiredInstances(from: json, arrayKey: "reason")
        let statusChangeDate = json["statusChangeDate"] as? String
        let statusTypeCode = json["statusTypeCode"] as? String
        guard let statusTypeKey = json["statusTypeKey"] as? Int else { return nil }
        let statusTypeName = json["statusTypeName"] as? String
        let systemAwareOn = json["systemAwareOn"] as? String
        let typeCode = json["typeCode"] as? String
        guard let typeKey = json["typeKey"] as? Int else { return nil }
        let typeName = json["typeName"] as? String
        self.init(categoryCode: categoryCode, categoryKey: categoryKey, categoryName: categoryName, contents: contents, earnedValue: earnedValue, effectiveDate: effectiveDate, eventId: eventId, id: id, partyId: partyId, pointsContributed: pointsContributed, pointsEntryMetadatas: pointsEntryMetadatas, potentialValue: potentialValue, prelimitValue: prelimitValue, reason: reason, statusChangeDate: statusChangeDate, statusTypeCode: statusTypeCode, statusTypeKey: statusTypeKey, statusTypeName: statusTypeName, systemAwareOn: systemAwareOn, typeCode: typeCode, typeKey: typeKey, typeName: typeName)
    }
}

public struct GDCContentsResponse: CreatableFromJSON {
    
    let label: String?
    let value: String?
    
    init(label: String?, value: String?) {
        self.label = label
        self.value = value
    }
    
    public init?(json: [String : Any]) {
        let label = json["label"] as? String
        let value = json["value"] as? String
        self.init(label: label, value: value)
    }
}

public struct GDCCPointsEntryMetadatasResponse: CreatableFromJSON {
    
    let typeCode: String?
    let typeKey: Int
    let typeName: String?
    let unitOfMeasure: String?
    let value: String?
    
    init(typeCode: String?, typeKey: Int, typeName: String?, unitOfMeasure: String?, value: String?) {
        self.typeCode = typeCode
        self.typeKey = typeKey
        self.typeName = typeName
        self.unitOfMeasure = unitOfMeasure
        self.value = value
    }
    
    public init?(json: [String : Any]) {
        let typeCode = json["typeCode"] as? String
        guard let typeKey = json["typeKey"] as? Int else { return nil }
        let typeName = json["typeName"] as? String
        let unitOfMeasure = json["unitOfMeasure"] as? String
        let value = json["value"] as? String
        self.init(typeCode: typeCode, typeKey: typeKey, typeName: typeName, unitOfMeasure: unitOfMeasure, value: value)
    }
}

public struct GDCReasonResponse: CreatableFromJSON {
    
    let categoryCode: String?
    let categoryKey: Int
    let categoryName: String?
    let reasonCode: String?
    let reasonKey: Int
    let reasonName: String?
    
    init(categoryCode: String?, categoryKey: Int, categoryName: String?, reasonCode: String?,
         reasonKey: Int, reasonName: String?) {
        self.categoryCode = categoryCode
        self.categoryKey = categoryKey
        self.categoryName = categoryName
        self.reasonCode = reasonCode
        self.reasonKey = reasonKey
        self.reasonName = reasonName
    }
    
    public init?(json: [String : Any]) {
        let categoryCode = json["categoryCode"] as? String
        guard let categoryKey = json["categoryKey"] as? Int else { return nil }
        let categoryName = json["categoryName"] as? String
        let reasonCode = json["reasonCode"] as? String
        guard let reasonKey = json["reasonKey"] as? Int else { return nil }
        let reasonName = json["reasonName"] as? String
        self.init(categoryCode: categoryCode, categoryKey: categoryKey, categoryName: categoryName, reasonCode: reasonCode, reasonKey: reasonKey, reasonName: reasonName)
    }
}




