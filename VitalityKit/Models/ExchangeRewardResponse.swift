//
//  ExchangeRewardResponse.swift
//  VitalityActive
//
//  Created by Swagger Codegen on 11/22/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file

import Foundation

public struct ExchangeRewardResponse: CreatableFromJSON {
    let exchangeRewardResponse: ARERExchangeRewardResponse?
    init(exchangeRewardResponse: ARERExchangeRewardResponse?) {
        self.exchangeRewardResponse = exchangeRewardResponse
    }
    public init?(json: [String: Any]) {
        let exchangeRewardResponse = ARERExchangeRewardResponse(json: json, key: "exchangeRewardResponse")
        self.init(exchangeRewardResponse: exchangeRewardResponse)
    }
}

public struct ARERExchangeRewardResponse: CreatableFromJSON {
    let awardedRewardReferences: [ARERAwardedRewardReferences]? // A reference related to the entity that provided the reward.
    let exchangedRewardId: Int? 
    init(awardedRewardReferences: [ARERAwardedRewardReferences]?, exchangedRewardId: Int?) {
        self.awardedRewardReferences = awardedRewardReferences
        self.exchangedRewardId = exchangedRewardId
    }
    public init?(json: [String: Any]) {
        let awardedRewardReferences = ARERAwardedRewardReferences.createRequiredInstances(from: json, arrayKey: "awardedRewardReferences")
        let exchangedRewardId = json["exchangedRewardId"] as? Int
        self.init(awardedRewardReferences: awardedRewardReferences, exchangedRewardId: exchangedRewardId)
    }
}

public struct ARERAwardedRewardReferences: CreatableFromJSON {
    let typeCode: String? 
    let typeKey: Int
    let typeName: String? 
    let value: String
    init(typeCode: String?, typeKey: Int, typeName: String?, value: String) {
        self.typeCode = typeCode
        self.typeKey = typeKey
        self.typeName = typeName
        self.value = value
    }
    public init?(json: [String: Any]) {
        let typeCode = json["typeCode"] as? String
        guard let typeKey = json["typeKey"] as? Int else { debugPrint("Expected non-optional property [typeKey] of type [Int] on object [   ARERAwardedRewardReferences] but did not find");return nil; }
        let typeName = json["typeName"] as? String
        guard let value = json["value"] as? String else { debugPrint("Expected non-optional property [value] of type [String] on object [   ARERAwardedRewardReferences] but did not find");return nil; }
        self.init(typeCode: typeCode, typeKey: typeKey, typeName: typeName, value: value)
    }
}
