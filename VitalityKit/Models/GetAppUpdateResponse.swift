import Foundation

//{
//    "checkAppVersionResponse": {
//        "upgradeRequired": true,
//        "upgradeUrl": "https://www.apple.com/za/ios/app-store/"
//    }
//}

public struct GetAppUpdateResponse: CreatableFromJSON {
    public let checkAppVersionResponse: AppVersionResponse?
    
    init(checkAppVersionResponse: AppVersionResponse?){
        self.checkAppVersionResponse = checkAppVersionResponse
    }
    
    public init?(json: [String : Any]) {
        let checkAppVersionResponse = AppVersionResponse(json: json, key: "checkAppVersionResponse")
        self.init(checkAppVersionResponse: checkAppVersionResponse)
    }
}

public struct AppVersionResponse: CreatableFromJSON {
    public let upgradeUrl: String
    public let upgradeRequired: Bool
    
    init(upgradeUrl: String, upgradeRequired: Bool){
        self.upgradeUrl = upgradeUrl
        self.upgradeRequired = upgradeRequired
    }
    
    public init?(json: [String : Any]) {
        let upgradeUrl = json["upgradeUrl"] as? String ?? ""
        let upgradeRequired = json["upgradeRequired"] as? Bool ?? false
        self.init(upgradeUrl: upgradeUrl, upgradeRequired: upgradeRequired)
    }
}
