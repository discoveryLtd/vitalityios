//
//  GetDeviceBenefitResponse.swift
//  VitalityActive
//
//  Created by Von Kervin R. Tuico on 24/04/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation

public struct GetDeviceBenefitResponse: CreatableFromJSON {
    let benefit: DCBenefitResponse?
    let goal: DCGoalResponse?
    let purchase: DCPurchaseResponse?
    let purchaseProduct: DCPurchaseProductResponse?
    let qualifyingDeviceses: [DCQualifyingDevicesesResponse]?
    
    init(benefit: DCBenefitResponse?, goal: DCGoalResponse?, purchase: DCPurchaseResponse?,
         purchaseProduct: DCPurchaseProductResponse?, qualifyingDeviceses: [DCQualifyingDevicesesResponse]?) {
        self.benefit = benefit
        self.goal = goal
        self.purchase = purchase
        self.purchaseProduct = purchaseProduct
        self.qualifyingDeviceses = qualifyingDeviceses
    }
    
    public init?(json: [String : Any]) {
        let benefit = DCBenefitResponse(json: json, key: "benefit")
        let goal = DCGoalResponse(json: json, key: "goal")
        let purchase = DCPurchaseResponse(json: json, key: "purchase")
        let purchaseProduct = DCPurchaseProductResponse(json: json, key: "purchaseProduct")
        let qualifyingDeviceses = DCQualifyingDevicesesResponse.createRequiredInstances(from: json, arrayKey: "qualifyingDeviceses")
        self.init(benefit: benefit, goal: goal, purchase: purchase, purchaseProduct: purchaseProduct, qualifyingDeviceses: qualifyingDeviceses)
    }
}

/* Benefit */

public struct DCBenefitResponse: CreatableFromJSON {
    let benefitId: Int?
    let currentBenefitPeriod: Int?
    let productAttributes: [DCProductAttributesResponse]?
    let productCode: String?
    let productKey: Int?
    let productName: String?
    let renewalPeriodUnit: Int?
    let state: DCStateResponse?
    
    init(benefitId: Int?, currentBenefitPeriod: Int?, productAttributes: [DCProductAttributesResponse]?,
         productCode: String?, productKey: Int?, productName: String?, renewalPeriodUnit: Int?,
         state: DCStateResponse?) {
        self.benefitId = benefitId
        self.currentBenefitPeriod = currentBenefitPeriod
        self.productAttributes = productAttributes
        self.productCode = productCode
        self.productKey = productKey
        self.productName = productName
        self.renewalPeriodUnit = renewalPeriodUnit
        self.state = state
    }
    
    public init?(json: [String : Any]) {
        let benefitId = json["benefitId"] as? Int
        let currentBenefitPeriod = json["currentBenefitPeriod"] as? Int
        let productAttributes = DCProductAttributesResponse.createRequiredInstances(from: json, arrayKey: "productAttributes")
        let productCode = json["productCode"] as? String
        let productKey = json["productKey"] as? Int
        let productName = json["productName"] as? String
        let renewalPeriodUnit = json["renewalPeriodUnit"] as? Int
        let state = DCStateResponse(json: json, key: "state")
        self.init(benefitId: benefitId, currentBenefitPeriod: currentBenefitPeriod, productAttributes: productAttributes, productCode: productCode, productKey: productKey, productName: productName, renewalPeriodUnit: renewalPeriodUnit, state: state)
    }
}

public struct DCProductAttributesResponse: CreatableFromJSON {
    let typeCode: String?
    let typeKey: Int?
    let typeName: String?
    let value: String?
    
    init(typeCode: String?, typeKey: Int?, typeName: String?, value: String?) {
        self.typeCode = typeCode
        self.typeKey = typeKey
        self.typeName = typeName
        self.value = value
    }
    
    public init?(json: [String : Any]) {
        let typeCode = json["typeCode"] as? String
        let typeKey = json["typeKey"] as? Int
        let typeName = json["typeName"] as? String
        let value = json["value"] as? String
        self.init(typeCode: typeCode, typeKey: typeKey, typeName: typeName, value: value)
    }
}

public struct DCStateResponse: CreatableFromJSON {
    let effectiveFrom: String?
    let effectiveTo: String?
    let reason: String?
    let statusTypeCode: String?
    let statusTypeKey: Int?
    let statusTypeName: String?
    
    init(effectiveFrom: String?, effectiveTo: String?, reason: String?, statusTypeCode: String?,
         statusTypeKey: Int?, statusTypeName: String?) {
        self.effectiveFrom = effectiveFrom
        self.effectiveTo = effectiveTo
        self.reason = reason
        self.statusTypeCode = statusTypeCode
        self.statusTypeKey = statusTypeKey
        self.statusTypeName = statusTypeName
    }
    
    public init?(json: [String : Any]) {
        let effectiveFrom = json["effectiveFrom"] as? String
        let effectiveTo = json["effectiveTo"] as? String
        let reason = json["reason"] as? String
        let statusTypeCode = json["statusTypeCode"] as? String
        let statusTypeKey = json["statusTypeKey"] as? Int
        let statusTypeName = json["statusTypeName"] as? String
        self.init(effectiveFrom: effectiveFrom, effectiveTo: effectiveTo, reason: reason, statusTypeCode: statusTypeCode, statusTypeKey: statusTypeKey, statusTypeName: statusTypeName)
    }
}

/* Goal */

public struct DCGoalResponse: CreatableFromJSON {
    let applicablePointsEntryTypes: [DCApplicablePointsEntryTypesResponse]?
    let goalTracker: DCGoalTrackerResponse?
    let key: Int?
    let levels: [DCLevelsResponse]?
    let pointsEntry: [DCPointsEntryResponse]?
    
    init(applicablePointsEntryTypes: [DCApplicablePointsEntryTypesResponse]?, goalTracker: DCGoalTrackerResponse?, key: Int?,
         levels: [DCLevelsResponse]?, pointsEntry: [DCPointsEntryResponse]?) {
        self.applicablePointsEntryTypes = applicablePointsEntryTypes
        self.goalTracker = goalTracker
        self.key = key
        self.levels = levels
        self.pointsEntry = pointsEntry
    }
    
    public init?(json: [String : Any]) {
        let applicablePointsEntryTypes = DCApplicablePointsEntryTypesResponse.createRequiredInstances(from: json, arrayKey: "applicablePointsEntryTypes")
        let goalTracker = DCGoalTrackerResponse(json: json, key: "goalTracker")
        let key = json["key"] as? Int
        let levels = DCLevelsResponse.createRequiredInstances(from: json, arrayKey: "levels")
        let pointsEntry = DCPointsEntryResponse.createRequiredInstances(from: json, arrayKey: "pointsEntry")
        self.init(applicablePointsEntryTypes: applicablePointsEntryTypes, goalTracker: goalTracker, key: key, levels: levels, pointsEntry: pointsEntry)
    }
}

public struct DCApplicablePointsEntryTypesResponse: CreatableFromJSON {
    let eventTypeCode: String?
    let eventTypeKey: Int?
    let eventTypeName: String?
    let pointsEntryTypeCode: String?
    let pointsEntryTypeKey: Int?
    let pointsEntryTypeName: String?
    
    init(eventTypeCode: String?, eventTypeKey: Int?, eventTypeName: String?, pointsEntryTypeCode: String?,
         pointsEntryTypeKey: Int?, pointsEntryTypeName: String?) {
        self.eventTypeCode = eventTypeCode
        self.eventTypeKey = eventTypeKey
        self.eventTypeName = eventTypeName
        self.pointsEntryTypeCode = pointsEntryTypeCode
        self.pointsEntryTypeKey = pointsEntryTypeKey
        self.pointsEntryTypeName = pointsEntryTypeName
    }
    
    public init?(json: [String : Any]) {
        let eventTypeCode = json["eventTypeCode"] as? String
        let eventTypeKey = json["eventTypeKey"] as? Int
        let eventTypeName = json["eventTypeName"] as? String
        let pointsEntryTypeCode = json["pointsEntryTypeCode"] as? String
        let pointsEntryTypeKey = json["pointsEntryTypeKey"] as? Int
        let pointsEntryTypeName = json["pointsEntryTypeName"] as? String
        self.init(eventTypeCode: eventTypeCode, eventTypeKey: eventTypeKey, eventTypeName: eventTypeName, pointsEntryTypeCode: pointsEntryTypeCode, pointsEntryTypeKey: pointsEntryTypeKey, pointsEntryTypeName: pointsEntryTypeName)
    }
}

public struct DCGoalTrackerResponse: CreatableFromJSON {
    let completedObjectives: Int?
    let effectiveFrom: String?
    let effectiveTo: String?
    let monitorUntil: String?
    let percentageCompleted: Int?
    let statusChangedOn: String?
    let statusCode: String?
    let statusKey: Int?
    let statusName: String?
    let totalObjectives: Int?
    
    init(completedObjectives: Int?, effectiveFrom: String?, effectiveTo: String?, monitorUntil: String?,
         percentageCompleted: Int?, statusChangedOn: String?, statusCode: String?, statusKey: Int?,
         statusName: String?, totalObjectives: Int?) {
        self.completedObjectives = completedObjectives
        self.effectiveFrom = effectiveFrom
        self.effectiveTo = effectiveTo
        self.monitorUntil = monitorUntil
        self.percentageCompleted = percentageCompleted
        self.statusChangedOn = statusChangedOn
        self.statusCode = statusCode
        self.statusKey = statusKey
        self.statusName = statusName
        self.totalObjectives = totalObjectives
    }
    
    public init?(json: [String : Any]) {
        let completedObjectives = json["completedObjectives"] as? Int
        let effectiveFrom = json["effectiveFrom"] as? String
        let effectiveTo = json["effectiveTo"] as? String
        let monitorUntil = json["monitorUntil"] as? String
        let percentageCompleted = json["percentageCompleted"] as? Int
        let statusChangedOn = json["statusChangedOn"] as? String
        let statusCode = json["statusCode"] as? String
        let statusKey = json["statusKey"] as? Int
        let statusName = json["statusName"] as? String
        let totalObjectives = json["totalObjectives"] as? Int
        self.init(completedObjectives: completedObjectives, effectiveFrom: effectiveFrom, effectiveTo: effectiveTo, monitorUntil: monitorUntil, percentageCompleted: percentageCompleted, statusChangedOn: statusChangedOn, statusCode: statusCode, statusKey: statusKey, statusName: statusName, totalObjectives: totalObjectives)
    }
}

public struct DCLevelsResponse: CreatableFromJSON {
    let isAchieved: Bool?
    let isHighestLevelAchieved: Bool?
    let level: Int?
    let maximumPoints: Int?
    let minimumPoints: Int?
    let objectiveKey: Int?
    let pointsAchieved: Int?
    let reward: DCRewardResponse?
    
    init(isAchieved: Bool?, isHighestLevelAchieved: Bool?, level: Int?, maximumPoints: Int?,
         minimumPoints: Int?, objectiveKey: Int?, pointsAchieved: Int?, reward: DCRewardResponse?) {
        self.isAchieved = isAchieved
        self.isHighestLevelAchieved = isHighestLevelAchieved
        self.level = level
        self.maximumPoints = maximumPoints
        self.minimumPoints = minimumPoints
        self.objectiveKey = objectiveKey
        self.pointsAchieved = pointsAchieved
        self.reward = reward
    }
    
    public init?(json: [String : Any]) {
        let isAchieved = json["isAchieved"] as? Bool
        let isHighestLevelAchieved = json["isHighestLevelAchieved"] as? Bool
        let level = json["level"] as? Int
        let maximumPoints = json["maximumPoints"] as? Int
        let minimumPoints = json["minimumPoints"] as? Int
        let objectiveKey = json["objectiveKey"] as? Int
        let pointsAchieved = json["pointsAchieved"] as? Int
        let reward = DCRewardResponse(json: json, key: "reward")
        self.init(isAchieved: isAchieved, isHighestLevelAchieved: isHighestLevelAchieved, level: level, maximumPoints: maximumPoints, minimumPoints: minimumPoints, objectiveKey: objectiveKey, pointsAchieved: pointsAchieved, reward: reward)
    }
}

public struct DCRewardResponse: CreatableFromJSON {
    let accumulatedAmount: String?
    let accumulatedPercentage: Int?
    let accumulatedQuantity: Int?
    let id: Int?
    let typeCode: String?
    let typeKey: Int?
    let typeName: String?
    
    init(accumulatedAmount: String?, accumulatedPercentage: Int?, accumulatedQuantity: Int?,
         id: Int?, typeCode: String?, typeKey: Int?, typeName: String?) {
        self.accumulatedAmount = accumulatedAmount
        self.accumulatedPercentage = accumulatedPercentage
        self.accumulatedQuantity = accumulatedQuantity
        self.id = id
        self.typeCode = typeCode
        self.typeKey = typeKey
        self.typeName = typeName
    }
    
    public init?(json: [String : Any]) {
        let accumulatedAmount = json["accumulatedAmount"] as? String
        let accumulatedPercentage = json["accumulatedPercentage"] as? Int
        let accumulatedQuantity = json["accumulatedQuantity"] as? Int
        let id = json["id"] as? Int
        let typeCode = json["typeCode"] as? String
        let typeKey = json["typeKey"] as? Int
        let typeName = json["typeName"] as? String
        self.init(accumulatedAmount: accumulatedAmount, accumulatedPercentage: accumulatedPercentage, accumulatedQuantity: accumulatedQuantity, id: id, typeCode: typeCode, typeKey: typeKey, typeName: typeName)
    }
}

public struct DCPointsEntryResponse: CreatableFromJSON {
    let activity: String?
    let effectiveDate: String?
    let pointsContributed: Int?
    let pointsEntryMetadatas: [DCPointsEntryMetadatasResponse]?
    let typeCode: String?
    let typeKey: Int?
    let typeName: String?
    
    init(activity: String?, effectiveDate: String?, pointsContributed: Int?,
         pointsEntryMetadatas: [DCPointsEntryMetadatasResponse]?, typeCode: String?, typeKey: Int?,
         typeName: String?) {
        self.activity = activity
        self.effectiveDate = effectiveDate
        self.pointsContributed = pointsContributed
        self.pointsEntryMetadatas = pointsEntryMetadatas
        self.typeCode = typeCode
        self.typeKey = typeKey
        self.typeName = typeName
    }
    
    public init?(json: [String : Any]) {
        let activity = json["activity"] as? String
        let effectiveDate = json["effectiveDate"] as? String
        let pointsContributed = json["pointsContributed"] as? Int
        let pointsEntryMetadatas = DCPointsEntryMetadatasResponse.createRequiredInstances(from: json, arrayKey: "pointsEntryMetadatas")
        let typeCode = json["typeCode"] as? String
        let typeKey = json["typeKey"] as? Int
        let typeName = json["typeName"] as? String
        self.init(activity: activity, effectiveDate: effectiveDate, pointsContributed: pointsContributed, pointsEntryMetadatas: pointsEntryMetadatas, typeCode: typeCode, typeKey: typeKey, typeName: typeName)
    }
}

public struct DCPointsEntryMetadatasResponse: CreatableFromJSON {
    let typeCode: String?
    let typeKey: Int?
    let typeName: String?
    let unitOfMeasure: String?
    let value: String?
    
    init(typeCode: String?, typeKey: Int?, typeName: String?,
         unitOfMeasure: String?, value: String?) {
        self.typeCode = typeCode
        self.typeKey = typeKey
        self.typeName = typeName
        self.unitOfMeasure = unitOfMeasure
        self.value = value
    }
    
    public init?(json: [String : Any]) {
        let typeCode = json["typeCode"] as? String
        let typeKey = json["typeKey"] as? Int
        let typeName = json["typeName"] as? String
        let unitOfMeasure = json["unitOfMeasure"] as? String
        let value = json["value"] as? String
        self.init(typeCode: typeCode, typeKey: typeKey, typeName: typeName, unitOfMeasure: unitOfMeasure, value: value)
    }
}

/* Purchase */

public struct DCPurchaseResponse: CreatableFromJSON {
    let deliveryDate: String?
    let id: Int?
    let purchaseAmount: [DCPurchaseAmountResponse]?
    let purchaseDate: String?
    let purchaseItem: DCPurchaseItemResponse?
    let purchaseReference: [DCPurchaseReferenceResponse]?
    
    init(deliveryDate: String?, id: Int?, purchaseAmount: [DCPurchaseAmountResponse]?, purchaseDate: String?,
         purchaseItem: DCPurchaseItemResponse?, purchaseReference: [DCPurchaseReferenceResponse]?) {
        self.deliveryDate = deliveryDate
        self.id = id
        self.purchaseAmount = purchaseAmount
        self.purchaseDate = purchaseDate
        self.purchaseItem = purchaseItem
        self.purchaseReference = purchaseReference
    }
    
    public init?(json: [String : Any]) {
        let deliveryDate = json["deliveryDate"] as? String
        let id = json["id"] as? Int
        let purchaseAmount = DCPurchaseAmountResponse.createRequiredInstances(from: json, arrayKey: "purchaseAmount")
        let purchaseDate = json["purchaseDate"] as? String
        let purchaseItem = DCPurchaseItemResponse(json: json, key: "purchaseItem")
        let purchaseReference = DCPurchaseReferenceResponse.createRequiredInstances(from: json, arrayKey: "purchaseReference")
        self.init(deliveryDate: deliveryDate, id: id, purchaseAmount: purchaseAmount, purchaseDate: purchaseDate, purchaseItem: purchaseItem, purchaseReference: purchaseReference)
    }
}

public struct DCPurchaseAmountResponse: CreatableFromJSON {
    let typeCode: String?
    let typeKey: Int?
    let typeName: String?
    let value: String?
    
    init(typeCode: String?, typeKey: Int?, typeName: String?, value: String?) {
        self.typeCode = typeCode
        self.typeKey = typeKey
        self.typeName = typeName
        self.value = value
    }
    
    public init?(json: [String : Any]) {
        let typeCode = json["typeCode"] as? String
        let typeKey = json["typeKey"] as? Int
        let typeName = json["typeName"] as? String
        let value = json["value"] as? String
        self.init(typeCode: typeCode, typeKey: typeKey, typeName: typeName, value: value)
    }
}

public struct DCPurchaseItemResponse: CreatableFromJSON {
    let description: String?
    let productCode: String?
    let productKey: Int?
    let productName: String?
    let purchaseAmount: [DCPIPurchaseAmountResponse]?
    let purchaseReference: [DCPIPurchaseReferenceResponse]?
    
    init(description: String?, productCode: String?, productKey: Int?, productName: String?,
         purchaseAmount: [DCPIPurchaseAmountResponse]?, purchaseReference: [DCPIPurchaseReferenceResponse]?) {
        self.description = description
        self.productCode = productCode
        self.productKey = productKey
        self.productName = productName
        self.purchaseAmount = purchaseAmount
        self.purchaseReference = purchaseReference
    }
    
    public init?(json: [String : Any]) {
        let description = json["description"] as? String
        let productCode = json["productCode"] as? String
        let productKey = json["productKey"] as? Int
        let productName = json["productName"] as? String
        let purchaseAmount = DCPIPurchaseAmountResponse.createRequiredInstances(from: json, arrayKey: "purchaseAmount")
        let purchaseReference = DCPIPurchaseReferenceResponse.createRequiredInstances(from: json, arrayKey: "purchaseReference")
        self.init(description: description, productCode: productCode, productKey: productKey, productName: productName, purchaseAmount: purchaseAmount, purchaseReference: purchaseReference)
    }
}

public struct DCPIPurchaseAmountResponse: CreatableFromJSON {
    let typeCode: String?
    let typeKey: Int?
    let typeName: String?
    let value: String?
    
    init(typeCode: String?, typeKey: Int?, typeName: String?, value: String?) {
        self.typeCode = typeCode
        self.typeKey = typeKey
        self.typeName = typeName
        self.value = value
    }
    
    public init?(json: [String : Any]) {
        let typeCode = json["typeCode"] as? String
        let typeKey = json["typeKey"] as? Int
        let typeName = json["typeName"] as? String
        let value = json["value"] as? String
        self.init(typeCode: typeCode, typeKey: typeKey, typeName: typeName, value: value)
    }
}

public struct DCPIPurchaseReferenceResponse: CreatableFromJSON {
    let typeCode: String?
    let typeKey: Int?
    let typeName: String?
    let value: String?
    
    init(typeCode: String?, typeKey: Int?, typeName: String?, value: String?) {
        self.typeCode = typeCode
        self.typeKey = typeKey
        self.typeName = typeName
        self.value = value
    }
    
    public init?(json: [String : Any]) {
        let typeCode = json["typeCode"] as? String
        let typeKey = json["typeKey"] as? Int
        let typeName = json["typeName"] as? String
        let value = json["value"] as? String
        self.init(typeCode: typeCode, typeKey: typeKey, typeName: typeName, value: value)
    }
}

public struct DCPurchaseReferenceResponse: CreatableFromJSON {
    let typeCode: String?
    let typeKey: Int?
    let typeName: String?
    let value: String?
    
    init(typeCode: String?, typeKey: Int?, typeName: String?, value: String?) {
        self.typeCode = typeCode
        self.typeKey = typeKey
        self.typeName = typeName
        self.value = value
    }
    
    public init?(json: [String : Any]) {
        let typeCode = json["typeCode"] as? String
        let typeKey = json["typeKey"] as? Int
        let typeName = json["typeName"] as? String
        let value = json["value"] as? String
        self.init(typeCode: typeCode, typeKey: typeKey, typeName: typeName, value: value)
    }
}

/* Purchase Product */

public struct DCPurchaseProductResponse: CreatableFromJSON {
    let productAttributes: [DCPPProductAttributesResponse]?
    let productCode: String?
    let productKey: Int?
    let productName: String?
    
    init(productAttributes: [DCPPProductAttributesResponse]?, productCode: String?,
         productKey: Int?, productName: String?) {
        self.productAttributes = productAttributes
        self.productCode = productCode
        self.productKey = productKey
        self.productName = productName
    }
    
    public init?(json: [String : Any]) {
        let productAttributes = DCPPProductAttributesResponse.createRequiredInstances(from: json, arrayKey: "productAttributes")
        let productCode = json["productCode"] as? String
        let productKey = json["productKey"] as? Int
        let productName = json["productName"] as? String
        self.init(productAttributes: productAttributes, productCode: productCode, productKey: productKey, productName: productName)
    }
}

public struct DCPPProductAttributesResponse: CreatableFromJSON {
    let typeCode: String?
    let typeKey: Int?
    let typeName: String?
    let value: String?
    
    init(typeCode: String?, typeKey: Int?, typeName: String?, value: String?) {
        self.typeCode = typeCode
        self.typeKey = typeKey
        self.typeName = typeName
        self.value = value
    }
    
    public init?(json: [String : Any]) {
        let typeCode = json["typeCode"] as? String
        let typeKey = json["typeKey"] as? Int
        let typeName = json["typeName"] as? String
        let value = json["value"] as? String
        self.init(typeCode: typeCode, typeKey: typeKey, typeName: typeName, value: value)
    }
}

/* Qualifying Deviceses */

public struct DCQualifyingDevicesesResponse: CreatableFromJSON {
    let linked: Bool?
    let productAttributes: [DCQDProductAttributesResponse]?
    let productCode: String?
    let productKey: Int?
    let productName: String?
    
    init(linked: Bool?, productAttributes: [DCQDProductAttributesResponse]?, productCode: String?,
         productKey: Int?, productName: String?) {
        self.linked = linked
        self.productAttributes = productAttributes
        self.productCode = productCode
        self.productKey = productKey
        self.productName = productName
    }
    
    public init?(json: [String : Any]) {
        let linked = json["linked"] as? Bool
        let productAttributes = DCQDProductAttributesResponse.createRequiredInstances(from: json, arrayKey: "productAttributes")
        let productCode = json["productCode"] as? String
        let productKey = json["productKey"] as? Int
        let productName = json["productName"] as? String
        self.init(linked: linked, productAttributes: productAttributes, productCode: productCode, productKey: productKey, productName: productName)
    }
}

public struct DCQDProductAttributesResponse: CreatableFromJSON {
    let typeCode: String?
    let typeKey: Int?
    let typeName: String?
    let value: String?
    
    init(typeCode: String?, typeKey: Int?, typeName: String?, value: String?) {
        self.typeCode = typeCode
        self.typeKey = typeKey
        self.typeName = typeName
        self.value = value
    }
    
    public init?(json: [String : Any]) {
        let typeCode = json["typeCode"] as? String
        let typeKey = json["typeKey"] as? Int
        let typeName = json["typeName"] as? String
        let value = json["value"] as? String
        self.init(typeCode: typeCode, typeKey: typeKey, typeName: typeName, value: value)
    }
}
