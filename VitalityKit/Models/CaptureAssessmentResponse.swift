//
//  CaptureAssessmentResponse.swift
//  VitalityActive
//
//  Created by Swagger Codegen on 08/24/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file

import Foundation

public struct CaptureAssessmentResponse: CreatableFromJSON {
    let assessmentOuts: [CAAssessmentOuts]?
    let healthAttributeMetadatas: [CAHealthAttributeMetadatas]? // A Health Attribute can have metadata associated with it. This metadata may include information such as: fasting / non fasting test
    init(assessmentOuts: [CAAssessmentOuts]?, healthAttributeMetadatas: [CAHealthAttributeMetadatas]?) {
        self.assessmentOuts = assessmentOuts
        self.healthAttributeMetadatas = healthAttributeMetadatas
    }
    public init?(json: [String: Any]) {
        debugPrint(json)
        let assessmentOuts = CAAssessmentOuts.createRequiredInstances(from: json, arrayKey: "assessmentOuts")
        let healthAttributeMetadatas = CAHealthAttributeMetadatas.createRequiredInstances(from: json, arrayKey: "healthAttributeMetadatas")
        self.init(assessmentOuts: assessmentOuts, healthAttributeMetadatas: healthAttributeMetadatas)
    }
}

public struct CAAssessmentOuts: CreatableFromJSON {
    let eventId: Int? // A unique identifier within the system of record that describes a specific instance of an entity or attribute.
    let id: Int
    let questionnaireFeedback: CAQuestionnaireFeedback?
    init(eventId: Int?, id: Int, questionnaireFeedback: CAQuestionnaireFeedback?) {
        self.eventId = eventId
        self.id = id
        self.questionnaireFeedback = questionnaireFeedback
    }
    public init?(json: [String: Any]) {
        let eventId = json["eventId"] as? Int
        guard let id = json["id"] as? Int else { debugPrint("Expected non-optional property [id] of type [Int] on object [CAAssessmentOuts] but did not find");return nil; }
        let questionnaireFeedback = CAQuestionnaireFeedback(json: json, key: "questionnaireFeedback")
        self.init(eventId: eventId, id: id, questionnaireFeedback: questionnaireFeedback)
    }
}

public struct CAQuestionnaireFeedback: CreatableFromJSON {
    let feedbacks: [CAFeedbacks]? // Purpose Description of the feedback.
    let templateCode: String
    let templateVersion: String?
    init(feedbacks: [CAFeedbacks]?, templateCode: String, templateVersion: String?) {
        self.feedbacks = feedbacks
        self.templateCode = templateCode
        self.templateVersion = templateVersion
    }
    public init?(json: [String: Any]) {
        let feedbacks = CAFeedbacks.createRequiredInstances(from: json, arrayKey: "feedbacks")
        guard let templateCode = json["templateCode"] as? String else { debugPrint("Expected non-optional property [templateCode] of type [String] on object [CAQuestionnaireFeedback] but did not find");return nil; }
        let templateVersion = json["templateVersion"] as? String
        self.init(feedbacks: feedbacks, templateCode: templateCode, templateVersion: templateVersion)
    }
}

public struct CAFeedbacks: CreatableFromJSON {
    let feedbackTypeCode: String
    let feedbackTypeKey: Int
    let feedbackTypeName: String
    let sortOrderId: Int
    let typeCode: String
    let typeKey: Int
    let typeName: String
    init(feedbackTypeCode: String, feedbackTypeKey: Int, feedbackTypeName: String, sortOrderId: Int, typeCode: String, typeKey: Int, typeName: String) {
        self.feedbackTypeCode = feedbackTypeCode
        self.feedbackTypeKey = feedbackTypeKey
        self.feedbackTypeName = feedbackTypeName
        self.sortOrderId = sortOrderId
        self.typeCode = typeCode
        self.typeKey = typeKey
        self.typeName = typeName
    }
    public init?(json: [String: Any]) {
        guard let feedbackTypeCode = json["feedbackTypeCode"] as? String else { debugPrint("Expected non-optional property [feedbackTypeCode] of type [String] on object [CAFeedbacks] but did not find");return nil; }
        guard let feedbackTypeKey = json["feedbackTypeKey"] as? Int else { debugPrint("Expected non-optional property [feedbackTypeKey] of type [Int] on object [CAFeedbacks] but did not find");return nil; }
        guard let feedbackTypeName = json["feedbackTypeName"] as? String else { debugPrint("Expected non-optional property [feedbackTypeName] of type [String] on object [CAFeedbacks] but did not find");return nil; }
        guard let sortOrderId = json["sortOrderId"] as? Int else { debugPrint("Expected non-optional property [sortOrderId] of type [Int] on object [CAFeedbacks] but did not find");return nil; }
        guard let typeCode = json["typeCode"] as? String else { debugPrint("Expected non-optional property [typeCode] of type [String] on object [CAFeedbacks] but did not find");return nil; }
        guard let typeKey = json["typeKey"] as? Int else { debugPrint("Expected non-optional property [typeKey] of type [Int] on object [CAFeedbacks] but did not find");return nil; }
        guard let typeName = json["typeName"] as? String else { debugPrint("Expected non-optional property [typeName] of type [String] on object [CAFeedbacks] but did not find");return nil; }
        self.init(feedbackTypeCode: feedbackTypeCode, feedbackTypeKey: feedbackTypeKey, feedbackTypeName: feedbackTypeName, sortOrderId: sortOrderId, typeCode: typeCode, typeKey: typeKey, typeName: typeName)
    }
}

public struct CAHealthAttributeMetadatas: CreatableFromJSON {
    let healthAttributeFeedbacks: [CAHealthAttributeFeedbacks]? // An instance of a Health Attribute details the value of a specific attribute type for a Party. Health Attributes should only be used to information about specific attribute values which Vitality is interested in. Health Attributes are separate from Assessment answers. Attributes can be supplied direct to Health Attributes, assessments are only 1 of the mechanisms to obtain Health attribute data
    let measurementUnitId: Int?
    let typeCode: String? // A short name describing the; reference, association type associated to a specific key. It's purpose is to identify a unique key, in order to facilitate easy reference and understanding. The short name is used for easy identification for debugging purposes but does not necessarily exclude the option of using it as a reference in itself. For example it could be used to populate a drop down box.
    let typeKey: Int
    let typeName: String? // A detailed description of the; reference, association type associated to a specific key. Its purpose is to describe the context of the unique key in more detail, in order to facilitate easy understanding of the use of the key it describes.
    let value: String
    init(healthAttributeFeedbacks: [CAHealthAttributeFeedbacks]?, measurementUnitId: Int?, typeCode: String?, typeKey: Int, typeName: String?, value: String) {
        self.healthAttributeFeedbacks = healthAttributeFeedbacks
        self.measurementUnitId = measurementUnitId
        self.typeCode = typeCode
        self.typeKey = typeKey
        self.typeName = typeName
        self.value = value
    }
    public init?(json: [String: Any]) {
        let healthAttributeFeedbacks = CAHealthAttributeFeedbacks.createRequiredInstances(from: json, arrayKey: "healthAttributeFeedbacks")
        let measurementUnitId = json["measurementUnitId"] as? Int
            //else { debugPrint("Expected non-optional property [measurementUnitId] of type [Int] on object [CAHealthAttributeMetadatas] but did not find");return nil; }
        let typeCode = json["typeCode"] as? String
        guard let typeKey = json["typeKey"] as? Int else { debugPrint("Expected non-optional property [typeKey] of type [Int] on object [CAHealthAttributeMetadatas] but did not find");return nil; }
        let typeName = json["typeName"] as? String
        guard let value = json["value"] as? String else { debugPrint("Expected non-optional property [value] of type [String] on object [CAHealthAttributeMetadatas] but did not find");return nil; }
        self.init(healthAttributeFeedbacks: healthAttributeFeedbacks, measurementUnitId: measurementUnitId, typeCode: typeCode, typeKey: typeKey, typeName: typeName, value: value)
    }
}

public struct CAHealthAttributeFeedbacks: CreatableFromJSON {
    let feedbackTypeCode: String? // A short name describing the; reference, association type associated to a specific key. It's purpose is to identify a unique key, in order to facilitate easy reference and understanding. The short name is used for easy identification for debugging purposes but does not necessarily exclude the option of using it as a reference in itself. For example it could be used to populate a drop down box.
    let feedbackTypeKey: Int
    let feedbackTypeName: String
    let feedbackTypeTypeCode: String? // A short name describing the; reference, association type associated to a specific key. It's purpose is to identify a unique key, in order to facilitate easy reference and understanding. The short name is used for easy identification for debugging purposes but does not necessarily exclude the option of using it as a reference in itself. For example it could be used to populate a drop down box.
    let feedbackTypeTypeKey: Int
    let feedbackTypeTypeName: String
    init(feedbackTypeCode: String?, feedbackTypeKey: Int, feedbackTypeName: String, feedbackTypeTypeCode: String?, feedbackTypeTypeKey: Int, feedbackTypeTypeName: String) {
        self.feedbackTypeCode = feedbackTypeCode
        self.feedbackTypeKey = feedbackTypeKey
        self.feedbackTypeName = feedbackTypeName
        self.feedbackTypeTypeCode = feedbackTypeTypeCode
        self.feedbackTypeTypeKey = feedbackTypeTypeKey
        self.feedbackTypeTypeName = feedbackTypeTypeName
    }
    public init?(json: [String: Any]) {
        let feedbackTypeCode = json["feedbackTypeCode"] as? String
        guard let feedbackTypeKey = json["feedbackTypeKey"] as? Int else { debugPrint("Expected non-optional property [feedbackTypeKey] of type [Int] on object [CAHealthAttributeFeedbacks] but did not find");return nil; }
        guard let feedbackTypeName = json["feedbackTypeName"] as? String else { debugPrint("Expected non-optional property [feedbackTypeName] of type [String] on object [CAHealthAttributeFeedbacks] but did not find");return nil; }
        let feedbackTypeTypeCode = json["feedbackTypeTypeCode"] as? String
        guard let feedbackTypeTypeKey = json["feedbackTypeTypeKey"] as? Int else { debugPrint("Expected non-optional property [feedbackTypeTypeKey] of type [Int] on object [CAHealthAttributeFeedbacks] but did not find");return nil; }
        guard let feedbackTypeTypeName = json["feedbackTypeTypeName"] as? String else { debugPrint("Expected non-optional property [feedbackTypeTypeName] of type [String] on object [CAHealthAttributeFeedbacks] but did not find");return nil; }
        self.init(feedbackTypeCode: feedbackTypeCode, feedbackTypeKey: feedbackTypeKey, feedbackTypeName: feedbackTypeName, feedbackTypeTypeCode: feedbackTypeTypeCode, feedbackTypeTypeKey: feedbackTypeTypeKey, feedbackTypeTypeName: feedbackTypeTypeName)
    }
}
