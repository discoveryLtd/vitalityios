//
//  BenefitAndRewardInformation.swift
//  VitalityActive
//
//  Created by Swagger Codegen on 12/05/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file

import Foundation

public struct GMIGetHealthInformation: CreatableFromJSON {
    let sections: [GMISection]? 
    init(sections: [GMISection]?) {
        self.sections = sections
    }
    public init?(json: [String: Any]) {
        let sections = GMISection.createRequiredInstances(from: json, arrayKey: "sections")
        self.init(sections: sections)
    }
}

public struct GMISection: CreatableFromJSON {
    let attributes: [GMIAttribute]?
    let sections: [GMISection]? 
    let sortOrder: Int
    let typeCode: String?
    let typeKey: Int
    let typeName: String?
    init(attributes: [GMIAttribute]?, sections: [GMISection]?, sortOrder: Int, typeCode: String?, typeKey: Int, typeName: String?) {
        self.attributes = attributes
        self.sections = sections
        self.sortOrder = sortOrder
        self.typeCode = typeCode
        self.typeKey = typeKey
        self.typeName = typeName
    }
    public init?(json: [String: Any]) {
        let attributes = GMIAttribute.createRequiredInstances(from: json, arrayKey: "attributes")
        let sections = GMISection.createRequiredInstances(from: json, arrayKey: "sections")
        guard let sortOrder = json["sortOrder"] as? Int else { debugPrint("Expected non-optional property [sortOrder] of type [Int] on object [GMISection] but did not find");return nil; }
        let typeCode = json["typeCode"] as? String
        guard let typeKey = json["typeKey"] as? Int else { debugPrint("Expected non-optional property [typeKey] of type [Int] on object [GMISection] but did not find");return nil; }
        let typeName = json["typeName"] as? String
        self.init(attributes: attributes, sections: sections, sortOrder: sortOrder, typeCode: typeCode, typeKey: typeKey, typeName: typeName)
    }
}

public struct GMIAttribute: CreatableFromJSON {
    let attributeTypeCode: String?
    let attributeTypeKey: Int
    let attributeTypeName: String?
    let event: GMIEvent? 
    let friendlyValue: String? 
    let healthAttributeFeedbacks: [GMIHealthAttributeFeedback]?
    let healthAttributeMetadatas: [GMIHealthAttributeMetadata]?
    let measuredOn: String?
    let recommendations: [GMIRecommendation]? 
    let sortOrder: Int
    let sourceEventId: Int?
    let unitofMeasure: String?

    let value: String?
    init(attributeTypeCode: String?, attributeTypeKey: Int, attributeTypeName: String?, event: GMIEvent?, friendlyValue: String?, healthAttributeFeedbacks: [GMIHealthAttributeFeedback]?, healthAttributeMetadatas: [GMIHealthAttributeMetadata]?, measuredOn: String?, recommendations: [GMIRecommendation]?, sortOrder: Int, sourceEventId: Int?, unitofMeasure: String?, value: String?) {
        self.attributeTypeCode = attributeTypeCode
        self.attributeTypeKey = attributeTypeKey
        self.attributeTypeName = attributeTypeName
        self.event = event
        self.friendlyValue = friendlyValue
        self.healthAttributeFeedbacks = healthAttributeFeedbacks
        self.healthAttributeMetadatas = healthAttributeMetadatas
        self.measuredOn = measuredOn
        self.recommendations = recommendations
        self.sortOrder = sortOrder
        self.sourceEventId = sourceEventId
        self.unitofMeasure = unitofMeasure
        self.value = value
    }
    public init?(json: [String: Any]) {
        let attributeTypeCode = json["attributeTypeCode"] as? String
        guard let attributeTypeKey = json["attributeTypeKey"] as? Int else { debugPrint("Expected non-optional property [attributeTypeKey] of type [Int] on object [GMIAttributeGetHealthInformation] but did not find");return nil; }
        let attributeTypeName = json["attributeTypeName"] as? String
        let event = GMIEvent(json: json, key: "event")
        let friendlyValue = json["friendlyValue"] as? String
        let healthAttributeFeedbacks = GMIHealthAttributeFeedback.createRequiredInstances(from: json, arrayKey: "healthAttributeFeedbacks")
        let healthAttributeMetadatas = GMIHealthAttributeMetadata.createRequiredInstances(from: json, arrayKey: "healthAttributeMetadatas")
        let measuredOn = json["measuredOn"] as? String
        let recommendations = GMIRecommendation.createRequiredInstances(from: json, arrayKey: "recommendations")
        guard let sortOrder = json["sortOrder"] as? Int else { debugPrint("Expected non-optional property [sortOrder] of type [Int] on object [GMIAttribute] but did not find");return nil; }
        let sourceEventId = json["sourceEventId"] as? Int
        let unitofMeasure = json["unitofMeasure"] as? String
        let value = json["value"] as? String
        self.init(attributeTypeCode: attributeTypeCode, attributeTypeKey: attributeTypeKey, attributeTypeName: attributeTypeName, event: event, friendlyValue: friendlyValue, healthAttributeFeedbacks: healthAttributeFeedbacks, healthAttributeMetadatas: healthAttributeMetadatas, measuredOn: measuredOn, recommendations: recommendations, sortOrder: sortOrder, sourceEventId: sourceEventId, unitofMeasure: unitofMeasure, value: value)
    }
}

public struct GMIEvent: CreatableFromJSON {
    let applicableTo: Int
    let dateLogged: String
    let effectiveDateTime: String?
    let eventId: Int
    let eventSourceTypeCode: String?
    let eventSourceTypeKey: Int
    let eventSourceTypeName: String?
    let reportedBy: Int?
    let typeCode: String
    let typeKey: Int
    let typeName: String
    init(applicableTo: Int, dateLogged: String, effectiveDateTime: String?, eventId: Int, eventSourceTypeCode: String?, eventSourceTypeKey: Int, eventSourceTypeName: String?, reportedBy: Int?, typeCode: String, typeKey: Int, typeName: String) {
        self.applicableTo = applicableTo
        self.dateLogged = dateLogged
        self.effectiveDateTime = effectiveDateTime
        self.eventId = eventId
        self.eventSourceTypeCode = eventSourceTypeCode
        self.eventSourceTypeKey = eventSourceTypeKey
        self.eventSourceTypeName = eventSourceTypeName
        self.reportedBy = reportedBy
        self.typeCode = typeCode
        self.typeKey = typeKey
        self.typeName = typeName
    }
    public init?(json: [String: Any]) {
        guard let applicableTo = json["applicableTo"] as? Int else { debugPrint("Expected non-optional property [applicableTo] of type [Int] on object [GMIEvent] but did not find");return nil; }
        guard let dateLogged = json["dateLogged"] as? String else { debugPrint("Expected non-optional property [dateLogged] of type [String] on object [GMIEvent] but did not find");return nil; }
        let effectiveDateTime = json["effectiveDateTime"] as? String
        guard let eventId = json["eventId"] as? Int else { debugPrint("Expected non-optional property [eventId] of type [Int] on object [GMIEvent] but did not find");return nil; }
        let eventSourceTypeCode = json["eventSourceTypeCode"] as? String
        guard let eventSourceTypeKey = json["eventSourceTypeKey"] as? Int else { debugPrint("Expected non-optional property [eventSourceTypeKey] of type [Int] on object [GMIEvent] but did not find");return nil; }
        let eventSourceTypeName = json["eventSourceTypeName"] as? String
        let reportedBy = json["reportedBy"] as? Int
        guard let typeCode = json["typeCode"] as? String else { debugPrint("Expected non-optional property [typeCode] of type [String] on object [GMIEvent] but did not find");return nil; }
        guard let typeKey = json["typeKey"] as? Int else { debugPrint("Expected non-optional property [typeKey] of type [Int] on object [GMIEvent] but did not find");return nil; }
        guard let typeName = json["typeName"] as? String else { debugPrint("Expected non-optional property [typeName] of type [String] on object [GMIEvent] but did not find");return nil; }
        self.init(applicableTo: applicableTo, dateLogged: dateLogged, effectiveDateTime: effectiveDateTime, eventId: eventId, eventSourceTypeCode: eventSourceTypeCode, eventSourceTypeKey: eventSourceTypeKey, eventSourceTypeName: eventSourceTypeName, reportedBy: reportedBy, typeCode: typeCode, typeKey: typeKey, typeName: typeName)
    }
}

public struct GMIHealthAttributeFeedback: CreatableFromJSON {
    let feedbackTips: [GMIFeedbackTip]? 
    let feedbackTypeCode: String?
    let feedbackTypeKey: Int
    let feedbackTypeName: String
    let feedbackTypeTypeCode: String?
    let feedbackTypeTypeKey: Int
    let feedbackTypeTypeName: String
    let whyIsThisImportant: String? 
    init(feedbackTips: [GMIFeedbackTip]?, feedbackTypeCode: String?, feedbackTypeKey: Int, feedbackTypeName: String, feedbackTypeTypeCode: String?, feedbackTypeTypeKey: Int, feedbackTypeTypeName: String, whyIsThisImportant: String?) {
        self.feedbackTips = feedbackTips
        self.feedbackTypeCode = feedbackTypeCode
        self.feedbackTypeKey = feedbackTypeKey
        self.feedbackTypeName = feedbackTypeName
        self.feedbackTypeTypeCode = feedbackTypeTypeCode
        self.feedbackTypeTypeKey = feedbackTypeTypeKey
        self.feedbackTypeTypeName = feedbackTypeTypeName
        self.whyIsThisImportant = whyIsThisImportant
    }
    public init?(json: [String: Any]) {
        let feedbackTips = GMIFeedbackTip.createRequiredInstances(from: json, arrayKey: "feedbackTips")
        let feedbackTypeCode = json["feedbackTypeCode"] as? String
        guard let feedbackTypeKey = json["feedbackTypeKey"] as? Int else { debugPrint("Expected non-optional property [feedbackTypeKey] of type [Int] on object [GMIHealthAttributeFeedbacksGetHealthInformation] but did not find");return nil; }
        let feedbackTypeName = json["feedbackTypeName"] as? String ?? ""
        let feedbackTypeTypeCode = json["feedbackTypeTypeCode"] as? String
        guard let feedbackTypeTypeKey = json["feedbackTypeTypeKey"] as? Int else { debugPrint("Expected non-optional property [feedbackTypeTypeKey] of type [Int] on object [GMIHealthAttributeFeedbacksGetHealthInformation] but did not find");return nil; }
        guard let feedbackTypeTypeName = json["feedbackTypeTypeName"] as? String else { debugPrint("Expected non-optional property [feedbackTypeTypeName] of type [String] on object [GMIHealthAttributeFeedbacksGetHealthInformation] but did not find");return nil; }
        let whyIsThisImportant = json["whyIsThisImportant"] as? String
        self.init(feedbackTips: feedbackTips, feedbackTypeCode: feedbackTypeCode, feedbackTypeKey: feedbackTypeKey, feedbackTypeName: feedbackTypeName, feedbackTypeTypeCode: feedbackTypeTypeCode, feedbackTypeTypeKey: feedbackTypeTypeKey, feedbackTypeTypeName: feedbackTypeTypeName, whyIsThisImportant: whyIsThisImportant)
    }
}

public struct GMIFeedbackTip: CreatableFromJSON {
    let note: String
    let sortOrder: Int?
    let typeCode: String?
    let typeKey: Int
    let typeName: String?
    init(note: String, sortOrder: Int?, typeCode: String?, typeKey: Int, typeName: String?) {
        self.note = note
        self.sortOrder = sortOrder
        self.typeCode = typeCode
        self.typeKey = typeKey
        self.typeName = typeName
    }
    public init?(json: [String: Any]) {
        guard let note = json["note"] as? String else { debugPrint("Expected non-optional property [note] of type [String] on object [GMIFeedbackTipsGetHealthInformation] but did not find");return nil; }
        let sortOrder = json["sortOrder"] as? Int
        let typeCode = json["typeCode"] as? String
        guard let typeKey = json["typeKey"] as? Int else { debugPrint("Expected non-optional property [typeKey] of type [Int] on object [GMIFeedbackTipsGetHealthInformation] but did not find");return nil; }
        let typeName = json["typeName"] as? String
        self.init(note: note, sortOrder: sortOrder, typeCode: typeCode, typeKey: typeKey, typeName: typeName)
    }
}

public struct GMIHealthAttributeMetadata: CreatableFromJSON {
    let friendlyValue: String?
    let healthAttributeFeedbacks: [GMIHealthAttributeFeedback]?
    let measurementUnitId: Int?
    let typeCode: String?
    let typeKey: Int
    let typeName: String?
    let value: String
    init(friendlyValue: String?, healthAttributeFeedbacks: [GMIHealthAttributeFeedback]?, measurementUnitId: Int?, typeCode: String?, typeKey: Int, typeName: String?, value: String) {
        self.friendlyValue = friendlyValue
        self.healthAttributeFeedbacks = healthAttributeFeedbacks
        self.measurementUnitId = measurementUnitId
        self.typeCode = typeCode
        self.typeKey = typeKey
        self.typeName = typeName
        self.value = value
    }
    public init?(json: [String: Any]) {
        let friendlyValue = json["friendlyValue"] as? String
        let healthAttributeFeedbacks = GMIHealthAttributeFeedback.createRequiredInstances(from: json, arrayKey: "healthAttributeFeedbacks")
        let measurementUnitId = json["measurementUnitId"] as? Int
        let typeCode = json["typeCode"] as? String
        guard let typeKey = json["typeKey"] as? Int else { debugPrint("Expected non-optional property [typeKey] of type [Int] on object [GMIHealthAttributeMetadatasGetHealthInformation] but did not find");return nil; }
        let typeName = json["typeName"] as? String
        guard let value = json["value"] as? String else { debugPrint("Expected non-optional property [value] of type [String] on object [GMIHealthAttributeMetadatasGetHealthInformation] but did not find");return nil; }
        self.init(friendlyValue: friendlyValue, healthAttributeFeedbacks: healthAttributeFeedbacks, measurementUnitId: measurementUnitId, typeCode: typeCode, typeKey: typeKey, typeName: typeName, value: value)
    }
}

public struct GMIRecommendation: CreatableFromJSON {
    let friendlyValue: String? 
    let fromValue: String?
    let toValue: String?
    let unitOfMeasureId: Int?

    let value: String?
    init(friendlyValue: String?, fromValue: String?, toValue: String?, unitOfMeasureId: Int?, value: String?) {
        self.friendlyValue = friendlyValue
        self.fromValue = fromValue
        self.toValue = toValue
        self.unitOfMeasureId = unitOfMeasureId
        self.value = value
    }
    public init?(json: [String: Any]) {
        let friendlyValue = json["friendlyValue"] as? String
        let fromValue = json["fromValue"] as? String
        let toValue = json["toValue"] as? String
        let unitOfMeasureId = json["unitOfMeasureId"] as? Int
        let value = json["value"] as? String
        self.init(friendlyValue: friendlyValue, fromValue: fromValue, toValue: toValue, unitOfMeasureId: unitOfMeasureId, value: value)
    }
}
