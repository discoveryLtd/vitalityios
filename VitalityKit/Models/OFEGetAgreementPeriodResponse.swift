//
//  OFEEventsAndPointsTracking.swift
//  VitalityActive
//
//  Created by Harry C. Lingad on 4/16/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation

public struct OFEGetAgreementPeriodResponse: CreatableFromJSON {
    let agreementPeriods: AgreementPeriod? // The points account. This covers the points earned and the period in which the points were earned
    
    init(agreementPeriods: AgreementPeriod?) {
        self.agreementPeriods = agreementPeriods
    }
    
    public init?(json: [String: Any]) {
        let agreementPeriods = AgreementPeriod(json: json, key: "getAgreementPeriodResponse")
        self.init(agreementPeriods: agreementPeriods)
    }
}

public struct AgreementPeriod: CreatableFromJSON {
    
    
    let effectivePeriods: [EffectivePeriod]?
    
    init(effectivePeriods: [EffectivePeriod]?){
        self.effectivePeriods = effectivePeriods!
    }
    
    public init?(json: [String: Any]) {
        let effectivePeriod = EffectivePeriod.createRequiredInstances(from: json, arrayKey: "effectivePeriod")
        self.init(effectivePeriods: effectivePeriod)
        
    }
    
}

//------------------------------------------------------




public struct EffectivePeriod: CreatableFromJSON {
    
    var effectiveFrom: String = ""
    var effectiveTo: String = ""
    
    init(effectiveFrom: String?, effectiveTo: String?){
        
        self.effectiveFrom = effectiveFrom!
        self.effectiveTo = effectiveTo!
    }
    
    public init?(json: [String: Any]) {
        
        let effectiveFrom = json["effectiveFrom"] as? String
        let effectiveTo = json["effectiveTo"] as? String
        
        self.init(effectiveFrom: effectiveFrom, effectiveTo: effectiveTo)
        
    }
    
}



