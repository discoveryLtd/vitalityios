//
//  ResendInsurerCodeErrorResponse.swift
//  VitalityActive
//
//  Created by Dexter Anthony Ambrad on 3/22/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

public struct ResendInsurerCodeErrorResponse: CreatableFromJSON {
    let errors: [ResendInsurerCodeError]?
    
    public init(errors: [ResendInsurerCodeError]?) {
        self.errors = errors
    }
    public init?(json: [String: Any]) {
        let errors = ResendInsurerCodeError.createRequiredInstances(from: json, arrayKey: "errors")
        self.init(errors: errors)
    }
    public func getErrorCode() -> Int {
        return self.errors!.first!.code
    }
}
public struct ResendInsurerCodeError: CreatableFromJSON{
    let code: Int
    let message: String
    let type: String
    let context: String
    let service: String
    
    public init(code:Int, message:String, type:String, context:String, service:String) {
        self.code       = code
        self.message    = message
        self.type       = type
        self.context    = context
        self.service    = service
    }
    
    public init?(json: [String: Any]) {
        self.code       = json["code"] as? Int ?? 0
        self.message    = json["message"] as? String ?? ""
        self.type       = json["type"] as? String ?? ""
        self.context    = json["context"] as? String ?? ""
        self.service    = json["service"] as? String ?? ""
    }
}
