//
//  ProductFeaturePoints.swift
//  VitalityActive
//
//  Created by Swagger Codegen on 10/31/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file

import Foundation

public struct GetProductFeaturePointsResponse: CreatableFromJSON {
    let productFeatureCategoryAndPointsInformations: [PFPProductFeatureCategoryAndPointsInformations]?
    init(productFeatureCategoryAndPointsInformations: [PFPProductFeatureCategoryAndPointsInformations]?) {
        self.productFeatureCategoryAndPointsInformations = productFeatureCategoryAndPointsInformations
    }
    public init?(json: [String: Any]) {
        let productFeatureCategoryAndPointsInformations = PFPProductFeatureCategoryAndPointsInformations.createRequiredInstances(from: json, arrayKey: "productFeatureCategoryAndPointsInformations")
        self.init(productFeatureCategoryAndPointsInformations: productFeatureCategoryAndPointsInformations)
    }
}

public struct PFPProductFeatureCategoryAndPointsInformations: CreatableFromJSON {
    let code: String?
    let key: Int
    let name: String?
    let pointsCategoryLimit: Int?
    let pointsEarned: Int
    let pointsEarningFlag: String
    let potentialPoints: Int
    let productFeatureAndPointsInformations: [PFPProductFeatureAndPointsInformations]?
    let typeCode: String?
    let typeKey: Int
    let typeName: String?
    init(code: String?, key: Int, name: String?, pointsCategoryLimit: Int?, pointsEarned: Int, pointsEarningFlag: String, potentialPoints: Int, productFeatureAndPointsInformations: [PFPProductFeatureAndPointsInformations]?, typeCode: String?, typeKey: Int, typeName: String?) {
        self.code = code
        self.key = key
        self.name = name
        self.pointsCategoryLimit = pointsCategoryLimit
        self.pointsEarned = pointsEarned
        self.pointsEarningFlag = pointsEarningFlag
        self.potentialPoints = potentialPoints
        self.productFeatureAndPointsInformations = productFeatureAndPointsInformations
        self.typeCode = typeCode
        self.typeKey = typeKey
        self.typeName = typeName
    }
    public init?(json: [String: Any]) {
        let code = json["code"] as? String
        guard let key = json["key"] as? Int else { debugPrint("Expected non-optional property [key] of type [Int] on object [PFPProductFeatureCategoryAndPointsInformations] but did not find");return nil; }
        let name = json["name"] as? String
        let pointsCategoryLimit = json["pointsCategoryLimit"] as? Int
        guard let pointsEarned = json["pointsEarned"] as? Int else { debugPrint("Expected non-optional property [pointsEarned] of type [Int] on object [PFPProductFeatureCategoryAndPointsInformations] but did not find");return nil; }
        guard let pointsEarningFlag = json["pointsEarningFlag"] as? String else { debugPrint("Expected non-optional property [pointsEarningFlag] of type [String] on object [PFPProductFeatureCategoryAndPointsInformations] but did not find");return nil; }
        guard let potentialPoints = json["potentialPoints"] as? Int else { debugPrint("Expected non-optional property [potentialPoints] of type [Int] on object [PFPProductFeatureCategoryAndPointsInformations] but did not find");return nil; }
        let productFeatureAndPointsInformations = PFPProductFeatureAndPointsInformations.createRequiredInstances(from: json, arrayKey: "productFeatureAndPointsInformations")
        let typeCode = json["typeCode"] as? String
        guard let typeKey = json["typeKey"] as? Int else { debugPrint("Expected non-optional property [typeKey] of type [Int] on object [PFPProductFeatureCategoryAndPointsInformations] but did not find");return nil; }
        let typeName = json["typeName"] as? String
        self.init(code: code, key: key, name: name, pointsCategoryLimit: pointsCategoryLimit, pointsEarned: pointsEarned, pointsEarningFlag: pointsEarningFlag, potentialPoints: potentialPoints, productFeatureAndPointsInformations: productFeatureAndPointsInformations, typeCode: typeCode, typeKey: typeKey, typeName: typeName)
    }
}

public struct PFPProductFeatureAndPointsInformations: CreatableFromJSON {
    let code: String?
    let eventTypes: [PFPEventTypes]?
    let key: Int
    let name: String?
    let pointsEarned: Int
    let pointsEarningFlag: String
    let pointsEntries: [PFPPointsEntry]?
    let potentialPoints: Int
    let productSubfeatures: [PFPProductSubfeatures]?
    init(code: String?, eventTypes: [PFPEventTypes]?, key: Int, name: String?, pointsEarned: Int, pointsEarningFlag: String, pointsEntries: [PFPPointsEntry]?, potentialPoints: Int, productSubfeatures: [PFPProductSubfeatures]?) {
        self.code = code
        self.eventTypes = eventTypes
        self.key = key
        self.name = name
        self.pointsEarned = pointsEarned
        self.pointsEarningFlag = pointsEarningFlag
        self.pointsEntries = pointsEntries
        self.potentialPoints = potentialPoints
        self.productSubfeatures = productSubfeatures
    }
    public init?(json: [String: Any]) {
        let code = json["code"] as? String
        let eventTypes = PFPEventTypes.createRequiredInstances(from: json, arrayKey: "eventTypes")
        guard let key = json["key"] as? Int else { debugPrint("Expected non-optional property [key] of type [Int] on object [PFPProductFeatureAndPointsInformations] but did not find");return nil; }
        let name = json["name"] as? String
        guard let pointsEarned = json["pointsEarned"] as? Int else { debugPrint("Expected non-optional property [pointsEarned] of type [Int] on object [PFPProductFeatureAndPointsInformations] but did not find");return nil; }
        guard let pointsEarningFlag = json["pointsEarningFlag"] as? String else { debugPrint("Expected non-optional property [pointsEarningFlag] of type [String] on object [PFPProductFeatureAndPointsInformations] but did not find");return nil; }
        let pointsEntries = PFPPointsEntry.createRequiredInstances(from: json, arrayKey: "pointsEntries")
        guard let potentialPoints = json["potentialPoints"] as? Int else { debugPrint("Expected non-optional property [potentialPoints] of type [Int] on object [PFPProductFeatureAndPointsInformations] but did not find");return nil; }
        let productSubfeatures = PFPProductSubfeatures.createRequiredInstances(from: json, arrayKey: "productSubfeatures")
        self.init(code: code, eventTypes: eventTypes, key: key, name: name, pointsEarned: pointsEarned, pointsEarningFlag: pointsEarningFlag, pointsEntries: pointsEntries, potentialPoints: potentialPoints, productSubfeatures: productSubfeatures)
    }
}

public struct PFPEventTypes: CreatableFromJSON {
    let code: String?
    let key: Int
    let name: String?
    init(code: String?, key: Int, name: String?) {
        self.code = code
        self.key = key
        self.name = name
    }
    public init?(json: [String: Any]) {
        let code = json["code"] as? String
        guard let key = json["key"] as? Int else { debugPrint("Expected non-optional property [key] of type [Int] on object [PFPEventTypes] but did not find");return nil; }
        let name = json["name"] as? String
        self.init(code: code, key: key, name: name)
    }
}

public struct PFPPointsEntry: CreatableFromJSON {
    let categoryCode: String?
    let categoryKey: Int
    let categoryLimit: Int?
    let categoryName: String?
    let frequencyLimit: Int
    let maximumMembershipPeriodPoints: Int
    let pointsEarned: Int
    let pointsEntryLimit: Int?
    let potentialPoints: Int
    let potentialPointses: [PFPPotentialPoints]?
    let typeCode: String?
    let typeKey: Int
    let typeName: String?
    init(categoryCode: String?, categoryKey: Int, categoryLimit: Int?, categoryName: String?, frequencyLimit: Int, maximumMembershipPeriodPoints: Int, pointsEarned: Int, pointsEntryLimit: Int?, potentialPoints: Int, potentialPointses: [PFPPotentialPoints]?, typeCode: String?, typeKey: Int, typeName: String?) {
        self.categoryCode = categoryCode
        self.categoryKey = categoryKey
        self.categoryLimit = categoryLimit
        self.categoryName = categoryName
        self.frequencyLimit = frequencyLimit
        self.maximumMembershipPeriodPoints = maximumMembershipPeriodPoints
        self.pointsEarned = pointsEarned
        self.pointsEntryLimit = pointsEntryLimit
        self.potentialPoints = potentialPoints
        self.potentialPointses = potentialPointses
        self.typeCode = typeCode
        self.typeKey = typeKey
        self.typeName = typeName
    }
    public init?(json: [String: Any]) {
        let categoryCode = json["categoryCode"] as? String
        guard let categoryKey = json["categoryKey"] as? Int else { debugPrint("Expected non-optional property [categoryKey] of type [Int] on object [PFPPointsEntry] but did not find");return nil; }
        let categoryLimit = json["categoryLimit"] as? Int
        let categoryName = json["categoryName"] as? String
        guard let frequencyLimit = json["frequencyLimit"] as? Int else { debugPrint("Expected non-optional property [frequencyLimit] of type [Int] on object [PFPPointsEntry] but did not find");return nil; }
        guard let maximumMembershipPeriodPoints = json["maximumMembershipPeriodPoints"] as? Int else { debugPrint("Expected non-optional property [maximumMembershipPeriodPoints] of type [Int] on object [PFPPointsEntry] but did not find");return nil; }
        guard let pointsEarned = json["pointsEarned"] as? Int else { debugPrint("Expected non-optional property [pointsEarned] of type [Int] on object [PFPPointsEntry] but did not find");return nil; }
        let pointsEntryLimit = json["pointsEntryLimit"] as? Int
        guard let potentialPoints = json["potentialPoints"] as? Int else { debugPrint("Expected non-optional property [potentialPoints] of type [Int] on object [PFPPointsEntry] but did not find");return nil; }
        let potentialPointses = PFPPotentialPoints.createRequiredInstances(from: json, arrayKey: "potentialPointses")
        let typeCode = json["typeCode"] as? String
        guard let typeKey = json["typeKey"] as? Int else { debugPrint("Expected non-optional property [typeKey] of type [Int] on object [PFPPointsEntry] but did not find");return nil; }
        let typeName = json["typeName"] as? String
        self.init(categoryCode: categoryCode, categoryKey: categoryKey, categoryLimit: categoryLimit, categoryName: categoryName, frequencyLimit: frequencyLimit, maximumMembershipPeriodPoints: maximumMembershipPeriodPoints, pointsEarned: pointsEarned, pointsEntryLimit: pointsEntryLimit, potentialPoints: potentialPoints, potentialPointses: potentialPointses, typeCode: typeCode, typeKey: typeKey, typeName: typeName)
    }
}

public struct PFPPotentialPoints: CreatableFromJSON {
    let conditions: [PFPConditions]?
    let potentialPoints: Int
    init(conditions: [PFPConditions]?, potentialPoints: Int) {
        self.conditions = conditions
        self.potentialPoints = potentialPoints
    }
    public init?(json: [String: Any]) {
        let conditions = PFPConditions.createRequiredInstances(from: json, arrayKey: "conditions")
        guard let potentialPoints = json["potentialPoints"] as? Int else { debugPrint("Expected non-optional property [potentialPoints] of type [Int] on object [PFPPotentialPoints] but did not find");return nil; }
        self.init(conditions: conditions, potentialPoints: potentialPoints)
    }
}

public struct PFPConditions: CreatableFromJSON {
    let greaterThan: Int?
    let greaterThanOrEqualTo: Int?
    let lessThan: Int?
    let lessThanOrEqualTo: Int?
    let metadataTypeCode: String?
    let metadataTypeKey: Int
    let metadataTypeName: String?
    let unitOfMeasure: String?
    init(greaterThan: Int?, greaterThanOrEqualTo: Int?, lessThan: Int?, lessThanOrEqualTo: Int?, metadataTypeCode: String?, metadataTypeKey: Int, metadataTypeName: String?, unitOfMeasure: String?) {
        self.greaterThan = greaterThan
        self.greaterThanOrEqualTo = greaterThanOrEqualTo
        self.lessThan = lessThan
        self.lessThanOrEqualTo = lessThanOrEqualTo
        self.metadataTypeCode = metadataTypeCode
        self.metadataTypeKey = metadataTypeKey
        self.metadataTypeName = metadataTypeName
        self.unitOfMeasure = unitOfMeasure
    }
    public init?(json: [String: Any]) {
        let greaterThan = json["greaterThan"] as? Int
        let greaterThanOrEqualTo = json["greaterThanOrEqualTo"] as? Int
        let lessThan = json["lessThan"] as? Int
        let lessThanOrEqualTo = json["lessThanOrEqualTo"] as? Int
        let metadataTypeCode = json["metadataTypeCode"] as? String
        guard let metadataTypeKey = json["metadataTypeKey"] as? Int else { debugPrint("Expected non-optional property [metadataTypeKey] of type [Int] on object [PFPConditions] but did not find");return nil; }
        let metadataTypeName = json["metadataTypeName"] as? String
        let unitOfMeasure = json["unitOfMeasure"] as? String
        self.init(greaterThan: greaterThan, greaterThanOrEqualTo: greaterThanOrEqualTo, lessThan: lessThan, lessThanOrEqualTo: lessThanOrEqualTo, metadataTypeCode: metadataTypeCode, metadataTypeKey: metadataTypeKey, metadataTypeName: metadataTypeName, unitOfMeasure: unitOfMeasure)
    }
}

public struct PFPProductSubfeatures: CreatableFromJSON {
    let code: String?
    let key: Int
    let name: String?
    let pointsEarned: Int
    let pointsEarningFlag: String
    let potentialPoints: Int
    let productSubfeatureEventTypes: [PFPProductSubfeatureEventTypes]?
    let productSubfeaturePointsEntries: [PFPProductSubfeaturePointsEntry]?
    init(code: String?, key: Int, name: String?, pointsEarned: Int, pointsEarningFlag: String, potentialPoints: Int, productSubfeatureEventTypes: [PFPProductSubfeatureEventTypes]?, productSubfeaturePointsEntries: [PFPProductSubfeaturePointsEntry]?) {
        self.code = code
        self.key = key
        self.name = name
        self.pointsEarned = pointsEarned
        self.pointsEarningFlag = pointsEarningFlag
        self.potentialPoints = potentialPoints
        self.productSubfeatureEventTypes = productSubfeatureEventTypes
        self.productSubfeaturePointsEntries = productSubfeaturePointsEntries
    }
    public init?(json: [String: Any]) {
        let code = json["code"] as? String
        guard let key = json["key"] as? Int else { debugPrint("Expected non-optional property [key] of type [Int] on object [PFPProductSubfeatures] but did not find");return nil; }
        let name = json["name"] as? String
        guard let pointsEarned = json["pointsEarned"] as? Int else { debugPrint("Expected non-optional property [pointsEarned] of type [Int] on object [PFPProductSubfeatures] but did not find");return nil; }
        guard let pointsEarningFlag = json["pointsEarningFlag"] as? String else { debugPrint("Expected non-optional property [pointsEarningFlag] of type [String] on object [PFPProductSubfeatures] but did not find");return nil; }
        guard let potentialPoints = json["potentialPoints"] as? Int else { debugPrint("Expected non-optional property [potentialPoints] of type [Int] on object [PFPProductSubfeatures] but did not find");return nil; }
        let productSubfeatureEventTypes = PFPProductSubfeatureEventTypes.createRequiredInstances(from: json, arrayKey: "productSubfeatureEventTypes")
        let productSubfeaturePointsEntries = PFPProductSubfeaturePointsEntry.createRequiredInstances(from: json, arrayKey: "productSubfeaturePointsEntries")
        self.init(code: code, key: key, name: name, pointsEarned: pointsEarned, pointsEarningFlag: pointsEarningFlag, potentialPoints: potentialPoints, productSubfeatureEventTypes: productSubfeatureEventTypes, productSubfeaturePointsEntries: productSubfeaturePointsEntries)
    }
}

public struct PFPProductSubfeatureEventTypes: CreatableFromJSON {
    let code: String?
    let key: Int
    let name: String?
    init(code: String?, key: Int, name: String?) {
        self.code = code
        self.key = key
        self.name = name
    }
    public init?(json: [String: Any]) {
        let code = json["code"] as? String
        guard let key = json["key"] as? Int else { debugPrint("Expected non-optional property [key] of type [Int] on object [PFPProductSubfeatureEventTypes] but did not find");return nil; }
        let name = json["name"] as? String
        self.init(code: code, key: key, name: name)
    }
}

public struct PFPProductSubfeaturePointsEntry: CreatableFromJSON {
    let categoryCode: String?
    let categoryKey: Int
    let categoryLimit: Int?
    let categoryName: String?
    let frequencyLimit: Int
    let maximumMembershipPeriodPoints: Int
    let pointsEarned: Int
    let pointsEntryLimit: Int?
    let potentialPoints: Int
    let productSubfeaturePotentialPointses: [PFPProductSubfeaturePotentialPoints]?
    let typeCode: String?
    let typeKey: Int
    let typeName: String?
    init(categoryCode: String?, categoryKey: Int, categoryLimit: Int?, categoryName: String?, frequencyLimit: Int, maximumMembershipPeriodPoints: Int, pointsEarned: Int, pointsEntryLimit: Int?, potentialPoints: Int, productSubfeaturePotentialPointses: [PFPProductSubfeaturePotentialPoints]?, typeCode: String?, typeKey: Int, typeName: String?) {
        self.categoryCode = categoryCode
        self.categoryKey = categoryKey
        self.categoryLimit = categoryLimit
        self.categoryName = categoryName
        self.frequencyLimit = frequencyLimit
        self.maximumMembershipPeriodPoints = maximumMembershipPeriodPoints
        self.pointsEarned = pointsEarned
        self.pointsEntryLimit = pointsEntryLimit
        self.potentialPoints = potentialPoints
        self.productSubfeaturePotentialPointses = productSubfeaturePotentialPointses
        self.typeCode = typeCode
        self.typeKey = typeKey
        self.typeName = typeName
    }
    public init?(json: [String: Any]) {
        let categoryCode = json["categoryCode"] as? String
        guard let categoryKey = json["categoryKey"] as? Int else { debugPrint("Expected non-optional property [categoryKey] of type [Int] on object [PFPProductSubfeaturePointsEntry] but did not find");return nil; }
        let categoryLimit = json["categoryLimit"] as? Int
        let categoryName = json["categoryName"] as? String
        guard let frequencyLimit = json["frequencyLimit"] as? Int else { debugPrint("Expected non-optional property [frequencyLimit] of type [Int] on object [PFPProductSubfeaturePointsEntry] but did not find");return nil; }
        guard let maximumMembershipPeriodPoints = json["maximumMembershipPeriodPoints"] as? Int else { debugPrint("Expected non-optional property [maximumMembershipPeriodPoints] of type [Int] on object [PFPProductSubfeaturePointsEntry] but did not find");return nil; }
        guard let pointsEarned = json["pointsEarned"] as? Int else { debugPrint("Expected non-optional property [pointsEarned] of type [Int] on object [PFPProductSubfeaturePointsEntry] but did not find");return nil; }
        let pointsEntryLimit = json["pointsEntryLimit"] as? Int
        guard let potentialPoints = json["potentialPoints"] as? Int else { debugPrint("Expected non-optional property [potentialPoints] of type [Int] on object [PFPProductSubfeaturePointsEntry] but did not find");return nil; }
        let productSubfeaturePotentialPointses = PFPProductSubfeaturePotentialPoints.createRequiredInstances(from: json, arrayKey: "productSubfeaturePotentialPointses")
        let typeCode = json["typeCode"] as? String
        guard let typeKey = json["typeKey"] as? Int else { debugPrint("Expected non-optional property [typeKey] of type [Int] on object [PFPProductSubfeaturePointsEntry] but did not find");return nil; }
        let typeName = json["typeName"] as? String
        self.init(categoryCode: categoryCode, categoryKey: categoryKey, categoryLimit: categoryLimit, categoryName: categoryName, frequencyLimit: frequencyLimit, maximumMembershipPeriodPoints: maximumMembershipPeriodPoints, pointsEarned: pointsEarned, pointsEntryLimit: pointsEntryLimit, potentialPoints: potentialPoints, productSubfeaturePotentialPointses: productSubfeaturePotentialPointses, typeCode: typeCode, typeKey: typeKey, typeName: typeName)
    }
}

public struct PFPProductSubfeaturePotentialPoints: CreatableFromJSON {
    let potentialPoints: Int
    let productSubfeatureConditions: [PFPProductSubfeatureConditions]?
    init(potentialPoints: Int, productSubfeatureConditions: [PFPProductSubfeatureConditions]?) {
        self.potentialPoints = potentialPoints
        self.productSubfeatureConditions = productSubfeatureConditions
    }
    public init?(json: [String: Any]) {
        guard let potentialPoints = json["potentialPoints"] as? Int else { debugPrint("Expected non-optional property [potentialPoints] of type [Int] on object [PFPProductSubfeaturePotentialPoints] but did not find");return nil; }
        let productSubfeatureConditions = PFPProductSubfeatureConditions.createRequiredInstances(from: json, arrayKey: "productSubfeatureConditions")
        self.init(potentialPoints: potentialPoints, productSubfeatureConditions: productSubfeatureConditions)
    }
}

public struct PFPProductSubfeatureConditions: CreatableFromJSON {
    let greaterThan: Int?
    let greaterThanOrEqualTo: Int?
    let lessThan: Int?
    let lessThanOrEqualTo: Int?
    let metadataTypeCode: String?
    let metadataTypeKey: Int
    let metadataTypeName: String?
    let unitOfMeasure: String?
    init(greaterThan: Int?, greaterThanOrEqualTo: Int?, lessThan: Int?, lessThanOrEqualTo: Int?, metadataTypeCode: String?, metadataTypeKey: Int, metadataTypeName: String?, unitOfMeasure: String?) {
        self.greaterThan = greaterThan
        self.greaterThanOrEqualTo = greaterThanOrEqualTo
        self.lessThan = lessThan
        self.lessThanOrEqualTo = lessThanOrEqualTo
        self.metadataTypeCode = metadataTypeCode
        self.metadataTypeKey = metadataTypeKey
        self.metadataTypeName = metadataTypeName
        self.unitOfMeasure = unitOfMeasure
    }
    public init?(json: [String: Any]) {
        let greaterThan = json["greaterThan"] as? Int
        let greaterThanOrEqualTo = json["greaterThanOrEqualTo"] as? Int
        let lessThan = json["lessThan"] as? Int
        let lessThanOrEqualTo = json["lessThanOrEqualTo"] as? Int
        let metadataTypeCode = json["metadataTypeCode"] as? String
        guard let metadataTypeKey = json["metadataTypeKey"] as? Int else { debugPrint("Expected non-optional property [metadataTypeKey] of type [Int] on object [PFPProductSubfeatureConditions] but did not find");return nil; }
        let metadataTypeName = json["metadataTypeName"] as? String
        let unitOfMeasure = json["unitOfMeasure"] as? String
        self.init(greaterThan: greaterThan, greaterThanOrEqualTo: greaterThanOrEqualTo, lessThan: lessThan, lessThanOrEqualTo: lessThanOrEqualTo, metadataTypeCode: metadataTypeCode, metadataTypeKey: metadataTypeKey, metadataTypeName: metadataTypeName, unitOfMeasure: unitOfMeasure)
    }
}
