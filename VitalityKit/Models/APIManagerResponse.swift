//
//  APIManagerResponse.swift
//  VitalityActive
//
//  Created by Wilmar van Heerden on 2016/12/09.
//  Copyright © 2016 Glucode. All rights reserved.
//

import Foundation

//public struct APIManagerResponse: CreatableFromJSON {
//    let loginResponse: LoginResponse?
//    let reason: String?
//    let success: Bool?
//    init(loginResponse: LoginResponse?, reason: String?, success: Bool?) {
//        self.loginResponse = loginResponse
//        self.reason = reason
//        self.success = success
//    }
//    public init?(json: [String: Any]) {
//        let loginResponse = LoginResponse(json: json, key: "loginResponse")
//        let reason = json["reason"] as? String
//        let success = json["success"] as? Bool
//        self.init(loginResponse: loginResponse, reason: reason, success: success)
//    }
//}

public struct APIManagerResponse {
    var responseString: String?
    var json: [String: Any]
    var errors: [APIError]?
    var warnings: [APIWarning]?

    public init(responseString: String?, json: [String: Any]?, errors: [APIError]?, warnings: [APIWarning]?) {
        self.responseString = responseString
        self.json = json ?? [:]
        self.errors = errors
        self.warnings = warnings
    }
}
