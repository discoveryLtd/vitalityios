//
//  GetAllPointsHistoryResponse.swift
//  VitalityActive
//
//  Created by Swagger Codegen on 10/25/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file

import Foundation

public struct GetAllPointsHistoryResponse: CreatableFromJSON {
    let pointsAccounts: [APHPointsAccounts]? // The points account. This covers the points earned and the period in which the points were earned
    init(pointsAccounts: [APHPointsAccounts]?) {
        self.pointsAccounts = pointsAccounts
    }
    public init?(json: [String: Any]) {
        let pointsAccounts = APHPointsAccounts.createRequiredInstances(from: json, arrayKey: "pointsAccounts")
        self.init(pointsAccounts: pointsAccounts)
    }
}

public struct APHPointsAccounts: CreatableFromJSON {
    let carryOverPoints: Int? // The points carried over from the previous points account (i.e. from the previous period)
    let effectiveFrom: String
    let effectiveTo: String
    let pointsEntries: [APHPointsEntry]? // A points entry details how many points are earned for an event based on the evaluation of the rules configured for that event type
    let pointsTotal: Int
    init(carryOverPoints: Int?, effectiveFrom: String, effectiveTo: String, pointsEntries: [APHPointsEntry]?, pointsTotal: Int) {
        self.carryOverPoints = carryOverPoints
        self.effectiveFrom = effectiveFrom
        self.effectiveTo = effectiveTo
        self.pointsEntries = pointsEntries
        self.pointsTotal = pointsTotal
    }
    public init?(json: [String: Any]) {
        let carryOverPoints = json["carryOverPoints"] as? Int
        guard let effectiveFrom = json["effectiveFrom"] as? String else { debugPrint("Expected non-optional property [effectiveFrom] of type [String] on object [APHPointsAccounts] but did not find");return nil; }
        guard let effectiveTo = json["effectiveTo"] as? String else { debugPrint("Expected non-optional property [effectiveTo] of type [String] on object [APHPointsAccounts] but did not find");return nil; }
        let pointsEntries = APHPointsEntry.createRequiredInstances(from: json, arrayKey: "pointsEntries")
        guard let pointsTotal = json["pointsTotal"] as? Int else { debugPrint("Expected non-optional property [pointsTotal] of type [Int] on object [APHPointsAccounts] but did not find");return nil; }
        self.init(carryOverPoints: carryOverPoints, effectiveFrom: effectiveFrom, effectiveTo: effectiveTo, pointsEntries: pointsEntries, pointsTotal: pointsTotal)
    }
}

public struct APHPointsEntry: CreatableFromJSON {
    let categoryCode: String? // Category of the points entry type E.g Fitness
    let categoryKey: Int
    let categoryName: String
    let earnedValue: Int
    let effectiveDate: String
    let eventId: Int
    let goalPointsTrackers: [APHGoalPointsTrackers]? // A listing of events logged for a progress tracker.
    let id: Int
    let metadatas: [APHMetadatas]? // Metadata provided data information about other data. In the points context this details describing the event details etc.
    let party: Int? // Party that the points entry belongs to
    
    let potentialValue: Int
    let prelimitValue: Int
    let reasons: [APHReasons]? // Reason for awarding/not awarding the points
    let typeCode: String? // This is the human readable text related to the typeKey specified in the payload.. Essentially may be used by requesting parties but in general is to facilitate the payload context. It is in essence the short text description of the Type attribute specified in the functional requirements and will be specific to the language chosen by application for the carrier
    
    let typeKey: Int
    let typeName: String? // This is the human readable text related to the typeKey specified in the payload.. Essentially may be used by requesting parties but in general is to facilitate the payload context. It is in essence the short text description of the Type attribute specified in the functional requirements and will be specific to the language chosen by application for the carrier
    
    init(categoryCode: String?, categoryKey: Int, categoryName: String, earnedValue: Int, effectiveDate: String, eventId: Int, goalPointsTrackers: [APHGoalPointsTrackers]?, id: Int, metadatas: [APHMetadatas]?, party: Int?, potentialValue: Int, prelimitValue: Int, reasons: [APHReasons]?, typeCode: String?, typeKey: Int, typeName: String?) {
        self.categoryCode = categoryCode
        self.categoryKey = categoryKey
        self.categoryName = categoryName
        self.earnedValue = earnedValue
        self.effectiveDate = effectiveDate
        self.eventId = eventId
        self.goalPointsTrackers = goalPointsTrackers
        self.id = id
        self.metadatas = metadatas
        self.party = party
        self.potentialValue = potentialValue
        self.prelimitValue = prelimitValue
        self.reasons = reasons
        self.typeCode = typeCode
        self.typeKey = typeKey
        self.typeName = typeName
    }
    public init?(json: [String: Any]) {
        let categoryCode = json["categoryCode"] as? String
        guard let categoryKey = json["categoryKey"] as? Int else { debugPrint("Expected non-optional property [categoryKey] of type [Int] on object [APHPointsEntry] but did not find");return nil; }
        guard let categoryName = json["categoryName"] as? String else { debugPrint("Expected non-optional property [categoryName] of type [String] on object [APHPointsEntry] but did not find");return nil; }
        guard let earnedValue = json["earnedValue"] as? Int else { debugPrint("Expected non-optional property [earnedValue] of type [Int] on object [APHPointsEntry] but did not find");return nil; }
        guard let effectiveDate = json["effectiveDate"] as? String else { debugPrint("Expected non-optional property [effectiveDate] of type [String] on object [APHPointsEntry] but did not find");return nil; }
        guard let eventId = json["eventId"] as? Int else { debugPrint("Expected non-optional property [eventId] of type [Int] on object [APHPointsEntry] but did not find");return nil; }
        let goalPointsTrackers = APHGoalPointsTrackers.createRequiredInstances(from: json, arrayKey: "goalPointsTrackers")
        guard let id = json["id"] as? Int else { debugPrint("Expected non-optional property [id] of type [Int] on object [APHPointsEntry] but did not find");return nil; }
        let metadatas = APHMetadatas.createRequiredInstances(from: json, arrayKey: "metadatas")
        let party = json["party"] as? Int
        guard let potentialValue = json["potentialValue"] as? Int else { debugPrint("Expected non-optional property [potentialValue] of type [Int] on object [APHPointsEntry] but did not find");return nil; }
        guard let prelimitValue = json["prelimitValue"] as? Int else { debugPrint("Expected non-optional property [prelimitValue] of type [Int] on object [APHPointsEntry] but did not find");return nil; }
        let reasons = APHReasons.createRequiredInstances(from: json, arrayKey: "reasons")
        let typeCode = json["typeCode"] as? String
        guard let typeKey = json["typeKey"] as? Int else { debugPrint("Expected non-optional property [typeKey] of type [Int] on object [APHPointsEntry] but did not find");return nil; }
        let typeName = json["typeName"] as? String
        self.init(categoryCode: categoryCode, categoryKey: categoryKey, categoryName: categoryName, earnedValue: earnedValue, effectiveDate: effectiveDate, eventId: eventId, goalPointsTrackers: goalPointsTrackers, id: id, metadatas: metadatas, party: party, potentialValue: potentialValue, prelimitValue: prelimitValue, reasons: reasons, typeCode: typeCode, typeKey: typeKey, typeName: typeName)
    }
}

public struct APHGoalPointsTrackers: CreatableFromJSON {
    let goalCode: String? // This is the human readable text related to the typeKey specified in the payload.. Essentially may be used by requesting parties but in general is to facilitate the payload context. It is in essence the short text description of the Type attribute specified in the functional requirements and will be specific to the language chosen by application for the carrier
    let goalKey: Int
    let goalName: String? // This is the human readable text related to the typeKey specified in the payload.. Essentially may be used by requesting parties but in general is to facilitate the payload context. It is in essence the short text description of the Type attribute specified in the functional requirements and will be specific to the language chosen by application for the carrier
    let pointsContributing: Int
    init(goalCode: String?, goalKey: Int, goalName: String?, pointsContributing: Int) {
        self.goalCode = goalCode
        self.goalKey = goalKey
        self.goalName = goalName
        self.pointsContributing = pointsContributing
    }
    public init?(json: [String: Any]) {
        let goalCode = json["goalCode"] as? String
        guard let goalKey = json["goalKey"] as? Int else { debugPrint("Expected non-optional property [goalKey] of type [Int] on object [APHGoalPointsTrackers] but did not find");return nil; }
        let goalName = json["goalName"] as? String
        guard let pointsContributing = json["pointsContributing"] as? Int else { debugPrint("Expected non-optional property [pointsContributing] of type [Int] on object [APHGoalPointsTrackers] but did not find");return nil; }
        self.init(goalCode: goalCode, goalKey: goalKey, goalName: goalName, pointsContributing: pointsContributing)
    }
}

public struct APHMetadatas: CreatableFromJSON {
    let typeCode: String? // This is the human readable text related to the typeKey specified in the payload.. Essentially may be used by requesting parties but in general is to facilitate the payload context. It is in essence the short text description of the Type attribute specified in the functional requirements and will be specific to the language chosen by application for the carrier
    let typeKey: Int
    let typeName: String? // This is the human readable text related to the typeKey specified in the payload.. Essentially may be used by requesting parties but in general is to facilitate the payload context. It is in essence the short text description of the Type attribute specified in the functional requirements and will be specific to the language chosen by application for the carrier
    let unitOfMeasure: String? // Unit of Measure
    let value: String
    init(typeCode: String?, typeKey: Int, typeName: String?, unitOfMeasure: String?, value: String) {
        self.typeCode = typeCode
        self.typeKey = typeKey
        self.typeName = typeName
        self.unitOfMeasure = unitOfMeasure
        self.value = value
    }
    public init?(json: [String: Any]) {
        let typeCode = json["typeCode"] as? String
        guard let typeKey = json["typeKey"] as? Int else { debugPrint("Expected non-optional property [typeKey] of type [Int] on object [APHMetadatas] but did not find");return nil; }
        let typeName = json["typeName"] as? String
        let unitOfMeasure = json["unitOfMeasure"] as? String
        guard let value = json["value"] as? String else { debugPrint("Expected non-optional property [value] of type [String] on object [APHMetadatas] but did not find");return nil; }
        self.init(typeCode: typeCode, typeKey: typeKey, typeName: typeName, unitOfMeasure: unitOfMeasure, value: value)
    }
}

public struct APHReasons: CreatableFromJSON {
    let categoryCode: String? // This is the human readable text related to the typeKey specified in the payload.. Essentially may be used by requesting parties but in general is to facilitate the payload context. It is in essence the short text description of the Type attribute specified in the functional requirements and will be specific to the language chosen by application for the carrier
    let categoryKey: Int
    let categoryName: String? // This is the human readable text related to the typeKey specified in the payload.. Essentially may be used by requesting parties but in general is to facilitate the payload context. It is in essence the short text description of the Type attribute specified in the functional requirements and will be specific to the language chosen by application for the carrier
    let reasonCode: String? // Identifies the reason for awarding/not awarding the points
    
    let reasonKey: Int
    let reasonName: String? // A detailed description of the; reference, association type associated to a specific key. Its purpose is to describe the context of the unique key in more detail, in order to facilitate easy understanding of the use of the key it describes.
    init(categoryCode: String?, categoryKey: Int, categoryName: String?, reasonCode: String?, reasonKey: Int, reasonName: String?) {
        self.categoryCode = categoryCode
        self.categoryKey = categoryKey
        self.categoryName = categoryName
        self.reasonCode = reasonCode
        self.reasonKey = reasonKey
        self.reasonName = reasonName
    }
    public init?(json: [String: Any]) {
        let categoryCode = json["categoryCode"] as? String
        guard let categoryKey = json["categoryKey"] as? Int else { debugPrint("Expected non-optional property [categoryKey] of type [Int] on object [APHReasons] but did not find");return nil; }
        let categoryName = json["categoryName"] as? String
        let reasonCode = json["reasonCode"] as? String
        guard let reasonKey = json["reasonKey"] as? Int else { debugPrint("Expected non-optional property [reasonKey] of type [Int] on object [APHReasons] but did not find");return nil; }
        let reasonName = json["reasonName"] as? String
        self.init(categoryCode: categoryCode, categoryKey: categoryKey, categoryName: categoryName, reasonCode: reasonCode, reasonKey: reasonKey, reasonName: reasonName)
    }
}
