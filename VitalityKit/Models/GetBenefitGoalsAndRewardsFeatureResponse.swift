//
//  GetBenefitGoalsAndRewardsFeatureResponse.swift
//  VitalityActive
//
//  Created by Steven Layug on 4/24/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import UIKit

public struct GetBenefitGoalsAndRewardsFeatureResponse: CreatableFromJSON {
    let getBenefitGoalsAndRewardsFeatureResponse: [BGRFeatureBenefit]?
    init(getBenefitGoalsAndRewardsFeatureResponse: [BGRFeatureBenefit]?) {
        self.getBenefitGoalsAndRewardsFeatureResponse = getBenefitGoalsAndRewardsFeatureResponse
    }
    public init?(json: [String : Any]) {
        let getBenefitGoalsAndRewardsFeatureResponse = BGRFeatureBenefit.createRequiredInstances(from: json, arrayKey: "benefit")
        self.init(getBenefitGoalsAndRewardsFeatureResponse: getBenefitGoalsAndRewardsFeatureResponse)
    }
}

public struct BGRFeatureBenefit: CreatableFromJSON {
    let effectiveFrom: String?
    let effectiveTo: String?
    
    let goalTrackers: [BGRGoalTracker]?
    
    let goalsRemaining: Int?
    let id: Int?
    let productKey: Int?

    let purchase: BGRPurchase?
    
    let totalGoalsCompleted: Int?
    let totalGoalsForBenefit: Int?
    let totalGoalsRewardAmount: String?
    let totalGoalsRewardQuantity: Int?
    
    init(effectiveFrom: String?, effectiveTo: String?, goalsRemaining: Int?, id: Int?, productKey: Int?, goalTrackers: [BGRGoalTracker]?, purchase: BGRPurchase?, totalGoalsCompleted: Int?, totalGoalsForBenefit: Int?, totalGoalsRewardAmount: String?, totalGoalsRewardQuantity: Int?) {
        self.effectiveFrom = effectiveFrom
        self.effectiveTo = effectiveTo
   
        self.goalTrackers = goalTrackers
        
        self.goalsRemaining = goalsRemaining
        self.id = id
        self.productKey = productKey
        
        self.purchase = purchase
        
        self.totalGoalsCompleted = totalGoalsCompleted
        self.totalGoalsForBenefit = totalGoalsForBenefit
        self.totalGoalsRewardAmount = totalGoalsRewardAmount
        self.totalGoalsRewardQuantity = totalGoalsRewardQuantity
    }
    public init?(json: [String : Any]) {
    let effectiveFrom = json["effectiveFrom"] as? String
    let effectiveTo = json["effectiveTo"] as? String
       
    let goalTrackers = BGRGoalTracker.createRequiredInstances(from: json, arrayKey: "goalTrackers")
        
    let goalsRemaining = json["goalsRemaining"] as? Int
    let id = json["id"] as? Int
    let productKey = json["productKey"] as? Int
   
    let purchase = BGRPurchase(json: json, key: "purchase")
        
    let totalGoalsCompleted = json["totalGoalsCompleted"] as? Int
    let totalGoalsForBenefit = json["totalGoalsForBenefit"] as? Int
    let totalGoalsRewardAmount = json["totalGoalsRewardAmount"] as? String
    let totalGoalsRewardQuantity = json["totalGoalsRewardQuantity"] as? Int
        self.init(effectiveFrom: effectiveFrom, effectiveTo: effectiveTo, goalsRemaining: goalsRemaining, id: id, productKey: productKey, goalTrackers: goalTrackers, purchase: purchase, totalGoalsCompleted: totalGoalsCompleted, totalGoalsForBenefit: totalGoalsForBenefit, totalGoalsRewardAmount:totalGoalsRewardAmount, totalGoalsRewardQuantity: totalGoalsRewardQuantity)
    }
}

public struct BGRGoalTracker: CreatableFromJSON {
    let goalCode: String?
    let goalKey: Int?
    let goalName: String?
    let monitorUntil: String?
    let pointsAchievedTowardsGoal: Int?

    let rewardValues: [BGRRewardValues]?
    
    let validFrom: String?
    let validTo: String?
    let highestObjectiveAchievedName: String?
    
    init(goalCode: String?, goalKey: Int?, goalName: String?, monitorUntil: String?, pointsAchievedTowardsGoal: Int?, rewardValues: [BGRRewardValues]?, validFrom: String?, validTo: String?, highestObjectiveAchievedName: String?) {
        self.goalCode = goalCode
        self.goalKey = goalKey
        self.goalName = goalName
        self.monitorUntil = monitorUntil
        self.pointsAchievedTowardsGoal = pointsAchievedTowardsGoal
        
        self.rewardValues = rewardValues
        
        self.validFrom = validFrom
        self.validTo = validTo
        self.highestObjectiveAchievedName = highestObjectiveAchievedName
    }
    public init?(json: [String : Any]) {
        let goalCode = json["goalCode"] as? String
        let goalKey = json["goalKey"] as? Int
        let goalName = json["goalName"] as? String
        let monitorUntil = json["monitorUntil"] as? String
        let pointsAchievedTowardsGoal = json["pointsAchievedTowardsGoal"] as? Int
        
        let rewardValues = BGRRewardValues.createRequiredInstances(from: json, arrayKey: "rewardValues")
        
        let validFrom = json["validFrom"] as? String
        let validTo = json["validTo"] as? String
        let highestObjectiveAchievedName = json["highestObjectiveAchievedName"] as? String
        
        self.init(goalCode: goalCode, goalKey: goalKey, goalName: goalName, monitorUntil: monitorUntil, pointsAchievedTowardsGoal: pointsAchievedTowardsGoal, rewardValues: rewardValues, validFrom: validFrom, validTo: validTo, highestObjectiveAchievedName: highestObjectiveAchievedName)
    }
}

public struct BGRRewardValues: CreatableFromJSON {
    let totalRewardValueAmount: String?
    let totalRewardValueQuantity: Int?
    
    init(totalRewardValueAmount: String?, totalRewardValueQuantity: Int?) {
        self.totalRewardValueAmount = totalRewardValueAmount
        self.totalRewardValueQuantity = totalRewardValueQuantity
    }
    public init?(json: [String : Any]) {
        let totalRewardValueAmount = json["totalRewardValueAmount"] as? String
        let totalRewardValueQuantity = json["totalRewardValueQuantity"] as? Int
        self.init(totalRewardValueAmount: totalRewardValueAmount, totalRewardValueQuantity: totalRewardValueQuantity)
    }
}


public struct BGRPurchase: CreatableFromJSON {
    let amounts: [BGRAmounts]?
    let purchaseId: Int?
    let purchaseItemProduct: BGRPurchaseItemProduct?
    
    init(amounts: [BGRAmounts]?, purchaseId: Int?, purchaseItemProduct: BGRPurchaseItemProduct?){
        self.amounts = amounts
        self.purchaseId = purchaseId
        self.purchaseItemProduct = purchaseItemProduct
    }
    public init?(json: [String: Any]) {
        let amounts = BGRAmounts.createRequiredInstances(from: json, arrayKey: "amounts")
        let purchaseId = json["purchaseId"] as? Int
        let purchaseItemProduct = BGRPurchaseItemProduct(json: json, key: "purchaseItemProduct")
        self.init(amounts: amounts, purchaseId: purchaseId, purchaseItemProduct: purchaseItemProduct)
    }
}

public struct BGRAmounts: CreatableFromJSON {
    let typeCode: String?
    let typeKey: Int?
    let typeName: String?
    let value: String?
    
    init(typeCode: String?, typeKey: Int?, typeName: String?, value: String?){
        self.typeCode = typeCode
        self.typeKey = typeKey
        self.typeName = typeName
        self.value = value
    }
    public init?(json: [String: Any]) {
        let typeCode = json["typeCode"] as? String
        let typeKey = json["typeKey"] as? Int
        let typeName = json["typeName"] as? String
        let value = json["value"] as? String
        self.init(typeCode: typeCode, typeKey: typeKey, typeName: typeName, value: value)
    }
}

public struct BGRPurchaseItemProduct: CreatableFromJSON {
    let productDescription: String?
    let productKey: Int?
    
    init(productDescription: String?, productKey: Int?) {
        self.productDescription = productDescription
        self .productKey = productKey
    }
    public init?(json: [String : Any]) {
        let productDescription = json["description"] as? String
        let productKey = json["productKey"] as? Int
        self.init(productDescription: productDescription, productKey: productKey)
    }
}
