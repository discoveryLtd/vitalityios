//
//  GetPartnersByCategoryResponse.swift
//  VitalityActive
//
//  Created by Swagger Codegen on 11/21/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file

import Foundation

public struct GetPartnersByCategoryResponse: CreatableFromJSON {
    let productFeatureGroups: [PBCProductFeatureGroups]?
    let typeCode: String
    let typeKey: Int
    let typeName: String
    init(productFeatureGroups: [PBCProductFeatureGroups]?, typeCode: String, typeKey: Int, typeName: String) {
        self.productFeatureGroups = productFeatureGroups
        self.typeCode = typeCode
        self.typeKey = typeKey
        self.typeName = typeName
    }
    public init?(json: [String: Any]) {
        let productFeatureGroups = PBCProductFeatureGroups.createRequiredInstances(from: json, arrayKey: "productFeatureGroups")
        guard let typeCode = json["typeCode"] as? String else { debugPrint("Expected non-optional property [typeCode] of type [String] on object [GetPartnersByCategoryResponse] but did not find");return nil; }
        guard let typeKey = json["typeKey"] as? Int else { debugPrint("Expected non-optional property [typeKey] of type [Int] on object [GetPartnersByCategoryResponse] but did not find");return nil; }
        guard let typeName = json["typeName"] as? String else { debugPrint("Expected non-optional property [typeName] of type [String] on object [GetPartnersByCategoryResponse] but did not find");return nil; }
        self.init(productFeatureGroups: productFeatureGroups, typeCode: typeCode, typeKey: typeKey, typeName: typeName)
    }
}

public struct PBCProductFeatureGroups: CreatableFromJSON {
    let key: Int
    let name: String
    let partnerProductFeatures: [PBCPartnerProductFeatures]? // Defines the time period when a product feature is applicable to the product
    init(key: Int, name: String, partnerProductFeatures: [PBCPartnerProductFeatures]?) {
        self.key = key
        self.name = name
        self.partnerProductFeatures = partnerProductFeatures
    }
    public init?(json: [String: Any]) {
        guard let key = json["key"] as? Int else { debugPrint("Expected non-optional property [key] of type [Int] on object [PBCProductFeatureGroups] but did not find");return nil; }
        guard let name = json["name"] as? String else { debugPrint("Expected non-optional property [name] of type [String] on object [PBCProductFeatureGroups] but did not find");return nil; }
        let partnerProductFeatures = PBCPartnerProductFeatures.createRequiredInstances(from: json, arrayKey: "partnerProductFeatures")
        self.init(key: key, name: name, partnerProductFeatures: partnerProductFeatures)
    }
}

public struct PBCPartnerProductFeatures: CreatableFromJSON {
    let description: String
    let logoFileName: String?
    let longDescription: String
    let name: String
    let typeCode: String? // A feature is a distinctive aspect of a product.
    
    let typeKey: Int
    let typeName: String? // A feature is a distinctive aspect of a product.
    let heading: String? // Available in Employer Rewards
    
    init(description: String, logoFileName: String, longDescription: String, name: String, typeCode: String?, typeKey: Int, typeName: String?, heading: String?) {
        self.description = description
        self.logoFileName =  logoFileName
        self.longDescription = longDescription
        self.name = name
        self.typeCode = typeCode
        self.typeKey = typeKey
        self.typeName = typeName
        self.heading = heading
    }
    public init?(json: [String: Any]) {
        guard let description = json["description"] as? String else { debugPrint("Expected non-optional property [description] of type [String] on object [PBCPartnerProductFeatures] but did not find");return nil; }
        let logoFileName = json["logoFileName"] as? String ?? "none"
        guard let longDescription = json["longDescription"] as? String else { debugPrint("Expected non-optional property [longDescription] of type [String] on object [PBCPartnerProductFeatures] but did not find");return nil; }
        guard let name = json["name"] as? String else { debugPrint("Expected non-optional property [name] of type [String] on object [PBCPartnerProductFeatures] but did not find");return nil; }
        let typeCode = json["typeCode"] as? String
        guard let typeKey = json["typeKey"] as? Int else { debugPrint("Expected non-optional property [typeKey] of type [Int] on object [PBCPartnerProductFeatures] but did not find");return nil; }
        let typeName = json["typeName"] as? String
        let heading = json["heading"] as? String
        self.init(description: description, logoFileName: logoFileName, longDescription: longDescription, name: name, typeCode: typeCode, typeKey: typeKey, typeName: typeName, heading: heading)
    }
}
