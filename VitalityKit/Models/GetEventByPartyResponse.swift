//
//  GetEventByPartyResponse.swift
//  VitalityActive
//
//  Created by Swagger Codegen on 08/08/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file

import Foundation

public struct GetEventByPartyResponse: CreatableFromJSON {
    public let event: [GEBPEvent]? // An event details something that has happened or been done. Events happen at a point in time and this can be different to the point in time it became known. In the request we will need to know who reported the event, who is this applicable to and the date.
    init(event: [GEBPEvent]?) {
        self.event = event
    }
    public init?(json: [String: Any]) {
        let event = GEBPEvent.createRequiredInstances(from: json, arrayKey: "event")
        self.init(event: event)
    }
}

public struct GEBPEvent: CreatableFromJSON {
    let eventDateTime: String?
    let typeKey: Int
    let eventSourceName: String?
    let eventExternalReference: [GEBPEventExternalReference]? // this is a class to facilitate the language requirements of renaming type attribute to type key and adding typeCode attribute. This is for use in Outbound payloads
    let dateLogged: String?
    let typeName: String?
    let reportedBy: Int? // This is the partyID of the party that logged the event.  This is always a party (This could be the source system of the event or the party who reported the event). For instance when a servicing agent (ReportedBy) logs an event for a party (ApplicableTo).
    
    let typeCode: String?
    let associatedEvents: [GEBPAssociatedEvents]? // Associated Event Class used by Get Event by External Reference Respense  Events may be associated with one another. This association depicts a relationship between them.
    let eventSourceKey: Int? // Identifying code for the source of the event.  E.g. Vitality Device Platform, Core - Goals subsystem, etc
    let eventMetaDataOuts: [GEBPEventMetaDataOuts]? // Additional optional descriptive attributes for the event.
    let id: Int?
    let eventSourceCode: String?
    let partyId: Int?
    
    let categoryKey: Int?
    let categoryCode:String?
    let categoryName:String?
    
    init(eventDateTime: String?, typeKey: Int, eventSourceName: String?, eventExternalReference: [GEBPEventExternalReference]?, dateLogged: String?, typeName: String?, reportedBy: Int?, typeCode: String?, associatedEvents: [GEBPAssociatedEvents]?, eventSourceKey: Int?, eventMetaDataOuts: [GEBPEventMetaDataOuts]?, id: Int?, eventSourceCode: String?, partyId: Int?, categoryKey:Int?, categoryCode:String?, categoryName:String?) {
        self.eventDateTime = eventDateTime
        self.typeKey = typeKey
        self.eventSourceName = eventSourceName
        self.eventExternalReference = eventExternalReference
        self.dateLogged = dateLogged
        self.typeName = typeName
        self.reportedBy = reportedBy
        self.typeCode = typeCode
        self.associatedEvents = associatedEvents
        self.eventSourceKey = eventSourceKey
        self.eventMetaDataOuts = eventMetaDataOuts
        self.id = id
        self.eventSourceCode = eventSourceCode
        self.partyId = partyId
        self.categoryKey = categoryKey
        self.categoryCode = categoryCode
        self.categoryName = categoryName
    }
    public init?(json: [String: Any]) {
        let eventDateTime = json["eventDateTime"] as? String
        guard let typeKey = json["typeKey"] as? Int else { debugPrint("Expected non-optional property [typeKey] of type [Int] on object [GEBPEvent] but did not find");return nil; }
        let eventSourceName = json["eventSourceName"] as? String
        let eventExternalReference = GEBPEventExternalReference.createRequiredInstances(from: json, arrayKey: "eventExternalReference")
        let dateLogged = json["dateLogged"] as? String
        let typeName = json["typeName"] as? String
        let reportedBy = json["reportedBy"] as? Int
        let typeCode = json["typeCode"] as? String
        let associatedEvents = GEBPAssociatedEvents.createRequiredInstances(from: json, arrayKey: "associatedEvents")
        let eventSourceKey = json["eventSourceKey"] as? Int
        let eventMetaDataOuts = GEBPEventMetaDataOuts.createRequiredInstances(from: json, arrayKey: "eventMetaDataOuts")
        let id = json["id"] as? Int
        let eventSourceCode = json["eventSourceCode"] as? String
        let partyId = json["partyId"] as? Int
        let categoryKey = json["categoryKey"] as? Int
        let categoryCode = json["categoryCode"] as? String
        let categoryName = json["categoryName"] as? String
        self.init(eventDateTime: eventDateTime,
                  typeKey: typeKey,
                  eventSourceName: eventSourceName,
                  eventExternalReference: eventExternalReference,
                  dateLogged: dateLogged,
                  typeName: typeName,
                  reportedBy: reportedBy,
                  typeCode: typeCode,
                  associatedEvents: associatedEvents,
                  eventSourceKey: eventSourceKey,
                  eventMetaDataOuts: eventMetaDataOuts,
                  id: id,
                  eventSourceCode: eventSourceCode,
                  partyId: partyId,
                  categoryKey: categoryKey,
                  categoryCode: categoryCode,
                  categoryName: categoryName)
    }
}

public struct GEBPEventExternalReference: CreatableFromJSON {
    let typeKey: Int
    let typeName: String?
    let value: String
    let typeCode: String?
    init(typeKey: Int, typeName: String?, value: String, typeCode: String?) {
        self.typeKey = typeKey
        self.typeName = typeName
        self.value = value
        self.typeCode = typeCode
    }
    public init?(json: [String: Any]) {
        guard let typeKey = json["typeKey"] as? Int else { debugPrint("Expected non-optional property [typeKey] of type [Int] on object [GEBPEventExternalReference] but did not find");return nil; }
        let typeName = json["typeName"] as? String
        guard let value = json["value"] as? String else { debugPrint("Expected non-optional property [value] of type [String] on object [GEBPEventExternalReference] but did not find");return nil; }
        let typeCode = json["typeCode"] as? String
        self.init(typeKey: typeKey, typeName: typeName, value: value, typeCode: typeCode)
    }
}

public struct GEBPAssociatedEvents: CreatableFromJSON {
    let eventId: Int? // The unique ID relating to the Event (system generated).
    
    let eventDateTime: String?
    let eventTypeKey: Int
    let associationTypeCode: String?
    let eventTypeCode: String?
    let eventSourceName: String
    let dateLogged: String? // Date the Event was captured
    let dateTimeAssociated: String? // Date and Time associated with the event
    let associationTypeName: String
    let eventSourceKey: Int?
    let eventTypeName: String?
    let associationTypeKey: Int
    let eventSourceCode: String? // Identifying code for the source of the event. E.g. Vitality Device Platform, Core - Goals subsystem, etc
    
    let categoryCode: String?
    let categoryKey: Int?
    let categoryName: String?
    
    init(eventId: Int?, eventDateTime: String?, eventTypeKey: Int, associationTypeCode: String?, eventTypeCode: String?, eventSourceName: String, dateLogged: String?, dateTimeAssociated: String?, associationTypeName: String, eventSourceKey: Int?, eventTypeName: String?, associationTypeKey: Int, eventSourceCode: String?, categoryCode: String?, categoryKey: Int?, categoryName: String?) {
        self.eventId = eventId
        self.eventDateTime = eventDateTime
        self.eventTypeKey = eventTypeKey
        self.associationTypeCode = associationTypeCode
        self.eventTypeCode = eventTypeCode
        self.eventSourceName = eventSourceName
        self.dateLogged = dateLogged
        self.dateTimeAssociated = dateTimeAssociated
        self.associationTypeName = associationTypeName
        self.eventSourceKey = eventSourceKey
        self.eventTypeName = eventTypeName
        self.associationTypeKey = associationTypeKey
        self.eventSourceCode = eventSourceCode
        self.categoryCode = categoryCode
        self.categoryKey = categoryKey
        self.categoryName = categoryName
    }
    public init?(json: [String: Any]) {
        let eventId = json["eventId"] as? Int
        let eventDateTime = json["eventDateTime"] as? String
        guard let eventTypeKey = json["eventTypeKey"] as? Int else { debugPrint("Expected non-optional property [eventTypeKey] of type [Int] on object [GEBPAssociatedEvents] but did not find");return nil; }
        let associationTypeCode = json["associationTypeCode"] as? String
        let eventTypeCode = json["eventTypeCode"] as? String
        guard let eventSourceName = json["eventSourceName"] as? String else { debugPrint("Expected non-optional property [eventSourceName] of type [String] on object [GEBPAssociatedEvents] but did not find");return nil; }
        let dateLogged = json["dateLogged"] as? String
        let dateTimeAssociated = json["dateTimeAssociated"] as? String
        guard let associationTypeName = json["associationTypeName"] as? String else { debugPrint("Expected non-optional property [associationTypeName] of type [String] on object [GEBPAssociatedEvents] but did not find");return nil; }
        let eventSourceKey = json["eventSourceKey"] as? Int
        let eventTypeName = json["eventTypeName"] as? String
        guard let associationTypeKey = json["associationTypeKey"] as? Int else { debugPrint("Expected non-optional property [associationTypeKey] of type [Int] on object [GEBPAssociatedEvents] but did not find");return nil; }
        let eventSourceCode = json["eventSourceCode"] as? String
        let categoryCode = json["categoryCode"] as? String
        let categoryKey = json["categoryKey"] as? Int
        let categoryName = json["categoryName"] as? String
        self.init(eventId: eventId, eventDateTime: eventDateTime, eventTypeKey: eventTypeKey, associationTypeCode: associationTypeCode, eventTypeCode: eventTypeCode, eventSourceName: eventSourceName, dateLogged: dateLogged, dateTimeAssociated: dateTimeAssociated, associationTypeName: associationTypeName, eventSourceKey: eventSourceKey, eventTypeName: eventTypeName, associationTypeKey: associationTypeKey, eventSourceCode: eventSourceCode, categoryCode: categoryCode, categoryKey: categoryKey, categoryName: categoryName)
    }
}

public struct GEBPEventMetaDataOuts: CreatableFromJSON {
    let typeKey: Int
    let unitOfMeasure: String? // Unit of measure of the metadata.  Not all metadata has a unit of measure
    
    let typeName: String?
    let value: String?
    let typeCode: String?
    init(typeKey: Int, unitOfMeasure: String?, typeName: String?, value: String?, typeCode: String?) {
        self.typeKey = typeKey
        self.unitOfMeasure = unitOfMeasure
        self.typeName = typeName
        self.value = value
        self.typeCode = typeCode
    }
    public init?(json: [String: Any]) {
        guard let typeKey = json["typeKey"] as? Int else { debugPrint("Expected non-optional property [typeKey] of type [Int] on object [GEBPEventMetaDataOuts] but did not find");return nil; }
        let unitOfMeasure = json["unitOfMeasure"] as? String
        let typeName = json["typeName"] as? String
        let value = json["value"] as? String
        let typeCode = json["typeCode"] as? String
        self.init(typeKey: typeKey, unitOfMeasure: unitOfMeasure, typeName: typeName, value: value, typeCode: typeCode)
    }
}
