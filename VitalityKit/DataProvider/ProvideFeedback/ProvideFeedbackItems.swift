//
//  ProvideFeedbackItems.swift
//  VitalityActive
//
//  Created by Von Kervin R. Tuico on 28/03/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import RealmSwift
import VIAUtilities

public class ProvideFeedbackItems: Object {
    @objc public dynamic var selectedFeedbackTypeId: String = ""
    @objc public dynamic var selectedFeedbackType: String = ""
    @objc public dynamic var userFeedback: String = ""
    @objc public dynamic var userEmailAddress: String = ""
    @objc public dynamic var userContactNumber: String = ""
    @objc public dynamic var memberFirstName: String = ""
    @objc public dynamic var memberLastName: String = ""
    
}

extension Realm {
    public func allProvideFeedbackItems() -> Results<ProvideFeedbackItems> {
        return self.objects(ProvideFeedbackItems.self)
    }
}
