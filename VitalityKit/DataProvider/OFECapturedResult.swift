//
//  OFECapturedResult.swift
//  VitalityActive
//
//  Created by Val Tomol on 18/04/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import RealmSwift
import VIAUtilities

public class OFECapturedResult: Object {
    @objc public dynamic var eventType: OFEEventType?
    @objc public dynamic var eventTypeCode: String?
    
    override public class func primaryKey() -> String? {
        return "eventTypeCode"
    }
}

extension Realm {
    public func allOFECapturedResults() -> Results<OFECapturedResult> {
        return self.objects(OFECapturedResult.self)
    }
    
    public func capturedResult(eventType: OFEEventType?,
                               eventTypeCode: String?) {
        let capturedResult = OFECapturedResult()
        try! self.write {
            capturedResult.eventType = eventType
            capturedResult.eventTypeCode = eventTypeCode
            
            self.create(OFECapturedResult.self, value:capturedResult, update: true)
            self.add(capturedResult, update: true)
        }
    }
}
