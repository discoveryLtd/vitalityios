import Foundation
import VIAUtilities
import RealmSwift

public class GMIReportedHealthInformation: Object {
    @objc public dynamic var key = UUID().uuidString
    public let sections: List<GMIReportedSection> = List<GMIReportedSection>()

    override public static func primaryKey() -> String? {
        return "key"
    }

    public static func build(response: GMIGetHealthInformation?) -> GMIReportedHealthInformation {
        let healthInformation = GMIReportedHealthInformation()
        healthInformation.sections.append(objectsIn: GMIReportedSection.build(sectionsP: response?.sections))
        return healthInformation
    }
}

public class GMIReportedSection : Object {
    @objc public dynamic var sortOrder: Int = -1
    @objc public dynamic var typeCode: String? = ""
    @objc public dynamic var typeKey = SectionRef.Unknown
    @objc public dynamic var typeName: String? = ""
    public let healthAttributes: List<GMIReportedHealthAttribute> = List<GMIReportedHealthAttribute>()
    public let sections: List<GMIReportedSection> = List<GMIReportedSection>()

    override public static func primaryKey() -> String? {
        return "typeKey"
    }

    public static func build(sectionP: GMISection) -> GMIReportedSection {
        let section = GMIReportedSection()
        section.sortOrder = sectionP.sortOrder
        section.typeCode = sectionP.typeCode
        if let type = SectionRef(rawValue: sectionP.typeKey) {
            section.typeKey = type
        }
        section.typeName = sectionP.typeName
        section.healthAttributes.append(objectsIn: GMIReportedHealthAttribute.build(attributesP: sectionP.attributes));
        section.sections.append(objectsIn: GMIReportedSection.build(sectionsP: sectionP.sections));

        return section;
    }

    public static func build(sectionsP: [GMISection]?) -> List<GMIReportedSection> {
        let sections = List<GMIReportedSection>()
        if let sectionsT = sectionsP {
            sections.append(objectsIn: sectionsT.map({ (section) in GMIReportedSection.build(sectionP: section )}))
        }

        return sections
    }
}

public class GMIReportedHealthAttribute : Object {
    @objc public dynamic var attributeTypeCode: String? = ""
    @objc public dynamic var attributeTypeKey = PartyAttributeTypeRef.Unknown
    @objc public dynamic var attributeTypeName: String? = ""
    public dynamic var event: GMIReportedEvent? = GMIReportedEvent()
    public let feedbacks = List<GMIReportedFeedback>()
    @objc public dynamic var measuredOn: String? = ""
    public let metaDatas = List<GMIReportedHealthAttributeMetaData>()
    public let recommendations = List<GMIReportedRecommendation>()
    @objc public dynamic var sortOrder: Int = -1
    @objc public dynamic var sourceEventId: Int = -1
    @objc public dynamic var unitOfMeasure: String? = ""
    @objc public dynamic var friendlyValue: String? = ""

    @objc public dynamic var value: String? = ""

    override public static func primaryKey() -> String? {
        return "attributeTypeKey"
    }

    public func feedbackWith(keys: [PartyAttributeFeedbackRef]) -> GMIReportedFeedback? {
        return self.feedbacks.first { (feedback) -> Bool in return keys.contains(feedback.feedbackTypeKey)}
    }
    
    public func mhFeedback() -> GMIReportedFeedback? {
        return (self.feedbacks.count > 1) ? self.feedbacks.last : nil
    }
    
    public func metadataWith(keys: [PartyAttributeMetaDataTypeRef]) -> GMIReportedHealthAttributeMetaData? {
        return self.metaDatas.first(where: { (metadata) -> Bool in return keys.contains(metadata.typeKey) })
    }

    public func outdated() -> Bool {
        let outdatedFeedback = self.feedbackWith(keys: [.VAgeOutdated])
        return outdatedFeedback != nil
    }

    public static func build(attributeP: GMIAttribute) -> GMIReportedHealthAttribute {
        let attribute = GMIReportedHealthAttribute()
        attribute.attributeTypeCode = attributeP.attributeTypeCode
        if let type = PartyAttributeTypeRef(rawValue: attributeP.attributeTypeKey) {
            attribute.attributeTypeKey = type
        }
        attribute.attributeTypeName = attributeP.attributeTypeName
        attribute.feedbacks.append(objectsIn: GMIReportedFeedback.build(feedbacksP: attributeP.healthAttributeFeedbacks))
        attribute.measuredOn = attributeP.measuredOn
        attribute.metaDatas.append(objectsIn: GMIReportedHealthAttributeMetaData.build(metaDatasP: attributeP.healthAttributeMetadatas))
        attribute.recommendations.append(objectsIn: GMIReportedRecommendation.build(recommendationsP: attributeP.recommendations))
        attribute.sortOrder = attributeP.sortOrder
        attribute.friendlyValue = attributeP.friendlyValue?.trimmingCharacters(in: .whitespacesAndNewlines)
        if let sourceEventId = attributeP.sourceEventId {
            attribute.sourceEventId = sourceEventId
        }
        attribute.unitOfMeasure = attributeP.unitofMeasure
        attribute.value = attributeP.value
        if let event = attributeP.event {
            attribute.event = GMIReportedEvent.build(eventP: event)
        }

        return attribute
    }

    public static func build(attributesP: [GMIAttribute]?) -> List<GMIReportedHealthAttribute> {
        let attributes = List<GMIReportedHealthAttribute>()
        if let attributesT = attributesP {
            attributes.append(objectsIn: attributesT.map({(attribute) in GMIReportedHealthAttribute.build(attributeP: attribute)}))
        }

        return attributes
    }
    
    public var displayValue: String? {
        get {            
            if let md = self.metaDatas.first(where: { (md) in md.typeKey == PartyAttributeMetaDataTypeRef.SourceValue}) {
                if let friendlyValue = md.friendlyValue, !String.isNilOrEmpty(string: friendlyValue) {
                    return friendlyValue
                }
            }

            if let friendlyValue = self.friendlyValue, !String.isNilOrEmpty(string: friendlyValue) {
                return friendlyValue
            }
            
            return self.value
        }
    }
}

public class GMIReportedEvent : Object {
    @objc public dynamic var applicableTo: Int = -1
    @objc public dynamic var dateLogged: String = ""
    @objc public dynamic var effectiveDateTime: String? = ""
    @objc public dynamic var eventId: Int = -1
    @objc public dynamic var eventSourceTypeCode: String? = ""
    @objc public dynamic var eventSourceTypeKey = EventSourceRef.Unknown
    @objc public dynamic var eventSourceTypeName: String? = ""
    @objc public dynamic var reportedBy: Int = -1
    @objc public dynamic var typeCode: String = ""
    @objc public dynamic var typeKey = EventTypeRef.Unknown
    @objc public dynamic var typeName: String = ""

    override public static func primaryKey() -> String? {
        return "eventId"
    }

    public static func build(eventP: GMIEvent) -> GMIReportedEvent {
        let event = GMIReportedEvent()
        event.applicableTo = eventP.applicableTo
        event.dateLogged = eventP.dateLogged
        event.effectiveDateTime = eventP.effectiveDateTime
        event.eventId = eventP.eventId
        event.eventSourceTypeCode = eventP.eventSourceTypeCode
        if let eventSource = EventSourceRef(rawValue: eventP.eventSourceTypeKey) {
            event.eventSourceTypeKey = eventSource
        }
        event.eventSourceTypeName = eventP.eventSourceTypeName
        if let reportedBy = eventP.reportedBy {
            event.reportedBy = reportedBy
        }
        event.typeCode = eventP.typeCode
        if let eventType = EventTypeRef(rawValue: eventP.typeKey) {
            event.typeKey = eventType
        }
        event.typeName = eventP.typeName

        return event
    }

    public static func build(eventsP: [GMIEvent]?) -> List<GMIReportedEvent> {
        let events = List<GMIReportedEvent>()
        if let eventsT = eventsP {
            events.append(objectsIn: eventsT.map({(event) in GMIReportedEvent.build(eventP: event)}))
        }

        return events
    }
}

public class GMIReportedFeedback: Object {
    public let feedbackTips = List<GMIReportedFeedbackTip>()
    @objc public dynamic var feedbackTypeCode: String? = ""
    @objc public dynamic var feedbackTypeKey = PartyAttributeFeedbackRef.Unknown
    @objc public dynamic var feedbackTypeName: String = ""
    @objc public dynamic var feedbackTypeTypeCode: String? = ""
    @objc public dynamic var feedbackTypeTypeKey: Int = -1
    @objc public dynamic var feedbackTypeTypeName: String = ""
    @objc public dynamic var whyIsThisImportant: String? = ""

    override public static func primaryKey() -> String? {
        return "feedbackTypeKey"
    }

    public static func build(feedbackP: GMIHealthAttributeFeedback) -> GMIReportedFeedback {
        let feedback = GMIReportedFeedback()
        feedback.feedbackTips.append(objectsIn: GMIReportedFeedbackTip.build(tipsP: feedbackP.feedbackTips))
        feedback.feedbackTypeCode = feedbackP.feedbackTypeCode
        if let typeKey = PartyAttributeFeedbackRef(rawValue: feedbackP.feedbackTypeKey) {
            feedback.feedbackTypeKey = typeKey
        }
        feedback.feedbackTypeName = feedbackP.feedbackTypeName
        feedback.feedbackTypeTypeCode = feedbackP.feedbackTypeTypeCode
        feedback.feedbackTypeTypeKey = feedbackP.feedbackTypeTypeKey
        feedback.feedbackTypeTypeName = feedbackP.feedbackTypeTypeName
        feedback.whyIsThisImportant = feedbackP.whyIsThisImportant

        return feedback
    }

    public static func build(feedbacksP: [GMIHealthAttributeFeedback]?) -> List<GMIReportedFeedback> {
        let feedbacks = List<GMIReportedFeedback>()
        if let feedbacksT = feedbacksP {
            feedbacks.append(objectsIn: feedbacksT.map({(feedback) in GMIReportedFeedback.build(feedbackP: feedback)}))
        }

        return feedbacks
    }
}

public class GMIReportedFeedbackTip: Object {
    @objc public dynamic var key = UUID().uuidString
    @objc public dynamic var note: String = ""
    @objc dynamic var sortOrder: Int = -1
    @objc public dynamic var typeCode: String? = ""
    @objc public dynamic var typeKey = PartyAttributeFeedbackTipRef.Unknown
    @objc public dynamic var typeName: String? = ""

    override public static func primaryKey() -> String? {
        return "key"
    }

    public static func build(tipP: GMIFeedbackTip) -> GMIReportedFeedbackTip {
        let tip = GMIReportedFeedbackTip()
        tip.note = tipP.note
        if let sortOrder = tipP.sortOrder {
            tip.sortOrder = sortOrder
        }
        tip.typeCode = tipP.typeCode
        if let typeKey = PartyAttributeFeedbackTipRef(rawValue: tipP.typeKey) {
            tip.typeKey = typeKey
        }
        tip.typeName = tipP.typeName

        return tip
    }

    public static func build(tipsP: [GMIFeedbackTip]?) -> List<GMIReportedFeedbackTip> {
        let tips = List<GMIReportedFeedbackTip>()
        if let tipsT = tipsP {
            tips.append(objectsIn: tipsT.map({(tip) in GMIReportedFeedbackTip.build(tipP: tip)}))
        }

        return tips
    }
}

public class GMIReportedRecommendation: Object {
    @objc public dynamic var fromValue: String? = ""
    @objc public dynamic var key = UUID().uuidString
    @objc public dynamic var toValue: String? = ""
    @objc public dynamic var unitOfMeasureId: Int = -1
    @objc public dynamic var value: String? = ""
    @objc public dynamic var friendlyValue: String? = ""

    override public static func primaryKey() -> String? {
        return "key"
    }

    public static func build(recommendationP: GMIRecommendation) -> GMIReportedRecommendation {
        let recommendation = GMIReportedRecommendation()
        recommendation.fromValue = recommendationP.fromValue
        recommendation.toValue = recommendationP.toValue
        if let unitOfMeasureId = recommendationP.unitOfMeasureId {
            recommendation.unitOfMeasureId = unitOfMeasureId
        }
        recommendation.value = recommendationP.value
        recommendation.friendlyValue = recommendationP.friendlyValue

        return recommendation
    }

    public static func build(recommendationsP: [GMIRecommendation]?) -> List<GMIReportedRecommendation> {
        let recommendations = List<GMIReportedRecommendation>()

        if let recommendationsT = recommendationsP {
            recommendations.append(objectsIn: recommendationsT.map({(recommendation) in GMIReportedRecommendation.build(recommendationP: recommendation)}))
        }

        return recommendations
    }

    public static func == (lhs: GMIReportedRecommendation, rhs: GMIReportedRecommendation) -> Bool {
        return false
    }
}

public class GMIReportedHealthAttributeMetaData: Object {
    @objc public dynamic var friendlyValue: String? = ""
    @objc public dynamic var key = UUID().uuidString
    @objc public dynamic var typeCode: String = ""
    @objc public dynamic var typeKey: PartyAttributeMetaDataTypeRef = PartyAttributeMetaDataTypeRef.Unknown
    @objc public dynamic var typeName: String = ""
    @objc public dynamic var unitOfMeasureId: Int = -1
    @objc public dynamic var value: String? = ""

    override public static func primaryKey() -> String? {
        return "key"
    }

    public static func build(metaDataP: GMIHealthAttributeMetadata) -> GMIReportedHealthAttributeMetaData {
        let hAMD = GMIReportedHealthAttributeMetaData()
        hAMD.friendlyValue = metaDataP.friendlyValue
        if let typeCode = metaDataP.typeCode {
            hAMD.typeCode = typeCode
        }
        if let typeKey = PartyAttributeMetaDataTypeRef(rawValue: metaDataP.typeKey) {
            hAMD.typeKey = typeKey
        }
        if let typeName = metaDataP.typeName {
            hAMD.typeName = typeName
        }
        if let unitOfMeasureId = metaDataP.measurementUnitId {
            hAMD.unitOfMeasureId = unitOfMeasureId
        }
        hAMD.value = metaDataP.value

        return hAMD
    }

    public static func build(metaDatasP: [GMIHealthAttributeMetadata]?) -> List<GMIReportedHealthAttributeMetaData> {
        let metaDatas = List<GMIReportedHealthAttributeMetaData>()

        if let metaDatasT = metaDatasP {
            metaDatas.append(objectsIn: metaDatasT.map({(metaData) in GMIReportedHealthAttributeMetaData.build(metaDataP: metaData)}))
        }

        return metaDatas
    }
}
