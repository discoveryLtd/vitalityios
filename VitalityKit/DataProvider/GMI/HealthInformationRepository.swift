import Foundation
import VIAUtilities
import RealmSwift

public class HealthInformationRepository: Object {
    internal var repo = DataProvider.newHealthInformationRealm()

    public func getHealthInformation() -> GMIReportedHealthInformation? {
        return self.repo.objects(GMIReportedHealthInformation.self).first
    }

    public func save(healthInformationP: GMIReportedHealthInformation) -> GMIReportedHealthInformation? {
        self.scrubTheRealm()

        try! self.repo.write {
            self.saveSections(sectionsP: healthInformationP.sections)
            self.repo.add(healthInformationP, update: true)
        }

        return self.repo.objects(GMIReportedHealthInformation.self).first
    }

    internal func scrubTheRealm() {
        
        try! self.repo.write {
            self.repo.delete(self.repo.objects(GMIReportedEvent.self))
            self.repo.delete(self.repo.objects(GMIReportedFeedback.self))
            self.repo.delete(self.repo.objects(GMIReportedFeedbackTip.self))
            self.repo.delete(self.repo.objects(GMIReportedHealthAttribute.self))
            self.repo.delete(self.repo.objects(GMIReportedHealthAttributeMetaData.self))
            self.repo.delete(self.repo.objects(GMIReportedHealthInformation.self))
            self.repo.delete(self.repo.objects(GMIReportedRecommendation.self))
            self.repo.delete(self.repo.objects(GMIReportedSection.self))
        }
    }

    internal func saveSections(sectionsP: List<GMIReportedSection>) {
        sectionsP.forEach({(section) in self.saveSection(sectionP: section)})
    }

    internal func saveSection(sectionP: GMIReportedSection) {
        self.saveHealthAttributes(healthAttributesP: sectionP.healthAttributes)
        self.repo.add(sectionP, update: true)
    }

    internal func saveHealthAttributes(healthAttributesP: List<GMIReportedHealthAttribute>) {
        healthAttributesP.forEach({(healthAttribute) in self.saveHealthAttribute(healthAttributeP: healthAttribute)})
    }

    internal func saveHealthAttribute(healthAttributeP: GMIReportedHealthAttribute) {
        self.saveEvent(eventP: healthAttributeP.event)
        self.saveFeedbacks(feedbacksP: healthAttributeP.feedbacks)
        self.saveHealthAttributeMetaDatas(metaDatasP: healthAttributeP.metaDatas)
        self.saveRecommendations(recommendationsP: healthAttributeP.recommendations)
        self.repo.add(healthAttributeP, update: true)
    }

    internal func saveHealthAttributeMetaDatas(metaDatasP: List<GMIReportedHealthAttributeMetaData>) {
        metaDatasP.forEach({(metaData) in self.saveHealthAttributeMetaData(metaDataP: metaData)})
    }

    internal func saveHealthAttributeMetaData(metaDataP: GMIReportedHealthAttributeMetaData) {
        self.repo.add(metaDataP, update: true)
    }

    internal func saveEvent(eventP: GMIReportedEvent?) {
        if let event = eventP {
            self.repo.add(event, update: true)
        }
    }

    internal func saveFeedbacks(feedbacksP: List<GMIReportedFeedback>) {
        feedbacksP.forEach({(feedback) in self.saveFeedback(feedbackP: feedback)})
    }

    internal func saveFeedback(feedbackP: GMIReportedFeedback) {
        self.saveFeedbackTips(feedbackTipsP: feedbackP.feedbackTips)
        self.repo.add(feedbackP, update: true)
    }

    internal func saveFeedbackTips(feedbackTipsP: List<GMIReportedFeedbackTip>) {
        feedbackTipsP.forEach({(feedbackTip) in self.saveFeedbackTip(feedbackTipP: feedbackTip)})
    }

    internal func saveFeedbackTip(feedbackTipP: GMIReportedFeedbackTip) {
        self.repo.add(feedbackTipP, update: true)
    }

    internal func saveRecommendations(recommendationsP: List<GMIReportedRecommendation>) {
        recommendationsP.forEach({(recommendation) in self.saveRecommendation(recommendationP: recommendation)})
    }

    internal func saveRecommendation(recommendationP: GMIReportedRecommendation) {
        self.repo.add(recommendationP, update: true)
    }

    public func getAllHealthAttributes() -> List<GMIReportedHealthAttribute> {
        let healthAttributes = List<GMIReportedHealthAttribute>()
        healthAttributes.append(objectsIn: self.repo.objects(GMIReportedHealthAttribute.self)
            .filter({(ha) -> Bool in
                if let value = ha.value {
                    return value != ""
                }
                return false
              }))
        
        return healthAttributes
    }

    public func hasValidVitalityAge() -> Bool {
        if let vitalityAge = self.getHealthAttributeBy(attributeType: PartyAttributeTypeRef.VitalityAge)?.value {
             return Double(vitalityAge) != nil
        }

        return false
    }

    public func getHealthAttributeBy(attributeType: PartyAttributeTypeRef) -> GMIReportedHealthAttribute? {
        return self.getAllHealthAttributes().first(where: { (ha) in ha.attributeTypeKey == attributeType })
    }

    public func getSectionBy(sectionKey: SectionRef) -> GMIReportedSection? {
        return repo.objects(GMIReportedSection.self).first(where: { (section) in return section.typeKey == sectionKey})
    }
}
