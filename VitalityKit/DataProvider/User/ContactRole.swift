//
//  ContactRole.swift
//  VitalityActive
//
//  Created by Wilmar van Heerden on 2016/12/05.
//  Copyright © 2016 Glucode. All rights reserved.
//

import Foundation
import RealmSwift

public class ContactRole: Object {
    @objc public dynamic var availabilityFrom: Date?
    @objc public dynamic var availabilityTo: Date?
    @objc public dynamic var rolePurposeType: String = ""
    @objc public dynamic var roleType: String = ""
}
