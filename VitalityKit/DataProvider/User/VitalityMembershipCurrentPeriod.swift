//
//  VitalityMembershipCurrentPeriod.swift
//  VitalityActive
//
//  Created by Lionel Camacho on 2017/04/26.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import RealmSwift

public class VitalityMembershipCurrentPeriod: Object {
    @objc public dynamic var effectiveTo: Date?
    @objc public dynamic var effectiveFrom: Date?
}

extension DataProvider {
    public class func currentMembershipPeriodStartDate() -> Date? {
        let realm = DataProvider.newRealm()
        return realm.objects(VitalityMembershipCurrentPeriod.self).first?.effectiveFrom
    }

    public class func currentMembershipPeriodEndDate() -> Date? {
        let realm = DataProvider.newRealm()
        return realm.objects(VitalityMembershipCurrentPeriod.self).first?.effectiveTo
    }

}
