//
//  GeneralPreference.swift
//  VitalityActive
//
//  Created by Wilmar van Heerden on 2016/12/05.
//  Copyright © 2016 Glucode. All rights reserved.
//

import Foundation
import RealmSwift

public class PartyGeneralPreference: Object {
    @objc public dynamic var effectiveFrom: Date?
    @objc public dynamic var effectiveTo: Date?
    @objc public dynamic var type: Int = 0
    @objc public dynamic var value: String = ""
}

extension Realm {
    public func allPartyGeneralPreference() -> Results<PartyGeneralPreference> {
        return self.objects(PartyGeneralPreference.self)
    }

    public func partyGeneralPreference(with type: Int) -> PartyGeneralPreference? {
        let partyGeneralPreferences = self.objects(PartyGeneralPreference.self)
        let filteredPartyGeneralPreferences = partyGeneralPreferences.filter("type == \(type)")
        return filteredPartyGeneralPreferences.first
    }
}
