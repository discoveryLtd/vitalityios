//
//  Reference.swift
//  VitalityActive
//
//  Created by Wilmar van Heerden on 2016/12/05.
//  Copyright © 2016 Glucode. All rights reserved.
//

import Foundation
import RealmSwift
import VIAUtilities

public class PartyReference: Object {
    @objc public dynamic var effectiveFrom: Date?
    @objc public dynamic var effectiveTo: Date?
    @objc public dynamic var comments: String = ""
    @objc public dynamic var issuedBy: String = ""
    @objc public dynamic var type: String = ""
    @objc public dynamic var value: String = ""
}

extension Realm {
    public func allPartyReferences() -> Results<PartyReference> {
        return self.objects(PartyReference.self)
    }
}
