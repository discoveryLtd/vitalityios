//
//  EmailAddress.swift
//  VitalityActive
//
//  Created by Wilmar van Heerden on 2016/12/05.
//  Copyright © 2016 Glucode. All rights reserved.
//

import Foundation
import RealmSwift

public class PartyEmailAddress: Object {
    @objc public dynamic var value: String = ""
    public let contactRoles = List<PartyContactRole>()
}

extension Realm {
    public func  allPartyEmailAddresses() -> Results<PartyEmailAddress> {
        return self.objects(PartyEmailAddress.self)
    }
    
    public func currentEmailAddress() -> PartyEmailAddress?{
        if let party = self.objects(VitalityParty.self).first{
            for emailAddress in party.emailAddresses{
                for contactRole in emailAddress.contactRoles{
                    if let effectiveTo = contactRole.effectiveTo{
                        debugPrint("Year: \(effectiveTo.year)")
                        if effectiveTo.year == 9999{
                            return emailAddress
                        }
                    }
                }
            }
        }
        return nil
    }
}
