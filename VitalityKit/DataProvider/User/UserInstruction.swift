import Foundation
import RealmSwift
import VIAUtilities

public class UserInstruction: Object {
    public static let defaultUserInstructionValue = -999

    @objc public dynamic var typeKey: Int = UserInstruction.defaultUserInstructionValue //TODO: make enum
    @objc public dynamic var id: Int = UserInstruction.defaultUserInstructionValue
}

extension DataProvider {

    public class func currentUserInstructionForLoginTermsAndConditions() -> UserInstruction? {
        let realm = DataProvider.newRealm()
        let result = realm.objects(UserInstruction.self).filter("typeKey == \(ProcessInstructionTypeRef.LOGIN_T_AND_C.rawValue)").first
        return result
    }

    public class func deleteCurrentUserInstructionForLoginTermsAndConditions() {
        let realm = DataProvider.newRealm()
        if let result = realm.objects(UserInstruction.self).filter("typeKey == \(ProcessInstructionTypeRef.LOGIN_T_AND_C.rawValue)").first {
            try! realm.write {
                realm.delete(result)
                debugPrint("Deleted user instruction for LoginTermsAndConditions")
            }
        } else {
            debugPrint("No user instruction for terms and conditions to delete")
        }
    }
    
    public class func currentUserInstructionForShowNextStatusAchieved() -> UserInstruction? {
        let realm = DataProvider.newRealm()
        let result = realm.objects(UserInstruction.self).filter("typeKey == \(ProcessInstructionTypeRef.NEXT_STATUS_ACHIEVED.rawValue)").first
        return result
    }
    
    public class func deleteCurrentUserInstructionForShowNextStatusAchieved() {
        let realm = DataProvider.newRealm()
        if let result = realm.objects(UserInstruction.self).filter("typeKey == \(ProcessInstructionTypeRef.NEXT_STATUS_ACHIEVED.rawValue)").first {
            try! realm.write {
                realm.delete(result)
                debugPrint("Deleted user instruction for NextStatusAchieved")
            }
        } else {
            debugPrint("No user instruction for Next Status Achieved to delete")
        }
    }
    
    public class func currentUserInstructionForShowCarryOverStatus() -> UserInstruction? {
        let realm = DataProvider.newRealm()
        let result = realm.objects(UserInstruction.self).filter("typeKey == \(ProcessInstructionTypeRef.SHOW_CARRY_OVER_STATUS.rawValue)").first
        return result
    }
    
    public class func deleteCurrentUserInstructionForShowCarryOverStatus() {
        let realm = DataProvider.newRealm()
        if let result = realm.objects(UserInstruction.self).filter("typeKey == \(ProcessInstructionTypeRef.SHOW_CARRY_OVER_STATUS.rawValue)").first {
            try! realm.write {
                realm.delete(result)
                debugPrint("Deleted user instruction for LoginTermsAndConditions")
            }
        } else {
            debugPrint("No user instruction for terms and conditions to delete")
        }
    }

}

extension Realm {
    public func userInstruction(with typeKey: Int) -> UserInstruction? {
        if let userInstruction = objects(UserInstruction.self).filter("typeKey == %@", typeKey).first {
            return userInstruction
        }
        return nil
    }
}
