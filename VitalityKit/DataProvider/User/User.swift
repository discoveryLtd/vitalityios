//
//  User.swift
//  VitalityActive
//
//  Created by Wilmar van Heerden on 2016/11/17.
//  Copyright © 2016 Glucode. All rights reserved.
//

import Foundation
import RealmSwift

public class User: Object {
//    public static let demoPartyId: Int = 100001
//    public static let vitalityMembershipId: Int = 100000

    //@objc public dynamic var partyId: Int = 0
    @objc public dynamic var username: String = ""
//    @objc public dynamic var middleNames: String = ""
//    @objc public dynamic var gender: String = ""
//    @objc public dynamic var dateOfBirth: Date?
//    @objc public dynamic var givenName: String = ""
//    @objc public dynamic var familyName: String = ""
//    @objc public dynamic var language: String = ""
//    @objc public dynamic var preferredName: String = ""
//    @objc public dynamic var title: String = ""
//    @objc public dynamic var suffix: String = ""

    override public static func primaryKey() -> String? {
        return "username"
    }
}
