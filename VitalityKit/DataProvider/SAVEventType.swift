//
//  SAVEventType.swift
//  VitalityActive
//
//  Created by Michelle Oratil on 30/11/2017.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import RealmSwift
import VIAUtilities

public class SAVEventType: Object {
    @objc public dynamic var totalEarnedPoints: Int = 0
    @objc public dynamic var totalPotentialPoints: Int = 0
    @objc public dynamic var typeName: String = ""
    @objc public dynamic var type: EventTypeRef = EventTypeRef.Unknown
    @objc public dynamic var reasonCode: String?
    @objc public dynamic var reasonName: String?
    @objc public dynamic var categoryCode: String?
    @objc public dynamic var categoryType: EventCategoryRef = EventCategoryRef.Unknown
    public let reasonKey = RealmOptional<Int>()
    
    public let events = List<SAVEvent>()
    
    override public static func primaryKey() -> String? {
        return "type"
    }
}

extension Realm {
    public func allSAVPotentialPointsEventTypes() -> Results<SAVEventType> {
        return self.objects(SAVEventType.self)
    }
    
    public func SAV_eventType(for eventTypeRef: EventTypeRef) -> SAVEventType? {
        let eventTypes = self.objects(SAVEventType.self)
        let filtered = eventTypes.filter("type == %@", eventTypeRef.rawValue)
        return filtered.first
    }
    
    public func SAV_pointsReason(for eventTypeRef: EventTypeRef) -> String? {
        guard let eventType = SAV_eventType(for: eventTypeRef) else { return nil }
        return eventType.reasonName
    }
    
    public func SAV_pointsReasonKey(for eventTypeRef: EventTypeRef) -> Int? {
        guard let eventType = SAV_eventType(for: eventTypeRef) else { return nil }
        return eventType.reasonKey.value
    }
    
    public func SAV_potentialPoints(for eventTypeRef: EventTypeRef) -> Int? {
        guard let eventType = SAV_eventType(for: eventTypeRef) else { return nil }
        return eventType.totalPotentialPoints
    }
    
    public func SAV_earnedPoints(for eventTypeRef: EventTypeRef) -> Int? {
        guard let eventType = SAV_eventType(for: eventTypeRef) else { return nil }
        return eventType.totalEarnedPoints
    }
    
}
