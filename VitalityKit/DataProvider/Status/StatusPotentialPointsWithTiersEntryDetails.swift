//
//  StatusPotentialPointsWithTiersEventType.swift
//  VitalityActive
//
//  Created by admin on 2017/10/18.
//  Copyright © 2017 Glucode. All rights reserved.
//

import RealmSwift

public class StatusPotentialPointsWithTiersEntryDetails: Object {
    public let potentialPoints: RealmOptional<Int> = RealmOptional<Int>()
    public let conditions: List<StatusPotentialPointsWithTiersConditions> = List<StatusPotentialPointsWithTiersConditions>()
}
