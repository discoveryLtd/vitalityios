import RealmSwift

public class StatusPointsEntry: Object {
    public dynamic var categoryCode: String? = ""
    public dynamic var categoryKey: Int = -1
    public var categoryLimit: RealmOptional<Int> = RealmOptional<Int>()
    public dynamic var categoryName: String? = ""
    public dynamic var frequencyLimit: Int = -1
    public dynamic var maximumMembershipPeriodPoints: Int = -1
    public dynamic var pointsEarned: Int = -1
    public var pointsEntryLimit: RealmOptional<Int> = RealmOptional<Int>()
    public dynamic var potentialPoints: Int = -1
    public var potentialPointsEntries: List<StatusPotentialPointsWithTiersEntryDetails> = List<StatusPotentialPointsWithTiersEntryDetails>()
    public dynamic var typeCode: String? = ""
    public dynamic var typeKey: Int = -1
    public dynamic var typeName: String? = ""
}

extension Realm {
    func allStatusPointsEntry() -> Results<StatusPointsEntry> {
        return self.objects(StatusPointsEntry.self)
    }
    
    func statusPointsEntry(with key: Int) -> Results<StatusPointsEntry> {
        return allStatusPointsEntry().filter("typeKey == %@", key)
    }
}
