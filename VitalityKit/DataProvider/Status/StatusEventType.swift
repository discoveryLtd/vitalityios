import RealmSwift

public class StatusEventType: Object {
    public dynamic var code: String? = ""
    public dynamic var key: Int = -1
    public dynamic var name: String? = ""
}
