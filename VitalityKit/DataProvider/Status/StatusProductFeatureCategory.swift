import RealmSwift

public class StatusProductFeatureCategory: Object {
    @objc public dynamic var code: String? = ""
    @objc public dynamic var key: Int = Int()
    @objc public dynamic var name: String? = ""
    public var pointsCategoryLimit: RealmOptional<Int> = RealmOptional<Int>()
    @objc public dynamic var pointsEarned: Int = Int()
    @objc public dynamic var pointsEarningFlag: String = ""
    public var productFeatureAndPointsInformations: List<StatusProductFeaturePointsInformation> = List<StatusProductFeaturePointsInformation>()
    @objc public dynamic var typeCode: String? = ""
    @objc public dynamic var typeKey: Int = Int()
    @objc public dynamic var typeName: String? = ""
    @objc public dynamic var potentialPoints = Int()
}

public extension Realm {
    public func productFeatureCategories() -> Results<StatusProductFeatureCategory> {
        return self.objects(StatusProductFeatureCategory)
    }
    
    public func productFeatureCategories(with key: Int) -> Results<StatusProductFeatureCategory> {
        return self.objects(StatusProductFeatureCategory).filter("key == %@", key)
    }
}
