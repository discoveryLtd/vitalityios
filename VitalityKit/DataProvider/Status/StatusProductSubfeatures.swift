import RealmSwift

public class StatusProductSubfeatures: Object {
    public dynamic var code: String? = ""
    public dynamic var key: Int = Int()
    public dynamic var name: String? = ""
    public dynamic var pointsEarned: Int = -1
    public dynamic var pointsEarningFlag: String = ""
    public dynamic var potentialPoints: Int = -1
    public var productSubfeatureEventTypes: List<StatusEventType> = List<StatusEventType>()
    public var productSubfeaturePointsEntries: List<StatusPointsEntry> = List<StatusPointsEntry>()
}

public extension Realm {
    public func allStatusProductSubFeatures(for key: Int) -> Results<StatusProductSubfeatures> {
        return self.objects(StatusProductSubfeatures.self).filter("key == %@", key)
    }
}
