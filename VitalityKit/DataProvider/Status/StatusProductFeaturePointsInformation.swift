import RealmSwift

public class StatusProductFeaturePointsInformation: Object {
    @objc public dynamic var code: String? = ""
    public var eventTypes: List<StatusEventType> = List<StatusEventType>()
    @objc public dynamic var key: Int = -1
    @objc public dynamic var name: String? = ""
    @objc public dynamic var pointsEarned: Int = -1
    @objc public dynamic var pointsEarningFlag: String = ""
    public var pointsEntries:List<StatusPointsEntry> = List<StatusPointsEntry>()
    @objc public dynamic var potentialPoints: Int = Int()
    public var productSubFeatures: List<StatusProductSubfeatures> = List<StatusProductSubfeatures>()
}

public extension Realm {
    public func productFeatureCategoriesPointsInformation(with key: Int) -> Results<StatusProductFeaturePointsInformation> {
        return self.objects(StatusProductFeaturePointsInformation).filter("key == %@", key)
    }
}
