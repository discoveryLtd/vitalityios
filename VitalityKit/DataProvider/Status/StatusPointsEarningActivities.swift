import RealmSwift

public class StatusPointsEarningActivities: Object {
    @objc public dynamic var name: String = ""
    public var pointsPotential: RealmOptional<Int> = RealmOptional<Int>()
    @objc public var pointsPotentialType: Int = Int()
    @objc public var eventKey: Int = Int()
}

public extension Realm {
    public func allStatuSPointEarningActivities(with eventKey: Int) -> Results<StatusPointsEarningActivities> {
        return self.objects(StatusPointsEarningActivities.self)
    }
}
