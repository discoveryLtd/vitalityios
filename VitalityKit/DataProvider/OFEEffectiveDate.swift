//
//  OFEEffectiveDate.swift
//  VitalityActive
//
//  Created by Harry C. Lingad on 4/23/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import RealmSwift
import VIAUtilities

public class OFEEffectiveDate: Object {
    
    @objc public dynamic var effectiveDate: String = ""
    public let periodOffset =  List<OFEPeriodOffset>()

}

extension Realm {
    public func allEffectiveDate() -> Results<OFEEffectiveDate> {
        return self.objects(OFEEffectiveDate.self)
    }
}
