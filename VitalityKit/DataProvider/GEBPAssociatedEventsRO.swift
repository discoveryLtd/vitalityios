//
//  GEBPAssociatedEventsRO.swift
//  VitalityActive
//
//  Created by OJ Garde on 17/11/2017.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import RealmSwift
import VIAUtilities

public class GEBPAssociatedEventsRO: Object {
    @objc public dynamic var dateLogged:Date? // Date the Event was captured
    @objc public dynamic var dateTimeAssociated:Date? // Date and Time associated with the event
    
    @objc public dynamic var eventId: Int = 0 // The unique ID relating to the Event (system generated).
    @objc public dynamic var eventDateTime:Date?
    
    @objc public dynamic var eventTypeKey: EventTypeRef = .Unknown
    @objc public dynamic var eventTypeName = ""
    @objc public dynamic var eventTypeCode = ""
    
    @objc public dynamic var eventSourceKey: EventSourceRef = .Unknown
    @objc public dynamic var eventSourceName = ""
    @objc public dynamic var eventSourceCode = "" // Identifying code for the source of the event. E.g. Vitality Device Platform, Core - Goals subsystem, etc
    
    @objc public dynamic var associationTypeKey: EventAssociationTypeRef = .Unknown
    @objc public dynamic var associationTypeCode = ""
    @objc public dynamic var associationTypeName = ""
}
extension Realm {
    public func allEventsAssociatedReference() -> Results<GEBPAssociatedEventsRO> {
        return self.objects(GEBPAssociatedEventsRO.self)
    }
}
