import UIKit
import RealmSwift

public class WDAAssets: Object {
    @objc public dynamic var partnerLogoUrl: String = ""
    @objc public dynamic var partnerDescription: String = ""
    @objc public dynamic var partnerWebsiteUrl: String?
    @objc public dynamic var aboutPartnerContentId: String?
    @objc public dynamic var stepsToLinkContentId: String?
}

extension Realm {
    public func allWDAAssets() -> Results<WDAAssets> {
        return self.objects(WDAAssets.self)
    }
}
