//
//  VNAValidValue.swift
//  VitalityActive
//
//  Created by Von Kervin R. Tuico on 09/02/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import RealmSwift
import VIAUtilities

public class VNAValidValue: Object {
    @objc public dynamic var unitOfMeasure: UnitOfMeasureRef = UnitOfMeasureRef.Unknown
    @objc public dynamic var name: String = ""
    @objc public dynamic var value: String = ""
    @objc public dynamic var valueDescription: String = ""
    @objc public dynamic var valueNote: String = ""
    @objc public dynamic var type: QuestionValidValueTypeRef = QuestionValidValueTypeRef.Unknown
    @objc public dynamic var typeName: String?
    @objc public dynamic var typeCode: String?
}

extension Realm {
    public func allVNAValidValues() -> Results<VNAValidValue> {
        return self.objects(VNAValidValue.self)
    }
}

