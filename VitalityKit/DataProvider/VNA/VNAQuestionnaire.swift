//
//  VNAQuestionnaire.swift
//  VitalityActive
//
//  Created by Von Kervin R. Tuico on 09/02/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import UIKit
import RealmSwift
import VIAUtilities

@objc public enum VNACompletionState: Int, EnumCollection {
    case Unknown = -1
    case NotStarted = 0
    case InProgress = 1
    case Complete = 2
}

public class VNAQuestionnaire: Object {
    // This is the section/s that a questionnaire may be broken down into. Some questionnaires may not have sections.
    public let questionnaireSections: List<VNAQuestionnaireSection> = List<VNAQuestionnaireSection>()
    @objc public dynamic var typeKey = Int()
    // Date the questionnaire was completed
    @objc public dynamic var completedOn: Date?
    @objc public dynamic var typeName: String?
    @objc public dynamic var completionFlag: Bool = false
    @objc public dynamic var typeCode: String = ""
    @objc public dynamic var currentSectionTypeKey = Int()
    internal dynamic var completionState: VNACompletionState = VNACompletionState.Unknown
    public let sortOrderIndex = RealmOptional<Int>()
    
    @objc public dynamic var text: String = ""
    @objc public dynamic var textDescription: String = ""
    @objc public dynamic var textNote: String = ""
    
    public func getCompletionState() -> VNACompletionState {
        if self.completionState == VNACompletionState.NotStarted {
            if hasUserEnteredCapturedResults() {
                return VNACompletionState.InProgress
            }
        }
        return self.completionState
    }
    
    public func setCompletedAndSubmitted() {
        self.completionFlag = true
        self.updateCompletionState(.Complete)
    }
    
    public func updateCompletionState(_ state: VNACompletionState) {
        self.completionState = state
    }
    
    public func sortedQuestionnaireSections() -> Results<VNAQuestionnaireSection> {
        return self.questionnaireSections.sorted(byKeyPath: "sortOrderIndex", ascending: true)
    }
    
    public func sortedVisibleQuestionnaireSections() -> Results<VNAQuestionnaireSection> {
        let sorted = self.questionnaireSections.sorted(byKeyPath: "sortOrderIndex", ascending: true).filter("isVisible == true")
        return sorted
    }
    
    static func reevaluateVisibilityTagsForAllCapturedResults(in realm: Realm) {
        let capturedResults = realm.allVNACapturedResults()
        capturedResults.forEach({ result in
            if let answer = result.answer {
                VNAQuestionnaire.updateVisibilityTags(for: answer, for: result.questionTypeKey, in: realm)
            } else {
                debugPrint("No answer to use")
            }
        })
    }
    
    public static func updateVisibilityTags(for answer: String, for questionTypeKey: Int,
                                            in realm: Realm = DataProvider.newVNARealm(),
                                            allowsMultipleAnswers: Bool = false) {
        let questionnaireSections = realm.allVNAQuestionnaireSections()
        let questions = realm.allVNAQuestions()
        try! realm.write {
            questionnaireSections.forEach({ $0.updateVisibility(basedOn: questionTypeKey, answer: answer,
                                                                allowsMultipleAnswers: allowsMultipleAnswers) })
            questions.forEach({ $0.updateVisibility(basedOn: questionTypeKey, answer: answer,
                                                    allowsMultipleAnswers: allowsMultipleAnswers) })
            updateAllQuestionsVisibilityBasedOnParentVisibility()
        }
    }
    
    public static func updateAllQuestionsVisibilityBasedOnParentVisibility() {
        let realm = DataProvider.newVNARealm()
        let allQuestionsWithVisibilityTags = realm.allVNAQuestions().filter({ !$0.visibilityTag.isEmpty })
        var allQuestionTypeKeys: [Int] = allQuestionsWithVisibilityTags.map({ $0.typeKey })
        
        while allQuestionTypeKeys.count > 0 {
            guard let questionTypeKey = allQuestionTypeKeys.first else { return }
            guard let question = realm.vnaQuestion(by: questionTypeKey) else { return }
            updateQuestionVisibilityRecursivelyBasedOnParentVisibility(question, keys: &allQuestionTypeKeys)
        }
    }
    
    public static func updateQuestionVisibilityRecursivelyBasedOnParentVisibility(_ question: VNAQuestion, keys: inout [Int]) {
        if let parentQuestionTypeKey = Int(question.visibilityTag.vnaTagComponents()?.first ?? ""),
            let parentQuestion = question.realm?.vnaQuestion(by: parentQuestionTypeKey) {
            // call recursively to check that a parent doesn't exist which has to be toggle first
            updateQuestionVisibilityRecursivelyBasedOnParentVisibility(parentQuestion, keys: &keys)
            
            // update
            let newVisibility = question.isVisible && parentQuestion.isVisible
            if question.isVisible != newVisibility {
//                debugPrint("Changing \(question.typeKey) from isVisible \(question.isVisible ? "✅" : "❌") to \(newVisibility ? "✅" : "❌")")
                question.isVisible = newVisibility
            }
        }
        
        // finally remove the key, whether it was actioned or not
        keys.remove(object: question.typeKey)
    }
    
    public func hasUserEnteredCapturedResults() -> Bool {
        for section in self.questionnaireSections {
            if let capturedResults = self.realm?.vnaCapturedResults(for: section.typeKey) {
                if Array(capturedResults).filter({ $0.prepopulationEventKey.value == nil }).count > 0 {
                    return true
                }
            }
        }
        return false
    }
}

extension Realm {
    public func allVNAQuestionnaires() -> Results<VNAQuestionnaire> {
        return self.objects(VNAQuestionnaire.self)
    }
    
    public func allSortedVNAQuestionnaires() -> Results<VNAQuestionnaire> {
        return self.objects(VNAQuestionnaire.self).sorted(byKeyPath: "sortOrderIndex", ascending: true)
    }
    
    public func getVNAQuestionnaire(using selectedQuestionnaireTypeKey: Int) -> VNAQuestionnaire? {
        let result = self.objects(VNAQuestionnaire.self).filter { $0.typeKey == selectedQuestionnaireTypeKey }
        return result.first
    }
    
    public func incompleteVNAQuestionnaires() -> [VNAQuestionnaire] {
        let result = allSortedVNAQuestionnaires().filter("completionFlag == false")
        var allIncomplete = [VNAQuestionnaire]()
        for item in result {
            allIncomplete.append(item)
        }
        return allIncomplete
    }
    
    public func hasAnswersSaved(questionnaire: VNAQuestionnaire) -> Bool {
        let results = self.objects(VNACapturedResult.self).filter { $0.questionnaireTypeKey == questionnaire.typeKey }
        return results.count > 0
    }
    
}

