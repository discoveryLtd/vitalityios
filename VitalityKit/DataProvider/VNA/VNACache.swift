//
//  VNACache.swift
//  VitalityActive
//
//  Created by Von Kervin R. Tuico on 09/02/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import UIKit
import RealmSwift
import SwiftDate

public class VNACache: Object {
    @objc public dynamic var dateCreated: Date = Date()
}

extension Realm {
    public class func resetVNARealmIfOutdated() {
        let realm = DataProvider.newVNARealm()
        realm.reset()
        if VNACache.vnaDataIsOutdated() {
            let vnaCache = VNACache()
            try! realm.write {
                realm.add(vnaCache)
            }
        }
    }
    
    public class func resetVNARealm() {
        let realm = DataProvider.newVNARealm()
        realm.reset()
    }
}

extension VNACache {
    
    public class func vnaDataIsOutdated(_ daysValid: Int = 30) -> Bool {
        guard let vnaCache = VNACache.currentVNACache() else {
            return true
        }
        let calendar = Calendar.current
        let cachedDate = calendar.startOfDay(for: vnaCache.dateCreated)
        let todaysDate = calendar.startOfDay(for: Date())
        
        let components = calendar.dateComponents([.day], from: cachedDate, to: todaysDate)
        
        if let daysDifference = components.day {
            if daysDifference > daysValid {
                return true
            } else {
                return false
            }
        } else {
            return true
        }
    }
    
    public class func currentVNACache() -> VNACache? {
        let realm = DataProvider.newVNARealm()
        return realm.objects(VNACache.self).first
    }
}
