//
//  VNAQuestionnaireSection.swift
//  VitalityActive
//
//  Created by Von Kervin R. Tuico on 09/02/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import RealmSwift

public class VNAQuestionnaireSection: Object {
    @objc public dynamic var typeKey = Int()  // don't use QuestionnaireSectionsRef as the type. we have to stay dumb to new sections
    @objc public dynamic var typeName: String?
    @objc public dynamic var visibilityTag: VisibilityTag = ""
    public let questions: List<VNAQuestion> = List<VNAQuestion>()
    @objc public dynamic var isVisible: Bool = false
    @objc public dynamic var typeCode: String?
    public let sortOrderIndex = RealmOptional<Int>()
    
    @objc public dynamic var text: String = ""
    @objc public dynamic var textDescription: String = ""
    @objc public dynamic var textNote: String = ""
    
    public func sortedQuestions() -> Results<VNAQuestion> {
        let sorted = self.questions.sorted(byKeyPath: "sortOrderIndex", ascending: true)
        return sorted
    }
    
    public func sortedVisibleQuestions() -> Results<VNAQuestion> {
        let sorted = self.questions.sorted(byKeyPath: "sortOrderIndex", ascending: true).filter("isVisible == true")
        return sorted
    }
    
    public func isValid() -> Bool {
        var capturedResults = [VNACapturedResult]()
        var questionsWithoutCapturedResults = [VNAQuestion]()
        
        // exclude Label questions from validation
        let questionsToBeValidated = questions.filter({ $0.questionType != .Label })
        for question in questionsToBeValidated {
            if let results = question.capturedResults(), results.count > 0, question.isVisible {
                capturedResults.append(contentsOf: results)
            } else if question.isVisible {
                questionsWithoutCapturedResults.append(question)
            }
        }
        // validate
        let invalidCapturedResults = capturedResults.filter({ $0.valid == false })
        return invalidCapturedResults.count == 0 && questionsWithoutCapturedResults.count == 0
    }
}

extension Realm {
    public func allVNAQuestionnaireSections() -> Results<VNAQuestionnaireSection> {
        return self.objects(VNAQuestionnaireSection.self)
    }
    
    public func vnaQuestionnaireSection(by questionnaireSectionTypeKey: Int) -> VNAQuestionnaireSection? {
        let result = self.objects(VNAQuestionnaireSection.self).filter { $0.typeKey == questionnaireSectionTypeKey }
        return result.first
    }
}

