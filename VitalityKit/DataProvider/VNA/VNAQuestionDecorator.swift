//
//  VNAQuestionDecorator.swift
//  VitalityActive
//
//  Created by Von Kervin R. Tuico on 09/02/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import UIKit
import RealmSwift
import VIAUtilities

public class VNAQuestionDecorator: Object {
    @objc public dynamic var channelType: QuestionnaireChannelRef = QuestionnaireChannelRef.Unknown
    @objc public dynamic var type: QuestionDecoratorRef = QuestionDecoratorRef.Unknown
    @objc public dynamic var channelTypeCode: String?
    @objc public dynamic var typeName: String?
    @objc public dynamic var channelTypeName: String?
    @objc public dynamic var typeCode: String?
    public let unitOfMeasures: List<VNAUnitOfMeasure> = List<VNAUnitOfMeasure>()
}

extension Realm {
    public func allVNAQuestionDecorators() -> Results<VNAQuestionDecorator> {
        return self.objects(VNAQuestionDecorator.self)
    }
}

