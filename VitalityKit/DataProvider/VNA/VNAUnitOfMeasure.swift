//
//  VNAUnitOfMeasure.swift
//  VitalityActive
//
//  Created by Von Kervin R. Tuico on 09/02/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import UIKit
import RealmSwift
import VIAUtilities

public class VNAUnitOfMeasure: Object {
    @objc public dynamic var value: String = ""
    
    public func unitOfMeasureRef() -> UnitOfMeasureRef? {
        if let rawValue = Int(self.value) {
            return UnitOfMeasureRef(rawValue: rawValue)
        }
        return nil
    }
}

extension Realm {
    public func allVNAUnitOfMeasures() -> Results<VNAUnitOfMeasure> {
        return self.objects(VNAUnitOfMeasure.self)
    }
}

