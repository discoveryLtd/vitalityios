//
//  VNAQuestionAssociation.swift
//  VitalityActive
//
//  Created by Von Kervin R. Tuico on 09/02/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import RealmSwift

public class VNAQuestionAssociations: Object {
    @objc public dynamic var childQuestionKey = Int()
    @objc public dynamic var sortIndex = Int()
    //    public var childQuestionKey: RealmOptional<Int> = RealmOptional<Int>()
    //    public var sortIndex: RealmOptional<Int> = RealmOptional<Int>()
    
    
}

extension Realm {
    public func allVNAQuestionAssociations() -> Results<VNAQuestionAssociations> {
        return self.objects(VNAQuestionAssociations.self)
    }
    
    //ge20180113 : This would return true if given childkey exists
    public func vnaFindQuestionAssociation(key:Int) -> Bool? {
        let assoc = self.allVNAQuestionAssociations().filter("childQuestionKey == %@", key)
        if(assoc.count > 0){
            return true
        }else{
            return false
        }
    }
}

