//
//  VNAQuestionnaireProgressAndPointsTracking.swift
//  VitalityActive
//
//  Created by Von Kervin R. Tuico on 09/02/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import UIKit
import RealmSwift
import VIAUtilities

public class VNAQuestionnaireProgressAndPointsTracking: Object {
    public let questionnaire: List<VNAQuestionnaire> = List<VNAQuestionnaire>()
    @objc public dynamic var questionnaireSetTypeCode: String?
    @objc public dynamic var questionnaireSetType: QuestionnaireSetRef = QuestionnaireSetRef.Unknown
    
    @objc public dynamic var totalQuestionnaires: Int = 0
    public let totalQuestionnaireCompleted = RealmOptional<Int>()
    @objc public dynamic var questionnaireSetCompleted: Bool = false
    @objc public dynamic var questionnaireSetTypeName: String?
    
    public let totalPotentialPoints = RealmOptional<Int>()
    public let totalEarnedPoints = RealmOptional<Int>()
    
    @objc public dynamic var questionnaireSetText: String = ""
    @objc public dynamic var questionnaireSetTextDescription: String = ""
    @objc public dynamic var questionnaireSetTextNote: String = ""
}

extension Realm {
    public func allVNAQuestionnaireProgressAndPointsTracking() -> Results<VNAQuestionnaireProgressAndPointsTracking> {
        return self.objects(VNAQuestionnaireProgressAndPointsTracking.self)
    }
}
