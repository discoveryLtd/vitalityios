//
//  VNAQuestion.swift
//  VitalityActive
//
//  Created by Von Kervin R. Tuico on 09/02/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import RealmSwift
import VIAUtilities

public typealias VNAVisibilityTag = String

public class VNAQuestion: Object {
    @objc public dynamic var questionDecorator: VNAQuestionDecorator?
    // Don't use the reference data here for the typeKey,
    // if new questions types are added we'll fail due to outdated data
    @objc public dynamic var typeKey = Int()
    @objc public dynamic var typeCode: String = ""
    @objc public dynamic var typeName: String?
    @objc public dynamic var isVisible: Bool = false
    
    public var unitsOfMeasure: List<VNAUnitOfMeasure> = List<VNAUnitOfMeasure>()
    
    @objc public dynamic var visibilityTag: VNAVisibilityTag = ""
    @objc public dynamic var text: String = ""
    @objc public dynamic var textDescription: String = ""
    @objc public dynamic var textNote: String = ""
    @objc public dynamic var format: String? // regular expression
    public let length = RealmOptional<Int>()
    public var validValues: List<VNAValidValue> = List<VNAValidValue>()
    
    @objc public dynamic var questionTypeName: String? // This indicates the type of question. Depicts what format the answer would be requested in the questionnaire e.g. option list
    @objc public dynamic var questionTypeCode: String?
    @objc public dynamic var questionType: QuestionTypeRef = QuestionTypeRef.Unknown
    public let sortOrderIndex = RealmOptional<Int>()
    @objc public dynamic var isChild: Bool = false
    
    public func capturedResult() -> VNACapturedResult? {
        // Assumes that only one captured result for a question can ever exist, and returns it.
        let results = self.realm?.allVNACapturedResults().filter("questionTypeKey == %@", self.typeKey)
        return results?.first
    }
    
    public func capturedResults() -> Results<VNACapturedResult>? {
        return self.realm?.allVNACapturedResults().filter("questionTypeKey == %@", self.typeKey)
    }
}

extension Realm {
    
    public func allVNAQuestions() -> Results<VNAQuestion> {
        return self.objects(VNAQuestion.self)
    }
    
    public func vnaQuestion(by typeKey: Int) -> VNAQuestion? {
        return self.allVNAQuestions().filter("typeKey == %@", typeKey).first
    }
    
}
