import UIKit
import RealmSwift


public class WDAMarket: Object {
    @objc public dynamic var partner: WDAPartner?
    @objc public dynamic var assets: WDAAssets?
}

extension Realm {
    public func allWDAMarkets() -> Results<WDAMarket> {
        return self.objects(WDAMarket.self).sorted(byKeyPath: "partner.device", ascending: true)
    }

    public func allWDAMarketsExceptFor(device: String) -> Results<WDAMarket> {
        let markets = allWDAMarkets().filter("partner.device != %@", device)
        return markets
    }

    public func wdaMarketFor(device: String) -> WDAMarket? {
        let market = allWDAMarkets().filter("partner.device == %@", device).first
        return market
    }

}
