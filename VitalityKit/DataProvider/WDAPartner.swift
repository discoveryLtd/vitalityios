import UIKit
import RealmSwift
import VIAUtilities

@objc public enum WDADeviceLinkedStatus: Int, EnumCollection {
    case Unknown = 0
    case Unlinked = 1
    case Pending = 2
    case Active = 3

    public func statusString() -> String {
        switch self {
        case .Unlinked:
            return "UNLINKED"
        case .Pending:
            return "PENDING"
        case .Active:
            return "ACTIVE"
        default:
            return "UNKNOWN"
        }
    }
}

public class WDAPartner: Object {
    @objc public dynamic var partnerSystem: String = ""
    @objc public dynamic var device: String = ""
    @objc public dynamic var partnerLink: WDAPartnerUrlDetail?
    @objc public dynamic var partnerDelink: WDAPartnerUrlDetail?
    @objc public dynamic var partnerSync: WDAPartnerUrlDetail?
    @objc public dynamic var partnerLinkedStatus: WDADeviceLinkedStatus = .Unknown
    @objc public dynamic var partnerLastSync: Date?
    @objc public dynamic var partnerLastWorkout: Date?

    override public static func primaryKey() -> String? {
        return "device"
    }
}

extension Realm {
    public func allWDAPartners() -> Results<WDAPartner> {
        return self.objects(WDAPartner.self)
    }
    
    public func allLinkedWDAPartners() -> Results<WDAPartner> {
        let partners = allWDAPartners()
        let linkedPartners = partners.filter("partnerLinkedStatus == %@ OR partnerLinkedStatus == %@", WDADeviceLinkedStatus.Active.rawValue, WDADeviceLinkedStatus.Pending.rawValue)
        return linkedPartners
    }
}
