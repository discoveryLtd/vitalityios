//
//  OFEEventsAndPoints.swift
//  VitalityActive
//
//  Created by Harry C. Lingad on 4/24/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import RealmSwift
import SwiftDate

public class OFEEventsAndPoints: Object {
    
    @objc public dynamic var id: Int = 0
    @objc public dynamic var activityDate: String = ""
    @objc public dynamic var dateLogged: String = ""
    @objc public dynamic var typeName: String = ""
    @objc public dynamic var typeCode: String = ""
    @objc public dynamic var typeKey: Int = 0
    public let eventMetadatas =  List<OFEEventMetadata>()
    
    override public class func primaryKey() -> String? {
        return "id"
    }
}

extension Realm {
    public func allEventsAndPoints() -> Results<OFEEventsAndPoints> {
        return self.objects(OFEEventsAndPoints.self).sorted(byKeyPath: "activityDate", ascending: false)
    }
}
