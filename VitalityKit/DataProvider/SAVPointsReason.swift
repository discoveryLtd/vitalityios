//
//  SAVPointsReason.swift
//  VitalityActive
//
//  Created by Michelle Oratil on 30/11/2017.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import RealmSwift
import VIAUtilities

public class SAVPointsReason: Object {
    @objc public dynamic var reasonKey: Int = 0
    @objc public dynamic var reasonName: String = ""
    @objc public dynamic var reasonCode: String = ""
    
    @objc public dynamic var categoryKey: Int = 0
    @objc public dynamic var categoryName: String = ""
    @objc public dynamic var categoryCode: String = ""
}

extension Realm {
    public func allSAVPointsReasons() -> Results<SAVPointsReason> {
        return self.objects(SAVPointsReason.self)
    }
}
