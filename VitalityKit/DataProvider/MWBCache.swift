//
//  MWBCache.swift
//  VitalityActive
//
//  Created by admin on 2017/05/25.
//  Copyright © 2017 Glucode. All rights reserved.
//

import UIKit
import RealmSwift
import SwiftDate

public class MWBCache: Object {
    @objc public dynamic var dateCreated: Date = Date()
}

extension Realm {
    public class func resetMWBRealmIfOutdated() {
        let realm = DataProvider.newMWBRealm()
        realm.reset()
        if MWBCache.mwbDataIsOutdated() {
            let mwbCache = MWBCache()
            try! realm.write {
                realm.add(mwbCache)
            }
        }
    }
    
    public class func resetMWBRealm() {
        let realm = DataProvider.newMWBRealm()
        realm.reset()
    }
}

extension MWBCache {
    
    public class func mwbDataIsOutdated(_ daysValid: Int = 30) -> Bool {
        guard let mwbCache = MWBCache.currentMWBCache() else {
            return true
        }
        let calendar = Calendar.current
        let cachedDate = calendar.startOfDay(for: mwbCache.dateCreated)
        let todaysDate = calendar.startOfDay(for: Date())
        
        let components = calendar.dateComponents([.day], from: cachedDate, to: todaysDate)
        
        if let daysDifference = components.day {
            if daysDifference > daysValid {
                return true
            } else {
                return false
            }
        } else {
            return true
        }
    }
    
    public class func currentMWBCache() -> MWBCache? {
        let realm = DataProvider.newMWBRealm()
        return realm.objects(MWBCache.self).first
    }
}
