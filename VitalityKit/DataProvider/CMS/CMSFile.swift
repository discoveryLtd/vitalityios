import RealmSwift

public class CMSFile: Object {
    @objc public dynamic var filename: String = ""
    @objc public dynamic var lastAccessedDate: Date = Date()

    override public class func primaryKey() -> String? {
        return "filename"
    }

    public class func downloadDate(for fileName: String) -> Date? {
        let realm = DataProvider.newRealm()
        let file = realm.getFile(with: fileName)
        return file?.lastAccessedDate
    }
}

extension Realm {
    fileprivate func getFile(with fileName: String) -> CMSFile? {
        let files = self.objects(CMSFile.self)
        return files.filter("filename == %@", fileName).first
    }

    fileprivate func getOrCreateFile(with fileName: String) -> CMSFile {
        if let file = self.getFile(with: fileName) {
            return file
        }
        let newFile = CMSFile()
        newFile.filename = fileName
        return newFile
    }

    fileprivate func getFileLastAccessedDate(for fileName: String) -> Date? {
        if let fileAccessInfo = self.getFile(with: fileName) {
            return fileAccessInfo.lastAccessedDate
        }
        return nil
    }

    public func updateFileLastAccessedDate(for fileName: String) {
        try! self.write {
            let file = getOrCreateFile(with: fileName)
            file.lastAccessedDate = Date()
            self.add(file, update: true)
        }
    }
}
