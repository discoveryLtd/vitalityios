//
//  OFEEventMetadata.swift
//  VitalityActive
//
//  Created by Harry C. Lingad on 4/18/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import RealmSwift

public class OFEEventMetadata: Object {
    
    @objc public dynamic var typeCode: String = ""
    @objc public dynamic var typeKey: Int = 0
    @objc public dynamic var typeName: String = ""
    @objc public dynamic var unitOfMeasure: String = ""
    @objc public dynamic var value: String = ""
    
    
}

extension Realm {
    public func allEventMetadata() -> Results<OFEEventMetadata> {
        return self.objects(OFEEventMetadata.self)
    }
}
