//
//  OFESelectionOption.swift
//  VitalityActive
//
//  Created by Val Tomol on 16/04/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import RealmSwift
import VIAUtilities

public class OFESelectionOption: Object {
    @objc public dynamic var featureCode: String = ""
    @objc public dynamic var featureKey: Int = 0 // TODO: Transfer to a TypeRef file.
    @objc public dynamic var featureName: String = ""
    @objc public dynamic var potentialPoints: Int = 0
}

extension Realm {
    public func allOFESelectionOptions() -> Results<OFESelectionOption> {
        return self.objects(OFESelectionOption.self)
    }
}
