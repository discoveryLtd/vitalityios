//
//  OFECapturedResult.swift
//  VitalityActive
//
//  Created by Val Tomol on 18/04/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import RealmSwift
import VIAUtilities

public class OFECapturedResult: Object {
    @objc public dynamic var eventTypeCode: String?
    @objc public dynamic var eventType: OFEEventType?
    @objc public dynamic var selectionOption: OFESelectionOption?
    @objc public dynamic var finishTime: String?
    @objc public dynamic var eventDate: Date?
    @objc public dynamic var eventName: String?
    @objc public dynamic var raceNumber: String?
    @objc public dynamic var organiser: String?
    @objc public dynamic var weblinkProof: String?
    
    override public class func primaryKey() -> String? {
        return "eventTypeCode"
    }
}

extension Realm {
    public func allOFECapturedResults() -> Results<OFECapturedResult> {
        return self.objects(OFECapturedResult.self)
    }
    
    public func capturedResult(eventTypeCode: String? = nil,
                               eventType: OFEEventType? = nil,
                               selectionOption: OFESelectionOption? = nil,
                               finishTime: String? = nil,
                               eventDate: Date? = nil,
                               eventName: String? = nil,
                               raceNumber: String? = nil,
                               organiser: String? = nil,
                               weblinkProof: String? = nil) {
        let capturedResult = OFECapturedResult()
        try! self.write {
            capturedResult.eventTypeCode = eventTypeCode
            capturedResult.eventType = eventType
            capturedResult.selectionOption = selectionOption
            capturedResult.finishTime = finishTime
            capturedResult.eventDate = eventDate
            capturedResult.eventName = eventName
            capturedResult.raceNumber = raceNumber
            capturedResult.organiser = organiser
            capturedResult.weblinkProof = weblinkProof
            
            self.create(OFECapturedResult.self, value:capturedResult, update: true)
            self.add(capturedResult, update: true)
        }
    }
}
