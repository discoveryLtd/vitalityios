import RealmSwift
import VIAUtilities

public class DataProvider {
    
    // MARK: Singleton
    
    private init() {
        // use `default`
    }
    
    open static let `default` = {
        return DataProvider()
    }()
    
    // MARK: New Realm convenience
    
    public static func newInMemoryRealm() -> Realm {
        var configuration = Realm.Configuration.defaultConfiguration
        configuration.inMemoryIdentifier = UUID().uuidString
        return try! Realm(configuration: configuration)
    }
    
    public static func newRealm(for domain: Domain) -> Realm {
        var configuration = Realm.Configuration.defaultConfiguration
        
        configuration.fileURL = Realm.Configuration.defaultConfiguration.fileURL?.deletingLastPathComponent().appendingPathComponent("\(domain.rawValue).realm")
        
//        debugPrint(configuration.fileURL)
        
        configuration.deleteRealmIfMigrationNeeded = true
        configuration.objectTypes = domain.objectsTypes()
        
        return try! Realm(configuration: configuration)
    }
    
    // MARK: Bootstrapping
    
    public static func startup() {
        DataProvider.cleanupOldUserInstructionsIfNeeded()
    }
    
    public static func reset() {
        DataProvider.newRealm().reset()
        DataProvider.newVHRRealm().reset()
        DataProvider.newVNARealm().reset()
        DataProvider.newVHCRealm().reset()
        DataProvider.newWDARealm().reset()
        DataProvider.newARRealm().reset()
        DataProvider.newOFERealm().reset()
        DataProvider.newMWBRealm().reset()
        DataProvider.newHealthAttributesRealm().reset()
        DataProvider.newHealthInformationRealm().reset()
        DataProvider.newProvideFeedbackRealm().reset()
        DataProvider.newPartnerRealm().reset()
        DataProvider.newGDCRealm().reset()
        
        if AppSettings.getAppTenant() == .SLI {
            URLCache.shared.removeAllCachedResponses()
        }
    }
    
    static func cleanupOldUserInstructionsIfNeeded() {
        let realm = DataProvider.newRealm()
        let userInstructions = realm.objects(UserInstruction.self)
        try! realm.write {
            if self.currentUserInstructionForLoginTermsAndConditions() == nil {
                realm.delete(userInstructions)
            }
        }
    }
    
    // MARK: Domain
    
    lazy var allRealmObjectClassesInfo: [ClassInfo] = {
        var classes = [ClassInfo]()
        var count = UInt32(0)
        
        guard let objectClassInfo = ClassInfo(Object.self) else { return classes }
        guard let classList = objc_copyClassList(&count) else { return classes }
        
        for i in 0..<Int(count) {
            guard let classInfo = ClassInfo(classList[i]), objectClassInfo == classInfo.superclassInfo else { continue }
            classes.append(classInfo)
        }
        return classes
    }()
    
    public enum Domain: String {
        
        typealias RealmObjects = Array<RealmSwift.Object.Type>
        
        case core = "Core"
        case vhr = "VHR"
        case mwb = "MWB"
        case vna = "VNA"
        case vhc = "VHC"
        case sav = "SAV"
        case ofe = "OFE"
        case wda = "WDA"
        case activeRewards = "AR"
        case healthAttributes = "HA"
        case healthInformation = "GMI"
        case status = "Status"
        case provideFeedback = "ProvideFeedback"
        case partner = "Partner"
        case gdc = "GDC"
        
        func objectsTypes() -> RealmObjects {
            switch self {
            case .core :
                return objectsTypes(excluding: [Domain.vhr.rawValue,
                                                Domain.mwb.rawValue,
                                                Domain.vna.rawValue,
                                                Domain.vhc.rawValue,
                                                Domain.sav.rawValue,
                                                Domain.ofe.rawValue,
                                                Domain.activeRewards.rawValue,
                                                Domain.wda.rawValue,
                                                Domain.healthAttributes.rawValue,
                                                Domain.healthInformation.rawValue,
                                                Domain.provideFeedback.rawValue,
                                                Domain.partner.rawValue,
                                                Domain.gdc.rawValue,
                                                "PhotoAsset"])
            case .vhc,
                 .sav,
                 .ofe,
                 .provideFeedback,
                 .gdc:
                return objectsTypes(including: [self.rawValue,
                                                "PhotoAsset"])
            case .vhr,
                 .mwb,
                 .vna,
                 .wda,
                 .sav,
                 .ofe,
                 .activeRewards,
                 .healthAttributes,
                 .healthInformation,
                 .status,
                 .provideFeedback,
                 .partner,
                 .gdc:
                return objectsTypes(including: [self.rawValue])
            }
        }
        
        internal func objectsTypes(excluding prefixes: [String]) -> RealmObjects {
            let objects = Domain.objectsTypes(shouldEqual: false, forPrefixes: prefixes)
            return objects
        }
        
        internal func objectsTypes(including prefixes: [String]) -> RealmObjects {
            let objects = Domain.objectsTypes(shouldEqual: true, forPrefixes: prefixes)
            return objects
        }
        
        static func objectsTypes(shouldEqual result: Bool, forPrefixes prefixes: [String]) -> RealmObjects {
            let classes = DataProvider.default.allRealmObjectClassesInfo
            
            // we have to either build up or subtract based on whether
            // we have to include or exclude classes
            var temp = result == true ? [ClassInfo]() : DataProvider.default.allRealmObjectClassesInfo
            
            for classInfo in classes {
                for prefix in prefixes {
                    if classInfo.className.hasPrefix(prefix) {
                        if result {
                            temp.append(classInfo)
                        } else {
                            temp.remove(object: classInfo)
                        }
                        break
                    }
                }
            }
            
            let objects = temp.flatMap({ $0.classObject as? Object.Type })
            return objects
        }
        
    }
    
}

extension DataProvider {
    public static func newRealm() -> Realm {
        return DataProvider.newRealm(for: .core)
    }
    
    public static func newVHRRealm() -> Realm {
        return DataProvider.newRealm(for: .vhr)
    }
    
    public static func newMWBRealm() -> Realm {
        return DataProvider.newRealm(for: .mwb)
    }
    
    public static func newVNARealm() -> Realm {
        return DataProvider.newRealm(for: .vna)
    }
    
    public static func newVHCRealm() -> Realm {
        return DataProvider.newRealm(for: .vhc)
    }
    
    public static func newSAVRealm() -> Realm {
        return DataProvider.newRealm(for: .sav)
    }
    
    public static func newOFERealm() -> Realm {
        return DataProvider.newRealm(for: .ofe)
    }
    
    public static func newWDARealm() -> Realm {
        return DataProvider.newRealm(for: .wda)
    }
    
    public static func newARRealm() -> Realm {
        return DataProvider.newRealm(for: .activeRewards)
    }
    
    public static func newStatusRealm() -> Realm {
        return DataProvider.newRealm(for: .status)
    }
    
    public static func newHealthAttributesRealm() -> Realm {
        return DataProvider.newRealm(for: .healthAttributes)
    }
    
    public static func newHealthInformationRealm() -> Realm {
        return DataProvider.newRealm(for: .healthInformation)
    }
    
    public static func newProvideFeedbackRealm() -> Realm {
        return DataProvider.newRealm(for: .provideFeedback)
    }
    
    public static func newGDCRealm() -> Realm {
        return DataProvider.newRealm(for: .gdc)
    }
    
    public static func newPartnerRealm() -> Realm {
        return DataProvider.newRealm(for: .partner)
    }
}

extension Realm {
    
    func reset() {
        try! self.write {
            self.deleteAll()
        }
    }
    
}
