import Foundation
import VIAUtilities

extension EventTypeRef {
    public static func configuredTypesForVHC() -> [EventTypeRef] {
        let realm = DataProvider.newRealm()
        let featureTypes = VitalityProductFeature.allVHCFeatureTypes().flatMap({ $0.rawValue })
        let productFeatures = realm.allProductFeatures().filter("productFeatureType IN %@", featureTypes)
        let types = productFeatures.flatMap({ $0.eventType() }) as Array<EventTypeRef>
        return types
    }
    
    public static func configuredTypesForSAV() -> [EventTypeRef] {
        let realm = DataProvider.newRealm()
        let featureTypes = VitalityProductFeature.allSAVFeatureTypes().flatMap({ $0.rawValue })
        let productFeatures = realm.allProductFeatures().filter("productFeatureType IN %@", featureTypes)
        let types = productFeatures.flatMap({ $0.eventType() }) as Array<EventTypeRef>
        return types
    }
    
    public static func configuredTypesforSAVScreenings(forProductFeature feature: ProductFeatureTypeRef = .Screenings) -> [EventTypeRef] {
        return DataProvider.newRealm().productFeatures(of: feature).flatMap({ $0.eventType()}) as Array<EventTypeRef>
    }
    
    public static func configuredTypesforSAVVaccinations(forProductFeature feature: ProductFeatureTypeRef = .Vaccinations) -> [EventTypeRef] {
        return DataProvider.newRealm().productFeatures(of: feature).flatMap({ $0.eventType()}) as Array<EventTypeRef>
    }
    
    public static func configuredTypesForViewHistory(forProductFeature screeningFeatures: ProductFeatureTypeRef = .Screenings,
                                                     forProductFeature vaccinationFeatures: ProductFeatureTypeRef = .Vaccinations) -> [EventTypeRef] {
        let realm = DataProvider.newRealm()
        var screeningTypeKey = realm.productFeatures(of: screeningFeatures).flatMap({ $0.eventType()}) as Array<EventTypeRef>
        let vaccinationTypeKey = realm.productFeatures(of: vaccinationFeatures).flatMap({ $0.eventType()}) as Array<EventTypeRef>
        screeningTypeKey.append(contentsOf: vaccinationTypeKey)
        return screeningTypeKey
    }
    
}
