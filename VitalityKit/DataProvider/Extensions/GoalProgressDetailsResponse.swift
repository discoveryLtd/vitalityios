import Foundation

public extension GoalProgressDetailsResponse {
    
    public func didRecieveGoalTrackers() -> Bool {
        guard let goalTrackers = self.getGoalProgressAndDetailsResponse?.goalTrackerOuts else { return false }
        
        if goalTrackers.count > 0 {
            return true
        } else {
            return false
        }
    }
    
}
