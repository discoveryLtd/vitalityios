//
//  OFEAgreementPeriod.swift
//  VitalityActive
//
//  Created by Harry C. Lingad on 4/23/18.
//  Copyright © 2018 Glucode. All rights reserved.
//


import Foundation
import RealmSwift
import SwiftDate

public class OFEAgreementPeriod: Object {
    
    @objc public dynamic var id: Int = 0
    public let effectivePeriods =  List<OFEEffectivePeriod>()
    
    override public class func primaryKey() -> String? {
        return "id"
    }
    
    //    public class func latest() -> OFEAgreementPeriod? {
    //        let realm = DataProvider.newRealm()
    //        let account = realm.objects(OFEAgreementPeriod.self).sorted(byKeyPath: "activityDate", ascending: false).first
    //        return account
    //    }
    //
    //    public func filterPointsEventsEntries(by category: String) -> Results<OFEEventsPointsEntry> {
    //        let filteredPointsEventsEntries = pointsEntries.filter("category == '\(effectiveDate)'")
    //        return filteredPointsEventsEntries
    //    }
    //
    //    public func sortPointsEventsEntriesByDateLogged() -> Results<OFEEffectiveDate> {
    //        let sortedPointsEventsEntries = pointsEntries.sorted(byKeyPath: "effectiveDate", ascending: false)
    //        return sortedPointsEventsEntries
    //    }
}

extension Realm {
    public func allAgreementPeriod() -> Results<OFEAgreementPeriod> {
        return self.objects(OFEAgreementPeriod.self) //.sorted(byKeyPath: "effectiveDate", ascending: false)
    }
}


