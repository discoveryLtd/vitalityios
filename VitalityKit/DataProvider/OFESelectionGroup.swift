//
//  OFESelectionGroup.swift
//  VitalityActive
//
//  Created by Val Tomol on 16/04/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import RealmSwift
import VIAUtilities

public class OFESelectionGroup: Object {
    public let selectionOptions = List<OFESelectionOption>()
    @objc public dynamic var categoryCode: String = ""
    @objc public dynamic var categoryKey: Int = 0 // TODO: Transfer to a TypeRef file.
    @objc public dynamic var categoryName: String = ""
    @objc public dynamic var featureCode: String = ""
    @objc public dynamic var featureKey: Int = 0 // TODO: Transfer to a TypeRef file.
    @objc public dynamic var featureName: String = ""
}

extension Realm {
    public func allOFESelectionGroups() -> Results<OFESelectionGroup> {
        return self.objects(OFESelectionGroup.self)
    }
}
