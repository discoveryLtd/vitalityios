//
//  Application.swift
//  VitalityActive
//
//  Created by Marius Janse van Vuuren on 2017/03/01.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import RealmSwift

public class AppConfig: Object {

    @objc public enum ConfigType: Int {
        case Unknown = -1
        case Sumitomo = 1
    }

    @objc public dynamic var appConfigVersion: AppConfigVersion?
    @objc public dynamic var typeKey: ConfigType = AppConfig.ConfigType.Unknown
    @objc public dynamic var typeName: String = ""
    @objc public dynamic var name: String = ""
    @objc public dynamic var typeCode: String = ""
    @objc public dynamic var upgradeUrl: String = "" // Appstore URL to download update version of the app

    public class func deleteExisting() {
        let realm = DataProvider.newRealm()
        try! realm.write {
            let currentParameters = realm.objects(AppConfigFeatureParameter.self)
            for currentParameter in currentParameters {
                realm.delete(currentParameter)
            }
            let currentApplications = realm.objects(AppConfig.self)
            for currentApplication in currentApplications {
                realm.delete(currentApplication)
            }
            let currentVersions = realm.objects(AppConfigVersion.self)
            for currentVersion in currentVersions {
                realm.delete(currentVersion)
            }
            let currentFeatures = realm.objects(AppConfigFeature.self)
            for currentFeature in currentFeatures {
                realm.delete(currentFeature)
            }
        }
    }
    
    public class func getUpgradeUrl() -> String {
        let realm = DataProvider.newRealm()
        guard let appConfig = realm.objects(AppConfig.self).first else {
            return ""
        }
        return appConfig.upgradeUrl
    }
}
