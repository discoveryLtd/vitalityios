import Foundation
import RealmSwift
import VIAUtilities

public class AppConfigFeature: Object {

    @objc public enum AppConfigFeatureType: Int, EnumCollection {
        case Unknown = -1
        case InsConfig = 1
        case TermsAndConditionsContent = 2
        case NonsmokersDclJrny = 3
        case NonsmokersDclrtnContent = 4
        case NonsmokersDSConsentContent = 5
        //case NonsmokersDclResFils = 6 // Discontinued - don't reuse number
        case vhcJrny = 7
        case VHCDSConsentContent = 8
        //case vhcResFiles = 9 // Discontinued - don't reuse number
        case vhrJrny = 10
        case VHRDSConsentContent = 11
        // case vhrResFiles = 12 // Discontinued - don't reuse number
        // case wellnessDevicesAppsResFiles = 13 // Discontinued - don't reuse number
        case userPreferencesPrivacyPolicy = 14
        case WDADSConsentContent = 15
        case WDAActivityMapping = 16
        case VHCHealthcareBenefitUKEIGI = 35
        case VHCHealthcareBenefit = 17
        case VHRDisclaimerContent = 18
        case ARMedicallyFitAgreementContent = 19
        case AppleHealthAppAboutContent = 20
        case ActiveRewardsBenefit = 21
        case AppleHealthAppLearnMoreContent = 22
        case SVDSConsentContent = 23
        case vitalityStatusConsentContent = 25
        case VNADSConsentContent = 26
        case SBDSConsentContent = 36
        case AREasyTicketsDS = 38
        case ARFoodPandaDS = 37
    }

    public let featureParameters = List<AppConfigFeatureParameter>()
    @objc public dynamic var typeKey: AppConfigFeatureType = AppConfigFeatureType.Unknown
    @objc public dynamic var name: String = ""
    @objc public dynamic var toggle: Bool = false
    @objc public dynamic var typeCode: String = ""

    public static func allFilesToBeDownloaded() -> [String] {
        let files: [AppConfigFeatureType] = [.WDAActivityMapping]
        let filenames = files.flatMap({ AppConfigFeature.allFiles(for: $0) })
        return filenames
    }

    public static func getFileName(type: AppConfigFeature.AppConfigFeatureType, preferredParameterName: String = "filename") -> String? {
        let realm = DataProvider.newRealm()
        guard let feature = realm.feature(of: type) else { return nil }
        guard let filename = feature.featureParameters.filter("name == %@", preferredParameterName).first?.value else { return nil }
        return filename
    }

    static func allFiles(for feature: AppConfigFeature.AppConfigFeatureType) -> [String] {
        let realm = DataProvider.newRealm()
        guard let feature = realm.feature(of: feature) else { return [] }
        return feature.featureParameters.flatMap({ $0.value })
    }

    public class func contentId(for feature: AppConfigFeatureType) -> String? {
        let realm = DataProvider.newRealm()
        let features = realm.features(of: feature)
        for feature in features{
            if let param = feature.featureParameters.filter("name == 'lifeRayContentId'").first{
                return param.value
            }
        }
        return nil
    }

    public class func insurerCMSGroupId() -> String? {
        let realm = DataProvider.newRealm()
        guard let feature = realm.feature(of: .InsConfig) else { return nil }
        guard let groupId = feature.featureParameters.filter("name == 'liferayGroupId'").first?.value else { return nil }
        return groupId
    }

    public class func insurerDefaultLanguage() -> String? {
        let realm = DataProvider.newRealm()
        guard let feature = realm.feature(of: .InsConfig) else { return nil }
        guard let language = feature.featureParameters.filter("name == 'defaultLanguage'").first?.value else { return nil }
        return language
    }

    public class func insurerGradientColor1Hex() -> String {
        let realm = DataProvider.newRealm()
        guard let feature = realm.feature(of: .InsConfig) else { return "fff" }
        guard let color = feature.featureParameters.filter("name == 'gradientColor1'").first?.value else { return "fff" }
        return color
    }

    public class func insurerGradientColor2Hex() -> String {
        let realm = DataProvider.newRealm()
        guard let feature = realm.feature(of: .InsConfig) else { return "fff" }
        guard let color = feature.featureParameters.filter("name == 'gradientColor2'").first?.value else { return "fff" }
        return color
    }

    public class func insurerGlobalTintColor() -> String? {
        let realm = DataProvider.newRealm()
        guard let feature = realm.feature(of: .InsConfig) else { return nil }
        guard let colorString = feature.featureParameters.filter("name == 'globalTintColor'").first?.value else { return nil }
        return colorString
    }
}

extension Realm {
    public func allAppConfigFeatures() -> Results<AppConfigFeature> {
        return self.objects(AppConfigFeature.self).sorted(byKeyPath: "typeKey", ascending: true)
    }

    public func feature(of type: AppConfigFeature.AppConfigFeatureType) -> AppConfigFeature? {
        let features = self.allAppConfigFeatures()
        let filtered = features.filter("typeKey == %@", type.rawValue).first
        return filtered
    }
    
    public func features(of type: AppConfigFeature.AppConfigFeatureType) -> Results<AppConfigFeature> {
        let features = self.allAppConfigFeatures()
        let filtered = features.filter("typeKey == %@", type.rawValue)
        return filtered
    }
}
