//
//  AppConfigFeatureParameter.swift
//  VitalityActive
//
//  Created by Marius Janse van Vuuren on 2017/03/01.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import RealmSwift

public class AppConfigFeatureParameter: Object {
    @objc public dynamic var name: String?
    @objc public dynamic var value: String?
}
