//
//  AppConfig.swift
//  VitalityActive
//
//  Created by Marius Janse van Vuuren on 2017/03/01.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import RealmSwift

public class AppConfigVersion: Object {
    @objc public dynamic var effectiveTo: Date?
    public let appFeature: List<AppConfigFeature> = List<AppConfigFeature>()
    @objc public dynamic var releaseVersion: String = "0.0"
    @objc public dynamic var effectiveFrom: Date?

    public class func currentAppConfigReleaseVersion() -> String {
        let realm = DataProvider.newRealm()
        guard let appConfigVersion = realm.objects(AppConfigVersion.self).first else {
            return "0.0"
        }
        return appConfigVersion.releaseVersion
    }

    public class func currentAppConfigReleaseVersionFloat() -> Float {
        guard let appConfigVersion = Float(currentAppConfigReleaseVersion()) else {
            return 0.0
        }

        return appConfigVersion
    }
}
