import Foundation
import RealmSwift
import VIAUtilities

public class VitalityProductFeature: Object {
    @objc public dynamic var effectiveTo: Date?
    @objc public dynamic var effectiveFrom: Date?
    @objc public dynamic var type: ProductFeatureRef = ProductFeatureRef.Unknown
    public let featureLinks: List<VitalityProductFeatureLinks> = List<VitalityProductFeatureLinks>()
    @objc public dynamic var productFeatureType: ProductFeatureTypeRef = ProductFeatureTypeRef.Unknown
    @objc public dynamic var typeName: String = ""
    @objc public dynamic var typeCode: String = ""
    
    public class func isEnabled(_ featureType: ProductFeatureRef) -> Bool {
        if AppSettings.hideDSConsent(){
            return false
        }
        let realm = DataProvider.newRealm()
        return realm.feature(of: featureType) != nil
    }
    
    public class func allVHCFeatureTypes() -> [ProductFeatureTypeRef] {
        var featureType:[ProductFeatureTypeRef] = [.VHCBMI, .VHCWaistCircum, .VHCBloodGlucose, .VHCBloodPressure, .VHCCholesterol, .VHCHbA1c, .VHCUrinaryProtein]
        let realm = DataProvider.newRealm()
        if (realm.productFeatureTypes(of: ProductFeatureTypeRef.VHCLipidProfile)).count > 0 {
            featureType.append(.VHCLipidProfile)
        }
        return featureType
    }
    
    public class func allSAVFeatureTypes() -> [ProductFeatureTypeRef] {
        return [.Screenings, .Vaccinations]
    }
    
    public func eventType() -> EventTypeRef? {
        if let link = self.featureLinks.filter("type == %@", LinkTypeRef.EventType.rawValue).first, let key = link.linkedKey.value {
            let eventType = EventTypeRef(rawValue: key)
            return eventType
        }
        return nil
    }
    
    public func healthAttributeType() -> PartyAttributeTypeRef {
        if let link = self.featureLinks.filter("type == %@", LinkTypeRef.HealthAttributeType.rawValue).first, let key = link.linkedKey.value {
            let healthAttributeType = PartyAttributeTypeRef(rawValue: key) ?? .Unknown
            return healthAttributeType
        }
        return .Unknown
    }
    
    public func pointCategoryType() -> PointsEntryCategoryRef {
        if let link = self.featureLinks.filter("type == %@", LinkTypeRef.PointsCategory.rawValue).first, let key = link.linkedKey.value {
            let pointCategoryType = PointsEntryCategoryRef(rawValue: key) ?? .Unknown
            return pointCategoryType
        }
        return .Unknown
    }
    
    public func eventCategoryType() -> EventCategoryRef {
        if let link = self.featureLinks.filter("type == %@", LinkTypeRef.EventCategory.rawValue).first, let key = link.linkedKey.value {
            let eventCategory = EventCategoryRef(rawValue: key) ?? .Unknown
            return eventCategory
        }
        return .Unknown
    }
}

extension Realm {
    public func allProductFeatures() -> Results<VitalityProductFeature> {
        return self.objects(VitalityProductFeature.self).sorted(byKeyPath: "type", ascending: true)
    }
    
    public func feature(of type: ProductFeatureRef) -> VitalityProductFeature? {
        let features = self.allProductFeatures()
        let feature = features.filter("type == %@", type.rawValue).first
        return feature
    }
    
    public func productFeatures(of featureType: ProductFeatureTypeRef) -> Results<VitalityProductFeature> {
        let features = self.allProductFeatures().filter("productFeatureType == %@", featureType.rawValue)
        return features
    }
    
    public func productFeatureTypes(of featureType: ProductFeatureTypeRef) -> [VitalityProductFeature] {
        var healthAttributes = [VitalityProductFeature]()
        let features = self.allProductFeatures().filter("productFeatureType == %@", featureType.rawValue)
        for feature in features{
            if (feature.featureLinks.filter("type == %@",LinkTypeRef.HealthAttributeType.rawValue)).count > 0 {
                healthAttributes.append(feature)
            }
        }
        return healthAttributes
    }
    
    public func rewardFeatureActive(with typeKey: Int) -> Bool {
        if let productFeature = activeRewardsPartnerProductFeature(linkTypeKey: typeKey) {
            if let featureLink = productFeature.featureLinks.filter("type == %@", LinkTypeRef.DSInstructionType.rawValue).first {
                if let value = featureLink.linkedKey.value {
                    if userInstruction(with: value) != nil {
                        return true
                    }
                }
            }
        }
        return false
    }
    
    internal func activeRewardsPartnerProductFeature(linkTypeKey: Int) -> VitalityProductFeature? {
        let productFeatures = allProductFeatures()
        let rewardFeatures = productFeatures.filter("productFeatureType == %@", ProductFeatureTypeRef.ActiveRewardsPartner.rawValue)
        for rewardFeature in rewardFeatures {
            if rewardFeature.featureLinks.filter("type == %@ && linkedKey == %@", LinkTypeRef.Reward.rawValue, linkTypeKey).first != nil{
                return rewardFeature
            }
        }
        return nil
    }
    
    internal func productFeature(linkTypeKey: Int) -> VitalityProductFeature? {
        let productFeatures = allProductFeatures()
        let rewardFeatures = productFeatures.filter("type == %@", ProductFeatureRef.ActiveReward.rawValue)
        for rewardFeature in rewardFeatures {
            if rewardFeature.featureLinks.filter("type == %@ && linkedKey == %@", LinkTypeRef.Reward.rawValue, linkTypeKey).first != nil{
                return rewardFeature
            }
        }
        return nil
    }
}
