//
//  VitalityProductAssociations.swift
//  VitalityActive
//
//  Created by Marius Janse van Vuuren on 2017/03/01.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import RealmSwift

public class VitalityProductAssociations: Object {
    @objc public dynamic var effectiveTo: Date?
    @objc public dynamic var typeKey: Int = 0 //TODO: make enum
    @objc public dynamic var targetProductName: String = ""
    @objc public dynamic var typeName: String = ""
    @objc public dynamic var effectiveFrom: Date?
    @objc public dynamic var typeCode: String = ""
    @objc public dynamic var associationTypeCode: String = ""
    @objc public dynamic var associationTypeKey: Int = 0
    @objc public dynamic var associationTypeName: String = ""
}
