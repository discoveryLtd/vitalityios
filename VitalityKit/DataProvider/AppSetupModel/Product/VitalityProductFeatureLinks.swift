import Foundation
import RealmSwift
import VIAUtilities

public class VitalityProductFeatureLinks: Object {
    @objc public dynamic var type: LinkTypeRef = LinkTypeRef.Unknown
    @objc public dynamic var typeName: String?
    public let linkedKey: RealmOptional<Int> = RealmOptional<Int>()
    @objc public dynamic var typeCode: String?
}

extension Realm {
    public func allProductFeatureLinks() -> Results<VitalityProductFeatureLinks> {
        return self.objects(VitalityProductFeatureLinks.self)
    }

    public func ProductFeatureLink(for type: LinkTypeRef) -> Results<VitalityProductFeatureLinks> {
        let productfeatureLinks = self.allProductFeatureLinks()
        return productfeatureLinks.filter("type == %@", type.rawValue)
    }

}
