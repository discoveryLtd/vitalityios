//
//  VitalityProductRenewalPeriod.swift
//  VitalityActive
//
//  Created by Marius Janse van Vuuren on 2017/03/01.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import RealmSwift

public class VitalityProductAttribute: Object {
    @objc public dynamic var effectiveTo: Date?
    @objc public dynamic var productAttributeTypeName: String = ""
    @objc public dynamic var productAttributeTypeKey: Int = 0 //TODO: make enum
    @objc public dynamic var productAttributeTypeCode: String = ""
    @objc public dynamic var effectiveFrom: Date?
    @objc public dynamic var value: String = ""
}
