//
//  VitalityProduct.swift
//  VitalityActive
//
//  Created by Marius Janse van Vuuren on 2017/03/01.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import RealmSwift

public class VitalityProduct: Object {
    @objc public dynamic var effectiveTo: Date?
    @objc public dynamic var effectiveFrom: Date?
    public let productKey = RealmOptional<Int>()
    @objc public dynamic var productName: String?
    @objc public dynamic var productCode: String?
    public let attributes = List<VitalityProductAttribute>()
    public let features = List<VitalityProductFeature>()
    public let associations = List<VitalityProductAssociations>()
}
