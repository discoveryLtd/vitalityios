//
//  PartyPhysicalAddress.swift
//  VitalityActive
//
//  Created by Marius Janse van Vuuren on 2017/03/01.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import RealmSwift

public class PartyPhysicalAddress: Object {
    @objc public dynamic var country: String = ""
    @objc public dynamic var poBox: Bool = false
    @objc public dynamic var complex: String?
    @objc public dynamic var postalCode: String = ""
    @objc public dynamic var streetAddress1: String = ""
    @objc public dynamic var unitNumber: String = ""
    @objc public dynamic var streetAddress2: String = ""
    @objc public dynamic var streetAddress3: String = ""
    public let contactRoles = List<PartyContactRole>()
    @objc public dynamic var place: String = ""
}
