//
//  PartyPerson.swift
//  VitalityActive
//
//  Created by Marius Janse van Vuuren on 2017/03/01.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import RealmSwift

public class PartyPerson: Object {
    @objc public dynamic var title: String?
    @objc public dynamic var gender: String?
    @objc public dynamic var givenName: String?
    @objc public dynamic var familyName: String?
    @objc public dynamic var bornOn: Date?
    @objc public dynamic var preferredName: String?
    @objc public dynamic var suffix: String?
	@objc public dynamic var avatarPath: String?
}
