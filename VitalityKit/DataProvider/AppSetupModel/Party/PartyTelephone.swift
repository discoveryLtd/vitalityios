//
//  PartyTelephone.swift
//  VitalityActive
//
//  Created by Marius Janse van Vuuren on 2017/03/01.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import RealmSwift

public class PartyTelephone: Object {
    public let contactRoles = List<PartyContactRole>()
    @objc public dynamic var countryDialCode: String = ""
    @objc public dynamic var areaDialCode: String = ""
    @objc public dynamic var typeKey: Int = 0
    @objc public dynamic var contactNumber: String = ""
    @objc public dynamic var numberExtension: String?
}
