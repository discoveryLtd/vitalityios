//
//  PartyTelephoneContactRole.swift
//  VitalityActive
//
//  Created by Marius Janse van Vuuren on 2017/03/01.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import RealmSwift

public class PartyContactRole: Object {
    @objc public dynamic var roleTypeKey: Int = 0 //TODO: make enum
    @objc public dynamic var rolePurposeTypeKey: Int = 0 //TODO: make enum
    @objc public dynamic var effectiveFrom: Date?
    @objc public dynamic var effectiveTo: Date?
}
