//
//  VitalityParty.swift
//  VitalityActive
//
//  Created by Marius Janse van Vuuren on 2017/03/01.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import RealmSwift

public class VitalityParty: Object {
    public static let defaultIdValue = -999

    public let telephones = List<PartyTelephone>()
    public let emailAddresses = List<PartyEmailAddress>()
    public let references = List<PartyReference>()
    public let generalPreferences = List<PartyGeneralPreference>()
    public let physicalAddresses = List<PartyPhysicalAddress>()
    public let webAddresses = List<PartyWebAddress>()
    @objc public dynamic var person: PartyPerson?
    @objc public dynamic var geographicalAreaPreference: PartyGeographicalAreaPreference?
    @objc public dynamic var languagePreference: PartyLanguagePreference?
    @objc public dynamic var timeZonePreference: PartyTimeZonePreference?
    @objc public dynamic var measurementPreference: PartyMeasurementSystemPreference?
    @objc public dynamic var partyId: Int = VitalityParty.defaultIdValue
    @objc public dynamic var accessToken: String = ""
    @objc public dynamic var tenantId: Int = VitalityParty.defaultIdValue
}

extension VitalityParty {
    public class func accessTokenForCurrentParty() -> String {
        let realm = DataProvider.newRealm()
        guard let party = realm.currentVitalityParty() else {
            debugPrint("[DebugMode] accessTokenForCurrentParty() currentVitalityParty is NIL")
            return ""
        }
        return party.accessToken
    }
}

extension Realm {
    public func currentVitalityParty() -> VitalityParty? {
        return self.objects(VitalityParty.self).first
    }

    public func getPartyId(shouldLogoutOnNilPartyID: Bool = true) -> Int {
        guard let party = self.currentVitalityParty() else {
            debugPrint("[DebugMode] getPartyId() currentVitalityParty is NIL. User must be logged out.")
            if shouldLogoutOnNilPartyID {
                NotificationCenter.default.post(name: .VIAServerSecurityException, object: nil)
            }
            return 0
        }
        return party.partyId
    }

    public func getTenantId(shouldLogoutOnNilTenantID: Bool = true) -> Int {
        guard let party = self.currentVitalityParty() else {
            debugPrint("[DebugMode] getTenantId() currentVitalityParty is NIL. User must be logged out.")
            if shouldLogoutOnNilTenantID {
                NotificationCenter.default.post(name: .VIAServerSecurityException, object: nil)
            }
            return 0
        }
        return party.tenantId
    }
    
    public func getBirthdate(shouldLogoutOnNilBirthDate: Bool = true) -> Date?{
        guard let party = self.currentVitalityParty() else {
            debugPrint("[DebugMode] getBirthdate() currentVitalityParty is NIL. User must be logged out.")
            if shouldLogoutOnNilBirthDate {
                NotificationCenter.default.post(name: .VIAServerSecurityException, object: nil)
            }
            return nil
        }
        return party.person?.bornOn
    }
}

extension DataProvider {

    public class func currentVitalityPartyIsValid(shouldLogoutOnNilVitalityParty: Bool = true) -> Bool {
        let realm = DataProvider.newRealm()
        guard let party = realm.currentVitalityParty() else {
            debugPrint("[DebugMode] currentVitalityPartyIsValid() currentVitalityParty is NIL. User must be logged out.")
            if shouldLogoutOnNilVitalityParty {
                NotificationCenter.default.post(name: .VIAServerSecurityException, object: nil)
            }
            return false
        }
        return party.partyId != VitalityParty.defaultIdValue && party.tenantId != VitalityParty.defaultIdValue && !party.accessToken.isEmpty
    }

}
