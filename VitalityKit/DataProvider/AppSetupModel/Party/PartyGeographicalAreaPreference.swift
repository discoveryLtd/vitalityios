//
//  PartyGeographicalAreaPreference.swift
//  VitalityActive
//
//  Created by Marius Janse van Vuuren on 2017/03/01.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import RealmSwift

public class PartyGeographicalAreaPreference: Object {
    @objc public dynamic var effectiveFrom: Date?
    @objc public dynamic var effectiveTo: Date?
    @objc public dynamic var type: String = ""
    @objc public dynamic var value: String = ""
}
