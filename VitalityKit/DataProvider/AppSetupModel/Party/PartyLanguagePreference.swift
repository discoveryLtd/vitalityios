//
//  PartyLanguagePreference.swift
//  VitalityActive
//
//  Created by Marius Janse van Vuuren on 2017/03/01.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import RealmSwift

public class PartyLanguagePreference: Object {
    @objc public dynamic var isoCode: String = ""
    @objc public dynamic var name: String = ""
}
