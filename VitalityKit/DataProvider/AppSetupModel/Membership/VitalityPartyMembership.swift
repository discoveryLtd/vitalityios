//
//  VitalityMembership.swift
//  VitalityActive
//
//  Created by Marius Janse van Vuuren on 2017/03/01.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import RealmSwift

public class VitalityPartyMembership: Object {
    public let references = List<VitalityMembershipReference>()
    @objc public dynamic var ID: Int = 0
    @objc public dynamic var currentMembershipPeriod: VitalityMembershipCurrentPeriod?
    public let vitalityProducts = List<VitalityProduct>()
    public let membershipParties = List<VitalityMembershipParty>()
    public let renewalPeriods = List<VitalityMembershipRenewalPeriod>()
    public let stateCategories = List<VitalityMembershipStateCategory>()

}

extension Realm {
    public func getMembershipId() -> Int {
        if let id = self.objects(VitalityPartyMembership.self).first?.ID {
            return id
        }
        fatalError("Missing VitalityPartyMembership ID")
    }
    
    public func getRenewalPeriods() -> List<VitalityMembershipRenewalPeriod> {
        if let list = self.objects(VitalityPartyMembership.self).first?.renewalPeriods {
            return list
        }
        fatalError("Missing VitalityPartyMembership Renewal Periods")
    }
}
