//
//  VitalityMembershipStateCategory.swift
//  VitalityActive
//
//  Created by Marius Janse van Vuuren on 2017/03/01.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import RealmSwift

public class VitalityMembershipStateCategory: Object {
    @objc public dynamic var typeKey: Int = 0 //TODO: make enum
    @objc public dynamic var typeName: String = ""
    public let states = List<VitalityMembershipState>()
    @objc public dynamic var typeCode: String = ""
}
