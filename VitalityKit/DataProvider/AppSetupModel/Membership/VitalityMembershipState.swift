//
//  VitalityMembershipState.swift
//  VitalityActive
//
//  Created by Marius Janse van Vuuren on 2017/03/01.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import RealmSwift

public class VitalityMembershipState: Object {
    @objc public dynamic var reasonTypeKey: Int = 0 //TODO: make enum
    @objc public dynamic var effectiveTo: Date?
    @objc public dynamic var statusTypeName: String = ""
    @objc public dynamic var reasonTypeName: String = ""
    @objc public dynamic var statusTypeKey: Int = 0 //TODO: make enum
    @objc public dynamic var reasonTypeCode: String = ""
    @objc public dynamic var statusTypeCode: String = ""
    @objc public dynamic var effectiveFrom: Date?
}
