//
//  VitalityMembershipRenewalPeriod.swift
//  VitalityActive
//
//  Created by Marius Janse van Vuuren on 2017/03/01.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import RealmSwift

public class VitalityMembershipRenewalPeriod: Object {
    @objc public dynamic var effectiveTo: Date?
    @objc public dynamic var productAttributeTypeName: String = ""
    @objc public dynamic var productAttributeTypeKey: Int = 0 //TODO: make enum
    @objc public dynamic var productAttributeTypeCode: String = ""
    @objc public dynamic var effectiveFrom: Date?
    @objc public dynamic var value: String = ""
    @objc public dynamic var period: Int = 0
    @objc public dynamic var unit: String = ""
}
