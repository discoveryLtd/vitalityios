//
//  OFEEvent.swift
//  VitalityActive
//
//  Created by Val Tomol on 16/04/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import RealmSwift
import VIAUtilities

public class OFEEvent: Object {
    public let ofeEvent = List<OFEEventType>()
}

extension Realm {
    public func allOFEEvents() -> Results<OFEEvent> {
        return self.objects(OFEEvent.self)
    }
}
