//
//  MWBQuestionnaireProgressAndPointsTracking.swift
//  VitalityActive
//
//  Created by Harry C. Lingad on 2/5/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import UIKit
import RealmSwift
import VIAUtilities

public class MWBQuestionnaireProgressAndPointsTracking: Object {
    public let questionnaire: List<MWBQuestionnaire> = List<MWBQuestionnaire>()
    @objc public dynamic var questionnaireSetTypeCode: String?
    @objc public dynamic var questionnaireSetType: QuestionnaireSetRef = QuestionnaireSetRef.Unknown
    
    @objc public dynamic var totalQuestionnaires: Int = 0
    public let totalQuestionnaireCompleted = RealmOptional<Int>()
    @objc public dynamic var questionnaireSetCompleted: Bool = false
    @objc public dynamic var questionnaireSetTypeName: String?
    
    public let totalPotentialPoints = RealmOptional<Int>()
    public let totalEarnedPoints = RealmOptional<Int>()
    
    @objc public dynamic var questionnaireSetText: String = ""
    @objc public dynamic var questionnaireSetTextDescription: String = ""
    @objc public dynamic var questionnaireSetTextNote: String = ""
}

extension Realm {
    public func allMWBQuestionnaireProgressAndPointsTracking() -> Results<MWBQuestionnaireProgressAndPointsTracking> {
        return self.objects(MWBQuestionnaireProgressAndPointsTracking.self)
    }
}

