import Foundation
import RealmSwift
import VIAUtilities

public class VHCHealthAttributeValidValues: Object {
    public let lowerLimit = RealmOptional<Double>()
    public let upperLimit = RealmOptional<Double>()
    @objc public dynamic var maxValue: String  = ""
    @objc public dynamic var minValue: String  = ""
    @objc public dynamic var validValuesList: String?
    @objc public dynamic var typeTypeName: String  = ""
    @objc public dynamic var unitOfMeasureType: UnitOfMeasureRef = .Unknown
    @objc public dynamic var type: Int = -1
    public let healthAttribute = LinkingObjects(fromType: VHCHealthAttribute.self, property: "validValues")
}

extension Realm {
    public func allVHCHealthAttributeValidValues() -> Results<VHCHealthAttributeValidValues> {
        return self.objects(VHCHealthAttributeValidValues.self)
    }
}
