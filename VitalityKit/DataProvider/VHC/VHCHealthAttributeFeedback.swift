//
//  VHCHealthAttributeFeedback.swift
//  VitalityActive
//
//  Created by Lionel Camacho on 2017/03/16.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import RealmSwift
import VIAUtilities

public class VHCHealthAttributeFeedback: Object {

    @objc public dynamic var feedbackTypeName: String = ""
    @objc public dynamic var feedbackCategoryName: String = ""

    @objc public dynamic var type: PartyAttributeFeedbackRef = PartyAttributeFeedbackRef.Unknown
    @objc public dynamic var category: PartyAttributeFeedbackTypeRef = PartyAttributeFeedbackTypeRef.Unknown

}

extension Realm {
    public func allVHCHealthAttributeFeedback() -> Results<VHCHealthAttributeFeedback> {
        return self.objects(VHCHealthAttributeFeedback.self)
    }
}
