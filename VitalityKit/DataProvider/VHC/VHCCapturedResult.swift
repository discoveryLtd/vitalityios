import Foundation
import RealmSwift
import VIAUtilities

public class VHCCapturedResult: Object {
    @objc public dynamic var isValid: Bool = false
    @objc public dynamic var dateCaptured = Date()
    @objc public dynamic var unitOfMeasureType: UnitOfMeasureRef = .Unknown
    @objc public dynamic var rawInput: String?
    @objc public dynamic var inputUnitSymbol: String?
    public let value = RealmOptional<Double>()
    @objc public dynamic var healthAttributeType: PartyAttributeTypeRef = .Unknown

    override public class func primaryKey() -> String? {
        return "healthAttributeType"
    }

    public func measurement() -> Measurement<Unit>? {
        if let symbol = self.inputUnitSymbol, let value = self.value.value {
            // This will possibly cause a bug where the unit is not translating between the symbol's locale and the device locale.
            // The fix is to not init a Unit from the symbol, but to call unit() on UnitOfMeasureRef
            let unit = Unit(symbol: symbol)
            let measurement = Measurement(value: value, unit: unit)
            return measurement
        }
        return nil
    }

    public func eventType() -> EventTypeRef {
        let realm = DataProvider.newRealm()
        let features = realm.allProductFeatures().filter("(ANY featureLinks.type == %@) AND (ANY featureLinks.linkedKey == %@)",
                                       LinkTypeRef.HealthAttributeType.rawValue, self.healthAttributeType.rawValue)
        /**
         * Removing the original implementation.
         
         let features = realm.allProductFeatures()
         if let feature = features.filter("(ANY featureLinks.type == %@) AND (ANY featureLinks.linkedKey == %@)", LinkTypeRef.HealthAttributeType.rawValue, self.healthAttributeType.rawValue).first {
         let eventType = feature.eventType() ?? .Unknown
         return eventType
         }

         * Getting the first item from the results is not recommended
         * We need to check the features (results) by comparing featureLink.type
         * and featureLink.linkedKey
         */
            for feature in features{
                for featureLink in feature.featureLinks{
                    if featureLink.type.rawValue == LinkTypeRef.HealthAttributeType.rawValue
                    && featureLink.linkedKey.value == self.healthAttributeType.rawValue{
                        return feature.eventType() ?? .Unknown
                    }
                }
            }
        return .Unknown
    }

    public static func deleteAll() {
        let realm = DataProvider.newVHCRealm()
        try! realm.write {
            realm.delete(realm.allVHCCapturedResults())
        }
    }
}

extension Realm {
    public func captureResult(type: PartyAttributeTypeRef,
                              date: Date = Date(),
                              unitOfMeasureType: UnitOfMeasureRef = .Unknown,
                              value: Double? = nil,
                              isValid: Bool,
                              rawInput: String?,
                              inputUnitSymbol: String? = nil) {
        let capturedResult = VHCCapturedResult()
        try! self.write {
            capturedResult.isValid = isValid
            capturedResult.dateCaptured = date
            capturedResult.unitOfMeasureType = unitOfMeasureType
            capturedResult.value.value = value
            capturedResult.healthAttributeType = type
            capturedResult.rawInput = rawInput
            capturedResult.inputUnitSymbol = inputUnitSymbol

            self.create(VHCCapturedResult.self, value: capturedResult, update: true)
            self.add(capturedResult, update: true)
        }
    }

    public func allVHCCapturedResults() -> Results<VHCCapturedResult> {
        return self.objects(VHCCapturedResult.self)
    }

    public func vhcCapturedResult(of type: PartyAttributeTypeRef) -> VHCCapturedResult? {
        let attributes = self.allVHCCapturedResults()
        let filtered = attributes.filter("healthAttributeType == %@", type.rawValue).first
        return filtered
    }
}
