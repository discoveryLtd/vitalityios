import Foundation
import RealmSwift
import VIAUtilities

public class VHCHealthAttributeGroup: Object {
    @objc public dynamic var type: ProductFeatureTypeRef = ProductFeatureTypeRef.Unknown

    public let healthAttributes = List<VHCHealthAttribute>()

    override public static func primaryKey() -> String? {
        return "type"
    }

    public func containsBothBloodPressureMeasurables() -> Bool {
        let containsSystolic = self.healthAttributes.contains(where: { $0.type == .BloodPressureSystol })
        let containsDiastolic = self.healthAttributes.contains(where: { $0.type == .BloodPressureDiasto })
        return containsSystolic && containsDiastolic
    }
    
    public func containsBothBMIMeasurables() -> Bool {
        let containsHeight = self.healthAttributes.contains(where: { $0.type == .Height })
        let containsWeight = self.healthAttributes.contains(where: { $0.type == .Weight })
        return containsHeight && containsWeight
    }

    public func healthAttribute(of type: PartyAttributeTypeRef) -> VHCHealthAttribute? {
        return self.healthAttributes.filter({ $0.type == type }).first
    }

}

extension Realm {
    public func allVHCHealthAttributeGroups() -> Results<VHCHealthAttributeGroup> {
        return self.objects(VHCHealthAttributeGroup.self)
    }

    public func vhcHealthAttributeGroups(from productFeatureType: ProductFeatureTypeRef) -> Results<VHCHealthAttributeGroup> {
        return self.objects(VHCHealthAttributeGroup.self).filter("type == %@", productFeatureType.rawValue)
    }

    public func vhcHealthAttributeGroup(from productFeatureType: ProductFeatureTypeRef) -> VHCHealthAttributeGroup? {
        return vhcHealthAttributeGroups(from: productFeatureType).first
    }

    public func vhcHealthAttributeGroup(from healthAttributeType: PartyAttributeTypeRef) -> VHCHealthAttributeGroup? {
        let groups = self.allVHCHealthAttributeGroups()
        for group in groups {
            for healthAttribute in group.healthAttributes {
                if (healthAttribute.type.rawValue == healthAttributeType.rawValue) {
                    return group
                }
            }
        }
        return nil
    }
}
