import Foundation
import RealmSwift
import VIAUtilities

public class VHCHealthAttributeReading: Object {
    //TODO:- Use enums for type
    @objc public dynamic var healthAttributeTypeCode: String = ""
    @objc public dynamic var healthAttributeTypeName: String = ""
    @objc public dynamic var healthAttributeTypeKey: Int = 0
    @objc public dynamic var measuredOn: Date?
    @objc public dynamic var unitOfMeasure: String = ""
    public let value = RealmOptional<Double>()
    @objc public dynamic var rawValue: String = ""

    public let healthAttributeFeedbacks = List<VHCHealthAttributeFeedback>()

    public let event = LinkingObjects(fromType: VHCEvent.self, property: "healthAttributeReadings")
}

extension Realm {
    public func allVHCHealthAttributeReadings() -> Results<VHCHealthAttributeReading> {
        return self.objects(VHCHealthAttributeReading.self)
    }

    public func allVHCHealthAttributeReadings(for healthAttributeType: PartyAttributeTypeRef) -> Results<VHCHealthAttributeReading> {
        let readings = self.allVHCHealthAttributeReadings()
        let filtered = readings.filter("healthAttributeTypeKey == %@", healthAttributeType.rawValue).sorted(byKeyPath: "measuredOn", ascending: false)
        return filtered
    }

    public func latestVHCHealthAttributeReading(for healthAttributeType: PartyAttributeTypeRef) -> VHCHealthAttributeReading? {
        guard let latestEvent = latestVHCEventWithReadingAndEventMetadata(for: healthAttributeType) else { return nil }
        guard let latestReading = latestEvent.healthAttributeReadings.filter("healthAttributeTypeKey == %@", healthAttributeType.rawValue).sorted(byKeyPath: "measuredOn", ascending: false).first else {return nil }

        return latestReading
    }


    public func latestVHCHealthAttributeReadingFeedback(for healthAttributeType: PartyAttributeTypeRef) -> List<VHCHealthAttributeFeedback>? {

        guard let latestReading = latestVHCHealthAttributeReading(for: healthAttributeType) else { return nil }

        return latestReading.healthAttributeFeedbacks
    }

}
