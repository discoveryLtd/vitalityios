import Foundation
import RealmSwift
import VIAUtilities

public class VHCEventPointsEntry: Object {
    @objc public dynamic var typeKey: Int = 0
    @objc public dynamic var typeName: String = ""
    @objc public dynamic var typeCode: String = ""

    @objc public dynamic var categoryKey: Int = 0
    @objc public dynamic var categoryName: String = ""
    @objc public dynamic var categoryCode: String = ""

    @objc public dynamic var id: Int = 0
    @objc public dynamic var preLimitValue: Int = 0
    @objc public dynamic var potentialValue: Int = 0
    @objc public dynamic var earnedValue: Int = 0

    public let reasons = List<VHCPointsReason>()
}

extension Realm {
    public func allVHCPointsEntries() -> Results<VHCEventPointsEntry> {
        return self.objects(VHCEventPointsEntry.self)
    }
}
