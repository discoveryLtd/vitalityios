import Foundation
import RealmSwift
import VIAUtilities

public class VHCHealthAttribute: Object {
    @objc public dynamic var typeName: String = ""
    @objc public dynamic var type: PartyAttributeTypeRef = .Unknown
    public let validValues = List<VHCHealthAttributeValidValues>()
    public let group = LinkingObjects(fromType: VHCHealthAttributeGroup.self, property: "healthAttributes")
    public let eventType = LinkingObjects(fromType: VHCEventType.self, property: "healthAttributes")

    override public static func primaryKey() -> String? {
        return "type"
    }
}

extension Realm {

    public func allVHCHealthAttributes() -> Results<VHCHealthAttribute> {
        return self.objects(VHCHealthAttribute.self)
    }

    public func vhcHealthAttribute(of type: PartyAttributeTypeRef) -> VHCHealthAttribute? {
        let attributes = self.allVHCHealthAttributes()
        let filtered = attributes.filter("type == %@", type.rawValue).first
        return filtered
    }

    public func vhcHealthAttributeTypeName(for attributeRawValue: Int) -> String? {
        if let attributeType = PartyAttributeTypeRef(rawValue: attributeRawValue) {
            if let vhcHealthAttribute: VHCHealthAttribute = self.vhcHealthAttribute(of: attributeType) {
                return vhcHealthAttribute.typeName
            }
        }
        return nil
    }
}
