import Foundation
import RealmSwift
import VIAUtilities

public class VHCEvent: Object {

    @objc public dynamic var eventDateTime: Date?
    @objc public dynamic var eventId: Int = 0
    @objc public dynamic var eventSource: VHCEventSource?
    @objc public dynamic var reasonName: String = ""
    @objc public dynamic var reasonKey: Int = 0
    @objc public dynamic var reasonCode: String = ""
    public let eventMetaData = List<VHCEventMetaData>()
    public let healthAttributeReadings = List<VHCHealthAttributeReading>()
    public let pointsEntries = List<VHCEventPointsEntry>()
    public let eventType = LinkingObjects(fromType: VHCEventType.self, property: "events")

}

extension Realm {
    public func allVHCEvents() -> Results<VHCEvent> {
        return self.objects(VHCEvent.self).sorted(byKeyPath: "eventDateTime", ascending: false)
    }

    public func latestVHCEventWithReadingAndEventMetadata(for healthAttributeType: PartyAttributeTypeRef) -> VHCEvent? {
        let latestEventsWithReadingsForHealthAttribute = allVHCEvents().filter("ANY healthAttributeReadings.healthAttributeTypeKey = %@", healthAttributeType.rawValue)
        let latestEventWithReadingAndEventMetadata = latestEventsWithReadingsForHealthAttribute.filter("eventMetaData.@count > 0").first
        guard latestEventWithReadingAndEventMetadata?.eventMetaData.first?.rawValue != nil else { return nil }
        return latestEventWithReadingAndEventMetadata
    }

    public func latestVHCEventDateTime(for healthAttributeType: PartyAttributeTypeRef) -> Date? {

        guard let latestEvent = latestVHCEventWithReadingAndEventMetadata(for: healthAttributeType) else { return nil }

        return latestEvent.eventDateTime
    }


    public func eventSourceFromLatestVHCEvent(for healthAttributeType: PartyAttributeTypeRef) -> VHCEventSource? {
        guard let latestEvent = latestVHCEventWithReadingAndEventMetadata(for: healthAttributeType) else { return nil }

        return latestEvent.eventSource
    }

    public func pointsReasonFromLatestVHCEvent(for healthAttributeType: PartyAttributeTypeRef) -> String? {
        guard let latestEvent = latestVHCEventWithReadingAndEventMetadata(for: healthAttributeType) else { return nil }

        return latestEvent.eventType.first?.reasonName
    }

    public func potentialPointsFromLatestVHCEvent(for healthAttributeType: PartyAttributeTypeRef) -> Int? {
        guard  let latestEvent = latestVHCEventWithReadingAndEventMetadata(for: healthAttributeType) else { return nil }
        return latestEvent.eventType.first?.totalPotentialPoints
    }

    public func earnedPointsFromLatestVHCEvent(for healthAttributeType: PartyAttributeTypeRef) -> Int? {
        guard  let latestEvent = latestVHCEventWithReadingAndEventMetadata(for: healthAttributeType) else { return nil }
        return latestEvent.eventType.first?.totalEarnedPoints
    }
}
