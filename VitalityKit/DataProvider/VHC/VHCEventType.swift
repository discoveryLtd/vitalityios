import Foundation
import RealmSwift
import VIAUtilities

public class VHCEventType: Object {
    @objc public dynamic var totalEarnedPoints: Int = 0
    @objc public dynamic var totalPotentialPoints: Int = 0
    @objc public dynamic var typeName: String = ""
    @objc public dynamic var type: EventTypeRef = EventTypeRef.Unknown
    @objc public dynamic var reasonCode: String?
    @objc public dynamic var reasonName: String?
    public let reasonKey = RealmOptional<Int>()

    public let events = List<VHCEvent>()
    public let healthAttributes = List<VHCHealthAttribute>()

    override public static func primaryKey() -> String? {
        return "type"
    }
}

extension Realm {
    public func allVHCPotentialPointsEventTypes() -> Results<VHCEventType> {
        return self.objects(VHCEventType.self)
    }

    public func eventType(for eventTypeRef: EventTypeRef) -> VHCEventType? {
         let eventTypes = self.objects(VHCEventType.self)
        let filtered = eventTypes.filter("type == %@", eventTypeRef.rawValue)
        return filtered.first
    }

    public func pointsReason(for eventTypeRef: EventTypeRef) -> String? {
        guard let eventType = eventType(for: eventTypeRef) else { return nil }
        return eventType.reasonName
    }

    public func pointsReasonKey(for eventTypeRef: EventTypeRef) -> Int? {
        guard let eventType = eventType(for: eventTypeRef) else { return nil }
        return eventType.reasonKey.value
    }

    public func potentialPoints(for eventTypeRef: EventTypeRef) -> Int? {
        guard let eventType = eventType(for: eventTypeRef) else { return nil }
        return eventType.totalPotentialPoints
    }

    public func earnedPoints(for eventTypeRef: EventTypeRef) -> Int? {
        guard let eventType = eventType(for: eventTypeRef) else { return nil }
        return eventType.totalEarnedPoints
    }

}
