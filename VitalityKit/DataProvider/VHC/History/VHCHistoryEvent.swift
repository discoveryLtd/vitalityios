//
//  VHCHistoryEvent.swift
//  VitalityKit
//
//  Created by Dexter Anthony Ambrad on 1/17/19.
//  Copyright © 2019 Glucode. All rights reserved.
//

import Foundation
import RealmSwift
import VIAUtilities

public class VHCHistoryEvent: Object {
    
    @objc public dynamic var categoryCode: String?
    @objc public dynamic var categoryKey: Int = 0
    @objc public dynamic var categoryName: String?
    @objc public dynamic var dateLogged: Date = Date.distantPast
    @objc public dynamic var eventDateTime: Date?
    @objc public dynamic var eventSourceCode: String?
    @objc public dynamic var eventSourceKey: Int = 0
    @objc public dynamic var eventSourceName: String?
    @objc public dynamic var id: Int = 0
    @objc public dynamic var partyId: Int = 0
    @objc public dynamic var reportedBy: Int = 0
    @objc public dynamic var typeCode: String?
    @objc public dynamic var typeKey: Int = 0
    @objc public dynamic var typeName: String?
    public var associatedEvents = List<VHCAssociatedEvents>()
    public let eventExternalReference = List<VHCEventExternalReference>()
    public let eventMetaDataOuts = List<VHCEventMetaDataOuts>()
    
    override public static func primaryKey() -> String? {
        return "id"
    }
}

extension Realm {
    public func allVHCHistoryEvents() -> Results<VHCHistoryEvent> {
        return self.objects(VHCHistoryEvent.self)
    }
}

