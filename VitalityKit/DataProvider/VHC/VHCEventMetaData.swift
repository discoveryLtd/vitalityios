//
//  VHCEventMetaData.swift
//  VitalityActive
//
//  Created by Lionel Camacho on 2017/03/15.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import RealmSwift
import VIAUtilities

public class VHCEventMetaData: Object {
    @objc public dynamic var code: String = ""
    @objc public dynamic var key: Int = 0
    @objc public dynamic var name: String = ""
    @objc public dynamic var note: String = ""
    @objc public dynamic var unitOfMeasure: String = ""
    public let reading = RealmOptional<Double>()
    @objc public dynamic var rawValue: String?
}

extension Realm {
    public func allVHCEventMetaData() -> Results<VHCEventMetaData> {
        return self.objects(VHCEventMetaData.self)
    }

    public func latestVHCEventMetadata(for healthAttributeType: PartyAttributeTypeRef) -> VHCEventMetaData? {
        let latestEvent = self.latestVHCEventWithReadingAndEventMetadata(for: healthAttributeType)

        guard let latestEventMetaData = latestEvent?.eventMetaData.first  else { return nil }

        return latestEventMetaData
    }

    public func latestReadingValueFromEventMetadata(for healthAttributeType: PartyAttributeTypeRef) -> Double? {
        guard let latestEventMetaData = latestVHCEventMetadata(for: healthAttributeType) else { return nil }

        return latestEventMetaData.reading.value
    }

        public func latestEventMetadataRawValue(for healthAttributeType: PartyAttributeTypeRef) -> String {

             guard let latestEventMetaDataRawValue = latestVHCEventMetadata(for: healthAttributeType)?.rawValue else { return "" }

            return latestEventMetaDataRawValue
        }

}
