import UIKit
import RealmSwift
import SwiftDate

public class VHCCache: Object {
    @objc public dynamic var dateCreated: Date = Date()
}

extension Realm {
    public class func resetVHCRealmIfOutdated() {
        let realm = DataProvider.newVHCRealm()
        realm.reset()
        if VHCCache.isVHCDataOutdated() {
            let vhrCache = VHCCache()
            try! realm.write {
                realm.add(vhrCache)
            }
        }
    }

    public class func resetVHCRealm() {
        let realm = DataProvider.newVHCRealm()
        realm.reset()
    }

    public class func deleteCurrentVHCCache() {
        let realm = DataProvider.newVHCRealm()
        try! realm.write {
            guard let cache = VHCCache.currentVHCCache() else { return }
            realm.delete(cache)
        }
    }
}

extension VHCCache {

    public class func isVHCDataOutdated(_ daysValid: Int = 1) -> Bool {
        guard let vhcCache = VHCCache.currentVHCCache() else {
            return true
        }
        let calendar = Calendar.current
        let cachedDate = calendar.startOfDay(for: vhcCache.dateCreated)
        let todaysDate = calendar.startOfDay(for: Date())

        let components = calendar.dateComponents([.day], from: cachedDate, to: todaysDate)

        if let daysDifference = components.day {
            if daysDifference > daysValid {
                return true
            } else {
                return false
            }
        } else {
            return true
        }
    }

    public class func currentVHCCache() -> VHCCache? {
        let realm = DataProvider.newVHCRealm()
        return realm.objects(VHCCache.self).first
    }
}
