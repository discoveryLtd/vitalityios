//
//  VHCEventMetaDataOuts.swift
//  VitalityKit
//
//  Created by Dexter Anthony Ambrad on 1/17/19.
//  Copyright © 2019 Glucode. All rights reserved.
//

import Foundation
import RealmSwift
import VIAUtilities

public class VHCEventMetaDataOuts: Object {
    
    @objc public dynamic var typeCode: String?
    @objc public dynamic var typeKey: Int = 0
    @objc public dynamic var typeName: String?
    @objc public dynamic var unitOfMeasure: String?
    @objc public dynamic var value: String?
    
}

extension Realm {
    public func allVHCEventMetaDataOuts() -> Results<VHCEventMetaDataOuts> {
        return self.objects(VHCEventMetaDataOuts.self)
    }
}
