import Foundation
import RealmSwift
import VIAUtilities

public class VHCPointsReason: Object {
    @objc public dynamic var reasonKey: Int = 0
    @objc public dynamic var reasonName: String = ""
    @objc public dynamic var reasonCode: String = ""

    @objc public dynamic var categoryKey: Int = 0
    @objc public dynamic var categoryName: String = ""
    @objc public dynamic var categoryCode: String = ""
}

extension Realm {
    public func allVHCPointsReasons() -> Results<VHCPointsReason> {
        return self.objects(VHCPointsReason.self)
    }
}
