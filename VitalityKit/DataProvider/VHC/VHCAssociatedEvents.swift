//
//  VHCAssociatedEvents.swift
//  VitalityKit
//
//  Created by Dexter Anthony Ambrad on 1/17/19.
//  Copyright © 2019 Glucode. All rights reserved.
//

import Foundation
import RealmSwift
import VIAUtilities

public class VHCAssociatedEvents: Object {
    
    @objc public dynamic var associationTypeCode: String?
    @objc public dynamic var associationTypeKey: Int = 0
    @objc public dynamic var associationTypeName: String?
    
    @objc public dynamic var dateLogged: String?
    @objc public dynamic var dateTimeAssociated: String?
    @objc public dynamic var eventDateTime: String?
    @objc public dynamic var eventId: Int = 0
    @objc public dynamic var eventSourceCode: String?
    @objc public dynamic var eventSourceKey: Int = 0
    @objc public dynamic var eventSourceName: String?
    
    @objc public dynamic var categoryCode: String?
    @objc public dynamic var categoryKey: Int = 0
    @objc public dynamic var categoryName: String?
    
    @objc public dynamic var eventTypeCode: String?
    @objc public dynamic var eventTypeKey: Int = 0
    @objc public dynamic var eventTypeName: String?
    
}

extension Realm {
    public func allVHCAssociatedEvents() -> Results<VHCAssociatedEvents> {
        return self.objects(VHCAssociatedEvents.self)
    }
}
