import Foundation
import RealmSwift
import VIAUtilities

public class SAVEvent: Object {
    
    @objc public dynamic var eventDateTime: Date?
    @objc public dynamic var eventId: Int = 0
    @objc public dynamic var eventSource: SAVEventSource?
    @objc public dynamic var reasonName: String = ""
    @objc public dynamic var reasonKey: Int = 0
    @objc public dynamic var reasonCode: String = ""
    public let eventMetaData = List<SAVEventMetaData>()
    public let pointsEntries = List<SAVEventPointsEntry>()
    public let eventType = LinkingObjects(fromType: SAVEventType.self, property: "events")
    
}

extension Realm {
    public func allSAVEvents() -> Results<SAVEvent> {
        return self.objects(SAVEvent.self).sorted(byKeyPath: "eventDateTime", ascending: false)
    }
}
