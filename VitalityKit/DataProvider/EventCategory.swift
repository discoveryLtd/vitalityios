//
//  EventCategory.swift
//  VitalityActive
//
//  Created by OJ Garde on 28/11/2017.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import RealmSwift
import VIAUtilities

public class EventCategory: Object {
    @objc public dynamic var name: String = ""
    @objc public dynamic var categoryId: EventCategoryRef = .Unknown
    @objc public dynamic var reference: Int = 0
    
    override public static func primaryKey() -> String? {
        return "reference"
    }
}

extension EventCategory {
    public class func setupEventCategory(name: String, categoryId: EventCategoryRef) {
        let category = EventCategory()
        category.name = name
        category.categoryId = categoryId
        category.reference = categoryId.rawValue
        
        let realm = DataProvider.newRealm()
        try! realm.write {
            debugPrint("Saving category \(name) with ID \(categoryId.rawValue)")
            realm.add(category)
        }
        
    }
}
extension Realm {
    public func allEventCategories() -> Results<EventCategory> {
        return self.objects(EventCategory.self)
    }
    
    public func eventCategory(with reference: EventCategoryRef) -> EventCategory? {
        let categories = self.objects(EventCategory.self)
        return categories.filter("categoryId == %@", reference.rawValue).first
    }
    
    public func allEventCategories(excluding references: [EventCategoryRef]) -> Results<EventCategory> {
        let categories = self.objects(EventCategory.self)
        let predicate = NSPredicate(format: "NOT categoryId IN %@", references.map({$0.rawValue}))
        let categoryFiltered = categories.filter(predicate)
        
        return categoryFiltered
    }
    
    public func allEventCategories(including references: [EventCategoryRef]) -> Results<EventCategory> {
        let categories = self.objects(EventCategory.self)
        let predicate = NSPredicate(format: "categoryId IN %@", references.map({$0.rawValue}))
        let categoryFiltered = categories.filter(predicate)
        
        return categoryFiltered
    }
    
    public func areSelectedCategories(ofType categoryName: String, selectedCategories: [EventCategoryRef]) -> Bool{
        let categories = self.objects(EventCategory.self)
        let predicate = NSPredicate(format: "name == '%@' AND categoryId IN %@", categoryName, selectedCategories.map({$0.rawValue}))
        let categoryFiltered = categories.filter(predicate)
        
        return categoryFiltered.count > 0
        
    }
}
