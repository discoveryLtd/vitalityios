import Foundation
import VIAUtilities
import RealmSwift

public class HAReportedHealthAttribute: Object {

    @objc public dynamic var measurementUnitId: Int = 0
    @objc public dynamic var typeCode: String? = ""
    @objc public dynamic var attributeType: PartyAttributeTypeRef = PartyAttributeTypeRef.Unknown
    @objc public dynamic var typeName: String? = ""
    @objc public dynamic var value: String = ""
    @objc public dynamic var sourceEventId: Int = 0
    public let feedbacks: List<HAReportedFeedback> = List<HAReportedFeedback>()

    override public static func primaryKey() -> String? {
        return "attributeType"
    }

    public func feedbackWith(keys: [PartyAttributeFeedbackRef]) -> HAReportedFeedback? {
        
        let rawMap = keys.map { (feedbackRef) -> Int in
            return feedbackRef.rawValue
        }
        
        return feedbacks.first { (feedback) -> Bool in
            return rawMap.contains(feedback.key)
        }
    }

    internal static func saveToRealm(healthAttribute: HAReportedHealthAttribute) {

        let realm = DataProvider.newHealthAttributesRealm()
        let healthAttributes = realm.objects(HAReportedHealthAttribute.self)

        try! realm.write {

            var updated = false

            for ha in healthAttributes {
                if ha.attributeType == healthAttribute.attributeType {

                    ha.measurementUnitId = healthAttribute.measurementUnitId
                    ha.typeCode = healthAttribute.typeCode
                    ha.typeName = healthAttribute.typeName
                    ha.value = healthAttribute.value
                    ha.sourceEventId = healthAttribute.sourceEventId

                    ha.feedbacks.removeAll()
                    realm.add(healthAttribute.feedbacks, update: true)
                    ha.feedbacks.append(objectsIn: healthAttribute.feedbacks)

                    updated = true
                }
            }

            if !updated {
                realm.add(healthAttribute.feedbacks, update: true)
                realm.add(healthAttribute, update: true)
            }
        }
    }

    public static func hasValidVitalityAge() -> Bool {
        if let vitalityAge = HAReportedHealthAttribute.healthAttributeBy(attributeType: PartyAttributeTypeRef.VitalityAge) {
            let age = Double(vitalityAge.value)
            return age != nil
        }

        return false
    }

    public static func healthAttributeBy(attributeType: PartyAttributeTypeRef) -> HAReportedHealthAttribute? {
        let realm = DataProvider.newHealthAttributesRealm()
        let results = realm.objects(HAReportedHealthAttribute.self)

        return results.first(where: { (ha) -> Bool in
            return ha.attributeType == attributeType
        })
    }

    public static func buildHealthAttribute(data: HAFHealthAttribute) -> HAReportedHealthAttribute {
        let healthAttribute = HAReportedHealthAttribute()
        if let uom = data.unitofMeasure {
            if let uom = Int(uom) {
                healthAttribute.measurementUnitId = uom
            }
        }
        healthAttribute.typeCode = data.attributeTypeCode
        healthAttribute.typeName = data.attributeTypeName

        if let sourceEvent = data.sourceEventId {
            healthAttribute.sourceEventId = sourceEvent
        }

        healthAttribute.attributeType = PartyAttributeTypeRef.Unknown
        if let type = PartyAttributeTypeRef(rawValue: data.attributeTypeKey) {
            healthAttribute.attributeType = type
        }
        healthAttribute.value = data.value
        healthAttribute.feedbacks.append(objectsIn: HAReportedFeedback.buildFeedbacks(feedbacksP: data.healthAttributeFeedbacks))

        return healthAttribute
    }

    public static func buildHealthAttribute(data: CAHealthAttributeMetadatas) -> HAReportedHealthAttribute {
        let healthAttribute = HAReportedHealthAttribute()
        if let measurementUnitId = data.measurementUnitId {
            healthAttribute.measurementUnitId = measurementUnitId
        }

        healthAttribute.typeCode = data.typeCode
        healthAttribute.attributeType = PartyAttributeTypeRef.Unknown
        if let type = PartyAttributeTypeRef(rawValue: data.typeKey) {
            healthAttribute.attributeType = type
        }
        healthAttribute.typeName = data.typeName
        healthAttribute.value = data.value
        healthAttribute.feedbacks.append(objectsIn: HAReportedFeedback.buildFeedbacks(feedbacksP: data.healthAttributeFeedbacks))
        return healthAttribute
    }

    public static func buildHealthAttributes(response: CaptureAssessmentResponse?) -> Results<HAReportedHealthAttribute>? {
        response?.healthAttributeMetadatas?.forEach({(ha) in HAReportedHealthAttribute.saveToRealm(healthAttribute: HAReportedHealthAttribute.buildHealthAttribute(data: ha))})
        return DataProvider.newHealthAttributesRealm().objects(HAReportedHealthAttribute.self)
    }

    public static func buildHealthAttributes(response: GetHealthAttributeFeedbackResponse?) -> Results<HAReportedHealthAttribute>? {

        let sorted = response?.healthAttribute?.sorted(by: { (this, that) -> Bool in
            return this.sourceEventId! < that.sourceEventId!
        })

        sorted?.forEach({(ha) in HAReportedHealthAttribute.saveToRealm(healthAttribute: HAReportedHealthAttribute.buildHealthAttribute(data: ha))})
        return DataProvider.newHealthAttributesRealm().objects(HAReportedHealthAttribute.self)
    }

}

public class HAReportedFeedback: Object {
    @objc public dynamic var code: String? = ""
    @objc public dynamic var key: Int = -1
    @objc public dynamic var name: String = ""
    @objc public dynamic var typeCode: String? = ""
    @objc public dynamic var typeKey: Int = -1
    @objc public dynamic var typeName: String = ""

    override public static func primaryKey() -> String? {
        return "code"
    }

    public static func buildFeedback(feedbackP: HAFHealthAttributeFeedbacks) -> HAReportedFeedback {
        let feedback = HAReportedFeedback()
        feedback.code = feedbackP.feedbackTypeCode
        feedback.key = feedbackP.feedbackTypeKey
        feedback.name = feedbackP.feedbackTypeName
        feedback.typeCode = feedbackP.feedbackTypeTypeCode
        feedback.typeKey = feedbackP.feedbackTypeTypeKey
        feedback.typeName = feedbackP.feedbackTypeTypeName
        return feedback
    }

    public static func buildFeedback(feedbackP: CAHealthAttributeFeedbacks) -> HAReportedFeedback {
        let feedback = HAReportedFeedback()
        feedback.code = feedbackP.feedbackTypeCode
        feedback.key = feedbackP.feedbackTypeKey
        feedback.name = feedbackP.feedbackTypeName
        feedback.typeCode = feedbackP.feedbackTypeTypeCode
        feedback.typeKey = feedbackP.feedbackTypeTypeKey
        feedback.typeName = feedbackP.feedbackTypeTypeName
        return feedback
    }

    public static func buildFeedbacks(feedbacksP: [CAHealthAttributeFeedbacks]?) -> List<HAReportedFeedback> {
        let feedbacks = List<HAReportedFeedback>()
        if let feedbacksT = feedbacksP {
            feedbacks.append(objectsIn: feedbacksT.map({ (feedback) in HAReportedFeedback.buildFeedback(feedbackP: feedback)}))
        }
        return feedbacks
    }

    public static func buildFeedbacks(feedbacksP: [HAFHealthAttributeFeedbacks]?) -> List<HAReportedFeedback> {
        let feedbacks = List<HAReportedFeedback>()
        if let feedbacksT = feedbacksP {
            feedbacks.append(objectsIn: feedbacksT.map({ (feedback) in HAReportedFeedback.buildFeedback(feedbackP: feedback )}))
        }
        return feedbacks
    }
}
