//
//  MWBUnitOfMeasure.swift
//  VitalityActive
//
//  Created by Harry C. Lingad on 2/5/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import UIKit
import RealmSwift
import VIAUtilities

public class MWBUnitOfMeasure: Object {
    @objc public dynamic var value: String = ""
    
    public func unitOfMeasureRef() -> UnitOfMeasureRef? {
        if let rawValue = Int(self.value) {
            return UnitOfMeasureRef(rawValue: rawValue)
        }
        return nil
    }
}

extension Realm {
    public func allMWBUnitOfMeasures() -> Results<MWBUnitOfMeasure> {
        return self.objects(MWBUnitOfMeasure.self)
    }
}

