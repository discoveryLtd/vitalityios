//
//  SAVEventPointsEntry.swift
//  VitalityActive
//
//  Created by Michelle Oratil on 30/11/2017.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import RealmSwift
import VIAUtilities

public class SAVEventPointsEntry: Object {
    @objc public dynamic var typeKey: Int = 0
    @objc public dynamic var typeName: String = ""
    @objc public dynamic var typeCode: String = ""
    
    @objc public dynamic var categoryKey: Int = 0
    @objc public dynamic var categoryName: String = ""
    @objc public dynamic var categoryCode: String = ""
    
    @objc public dynamic var id: Int = 0
    @objc public dynamic var preLimitValue: Int = 0
    @objc public dynamic var potentialValue: Int = 0
    @objc public dynamic var earnedValue: Int = 0
    
    public let reasons = List<SAVPointsReason>()
}

extension Realm {
    public func allSAVPointsEntries() -> Results<SAVEventPointsEntry> {
        return self.objects(SAVEventPointsEntry.self)
    }
}
