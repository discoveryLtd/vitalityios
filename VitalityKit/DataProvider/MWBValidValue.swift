//
//  MWBValidValue.swift
//  VitalityActive
//
//  Created by Harry C. Lingad on 2/5/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import RealmSwift
import VIAUtilities

public class MWBValidValue: Object {
    @objc public dynamic var unitOfMeasure: UnitOfMeasureRef = UnitOfMeasureRef.Unknown
    @objc public dynamic var name: String = ""
    @objc public dynamic var value: String = ""
    @objc public dynamic var valueDescription: String = ""
    @objc public dynamic var valueNote: String = ""
    @objc public dynamic var type: QuestionValidValueTypeRef = QuestionValidValueTypeRef.Unknown
    @objc public dynamic var typeName: String?
    @objc public dynamic var typeCode: String?
}

extension Realm {
    public func allMWBValidValues() -> Results<MWBValidValue> {
        return self.objects(MWBValidValue.self)
    }
}

