//
//  SAVEventMetaData.swift
//  VitalityActive
//
//  Created by Michelle Oratil on 30/11/2017.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import RealmSwift
import VIAUtilities

public class SAVEventMetaData: Object {
    @objc public dynamic var code: String = ""
    @objc public dynamic var key: Int = 0
    @objc public dynamic var name: String = ""
    @objc public dynamic var note: String = ""
    @objc public dynamic var unitOfMeasure: String = ""
    public let reading = RealmOptional<Double>()
    @objc public dynamic var rawValue: String?
}

extension Realm {
    public func allSAVEventMetaData() -> Results<SAVEventMetaData> {
        return self.objects(SAVEventMetaData.self)
    }    
}
