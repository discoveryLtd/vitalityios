//
//  SAVEventTypeGroup.swift
//  VitalityActive
//
//  Created by Von Kervin R. Tuico on 10/01/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import RealmSwift
import VIAUtilities

public class SAVEventTypeGroup: Object {
    @objc public dynamic var type: ProductFeatureTypeRef = ProductFeatureTypeRef.Unknown
    
    public let eventTypes = List<SAVEventType>()
    
    override public static func primaryKey() -> String? {
        return "type"
    }
    
    public func eventType(of type: EventTypeRef) -> SAVEventType? {
        return self.eventTypes.filter({ $0.type == type }).first
    }
    
}

extension Realm {
    public func allSAVEventTypeGroups() -> Results<SAVEventTypeGroup> {
        return self.objects(SAVEventTypeGroup.self)
    }
    
    public func savEventTypeGroups(from productFeatureType: ProductFeatureTypeRef) -> Results<SAVEventTypeGroup> {
        return self.objects(SAVEventTypeGroup.self).filter("type == %@", productFeatureType.rawValue)
    }
    
    public func savEventTypeGroup(from productFeatureType: ProductFeatureTypeRef) -> SAVEventTypeGroup? {
        return savEventTypeGroups(from: productFeatureType).first
    }
    
    public func savEventTypeGroup(from type: EventTypeRef) -> SAVEventTypeGroup? {
        let groups = self.allSAVEventTypeGroups()
        for group in groups {
            for eventType in group.eventTypes {
                if (eventType.type.rawValue == type.rawValue) {
                    return group
                }
            }
        }
        return nil
    }
}
