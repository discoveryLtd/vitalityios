////
////  ActiveRewardsReward.swift
////  VitalityActive
////
////  Created by Joshua Ryan on 6/27/17.
////  Copyright © 2017 Glucode. All rights reserved.
////
//
//import Foundation
//import RealmSwift
//
//// MARK: ARRewardCredit
//// An earned credit toward a Spin or Choice
//public class ARRewardCredit: Object {
//	@objc public dynamic var id: Int = 0
//	@objc public dynamic var dateEarned: Date?
//	@objc public dynamic var dateExpiration: Date?
//	@objc public dynamic var voucher: ARVoucher?
//
//	override public static func primaryKey() -> String? {
//		return "id"
//	}
//}
//
//extension Realm {
//	public func allActiveRewardsRewardCredits() -> Results<ARRewardCredit> {
//		let predicate = NSPredicate(format: "dateExpiration > %@ AND (voucher == nil OR voucher.dateConfirmed == nil)", NSDate())
//		return self.objects(ARRewardCredit.self).filter(predicate)
//	}
//}
//
//// MARK: ARReward
//// Available reward options for spin / selection
//public class ARReward: Object {
//	@objc public dynamic var id: Int = 0
//	@objc public dynamic var vendor: String?
//	@objc public dynamic var title: String?
//	@objc public dynamic var info: String?
//	@objc public dynamic var value: Double = 0
//	@objc public dynamic var instructions: String?
//	@objc public dynamic var imageUrl: String?
//	@objc public dynamic var termsUrl: String?
//	@objc public dynamic var available: Bool = false
//	@objc public dynamic var bespokeRewardBoardId: String?
//
//	override public static func primaryKey() -> String? {
//		return "id"
//	}
//}
//
//extension Realm {
//	public func allActiveRewardsRewardOptions() -> Results<ARReward> {
//		let predicate = NSPredicate(format: "available == true")
//		return self.objects(ARReward.self).filter(predicate)
//	}
//}
//
//// MARK: ARVoucher
//// An tangible usable / used reward instance
//public class ARVoucher: Object {
//	@objc public dynamic var id: Int = 0
//	@objc public dynamic var reward: ARReward?
//	@objc public dynamic var code: String?
//	@objc public dynamic var dateSelected: Date?
//	@objc public dynamic var dateExpiration: Date?
//	@objc public dynamic var dateConfirmed: Date?
//	@objc public dynamic var dateRedeemed: Date?
//
//	override public static func primaryKey() -> String? {
//		return "id"
//	}
//}
//
//extension Realm {
//	public func allActiveRewardsVoucher() -> Results<ARVoucher> {
//		return self.objects(ARVoucher.self)
//	}
//
//	public func allUsedRewardsVoucher() -> Results<ARVoucher> {
//		let predicate = NSPredicate(format: "dateExpiration < %@ OR dateRedeemed != nil", NSDate())
//		return self.objects(ARVoucher.self).filter(predicate)
//	}
//
//	public func allUnusedRewardsVoucher() -> Results<ARVoucher> {
//		let predicate = NSPredicate(format: "dateExpiration > %@ AND dateRedeemed == nil", NSDate())
//		return self.objects(ARVoucher.self).filter(predicate)
//	}
//}

