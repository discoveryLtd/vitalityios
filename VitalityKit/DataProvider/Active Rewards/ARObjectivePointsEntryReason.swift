import Foundation
import RealmSwift
import VIAUtilities

public class ARObjectivePointsEntryReason: Object {
    
    @objc public dynamic var reasonKey: ReasonRef = ReasonRef.Unknown
    @objc public dynamic var reasonCode: String?
    @objc public dynamic var reasonName: String?

    @objc public dynamic var categoryKey: ReasonCategoryRef = ReasonCategoryRef.Unknown
    @objc public dynamic var categoryCode: String?
    @objc public dynamic var categoryName: String?
    
}

extension Realm {
    public func allARObjectivePointsEntryReasons() -> Results<ARObjectivePointsEntryReason> {
        return self.objects(ARObjectivePointsEntryReason.self)
    }
}
