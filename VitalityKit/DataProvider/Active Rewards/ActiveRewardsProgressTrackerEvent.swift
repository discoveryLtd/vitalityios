//
//  ProgressTrackerEvent.swift
//  VitalityActive
//
//  Created by Lionel Camacho on 2017/01/31.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import RealmSwift

public class ActiveRewardsProgressTrackerEvent: Object {
    @objc public dynamic var goalTypeCode: String = ""
    @objc public dynamic var goalTypeKey: Int = 0
    @objc public dynamic var points: Int = 0
    public let pointsEntry = LinkingObjects(fromType: PointsEntry.self, property: "activeRewardsProgressTrackerEvents")
}

extension Realm {
    public func allActiveRewardsProgressTrackerEvents() -> Results<ActiveRewardsProgressTrackerEvent> {
        return self.objects(ActiveRewardsProgressTrackerEvent.self)
    }
}
