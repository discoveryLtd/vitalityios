import Foundation
import RealmSwift
import VIAUtilities

public class ARObjectiveTracker: Object {

    @objc public dynamic var objectiveKey: ObjectiveRef = ObjectiveRef.Unknown
    @objc public dynamic var objectiveCode: String?
    @objc public dynamic var objectiveName: String?

    @objc public dynamic var status: ObjectiveTrackerStatusRef = ObjectiveTrackerStatusRef.Unknown
    @objc public dynamic var statusCode: String?
    @objc public dynamic var statusName: String?

    public let eventOutcomeAchieved = RealmOptional<Bool>()
    public let eventCountAchieved = RealmOptional<Int>()
    public let eventCountTarget = RealmOptional<Int>()
    @objc public dynamic var eventOutcomeTarget: String?

    public let pointsAchieved = RealmOptional<Int>()
    public let pointsTarget = RealmOptional<Int>()
    public let percentageCompleted = RealmOptional<Int>()

    @objc public dynamic var effectiveFrom = Date()
    @objc public dynamic var effectiveTo = Date()
    @objc public dynamic var monitorUntil = Date()
    @objc public dynamic var statusChangedOn = Date()

    public let objectivePointsEntries = List<ARObjectivePointsEntry>()
    public let objectiveEvents = List<ARObjectiveEvent>()

}

extension Realm {
    public func allARObjectiveTrackers() -> Results<ARObjectiveTracker> {
        return self.objects(ARObjectiveTracker.self)
    }
}
