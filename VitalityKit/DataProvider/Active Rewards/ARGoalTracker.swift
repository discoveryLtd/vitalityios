import Foundation
import RealmSwift
import VIAUtilities

public class ARGoalTracker: Object, EffectiveDateFilterable {

    @objc public dynamic var goalKey: GoalRef = GoalRef.Unknown
    @objc public dynamic var goalCode: String?
    @objc public dynamic var goalName: String?

    @objc public dynamic var goalTrackerStatus: GoalTrackerStatusRef = GoalTrackerStatusRef.Unknown
    @objc public dynamic var goalTrackerStatusCode: String?
    @objc public dynamic var goalTrackerStatusName: String?

    @objc public dynamic var totalObjectives: Int = 0
    @objc public dynamic var completedObjectives: Int = 0
    
    @objc public dynamic var id: Int = -1
    @objc public dynamic var partyId: Int = -1
    @objc public dynamic var percentageCompleted: Int = 0

    @objc public dynamic var statusChangedOn = Date()
    @objc public dynamic var monitorUntil = Date()

    @objc public dynamic var effectiveFrom = Date()
    @objc public dynamic var effectiveTo = Date()

    public let objectiveTrackers = List<ARObjectiveTracker>()
    
    override public static func primaryKey() -> String? {
        return "id"
    }

    public func effectiveDateForFilter() -> Date {
        return self.effectiveTo
    }

    public func totalPotentialPoints() -> Int {
        let potentialPoints = Array(objectiveTrackers).reduce(0) {
            $0 + ($1.pointsTarget.value ?? 0)
        }
        return potentialPoints
    }
    
    public func totalPointsAcheived() -> Int {
        let pointsAcheived = Array(objectiveTrackers).reduce(0) {
            $0 + ($1.pointsAchieved.value ?? 0)
        }
        return pointsAcheived
    }
}

extension Realm {
    public func allARGoalTrackers() -> Results<ARGoalTracker> {
        return self.objects(ARGoalTracker.self)
    }
    
    public func allARGoalTrackersExcludingFutureGoals() -> Results<ARGoalTracker> {
        return allARGoalTrackers().filter("effectiveFrom <= %@", Date())
    }
    
    public func arGoalTracker(with id: Int) -> ARGoalTracker? {
        return self.objects(ARGoalTracker.self).filter("id == %@", id).first
    }
    
    public func firstGoalTrackersForThisCycle(with date: Date) -> ARGoalTracker? {
        let goals = Array(allARGoalTrackersExcludingFutureGoals())
        let goalsWithinCycle = goals.filter { goal in
            return goal.effectiveFrom.compare(to: date, granularity: .day) == date.compare(to: goal.effectiveTo, granularity: .day) || date.compare(to: goal.effectiveTo, granularity: .day) == .orderedSame || goal.effectiveFrom.compare(to: date, granularity: .day)  == .orderedSame
        }
        let sortedGoals = goalsWithinCycle.sorted(by: { $0.effectiveFrom < $1.effectiveFrom })
        return sortedGoals.first
    }
    
    public func firstFutureGoalTracker(after date: Date) -> ARGoalTracker? {
        let goals = Array(allARGoalTrackers())
        let goalsWithinCycle = goals.filter { goal in
            return date.compare(goal.effectiveFrom) == ComparisonResult.orderedAscending && date.compare(goal.effectiveTo) == ComparisonResult.orderedAscending
        }
        let sortedGoals = goalsWithinCycle.sorted(by: { $0.effectiveFrom < $1.effectiveFrom })
        return sortedGoals.first
    }
    
    public func currentGoalExists(for date: Date) -> Bool {
        for goal in Array(allARGoalTrackersExcludingFutureGoals()) {
            if goal.effectiveFrom.compare(to: date, granularity: .day) == date.compare(to: goal.effectiveTo, granularity: .day) || date.compare(to: goal.effectiveTo, granularity: .day) == .orderedSame || goal.effectiveFrom.compare(to: date, granularity: .day)  == .orderedSame {
                return true
            }
        }
        return false
    }
}
