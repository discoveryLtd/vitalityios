import Foundation
import RealmSwift
import VIAUtilities

public class ARObjectivePointsEntry: Object {

    @objc public dynamic var typeKey: PointsEntryTypeRef = PointsEntryTypeRef.Unknown
    @objc public dynamic var typeCode: String?
    @objc public dynamic var typeName: String?

    @objc public dynamic var statusTypeKey: PointsEntryStatusTypeRef = PointsEntryStatusTypeRef.Unknown
    @objc public dynamic var statusTypeCode: String?
    @objc public dynamic var statusTypeName: String?

    @objc public dynamic var categoryKey: PointsEntryCategoryRef = PointsEntryCategoryRef.Unknown
    @objc public dynamic var categoryCode: String?
    @objc public dynamic var categoryName: String?

    @objc public dynamic var eventId: Int = -1
    @objc public dynamic var id: Int = -1

    @objc public dynamic var earnedValue: Int = 0
    @objc public dynamic var potentialValue: Int = 0
    @objc public dynamic var prelimitValue: Int = 0
    @objc public dynamic var pointsContributed: Int = 0

    @objc public dynamic var systemAwareOn: String = ""

    @objc public dynamic var effectiveDate = Date()
    @objc public dynamic var statusChangeDate = Date()

    public let partyId = RealmOptional<Int>()

    public let objectivePointsEntryMetaDatas = List<ARObjectivePointsEntryMetaData>()
    public let objectivePointsEntryReasons = List<ARObjectivePointsEntryReason>()

    override public static func primaryKey() -> String? {
        return "id"
    }

    public func manufacturerName() -> String? {
        for metadata in self.objectivePointsEntryMetaDatas {
            if metadata.typeKey == EventMetaDataTypeRef.Manufacturer {
                return metadata.value
            }
        }
        return nil
    }
    
}

extension Realm {
    public func allARObjectivePointsEntries() -> Results<ARObjectivePointsEntry> {
        return self.objects(ARObjectivePointsEntry.self)
    }

    public func arObjectivePointsEntry(with id: Int) -> ARObjectivePointsEntry? {
        return self.objects(ARObjectivePointsEntry.self).filter("id == %@", id).first
    }
}
