import Foundation
import RealmSwift
import VIAUtilities

public class ARObjectiveEvent: Object {

    @objc public dynamic var typeKey: EventTypeRef = EventTypeRef.Unknown
    @objc public dynamic var typeCode: String?
    @objc public dynamic var typeName: String?

    @objc public dynamic var eventSourceKey: EventSourceRef = EventSourceRef.Unknown
    @objc public dynamic var eventSourceCode: String?
    @objc public dynamic var eventSourceName: String?

    @objc public dynamic var dateLogged: Date?
    @objc public dynamic var eventDateTime: Date?

    public let id = RealmOptional<Int>()
    public let partyId = RealmOptional<Int>()
    public let reportedBy = RealmOptional<Int>()

    public let objectiveEventMetaDatas = List<ARObjectiveEventMetaData>()

}

extension Realm {
    public func allARObjectiveEvents() -> Results<ARObjectiveEvent> {
        return self.objects(ARObjectiveEvent.self)
    }
}
