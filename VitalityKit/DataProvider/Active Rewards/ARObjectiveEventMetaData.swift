import Foundation
import RealmSwift
import VIAUtilities

public class ARObjectiveEventMetaData: Object, MetadataContainer {

    @objc public dynamic var typeKey: EventMetaDataTypeRef = EventMetaDataTypeRef.Unknown
    @objc public dynamic var typeCode: String?
    @objc public dynamic var typeName: String?

    @objc public dynamic var unitOfMeasureType: UnitOfMeasureRef = UnitOfMeasureRef.Unknown

    @objc public dynamic var value: String = ""

}

extension Realm {
    public func allARObjectiveEventMetaDatas() -> Results<ARObjectiveEventMetaData> {
        return self.objects(ARObjectiveEventMetaData.self)
    }
}
