import RealmSwift

public class ARGoalTracker: Object {
    @objc public dynamic var completedObjectives: Int = 0
    @objc public dynamic var typeCode: String = ""
    @objc public dynamic var typeKey: Int = -1
    @objc public dynamic var typeName: String = ""

    @objc public dynamic var monitorUntil: String = ""

    public let trackers: List<ARObjectiveTracker> = List<ARObjectiveTracker>()
    public let completionPercentage: RealmOptional<Int> = RealmOptional<Int>()

    @objc public dynamic var statusDate: String = ""
    @objc public dynamic var statusTypeCode: String = ""
    @objc public dynamic var statusTypeKey: Int = -1
    @objc public dynamic var statusTypeName: String = ""

    @objc public dynamic var totalObjectives: Int = 0
    @objc public dynamic var validFrom: String = ""
    @objc public dynamic var validTo: String = ""
}

public class ARObjectiveTracker: Object {
    // placeholder to get stuff to build
}
