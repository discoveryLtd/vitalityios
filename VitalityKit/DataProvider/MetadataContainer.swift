import Foundation
import VIAUtilities

public protocol MetadataContainer: class {
    
    var typeKey: EventMetaDataTypeRef { get }
    
    var unitOfMeasureType: UnitOfMeasureRef { get }
    
    var value: String { get }
    
    func format() -> String
    
}
