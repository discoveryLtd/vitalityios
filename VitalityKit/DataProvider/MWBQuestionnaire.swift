//
//  MWBQuestionnaire.swift
//  VitalityActive
//
//  Created by Harry C. Lingad on 2/5/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import UIKit
import RealmSwift
import VIAUtilities

@objc public enum MWBCompletionState: Int, EnumCollection {
    case Unknown = -1
    case NotStarted = 0
    case InProgress = 1
    case Complete = 2
}

public class MWBQuestionnaire: Object {
    // This is the section/s that a questionnaire may be broken down into. Some questionnaires may not have sections.
    public let questionnaireSections: List<MWBQuestionnaireSection> = List<MWBQuestionnaireSection>()
    @objc public dynamic var typeKey = Int()
    // Date the questionnaire was completed
    @objc public dynamic var completedOn: Date?
    @objc public dynamic var typeName: String?
    @objc public dynamic var completionFlag: Bool = false
    @objc public dynamic var typeCode: String = ""
    @objc public dynamic var currentSectionTypeKey = Int()
    internal dynamic var completionState: MWBCompletionState = MWBCompletionState.Unknown
    public let sortOrderIndex = RealmOptional<Int>()
    
    @objc public dynamic var text: String = ""
    @objc public dynamic var textDescription: String = ""
    @objc public dynamic var textNote: String = ""
    
    public func getCompletionState() -> MWBCompletionState {
        if self.completionState == MWBCompletionState.NotStarted {
            if hasUserEnteredCapturedResults() {
                return MWBCompletionState.InProgress
            }
        }
        return self.completionState
    }
    
    public func setCompletedAndSubmitted() {
        self.completionFlag = true
        self.updateCompletionState(.Complete)
    }
    
    public func updateCompletionState(_ state: MWBCompletionState) {
        self.completionState = state
    }
    
    public func sortedQuestionnaireSections() -> Results<MWBQuestionnaireSection> {
        return self.questionnaireSections.sorted(byKeyPath: "sortOrderIndex", ascending: true)
    }
    
    public func sortedVisibleQuestionnaireSections() -> Results<MWBQuestionnaireSection> {
        let sorted = self.questionnaireSections.sorted(byKeyPath: "sortOrderIndex", ascending: true).filter("isVisible == true")
        return sorted
    }
    
    static func reevaluateVisibilityTagsForAllCapturedResults(in realm: Realm) {
        let capturedResults = realm.allMWBCapturedResults()
        capturedResults.forEach({ result in
            if let answer = result.answer {
                MWBQuestionnaire.updateVisibilityTags(for: answer, for: result.questionTypeKey, in: realm)
            } else {
                debugPrint("No answer to use")
            }
        })
    }
    
    public static func updateVisibilityTags(for answer: String, for questionTypeKey: Int,
                                            in realm: Realm = DataProvider.newMWBRealm(),
                                            allowsMultipleAnswers: Bool = false) {
        let questionnaireSections = realm.allMWBQuestionnaireSections()
        let questions = realm.allMWBQuestions()
        try! realm.write {
            questionnaireSections.forEach({ $0.updateVisibility(basedOn: questionTypeKey, answer: answer,
                                                                allowsMultipleAnswers: allowsMultipleAnswers) })
            questions.forEach({ $0.updateVisibility(basedOn: questionTypeKey, answer: answer,
                                                    allowsMultipleAnswers: allowsMultipleAnswers) })
            updateAllQuestionsVisibilityBasedOnParentVisibility()
        }
    }
    
    public static func updateAllQuestionsVisibilityBasedOnParentVisibility() {
        let realm = DataProvider.newMWBRealm()
        let allQuestionsWithVisibilityTags = realm.allMWBQuestions().filter({ !$0.visibilityTag.isEmpty })
        var allQuestionTypeKeys: [Int] = allQuestionsWithVisibilityTags.map({ $0.typeKey })
        
        while allQuestionTypeKeys.count > 0 {
            guard let questionTypeKey = allQuestionTypeKeys.first else { return }
            guard let question = realm.mwbquestion(by: questionTypeKey) else { return }
            updateQuestionVisibilityRecursivelyBasedOnParentVisibility(question, keys: &allQuestionTypeKeys)
        }
    }
    
    public static func updateQuestionVisibilityRecursivelyBasedOnParentVisibility(_ question: MWBQuestion, keys: inout [Int]) {
        if let parentQuestionTypeKey = Int(question.visibilityTag.tagComponents()?.first ?? ""),
            let parentQuestion = question.realm?.mwbquestion(by: parentQuestionTypeKey) {
            // call recursively to check that a parent doesn't exist which has to be toggle first
            updateQuestionVisibilityRecursivelyBasedOnParentVisibility(parentQuestion, keys: &keys)
            
            // update
            let newVisibility = question.isVisible && parentQuestion.isVisible
            if question.isVisible != newVisibility {
//                debugPrint("Changing \(question.typeKey) from isVisible \(question.isVisible ? "✅" : "❌") to \(newVisibility ? "✅" : "❌")")
                question.isVisible = newVisibility
            }
        }
        
        // finally remove the key, whether it was actioned or not
        keys.remove(object: question.typeKey)
    }
    
    public func hasUserEnteredCapturedResults() -> Bool {
        for section in self.questionnaireSections {
            if let capturedResults = self.realm?.mwbCapturedResults(for: section.typeKey) {
                if Array(capturedResults).filter({ $0.prepopulationEventKey.value == nil }).count > 0 {
                    return true
                }
            }
        }
        return false
    }
}

extension Realm {
    public func allMWBQuestionnaires() -> Results<MWBQuestionnaire> {
        return self.objects(MWBQuestionnaire.self)
    }
    
    public func allSortedMWBQuestionnaires() -> Results<MWBQuestionnaire> {
        return self.objects(MWBQuestionnaire.self).sorted(byKeyPath: "sortOrderIndex", ascending: true)
    }
    
    public func getMWBQuestionnaire(using selectedQuestionnaireTypeKey: Int) -> MWBQuestionnaire? {
        let result = self.objects(MWBQuestionnaire.self).filter { $0.typeKey == selectedQuestionnaireTypeKey }
        return result.first
    }
    
    public func incompleteMWBQuestionnaires() -> [MWBQuestionnaire] {
        let result = allSortedMWBQuestionnaires().filter("completionFlag == false")
        var allIncomplete = [MWBQuestionnaire]()
        for item in result {
            allIncomplete.append(item)
        }
        return allIncomplete
    }
    
    public func hasAnswersSaved(questionnaire: MWBQuestionnaire) -> Bool {
        let results = self.objects(MWBCapturedResult.self).filter { $0.questionnaireTypeKey == questionnaire.typeKey }
        return results.count > 0
    }
    
}
