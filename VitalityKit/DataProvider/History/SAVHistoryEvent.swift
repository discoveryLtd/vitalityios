import Foundation
import RealmSwift
import VIAUtilities

public class SAVHistoryEvent: Object {
    
    @objc public dynamic var categoryCode: String?
    @objc public dynamic var categoryKey: Int = 0
    @objc public dynamic var categoryName: String?
    @objc public dynamic var dateLogged: Date = Date.distantPast
    @objc public dynamic var eventDateTime: Date?
    @objc public dynamic var eventSourceCode: String?
    @objc public dynamic var eventSourceKey: Int = 0
    @objc public dynamic var eventSourceName: String?
    @objc public dynamic var id: Int = 0
    @objc public dynamic var partyId: Int = 0
    @objc public dynamic var reportedBy: Int = 0
    @objc public dynamic var typeCode: String?
    @objc public dynamic var typeKey: Int = 0
    @objc public dynamic var typeName: String?
    public var associatedEvents = List<SAVAssociatedEvents>()
    public let eventExternalReference = List<SAVEventExternalReference>()
    public let eventMetaDataOuts = List<SAVEventMetaDataOuts>()
    
    override public static func primaryKey() -> String? {
        return "id"
    }
}

extension Realm {
    public func allSAVHistoryEvents() -> Results<SAVHistoryEvent> {
        return self.objects(SAVHistoryEvent.self)
    }
}
