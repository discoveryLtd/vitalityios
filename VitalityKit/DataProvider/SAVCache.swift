//
//  SAVCache.swift
//  VitalityActive
//
//  Created by Michelle Oratil on 30/11/2017.
//  Copyright © 2017 Glucode. All rights reserved.
//

import UIKit
import RealmSwift
import SwiftDate

public class SAVCache: Object {
    @objc public dynamic var dateCreated: Date = Date()
}

extension Realm {
    public class func resetSAVRealmIfOutdated() {
        let realm = DataProvider.newSAVRealm()
        realm.reset()
        if SAVCache.isSAVDataOutdated() {
            let vhrCache = SAVCache()
            try! realm.write {
                realm.add(vhrCache)
            }
        }
    }
    
    public class func resetSAVRealm() {
        let realm = DataProvider.newSAVRealm()
        realm.reset()
    }
    
    public class func deleteCurrentSAVCache() {
        let realm = DataProvider.newSAVRealm()
        try! realm.write {
            guard let cache = SAVCache.currentSAVCache() else { return }
            realm.delete(cache)
        }
    }
}

extension SAVCache {
    
    public class func isSAVDataOutdated(_ daysValid: Int = 1) -> Bool {
        guard let savCache = SAVCache.currentSAVCache() else {
            return true
        }
        let calendar = Calendar.current
        let cachedDate = calendar.startOfDay(for: savCache.dateCreated)
        let todaysDate = calendar.startOfDay(for: Date())
        
        let components = calendar.dateComponents([.day], from: cachedDate, to: todaysDate)
        
        if let daysDifference = components.day {
            if daysDifference > daysValid {
                return true
            } else {
                return false
            }
        } else {
            return true
        }
    }
    
    public class func currentSAVCache() -> SAVCache? {
        let realm = DataProvider.newSAVRealm()
        return realm.objects(SAVCache.self).first
    }
}
