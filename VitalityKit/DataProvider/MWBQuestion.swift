//
//  MWBQuestion.swift
//  VitalityActive
//
//  Created by Harry C. Lingad on 2/5/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import RealmSwift
import VIAUtilities

public typealias MWBVisibilityTag = String

public class MWBQuestion: Object {
    @objc public dynamic var questionDecorator: MWBQuestionDecorator?
    // Don't use the reference data here for the typeKey,
    // if new questions types are added we'll fail due to outdated data
    @objc public dynamic var typeKey = Int()
    @objc public dynamic var typeCode: String = ""
    @objc public dynamic var typeName: String?
    @objc public dynamic var isVisible: Bool = false
    
    public var unitsOfMeasure: List<MWBUnitOfMeasure> = List<MWBUnitOfMeasure>()
    
    @objc public dynamic var visibilityTag: MWBVisibilityTag = ""
    @objc public dynamic var text: String = ""
    @objc public dynamic var textDescription: String = ""
    @objc public dynamic var textNote: String = ""
    @objc public dynamic var format: String? // regular expression
    public let length = RealmOptional<Int>()
    public var validValues: List<MWBValidValue> = List<MWBValidValue>()
    
    @objc public dynamic var questionTypeName: String? // This indicates the type of question. Depicts what format the answer would be requested in the questionnaire e.g. option list
    @objc public dynamic var questionTypeCode: String?
    @objc public dynamic var questionType: QuestionTypeRef = QuestionTypeRef.Unknown
    public let sortOrderIndex = RealmOptional<Int>()
    @objc public dynamic var isChild: Bool = false
    
    public func capturedResult() -> MWBCapturedResult? {
        // Assumes that only one captured result for a question can ever exist, and returns it.
        let results = self.realm?.allMWBCapturedResults().filter("questionTypeKey == %@", self.typeKey)
        return results?.first
    }
    
    public func capturedResults() -> Results<MWBCapturedResult>? {
        return self.realm?.allMWBCapturedResults().filter("questionTypeKey == %@", self.typeKey)
    }
}

extension Realm {
    
    public func allMWBQuestions() -> Results<MWBQuestion> {
        return self.objects(MWBQuestion.self)
    }
    
    public func mwbquestion(by typeKey: Int) -> MWBQuestion? {
        return self.allMWBQuestions().filter("typeKey == %@", typeKey).first
    }
    
}
