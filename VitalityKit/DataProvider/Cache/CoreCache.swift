import RealmSwift

public class CoreCache: Object {
    @objc public dynamic var dateCreated: Date = Date()
}

extension Realm {
    public func currentCoreCacheFromRealm() -> CoreCache? {
        return self.objects(CoreCache.self).first
    }
    
    public class func deleteCurrentCoreCache() {
        let realm = DataProvider.newRealm()
        try! realm.write {
            guard let coreCache = CoreCache.currentCoreCache() else { return }
            realm.delete(coreCache)
        }
    }
}

extension CoreCache {
    public class func currentCoreCache() -> CoreCache? {
        let realm = DataProvider.newRealm()
        return realm.objects(CoreCache.self).first
    }
}
