//
//  PhotoAsset.swift
//  VitalityActive
//
//  Created by Marius Janse van Vuuren on 4/3/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

import UIKit
import RealmSwift
import Photos

public class PhotoAsset: Object {
    @objc public dynamic var assetData: Data?
    @objc public dynamic var assetURL: String?
}

extension PhotoAsset {
    public class func compress(image: UIImage) -> Data? {
        if var data = UIImageJPEGRepresentation(image, 1) {
            var compression = 0.8
            let maxCompression = 0.1
            let maxFileSize = 15.0

            var dataCount: Double = ceil(Double(data.count) / pow(1024, 2))
            var imageData: Data? = data
            while (dataCount > maxFileSize && compression > maxCompression) {
                compression = compression - 0.1
                imageData = UIImageJPEGRepresentation(image, CGFloat(compression))
                let megabyteSize = pow(1024, 2)
                let imageDataSize = Double(imageData?.count ?? 0)
                dataCount = ceil(imageDataSize / NSDecimalNumber(decimal:megabyteSize).doubleValue)
            }
            return imageData
        }
        return nil
    }
}

extension Realm {
    public func allPhotosAssets() -> Results<PhotoAsset> {
        return self.objects(PhotoAsset.self)
    }

    public func allPhotos() -> Array<PhotoAsset>? {
        let capturePhotos = self.allPhotosAssets()
        return Array(capturePhotos)
    }
}

extension UIImage{
    
    public func checkImageSize(maxSize:CGSize = CGSize(width: 1024, height: 768)) -> UIImage?{
        let size = self.size
        
        if size.width > size.height{
            /* If image is in landscape mode */
            if size.width > maxSize.width || size.height > maxSize.height{
                return resizeImage(targetSize: CGSize(width: 1024, height: 768))?.scaleImage()
            }
        }else{
            /* If image is in portrait mode */
            if size.width > maxSize.height || size.height > maxSize.width{
                return resizeImage(targetSize: CGSize(width: 768, height: 1024))?.scaleImage()
            }
        }
        
        return self
    }
    
    /*
     * Reference:
     * https://stackoverflow.com/questions/19972945/how-to-get-dpi-ppi-of-uiimage
     */
    public func scaleImage(scale: CGFloat = 75.0) -> UIImage?{
        if let cgSource = self.cgImage{
            return UIImage(cgImage: cgSource, scale: scale / 72.0, orientation: self.imageOrientation)
        }
        return nil
    }
    
    /*
     * Referrence:
     * https://stackoverflow.com/questions/31314412/how-to-resize-image-in-swift
     */
    public func resizeImage(targetSize: CGSize = CGSize(width: 1024, height: 768)) -> UIImage? {
        let size = self.size
        
        let widthRatio  = targetSize.width  / size.width
        let heightRatio = targetSize.height / size.height
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
        }
        
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        self.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage
    }
}
