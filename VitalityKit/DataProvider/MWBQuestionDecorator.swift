//
//  MWBQuestionDecorator.swift
//  VitalityActive
//
//  Created by Harry C. Lingad on 2/5/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import UIKit
import RealmSwift
import VIAUtilities

public class MWBQuestionDecorator: Object {
    @objc public dynamic var channelType: QuestionnaireChannelRef = QuestionnaireChannelRef.Unknown
    @objc public dynamic var type: QuestionDecoratorRef = QuestionDecoratorRef.Unknown
    @objc public dynamic var channelTypeCode: String?
    @objc public dynamic var typeName: String?
    @objc public dynamic var channelTypeName: String?
    @objc public dynamic var typeCode: String?
    public let unitOfMeasures: List<MWBUnitOfMeasure> = List<MWBUnitOfMeasure>()
}

extension Realm {
    public func allMWBQuestionDecorators() -> Results<MWBQuestionDecorator> {
        return self.objects(MWBQuestionDecorator.self)
    }
}
