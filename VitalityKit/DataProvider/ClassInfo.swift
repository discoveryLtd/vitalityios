//
//  ClassInfo.swift
//  VitalityActive
//
//  Created by admin on 2017/05/29.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation

struct ClassInfo: CustomStringConvertible, Equatable {
    let classObject: AnyClass
    let moduleAndClassName: String

    init?(_ classObject: AnyClass?) {
        guard classObject != nil else {
            return nil
        }

        self.classObject = classObject!
        let cName = class_getName(classObject)!
        self.moduleAndClassName = String(cString: cName)
    }

    var superclassInfo: ClassInfo? {
        let superclassObject: AnyClass? = class_getSuperclass(self.classObject)
        return ClassInfo(superclassObject)
    }

    var description: String {
        return self.moduleAndClassName
    }

    var className: String {
        return self.moduleAndClassName.components(separatedBy: ".").last ?? self.moduleAndClassName
    }

    static func ==(lhs: ClassInfo, rhs: ClassInfo) -> Bool {
        return lhs.moduleAndClassName == rhs.moduleAndClassName
    }
}
