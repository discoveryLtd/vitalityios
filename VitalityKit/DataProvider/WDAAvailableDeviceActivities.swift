import UIKit
import RealmSwift
import VIAUtilities

public class WDAAvailableDeviceActivities: Object {
    @objc public dynamic var device: String = ""
    public let availableActivitiesTypeRefs: List<WDAPointsEntryTypeRef> = List<WDAPointsEntryTypeRef>()

    override public static func primaryKey() -> String? {
        return "device"
    }
}

public class WDAPointsEntryTypeRef: Object {
    @objc public dynamic var value: PointsEntryTypeRef = .Unknown

    public func alphabeticalSortOrder() -> Int {
        switch self.value {
        case .BMI:
            return 0
        // EnergyExpend is displayed as "Calories Burned"
        case .EnergyExpend:
            return 1
        case .Distance:
            return 2
        case .Heartrate:
            return 3
        case .Speed:
            return 4
        case .Steps:
            return 5
        default:
            return 999
        }
    }
}

extension Realm {
    public func allWDAAvailableDeviceActivities() -> Results<WDAAvailableDeviceActivities> {
        return self.objects(WDAAvailableDeviceActivities.self)
    }

    public func allWDAAvailableDeviceActivitiesWithTypeRefs() -> Results<WDAAvailableDeviceActivities> {
        return self.allWDAAvailableDeviceActivities().filter("availableActivitiesTypeRefs.@count > 0")
    }

    public func availableDeviceActivitiesFor(device: String) -> Results<WDAAvailableDeviceActivities> {
        return self.allWDAAvailableDeviceActivitiesWithTypeRefs().filter("device = %@", device)
    }
}
