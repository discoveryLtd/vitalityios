//
//  GEBPEventExternalReferenceRO.swift
//  VitalityActive
//
//  Created by OJ Garde on 17/11/2017.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import RealmSwift
import VIAUtilities

public class GEBPEventExternalReferenceRO: Object {
    @objc public dynamic var typeKey: EventExternalReferenceTypeRef = .Unknown
    @objc public dynamic var typeName = ""
    @objc public dynamic var value = ""
    @objc public dynamic var typeCode = ""
}

extension Realm {
    public func allEventsExternalReference() -> Results<GEBPEventExternalReferenceRO> {
        return self.objects(GEBPEventExternalReferenceRO.self)
    }
}
