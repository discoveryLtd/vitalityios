import Foundation
import RealmSwift
import VIAUtilities

public class SAVEventMetaDataOuts: Object {
    
    @objc public dynamic var typeCode: String?
    @objc public dynamic var typeKey: Int = 0
    @objc public dynamic var typeName: String?
    @objc public dynamic var unitOfMeasure: String?
    @objc public dynamic var value: String?
    
}

extension Realm {
    public func allSAVEventMetaDataOuts() -> Results<SAVEventMetaDataOuts> {
        return self.objects(SAVEventMetaDataOuts.self)
    }
}
