//
//  OFEEffectivePeriod.swift
//  VitalityActive
//
//  Created by Harry C. Lingad on 4/23/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import RealmSwift
import VIAUtilities

public class OFEEffectivePeriod: Object {
    @objc public dynamic var effectiveFrom: String = ""
    @objc public dynamic var effectiveTo: String = ""

}

extension Realm {
    public func allEffectivePeriod() -> Results<OFEEffectivePeriod> {
        return self.objects(OFEEffectivePeriod.self)
    }
}
