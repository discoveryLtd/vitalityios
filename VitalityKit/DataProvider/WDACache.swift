import UIKit
import RealmSwift
import SwiftDate

public class WDACache: Object {
    @objc public dynamic var dateCreated: Date = Date()
}

extension Realm {
    public class func resetWDARealmIfOutdated() {
        let wdaRealm = DataProvider.newWDARealm()
        wdaRealm.reset()
        if WDACache.wdaDataIsOutdated() {
            let wdaCache = WDACache()
            try! wdaRealm.write {
                wdaRealm.add(wdaCache)
            }
        }
    }

    public class func resetWDARealm() {
        let wdaRealm = DataProvider.newWDARealm()
        wdaRealm.reset()
    }

    public class func deleteCurrentWDACache() {
        let wdaRealm = DataProvider.newWDARealm()
        try! wdaRealm.write {
            guard let wdaCache = WDACache.currentWDACache() else { return }
            wdaRealm.delete(wdaCache)
        }
    }

    public func currentWDACacheFromRealm() -> WDACache? {
        return self.objects(WDACache.self).first
    }
}

extension WDACache {

    public class func wdaDataIsOutdated(_ daysValid: Int = 30) -> Bool {
        guard let wdaCache = WDACache.currentWDACache() else {
            return true
        }
        let calendar = Calendar.current
        let cachedDate = calendar.startOfDay(for: wdaCache.dateCreated)
        let todaysDate = calendar.startOfDay(for: Date())

        let components = calendar.dateComponents([.day], from: cachedDate, to: todaysDate)

        if let daysDifference = components.day {
            if daysDifference > daysValid {
                return true
            } else {
                return false
            }
        } else {
            return true
        }
    }

    public class func currentWDACache() -> WDACache? {
        let wdaRealm = DataProvider.newWDARealm()
        return wdaRealm.objects(WDACache.self).first
    }
}
