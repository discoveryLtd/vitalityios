//
//  MWBCapturedResult.swift
//  VitalityActive
//
//  Created by Harry C. Lingad on 2/5/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import RealmSwift
import SwiftDate
import VIAUtilities

public class MWBCapturedResult: Object {
    @objc public dynamic var dateCaptured: Date = Date()
    @objc public dynamic var submitted: Bool = false
    @objc public dynamic var valid: Bool = false
    @objc public dynamic var answer: String?
    @objc public dynamic var unitOfMeasure: UnitOfMeasureRef = UnitOfMeasureRef.Unknown
    @objc public dynamic var questionnaireTypeKey = Int()
    @objc public dynamic var questionnaireSectionTypeKey = Int()
    @objc public dynamic var questionTypeKey = Int() // This is the question's typeKey, not the same as QuestionTypeRef
    @objc public dynamic var type: QuestionTypeRef = QuestionTypeRef.Unknown
    public let prepopulationEventKey = RealmOptional<Int>()
    @objc public dynamic var prepopulationEventName: String?
    @objc public dynamic var prepopulationEventCode: String?
}

extension Realm {
    public func allMWBCapturedResults() -> Results<MWBCapturedResult> {
        return self.objects(MWBCapturedResult.self)
    }
    
    public func mwbCapturedResults(for questionnaireSectionTypeKey: Int) -> Results<MWBCapturedResult> {
        return self.objects(MWBCapturedResult.self).filter("questionnaireSectionTypeKey == %@", questionnaireSectionTypeKey)
    }
    
    public func allUnsubmittedMWBCapturedResults() -> Results<MWBCapturedResult> {
        var results = self.objects(MWBCapturedResult.self)
        results = results.filter("submitted == false")
        return results
    }
    
    public func markAllUnsubmittedMWBCapturedResultsAsSubmitted() {
        do {
            try self.write {
                for capturedResult in self.allMWBCapturedResults() {
                    capturedResult.submitted = true
                }
            }
        } catch {
            print("Error occured marking unsubmitted MWB questionnaires")
        }
    }
    
    public func allUnsubmittedAndVisibleMWBCapturedResults(for questionnaireTypeKey: Int) -> Array<MWBCapturedResult> {
        let results = self.objects(MWBCapturedResult.self)
        let submittedResults = Array(results.filter("submitted == false AND questionnaireTypeKey == %@", questionnaireTypeKey))
        let visableResults = submittedResults.filter({ capturedResult in
            if let question = self.mwbquestion(by: capturedResult.questionTypeKey) {
                return question.isVisible
            }
            return false
        })
        return visableResults
    }
    
    public func mwbCapturedResultExists(for questionTypeKey: Int) -> Bool {
        return self.mwbCapturedResult(for: questionTypeKey) != nil
    }
    
    public func mwbCapturedResult(for questionTypeKey: Int, answer: String, allowsMultipleAnswers: Bool) -> MWBCapturedResult? {
        if allowsMultipleAnswers {
            return self.mwbCapturedResult(for: questionTypeKey, answer: answer)
        }
        return self.mwbCapturedResult(for: questionTypeKey)
    }
    
    //ge20180130: This would return an array of answers. Specifically used for multi-select.
    public func mwbMultiCapturedResults(for questionTypeKey: Int) -> [String] {
        let answers = self.allMWBCapturedResults().filter("questionTypeKey == %@", questionTypeKey)
        var filteredAnswer = [String]()
        for answer in answers{
            filteredAnswer.append(answer.answer!)
        }
        return filteredAnswer
    }
    
    private func mwbCapturedResult(for questionTypeKey: Int) -> MWBCapturedResult? {
        return self.allMWBCapturedResults().filter("questionTypeKey == %@", questionTypeKey).first
    }
    
    private func mwbCapturedResult(for questionTypeKey: Int, answer: String) -> MWBCapturedResult? {
        return self.allMWBCapturedResults().filter("questionTypeKey == %@ AND answer == %@", questionTypeKey, answer).first
    }
}

public protocol MWBVisibilityTagUpdatable: class {
    
    var typeKey: Int { get }
    
    var isVisible: Bool { get set }
    
    var visibilityTag: MWBVisibilityTag { get }
    
    func updateVisibility(basedOn updatedQuestionTypeKey: Int, answer: String, allowsMultipleAnswers: Bool)
    
}

extension MWBVisibilityTagUpdatable {
    
    public func updateVisibility(basedOn updatedQuestionTypeKey: Int, answer: String, allowsMultipleAnswers: Bool = false) {
        guard !visibilityTag.isEmpty else { return }
        
        // we're dealing with either an array of AND visibility tags or OR visibility tags
        var tagsToEvaluate = [visibilityTag]
        let andTags = visibilityTag.components(separatedBy: "&&").filter({ !$0.isEmpty })
        let containsAndTags = andTags.count > 1
        let orTags = visibilityTag.components(separatedBy: "||").filter({ !$0.isEmpty })
        let containsOrTags = orTags.count > 1
        // if the AND and OR tags count differs, it means that there's either
        // a composite AND or composite OR visibility tag. so we check the
        // count and select the array with the highest number of tags.
        if containsAndTags != containsOrTags {
            tagsToEvaluate = andTags.count > orTags.count ? andTags : orTags
        }
        
        var evaluatedTagResults = [Bool]()
        for tag in tagsToEvaluate {
            // check if the/any tag is malformed, if so, return without action
            guard let tagComponents = tag.mwbtagComponents() else {
                debugPrint("Malformed visibility tag: \(tag)")
                // we don't only continue, but rather return completely.
                // if we continued we open ourselves up for processing valid bits
                // of an AND or OR tag, where we should be failing as soon as ANY
                // part of the tag is invalid
                return
            }
            
            // check if this current question's visibility should be toggled based on the captured answer
            let questionTypeKey = tagComponents[0]
            guard Int(questionTypeKey) == updatedQuestionTypeKey else {
                // debugPrint("Question \(questionTypeKey) visibility does not depend on Question \(updatedQuestionTypeKey)")
                continue
            }
            
            // evaluate the tag
            let tagOperator = tagComponents[1]
            let tagValue = tagComponents[2]
            let tagValueType = tagComponents[3]
            
            // handle different value types (bool, value, name)
            var shouldToggle = false
            if tagValueType.lowercased() == "boolean" {
                shouldToggle = answer.lowercased() == tagValue.lowercased()
            } else if tagValueType.lowercased() == "name" {
                shouldToggle = Self.shouldToggleName(operator: tagOperator, lhs: answer, rhs: tagValue)
                print("ge-->MWB: Multi: result OLD \(shouldToggle) : \(answer)\(tagOperator)\(tagValue)")
                
                /**
                 * Special treatment for questionnaires of type multi answers.
                 */
                if allowsMultipleAnswers{
                    //ge20180130 : since there are multi-select, should check 'inclusion' instead
                    //           : above can be erroneous for multi-select q's
                    let realm = DataProvider.newMWBRealm()
                    let capturedResults = realm.mwbMultiCapturedResults(for: Int(tagComponents[0])!)
                    if(capturedResults.contains(tagValue)){
                        shouldToggle = true
                    }else{
                        shouldToggle = false
                    }
                    print("ge-->MWB: Multi: result NEW \(shouldToggle) : \(capturedResults) contains \(tagValue)")
                }
                
            } else if tagValueType.lowercased() == "number" {
                let serviceFormatter = NumberFormatter.serviceFormatter()
                if let lhs = serviceFormatter.number(from: answer), let rhs = serviceFormatter.number(from: tagValue) {
                    shouldToggle = Self.shouldToggleValue(operator: tagOperator, lhs: lhs, rhs: rhs)
                } else {
                    debugPrint("Cannot convert to number")
                }
            } else if tagValueType.lowercased() == "date" {
                let dateFormatter = DateFormatter.yearMonthDayFormatter()
                if let lhs = dateFormatter.date(from: answer), let rhs = dateFormatter.date(from: tagValue) {
                    shouldToggle = Self.shouldToggleDate(operator: tagOperator, lhs: lhs, rhs: rhs)
                }
            } else {
                shouldToggle = answer.lowercased() == tagValue.lowercased()
            }
            
            evaluatedTagResults.append(shouldToggle)
        }
        
        // update visibility only if there were any tags that were evaluated
        if evaluatedTagResults.count > 0 {
            // finally calculate the result
            var isVisible = self.isVisible
            if containsAndTags {
                // if any tag's result is false, the visibility is false
                isVisible = !evaluatedTagResults.contains(false)
            } else if containsOrTags {
                // if any tag's result is true, the visibility is true
                isVisible = evaluatedTagResults.contains(true)
            } else {
                // if the single tag's result is true, the visibility is true
                isVisible = evaluatedTagResults.contains(true)
            }
            self.isVisible = isVisible
        } else {
            // debugPrint("No tags have been evaluated, no updates required to visibility")
        }
    }
    
    static func shouldToggleName(operator: String, lhs: String, rhs: String) -> Bool {
        if `operator` == "==" {
            return lhs == rhs
        } else if `operator` == "!=" {
            return lhs != rhs
        }
        return false
    }
    
    static func shouldToggleValue(operator: String, lhs: NSNumber, rhs: NSNumber) -> Bool {
        if `operator` == "==" {
            return lhs.doubleValue == rhs.doubleValue
        } else if `operator` == "!=" {
            return lhs.doubleValue != rhs.doubleValue
        } else if `operator` == ">" {
            return lhs.doubleValue > rhs.doubleValue
        } else if `operator` == ">=" {
            return lhs.doubleValue >= rhs.doubleValue
        } else if `operator` == "<" {
            return lhs.doubleValue < rhs.doubleValue
        } else if `operator` == "<=" {
            return lhs.doubleValue <= rhs.doubleValue
        }
        return false
    }
    
    static func shouldToggleDate(operator: String, lhs: Date, rhs: Date) -> Bool {
        if `operator` == "==" {
            return lhs == rhs
        } else if `operator` == "!=" {
            return lhs != rhs
        } else if `operator` == ">" {
            return lhs > rhs
        } else if `operator` == ">=" {
            return lhs >= rhs
        } else if `operator` == "<" {
            return lhs < rhs
        } else if `operator` == "<=" {
            return lhs <= rhs
        }
        return false
    }
    
}

extension MWBQuestion: MWBVisibilityTagUpdatable { }
extension MWBQuestionnaireSection: MWBVisibilityTagUpdatable { }

public extension MWBVisibilityTag {
    
    func mwbtagComponents() -> [String]? {
        guard !self.isEmpty else { return nil }
        
        let components = self.components(separatedBy: ";")
        guard components.count == 4 else {
            debugPrint("Incorrect number of visibility components: \(components)")
            return nil
        }
        return components
    }
    
}
