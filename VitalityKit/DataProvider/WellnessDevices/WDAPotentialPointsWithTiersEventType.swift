//
//  PotentialPointsWithTiersEventOuts.swift
//  VitalityActive
//
//  Created by admin on 2017/06/23.
//  Copyright © 2017 Glucode. All rights reserved.
//

import RealmSwift

public class PotentialPointsWithTiersEventType: Object {
    public let totalPotentialPoints: RealmOptional<Int> = RealmOptional<Int>()
    @objc public dynamic var typeCode: String?
    public let typeKey: RealmOptional<Int> = RealmOptional<Int>()
    @objc public dynamic var typeName: String?

    public let potentialPointsEntries: List<PotentialPointsWithTiersPotentialPointsEntry> = List<PotentialPointsWithTiersPotentialPointsEntry>()

}

extension Realm {
    public func getAllPotentialPointsWithTiersEventType() -> Results<PotentialPointsWithTiersEventType> {
        return self.objects(PotentialPointsWithTiersEventType.self)
    }

    public func getPotentialPointsWithTiersEventType(with typeKey: Int) -> PotentialPointsWithTiersEventType? {
        let potentialPointsEvents = self.getAllPotentialPointsWithTiersEventType()
        return potentialPointsEvents.filter("typeKey == %@", typeKey).first
    }
}
