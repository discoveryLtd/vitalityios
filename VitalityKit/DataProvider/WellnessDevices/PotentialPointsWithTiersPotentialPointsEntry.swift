//
//  PotentialPointsWithTiersPotentialPoints.swift
//  VitalityActive
//
//  Created by admin on 2017/06/23.
//  Copyright © 2017 Glucode. All rights reserved.
//

import RealmSwift

public class PotentialPointsWithTiersPotentialPointsEntry: Object {
    public let potentialPointsValue: RealmOptional<Int> = RealmOptional<Int>()
    @objc public dynamic var typeCode: String?
    public let typeKey: RealmOptional<Int> = RealmOptional<Int>()
    @objc public dynamic var typeName: String?

    public let pointsDetails: List<PotentialPointsWithTiersEntryDetails> = List<PotentialPointsWithTiersEntryDetails>()
}

extension Realm {
    public func getAllPotentialPointsWithTiersEventTypeEntry() -> Results<PotentialPointsWithTiersPotentialPointsEntry> {
        return self.objects(PotentialPointsWithTiersPotentialPointsEntry.self)
    }

    public func getPotentialPointsWithTiersEventTypeEntry(with typeKey: Int) -> PotentialPointsWithTiersPotentialPointsEntry? {
        let potentialPointsEvents = self.getAllPotentialPointsWithTiersEventTypeEntry()
        return potentialPointsEvents.filter("typeKey == %@", typeKey).first
    }
}
