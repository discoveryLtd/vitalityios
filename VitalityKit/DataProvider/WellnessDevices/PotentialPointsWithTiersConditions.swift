//
//  PotentialPointsWithTiersConditions.swift
//  VitalityActive
//
//  Created by admin on 2017/07/06.
//  Copyright © 2017 Glucode. All rights reserved.
//

import RealmSwift
import VIAUtilities

public class PotentialPointsWithTiersConditions: Object, Conditions {
    public let lessThan: RealmOptional<Int> = RealmOptional<Int>()
    public let greaterThan: RealmOptional<Int> = RealmOptional<Int>()
    public let lessThanOrEqual: RealmOptional<Int> = RealmOptional<Int>()
    public let greaterThanOrEqual: RealmOptional<Int> = RealmOptional<Int>()

    @objc public dynamic var metaTypeCode: String?
    public let metaTypeKey: RealmOptional<Int> = RealmOptional<Int>()
    @objc public dynamic var metaTypeName: String?
    @objc public dynamic var UOM: UnitOfMeasureRef = .Unknown
    
    public var lessThanCondition: Int? {
        return lessThan.value
    }
    public var greaterThanCondition: Int? {
        return greaterThan.value
    }
    public var lessThanOrEqualCondition: Int? {
        return lessThanOrEqual.value
    }
    public var greaterThanOrEqualCondition: Int? {
        return greaterThanOrEqual.value
    }
    public var metadataTypeCode: String? {
        return metaTypeCode
    }
    public var metadataTypeKey: Int? {
        return metaTypeKey.value
    }
    public var metadataTypeName: String? {
        return metaTypeName
    }
    public var unitOfMeasure: UnitOfMeasureRef {
        return UOM
    }
    
    public func lowerboundMinutes() -> Int? {
        let secondsInMinutes = 60
        guard let lowerBoundSeconds = self.lessThanCondition else { return nil }
        return lowerBoundSeconds / secondsInMinutes
    }
    
    public func upperboundMinutes() -> Int? {
        let secondsInMinutes = 60
        if let upperBoundSeconds = self.greaterThanCondition {
            return upperBoundSeconds / secondsInMinutes
        }
        return nil
    }
}
