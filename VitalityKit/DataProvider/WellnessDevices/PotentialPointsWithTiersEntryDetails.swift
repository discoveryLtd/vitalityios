//
//  PotentialPointsWithTiersPointsDetails.swift
//  VitalityActive
//
//  Created by admin on 2017/06/23.
//  Copyright © 2017 Glucode. All rights reserved.
//

import RealmSwift

public class PotentialPointsWithTiersEntryDetails: Object {
    public let potentialPoints: RealmOptional<Int> = RealmOptional<Int>()
    public let conditions: List<PotentialPointsWithTiersConditions> = List<PotentialPointsWithTiersConditions>()

}
