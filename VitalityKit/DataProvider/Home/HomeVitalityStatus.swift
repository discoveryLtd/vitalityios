import Foundation
import RealmSwift
import VIAUtilities

public class HomeVitalityStatus: Object {
    @objc public dynamic var highestVitalityStatusCode: String? = nil
    @objc public dynamic var highestVitalityStatusKey: StatusTypeRef = .Unknown
    @objc public dynamic var highestVitalityStatusName: String? = nil
    
    @objc public dynamic var lowestVitalityStatusCode: String? = nil
    @objc public dynamic var lowestVitalityStatusKey: StatusTypeRef = .Unknown
    @objc public dynamic var lowestVitalityStatusName: String? = nil
    
    @objc public dynamic var nextVitalityStatusCode: String? = nil
    @objc public dynamic var nextVitalityStatusKey: StatusTypeRef = .Unknown
    @objc public dynamic var nextVitalityStatusName: String? = nil
    
    @objc public dynamic var overallVitalityStatusCode: String? = nil
    @objc public dynamic var overallVitalityStatusKey: StatusTypeRef = .Unknown
    @objc public dynamic var overallVitalityStatusName: String? = nil
    
    @objc public dynamic var pointsStatusCode: String? = nil
    @objc public dynamic var pointsStatusKey: StatusTypeRef = .Unknown
    @objc public dynamic var pointsStatusName: String? = nil

    @objc public dynamic var carryOverVitalityStatusCode: String? = nil
    @objc public dynamic var carryOverVitalityStatusKey: StatusTypeRef = .Unknown
    @objc public dynamic var carryOverVitalityStatusName: String? = nil
    
    public let totalPoints = RealmOptional<Int>()
    public let pointsToMaintainStatus = RealmOptional<Int>()
}

extension Realm {
    public func allHomeVitalityStatuses() -> Results<HomeVitalityStatus> {
        return self.objects(HomeVitalityStatus.self)
    }
}
