import Foundation
import RealmSwift
import VIAUtilities

public class HomePointsAccount: Object {
    @objc public dynamic var pointsNeeded: Int = 0
    @objc public dynamic var sortOrder: Int = 0
    @objc public dynamic var statusPoints: Int = 0
    @objc public dynamic var statusCode: String = ""
    @objc public dynamic var statusName: String = ""
    @objc public dynamic var statusKey: StatusTypeRef = .Unknown
}

extension Realm {
    public func allHomePointsAccounts() -> Results<HomePointsAccount> {
        return self.objects(HomePointsAccount.self).sorted(byKeyPath: "sortOrder")
    }
    
    public func pointsAccount(for statusKey: StatusTypeRef) -> HomePointsAccount? {
        let pointsAccount = self.objects(HomePointsAccount.self).filter("statusKey == %@", statusKey.rawValue)
        return pointsAccount.first
    }
}
