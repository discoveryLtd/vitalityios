import Foundation
import RealmSwift
import VIAUtilities

public class HomeScreenData: Object {
    @objc public dynamic var currentVitalityStatus: HomeVitalityStatus?
    @objc public dynamic var daysLeftInMembershipPeriod: HomeDaysLeftInMembershipPeriod?
    
    public let pointsToNextStatuses = List<HomePointsAccount>()
    public let sections = List<HomeSection>()
}

extension Realm {
    public func allHomeScreenData() -> Results<HomeScreenData> {
        return self.objects(HomeScreenData.self)
    }
}
