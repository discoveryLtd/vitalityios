import Foundation
import RealmSwift
import VIAUtilities

public class HomeCard: Object {
    @objc public dynamic var validFrom: Date = Date()
    @objc public dynamic var validTo: Date = Date()
    @objc public dynamic var amountCompleted: Int = 0
    @objc public dynamic var statusTypeCode: String = ""
    @objc public dynamic var statusTypeName: String = ""
    @objc public dynamic var status: CardStatusTypeRef = CardStatusTypeRef.Unknown
    @objc public dynamic var unitTypeCode: String? = nil
    @objc public dynamic var unitTypeName: String = ""
    @objc public dynamic var unit: CardUnitTypeRef = CardUnitTypeRef.Unknown
    @objc public dynamic var priority: Int = 0
    @objc public dynamic var total: Int = 0
    @objc public dynamic var typeCode: String? = nil
    @objc public dynamic var typeName: String? = nil
    @objc public dynamic var type: CardTypeRef = CardTypeRef.Unknown

    public let cardItems = List<HomeCardItem>()
    public let cardMetadatas = List<HomeCardMetadata>()

    override public static func primaryKey() -> String? {
        return "type"
    }
    
    public func potentialPoints() -> String {
        return value(for: .PotentialPoints)
    }
    
    public func earnedPoints() -> String {
        return value(for: .EarnedPoints)
    }
    
    public func value(for metadata: CardMetadataTypeRef) -> String {
        let noMetadataValue = ""
        for cardMetadata in cardMetadatas {
            if cardMetadata.type.rawValue == metadata.rawValue {
                return cardMetadata.value
            }
        }
        return noMetadataValue
    }
    
}

extension Realm {
    public func allHomeCards() -> Results<HomeCard> {
        return self.objects(HomeCard.self)
    }

    public func homeCard(by cardType: CardTypeRef) -> HomeCard? {
        return self.objects(HomeCard.self).filter("type == %@", cardType.rawValue).first
    }
    
    public func getCardStatus(ofType cardType:CardTypeRef) -> CardStatusTypeRef? {
        let cards = self.allHomeCards()
        
        var cardStatus: CardStatusTypeRef?
        for card in cards {
            if card.type == cardType {
                cardStatus = card.status
            }
        }
        return cardStatus
    }
}
