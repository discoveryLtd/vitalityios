import Foundation
import RealmSwift
import VIAUtilities

public class HomeCardMetadata: Object {
    @objc public dynamic var type: CardMetadataTypeRef = CardMetadataTypeRef.Unknown
    @objc public dynamic var typeName: String? = nil
    @objc public dynamic var typeCode: String? = nil
    @objc public dynamic var value: String = ""
}

extension Realm {
    public func allHomeCardMetadatas() -> Results<HomeCardMetadata> {
        return self.objects(HomeCardMetadata.self)
    }
}
