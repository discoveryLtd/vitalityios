import Foundation
import RealmSwift
import VIAUtilities

public class HomeDaysLeftInMembershipPeriod: Object {
     @objc public dynamic var daysRemaining: Int = 0
}

extension Realm {
    public func allHomeDaysLeftInMembershipPeriod() -> Results<HomeDaysLeftInMembershipPeriod> {
        return self.objects(HomeDaysLeftInMembershipPeriod.self)
    }
}
