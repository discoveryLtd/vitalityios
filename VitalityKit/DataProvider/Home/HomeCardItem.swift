import Foundation
import RealmSwift
import VIAUtilities

public class HomeCardItem: Object {
    @objc public dynamic var validFrom: Date = Date()
    @objc public dynamic var validTo: Date = Date()
    @objc public dynamic var statusType: CardItemStatusTypeRef = CardItemStatusTypeRef.Unknown
    @objc public dynamic var statusTypeCode: String? = nil
    @objc public dynamic var statusTypeName: String? = nil
    @objc public dynamic var type: CardItemTypeRef = CardItemTypeRef.Unknown
    @objc public dynamic var typeCode: String? = nil
    @objc public dynamic var typeName: String? = nil
    

    public let homeCardItemMetadatas = List<HomeCardItemMetadata>()
	
	public func value(for metadata: CardItemMetadataTypeRef) -> String {
		let noMetadataValue = ""
		for homeCardItemMetadata in homeCardItemMetadatas {
			if homeCardItemMetadata.type.rawValue == metadata.rawValue {
				return homeCardItemMetadata.value
			}
		}
		return noMetadataValue
	}
}

extension Realm {
    public func allHomeCardItems() -> Results<HomeCardItem> {
        return self.objects(HomeCardItem.self)
    }
    
    public func availableForSpinCount() -> Int{
        return self.allHomeCardItems().filter("type == \(CardItemTypeRef.WheelSpin.rawValue) && statusType == \(CardItemStatusTypeRef.Available.rawValue)").count
    }
}
