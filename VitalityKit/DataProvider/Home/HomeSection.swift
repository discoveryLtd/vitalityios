//
//  Section.swift
//  VitalityActive
//
//  Created by Lionel Camacho on 2017/02/27.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import RealmSwift
import VIAUtilities

public class HomeSection: Object {
    @objc public dynamic var type: SectionTypeRef = SectionTypeRef.Unknown
    @objc public dynamic var typeCode: String? = nil
    @objc public dynamic var typeName: String? = nil

    @objc public dynamic var priority: Bool = false

    public let cards = List<HomeCard>()
	public let rewardCards = List<RewardHomeCard>()

    override public static func primaryKey() -> String? {
        return "type"
    }
}

extension Realm {
    public func allHomeSections() -> Results<HomeSection> {
        return self.objects(HomeSection.self)
    }
    
    public func homeSection(by sectionType: SectionTypeRef) -> HomeSection? {
        return self.objects(HomeSection.self).filter("type == %@", sectionType.rawValue).first
    }
}
