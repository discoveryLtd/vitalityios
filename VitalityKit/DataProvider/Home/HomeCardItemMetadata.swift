import Foundation
import RealmSwift
import VIAUtilities

public class HomeCardItemMetadata: Object {
    @objc public dynamic var type: CardItemMetadataTypeRef = CardItemMetadataTypeRef.Unknown
    @objc public dynamic var typeName: String? = nil
    @objc public dynamic var typeCode: String? = nil
    @objc public dynamic var value: String = ""
}

extension Realm {
    public func allHomeCardItemMetadatas() -> Results<HomeCardItemMetadata> {
        return self.objects(HomeCardItemMetadata.self)
    }
}
