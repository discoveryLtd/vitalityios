//
//  GEBPEventMetaDataOutsRO.swift
//  VitalityActive
//
//  Created by OJ Garde on 17/11/2017.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import RealmSwift
import VIAUtilities

public class GEBPEventMetaDataOutsRO: Object {
    @objc public dynamic var typeKey: EventMetaDataTypeRef = .Unknown
    @objc public dynamic var typeName = ""
    @objc public dynamic var typeCode = ""
    
    @objc public dynamic var value = ""
    @objc public dynamic var unitOfMeasure = "" // Unit of measure of the metadata.  Not all metadata has a unit of measure
}

extension Realm {
    public func allEventsMetaDataOuts() -> Results<GEBPEventMetaDataOutsRO> {
        return self.objects(GEBPEventMetaDataOutsRO.self)
    }
}
