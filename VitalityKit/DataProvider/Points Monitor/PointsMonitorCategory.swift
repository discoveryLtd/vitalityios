//
//  categoryFilter.swift
//  VitalityActive
//
//  Created by Lionel Camacho on 2017/02/13.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import RealmSwift
import VIAUtilities

public class PointsMonitorCategory: Object {
    @objc public dynamic var name: String = ""
    @objc public dynamic var categoryId: PointsEntryCategoryRef = .Unknown
    @objc public dynamic var reference: Int = 0
//    override public static func primaryKey() -> String? {
//        return "reference"
//    }
}

extension PointsMonitorCategory {
    public class func setupPointsCategory(name: String, categoryId: PointsEntryCategoryRef) {
        let pointsCategory = PointsMonitorCategory()
        pointsCategory.name = name
        pointsCategory.categoryId = categoryId
        pointsCategory.reference = categoryId.rawValue

        let realm = DataProvider.newRealm()
        try! realm.write {
            realm.add(pointsCategory)
        }

    }
}
extension Realm {
    public func allPointsMonitorCategories() -> Results<PointsMonitorCategory> {
        return self.objects(PointsMonitorCategory.self)
    }

    public func pointsMonitorCategory(with reference: PointsEntryCategoryRef) -> PointsMonitorCategory? {
        let pointsMonitorCategories = self.objects(PointsMonitorCategory.self)
        return pointsMonitorCategories.filter("categoryId == %@", reference.rawValue).first
    }
}
