import Foundation
import RealmSwift
import SwiftDate
import VIAUtilities

public class PointsEntry: Object, EffectiveDateFilterable {

    @objc public dynamic var category: PointsMonitorCategory?
    @objc public dynamic var categoryKey: PointsEntryCategoryRef = PointsEntryCategoryRef.Unknown
    @objc public dynamic var categoryCode: String = ""
    @objc public dynamic var categoryName: String = ""
    @objc public dynamic var earnedValue: Int = 0
    @objc public dynamic var effectiveDate: Date = Date.distantFuture
    @objc public dynamic var eventId: Int = 0
    @objc public dynamic var id: Int = 0
    @objc public dynamic var party: Int = 0
    @objc public dynamic var potentialValue: Int = 0
    @objc public dynamic var prelimitValue: Int = 0
    @objc public dynamic var typeKey: PointsEntryTypeRef = PointsEntryTypeRef.Unknown
    @objc public dynamic var typeCode: String = ""
    @objc public dynamic var typeName: String = ""
    public let reasons = List<PointsEntryReason>()
    public let metadatas = List<PointsEntryMetadata>()
    public let activeRewardsProgressTrackerEvents = List<ActiveRewardsProgressTrackerEvent>()
    public let pointsAccount = LinkingObjects(fromType: PointsAccount.self, property: "pointsEntries")

    override public static func primaryKey() -> String? {
        return "id"
    }

    public func effectiveDateForFilter() -> Date {
        return self.effectiveDate
    }
}

public protocol EffectiveDateFilterable {
    func effectiveDateForFilter() -> Date
}

public extension Results where T: EffectiveDateFilterable {
    func filterForElementsBetweenStartAndEnd(month: Date, fieldName: String) -> Results<T> {
        let start = month.startOf(component: .month)
        let end = month.endOf(component: .month)
        let filtered = self.filter("\(fieldName) BETWEEN {%@, %@}", start, end)
        return filtered
    }

    func filterForElementsBetweenStartAndEnd(day: Date, fieldName: String) -> Results<T> {
        let start = day.startOf(component: .day)
        let end = day.endOf(component: .day)
        let filtered = self.filter("\(fieldName) BETWEEN {%@, %@}", start, end)
        return filtered
    }
}

extension Realm {
    public func allPointsEntries() -> Results<PointsEntry> {
        return self.objects(PointsEntry.self).sorted(byKeyPath: "effectiveDate", ascending: false)
    }

    public func pointsEntries(for monthAndYear: Date) -> Results<PointsEntry> {
        let filtered = self.allPointsEntries().filterForElementsBetweenStartAndEnd(month: monthAndYear, fieldName: "effectiveDate")
        let sorted = filtered.sorted(byKeyPath: "effectiveDate", ascending: false)
        return sorted
    }

    public func pointsEntries(for categoriesRefs: Array<PointsEntryCategoryRef>, during monthAndYear: Date) -> Results<PointsEntry> {
        let filtered = self.allPointsEntries().filterForElementsBetweenStartAndEnd(month: monthAndYear, fieldName: "effectiveDate")
        let categoryFiltered = filtered.filter("categoryKey IN %@", categoriesRefs.map({$0.rawValue}))
        return categoryFiltered
    }

}
