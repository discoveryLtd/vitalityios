import Foundation
import RealmSwift
import VIAUtilities

public class PointsEntryMetadata: Object, MetadataContainer {
    @objc public dynamic var typeCode: String?
    @objc public dynamic var typeName: String?
    @objc public dynamic var typeKey: EventMetaDataTypeRef = EventMetaDataTypeRef.Unknown
    @objc public dynamic var unitOfMeasureType: UnitOfMeasureRef = UnitOfMeasureRef.Unknown
    @objc public dynamic var unitOfMeasure: String?
    @objc public dynamic var value: String = ""
    public let pointsEntry = LinkingObjects(fromType: PointsEntry.self, property: "metadatas")
}

extension Realm {
    public func allPointsEntryMetadatas() -> Results<PointsEntryMetadata> {
        return self.objects(PointsEntryMetadata.self)
    }
}
