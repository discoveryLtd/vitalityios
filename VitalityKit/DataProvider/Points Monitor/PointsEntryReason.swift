import Foundation
import RealmSwift

public class PointsEntryReason: Object {
    @objc public dynamic var categoryCode: String = ""
    @objc public dynamic var categoryKey: Int = 0
    @objc public dynamic var categoryName: String = ""

    @objc public dynamic var key: Int = 0
    @objc public dynamic var name: String = ""

    public let pointsEntry = LinkingObjects(fromType: PointsEntry.self, property: "reasons")
}

extension Realm {
    public func allReasons() -> Results<PointsEntryReason> {
        return self.objects(PointsEntryReason.self)
    }
}
