import Foundation
import RealmSwift
import VIAUtilities

public class SAVEventExternalReference: Object {
    
    @objc public dynamic var typeCode: String?
    @objc public dynamic var typeKey: Int = 0
    @objc public dynamic var typeName: String?
    @objc public dynamic var value: String?
    
}

extension Realm {
    public func allSAVEventExternalReference() -> Results<SAVEventExternalReference> {
        return self.objects(SAVEventExternalReference.self)
    }
}
