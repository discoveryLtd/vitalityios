//
//  MWBQuestionAssociation.swift
//  VitalityActive
//
//  Created by Harry C. Lingad on 2/5/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import RealmSwift

public class MWBQuestionAssociation: Object {
    @objc public dynamic var childQuestionKey = Int()
    @objc public dynamic var sortIndex = Int()
    //    public var childQuestionKey: RealmOptional<Int> = RealmOptional<Int>()
    //    public var sortIndex: RealmOptional<Int> = RealmOptional<Int>()
    
    
}

extension Realm {
    public func allMWBQuestionAssociations() -> Results<MWBQuestionAssociation> {
        return self.objects(MWBQuestionAssociation.self)
    }
    
    //ge20180113 : This would return true if given childkey exists
    public func findMWBQuestionAssociation(key:Int) -> Bool? {
        let assoc = self.allMWBQuestionAssociations().filter("childQuestionKey == %@", key)
        if(assoc.count > 0){
            return true
        }else{
            return false
        }
    }
}

