import Foundation
import RealmSwift
import VIAUtilities

public class PartnerGroup: Object {
    @objc public dynamic var name: String = ""
    @objc public dynamic var key: Int = -1
    
    public let partners = List<Partner>()
}

extension Realm {
    public func allPartnerGroups() -> Results<PartnerGroup> {
        return self.objects(PartnerGroup.self)
    }
    
    public func partnersInGroup(with key: Int) -> List<Partner> {
        let filteredResults = allPartnerGroups().filter("key == %@", key)
        
        guard let results = filteredResults.first?.partners else {
            return List<Partner>()
        }
        
        return results
    }
}
