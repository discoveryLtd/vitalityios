import Foundation
import RealmSwift
import VIAUtilities

public class PartnerCategory: Object {
    
    @objc public dynamic var typeKey: Int = -1
    @objc public dynamic var typeName: String = ""
    @objc public dynamic var typeCode: String = ""
    
      public let partnerGroups = List<PartnerGroup>()
}

extension Realm {
    public func allPartnerCategories() -> Results<PartnerCategory> {
        return self.objects(PartnerCategory.self)
    }
    
    public func partnerCategory(with typeKey: Int) -> PartnerCategory? {
        let filteredResults = allPartnerCategories().filter("typeKey == %@", typeKey)
        
        guard let result = filteredResults.first else {
            return nil
        }
        
        return result
    }
}
