import Foundation
import RealmSwift
import VIAUtilities

public class Partner: Object {
    @objc public dynamic var shortDescription: String = ""
    @objc public dynamic var logoFileName: String = ""
    @objc public dynamic var longDescription: String = ""
    @objc public dynamic var name: String = ""
    @objc public dynamic var typeKey: Int = -1
    @objc public dynamic var typeCode: String? = nil
    @objc public dynamic var typeName: String? = nil
    @objc public dynamic var heading: String? = nil
}

extension Realm {
    public func allPartners() -> Results<Partner> {
        return self.objects(Partner.self)
    }
}
