//
//  Event.swift
//  VitalityActive
//
//  Created by OJ Garde on 17/11/2017.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import RealmSwift
import VIAUtilities

public class EventRO: Object,EventsFilterable {
    @objc public dynamic var id: Int = 0
    @objc public dynamic var partyId: Int = 0
    @objc public dynamic var reportedBy:Int = 0 // This is the partyID of the party that logged the event.  This is always a party (This could be the source system of the event or the party who reported the event). For instance when a servicing agent (ReportedBy) logs an event for a party (ApplicableTo).
    @objc public dynamic var dateLogged:Date = Date.distantPast
    
    @objc public dynamic var typeKey: EventTypeRef = .Unknown
    @objc public dynamic var typeName = ""
    @objc public dynamic var typeCode: String = ""
    
    @objc public dynamic var eventDateTime:Date?
    @objc public dynamic var eventSourceKey: EventSourceRef = .Unknown // Identifying code for the source of the event.  E.g. Vitality Device Platform, Core - Goals subsystem, etc
    @objc public dynamic var eventSourceName = ""
    @objc public dynamic var eventSourceCode = ""
    
    @objc public dynamic var eventCategoryKey: EventCategoryRef = .Unknown
    @objc public dynamic var eventCategoryName = ""
    @objc public dynamic var eventCategoryCode = ""
    
    public var eventExternalReference =  List<GEBPEventExternalReferenceRO>() // this is a class to facilitate the language requirements of renaming type attribute to type key and adding typeCode attribute. This is for use in Outbound payloads
    public var associatedEvents = List<GEBPAssociatedEventsRO>() // Associated Event Class used by Get Event by External Reference Respense  Events may be associated with one another. This association depicts a relationship between them.
    public var eventMetaDataOuts = List<GEBPEventMetaDataOutsRO>() // Additional optional descriptive attributes for the event.
    
    override public static func primaryKey() -> String? {
        return "id"
    }
}

public protocol EventsFilterable {
}

public extension Results where T: EventsFilterable {
    func filterEventDateTimeBetweenStartAndEnd(month: Date, fieldName: String) -> Results<T> {
        let start = month.startOf(component: .month)
        let end = month.endOf(component: .month)
        let filtered = self.filter("\(fieldName) BETWEEN {%@, %@}", start, end)
        return filtered
    }
    
    func filterEventDateTimeBetweenStartAndEnd(day: Date, fieldName: String) -> Results<T> {
        let start = day.startOf(component: .day)
        let end = day.endOf(component: .day)
        let filtered = self.filter("\(fieldName) BETWEEN {%@, %@}", start, end)
        return filtered
    }
}

extension Realm {
    public func allEvents() -> Results<EventRO> {
        return self.objects(EventRO.self)
    }
    
    public func events(for monthAndYear: Date) -> Results<EventRO> {
        let filtered = self.allEvents().filterEventDateTimeBetweenStartAndEnd(month: monthAndYear, fieldName: "eventDateTime")
        let sorted = filtered.sorted(byKeyPath: "eventDateTime", ascending: false)
        return sorted
    }
    
    public func events(for categoriesRefs: Array<EventCategoryRef>, during monthAndYear: Date) -> Results<EventRO> {
        let filtered = self.allEvents().filterEventDateTimeBetweenStartAndEnd(month: monthAndYear, fieldName: "eventDateTime")
        let categoryFiltered = filtered.filter("eventCategoryKey IN %@", categoriesRefs.map({$0.rawValue}))
        return categoryFiltered
    }
}

