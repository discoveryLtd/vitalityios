//
//  OFEEventType.swift
//  VitalityActive
//
//  Created by Val Tomol on 16/04/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import RealmSwift
import VIAUtilities

public class OFEEventType: Object {
    public let eventDetails = List<OFEEventDetail>()
    @objc public dynamic var eventTypeCode: String = ""
    @objc public dynamic var eventTypeKey: Int = 0 // TODO: Transfer to a TypeRef file
    @objc public dynamic var eventTypeName: String = ""
    @objc public dynamic var featureCode: String = ""
    @objc public dynamic var featureKey: Int = 0 // TODO: Transfer to a TypeRef file
    @objc public dynamic var featureName: String = ""
    @objc public dynamic var potentialPoints: Int = 0
    
    override public static func primaryKey() -> String? {
        return "eventTypeKey"
    }
}

extension Realm {
    public func allOFEEventTypes() -> Results<OFEEventType> {
        return self.objects(OFEEventType.self)
    }
}
