//
//  SAVEventSource.swift
//  VitalityActive
//
//  Created by Michelle Oratil on 30/11/2017.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import RealmSwift
import VIAUtilities

public class SAVEventSource: Object {
    @objc public dynamic var eventSourceName: String = ""
    @objc public dynamic var note: String = ""
    @objc public dynamic var type: EventSourceRef = EventSourceRef.Unknown
}

extension Realm {
    public func allSAVEventSources() -> Results<SAVEventSource> {
        return self.objects(SAVEventSource.self)
    }
}
