//
//  PurchaseDetailSummary.swift
//  VitalityActive
//
//  Created by Von Kervin R. Tuico on 15/04/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift
import VIAUtilities
import Realm

public class GDCPurchaseDetailSummary: Object {
    @objc public dynamic var model: String = ""
    @objc public dynamic var modelDetails: GDCQualifyingDeviceses?
    @objc public dynamic var invoiceNumber: String = ""
    @objc public dynamic var deviceSerialNumber: String = ""
    @objc public dynamic var purchaseAmount: String = ""
    @objc public dynamic var purchaseDate: String = ""
    @objc public dynamic var purchaseDateWithTimezoneAPI: String = ""
    @objc public dynamic var purchaseDateForDisplay: String = ""
}

extension Realm {
    public func allPurchaseDetailSummaryItems() -> Results<GDCPurchaseDetailSummary> {
        return self.objects(GDCPurchaseDetailSummary.self)
    }
}
