//
//  GDCAmounts.swift
//  VitalityActive
//
//  Created by Steven Layug on 4/25/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import RealmSwift
import VIAUtilities

public class GDCAmounts: Object {
    @objc public dynamic var typeCode: String = ""
    @objc public dynamic var typeKey: Int = 0
    @objc public dynamic var typeName: String = ""
    @objc public dynamic var value: String = ""
}

extension Realm {
    public func allGDCAmounts() -> Results<GDCAmounts> {
        return self.objects(GDCAmounts.self)
    }
}
