//
//  GDCBenefit.swift
//  VitalityActive
//
//  Created by Steven Layug on 4/25/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import RealmSwift
import VIAUtilities

public class GDCBGRBenefit: Object {
    @objc public dynamic var effectiveFrom: String = ""
    @objc public dynamic var effectiveTo: String = ""

    public let goalTrackers = List<GDCGoalTrackers>()
    
    @objc public dynamic var goalsRemaining: Int = 0
    @objc public dynamic var id: Int = 0
    @objc public dynamic var productKey: Int = 0
    
    @objc public dynamic var purchase: GDCBGRPurchase?
    
    @objc public dynamic var totalGoalsCompleted: Int = 0
    @objc public dynamic var totalGoalsForBenefit: Int = 0
    @objc public dynamic var totalGoalsRewardAmount: String = ""
    @objc public dynamic var totalGoalsRewardQuantity: Int = 0
}

extension Realm {
    public func allGDCBGRBenefits() -> Results<GDCBGRBenefit> {
        return self.objects(GDCBGRBenefit.self)
    }
}
