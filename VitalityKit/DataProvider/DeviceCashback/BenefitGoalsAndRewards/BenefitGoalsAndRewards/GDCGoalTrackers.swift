//
//  GDCGoalTrackers.swift
//  VitalityActive
//
//  Created by Steven Layug on 4/25/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import RealmSwift
import VIAUtilities

public class GDCGoalTrackers: Object {
    @objc public dynamic var goalCode: String = ""
    @objc public dynamic var goalKey: Int = 0
    @objc public dynamic var goalName: String = ""
    @objc public dynamic var monitorUntil: String = ""
    @objc public dynamic var pointsAchievedTowardsGoal: Int = 0
    
    public let rewardValues = List<GDCRewardValues>()
    
    @objc public dynamic var validFrom: String = ""
    @objc public dynamic var validTo: String = ""
    @objc public dynamic var highestObjectiveAchievedName: String = ""
}

extension Realm {
    public func allGDCGoalTrackers() -> Results<GDCGoalTrackers> {
        return self.objects(GDCGoalTrackers.self).sorted(byKeyPath: "validFrom", ascending: true)
    }
    
    public func allGDCGoalTrackersSortedByValidFrom() -> Results<GDCGoalTrackers> {
        return self.allGDCGoalTrackers().sorted(byKeyPath: "validFrom", ascending: false)
    }
}
    
