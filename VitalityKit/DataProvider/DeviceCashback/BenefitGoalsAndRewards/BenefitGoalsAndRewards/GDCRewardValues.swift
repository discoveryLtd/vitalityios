//
//  GDCRewardValues.swift
//  VitalityActive
//
//  Created by Steven Layug on 4/25/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import RealmSwift
import VIAUtilities

public class GDCRewardValues: Object {
    @objc public dynamic var totalRewardValueAmount: String = ""
    @objc public dynamic var totalRewardValueQuantity: Int = 0
}

extension Realm {
    public func allGDCRewardValues() -> Results<GDCRewardValues> {
        return self.objects(GDCRewardValues.self)
    }
}
