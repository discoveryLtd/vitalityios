//
//  GDCPurchase.swift
//  VitalityActive
//
//  Created by Steven Layug on 4/25/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import RealmSwift
import VIAUtilities

public class GDCBGRPurchase: Object {
    @objc public dynamic var purchaseId: Int = 0
    public let amounts: List<GDCAmounts> = List<GDCAmounts>()
    @objc public dynamic var purchaseItemProduct: GDCPurchaseItemProduct?
}

extension Realm {
    public func allGDCBGRPurchase() -> Results<GDCBGRPurchase> {
        return self.objects(GDCBGRPurchase.self)
    }
}
