//
//  GDCBenefitData.swift
//  VitalityActive
//
//  Created by Steven Layug on 4/27/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import RealmSwift
import VIAUtilities

public class GDCBenefitData: Object {

    public let benefit = List<GDCBGRBenefit>()
    
}

extension Realm {
    public func allGDCBenefitData() -> Results<GDCBenefitData> {
        return self.objects(GDCBenefitData.self)
    }
}

