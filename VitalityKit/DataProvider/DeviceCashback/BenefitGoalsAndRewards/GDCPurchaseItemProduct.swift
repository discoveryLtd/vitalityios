//
//  GDCPurchaseItemProduct.swift
//  VitalityActive
//
//  Created by Steven Layug on 4/25/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import RealmSwift
import VIAUtilities

public class GDCPurchaseItemProduct: Object {
    @objc public dynamic var productDescription: String = ""
    @objc public dynamic var productKey: Int = 0
}

extension Realm {
    public func allGDCPurchaseItemProduct() -> Results<GDCPurchaseItemProduct> {
        return self.objects(GDCPurchaseItemProduct.self)
    }
}
