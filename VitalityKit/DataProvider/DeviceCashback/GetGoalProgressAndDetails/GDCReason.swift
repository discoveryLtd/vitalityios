//
//  GDCReason.swift
//  VitalityActive
//
//  Created by Von Kervin R. Tuico on 05/05/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import RealmSwift
import VIAUtilities

public class GDCReason: Object {
    
    @objc public dynamic var categoryCode: String?
    @objc public dynamic var categoryKey: ReasonCategoryRef = ReasonCategoryRef.Unknown
    @objc public dynamic var categoryName: String?
    
    @objc public dynamic var reasonCode: String?
    @objc public dynamic var reasonKey: ReasonRef = ReasonRef.Unknown
    @objc public dynamic var reasonName: String?

}

extension Realm {
    public func allGDCReason() -> Results<GDCReason> {
        return self.objects(GDCReason.self)
    }
}
