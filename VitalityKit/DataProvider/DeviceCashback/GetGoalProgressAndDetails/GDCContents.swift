//
//  GDCContents.swift
//  VitalityActive
//
//  Created by Von Kervin R. Tuico on 05/05/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import RealmSwift
import VIAUtilities

public class GDCContents: Object {
    
    @objc public dynamic var label: String?
    @objc public dynamic var value: String?
    
}

extension Realm {
    public func allGDCContents() -> Results<GDCContents> {
        return self.objects(GDCContents.self)
    }
}
