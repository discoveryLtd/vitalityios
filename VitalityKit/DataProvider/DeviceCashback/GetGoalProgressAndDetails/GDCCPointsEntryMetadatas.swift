//
//  GDCCPointsEntryMetadatas.swift
//  VitalityActive
//
//  Created by Von Kervin R. Tuico on 05/05/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift
import VIAUtilities

public class GDCCPointsEntryMetadatas: Object {
    @objc public dynamic var typeCode: String?
    @objc public dynamic var typeKey: EventMetaDataTypeRef = EventMetaDataTypeRef.Unknown
    @objc public dynamic var typeName: String?
    
    @objc public dynamic var unitOfMeasureType: UnitOfMeasureRef = UnitOfMeasureRef.Unknown
    
    @objc public dynamic var value: String?
}

extension Realm {
    public func allGDCCPointsEntryMetadatas() -> Results<GDCCPointsEntryMetadatas> {
        return self.objects(GDCCPointsEntryMetadatas.self)
    }
}
