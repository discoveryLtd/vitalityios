//
//  GDCEvents.swift
//  VitalityActive
//
//  Created by Von Kervin R. Tuico on 05/05/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import RealmSwift
import VIAUtilities

public class GDCEvents: Object {
    
    @objc public dynamic var dateLogged: String?
    @objc public dynamic var eventDateTime: String?
    
    @objc public dynamic var eventSourceCode: String?
    @objc public dynamic var eventSourceKey: EventSourceRef = EventSourceRef.Unknown
    @objc public dynamic var eventSourceName: String?
    
    @objc public dynamic var id: Int = 0
    @objc public dynamic var partyId: Int = 0
    @objc public dynamic var reportedBy: Int = 0
    
    @objc public dynamic var typeCode: String?
    @objc public dynamic var typeKey: EventTypeRef = EventTypeRef.Unknown
    @objc public dynamic var typeName: String?
    
    public let eventMetaDatas = List<GDCEventMetaDatas>()
    
}

extension Realm {
    public func allGDCEvents() -> Results<GDCEvents> {
        return self.objects(GDCEvents.self)
    }
}
