//
//  GDCObjectivePointsEntries.swift
//  VitalityActive
//
//  Created by Von Kervin R. Tuico on 05/05/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import RealmSwift
import VIAUtilities

public class GDCObjectivePointsEntries: Object {
    
    @objc public dynamic var categoryCode: String?
    @objc public dynamic var categoryKey: PointsEntryCategoryRef = PointsEntryCategoryRef.Unknown
    @objc public dynamic var categoryName: String?
    
    @objc public dynamic var earnedValue: Int = 0
    @objc public dynamic var effectiveDate: String?
    
    @objc public dynamic var eventId: Int = 0
    @objc public dynamic var id: Int = 0
    @objc public dynamic var partyId: Int = 0
    
    @objc public dynamic var pointsContributed: Int = 0
    @objc public dynamic var potentialValue: Int = 0
    @objc public dynamic var prelimitValue: Int = 0
    
    @objc public dynamic var statusChangeDate: String?
    @objc public dynamic var statusTypeCode: String?
    @objc public dynamic var statusTypeKey: PointsEntryStatusTypeRef = PointsEntryStatusTypeRef.Unknown
    @objc public dynamic var statusTypeName: String?
    
    @objc public dynamic var systemAwareOn: String?
    
    @objc public dynamic var typeCode: String?
    @objc public dynamic var typeKey: PointsEntryTypeRef = PointsEntryTypeRef.Unknown
    @objc public dynamic var typeName: String?
    
    public let contents = List<GDCContents>()
    public let pointsEntryMetadatas = List<GDCCPointsEntryMetadatas>()
    public let reason = List<GDCReason>()
}

extension Realm {
    public func allGDCObjectivePointsEntries() -> Results<GDCObjectivePointsEntries> {
        return self.objects(GDCObjectivePointsEntries.self)
    }
    
    public func filterGDCObjectivePointsEntriesByID(with id: Int) -> GDCObjectivePointsEntries? {
        return self.objects(GDCObjectivePointsEntries.self).filter("id == %@", id).first
    }
}

