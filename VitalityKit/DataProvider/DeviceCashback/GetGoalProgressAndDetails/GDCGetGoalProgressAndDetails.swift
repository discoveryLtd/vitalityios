//
//  GDCGetGoalProgressAndDetails.swift
//  VitalityActive
//
//  Created by Von Kervin R. Tuico on 04/05/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import RealmSwift
import VIAUtilities

public class GDCGetGoalProgressAndDetails: Object {
    
   public let goalTrackerOuts = List<GDCGoalTrackerOuts>()
    
}

extension Realm {
    public func allGDCGetGoalProgressAndDetails() -> Results<GDCGetGoalProgressAndDetails> {
        return self.objects(GDCGetGoalProgressAndDetails.self)
    }
}

