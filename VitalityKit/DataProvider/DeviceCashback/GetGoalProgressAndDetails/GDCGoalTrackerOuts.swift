//
//  GDCGoalTrackerOuts.swift
//  VitalityActive
//
//  Created by Von Kervin R. Tuico on 04/05/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import RealmSwift
import VIAUtilities

public class GDCGoalTrackerOuts: Object {
    
    @objc public dynamic var agreementId: Int = 0
    @objc public dynamic var completedObjectives: Int = 0
    @objc public dynamic var effectiveFrom: String?
    @objc public dynamic var effectiveTo: String?
    
    @objc public dynamic var goalCode: String?
    @objc public dynamic var goalKey: GoalRef = GoalRef.Unknown
    @objc public dynamic var goalName: String?
    
    @objc public dynamic var goalTrackerStatusCode: String?
    @objc public dynamic var goalTrackerStatusKey: GoalTrackerStatusRef = GoalTrackerStatusRef.Unknown
    @objc public dynamic var goalTrackerStatusName: String?
    
    @objc public dynamic var id: Int = 0
    @objc public dynamic var monitorUntil: String?

    @objc public dynamic var partyId: Int = 0
    @objc public dynamic var percentageCompleted: Int = 0
    
    @objc public dynamic var statusChangedOn: String?
    @objc public dynamic var totalObjectives: Int = 0
    
    public let goalResultantEvents = List<GDCGoalResultantEvents>()
    public let objectiveTrackers = List<GDCObjectiveTrackers>()
    
}

extension Realm {
    public func allGDCGoalTrackerOuts() -> Results<GDCGoalTrackerOuts> {
        return self.objects(GDCGoalTrackerOuts.self)
    }
    
}


