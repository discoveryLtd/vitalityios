//
//  GDCObjectiveTrackers.swift
//  VitalityActive
//
//  Created by Von Kervin R. Tuico on 05/05/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import RealmSwift
import VIAUtilities

public class GDCObjectiveTrackers: Object {
    
    @objc public dynamic var effectiveFrom: String?
    @objc public dynamic var effectiveTo: String?
    
    @objc public dynamic var eventCountAchieved: Int = 0
    @objc public dynamic var eventCountTarget: Int = 0
    @objc public dynamic var eventOutcomeAchieved: Bool = false
    @objc public dynamic var eventOutcomeTarget: String?
    
    @objc public dynamic var monitorUntil: String?
    
    @objc public dynamic var objectiveCode: String?
    @objc public dynamic var objectiveKey: ObjectiveRef = ObjectiveRef.Unknown
    @objc public dynamic var objectiveName: String?
    
    @objc public dynamic var percentageCompleted: Int = 0
    @objc public dynamic var pointsAchieved: Int = 0
    @objc public dynamic var pointsTarget: Int = 0
    @objc public dynamic var statusChangedOn: String?
    
    @objc public dynamic var statusCode: String?
    @objc public dynamic var statusKey: ObjectiveTrackerStatusRef = ObjectiveTrackerStatusRef.Unknown
    @objc public dynamic var statusName: String?
    
    public let events = List<GDCEvents>()
    public let objectivePointsEntries = List<GDCObjectivePointsEntries>()
    
}

extension Realm {
    public func allGDCObjectiveTrackers() -> Results<GDCObjectiveTrackers> {
        return self.objects(GDCObjectiveTrackers.self)
    }
    
}
