//
//  GDCGetGoalProgressAndDetailsData.swift
//  VitalityActive
//
//  Created by Von Kervin R. Tuico on 05/05/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import RealmSwift
import VIAUtilities
import Realm

public class GDCGetGoalProgressAndDetailsData: Object {
    
    @objc public dynamic var getGoalProgressAndDetailsResponse: GDCGetGoalProgressAndDetails?
}

extension Realm {
    public func allGDCGetGoalProgressAndDetailsData() -> Results<GDCGetGoalProgressAndDetailsData> {
        return self.objects(GDCGetGoalProgressAndDetailsData.self)
    }
}
