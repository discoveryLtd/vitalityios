//
//  GDCGoalResultantEvents.swift
//  VitalityActive
//
//  Created by Von Kervin R. Tuico on 05/05/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import RealmSwift
import VIAUtilities

public class GDCGoalResultantEvents: Object {
    
    @objc public dynamic var id: Int = 0
    
}

extension Realm {
    public func allGDCGoalResultantEvents() -> Results<GDCGoalResultantEvents> {
        return self.objects(GDCGoalResultantEvents.self)
    }
}

