//
//  DCPurchaseItem.swift
//  VitalityActive
//
//  Created by Von Kervin R. Tuico on 26/04/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift
import VIAUtilities

public class GDCPurchaseItem: Object {
    @objc public dynamic var itemDescription: String?
    @objc public dynamic var productCode: String?
    @objc public dynamic var productKey: Int = 0
    @objc public dynamic var productName: String?
    
    public var purchaseAmount: List<GDCPIPurchaseAmount> = List<GDCPIPurchaseAmount>()
    public var purchaseReference: List<GDCPIPurchaseReference> = List<GDCPIPurchaseReference>()
}

extension Realm {
    public func allGDCPurchaseItem() -> Results<GDCPurchaseItem> {
        return self.objects(GDCPurchaseItem.self)
    }
}
