//
//  DCPurchaseAmount.swift
//  VitalityActive
//
//  Created by Von Kervin R. Tuico on 26/04/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift
import VIAUtilities

public class GDCPurchaseAmount: Object {
    @objc public dynamic var typeCode: String?
    @objc public dynamic var typeKey: Int = 0
    @objc public dynamic var typeName: String?
    @objc public dynamic var value: String?
}

extension Realm {
    public func allGDCPurchaseAmount() -> Results<GDCPurchaseAmount> {
        return self.objects(GDCPurchaseAmount.self)
    }
}
