//
//  DCPurchase.swift
//  VitalityActive
//
//  Created by Von Kervin R. Tuico on 26/04/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift
import VIAUtilities

public class GDCPurchase: Object {
    @objc public dynamic var deliveryDate: String?
    @objc public dynamic var id: Int = 0
    @objc public dynamic var purchaseDate: String?
    @objc public dynamic var purchaseItem: GDCPurchaseItem?
    
    public var purchaseAmount: List<GDCPurchaseAmount> = List<GDCPurchaseAmount>()
    public var purchaseReference: List<GDCPurchaseReference> = List<GDCPurchaseReference>()
}

extension Realm {
    public func allGDCPurchase() -> Results<GDCPurchase> {
        return self.objects(GDCPurchase.self)
    }
}
