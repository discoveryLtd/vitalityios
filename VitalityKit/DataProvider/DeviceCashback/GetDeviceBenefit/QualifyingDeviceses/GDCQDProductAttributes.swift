//
//  DCQDProductAttributes.swift
//  VitalityActive
//
//  Created by Von Kervin R. Tuico on 03/05/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import RealmSwift
import VIAUtilities

public class GDCQDProductAttributes: Object {
    @objc public dynamic var typeCode: String = ""
    @objc public dynamic var typeKey: Int = 0
    @objc public dynamic var typeName: String = ""
    @objc public dynamic var value: String = ""
}

extension Realm {
    public func allGDCQDProductAttributes() -> Results<GDCQDProductAttributes> {
        return self.objects(GDCQDProductAttributes.self)
    }
}

