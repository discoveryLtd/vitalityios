//
//  DCQualifyingDeviceses.swift
//  VitalityActive
//
//  Created by Von Kervin R. Tuico on 26/04/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift
import VIAUtilities

public class GDCQualifyingDeviceses: Object {
    @objc public dynamic var linked: Bool = false
    @objc public dynamic var productCode: String = ""
    @objc public dynamic var productKey: Int = 0
    @objc public dynamic var productName: String = ""
    
    public var productAttributes: List<GDCQDProductAttributes> = List<GDCQDProductAttributes>()
}

extension Realm {
    public func allGDCQualifyingDeviceses() -> Results<GDCQualifyingDeviceses> {
        return self.objects(GDCQualifyingDeviceses.self)
    }
}
