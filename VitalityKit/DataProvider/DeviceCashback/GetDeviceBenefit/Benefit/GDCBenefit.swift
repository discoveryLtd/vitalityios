//
//  DCBenefit.swift
//  VitalityActive
//
//  Created by Von Kervin R. Tuico on 25/04/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift
import VIAUtilities

public class GDCBenefit: Object {
    @objc public dynamic var benefitId: Int = 0
    @objc public dynamic var currentBenefitPeriod: Int = 0
    @objc public dynamic var productCode: String?
    @objc public dynamic var productKey: Int = 0
    @objc public dynamic var productName: String?
    @objc public dynamic var renewalPeriodUnit: Int = 0
    
    public var productAttributes: List<GDCProductAttributes> = List<GDCProductAttributes>()
    @objc public dynamic var state: GDCState?
}

extension Realm {
    public func allGDCBenefit() -> Results<GDCBenefit> {
        return self.objects(GDCBenefit.self)
    }
}
