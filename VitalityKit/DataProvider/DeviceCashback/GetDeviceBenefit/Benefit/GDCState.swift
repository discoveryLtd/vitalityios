//
//  DCState.swift
//  VitalityActive
//
//  Created by Von Kervin R. Tuico on 25/04/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift
import VIAUtilities

public class GDCState: Object {
    @objc public dynamic var effectiveFrom: String?
    @objc public dynamic var effectiveTo: String?
    @objc public dynamic var reason: String?
    @objc public dynamic var statusTypeCode: String?
    @objc public dynamic var statusTypeKey: Int = 0
    @objc public dynamic var statusTypeName: String?
}

extension Realm {
    public func allGDCState() -> Results<GDCState> {
        return self.objects(GDCState.self)
    }
}
