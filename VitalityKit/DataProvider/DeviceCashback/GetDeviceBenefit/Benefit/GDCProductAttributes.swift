//
//  DCProductAttributes.swift
//  VitalityActive
//
//  Created by Von Kervin R. Tuico on 25/04/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import RealmSwift
import VIAUtilities

public class GDCProductAttributes: Object {
    @objc public dynamic var typeCode: String?
    @objc public dynamic var typeKey: Int = 0
    @objc public dynamic var typeName: String?
    @objc public dynamic var value: String?
}

extension Realm {
    public func allGDCProductAttributes() -> Results<GDCProductAttributes> {
        return self.objects(GDCProductAttributes.self)
    }
}
