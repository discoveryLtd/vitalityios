//
//  DCLevels.swift
//  VitalityActive
//
//  Created by Von Kervin R. Tuico on 25/04/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift
import VIAUtilities

public class GDCLevels: Object {
    @objc public dynamic var isAchieved: Bool = false
    @objc public dynamic var isHighestLevelAchieved: Bool = false
    @objc public dynamic var level: Int = 0
    @objc public dynamic var maximumPoints: Int = 0
    @objc public dynamic var minimumPoints: Int = 0
    @objc public dynamic var objectiveKey: Int = 0
    @objc public dynamic var pointsAchieved: Int = 0
    @objc public dynamic var reward: GDCReward?
}

extension Realm {
    public func allGDCLevels() -> Results<GDCLevels> {
        return self.objects(GDCLevels.self)
    }
}
