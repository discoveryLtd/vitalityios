//
//  DCPointsEntry.swift
//  VitalityActive
//
//  Created by Von Kervin R. Tuico on 25/04/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift
import VIAUtilities

public class GDCPointsEntry: Object {
    @objc public dynamic var activity: String?
    @objc public dynamic var effectiveDate: String?
    @objc public dynamic var pointsContributed: Int = 0
    @objc public dynamic var typeCode: String?
    @objc public dynamic var typeKey: Int = 0
    @objc public dynamic var typeName: String?
    
    public var pointsEntryMetadatas: List<GDCPointsEntryMetadatas> = List<GDCPointsEntryMetadatas>()
}

extension Realm {
    public func allGDCPointsEntry() -> Results<GDCPointsEntry> {
        return self.objects(GDCPointsEntry.self)
    }
}
