//
//  DCGoalTracker.swift
//  VitalityActive
//
//  Created by Von Kervin R. Tuico on 25/04/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift
import VIAUtilities

public class GDCGoalTracker: Object {
    @objc public dynamic var completedObjectives: Int = 0
    @objc public dynamic var effectiveFrom: String?
    @objc public dynamic var effectiveTo: String?
    @objc public dynamic var monitorUntil: String?
    @objc public dynamic var percentageCompleted: Int = 0
    @objc public dynamic var statusChangedOn: String?
    @objc public dynamic var statusCode: String?
    @objc public dynamic var statusKey: Int = 0
    @objc public dynamic var statusName: String?
    @objc public dynamic var totalObjectives: Int = 0
}

extension Realm {
    public func allGDCGoalTracker() -> Results<GDCGoalTracker> {
        return self.objects(GDCGoalTracker.self)
    }
}
