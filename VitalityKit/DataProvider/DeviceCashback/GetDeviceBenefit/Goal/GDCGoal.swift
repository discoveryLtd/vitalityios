//
//  DCGoal.swift
//  VitalityActive
//
//  Created by Von Kervin R. Tuico on 25/04/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift
import VIAUtilities

public class GDCGoal: Object {
    @objc public dynamic var key: Int = 0
    @objc public dynamic var goalTracker: GDCGoalTracker?
    
    public var applicablePointsEntryTypes: List<GDCApplicablePointsEntryTypes> = List<GDCApplicablePointsEntryTypes>()
    public var levels: List<GDCLevels> = List<GDCLevels>()
    public var pointsEntry: List<GDCPointsEntry> = List<GDCPointsEntry>()
}

extension Realm {
    public func allGDCGoal() -> Results<GDCGoal> {
        return self.objects(GDCGoal.self)
    }
}
