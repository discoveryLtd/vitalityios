//
//  DCApplicablePointsEntryTypes.swift
//  VitalityActive
//
//  Created by Von Kervin R. Tuico on 25/04/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift
import VIAUtilities

public class GDCApplicablePointsEntryTypes: Object {
    @objc public dynamic var eventTypeCode: String?
    @objc public dynamic var eventTypeKey: Int = 0
    @objc public dynamic var eventTypeName: String?
    @objc public dynamic var pointsEntryTypeCode: String?
    @objc public dynamic var pointsEntryTypeKey: Int = 0
    @objc public dynamic var pointsEntryTypeName: String?
}

extension Realm {
    public func allGDCApplicablePointsEntryTypes() -> Results<GDCApplicablePointsEntryTypes> {
        return self.objects(GDCApplicablePointsEntryTypes.self)
    }
}
