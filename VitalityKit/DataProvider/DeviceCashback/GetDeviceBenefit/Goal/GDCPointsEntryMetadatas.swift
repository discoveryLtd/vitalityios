//
//  DCPointsEntryMetadatas.swift
//  VitalityActive
//
//  Created by Von Kervin R. Tuico on 25/04/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift
import VIAUtilities

public class GDCPointsEntryMetadatas: Object {
    @objc public dynamic var typeCode: String?
    @objc public dynamic var typeKey: Int = 0
    @objc public dynamic var typeName: String?
    @objc public dynamic var unitOfMeasure: String?
    @objc public dynamic var value: String?
}

extension Realm {
    public func allGDCPointsEntryMetadatas() -> Results<GDCPointsEntryMetadatas> {
        return self.objects(GDCPointsEntryMetadatas.self)
    }
}
