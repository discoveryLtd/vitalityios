//
//  DCReward.swift
//  VitalityActive
//
//  Created by Von Kervin R. Tuico on 25/04/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift
import VIAUtilities

public class GDCReward: Object {
    @objc public dynamic var accumulatedAmount: String?
    @objc public dynamic var accumulatedPercentage: Int = 0
    @objc public dynamic var accumulatedQuantity: Int = 0
    @objc public dynamic var id: Int = 0
    @objc public dynamic var typeCode: String?
    @objc public dynamic var typeKey: Int = 0
    @objc public dynamic var typeName: String?
}

extension Realm {
    public func allGDCReward() -> Results<GDCReward> {
        return self.objects(GDCReward.self)
    }
}
