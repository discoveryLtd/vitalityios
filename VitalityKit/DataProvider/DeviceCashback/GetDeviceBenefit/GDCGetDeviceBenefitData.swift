//
//  GetDeviceBenefitData.swift
//  VitalityActive
//
//  Created by Von Kervin R. Tuico on 26/04/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import RealmSwift
import VIAUtilities
import Realm

public class GDCGetDeviceBenefitData: Object {
    @objc public dynamic var benefit: GDCBenefit?
    @objc public dynamic var goal: GDCGoal?
    @objc public dynamic var purchase: GDCPurchase?
    @objc public dynamic var purchaseProduct: GDCPurchaseProduct?
    
    public let qualifyingDeviceses = List<GDCQualifyingDeviceses>()
}

extension Realm {
    public func allGDCGetDeviceBenefitData() -> Results<GDCGetDeviceBenefitData> {
        return self.objects(GDCGetDeviceBenefitData.self)
    }
}

