//
//  DCPurchaseProduct.swift
//  VitalityActive
//
//  Created by Von Kervin R. Tuico on 26/04/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift
import VIAUtilities

public class GDCPurchaseProduct: Object {
    @objc public dynamic var productCode: String?
    @objc public dynamic var productKey: Int = 0
    @objc public dynamic var productName: String?
    
    public var productAttributes: List<GDCPPProductAttributes> = List<GDCPPProductAttributes>()
}

extension Realm {
    public func allGDCPurchaseProduct() -> Results<GDCPurchaseProduct> {
        return self.objects(GDCPurchaseProduct.self)
    }
}

