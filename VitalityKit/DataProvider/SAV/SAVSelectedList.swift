//
//  SAVSelectedItem.swift
//  VitalityActive
//
//  Created by Dexter Anthony Ambrad on 12/9/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import RealmSwift
import VIAUtilities

public class SAVSelectedItem: Object {
    @objc public dynamic var typeName: String = ""
    @objc public dynamic var eventType: EventTypeRef = EventTypeRef.Unknown
    @objc public dynamic var featureType: ProductFeatureTypeRef = ProductFeatureTypeRef.Unknown
    @objc public dynamic var testedDate: Date? = nil
    
}

extension Realm {
    public func allSAVItems() -> Results<SAVSelectedItem> {
        return self.objects(SAVSelectedItem.self)
    }
 }
