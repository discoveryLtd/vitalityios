//
//  MWBQuestionnaireSection.swift
//  VitalityActive
//
//  Created by Harry C. Lingad on 2/5/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import RealmSwift

public class MWBQuestionnaireSection: Object {
    @objc public dynamic var typeKey = Int()  // don't use QuestionnaireSectionsRef as the type. we have to stay dumb to new sections
    @objc public dynamic var typeName: String?
    @objc public dynamic var visibilityTag: MWBVisibilityTag = ""
    public let questions: List<MWBQuestion> = List<MWBQuestion>()
    @objc public dynamic var isVisible: Bool = false
    @objc public dynamic var typeCode: String?
    public let sortOrderIndex = RealmOptional<Int>()
    
    @objc public dynamic var text: String = ""
    @objc public dynamic var textDescription: String = ""
    @objc public dynamic var textNote: String = ""
    
    public func sortedQuestions() -> Results<MWBQuestion> {
        let sorted = self.questions.sorted(byKeyPath: "sortOrderIndex", ascending: true)
        return sorted
    }
    
    public func sortedVisibleQuestions() -> Results<MWBQuestion> {
        let sorted = self.questions.sorted(byKeyPath: "sortOrderIndex", ascending: true).filter("isVisible == true")
        return sorted
    }
    
    public func isValid() -> Bool {
        var capturedResults = [MWBCapturedResult]()
        var questionsWithoutCapturedResults = [MWBQuestion]()
        
        // exclude Label questions from validation
        let questionsToBeValidated = questions.filter({ $0.questionType != .Label })
        for question in questionsToBeValidated {
            if let results = question.capturedResults(), results.count > 0 {
                capturedResults.append(contentsOf: results)
            } else if question.isVisible {
                questionsWithoutCapturedResults.append(question)
            }
        }
        // validate
        let invalidCapturedResults = capturedResults.filter({ $0.valid == false })
        return invalidCapturedResults.count == 0 && questionsWithoutCapturedResults.count == 0
    }
}

extension Realm {
    public func allMWBQuestionnaireSections() -> Results<MWBQuestionnaireSection> {
        return self.objects(MWBQuestionnaireSection.self)
    }
    
    public func mwbQuestionnaireSection(by questionnaireSectionTypeKey: Int) -> MWBQuestionnaireSection? {
        let result = self.objects(MWBQuestionnaireSection.self).filter { $0.typeKey == questionnaireSectionTypeKey }
        return result.first
    }
}
