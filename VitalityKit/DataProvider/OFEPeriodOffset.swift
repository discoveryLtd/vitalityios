//
//  OFEPeriodOffset.swift
//  VitalityActive
//
//  Created by Harry C. Lingad on 4/23/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation

import RealmSwift
import VIAUtilities

public class OFEPeriodOffset: Object {
    @objc public dynamic var offsetForward: Int = 0
    @objc public dynamic var offsetPast: Int = 0
    
}

extension Realm {
    public func allPeriodOffset() -> Results<OFEEffectivePeriod> {
        return self.objects(OFEEffectivePeriod.self)
    }
}
