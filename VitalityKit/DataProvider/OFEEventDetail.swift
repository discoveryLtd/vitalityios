//
//  OFEEventDetail.swift
//  VitalityActive
//
//  Created by Val Tomol on 16/04/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import RealmSwift
import VIAUtilities

public class OFEEventDetail: Object {
    public let selectionGroups = List<OFESelectionGroup>()
    @objc public dynamic var metadataCode: String = ""
    @objc public dynamic var metadataKey: Int = 0 // TODO: Transfer to a TypeRef file.
    @objc public dynamic var metadataName: String = ""
}

extension Realm {
    public func allOFEEventDetails() -> Results<OFEEventDetail> {
        return self.objects(OFEEventDetail.self)
    }
}
