import UIKit
import RealmSwift


public class WDAPartnerUrlDetail: Object {
    @objc public dynamic var url: String = ""
    @objc public dynamic var method: String = ""
}

extension Realm {
    public func allWDAPartnerUrlDetails() -> Results<WDAPartnerUrlDetail> {
        return self.objects(WDAPartnerUrlDetail.self)
    }
}
