import RealmSwift
import VIAUtilities

public typealias VisibilityTag = String

public class VHRQuestion: Object {
    @objc public dynamic var questionDecorator: VHRQuestionDecorator?
    // Don't use the reference data here for the typeKey,
    // if new questions types are added we'll fail due to outdated data
    @objc public dynamic var typeKey = Int()
    @objc public dynamic var typeCode: String = ""
    @objc public dynamic var typeName: String?
    @objc public dynamic var isVisible: Bool = false

    public var unitsOfMeasure: List<VHRUnitOfMeasure> = List<VHRUnitOfMeasure>()

    @objc public dynamic var visibilityTag: VisibilityTag = ""
    @objc public dynamic var text: String = ""
    @objc public dynamic var textDescription: String = ""
    @objc public dynamic var textNote: String = ""
    @objc public dynamic var format: String? // regular expression
    public let length = RealmOptional<Int>()
    public var validValues: List<VHRValidValue> = List<VHRValidValue>()

    @objc public dynamic var questionTypeName: String? // This indicates the type of question. Depicts what format the answer would be requested in the questionnaire e.g. option list
    @objc public dynamic var questionTypeCode: String?
    @objc public dynamic var questionType: QuestionTypeRef = QuestionTypeRef.Unknown
    public let sortOrderIndex = RealmOptional<Int>()
    @objc public dynamic var isChild: Bool = false
    public var childQuestions: List<VHRQuestionAssociation> = List<VHRQuestionAssociation>()
    
    public func capturedResult() -> VHRCapturedResult? {
        // Assumes that only one captured result for a question can ever exist, and returns it.
        let results = self.realm?.allVHRCapturedResults().filter("questionTypeKey == %@", self.typeKey)
        return results?.first
    }

    public func capturedResults() -> Results<VHRCapturedResult>? {
        return self.realm?.allVHRCapturedResults().filter("questionTypeKey == %@", self.typeKey)
    }
}

extension Realm {

    public func allVHRQuestions() -> Results<VHRQuestion> {
        return self.objects(VHRQuestion.self)
    }

    public func question(by typeKey: Int) -> VHRQuestion? {
        return self.allVHRQuestions().filter("typeKey == %@", typeKey).first
    }

}
