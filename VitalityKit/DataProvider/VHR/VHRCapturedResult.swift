import Foundation
import RealmSwift
import SwiftDate
import VIAUtilities

public class VHRCapturedResult: Object {
    @objc public dynamic var dateCaptured: Date = Date()
    @objc public dynamic var submitted: Bool = false
    @objc public dynamic var valid: Bool = false
    @objc public dynamic var answer: String?
    @objc public dynamic var unitOfMeasure: UnitOfMeasureRef = UnitOfMeasureRef.Unknown
    @objc public dynamic var questionnaireTypeKey = Int()
    @objc public dynamic var questionnaireSectionTypeKey = Int()
    @objc public dynamic var questionTypeKey = Int() // This is the question's typeKey, not the same as QuestionTypeRef
    @objc public dynamic var type: QuestionTypeRef = QuestionTypeRef.Unknown
    public let prepopulationEventKey = RealmOptional<Int>()
    @objc public dynamic var prepopulationEventName: String?
    @objc public dynamic var prepopulationEventCode: String?
}

extension Realm {
    public func allVHRCapturedResults() -> Results<VHRCapturedResult> {
        return self.objects(VHRCapturedResult.self)
    }

    public func vhrCapturedResults(for questionnaireSectionTypeKey: Int) -> Results<VHRCapturedResult> {
        return self.objects(VHRCapturedResult.self).filter("questionnaireSectionTypeKey == %@", questionnaireSectionTypeKey)
    }

    public func allUnsubmittedVHRCapturedResults() -> Results<VHRCapturedResult> {
        var results = self.objects(VHRCapturedResult.self)
        results = results.filter("submitted == false")
        return results
    }

    public func markAllUnsubmittedVHRCapturedResultsAsSubmitted() {
        do {
            try self.write {
                for capturedResult in self.allVHRCapturedResults() {
                    capturedResult.submitted = true
                }
            }
        } catch {
            print("Error occured marking unsubmitted VHR questionnaires")
        }
    }

    public func allUnsubmittedAndVisibleCapturedResults(for questionnaireTypeKey: Int) -> Array<VHRCapturedResult> {
        let results = self.objects(VHRCapturedResult.self)
        let submittedResults = Array(results.filter("submitted == false AND questionnaireTypeKey == %@", questionnaireTypeKey))
        let visableResults = submittedResults.filter({ capturedResult in
            if let question = self.question(by: capturedResult.questionTypeKey) {
                return question.isVisible
            }
            return false
        })
        return visableResults
    }

    public func vhrCapturedResultExists(for questionTypeKey: Int) -> Bool {
        return self.vhrCapturedResult(for: questionTypeKey) != nil
    }

    public func vhrCapturedResult(for questionTypeKey: Int, answer: String, allowsMultipleAnswers: Bool) -> VHRCapturedResult? {
        if allowsMultipleAnswers {
            return self.vhrCapturedResult(for: questionTypeKey, answer: answer)
        }
        return self.vhrCapturedResult(for: questionTypeKey)
    }
    
    //ge20180130: This would return an array of answers. Specifically used for multi-select.
    public func vhrMultiCapturedResults(for questionTypeKey: Int) -> [String] {
        let answers = self.allVHRCapturedResults().filter("questionTypeKey == %@", questionTypeKey)
        var filteredAnswer = [String]()
        for answer in answers{
            filteredAnswer.append(answer.answer!)
        }
        return filteredAnswer
    }

    public func vhrCapturedResult(for questionTypeKey: Int) -> VHRCapturedResult? {
        return self.allVHRCapturedResults().filter("questionTypeKey == %@", questionTypeKey).first
    }

    public func vhrCapturedResult(for uom: UnitOfMeasureRef, andNot questionTypeKey: Int) -> VHRCapturedResult? {
        return self.allVHRCapturedResults().filter("unitOfMeasure == %@ AND questionTypeKey != %@", uom.rawValue, questionTypeKey).first
    }

    private func vhrCapturedResult(for questionTypeKey: Int, answer: String) -> VHRCapturedResult? {
        return self.allVHRCapturedResults().filter("questionTypeKey == %@ AND answer == %@", questionTypeKey, answer).first
    }
}

public protocol VisibilityTagUpdatable: class {

    var typeKey: Int { get }

    var isVisible: Bool { get set }

    var visibilityTag: VisibilityTag { get }

    func updateVisibility(basedOn updatedQuestionTypeKey: Int, answer: String, allowsMultipleAnswers: Bool)

}

extension VisibilityTagUpdatable {

    public func updateVisibility(basedOn updatedQuestionTypeKey: Int, answer: String, allowsMultipleAnswers: Bool = false) {
        guard !visibilityTag.isEmpty else { return }

        // we're dealing with either an array of AND visibility tags or OR visibility tags
        var tagsToEvaluate = [visibilityTag]
        let andTags = visibilityTag.components(separatedBy: "&&").filter({ !$0.isEmpty })
        let containsAndTags = andTags.count > 1
        let orTags = visibilityTag.components(separatedBy: "||").filter({ !$0.isEmpty })
        let containsOrTags = orTags.count > 1
        // if the AND and OR tags count differs, it means that there's either
        // a composite AND or composite OR visibility tag. so we check the
        // count and select the array with the highest number of tags.
        if containsAndTags != containsOrTags {
            tagsToEvaluate = andTags.count > orTags.count ? andTags : orTags
        }

        var evaluatedTagResults = [Bool]()
        for tag in tagsToEvaluate {
            // check if the/any tag is malformed, if so, return without action
            guard let tagComponents = tag.tagComponents() else {
                 debugPrint("Malformed visibility tag: \(tag)")
                // we don't only continue, but rather return completely.
                // if we continued we open ourselves up for processing valid bits
                // of an AND or OR tag, where we should be failing as soon as ANY
                // part of the tag is invalid
                return
            }

            // check if this current question's visibility should be toggled based on the captured answer
            let questionTypeKey = tagComponents[0]
            guard Int(questionTypeKey) == updatedQuestionTypeKey else {
                // debugPrint("Question \(questionTypeKey) visibility does not depend on Question \(updatedQuestionTypeKey)")
                continue
            }

            // evaluate the tag
            let tagOperator = tagComponents[1]
            let tagValue = tagComponents[2]
            let tagValueType = tagComponents[3]

            // handle different value types (bool, value, name)
            var shouldToggle = false
            if tagValueType.lowercased() == "boolean" {
                shouldToggle = answer.lowercased() == tagValue.lowercased()
            } else if tagValueType.lowercased() == "name" {
                shouldToggle = Self.shouldToggleName(operator: tagOperator, lhs: answer, rhs: tagValue)
//                print("ge-->VHR: Multi: result OLD \(shouldToggle) : \(answer)\(tagOperator)\(tagValue)")
                
                /**
                 * Special treatment for questionnaires of type multi answers.
                 */
                if allowsMultipleAnswers{
                    //ge20180130 : since there are multi-select, should check 'inclusion' instead
                    //           : above can be erroneous for multi-select q's
                    let realm = DataProvider.newVHRRealm()
                    let capturedResults = realm.vhrMultiCapturedResults(for: Int(tagComponents[0])!)
                    if(tagOperator == "=="){
                        if(capturedResults.contains(tagValue)){
                            shouldToggle = true
                        }else{
                            shouldToggle = false
                        }
                    }else{
                        if(!capturedResults.contains(tagValue) && capturedResults.count > 0){
                            shouldToggle = true
                        }else{
                            shouldToggle = false
                        }
                    }
//                    print("ge-->VHR: Multi: result NEW \(shouldToggle) : \(capturedResults) \(tagOperator) \(tagValue)")
                }
                
            } else if tagValueType.lowercased() == "number" {
                let serviceFormatter = NumberFormatter.serviceFormatter()
                if let lhs = serviceFormatter.number(from: answer), let rhs = serviceFormatter.number(from: tagValue) {
                    shouldToggle = Self.shouldToggleValue(operator: tagOperator, lhs: lhs, rhs: rhs)
                } else {
                    debugPrint("Cannot convert to number")
                }
            } else if tagValueType.lowercased() == "date" {
                let dateFormatter = DateFormatter.yearMonthDayFormatter()
                if let lhs = dateFormatter.date(from: answer), let rhs = dateFormatter.date(from: tagValue) {
                    shouldToggle = Self.shouldToggleDate(operator: tagOperator, lhs: lhs, rhs: rhs)
                }
            } else {
                shouldToggle = answer.lowercased() == tagValue.lowercased()
            }

            evaluatedTagResults.append(shouldToggle)
        }

        // update visibility only if there were any tags that were evaluated
        if evaluatedTagResults.count > 0 {
            // finally calculate the result
            var isVisible = self.isVisible
            if containsAndTags {
                // if any tag's result is false, the visibility is false
                isVisible = !evaluatedTagResults.contains(false)
            } else if containsOrTags {
                // if any tag's result is true, the visibility is true
                isVisible = evaluatedTagResults.contains(true)
            } else {
                // if the single tag's result is true, the visibility is true
                isVisible = evaluatedTagResults.contains(true)
            }
            self.isVisible = isVisible
        } else {
            // debugPrint("No tags have been evaluated, no updates required to visibility")
        }
    }

    static func shouldToggleName(operator: String, lhs: String, rhs: String) -> Bool {
        if `operator` == "==" {
            return lhs == rhs
        } else if `operator` == "!=" {
            return lhs != rhs
        }
        return false
    }

    static func shouldToggleValue(operator: String, lhs: NSNumber, rhs: NSNumber) -> Bool {
        if `operator` == "==" {
            return lhs.doubleValue == rhs.doubleValue
        } else if `operator` == "!=" {
            return lhs.doubleValue != rhs.doubleValue
        } else if `operator` == ">" {
            return lhs.doubleValue > rhs.doubleValue
        } else if `operator` == ">=" {
            return lhs.doubleValue >= rhs.doubleValue
        } else if `operator` == "<" {
            return lhs.doubleValue < rhs.doubleValue
        } else if `operator` == "<=" {
            return lhs.doubleValue <= rhs.doubleValue
        }
        return false
    }

    static func shouldToggleDate(operator: String, lhs: Date, rhs: Date) -> Bool {
        if `operator` == "==" {
            return lhs == rhs
        } else if `operator` == "!=" {
            return lhs != rhs
        } else if `operator` == ">" {
            return lhs > rhs
        } else if `operator` == ">=" {
            return lhs >= rhs
        } else if `operator` == "<" {
            return lhs < rhs
        } else if `operator` == "<=" {
            return lhs <= rhs
        }
        return false
    }

}

extension VHRQuestion: VisibilityTagUpdatable { }
extension VHRQuestionnaireSection: VisibilityTagUpdatable { }

public extension VisibilityTag {

    func tagComponents() -> [String]? {
        guard !self.isEmpty else { return nil }

        let components = self.components(separatedBy: ";")
        guard components.count == 4 else {
            debugPrint("Incorrect number of visibility components: \(components)")
            return nil
        }
        return components
    }

}
