import UIKit
import RealmSwift
import VIAUtilities

public class VHRQuestionDecorator: Object {
    @objc public dynamic var channelType: QuestionnaireChannelRef = QuestionnaireChannelRef.Unknown
    @objc public dynamic var type: QuestionDecoratorRef = QuestionDecoratorRef.Unknown
    @objc public dynamic var channelTypeCode: String?
    @objc public dynamic var typeName: String?
    @objc public dynamic var channelTypeName: String?
    @objc public dynamic var typeCode: String?
    public let unitOfMeasures: List<VHRUnitOfMeasure> = List<VHRUnitOfMeasure>()
}

extension Realm {
    public func allVHRQuestionDecorators() -> Results<VHRQuestionDecorator> {
        return self.objects(VHRQuestionDecorator.self)
    }
}
