import UIKit
import RealmSwift
import VIAUtilities

public class VHRQuestionnaireProgressAndPointsTracking: Object {
    public let questionnaire: List<VHRQuestionnaire> = List<VHRQuestionnaire>()
    @objc public dynamic var questionnaireSetTypeCode: String?
    @objc public dynamic var questionnaireSetType: QuestionnaireSetRef = QuestionnaireSetRef.Unknown

    @objc public dynamic var totalQuestionnaires: Int = 0
    public let totalQuestionnaireCompleted = RealmOptional<Int>()
    @objc public dynamic var questionnaireSetCompleted: Bool = false
    @objc public dynamic var questionnaireSetTypeName: String?

    public let totalPotentialPoints = RealmOptional<Int>()
    public let totalEarnedPoints = RealmOptional<Int>()

    @objc public dynamic var questionnaireSetText: String = ""
    @objc public dynamic var questionnaireSetTextDescription: String = ""
    @objc public dynamic var questionnaireSetTextNote: String = ""
}

extension Realm {
    public func allVHRQuestionnaireProgressAndPointsTracking() -> Results<VHRQuestionnaireProgressAndPointsTracking> {
        return self.objects(VHRQuestionnaireProgressAndPointsTracking.self)
    }
}
