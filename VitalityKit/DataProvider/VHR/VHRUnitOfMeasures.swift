import UIKit
import RealmSwift
import VIAUtilities

public class VHRUnitOfMeasure: Object {
    @objc public dynamic var value: String = ""

    public func unitOfMeasureRef() -> UnitOfMeasureRef? {
        if let rawValue = Int(self.value) {
            return UnitOfMeasureRef(rawValue: rawValue)
        }
        return nil
    }
}

extension Realm {
    public func allVHRUnitOfMeasures() -> Results<VHRUnitOfMeasure> {
        return self.objects(VHRUnitOfMeasure.self)
    }
}
