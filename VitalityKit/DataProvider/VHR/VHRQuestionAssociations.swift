//
//  VHRQuestionAssociation.swift
//  VitalityActive
//
//  Created by admin on 2017/06/26.
//  Copyright © 2017 Glucode. All rights reserved.
//

import RealmSwift

public class VHRQuestionAssociation: Object {
    @objc public dynamic var childQuestionKey = Int()
    @objc public dynamic var sortIndex = Int()
//    public var childQuestionKey: RealmOptional<Int> = RealmOptional<Int>()
//    public var sortIndex: RealmOptional<Int> = RealmOptional<Int>()
    

}

extension Realm {
    public func allQuestionAssociations() -> Results<VHRQuestionAssociation> {
        return self.objects(VHRQuestionAssociation.self)
    }

    //ge20180113 : This would return true if given childkey exists
    public func findQuestionAssociation(key:Int) -> Bool? {
        let assoc = self.allQuestionAssociations().filter("childQuestionKey == %@", key)
        if(assoc.count > 0){
            return true
        }else{
            return false
        }
    }
}
