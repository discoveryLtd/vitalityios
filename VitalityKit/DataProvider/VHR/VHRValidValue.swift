import RealmSwift
import VIAUtilities

public class VHRValidValue: Object {
    @objc public dynamic var unitOfMeasure: UnitOfMeasureRef = UnitOfMeasureRef.Unknown
    @objc public dynamic var name: String = ""
    @objc public dynamic var value: String = ""
    @objc public dynamic var valueDescription: String = ""
    @objc public dynamic var valueNote: String = ""
    @objc public dynamic var type: QuestionValidValueTypeRef = QuestionValidValueTypeRef.Unknown
    @objc public dynamic var typeName: String?
    @objc public dynamic var typeCode: String?
}

extension Realm {
    public func allVHRValidValues() -> Results<VHRValidValue> {
        return self.objects(VHRValidValue.self)
    }
}
