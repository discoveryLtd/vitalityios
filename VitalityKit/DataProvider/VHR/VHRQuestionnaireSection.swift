import RealmSwift

public class VHRQuestionnaireSection: Object {
    @objc public dynamic var typeKey = Int()  // don't use QuestionnaireSectionsRef as the type. we have to stay dumb to new sections
    @objc public dynamic var typeName: String?
    @objc public dynamic var visibilityTag: VisibilityTag = ""
    public let questions: List<VHRQuestion> = List<VHRQuestion>()
    @objc public dynamic var isVisible: Bool = false
    @objc public dynamic var typeCode: String?
    public let sortOrderIndex = RealmOptional<Int>()

    @objc public dynamic var text: String = ""
    @objc public dynamic var textDescription: String = ""
    @objc public dynamic var textNote: String = ""

    public func sortedQuestions() -> Results<VHRQuestion> {
        let sorted = self.questions.sorted(byKeyPath: "sortOrderIndex", ascending: true)
        return sorted
    }

    public func sortedVisibleQuestions() -> Results<VHRQuestion> {
        let sorted = self.questions.sorted(byKeyPath: "sortOrderIndex", ascending: true).filter("isVisible == true")
        return sorted
    }

    public func isValid() -> Bool {
        var capturedResults = [VHRCapturedResult]()
        var questionsWithoutCapturedResults = [VHRQuestion]()

        // exclude Label questions from validation
        let questionsToBeValidated = questions.filter({ $0.questionType != .Label })
        for question in questionsToBeValidated {
            if let results = question.capturedResults(), results.count > 0 {
                capturedResults.append(contentsOf: results)
            } else if question.isVisible {
                questionsWithoutCapturedResults.append(question)
            }
        }
        
        // validate
        let invalidCapturedResults = capturedResults.filter({ $0.valid == false })
        
        //ge20180418: different validation for medical history because we want to force selection
        if(self.typeCode == "MedicalHistory"){
            var trueCount : Int = 0
            for capturedAnswer in capturedResults {
                if(capturedAnswer.answer == "True"){
                    trueCount += 1
                }
            }
            return trueCount > 0 && invalidCapturedResults.count == 0 && questionsWithoutCapturedResults.count == 0
        }
    
        return invalidCapturedResults.count == 0 && questionsWithoutCapturedResults.count == 0
    }
    
    

    
    /**
     *ge20180416 : create another validation for MedicalHistory's toggle question
     *todo: This needs to be dynamic (not look at typeCode). Waiting for BE to be ready.
     **/
    public func isValidExclusive() -> Bool {
        var capturedResults = [VHRCapturedResult]()
        var questionsWithoutCapturedResults = [VHRQuestion]()
        
        // exclude Label questions from validation
        let questionsToBeValidated = questions.filter({ $0.questionType != .Label })
        for question in questionsToBeValidated {
            if let results = question.capturedResults(), results.count > 0 {
                capturedResults.append(contentsOf: results)
            } else if question.isVisible {
                questionsWithoutCapturedResults.append(question)
            }
        }
        
        var trueCount : Int = 0
        for capturedAnswer in capturedResults {
            if(capturedAnswer.answer == "True"){
                trueCount += 1
            }
        }
        return trueCount > 0
    }
    
    
    /**
     *ge20180416 : create another validation for Multipart questions
     *
     **/
    public func isValidMultipart(parentTypeCode: String) -> Bool {
        
        var questionsWithoutCapturedResults = [VHRQuestion]()
        
        var childQuestionKeys = [Int]()
        var questionsToValidate = [VHRQuestion]()
        var capturedResults = [VHRCapturedResult]()
        var childQs = List<VHRQuestionAssociation>()
        
        childQs = self.questions.filter({ $0.typeCode == parentTypeCode }).first!.childQuestions

        for childQ in childQs{
            childQuestionKeys.append(childQ.childQuestionKey)
            let childQuestion = self.questions.filter({ $0.typeKey == childQ.childQuestionKey }).first!
            questionsToValidate.append(childQuestion)
            
            if let results = childQuestion.capturedResults(), results.count > 0 {
                capturedResults.append(contentsOf: results)
            }else if childQuestion.isVisible {
                questionsWithoutCapturedResults.append(childQuestion)
            }
        }

        return questionsWithoutCapturedResults.count == 0
    }
    
}

extension Realm {
    public func allVHRQuestionnaireSections() -> Results<VHRQuestionnaireSection> {
        return self.objects(VHRQuestionnaireSection.self)
    }

    public func vhrQuestionnaireSection(by questionnaireSectionTypeKey: Int) -> VHRQuestionnaireSection? {
        let result = self.objects(VHRQuestionnaireSection.self).filter { $0.typeKey == questionnaireSectionTypeKey }
        return result.first
    }
}
