import UIKit
import RealmSwift
import VIAUtilities

@objc public enum CompletionState: Int, EnumCollection {
    case Unknown = -1
    case NotStarted = 0
    case InProgress = 1
    case Complete = 2
}

public class VHRQuestionnaire: Object {
    // This is the section/s that a questionnaire may be broken down into. Some questionnaires may not have sections.
    public let questionnaireSections: List<VHRQuestionnaireSection> = List<VHRQuestionnaireSection>()
    @objc public dynamic var typeKey = Int()
    // Date the questionnaire was completed
    @objc public dynamic var completedOn: Date?
    @objc public dynamic var typeName: String?
    @objc public dynamic var completionFlag: Bool = false
    @objc public dynamic var typeCode: String = ""
    @objc public dynamic var currentSectionTypeKey = Int()
    internal dynamic var completionState: CompletionState = CompletionState.Unknown
    public let sortOrderIndex = RealmOptional<Int>()

    @objc public dynamic var text: String = ""
    @objc public dynamic var textDescription: String = ""
    @objc public dynamic var textNote: String = ""
    
    public func getCompletionState() -> CompletionState {
        if self.completionState == CompletionState.NotStarted {
            if hasUserEnteredCapturedResults() {
                return CompletionState.InProgress
            }
        }
        return self.completionState
    }
    
    public func setCompletedAndSubmitted() {
        self.completionFlag = true
        self.updateCompletionState(.Complete)
    }
    
    public func updateCompletionState(_ state: CompletionState) {
        self.completionState = state
    }

    public func sortedQuestionnaireSections() -> Results<VHRQuestionnaireSection> {
        return self.questionnaireSections.sorted(byKeyPath: "sortOrderIndex", ascending: true)
    }

    public func sortedVisibleQuestionnaireSections() -> Results<VHRQuestionnaireSection> {
        let sorted = self.questionnaireSections.sorted(byKeyPath: "sortOrderIndex", ascending: true).filter("isVisible == true")
        return sorted
    }

    static func reevaluateVisibilityTagsForAllCapturedResults(in realm: Realm) {
        let capturedResults = realm.allVHRCapturedResults()
        capturedResults.forEach({ result in
            if let answer = result.answer {
                VHRQuestionnaire.updateVisibilityTags(for: answer, for: result.questionTypeKey, in: realm)
            } else {
                debugPrint("No answer to use")
            }
        })
    }

    public static func updateVisibilityTags(for answer: String, for questionTypeKey: Int,
                                            in realm: Realm = DataProvider.newVHRRealm(),
                                            allowsMultipleAnswers: Bool = false) {
        let questionnaireSections = realm.allVHRQuestionnaireSections()
        let questions = realm.allVHRQuestions()
        try! realm.write {
            questionnaireSections.forEach({ $0.updateVisibility(basedOn: questionTypeKey, answer: answer,
                                                                allowsMultipleAnswers: allowsMultipleAnswers) })
            questions.forEach({ $0.updateVisibility(basedOn: questionTypeKey, answer: answer,
                                                    allowsMultipleAnswers: allowsMultipleAnswers) })
            updateAllQuestionsVisibilityBasedOnParentVisibility()
        }
    }

    public static func updateAllQuestionsVisibilityBasedOnParentVisibility() {
        let realm = DataProvider.newVHRRealm()
        let allQuestionsWithVisibilityTags = realm.allVHRQuestions().filter({ !$0.visibilityTag.isEmpty })
        var allQuestionTypeKeys: [Int] = allQuestionsWithVisibilityTags.map({ $0.typeKey })

        while allQuestionTypeKeys.count > 0 {
            guard let questionTypeKey = allQuestionTypeKeys.first else { return }
            guard let question = realm.question(by: questionTypeKey) else { return }
            updateQuestionVisibilityRecursivelyBasedOnParentVisibility(question, keys: &allQuestionTypeKeys)
        }
    }

    public static func updateQuestionVisibilityRecursivelyBasedOnParentVisibility(_ question: VHRQuestion, keys: inout [Int]) {
        if let parentQuestionTypeKey = Int(question.visibilityTag.tagComponents()?.first ?? ""),
            let parentQuestion = question.realm?.question(by: parentQuestionTypeKey) {
            // call recursively to check that a parent doesn't exist which has to be toggle first
            updateQuestionVisibilityRecursivelyBasedOnParentVisibility(parentQuestion, keys: &keys)

            // update
            if let tenantID = AppSettings.getAppTenant(), tenantID == .GI {
                /** FC-22276 & FC-22274 : Generali Germany
                 * Input Field is not being displayed
                 */
                
                /* Check if the question decorator is in YesNo */
                if parentQuestion.questionDecorator?.typeCode == "YesNo" {
                    /* If the parentQuestion contains an answer and the answer is Yes, make the child question visible. */
                    if let answer = parentQuestion.capturedResult()?.answer,
                        answer == "PositiveResponse" && parentQuestion.isVisible {
                        question.isVisible = true
                    } else {
                        question.isVisible = false
                    }
                } else {
                    let newVisibility = question.isVisible && parentQuestion.isVisible
                    if question.isVisible != newVisibility {
                        //                debugPrint("Changing \(question.typeKey) from isVisible \(question.isVisible ? "✅" : "❌") to \(newVisibility ? "✅" : "❌")")
                        question.isVisible = newVisibility
                    }
                }
            } else {
                let newVisibility = question.isVisible && parentQuestion.isVisible
                if question.isVisible != newVisibility {
                    //                debugPrint("Changing \(question.typeKey) from isVisible \(question.isVisible ? "✅" : "❌") to \(newVisibility ? "✅" : "❌")")
                    question.isVisible = newVisibility
                }
            }
        }

        // finally remove the key, whether it was actioned or not
        keys.remove(object: question.typeKey)
    }
    
    public func hasUserEnteredCapturedResults() -> Bool {
        for section in self.questionnaireSections {
            if let capturedResults = self.realm?.vhrCapturedResults(for: section.typeKey) {
                if Array(capturedResults).filter({ $0.prepopulationEventKey.value == nil }).count > 0 {
                    return true
                }
            }
        }
        return false
    }
}

extension Realm {
    public func allVHRQuestionnaires() -> Results<VHRQuestionnaire> {
        return self.objects(VHRQuestionnaire.self)
    }

    public func allSortedVHRQuestionnaires() -> Results<VHRQuestionnaire> {
        return self.objects(VHRQuestionnaire.self).sorted(byKeyPath: "sortOrderIndex", ascending: true)
    }

    public func getVHRQuestionnaire(using selectedQuestionnaireTypeKey: Int) -> VHRQuestionnaire? {
        let result = self.objects(VHRQuestionnaire.self).filter { $0.typeKey == selectedQuestionnaireTypeKey }
        return result.first
    }

    public func incompleteVHRQuestionnaires() -> [VHRQuestionnaire] {
        let result = allSortedVHRQuestionnaires().filter("completionFlag == false")
        var allIncomplete = [VHRQuestionnaire]()
        for item in result {
            allIncomplete.append(item)
        }
        return allIncomplete
    }

    public func hasAnswersSaved(questionnaire: VHRQuestionnaire) -> Bool {
        let results = self.objects(VHRCapturedResult.self).filter { $0.questionnaireTypeKey == questionnaire.typeKey }
        return results.count > 0
    }

}
