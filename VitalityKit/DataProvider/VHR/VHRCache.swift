//
//  VHRCache.swift
//  VitalityActive
//
//  Created by admin on 2017/05/25.
//  Copyright © 2017 Glucode. All rights reserved.
//

import UIKit
import RealmSwift
import SwiftDate

public class VHRCache: Object {
    @objc public dynamic var dateCreated: Date = Date()
}

extension Realm {
    public class func resetVHRRealmIfOutdated() {
        let realm = DataProvider.newVHRRealm()
        realm.reset()
        if VHRCache.vhrDataIsOutdated() {
            let vhrCache = VHRCache()
            try! realm.write {
                realm.add(vhrCache)
            }
        }
    }

    public class func resetVHRRealm() {
        let realm = DataProvider.newVHRRealm()
        realm.reset()
    }
}

extension VHRCache {

    public class func vhrDataIsOutdated(_ daysValid: Int = 30) -> Bool {
        guard let vhrCache = VHRCache.currentVHRCache() else {
            return true
        }
        let calendar = Calendar.current
        let cachedDate = calendar.startOfDay(for: vhrCache.dateCreated)
        let todaysDate = calendar.startOfDay(for: Date())

        let components = calendar.dateComponents([.day], from: cachedDate, to: todaysDate)

        if let daysDifference = components.day {
            if daysDifference > daysValid {
                return true
            } else {
                return false
            }
        } else {
            return true
        }
    }

    public class func currentVHRCache() -> VHRCache? {
        let realm = DataProvider.newVHRRealm()
        return realm.objects(VHRCache.self).first
    }
}
