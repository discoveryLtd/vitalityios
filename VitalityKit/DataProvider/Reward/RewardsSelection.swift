import RealmSwift
import VIAUtilities

public class ARRewardsSelection: Object {
    @objc public dynamic var id: Int = -1
    @objc public dynamic var unclaimedRewardId: Int = -1
    @objc public dynamic var rewardCategoryCode: String?
    @objc public dynamic var rewardCategoryKey: Int = -1 // ref data
    @objc public dynamic var rewardCategoryName: String?
    @objc public dynamic var rewardId: RewardReferences = RewardReferences.Unknown
    @objc public dynamic var rewardKey: Int = -1 // ref data
    @objc public dynamic var rewardName: String = ""
    @objc public dynamic var rewardOptionTypeCode: String?
    @objc public dynamic var rewardOptionTypeKey: Int = -1 // ref data
    @objc public dynamic var rewardOptionTypeName: String = ""
    @objc public dynamic var rewardProvidedByPartyId: Int = -1
    @objc public dynamic var rewardTypeCategoryCode: String?
    @objc public dynamic var rewardTypeCategoryKey: Int = -1 // ref data
    @objc public dynamic var rewardTypeCategoryName: String?
    @objc public dynamic var rewardTypeCode: String?
    @objc public dynamic var rewardTypeKey: Int = -1 // ref data
    @objc public dynamic var rewardTypeName: String?
    @objc public dynamic var rewardValueItemDescription: String?
    @objc public dynamic var rewardValueLinkId: Int = -1
    public let rewardValueQuantity: RealmOptional<Int> = RealmOptional<Int>()
    public let rewardValuePercent: RealmOptional<Int> = RealmOptional<Int>()
    public dynamic var rewardValueAmount: String?
    @objc public dynamic var rewardValueTypeCode: String?
    @objc public dynamic var rewardValueTypeKey: Int = -1 // ref data
    @objc public dynamic var rewardValueTypeName: String = ""
    @objc public dynamic var sortOrder: Int = -1
}

extension Realm {
    public func arRewardSelections(with unclaimedRewardId: Int) -> Results<ARRewardsSelection> {
        let allSelections = self.objects(ARRewardsSelection.self)
        return allSelections.filter("unclaimedRewardId == %@", unclaimedRewardId)
    }
}
