import RealmSwift
import VIAUtilities

public class ARUnclaimedReward: Object {
    @objc public dynamic var id: Int = -1
    @objc public dynamic var agreementPartyId: Int = -1
    @objc public dynamic var awardedOn: Date?
    @objc public dynamic var awardedRewardStatus: Int = -1 // ref data - loop and find latest [
    @objc public dynamic var awardedRewardStatusEffectiveOn: Date?
    @objc public dynamic var effectiveFrom: Date?
    @objc public dynamic var effectiveTo: Date?
    public let eventId: RealmOptional<Int> = RealmOptional<Int>()
    @objc public dynamic var unclaimedRewardId: Int = -1
    public let outcomeRewardValueLinkId: RealmOptional<Int> = RealmOptional<Int>()
    @objc public dynamic var partyId: Int = -1
    @objc public dynamic var rewardCategoryCode: String?
    @objc public dynamic var rewardCategoryKey: Int = -1
    @objc public dynamic var rewardCategoryName: String?
    @objc public dynamic var rewardId: RewardReferences = RewardReferences.Unknown
    @objc public dynamic var rewardkey: Int = -1// not in reference data but we need to 1 = Wheel Spin, 7 = Choose Reward
    @objc public dynamic var rewardName: String = ""
    @objc public dynamic var rewardVoucherLink: String = ""
    @objc public dynamic var rewardOptionTypeCode: String?
    @objc public dynamic var rewardOptionTypeKey:  Int = -1// ref data
    @objc public dynamic var rewardOptionTypeName: String = ""
    @objc public dynamic var rewardProvidedByPartyId: Int = -1
    @objc public dynamic var rewardTypeCategoryCode: String?
    @objc public dynamic var rewardTypeCategoryKey: Int = -1 // refdata
    @objc public dynamic var rewardTypeCategoryName: String?
    @objc public dynamic var rewardTypeCode: String?
    @objc public dynamic var rewardTypeKey: Int = -1 // ref data
    @objc public dynamic var rewardTypeName: String?
    @objc public dynamic var rewardProviderPartyId: Int = -1
    @objc public dynamic var rewardSelectionTypeCode: String?
    public let rewardSelectionTypeKey: RealmOptional<Int> = RealmOptional<Int>()
    @objc public dynamic var rewardSelectionTypeName: String?
    public let rewardSelections: List<ARRewardsSelection> = List<ARRewardsSelection>()
    @objc public dynamic var rewardValueItemDescription: String?
    public let rewardValueLinkId: RealmOptional<Int> = RealmOptional<Int>()
    public let rewardValueQuantity: RealmOptional<Int> = RealmOptional<Int>()
    public let rewardValuePercent: RealmOptional<Int> = RealmOptional<Int>()
    public dynamic var rewardValueAmount: String?
    @objc public dynamic var rewardValueTypeCode: String?
    @objc public dynamic var rewardValueTypeKey: Int = -1 // ref data
    @objc public dynamic var rewardValueTypeName: String?
    
    override public static func primaryKey() -> String? {
        return "id"
    }
}

extension Realm {
    public func allUnclaimedRewards() -> Results<ARUnclaimedReward> {
        let allUnclaimedRewards = self.objects(ARUnclaimedReward.self).filter("awardedRewardStatus != %@", AwardedRewardStatusRef.Used.rawValue)
        return allUnclaimedRewards
    }
    
    public func allUnclaimedUsedRewards() -> Results<ARUnclaimedReward> {
        let allUnclaimedRewards = self.objects(ARUnclaimedReward.self).filter("awardedRewardStatus == %@", AwardedRewardStatusRef.Used.rawValue)
        return allUnclaimedRewards
    }
    
    public func allUnclaimedExpiredRewards() -> Results<ARUnclaimedReward> {
        let allUnclaimedRewards = self.objects(ARUnclaimedReward.self).filter("awardedRewardStatus == %@", AwardedRewardStatusRef.Expired.rawValue)
        return allUnclaimedRewards
    }
    
    public func unclaimedRewards(with id: Int) -> Results<ARUnclaimedReward> {
        let unclaimedReward = self.allUnclaimedRewards().filter("unclaimedRewardId == %@", id)
        return unclaimedReward.sorted(byKeyPath: "effectiveFrom")
    }
    
    public func availableSwapRewards() -> Results<ARUnclaimedReward> {
        let availableSwapReward = self.allUnclaimedRewards()
        return availableSwapReward.sorted(byKeyPath: "effectiveFrom")
    }
}
