import RealmSwift
import VIAUtilities

public class ARVoucher: Object {
    public dynamic var agreementPartyId: Int = -1
    public dynamic var awardedOn: Date?
    public let awardedRewardReferenceVoucherNumbers = List<ARVoucherCode>() // loop through array of references and pull put voucher typekey, cinema might have multiple voucher codes
    public dynamic var awardedRewardStatus: Int = -1 // reference data, keep only latest status
    public dynamic var awardedRewardStatusEffectiveOn: Date?
    public dynamic var effectiveFrom: Date?
    public dynamic var effectiveTo: Date?
    public dynamic var exchangeExchangeOn: Date?
    public let exchangeFromRewardId: RealmOptional<Int> = RealmOptional<Int>()
    public let exchangeToRewardId: RealmOptional<Int> = RealmOptional<Int>()
    public dynamic var exchangeTypeCode: String?
    public let exchangeTypeKey: RealmOptional<Int> = RealmOptional<Int>()
    public dynamic var exchangeTypeName: String?
    public let partnerSysRewardId = List<ARPartnerSysRewardId>()
    public dynamic var id: Int = -1
    public dynamic var partyId: Int = -1
    public dynamic var rewardCategoryCode: String?
    public dynamic var rewardCategoryKey: Int = -1
    public dynamic var rewardCategoryName: String?
    public dynamic var rewardId: RewardReferences = RewardReferences.Unknown
    public dynamic var rewardkey: Int = -1 // not in reference data but we need to
    public dynamic var rewardName: String = ""
    public dynamic var rewardOptionTypeCode: String?
    public dynamic var rewardOptionTypeKey: Int = -1 // reference data
    public dynamic var rewardOptionTypeName: String = ""
    public dynamic var rewardProvidedByPartyId: Int = -1
    public dynamic var rewardTypeCategoryCode: String?
    public dynamic var rewardTypeCategoryKey: Int = -1 // reference data
    public dynamic var rewardTypeCategoryName: String?
    public dynamic var rewardTypeCode: String?
    public dynamic var rewardTypeKey: Int = -1 // reference data
    public dynamic var rewardTypeName: String?
    public dynamic var rewardProviderPartyId: Int = -1 //Probably reference data
    public dynamic var rewardValueAmount: String?
    @objc public dynamic var rewardSelectionTypeCode: String?
    public let rewardSelectionTypeKey: RealmOptional<Int> = RealmOptional<Int>()
    @objc public dynamic var rewardSelectionTypeName: String?
    public let rewardSelections: List<ARRewardsSelection> = List<ARRewardsSelection>()
    public let rewardValuePercent: RealmOptional<Int> = RealmOptional<Int>()
    public let rewardValueQuantity: RealmOptional<Int> = RealmOptional<Int>()
    public dynamic var rewardValueLinkId: Int = -1
    public dynamic var rewardValueTypeCode: String?
    public dynamic var rewardValueTypeKey: Int = -1 // reference data
    public dynamic var rewardValueTypeName: String?
    public dynamic var rewardValueItemDescription: String?
    
    override public static func primaryKey() -> String? {
        return "id"
    }
    
    public func partnerSysRewardIds() -> [String] {
        var partnerSysRewardIds = [String]()
        for sysRewardId in partnerSysRewardId {
            if let value = sysRewardId.value {
                partnerSysRewardIds.append(value)
            }
        }
        return partnerSysRewardIds
    }
    
    public func voucherCodes() -> [String] {
        var voucherCodes = [String]()
        for voucher in awardedRewardReferenceVoucherNumbers {
            if let value = voucher.value {
                voucherCodes.append(value)
            }
        }
        return voucherCodes
    }
    
}

public class ARVoucherCode: Object {
    public dynamic var value: String? = nil
}

public class ARPartnerSysRewardId: Object {
    public dynamic var value: String? = nil
}

extension Realm {
    
    public func allVouchers() -> Results<ARVoucher> {
        let allVouchers = self.objects(ARVoucher.self)
        return allVouchers
    }
    
    public func allEffectiveVouchers() -> Results<ARVoucher> {
        let allVouchers = self.allVouchers()
        return allVouchers.filter("(awardedRewardStatus != %@) && (awardedRewardStatus != %@)", AwardedRewardStatusRef.Expired.rawValue, AwardedRewardStatusRef.Used.rawValue)
    }
    
    public func allUsedVouchers() -> Results<ARVoucher> {
        let allVouchers = self.allVouchers()
        return allVouchers.filter("awardedRewardStatus == %@", AwardedRewardStatusRef.Used.rawValue)
    }
    
    public func allAcknowledgedVouchers() -> Results<ARVoucher> {
        let allVouchers = self.allVouchers()
        return allVouchers.filter("awardedRewardStatus == %@", AwardedRewardStatusRef.Acknowledged.rawValue)
    }
	
    public func allExpiredVouchers() -> Results<ARVoucher> {
        let allVouchers = self.allVouchers()
        return allVouchers.filter("awardedRewardStatus == %@", AwardedRewardStatusRef.Expired.rawValue)
	}
	
    public func allCanceledVouchers() -> Results<ARVoucher> {
        let allVouchers = self.allVouchers()
        return allVouchers.filter("awardedRewardStatus == %@", AwardedRewardStatusRef.Canceled.rawValue)
    }
    
    public func allIssueFailedVouchers() -> Results<ARVoucher> {
        let allVouchers = self.allVouchers()
        return allVouchers.filter("awardedRewardStatus == %@", AwardedRewardStatusRef.IssueFailed.rawValue)
    }
    
    public func rewardVouchers(with id: Int) -> Results<ARVoucher> {
        let allVouchers = self.allVouchers()
        return allVouchers.filter("id == %@", id)
    }
        
    public func voucherCode(with value: String) -> Results<ARVoucherCode> {
        return self.objects(ARVoucherCode.self).filter("value == %@", value)
    }
    
    public func partnerSysRewardId(with value: String) -> Results<ARPartnerSysRewardId> {
        return self.objects(ARPartnerSysRewardId.self).filter("value == %@", value)
    }
}
