import Foundation
import Alamofire

class UnauthorisedAPIManagerAccessAdapter: RequestAdapter {

    var accessToken: String {
        return Wire.default.apiManagerIdentifier
    }
    
    private let userAgent: String

    init(_ userAgent: String) {
        self.userAgent = userAgent
    }

    func adapt(_ urlRequest: URLRequest) throws -> URLRequest {
        var adaptedRequest = urlRequest

        let urlString = try Wire.default.baseURL().absoluteString
        if let hasPrefix = adaptedRequest.url?.absoluteString.hasPrefix(urlString), hasPrefix == true {
            adaptedRequest = AdapterUtilities.addCommonHeaders(urlRequest: adaptedRequest, userAgent: self.userAgent)
            adaptedRequest.setValue("Basic " + accessToken, forHTTPHeaderField: "Authorization")
        } else {
            debugPrint("Unknown request") // TODO: ?  throw exception?
        }

        return adaptedRequest
    }
}
