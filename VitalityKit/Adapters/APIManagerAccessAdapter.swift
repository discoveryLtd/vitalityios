import Foundation
import Alamofire

class APIManagerAccessAdapter: RequestAdapter {

    private let accessToken: String
    private let userAgent: String

    init(accessToken: String, userAgent: String) {
        self.accessToken = accessToken
        self.userAgent = userAgent
    }

    func adapt(_ urlRequest: URLRequest) throws -> URLRequest {
        var adaptedRequest = urlRequest

        let urlString = try Wire.default.baseURL().absoluteString
        if let hasPrefix = adaptedRequest.url?.absoluteString.hasPrefix(urlString), hasPrefix == true {
            adaptedRequest = AdapterUtilities.addCommonHeaders(urlRequest: adaptedRequest, userAgent: self.userAgent)
            adaptedRequest.setValue("Bearer " + accessToken, forHTTPHeaderField: "Authorization")

        } else {
            debugPrint("Unknown request") // TODO: ?  throw exception?
        }

        return adaptedRequest
    }
}
