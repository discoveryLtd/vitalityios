//
//  AdapterUtilities.swift
//  VitalityActive
//
//  Created by Simon Stewart on 3/9/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation

public final class AdapterUtilities {
    static func addCommonHeaders(urlRequest: URLRequest, userAgent: String = UserAgent.toString()) -> URLRequest {

        var adaptedRequest = urlRequest

        adaptedRequest.setValue(userAgent,              forHTTPHeaderField: "User-Agent")
        adaptedRequest.setValue(UUID().uuidString,      forHTTPHeaderField: "correlation-id")
        adaptedRequest.setValue(Wire.default.sessionID, forHTTPHeaderField: "session-id")
        adaptedRequest.setValue(UUID().uuidString,      forHTTPHeaderField: "message-id")
        adaptedRequest.setValue(DeviceLocale.toString(),forHTTPHeaderField: "locale")
        adaptedRequest.setValue(getTimestamp(),         forHTTPHeaderField: "x-vsl-timestamp")
        adaptedRequest.setValue("1",                    forHTTPHeaderField: "x-vsl-version")

        return adaptedRequest

    }
    
    private static func getTimestamp() -> String{
        return "\(UInt64(floor(Date().timeIntervalSince1970 * 1000)))"
    }
}
