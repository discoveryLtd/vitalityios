//
//  UIDevice.swift
//  VitalityActive
//
//  Created by Von Kervin R. Tuico on 02/04/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import UIKit

public extension UIDevice {
    //Device Type enum
    enum DeviceType: Int {
        
        //Apple UnknownDevices
        case UnknownDevice = 0
        
        //Simulator
        case Simulator
        
        //Apple Air pods
        case AppleAirPods
        
        //Apple TV
        case AppleTV2G
        case AppleTV3G
        case AppleTV4G
        case AppleTV4K
        
        //Apple Watch
        case AppleWatch
        case AppleWatchSeries1
        case AppleWatchSeries2
        case AppleWatchSeries3
        
        //Apple Home Pods
        case AppleHomePods
        
        //Apple iPad
        case AppleIpad
        case AppleIpad2
        case AppleIpad3
        case AppleIpad4
        case AppleIpadAir
        case AppleIpadAir2
        case AppleIpadPro_12_9
        case AppleIpadPro_9_7
        case AppleIpad5
        case AppleIpadPro_12_9_Gen_2
        case AppleIpadPro_10_5
        case AppleIpadMini
        case AppleIpadMini2
        case AppleIpadMini3
        case AppleIpadMini4
        
        //Apple iPhone
        case AppleIphone
        case AppleIphone3G
        case AppleIphone3GS
        case AppleIphone4
        case AppleIphone4S
        case AppleIphone5
        case AppleIphone5C
        case AppleIphone5S
        case AppleIphone6
        case AppleIphone6P
        case AppleIphone6S
        case AppleIphone6SP
        case AppleIphoneSE
        case AppleIphone7
        case AppleIphone7P
        case AppleIphone8
        case AppleIphone8P
        case AppleIphoneX
        
        //Apple iPod touch
        case AppleIpodTouch
        case AppleIpodTouch2G
        case AppleIpodTouch3G
        case AppleIpodTouch4G
        case AppleIpodTouch5G
        case AppleIpodTouch6G
    }
    
    // Method for device type
    func getDeviceType() -> DeviceType{
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        
        switch identifier {
            
        //Simulator
        case "i386","x86_64": return .Simulator
            
        //Apple Air Pods
        case "AirPods1,1": return .AppleAirPods
            
        //Apple TV
        case "AppleTV2,1": return .AppleTV2G
        case "AppleTV3,1", "AppleTV3,2": return .AppleTV3G
        case "AppleTV5,3": return .AppleTV4G
        case "AppleTV6,2": return .AppleTV4K
            
        //Apple Watch
        case "Watch1,1", "Watch1,2": return .AppleWatch
        case "Watch2,6", "Watch2,7": return .AppleWatchSeries1
        case "Watch2,3", "Watch2,4": return .AppleWatchSeries2
        case "Watch3,1", "Watch3,2", "Watch3,3", "Watch3,4": return .AppleWatchSeries3
            
        // Apple HomePods
        case "AudioAccessory1,1": return .AppleHomePods
            
        //Apple iPad
        case "iPad1,1": return .AppleIpad
        case "iPad2,1", "iPad2,2", "iPad2,3", "iPad2,4": return .AppleIpad2
        case "iPad3,1", "iPad3,2", "iPad3,3": return .AppleIpad3
        case "iPad3,4", "iPad3,5", "iPad3,6": return .AppleIpad4
        case "iPad4,1", "iPad4,2", "iPad4,3": return .AppleIpadAir
        case "iPad5,3", "iPad5,4": return .AppleIpadAir2
        case "iPad6,7", "iPad6,8": return .AppleIpadPro_12_9
        case "iPad6,3", "iPad6,4": return .AppleIpadPro_9_7
        case "iPad6,11", "iPad6,12": return .AppleIpad5
        case "iPad7,1", "iPad7,2" : return .AppleIpadPro_12_9_Gen_2
        case "iPad7,3", "iPad7,4" : return .AppleIpadPro_10_5
        case "iPad2,5", "iPad2,6", "iPad2,7": return .AppleIpadMini
        case "iPad4,4", "iPad4,5", "iPad4,6": return .AppleIpadMini2
        case "iPad4,7", "iPad4,8", "iPad4,9": return .AppleIpadMini3
        case "iPad5,1", "iPad5,2": return .AppleIpadMini4
            
        //Apple iPhone
        case "iPhone1,1": return .AppleIphone
        case "iPhone1,2": return .AppleIphone3G
        case "iPhone2,1": return .AppleIphone3GS
        case "iPhone3,1", "iPhone3,2", "iPhone3,3": return .AppleIphone4
        case "iPhone4,1": return .AppleIphone4S
        case "iPhone5,1", "iPhone5,2": return .AppleIphone5
        case "iPhone5,3", "iPhone5,4": return .AppleIphone5C
        case "iPhone6,1", "iPhone6,2": return .AppleIphone5S
        case "iPhone7,2": return .AppleIphone6
        case "iPhone7,1": return .AppleIphone6P
        case "iPhone8,1": return .AppleIphone6S
        case "iPhone8,2": return .AppleIphone6SP
        case "iPhone8,4": return .AppleIphoneSE
        case "iPhone9,1", "iPhone9,3": return .AppleIphone7
        case "iPhone9,2", "iPhone9,4": return .AppleIphone7P
        case "iPhone10,1", "iPhone10,4": return .AppleIphone8
        case "iPhone10,2", "iPhone10,5": return .AppleIphone8P
        case "iPhone10,3", "iPhone10,6": return .AppleIphoneX
            
        //Apple iPod touch
        case "iPod1,1": return .AppleIpodTouch
        case "iPod2,1": return .AppleIpodTouch2G
        case "iPod3,1": return .AppleIpodTouch3G
        case "iPod4,1": return .AppleIpodTouch4G
        case "iPod5,1": return .AppleIpodTouch5G
        case "iPod7,1": return .AppleIpodTouch6G
            
        default:
            return .UnknownDevice
        }
    }
}
