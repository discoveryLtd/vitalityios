import Foundation


public class ResourceFileDownloader {

    // MARK: Enums

    public enum DownloaderError: Error {
        case couldNotDownload(message: String?, fileName: String?)
        case couldNotSave(message: String?, fileName: String?)

        func fileName() -> String? {
            switch self {
            case .couldNotDownload(_, let fileName):
                return fileName
            case .couldNotSave(_, let fileName):
                return fileName
            }
        }
    }

    public enum DownloaderState {
        case idle
        case downloading
    }

    // MARK: Properties

    public private (set) var state: DownloaderState = .idle

    private var maxRetries = 3

    // MARK: Init

    public init() {

    }

    // MARK: Functions

    public func downloadResourceFilesWithRetries(completion: @escaping () -> Void) {
//        debugPrint("ResourceFileDownloader.downloadResourceFilesIfNeeded")
        var filesToBeDownloaded = AppConfigFeature.allFilesToBeDownloaded()

        var download: (([String]) -> Void)!
        download = { fileNames in
//            debugPrint("Downloading files: \(fileNames)")
            self.download(fileNames: fileNames) { (errors) in
                // check if we reached our limit
                if (self.maxRetries == 0 ) {
                    // argh!
//                    debugPrint("Guess we'll just load the files from bundle...")
                    completion()
                    return
                }

                // decrease retries
                self.maxRetries -= 1

                // handle any failures by redownloading, recursive closure!
                if errors.count > 0 {
                    filesToBeDownloaded = errors.flatMap({ $0.fileName() })
//                    debugPrint("Try to redownload failed files: \(filesToBeDownloaded)")
                    download(filesToBeDownloaded)
                // everything downloaded successfully, complete
                } else {
                    completion()
                }
            }
        }

        download(filesToBeDownloaded)
    }

    public func download(fileNames: [String], completion: @escaping ((_: [DownloaderError]) -> Void)) {

        DispatchQueue.global(qos: .userInitiated).async {

            self.state = .downloading
            var storedErrors = [DownloaderError]()
            let dispatchGroup = DispatchGroup()

            for fileName in fileNames {
                dispatchGroup.enter()

                self.doWork(fileName: fileName) { fileUrl, error in
                    if error != nil {
                        storedErrors.append(error!)
                    }
                    dispatchGroup.leave()
                }
            }

            dispatchGroup.wait()
            DispatchQueue.main.async {
                self.state = .idle
                completion(storedErrors)
            }
        }
    }

    func fileExists(_ fileURL: URL) -> Bool {
        let filePath = fileURL.path
        return FileManager.default.fileExists(atPath: filePath)
    }

    func destinationURL(for fileName: String) -> URL {
        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        return documentsURL.appendingPathComponent("\(fileName)")
    }

    private func doWork(fileName: String, completion: @escaping ((_: String?, _: DownloaderError?) -> Void)) {

        guard let insurerCMSGroupId = AppConfigFeature.insurerCMSGroupId() else {
            let error = DownloaderError.couldNotDownload(message:"Missing insurer group id for CMS.", fileName: nil)
            completion(nil, error)
            return
        }

        let lastModifiedDate = CMSFile.downloadDate(for: fileName)
        Wire.Content.getFileByGroupId(fileName: fileName, groupId: insurerCMSGroupId, modifiedSince: lastModifiedDate, completion: { data, error in

            guard error == nil else {
                let error = DownloaderError.couldNotDownload(message: "Error getting file \(fileName) from CMS.  Error \(error!.localizedDescription)", fileName: fileName)
                return completion(nil, error)
            }

            guard let responseData = data else {
                let error = DownloaderError.couldNotDownload(message:"No response getting file \(fileName) from CMS.", fileName: fileName)
                return completion(nil, error)
            }

            do {
                let fileURL = self.destinationURL(for: fileName)
                try responseData.write(to: fileURL)

                guard self.fileExists(fileURL) else {
                    let error = DownloaderError.couldNotSave(message:"Did not save file \(fileName)- no reason", fileName: fileName)
                    return completion(nil, error)
                }

                let realm = DataProvider.newRealm()
                realm.updateFileLastAccessedDate(for: fileName)
                return completion(fileURL.path, nil)

            } catch let error as NSError {
                let error = DownloaderError.couldNotSave(message:"File \(fileName).  Error \(error.description)", fileName: fileName)
                return completion(nil, error)
            }

        })
    }

}
