import Foundation
import VIAUtilities

public class DeviceLocale {
    
    public static func toString() -> String {
        if let tenantID = AppSettings.getAppTenantId(), let tenant = TenantReference(rawValue: tenantID) {
            switch tenant{
            case .CA, .SLI, .UKE, .IGI, .EC, .ASR, .MLI, .GI:
                if Locale.current.languageCode == "en" {
                    return "en_US"
                } else if Locale.current.languageCode == "ja" {
                    return "ja_JP"
                }
                break
            default:
                /* Fallback below */
                break
            }
        }
        
        guard let languageCode = Locale.current.languageCode, let languageRegion = Locale.current.regionCode else {
            return Locale.current.identifier
        }
        
        return languageCode.appending("_").appending(languageRegion)
    }
}
