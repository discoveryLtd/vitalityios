import Foundation
import RealmSwift

extension Parser {
    public struct WellnessDevicesAndApps {
        public static func parseGetDevices(response: VDPGetDevicesResponse, completion: @escaping (_ response: VDPGetDevicesResponse?, _ error: Error?) -> Void) {
            Parser.main.parse {
                WDAMarket.persistGetDevices(response: response)
                DispatchQueue.main.async {
                    completion(nil, nil)
                }
            }
        }
    }
}

extension DataProvider {

    public class func deleteAllWDARealmDataExceptDeviceActivities() {
        let wdaRealm = DataProvider.newWDARealm()
        //if cache is outdated we can delete all data
        guard WDACache.wdaDataIsOutdated() == false else { wdaRealm.reset(); return }

        let inMemoryWDARealm = DataProvider.newInMemoryRealm()
        //Copy cache, all WDA device activities and points with tiers results to in-memory realm
        let currentWDACache = wdaRealm.currentWDACacheFromRealm()
        let deviceActivities = wdaRealm.allWDAAvailableDeviceActivities()
        try! inMemoryWDARealm.write {
            for deviceActivity in deviceActivities {
                inMemoryWDARealm.create(WDAAvailableDeviceActivities.self, value: deviceActivity)
            }
            guard  let wdaCache = currentWDACache else {return}
            inMemoryWDARealm.create(WDACache.self, value: wdaCache)
        }
        
        //Delete all data in WDA realm
        wdaRealm.reset()

        //Copy cache, WDA device activities and points with tiers results from in-memory realm back into to wdaRealm
        let inMemoryWDACache = inMemoryWDARealm.currentWDACacheFromRealm()
        let inMemoryDeviceActivities = inMemoryWDARealm.allWDAAvailableDeviceActivities()
        try! wdaRealm.write {
            for deviceActivity in inMemoryDeviceActivities {
                wdaRealm.create(WDAAvailableDeviceActivities.self, value: deviceActivity)
            }
            guard let cache = inMemoryWDACache else { return }
            wdaRealm.create(WDACache.self, value: cache)
        }
    }

}

extension WDAMarket {

    public class func persistGetDevices(response: VDPGetDevicesResponse) {
        DataProvider.deleteAllWDARealmDataExceptDeviceActivities()
        let wdaRealm = DataProvider.newWDARealm()
        let markets = response.markets
        var wdaMarketRealmItems = [WDAMarket]()

        for market in markets {
            if !wdaMarketRealmItems.contains(where: { $0.partner?.device == market.partner.device}) {
                let wdaMarket = WDAMarket.parseWDAMarket(responseMarket: market)
                wdaMarketRealmItems.append(wdaMarket)
            }
        }


        try! wdaRealm.write {
            for realmItem in wdaMarketRealmItems {
                wdaRealm.add(realmItem)
            }
        }
        NotificationCenter.default.post(name: .VIAWDADidFetchDevices, object: nil)
    }

    public class func parseWDAMarket(responseMarket: VDPMarkets) -> WDAMarket {
        let wdaMarket = WDAMarket()
        let partner = responseMarket.partner
        let assets = responseMarket.assets

        wdaMarket.assets = WDAAssets.parseWDAAssets(responseAssets: assets)
        wdaMarket.partner = WDAPartner.parseWDAPartner(responsePartner: partner)

        return wdaMarket
    }

}

extension WDAAssets {
    public class func parseWDAAssets(responseAssets: VDPAssets) -> WDAAssets {
        let wdaAssets = WDAAssets()

        wdaAssets.partnerLogoUrl = responseAssets.partnerLogoUrl
        wdaAssets.partnerDescription = responseAssets.partnerDescription
        wdaAssets.partnerWebsiteUrl = responseAssets.partnerWebsiteUrl
        wdaAssets.aboutPartnerContentId = responseAssets.aboutPartnerContentId
        wdaAssets.stepsToLinkContentId = responseAssets.stepsToLinkContentId

        return wdaAssets
    }
}

extension WDAPartner {
    public class func parseWDAPartner(responsePartner: VDPPartner) -> WDAPartner {
        let wdaPartner = WDAPartner()

        if let partnerLink = responsePartner.partnerLink {
            wdaPartner.partnerLink = WDAPartnerUrlDetail.parseWDAPartnerUrlDetail(responsePartnerDetail: partnerLink)
        }

        if let partnerDelink = responsePartner.partnerDelink {
            wdaPartner.partnerDelink = WDAPartnerUrlDetail.parseWDAPartnerUrlDetail(responsePartnerDetail: partnerDelink)
        }

        if let partnerSync = responsePartner.partnerSync {
            wdaPartner.partnerSync = WDAPartnerUrlDetail.parseWDAPartnerUrlDetail(responsePartnerDetail: partnerSync)
        }

        wdaPartner.device = responsePartner.device
        wdaPartner.partnerLastSync = Parser.main.parseDateString(responsePartner.partnerLastSync)
        wdaPartner.partnerLastWorkout = Parser.main.parseDateString(responsePartner.partnerLastWorkout)
        if responsePartner.partnerLinkedStatus == WDADeviceLinkedStatus.Unlinked.statusString() {
            wdaPartner.partnerLinkedStatus = .Unlinked
        } else if responsePartner.partnerLinkedStatus == WDADeviceLinkedStatus.Pending.statusString() {
            wdaPartner.partnerLinkedStatus = .Pending
        } else if responsePartner.partnerLinkedStatus == WDADeviceLinkedStatus.Active.statusString() {
            wdaPartner.partnerLinkedStatus = .Active
        } else {
            wdaPartner.partnerLinkedStatus = .Unknown
        }
        wdaPartner.partnerSystem = responsePartner.partnerSystem

        return wdaPartner
    }
}

extension WDAPartnerUrlDetail {
    public class func parseWDAPartnerUrlDetail(responsePartnerDetail: VDPPartnerDetail) -> WDAPartnerUrlDetail {
        let wdaPartnerUrlDetail = WDAPartnerUrlDetail()

        wdaPartnerUrlDetail.url = responsePartnerDetail.url
        wdaPartnerUrlDetail.method = responsePartnerDetail.method

        return wdaPartnerUrlDetail
    }
}
