import Foundation
import RealmSwift
import VIAUtilities

extension Parser {
    public struct ARGoalProgressAndDetails {
        public static func parseGoalProgressAndDetails(response: GoalProgressDetailsResponse, completion: @escaping (_ error: Error?) -> Void) {
            Parser.main.parse {
                var parseError: Error? = nil
                do {
                    if let goalProgressAndDetailsResponse = response.getGoalProgressAndDetailsResponse {
                        try ARGoalTracker.parseAndPersistGoalProgressAndDetails(with: goalProgressAndDetailsResponse)
                    } else {
                        parseError = ParsingError.emptyResponse
                    }
                } catch {
                    parseError = ParsingError.serializationError
                }
                DispatchQueue.main.async {
                    completion(parseError)
                }
            }
        }
    }
}

extension ARGoalTracker {
    public static func parseAndPersistGoalProgressAndDetails(with getGoalProgressAndDetailsResponse: GPDResponseGetGoalProgressAndDetails) {
        guard let goalTrackers = getGoalProgressAndDetailsResponse.goalTrackerOuts else { return }
        var realmGoalTrackers = [ARGoalTracker]()
        
        for goalTracker in goalTrackers {
            realmGoalTrackers.append(parse(goalTracker: goalTracker))
        }
        
        for goalTracker in realmGoalTrackers {
            addOrUpdate(goalTracker: goalTracker)
        }
        
    }
    
    class func addOrUpdate(goalTracker: ARGoalTracker) {
        let arRealm = DataProvider.newARRealm()
        arRealm.refresh()
        try! arRealm.write {
            if let exisitingGoalTracker = arRealm.arGoalTracker(with: goalTracker.id) {
                for objectiveTracker in exisitingGoalTracker.objectiveTrackers {
                    for pointsEntry in objectiveTracker.objectivePointsEntries {
                        arRealm.delete(pointsEntry.objectivePointsEntryMetaDatas)
                        arRealm.delete(pointsEntry.objectivePointsEntryReasons)
                    }
                    for event in objectiveTracker.objectiveEvents {
                        arRealm.delete(event.objectiveEventMetaDatas)
                    }
                    arRealm.delete(objectiveTracker.objectiveEvents)
                    arRealm.delete(objectiveTracker.objectivePointsEntries)
                }
                arRealm.delete(exisitingGoalTracker.objectiveTrackers)
                arRealm.add(goalTracker, update: true)
            } else {
                arRealm.add(goalTracker, update: true)
            }
        }
    }
    
    class func parse(goalTracker: GPDGoalTrackerOuts) -> ARGoalTracker {
        let arGoalTracker = ARGoalTracker()
        arGoalTracker.id = goalTracker.id
        arGoalTracker.completedObjectives = goalTracker.completedObjectives
        arGoalTracker.effectiveFrom = Parser.main.parseDateString(goalTracker.effectiveFrom, fallback: Date())
        arGoalTracker.effectiveTo = Parser.main.parseDateString(goalTracker.effectiveTo, fallback: Date())
        arGoalTracker.goalCode = goalTracker.goalCode
        arGoalTracker.goalKey = GoalRef(rawValue: goalTracker.goalKey) ?? .Unknown
        arGoalTracker.goalName = goalTracker.goalName
        arGoalTracker.goalTrackerStatusCode = goalTracker.goalTrackerStatusCode
        arGoalTracker.goalTrackerStatus = GoalTrackerStatusRef(rawValue: goalTracker.goalTrackerStatusKey) ?? .Unknown
        arGoalTracker.goalTrackerStatusName = goalTracker.goalTrackerStatusName
        arGoalTracker.monitorUntil = Parser.main.parseDateString(goalTracker.monitorUntil, fallback: Date())
        if let objectiveTrackers = goalTracker.objectiveTrackers {
            for objectiveTracker in objectiveTrackers {
                arGoalTracker.objectiveTrackers.append(ARObjectiveTracker.parseARObjectiveTracker(with: objectiveTracker))
            }
        }
        arGoalTracker.partyId = goalTracker.partyId
        arGoalTracker.percentageCompleted = goalTracker.percentageCompleted
        arGoalTracker.statusChangedOn = Parser.main.parseDateString(goalTracker.statusChangedOn, fallback: Date())
        arGoalTracker.totalObjectives = goalTracker.totalObjectives
        
        return arGoalTracker
    }
}

extension ARObjectiveTracker {
    public class func parseARObjectiveTracker(with objectiveTracker: GPDObjectiveTrackers) -> ARObjectiveTracker {
        let arObjectiveTracker = ARObjectiveTracker()
        
        arObjectiveTracker.effectiveFrom = Parser.main.parseDateString(objectiveTracker.effectiveFrom, fallback: Date())
        arObjectiveTracker.effectiveTo = Parser.main.parseDateString(objectiveTracker.effectiveTo, fallback: Date())
        arObjectiveTracker.eventCountAchieved.value = objectiveTracker.eventCountAchieved
        arObjectiveTracker.eventCountTarget.value = objectiveTracker.eventCountTarget
        arObjectiveTracker.eventOutcomeAchieved.value = objectiveTracker.eventOutcomeAchieved
        arObjectiveTracker.eventOutcomeTarget = objectiveTracker.eventOutcomeTarget
        arObjectiveTracker.monitorUntil = Parser.main.parseDateString(objectiveTracker.monitorUntil, fallback: Date())
        arObjectiveTracker.objectiveCode = objectiveTracker.objectiveCode
        arObjectiveTracker.objectiveKey = ObjectiveRef(rawValue: objectiveTracker.objectiveKey) ?? .Unknown
        arObjectiveTracker.objectiveName = objectiveTracker.objectiveName
        arObjectiveTracker.percentageCompleted.value = objectiveTracker.percentageCompleted
        arObjectiveTracker.pointsAchieved.value = objectiveTracker.pointsAchieved
        arObjectiveTracker.pointsTarget.value = objectiveTracker.pointsTarget
        arObjectiveTracker.statusChangedOn = Parser.main.parseDateString(objectiveTracker.statusChangedOn, fallback: Date())
        arObjectiveTracker.statusCode = objectiveTracker.statusCode
        arObjectiveTracker.status = ObjectiveTrackerStatusRef(rawValue: objectiveTracker.statusKey) ?? .Unknown
        arObjectiveTracker.statusName = objectiveTracker.statusName
        if let pointsEntries = objectiveTracker.objectivePointsEntries {
            for pointsEntry in pointsEntries {
                arObjectiveTracker.objectivePointsEntries.append(ARObjectivePointsEntry.parseARObjectivePointsEntry(with: pointsEntry))
            }
        }
        if let events = objectiveTracker.events {
            for event in events {
                arObjectiveTracker.objectiveEvents.append(ARObjectiveEvent.parseARObjectiveEvent(with: event))
            }
        }
        
        return arObjectiveTracker
    }
}

extension ARObjectiveEvent {
    public class func parseARObjectiveEvent(with event: GPDEvents) -> ARObjectiveEvent {
        let arObjectiveEvent = ARObjectiveEvent()
        
        arObjectiveEvent.dateLogged = Parser.main.parseDateString(event.dateLogged)
        arObjectiveEvent.eventDateTime = Parser.main.parseDateString(event.eventDateTime)
        arObjectiveEvent.eventSourceCode = event.eventSourceCode
        arObjectiveEvent.eventSourceKey = EventSourceRef(rawValue: event.eventSourceKey) ?? .Unknown
        arObjectiveEvent.eventSourceName = event.eventSourceName
        arObjectiveEvent.id.value = event.id
        arObjectiveEvent.partyId.value = event.partyId
        arObjectiveEvent.reportedBy.value = event.reportedBy
        arObjectiveEvent.typeCode = event.typeCode
        arObjectiveEvent.typeKey = EventTypeRef(rawValue: event.typeKey) ?? .Unknown
        arObjectiveEvent.typeName = event.typeName
        if let eventMetadatas = event.eventMetaDatas {
            for eventMetadata in eventMetadatas {
                arObjectiveEvent.objectiveEventMetaDatas.append(ARObjectiveEventMetaData.parseObjectivePointsEntryMetaData(with: eventMetadata))
            }
        }
        
        return arObjectiveEvent
    }
}

extension ARObjectiveEventMetaData {
    public class func parseObjectivePointsEntryMetaData(with eventMetadata: GPDEventMetaDatas) -> ARObjectiveEventMetaData {
        let arObjectiveEventMetaData = ARObjectiveEventMetaData()
        
        arObjectiveEventMetaData.typeCode = eventMetadata.typeCode
        arObjectiveEventMetaData.typeKey = EventMetaDataTypeRef(rawValue: eventMetadata.typeKey) ?? .Unknown
        arObjectiveEventMetaData.typeName = eventMetadata.typeName
        if let uom = eventMetadata.unitOfMeasure {
            let unitOfMeasure = Int(uom) ?? UnitOfMeasureRef.Unknown.rawValue
            arObjectiveEventMetaData.unitOfMeasureType = UnitOfMeasureRef(rawValue: unitOfMeasure) ?? .Unknown
        }
        arObjectiveEventMetaData.value = eventMetadata.value
        
        return arObjectiveEventMetaData
    }
}

extension ARObjectivePointsEntry {
    public class func parseARObjectivePointsEntry(with objectivePointsEntry: GPDObjectivePointsEntry) -> ARObjectivePointsEntry {
        let arObjectivePointsEntry = ARObjectivePointsEntry()
        
        arObjectivePointsEntry.categoryCode = objectivePointsEntry.categoryCode
        arObjectivePointsEntry.categoryKey = PointsEntryCategoryRef(rawValue: objectivePointsEntry.categoryKey) ?? .Unknown
        arObjectivePointsEntry.categoryName = objectivePointsEntry.categoryName
        arObjectivePointsEntry.earnedValue = objectivePointsEntry.earnedValue
        if let tenantID = AppSettings.getAppTenant(), tenantID == .SLI {
            arObjectivePointsEntry.effectiveDate = Parser.main.parsePointsHistoryEffectiveDate(objectivePointsEntry.effectiveDate, fallback: Date())
        } else {
            arObjectivePointsEntry.effectiveDate = Parser.main.parseDateString(objectivePointsEntry.effectiveDate, fallback: Date())
        }
        arObjectivePointsEntry.eventId = objectivePointsEntry.eventId
        arObjectivePointsEntry.id = objectivePointsEntry.id
        arObjectivePointsEntry.partyId.value = objectivePointsEntry.partyId
        arObjectivePointsEntry.pointsContributed = objectivePointsEntry.pointsContributed
        arObjectivePointsEntry.potentialValue = objectivePointsEntry.potentialValue
        arObjectivePointsEntry.prelimitValue = objectivePointsEntry.prelimitValue
        arObjectivePointsEntry.statusChangeDate = Parser.main.parseDateString(objectivePointsEntry.statusChangeDate, fallback: Date())
        arObjectivePointsEntry.statusTypeCode = objectivePointsEntry.statusTypeCode
        arObjectivePointsEntry.statusTypeKey = PointsEntryStatusTypeRef(rawValue: objectivePointsEntry.statusTypeKey) ?? .Unknown
        arObjectivePointsEntry.statusTypeName = objectivePointsEntry.statusTypeName
        arObjectivePointsEntry.systemAwareOn = objectivePointsEntry.systemAwareOn
        arObjectivePointsEntry.typeCode = objectivePointsEntry.typeCode
        arObjectivePointsEntry.typeKey = PointsEntryTypeRef(rawValue: objectivePointsEntry.typeKey) ?? .Unknown
        arObjectivePointsEntry.typeName = objectivePointsEntry.typeName
        if let pointsEntryMetadatas = objectivePointsEntry.pointsEntryMetadatas {
            for pointsEntryMetadata in pointsEntryMetadatas {
                arObjectivePointsEntry.objectivePointsEntryMetaDatas.append(ARObjectivePointsEntryMetaData.parseObjectivePointsEntryMetaData(with: pointsEntryMetadata))
            }
        }
        if let pointsEntryReasons = objectivePointsEntry.reason {
            for reason in pointsEntryReasons {
                arObjectivePointsEntry.objectivePointsEntryReasons.append(ARObjectivePointsEntryReason.parseObjectivePointsEntryReason(with: reason))
            }
        }
        
        return arObjectivePointsEntry
    }
}

extension ARObjectivePointsEntryMetaData {
    public class func parseObjectivePointsEntryMetaData(with pointsEntryMetadata: GPDPointsEntryMetadatas) -> ARObjectivePointsEntryMetaData {
        let arObjectivePointsEntryMetaData = ARObjectivePointsEntryMetaData()
        
        arObjectivePointsEntryMetaData.typeCode = pointsEntryMetadata.typeCode
        arObjectivePointsEntryMetaData.typeKey = EventMetaDataTypeRef(rawValue: pointsEntryMetadata.typeKey) ?? .Unknown
        arObjectivePointsEntryMetaData.typeName = pointsEntryMetadata.typeName
        if let uom = pointsEntryMetadata.unitOfMeasure {
            let unitOfMeasure = Int(uom) ?? UnitOfMeasureRef.Unknown.rawValue
            arObjectivePointsEntryMetaData.unitOfMeasureType = UnitOfMeasureRef(rawValue: unitOfMeasure) ?? .Unknown
        }
        arObjectivePointsEntryMetaData.value = pointsEntryMetadata.value
        
        return arObjectivePointsEntryMetaData
    }
}

extension ARObjectivePointsEntryReason {
    public class func parseObjectivePointsEntryReason(with pointsEntryReason: GPDReason) -> ARObjectivePointsEntryReason {
        let arObjectivePointsEntryReason = ARObjectivePointsEntryReason()
        
        arObjectivePointsEntryReason.reasonCode = pointsEntryReason.reasonCode
        arObjectivePointsEntryReason.reasonKey = ReasonRef(rawValue: pointsEntryReason.reasonKey) ?? .Unknown
        arObjectivePointsEntryReason.reasonName = pointsEntryReason.reasonName
        arObjectivePointsEntryReason.categoryCode = pointsEntryReason.categoryCode
        arObjectivePointsEntryReason.categoryKey = ReasonCategoryRef(rawValue: pointsEntryReason.categoryKey) ?? .Unknown
        arObjectivePointsEntryReason.categoryName = pointsEntryReason.categoryName
        
        return arObjectivePointsEntryReason
    }
}

public extension Realm {
    public func deleteOldARGoalProgressAndDetailsData() {
        try! self.write {
            let oldARObjectiveEvents = self.allARObjectiveEvents()
            self.delete(oldARObjectiveEvents)
            let oldARObjectiveEventMetaDatas = self.allARObjectiveEventMetaDatas()
            self.delete(oldARObjectiveEventMetaDatas)
            let oldARObjectivePointsEntries = self.allARObjectivePointsEntries()
            self.delete(oldARObjectivePointsEntries)
            let oldARObjectivePointsEntryMetaDatas = self.allARObjectivePointsEntryMetaDatas()
            self.delete(oldARObjectivePointsEntryMetaDatas)
            let oldARObjectivePointsEntryReasons = self.allARObjectivePointsEntryReasons()
            self.delete(oldARObjectivePointsEntryReasons)
        }
    }
}
