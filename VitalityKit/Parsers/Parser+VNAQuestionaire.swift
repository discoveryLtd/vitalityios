//
//  Parser+VNAQuestionaire.swift
//  VitalityActive
//
//  Created by Von Kervin R. Tuico on 09/02/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import UIKit
import RealmSwift
import VIAUtilities

extension Parser {
    public struct VNA {
        
        public static func parseQuestionnaires(response: QuestionnaireProgressAndPointsTracking, completion: @escaping (_: Error?) -> Void) {
            Parser.main.parse {
                VNAQuestionnaireProgressAndPointsTracking.persistQuestionnaireProgressAndPointsTracking(response: response)
                DispatchQueue.main.async {
                    completion(nil)
                }
            }
        }
    }
}

extension DataProvider {
    
    /* 
     * TODO: Please document the purpose of this function.
     */
    public class func deleteAllVNARealmDataExceptUnsubmittedSavedAnswers() {
        let realm = DataProvider.newVNARealm()
        let inMemoryRealm = DataProvider.newInMemoryRealm()
        
        /* 
         * Copy all unsubmitted VNA results to in-memory realm
         */
        let capturedResults = realm.allUnsubmittedVNACapturedResults()
        /*
         * Don't force the writing in realm (try!), this will crash the app on runtime if it fails.
         * Instead make use of `try?` for fail safe.
         */
        try? inMemoryRealm.write {
            for capturedResult in capturedResults {
                inMemoryRealm.create(VNACapturedResult.self, value: capturedResult)
            }
        }
        
        /* 
         * Delete all data in VNA realm
         */
        realm.reset()
        
        /* 
         * Copy all unsubmitted VNA results from in-memory realm back into to vnaRealm
         */
        let inMemoryCapturedResults = inMemoryRealm.allUnsubmittedVNACapturedResults()
        /*
         * Don't force the writing in realm (try!), this will crash the app on runtime if it fails.
         * Instead make use of `try?` for fail safe.
         */
        try? realm.write {
            for capturedResult in inMemoryCapturedResults {
                realm.create(VNACapturedResult.self, value: capturedResult)
            }
        }
    }
    
}

extension VNAQuestionnaireProgressAndPointsTracking {
    
    public class func persistQuestionnaireProgressAndPointsTracking(response: QuestionnaireProgressAndPointsTracking) {
        let realm = DataProvider.newVNARealm()
        
        
        /* FIXME: What is the purpose of this line of code?
         * DataProvider.deleteAllVNARealmDataExceptUnsubmittedSavedAnswers()
         *
         * This causes the crash error after submitting an assessment and when about to return to the landing page
         *
         * VA-32009
         * Sumitomo [Regression ] [iOS] [VNA] : App is crashing when user completing one section and navigating to landing page for VNA
         */
        
        //        DataProvider.deleteAllVNARealmDataExceptUnsubmittedSavedAnswers()
        removeExistingQuestionnaires(realm: realm)
        
        createProgressAndPointsTracking(realm: realm, response: response)
        
        updateQuestionnaire(realm: realm)
    }
    
    /* 
     * OJ: Delete all existing questionnaires to avoid duplicate entries on the landing page.
     * FIXME: Interim solution to the crashing issue while invoking the DataProvider.deleteAllVNARealmDataExceptUnsubmittedSavedAnswers()
     */
    private class func removeExistingQuestionnaires(realm: Realm){
        let sortedQuestionnaires = realm.allSortedVNAQuestionnaires()
        /*
         * Don't force the writing in realm (try!), this will crash the app on runtime if it fails.
         * Instead make use of `try?` for fail safe.
         */
        try? realm.write{
            for questionnaire in sortedQuestionnaires{
                realm.delete(questionnaire)
            }
        }
        
        let allVNAQuestionnaireProgressAndPointsTracking = realm.allVNAQuestionnaireProgressAndPointsTracking()
        /*
         * Don't force the writing in realm (try!), this will crash the app on runtime if it fails.
         * Instead make use of `try?` for fail safe.
         */
        try? realm.write{
            for questionnaire in allVNAQuestionnaireProgressAndPointsTracking{
                realm.delete(questionnaire)
            }
        }
    }
    
    /*
     * OJ: Persist progress and points tracking data from service response.
     */
    private class func createProgressAndPointsTracking(realm: Realm, response: QuestionnaireProgressAndPointsTracking){
        let item = VNAQuestionnaireProgressAndPointsTracking()
        item.totalEarnedPoints.value = response.totalEarnedPoints
        item.questionnaireSetTypeCode = response.questionnaireSetTypeCode
        item.totalPotentialPoints.value = response.totalPotentialPoints
        item.totalQuestionnaireCompleted.value = response.totalQuestionnaireCompleted
        item.questionnaireSetCompleted = response.questionnaireSetCompleted
        item.questionnaireSetTypeName = response.questionnaireSetTypeName
        item.totalEarnedPoints.value = response.totalEarnedPoints
        item.questionnaireSetType = QuestionnaireSetRef(rawValue: response.questionnaireSetTypeKey) ?? .Unknown
        item.totalQuestionnaires = response.totalQuestionnaires
        item.questionnaireSetText = response.setText ?? ""
        item.questionnaireSetTextDescription = response.setTextDescription ?? ""
        item.questionnaireSetTextNote = response.setTextNote ?? ""
        
        response.questionnaire.flatMap({ $0 })?.forEach({ questionnaire in
            let vnaQuestionnaire = VNAQuestionnaire.parseVNAQuestionare(responseQuestionnare: questionnaire)
            item.questionnaire.append(vnaQuestionnaire)
        })
        
        /*
         * Don't force the writing in realm (try!), this will crash the app on runtime if it fails.
         * Instead make use of `try?` for fail safe.
         */
        try? realm.write {
            // create VNA questionnaire set and cache
            let vnaCache = VNACache()
            realm.add(vnaCache)
            realm.add(item)
        }
    }
    
    /*
     * OJ: Update VNA child questions and reevaluate all the answers to make sure our sections 
     * and questions have the correct visibility
     */
    private class func updateQuestionnaire(realm: Realm){
        /*
         * Always disable your debug logs to avoid cluttered logs in the console.
         * ge20180115: Update VNA child questions
         * print("ge-->VNA: Update questions to indicate they are child q")
         */
        let associations = realm.objects(VNAQuestionAssociations.self)
        for assoc in associations {
            /* FIXME: It is exhaustive to use filter if you will only need the first occurence.
             * Make use of for-loop then break once you found what you are looking for.
             * Create a func to filter what you need.
             */
            let question = realm.objects(VNAQuestion.self).filter("typeKey == %@",assoc.childQuestionKey).first
            /*
             * Don't force the writing in realm (try!), this will crash the app on runtime if it fails.
             * Instead make use of `try?` for fail safe.
             */
            try? realm.write {
                question?.isChild = true
            }
        }
        
        /*
         * reevaluate all the answers to make sure our sections and questions have the correct visibility
         */
        VNAQuestionnaire.reevaluateVisibilityTagsForAllCapturedResults(in: realm)
    }
}

extension VNAQuestionnaire {
    public class func parseVNAQuestionare(responseQuestionnare: QPPTQuestionnaire) -> VNAQuestionnaire {
        let vnaQuestionnare = VNAQuestionnaire()
        if let sections = responseQuestionnare.questionnaireSections {
            for section in sections {
                let vnaQuestionnaireSection = VNAQuestionnaireSection.parseVNAQuestionnaireSection(section: section, questionnaire: responseQuestionnare)
                vnaQuestionnare.questionnaireSections.append(vnaQuestionnaireSection)
            }
        }
        vnaQuestionnare.sortOrderIndex.value = responseQuestionnare.sortOrderIndex
        vnaQuestionnare.typeKey = responseQuestionnare.typeKey
        if let dateString = responseQuestionnare.completedOn {
            vnaQuestionnare.completedOn = Parser.main.yearMonthDayFormatter.date(from: dateString)
        }
        vnaQuestionnare.typeName = responseQuestionnare.typeName
        vnaQuestionnare.completionFlag = responseQuestionnare.completionFlag
        vnaQuestionnare.typeCode = responseQuestionnare.typeCode
        
        vnaQuestionnare.text = responseQuestionnare.text ?? ""
        vnaQuestionnare.textDescription = responseQuestionnare.textDescription ?? ""
        vnaQuestionnare.textNote = responseQuestionnare.textNote ?? ""
        
        if vnaQuestionnare.completionFlag == true {
            vnaQuestionnare.completionState = .Complete
        } else if vnaQuestionnare.completionFlag == false {
            vnaQuestionnare.completionState = .NotStarted
        } else {
            vnaQuestionnare.completionState = .Unknown
        }
        
        return vnaQuestionnare
    }
}

extension VNAQuestionnaireSection {
    public class func parseVNAQuestionnaireSection(section: QPPTQuestionnaireSections, questionnaire: QPPTQuestionnaire) -> VNAQuestionnaireSection {
        let vnaQuestionnaireSection = VNAQuestionnaireSection()
        vnaQuestionnaireSection.typeKey = section.typeKey
        vnaQuestionnaireSection.typeName = section.typeName
        vnaQuestionnaireSection.visibilityTag = section.visibilityTagName
        if let questions = section.questions {
            for question in questions {
                let sectionQuestion = VNAQuestion.parseVNAQuestion(question, for: section, in: questionnaire)
                vnaQuestionnaireSection.questions.append(sectionQuestion)
                
                /*
                 * Always disable your debug logs to avoid cluttered logs in the console.
                 * ge20180113: Add question associations to realm
                 * print("ge-->VNA: Add question associations to realm")
                 */
                if let associations = question.questionAssociations {
                    for association in associations {
                        let childQ = VNAQuestionAssociations()
                        childQ.childQuestionKey = association.childQuestionKey
                        childQ.sortIndex = association.sortIndex
                        let realm = DataProvider.newVNARealm()
                        /*
                         * Don't force the writing in realm (try!), this will crash the app on runtime if it fails.
                         * Instead make use of `try?` for fail safe.
                         */
                        try? realm.write {
                            realm.add(childQ)
                        }
                    }
                }
                
            }
        }
        vnaQuestionnaireSection.isVisible = section.isVisible
        vnaQuestionnaireSection.typeCode = section.typeCode
        vnaQuestionnaireSection.sortOrderIndex.value = section.sortOrderIndex
        
        vnaQuestionnaireSection.text = section.text ?? ""
        vnaQuestionnaireSection.textDescription = section.textDescription ?? ""
        vnaQuestionnaireSection.textNote = section.textNote ?? ""
        
        return vnaQuestionnaireSection
    }
}

extension VNAQuestion {
    
    private static func hasValidUnitOfMeasureCombination(question: QPPTQuestions, populationValue: QPPTPopulationValues) -> Bool {
        guard let populationValueUOM = populationValue.unitOfMeasure, let questionUOMs = question.unitOfMeasures else {
            return true // No units of measure for either population value or question, passing through
        }
        
        guard questionUOMs.filter({ $0.value == populationValueUOM }).count > 0 else {
            debugPrint("Skipping prepopulation value for VHR, cannot match with UOM sent back for question")
            debugPrint("questionUOMs: \(questionUOMs) - populationValueUOM: \(populationValueUOM)")
            return false
        }
        
        return true
    }
    
    private class func createCapturedResult(for question: QPPTQuestions, populationValue: QPPTPopulationValues, section: QPPTQuestionnaireSections, questionnaire: QPPTQuestionnaire) {
        
        // check that the prepopulated value matches with any of the units of measure
        // that is being sent back with the question, otherwise skip over it, as
        // the frontend won't know how to convert between the 2.
        guard VNAQuestion.hasValidUnitOfMeasureCombination(question: question, populationValue: populationValue) else {
            debugPrint("Skipping prepopulation value for VNA, cannot match with UOM sent back for question")
            return
        }
        
        let realm = DataProvider.newVNARealm()
        if question.questionTypeCode == "MultipleSelect" {
            if let answerValue = populationValue.value, realm.vnaCapturedResultExists(for: question.typeKey, answer: answerValue) {
                // if we hit this, it means that the user has captured something locally
                // and the parser is trying to prepopulate over the user's captured value.
                // we can't allow this, so we skip creating this prepopulated value / captured result
                return
            }
        } else {
            if realm.vnaCapturedResultExists(for: question.typeKey) {
                // if we hit this, it means that the user has captured something locally
                // and the parser is trying to prepopulate over the user's captured value.
                // we can't allow this, so we skip creating this prepopulated value / captured result
                return
            }
        }
        
        /*
         * Don't force the writing in realm (try!), this will crash the app on runtime if it fails.
         * Instead make use of `try?` for fail safe.
         */
        try? realm.write {
            let capturedResult = VNACapturedResult()
            capturedResult.dateCaptured = Parser.main.parseDateString(populationValue.eventDate) ?? Date()
            capturedResult.valid = true
            capturedResult.questionTypeKey = question.typeKey
            capturedResult.type = QuestionTypeRef(rawValue: question.questionTypeKey) ?? .Unknown
            capturedResult.questionnaireSectionTypeKey = section.typeKey
            capturedResult.questionnaireTypeKey = questionnaire.typeKey
            capturedResult.prepopulationEventKey.value = populationValue.eventKey
            capturedResult.prepopulationEventName = populationValue.eventName
            capturedResult.prepopulationEventCode = populationValue.eventCode
            
            if let populationValueUOMRawValue = Int(populationValue.unitOfMeasure ?? ""), let populationValueUOM = UnitOfMeasureRef(rawValue: populationValueUOMRawValue) {
                capturedResult.unitOfMeasure = populationValueUOM
            } else {
                //                debugPrint("Prepopulation value doesn't have a UOM, this will result in oddities on the frontend")
            }
            
            // Henri Langenhoven
            // [2 hours ago]
            // @vanhalen Werner mentioned on Friday that every prepop value will be coming back in the Value field from now on
            // https://glucode.slack.com/archives/C5A563YJZ/p1499075252442704?thread_ts=1499068899.224246&cid=C5A563YJZ
            capturedResult.answer = populationValue.value
            realm.add(capturedResult)
        }
    }
    
    public class func parseVNAQuestion(_ question: QPPTQuestions, for section: QPPTQuestionnaireSections, in questionnaire: QPPTQuestionnaire) -> VNAQuestion {
        let vnaQuestion = VNAQuestion()
        vnaQuestion.questionDecorator = VNAQuestionDecorator()
        if let decorator = question.questionDecorator {
            vnaQuestion.questionDecorator = VNAQuestionDecorator.parseVNAQuestionDecorator(decorator: decorator)
        }
        vnaQuestion.typeKey = question.typeKey
        if let populationValues = question.populationValues {
            for populationValue in populationValues {
                VNAQuestion.createCapturedResult(for: question, populationValue: populationValue, section: section, questionnaire: questionnaire)
            }
        }
        if let unitOfMeasures = question.unitOfMeasures {
            for unitOfMeasure in unitOfMeasures {
                let vnaUnitOfMeasure = VNAUnitOfMeasure.parseVNAUnitOfMeasures(unitOfMeasure: unitOfMeasure)
                vnaQuestion.unitsOfMeasure.append(vnaUnitOfMeasure)
            }
        }
        vnaQuestion.typeName = question.typeName
        vnaQuestion.visibilityTag = question.visibilityTagName ?? ""
        vnaQuestion.isVisible = vnaQuestion.visibilityTag.isEmpty
        // regular expression
        vnaQuestion.format = question.format
        vnaQuestion.length.value = question.length ?? 0
        if let validValues = question.validValues {
            for validValue in validValues {
                let vnaValidValue = VNAValidValue.parseVNAValidValue(value: validValue)
                vnaQuestion.validValues.append(vnaValidValue)
            }
        }
        
        vnaQuestion.text = question.text ?? ""
        vnaQuestion.textDescription = question.textDescription ?? ""
        vnaQuestion.textNote = question.textNote ?? ""
        vnaQuestion.typeCode = question.typeCode ?? ""
        // This indicates the type of question. Depicts what format the answer would be requested in the questionnaire e.g. option list
        vnaQuestion.questionTypeName = question.questionTypeName
        vnaQuestion.questionTypeCode = question.questionTypeCode
        vnaQuestion.questionType = QuestionTypeRef(rawValue: question.questionTypeKey) ?? .Unknown
        vnaQuestion.sortOrderIndex.value = question.sortOrderIndex
        
        return vnaQuestion
    }
}

extension VNAQuestionDecorator {
    public class func parseVNAQuestionDecorator(decorator: QPPTQuestionDecorator) -> VNAQuestionDecorator {
        let vnaQuestionDecorator = VNAQuestionDecorator()
        vnaQuestionDecorator.channelType = QuestionnaireChannelRef(rawValue: decorator.channelTypeKey) ?? .Unknown
        vnaQuestionDecorator.type = QuestionDecoratorRef(rawValue: decorator.typeKey) ?? .Unknown
        vnaQuestionDecorator.channelTypeCode = decorator.channelTypeCode
        vnaQuestionDecorator.typeName = decorator.typeName
        vnaQuestionDecorator.channelTypeName = decorator.channelTypeName
        vnaQuestionDecorator.typeCode = decorator.typeCode
        return vnaQuestionDecorator
    }
}

extension VNAUnitOfMeasure {
    public class func parseVNAUnitOfMeasures(unitOfMeasure: QPPTUnitOfMeasures) -> VNAUnitOfMeasure {
        let vnaUnitOfMeasure = VNAUnitOfMeasure()
        vnaUnitOfMeasure.value = unitOfMeasure.value
        return vnaUnitOfMeasure
    }
}

extension VNAValidValue {
    public class func parseVNAValidValue(value: QPPTValidValues) -> VNAValidValue {
        let vnaValidValue = VNAValidValue()
        
        if let vnaUnitOfMeasure = value.unitOfMeasure {
            let vnaUnitOfMeasureRawValue = Int(vnaUnitOfMeasure.value) ?? UnitOfMeasureRef.Unknown.rawValue
            vnaValidValue.unitOfMeasure = UnitOfMeasureRef(rawValue: vnaUnitOfMeasureRawValue) ?? UnitOfMeasureRef.Unknown
        }
        
        vnaValidValue.name = value.name
        vnaValidValue.value = value.value
        vnaValidValue.valueDescription = value.description ?? ""
        vnaValidValue.valueNote = value.note ?? ""
        if let validValueType = value.validValueTypes?.first {
            vnaValidValue.type = QuestionValidValueTypeRef(rawValue: validValueType.typeKey) ?? .Unknown
            vnaValidValue.typeName = validValueType.typeName
            vnaValidValue.typeCode = validValueType.typeCode
        }
        
        return vnaValidValue
    }
}

