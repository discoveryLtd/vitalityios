//
//  Parser+OFEEventsAndPoints.swift
//  VitalityActive
//
//  Created by Harry C. Lingad on 4/24/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import RealmSwift
import VIAUtilities

extension Parser {
    public struct EventsAndPoints {
        
        public static func parseEventsPointsHistory(response: OFEGetEventsPointsResponse, completion: @escaping (_ result: Any?, _ error: Error?, _ useCustomError: Bool?) -> Void) {
            Parser.main.parse {
                let eventsAndPointsResult = OFEEventsAndPoints.persistEventsAndPoints(response: response)
                DispatchQueue.main.async {
                    completion(eventsAndPointsResult, nil, nil)
                }
            }
        }
    }
}

extension OFEEventsAndPoints {
    
    public static func persistEventsAndPoints(response: OFEGetEventsPointsResponse) -> OFEEventsAndPoints? {
        
        let realm = DataProvider.newOFERealm()
        
        let eventsPoints = response.eventsPoints
        debugPrint("Parsing \(String(describing: eventsPoints?.count)) number of points accounts")
        
        try! realm.write {
            // Clear all old data before adding new pointsEntry data to prevent duplicates
            
            // let allEventMetadata = realm.allEventMetadata()
            // realm.delete(allEventMetadata)
            //
            // let allEventsAndPoints = realm.allEventsAndPoints()
            // realm.delete(allEventsAndPoints)
            
            for eventsPoint in eventsPoints ?? [EventsPoints]() {
                let eventsPointModel = OFEEventsAndPoints()
                
                eventsPointModel.id = eventsPoint.id
                eventsPointModel.activityDate = eventsPoint.activityDate!
                eventsPointModel.dateLogged = eventsPoint.dateLogged!
                eventsPointModel.typeName = eventsPoint.typeName!
                eventsPointModel.typeCode = eventsPoint.typeCode!
                eventsPointModel.typeKey = eventsPoint.typeKey
                
                for eventMetadata in eventsPoint.eventMetadatas ?? [EventMetadata](){
                    
                    let eventMetadataModel = OFEEventMetadata()
                    
                    eventMetadataModel.typeCode = eventMetadata.typeCode!
                    eventMetadataModel.typeKey = eventMetadata.typeKey
                    eventMetadataModel.typeName = eventMetadata.typeName!
                    eventMetadataModel.unitOfMeasure = eventMetadata.unitOfMeasure!
                    eventMetadataModel.value = eventMetadata.value!
                    
                    eventsPointModel.eventMetadatas.append(eventMetadataModel)
                    
                }
                
                realm.create(OFEEventsAndPoints.self, value: eventsPointModel, update: true)
                //realm.add(eventsPointModel)
            }
            
        }
        
        //TODO:- Do member number lookup and return the relevant account based on member number (member number still needs to be persisted along with account)
        return realm.objects(OFEEventsAndPoints.self).first
    }
}

