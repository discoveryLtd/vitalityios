import Foundation
import RealmSwift
import VIAUtilities
import SwiftDate

extension Parser {
    public struct HomeScreen {
        
        public static func parseGetHomeScreen(response: GetHomeScreenResponse, completion: @escaping (_ error: Error?) -> Void) {
            Parser.main.parse {
                HomeScreenData.parseAndPersistGetHomeScreenData(response: response)
                DispatchQueue.main.async {
                    completion(nil)
                }
            }
        }
    }
}

extension HomeScreenData {
    
    public static func parseAndPersistGetHomeScreenData(response: GetHomeScreenResponse) {
        let realm = DataProvider.newRealm()
        
        try! realm.write {
            
            realm.deleteOldHomeScreenData()
            
            let homeScreenData = HomeScreenData()
            realm.add(homeScreenData)
            
            if let homeVitalityStatus = response.currentStatus {
                homeScreenData.currentVitalityStatus = HomeVitalityStatus.parseHomeVitalityStatus(with: homeVitalityStatus)
            }
            if let homeDaysLeftInMembershipPeriod = response.daysLeftInMembershipPeriod {
                homeScreenData.daysLeftInMembershipPeriod = HomeDaysLeftInMembershipPeriod.parseHomeDaysLeftInMembershipPeriod(with: homeDaysLeftInMembershipPeriod)
            }
            if let homePointsAccount = response.pointsToNextStatuses {
                for pointsAccount in homePointsAccount {
                    homeScreenData.pointsToNextStatuses.append(HomePointsAccount.parseHomePointsAccount(with: pointsAccount))
                }
            }
            if let homeSections = response.sections {
                homeScreenData.sections.removeAll()
                for section in homeSections {
                    if let section = HomeSection.parseHomeSection(with: section, realm: realm) {
                        if section.cards.count > 0 || section.rewardCards.count > 0 {
                            homeScreenData.sections.append(section)
                        } else {
                            realm.delete(section)
                        }
                    }
                }
            }
        }
    }
    
}

extension HomeVitalityStatus {
    public class func parseHomeVitalityStatus(with currentVitalityStatus: GHSCurrentStatus) -> HomeVitalityStatus {
        let homeVitalityStatus = HomeVitalityStatus()
        
        homeVitalityStatus.highestVitalityStatusCode = currentVitalityStatus.highestVitalityStatusCode
        homeVitalityStatus.highestVitalityStatusKey = StatusTypeRef(rawValue: currentVitalityStatus.highestVitalityStatusKey) ?? .Unknown
        homeVitalityStatus.highestVitalityStatusName = currentVitalityStatus.highestVitalityStatusName
        homeVitalityStatus.lowestVitalityStatusCode = currentVitalityStatus.lowestVitalityStatusCode
        homeVitalityStatus.lowestVitalityStatusKey = StatusTypeRef(rawValue: currentVitalityStatus.lowestVitalityStatusKey) ?? .Unknown
        homeVitalityStatus.lowestVitalityStatusName = currentVitalityStatus.lowestVitalityStatusName
        homeVitalityStatus.nextVitalityStatusCode = currentVitalityStatus.nextVitalityStatusCode
        if let nextVitalityStatusKey = currentVitalityStatus.nextVitalityStatusKey {
            homeVitalityStatus.nextVitalityStatusKey = StatusTypeRef(rawValue: nextVitalityStatusKey) ?? .Unknown
        } else {
            homeVitalityStatus.nextVitalityStatusKey = .Unknown
        }
        homeVitalityStatus.nextVitalityStatusName = currentVitalityStatus.nextVitalityStatusName
        homeVitalityStatus.overallVitalityStatusCode = currentVitalityStatus.overallVitalityStatusCode
        homeVitalityStatus.overallVitalityStatusKey = StatusTypeRef(rawValue: currentVitalityStatus.overallVitalityStatusKey) ?? .Unknown
        homeVitalityStatus.overallVitalityStatusName = currentVitalityStatus.overallVitalityStatusName
        homeVitalityStatus.pointsStatusCode = currentVitalityStatus.pointsStatusCode
        homeVitalityStatus.pointsStatusKey = StatusTypeRef(rawValue: currentVitalityStatus.pointsStatusKey) ?? .Unknown
        homeVitalityStatus.pointsStatusName = currentVitalityStatus.pointsStatusName
        homeVitalityStatus.pointsToMaintainStatus.value = currentVitalityStatus.pointsToMaintainStatus
        homeVitalityStatus.totalPoints.value = currentVitalityStatus.totalPoints
        homeVitalityStatus.carryOverVitalityStatusName = currentVitalityStatus.carryOverStatusName
        homeVitalityStatus.carryOverVitalityStatusCode = currentVitalityStatus.carryOverStatusCode
        if let carryOverVitalityStatusKey = currentVitalityStatus.carryOverStatusKey {
            homeVitalityStatus.carryOverVitalityStatusKey = StatusTypeRef(rawValue: carryOverVitalityStatusKey) ?? .Unknown
        } else {
            homeVitalityStatus.carryOverVitalityStatusKey = .Unknown
        }
        return homeVitalityStatus
    }
}

extension HomeDaysLeftInMembershipPeriod {
    public class func parseHomeDaysLeftInMembershipPeriod(with daysLeftInPeriod: GHSDaysLeftInMembershipPeriod) -> HomeDaysLeftInMembershipPeriod {
        let homeDaysLeftInMembershipPeriod = HomeDaysLeftInMembershipPeriod()
        
        homeDaysLeftInMembershipPeriod.daysRemaining = daysLeftInPeriod.daysRemaining
        
        return homeDaysLeftInMembershipPeriod
    }
}

extension HomePointsAccount {
    public class func parseHomePointsAccount(with pointsToNextStatus: GHSPointsToNextStatus) -> HomePointsAccount {
        let homePointsAccount = HomePointsAccount()
        
        homePointsAccount.pointsNeeded = pointsToNextStatus.pointsNeeded
        homePointsAccount.sortOrder = pointsToNextStatus.sortOrder
        homePointsAccount.statusCode = pointsToNextStatus.statusCode
        homePointsAccount.statusKey = StatusTypeRef(rawValue: pointsToNextStatus.statusKey) ?? .Unknown
        homePointsAccount.statusName = pointsToNextStatus.statusName
        homePointsAccount.statusPoints = pointsToNextStatus.statusPoints
        
        return homePointsAccount
    }
}

extension HomeSection {
    public class func parseHomeSection(with section: GHSSections, realm: Realm) -> HomeSection? {
        guard let sectionType = SectionTypeRef(rawValue: section.typeKey) else { return nil }
        
        let homeSection = realm.homeSection(by: sectionType) ?? HomeSection()
        var cardsToBeDeleted = Array(homeSection.cards.map({ $0.type }))
		var rewardCardsToBeDeleted = Array(homeSection.rewardCards.map({ $0.awardedRewardId }))
        if homeSection.type == .Unknown {
            homeSection.type = sectionType
        }
        homeSection.priority = section.priorityFlag
        homeSection.typeCode = section.typeCode
        homeSection.typeName = section.typeName
        homeSection.cards.removeAll()
		homeSection.rewardCards.removeAll()
        if var cards = section.cards {
            
            /* Hide Cards */
            cards = hideCards(for: section)!
            
            /* Generate and append cards for Alpha and Beta Development */
            cards.append(contentsOf: generateAlphaBetaCards(for: section))
            
            if let tenantID = AppSettings.getAppTenant(){
                if tenantID == .IGI || tenantID == .EC || tenantID == .ASR {
                    if section.typeKey == 3 {
                        let newItem = GHSCards(amountCompleted: 0, cardItems: nil, cardMetadatas: nil, priority: 0, statusTypeCode: "", statusTypeKey: -1, statusTypeName: "", total: 0, typeCode: "PolicyCashback", typeKey: 21, typeName: "Policy Cashback", unitTypeCode: "Points", unitTypeKey: 1, unitTypeName: "Vitality", validFrom: "2017-11-30", validTo: "9999-12-31")
                        cards.append(newItem)
                    }
                }
            }
            
            /** This is to populate nuffield services card when not returned by BE*/
            /**
            let nuffieldSearch = cards.filter { $0.typeKey == 24 }
            let nuffieldExist = (nuffieldSearch.count > 0)
            if let tenantID = Bundle.main.object(forInfoDictionaryKey: "VATenantId") as? NSNumber {
                if(tenantID == 26 && section.typeKey == 2 && !nuffieldExist){
                    cards.append(getNuffieldHealthServices())
                }
            }
             */
            
            for card in cards  {
				if let rewardCard = RewardHomeCard.parseRewardHomeCard(with: card, realm: realm) {

					if isCompletedRewardCard(rewardCard:rewardCard) {
//						debugPrint("Reward card expired or already Done")
					} else if !homeSection.rewardCards.contains(rewardCard) {
						homeSection.rewardCards.append(rewardCard)
						rewardCardsToBeDeleted.remove(object: rewardCard.awardedRewardId)
					} else {
//						debugPrint("Reward card already in list")
					}
		
				} else if let homeCard = HomeCard.parseHomeCard(with: card, realm: realm) {
                    if !homeSection.cards.contains(homeCard) {
                        homeSection.cards.append(homeCard)
                        cardsToBeDeleted.remove(object: homeCard.type)
                    } else {
//                        debugPrint("Card already in list")
                    }
                }
            }
        }
        
        // delete old cards
        cardsToBeDeleted.forEach({ type in
            if let card = realm.homeCard(by: type) {
                realm.delete(card)
            }
        })
		
		rewardCardsToBeDeleted.forEach({ awardedRewardId in
			if let card = realm.rewardHomeCard(by: awardedRewardId) {
				realm.delete(card)
			}
		})
		
        realm.create(HomeSection.self, value: homeSection, update: true)
        let realmHomeSection = realm.object(ofType: HomeSection.self, forPrimaryKey: sectionType.rawValue)
        return realmHomeSection
    }
	
	private class func isCompletedRewardCard(rewardCard: RewardHomeCard) -> Bool {
		
		if rewardCard.type == CardTypeRef.Vouchers || rewardCard.type == CardTypeRef.RewardSelection {
			return rewardCard.validTo.isInPast || rewardCard.status == CardStatusTypeRef.Done
		}
		
		return false
	}
	
	private class func isCompletedRewardCard(card: HomeCard) -> Bool {

		if card.type == CardTypeRef.Rewards {
			if card.cardItems
				.filter({ !$0.validTo.isInPast })
				.filter({ $0.statusType == CardItemStatusTypeRef.Available })
				.count > 0 {
				
				return false
			} else {
				return true
			}
		}
		
		return false
	}
}

extension HomeSection{
    
    fileprivate class func generateAlphaBetaCards(for section: GHSSections) -> [GHSCards]{
        var cards = [GHSCards]()
        
        if let tenantID = AppSettings.getAppTenant(){
            
            /* Show in Improve Your Health section */
            if section.typeKey == 2 {
                
                /* For Debug build only. Not visible in Release build. */
                #if DEBUG
                    /* No restriction. Will be shown to all markets in debug build. */
                    cards.append(getOrganisedFitnessEventsCard())
                    //cards.append(getNuffieldHealthServices())
                #endif
                
                /* Tenant 2: CA */
                if tenantID == .CA{
                    
                }
            }
            
            /* Show in Get Rewarded section */
            if section.typeKey == 3 {
                
                /* Tenant 26: UKE */
                if tenantID == .UKE {
                    /* For Debug build only. Not visible in Release build. */
                    #if DEBUG
                        //cards.append(getEmployerStatusCard())
                    #endif
                }

                /* Tenant 2: CA */
                if tenantID == .CA {
                    cards.append(getPolicyCashbackCard())
                }
                
                /* Tenant 25: SLI */
                if tenantID == .SLI {
                    cards.append(getPolicyCashbackCard())
                }
            }
        }
        
        return cards
    }
    
    fileprivate class func hideCards(for section: GHSSections) -> [GHSCards]? {
        let tenantIDFromRealm = DataProvider.newRealm().getTenantId()
        var cards = section.cards
        
        if let tenantID = AppSettings.getAppTenant() {
            /* Remove in Improve Your Health section */
            if section.typeKey == 2 {
                
                /* CA=2 | UKE=26 | IGI=28 | SLI=25 */
                
                if tenantID == .UKE || tenantID == .IGI {
                    cards = hideDeviceCashbackHomeScreenCard(for: cards!)
                    cards = hideAppleWatchCashbackHomeScreenCard(for: cards!)
                    cards = hideActivationBarcodeHomeScreenCard(for: cards!)
                }
                
                if tenantID == .CA  && tenantIDFromRealm == 2 {
                    cards = hideAppleWatchCashbackHomeScreenCard(for: cards!)
                }
                
                if tenantID == .EC {
                    cards = hideOrganisedFitnessEventsHomeScreenCard(for: cards!)
                }
            }
            
            /**
             In IGI, if there is no item in choose reward, let's hide "Earn Rewards" card
             **/
            if tenantID == .IGI {
                let arRealm = DataProvider.newARRealm()
                let unclaimedRewards = arRealm.allUnclaimedRewards().filter("awardedRewardStatus != %@", AwardedRewardStatusRef.AvailableToRedeem.rawValue).sorted(byKeyPath: "awardedOn", ascending: false)
                let vouchers: [ARVoucher] = Array(arRealm.allEffectiveVouchers())
                if (unclaimedRewards.count == 0 && vouchers.count > 0) {
                    cards = hideEarnRewardsHomeScreenCard(for: cards!)
                }
            }
            
            /** Temporary Only
             * Hide Cards for Generali.
             */
            if tenantID == .GI {
                /* Remove in Know Your Health section */
                if section.typeKey == 1 {
                    cards = hideKnowYourHealthCardsForGenerali(for: cards!)
                }
                
                /* Remove in Improve Your Health section */
                if section.typeKey == 2 {
                    cards = hideImproveYourHealthCardsForGenerali(for: cards!)
                }
                
                if section.typeKey == 3 {
                    cards = []
                }
            }
            
            // Remove available rewards card for Ecuador.
            if tenantID == .EC {
                if section.typeKey == 3 {
                    cards = hideAvailableRewardsHomeScreenCard(for: cards!)
                }
            }
        }
        return cards
    }
}

/* Create Stub data here */
extension HomeSection{
    
    fileprivate class func hideEarnRewardsHomeScreenCard(for homeCards: [GHSCards]) -> [GHSCards] {
        var cards = homeCards
        
        if let index = cards.index(where: {$0.typeKey == CardTypeRef.Rewards.rawValue}) {
            cards.remove(at: index)
        }
        return cards
    }
    
    fileprivate class func hideDeviceCashbackHomeScreenCard(for homeCards: [GHSCards]) -> [GHSCards] {
        var cards = homeCards
        
        if let index = cards.index(where: {$0.typeKey == CardTypeRef.DeviceCashback.rawValue}) {
            cards.remove(at: index)
        }
        return cards
    }
    
    fileprivate class func hideAppleWatchCashbackHomeScreenCard(for homeCards: [GHSCards]) -> [GHSCards] {
        var cards = homeCards
        
        if let index = cards.index(where: {$0.typeKey == CardTypeRef.AppleWatch.rawValue}) {
            cards.remove(at: index)
        }
        return cards
    }
    
    fileprivate class func hideEmployerServiceCard(for homeCards: [GHSCards]) -> [GHSCards] {
        var cards = homeCards
        
        if let index = cards.index(where: {$0.typeKey == CardTypeRef.HealthServices.rawValue}) {
            cards.remove(at: index)
        }
        return cards
    }
    
    fileprivate class func hideEmployerRewardCard(for homeCards: [GHSCards]) -> [GHSCards] {
        var cards = homeCards
        
        if let index = cards.index(where: {$0.typeKey == CardTypeRef.UKEEmployerStatus.rawValue}) {
            cards.remove(at: index)
        }
        
        return cards
    }
    
    fileprivate class func hideActivationBarcodeHomeScreenCard(for homeCards: [GHSCards]) -> [GHSCards] {
        var cards = homeCards
        
        if let index = cards.index(where: {$0.typeKey == CardTypeRef.ActivationBarcode.rawValue}) {
            cards.remove(at: index)
        }
        return cards
    }
    
    fileprivate class func hideOrganisedFitnessEventsHomeScreenCard(for homeCards: [GHSCards]) -> [GHSCards] {
        var cards = homeCards
        
        if let index = cards.index(where: {$0.typeKey == CardTypeRef.OrganisedFitnessEvents.rawValue}) {
            cards.remove(at: index)
        }
        return cards
    }
    
    fileprivate class func hideAvailableRewardsHomeScreenCard(for homeCards: [GHSCards]) -> [GHSCards] {
        var cards = homeCards
        
        if let index = cards.index(where: {$0.typeKey == CardTypeRef.AvailableRewards.rawValue}) {
            cards.remove(at: index)
        }
        
        return cards
    }
    
    //-------------------------------------------------------------------------------------------------------------------------
    // Temporary Only
    fileprivate class func hideKnowYourHealthCardsForGenerali(for homeCards: [GHSCards]) -> [GHSCards] {
        let cards = homeCards
        var visibleCards: [GHSCards] = []
        
        for card in cards {
            switch card.typeKey {
            case CardTypeRef.VitalityHealthReview.rawValue,
                 CardTypeRef.NonSmokersDecl.rawValue,
                 CardTypeRef.VitalityHealthCheck.rawValue,
                 CardTypeRef.VitNutAssessment.rawValue,
                 CardTypeRef.ScreenandVacc.rawValue:
                visibleCards.append(card)
            default: break
            }
        }
        return visibleCards
    }
    
    fileprivate class func hideImproveYourHealthCardsForGenerali(for homeCards: [GHSCards]) -> [GHSCards] {
        let cards = homeCards
        var visibleCards: [GHSCards] = []
        
        for card in cards {
            switch card.typeKey {
            case CardTypeRef.ActiveRewards.rawValue,
                 CardTypeRef.WellDevandApps.rawValue,
                 CardTypeRef.AppleWatch.rawValue:
                visibleCards.append(card)
            default: break
            }
        }
        return visibleCards
    }
    //-------------------------------------------------------------------------------------------------------------------------

    fileprivate class func getHealthServices() -> GHSCards {
        return GHSCards(amountCompleted: 0, cardItems: nil, cardMetadatas: nil, priority: 0, statusTypeCode: "", statusTypeKey: -1, statusTypeName: "", total: 0, typeCode: "HealthServices", typeKey: CardTypeRef.HealthServices.rawValue, typeName: "Health Services", unitTypeCode: "Points", unitTypeKey: 1, unitTypeName: "Vitality", validFrom: "2017-11-30", validTo: "9999-12-31")
    }
    
    fileprivate class func getEmployerStatusCard() -> GHSCards {
        return GHSCards(amountCompleted: 0, cardItems: nil, cardMetadatas: nil, priority: 0, statusTypeCode: "", statusTypeKey: -1, statusTypeName: "", total: 0, typeCode: "EmployerStatus", typeKey: CardTypeRef.EmployerStatus.rawValue, typeName: "Employer Status", unitTypeCode: "Points", unitTypeKey: 1, unitTypeName: "Vitality", validFrom: "2017-11-30", validTo: "9999-12-31")
    }
    
    fileprivate class func getNuffieldHealthServices() -> GHSCards {
        return GHSCards(amountCompleted: 0, cardItems: nil, cardMetadatas: nil, priority: 0, statusTypeCode: "", statusTypeKey: -1, statusTypeName: "", total: 0, typeCode: "NuffieldHealthServices", typeKey: CardTypeRef.NuffieldHealthServices.rawValue, typeName: "Nuffield Health Services", unitTypeCode: "Points", unitTypeKey: 1, unitTypeName: "Vitality", validFrom: "2017-11-30", validTo: "9999-12-31")
    }
    
    fileprivate class func getOrganisedFitnessEventsCard() -> GHSCards {
        return GHSCards(amountCompleted: 0, cardItems: nil, cardMetadatas: nil, priority: 0, statusTypeCode: "", statusTypeKey: -1, statusTypeName: "", total: 0, typeCode: "OrganisedFitnessEvents", typeKey: CardTypeRef.OrganisedFitnessEvents.rawValue, typeName: "Organised Fitness Events", unitTypeCode: "Points", unitTypeKey: 1, unitTypeName: "Vitality", validFrom: "2017-11-30", validTo: "9999-12-31")
    }
    
    fileprivate class func getDeviceCashbackCard() -> GHSCards {
        return GHSCards(amountCompleted: 0, cardItems: nil, cardMetadatas: nil, priority: 0, statusTypeCode: "NotStarted", statusTypeKey: 1, statusTypeName: "", total: 0, typeCode: "DeviceCashback", typeKey: CardTypeRef.DeviceCashback.rawValue, typeName: "Device Cashback", unitTypeCode: "Points", unitTypeKey: 1, unitTypeName: "Vitality", validFrom: "2017-11-30", validTo: "9999-12-31")
    }
    
    fileprivate class func getPolicyCashbackCard() -> GHSCards {
        return GHSCards(amountCompleted: 0, cardItems: nil, cardMetadatas: nil, priority: 0, statusTypeCode: "", statusTypeKey: -1, statusTypeName: "", total: 0, typeCode: "PolicyCashback", typeKey: CardTypeRef.PolicyCashback.rawValue, typeName: "Policy Cashback", unitTypeCode: "Points", unitTypeKey: 1, unitTypeName: "Vitality", validFrom: "2017-11-30", validTo: "9999-12-31")
    }
}

extension HomeCard {
    public class func parseHomeCard(with card: GHSCards, realm: Realm) -> HomeCard? {
        guard let cardType = CardTypeRef(rawValue: card.typeKey) else { return nil }
        
        let homeCard = realm.homeCard(by: cardType) ?? HomeCard()
        if homeCard.type == .Unknown {
            homeCard.type = cardType
        }
        homeCard.amountCompleted = card.amountCompleted
        homeCard.priority = card.priority
        homeCard.status = CardStatusTypeRef(rawValue: card.statusTypeKey) ?? .Unknown
        homeCard.statusTypeCode = card.statusTypeCode
        homeCard.statusTypeName = card.statusTypeName
        homeCard.total = card.total
        homeCard.typeCode = card.typeCode
        homeCard.typeName = card.typeName
        homeCard.unit = CardUnitTypeRef(rawValue: card.unitTypeKey) ?? .Unknown
        homeCard.unitTypeCode = card.unitTypeCode
        homeCard.validFrom = Parser.main.parseDateString(card.validFrom, fallback: Date())
        homeCard.validTo = Parser.main.parseDateString(card.validTo, fallback: Date())
        
        if let cardMetadatas = card.cardMetadatas {
            for cardMetadata in cardMetadatas {
                homeCard.cardMetadatas.append(HomeCardMetadata.parseHomeCardMetadata(with: cardMetadata))
            }
        }
        if let cardItems = card.cardItems {
            for cardItem in cardItems {
                homeCard.cardItems.append(HomeCardItem.parseHomeCardItem(with: cardItem))
            }
        }
		
        realm.create(HomeCard.self, value: homeCard, update: true)
        let realmHomeCard = realm.object(ofType: HomeCard.self, forPrimaryKey: cardType.rawValue)
        return realmHomeCard
    }
}

extension RewardHomeCard {
	public class func parseRewardHomeCard(with card: GHSCards, realm: Realm) -> RewardHomeCard? {
		
		guard card.typeKey == CardTypeRef.Rewards.rawValue ||
			card.typeKey == CardTypeRef.RewardSelection.rawValue ||
			card.typeKey == CardTypeRef.Vouchers.rawValue else {
				
			return nil
		}

		var metaAwardedRewardId: String?
		if let cardMetadatas = card.cardMetadatas {
			for cardMetadata in cardMetadatas {
				if cardMetadata.typeKey == CardMetadataTypeRef.AwardedRewardId.rawValue {
					metaAwardedRewardId = cardMetadata.value
				}
			}
		}
		
		guard let awardedRewardId = metaAwardedRewardId else {
			debugPrint("Reward Home Card missing required meta data")
			return nil
		}
		guard let cardType = CardTypeRef(rawValue: card.typeKey) else { return nil }
		
		let homeCard = realm.rewardHomeCard(by: awardedRewardId) ?? RewardHomeCard()

		if homeCard.awardedRewardId == "" {
			homeCard.awardedRewardId = awardedRewardId
		}
		homeCard.type = cardType
		homeCard.amountCompleted = card.amountCompleted
		homeCard.priority = card.priority
		homeCard.status = CardStatusTypeRef(rawValue: card.statusTypeKey) ?? .Unknown
		homeCard.statusTypeCode = card.statusTypeCode
		homeCard.statusTypeName = card.statusTypeName
		homeCard.total = card.total
		homeCard.typeCode = card.typeCode
		homeCard.typeName = card.typeName
		homeCard.unit = CardUnitTypeRef(rawValue: card.unitTypeKey) ?? .Unknown
		homeCard.unitTypeCode = card.unitTypeCode
		homeCard.validFrom = Parser.main.parseDateString(card.validFrom, fallback: Date())
		homeCard.validTo = Parser.main.parseDateString(card.validTo, fallback: Date())
		
		if let cardMetadatas = card.cardMetadatas {
			for cardMetadata in cardMetadatas {
				homeCard.cardMetadatas.append(HomeCardMetadata.parseHomeCardMetadata(with: cardMetadata))
			}
		}
		if let cardItems = card.cardItems {
			for cardItem in cardItems {
				homeCard.cardItems.append(HomeCardItem.parseHomeCardItem(with: cardItem))
			}
		}

		realm.create(RewardHomeCard.self, value: homeCard, update: true)
		let realmHomeCard = realm.object(ofType: RewardHomeCard.self, forPrimaryKey: awardedRewardId)
		return realmHomeCard
	}
}

extension HomeCardMetadata {
    public class func parseHomeCardMetadata(with cardMetadata: GHSCardMetadatas) -> HomeCardMetadata {
        let homeCardMetadata = HomeCardMetadata()
		
        homeCardMetadata.typeCode = cardMetadata.typeCode
        homeCardMetadata.type = CardMetadataTypeRef(rawValue: cardMetadata.typeKey) ?? .Unknown
        homeCardMetadata.typeName = cardMetadata.typeName
        homeCardMetadata.value = cardMetadata.value
        
        return homeCardMetadata
    }
}

extension HomeCardItem {
    public class func parseHomeCardItem(with cardItem: GHSCardItems) -> HomeCardItem {
        let homeCardItem = HomeCardItem()
        
        homeCardItem.statusTypeCode = cardItem.statusTypeCode
        homeCardItem.statusType = CardItemStatusTypeRef(rawValue: cardItem.statusTypeKey) ?? .Unknown
        homeCardItem.statusTypeName = cardItem.statusTypeName
        homeCardItem.typeCode = cardItem.typeCode
        homeCardItem.type = CardItemTypeRef(rawValue: cardItem.typeKey) ?? .Unknown
        homeCardItem.typeName = cardItem.typeName
        homeCardItem.validFrom = Parser.main.parseDateString(cardItem.validFrom, fallback: Date())
        homeCardItem.validTo = Parser.main.parseDateString(cardItem.validTo, fallback: Date())
        if let metadatas = cardItem.cardItemMetadatas {
            for metadata in metadatas {
                homeCardItem.homeCardItemMetadatas.append(HomeCardItemMetadata.parseHomeCardItemMetadata(with: metadata))
            }
        }
        
        return homeCardItem
    }
}

extension HomeCardItemMetadata {
    public class func parseHomeCardItemMetadata(with cardItemMetadata: GHSCardItemMetadatas) -> HomeCardItemMetadata {
        let homeCardItemMetadata = HomeCardItemMetadata()
        
        homeCardItemMetadata.typeCode = cardItemMetadata.typeCode
        homeCardItemMetadata.type = CardItemMetadataTypeRef(rawValue: cardItemMetadata.typeKey) ?? .Unknown
        homeCardItemMetadata.typeName = cardItemMetadata.typeName
        homeCardItemMetadata.value = cardItemMetadata.value
        
        return homeCardItemMetadata
    }
}

extension Realm {
    public func deleteOldHomeScreenData() {
        let oldHomeScreenData = self.allHomeScreenData()
        self.delete(oldHomeScreenData)
        let oldHomeVitalityStatus = self.allHomeVitalityStatuses()
        self.delete(oldHomeVitalityStatus)
        let oldDaysLeft = self.allHomeDaysLeftInMembershipPeriod()
        self.delete(oldDaysLeft)
        let oldHomePointsAccounts = self.allHomePointsAccounts()
        self.delete(oldHomePointsAccounts)
        let oldHomeCardMetadatas = self.allHomeCardMetadatas()
        self.delete(oldHomeCardMetadatas)
        let oldHomeCardItems = self.allHomeCardItems()
        self.delete(oldHomeCardItems)
        let oldHomeCardItemMetadatas = self.allHomeCardItemMetadatas()
        self.delete(oldHomeCardItemMetadatas)
        // don't delete the homesections or homecards/rewardshomecards
    }
}
