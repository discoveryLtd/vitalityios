//
//  Parser+GDCGoalProgressAndDetails.swift
//  VitalityActive
//
//  Created by Von Kervin R. Tuico on 04/05/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import RealmSwift
import VIAUtilities

extension Parser {
    public struct GDCGetGoalProgressAndDetails {
        public static func parseGoalProgressAndDetails(response: GetGoalProgressDetailsResponse, completion: @escaping (_ error: Error?) -> Void) {
            Parser.main.parse {
                GDCGetGoalProgressAndDetailsData.parseGetDeviceBenefit(response: response, completion: completion)
            }
        }
    }
}

extension GDCGetGoalProgressAndDetailsData {
    
    public static func parseGetDeviceBenefit(response: GetGoalProgressDetailsResponse, deleteOldData: Bool = true, completion: @escaping (_ error: Error?) -> Void) {
        let realm = DataProvider.newGDCRealm()
        
        try! realm.write {
            
            realm.deleteOldGoalProgressAndDetailsData()
            
            let goalProgressAndDetailsData = GDCGetGoalProgressAndDetailsData()
            
            if let goalProgressAndDetails = response.getGoalProgressAndDetailsResponse {
                goalProgressAndDetailsData.getGoalProgressAndDetailsResponse = GDCGetGoalProgressAndDetails.parseGDCGetGoalProgressAndDetails(with: goalProgressAndDetails)
            }
            realm.add(goalProgressAndDetailsData)
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(3)) {
            completion(nil)
        }
    }
}

extension GDCGetGoalProgressAndDetails {
    
    public static func parseGDCGetGoalProgressAndDetails(with response: GDCGetGoalProgressAndDetailsResponse) -> GDCGetGoalProgressAndDetails {
        
        let gdcGetGoalProgressAndDetails = GDCGetGoalProgressAndDetails()
        
        if let goalTrackerOuts = response.goalTrackerOuts {
            for goalTracker in goalTrackerOuts {
                gdcGetGoalProgressAndDetails.goalTrackerOuts.append(GDCGoalTrackerOuts.parseGDCGoalTrackerOuts(with: goalTracker))
            }
        }
        
        return gdcGetGoalProgressAndDetails
    }
}

extension GDCGoalTrackerOuts {
    
    public static func parseGDCGoalTrackerOuts(with response: GDCGoalTrackerOutsResponse) -> GDCGoalTrackerOuts {
        
        let gdcGoalTrackerOuts = GDCGoalTrackerOuts()
        gdcGoalTrackerOuts.agreementId = response.agreementId ?? 0
        gdcGoalTrackerOuts.completedObjectives = response.completedObjectives ?? 0
        gdcGoalTrackerOuts.effectiveFrom = response.effectiveFrom
        gdcGoalTrackerOuts.effectiveTo = response.effectiveTo
        gdcGoalTrackerOuts.goalCode = response.goalCode
        gdcGoalTrackerOuts.goalKey = GoalRef(rawValue: response.goalKey) ?? .Unknown
        gdcGoalTrackerOuts.goalName = response.goalName
        gdcGoalTrackerOuts.goalTrackerStatusCode = response.goalTrackerStatusCode
        gdcGoalTrackerOuts.goalTrackerStatusKey = GoalTrackerStatusRef(rawValue: response.goalTrackerStatusKey) ?? .Unknown
        gdcGoalTrackerOuts.goalTrackerStatusName = response.goalTrackerStatusName
        gdcGoalTrackerOuts.id = response.id ?? 0
        gdcGoalTrackerOuts.monitorUntil = response.monitorUntil
        gdcGoalTrackerOuts.partyId = response.partyId ?? 0
        gdcGoalTrackerOuts.percentageCompleted = response.percentageCompleted ?? 0
        gdcGoalTrackerOuts.statusChangedOn = response.statusChangedOn
        gdcGoalTrackerOuts.totalObjectives = response.totalObjectives ?? 0
        
        if let goalResultantEvents = response.goalResultantEvents {
            for goalResultantEvent in goalResultantEvents {
                gdcGoalTrackerOuts.goalResultantEvents.append(GDCGoalResultantEvents.parseGDCGoalResultantEvents(with: goalResultantEvent))
            }
        }
        
        if let objectiveTrackers = response.objectiveTrackers {
            for objectiveTracker in objectiveTrackers {
                gdcGoalTrackerOuts.objectiveTrackers.append(GDCObjectiveTrackers.parseGDCObjectiveTrackers(with: objectiveTracker))
            }
        }
        
        return gdcGoalTrackerOuts
    }
}

extension GDCGoalResultantEvents {
    
    public static func parseGDCGoalResultantEvents(with response: GDCGoalResultantEventsResponse) -> GDCGoalResultantEvents {
        
        let gdcGoalResultantEvents = GDCGoalResultantEvents()
        gdcGoalResultantEvents.id = response.id ?? 0
        
        return gdcGoalResultantEvents
    }
}

extension GDCObjectiveTrackers {
    
    public static func parseGDCObjectiveTrackers(with response: GDCObjectiveTrackersResponse) -> GDCObjectiveTrackers {
        
        let gdcObjectiveTrackers = GDCObjectiveTrackers()
        gdcObjectiveTrackers.effectiveFrom = response.effectiveFrom
        gdcObjectiveTrackers.effectiveTo = response.effectiveTo
        gdcObjectiveTrackers.eventCountAchieved = response.eventCountAchieved ?? 0
        gdcObjectiveTrackers.eventCountTarget = response.eventCountTarget ?? 0
        gdcObjectiveTrackers.eventOutcomeAchieved = response.eventOutcomeAchieved ?? false
        gdcObjectiveTrackers.eventOutcomeTarget = response.eventOutcomeTarget
        gdcObjectiveTrackers.monitorUntil = response.monitorUntil
        gdcObjectiveTrackers.objectiveCode = response.objectiveCode
        gdcObjectiveTrackers.objectiveKey = ObjectiveRef(rawValue: response.objectiveKey) ?? .Unknown
        gdcObjectiveTrackers.objectiveName = response.objectiveName
        gdcObjectiveTrackers.percentageCompleted = response.percentageCompleted ?? 0
        gdcObjectiveTrackers.pointsAchieved = response.pointsAchieved ?? 0
        gdcObjectiveTrackers.pointsTarget = response.pointsTarget ?? 0
        gdcObjectiveTrackers.statusChangedOn = response.statusChangedOn
        gdcObjectiveTrackers.statusCode = response.statusCode
        gdcObjectiveTrackers.statusKey = ObjectiveTrackerStatusRef(rawValue: response.statusKey) ?? .Unknown
        gdcObjectiveTrackers.statusName = response.statusName
        
        if let events = response.events {
            for event in events {
                gdcObjectiveTrackers.events.append(GDCEvents.parseGDCEvents(with: event))
            }
        }
        
        if let objectivePointsEntries = response.objectivePointsEntries {
            for objectivePointsEntry in objectivePointsEntries {
                gdcObjectiveTrackers.objectivePointsEntries.append(GDCObjectivePointsEntries.parseGDCObjectivePointsEntries(with: objectivePointsEntry))
            }
        }
        
        return gdcObjectiveTrackers
    }
}

extension GDCEvents {
    
    public static func parseGDCEvents(with response: GDCEventsResponse) -> GDCEvents {
        
        let gdcEvents = GDCEvents()
        gdcEvents.dateLogged = response.dateLogged
        gdcEvents.eventDateTime = response.eventDateTime
        gdcEvents.eventSourceCode = response.eventSourceCode
        gdcEvents.eventSourceKey = EventSourceRef(rawValue: response.eventSourceKey) ?? .Unknown
        gdcEvents.eventSourceName = response.eventSourceName
        gdcEvents.id = response.id ?? 0
        gdcEvents.partyId = response.partyId ?? 0
        gdcEvents.reportedBy = response.reportedBy ?? 0
        gdcEvents.typeCode = response.typeCode
        gdcEvents.typeKey = EventTypeRef(rawValue: response.typeKey) ?? .Unknown
        gdcEvents.typeName = response.typeName
        
        if let eventMetaDatas = response.eventMetaDatas {
            for eventMetaData in eventMetaDatas {
                gdcEvents.eventMetaDatas.append(GDCEventMetaDatas.parseGDCEventMetaDatas(with: eventMetaData))
            }
        }
        
        return gdcEvents
    }
}

extension GDCEventMetaDatas {
    
    public static func parseGDCEventMetaDatas(with response: GDCEventMetaDatasResponse) -> GDCEventMetaDatas {
        
        let gdcEventMetaDatas = GDCEventMetaDatas()
        gdcEventMetaDatas.typeCode = response.typeCode
        gdcEventMetaDatas.typeKey = EventMetaDataTypeRef(rawValue: response.typeKey) ?? .Unknown
        gdcEventMetaDatas.typeName = response.typeName
        gdcEventMetaDatas.value = response.value
        
        if let uom = response.unitOfMeasure {
            let unitOfMeasure = Int(uom) ?? UnitOfMeasureRef.Unknown.rawValue
            gdcEventMetaDatas.unitOfMeasureType = UnitOfMeasureRef(rawValue: unitOfMeasure) ?? .Unknown
        }
        
        return gdcEventMetaDatas
    }
}

extension GDCObjectivePointsEntries {
    
    public static func parseGDCObjectivePointsEntries(with response: GDCObjectivePointsEntriesResponse) -> GDCObjectivePointsEntries {
        
        let gdcObjectivePointsEntries = GDCObjectivePointsEntries()
        gdcObjectivePointsEntries.categoryCode = response.categoryCode
        gdcObjectivePointsEntries.categoryKey = PointsEntryCategoryRef(rawValue: response.categoryKey) ?? .Unknown
        gdcObjectivePointsEntries.categoryName = response.categoryName
        gdcObjectivePointsEntries.earnedValue = response.earnedValue ?? 0
        gdcObjectivePointsEntries.effectiveDate = response.effectiveDate
        gdcObjectivePointsEntries.eventId = response.eventId ?? 0
        gdcObjectivePointsEntries.id = response.id ?? 0
        gdcObjectivePointsEntries.partyId = response.partyId ?? 0
        gdcObjectivePointsEntries.pointsContributed = response.pointsContributed ?? 0
        gdcObjectivePointsEntries.potentialValue = response.potentialValue ?? 0
        gdcObjectivePointsEntries.prelimitValue = response.prelimitValue ?? 0
        gdcObjectivePointsEntries.statusChangeDate = response.statusChangeDate
        gdcObjectivePointsEntries.statusTypeCode = response.statusTypeCode
        gdcObjectivePointsEntries.statusTypeKey = PointsEntryStatusTypeRef(rawValue: response.statusTypeKey) ?? .Unknown
        gdcObjectivePointsEntries.statusTypeName = response.statusTypeName
        gdcObjectivePointsEntries.systemAwareOn = response.systemAwareOn
        gdcObjectivePointsEntries.typeCode = response.typeCode
        gdcObjectivePointsEntries.typeKey = PointsEntryTypeRef(rawValue: response.typeKey) ?? .Unknown
        gdcObjectivePointsEntries.typeName = response.typeName
        
        if let contents = response.contents {
            for content in contents {
                gdcObjectivePointsEntries.contents.append(GDCContents.parseGDCContents(with: content))
            }
        }
        
        if let pointsEntryMetadatas = response.pointsEntryMetadatas {
            for pointsEntryMetadata in pointsEntryMetadatas {
                gdcObjectivePointsEntries.pointsEntryMetadatas.append(
                    GDCCPointsEntryMetadatas.parseGDCCPointsEntryMetadatas(with: pointsEntryMetadata))
            }
        }
        
        if let reasons = response.reason {
            for reason in reasons {
                gdcObjectivePointsEntries.reason.append(GDCReason.parseGDCReason(with: reason))
            }
        }
        
        return gdcObjectivePointsEntries
    }
}

extension GDCContents {
    
    public static func parseGDCContents(with response: GDCContentsResponse) -> GDCContents {
        
        let gdcContents = GDCContents()
        gdcContents.label = response.label
        gdcContents.value = response.value
        
        return gdcContents
    }
}

extension GDCCPointsEntryMetadatas {
    
    public static func parseGDCCPointsEntryMetadatas(with response: GDCCPointsEntryMetadatasResponse) -> GDCCPointsEntryMetadatas {
        
        let gdccPointsEntryMetadatas = GDCCPointsEntryMetadatas()
        gdccPointsEntryMetadatas.typeCode = response.typeCode
        gdccPointsEntryMetadatas.typeKey = EventMetaDataTypeRef(rawValue: response.typeKey) ?? .Unknown
        gdccPointsEntryMetadatas.typeName = response.typeName
        gdccPointsEntryMetadatas.value = response.value
        
        if let uom = response.unitOfMeasure {
            let unitOfMeasure = Int(uom) ?? UnitOfMeasureRef.Unknown.rawValue
            gdccPointsEntryMetadatas.unitOfMeasureType = UnitOfMeasureRef(rawValue: unitOfMeasure) ?? .Unknown
        }

        return gdccPointsEntryMetadatas
    }
}

extension GDCReason {
    
    public static func parseGDCReason(with response: GDCReasonResponse) -> GDCReason {
        
        let gdcReason = GDCReason()
        gdcReason.categoryCode = response.categoryCode
        gdcReason.categoryKey = ReasonCategoryRef(rawValue: response.categoryKey) ?? .Unknown
        gdcReason.categoryName = response.categoryName
        gdcReason.reasonCode = response.reasonCode
        gdcReason.reasonKey = ReasonRef(rawValue: response.reasonKey) ?? .Unknown
        gdcReason.reasonName = response.reasonName
        
        return gdcReason
    }
}

extension Realm {
    public func deleteOldGoalProgressAndDetailsData() {
        // Create deletion..
    }
}
