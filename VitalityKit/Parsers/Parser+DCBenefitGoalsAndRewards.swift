//
//  Parser+DCBenefitGoalsAndRewards.swift
//  VitalityActive
//
//  Created by Steven Layug on 4/27/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import UIKit

import Foundation
import RealmSwift
import VIAUtilities

extension Parser {
    public struct BenefitGoalsAndRewards {
        public static func parseGDCBenefit(response: GetBenefitGoalsAndRewardsFeatureResponse, deleteOldData:Bool = true, completion: @escaping (_ error: Error?) -> Void) {
            Parser.main.parse {
                GDCBenefitData.parseGDCBenefitData(response: response, deleteOldData: deleteOldData, completion: completion)
            }
        }
    }
}

extension GDCBenefitData {
    public static func parseGDCBenefitData(response: GetBenefitGoalsAndRewardsFeatureResponse, deleteOldData:Bool = true, completion: @escaping (_ error: Error?) -> Void) {
        
        let deviceCashbackRealm = DataProvider.newGDCRealm()
        
        try! deviceCashbackRealm.write {
            if deleteOldData {
                deviceCashbackRealm.deleteOldDBGRData()
            }
            
            let benefitData = GDCBenefitData()
            
            if let benefits = response.getBenefitGoalsAndRewardsFeatureResponse {
                for benefit in benefits {
                    benefitData.benefit.append(GDCBGRBenefit.parseGDCBenefit(with: benefit))
                }
            }
            deviceCashbackRealm.add(benefitData)
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(3)) {
            completion(nil)
        }
    }
}

extension GDCBGRBenefit {
    public static func parseGDCBenefit(with response: BGRFeatureBenefit) -> GDCBGRBenefit {
        let benefit = GDCBGRBenefit()
        
        benefit.effectiveTo = response.effectiveTo ?? ""
        benefit.effectiveFrom = response.effectiveFrom ?? ""
        benefit.goalsRemaining = response.goalsRemaining ?? 0
        benefit.totalGoalsCompleted = response.totalGoalsCompleted ?? 0
        benefit.totalGoalsForBenefit = response.totalGoalsForBenefit ?? 0
        benefit.totalGoalsRewardAmount = response.totalGoalsRewardAmount ?? ""
        benefit.totalGoalsRewardQuantity = response.totalGoalsRewardQuantity ?? 0
        benefit.id = response.id ?? 0
        
        if let goalTrackers = response.goalTrackers {
            for goalTracker in goalTrackers {
                benefit.goalTrackers.append(GDCGoalTrackers.parseGDCGoalTrackers(with: goalTracker))
            }
        }
        
        if let purchase = response.purchase {
            benefit.purchase = GDCBGRPurchase.parseGDCPurchase(with: purchase)
        }
        
        return benefit
    }
}

extension GDCGoalTrackers {
    public static func parseGDCGoalTrackers(with response: BGRGoalTracker) -> GDCGoalTrackers{
        let goalTracker = GDCGoalTrackers()
        
        goalTracker.goalCode = response.goalCode ?? ""
        goalTracker.goalKey = response.goalKey ?? 0
        goalTracker.goalName = response.goalName ?? ""
        goalTracker.monitorUntil = response.monitorUntil ?? ""
        goalTracker.pointsAchievedTowardsGoal = response.pointsAchievedTowardsGoal ?? 0
        goalTracker.validFrom = response.validFrom ?? ""
        goalTracker.validTo = response.validTo ?? ""
        goalTracker.highestObjectiveAchievedName = response.highestObjectiveAchievedName ?? ""
        
        if let rewardValues = response.rewardValues {
            for rewardValue in rewardValues {
                goalTracker.rewardValues.append(GDCRewardValues.parseGDCRewardValues(with: rewardValue))
            }
        }
        
        return goalTracker
    }
}
    
extension GDCRewardValues {
    public static func parseGDCRewardValues(with response: BGRRewardValues) -> GDCRewardValues {
        let rewardValue = GDCRewardValues()
        
        rewardValue.totalRewardValueAmount = response.totalRewardValueAmount ?? ""
        rewardValue.totalRewardValueQuantity = response.totalRewardValueQuantity ?? 0
        
        return rewardValue
    }
}

extension GDCBGRPurchase {
    public static func parseGDCPurchase(with response: BGRPurchase) -> GDCBGRPurchase {
        let purchase = GDCBGRPurchase()
        
        purchase.purchaseId = response.purchaseId ?? 0
       
        if let amounts = response.amounts {
            for amount in amounts {
                purchase.amounts.append(GDCAmounts.parseGDCAmounts(with: amount))
            }
        }
        
        if let purchaseItem = response.purchaseItemProduct {
            purchase.purchaseItemProduct = GDCPurchaseItemProduct.parseGDCPurchaseItemProduct(with: purchaseItem)
        }
        
        return purchase
    }
}

extension GDCAmounts {
    public static func parseGDCAmounts(with response: BGRAmounts) -> GDCAmounts{
        let amount = GDCAmounts()
        
        amount.typeCode = response.typeCode ?? ""
        amount.typeKey = response.typeKey ?? 0
        amount.typeName = response.typeName ?? ""
        amount.value = response.value ?? ""
        
        return amount
    }
}

extension GDCPurchaseItemProduct {
    public static func parseGDCPurchaseItemProduct(with response: BGRPurchaseItemProduct) -> GDCPurchaseItemProduct{
        let purchaseItemProduct = GDCPurchaseItemProduct()
        
        purchaseItemProduct.productDescription = response.productDescription ?? ""
        purchaseItemProduct.productKey = response.productKey ?? 0
        
        return purchaseItemProduct
    }
}

extension Realm {
    public func deleteOldDBGRData() {
        let getGoalsAndRewardsData = self.allGDCBenefitData()
        self.delete(getGoalsAndRewardsData)
        let getGDCBGRBenefit = self.allGDCBGRBenefits()
        self.delete(getGDCBGRBenefit)
        let getGDCGoalTrackers = self.allGDCGoalTrackers()
        self.delete(getGDCGoalTrackers)
        let getGDCRewardValues = self.allGDCRewardValues()
        self.delete(getGDCRewardValues)
        let getGDCBGRPurchase = self.allGDCBGRPurchase()
        self.delete(getGDCBGRPurchase)
        let getGDCAmounts = self.allGDCAmounts()
        self.delete(getGDCAmounts)
        let getGDCPurchaseItemProduct = self.allGDCPurchaseItemProduct()
        self.delete(getGDCPurchaseItemProduct)
    }
}
