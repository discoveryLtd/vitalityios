import Foundation
import RealmSwift
import VIAUtilities

extension Parser {
    public struct Partners {
        public static func parseGetPartnersByCategory(response: GetPartnersByCategoryResponse, completion: @escaping (_ error: Error?) -> Void) {
            Parser.main.parse {
                PartnerCategory.parseAndPersistPartners(with: response)
                DispatchQueue.main.async {
                    completion(nil)
                }
            }
        }
    }
}

extension PartnerCategory {
    public static func parseAndPersistPartners(with getPartnersByCategoryResponse: GetPartnersByCategoryResponse) {
        let coreRealm = DataProvider.newPartnerRealm()
        coreRealm.deleteOldPartnersData()
        
        let partnerCategoryModel = PartnerCategory()
        partnerCategoryModel.typeCode = getPartnersByCategoryResponse.typeCode
        partnerCategoryModel.typeKey = getPartnersByCategoryResponse.typeKey
        partnerCategoryModel.typeName = getPartnersByCategoryResponse.typeName
        
        if let groups = getPartnersByCategoryResponse.productFeatureGroups {
            for group in groups {
                partnerCategoryModel.partnerGroups.append(PartnerGroup.parse(partnerProductFeatureGroup: group))
            }
        }
        
        try! coreRealm.write {
            coreRealm.add(partnerCategoryModel)
        }
        
    }
}

extension PartnerGroup {
    
    class func parse(partnerProductFeatureGroup: PBCProductFeatureGroups) -> PartnerGroup {
        let partnerGroupModel = PartnerGroup()
        
        partnerGroupModel.name = partnerProductFeatureGroup.name
        partnerGroupModel.key = partnerProductFeatureGroup.key
        
        if let partners = partnerProductFeatureGroup.partnerProductFeatures {
            for partner in partners {
                partnerGroupModel.partners.append(Partner.parsePartner(with: partner))
            }
        }
        
        return partnerGroupModel
    }
}

extension Partner {
    public class func parsePartner(with productFeature: PBCPartnerProductFeatures) -> Partner {
        let partnerModel = Partner()
        
        partnerModel.logoFileName = productFeature.logoFileName!
        partnerModel.longDescription = productFeature.longDescription
        partnerModel.shortDescription = productFeature.description
        partnerModel.name = productFeature.name
        partnerModel.typeCode = productFeature.typeCode
        partnerModel.typeName = productFeature.typeName
        partnerModel.typeKey = productFeature.typeKey
        partnerModel.heading = productFeature.heading
        
        return partnerModel
    }
}

public extension Realm {
    public func deleteOldPartnersData() {
        try! self.write {
            let oldPartners = self.allPartners()
            self.delete(oldPartners)
            let oldPartnerGroups = self.allPartnerGroups()
            self.delete(oldPartnerGroups)
            let oldPartnerCategories = self.allPartnerCategories()
            self.delete(oldPartnerCategories)
        }
    }
}
