//
//  Parser+Events.swift
//  VitalityActive
//
//  Created by OJ Garde on 17/11/2017.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import RealmSwift
import VIAUtilities

extension Parser {
    public struct Events {
        
        public static func parseEvents(response: GetEventByPartyResponse, completion: @escaping (( _: Error?, _: EventRO?) -> Void)) {
            Parser.main.parse {
                let pointsAccountResult = EventRO.persistEvents(response: response)
                DispatchQueue.main.async {
                    completion(nil, pointsAccountResult)
                }
            }
        }
    }
}


extension EventRO {
    
    public static func persistEvents(response: GetEventByPartyResponse) -> EventRO? {
        
        let realm = DataProvider.newRealm()
        
        let events = response.event
        debugPrint("Parsing \(String(describing: events?.count)) number of events")
        try! realm.write {
            
            resetDB(realm)
            
            for event in events ?? [GEBPEvent]() {
                let eventModel = EventRO()
                
                eventModel.dateLogged        = Parser.main.parseDateString(event.dateLogged) ?? Date.distantPast
                eventModel.eventDateTime     = Parser.main.parseDateString(event.eventDateTime)
                eventModel.eventSourceCode   = event.eventSourceCode ?? ""
                eventModel.eventSourceKey    = EventSourceRef(rawValue: event.eventSourceKey ?? -1) ?? .Unknown
                eventModel.eventSourceName   = event.eventSourceName ?? ""
                eventModel.id                = event.id ?? 0
                eventModel.partyId           = event.partyId ?? 0
                eventModel.reportedBy        = event.reportedBy ?? 0
                eventModel.typeCode          = event.typeCode ?? ""
                eventModel.typeKey           = EventTypeRef(rawValue: event.typeKey) ?? .Unknown
                eventModel.typeName          = event.typeName ?? ""
                
                
                eventModel.eventExternalReference   = getExternalReferences(realm,  data: event.eventExternalReference)
                eventModel.associatedEvents         = getAssociatedEvents(realm,    data: event.associatedEvents)
                eventModel.eventMetaDataOuts        = getMetaDataOuts(realm,        data: event.eventMetaDataOuts)
                
                eventModel.eventCategoryKey         = EventCategoryRef(rawValue: event.categoryKey ?? -1) ?? .Unknown
                eventModel.eventCategoryCode        = event.categoryCode ?? ""
                eventModel.eventCategoryName        = event.categoryName ?? ""
                
                
                realm.create(EventRO.self, value: eventModel, update: true)
            }
        }
        
        return realm.objects(EventRO.self).first
    }
    
    private static func resetDB(_ realm: Realm){
        //Clear all old data before adding new pointsEntry data to prevent duplicates
        let oldEventsExternalReferences = realm.allEventsExternalReference()
        realm.delete(oldEventsExternalReferences)
        let oldEventsAssociatedReferences = realm.allEventsAssociatedReference()
        realm.delete(oldEventsAssociatedReferences)
        let oldEventsMetaDataOuts = realm.allEventsMetaDataOuts()
        realm.delete(oldEventsMetaDataOuts)
        let oldEvents = realm.allEvents()
        realm.delete(oldEvents)
    }
    
    private static func getMetaDataOuts(_ realm: Realm, data: [GEBPEventMetaDataOuts]?) -> List<GEBPEventMetaDataOutsRO>{
        let container = List<GEBPEventMetaDataOutsRO>()
        for src in data ?? []{
            let dest            = GEBPEventMetaDataOutsRO()
            dest.typeCode       = src.typeCode ?? ""
            dest.typeKey        = EventMetaDataTypeRef(rawValue: src.typeKey) ?? .Unknown
            dest.typeName       = src.typeName ?? ""
            dest.unitOfMeasure  = src.unitOfMeasure ?? ""
            dest.value          = src.value ?? ""
            
            container.append(dest)
        }
        return container
    }
    
    private static func getExternalReferences(_ realm: Realm, data: [GEBPEventExternalReference]?) -> List<GEBPEventExternalReferenceRO>{
        let container = List<GEBPEventExternalReferenceRO>()
        for src in data ?? []{
            let dest         = GEBPEventExternalReferenceRO()
            dest.typeCode    = src.typeCode ?? ""
            dest.typeKey     = EventExternalReferenceTypeRef(rawValue: src.typeKey) ?? .Unknown
            dest.typeName    = src.typeName ?? ""
            
            container.append(dest)
        }
        return container
    }
    
    private static func getAssociatedEvents(_ realm: Realm, data: [GEBPAssociatedEvents]?) -> List<GEBPAssociatedEventsRO>{
        let container = List<GEBPAssociatedEventsRO>()
        for src in data ?? []{
            let dest = GEBPAssociatedEventsRO()
            
            dest.associationTypeCode    = src.associationTypeCode ?? ""
            dest.associationTypeKey     = EventAssociationTypeRef(rawValue: src.associationTypeKey) ?? .Unknown
            dest.associationTypeName    = src.associationTypeName
            dest.dateLogged             = Parser.main.parseDateString(src.dateLogged)
            dest.dateTimeAssociated     = Parser.main.parseDateString(src.dateTimeAssociated)
            dest.eventDateTime          = Parser.main.parseDateString(src.eventDateTime)
            dest.eventId                = src.eventId ?? 0
            dest.eventSourceCode        = src.eventSourceCode ?? ""
            dest.eventSourceKey         = EventSourceRef(rawValue: src.eventSourceKey ?? -1) ?? .Unknown
            dest.eventSourceName        = src.eventSourceName
            dest.eventTypeCode          = src.eventTypeCode ?? ""
            dest.eventTypeName          = src.eventTypeName ?? ""
            dest.eventTypeKey           = EventTypeRef(rawValue: src.eventTypeKey) ?? .Unknown
            
            container.append(dest)
        }
        return container
    }
}

