//
//  Parser+SAVGetEventByParty.swift
//  VitalityActive
//
//  Created by Von Kervin R. Tuico on 14/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import RealmSwift
import VIAUtilities

extension Parser {
    public struct SAVHistory {
        
        public static func parseGetEventByParty(response: GetEventByPartyResponse, deleteOldData: Bool = true, completion: @escaping (_ error: Error?) -> Void) {
            Parser.main.parse {
                SAVHistoryEvent.persistGetEventByParty(response: response, deleteOldData: deleteOldData, completion: completion)
            }
        }
    }
}

extension SAVHistoryEvent {
    
    public static func persistGetEventByParty(response: GetEventByPartyResponse, deleteOldData: Bool = true, completion: @escaping (_ error: Error?) -> Void) {
        
        let savHistoryRealm = DataProvider.newSAVRealm()
        let historyEvents = response.event
        try! savHistoryRealm.write {
            if deleteOldData{
                savHistoryRealm.deleteOldSAVHistoryData()
            }
            
            if let events = historyEvents {
                
                for event in events {
                    let eventModel = SAVHistoryEvent()
                    eventModel.categoryCode = event.categoryCode
                    eventModel.categoryKey = event.categoryKey ?? 0
                    eventModel.categoryName = event.categoryName
                    eventModel.dateLogged = Parser.main.parseDateString(event.dateLogged) ?? Date.distantPast
                    eventModel.eventDateTime = Parser.main.parseDateString(event.eventDateTime)
                    eventModel.eventSourceCode = event.eventSourceCode
                    eventModel.eventSourceKey = event.eventSourceKey ?? 0
                    eventModel.eventSourceName = event.eventSourceName
                    eventModel.id = event.id ?? 0
                    eventModel.partyId = event.partyId ?? 0
                    eventModel.reportedBy = event.reportedBy ?? 0
                    eventModel.typeCode = event.typeCode
                    eventModel.typeKey = event.typeKey
                    eventModel.typeName = event.typeName
                    
                    if let associatedEvents = event.associatedEvents {
                        for associatedEvent in associatedEvents {
                            let associatedEventModel = SAVAssociatedEvents()
                            associatedEventModel.associationTypeCode = associatedEvent.associationTypeCode
                            associatedEventModel.associationTypeKey = associatedEvent.associationTypeKey
                            associatedEventModel.associationTypeName = associatedEvent.associationTypeName
                            associatedEventModel.dateLogged = associatedEvent.dateLogged
                            associatedEventModel.dateTimeAssociated = associatedEvent.dateTimeAssociated
                            associatedEventModel.eventDateTime = associatedEvent.eventDateTime
                            associatedEventModel.eventId = associatedEvent.eventId ?? 0
                            associatedEventModel.eventSourceCode = associatedEvent.eventSourceCode
                            associatedEventModel.eventSourceKey = associatedEvent.eventSourceKey ?? 0
                            associatedEventModel.eventSourceName = associatedEvent.eventSourceName
                            associatedEventModel.eventTypeCode = associatedEvent.eventTypeCode
                            associatedEventModel.eventTypeKey = associatedEvent.eventTypeKey
                            associatedEventModel.eventTypeName = associatedEvent.eventTypeName
                            associatedEventModel.categoryCode = associatedEvent.categoryCode
                            associatedEventModel.categoryKey = associatedEvent.categoryKey ?? 0
                            associatedEventModel.categoryName = associatedEvent.categoryName
                            
                            eventModel.associatedEvents.append(associatedEventModel)
                        }
                    }
                    
                    if let eventExternalReferences = event.eventExternalReference {
                        for eventExternalReference in eventExternalReferences {
                            let eventExternalReferenceModel = SAVEventExternalReference()
                            eventExternalReferenceModel.typeCode = eventExternalReference.typeCode
                            eventExternalReferenceModel.typeKey = eventExternalReference.typeKey
                            eventExternalReferenceModel.typeName = eventExternalReference.typeName
                            eventExternalReferenceModel.value = eventExternalReference.value
                            
                            eventModel.eventExternalReference.append(eventExternalReferenceModel)
                        }
                    }
                    
                    if let eventMetaDataOuts = event.eventMetaDataOuts {
                        for eventMetaDataOut in eventMetaDataOuts {
                            let eventMetaDataOutModel = SAVEventMetaDataOuts()
                            eventMetaDataOutModel.typeCode = eventMetaDataOut.typeCode
                            eventMetaDataOutModel.typeKey = eventMetaDataOut.typeKey
                            eventMetaDataOutModel.typeName = eventMetaDataOut.typeName
                            eventMetaDataOutModel.unitOfMeasure = eventMetaDataOut.unitOfMeasure
                            eventMetaDataOutModel.value = eventMetaDataOut.value
                            
                            eventModel.eventMetaDataOuts.append(eventMetaDataOutModel)
                        }
                    }
                    
                    savHistoryRealm.create(SAVHistoryEvent.self, value: eventModel, update: true)
                }
            }
        }
        DispatchQueue.main.async {
            completion(nil)
        }
    }
    
    static func setSAVCache() {
        Realm.deleteCurrentSAVCache()
        let savRealm = DataProvider.newSAVRealm()
        try! savRealm.write {
            let savCache = SAVCache()
            savRealm.add(savCache)
        }
    }
}

extension Realm {
    public func deleteOldSAVHistoryData() {
        let oldSAVHistoryEvents = self.allSAVHistoryEvents()
        self.delete(oldSAVHistoryEvents)
        let oldSAVAssociatedEvents = self.allSAVAssociatedEvents()
        self.delete(oldSAVAssociatedEvents)
        let oldSAVEventExternalReference = self.allSAVEventExternalReference()
        self.delete(oldSAVEventExternalReference)
        let oldSAVEventMetaDataOuts = self.allSAVEventMetaDataOuts()
        self.delete(oldSAVEventMetaDataOuts)
    }
}
