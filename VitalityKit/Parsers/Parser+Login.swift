import Foundation
import SwiftDate
import RealmSwift
import VIAUtilities

enum MissingLoginDataError: Error {
    case missingSection
}

extension Parser {
    public struct Login {

        public static func parse(response: LoginResponse?, completion: @escaping (_ error: Error?) -> Void) {
            DispatchQueue.global(qos: .background).async {

                let group = DispatchGroup()
                let subsectionParsingComplete = {
                    group.leave()
                }

                // Parse
                do {
                    group.enter()
                    try Parser.Login.parseUserInstructions(response?.userInstructions, completion: subsectionParsingComplete)

                    group.enter()
                    try Parser.Login.parseVitalityParty(response?.partyDetails, completion: subsectionParsingComplete)

                    group.enter()
                    try Parser.Login.parseVitalityPartyMemberships(response?.vitalityMembership, completion: subsectionParsingComplete)

                    if let application = response?.application {
                        group.enter()
                        try Parser.Login.parseVitalityAppConfig(application, completion: subsectionParsingComplete)
                    } else {
                        debugPrint("App config up to date with current on service")
                    }

                    // Set API manager auth
                    _ = group.wait(timeout: .distantFuture)
                    Wire.setAuthorisedAPIManagerToken(token: VitalityParty.accessTokenForCurrentParty())

                    // Complete
                    DispatchQueue.main.async {
                        completion(nil)
                    }
                } catch _ {
                    DispatchQueue.main.async {
                        completion(MissingLoginDataError.missingSection)
                    }
                }
            }
        }

        public static func parseUserInstructions(_ userInstructions: [LoginUserInstructions]?, completion: @escaping () -> Void) throws {
            guard let validUserInstructions = userInstructions else {
                throw MissingLoginDataError.missingSection
            }

            Parser.main.parse {
                UserInstruction.parseAndPersist(validUserInstructions)
                completion()
            }
        }

        public static func parseVitalityParty(_ partyDetails: LoginPartyDetails?, completion: @escaping () -> Void) throws {
            guard let validPartyDetails = partyDetails else {
                throw MissingLoginDataError.missingSection
            }

            Parser.main.parse {
                VitalityParty.parseAndPersist(validPartyDetails)
                completion()
            }
        }

        public static func parseVitalityPartyMemberships(_ memberships: [LoginVitalityMembership?]?, completion: @escaping () -> Void) throws {
            guard let validMemberships = memberships else {
                throw MissingLoginDataError.missingSection
            }

            Parser.main.parse {
                VitalityPartyMembership.parseAndPersist(validMemberships)
                completion()
            }
        }

        public static func parseVitalityAppConfig(_ application: LoginApplication?, completion: @escaping () -> Void) throws {
            guard let validApplication = application else {
                throw MissingLoginDataError.missingSection
            }

            Parser.main.parse {
                AppConfig.parseAndPersist(validApplication)
                completion()
            }
        }
    }
}

extension AppConfig {
    public static func parseAndPersist(_ application: LoginApplication) {
        if let returnedReleaseVersion = application.configurationVersion.releaseVersion {
            if let responseReleaseVersionValue = Float(returnedReleaseVersion) {
                if (AppConfigVersion.currentAppConfigReleaseVersionFloat() > responseReleaseVersionValue) {
                    return
                }
            }
        }

        AppConfig.deleteExisting()

        let realm = DataProvider.newRealm()
        try! realm.write {
            let appConfig = AppConfig()
            if let typeKey = AppConfig.ConfigType(rawValue: application.typeKey) {
                appConfig.typeKey = typeKey
            }
            appConfig.name = application.name ?? ""
            appConfig.typeCode = application.typeCode ?? ""
            appConfig.appConfigVersion = AppConfigVersion.parseAndReturn(application.configurationVersion)
            appConfig.upgradeUrl = application.upgradeUrl

            realm.add(appConfig)
        }
    }
}

extension AppConfigVersion {
    public static func parseAndReturn(_ applicationVersion: LoginConfigurationVersion_isRequired_) -> AppConfigVersion {
        let appConfigVersion = AppConfigVersion()
        appConfigVersion.releaseVersion = applicationVersion.releaseVersion ?? ""
        appConfigVersion.effectiveTo = Parser.main.parseDateString(applicationVersion.effectiveTo)
        appConfigVersion.effectiveFrom = Parser.main.parseDateString(applicationVersion.effectiveFrom)

        if let features = AppConfigFeature.parseAndReturn(applicationVersion.applicationFeatures) {
            for feature in features {
                appConfigVersion.appFeature.append(feature)
            }
        }
        return appConfigVersion
    }
}

extension AppConfigFeature {
    public static func  parseAndReturn(_ features: [LoginApplicationFeatures]?) -> Array<AppConfigFeature>? {
        if let configFeatures = features {
            var configurationFeatures = Array<AppConfigFeature>()
            for feature in configFeatures {
                let configFeature = AppConfigFeature()
                configFeature.name = feature.name ?? ""
                configFeature.toggle = feature.toggle ?? false
                configFeature.typeCode = feature.typeCode ?? ""
                if let typeKey = AppConfigFeature.AppConfigFeatureType(rawValue: feature.typeKey) {
                    configFeature.typeKey = typeKey
                }
                if let parameters = AppConfigFeatureParameter.parseAndReturn(feature.applicationFeatureParameters) {
                    for parameter in parameters {
                        configFeature.featureParameters.append(parameter)
                    }
                }
                configurationFeatures.append(configFeature)
            }
            return configurationFeatures
        }
        return nil
    }
}

extension AppConfigFeatureParameter {
    public static func parseAndReturn(_ parameters: [LoginApplicationFeatureParameters]?) -> Array<AppConfigFeatureParameter>? {
        if let configFeatureParameters = parameters {
            var featuresParameters = Array<AppConfigFeatureParameter>()
            for parameter in configFeatureParameters {
                let configParameter = AppConfigFeatureParameter()
                configParameter.name = parameter.name
                configParameter.value = parameter.value
                featuresParameters.append(configParameter)
            }
            return featuresParameters
        }
        return nil
    }
}

// MARK: - Membership

extension VitalityPartyMembership {

    public static func currentVitalityMembershipPeriod() -> VitalityMembershipCurrentPeriod? {
        let realm = DataProvider.newRealm()
        let currentMemberships = realm.objects(VitalityPartyMembership.self)
        return currentMemberships.last?.currentMembershipPeriod ?? nil
    }
    
    public static func currentVitalityProduct() -> VitalityProduct? {
        let realm = DataProvider.newRealm()
        let currentMemberships = realm.objects(VitalityPartyMembership.self)
        return currentMemberships.last?.vitalityProducts.first ?? nil
    }

    public static func parseAndPersist(_ memberships: [LoginVitalityMembership?]) {
        let realm = DataProvider.newRealm()
        try! realm.write {
            let currentMemberships = realm.objects(VitalityPartyMembership.self)
            for currentMembership in currentMemberships {
                realm.delete(currentMembership)
            }
        }

        try! realm.write {
            for membership in memberships {
                if let existingMemberShip = membership {
                    let vitalityMembership = VitalityPartyMembership()
                    vitalityMembership.ID = existingMemberShip.id ?? 0 // TODO: this is not a good way to handle the error
                    let references = VitalityMembershipReference.parseAndReturn(existingMemberShip.memberReferences)
                    for reference in references {
                        vitalityMembership.references.append(reference)
                    }

                    if let currentMembershipPeriod = existingMemberShip.currentVitalityMembershipPeriod {
                        let currentMembershipPeriods = realm.objects(VitalityMembershipCurrentPeriod.self)
                        for currentMembershipPeriod in currentMembershipPeriods {
                            realm.delete(currentMembershipPeriod)
                        }
                        let currentMembershipPeriodModel = VitalityMembershipCurrentPeriod()
                        currentMembershipPeriodModel.effectiveFrom = Parser.main.parseDateString(currentMembershipPeriod.effectiveFrom)
                        currentMembershipPeriodModel.effectiveTo = Parser.main.parseDateString(currentMembershipPeriod.effectiveTo)

                        vitalityMembership.currentMembershipPeriod = currentMembershipPeriodModel
                    }

                    let products = VitalityProduct.parseAndReturn(existingMemberShip.membershipProducts ?? [LoginMembershipProducts]())
                    for product in products {
                        vitalityMembership.vitalityProducts.append(product)
                    }

                    let parties = VitalityMembershipParty.parseAndReturn(existingMemberShip.parties ?? [LoginParty]())
                    for party in parties {
                        vitalityMembership.membershipParties.append(party)
                    }
                    
                    let periods = VitalityMembershipRenewalPeriod.parseAndReturn(existingMemberShip.renewalPeriods ?? [LoginRenewalPeriods]())
                    for period in periods {
                        vitalityMembership.renewalPeriods.append(period)
                    }
                    realm.add(vitalityMembership)
                }
            }
        }
    }
    
    /**
     ge20180309 : VA-19197 : Before navigating to home screen, validate if vitality membership has indeed started.
     */
    public static func startedMembership() -> Bool {
        
        var start = VitalityPartyMembership.currentVitalityMembershipPeriod()?.effectiveFrom
        if(start == nil) {
            start = VitalityPartyMembership.currentVitalityProduct()?.effectiveFrom
        }
        
        guard let effectivity = start else{
            return false
        }
        
        guard let startDate = wrapDateWithLocale(effectivity),
            let curDate = wrapDateWithLocale(Date()) else {
                return false
        }
        
//        debugPrint("curDate: \(Wire.getFormattedDateWithTimezone(date: curDate))")
//        debugPrint("startDate: \(Wire.getFormattedDateWithTimezone(date: startDate))")
        return curDate >= startDate
    }
    
    public static func wrapDateWithLocale(_ date: Date) -> Date?{
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .full
        dateFormatter.timeStyle = .none
        
        if let timezone = Bundle.main.object(forInfoDictionaryKey: "VIATimezone") as? String{
            dateFormatter.timeZone = TimeZone(identifier: timezone)
        }
        let dateString = dateFormatter.string(from: date)
        
        return dateFormatter.date(from: dateString)
    }
    
    /**
     ge20180309 : VA-19197 : Get the membership start date
     */
    public static func membershipStartDate() -> Date {
        return VitalityPartyMembership.currentVitalityMembershipPeriod()?.effectiveFrom ?? (VitalityPartyMembership.currentVitalityProduct()?.effectiveFrom)!
    }
}

extension VitalityProduct {
    public static func parseAndReturn(_ products: [LoginMembershipProducts]) -> Array<VitalityProduct> {
        let realm = DataProvider.newRealm()
        let currentMembershipProducts = realm.objects(VitalityProduct.self)
        for currentMembershipProduct in currentMembershipProducts {
            realm.delete(currentMembershipProduct)
        }

        var realmObjects = Array<VitalityProduct>()
        for product in products {
            let vitalityProduct = VitalityProduct()
            vitalityProduct.effectiveFrom = Parser.main.parseDateString(product.effectiveFrom)
            vitalityProduct.effectiveTo = Parser.main.parseDateString(product.effectiveTo)
//            vitalityProduct.productKey = product.productKey // TODO:  eg: product.productAttributes[0].productAttributeTypeKey
//            vitalityProduct.productCode = product.productCode // TODO:
//            vitalityProduct.productName = product.productName // TODO:
            vitalityProduct.productKey.value = product.typeKey
            vitalityProduct.productCode = product.typeCode
            vitalityProduct.productName = product.typeName

            if let productApplicabilities = product.productFeatureApplicabilities {
                let productFeatures = VitalityProductFeature.parseAndReturn(productApplicabilities)
                for feature in productFeatures {
                    vitalityProduct.features.append(feature)
                }
            }

            if let productAttributes = product.productAttributes {
                let attributes = VitalityProductAttribute.parseAndReturn(productAttributes)
                for attribute in attributes {
                    vitalityProduct.attributes.append(attribute)
                }
            }

            if let productAssociations = product.productAssociations {
                let associations = VitalityProductAssociations.parseAndReturn(productAssociations)
                for association in associations {
                    vitalityProduct.associations.append(association)
                }
            }

            realmObjects.append(vitalityProduct)
        }
        return realmObjects
    }
}

extension VitalityProductAssociations {
    public static func parseAndReturn(_ associations: [LoginProductAssociations]) -> Array<VitalityProductAssociations> {
        let realm = DataProvider.newRealm()
        let currentProductAssociations = realm.objects(VitalityProduct.self)
        for currentProductAssociation in currentProductAssociations {
            realm.delete(currentProductAssociation)
        }
        var realmObjects = Array<VitalityProductAssociations>()
        for association in associations {
            let productAssociation = VitalityProductAssociations()
            productAssociation.effectiveFrom = Parser.main.parseDateString(association.effectiveFrom)
            productAssociation.effectiveTo = Parser.main.parseDateString(association.effectiveTo)
            //productAssociation.targetProductName = association.targetProductName // TODO: 29 Mar login change
            productAssociation.typeName = association.typeName ?? ""
            productAssociation.typeCode = association.typeCode ?? ""
            productAssociation.typeKey = association.typeKey
            realmObjects.append(productAssociation)
        }
        return realmObjects
    }
}

extension VitalityProductAttribute {
    public static func parseAndReturn(_ attributes: [LoginProductAttributes]) -> Array<VitalityProductAttribute> {
        let realm = DataProvider.newRealm()
        let currentProductAttributes = realm.objects(VitalityProduct.self)
        for currentProductAttribute in currentProductAttributes {
            realm.delete(currentProductAttribute)
        }

        var realmObjects = Array<VitalityProductAttribute>()
        for attribute in attributes {
            let productAttribute = VitalityProductAttribute()
            productAttribute.effectiveFrom = Parser.main.parseDateString(attribute.effectiveFrom)
            productAttribute.effectiveTo = Parser.main.parseDateString(attribute.effectiveTo)
            productAttribute.value = attribute.value
            productAttribute.productAttributeTypeKey = attribute.typeKey //TODO: .productAttributeTypeKey
            productAttribute.productAttributeTypeCode = attribute.typeCode ?? "" // TODO: .productAttributeTypeCode
            productAttribute.productAttributeTypeName = attribute.typeName ?? "" // TODO: .productAttributeTypeName
            realmObjects.append(productAttribute)
        }
        return realmObjects
    }
}

extension VitalityProductFeature {
    public static func parseAndReturn(_ productFeatures: [LoginProductFeatureApplicability]) -> Array<VitalityProductFeature> {
        let realm = DataProvider.newRealm()
        let currentProductsFeaturesApplicabilities = realm.objects(VitalityProductFeature.self)
        for currentProductsFeaturesApplicability in currentProductsFeaturesApplicabilities {
            realm.delete(currentProductsFeaturesApplicability)
        }

        var realmObjects = Array<VitalityProductFeature>()
        for productFeature in productFeatures {
            let vitalityProductFeature = VitalityProductFeature()
            vitalityProductFeature.effectiveFrom = Parser.main.parseDateString(productFeature.effectiveFrom)
            vitalityProductFeature.effectiveTo = Parser.main.parseDateString(productFeature.effectiveTo)

            if let featureLinks = productFeature.featureLinks {
                for featureLink in featureLinks {
                    let vitalityFeatureLink = VitalityProductFeatureLinks.parseAndReturn(featureLink)
                    vitalityProductFeature.featureLinks.append(vitalityFeatureLink)
                }
            }

            if let feature = ProductFeatureRef(rawValue: productFeature.typeKey) {
                vitalityProductFeature.type = feature
            }
            if let featureType = ProductFeatureTypeRef(rawValue: productFeature.featureTypeKey) {
                vitalityProductFeature.productFeatureType = featureType
            }
            vitalityProductFeature.typeCode = productFeature.typeCode ?? ""
            vitalityProductFeature.typeName = productFeature.typeName ?? ""
            realmObjects.append(vitalityProductFeature)
        }
        return realmObjects
    }
}

extension VitalityProductFeatureLinks {
    public static func parseAndReturn(_ link: LoginFeatureLinks) -> VitalityProductFeatureLinks {
        let featureLink = VitalityProductFeatureLinks()
        featureLink.linkedKey.value = link.linkedKey
        featureLink.typeCode = link.typeCode
        featureLink.type = LinkTypeRef(rawValue: link.typeKey) ?? .Unknown
        featureLink.typeName = link.typeName
        return featureLink
    }
}

extension VitalityMembershipParty {
    public static func parseAndReturn(_ parties: [LoginParty]) -> Array<VitalityMembershipParty> {
        let realm = DataProvider.newRealm()
        let currentMembershipParties = realm.objects(VitalityMembershipParty.self)
        for currentMembershipParty in currentMembershipParties {
            realm.delete(currentMembershipParty)
        }

        var realmObjects = Array<VitalityMembershipParty>()
        for party in parties {
            let vitalityParty = VitalityMembershipParty()
            vitalityParty.effectiveFrom = Parser.main.parseDateString(party.effectiveFrom)
            vitalityParty.effectiveTo = Parser.main.parseDateString(party.effectiveTo)
            vitalityParty.id = party.id ?? 0 // TODO:
            vitalityParty.typeCode = party.typeCode ?? ""
            vitalityParty.typeKey = party.typeKey
            realmObjects.append(vitalityParty)
        }
        return realmObjects
    }
}

extension VitalityMembershipRenewalPeriod {
    public static func parseAndReturn(_ periods: [LoginRenewalPeriods]) -> Array<VitalityMembershipRenewalPeriod> {
        let realm = DataProvider.newRealm()
        let membershipRenewalPeriods = realm.objects(VitalityMembershipRenewalPeriod.self)
        for membershipRenewalPeriod in membershipRenewalPeriods {
            realm.delete(membershipRenewalPeriod)
        }
        
        var realmObjects = Array<VitalityMembershipRenewalPeriod>()
        for period in periods {
            let renewalPeriod = VitalityMembershipRenewalPeriod()
            renewalPeriod.effectiveFrom = Parser.main.parseDateString(period.effectiveFrom)
            renewalPeriod.effectiveTo = Parser.main.parseDateString(period.effectiveTo)
            renewalPeriod.period = period.period!
            renewalPeriod.unit = period.unit!
            realmObjects.append(renewalPeriod)
        }
        return realmObjects
    }
}

extension VitalityMembershipReference {
    public static func parseAndReturn(_ references: [LoginMemberReferences]?) -> Array<VitalityMembershipReference> {
        let realm = DataProvider.newRealm()
        let currentMembershipReferences = realm.objects(VitalityMembershipReference.self)
        for currentMembershipReference in currentMembershipReferences {
            realm.delete(currentMembershipReference)
        }

        var realmObjects = Array<VitalityMembershipReference>()
        if let membershipReferences = references {
            for reference in membershipReferences {
                let vitalityReference = VitalityMembershipReference()
                vitalityReference.issuedBy = reference.issuedBy ?? 0
                vitalityReference.typeKey = reference.typeKey
                vitalityReference.typeName = reference.typeName ?? "" // was code
                vitalityReference.value = reference.value ?? ""
                realmObjects.append(vitalityReference)
            }
        }
        return realmObjects
    }
}

extension UserInstruction {
    public static func parseAndPersist(_ userInstructions: [LoginUserInstructions]) {
        let realm = DataProvider.newRealm()
        try! realm.write {
            for userInstruction in userInstructions {
                let newUserInstruction = UserInstruction()
                newUserInstruction.id = userInstruction.id ?? UserInstruction.defaultUserInstructionValue
                newUserInstruction.typeKey = userInstruction.typeKey
                realm.add(newUserInstruction)
            }
        }
    }
}

extension VitalityParty {
    public static func parseAndPersist(_ partyDetails: LoginPartyDetails) {
        let realm = DataProvider.newRealm()
        try! realm.write {
            let currentPartyDetails = realm.objects(VitalityParty.self)
            for party in currentPartyDetails {
                realm.delete(party)
            }
            let newPartyDetail = VitalityParty()

            //TELEPHONE
            let currentTelephones = realm.objects(PartyTelephone.self)
            for telephone in currentTelephones {
                realm.delete(telephone)
            }
            let phones = PartyTelephone.parseAndReturn(partyDetails.telephoneNumbers ?? [LoginTelephoneNumbers]())
            for phone in phones {
                newPartyDetail.telephones.append(phone)
            }

            //EMAILS
            let currentEmailAddresses = realm.objects(PartyEmailAddress.self)
            for emailAddresses in currentEmailAddresses {
                realm.delete(emailAddresses)
            }
            let emails = PartyEmailAddress.parseAndReturn(partyDetails.emailAddresses ?? [LoginEmailAddress]())
            for email in emails {
                newPartyDetail.emailAddresses.append(email)
            }

            //REFERENCES
            let currentReferences = realm.objects(PartyReference.self)
            for currentReference in currentReferences {
                realm.delete(currentReference)
            }
            let references = PartyReference.parseAndReturn(partyDetails.references ?? [LoginReferences]())
            for reference in references {
                newPartyDetail.references.append(reference)
            }

            //GENEREAL PREFERENCES
            let currentGeneralPreferences = realm.objects(PartyGeneralPreference.self)
            for currentGeneralPreference in currentGeneralPreferences {
                realm.delete(currentGeneralPreference)
            }
            let generalPreferences = PartyGeneralPreference.parseAndReturn(partyDetails.generalPreferences ?? [LoginGeneralPreferences]())
            for generalPreference in generalPreferences {
                newPartyDetail.generalPreferences.append(generalPreference)
            }

            //PHYSICAL ADDRESSES
            let currentPhysicalAddresses = realm.objects(PartyPhysicalAddress.self)
            for currentPhysicalAddress in currentPhysicalAddresses {
                realm.delete(currentPhysicalAddress)
            }
            let physicalAddresses = PartyPhysicalAddress.parseAndReturn(partyDetails.physicalAddresses ?? [LoginPhysicalAddress]())
            for address in physicalAddresses {
                newPartyDetail.physicalAddresses.append(address)
            }

            //WEB ADDRESSES
            let currentWebAddresses = realm.objects(PartyWebAddress.self)
            for currentWebAddress in currentWebAddresses {
                realm.delete(currentWebAddress)
            }
            let webAddresses = PartyWebAddress.parseAndReturn(partyDetails.webAddresses ?? [LoginWebAddress]())
            for webAddress in webAddresses {
                newPartyDetail.webAddresses.append(webAddress)
            }

            //PERSON
            let currentPersons = realm.objects(PartyPerson.self)
            for currentPerson in currentPersons {
                realm.delete(currentPerson)
            }
            if let person = partyDetails.person {
                newPartyDetail.person = PartyPerson.parseAndReturn(person)
            }

            //GEOGRAPHICAL PREFERENCES
            let currentgGographicalAreaPreferences = realm.objects(PartyGeographicalAreaPreference.self)
            for currentgGographicalAreaPreference in currentgGographicalAreaPreferences {
                realm.delete(currentgGographicalAreaPreference)
            }
            if let partyDetails = partyDetails.geographicalAreaPreference {
                newPartyDetail.geographicalAreaPreference = PartyGeographicalAreaPreference.parseAndReturn(partyDetails)
            }

            //LANGUAGE
            let currentLanguagePreferences = realm.objects(PartyLanguagePreference.self)
            for currentLanguagePreference in currentLanguagePreferences {
                realm.delete(currentLanguagePreference)
            }
            if let partyDetails = partyDetails.languagePreference {
                newPartyDetail.languagePreference = PartyLanguagePreference.parseAndReturn(partyDetails)
            }

            //TIME ZONE
            let currentTimeZonePreferences = realm.objects(PartyTimeZonePreference.self)
            for currentTimeZonePreference in currentTimeZonePreferences {
                realm.delete(currentTimeZonePreference)
            }
            if let partyDetails = partyDetails.timeZonePreference {
                newPartyDetail.timeZonePreference = PartyTimeZonePreference.parseAndReturn(partyDetails)
            }

            //MEASUREMENT
            let currentMeasurementPreferences = realm.objects(PartyMeasurementSystemPreference.self)
            for currentMeasurementPreference in currentMeasurementPreferences {
                realm.delete(currentMeasurementPreference)
            }
            if let partyDetails = partyDetails.measurementSystemPreference {
                newPartyDetail.measurementPreference = PartyMeasurementSystemPreference.parseAndReturn(partyDetails)
            }

            newPartyDetail.partyId = partyDetails.partyId
            newPartyDetail.accessToken = partyDetails.accessToken ?? "" // TODO:
            newPartyDetail.tenantId = partyDetails.tenantId ?? VitalityParty.defaultIdValue

            realm.add(newPartyDetail)
            
            //PUSHWOOSH
            let userIDsDict:[String: Int] = ["partyId": newPartyDetail.partyId, "tenantId": newPartyDetail.tenantId]
            NotificationCenter.default.post(name: Notification.Name("SetTagsForPushNotification"), object: nil, userInfo: userIDsDict)
            //PUSHWOOSH

        }
    }
}

extension PartyTelephone {
    public static func parseAndReturn(_ telephones: [LoginTelephoneNumbers]) -> Array<PartyTelephone> {
        var realmObjects = Array<PartyTelephone>()
        for phone in telephones {
            let telephone = PartyTelephone()
            telephone.countryDialCode = phone.countryDialCode
            telephone.areaDialCode = phone.areaDialCode ?? ""
            telephone.typeKey = phone.typeKey
            telephone.contactNumber = phone.contactNumber
            telephone.numberExtension = phone.`extension`
            let roles = PartyContactRole.parseAndReturn(phone.contactRoles ?? [LoginContactRoles]())
            for role in roles {
                telephone.contactRoles.append(role)
            }
            realmObjects.append(telephone)
        }
        return realmObjects
    }
}

extension PartyEmailAddress {
    public static func parseAndReturn(_ emails: [LoginEmailAddress]) -> Array<PartyEmailAddress> {
        var realmObjects = Array<PartyEmailAddress>()
        for mail in emails {
            let partyEmail = PartyEmailAddress()

            partyEmail.value = mail.value // TODO:
            let roles = PartyContactRole.parseAndReturn(mail.contactRoles ?? [LoginContactRoles]())
            for role in roles {
                partyEmail.contactRoles.append(role)
            }
            realmObjects.append(partyEmail)
        }
        return realmObjects
    }
}

extension PartyReference {
    public static func parseAndReturn(_ references: [LoginReferences]) -> Array<PartyReference> {
        var realmObjects = Array<PartyReference>()
        for reference in references {
            let partyReference = PartyReference()
            if let issuedBy = reference.issuedBy {
                partyReference.comments = String(issuedBy)
            }
            partyReference.type = String(reference.typeKey)
            partyReference.value = reference.value ?? ""

            // TODO: 29 March login API change
//            if (reference.effectiveFrom != nil) {
//                partyReference.effectiveFrom = Parser.main.parseDateString(reference.effectiveFrom!)
//            }
//            if (reference.effectiveTo != nil) {
//                partyReference.effectiveTo = Parser.main.parseDateString(reference.effectiveTo!)
//            }
            realmObjects.append(partyReference)
        }
        return realmObjects
    }
}

extension PartyGeneralPreference {
    public static func parseAndReturn(_ preferences: [LoginGeneralPreferences]) -> Array<PartyGeneralPreference> {
        var realmObjects = Array<PartyGeneralPreference>()

        let types = Set<Int>(preferences.map({ $0.typeKey }))
        let latestForEachType = types.flatMap { type -> LoginGeneralPreferences? in
            let sorted = preferences.sorted { (lhs, rhs) -> Bool in
                if let lhsDate = Parser.main.parseDateString(lhs.effectiveTo), let rhsDate = Parser.main.parseDateString(rhs.effectiveTo) {
                    return lhsDate > rhsDate
                }
                return false
            }
            return sorted.filter({ $0.typeKey == type }).first
        }

        for preference in latestForEachType {
            let generalPreference = PartyGeneralPreference()

            if let effectiveFrom = preference.effectiveFrom {
                generalPreference.effectiveFrom = Parser.main.parseDateString(effectiveFrom)
            }
            if let effectiveTo = preference.effectiveTo {
                generalPreference.effectiveTo = Parser.main.parseDateString(effectiveTo)
            }
            generalPreference.type = preference.typeKey
            generalPreference.value = preference.value ?? ""
            realmObjects.append(generalPreference)
        }
        return realmObjects
    }
}

extension PartyPhysicalAddress {
    public static func parseAndReturn(_ addresses: [LoginPhysicalAddress]) -> Array<PartyPhysicalAddress> {
        var realmObjects = Array<PartyPhysicalAddress>()
        for address in addresses {
            let physicalAddress = PartyPhysicalAddress()
            physicalAddress.country = address.country ?? ""
            physicalAddress.poBox = address.pOBox ?? false
            physicalAddress.complex = address.complex ?? ""
            physicalAddress.postalCode = String(describing: address.postalCode)
            physicalAddress.streetAddress1 = address.streetAddress1 ?? "" // TODO:
            physicalAddress.streetAddress2 = address.streetAddress2 ?? ""
            physicalAddress.streetAddress3 = address.streetAddress3 ?? ""
            let roles = PartyContactRole.parseAndReturn(address.contactRoles ?? [LoginContactRoles]())
            for role in roles {
                physicalAddress.contactRoles.append(role)
            }
            physicalAddress.place = address.place ?? ""
            realmObjects.append(physicalAddress)
        }
        return realmObjects
    }
}

extension PartyContactRole {
    public static func parseAndReturn(_ contactRoles: [LoginContactRoles]) -> Array<PartyContactRole> {
        var realmObjects = Array<PartyContactRole>()
        for contactRole in contactRoles {
            let phoneContactRole = PartyContactRole()
            phoneContactRole.roleTypeKey = 0 // TODO: contactRole.roleTypeKey
            phoneContactRole.rolePurposeTypeKey = 0 // TODO: contactRole.rolePurposeTypeKey

            phoneContactRole.effectiveFrom = Parser.main.parseDateString(contactRole.effectiveFrom)
            phoneContactRole.effectiveTo = Parser.main.parseDateString(contactRole.effectiveTo)
            realmObjects.append(phoneContactRole)
        }
        return realmObjects
    }
}

extension PartyPerson {
    public static func parseAndReturn(_ person: LoginPerson) -> PartyPerson {
        let party = PartyPerson()
        party.bornOn = Parser.main.parseDateString(person.bornOn)
        party.gender = person.genderTypeName
        party.givenName = person.givenName
        party.familyName = person.familyName
        party.title = person.titleTypeName
        party.preferredName = person.preferredName
        party.suffix = person.suffix
        return party
    }
}

extension PartyWebAddress {
    public static func parseAndReturn(_ webAddresses: [LoginWebAddress]) -> Array<PartyWebAddress> {
        var realmObjects = Array<PartyWebAddress>()
        for webAddress in webAddresses {
            let partyWebAddress = PartyWebAddress()
            partyWebAddress.url = webAddress.uRL
            realmObjects.append(partyWebAddress)
        }
        return realmObjects
    }
}

extension PartyGeographicalAreaPreference {
    public static func parseAndReturn(_ geographicPreference: LoginGeographicalAreaPreference) -> PartyGeographicalAreaPreference {
        let partyGeographicPreference = PartyGeographicalAreaPreference()
        partyGeographicPreference.type = String(geographicPreference.typeKey)
        partyGeographicPreference.value = geographicPreference.value ?? ""
        if let effectiveFrom = geographicPreference.effectiveFrom {
            partyGeographicPreference.effectiveFrom = Parser.main.parseDateString(effectiveFrom)
        }
        if let effectiveTo = geographicPreference.effectiveTo {
            partyGeographicPreference.effectiveTo = Parser.main.parseDateString(effectiveTo)
        }

        return partyGeographicPreference
    }
}

extension PartyLanguagePreference {
    public static func parseAndReturn(_ language: LoginLanguagePreference) -> PartyLanguagePreference {
        let partyLanguagePreference = PartyLanguagePreference()
        partyLanguagePreference.isoCode = language.iSOCode ?? "" // TODO:
        partyLanguagePreference.name = language.value ?? "" // TODO:
        return partyLanguagePreference
    }
}

extension PartyTimeZonePreference {
    public static func parseAndReturn(_ timeZone: LoginTimeZonePreference) -> PartyTimeZonePreference {
        let partyTimeZonePreference = PartyTimeZonePreference()
        partyTimeZonePreference.code = timeZone.code
        partyTimeZonePreference.value = timeZone.value
        partyTimeZonePreference.daylightSavings = String(timeZone.daylightSavings)
        return partyTimeZonePreference
    }
}

extension PartyMeasurementSystemPreference {
    public static func parseAndReturn(_ measurementSystem: LoginMeasurementSystemPreference) -> PartyMeasurementSystemPreference {
        let partyMeasurementSystem = PartyMeasurementSystemPreference()
        partyMeasurementSystem.code = measurementSystem.typeCode ?? ""
        partyMeasurementSystem.name = measurementSystem.typeName ?? ""
        return partyMeasurementSystem
    }
}
