//
//  Parser+DCGetDeviceBenefit.swift
//  VitalityActive
//
//  Created by Von Kervin R. Tuico on 25/04/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import RealmSwift
import VIAUtilities

extension Parser {
    public struct GDC {
        public static func parseGDCDeviceBenefit(response: GetDeviceBenefitResponse, deleteOldData: Bool = true, completion: @escaping (_ error: Error?) -> Void) {
            Parser.main.parse {
                GDCGetDeviceBenefitData.parseGetDeviceBenefit(response: response, completion: completion)
            }
        }
    }
}

extension GDCGetDeviceBenefitData {
    
    public static func parseGetDeviceBenefit(response: GetDeviceBenefitResponse, deleteOldData: Bool = true, completion: @escaping (_ error: Error?) -> Void) {
        let realm = DataProvider.newGDCRealm()
        do {
            try! realm.write {
                
                realm.deleteOldDeviceBenefitData()
                
                let deviceBenefitData = GDCGetDeviceBenefitData()
                
                if let benefit = response.benefit {
                    deviceBenefitData.benefit = GDCBenefit.parseBenefit(with: benefit)
                }
                
                if let goal = response.goal {
                    deviceBenefitData.goal = GDCGoal.parseGoal(with: goal)
                }
                
                if let purchase = response.purchase {
                    deviceBenefitData.purchase = GDCPurchase.parsePurchase(with: purchase)
                }
                
                if let purchaseProduct = response.purchaseProduct {
                    deviceBenefitData.purchaseProduct = GDCPurchaseProduct.parsePurchaseProduct(with: purchaseProduct)
                }
                
                if let qualifyingDeviceses = response.qualifyingDeviceses {
                    for qualifyingDevices in qualifyingDeviceses {
                        deviceBenefitData.qualifyingDeviceses.append(GDCQualifyingDeviceses.parseQualifyingDeviceses(with: qualifyingDevices))
                    }
                }
                realm.add(deviceBenefitData)
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(3)) {
                completion(nil)
            }   
        } catch let error {
            debugPrint("\(error)")
            completion(error)
        }
    }
}

extension GDCBenefit {
    
    public static func parseBenefit(with benefitResponse: DCBenefitResponse) -> GDCBenefit {
        let benefit = GDCBenefit()
        
        benefit.benefitId = benefitResponse.benefitId ?? 0
        benefit.currentBenefitPeriod = benefitResponse.currentBenefitPeriod ?? 0
        benefit.productCode = benefitResponse.productCode
        benefit.productKey = benefitResponse.productKey ?? 0
        benefit.productName = benefitResponse.productName
        benefit.renewalPeriodUnit = benefitResponse.renewalPeriodUnit ?? 0
        
        if let state = benefitResponse.state {
            benefit.state = GDCState.parseState(with: state)
        }
        
        if let productAttributes = benefitResponse.productAttributes {
            for productAttribute in productAttributes {
                benefit.productAttributes.append(GDCProductAttributes.parseProductAttributes(with: productAttribute))
            }
        }
        
        return benefit
    }
}

extension GDCState {
    
    public static func parseState(with stateResponse: DCStateResponse) -> GDCState {
        let state = GDCState()
        
        state.effectiveFrom = stateResponse.effectiveFrom
        state.effectiveTo = stateResponse.effectiveTo
        state.reason = stateResponse.reason
        state.statusTypeCode = stateResponse.statusTypeCode
        state.statusTypeKey = stateResponse.statusTypeKey ?? 0
        state.statusTypeName = stateResponse.statusTypeName
        
        return state
    }
}

extension GDCProductAttributes {
    
    public static func parseProductAttributes(with productAttributesResponse: DCProductAttributesResponse) -> GDCProductAttributes {
        let productAttributes = GDCProductAttributes()
        
        productAttributes.typeCode = productAttributesResponse.typeCode
        productAttributes.typeKey = productAttributesResponse.typeKey ?? 0
        productAttributes.typeName = productAttributesResponse.typeName
        productAttributes.value = productAttributesResponse.value
        
        return productAttributes
    }
}

extension GDCGoal {
    
    public static func parseGoal(with goalResponse: DCGoalResponse) -> GDCGoal {
        let goal = GDCGoal()
        
        goal.key = goalResponse.key ?? 0
        
        if let applicablePointsEntryTypes = goalResponse.applicablePointsEntryTypes {
            for applicablePointsEntryType in applicablePointsEntryTypes {
                goal.applicablePointsEntryTypes.append(GDCApplicablePointsEntryTypes.parseApplicablePointsEntryTypes(with: applicablePointsEntryType))
            }
        }
        
        if let goalTracker = goalResponse.goalTracker {
            goal.goalTracker = GDCGoalTracker.parseGoalTracker(with: goalTracker)
        }
        
        if let levels = goalResponse.levels {
            for level in levels {
                goal.levels.append(GDCLevels.parseLevel(with: level))
            }
        }
        
        if let pointsEntry = goalResponse.pointsEntry {
            for points in pointsEntry {
                goal.pointsEntry.append(GDCPointsEntry.parsePointsEntry(with: points))
            }
        }
        
        return goal
    }
}

extension GDCApplicablePointsEntryTypes {
    
    public static func parseApplicablePointsEntryTypes(with applicablePointsEntryTypesResponse: DCApplicablePointsEntryTypesResponse) -> GDCApplicablePointsEntryTypes {
        let applicablePointsEntryTypes = GDCApplicablePointsEntryTypes()
        
        applicablePointsEntryTypes.eventTypeCode = applicablePointsEntryTypesResponse.eventTypeCode
        applicablePointsEntryTypes.eventTypeKey = applicablePointsEntryTypesResponse.eventTypeKey ?? 0
        applicablePointsEntryTypes.eventTypeName = applicablePointsEntryTypesResponse.eventTypeName
        applicablePointsEntryTypes.pointsEntryTypeCode = applicablePointsEntryTypesResponse.pointsEntryTypeCode
        applicablePointsEntryTypes.pointsEntryTypeKey = applicablePointsEntryTypesResponse.pointsEntryTypeKey ?? 0
        applicablePointsEntryTypes.pointsEntryTypeName = applicablePointsEntryTypesResponse.pointsEntryTypeName
        
        return applicablePointsEntryTypes
    }
}

extension GDCGoalTracker {
    
    public static func parseGoalTracker(with goalTrackerResponse: DCGoalTrackerResponse) -> GDCGoalTracker {
        let goalTracker = GDCGoalTracker()
        
        goalTracker.completedObjectives = goalTrackerResponse.completedObjectives ?? 0
        goalTracker.effectiveFrom = goalTrackerResponse.effectiveFrom
        goalTracker.effectiveTo = goalTrackerResponse.effectiveTo
        goalTracker.monitorUntil = goalTrackerResponse.monitorUntil
        goalTracker.percentageCompleted = goalTrackerResponse.percentageCompleted ?? 0
        goalTracker.statusChangedOn = goalTrackerResponse.statusChangedOn
        goalTracker.statusCode = goalTrackerResponse.statusCode
        goalTracker.statusKey = goalTrackerResponse.statusKey ?? 0
        goalTracker.statusName = goalTrackerResponse.statusName
        goalTracker.totalObjectives = goalTrackerResponse.totalObjectives ?? 0
        
        return goalTracker
    }
}

extension GDCLevels {
    
    public static func parseLevel(with levelsResponse: DCLevelsResponse) -> GDCLevels {
        let levels = GDCLevels()
        
        levels.isAchieved = levelsResponse.isAchieved ?? false
        levels.isHighestLevelAchieved = levelsResponse.isHighestLevelAchieved ?? false
        levels.level = levelsResponse.level ?? 0
        levels.maximumPoints = levelsResponse.maximumPoints ?? 0
        levels.minimumPoints = levelsResponse.minimumPoints ?? 0
        levels.objectiveKey = levelsResponse.objectiveKey ?? 0
        levels.pointsAchieved = levelsResponse.pointsAchieved ?? 0
        
        if let reward = levelsResponse.reward {
            levels.reward = GDCReward.parseReward(with: reward)
        }
        
        return levels
    }
}

extension GDCReward {
    
    public static func parseReward(with rewardResponse: DCRewardResponse) -> GDCReward {
        let reward = GDCReward()
        
        reward.accumulatedAmount = rewardResponse.accumulatedAmount
        reward.accumulatedPercentage = rewardResponse.accumulatedPercentage ?? 0
        reward.accumulatedQuantity = rewardResponse.accumulatedQuantity ?? 0
        reward.id = rewardResponse.id ?? 0
        reward.typeCode = rewardResponse.typeCode
        reward.typeKey = rewardResponse.typeKey ?? 0
        reward.typeName = rewardResponse.typeName
        
        return reward
    }
}

extension GDCPointsEntry {
    
    public static func parsePointsEntry(with pointsEntryResponse: DCPointsEntryResponse) -> GDCPointsEntry {
        let pointsEntry = GDCPointsEntry()
        
        pointsEntry.activity = pointsEntryResponse.activity
        pointsEntry.effectiveDate = pointsEntryResponse.effectiveDate
        pointsEntry.pointsContributed = pointsEntryResponse.pointsContributed ?? 0
        pointsEntry.typeCode = pointsEntryResponse.typeCode
        pointsEntry.typeKey = pointsEntryResponse.typeKey ?? 0
        pointsEntry.typeName = pointsEntryResponse.typeName
        
        if let pointsEntryMetadatas = pointsEntryResponse.pointsEntryMetadatas {
            for pointsEntryMetadata in pointsEntryMetadatas {
                pointsEntry.pointsEntryMetadatas.append(GDCPointsEntryMetadatas.parsePointsEntryMetadatas(with: pointsEntryMetadata))
            }
        }
        
        return pointsEntry
    }
}

extension GDCPointsEntryMetadatas {
    
    public static func parsePointsEntryMetadatas(with pointsEntryMetadatasResponse: DCPointsEntryMetadatasResponse) -> GDCPointsEntryMetadatas {
        let pointsEntryMetadatas = GDCPointsEntryMetadatas()
        
        pointsEntryMetadatas.typeCode = pointsEntryMetadatasResponse.typeCode
        pointsEntryMetadatas.typeKey = pointsEntryMetadatasResponse.typeKey ?? 0
        pointsEntryMetadatas.typeName = pointsEntryMetadatasResponse.typeName
        pointsEntryMetadatas.unitOfMeasure = pointsEntryMetadatasResponse.unitOfMeasure
        pointsEntryMetadatas.value = pointsEntryMetadatasResponse.value
        
        return pointsEntryMetadatas
    }
}

extension GDCPurchase {
    
    public static func parsePurchase(with purchaseResponse: DCPurchaseResponse) -> GDCPurchase {
        let purchase = GDCPurchase()
        
        purchase.deliveryDate = purchaseResponse.deliveryDate
        purchase.id = purchaseResponse.id ?? 0
        purchase.purchaseDate = purchaseResponse.purchaseDate
        
        if let purchaseAmount = purchaseResponse.purchaseAmount {
            for amount in purchaseAmount {
                purchase.purchaseAmount.append(GDCPurchaseAmount.parsePurchaseAmount(with: amount))
            }
        }
        
        if let purchaseItem = purchaseResponse.purchaseItem {
            purchase.purchaseItem = GDCPurchaseItem.parsePurchaseItem(with: purchaseItem)
        }
        
        if let purchaseReference = purchaseResponse.purchaseReference {
            for reference in purchaseReference {
                purchase.purchaseReference.append(GDCPurchaseReference.parsePurchaseReference(with: reference))
            }
        }
        
        return purchase
    }
}

extension GDCPurchaseAmount {
    
    public static func parsePurchaseAmount(with purchaseAmountResponse: DCPurchaseAmountResponse) -> GDCPurchaseAmount {
        let purchaseAmount = GDCPurchaseAmount()
        
        purchaseAmount.typeCode = purchaseAmountResponse.typeCode
        purchaseAmount.typeKey = purchaseAmountResponse.typeKey ?? 0
        purchaseAmount.typeName = purchaseAmountResponse.typeName
        purchaseAmount.value = purchaseAmountResponse.value
        
        return purchaseAmount
    }
}

extension GDCPurchaseItem {
    
    public static func parsePurchaseItem(with purchaseItemResponse: DCPurchaseItemResponse) -> GDCPurchaseItem {
        let purchaseItem = GDCPurchaseItem()
        
        purchaseItem.itemDescription = purchaseItemResponse.description
        purchaseItem.productCode = purchaseItemResponse.productCode
        purchaseItem.productKey = purchaseItemResponse.productKey ?? 0
        purchaseItem.productName = purchaseItemResponse.productName
        
        if let purchaseAmount = purchaseItemResponse.purchaseAmount {
            for amount in purchaseAmount {
                purchaseItem.purchaseAmount.append(GDCPIPurchaseAmount.parsePIPurchaseAmount(with: amount))
            }
        }
        
        if let purchaseReference = purchaseItemResponse.purchaseReference {
            for reference in purchaseReference {
                purchaseItem.purchaseReference.append(GDCPIPurchaseReference.parsePIPurchaseReference(with: reference))
            }
        }
        
        return purchaseItem
    }
}

extension GDCPIPurchaseAmount {
    
    public static func parsePIPurchaseAmount(with purchaseAmountResponse: DCPIPurchaseAmountResponse) -> GDCPIPurchaseAmount {
        let purchaseAmount = GDCPIPurchaseAmount()
        
        purchaseAmount.typeCode = purchaseAmountResponse.typeCode
        purchaseAmount.typeKey = purchaseAmountResponse.typeKey ?? 0
        purchaseAmount.typeName = purchaseAmountResponse.typeName
        purchaseAmount.value = purchaseAmountResponse.value
        
        return purchaseAmount
    }
}

extension GDCPIPurchaseReference {
    
    public static func parsePIPurchaseReference(with purchaseReferenceResponse: DCPIPurchaseReferenceResponse) -> GDCPIPurchaseReference {
        let purchaseReference = GDCPIPurchaseReference()
        
        purchaseReference.typeCode = purchaseReferenceResponse.typeCode
        purchaseReference.typeKey = purchaseReferenceResponse.typeKey ?? 0
        purchaseReference.typeName = purchaseReferenceResponse.typeName
        purchaseReference.value = purchaseReferenceResponse.value
        
        return purchaseReference
    }
}

extension GDCPurchaseReference {
    
    public static func parsePurchaseReference(with purchaseReferenceResponse: DCPurchaseReferenceResponse) -> GDCPurchaseReference {
        let purchaseReference = GDCPurchaseReference()
        
        purchaseReference.typeCode = purchaseReferenceResponse.typeCode
        purchaseReference.typeKey = purchaseReferenceResponse.typeKey ?? 0
        purchaseReference.typeName = purchaseReferenceResponse.typeName
        purchaseReference.value = purchaseReferenceResponse.value
        
        return purchaseReference
    }
}

extension GDCPurchaseProduct {
    
    public static func parsePurchaseProduct(with purchaseProductResponse: DCPurchaseProductResponse) -> GDCPurchaseProduct {
        let purchaseProduct = GDCPurchaseProduct()
        
        purchaseProduct.productCode = purchaseProductResponse.productCode
        purchaseProduct.productKey = purchaseProductResponse.productKey ?? 0
        purchaseProduct.productName = purchaseProductResponse.productName
        
        if let productAttributes = purchaseProductResponse.productAttributes {
            for productAttribute in productAttributes {
                purchaseProduct.productAttributes.append(GDCPPProductAttributes.parsePPProductAttributes(with: productAttribute))
            }
        }
        
        return purchaseProduct
    }
}

extension GDCPPProductAttributes {
    
    public static func parsePPProductAttributes(with productAttributesResponse: DCPPProductAttributesResponse) -> GDCPPProductAttributes {
        let productAttributes = GDCPPProductAttributes()
        
        productAttributes.typeCode = productAttributesResponse.typeCode
        productAttributes.typeKey = productAttributesResponse.typeKey ?? 0
        productAttributes.typeName = productAttributesResponse.typeName
        productAttributes.value = productAttributesResponse.value
        
        return productAttributes
    }
}

extension GDCQualifyingDeviceses {
    
    public static func parseQualifyingDeviceses(with qualifyingDevicesesResponse: DCQualifyingDevicesesResponse) -> GDCQualifyingDeviceses {
        let qualifyingDeviceses = GDCQualifyingDeviceses()
        
        qualifyingDeviceses.linked = qualifyingDevicesesResponse.linked ?? false
        qualifyingDeviceses.productCode = qualifyingDevicesesResponse.productCode ?? ""
        qualifyingDeviceses.productKey = qualifyingDevicesesResponse.productKey ?? 0
        qualifyingDeviceses.productName = qualifyingDevicesesResponse.productName ?? ""
        
        if let productAttributes = qualifyingDevicesesResponse.productAttributes {
            for productAttribute in productAttributes {
                qualifyingDeviceses.productAttributes.append(GDCQDProductAttributes.parseQDProductAttributes(with: productAttribute))
            }
        }
        
        return qualifyingDeviceses
    }
}

extension GDCQDProductAttributes {
    
    public static func parseQDProductAttributes(with productAttributesResponse: DCQDProductAttributesResponse) -> GDCQDProductAttributes {
        let productAttributes = GDCQDProductAttributes()
        
        productAttributes.typeCode = productAttributesResponse.typeCode ?? ""
        productAttributes.typeKey = productAttributesResponse.typeKey ?? 0
        productAttributes.typeName = productAttributesResponse.typeName ?? ""
        productAttributes.value = productAttributesResponse.value ?? ""
        
        return productAttributes
    }
}

extension Realm {
    public func deleteOldDeviceBenefitData() {
        // Create deletion..
        let getDeviceBenefitData = self.allGDCGetDeviceBenefitData()
        self.delete(getDeviceBenefitData)
        let benefit = self.allGDCBenefit()
        self.delete(benefit)
    }
}
