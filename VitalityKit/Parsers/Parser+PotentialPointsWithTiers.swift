//
//  Parser+PotentialPointsWithTiers.swift
//  VitalityActive
//
//  Created by admin on 2017/06/23.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import RealmSwift
import VIAUtilities

extension Parser {

    public struct PotentialPointsWithTiers {
        public static func parsePotentialPointsWithTiers(response: PotentialPointsByEventType, completion: @escaping (_ error: Error?) -> Void) {
                Parser.main.parse {
                PotentialPointsWithTiersEventType.persistEventOuts(response: response)
                DispatchQueue.main.async {
                    completion(nil)
                }
            }
        }
    }
}

extension PotentialPointsWithTiersEventType {
    public static func persistEventOuts(response: PotentialPointsByEventType) {
        let realm = DataProvider.newRealm()
        var eventsOut = Array<PotentialPointsWithTiersEventType>()
        for responseEventType in response.eventType ?? [] {
            let eventOut = PotentialPointsWithTiersEventType()
            eventOut.typeCode = responseEventType.typeCode
            eventOut.typeKey.value = responseEventType.typeKey
            eventOut.typeName = responseEventType.typeName
            eventOut.totalPotentialPoints.value = responseEventType.totalPotentialPoints
            for pointsDetail in responseEventType.potentialPointsEntries ?? [] {
                eventOut.potentialPointsEntries.append(PotentialPointsWithTiersPotentialPointsEntry.parsePointsEntry(response: pointsDetail))
            }
            eventsOut.append(eventOut)
        }
        try! realm.write {
            realm.add(eventsOut)
        }
        setCoreCache()
        NotificationCenter.default.post(name: .VIAPotentialPointsWithTiersReceived, object: nil)
    }

    static func setCoreCache() {
        Realm.deleteCurrentCoreCache()
        let realm = DataProvider.newRealm()
        try! realm.write {
            let coreCache = CoreCache()
            realm.add(coreCache)
        }
    }
}


extension PotentialPointsWithTiersPotentialPointsEntry {
    public static func parsePointsEntry(response: PPETPotentialPointsEntry) -> PotentialPointsWithTiersPotentialPointsEntry {
        let pointsDetail = PotentialPointsWithTiersPotentialPointsEntry()
        pointsDetail.typeCode = response.typeCode
        pointsDetail.typeKey.value = response.typeKey
        pointsDetail.typeName = response.typeName
        pointsDetail.potentialPointsValue.value = response.potentialPointsValue

        for potentialDetail in response.pointsEntryDetails ?? [] {
            pointsDetail.pointsDetails.append(PotentialPointsWithTiersEntryDetails.parsePotentialPointsDetail(response: potentialDetail))
        }

        return pointsDetail
    }
}


extension PotentialPointsWithTiersEntryDetails {
    static func parsePotentialPointsDetail(response: PPETPointsEntryDetails) -> PotentialPointsWithTiersEntryDetails {
        let potentialPointsDetails = PotentialPointsWithTiersEntryDetails()
        potentialPointsDetails.potentialPoints.value = response.potentialPoints

        for condition in response.conditions ?? [] {
            potentialPointsDetails.conditions.append(PotentialPointsWithTiersConditions.parsePotentialPointsConditions(response: condition))
        }
        return potentialPointsDetails
    }
}

extension PotentialPointsWithTiersConditions {
    static func parsePotentialPointsConditions(response: PPETConditions) -> PotentialPointsWithTiersConditions {
        let potentialPointsConditions = PotentialPointsWithTiersConditions()
        potentialPointsConditions.lessThan.value = response.lessThan
        potentialPointsConditions.greaterThan.value = response.greaterThan
        potentialPointsConditions.lessThanOrEqual.value = response.lessThanOrEqualTo
        potentialPointsConditions.greaterThanOrEqual.value = response.greaterThanOrEqualTo
        potentialPointsConditions.metaTypeCode = response.metadataTypeCode
        potentialPointsConditions.metaTypeKey.value = response.metadataTypeKey
        potentialPointsConditions.metaTypeName = response.metadataTypeName
        if let unitOfMeasure = response.unitOfMeasure {
            potentialPointsConditions.UOM = UnitOfMeasureRef(rawValue: Int(unitOfMeasure) ?? UnitOfMeasureRef.Unknown.rawValue) ?? UnitOfMeasureRef.Unknown
        }
        return potentialPointsConditions
    }
}
