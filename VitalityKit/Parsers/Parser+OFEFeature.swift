//
//  Parser+OFEFeature.swift
//  VitalityActive
//
//  Created by Val Tomol on 16/04/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import RealmSwift
import VIAUtilities

extension Parser {
    public struct OFE {
        public static func parseOFEFeature(response: GetOFEFeatureResponse, deleteOldData:Bool = true, completion: @escaping (_ error: Error?) -> Void) {
            Parser.main.parse {
                OFEEvent.persistGetOFEFeatures(response: response, deleteOldData: deleteOldData, completion: completion)
            }
        }
    }
}

extension OFEEvent {
    public static func persistGetOFEFeatures(response: GetOFEFeatureResponse, deleteOldData:Bool = true, completion: @escaping (_ error: Error?) -> Void) {
        let ofeRealm = DataProvider.newOFERealm()
        let ofeEventTypes = response.getOfeFeatureResponse?.ofeEventTypeses
        
        try? ofeRealm.write {
            if deleteOldData {
                ofeRealm.deleteOldOFEData()
            }
            
            if let eventTypes = ofeEventTypes {
                for eventType in eventTypes {
                    let eventTypeModel = OFEEventType()
                    eventTypeModel.eventTypeCode = eventType.eventTypeCode ?? ""
                    eventTypeModel.eventTypeKey = eventType.eventTypeKey ?? 0 // TODO: Transfer to a TypeRef file
                    eventTypeModel.eventTypeName = eventType.eventTypeName ?? ""
                    eventTypeModel.featureCode = eventType.featureCode ?? ""
                    eventTypeModel.featureKey = eventType.featureKey ?? 0 // TODO: Transfer to a TypeRef file
                    eventTypeModel.featureName = eventType.featureName ?? ""
                    eventTypeModel.potentialPoints = eventType.potentialPoints ?? 0
                    
                    if let eventDetails = eventType.eventDetails {
                        for eventDetail in eventDetails {
                            let eventDetailModel = OFEEventDetail()
                            eventDetailModel.metadataCode = eventDetail.metadataCode ?? ""
                            eventDetailModel.metadataKey = OFEFeatureTypeRef(rawValue: eventDetail.metadataKey ?? -1) ?? .Unknown
                            eventDetailModel.metadataName = eventDetail.metadataName ?? ""
                            
                            if let selectionGroups = eventDetail.selectionGroups {
                                for selectionGroup in selectionGroups {
                                    let selectionGroupModel = OFESelectionGroup()
                                    selectionGroupModel.categoryCode = selectionGroup.categoryCode ?? ""
                                    selectionGroupModel.categoryKey = OFEFeatureTypeRef(rawValue: selectionGroup.categoryKey ?? -1) ?? .Unknown
                                    selectionGroupModel.categoryName = selectionGroup.categoryName ?? ""
                                    selectionGroupModel.featureCode = selectionGroup.featureCode ?? ""
                                    selectionGroupModel.featureKey = selectionGroup.featureKey ?? 0 // TODO: Transfer to a TypeRef file.
                                    selectionGroupModel.featureName = selectionGroup.featureName ?? ""
                                    
                                    if let selectionOptions = selectionGroup.selectionOptions {
                                        for selectionOption in selectionOptions {
                                            let selectionOptionModel = OFESelectionOption()
                                            selectionOptionModel.featureCode = selectionOption.featureCode ?? ""
                                            selectionOptionModel.featureKey = selectionOption.featureKey ?? 0 // TODO: Transfer to a TypeRef file.
                                            selectionOptionModel.featureName = selectionOption.featureName ?? ""
                                            selectionOptionModel.potentialPoints = selectionOption.potentialPoints ?? 0
                                            
                                            selectionGroupModel.selectionOptions.append(selectionOptionModel)
                                        }
                                    }
                                    eventDetailModel.selectionGroups.append(selectionGroupModel)
                                }
                            }
                            eventTypeModel.eventDetails.append(eventDetailModel)
                        }
                    }
                    ofeRealm.create(OFEEventType.self, value: eventTypeModel, update: true)
                }
            }
        }
        DispatchQueue.main.async {
            completion(nil)
        }
    }
}

extension Realm {
    public func deleteOldOFEData() {
        // Create deletion..
    }
}
