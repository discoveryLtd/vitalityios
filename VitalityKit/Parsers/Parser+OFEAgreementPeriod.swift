//
//  Parser+EventsPointsHistory.swift
//  VitalityActive
//
//  Created by Harry C. Lingad on 4/18/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import RealmSwift
import VIAUtilities

extension Parser {
    public struct AgreementPeriod {
        
        public static func parseAgreementPeriodHistory(response: OFEGetAgreementPeriodResponse, completion: @escaping (_ result: Any?, _ error: Error?, _ useCustomError: Bool?) -> Void) {
            Parser.main.parse {
                let agreementPeriodResult = OFEAgreementPeriod.persistAgreementPeriod(response: response)
                DispatchQueue.main.async {
                    completion(agreementPeriodResult, nil, nil)
                }
            }
        }
    }
}

extension OFEAgreementPeriod {
    
    public static func persistAgreementPeriod(response: OFEGetAgreementPeriodResponse) -> OFEAgreementPeriod? {
        
        let realm = DataProvider.newOFERealm()
        
        let agreementPeriods = response.agreementPeriods
        debugPrint("Parsing \(String(describing: agreementPeriods)) number of points accounts")
        
        try! realm.write {
            //Clear all old data before adding new pointsEntry data to prevent duplicates
            //            let oldAssociatedEvent = realm.allAssociatedEvent()
            //            realm.delete(oldAssociatedEvent)
            //            let oldAllEventExternalReference = realm.allEventExternalReference()
            //            realm.delete(oldAllEventExternalReference)
            //            let oldAllEventMetadata = realm.allEventMetadata()
            //            realm.delete(oldAllEventMetadata)
            //            let oldAllEventSource = realm.allEventSource()
            //            realm.delete(oldAllEventSource)
            
            let agreementPeriodModel = OFEAgreementPeriod()
            agreementPeriodModel.id = 1
            
            for effectivePeriod in agreementPeriods?.effectivePeriods ?? [EffectivePeriod](){
                
                let effectivePeriodModel = OFEEffectivePeriod()
                effectivePeriodModel.effectiveFrom = effectivePeriod.effectiveFrom
                effectivePeriodModel.effectiveTo = effectivePeriod.effectiveTo
                
                agreementPeriodModel.effectivePeriods.append(effectivePeriodModel)
            }
            
            realm.create(OFEAgreementPeriod.self, value: agreementPeriodModel, update: true)
            
            
            //MARK: END OF OFEEventSource
            //realm.add(agreementPeriods)
        }
        
        //TODO:- Do member number lookup and return the relevant account based on member number (member number still needs to be persisted along with account)
        return realm.objects(OFEAgreementPeriod.self).first
    }
}

