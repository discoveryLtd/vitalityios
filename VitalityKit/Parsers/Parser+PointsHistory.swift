import Foundation
import RealmSwift
import VIAUtilities

extension Parser {
    public struct PointsMonitor {

        public static func parsePointsHistory(response: GetAllPointsHistoryResponse, completion: @escaping (_ result: Any?, _ error: Error?, _ useCustomError: Bool?) -> Void) {
            Parser.main.parse {
                let pointsAccountResult = PointsAccount.persistPointsHistoryEvents(response: response)
                DispatchQueue.main.async {
                    completion(pointsAccountResult, nil, nil)
                }
            }
        }
    }
}

extension PointsAccount {

    public static func persistPointsHistoryEvents(response: GetAllPointsHistoryResponse) -> PointsAccount? {

        let realm = DataProvider.newRealm()

        let pointsAccounts = response.pointsAccounts
        debugPrint("Parsing \(String(describing: pointsAccounts?.count)) number of points accounts")
        try! realm.write {
            //Clear all old data before adding new pointsEntry data to prevent duplicates
            let oldPointEntries = realm.allPointsEntries()
            realm.delete(oldPointEntries)
            let oldReasons = realm.allReasons()
            realm.delete(oldReasons)
            let oldMetadata = realm.allPointsEntryMetadatas()
            realm.delete(oldMetadata)
            let oldTrackerEvents = realm.allActiveRewardsProgressTrackerEvents()
            realm.delete(oldTrackerEvents)
            let oldPointsAccounts = realm.allPointsAccounts()
            realm.delete(oldPointsAccounts)

            for pointsAccount in pointsAccounts ?? [APHPointsAccounts]() {
                let pointsAccountModel = PointsAccount()

                pointsAccountModel.carryOverPoints = pointsAccount.carryOverPoints ?? 0
                pointsAccountModel.effectiveFrom = Parser.main.parseDateString(pointsAccount.effectiveFrom)
                pointsAccountModel.effectiveTo = Parser.main.parseDateString(pointsAccount.effectiveTo)
                pointsAccountModel.pointsTotal = pointsAccount.pointsTotal 

                // debugPrint("Parsing \(pointsAccount.pointsEntries.count) number of points entries")
                for pointsEntry in pointsAccount.pointsEntries ?? [] {
                    let pointsEntryModel = PointsEntry()
                    let categoryKey = pointsEntry.categoryKey
                    pointsEntryModel.categoryKey = PointsEntryCategoryRef(rawValue: categoryKey) ?? .Unknown
                    pointsEntryModel.categoryCode = pointsEntry.categoryCode ?? ""
                    pointsEntryModel.categoryName = pointsEntry.categoryName
                    pointsEntryModel.earnedValue = pointsEntry.earnedValue
                    
                    
                    /* Check if the tenant is SLI, UKE, EC or ASR */
                    if let tenantID = AppSettings.getAppTenant() {
                        if tenantID == .SLI || tenantID == .UKE || tenantID == .EC || tenantID == .ASR {
                            pointsEntryModel.effectiveDate = Parser.main.parsePointsHistoryEffectiveDate(pointsEntry.effectiveDate, fallback: pointsEntryModel.effectiveDate, ignoreTimezone: (tenantID == .EC || tenantID == .ASR))
                        } else {
                            pointsEntryModel.effectiveDate = Parser.main.parseDateString(pointsEntry.effectiveDate, fallback: pointsEntryModel.effectiveDate)
                        }
                    }
                    
                    pointsEntryModel.eventId = pointsEntry.eventId 
                    pointsEntryModel.id = pointsEntry.id 
                    pointsEntryModel.party = pointsEntry.party ?? 0
                    pointsEntryModel.potentialValue = pointsEntry.potentialValue 
                    pointsEntryModel.prelimitValue = pointsEntry.prelimitValue 
                    pointsEntryModel.typeKey = PointsEntryTypeRef.Unknown // TODO:
                    pointsEntryModel.typeCode = pointsEntry.typeCode ?? ""
                    pointsEntryModel.typeName = pointsEntry.typeName ?? ""

                    for reason in pointsEntry.reasons ?? [] {
                        let reasonModel = PointsEntryReason()
                        reasonModel.name = reason.reasonName ?? ""
                        reasonModel.key = reason.reasonKey // TODO:
                        reasonModel.categoryCode = reason.categoryCode ?? ""
                        reasonModel.categoryKey = reason.categoryKey // TODO:
                        pointsEntryModel.reasons.append(reasonModel)
                    }

                    for metadata in pointsEntry.metadatas ?? [] {
                        let metadataModel = PointsEntryMetadata()
                        metadataModel.typeCode = metadata.typeCode
                        metadataModel.typeName = metadata.typeName
                        metadataModel.typeKey = EventMetaDataTypeRef(rawValue: metadata.typeKey) ?? EventMetaDataTypeRef.Unknown
                        let unitOfMeasure = metadata.unitOfMeasure ?? String(UnitOfMeasureRef.Unknown.rawValue)
                        let uomRawValue = Int(unitOfMeasure) ?? UnitOfMeasureRef.Unknown.rawValue
                        metadataModel.unitOfMeasureType = UnitOfMeasureRef(rawValue: uomRawValue) ?? UnitOfMeasureRef.Unknown
                        metadataModel.unitOfMeasure = metadata.unitOfMeasure
                        metadataModel.value = metadata.value 
                        pointsEntryModel.metadatas.append(metadataModel)
                    }

                    for goalPointTracker in pointsEntry.goalPointsTrackers ?? [APHGoalPointsTrackers]() {
                        let trackerEventModel = ActiveRewardsProgressTrackerEvent()
                        trackerEventModel.goalTypeCode = goalPointTracker.goalCode ?? ""
                        trackerEventModel.goalTypeKey = goalPointTracker.goalKey // TODO:
                        trackerEventModel.points = goalPointTracker.pointsContributing // TODO:
                        pointsEntryModel.activeRewardsProgressTrackerEvents.append(trackerEventModel)
                    }

                    realm.create(PointsEntry.self, value: pointsEntryModel, update: true)
                }
                realm.add(pointsAccountModel)
            }
        }
        //TODO:- Do member number lookup and return the relevant account based on member number (member number still needs to be persisted along with account)
        return realm.objects(PointsAccount.self).first
    }
}
