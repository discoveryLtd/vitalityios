//
//  Parser+Rewards.swift
//  VitalityActive
//
//  Created by admin on 2017/11/14.
//  Copyright © 2017 Glucode. All rights reserved.
//

import UIKit
import VIAUtilities

extension Parser {
    public struct rewards {
        public static func parseProductFeaturesPoints(response: GetAwardedRewardResponse, resetRewards: Bool, completion: @escaping (_ error: Error?) -> Void) {
            Parser.main.parse {
                if resetRewards {
                    Parser.resetARRewards()
                }
                for awardedReward in response.awardedRewards ?? [] {
                    let sortedRewardStatuses = awardedReward.awardedRewardStatuses?.sorted(by: { (first, second) -> Bool in
                        first.effectiveOn < second.effectiveOn
                    })
                    let currentStatus = sortedRewardStatuses?.first
                    /** FC-26793 : UKE CR Interim Voucher Solution
                     * Added Reward Keys 36, 41, 46, 47 & 48 for the new rewards
                     */
                    if (currentStatus?.key == AwardedRewardStatusRef.Allocated.rawValue ||
                        currentStatus?.key == AwardedRewardStatusRef.Acknowledged.rawValue ||
						currentStatus?.key == AwardedRewardStatusRef.AvailableToRedeem.rawValue ||
						currentStatus?.key == AwardedRewardStatusRef.Used.rawValue) &&
                        (awardedReward.reward.key == 1 || awardedReward.reward.key == 7 || awardedReward.reward.key == 36
                            || awardedReward.reward.key == 41 || awardedReward.reward.key == 46
                            || awardedReward.reward.key == 47 || awardedReward.reward.key == 48
                            || awardedReward.reward.key == 49) {
                        _ = ARUnclaimedReward.parseAndPersistARRewardsVoucher(response: awardedReward)
                    } else if (currentStatus?.key == AwardedRewardStatusRef.Issued.rawValue ||
                        currentStatus?.key == AwardedRewardStatusRef.Allocated.rawValue ||
                        currentStatus?.key == AwardedRewardStatusRef.PartnerRegistration.rawValue ||
                        currentStatus?.key == AwardedRewardStatusRef.AvailableToRedeem.rawValue ||
						currentStatus?.key == AwardedRewardStatusRef.Used.rawValue ||
                        currentStatus?.key == AwardedRewardStatusRef.Expired.rawValue ||
                        currentStatus?.key == AwardedRewardStatusRef.Canceled.rawValue ||
                        currentStatus?.key == AwardedRewardStatusRef.IssueFailed.rawValue)  &&
                        !(awardedReward.reward.key == 1 || awardedReward.reward.key == 7 || awardedReward.reward.key == 36
                            || awardedReward.reward.key == 41 || awardedReward.reward.key == 46
                            || awardedReward.reward.key == 47 || awardedReward.reward.key == 48
                            || awardedReward.reward.key == 49) {
                        _ = ARVoucher.parseAndPersistARRewardsVoucher(response: awardedReward)
                    }
                }
                DispatchQueue.main.async {
                    completion(nil)
                }

            }
        }
        
        public static func parseAwardedReward(response: GEARAwardedRewards, resetRewards: Bool, completion: @escaping (_ error: Error?) -> Void) {
            Parser.main.parse {
                if let rewardSelections = response.rewardSelectionType?.rewardSelections, rewardSelections.count > 0 {
                    _ = ARUnclaimedReward.parseAndPersistARRewardsVoucher(response: response)
                } else {
                    _ = ARVoucher.parseAndPersistARRewardsVoucher(response: response)
                }
                DispatchQueue.main.async {
                    return completion(nil)
                }
            }
        }
    }
    
    internal static func resetARRewards() {
        do {
            let arRealm = DataProvider.newARRealm()
            let vouchers = arRealm.objects(ARVoucher.self)
            let unclaimedRewards = arRealm.objects(ARUnclaimedReward.self)
            let rewardSelections = arRealm.objects(ARRewardsSelection.self)
            let voucherCodes = arRealm.objects(ARVoucherCode.self)
            let partnerSysIds = arRealm.objects(ARPartnerSysRewardId.self)
            
            try arRealm.write {
                arRealm.delete(vouchers)
                arRealm.delete(unclaimedRewards)
                arRealm.delete(rewardSelections)
                arRealm.delete(voucherCodes)
                arRealm.delete(partnerSysIds)
            }
        } catch {
            print("Unable to delete ")
        }
    }
}

extension ARVoucher {
    public static func parseAndPersistARRewardsVoucher(response: GEARAwardedRewards) -> Error? {
        let voucher: ARVoucher = ARVoucher()
        let arRealm = DataProvider.newARRealm()
        voucher.agreementPartyId = response.agreementPartyId
        voucher.awardedOn = Parser.main.parseDateString(response.awardedOn)
        if let voucherNumbers = response.awardedRewardReferences?.filter({ $0.typeKey == AwardedRewardReferenceTypeRef.VoucherNumber.rawValue }) {
            for voucherNumber in voucherNumbers {
                    let voucherCode = ARVoucherCode()
                    voucherCode.value = voucherNumber.value
                    voucher.awardedRewardReferenceVoucherNumbers.append(voucherCode)
            }
            do {
                try arRealm.write {
                    for voucherCode in voucher.awardedRewardReferenceVoucherNumbers {
                        if let codeValue = voucherCode.value, let code = arRealm.voucherCode(with: codeValue).first {
                            arRealm.delete(code)
                        }
                    }
                }
            } catch {
                debugPrint("Unable to delete old voucher codes")
            }
        }
        
        if let partnerSysRewardIds = response.awardedRewardReferences?.filter({ $0.typeKey == AwardedRewardReferenceTypeRef.PartnerSysRewardID.rawValue }) {
            
            for partnerSysRewardId in partnerSysRewardIds {
                let sysRewardId = ARPartnerSysRewardId()
                var responsePartnerSysRewardId = partnerSysRewardId.value
                
                if let range = responsePartnerSysRewardId.range(of: "http") {
                    responsePartnerSysRewardId = responsePartnerSysRewardId.replacingOccurrences(of: "http", with: "https", options: String.CompareOptions.literal, range: range)
                }
                
                sysRewardId.value = responsePartnerSysRewardId
                
                voucher.partnerSysRewardId.append(sysRewardId)
            }
            do {
                try arRealm.write {
                    for sysRewardId in voucher.partnerSysRewardId {
                        if let codeValue = sysRewardId.value, let code = arRealm.partnerSysRewardId(with: codeValue).first {
                            arRealm.delete(code)
                        }
                    }
                }
            } catch {
                debugPrint("Unable to delete old voucher codes")
            }
        }
        
        let voucherRewardStatuses = response.awardedRewardStatuses?.sorted(by: { (first, second) -> Bool in
            first.effectiveOn < second.effectiveOn
        }).first
        voucher.awardedRewardStatus = voucherRewardStatuses?.key ?? -1
        voucher.awardedRewardStatusEffectiveOn = Parser.main.parseDateString(voucherRewardStatuses?.effectiveOn)
        voucher.effectiveFrom = Parser.main.parseDateString(response.effectiveFrom)
        voucher.effectiveTo = Parser.main.parseDateString(response.effectiveTo)
        voucher.exchangeExchangeOn = Parser.main.parseDateString(response.exchange?.exchangeOn)
        voucher.exchangeFromRewardId.value = response.exchange?.fromRewardId
        voucher.exchangeToRewardId.value = response.exchange?.toRewardId
        voucher.exchangeTypeCode = response.exchange?.typeCode
        voucher.exchangeTypeKey.value = response.exchange?.typeKey
        voucher.exchangeTypeName = response.exchange?.typeName
        voucher.id = response.id
        voucher.partyId = response.partyId
        voucher.rewardCategoryCode = response.reward.categoryCode
        voucher.rewardCategoryKey = response.reward.categoryKey
        voucher.rewardCategoryName = response.reward.categoryName
        voucher.rewardId  = RewardReferences(rawValue: response.reward.id) ?? .Unknown
        voucher.rewardkey = response.reward.key
        voucher.rewardName = response.reward.name
        voucher.rewardOptionTypeCode = response.reward.optionTypeCode
        voucher.rewardOptionTypeKey = response.reward.optionTypeKey
        voucher.rewardOptionTypeName = response.reward.optionTypeName
        voucher.rewardProvidedByPartyId = response.rewardProviderPartyId
        voucher.rewardTypeCategoryCode = response.reward.typeCategoryCode
        voucher.rewardTypeCategoryKey = response.reward.typeCategoryKey
        voucher.rewardTypeCategoryName = response.reward.typeCategoryName
        voucher.rewardTypeCode = response.reward.typeCode
        voucher.rewardTypeKey = response.reward.typeKey
        voucher.rewardTypeName = response.reward.typeName
        voucher.rewardProviderPartyId = response.reward.providedByPartyId
        voucher.rewardValueAmount = response.rewardValue.amount
        voucher.rewardValuePercent.value = response.rewardValue.percent
        voucher.rewardValueQuantity.value = response.rewardValue.quantity
        voucher.rewardSelectionTypeCode = response.rewardSelectionType?.code
        voucher.rewardSelectionTypeKey.value = response.rewardSelectionType?.key
        voucher.rewardSelectionTypeName = response.rewardSelectionType?.name
        do {
            try arRealm.write {
                arRealm.delete(voucher.rewardSelections)
            }
        } catch {
            debugPrint("Unable to delete reward selections")
        }
        for rewardSelection in response.rewardSelectionType?.rewardSelections ?? [] {
            let selection = ARRewardsSelection.parseARRewardsSelection(response: rewardSelection, unclaimedRewardId: response.id)
            voucher.rewardSelections.append(selection)
        }
        voucher.rewardValueLinkId = response.rewardValue.linkId
        voucher.rewardValueTypeCode = response.rewardValue.typeCode
        voucher.rewardValueTypeKey = response.rewardValue.typeKey
        voucher.rewardValueTypeName = response.rewardValue.typeName
        voucher.rewardValueItemDescription = response.rewardValue.itemDescription
        do {
            try arRealm.write {
                if (arRealm.objects(ARVoucher.self).filter("id == %@", response.id).first != nil) {
                    arRealm.add(voucher, update: true)
                } else {
                    if voucher.rewardName != "Weight Watchers Voucher"{
                        arRealm.add(voucher)
                    }
                }
            }
        } catch {
            return BackendError.other
        }
        return nil
    }
}

extension ARUnclaimedReward {
    public static func parseAndPersistARRewardsVoucher(response: GEARAwardedRewards) -> Error? {
        let arRealm = DataProvider.newARRealm()
        let unclaimedReward : ARUnclaimedReward = ARUnclaimedReward()
        
        unclaimedReward.agreementPartyId = response.agreementPartyId
        unclaimedReward.awardedOn = Parser.main.parseDateString(response.awardedOn)
        let voucherRewardStatuses = response.awardedRewardStatuses?.sorted(by: { (first, second) -> Bool in
            first.effectiveOn < second.effectiveOn
        }).first
        unclaimedReward.awardedRewardStatus = voucherRewardStatuses?.key ?? -1
        unclaimedReward.awardedRewardStatusEffectiveOn = Parser.main.parseDateString(voucherRewardStatuses?.effectiveOn)
        unclaimedReward.effectiveFrom = Parser.main.parseDateString(response.effectiveFrom)
        unclaimedReward.effectiveTo = Parser.main.parseDateString(response.effectiveTo)
        unclaimedReward.eventId.value = response.eventId
        unclaimedReward.id = response.id
        unclaimedReward.unclaimedRewardId = response.id
        unclaimedReward.outcomeRewardValueLinkId.value = response.outcomeRewardValueLinkId
        unclaimedReward.partyId = response.partyId
        unclaimedReward.rewardCategoryCode = response.reward.categoryCode
        unclaimedReward.rewardCategoryKey = response.reward.categoryKey
        unclaimedReward.rewardCategoryName = response.reward.categoryName
        unclaimedReward.rewardId  = RewardReferences(rawValue: response.reward.id) ?? .Unknown
        unclaimedReward.rewardkey = response.reward.key
        unclaimedReward.rewardName = response.reward.name
        unclaimedReward.rewardOptionTypeCode = response.reward.optionTypeCode
        unclaimedReward.rewardOptionTypeKey = response.reward.optionTypeKey
        unclaimedReward.rewardOptionTypeName = response.reward.optionTypeName
        unclaimedReward.rewardProvidedByPartyId = response.rewardProviderPartyId
        unclaimedReward.rewardTypeCategoryCode = response.reward.typeCategoryCode
        unclaimedReward.rewardTypeCategoryKey = response.reward.typeCategoryKey
        unclaimedReward.rewardTypeCategoryName = response.reward.typeCategoryName
        unclaimedReward.rewardTypeCode = response.reward.typeCode
        unclaimedReward.rewardTypeKey = response.reward.typeKey
        unclaimedReward.rewardTypeName = response.reward.typeName
        unclaimedReward.rewardProviderPartyId = response.reward.providedByPartyId
        unclaimedReward.rewardSelectionTypeCode = response.rewardSelectionType?.code
        unclaimedReward.rewardSelectionTypeKey.value = response.rewardSelectionType?.key
        unclaimedReward.rewardSelectionTypeName = response.rewardSelectionType?.name
        do {
            try arRealm.write {
                arRealm.delete(unclaimedReward.rewardSelections)
            }
        } catch {
            debugPrint("Unable to delete reward selections")
        }
        for rewardSelection in response.rewardSelectionType?.rewardSelections ?? [] {
            let selection = ARRewardsSelection.parseARRewardsSelection(response: rewardSelection, unclaimedRewardId: response.id)
            unclaimedReward.rewardSelections.append(selection)
        }
        unclaimedReward.rewardValueItemDescription = response.rewardValue.itemDescription
        unclaimedReward.rewardValueQuantity.value = response.rewardValue.quantity
        unclaimedReward.rewardValuePercent.value = response.rewardValue.percent
        unclaimedReward.rewardValueAmount = response.rewardValue.amount
        unclaimedReward.rewardValueLinkId.value = response.rewardValue.linkId
        unclaimedReward.rewardValueTypeCode = response.rewardValue.typeCode
        unclaimedReward.rewardValueTypeKey = response.rewardValue.typeKey
        unclaimedReward.rewardValueTypeName = response.rewardValue.typeName
        do {
            try arRealm.write {
                if (arRealm.objects(ARUnclaimedReward.self).filter("id == %@", response.id).first != nil) {
                    arRealm.add(unclaimedReward, update: true)
                } else {
                    arRealm.add(unclaimedReward)
                }
            }
        } catch {
            return BackendError.other
        }
        return nil
    }
}

extension ARRewardsSelection {
    public static func parseARRewardsSelection(response: GEARRewardSelections, unclaimedRewardId: Int) -> ARRewardsSelection {
        let rewardsSelection = ARRewardsSelection()
        rewardsSelection.id = response.reward.id
        rewardsSelection.unclaimedRewardId = unclaimedRewardId
        rewardsSelection.rewardCategoryCode = response.reward.categoryCode
        rewardsSelection.rewardCategoryKey = response.reward.categoryKey
        rewardsSelection.rewardCategoryName = response.reward.categoryName
        rewardsSelection.rewardId = RewardReferences(rawValue: response.reward.id) ?? .Unknown
        rewardsSelection.rewardKey = response.reward.key
        rewardsSelection.rewardName = response.reward.name
        rewardsSelection.rewardOptionTypeCode = response.reward.optionTypeCode
        rewardsSelection.rewardOptionTypeKey = response.reward.optionTypeKey
        rewardsSelection.rewardOptionTypeName = response.reward.optionTypeName
        rewardsSelection.rewardProvidedByPartyId = response.reward.providedByPartyId
        rewardsSelection.rewardTypeCategoryCode = response.reward.categoryCode
        rewardsSelection.rewardTypeCategoryKey = response.reward.categoryKey
        rewardsSelection.rewardTypeCategoryName = response.reward.categoryName
        rewardsSelection.rewardTypeCode = response.reward.typeCode
        rewardsSelection.rewardTypeKey = response.reward.typeKey
        rewardsSelection.rewardTypeName = response.reward.typeName
        rewardsSelection.rewardValueItemDescription = response.rewardValueItemDescription
        rewardsSelection.rewardValueLinkId = response.rewardValueLinkId
        rewardsSelection.rewardValueQuantity.value = response.rewardValueQuantity
        rewardsSelection.rewardValuePercent.value = response.rewardValuePercent
        rewardsSelection.rewardValueAmount = response.rewardValueAmount
        rewardsSelection.rewardValueTypeCode = response.rewardValueTypeCode
        rewardsSelection.rewardValueTypeKey = response.rewardValueTypeKey
        rewardsSelection.rewardValueTypeName = response.rewardValueTypeName
        rewardsSelection.sortOrder = response.sortOrder
        return rewardsSelection
    }
}
