import UIKit
import RealmSwift
import VIAUtilities

extension Parser {
    public struct VHR {

        public static func parseQuestionnaires(response: QuestionnaireProgressAndPointsTracking, completion: @escaping (_: Error?) -> Void) {
            Parser.main.parse {
                VHRQuestionnaireProgressAndPointsTracking.persistQuestionnaireProgressAndPointsTracking(response: response)
                DispatchQueue.main.async {
                    completion(nil)
                }
            }
        }
    }
}

extension DataProvider {

    public class func deleteAllVHRRealmDataExceptUnsubmittedSavedAnswers() {
        let realm = DataProvider.newVHRRealm()
        let inMemoryRealm = DataProvider.newInMemoryRealm()

        //Copy all unsubmitted VHR results to in-memory realm
        let capturedResults = realm.allUnsubmittedVHRCapturedResults()
        try! inMemoryRealm.write {
            for capturedResult in capturedResults {
                inMemoryRealm.create(VHRCapturedResult.self, value: capturedResult)
            }
        }

        //Delete all data in VHR realm
        realm.reset()

        //Copy all unsubmitted VHR results from in-memory realm back into to vhrRealm
        let inMemoryCapturedResults = inMemoryRealm.allUnsubmittedVHRCapturedResults()
        try! realm.write {
            for capturedResult in inMemoryCapturedResults {
                realm.create(VHRCapturedResult.self, value: capturedResult)
            }
        }
    }

}

extension VHRQuestionnaireProgressAndPointsTracking {

    public class func persistQuestionnaireProgressAndPointsTracking(response: QuestionnaireProgressAndPointsTracking) {
        let realm = DataProvider.newVHRRealm()
        DataProvider.deleteAllVHRRealmDataExceptUnsubmittedSavedAnswers()

        let item = VHRQuestionnaireProgressAndPointsTracking()
        item.totalEarnedPoints.value = response.totalEarnedPoints
        item.questionnaireSetTypeCode = response.questionnaireSetTypeCode
        item.totalPotentialPoints.value = response.totalPotentialPoints
        item.totalQuestionnaireCompleted.value = response.totalQuestionnaireCompleted
        item.questionnaireSetCompleted = response.questionnaireSetCompleted
        item.questionnaireSetTypeName = response.questionnaireSetTypeName
        item.totalEarnedPoints.value = response.totalEarnedPoints
        item.questionnaireSetType = QuestionnaireSetRef(rawValue: response.questionnaireSetTypeKey) ?? .Unknown
        item.totalQuestionnaires = response.totalQuestionnaires
        item.questionnaireSetText = response.setText ?? ""
        item.questionnaireSetTextDescription = response.setTextDescription ?? ""
        item.questionnaireSetTextNote = response.setTextNote ?? ""

        response.questionnaire.flatMap({ $0 })?.forEach({ questionnaire in
            let vhrQuestionnaire = VHRQuestionnaire.parseVHRQuestionare(responseQuestionnare: questionnaire)
            item.questionnaire.append(vhrQuestionnaire)
        })

        try! realm.write {
            // create VHR questionnaire set and cache
            let vhrCache = VHRCache()
            realm.add(vhrCache)
            realm.add(item)
        }
        
        //ge20180115: Update VHR child questions
//        print("ge-->VHR: Update questions to indicate they are child q")
        let associations = realm.objects(VHRQuestionAssociation.self)
        for assoc in associations {
            let question = realm.objects(VHRQuestion.self).filter("typeKey == %@",assoc.childQuestionKey).first
            try! realm.write {
                question?.isChild = true
            }
        }

        // reevaluate all the answers to make sure our sections and questions have the correct visibility
        VHRQuestionnaire.reevaluateVisibilityTagsForAllCapturedResults(in: realm)
    }
}

extension VHRQuestionnaire {
    public class func parseVHRQuestionare(responseQuestionnare: QPPTQuestionnaire) -> VHRQuestionnaire {
        let vhrQuestionnare = VHRQuestionnaire()
        if let sections = responseQuestionnare.questionnaireSections {
            for section in sections {
                let vhrQuestionnaireSection = VHRQuestionnaireSection.parseVHRQuestionnaireSection(section: section, questionnaire: responseQuestionnare)
                vhrQuestionnare.questionnaireSections.append(vhrQuestionnaireSection)
            }
        }
        vhrQuestionnare.sortOrderIndex.value = responseQuestionnare.sortOrderIndex
        vhrQuestionnare.typeKey = responseQuestionnare.typeKey
        if let dateString = responseQuestionnare.completedOn {
            vhrQuestionnare.completedOn = Parser.main.yearMonthDayFormatter.date(from: dateString)
        }
        vhrQuestionnare.typeName = responseQuestionnare.typeName
        vhrQuestionnare.completionFlag = responseQuestionnare.completionFlag
        vhrQuestionnare.typeCode = responseQuestionnare.typeCode

        vhrQuestionnare.text = responseQuestionnare.text ?? ""
        vhrQuestionnare.textDescription = responseQuestionnare.textDescription ?? ""
        vhrQuestionnare.textNote = responseQuestionnare.textNote ?? ""
        if vhrQuestionnare.completionFlag == true {
            vhrQuestionnare.completionState = .Complete
        } else if vhrQuestionnare.completionFlag == false {
            vhrQuestionnare.completionState = .NotStarted
        } else {
            vhrQuestionnare.completionState = .Unknown
        }

        return vhrQuestionnare
    }
}

extension VHRQuestionnaireSection {
    public class func parseVHRQuestionnaireSection(section: QPPTQuestionnaireSections, questionnaire: QPPTQuestionnaire) -> VHRQuestionnaireSection {
        let vhrQuestionnaireSection = VHRQuestionnaireSection()
        vhrQuestionnaireSection.typeKey = section.typeKey
        vhrQuestionnaireSection.typeName = section.typeName
        vhrQuestionnaireSection.visibilityTag = section.visibilityTagName
        if let questions = section.questions {
            for question in questions {
                let sectionQuestion = VHRQuestion.parseVHRQuestion(question, for: section, in: questionnaire)
                vhrQuestionnaireSection.questions.append(sectionQuestion)
                
                //ge20180113: Add question associations to realm
//                print("ge-->VHR: Add question associations to realm")
                if let associations = question.questionAssociations {
                    for association in associations {
                        let childQ = VHRQuestionAssociation()
                        childQ.childQuestionKey = association.childQuestionKey
                        childQ.sortIndex = association.sortIndex
                        let realm = DataProvider.newVHRRealm()
                        try! realm.write {
                            realm.add(childQ)
                        }
                    }
                }
                
            }
        }
        vhrQuestionnaireSection.isVisible = section.isVisible
        vhrQuestionnaireSection.typeCode = section.typeCode
        vhrQuestionnaireSection.sortOrderIndex.value = section.sortOrderIndex

        vhrQuestionnaireSection.text = section.text ?? ""
        vhrQuestionnaireSection.textDescription = section.textDescription ?? ""
        vhrQuestionnaireSection.textNote = section.textNote ?? ""
        
        return vhrQuestionnaireSection
    }
}

extension VHRQuestion {

    private static func hasValidUnitOfMeasureCombination(question: QPPTQuestions, populationValue: QPPTPopulationValues) -> Bool {
        guard let populationValueUOM = populationValue.unitOfMeasure, let questionUOMs = question.unitOfMeasures else {
            return true // No units of measure for either population value or question, passing through
        }

        guard questionUOMs.filter({ $0.value == populationValueUOM }).count > 0 else {
            debugPrint("Skipping prepopulation value for VHR, cannot match with UOM sent back for question")
            debugPrint("questionUOMs: \(questionUOMs) - populationValueUOM: \(populationValueUOM)")
            return false
        }

        return true
    }

    private class func createCapturedResult(for question: QPPTQuestions, populationValue: QPPTPopulationValues, section: QPPTQuestionnaireSections, questionnaire: QPPTQuestionnaire) {

        // check that the prepopulated value matches with any of the units of measure
        // that is being sent back with the question, otherwise skip over it, as 
        // the frontend won't know how to convert between the 2.
        guard VHRQuestion.hasValidUnitOfMeasureCombination(question: question, populationValue: populationValue) else {
            debugPrint("Skipping prepopulation value for VHR, cannot match with UOM sent back for question")
            return
        }

        let realm = DataProvider.newVHRRealm()
        
        if realm.vhrCapturedResultExists(for: question.typeKey) {
            
            /** This is used in VHR Social Life question "How long ago did you stop Smoking?"
                This will set the YEAR and MONTH textfield empty
                This is used when there is already captured result for questionTypeKey 238 and 239
             */
            if AppSettings.getAppTenant() == TenantReference.CA{
                if let captureResult238 = realm.vhrCapturedResult(for: 238){
                    try! realm.write {
                        captureResult238.answer = ""
                    }
                }
                
                if let captureResult239 = realm.vhrCapturedResult(for: 239){
                    try! realm.write {
                        captureResult239.answer = ""
                    }
                }
            }
 
            
            // if we hit this, it means that the user has captured something locally
            // and the parser is trying to prepopulate over the user's captured value.
            // we can't allow this, so we skip creating this prepopulated value / captured result
            return
        }

        try! realm.write {
            let capturedResult = VHRCapturedResult()
            capturedResult.dateCaptured = Parser.main.parseDateString(populationValue.eventDate) ?? Date()
            capturedResult.valid = true
            capturedResult.questionTypeKey = question.typeKey
            capturedResult.type = QuestionTypeRef(rawValue: question.questionTypeKey) ?? .Unknown
            capturedResult.questionnaireSectionTypeKey = section.typeKey
            capturedResult.questionnaireTypeKey = questionnaire.typeKey
            capturedResult.prepopulationEventKey.value = populationValue.eventKey
            capturedResult.prepopulationEventName = populationValue.eventName
            capturedResult.prepopulationEventCode = populationValue.eventCode

            if let populationValueUOMRawValue = Int(populationValue.unitOfMeasure ?? ""), let populationValueUOM = UnitOfMeasureRef(rawValue: populationValueUOMRawValue) {
                capturedResult.unitOfMeasure = populationValueUOM
            } else {
//                debugPrint("Prepopulation value doesn't have a UOM, this will result in oddities on the frontend")
            }

            // Henri Langenhoven
            // [2 hours ago]
            // @vanhalen Werner mentioned on Friday that every prepop value will be coming back in the Value field from now on
            // https://glucode.slack.com/archives/C5A563YJZ/p1499075252442704?thread_ts=1499068899.224246&cid=C5A563YJZ
            
            
            /** This is used in VHR Social Life question "How long ago did you stop Smoking?"
             This will set the YEAR and MONTH textfield empty
             This is used when there is no captured result for questionTypeKey 238 and 239
             */
            if AppSettings.getAppTenant() == TenantReference.CA{
                if(capturedResult.questionTypeKey == 239) || (capturedResult.questionTypeKey == 238){
                    capturedResult.answer = ""
                }else{
                    capturedResult.answer = populationValue.value
                }
            }else{
                capturedResult.answer = populationValue.value
            }
            
            /*
             * SystolicPressure [72], DiastolicPressure [73] = NumberRange -> BE return is Float() instead of Int()
             *
             * Since backend is not able to correct the value being returned,
             * mobile will have to adjust and convert the value which will be used as the default populated value
             * to prevent the service parsing error on assessment submission.
             * reference ticket: https://jira.vitalityservicing.com/browse/FC-25397
             */
            if AppSettings.getAppTenant() == .EC {
                if capturedResult.questionTypeKey == 72 || capturedResult.questionTypeKey == 73 {
                    if capturedResult.type == .NumberRange, let value = populationValue.value {
                        // Convert 'value' string to float -> float to int -> int back to string.
                        capturedResult.answer = String(Int(Float(value) ?? 0))
                    }
                }
            }
            
            realm.add(capturedResult)
        }
    }
    
    public class func parseVHRQuestion(_ question: QPPTQuestions, for section: QPPTQuestionnaireSections, in questionnaire: QPPTQuestionnaire) -> VHRQuestion {
        let vhrQuestion = VHRQuestion()
        vhrQuestion.questionDecorator = VHRQuestionDecorator()
        if let decorator = question.questionDecorator {
            vhrQuestion.questionDecorator = VHRQuestionDecorator.parseVHRQuestionDecorator(decorator: decorator)
        }
        vhrQuestion.typeKey = question.typeKey
        if let populationValues = question.populationValues {
            for populationValue in populationValues {
                VHRQuestion.createCapturedResult(for: question, populationValue: populationValue, section: section, questionnaire: questionnaire)
            }
        }
        if let unitOfMeasures = question.unitOfMeasures {
            for unitOfMeasure in unitOfMeasures {
                let vhrUnitOfMeasure = VHRUnitOfMeasure.parseVHRUnitOfMeasures(unitOfMeasure: unitOfMeasure)
                vhrQuestion.unitsOfMeasure.append(vhrUnitOfMeasure)
            }
        }
        vhrQuestion.typeName = question.typeName
        if question.visibilityTagName?.hasPrefix("\(question.typeKey)") ?? false {
            debugPrint(">>>>>>>>Malformed payload. Parent questionnaire MUST NOT have a visibilityTagName")
        } else {
            vhrQuestion.visibilityTag = question.visibilityTagName ?? ""
        }
        vhrQuestion.isVisible = vhrQuestion.visibilityTag.isEmpty
        // regular expression
        vhrQuestion.format = question.format
        vhrQuestion.length.value = question.length ?? 0
        if let validValues = question.validValues {
            for validValue in validValues {
                let vhrValidValue = VHRValidValue.parseVHRValidValue(value: validValue)
                vhrQuestion.validValues.append(vhrValidValue)
            }
        }
        
        //ge20180407 : Add questionAssociations to VHRQuestion in realm
        if let associations = question.questionAssociations {
            for association in associations {
                let childQ = VHRQuestionAssociation()
                childQ.childQuestionKey = association.childQuestionKey
                childQ.sortIndex = association.sortIndex
                vhrQuestion.childQuestions.append(childQ)
            }
        }

        vhrQuestion.text = question.text ?? ""
        vhrQuestion.textDescription = question.textDescription ?? ""
        vhrQuestion.textNote = question.textNote ?? ""
        vhrQuestion.typeCode = question.typeCode ?? ""
        // This indicates the type of question. Depicts what format the answer would be requested in the questionnaire e.g. option list
        vhrQuestion.questionTypeName = question.questionTypeName
        vhrQuestion.questionTypeCode = question.questionTypeCode
        vhrQuestion.questionType = QuestionTypeRef(rawValue: question.questionTypeKey) ?? .Unknown
        vhrQuestion.sortOrderIndex.value = question.sortOrderIndex
        
        return vhrQuestion
    }
}

extension VHRQuestionDecorator {
    public class func parseVHRQuestionDecorator(decorator: QPPTQuestionDecorator) -> VHRQuestionDecorator {
        let vhrQuestionDecorator = VHRQuestionDecorator()
        vhrQuestionDecorator.channelType = QuestionnaireChannelRef(rawValue: decorator.channelTypeKey) ?? .Unknown
        vhrQuestionDecorator.type = QuestionDecoratorRef(rawValue: decorator.typeKey) ?? .Unknown
        vhrQuestionDecorator.channelTypeCode = decorator.channelTypeCode
        vhrQuestionDecorator.typeName = decorator.typeName
        vhrQuestionDecorator.channelTypeName = decorator.channelTypeName
        vhrQuestionDecorator.typeCode = decorator.typeCode
        return vhrQuestionDecorator
    }
}

extension VHRUnitOfMeasure {
    public class func parseVHRUnitOfMeasures(unitOfMeasure: QPPTUnitOfMeasures) -> VHRUnitOfMeasure {
        let vhrUnitOfMeasure = VHRUnitOfMeasure()
        vhrUnitOfMeasure.value = unitOfMeasure.value
        return vhrUnitOfMeasure
    }
}

extension VHRValidValue {
    public class func parseVHRValidValue(value: QPPTValidValues) -> VHRValidValue {
        let vhrValidValue = VHRValidValue()
        //if let vhrUnitOfMeasure = value.unitOfMeasure {
        //    let vhrUnitOfMeasureRawValue = Int(vhrUnitOfMeasure.value) ?? UnitOfMeasureRef.Unknown.rawValue
        //    vhrValidValue.unitOfMeasure = UnitOfMeasureRef(rawValue: vhrUnitOfMeasureRawValue) ?? UnitOfMeasureRef.Unknown
        //}
        let vhrUnitOfMeasureRawValue = Int(value.unitOfMeasure?.value ?? "") ?? UnitOfMeasureRef.Unknown.rawValue
        vhrValidValue.unitOfMeasure = UnitOfMeasureRef(rawValue: vhrUnitOfMeasureRawValue) ?? UnitOfMeasureRef.Unknown
        vhrValidValue.name = value.name
        vhrValidValue.value = value.value
        vhrValidValue.valueDescription = value.description ?? ""
        vhrValidValue.valueNote = value.note ?? ""
        
        //ge20180227 : Just a small tweak to differentiate text & description whenever they are the same.
        //           : Same text & description will cause issues on func addAttributesIfNeeded(),
        //           : which is searching NSRange of text to add attributes
        if(vhrValidValue.value == vhrValidValue.valueDescription) {
            vhrValidValue.valueDescription = "\(vhrValidValue.valueDescription) "
        }
        
        if let validValueType = value.validValueTypes?.first {
            vhrValidValue.type = QuestionValidValueTypeRef(rawValue: validValueType.typeKey) ?? .Unknown
            vhrValidValue.typeName = validValueType.typeName
            vhrValidValue.typeCode = validValueType.typeCode
        }

        return vhrValidValue
    }
}
