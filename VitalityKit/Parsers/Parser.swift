import Foundation
import VIAUtilities

enum ParsingError: Error {
    case serializationError
    case emptyResponse
}

class Parser: PopularDateFormatContainer {
    
    // MARK: Singleton
    
    static let main = Parser()
    
    private let parsingQueue = DispatchQueue(label: "VAParsingQueue", qos: DispatchQoS.background)
    
    // MARK: Main parsing
    
    func parse(_ block: @escaping () -> Void) {
        self.parsingQueue.async {
            block()
        }
    }
    
    // MARK: Date parsing helpers
    
    lazy var dateFormatter: DateFormatter = {
        return DateFormatter.apiManagerFormatter()
    }()
    
    lazy var zerosFormatter: DateFormatter = {
        return DateFormatter.apiManagerFormatterWithMilliseconds(localeIdentifier: DeviceLocale.toString())
    }()
    
    lazy var alternativeDateFormatter: DateFormatter = {
        return DateFormatter.newAPIManagerAlternativeDateTimeFormatter()
    }()
    
    lazy var alternativeDateFormatterTwo: DateFormatter = {
        return DateFormatter.newAPIManagerAlternativeTwoDateTimeFormatter()
    }()
    
    lazy var yearMonthDayFormatter: DateFormatter = {
        return DateFormatter.yearMonthDayFormatter()
    }()
    
    
    
}

protocol PopularDateFormatContainer {
    
    var dateFormatter: DateFormatter { get } // DateFormatter.apiManagerFormatter()
    
    var zerosFormatter: DateFormatter { get } // DateFormatter.apiManagerFormatterWithMilliseconds()
    
    var alternativeDateFormatter: DateFormatter { get } // DateFormatter.newAPIManagerAlternativeDateTimeFormatter()
    
    var alternativeDateFormatterTwo: DateFormatter { get } // DateFormatter.newAPIManagerAlternativeTwoDateTimeFormatter()
    
    var yearMonthDayFormatter: DateFormatter { get } // DateFormatter.yearMonthDayFormatter()
    
    func parseDateString(_ dateString: String?) -> Date?
    
    func parseDateString(_ dateString: String?, fallback: Date) -> Date
    
}

extension PopularDateFormatContainer {
    
    public func parseDateString(_ dateString: String?) -> Date? {
        guard let validDateString = dateString else { return nil}
        
        if let date = dateFormatter.date(from: validDateString) {
            return date
        } else if let date = alternativeDateFormatter.date(from: validDateString) {
            return date
        } else if let date = alternativeDateFormatterTwo.date(from: validDateString) {
            return date
        } else if let date = yearMonthDayFormatter.date(from: validDateString) {
            return date
        } else if let date = zerosFormatter.date(from: validDateString) {
            return date
        }
        return nil
    }
    
    public func parseDateString(_ dateString: String?, fallback: Date) -> Date {
        guard let validDateString = dateString else { return fallback}
        
        if let date = self.parseDateString(validDateString) {
            return date
        }
        return fallback
    }
    
    /*
    // 12-hour format date string parser
    */
    public func parsePointsHistoryEffectiveDate(_ dateString: String?, fallback: Date, ignoreTimezone: Bool = false) -> Date {
        guard let validDateString = dateString else { return fallback}
        
        if let convertedDate = Date.parseStringToDate(validDateString, ignoreTimezone: ignoreTimezone) {
            return convertedDate
        }
        
        return fallback
    }
}


public struct DateParser: PopularDateFormatContainer {
    
    // easy to copy all the existing formatters from the main Parser, than to alloc new ones
    
    public let dateFormatter: DateFormatter = Parser.main.dateFormatter.copy() as! DateFormatter
    
    public let zerosFormatter: DateFormatter = Parser.main.zerosFormatter.copy() as! DateFormatter
    
    public let alternativeDateFormatter: DateFormatter = Parser.main.alternativeDateFormatter.copy() as! DateFormatter
    
    public let alternativeDateFormatterTwo: DateFormatter = Parser.main.alternativeDateFormatterTwo.copy() as! DateFormatter
    
    public let yearMonthDayFormatter: DateFormatter = Parser.main.yearMonthDayFormatter.copy() as! DateFormatter
    
    public init() {
        // New DateParser!
    }
    
}


extension Date {
    
    public func convertToLocalTime(fromTimeZone timeZoneAbbreviation: String) -> Date? {
        if let timeZone = TimeZone(abbreviation: timeZoneAbbreviation) {
            let targetOffset = TimeInterval(timeZone.secondsFromGMT(for: self))
            //            debugPrint(targetOffset)
            let localOffeset = TimeInterval(TimeZone.autoupdatingCurrent.secondsFromGMT(for: self))
            //            debugPrint(localOffeset)
            
            return self.addingTimeInterval(targetOffset - localOffeset)
        }
        
        return nil
    }
    
    public static func parseISO8601Date(_ isoDateString: String) -> Date?{
        let trimmedIsoString = isoDateString.replacingOccurrences(of: "\\.\\d+", with: "", options: .regularExpression)
        let formatter = ISO8601DateFormatter()
        formatter.formatOptions = .withInternetDateTime
        return formatter.date(from: trimmedIsoString)
    }
    
    public static func parseStringToDate(_ dateString: String, ignoreTimezone: Bool = false) -> Date? {
        if let index = dateString.range(of: "T")?.lowerBound {
            let substring = dateString[..<index]
            let yearMonthDay = String(substring)
            
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd"
            // For Ecuador, do not factor in locale and timezone in displaying actual dates returned from the backend.
            if !ignoreTimezone {
                if let localeID = Bundle.main.object(forInfoDictionaryKey: "VIALocaleID") as? String{
                    formatter.locale = Locale(identifier: localeID)
                }
                formatter.timeZone = TimeZone(abbreviation: "GMT")
            }
            
            if let convertedDate = formatter.date(from: yearMonthDay) {
                return convertedDate
            }
        }
        return nil
    }
}

