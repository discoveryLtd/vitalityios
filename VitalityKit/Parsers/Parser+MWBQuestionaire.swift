//
//  Parser+MWBQuestionaire.swift
//  VitalityActive
//
//  Created by Harry C. Lingad on 2/5/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import UIKit
import RealmSwift
import VIAUtilities

extension Parser {
    public struct MWB {
        
        public static func parseQuestionnaires(response: QuestionnaireProgressAndPointsTracking, completion: @escaping (_: Error?) -> Void) {
            Parser.main.parse {
                MWBQuestionnaireProgressAndPointsTracking.persistQuestionnaireProgressAndPointsTracking(response: response)
                DispatchQueue.main.async {
                    completion(nil)
                }
            }
        }
    }
}

extension DataProvider {
    
    public class func deleteAllMWBRealmDataExceptUnsubmittedSavedAnswers() {
        let realm = DataProvider.newMWBRealm()
        let inMemoryRealm = DataProvider.newInMemoryRealm()
        
        //Copy all unsubmitted MWB results to in-memory realm
        let capturedResults = realm.allUnsubmittedMWBCapturedResults()
        try! inMemoryRealm.write {
            for capturedResult in capturedResults {
                inMemoryRealm.create(MWBCapturedResult.self, value: capturedResult)
            }
        }
        
        //Delete all data in MWB realm
        realm.reset()
        
        //Copy all unsubmitted MWB results from in-memory realm back into to mwbRealm
        let inMemoryCapturedResults = inMemoryRealm.allUnsubmittedMWBCapturedResults()
        try! realm.write {
            for capturedResult in inMemoryCapturedResults {
                realm.create(MWBCapturedResult.self, value: capturedResult)
            }
        }
    }
    
}

extension MWBQuestionnaireProgressAndPointsTracking {
    
    public class func persistQuestionnaireProgressAndPointsTracking(response: QuestionnaireProgressAndPointsTracking) {
        let realm = DataProvider.newMWBRealm()
        DataProvider.deleteAllMWBRealmDataExceptUnsubmittedSavedAnswers()
        
        let item = MWBQuestionnaireProgressAndPointsTracking()
        item.totalEarnedPoints.value = response.totalEarnedPoints
        item.questionnaireSetTypeCode = response.questionnaireSetTypeCode
        item.totalPotentialPoints.value = response.totalPotentialPoints
        item.totalQuestionnaireCompleted.value = response.totalQuestionnaireCompleted
        item.questionnaireSetCompleted = response.questionnaireSetCompleted
        item.questionnaireSetTypeName = response.questionnaireSetTypeName
        item.totalEarnedPoints.value = response.totalEarnedPoints
        item.questionnaireSetType = QuestionnaireSetRef(rawValue: response.questionnaireSetTypeKey) ?? .Unknown
        item.totalQuestionnaires = response.totalQuestionnaires
        item.questionnaireSetText = response.setText ?? ""
        item.questionnaireSetTextDescription = response.setTextDescription ?? ""
        item.questionnaireSetTextNote = response.setTextNote ?? ""
        
        response.questionnaire.flatMap({ $0 })?.forEach({ questionnaire in
            let mwbQuestionnaire = MWBQuestionnaire.parseMWBQuestionare(responseQuestionnare: questionnaire)
            item.questionnaire.append(mwbQuestionnaire)
        })
        try! realm.write {
            // create MWB questionnaire set and cache
            let mwbCache = MWBCache()
            realm.add(mwbCache)
            realm.add(item)
        }
        
        //ge20180115: Update MWB child questions
        print("ge-->MWB: Update questions to indicate they are child q")
        let associations = realm.objects(MWBQuestionAssociation.self)
        for assoc in associations {
            let question = realm.objects(MWBQuestion.self).filter("typeKey == %@",assoc.childQuestionKey).first
            try! realm.write {
                question?.isChild = true
            }
        }
        
        // reevaluate all the answers to make sure our sections and questions have the correct visibility
        MWBQuestionnaire.reevaluateVisibilityTagsForAllCapturedResults(in: realm)
    }
}

extension MWBQuestionnaire {
    
    public class func parseMWBQuestionare(responseQuestionnare: QPPTQuestionnaire) -> MWBQuestionnaire {
        
        let mwbQuestionnare = MWBQuestionnaire()
        if let sections = responseQuestionnare.questionnaireSections {
            for section in sections {
                let mwbQuestionnaireSection = MWBQuestionnaireSection.parseMWBQuestionnaireSection(section: section, questionnaire: responseQuestionnare)
                mwbQuestionnare.questionnaireSections.append(mwbQuestionnaireSection)
            }
        }
        mwbQuestionnare.sortOrderIndex.value = responseQuestionnare.sortOrderIndex
        mwbQuestionnare.typeKey = responseQuestionnare.typeKey
        if let dateString = responseQuestionnare.completedOn {
            mwbQuestionnare.completedOn = Parser.main.yearMonthDayFormatter.date(from: dateString)
        }
        mwbQuestionnare.typeName = responseQuestionnare.typeName
        mwbQuestionnare.completionFlag = responseQuestionnare.completionFlag
        mwbQuestionnare.typeCode = responseQuestionnare.typeCode
        
        mwbQuestionnare.text = responseQuestionnare.text ?? ""
        mwbQuestionnare.textDescription = responseQuestionnare.textDescription ?? ""
        mwbQuestionnare.textNote = responseQuestionnare.textNote ?? ""
        
        if mwbQuestionnare.completionFlag == true {
            mwbQuestionnare.completionState = .Complete
        } else if mwbQuestionnare.completionFlag == false {
            mwbQuestionnare.completionState = .NotStarted
        } else {
            mwbQuestionnare.completionState = .Unknown
        }
        
        return mwbQuestionnare
    }
}

extension MWBQuestionnaireSection {
    public class func parseMWBQuestionnaireSection(section: QPPTQuestionnaireSections, questionnaire: QPPTQuestionnaire) -> MWBQuestionnaireSection {
        
        let mwbQuestionnaireSection = MWBQuestionnaireSection()
        mwbQuestionnaireSection.typeKey = section.typeKey
        mwbQuestionnaireSection.typeName = section.typeName
        mwbQuestionnaireSection.visibilityTag = section.visibilityTagName
        if let questions = section.questions {
            for question in questions {
                let sectionQuestion = MWBQuestion.parseMWBQuestion(question, for: section, in: questionnaire)
                mwbQuestionnaireSection.questions.append(sectionQuestion)
                
                //ge20180113: Add question associations to realm
//                print("ge-->MWB: Add question associations to realm")
                if let associations = question.questionAssociations {
                    for association in associations {
                        let childQ = MWBQuestionAssociation()
                        childQ.childQuestionKey = association.childQuestionKey
                        childQ.sortIndex = association.sortIndex
                        let realm = DataProvider.newMWBRealm()
                        try! realm.write {
                            realm.add(childQ)
                        }
                    }
                }
                
            }
        }
        mwbQuestionnaireSection.isVisible = section.isVisible
        mwbQuestionnaireSection.typeCode = section.typeCode
        mwbQuestionnaireSection.sortOrderIndex.value = section.sortOrderIndex
        
        mwbQuestionnaireSection.text = section.text ?? ""
        mwbQuestionnaireSection.textDescription = section.textDescription ?? ""
        mwbQuestionnaireSection.textNote = section.textNote ?? ""
        
        return mwbQuestionnaireSection
    }
}

extension MWBQuestion {
    
    private static func MWBhasValidUnitOfMeasureCombination(question: QPPTQuestions, populationValue: QPPTPopulationValues) -> Bool {
        guard let populationValueUOM = populationValue.unitOfMeasure, let questionUOMs = question.unitOfMeasures else {
            return true // No units of measure for either population value or question, passing through
        }
        
        guard questionUOMs.filter({ $0.value == populationValueUOM }).count > 0 else {
            debugPrint("Skipping prepopulation value for MWB, cannot match with UOM sent back for question")
            debugPrint("questionUOMs: \(questionUOMs) - populationValueUOM: \(populationValueUOM)")
            return false
        }
        
        return true
    }
    
    private class func MWBcreateCapturedResult(for question: QPPTQuestions, populationValue: QPPTPopulationValues, section: QPPTQuestionnaireSections, questionnaire: QPPTQuestionnaire) {
        
        // check that the prepopulated value matches with any of the units of measure
        // that is being sent back with the question, otherwise skip over it, as
        // the frontend won't know how to convert between the 2.
        guard MWBQuestion.MWBhasValidUnitOfMeasureCombination(question: question, populationValue: populationValue) else {
            debugPrint("Skipping prepopulation value for MWB, cannot match with UOM sent back for question")
            return
        }
        
        let realm = DataProvider.newMWBRealm()
        if realm.mwbCapturedResultExists(for: question.typeKey) {
            // if we hit this, it means that the user has captured something locally
            // and the parser is trying to prepopulate over the user's captured value.
            // we can't allow this, so we skip creating this prepopulated value / captured result
            return
        }
        
        try! realm.write {
            let capturedResult = MWBCapturedResult()
            capturedResult.dateCaptured = Parser.main.parseDateString(populationValue.eventDate) ?? Date()
            capturedResult.valid = true
            capturedResult.questionTypeKey = question.typeKey
            capturedResult.type = QuestionTypeRef(rawValue: question.questionTypeKey) ?? .Unknown
            capturedResult.questionnaireSectionTypeKey = section.typeKey
            capturedResult.questionnaireTypeKey = questionnaire.typeKey
            capturedResult.prepopulationEventKey.value = populationValue.eventKey
            capturedResult.prepopulationEventName = populationValue.eventName
            capturedResult.prepopulationEventCode = populationValue.eventCode
            
            if let populationValueUOMRawValue = Int(populationValue.unitOfMeasure ?? ""), let populationValueUOM = UnitOfMeasureRef(rawValue: populationValueUOMRawValue) {
                capturedResult.unitOfMeasure = populationValueUOM
            } else {
//                debugPrint("Prepopulation value doesn't have a UOM, this will result in oddities on the frontend")
            }
            
            // Henri Langenhoven
            // [2 hours ago]
            // @vanhalen Werner mentioned on Friday that every prepop value will be coming back in the Value field from now on
            // https://glucode.slack.com/archives/C5A563YJZ/p1499075252442704?thread_ts=1499068899.224246&cid=C5A563YJZ
            capturedResult.answer = populationValue.value
            realm.add(capturedResult)
        }
    }
    
    public class func parseMWBQuestion(_ question: QPPTQuestions, for section: QPPTQuestionnaireSections, in questionnaire: QPPTQuestionnaire) -> MWBQuestion {
        let mwbQuestion = MWBQuestion()
        mwbQuestion.questionDecorator = MWBQuestionDecorator()
        if let decorator = question.questionDecorator {
            mwbQuestion.questionDecorator = MWBQuestionDecorator.parseMWBQuestionDecorator(decorator: decorator)
        }
        mwbQuestion.typeKey = question.typeKey
        if let populationValues = question.populationValues {
            for populationValue in populationValues {
                MWBQuestion.MWBcreateCapturedResult(for: question, populationValue: populationValue, section: section, questionnaire: questionnaire)
            }
        }
        if let unitOfMeasures = question.unitOfMeasures {
            for unitOfMeasure in unitOfMeasures {
                let mwbUnitOfMeasure = MWBUnitOfMeasure.parseMWBUnitOfMeasures(unitOfMeasure: unitOfMeasure)
                mwbQuestion.unitsOfMeasure.append(mwbUnitOfMeasure)
            }
        }
        mwbQuestion.typeName = question.typeName
        mwbQuestion.visibilityTag = question.visibilityTagName ?? ""
        mwbQuestion.isVisible = mwbQuestion.visibilityTag.isEmpty
        // regular expression
        mwbQuestion.format = question.format
        mwbQuestion.length.value = question.length ?? 0
        if let validValues = question.validValues {
            for validValue in validValues {
                let mwbValidValue = MWBValidValue.parseMWBValidValue(value: validValue)
                mwbQuestion.validValues.append(mwbValidValue)
            }
        }
        
        mwbQuestion.text = question.text ?? ""
        mwbQuestion.textDescription = question.textDescription ?? ""
        mwbQuestion.textNote = question.textNote ?? ""
        mwbQuestion.typeCode = question.typeCode ?? ""
        // This indicates the type of question. Depicts what format the answer would be requested in the questionnaire e.g. option list
        mwbQuestion.questionTypeName = question.questionTypeName
        mwbQuestion.questionTypeCode = question.questionTypeCode
        mwbQuestion.questionType = QuestionTypeRef(rawValue: question.questionTypeKey) ?? .Unknown
        mwbQuestion.sortOrderIndex.value = question.sortOrderIndex
        
        return mwbQuestion
    }
}

extension MWBQuestionDecorator {
    public class func parseMWBQuestionDecorator(decorator: QPPTQuestionDecorator) -> MWBQuestionDecorator {
        let mwbQuestionDecorator = MWBQuestionDecorator()
        mwbQuestionDecorator.channelType = QuestionnaireChannelRef(rawValue: decorator.channelTypeKey) ?? .Unknown
        mwbQuestionDecorator.type = QuestionDecoratorRef(rawValue: decorator.typeKey) ?? .Unknown
        mwbQuestionDecorator.channelTypeCode = decorator.channelTypeCode
        mwbQuestionDecorator.typeName = decorator.typeName
        mwbQuestionDecorator.channelTypeName = decorator.channelTypeName
        mwbQuestionDecorator.typeCode = decorator.typeCode
        return mwbQuestionDecorator
    }
}

extension MWBUnitOfMeasure {
    public class func parseMWBUnitOfMeasures(unitOfMeasure: QPPTUnitOfMeasures) -> MWBUnitOfMeasure {
        let mwbUnitOfMeasure = MWBUnitOfMeasure()
        mwbUnitOfMeasure.value = unitOfMeasure.value
        return mwbUnitOfMeasure
    }
}

extension MWBValidValue {
    public class func parseMWBValidValue(value: QPPTValidValues) -> MWBValidValue {
        let mwbValidValue = MWBValidValue()
        let mwbUnitOfMeasureRawValue = Int(value.unitOfMeasure?.value ?? "") ?? UnitOfMeasureRef.Unknown.rawValue
        mwbValidValue.unitOfMeasure = UnitOfMeasureRef(rawValue: mwbUnitOfMeasureRawValue) ?? UnitOfMeasureRef.Unknown
        mwbValidValue.name = value.name
        mwbValidValue.value = value.value
        mwbValidValue.valueDescription = value.description ?? ""
        mwbValidValue.valueNote = value.note ?? ""
        if let validValueType = value.validValueTypes?.first {
            mwbValidValue.type = QuestionValidValueTypeRef(rawValue: validValueType.typeKey) ?? .Unknown
            mwbValidValue.typeName = validValueType.typeName
            mwbValidValue.typeCode = validValueType.typeCode
        }
        
        return mwbValidValue
    }
}

