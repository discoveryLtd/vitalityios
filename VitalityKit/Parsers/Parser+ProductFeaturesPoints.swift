import Foundation
import RealmSwift
import VIAUtilities

extension Parser {
    public struct ProductFeaturesPoints {
        public static func parseProductFeaturesPoints(response: GetProductFeaturePointsResponse, completion: @escaping (_ error: Error?) -> Void) {
            Parser.main.parse {
                Parser.main.parse {
                    let parsingError = StatusProductFeatureCategory.parseAndPersistProductFeatureCategoryAndPointsInformation(response: response)
                    DispatchQueue.main.async {
                        completion(parsingError)
                    }
                }
            }
        }
    }
}

extension StatusProductFeatureCategory {
    public static func parseAndPersistProductFeatureCategoryAndPointsInformation(response: GetProductFeaturePointsResponse) -> Error? {
        let realm = DataProvider.newStatusRealm()
        realm.reset()
        
        var prodFeatureCatAndPointsInfos = Array<StatusProductFeatureCategory>()
        for productFeatureCategoryAndPointsInformation in response.productFeatureCategoryAndPointsInformations ?? [] {
            let prodFeatureCatAndPointsInfo = StatusProductFeatureCategory()
            prodFeatureCatAndPointsInfo.code = productFeatureCategoryAndPointsInformation.code
            prodFeatureCatAndPointsInfo.key = productFeatureCategoryAndPointsInformation.key
            prodFeatureCatAndPointsInfo.name = productFeatureCategoryAndPointsInformation.name
            prodFeatureCatAndPointsInfo.pointsCategoryLimit.value = productFeatureCategoryAndPointsInformation.pointsCategoryLimit
            prodFeatureCatAndPointsInfo.pointsEarned = productFeatureCategoryAndPointsInformation.pointsEarned
            prodFeatureCatAndPointsInfo.pointsEarningFlag = productFeatureCategoryAndPointsInformation.pointsEarningFlag
            prodFeatureCatAndPointsInfo.potentialPoints = productFeatureCategoryAndPointsInformation.potentialPoints
            for featurePointInformation in productFeatureCategoryAndPointsInformation.productFeatureAndPointsInformations ?? []{
                let pointsInformation: StatusProductFeaturePointsInformation = StatusProductFeaturePointsInformation.parseStatusProductFeaturePointsInformation(response: featurePointInformation)
                prodFeatureCatAndPointsInfo.productFeatureAndPointsInformations.append(pointsInformation)
            }
            prodFeatureCatAndPointsInfo.typeCode = productFeatureCategoryAndPointsInformation.typeCode
            prodFeatureCatAndPointsInfo.typeKey = productFeatureCategoryAndPointsInformation.typeKey
            prodFeatureCatAndPointsInfo.typeName = productFeatureCategoryAndPointsInformation.typeName
            
            prodFeatureCatAndPointsInfos.append(prodFeatureCatAndPointsInfo)
        }
        
        do {
            try realm.write {
                realm.add(prodFeatureCatAndPointsInfos)
            }
            return nil
        } catch  {
            return BackendError.other
        }
    }
}

extension StatusProductFeaturePointsInformation {
    public static func parseStatusProductFeaturePointsInformation(response: PFPProductFeatureAndPointsInformations) -> StatusProductFeaturePointsInformation {
        let pointsInformation = StatusProductFeaturePointsInformation()
        pointsInformation.code = response.code
        
        for statusEventType in response.eventTypes ?? [] {
            let eventType = StatusEventType.parseStatusEventType(reesponse: statusEventType)
            pointsInformation.eventTypes.append(eventType)
        }
        
        pointsInformation.key = response.key
        pointsInformation.name = response.name
        pointsInformation.pointsEarned = response.pointsEarned
        pointsInformation.pointsEarningFlag = response.pointsEarningFlag
        pointsInformation.potentialPoints = response.potentialPoints
        for statusPointsEntry in response.pointsEntries ?? [] {
            let pointsEntry = StatusPointsEntry.parseStatusPointsEntry(response: statusPointsEntry)
            pointsInformation.pointsEntries.append(pointsEntry)
        }
        for statusSubFeatures in response.productSubfeatures ?? [] {
            let subFeature = StatusProductFeaturePointsInformation.parseStatusProductSubFeaturePointsInformation(response: statusSubFeatures)
            pointsInformation.productSubFeatures.append(subFeature)
        }
        return pointsInformation
    }
    
    public static func parseStatusProductSubFeaturePointsInformation(response: PFPProductSubfeatures) -> StatusProductSubfeatures {
        let pointsInformation = StatusProductSubfeatures()
        pointsInformation.code = response.code
        
        for statusEventType in response.productSubfeatureEventTypes ?? [] {
            let eventType = StatusEventType.parseStatusSubFeatureEventType(reesponse: statusEventType)
            pointsInformation.productSubfeatureEventTypes.append(eventType)
        }
        pointsInformation.key = response.key
        pointsInformation.name = response.name
        pointsInformation.pointsEarned = response.pointsEarned
        pointsInformation.pointsEarningFlag = response.pointsEarningFlag
        pointsInformation.potentialPoints = response.potentialPoints
        for statusPointsEntry in response.productSubfeaturePointsEntries ?? [] {
            let pointsEntry = StatusPointsEntry.parseStatusSubFeaturePointsEntry(response: statusPointsEntry)
            pointsInformation.productSubfeaturePointsEntries.append(pointsEntry)
        }
        return pointsInformation
    }
}

extension StatusEventType {
    public static func parseStatusEventType(reesponse: PFPEventTypes) -> StatusEventType {
        let eventType = StatusEventType()
        eventType.code = reesponse.code
        eventType.key = reesponse.key
        eventType.name = reesponse.name
        return eventType
    }
    
    public static func parseStatusSubFeatureEventType(reesponse: PFPProductSubfeatureEventTypes) -> StatusEventType {
        let eventType = StatusEventType()
        eventType.code = reesponse.code
        eventType.key = reesponse.key
        eventType.name = reesponse.name
        return eventType
    }
}

extension StatusPointsEntry {
    public static func parseStatusPointsEntry(response: PFPPointsEntry) -> StatusPointsEntry {
        let statusPointsEntry = StatusPointsEntry()
        statusPointsEntry.categoryKey = response.categoryKey
        statusPointsEntry.categoryCode = response.categoryCode
        statusPointsEntry.categoryName = response.categoryName
        statusPointsEntry.categoryLimit.value = response.categoryLimit
        statusPointsEntry.maximumMembershipPeriodPoints = response.maximumMembershipPeriodPoints
        statusPointsEntry.pointsEarned = response.pointsEarned
        statusPointsEntry.pointsEntryLimit.value = response.pointsEntryLimit
        statusPointsEntry.potentialPoints = response.potentialPoints
        statusPointsEntry.typeCode = response.typeCode
        statusPointsEntry.typeKey = response.typeKey
        statusPointsEntry.typeName = response.typeName
        for statusPotentialPoints in response.potentialPointses ?? [] {
            let potentialPoints = StatusPotentialPointsWithTiersEntryDetails.parseStatusPointsEntryDetails(response: statusPotentialPoints)
            statusPointsEntry.potentialPointsEntries.append(potentialPoints)
        }
        return statusPointsEntry
    }
    
    public static func parseStatusSubFeaturePointsEntry(response: PFPProductSubfeaturePointsEntry) -> StatusPointsEntry {
        let statusPointsEntry = StatusPointsEntry()
        statusPointsEntry.categoryKey = response.categoryKey
        statusPointsEntry.categoryCode = response.categoryCode
        statusPointsEntry.categoryName = response.categoryName
        statusPointsEntry.categoryLimit.value = response.categoryLimit
        statusPointsEntry.maximumMembershipPeriodPoints = response.maximumMembershipPeriodPoints
        statusPointsEntry.pointsEarned = response.pointsEarned
        statusPointsEntry.pointsEntryLimit.value = response.pointsEntryLimit
        statusPointsEntry.potentialPoints = response.potentialPoints
        statusPointsEntry.typeCode = response.typeCode
        statusPointsEntry.typeKey = response.typeKey
        statusPointsEntry.typeName = response.typeName
        for statusPotentialPoints in response.productSubfeaturePotentialPointses ?? [] {
            let potentialPoints = StatusPotentialPointsWithTiersEntryDetails.parseStatusPointsEntryDetails(response: statusPotentialPoints)
            statusPointsEntry.potentialPointsEntries.append(potentialPoints)
        }
        return statusPointsEntry
    }
}

extension StatusPotentialPointsWithTiersEntryDetails {
    public static func parseStatusPointsEntryDetails(response: PFPPotentialPoints) -> StatusPotentialPointsWithTiersEntryDetails {
        let potentialPoints = StatusPotentialPointsWithTiersEntryDetails()
        potentialPoints.potentialPoints.value = response.potentialPoints
        for statusConditions in response.conditions ?? [] {
            potentialPoints.conditions.append(StatusPotentialPointsWithTiersConditions.parseStatusPotentialPointsConditions(response: statusConditions))
        }
        return potentialPoints
    }

    public static func parseStatusPointsEntryDetails(response: PFPProductSubfeaturePotentialPoints) -> StatusPotentialPointsWithTiersEntryDetails {
        let potentialPoints = StatusPotentialPointsWithTiersEntryDetails()
        potentialPoints.potentialPoints.value = response.potentialPoints
        for statusConditions in response.productSubfeatureConditions ?? [] {
            potentialPoints.conditions.append(StatusPotentialPointsWithTiersConditions.parseStatusPotentialPointsConditions(response: statusConditions))
        }
        return potentialPoints
    }
}

extension StatusPotentialPointsWithTiersConditions {
    public static func parseStatusPotentialPointsConditions(response: PFPConditions) -> StatusPotentialPointsWithTiersConditions {
        let statusCondition = StatusPotentialPointsWithTiersConditions()
        statusCondition.greaterThan.value = response.greaterThan
        statusCondition.greaterThanOrEqual.value = response.greaterThanOrEqualTo
        statusCondition.lessThan.value = response.lessThan
        statusCondition.lessThanOrEqual.value = response.lessThanOrEqualTo
        statusCondition.metaTypeKey.value = response.metadataTypeKey
        statusCondition.metaTypeCode = response.metadataTypeCode
        statusCondition.metaTypeName = response.metadataTypeName
        if let unitOfMeasureString = response.unitOfMeasure {
            let unitOfMeasure = Int(unitOfMeasureString) ?? UnitOfMeasureRef.Unknown.rawValue
            statusCondition.UOM = UnitOfMeasureRef(rawValue: unitOfMeasure) ?? .Unknown
        }
        
        return statusCondition 
    }
    
    public static func parseStatusPotentialPointsConditions(response: PFPProductSubfeatureConditions) -> StatusPotentialPointsWithTiersConditions {
        let statusCondition = StatusPotentialPointsWithTiersConditions()
        statusCondition.greaterThan.value = response.greaterThan
        statusCondition.greaterThanOrEqual.value = response.greaterThanOrEqualTo
        statusCondition.lessThan.value = response.lessThan
        statusCondition.lessThanOrEqual.value = response.lessThanOrEqualTo
        statusCondition.metaTypeKey.value = response.metadataTypeKey
        statusCondition.metaTypeCode = response.metadataTypeCode
        statusCondition.metaTypeName = response.metadataTypeName
        if let unitOfMeasureString = response.unitOfMeasure {
            let unitOfMeasure = Int(unitOfMeasureString) ?? UnitOfMeasureRef.Unknown.rawValue
            statusCondition.UOM = UnitOfMeasureRef(rawValue: unitOfMeasure) ?? .Unknown
        }
        
        return statusCondition
    }
}
