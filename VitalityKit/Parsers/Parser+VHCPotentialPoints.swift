import Foundation
import RealmSwift
import VIAUtilities

extension Parser {
    public struct VHC {

        public static func parseGetPotentialPoints(response: GetVHCPotentialPointsResponse, completion: @escaping (_ error: Error?) -> Void) {
            Parser.main.parse {
                VHCEvent.persistGetPotentialPoints(response: response)
                DispatchQueue.main.async {
                    completion(nil)
                }
            }
        }
    }
}

extension VHCEvent {

    public static func persistGetPotentialPoints(response: GetVHCPotentialPointsResponse) {

        let vhcRealm = DataProvider.newVHCRealm()
        let serviceNumberFormatter = NumberFormatter.serviceFormatter()
        let potentialPointsEventTypes = response.eventType
        try! vhcRealm.write {
            vhcRealm.deleteOldVHCData()

            if let eventTypes = potentialPointsEventTypes {
                for eventType in eventTypes {
                    let eventTypeModel = VHCEventType()
                    eventTypeModel.totalEarnedPoints = eventType.totalEarnedPoints ?? 0
                    eventTypeModel.totalPotentialPoints = eventType.totalPotentialPoints ?? 0
                    eventTypeModel.typeName = eventType.typeName ?? ""
                    eventTypeModel.type = EventTypeRef(rawValue: eventType.typeKey) ?? .Unknown
                    eventTypeModel.reasonKey.value = eventType.reasonKey
                    eventTypeModel.reasonCode = eventType.reasonCode
                    eventTypeModel.reasonName = eventType.reasonName

                    if let events = eventType.event {
                        for event in events {
                            let eventModel = VHCEvent()
                            eventModel.eventDateTime = Parser.main.parseDateString(event.eventDateTime)
                            eventModel.eventId = event.eventId ?? 0
                            if let eventSource = event.eventSource {
                                let eventSourceModel = VHCEventSource()
                                eventSourceModel.type = EventSourceRef(rawValue: eventSource.eventSourceKey) ?? .Unknown
                                eventSourceModel.eventSourceName = eventSource.eventSourceName ?? ""
                                eventSourceModel.note = eventSource.note ?? ""

                                eventModel.eventSource = eventSourceModel
                            }
                            if let pointsEntries = event.pointsEntries {
                                for pointsEntry in pointsEntries {
                                    let pointsEntryModel = VHCEventPointsEntry()
                                    pointsEntryModel.typeCode = pointsEntry.typeCode ?? ""
                                    pointsEntryModel.typeKey = pointsEntry.typeKey
                                    pointsEntryModel.typeName = pointsEntry.typeName ?? ""

                                    pointsEntryModel.categoryCode = pointsEntry.categoryCode ?? ""
                                    pointsEntryModel.categoryKey = pointsEntry.categoryKey ?? 0
                                    pointsEntryModel.categoryName = pointsEntry.categoryName ?? ""

                                    pointsEntryModel.id = pointsEntry.id ?? 0
                                    pointsEntryModel.potentialValue = pointsEntry.potentialValue ?? 0
                                    pointsEntryModel.earnedValue = pointsEntry.earnedValue ?? 0
                                    pointsEntryModel.preLimitValue = pointsEntry.preLimitValue ?? 0

                                    if let reasons = pointsEntry.reason {
                                        for reason in reasons {
                                            let reasonModel = VHCPointsReason()
                                            reasonModel.reasonCode = reason.reasonCode ?? ""
                                            reasonModel.reasonKey = reason.reasonKey // TODO ?? 0
                                            reasonModel.reasonName = reason.reasonName ?? ""

                                            reasonModel.categoryCode = reason.categoryCode ?? ""
                                            reasonModel.categoryKey = reason.categoryKey
                                            reasonModel.categoryName = reason.categoryName ?? "" // TODO

                                            pointsEntryModel.reasons.append(reasonModel)
                                        }
                                    }
                                    eventModel.pointsEntries.append(pointsEntryModel)
                                }

                            }
                            if let eventMetaDatas = event.eventMetaDataType {
                                for metaData in eventMetaDatas {
                                    let eventMetaDataModel = VHCEventMetaData()
                                    eventMetaDataModel.code = metaData.code ?? ""
                                    eventMetaDataModel.key = metaData.key ?? 0
                                    eventMetaDataModel.name = metaData.name ?? ""
                                    eventMetaDataModel.note = metaData.note ?? ""
                                    eventMetaDataModel.unitOfMeasure = metaData.unitOfMeasure ?? ""
                                    eventMetaDataModel.rawValue = metaData.value

                                    if let value = metaData.value {
                                        eventMetaDataModel.reading.value = serviceNumberFormatter.number(from: value) as? Double
                                    }

                                    eventModel.eventMetaData.append(eventMetaDataModel)
                                }
                            }
                            if let healthAttributeReadings = event.healthAttributeReadings {
                                for healthReading in healthAttributeReadings {
                                    let healthReadingModel = VHCHealthAttributeReading()
                                    healthReadingModel.healthAttributeTypeCode = healthReading.healthAttributeTypeCode ?? ""
                                    healthReadingModel.healthAttributeTypeKey = healthReading.healthAttributeTypeKey
                                    healthReadingModel.healthAttributeTypeName = healthReading.healthAttributeTypeName ?? ""
                                    healthReadingModel.unitOfMeasure = healthReading.unitOfMeasure ?? ""
                                    healthReadingModel.value.value = serviceNumberFormatter.number(from: healthReading.value) as? Double
                                    healthReadingModel.rawValue = healthReading.value
                                    healthReadingModel.measuredOn = Parser.main.parseDateString(healthReading.measuredOn)

                                    if let healthAttributeFeedbacks = healthReading.healthAttributeFeedbacks {
                                        for feedback in healthAttributeFeedbacks {
                                            let healthFeedbackModel = VHCHealthAttributeFeedback()
                                            healthFeedbackModel.feedbackTypeName = feedback.feedbackTypeName ?? ""
                                            healthFeedbackModel.feedbackCategoryName = feedback.feedbackTypeTypeName ?? ""
                                            healthFeedbackModel.category = PartyAttributeFeedbackTypeRef(rawValue: feedback.feedbackTypeTypeKey) ?? .Unknown
                                            healthFeedbackModel.type = PartyAttributeFeedbackRef(rawValue: feedback.feedbackTypeKey) ?? .Unknown

                                            healthReadingModel.healthAttributeFeedbacks.append(healthFeedbackModel)
                                        }
                                    }
                                    eventModel.healthAttributeReadings.append(healthReadingModel)
                                }
                            }
                            eventTypeModel.events.append(eventModel)
                        }
                    }
                    if let healthAttributes = eventType.healthAttribute {
                        for healthAttribute in healthAttributes {
                            let healthAttributeModel = VHCHealthAttribute()

                            healthAttributeModel.typeName = healthAttribute.typeName ?? ""
                            if let attributeModelType = PartyAttributeTypeRef(rawValue: healthAttribute.typekey) {
                                healthAttributeModel.type = attributeModelType
                            }

                            if let validValues = healthAttribute.healthAttributeTypeValidValueses {
                                for validValue in validValues {
                                    let validValueModel = VHCHealthAttributeValidValues()
                                    if let lowerLimit = validValue.lowerLimit {
                                        validValueModel.lowerLimit.value = serviceNumberFormatter.number(from: lowerLimit) as? Double
                                    }
                                    if let upperLimit = validValue.upperLimit {
                                        validValueModel.upperLimit.value = serviceNumberFormatter.number(from: upperLimit) as? Double
                                    }
                                    if let minValue = validValue.minValue {
                                        validValueModel.minValue = minValue
                                    }
                                    if let maxValue = validValue.maxValue {
                                        validValueModel.maxValue = maxValue
                                    }
                                    validValueModel.typeTypeName = validValue.typeTypeName ?? ""
                                    if let values = validValue.validValuesList {
                                        validValueModel.validValuesList = values.joined(separator: ";")
                                    }
                                    if let uom = validValue.unitofMeasure {
                                        let unitOfMeasure = Int(uom) ?? UnitOfMeasureRef.Unknown.rawValue
                                        validValueModel.unitOfMeasureType = UnitOfMeasureRef(rawValue: unitOfMeasure) ?? .Unknown
                                    } else {
                                        debugPrint("No unit of measure returned for validValue: \(validValueModel)")
                                    }
                                    validValueModel.type = validValue.typeTypeKey

                                    healthAttributeModel.validValues.append(validValueModel)
                            }
                        }

                            eventTypeModel.healthAttributes.append(healthAttributeModel)
                        }
                    }
                    vhcRealm.create(VHCEventType.self, value:eventTypeModel, update: true)
                }
            }
        }
//  VHC caching turned off as requested by client until in app notifications implemented
//        setVHCCache()
        persistHealthAttributeGroups()
    }

    static func setVHCCache() {
        Realm.deleteCurrentVHCCache()
        let vhcRealm = DataProvider.newVHCRealm()
        try! vhcRealm.write {
            let vhcCache = VHCCache()
            vhcRealm.add(vhcCache)
        }
    }

    public static func persistHealthAttributeGroups() {

        let vhcRealm = DataProvider.newVHCRealm()
        let coreRealm = DataProvider.newRealm()

        try! vhcRealm.write {
            // get all the configured VHC features for the VHC featuretypes,
            // loop through them and create a group per type if there are
            // any features in that group. this in turn creates the link
            // between the feature and the health attribute, which we ultimately
            // want to sort into groups
            let vhcFeatureTypes = VitalityProductFeature.allVHCFeatureTypes()
            //var cholesterolFeatures = coreRealm.allProductFeatures()
            for type in vhcFeatureTypes {

//                if(type.rawValue == 11){
//                    cholesterolFeatures = coreRealm.productFeatures(of: type)
//                }
//                if(type.rawValue == 39){
//                    features = cholesterolFeatures
//                }else{
//                    features = coreRealm.productFeatures(of: type)
//                }
        
                let features = coreRealm.productFeatures(of: type)
                if features.count == 0 {
                    debugPrint("VHC Group \(type.rawValue) has no features, skipping")
                    continue
                }
                //===============================================
                let group = VHCHealthAttributeGroup()
                group.type = type
                for feature in features {
                    // get HA and append to group
                    if let healthAttribute = vhcRealm.vhcHealthAttribute(of: feature.healthAttributeType()) {
//                        debugPrint("Adding \(healthAttribute.typeName) to VHC group \(String(describing: group.type.rawValue))")
                        //ge20180105 : another way to MOCK : if featureType is 11, limit to ldl & try, if featureType is 39, limit to totcholesterol & hdl & lipid ratio
//                        if(type.rawValue == 11){
//                            if(healthAttribute.type.rawValue == 5 || healthAttribute.type.rawValue == 13){
//                                group.healthAttributes.append(healthAttribute)
//                            }
//                        }else if(type.rawValue == 39){
//                            if(!(healthAttribute.type.rawValue == 5 || healthAttribute.type.rawValue == 13)){
//                                group.healthAttributes.append(healthAttribute)
//                            }
//                        }else{
//                           group.healthAttributes.append(healthAttribute)
//                        }
                        group.healthAttributes.append(healthAttribute)
                        
                    }
                }
                //===============================================

                vhcRealm.create(VHCHealthAttributeGroup.self, value:group, update: true)
            }
        }
    }
}

extension Realm {
    public func deleteOldVHCData() {
        let oldPotentialPointsEventTypes = self.allVHCPotentialPointsEventTypes()
        self.delete(oldPotentialPointsEventTypes)
        let oldEventsPointsEntry = self.allVHCPointsEntries()
        self.delete(oldEventsPointsEntry)
        let oldPointsReasons = self.allVHCPointsReasons()
        self.delete(oldPointsReasons)
        let oldEvents = self.allVHCEvents()
        self.delete(oldEvents)
        let oldEventSources = self.allVHCEventSources()
        self.delete(oldEventSources)
        let oldEventMetaData = self.allVHCEventMetaData()
        self.delete(oldEventMetaData)
        let oldHealthAttributes = self.allVHCHealthAttributes()
        self.delete(oldHealthAttributes)
        let oldHealthAttributeReadings = self.allVHCHealthAttributeReadings()
        self.delete(oldHealthAttributeReadings)
        let oldHealthAttributeTypeValidValues = self.allVHCHealthAttributeValidValues()
        self.delete(oldHealthAttributeTypeValidValues)
        let oldHealthAttributeFeedback = self.allVHCHealthAttributeFeedback()
        self.delete(oldHealthAttributeFeedback)
        let oldHealthAttributeGroups = self.allVHCHealthAttributeGroups()
        self.delete(oldHealthAttributeGroups)
    }
}
