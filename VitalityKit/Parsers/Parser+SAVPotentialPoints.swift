//
//  Parser+SAVPotentialPoints.swift
//  VitalityActive
//
//  Created by Michelle Oratil on 25/11/2017.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import RealmSwift
import VIAUtilities

extension Parser {
    public struct SAV {
        
        public static func parseGetPotentialPoints(response: GetSAVPotentialPointsResponse, deleteOldData:Bool = true, completion: @escaping (_ error: Error?) -> Void) {
            Parser.main.parse {
                SAVEvent.persistGetPotentialPoints(response: response, deleteOldData: deleteOldData, completion: completion)
            }
        }
    }
}

extension SAVEvent {
    
    public static func persistGetPotentialPoints(response: GetSAVPotentialPointsResponse, deleteOldData:Bool = true, completion: @escaping (_ error: Error?) -> Void) {
        
        let savRealm = DataProvider.newSAVRealm()
        let serviceNumberFormatter = NumberFormatter.serviceFormatter()
        let potentialPointsEventTypes = response.eventType
        try! savRealm.write {
            if deleteOldData{
                savRealm.deleteOldSAVData()
            }
            
            if let eventTypes = potentialPointsEventTypes {
                for eventType in eventTypes {
                    let eventTypeModel = SAVEventType()
                    eventTypeModel.totalEarnedPoints = eventType.totalEarnedPoints ?? 0
                    eventTypeModel.totalPotentialPoints = eventType.totalPotentialPoints ?? 0
                    eventTypeModel.typeName = eventType.typeName ?? ""
                    eventTypeModel.type = EventTypeRef(rawValue: eventType.typeKey) ?? .Unknown
                    eventTypeModel.reasonKey.value = eventType.reasonKey
                    eventTypeModel.reasonCode = eventType.reasonCode
                    eventTypeModel.reasonName = eventType.reasonName
                    eventTypeModel.categoryCode = eventType.categoryCode
                    eventTypeModel.categoryType = EventCategoryRef(rawValue: eventType.categoryKey) ?? .Unknown
                    
                    if let events = eventType.event {
                        for event in events {
                            let eventModel = SAVEvent()
                            eventModel.eventDateTime = Parser.main.parseDateString(event.eventDateTime)
                            eventModel.eventId = event.eventId ?? 0
                            if let eventSource = event.eventSource {
                                let eventSourceModel = SAVEventSource()
                                eventSourceModel.type = EventSourceRef(rawValue: eventSource.eventSourceKey) ?? .Unknown
                                eventSourceModel.eventSourceName = eventSource.eventSourceName ?? ""
                                eventSourceModel.note = eventSource.note ?? ""
                                
                                eventModel.eventSource = eventSourceModel
                            }
                            if let pointsEntries = event.pointsEntries {
                                for pointsEntry in pointsEntries {
                                    let pointsEntryModel = SAVEventPointsEntry()
                                    pointsEntryModel.typeCode = pointsEntry.typeCode ?? ""
                                    pointsEntryModel.typeKey = pointsEntry.typeKey
                                    pointsEntryModel.typeName = pointsEntry.typeName ?? ""
                                    
                                    pointsEntryModel.categoryCode = pointsEntry.categoryCode ?? ""
                                    pointsEntryModel.categoryKey = pointsEntry.categoryKey ?? 0
                                    pointsEntryModel.categoryName = pointsEntry.categoryName ?? ""
                                    
                                    pointsEntryModel.id = pointsEntry.id ?? 0
                                    pointsEntryModel.potentialValue = pointsEntry.potentialValue ?? 0
                                    pointsEntryModel.earnedValue = pointsEntry.earnedValue ?? 0
                                    pointsEntryModel.preLimitValue = pointsEntry.preLimitValue ?? 0
                                    
                                    if let reasons = pointsEntry.reason {
                                        for reason in reasons {
                                            let reasonModel = SAVPointsReason()
                                            reasonModel.reasonCode = reason.reasonCode ?? ""
                                            reasonModel.reasonKey = reason.reasonKey // TODO ?? 0
                                            reasonModel.reasonName = reason.reasonName ?? ""
                                            
                                            reasonModel.categoryCode = reason.categoryCode ?? ""
                                            reasonModel.categoryKey = reason.categoryKey
                                            reasonModel.categoryName = reason.categoryName ?? "" // TODO
                                            
                                            pointsEntryModel.reasons.append(reasonModel)
                                        }
                                    }
                                    eventModel.pointsEntries.append(pointsEntryModel)
                                }
                                
                            }
                            if let eventMetaDatas = event.eventMetaDataType {
                                for metaData in eventMetaDatas {
                                    let eventMetaDataModel = SAVEventMetaData()
                                    eventMetaDataModel.code = metaData.code ?? ""
                                    eventMetaDataModel.key = metaData.key ?? 0
                                    eventMetaDataModel.name = metaData.name ?? ""
                                    eventMetaDataModel.note = metaData.note ?? ""
                                    eventMetaDataModel.unitOfMeasure = metaData.unitOfMeasure ?? ""
                                    eventMetaDataModel.rawValue = metaData.value
                                    
                                    if let value = metaData.value {
                                        eventMetaDataModel.reading.value = serviceNumberFormatter.number(from: value) as? Double
                                    }
                                    
                                    eventModel.eventMetaData.append(eventMetaDataModel)
                                }
                            }
                            eventTypeModel.events.append(eventModel)
                        }
                    }
                    
                    savRealm.create(SAVEventType.self, value:eventTypeModel, update: true)
                }
            }
        }
        persistScreeningAndVaccinationGroups()
        DispatchQueue.main.async {
            completion(nil)
        }
    }
    
    static func setSAVCache() {
        Realm.deleteCurrentSAVCache()
        let savRealm = DataProvider.newSAVRealm()
        try! savRealm.write {
            let savCache = SAVCache()
            savRealm.add(savCache)
        }
    }
    
    public static func persistScreeningAndVaccinationGroups() {
        
        let savRealm = DataProvider.newSAVRealm()
        let coreRealm = DataProvider.newRealm()
        
        try! savRealm.write {
            // get all the configured VHC features for the VHC featuretypes,
            // loop through them and create a group per type if there are
            // any features in that group. this in turn creates the link
            // between the feature and the health attribute, which we ultimately
            // want to sort into groups
            let savFeatureTypes = VitalityProductFeature.allSAVFeatureTypes()
            for type in savFeatureTypes {
                
                let features = coreRealm.productFeatures(of: type)
                if features.count == 0 {
                    debugPrint("SAV Group \(type.rawValue) has no features, skipping")
                    continue
                }
                
                let savGroup = SAVEventTypeGroup()
                savGroup.type = type
                for feature in features {
                    
                    /* Get EventType and append to EventTypeGroup */
                    
                    if let eventType = feature.eventType() {
                        if let savEventType = savRealm.SAV_eventType(for: eventType) {
//                            debugPrint("Adding \(savEventType.typeName) to SAV group \(String(describing: savGroup.type.rawValue))")
                            savGroup.eventTypes.append(savEventType)
                        }
                    }
                }
                
                savRealm.create(SAVEventTypeGroup.self, value: savGroup, update: true)
            }
        }
    }
}

extension Realm {
    public func deleteOldSAVData() {
        let oldPotentialPointsEventTypes = self.allSAVPotentialPointsEventTypes()
        self.delete(oldPotentialPointsEventTypes)
        let oldEventsPointsEntry = self.allSAVPointsEntries()
        self.delete(oldEventsPointsEntry)
        let oldPointsReasons = self.allSAVPointsReasons()
        self.delete(oldPointsReasons)
        let oldEvents = self.allSAVEvents()
        self.delete(oldEvents)
        let oldEventSources = self.allSAVEventSources()
        self.delete(oldEventSources)
        let oldEventMetaData = self.allSAVEventMetaData()
        self.delete(oldEventMetaData)
        
    }
}
