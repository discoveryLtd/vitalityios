import Foundation
import VIAUtilities

public extension MetadataContainer {
 
    public func format() -> String {
        let dateParser = DateParser()
        let metaDataType = self.typeKey
        let metaDataValue = self.value
        
        switch metaDataType {
        case .StartTime,
             .EndTime:
            guard let date = dateParser.parseDateString(metaDataValue) else { return metaDataValue }
            let formattedDate = Localization.shortTimeFormatter.string(from: date)
            return formattedDate
        case .Duration:
            guard let value = Localization.serviceFormatter.number(from: metaDataValue)?.doubleValue else { return metaDataValue }
            let converted = value.toHoursMinutesSeconds()
            let formattedDuration = CommonStrings.Ar.EventDetail.DurationHhMmSs851("\(converted.0)", "\(converted.1)", "\(converted.2)")
            return formattedDuration
        case .TotEnergyExpenditure:
            guard let value = Localization.serviceFormatter.number(from: metaDataValue)?.doubleValue else { return metaDataValue }
            let measurementValue = Self.totalEnergyExpenditureMeasurement(unitOfMeasure: self.unitOfMeasureType.unit(), metadataValue: value)
            return measurementValue
        case .Distance:
            guard let value = Localization.serviceFormatter.number(from: metaDataValue)?.doubleValue else { return metaDataValue }
            let measurementValue = Self.distanceMeasurement(unitOfMeasure: self.unitOfMeasureType.unit(), metadataValue: value)
            return measurementValue
        case .AverageSpeed,
             .MaximumSpeed,
             .MinimumSpeed:
            guard let value = Localization.serviceFormatter.number(from: metaDataValue)?.doubleValue else { return metaDataValue }
            let measurementValue = Self.speedMeasurement(unitOfMeasure: self.unitOfMeasureType.unit(), metadataValue: value)
            return measurementValue
        case .AveragePace,
             .MaximumPace,
             .MinimumPace:
            guard let value = Localization.serviceFormatter.number(from: metaDataValue)?.doubleValue else { return metaDataValue }
            let measurementValue = Self.paceMeasurement(unitOfMeasure: self.unitOfMeasureType.unit(), metadataValue: value)
            return measurementValue
        case .HeartRate,
             .HeartBeatCount,
             .ECGHeartRate,
             .MeanHeartRate,
             .AverageHeartRate,
             .MaximumHeartRate,
             .MinimumHeartRate,
             .RestingHeartRate:
            guard let value = Localization.serviceFormatter.number(from: metaDataValue)?.doubleValue else { return metaDataValue }
            let measurementValue = Self.bpmMeasurement(unitOfMeasure: self.unitOfMeasureType.unit(), metadataValue: value)
            return measurementValue
        case .TotalSteps,
             .FloorsAscended:
            guard let value = Localization.serviceFormatter.number(from: metaDataValue) else { return metaDataValue }
            guard let formattedValue = Localization.integerFormatter.string(from: value as NSNumber) else { return metaDataValue }
            return formattedValue
        default:
            return metaDataValue
        }
        
    }
    
    private static func totalEnergyExpenditureMeasurement(unitOfMeasure: Unit, metadataValue: Double) -> String {
        let measurement = Measurement(value: metadataValue, unit: unitOfMeasure)
        let formatter = Localization.decimalMediumStyleForcingProvidedUnit
        let formattedValue = formatter.string(from: measurement)
        return formattedValue
    }
    
    private static func distanceMeasurement(unitOfMeasure: Unit, metadataValue: Double) -> String {
        let measurement = Measurement(value: metadataValue, unit: unitOfMeasure)
        let formatter = Localization.decimalMediumStyleForcingProvidedUnit
        let formattedValue = formatter.string(from: measurement)
        return formattedValue
    }
    
    private static func speedMeasurement(unitOfMeasure: Unit, metadataValue: Double) -> String {
        let measurement = Measurement(value: metadataValue, unit: unitOfMeasure)
        let formatter = Localization.decimalMediumStyleForcingProvidedUnit
        let formattedValue = formatter.string(from: measurement)
        return formattedValue
    }
    
    private static func paceMeasurement(unitOfMeasure: Unit, metadataValue: Double) -> String {
        let measurement = Measurement(value: metadataValue, unit: unitOfMeasure)
        let formatter = Localization.decimalMediumStyleForcingProvidedUnit
        let formattedValue = formatter.string(from: measurement)
        return formattedValue
    }
    
    private static func bpmMeasurement(unitOfMeasure: Unit, metadataValue: Double) -> String {
        let measurement = Measurement(value: metadataValue, unit: unitOfMeasure)
        let formatter = Localization.integerStyle
        let formattedValue = formatter.string(from: measurement)
        return formattedValue
    }
    
}
