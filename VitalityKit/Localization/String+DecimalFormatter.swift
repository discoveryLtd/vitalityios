//
//  String+DecimalFormatter.swift
//  VitalityActive
//
//  Created by OJ Garde on 2/3/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation

extension String{
    
    public func localizeDecimalFormat() -> String?{
        
        var val = self.replacingOccurrences(of: Locale.current.decimalSeparator ?? ".", with: ".")
        
        /** FC-25717 : Generali Germany
         * Does not accept the value even if it is valid.
         */
        let currentLanguageCode = Locale.current.languageCode
        switch currentLanguageCode {
            /** Avoid the replacing occurences that causes the decimal value to be a whole number.
             * Example: Entered Value - (1,8) Result Value - (18)
             */
        case "es", "de", "nl": break 
        default:
            // The checker above is to make sure that the decimal separator (which is a grouping separator on 'es') isn't replaced by "".
            val = val.replacingOccurrences(of: Locale.current.groupingSeparator ?? ",", with: "")
            break
        }
        
        if let doubleNum = Double(val){
            let nsNumber = NSNumber(value: doubleNum)
            return NumberFormatter.localizedString(from: nsNumber, number: .decimal)            
        }
        
        return nil
    }
    
    public func removeDecimalFormatter() -> String? {
        var val = self.replacingOccurrences(of: Locale.current.decimalSeparator ?? ".", with: ".")
        val = val.replacingOccurrences(of: Locale.current.groupingSeparator ?? ",", with: "")
        
        return val
    }
    
    public func removeNumberGroupingSeparator() -> String? {
        let val = self.replacingOccurrences(of: Locale.current.groupingSeparator ?? ",", with: "")
        
        return val
    }
}
