import Foundation
import VIAUtilities

public extension EventTypeRef {
    
    public func asSource() -> String {
        switch self {
        case .BMI, .WaistCircum, .HeightCaptured, .WeightCaptured, .BloodPressure, .DiastolicPressure, .SystolicPressure, .FastingGlucose, .RandomGlucose, .HDLCholesterol, .LDLCholestrol, .TotCholesterol, .TriGlycCholesterol, .HbA1c, .UrinaryTest, .UrinaryTestAss, .LipidRatioCalculated:
            return CommonStrings.Assessment.Source.Vhc571
        case .DailyMealsComp, .FoodChoicesComp, .LifestyleChoicesComp:
            return CommonStrings.Assessment.Source.Vna573
        case .GnrlHealthComp, .LifestyleComp, .SocialHabitsComp:
            return CommonStrings.Assessment.Source.Vhr572
        case .DeviceDataUpload, .DeviceSyncing:
            return CommonStrings.Assessment.Source.Device574
        case .SocialAssComp, .StressorAssComp, .PsychologicalAssComp:
            return CommonStrings.Onboarding.Title1195
        default:
            return CommonStrings.GenericNotAvailable270
        }
    }

}
