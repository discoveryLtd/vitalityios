import Foundation
import VIAUtilities

extension UnitPerDay {

    public override var symbol: String {
        return CommonStrings.Assessment.UnitOfMeasure.PerDay505
    }

}

extension UnitPerWeek {

    public override var symbol: String {
        return CommonStrings.Assessment.UnitOfMeasure.PerWeek506
    }

}

extension UnitDays {
    
    public override var symbol: String {
        return CommonStrings.Assessment.UnitOfMeasure.DaysText642
    }
    
}

extension UnitBeatsPerMinute {
    
    public override var symbol: String {
        return CommonStrings.BpmUnitOfMeasure2189
    }
    
}

extension UnitMilligramsPerDeciLitre {
    
    public override var symbol: String {
        return CommonStrings.Assessment.UnitOfMeasure.MilligramsPerDeciliterAbbreviation2171
    }
}

