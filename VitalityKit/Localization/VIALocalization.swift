import Foundation
import VIAUtilities

public struct Localization {

    // TODO: add namespaces to better organise these formatters

    // MARK: DateFormatters

    public static let yearMonthDayFormatter = DateFormatter.yearMonthDayFormatter()

    public static let dayMonthAndYearFormatter = DateFormatter.dayMonthAndYearFormatter()

    public static let shortMonthAndYearFormatter = DateFormatter.shortMonthAndYearFormatter()

    public static let fullMonthAndYearFormatter = DateFormatter.fullMonthAndYearFormatter()

    public static let dayDateShortMonthFormatter = DateFormatter.dayDateShortMonthFormatter()
    
    public static let dayDateMonthYearFormatter = DateFormatter.dayDateMonthYearFormatter()

    public static let shortDayShortMonthFormatter = DateFormatter.shortDayShortMonthFormatter()

    public static let dayDateShortMonthyearFormatter = DateFormatter.dayDateShortMonthyearFormatter()

	public static let dayDateLongMonthyearFormatter = DateFormatter.dayDateLongMonthYearFormatter()

    public static let fullDateFormatter = DateFormatter.fullDateFormatter()
    
    public static let mediumDateFormatter = DateFormatter.mediumDateFormatter()

    public static let dayLongMonthLongYearShortTimeFormatter = DateFormatter.dayLongMonthLongYearShortTimeFormatter()

    public static let shortTimeFormatter = DateFormatter.shortTimeFormatter()

    // MARK: NumberFormatters

    public static let integerFormatter = NumberFormatter.integerFormatter()

    public static let decimalFormatter = NumberFormatter.decimalFormatter()
    
    public static let oneDecimalFormatter = NumberFormatter.oneDecimalFormatter()

    public static let serviceFormatter = NumberFormatter.serviceFormatter()

    // MARK: MeasurementFormatters

    public static let decimalShortStyle = MeasurementFormatter.decimalShortStyle()
    
    public static let decimalMediumStyleForcingProvidedUnit = MeasurementFormatter.decimalMediumStyleForcingProvidedUnit()
    
    public static let integerStyle = MeasurementFormatter.integerStyle()
    
}
