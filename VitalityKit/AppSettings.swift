import Foundation
import VIAUtilities

public class AppSettings: NSObject {
    
    private static let shareVitalityStatusUsernameKey = "shareVitalityStatusUsernameKey"
    private static let touchIdUsernameKey = "touchIdUsernameKey"
    private static let rememberMeUsernameKey = "rememberMeUsernameKey"
    private static let rememberMeUsernameEnvKey = "rememberMeUsernameEnvKey"
    private static let firstLoginUsernameForRememberMe = "firstLoginUsernameForRememberMe"
    private static let analyticsPreferenceKey = "analyticsPreferencekey"
    private static let crashReportPreferenceKey = "crashReportPreferenceKey"
    private static let lastLoginDateKey = "lastLoginDateKey"
    private static let hasShownUserPreferencesKey = "hasShownUserPreferencesKey"
    private static let hasShownVHCOnboardingKey = "hasShownVHCOnboardingKey"
    private static let hasShownVHROnboardingKey = "hasShownVHROnboardingKey"
    private static let hasShownMWBOnboardingKey = "hasShownMWBOnboardingKey"
    private static let hasShownVNAOnboardingKey = "hasShownVNAOnboardingKey"
    private static let hasShownDeviceCashbackOnboardingKey = "hasShownDeviceCashbackOnboardingKey"
    private static let hasShownAppleWatchCashbackOnboardingKey = "hasShownAppleWatchCashbackOnboardingKey"
    private static let hasShownOrganisedFitnessEventsOnboardingKey = "hasShownOrganisedFitnessEventsOnboardingKey"
    private static let hasShownMyHealthOnboardingKey = "hasShownMyHealthOnboardingKey"
    private static let hasShownVitalityAgeModalKey = "hasShownVitalityAgeModalKey"
    private static let hasShownVitalityAgePromptModalKey = "hasShownVitalityAgePromptModalKey"
    private static let hasShownWellnessDevicesOnboardingKey = "hasShownWellnessDevicesOnboardingKey"
    private static let hasShownVitalityStatusOnboardingKey = "hasShownVitalityStatusOnboardingKey"
    private static let hasShownRewardsOnboardingKey = "hasShownRewardsOnboardingKey"
    private static let hasVHCCompletedKey = "hasVHCCompletedKey"
    private static let hasVHCUpdatedKey = "hasVHCUpdatedKey"
    private static let hasShownSAVOnboardingKey = "hasShownSAVOnboardingKey"
    private static let touchIdDomainStateKey = "touchIdDomainStateKey"
    private static let notificationsUsernameKey = "notificationsUsernameKey"
    private static let hasLinkedAppleHealthDeviceKey = "hasLinkedAppleHealthDeviceKey"
    private static let hasEnteredValidAssessmentAnswers = "hasEnteredValidAssessmentAnswers"
    private static let hasUpdatedNumberRange = "hasUpdatedNumberRange"
    private static let ALL_KEYS = [shareVitalityStatusUsernameKey, touchIdUsernameKey, rememberMeUsernameKey, rememberMeUsernameEnvKey, firstLoginUsernameForRememberMe, analyticsPreferenceKey, crashReportPreferenceKey, lastLoginDateKey, hasShownUserPreferencesKey, hasShownVHCOnboardingKey, hasShownVHROnboardingKey, hasShownVNAOnboardingKey, hasShownDeviceCashbackOnboardingKey, hasShownDeviceCashbackOnboardingKey, hasShownOrganisedFitnessEventsOnboardingKey, hasShownMyHealthOnboardingKey, hasShownVitalityAgeModalKey, hasShownVitalityAgePromptModalKey, hasShownWellnessDevicesOnboardingKey, hasShownVitalityStatusOnboardingKey, hasShownRewardsOnboardingKey, hasVHCCompletedKey, hasVHCUpdatedKey, hasShownSAVOnboardingKey, touchIdDomainStateKey, touchIdDomainStateKey, notificationsUsernameKey, hasLinkedAppleHealthDeviceKey, hasEnteredValidAssessmentAnswers, hasUpdatedNumberRange, screenIndexFromNotification, notificationIsInApp]

    private static let homeScreenLoading = "homeScreenLoading"

    public class func stopHomeScreenLoading(){
        UserDefaults.standard.set(false, forKey: homeScreenLoading)
    }
    
    private static let screenIndexFromNotification = "screenIndexFromNotification"
    
    public class func getSelectedScreenFromNotification() -> Int {
        return UserDefaults.standard.object(forKey: screenIndexFromNotification) as? Int ?? 0
    }
    
    public class func setSelectedScreenFromNotification(screen: Int){
        return UserDefaults.standard.set(screen, forKey: screenIndexFromNotification)
    }
    private static let notificationIsInApp = "notificationIsInApp"
    
    public class func isNotificationInApp() -> Bool {
        return UserDefaults.standard.object(forKey: notificationIsInApp) as? Bool ?? false
    }
    
    public class func setNotificationInApp(isInApp: Bool) {
        UserDefaults.standard.set(isInApp, forKey: notificationIsInApp)
    }

    // TODO: Temporary holder for profile picture URL while waiting for the WS/CMS
    private static let profileImageUrlKey = "profileImageUrlKey"
    // TODO: Temporary holder for last login user to be used in verifying profile photo display
    private static let lastLoginUsername = "lastLoginUsername"
    
    public class func getMaxPasswordAttemptForTouchID() -> Int{
        return 4
    }
    public class func getLastLoginUsername() -> String {
        return UserDefaults.standard.object(forKey: lastLoginUsername) as? String ?? ""
    }
    
    public class func setLastLoginUsername(url:String) {
        UserDefaults.standard.set(url, forKey: lastLoginUsername)
    }
    
    public class func getProfileImageUrl() -> String? {
        return UserDefaults.standard.object(forKey: profileImageUrlKey) as? String
    }
    
    public class func setProfileImage(url:String) {
        UserDefaults.standard.set(url, forKey: profileImageUrlKey)
    }
    
    public class func lastLoginDate() -> Date? {
        let date = UserDefaults.standard.object(forKey: lastLoginDateKey) as? Date
        return date
    }
    
    public class func updateLastLoginDate() {
        UserDefaults.standard.set(Date(), forKey: lastLoginDateKey)
    }
    
    public class func hasLoggedInAtLeastOnce() -> Bool {
        return AppSettings.lastLoginDate() != nil
    }
    
    public class func hasShownUserPreferences() -> Bool {
        return UserDefaults.standard.object(forKey: hasShownUserPreferencesKey) != nil
    }
    
    public class func setHasShownUserPreferences() {
        UserDefaults.standard.set(Date(), forKey: hasShownUserPreferencesKey)
    }
    
    // MARK: VHC Completed
    public class func hasCompletedVHC() -> Bool {
        return UserDefaults.standard.object(forKey: hasVHCCompletedKey) != nil
    }
    
    public class func setHasCompletedVHC() {
        UserDefaults.standard.set(Date(), forKey: hasVHCCompletedKey)
    }
    
    // MARK: VHC Updated
    public class func clearHasUpdatedVHC() {
        UserDefaults.standard.removeObject(forKey: hasVHCUpdatedKey)
    }
    
    public class func hasUpdatedVHC() -> Bool {
        return UserDefaults.standard.object(forKey: hasVHCUpdatedKey) != nil
    }
    
    public class func setHasUpdatedVHC() {
        UserDefaults.standard.set(true, forKey: hasVHCUpdatedKey)
    }
    
    // MARK: VitalityAge Modal shown
    
    public class func clearHasShownVitalityAgePromptModal() {
        UserDefaults.standard.removeObject(forKey: hasShownVitalityAgePromptModalKey)
    }
    
    public class func hasShownVitalityAgePromptModal() -> Bool {
        return UserDefaults.standard.object(forKey: hasShownVitalityAgePromptModalKey) != nil
    }
    
    public class func setHasShownVitalityAgePromptModal() {
        UserDefaults.standard.set(Date(), forKey: hasShownVitalityAgePromptModalKey)
    }
    
    public class func lastVitalityAgePromptModelShownDate() -> Date? {
        let date = UserDefaults.standard.object(forKey: hasShownVitalityAgePromptModalKey) as? Date
        return date
    }
    
    public class func clearHasShownVitalityAgeModal() {
        UserDefaults.standard.removeObject(forKey: hasShownVitalityAgeModalKey)
    }
    
    public class func hasShownVitalityAgeModal() -> Bool {
        return UserDefaults.standard.object(forKey: hasShownVitalityAgeModalKey) != nil
    }
    
    public class func setHasShownVitalityAgeModal() {
        UserDefaults.standard.set(Date(), forKey: hasShownVitalityAgeModalKey)
    }
    
    public class func lastVitalityAgeModelShownDate() -> Date? {
        let date = UserDefaults.standard.object(forKey: hasShownVitalityAgeModalKey) as? Date
        return date
    }
    
    public class func hasShownMyHealthOnboarding() -> Bool {
        return UserDefaults.standard.object(forKey: hasShownMyHealthOnboardingKey) != nil
    }
    
    public class func setHasShownMyHealthOnboarding() {
        UserDefaults.standard.set(Date(), forKey: hasShownMyHealthOnboardingKey)
    }
    
    // MARK: VHC Onboarding shown
    
    public class func hasShownVHCOnboarding() -> Bool {
        return UserDefaults.standard.object(forKey: hasShownVHCOnboardingKey) != nil
    }
    
    public class func setHasShownVHCOnboarding() {
        UserDefaults.standard.set(Date(), forKey: hasShownVHCOnboardingKey)
    }
    
    // MARK: SAV Onboarding shown
    
    public class func hasShownSAVOnboarding() -> Bool {
        return UserDefaults.standard.object(forKey: hasShownSAVOnboardingKey) != nil
    }
    
    public class func setHasShownSAVOnboarding() {
        UserDefaults.standard.set(Date(), forKey: hasShownSAVOnboardingKey)
    }
    
    // MARK: VHR Onboarding shown
    
    public class func hasShownVHROnboarding() -> Bool {
        return UserDefaults.standard.object(forKey: hasShownVHROnboardingKey) != nil
    }
    
    public class func setHasShownVHROnboarding() {
        UserDefaults.standard.set(Date(), forKey: hasShownVHROnboardingKey)
    }
    
    // MARK: MWB Onboarding shown
    
    public class func hasShownMWBOnboarding() -> Bool {
        return UserDefaults.standard.object(forKey: hasShownMWBOnboardingKey) != nil
    }
    
    public class func setHasShownMWBOnboarding() {
        UserDefaults.standard.set(Date(), forKey: hasShownMWBOnboardingKey)
    }
    
    // MARK: VNA Onboarding shown
    
    public class func hasShownVNAOnboarding() -> Bool {
        return UserDefaults.standard.object(forKey: hasShownVNAOnboardingKey) != nil
    }
    
    public class func setHasShownVNAOnboarding() {
        UserDefaults.standard.set(Date(), forKey: hasShownVNAOnboardingKey)
    }
    
    // MARK: Device Cashback Onboarding shown
    
    public class func hasShownDeviceCashbackOnboarding() -> Bool {
        return UserDefaults.standard.object(forKey: hasShownDeviceCashbackOnboardingKey) != nil
    }
    
    public class func setHasShownDeviceCashbackOnboarding() {
        UserDefaults.standard.set(Date(), forKey: hasShownDeviceCashbackOnboardingKey)
    }
    
    // MARK: Apple Watch Cashback Onboarding shown
    
    public class func hasShownAppleWatchCashbackOnboarding() -> Bool {
        return UserDefaults.standard.object(forKey: hasShownAppleWatchCashbackOnboardingKey) != nil
    }
    
    public class func setHasShownAppleWatchCashbackOnboarding() {
        UserDefaults.standard.set(Date(), forKey: hasShownAppleWatchCashbackOnboardingKey)
    }
    
    // MARK: Organised Fitness Events Onboarding shown
    
    public class func hasShownOrganisedFitnessEventsOnboarding() -> Bool {
        return UserDefaults.standard.object(forKey: hasShownOrganisedFitnessEventsOnboardingKey) != nil
    }
    
    public class func setHasShownOrganisedFitnessEventsOnboarding() {
        UserDefaults.standard.set(Date(), forKey: hasShownOrganisedFitnessEventsOnboardingKey)
    }
    
    // MARK: Wellness Devices Onboarding shown
    
    public class func hasShownWellnessDevicesOnboarding() -> Bool {
        return UserDefaults.standard.object(forKey: hasShownWellnessDevicesOnboardingKey) != nil
    }
    
    public class func setHasShownWellnessDevicesOnboarding() {
        UserDefaults.standard.set(Date(), forKey: hasShownWellnessDevicesOnboardingKey)
    }
    
    // MARK: Vitality Status Onboarding
    public class func hasShownVitalityStatusOnboarding() -> Bool {
        return UserDefaults.standard.object(forKey: hasShownVitalityStatusOnboardingKey) != nil
    }
    
    public class func setHasShownVitalityStatusOnboarding() {
        UserDefaults.standard.set(Date(), forKey: hasShownVitalityStatusOnboardingKey)
    }
    
    // MARK: Rewards Onboarding
    public class func hasShownRewardsOnboarding() -> Bool {
        return UserDefaults.standard.object(forKey: hasShownRewardsOnboardingKey) != nil
    }
    
    public class func setHasShownRewardsOnboarding() {
        UserDefaults.standard.set(Date(), forKey: hasShownRewardsOnboardingKey)
    }
    
    // MARK: Share Vitality Status: delete, get, set
    public class func setUsernameForShareVitalityStatus(_ username: String) {
        UserDefaults.standard.set(username, forKey: shareVitalityStatusUsernameKey)
        
        if let readUserName = UserDefaults.standard.object(forKey: shareVitalityStatusUsernameKey) as? String {
            debugPrint(readUserName)
        }
    }
    
    public class func removeUsernameForShareVitalityStatus() {
        UserDefaults.standard.removeObject(forKey: shareVitalityStatusUsernameKey)
        
        if (UserDefaults.standard.object(forKey: shareVitalityStatusUsernameKey) as? String) != nil {
            debugPrint("Item was not deleted")
        } else {
            debugPrint("Item successfully deleted")
        }
    }
    
    public class func usernameForShareVitalityStatus() -> String? {
        let username = UserDefaults.standard.object(forKey: shareVitalityStatusUsernameKey) as? String
        return username
    }
    
    // MARK: Touch ID Domain State
    public class func setTouchIdDomainState(_ domainState: Data) {
        
        UserDefaults.standard.set(domainState, forKey: touchIdDomainStateKey)
        
        if let readDomainState = UserDefaults.standard.object(forKey: touchIdDomainStateKey) as? Data {
            debugPrint(readDomainState)
        }
    }
    
    public class func updateTouchIdDomainState(_ domainState: Data) {
        
        self.setTouchIdDomainState(domainState)
    }
    
    public class func touchIdDomainState() -> Data? {
        let domainState = UserDefaults.standard.object(forKey: touchIdDomainStateKey) as? Data
        return domainState
    }
    
    public class func removeTouchIdDomainState() {
        UserDefaults.standard.removeObject(forKey: touchIdDomainStateKey)
        
        if (UserDefaults.standard.object(forKey: touchIdDomainStateKey) as? Data) != nil {
            debugPrint("Item was not deleted")
        } else {
            debugPrint("Item successfully deleted")
        }
    }
    
    // NOTE: Temporary implementation until PushWoosh is implemented to comply with the BRD requirements
    // MARK: Notifications : delete, get, set
    public class func setUsernameForNotifications(_ username: String) {
        UserDefaults.standard.set(username, forKey: notificationsUsernameKey)
        
        if let readUserName = UserDefaults.standard.object(forKey: notificationsUsernameKey) as? String {
            debugPrint(readUserName)
        }
    }
    
    public class func removeUsernameForNotifications() {
        UserDefaults.standard.removeObject(forKey: notificationsUsernameKey)
        
        if (UserDefaults.standard.object(forKey: notificationsUsernameKey) as? String) != nil {
            debugPrint("Item was not deleted")
        } else {
            debugPrint("Item successfully deleted")
        }
    }
    
    public class func usernameForNotifications() -> String? {
        let username = UserDefaults.standard.object(forKey: notificationsUsernameKey) as? String
        return username
    }
    
    // MARK: Touch ID: delete, get, set
    public class func setUsernameForTouchId(_ username: String) {
        UserDefaults.standard.set(username, forKey: touchIdUsernameKey)
        
        if let readUserName = UserDefaults.standard.object(forKey: touchIdUsernameKey) as? String {
            debugPrint(readUserName)
        }
    }
    
    public class func removeUsernameForTouchId() {
        UserDefaults.standard.removeObject(forKey: touchIdUsernameKey)
        
        if (UserDefaults.standard.object(forKey: touchIdUsernameKey) as? String) != nil {
            debugPrint("Item was not deleted")
        } else {
            debugPrint("Item successfully deleted")
        }
    }
    
    public class func usernameForTouchId() -> String? {
        let username = UserDefaults.standard.object(forKey: touchIdUsernameKey) as? String
        return username
    }
    
    // MARK: Remember me: delete, get, set
    public class func setUsernameForRememberMe(_ username: String) {
        UserDefaults.standard.set(username, forKey: rememberMeUsernameKey)
        
        if let readUserName = UserDefaults.standard.object(forKey: rememberMeUsernameKey) as? String {
            debugPrint(readUserName)
        }
        
        UserDefaults.standard.set(Wire.default.currentEnvironment.prefix(), forKey: rememberMeUsernameEnvKey)
        
        if let readUsernameEnv = UserDefaults.standard.object(forKey: rememberMeUsernameEnvKey) as? String {
            debugPrint(readUsernameEnv)
        }
    }
    
    public class func hasSetFirstLoginUsernameForRememberMe() -> Bool {
        return UserDefaults.standard.object(forKey: firstLoginUsernameForRememberMe) != nil
    }
    
    public class func setFirstLoginUsernameForRememberMe(_ username: String) {
        UserDefaults.standard.set(username, forKey: firstLoginUsernameForRememberMe)
        
        if let readUserName = UserDefaults.standard.object(forKey: firstLoginUsernameForRememberMe) as? String {
            debugPrint(readUserName)
        }
    }
    
    public class func removeUsernameForRememberMe() {
        UserDefaults.standard.removeObject(forKey: rememberMeUsernameKey)
        if (UserDefaults.standard.object(forKey: rememberMeUsernameKey) as? String) != nil {
            debugPrint("Username item was not deleted")
        } else {
            debugPrint("Username item successfully deleted")
        }
        
        UserDefaults.standard.removeObject(forKey: rememberMeUsernameEnvKey)
        if (UserDefaults.standard.object(forKey: rememberMeUsernameEnvKey) as? String) != nil {
            debugPrint("UsernameEnv item was not deleted")
        } else {
            debugPrint("UsernameEnv item successfully deleted")
        }
    }
    
    public class func usernameForRememberMe() -> String? {
        let username = UserDefaults.standard.object(forKey: rememberMeUsernameKey) as? String
        return username
    }
    
    public class func usernameEnvForRememberMe() -> String? {
        let usernameEnv = UserDefaults.standard.object(forKey: rememberMeUsernameEnvKey) as? String
        return usernameEnv
    }
    
    // MARK: Analytics: delete, get, set
    public class func addAnalyticsPreference() {
        UserDefaults.standard.set(true, forKey: analyticsPreferenceKey)
        
        if let isAnalyticsAllowed = UserDefaults.standard.object(forKey: analyticsPreferenceKey) as? Bool {
            debugPrint("is Analytics Allowed : " + String(isAnalyticsAllowed))
        }
    }
    
    public class func removedAnalyticsPreference() {
        UserDefaults.standard.set(false, forKey: analyticsPreferenceKey)
        
        if let isAnalyticsAllowed = UserDefaults.standard.object(forKey: analyticsPreferenceKey) as? Bool {
            debugPrint("is Analytics Allowed : " + String(isAnalyticsAllowed))
        }
    }
    
    public class func getAnalyticsPreference() -> Bool? {
        let isAnalyticsAllowed = UserDefaults.standard.object(forKey: analyticsPreferenceKey) as? Bool
        return isAnalyticsAllowed
    }
    
    // MARK: Crash Report: delete, get, set
    public class func addCrashReportPreference() {
        UserDefaults.standard.set(true, forKey: crashReportPreferenceKey)
        
        if let isAnalyticsAllowed = UserDefaults.standard.object(forKey: crashReportPreferenceKey) as? Bool {
            debugPrint("is Crash Report Allowed : " + String(isAnalyticsAllowed))
        }
    }
    
    public class func removedCrashReportPreference() {
        UserDefaults.standard.set(false, forKey: crashReportPreferenceKey)
        
        if let isAnalyticsAllowed = UserDefaults.standard.object(forKey: crashReportPreferenceKey) as? Bool {
            debugPrint("is Crash Report Allowed : " + String(isAnalyticsAllowed))
        }
    }
    
    public class func getCrashReportPreference() -> Bool? {
        let isAnalyticsAllowed = UserDefaults.standard.object(forKey: crashReportPreferenceKey) as? Bool
        return isAnalyticsAllowed
    }
    
    // MARK: AWC Link Apple Health App
    
    public class func hasLinkedAppleHealthDevice() -> Bool {
        return UserDefaults.standard.object(forKey: hasLinkedAppleHealthDeviceKey) != nil
    }
    
    public class func setHasLinkedAppleHealthDevice() {
        UserDefaults.standard.set(true, forKey: hasLinkedAppleHealthDeviceKey)
    }
    
    // MARK: VNA/Assessments valid answer
    
    public class func hasProvidedValidAssessmentValues() -> Bool {
        return UserDefaults.standard.object(forKey: hasEnteredValidAssessmentAnswers) as? Bool == true
    }
    
    public class func setHasProvidedValidAssessmentValues(_ valid: Bool) {
        UserDefaults.standard.set(valid, forKey: hasEnteredValidAssessmentAnswers)
    }
    
    public class func hasUpdatedNumberRangeSection() -> Bool {
        return UserDefaults.standard.object(forKey: hasUpdatedNumberRange) as? Bool == true
    }
    
    public class func setHasUpdatedNumberRangeInSection(_ valid: Bool) {
        UserDefaults.standard.set(valid, forKey: hasUpdatedNumberRange)
    }
    
    // MARK: VHR Assessment
    
    public class func forceDotDecimalSeparator(unit: Unit) -> Bool {
        if Locale.current.languageCode == "es" && unit == UnitOfMeasureRef.FootInch.unit() {
            return true
        }
        return false
    }
    
    // MARK: Generic
    
    public class func getAppTenantId() -> Int?{
        return (Bundle.main.object(forInfoDictionaryKey: "VATenantId") as? NSNumber)?.intValue
    }
    
    public class func getAppTenant() -> TenantReference?{
        if let tenantID = (Bundle.main.object(forInfoDictionaryKey: "VATenantId") as? NSNumber)?.intValue{
            return TenantReference(rawValue: tenantID)
        }
        return nil
    }

    public class func isAODAEnabled() -> Bool{
        return Bundle.main.object(forInfoDictionaryKey: "VIAEnableAODA") as? Bool ?? false
    }
    
    public class func hideDSConsent() -> Bool{
        return Bundle.main.object(forInfoDictionaryKey: "VIAHideDSConsent") as? Bool ?? false
    }
    
    public class func resetAll() {
        for key in ALL_KEYS{
            UserDefaults.standard.set(nil, forKey: key)
        }
    }
    
    // MARK: NotificationHandler
    public class func isSelectedNotificationEnabled() -> Bool{
        return Bundle.main.object(forInfoDictionaryKey: "VIAEnableSelectedNotification") as? Bool ?? false
    }
}
