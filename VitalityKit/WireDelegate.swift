import Foundation

public protocol WireDelegate: class {
    var devBaseURL: URL { get }
    var testBaseURL: URL { get }
    var qaBaseURL: URL { get }
    var qaCentralAssetBaseURL: URL { get }
    var eagleBaseURL: URL { get }
    var qaFrankfurtURL: URL { get }
    var productionBaseURL: URL { get }
    
    var performanceBaseURL: URL { get }
    var test2BaseURL: URL { get }
    var qaSouthKoreaBaseURL: URL { get }
    
    var devAPIManagerIdentifier: String { get }
    var testAPIManagerIdentifier: String { get }
    var qaAPIManagerIdentifier: String { get }
    var qaCentralAssetAPIManagerIdentifier: String { get }
    var eagleAPIManagerIdentifier: String { get }
    var qaFrankfurtAPIManagerIdentifier: String { get }
    var productionAPIManagerIdentifier: String { get }
    
    
    
    
    
    
    var test2APIManagerIdentifier: String { get }
    var performanceAPIManagerIdentifier: String { get }
    var qaSouthKoreaAPIManagerIdentifier: String { get }
    
}

public extension WireDelegate {
    
    /* New Central Asset QA Environment */
    public var qaCentralAssetBaseURL: URL {
        //        return URL(string: "https://qa.vitalityservicing.com/v1/api")!
        return URL(string: "https://qa.vitalitydeveloper.com/api")!
        
        
    }
    
    /* New Central Asset QA Environment */
    public var qaCentralAssetAPIManagerIdentifier: String {
        //        return "WDVlODVDQ3RQTVFpOWpRZ2p4TERSTkxXSjV3YTpQSVZieHkyanRiOFMwRDc1NHJMa0FXWjR2aTBh"
        return "eFBnVzB6ZlhrT1o4dW5BNm03YnNiWEpDc3FjYTpmNHBCb0tBQlNYOThUZnpVMTNJbUJ5VWlrcE1h"
    }
    
    public var eagleBaseURL: URL {
        return URL(string: "http://eagle.discsrv.co.za/")!
    }
    
    public var eagleAPIManagerIdentifier: String {
        return "WDVlODVDQ3RQTVFpOWpRZ2p4TERSTkxXSjV3YTpQSVZieHkyanRiOFMwRDc1NHJMa0FXWjR2aTBh"
    }
    
    /* New Central Asset Test Environment */
    public var test2BaseURL: URL {
        return URL(string: "https://test.vitalitydeveloper.com/api")!
    }
    
    /* New Central Asset Test Environment */
    public var test2APIManagerIdentifier: String {
        return "dUpUdnlObWM5Zk9mSDlOVENhS1lqT0REeVJ3YTpWWE05Z1pVbGJRbDJSWkl3cE5zWEFGUm1oZVVh"
    }
    
    /* FC-17527 */
    public var devBaseURL: URL {
        return URL(string: "https://dev.vitalitydeveloper.com/api")!
    }
    
    
    
    public var devAPIManagerIdentifier: String {
        return "VFVfVE0xTjJxX3NwWnRQSjk5M3dmWnlLcXZZYTp3Zm1MNVhQSlVOY3hYU1hJT2ZLRkE5SUZWakFh"
    }
}
