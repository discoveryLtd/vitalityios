import VIAUtilities

public class PartyPreferenceHelper {
    public static func getEmailPreference(from preference: GetPartyByIdResponse?) -> String? {
        let generalPreferences = preference?.getPartyResponse?.party?.generalPreferences
        if let index = generalPreferences?.index(where: { $0.typeKey == PreferenceTypeRef.StarbucksEmail.rawValue}) {
            return generalPreferences?[index].value
        }
        return nil
    }
}
