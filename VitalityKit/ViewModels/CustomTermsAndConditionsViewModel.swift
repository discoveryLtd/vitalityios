import VIAUtilities

public protocol CustomTermsAndConditionsViewModelDelegate: class {
    func handle(error: Error)
}

public class CustomTermsConditionsViewModel: CMSConsumer {
    public weak var delegate: CustomTermsAndConditionsViewModelDelegate?
    public var articleId: String?
    internal let coreRealm = DataProvider.newRealm()
    public init(articleId: String?) {
        self.articleId = articleId
    }

    public func loadArticleContent(completion: @escaping (_ error: Error?, _ content: String?) -> Void) {
        guard let articleId = self.articleId else {
            completion(CMSError.missingArticleId, nil)
            return
        }

        self.getCMSArticleContent(articleId: articleId, completion: completion)
    }

    public func agreeToTermsAndConditions(with events: [EventTypeRef], completion: @escaping (Error?) -> ()) {
        self.createEvents(with: events, completion: completion)
    }

    public func disagreeToTermsAndConditions(with events: [EventTypeRef], completion: @escaping (Error?) -> ()) {
        self.createEvents(with: events, completion: completion)
    }

    internal func createEvents(with events: [EventTypeRef], completion: @escaping (Error?) -> ()) {
        let partyId = coreRealm.getPartyId()
        let tenantId = coreRealm.getTenantId()
        Wire.Events.createEvents(tenantId: tenantId, partyId: partyId, events: events) { error in
            completion(error)
        }
    }
}
