//
//  CMS.swift
//  VitalityActive
//
//  Created by Wilmar van Heerden on 2016/10/06.
//  Copyright © 2016 Glucode. All rights reserved.
//

import Foundation
import Alamofire


public enum CMSError: Error {
    case missingArticleId
    case invalidContent
    case fileNotFound
    case missingInsurerGroupId
    case missingArticleContent
}

// MARK: CMSConsumer protocol and extension

public protocol CMSConsumer: class {
    func getCMSFile(title fileTitle: String?, completion: @escaping ((_ fileData: Data?, _ error: Error?) -> Void))
    func getCMSArticleContent(articleId: String?, completion: @escaping ((_ error: Error?, _ content: String?) -> Void))
    func downloadCMSImage(file fileTitle: String?, completion: @escaping ((_ fileUrl: URL?, _ error: Error?) -> Void))
    func downloadCMSImageToCache(file fileTitle: String?, completion: @escaping ((_ fileUrl: URL?, _ error: Error?) -> Void))
    // TODO: Replace when Wellness Partners services are included
    func getTestHTML(testHTML: String?, completion: @escaping ((_ error: Error?, _ content: String?) -> Void))
    func downloadHistoryImage(referenceId: Int?, completion: @escaping ((_ fileUrl: URL?, _ error: Error?) -> Void))
    func getVNACMSArticleContent(articleId: String?, completion: @escaping ((_ error: Error?, _ content: String?) -> Void))
}

public extension CMSConsumer where Self: AnyObject {
    func getCMSFile(title fileTitle: String?, completion: @escaping ((_ fileData: Data?, _ error: Error?) -> Void)) {
        CMS().getLiferayFileData(title: fileTitle, completion: completion)
    }
    
    func getCMSArticleContent(articleId: String?, completion: @escaping ((_ error: Error?, _ content: String?) -> Void)) {
        CMS().getLiferayArticleContent(articleId: articleId, completion: completion)
    }
    
    func downloadCMSImage(file fileTitle: String?, completion: @escaping ((_ fileUrl: URL?, _ error: Error?) -> Void)) {
        CMS().downloadLiferayImage(file: fileTitle, to: .documentDirectory, completion: completion)
    }
    
    func downloadCMSImageToCache(file fileTitle: String?, completion: @escaping ((_ fileUrl: URL?, _ error: Error?) -> Void)) {
        CMS().downloadLiferayImage(file: fileTitle, to: .cachesDirectory, completion: completion)
    }
    
    // TODO: Replace when Wellness Partners services are included
    func getTestHTML(testHTML: String?, completion: @escaping ((_ error: Error?, _ content: String?) -> Void)) {
        completion(nil, CMS().testHTML(testHTML))
    }
    
    func downloadHistoryImage(referenceId: Int?, completion: @escaping ((_ fileUrl: URL?, _ error: Error?) -> Void)) {
        CMS().downloadHistoryImageFromLiferay(referenceId: referenceId, to: .documentDirectory, completion: completion)
    }
    
    func getVNACMSArticleContent(articleId: String?, completion: @escaping ((_ error: Error?, _ content: String?) -> Void)) {
        CMS().getVNATemporaryLiferayArticleContent(articleId: articleId, completion: completion)
    }
}

// MARK: CMS

public protocol LiferayConsumer: class {
    func getLiferayFileData(title fileTitle: String?, completion: @escaping ((_ fileData: Data?, _ error: Error?) -> Void))
    func getLiferayArticleContent(articleId: String?, completion: @escaping ((_ error: Error?, _ content: String?) -> Void))
    func downloadLiferayImage(file fileTitle: String?, to directory: FileManager.SearchPathDirectory, completion: @escaping ((_ fileUrl: URL?, _ error: Error?) -> Void)) //local file manager url
    func downloadHistoryImageFromLiferay(referenceId: Int?, to directory: FileManager.SearchPathDirectory, completion: @escaping ((_ fileUrl: URL?, _ error: Error?) -> Void))
}

class CMS: NSObject, LiferayConsumer {
    
    func downloadHistoryImageFromLiferay(referenceId: Int?, to directory: FileManager.SearchPathDirectory, completion: @escaping ((URL?, Error?) -> Void)) {
        
        guard let refId = referenceId else {
            return
        }
        
        Wire.Content.getFileByReferenceId(referenceId: refId, completion: {header, data, error in
            if error != nil {
                completion(nil, error)
                return
            }
            
            guard let fileNameFromHeader = header else {
                completion(nil, error)
                return
            }
            
            let dropFirstCharacters = String(fileNameFromHeader.characters.dropFirst(22))
            let fileName = String(dropFirstCharacters.characters.dropLast(1))
            
            guard let responseData = data else {
                completion(nil, error)
                return
            }
            
            if let image = UIImage(data: responseData) {
                do {
                    let documentsURL = FileManager.default.urls(for: directory, in: .userDomainMask).first!
                    let fileURL = documentsURL.appendingPathComponent("\(fileName)")
                    if let pngImageData = UIImagePNGRepresentation(image) {
                        try pngImageData.write(to: fileURL, options: .atomic)
                    }
                    let realm = DataProvider.newRealm()
                    realm.updateFileLastAccessedDate(for: fileName)
//                    NSLog(fileURL.absoluteString)
                    completion(fileURL, nil)
                } catch {
                    completion(nil, error)
                }
            }
        })
    }
    
    func getLiferayFileData(title fileTitle: String?, completion: @escaping ((_ fileData: Data?, _ error: Error?) -> Void)) {
        guard let fileName = fileTitle else {
            completion(nil, nil)
            return
        }
        guard let insurerCMSGroupId = AppConfigFeature.insurerCMSGroupId() else {
            completion(nil, CMSError.missingInsurerGroupId)
            return
        }
        
        let lastModifiedDate = CMSFile.downloadDate(for: fileName)
        Wire.Content.getFileByGroupId(fileName: fileName, groupId: insurerCMSGroupId, modifiedSince: lastModifiedDate, completion: {data, error in
            if error == nil {
                let realm = DataProvider.newRealm()
                realm.updateFileLastAccessedDate(for: fileName)
            }
            completion(data, error)
        })
    }
    
    func downloadLiferayImage(file fileTitle: String?, to directory: FileManager.SearchPathDirectory, completion: @escaping ((_ fileUrl: URL?, _ error: Error?) -> Void)) {
        guard let fileName = fileTitle else {
            completion(nil, nil)
            return
        }
        guard let insurerCMSGroupId = AppConfigFeature.insurerCMSGroupId() else {
            completion(nil, CMSError.missingInsurerGroupId)
            return
        }
        
        let lastModifiedDate = CMSFile.downloadDate(for: fileName)
        let escapedFilename = fileName.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) ?? fileName
        Wire.Content.getFileByGroupId(fileName: escapedFilename, groupId: insurerCMSGroupId, modifiedSince: lastModifiedDate, completion: {data, error in
            if error != nil {
                completion(nil, error)
                return
            }
            guard let responseData = data else {
                //TODO: - Possibly return error
                completion(nil, nil)
                return
            }
            if let image = UIImage(data: responseData) {
                do {
                    let documentsURL = FileManager.default.urls(for: directory, in: .userDomainMask).first!
                    let fileURL = documentsURL.appendingPathComponent("\(fileName)")
                    if let pngImageData = UIImagePNGRepresentation(image) {
                        try pngImageData.write(to: fileURL, options: .atomic)
                    }
                    let realm = DataProvider.newRealm()
                    realm.updateFileLastAccessedDate(for: fileName)
                    completion(fileURL, nil)
                } catch {
                    //TODO: - Possibly return error
                    completion(nil, nil)
                }
            }
        })
    }
    
    func getLiferayArticleContent(articleId: String?, completion: @escaping ((_ error: Error?, _ content: String?) -> Void)) {
        guard let validArticleId = articleId else {
            completion(CMSError.missingArticleContent, nil)
            return
        }
        
        guard let insurerCMSGroupId = AppConfigFeature.insurerCMSGroupId() else {
            completion(CMSError.missingInsurerGroupId, nil)
            return
        }
        
        Wire.Content.getArticleByURLTitle(urlTitle: validArticleId, groupId: insurerCMSGroupId, completion: { content, error in
            guard let validContent = content else {
                debugPrint("Missing webcontent")
                completion(CMSError.invalidContent, nil)
                return
            }
            
            let html = self.wrapperHtml().replacingOccurrences(of: "CONTENT_GOES_HERE", with: validContent)
            completion(nil, html)
        })
    }
    
    func getVNATemporaryLiferayArticleContent(articleId: String?, completion: @escaping ((_ error: Error?, _ content: String?) -> Void)) {
        guard let validArticleId = articleId else {
            completion(CMSError.missingArticleContent, nil)
            return
        }
        
        Wire.Content.getArticleByURLTitle(urlTitle: validArticleId, groupId: "20143", completion: { content, error in
            guard let validContent = content else {
                debugPrint("Missing webcontent")
                completion(CMSError.invalidContent, nil)
                return
            }
            
            let html = self.wrapperHtml().replacingOccurrences(of: "CONTENT_GOES_HERE", with: validContent)
            completion(nil, html)
        })
    }
    
    // TODO: Replace when Wellness Partners services are included
    func testHTML(_ content: String?) -> String? {
        guard let validContent = content else { return nil }
        return self.wrapperHtml().replacingOccurrences(of: "CONTENT_GOES_HERE", with: validContent)
    }
    
    func wrapperHtml() -> String {
        let bundle = Bundle(for: type(of: self))
        guard let path = bundle.path(forResource: "webcontent", ofType: "html") else {
            debugPrint("File does not exist: webcontent.html")
            return ""
        }
        
        do {
            return try String(contentsOfFile: path, encoding: .utf8)
        } catch let error {
            debugPrint("Could not load html from file webcontent.html: \(error)")
            return ""
        }
    }
}
