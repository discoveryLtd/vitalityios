import Foundation
import Alamofire
import AlamofireNetworkActivityIndicator
import VIAUtilities

// MARK: Errors

public enum WireError: Error {
    case parsing
    case securityException
    case unknown // TODO: check BackendError.other
}

public enum BackendError: Error {
    case network(error: NSError) // Capture any underlying Error from the URLSession API
    case dataSerialization(error: Error)
    case other
}

// MARK: Wire

public class Wire {

    public enum Environment: Int {
        static let identifier: String           = "vitality_env"
        static let developPrefix: String        = "dev--"
        static let testPrefix: String           = "test--"
        static let qaPrefix: String             = "qa--"
        static let qaCentralAssetPrefix: String = "qaca--"
        static let eaglePrefix: String          = "eagle--"
        static let qaFrankfurtPrefix: String    = "qaff--"
        
        static let performancePrefix: String    = "perf--"
        static let test2Prefix: String          = "ntest--"
        static let qaSouthKoreaPrefix: String   = "qask--"

        case develop        = 0
        case test           = 1
        case qa             = 2
        case qaCentralAsset = 3
        case eagle          = 4
        case production     = 5
        case qaFrankfurt    = 6
        case performance    = 7
        case test2          = 8
        case qaSouthKorea   = 9

        public func prefix() -> String {
            switch self {
            case .develop:
                return Environment.developPrefix
            case .test:
                return Environment.testPrefix
            case .qa:
                return Environment.qaPrefix
            case .qaCentralAsset:
                return Environment.qaCentralAssetPrefix
            case .eagle:
                return Environment.eaglePrefix
            case .production:
                return ""
            case .qaFrankfurt:
                return Environment.qaFrankfurtPrefix
            case .performance:
                return Environment.performancePrefix
            case .test2:
                return Environment.test2Prefix
            case .qaSouthKorea:
                return Environment.qaSouthKoreaPrefix
            }
        }
        
        public func baseURL() -> URL? {
            do{
                return try Wire.default.baseURL()
            }catch{
                return nil
            }
        }

        fileprivate static func trimEnvironment(_ environment: Environment, from email: String) -> String {
            return email.replacingOccurrences(of: environment.prefix(), with: "")
        }

        fileprivate static func determineEnvironment(from email: String) -> Environment {
            var environment: Environment = .production
            if email.hasPrefix(Environment.developPrefix) {
                environment = .develop
            } else if email.hasPrefix(Environment.testPrefix) {
                environment = .test
            } else if email.hasPrefix(Environment.qaPrefix) {
                environment = .qa
            } else if email.hasPrefix(Environment.qaCentralAssetPrefix) {
                environment = .qaCentralAsset
            } else if email.hasPrefix(Environment.eaglePrefix) {
                environment = .eagle
            } else if email.hasPrefix(Environment.qaFrankfurtPrefix) {
                environment = .qaFrankfurt
            } else if email.hasPrefix(Environment.performancePrefix) {
                environment = .performance
            } else if email.hasPrefix(Environment.test2Prefix) {
                environment = .test2
            } else if email.hasPrefix(Environment.qaSouthKoreaPrefix) {
                environment = .qaSouthKorea
            }
            return environment
        }

        static func baseURL(`for` environment: Environment) throws -> URL {
            switch environment {
            case .develop:
                return Wire.default.delegate.devBaseURL
            case .test:
                return Wire.default.delegate.testBaseURL
            case .qa:
                return Wire.default.delegate.qaBaseURL
            case .qaCentralAsset:
                return Wire.default.delegate.qaCentralAssetBaseURL
            case .eagle:
                return Wire.default.delegate.eagleBaseURL
            case .production:
                return Wire.default.delegate.productionBaseURL
            case .qaFrankfurt:
                return Wire.default.delegate.qaFrankfurtURL
            case .performance:
                return Wire.default.delegate.performanceBaseURL
            case .test2:
                return Wire.default.delegate.test2BaseURL
            case .qaSouthKorea:
                return Wire.default.delegate.qaSouthKoreaBaseURL
            }
        }

        public func name() -> String {
            switch self {
            case .develop:
                return "DEV"
            case .test:
                return "CA TEST"
            case .qa:
                return "QA"
            case .qaCentralAsset:
                return "QA CENTRAL ASSET"
            case .eagle:
                return "EAGLE 🦅"
            case .production:
                return "PRODUCTION"
            case .qaFrankfurt:
                return "UAT"
            case .performance:
                return "PERFORMANCE"
            case .test2:
                return "NTEST"
            case .qaSouthKorea:
                return "QA SOUTH KOREA"
            }
        }
        
        public func userAgent() -> String{
            switch self {
            case .test2:
                guard let tenantID = AppSettings.getAppTenant() else{
                    return UserAgent.toString()
                }

                if tenantID == .SLI || tenantID == .IGI || tenantID == .EC || tenantID == .ASR || tenantID == .MLI {
//                    return "VitalityActive/1.0.0.432/2.0 (iPhone 5,4; iOS 10.1)"
                    return UserAgent.toString("VitalityActive")
                }

                return UserAgent.toString()
            case .qa, .qaCentralAsset :
                guard let tenantID = AppSettings.getAppTenant() else {
                    return UserAgent.toString()
                }
                
                if tenantID == .EC || tenantID == .ASR || tenantID == .MLI {
                    return UserAgent.toString("VitalityActive")
                } else {
                    return UserAgent.toString()
                }
            case .develop:
                guard let tenantID = AppSettings.getAppTenant() else {
                    return UserAgent.toString()
                }
                
                if tenantID == .ASR {
                    return UserAgent.toString("VitalityActive")
                } else {
                    return UserAgent.toString()
                }
            case .production:
                guard let tenantID = AppSettings.getAppTenant() else {
                    return UserAgent.toString()
                }
                
                if tenantID == .EC || tenantID == .ASR {
                    return UserAgent.toString("VitalityActive")
                } else {
                    return UserAgent.toString()
                }
            default:
                return UserAgent.toString()
            }
        }
    }

    public var currentEnvironment: Environment = .production {
        didSet {
            UserDefaults.standard.setValue(self.currentEnvironment.rawValue, forKey: Wire.Environment.identifier)
            debugPrint("Switch to \(self.currentEnvironment.name()) environment")
        }
    }

    /// Identifier required to access API manager before a login token has been created.
    /// This value is used in a Basic auth header for login, registration and forgot password requests.
    /// It is unique per application and has to be generated on API Manager Store by Discovery team.
    var apiManagerIdentifier: String {
        switch currentEnvironment {
        case .develop:
            return self.delegate.devAPIManagerIdentifier
        case .test:
            return self.delegate.testAPIManagerIdentifier
        case .qa:
            return self.delegate.qaAPIManagerIdentifier
        case .qaCentralAsset:
            return self.delegate.qaCentralAssetAPIManagerIdentifier
        case .eagle:
            return self.delegate.eagleAPIManagerIdentifier
        case .production:
            return self.delegate.productionAPIManagerIdentifier
        case .qaFrankfurt:
            return self.delegate.qaFrankfurtAPIManagerIdentifier
        case .performance:
            return self.delegate.performanceAPIManagerIdentifier
        case .test2:
            return self.delegate.test2APIManagerIdentifier
        case .qaSouthKorea:
            return self.delegate.qaSouthKoreaAPIManagerIdentifier
        }
    }

    private static let API_VERSION = "1.0"

    public weak var delegate: WireDelegate!

    /// Unique identifier for each session. A session is defined as a launch
    /// of the application, regardless of how long it lives
    public lazy var sessionID: String = {
        return UUID().uuidString
    }()

    // MARK: Singleton

    private init() {
        // don't expose this init externally, consumers have to use `default` singleton
    }

    open static let `default`: Wire = {
        let wire = Wire()
        if let lastUsedEnvironment = wire.lastUsedEnvironment() {
            wire.currentEnvironment = lastUsedEnvironment
        } else {
            wire.currentEnvironment = .production
        }
        return wire
    }()

    // MARK: Fundamentals

    internal func baseURL() throws -> URL {
        #if WIREMOCK
            let url = try "http://localhost:8080".asURL()
        #else
            let url = try Environment.baseURL(for: self.currentEnvironment)
        #endif
        return url
    }

    private func lastUsedEnvironment() -> Environment? {
        guard let value = UserDefaults.standard.value(forKey: Environment.identifier) as? Int else {
            debugPrint("No previously used environment")
            return nil
        }
        guard let environment = Environment(rawValue: value) else {
            debugPrint("Unknown rawValue for previously used environment")
            return nil
        }
        return environment
    }

    public class func urlForPath(path: String) throws -> URL {
        let baseUrl: URL = try! Wire.default.baseURL()
        return baseUrl.appendingPathComponent(path).appendingPathComponent(API_VERSION)
    }

    public static func updateEnvironmentAndTrim(email: String) -> String {
        var environment = Wire.Environment.determineEnvironment(from: email)
        #if DEBUG
            if ProcessInfo.processInfo.arguments.contains("ALWAYS_USE_TEST_ENVIRONMENT") {
                environment = .test
            }
        #endif
        Wire.default.currentEnvironment = environment
        
        /* 
         * Session Managers are only created once in an app lifecycle.
         * This will force the User-Agent to be overwritten on succeedding logins.
         */
        Wire.unauthorisedSessionManager.adapter = UnauthorisedAPIManagerAccessAdapter(environment.userAgent())
        return Environment.trimEnvironment(environment, from: email)
    }

    // MARK: SessionManagers
    
    private class func certificatePinningEnabled() -> Bool {
        let enabled = Bundle.main.object(forInfoDictionaryKey: "VAEnableCertificatePinning") as? Bool ?? false
        return enabled
    }

    class func newSessionManager() -> SessionManager {
        assert(Wire.default.delegate != nil, "Assign a delegate to Wire before making requests")
        let configuration = URLSessionConfiguration.default
        configuration.httpAdditionalHeaders = SessionManager.defaultHTTPHeaders
        
        let sessionDelegate = SessionDelegate()
        
        if Wire.certificatePinningEnabled() {
            let securityTrustPolicyManager = Wire.newSecurityTrustPolicyManager()
            return SessionManager(configuration: configuration, delegate: sessionDelegate, serverTrustPolicyManager: securityTrustPolicyManager)
        } else {
            return SessionManager(configuration: configuration, delegate: sessionDelegate)
        }
    }

    open static let `sessionManager`: SessionManager = {
        assert(Wire.default.delegate != nil, "Assign a delegate to Wire before making requests")
        let manager = Wire.newSessionManager()
        NetworkActivityIndicatorManager.shared.isEnabled = true
        return manager
    }()

    open static let unauthorisedSessionManager: SessionManager = {
        assert(Wire.default.delegate != nil, "Assign a delegate to Wire before making requests")
        let manager = Wire.newSessionManager()
        manager.adapter = UnauthorisedAPIManagerAccessAdapter(Wire.default.currentEnvironment.userAgent())
        
        NetworkActivityIndicatorManager.shared.isEnabled = true
        return manager
    }()
    
    public class func setAuthorisedAPIManagerToken(token: String) {
        Wire.sessionManager.adapter = APIManagerAccessAdapter(accessToken: token,
                                                              userAgent: Wire.default.currentEnvironment.userAgent())
    }

    public class func cancelAllTasks() {
        Wire.unauthorisedSessionManager.session.getAllTasks { tasks in
            debugPrint("unauthorisedSessionManager Tasks: \(tasks)")
            tasks.forEach { $0.cancel() }
        }
        Wire.sessionManager.session.getAllTasks { tasks in
            debugPrint("sessionManager Tasks: \(tasks)")
            tasks.forEach { $0.cancel() }
        }
    }
    
    // MARK: Server trust
    
    private static func newSecurityTrustPolicyManager() -> ServerTrustPolicyManager {
        assert(Wire.default.delegate != nil, "Assign a delegate to Wire before configuring security")
        var serverTrustPolicies: [String: ServerTrustPolicy] = [:]
        let baseURL = try! Wire.default.baseURL()
        let mainBundleCertificates = ServerTrustPolicy.certificates()
        serverTrustPolicies[baseURL.host!] = .pinCertificates(
            certificates: mainBundleCertificates,
            validateCertificateChain: true,
            validateHost: true
        )
        return ServerTrustPolicyManager(policies: serverTrustPolicies)
    }

    // MARK: Formatters and other instance helpers

    public lazy var apiManagerFormatterWithMilliseconds: DateFormatter = { () -> DateFormatter in
        return DateFormatter.apiManagerFormatterWithMilliseconds()
    }()

    public lazy var apiManagerFormatterWithDateTime: DateFormatter = { () -> DateFormatter in
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        return formatter
    }()

    public lazy var apiManagerFormatterWithDateTimeTimeZone: DateFormatter = { () -> DateFormatter in
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"
        return formatter
    }()

    public lazy var simpleDateFormatter: DateFormatter = { () -> DateFormatter in
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter
    }()
    public lazy var gmtDateFormatter: DateFormatter = { () -> DateFormatter in
        // eg: Wed, 21 Oct 2015 07:28:00 GMT
        let formatter = DateFormatter()
        formatter.dateFormat = "E, dd MMM yyyy hh:mm:ss"
        formatter.timeZone = TimeZone(identifier:"GMT")
        return formatter
    }()
    public lazy var yearMonthDayFormatter: DateFormatter = { () -> DateFormatter in
        return DateFormatter.yearMonthDayFormatter()
    }()

    public lazy var serviceTimeZoneIdentifier: String = {
        return TimeZone.current.identifier
    }()
    
    public static func getFormattedDateWithTimezone(date: Date) -> String{
        if let timezoneString = Bundle.main.object(forInfoDictionaryKey: "VIATimezone") as? String,
            let timezone = TimeZone(identifier: timezoneString){
            return DateFormatter.apiManagerFormatterWithMilliseconds(localeIdentifier: DeviceLocale.toString(),
                                                                     timezone: timezone).string(from: date)
        }
        return DateFormatter.apiManagerFormatterWithMilliseconds(localeIdentifier: DeviceLocale.toString()).string(from: date)
    }

    // MARK: Namespaces - set up basic "namespace" structs for request grouping

    public struct Member {
    }
    public struct Content {
    }
    public struct Events {
    }
    public struct Rewards {
    }
    public struct Party {
    }
    public struct Devices {
    }
    public struct Partner {
    }
    public struct ActivationCode {
    }
    public class MyHealth{
        private static var instance:MyHealth!
        private var working = false
        public var dataRequest: DataRequest?
        
        public static func shared() -> MyHealth{
            if nil == instance{
                instance = MyHealth()
            }
            return instance
        }
        public func startWorking(){
            working = true
        }
        
        public func stopWorking(){
            working = false
        }
        
        public func isWorking() -> Bool{
            return working
        }
    }
    public struct Update{        
    }

}
