// Generated using SwiftGen, by O.Halligon — https://github.com/SwiftGen/SwiftGen

import Foundation

// swiftlint:disable file_length

// swiftlint:disable explicit_type_interface identifier_name line_length nesting type_body_length type_name
public enum CommonStrings {
  /// About %@
  public static func AboutText450(_ p1: String) -> String {
    return CommonStrings.tr("Common", "about_text_450", p1)
  }
  /// Activating...
  public static let ActivationLoadingIndicatorTitle369: String = CommonStrings.tr("Common", "activation_loading_indicator_title_369")
  /// Agree
  public static let AgreeButtonTitle50: String = CommonStrings.tr("Common", "agree_button_title_50")
  /// Connection Error
  public static let AlertConnectionErrorTitle1139: String = CommonStrings.tr("Common", "alert_connection_error_title_1139")
  /// The date of birth you entered does not match the authentication code.
  public static let AlertDateOfBirthMessage1053: String = CommonStrings.tr("Common", "alert_date_of_birth_message_1053")
  /// Date of Birth
  public static let AlertDateOfBirthTitle1052: String = CommonStrings.tr("Common", "alert_date_of_birth_title_1052")
  /// An unknown error occurred.
  public static let AlertErrorMessage269: String = CommonStrings.tr("Common", "alert_error_message_269")
  /// Error
  public static let AlertErrorTitle268: String = CommonStrings.tr("Common", "alert_error_title_268")
  /// The authentication code you entered is incorrect. Please enter it again or contact your employer if the issue persists.
  public static let AlertIncorrectCodeMessage371: String = CommonStrings.tr("Common", "alert_incorrect_code_message_371")
  /// Incorrect Authentication Code
  public static let AlertIncorrectCodeTitle370: String = CommonStrings.tr("Common", "alert_incorrect_code_title_370")
  /// The entity number you entered does not match the authentication code. Please try again or contact your employer if the issues persists.
  public static let AlertIncorrectNumberMessage373: String = CommonStrings.tr("Common", "alert_incorrect_number_message_373")
  /// Incorrect Entity Number
  public static let AlertIncorrectNumberTitle372: String = CommonStrings.tr("Common", "alert_incorrect_number_title_372")
  /// Invalid Account
  public static let AlertInvalidAccountTitle384: String = CommonStrings.tr("Common", "alert_invalid_account_title_384")
  /// Your membership policy has been cancelled. You will need to log in again with a new authentication code from your employer.
  public static let AlertInvalidAccountTitle385: String = CommonStrings.tr("Common", "alert_invalid_account_title_385")
  /// The website you are accessing has insecure web certificates in place. Please confirm if you would still like to continue.
  public static let AlertSslSiteIssue2125: String = CommonStrings.tr("Common", "alert_ssl_site_issue_2125")
  /// 
  public static let AlertSslSiteIssue9999: String = CommonStrings.tr("Common", "alert_ssl_site_issue_9999")
  /// An unknown error occurred. Please try again.
  public static let AlertUnknownMessage267: String = CommonStrings.tr("Common", "alert_unknown_message_267")
  /// Unknown Error
  public static let AlertUnknownTitle266: String = CommonStrings.tr("Common", "alert_unknown_title_266")
  /// Allow
  public static let AllowButton261: String = CommonStrings.tr("Common", "allow_button_261")
  /// Kilocalories per hour
  public static let AssessmentUnitOfMeasureKilocaloriesPerHourText2186: String = CommonStrings.tr("Common", "assessment_unit_of_measure_kilocalories_per_hour_text_2186")
  /// Back
  public static let BackButton336: String = CommonStrings.tr("Common", "back_button_336")
  /// Book Now
  public static let BookNowButtonTitle2188: String = CommonStrings.tr("Common", "book_now_button_title_2188")
  /// 
  public static let BookNowButtonTitle9999: String = CommonStrings.tr("Common", "book_now_button_title_9999")
  /// bpm
  public static let BpmUnitOfMeasure2189: String = CommonStrings.tr("Common", "bpm_unit_of_measure_2189")
  /// 
  public static let BpmUnitOfMeasure9992: String = CommonStrings.tr("Common", "bpm_unit_of_measure_9992")
  /// Cancel
  public static let CancelButtonTitle24: String = CommonStrings.tr("Common", "cancel_button_title_24")
  /// Activity Sync Pending
  public static let CardActionSubtitle2054: String = CommonStrings.tr("Common", "card_action_subtitle_2054")
  /// Complete Activation
  public static let CardActionTitle2053: String = CommonStrings.tr("Common", "card_action_title_2053")
  /// Activate New Cashback
  public static let CardActionTitle2068: String = CommonStrings.tr("Common", "card_action_title_2068")
  /// Cashback Activation Pending
  public static let CardActionTitleContinueActivation2190: String = CommonStrings.tr("Common", "card_action_title_continue_activation_2190")
  /// Cashback Activation Pending
  public static let CardActionTitleInprogress2191: String = CommonStrings.tr("Common", "card_action_title_inprogress_2191")
  /// Cashback Activation Pending
  public static let CardActionTitlePending2192: String = CommonStrings.tr("Common", "card_action_title_pending_2192")
  /// Redeem Reward
  public static let CardActionTitleRedeemReward1097: String = CommonStrings.tr("Common", "card_action_title_redeem_reward1097")
  /// Use Reward
  public static let CardActionTitleUseReward1093: String = CommonStrings.tr("Common", "card_action_title_use_reward1093")
  /// View Partners
  public static let CardActionTitleViewPartners842: String = CommonStrings.tr("Common", "card_action_title_view_partners_842")
  /// View Reward
  public static let CardActionTitleViewReward1092: String = CommonStrings.tr("Common", "card_action_title_view_reward1092")
  /// View Services
  public static let CardActionTitleViewServices1333: String = CommonStrings.tr("Common", "card_action_title_view_services_1333")
  /// View Vouchers
  public static let CardActionTitleViewVouchers2193: String = CommonStrings.tr("Common", "card_action_title_view_vouchers_2193")
  /// 
  public static let CardActionTitleViewVouchers999: String = CommonStrings.tr("Common", "card_action_title_view_vouchers_999")
  /// View Reward
  public static let CardActionViewReward1092: String = CommonStrings.tr("Common", "card_action_view_reward_1092")
  /// Corporate Reward
  public static let CardDetailScreenTitleCorporateReward2100: String = CommonStrings.tr("Common", "card_detail_screen_title_corporate_reward_2100")
  /// Active Challenge
  public static let CardHeadingActiveChallenge2194: String = CommonStrings.tr("Common", "card_heading_active_challenge_2194")
  /// 
  public static let CardHeadingActiveChallenge999: String = CommonStrings.tr("Common", "card_heading_active_challenge_999")
  /// Cineworld
  public static let CardHeadingCineworld1099: String = CommonStrings.tr("Common", "card_heading_cineworld1099")
  /// Cineworld or Vue
  public static let CardHeadingCineworldOrVue1098: String = CommonStrings.tr("Common", "card_heading_cineworld_or_vue1098")
  /// Employer Reward
  public static let CardHeadingEmployerReward2098: String = CommonStrings.tr("Common", "card_heading_employer_reward_2098")
  /// Gym Activation
  public static let CardHeadingGymActivation2073: String = CommonStrings.tr("Common", "card_heading_gym_activation_2073")
  /// Health Partners
  public static let CardHeadingHealthPartners843: String = CommonStrings.tr("Common", "card_heading_health_partners_843")
  /// Reward Partners
  public static let CardHeadingRewardPartners845: String = CommonStrings.tr("Common", "card_heading_reward_partners_845")
  /// Vue
  public static let CardHeadingVue1118: String = CommonStrings.tr("Common", "card_heading_vue_1118")
  /// Wellness Partners
  public static let CardHeadingWellnessPartners844: String = CommonStrings.tr("Common", "card_heading_wellness_partners_844")
  /// Available Rewards
  public static let CardTitleAvailableRewards2195: String = CommonStrings.tr("Common", "card_title_available_rewards_2195")
  /// 
  public static let CardTitleAvailableRewards999: String = CommonStrings.tr("Common", "card_title_available_rewards_999")
  /// Confirm Details
  public static let CardTitleConfirmDetails1091: String = CommonStrings.tr("Common", "card_title_confirm_details1091")
  /// Earn Rewards from Your Employer
  public static let CardTitleEarnRewardsFromYourEmployer2099: String = CommonStrings.tr("Common", "card_title_earn_rewards_from_your_employer_2099")
  /// Enjoy Great Rewards
  public static let CardTitleGreatRewards841: String = CommonStrings.tr("Common", "card_title_great_rewards_841")
  /// Enjoy Health Rewards
  public static let CardTitleHealthRewards839: String = CommonStrings.tr("Common", "card_title_health_rewards_839")
  /// Free Drink
  public static let CardTitleStarbucksFreeDrink1094: String = CommonStrings.tr("Common", "card_title_starbucks_free_drink1094")
  /// Free drink not awarded
  public static let CardTitleStarbucksFreeDrinkNotAwarded1096: String = CommonStrings.tr("Common", "card_title_starbucks_free_drink_not_awarded1096")
  /// Free drink pending
  public static let CardTitleStarbucksFreeDrinkPending1095: String = CommonStrings.tr("Common", "card_title_starbucks_free_drink_pending1095")
  /// Enjoy Wellness Rewards
  public static let CardTitleWellnessRewards840: String = CommonStrings.tr("Common", "card_title_wellness_rewards_840")
  /// Your Health Services
  public static let CardTitleYourHealthServices1332: String = CommonStrings.tr("Common", "card_title_your_health_services_1332")
  /// Cashbacks Earned
  public static let CashbacksEarned1568: String = CommonStrings.tr("Common", "cashbacks_earned_1568")
  /// 
  public static let CfBundleDisplayName: String = CommonStrings.tr("Common", "CFBundleDisplayName")
  /// 
  public static let CfBundleName: String = CommonStrings.tr("Common", "CFBundleName")
  /// What is Vitality Status?
  public static let CommunicationPrefButtonStatus377: String = CommonStrings.tr("Common", "communication_pref_button_status_377")
  /// Turn off "Keep Me Logged In" if you prefer to enter your email every time you access the app.
  public static let CommunicationPrefMessageKeepLoggedIn387: String = CommonStrings.tr("Common", "communication_pref_message_keep_logged_in_387")
  /// Enable sharing of your Vitality status if you would like your company to have this information.
  public static let CommunicationPrefMessageStatus376: String = CommonStrings.tr("Common", "communication_pref_message_status_376")
  /// Keep Me Logged In
  public static let CommunicationPrefTitleKeepLoggedIn386: String = CommonStrings.tr("Common", "communication_pref_title_keep_logged_in_386")
  /// Share Vitality Status
  public static let CommunicationPrefTitleStatus375: String = CommonStrings.tr("Common", "communication_pref_title_status_375")
  /// Your share vitality status preference could not be updated
  public static let CommunicationPrefUpdateError2405: String = CommonStrings.tr("Common", "communication_pref_update_error_2405")
  /// Confirm
  public static let ConfirmTitleButton182: String = CommonStrings.tr("Common", "confirm_title_button_182")
  /// A connection error has occurred. Please make sure that you are connected to a wi-fi or cellular network and try again.
  public static let ConnectivityErrorAlertMessage45: String = CommonStrings.tr("Common", "connectivity_error_alert_message_45")
  /// Connectivity error
  public static let ConnectivityErrorAlertTitle44: String = CommonStrings.tr("Common", "connectivity_error_alert_title_44")
  /// Continue
  public static let ContinueButtonTitle87: String = CommonStrings.tr("Common", "continue_button_title_87")
  /// Control your health data from the Health app.
  public static let ControlHealthAppDataMessage463: String = CommonStrings.tr("Common", "control_health_app_data_message_463")
  /// Data and Privacy Policy
  public static let DataAndPrivacyPolicyTitle123: String = CommonStrings.tr("Common", "data_and_privacy_policy_title_123")
  /// Date
  public static let DateTitle264: String = CommonStrings.tr("Common", "date_title_264")
  /// Invalid Model
  public static let DcCaptureModelError1499: String = CommonStrings.tr("Common", "dc_capture_model_error_1499")
  /// Invalid Purchase Amount
  public static let DcCapturePurchaseAmountError2218: String = CommonStrings.tr("Common", "dc_capture_purchase_amount_error_2218")
  /// Invalid Device Serial Number
  public static let DcCaptureSerialNumberError1500: String = CommonStrings.tr("Common", "dc_capture_serial_number_error_1500")
  /// %1$@ points
  public static func DcCashbackEarnedPointsSubtitle2219(_ p1: String) -> String {
    return CommonStrings.tr("Common", "dc_cashback_earned_points_subtitle_2219", p1)
  }
  /// $
  public static let DcCurrency2220: String = CommonStrings.tr("Common", "dc_currency_2220")
  /// Continue Activation
  public static let DcHomeCardFooter1516: String = CommonStrings.tr("Common", "dc_home_card_footer_1516")
  /// Starts %1$@
  public static func DcHomeCardFooter1528(_ p1: String) -> String {
    return CommonStrings.tr("Common", "dc_home_card_footer_1528", p1)
  }
  /// Estimated Delivery %1$@
  public static func DcHomeCardFooter1562(_ p1: String) -> String {
    return CommonStrings.tr("Common", "dc_home_card_footer_1562", p1)
  }
  /// Link Samsung Health
  public static let DcHomeCardFooterLinkSamsung2221: String = CommonStrings.tr("Common", "dc_home_card_footer_link_samsung_2221")
  /// Activity Sync Pending
  public static let DcHomeCardFooterSyncPending2222: String = CommonStrings.tr("Common", "dc_home_card_footer_sync_pending_2222")
  /// Activated
  public static let DcHomeCardMessage1527: String = CommonStrings.tr("Common", "dc_home_card_message_1527")
  /// Awaiting Shipment
  public static let DcHomeCardMessage1561: String = CommonStrings.tr("Common", "dc_home_card_message_1561")
  /// This month's activity
  public static let DcLandingActivityTitle1533: String = CommonStrings.tr("Common", "dc_landing_activity_title_1533")
  /// Once your monthly target begins you can start earning points towards your cashback.
  public static let DcLandingMonthlyTargetDescription1526: String = CommonStrings.tr("Common", "dc_landing_monthly_target_description_1526")
  /// Your Monthly Target will start %1$@, where you can start earning points towards your cashback.
  public static func DcLandingMonthlyTargetDescriptionActivated2223(_ p1: String) -> String {
    return CommonStrings.tr("Common", "dc_landing_monthly_target_description_activated_2223", p1)
  }
  /// No activity yet
  public static let DcLandingNoActivityIntro1534: String = CommonStrings.tr("Common", "dc_landing_no_activity_intro_1534")
  /// Only events resulting in points will be shown here
  public static let DcLandingNoActivitySubIntro1535: String = CommonStrings.tr("Common", "dc_landing_no_activity_sub_intro_1535")
  /// Month %1$@ of %2$@
  public static func DcLandingProgressMonth1531(_ p1: String, _ p2: String) -> String {
    return CommonStrings.tr("Common", "dc_landing_progress_month_1531", p1, p2)
  }
  /// %1$@ Points Earned
  public static func DcLandingProgressPointsEarned2224(_ p1: String) -> String {
    return CommonStrings.tr("Common", "dc_landing_progress_points_earned_2224", p1)
  }
  /// %1$@ Points Reached
  public static func DcLandingProgressPointsReached2225(_ p1: String) -> String {
    return CommonStrings.tr("Common", "dc_landing_progress_points_reached_2225", p1)
  }
  /// Monthly Target
  public static let DcLandingProgressTitle1530: String = CommonStrings.tr("Common", "dc_landing_progress_title_1530")
  /// Once your device is delivered, look for information on when your monthly target begins so you can start earning points towards your cashback.
  public static let DcLandingShipmentDescription1558: String = CommonStrings.tr("Common", "dc_landing_shipment_description_1558")
  /// Your device has an estimated delivery date for %1$@
  public static func DcLandingShipmentTitle1557(_ p1: String) -> String {
    return CommonStrings.tr("Common", "dc_landing_shipment_title_1557", p1)
  }
  /// %1$@ Cashback Earned
  public static func DcPreviousCashbackEarned2226(_ p1: String) -> String {
    return CommonStrings.tr("Common", "dc_previous_cashback_earned_2226", p1)
  }
  /// %1$@ Cashbacks Earned
  public static func DcPreviousCashbackEarnedSubtitle2227(_ p1: String) -> String {
    return CommonStrings.tr("Common", "dc_previous_cashback_earned_subtitle_2227", p1)
  }
  /// I Declare
  public static let DeclareButtonTitle114: String = CommonStrings.tr("Common", "declare_button_title_114")
  /// Are you sure you want to delink the %1$@ from Vitality? You will have to link it again to earn points from it.
  public static func DelinkDialogDescription465(_ p1: String) -> String {
    return CommonStrings.tr("Common", "delink_dialog_description_465", p1)
  }
  /// An error occured when trying to delink %@. Please try again.
  public static func DelinkFailedDialogDescription469(_ p1: String) -> String {
    return CommonStrings.tr("Common", "delink_failed_dialog_description_469", p1)
  }
  /// Deny
  public static let DenyButton260: String = CommonStrings.tr("Common", "deny_button_260")
  /// Disagree
  public static let DisagreeButtonTitle49: String = CommonStrings.tr("Common", "disagree_button_title_49")
  /// Disclaimer
  public static let DisclaimerTitle265: String = CommonStrings.tr("Common", "disclaimer_title_265")
  /// Done
  public static let DoneButtonTitle53: String = CommonStrings.tr("Common", "done_button_title_53")
  /// Email
  public static let EmailFieldPlaceholder18: String = CommonStrings.tr("Common", "email_field_placeholder_18")
  /// No Entity Number
  public static let EntityNumberFieldPlaceholder1103: String = CommonStrings.tr("Common", "entity_number_field_placeholder_1103")
  /// Range %1$@ - %2$@
  public static func ErrorRange180(_ p1: String, _ p2: String) -> String {
    return CommonStrings.tr("Common", "error_range_180", p1, p2)
  }
  /// Range > %@
  public static func ErrorRangeBigger281(_ p1: String) -> String {
    return CommonStrings.tr("Common", "error_range_bigger_281", p1)
  }
  /// Range < %@
  public static func ErrorRangeSmaller282(_ p1: String) -> String {
    return CommonStrings.tr("Common", "error_range_smaller_282", p1)
  }
  /// Required
  public static let ErrorRequired289: String = CommonStrings.tr("Common", "error_required_289")
  /// Required for Points
  public static let ErrorRequiredForPoints1170: String = CommonStrings.tr("Common", "error_required_for_points_1170")
  /// An unknown error occurred. Please try again.
  public static let ErrorUnableToLoadMessage504: String = CommonStrings.tr("Common", "error_unable_to_load_message_504")
  /// Unable to Load
  public static let ErrorUnableToLoadTitle503: String = CommonStrings.tr("Common", "error_unable_to_load_title_503")
  /// All Events
  public static let EventsAllEvents: String = CommonStrings.tr("Common", "events_all_events")
  /// Activation
  public static let EventsCategoryActivation1128: String = CommonStrings.tr("Common", "events_category_activation_1128")
  /// Agreement
  public static let EventsCategoryAgreement2563: String = CommonStrings.tr("Common", "events_category_agreement_2563")
  /// Assessment
  public static let EventsCategoryAssessment1129: String = CommonStrings.tr("Common", "events_category_assessment_1129")
  /// Data Sharing and Legal
  public static let EventsCategoryDataSharingAndLegal1130: String = CommonStrings.tr("Common", "events_category_data_sharing_and_legal_1130")
  /// Devices
  public static let EventsCategoryDevices1131: String = CommonStrings.tr("Common", "events_category_devices_1131")
  /// Disclaimer
  public static let EventsCategoryDisclaimer2564: String = CommonStrings.tr("Common", "events_category_disclaimer_2564")
  /// Documents
  public static let EventsCategoryDocuments1132: String = CommonStrings.tr("Common", "events_category_documents_1132")
  /// Enrollment
  public static let EventsCategoryEnrollment2565: String = CommonStrings.tr("Common", "events_category_enrollment_2565")
  /// Financials
  public static let EventsCategoryFinancials1133: String = CommonStrings.tr("Common", "events_category_financials_1133")
  /// Get Active
  public static let EventsCategoryGetActive1134: String = CommonStrings.tr("Common", "events_category_get_active_1134")
  /// Health Data
  public static let EventsCategoryHealthData1135: String = CommonStrings.tr("Common", "events_category_health_data_1135")
  /// Login
  public static let EventsCategoryLogin: String = CommonStrings.tr("Common", "events_category_login")
  /// Notification
  public static let EventsCategoryNotification2566: String = CommonStrings.tr("Common", "events_category_notification_2566")
  /// Nutrition
  public static let EventsCategoryNutrition: String = CommonStrings.tr("Common", "events_category_nutrition")
  /// Other
  public static let EventsCategoryOther: String = CommonStrings.tr("Common", "events_category_other")
  /// Points
  public static let EventsCategoryPoints2567: String = CommonStrings.tr("Common", "events_category_points_2567")
  /// Profile Management
  public static let EventsCategoryProfileManagement1136: String = CommonStrings.tr("Common", "events_category_profile_management_1136")
  /// Rewards
  public static let EventsCategoryRewards: String = CommonStrings.tr("Common", "events_category_rewards")
  /// ScreenAndVacc
  public static let EventsCategorySav2568: String = CommonStrings.tr("Common", "events_category_sav_2568")
  /// Screening
  public static let EventsCategoryScreenings: String = CommonStrings.tr("Common", "events_category_screenings")
  /// Servicing
  public static let EventsCategoryServicing1138: String = CommonStrings.tr("Common", "events_category_servicing_1138")
  /// Status
  public static let EventsCategoryStatus: String = CommonStrings.tr("Common", "events_category_status")
  /// Target
  public static let EventsCategoryTarget2569: String = CommonStrings.tr("Common", "events_category_target_2569")
  /// Terms and Conditions
  public static let EventsCategoryTermsandconditions2570: String = CommonStrings.tr("Common", "events_category_termsandconditions_2570")
  /// Vaccinations
  public static let EventsCategoryVaccinations2571: String = CommonStrings.tr("Common", "events_category_vaccinations_2571")
  /// VHC
  public static let EventsCategoryVhc2572: String = CommonStrings.tr("Common", "events_category_vhc_2572")
  /// CHOOSE EVENTS CATEGORY
  public static let EventsChooseCategory: String = CommonStrings.tr("Common", "events_choose_category")
  /// No Activity for %1$@ %2$@
  public static func EventsFooter(_ p1: String, _ p2: String) -> String {
    return CommonStrings.tr("Common", "events_footer", p1, p2)
  }
  /// Help
  public static let EventsHelp: String = CommonStrings.tr("Common", "events_help")
  /// No Events Activity
  public static let EventsNoEventsActivity: String = CommonStrings.tr("Common", "events_no_events_activity")
  /// There is no events activity recorded for the selected month.
  public static let EventsNoEventsRecorded: String = CommonStrings.tr("Common", "events_no_events_recorded")
  /// False
  public static let FalseTitle362: String = CommonStrings.tr("Common", "false_title_362")
  /// %1$@ feet %2$@ inches
  public static func FootInchSummary2231(_ p1: String, _ p2: String) -> String {
    return CommonStrings.tr("Common", "foot_inch_summary_2231", p1, p2)
  }
  /// 
  public static let FootInchSummary9999: String = CommonStrings.tr("Common", "foot_inch_summary_9999")
  /// less
  public static let FootnoteLessButton343: String = CommonStrings.tr("Common", "footnote_less_button_343")
  /// more
  public static let FootnoteMoreButton342: String = CommonStrings.tr("Common", "footnote_more_button_342")
  /// Close
  public static let GenericCloseButtonTitle10: String = CommonStrings.tr("Common", "generic_close_button_title_10")
  /// Disclaimer
  public static let GenericDisclaimerButtonTitle265: String = CommonStrings.tr("Common", "generic_disclaimer_button_title_265")
  /// Got it!
  public static let GenericGotItButtonTitle131: String = CommonStrings.tr("Common", "generic_got_it_button_title_131")
  /// N/A
  public static let GenericNotAvailable270: String = CommonStrings.tr("Common", "generic_not_available_270")
  /// Rate us
  public static let GenericRateUsButtonTitle575: String = CommonStrings.tr("Common", "generic_rate_us_button_title_575")
  /// Send Feedback
  public static let GenericSendFeedbackButtonTitle576: String = CommonStrings.tr("Common", "generic_send_feedback_button_title_576")
  /// Our system is not responding at the moment. Apologies for the inconvenience. Please try again later.
  public static let GenericServiceErrorMessage269: String = CommonStrings.tr("Common", "generic_service_error_message_269")
  /// Error
  public static let GenericServiceErrorTitle268: String = CommonStrings.tr("Common", "generic_service_error_title_268")
  /// Settings
  public static let GenericSettingsButtonTitle271: String = CommonStrings.tr("Common", "generic_settings_button_title_271")
  /// An unkown error occured. Please try again.
  public static let GenericUnkownErrorMessage267: String = CommonStrings.tr("Common", "generic_unkown_error_message_267")
  /// Unkown error
  public static let GenericUnkownErrorTitle266: String = CommonStrings.tr("Common", "generic_unkown_error_title_266")
  /// Get Cashbacks
  public static let GetCashbacks1462: String = CommonStrings.tr("Common", "get_cashbacks_1462")
  /// Get Started
  public static let GetStartedButtonTitle103: String = CommonStrings.tr("Common", "get_started_button_title_103")
  /// Great!
  public static let GreatButtonTitle120: String = CommonStrings.tr("Common", "great_button_title_120")
  /// View Barcode
  public static let GymActivationHeaderSubtitle2075: String = CommonStrings.tr("Common", "gym_activation_header_subtitle_2075")
  /// Activation Barcode
  public static let GymActivationHeaderTitle2074: String = CommonStrings.tr("Common", "gym_activation_header_title_2074")
  /// Having Trouble?
  public static let HavingTroubleButtonText461: String = CommonStrings.tr("Common", "having_trouble_button_text_461")
  /// Nuffield Health looks after your health and wellbeing in ways that go beyond getting you fit and getting you back on your feet, that's what makes us specialists in you.
  public static let HealthServicesLandingHeader1334: String = CommonStrings.tr("Common", "health_services_landing_header_1334")
  /// Health services
  public static let HealthServicesTitle1331: String = CommonStrings.tr("Common", "health_services_title_1331")
  /// Help
  public static let HelpButton141: String = CommonStrings.tr("Common", "help_button_141")
  /// Help
  public static let HelpButtonTitle141: String = CommonStrings.tr("Common", "help_button_title_141")
  /// Ask a question
  public static let HelpViewSearchbarPlaceholder263: String = CommonStrings.tr("Common", "help_view_searchbar_placeholder_263")
  /// History
  public static let HistoryButton140: String = CommonStrings.tr("Common", "history_button_140")
  /// Select
  public static let LabelSelect288: String = CommonStrings.tr("Common", "label_select_288")
  /// Learn More
  public static let LearnMoreButtonTitle104: String = CommonStrings.tr("Common", "learn_more_button_title_104")
  /// Learn More
  public static let LearnMoreTitle1082: String = CommonStrings.tr("Common", "learn_more_title_1082")
  /// Link Now
  public static let LinkNowButtonText434: String = CommonStrings.tr("Common", "link_now_button_text_434")
  /// Link Samsung Health
  public static let LinkSamsungHealth2057: String = CommonStrings.tr("Common", "link_samsung_health_2057")
  /// This account has already registered. Please input your user name and password to login.
  public static let LoginResendInsurerCodeErrorMessage2281: String = CommonStrings.tr("Common", "login_resend_insurer_code_error_message_2281")
  /// 
  public static let LoginResendInsurerCodeErrorMessage9999: String = CommonStrings.tr("Common", "login_resend_insurer_code_error_message_9999")
  /// You have already registered
  public static let LoginResendInsurerCodeErrorTitle2282: String = CommonStrings.tr("Common", "login_resend_insurer_code_error_title_2282")
  /// 
  public static let LoginResendInsurerCodeErrorTitle9999: String = CommonStrings.tr("Common", "login_resend_insurer_code_error_title_9999")
  /// For You
  public static let MenuForyouButton2521: String = CommonStrings.tr("Common", "menu_foryou_button_2521")
  /// Help
  public static let MenuHelpButton8: String = CommonStrings.tr("Common", "menu_help_button_8")
  /// Home
  public static let MenuHomeButton5: String = CommonStrings.tr("Common", "menu_home_button_5")
  /// Manage Devices and Apps
  public static let MenuManagedevicesButton2522: String = CommonStrings.tr("Common", "menu_managedevices_button_2522")
  /// My benefits
  public static let MenuMyBenefitsButton2288: String = CommonStrings.tr("Common", "menu_my_benefits_button_2288")
  /// 
  public static let MenuMyBenefitsButton999: String = CommonStrings.tr("Common", "menu_my_benefits_button_999")
  /// My Health
  public static let MenuMyhealthButton7: String = CommonStrings.tr("Common", "menu_myhealth_button_7")
  /// Points
  public static let MenuPointsButton6: String = CommonStrings.tr("Common", "menu_points_button_6")
  /// Profile
  public static let MenuProfileButton9: String = CommonStrings.tr("Common", "menu_profile_button_9")
  /// More Results
  public static let MoreResults792: String = CommonStrings.tr("Common", "more_results_792")
  /// More Tips
  public static let MoreTips791: String = CommonStrings.tr("Common", "more_tips_791")
  /// Explore our reward partners, improve your health and save money.
  public static let NewOnboardingEnjoyRewardsDescription2117: String = CommonStrings.tr("Common", "new_onboarding_enjoy_rewards_description_2117")
  /// Enjoy the rewards
  public static let NewOnboardingEnjoyRewardsTitle2116: String = CommonStrings.tr("Common", "new_onboarding_enjoy_rewards_title_2116")
  /// Swipe cards sideways to access different functionalities – and to uncover more ways to improve your health.
  public static let NewOnboardingExploreDescription2119: String = CommonStrings.tr("Common", "new_onboarding_explore_description_2119")
  /// Explore
  public static let NewOnboardingExploreTitle2118: String = CommonStrings.tr("Common", "new_onboarding_explore_title_2118")
  /// Take the Health Review to find out your Vitality Age and learn more about your health.
  public static let NewOnboardingHealthReviewDescription2113: String = CommonStrings.tr("Common", "new_onboarding_health_review_description_2113")
  /// Take the Health Review
  public static let NewOnboardingHealthReviewTitle2112: String = CommonStrings.tr("Common", "new_onboarding_health_review_title_2112")
  /// Link your health app or wearable device to track your activity and earn Vitality points.
  public static let NewOnboardingTrackActivityDescription2115: String = CommonStrings.tr("Common", "new_onboarding_track_activity_description_2115")
  /// Track your activity
  public static let NewOnboardingTrackActivityTitle2114: String = CommonStrings.tr("Common", "new_onboarding_track_activity_title_2114")
  /// Next
  public static let NextButtonTitle84: String = CommonStrings.tr("Common", "next_button_title_84")
  /// N/A
  public static let NotAvailableTitle270: String = CommonStrings.tr("Common", "not_available_title_270")
  /// 
  public static let NsCameraUsageDescription: String = CommonStrings.tr("Common", "NSCameraUsageDescription")
  /// 
  public static let NsFaceIDUsageDescription: String = CommonStrings.tr("Common", "NSFaceIDUsageDescription")
  /// 
  public static let NsHealthShareUsageDescription: String = CommonStrings.tr("Common", "NSHealthShareUsageDescription")
  /// 
  public static let NsHealthUpdateUsageDescription: String = CommonStrings.tr("Common", "NSHealthUpdateUsageDescription")
  /// 
  public static let NsPhotoLibraryAddUsageDescription: String = CommonStrings.tr("Common", "NSPhotoLibraryAddUsageDescription")
  /// 
  public static let NsPhotoLibraryUsageDescription: String = CommonStrings.tr("Common", "NSPhotoLibraryUsageDescription")
  /// Ok
  public static let OkButtonTitle40: String = CommonStrings.tr("Common", "ok_button_title_40")
  /// Achieve your weekly targets with Active Rewards and earn great rewards from our partners.
  public static let OnboardingEnjoyWeeklyRewardsMessage383: String = CommonStrings.tr("Common", "onboarding_enjoy_weekly_rewards_message_383")
  /// Enjoy Weekly Rewards
  public static let OnboardingEnjoyWeeklyRewardsTitle382: String = CommonStrings.tr("Common", "onboarding_enjoy_weekly_rewards_title_382")
  /// Activate Vitality Active Rewards and track your exercise to meet your weekly target.
  public static let OnboardingImproveYourHealthMessage381: String = CommonStrings.tr("Common", "onboarding_improve_your_health_message_381")
  /// Improve Your Health
  public static let OnboardingImproveYourHealthTitle380: String = CommonStrings.tr("Common", "onboarding_improve_your_health_title_380")
  /// Complete assessments and tasks to get an overview of your health status.
  public static let OnboardingKnowYourHealthMessage379: String = CommonStrings.tr("Common", "onboarding_know_your_health_message_379")
  /// Know Your Health
  public static let OnboardingKnowYourHealthTitle378: String = CommonStrings.tr("Common", "onboarding_know_your_health_title_378")
  /// Participating Partners
  public static let ParticipatingPartnersTitle262: String = CommonStrings.tr("Common", "participating_partners_title_262")
  /// Password
  public static let PasswordFieldPlaceholder19: String = CommonStrings.tr("Common", "password_field_placeholder_19")
  /// Previous Cashbacks
  public static let PreviousCashbacks1540: String = CommonStrings.tr("Common", "previous_cashbacks_1540")
  /// Cancel
  public static let ProfileChangeEmailCancel: String = CommonStrings.tr("Common", "profile_change_email_cancel")
  /// Continue
  public static let ProfileChangeEmailContinue: String = CommonStrings.tr("Common", "profile_change_email_continue")
  /// Current Email
  public static let ProfileChangeEmailCurrentEmailHeading: String = CommonStrings.tr("Common", "profile_change_email_current_email_heading")
  /// name@email.com
  public static let ProfileChangeEmailEmailPlaceholder: String = CommonStrings.tr("Common", "profile_change_email_email_placeholder")
  /// This email will be used for all Vitality communications as well as login. \n\nChanging this email will not affect your email with your insurer, contact them if you would like to update it.
  public static let ProfileChangeEmailFooterValue: String = CommonStrings.tr("Common", "profile_change_email_footerValue")
  /// Forgot Password?
  public static let ProfileChangeEmailForgotPassword: String = CommonStrings.tr("Common", "profile_change_email_forgot_password")
  /// The password you entered is incorrect. Please try again.
  public static let ProfileChangeEmailIncorrectPasswordBody: String = CommonStrings.tr("Common", "profile_change_email_incorrect_password_body")
  /// Incorrect Password
  public static let ProfileChangeEmailIncorrectPasswordHeading: String = CommonStrings.tr("Common", "profile_change_email_incorrect_password_heading")
  /// New Email
  public static let ProfileChangeEmailNewEmailHeading: String = CommonStrings.tr("Common", "profile_change_email_new_email_heading")
  /// Next
  public static let ProfileChangeEmailNext: String = CommonStrings.tr("Common", "profile_change_email_next")
  /// OK
  public static let ProfileChangeEmailOk: String = CommonStrings.tr("Common", "profile_change_email_ok")
  /// Password
  public static let ProfileChangeEmailPasswordPlaceholder: String = CommonStrings.tr("Common", "profile_change_email_password_placeholder")
  /// Enter your Vitality password to confirm the change of your email to
  public static let ProfileChangeEmailPasswordRequiredBody: String = CommonStrings.tr("Common", "profile_change_email_password_required_body")
  /// Password Required
  public static let ProfileChangeEmailPasswordRequiredHeading: String = CommonStrings.tr("Common", "profile_change_email_password_required_heading")
  /// You’ll need to use this new email address when you next log in. Please note that this will not change the email address that is linked to your insurer.
  public static let ProfileChangeEmailPromptContent: String = CommonStrings.tr("Common", "profile_change_email_prompt_content")
  /// Are You Sure You Want To Change Your Email?
  public static let ProfileChangeEmailPromptHeading: String = CommonStrings.tr("Common", "profile_change_email_prompt_heading")
  /// Change Email
  public static let ProfileChangeEmailTitle: String = CommonStrings.tr("Common", "profile_change_email_title")
  /// The email address you've entered is already in use. Please enter a different email address.
  public static let ProfileChangeEmailUnableToChangeBody: String = CommonStrings.tr("Common", "profile_change_email_unable_to_change_body")
  /// Unable to Change Email
  public static let ProfileChangeEmailUnableToChangeHeading: String = CommonStrings.tr("Common", "profile_change_email_unable_to_change_heading")
  /// Add Profile Image Later
  public static let ProfileDigitalPassAddLaterPrompt: String = CommonStrings.tr("Common", "profile_digital_pass_add_later_prompt")
  /// Add Profile Image Now
  public static let ProfileDigitalPassAddNowPrompt: String = CommonStrings.tr("Common", "profile_digital_pass_add_now_prompt")
  /// Your profile image will be used for verification purposes with qualifying partners
  public static let ProfileDigitalPassAlertDescripion: String = CommonStrings.tr("Common", "profile_digital_pass_alert_descripion")
  /// Provide a Profile image for your Vitality Digital Pass
  public static let ProfileDigitalPassAlertTitle: String = CommonStrings.tr("Common", "profile_digital_pass_alert_title")
  /// Change Profile Image
  public static let ProfileDigitalPassChangeImagePrompt: String = CommonStrings.tr("Common", "profile_digital_pass_change_image_prompt")
  /// Downloading digital pass...
  public static let ProfileDigitalPassDownloadingPrompt: String = CommonStrings.tr("Common", "profile_digital_pass_downloading_prompt")
  /// An error occured while downloading your digital pass. Please try again.
  public static let ProfileDigitalPassErrorMessagePrompt: String = CommonStrings.tr("Common", "profile_digital_pass_error_message_prompt")
  /// Are you sure you want to remove this pass?
  public static let ProfileDigitalPassRemovePassAlertDescription: String = CommonStrings.tr("Common", "profile_digital_pass_remove_pass_alert_description")
  /// Remove pass
  public static let ProfileDigitalPassRemovePassPrompt: String = CommonStrings.tr("Common", "profile_digital_pass_remove_pass_prompt")
  /// Show Pass in Wallet
  public static let ProfileDigitalPassShowInWalletPrompt: String = CommonStrings.tr("Common", "profile_digital_pass_show_in_wallet_prompt")
  /// Digital Pass
  public static let ProfileDigitalPassTitle: String = CommonStrings.tr("Common", "profile_digital_pass_title")
  /// Use Existing Profile Image
  public static let ProfileDigitalPassUseExistingImagePrompt: String = CommonStrings.tr("Common", "profile_digital_pass_use_existing_image_prompt")
  /// This information will be used for communication purposes.
  public static let ProfileEditCommunicationUsage: String = CommonStrings.tr("Common", "profile_edit_communication_usage")
  /// You'll need to use this email when logging in next time. Please note that this will not change the email linked to your insurer
  public static let ProfileEditMailPromptBody: String = CommonStrings.tr("Common", "profile_edit_mail_prompt_body")
  /// Are You Sure You Want To Change Your Email?
  public static let ProfileEditMailPromptTitle: String = CommonStrings.tr("Common", "profile_edit_mail_prompt_title")
  /// Edit Email
  public static let ProfileEditMailTitle: String = CommonStrings.tr("Common", "profile_edit_mail_title")
  /// Enter a valid email address
  public static let ProfileEditMailValidation: String = CommonStrings.tr("Common", "profile_edit_mail_validation")
  /// e.g. +27725418581
  public static let ProfileEditMobileExample: String = CommonStrings.tr("Common", "profile_edit_mobile_example")
  /// +27725418581
  public static let ProfileEditMobilePlaceholder: String = CommonStrings.tr("Common", "profile_edit_mobile_placeholder")
  /// Edit Mobile
  public static let ProfileEditMobileTitle: String = CommonStrings.tr("Common", "profile_edit_mobile_title")
  /// Enter a valid mobile number
  public static let ProfileEditMobileValidation: String = CommonStrings.tr("Common", "profile_edit_mobile_validation")
  /// Edit
  public static let ProfileEditTitle: String = CommonStrings.tr("Common", "profile_edit_title")
  /// This email will be used for all Vitality communications as well as login.
  public static let ProfileEmailCommunicationsNotice: String = CommonStrings.tr("Common", "profile_email_communications_notice")
  /// This email will be used for all Vitality communications as well as login.
  public static let ProfileEmailLoginNotice: String = CommonStrings.tr("Common", "profile_email_login_notice")
  /// Customer Number
  public static let ProfileFieldCustomerNumberTitle1124: String = CommonStrings.tr("Common", "profile_field_customer_number_title_1124")
  /// Date of Birth
  public static let ProfileFieldDobTitle: String = CommonStrings.tr("Common", "profile_field_dob_title")
  /// Email
  public static let ProfileFieldEmailTitle: String = CommonStrings.tr("Common", "profile_field_email_title")
  /// Gender
  public static let ProfileFieldGenderTitle: String = CommonStrings.tr("Common", "profile_field_gender_title")
  /// Membership Number
  public static let ProfileFieldMembershipNumberTitle1125: String = CommonStrings.tr("Common", "profile_field_membership_number_title_1125")
  /// Membership Start Date
  public static let ProfileFieldMembershipStartDateTitle: String = CommonStrings.tr("Common", "profile_field_membership_start_date_title")
  /// Membership Status
  public static let ProfileFieldMembershipStatusTitle: String = CommonStrings.tr("Common", "profile_field_membership_status_title")
  /// Mobile
  public static let ProfileFieldMobileTitle: String = CommonStrings.tr("Common", "profile_field_mobile_title")
  /// Vitality Membership number
  public static let ProfileFieldNumberTitle: String = CommonStrings.tr("Common", "profile_field_number_title")
  /// A unique numeric identifier for an individual member.
  public static let ProfileFieldVitalityCustomerNumber: String = CommonStrings.tr("Common", "profile_field_vitality_customer_number")
  /// A unique numeric identifier for a member's Vitality membership.
  public static let ProfileFieldVitalityMembershipNumber: String = CommonStrings.tr("Common", "profile_field_vitality_membership_number")
  /// Your Vitality number is a combination of your Membership number and Customer number. This number is used as a unique identifier with qualifying partners.
  public static let ProfileFieldVitalityNumberInformation: String = CommonStrings.tr("Common", "profile_field_vitality_number_information")
  /// Vitality Number
  public static let ProfileFieldVitalityNumberTitle: String = CommonStrings.tr("Common", "profile_field_vitality_number_title")
  /// Vitality Status
  public static let ProfileFieldVitalityStatusTitle: String = CommonStrings.tr("Common", "profile_field_vitality_status_title")
  /// Help
  public static let ProfileHelpTitle: String = CommonStrings.tr("Common", "profile_help_title")
  /// This information comes from your insurer. If it's incorrect please contact them. 
  public static let ProfileInsuranceDataNotice: String = CommonStrings.tr("Common", "profile_insurance_data_notice")
  /// Enter Email
  public static let ProfileMailPlaceholder: String = CommonStrings.tr("Common", "profile_mail_placeholder")
  /// Active
  public static let ProfileMembershipActiveStatusDescription1126: String = CommonStrings.tr("Common", "profile_membership_active_status_description_1126")
  /// Membership Details
  public static let ProfileMembershipDetailsTitle: String = CommonStrings.tr("Common", "profile_membership_details_title")
  /// Cancelled
  public static let ProfileMembershipInactiveStatusDescription1127: String = CommonStrings.tr("Common", "profile_membership_inactive_status_description_1127")
  /// Add your digital pass to your Apple Wallet to use at qualifying partners.
  public static let ProfileMembershipPassDescription: String = CommonStrings.tr("Common", "profile_membership_pass_description")
  /// Enter Mobile
  public static let ProfileMobilePlaceholder: String = CommonStrings.tr("Common", "profile_mobile_placeholder")
  /// Take Photo
  public static let ProfilePhotoCameraTitle: String = CommonStrings.tr("Common", "profile_photo_camera_title")
  /// Choose from Library
  public static let ProfilePhotoLibraryTitle: String = CommonStrings.tr("Common", "profile_photo_library_title")
  /// Personal Details
  public static let ProfileTitle: String = CommonStrings.tr("Common", "profile_title")
  /// Qualifying Samsung Devices
  public static let QualifyingSamsungDevices2046: String = CommonStrings.tr("Common", "qualifying_samsung_devices_2046")
  /// Recent Result
  public static let RecentResult1144: String = CommonStrings.tr("Common", "recent_result_1144")
  /// Get Rewarded
  public static let SectionTitleGetRewarded278: String = CommonStrings.tr("Common", "section_title_get_rewarded_278")
  /// Improve Your Health
  public static let SectionTitleImproveYourHealth277: String = CommonStrings.tr("Common", "section_title_improve_your_health_277")
  /// Know Your Health
  public static let SectionTitleKnowYourHealth276: String = CommonStrings.tr("Common", "section_title_know_your_health_276")
  /// Settings
  public static let SettingsButton271: String = CommonStrings.tr("Common", "settings_button_271")
  /// 
  public static let SettingsCommunicationTitle: String = CommonStrings.tr("Common", "settings_communication_title")
  /// 
  public static let SettingsFeedbackTitle: String = CommonStrings.tr("Common", "settings_feedback_title")
  /// 
  public static let SettingsLandingEventsTitle: String = CommonStrings.tr("Common", "settings_landing_events_title")
  /// 
  public static let SettingsLandingMembershipPassTitle: String = CommonStrings.tr("Common", "settings_landing_membership_pass_title")
  /// 
  public static let SettingsLandingSettingsTitle: String = CommonStrings.tr("Common", "settings_landing_settings_title")
  /// 
  public static let SettingsLandingTitle: String = CommonStrings.tr("Common", "settings_landing_title")
  /// 
  public static let SettingsLogoutPrompt: String = CommonStrings.tr("Common", "settings_logout_prompt")
  /// 
  public static let SettingsLogoutTitle: String = CommonStrings.tr("Common", "settings_logout_title")
  /// 
  public static let SettingsPrivacyTitle: String = CommonStrings.tr("Common", "settings_privacy_title")
  /// 
  public static let SettingsRateTitle: String = CommonStrings.tr("Common", "settings_rate_title")
  /// 
  public static let SettingsSecurityTitle: String = CommonStrings.tr("Common", "settings_security_title")
  /// 
  public static let SettingsTermsTitle: String = CommonStrings.tr("Common", "settings_terms_title")
  /// 
  public static let SettingsUnitsTitle: String = CommonStrings.tr("Common", "settings_units_title")
  /// Skip
  public static let SkipButtonTitle11: String = CommonStrings.tr("Common", "skip_button_title_11")
  /// Something went wrong
  public static let SomethingWentWrong2400: String = CommonStrings.tr("Common", "something_went_wrong_2400")
  /// 
  public static let SomethingWentWrong9999: String = CommonStrings.tr("Common", "something_went_wrong_9999")
  /// Steps To Link
  public static let StepsToLinkButtonText471: String = CommonStrings.tr("Common", "steps_to_link_button_text_471")
  /// steps
  public static let StepsUnitOfMeasure2401: String = CommonStrings.tr("Common", "steps_unit_of_measure_2401")
  /// 
  public static let StepsUnitOfMeasure9991: String = CommonStrings.tr("Common", "steps_unit_of_measure_9991")
  /// %1$@ stone %2$@ pound
  public static func StonePoundSummary2402(_ p1: String, _ p2: String) -> String {
    return CommonStrings.tr("Common", "stone_pound_summary_2402", p1, p2)
  }
  /// 
  public static let StonePoundSummary9999: String = CommonStrings.tr("Common", "stone_pound_summary_9999")
  /// Tips to Improve
  public static let TipsToImprove1121: String = CommonStrings.tr("Common", "tips_to_improve_1121")
  /// Tips to Maintain
  public static let TipsToMaintain1122: String = CommonStrings.tr("Common", "tips_to_maintain_1122")
  /// True
  public static let TrueTitle361: String = CommonStrings.tr("Common", "true_title_361")
  /// Try again
  public static let TryAgainButtonTitle43: String = CommonStrings.tr("Common", "try_again_button_title_43")
  /// 
  public static let UkeNewOnboardingEnjoyRewardsDescription2117: String = CommonStrings.tr("Common", "uke_new_onboarding_enjoy_rewards_description_2117")
  /// 
  public static let UkeNewOnboardingEnjoyRewardsTitle2116: String = CommonStrings.tr("Common", "uke_new_onboarding_enjoy_rewards_title_2116")
  /// 
  public static let UkeNewOnboardingExploreDescription2119: String = CommonStrings.tr("Common", "uke_new_onboarding_explore_description_2119")
  /// 
  public static let UkeNewOnboardingExploreTitle2118: String = CommonStrings.tr("Common", "uke_new_onboarding_explore_title_2118")
  /// 
  public static let UkeNewOnboardingHealthReviewDescription2113: String = CommonStrings.tr("Common", "uke_new_onboarding_health_review_description_2113")
  /// 
  public static let UkeNewOnboardingHealthReviewTitle2112: String = CommonStrings.tr("Common", "uke_new_onboarding_health_review_title_2112")
  /// 
  public static let UkeNewOnboardingTrackActivityDescription2115: String = CommonStrings.tr("Common", "uke_new_onboarding_track_activity_description_2115")
  /// 
  public static let UkeNewOnboardingTrackActivityTitle2114: String = CommonStrings.tr("Common", "uke_new_onboarding_track_activity_title_2114")
  /// km/h
  public static let UnitOfMeasureKilometerPerHourAbbreviation2406: String = CommonStrings.tr("Common", "unit_of_measure_kilometer_per_hour_abbreviation_2406")
  /// 
  public static let UnitOfMeasureKilometerPerHourAbbreviation9999: String = CommonStrings.tr("Common", "unit_of_measure_kilometer_per_hour_abbreviation_9999")
  /// Get Started
  public static let WelcomeButtonTitle368: String = CommonStrings.tr("Common", "welcome_button_title_368")
  /// Welcome
  public static let WelcomeTextTitle366: String = CommonStrings.tr("Common", "welcome_text_title_366")
  /// What you can improve
  public static let WhatYouCanImprove640: String = CommonStrings.tr("Common", "what_you_can_improve_640")

  public enum Accessibility {

    public enum Assessment {
      /// Checkbox selected
      public static let CheckboxSelectedAoda2470: String = CommonStrings.tr("Common", "accessibility.assessment.checkbox_selected_aoda_2470")
      /// Checkbox Unselected
      public static let CheckboxUnselectedAoda2471: String = CommonStrings.tr("Common", "accessibility.assessment.checkbox_unselected_aoda_2471")
      /// Selected
      public static let DropdownSelected2466: String = CommonStrings.tr("Common", "accessibility.assessment.dropdown_selected_2466")
      /// Unselected
      public static let UnselectedAoda2469: String = CommonStrings.tr("Common", "accessibility.assessment.unselected_aoda_2469")
      /// Upload Image
      public static let UploadImageAoda2468: String = CommonStrings.tr("Common", "accessibility.assessment.upload_image_aoda_2468")
    }

    public enum Common {
      /// Answer
      public static let ActionAnswer2472: String = CommonStrings.tr("Common", "accessibility.common.action_answer_2472")
      /// Change
      public static let ChangeAoda2474: String = CommonStrings.tr("Common", "accessibility.common.change_aoda_2474")
      /// See more details
      public static let SeeMoreDetailsAoda2467: String = CommonStrings.tr("Common", "accessibility.common.see_more_details_aoda_2467")
      /// Off
      public static let ToggleOffAoda2476: String = CommonStrings.tr("Common", "accessibility.common.toggle_off_aoda_2476")
      /// On
      public static let ToggleOnAoda2475: String = CommonStrings.tr("Common", "accessibility.common.toggle_on_aoda_2475")
      /// View menu
      public static let ViewMenu2473: String = CommonStrings.tr("Common", "accessibility.common.view_menu_2473")
    }

    public enum PointsMonitor {
      /// Select Points Category
      public static let SelectPointsCategoryAoda2477: String = CommonStrings.tr("Common", "accessibility.points_monitor.select_points_category_aoda_2477")
    }
  }

  public enum Activate {
    /// Activate
    public static let ActivateTitle353: String = CommonStrings.tr("Common", "activate.activate_title_353")
    /// Authentication Code
    public static let AuthenticationCode355: String = CommonStrings.tr("Common", "activate.authentication_code_355")
    /// If you did not receive an authentication code, please contact your employer.
    public static let AuthenticationCodeFooter357: String = CommonStrings.tr("Common", "activate.authentication_code_footer_357")
    /// Enter Code
    public static let AuthenticationCodePlaceholder356: String = CommonStrings.tr("Common", "activate.authentication_code_placeholder_356")
    /// Date of Birth
    public static let DateOfBirth363: String = CommonStrings.tr("Common", "activate.date_of_birth_363")
    /// Select Date
    public static let EnterDate354: String = CommonStrings.tr("Common", "activate.enter_date_354")
    /// Entity Number
    public static let EntityNumber358: String = CommonStrings.tr("Common", "activate.entity_number_358")
    /// If you are an existing Vitality member, please enter your entity number.
    public static let EntityNumberFooter360: String = CommonStrings.tr("Common", "activate.entity_number_footer_360")
    /// Enter Number (optional)
    public static let EntityNumberPlaceholder359: String = CommonStrings.tr("Common", "activate.entity_number_placeholder_359")
  }

  public enum AppUpdate {
    /// New Update Required
    public static let HeaderTitle2126: String = CommonStrings.tr("Common", "app_update.header_title_2126")
    /// 
    public static let HeaderTitle9999: String = CommonStrings.tr("Common", "app_update.header_title_9999")
    /// We have made some improvements to our app that require you to update to the latest version in order to continue. 
    public static let Message2127: String = CommonStrings.tr("Common", "app_update.message_2127")
    /// 
    public static let Message9999: String = CommonStrings.tr("Common", "app_update.message_9999")
    /// Update
    public static let UpdateButton2128: String = CommonStrings.tr("Common", "app_update.update_button_2128")
    /// 
    public static let UpdateButton9999: String = CommonStrings.tr("Common", "app_update.update_button_9999")
  }

  public enum Ar {
    /// About Skills Question
    public static let AboutSkillQuestionTitle2529: String = CommonStrings.tr("Common", "AR.about_skill_question_title_2529")
    /// Activated
    public static let ActivatedHeading672: String = CommonStrings.tr("Common", "AR.activated_heading_672")
    /// Your Weekly Target has successfully been activated!
    public static let ActivatedMessage645: String = CommonStrings.tr("Common", "AR.activated_message_645")
    /// Link Now
    public static let AlertButtonTitleLinkNow779: String = CommonStrings.tr("Common", "AR.alert_button_title_link_now_779")
    /// What the skills questions are and why you need to complete them\n\nYou are allowed one attempt to answer the Skills Test Question, as per Canadian legislative requirements. If you answer incorrectly, you will not be allowed any more attempts for the target period and you will forfeit your reward.
    public static let CanadianLegislationActContent2531: String = CommonStrings.tr("Common", "AR.canadian_legislation_act_content_2531")
    /// Canadian Legislation Act
    public static let CanadianLegislationActTitle2530: String = CommonStrings.tr("Common", "AR.canadian_legislation_act_title_2530")
    /// Retry
    public static let ConnectionErrorButtonRetry789: String = CommonStrings.tr("Common", "AR.connection_error_button_retry_789")
    /// Complete VHR
    public static let GetStartedCompleteVhrButtonTitle772: String = CommonStrings.tr("Common", "AR.get_started_complete_vhr_button_title_772")
    /// To start earning great rewards, you need to complete the Vitality Health Rewards in order to activate Active Rewards.
    public static let GetStartedVhrDescription770: String = CommonStrings.tr("Common", "AR.get_started_vhr_description_770")
    /// Vitality Health Rewards icon
    public static let GetStartedVhrIconContentDescription771: String = CommonStrings.tr("Common", "AR.get_started_vhr_icon_content_description_771")
    /// Complete your Vitality Health Review
    public static let GetStartedVhrTitle769: String = CommonStrings.tr("Common", "AR.get_started_vhr_title_769")
    /// In Progress
    public static let GoalInProgressTitle726: String = CommonStrings.tr("Common", "AR.goal_in_progress_title_726")
    /// Pending
    public static let GoalPendingTitle693: String = CommonStrings.tr("Common", "AR.goal_pending_title_693")
    /// of %1$@ points
    public static func GoalProgressMessage647(_ p1: String) -> String {
      return CommonStrings.tr("Common", "AR.goal_progress_message_647", p1)
    }
    /// Achieved!
    public static let LandingAchievedTitle768: String = CommonStrings.tr("Common", "AR.landing_achieved_title_768")
    /// Keep track of all your expired and used rewards.
    public static let NoHistoryMessage1155: String = CommonStrings.tr("Common", "AR.no_history_message_1155")
    /// Swap
    public static let RewardSwapBottomButton2141: String = CommonStrings.tr("Common", "AR.reward_swap_bottom_button_2141")
    /// 
    public static let RewardSwapBottomButton9999: String = CommonStrings.tr("Common", "AR.reward_swap_bottom_button_9999")
    /// 1 Reward Earned
    public static let RewardsEarnedOneRewardTitle782: String = CommonStrings.tr("Common", "AR.rewards_earned_one_reward_title_782")
    /// %1$@ Rewards Earned
    public static func RewardsEarnedTitle782(_ p1: String) -> String {
      return CommonStrings.tr("Common", "AR.rewards_earned_title_782", p1)
    }
    /// Expires on %1$@
    public static func RewardsExpiresOnTitle2599(_ p1: String) -> String {
      return CommonStrings.tr("Common", "AR.rewards_expires_on_title_2599", p1)
    }
    /// Expires soon
    public static let RewardsExpiresSoonTitle784: String = CommonStrings.tr("Common", "AR.rewards_expires_soon_title_784")
    /// %1$@ reward expiring soon
    public static func RewardsExpiringSoonTitle783(_ p1: String) -> String {
      return CommonStrings.tr("Common", "AR.rewards_expiring_soon_title_783", p1)
    }
    /// Click here
    public static let VoucherCodeClickableContent2147: String = CommonStrings.tr("Common", "AR.voucher_code_clickable_content_2147")
    /// 
    public static let VoucherCodeClickableContent9999: String = CommonStrings.tr("Common", "AR.voucher_code_clickable_content_9999")
    /// Voucher Code
    public static let VoucherCodeTitle664: String = CommonStrings.tr("Common", "AR.voucher_code_title_664")
    /// Expired %1$@
    public static func VoucherExpired689(_ p1: String) -> String {
      return CommonStrings.tr("Common", "AR.voucher_expired_689", p1)
    }
    /// Expires %1$@
    public static func VoucherExpires658(_ p1: String) -> String {
      return CommonStrings.tr("Common", "AR.voucher_expires_658", p1)
    }
    /// Target Achieved %1$@ 
    public static func VoucherTargetAchieved2148(_ p1: String) -> String {
      return CommonStrings.tr("Common", "AR.voucher_target_achieved_2148", p1)
    }
    /// 
    public static let VoucherTargetAchieved9999: String = CommonStrings.tr("Common", "AR.voucher_target_achieved_9999")
    /// Used %1$@
    public static func VoucherUsed671(_ p1: String) -> String {
      return CommonStrings.tr("Common", "AR.voucher_used_671", p1)
    }

    public enum ActivityDetail {
      /// Only qualifying Active Rewards activity is displayed here. Refer to the Points tab for all activity events.
      public static let ActivitySectionFooterText673: String = CommonStrings.tr("Common", "AR.activity_detail.activity_section_footer_text_673")
      /// ACTIVITY
      public static let ActivitySectionHeaderTitle723: String = CommonStrings.tr("Common", "AR.activity_detail.activity_section_header_title_723")
    }

    public enum Common {
      /// %1$@ Reward
      public static func RewardLabel1961(_ p1: String) -> String {
        return CommonStrings.tr("Common", "AR.common.reward_label_1961", p1)
      }
      /// %1$@ Voucher
      public static func VoucherLabel1962(_ p1: String) -> String {
        return CommonStrings.tr("Common", "AR.common.voucher_label_1962", p1)
      }
    }

    public enum EventDetail {
      /// %1$@ hr %2$@ min %3$@ sec
      public static func DurationHhMmSs851(_ p1: String, _ p2: String, _ p3: String) -> String {
        return CommonStrings.tr("Common", "AR.event_detail.duration_hh_mm_ss_851", p1, p2, p3)
      }
    }

    public enum GetStarted {
      /// Vitality Health Review icon
      public static let VitalityHealthReviewIconContentDescription763: String = CommonStrings.tr("Common", "AR.get_started.vitality_health_review_icon_content_description_763")
    }

    public enum GoalHistory {
      /// An error occurred while trying to load more activity
      public static let ErrorOccuredFooterText764: String = CommonStrings.tr("Common", "AR.goal_history.error_occured_footer_text_764")
      /// Achieved
      public static let GoalAchievedTitle692: String = CommonStrings.tr("Common", "AR.Goal_History.goal_achieved_title_692")
      /// No More Activity
      public static let NoMoreActivityFooterText739: String = CommonStrings.tr("Common", "AR.goal_history.no_more_activity_footer_text_739")
      /// Not enough activity to reach your goal
      public static let NotAchievedMessage846: String = CommonStrings.tr("Common", "AR.goal_history.not_achieved_message_846")
      /// Not Achieved
      public static let NotAchievedTitle709: String = CommonStrings.tr("Common", "AR.goal_history.not_achieved_title_709")
    }

    public enum History {
      /// Need help?
      public static let NoActivityButtonTitle676: String = CommonStrings.tr("Common", "AR.history.no_activity_button_title_676")
      /// This is where you will be able to view your weekly goal and activity history.
      public static let NoActivityHeadingMessage727: String = CommonStrings.tr("Common", "AR.history.no_activity_heading_message_727")
      /// No Activity
      public static let NoActivityHeadingTitle694: String = CommonStrings.tr("Common", "AR.history.no_activity_heading_title_694")
    }

    public enum Home {
      /// View goal
      public static let ViewGoalButtonTitle765: String = CommonStrings.tr("Common", "AR.home.view_goal_button_title_765")
    }

    public enum HomeCard {
      /// Your physical activity target starts %1$@
      public static func ActivatedStartDate2623(_ p1: String) -> String {
        return CommonStrings.tr("Common", "AR.home_card.activated_start_date_2623", p1)
      }
      /// Starts %1$@
      public static func ActivatedStartDate776(_ p1: String) -> String {
        return CommonStrings.tr("Common", "AR.home_card.activated_start_date_776", p1)
      }
      /// You met your physical activity target this week
      public static let ActivatedTitle2612: String = CommonStrings.tr("Common", "AR.home_card.activated_title_2612")
      /// Activated
      public static let ActivatedTitle775: String = CommonStrings.tr("Common", "AR.home_card.activated_title_775")
      /// %1$@ - %2$@
      public static func InProgressDates774(_ p1: String, _ p2: String) -> String {
        return CommonStrings.tr("Common", "AR.home_card.in_progress_dates_774", p1, p2)
      }
      /// %1$@ of %2$@ target points met
      public static func InProgressTitle2622(_ p1: String, _ p2: String) -> String {
        return CommonStrings.tr("Common", "AR.home_card.in_progress_title_2622", p1, p2)
      }
      /// %1$@ of %2$@ points
      public static func InProgressTitle773(_ p1: String, _ p2: String) -> String {
        return CommonStrings.tr("Common", "AR.home_card.in_progress_title_773", p1, p2)
      }
      /// Learn More
      public static let LearnMoreButtonTitle2129: String = CommonStrings.tr("Common", "AR.home_card.learn_more_button_title_2129")
      /// Reward Pending
      public static let RewardPending2618: String = CommonStrings.tr("Common", "AR.home_card.reward_pending_2618")
    }

    public enum HomescreenCard {
      /// View Available Spins
      public static let AvailableSpinsButtonTitle787: String = CommonStrings.tr("Common", "AR.homescreen_card.available_spins_button_title_787")
      /// Earn Rewards
      public static let EarnRewardsTitle786: String = CommonStrings.tr("Common", "AR.homescreen_card.earn_rewards_title_786")
      /// Rewards
      public static let RewardsTitle785: String = CommonStrings.tr("Common", "AR.homescreen_card.rewards_title_785")
    }

    public enum Landing {
      /// Active Rewards activation failed
      public static let ActivationFailedTitle2130: String = CommonStrings.tr("Common", "AR.landing.activation_failed_title_2130")
      /// 
      public static let ActivationFailedTitle9999: String = CommonStrings.tr("Common", "AR.landing.activation_failed_title_9999")
      /// Activity
      public static let ActivityCellTitle711: String = CommonStrings.tr("Common", "AR.landing.activity_cell_title_711")
      /// Today
      public static let DateIndicationToday781: String = CommonStrings.tr("Common", "AR.landing.date_indication_today_781")
      /// Yesterday
      public static let DateIndicationYesterday847: String = CommonStrings.tr("Common", "AR.landing.date_indication_yesterday_847")
      /// Vitality would like to send you notifications for Active Rewards to help you reach your target. If you disabled push notifications for this app, please enable them in Settings.
      public static let FirstGoalCellMessage659: String = CommonStrings.tr("Common", "AR.landing.first_goal_cell_message_659")
      /// Your Weekly Target will start %1$@
      public static func FirstGoalCellTitle677(_ p1: String) -> String {
        return CommonStrings.tr("Common", "AR.landing.first_goal_cell_title_677", p1)
      }
      /// Vitality Active Rewards
      public static let HomeViewTitle710: String = CommonStrings.tr("Common", "AR.Landing.home_view_title_710")
      /// Link a fitness device or app to easily track your physical activity.
      public static let LinkDeviceDialogMessage767: String = CommonStrings.tr("Common", "AR.landing.link_device_dialog_message_767")
      /// Link a device or app
      public static let LinkDeviceDialogTitle766: String = CommonStrings.tr("Common", "AR.landing.link_device_dialog_title_766")
      /// There's no activity for this week.
      public static let NoActivityCellTitle678: String = CommonStrings.tr("Common", "AR.landing.no_activity_cell_title_678")
      /// Event description goes here. It might be a long string, spanning multiple lines
      public static let PointsDescriptionText679: String = CommonStrings.tr("Common", "AR.landing.points_description_text_679")
      /// Rewards
      public static let RewardsCellTitle695: String = CommonStrings.tr("Common", "AR.landing.rewards_cell_title_695")
      /// Weekly Target
      public static let SubheaderWeeklyTarget780: String = CommonStrings.tr("Common", "AR.landing.subheader_weekly_target_780")
      /// This week's activity
      public static let ThisWeeksActivitySectionHeaderTitle660: String = CommonStrings.tr("Common", "AR.landing.this_weeks_activity_section_header_title_660")
    }

    public enum LearnMore {
      /// Open the app and tap on Vitality Active Rewards and follow the prompts to get started.
      public static let ActiveWeeklyPointsTargetContent712: String = CommonStrings.tr("Common", "AR.learn_more.active_weekly_points_target_content_712")
      /// Activate weekly points target
      public static let ActiveWeeklyPointsTargetTitle661: String = CommonStrings.tr("Common", "AR.learn_more.active_weekly_points_target_title_661")
      /// Benefit Guide
      public static let BenefitGuideTitle728: String = CommonStrings.tr("Common", "AR.learn_more.benefit_guide_title_728")
      /// Each week when you reach your target, you'll have the chance to choose a reward voucher
      public static let ClaimRewardChoiceContent1551: String = CommonStrings.tr("Common", "AR.learn_more.claim_reward_choice_content_1551")
      /// Each week when you reach your target, you receive a spin on the wheel for a chance to win reward vouchers.
      public static let ClaimRewardContent713: String = CommonStrings.tr("Common", "AR.learn_more.claim_reward_content_713")
      /// Claim your reward
      public static let ClaimRewardTitle662: String = CommonStrings.tr("Common", "AR.learn_more.claim_reward_title_662")
      /// Have fun and stay motivated with Vitality Active Rewards. Meet your weekly targets through physical activities and earn reward vouchers from our Vitality Active Rewards partners.
      public static let HowActiveRewardsWorksContent714: String = CommonStrings.tr("Common", "AR.learn_more.how_active_rewards_works_content_714")
      /// How Vitality Active Reward Works
      public static let HowActiveRewardsWorksTitle663: String = CommonStrings.tr("Common", "AR.learn_more.how_active_rewards_works_title_663")
      /// Participating Partners
      public static let ParticipatingPartnersTitle696: String = CommonStrings.tr("Common", "AR.learn_more.participating_partners_title_696")
      /// According to Canadian legislative requirements, you are allowed one attempt to answer the Skills Test Question, If you answer incorrectly, you will not be allowed any more attempts for the target period and you will forfeit your reward.
      public static let SkillTestContent2527: String = CommonStrings.tr("Common", "AR.learn_more.skill_test_content_2527")
      /// Skills Test Questions
      public static let SkillTestTitle2528: String = CommonStrings.tr("Common", "AR.learn_more.skill_test_title_2528")
      /// Types of Rewards
      public static let TypesOfRewardsTitle2526: String = CommonStrings.tr("Common", "AR.learn_more.types_of_rewards_title_2526")
      /// Receive a new points target every Monday. Complete physical activities to reach your weekly target.
      public static let WorkOutReachYourTargetContent680: String = CommonStrings.tr("Common", "AR.learn_more.work_out_reach_your_target_content_680")
      /// Work out and reach your target
      public static let WorkOutReachYourTargetTitle729: String = CommonStrings.tr("Common", "AR.learn_more.work_out_reach_your_target_title_729")
    }

    public enum Onboarding {
      /// Get Started With Vitality Active Rewards
      public static let CommonHeading682: String = CommonStrings.tr("Common", "AR.onboarding.common_heading_682")
      /// Receive a new points target every Monday.
      public static let CommonItem1731: String = CommonStrings.tr("Common", "AR.onboarding.common_item1_731")
      /// Weekly Target
      public static let CommonItem1Heading665: String = CommonStrings.tr("Common", "AR.onboarding.common_item1_heading_665")
      /// Complete your physical activities to reach your weekly points target.
      public static let CommonItem2683: String = CommonStrings.tr("Common", "AR.onboarding.common_item2_683")
      /// Get Active
      public static let CommonItem2Heading715: String = CommonStrings.tr("Common", "AR.onboarding.common_item2_heading_715")
      /// Each week when you meet the target, you’ll receive a spin on the wheel for a chance to win a voucher from one of our Vitality Active Rewards partners.
      public static let CommonItem3666: String = CommonStrings.tr("Common", "AR.onboarding.common_item3_666")
      /// Reach Your Target
      public static let CommonItem3Heading697: String = CommonStrings.tr("Common", "AR.onboarding.common_item3_heading_697")
      /// You have 30 days to choose your reward before it expires.
      public static let DefinedItem4732: String = CommonStrings.tr("Common", "AR.onboarding.defined_item4_732")
      /// Choose Your Reward
      public static let DefinedItem4Heading733: String = CommonStrings.tr("Common", "AR.onboarding.defined_item4_heading_733")
      /// You have 30 days to spin the wheel before your spin expires.
      public static let ProbabilisticItem4698: String = CommonStrings.tr("Common", "AR.onboarding.probabilistic_item4_698")
      /// Spin the Wheel
      public static let ProbabilisticItem4Heading699: String = CommonStrings.tr("Common", "AR.onboarding.probabilistic_item4_heading_699")
      /// After you spin the wheel, you will be prompted to answer a skills question in order for you to obtain your reward.
      public static let ProbabilisticItem52525: String = CommonStrings.tr("Common", "AR.onboarding.probabilistic_item5_2525")
      /// Answer a Skills Questions
      public static let ProbabilisticItem5Heading2524: String = CommonStrings.tr("Common", "AR.onboarding.probabilistic_item5_heading_2524")
    }

    public enum Parnters {
      /// Data Privacy
      public static let StarbucksDataPrivacyTitle1065: String = CommonStrings.tr("Common", "AR.parnters.starbucks_data_privacy_title_1065")
    }

    public enum Partners {
      /// Cinemark
      public static let Cinemark2478: String = CommonStrings.tr("Common", "AR.partners.cinemark_2478")
      /// Cineworld
      public static let CineworldName1054: String = CommonStrings.tr("Common", "AR.partners.cineworld_name_1054")
      /// Free Movie Tickets
      public static let CineworldVoucherValue1056: String = CommonStrings.tr("Common", "AR.partners.cineworld_voucher_value_1056")
      /// Fitbit
      public static let FitbitTitle707: String = CommonStrings.tr("Common", "AR.partners.fitbit_title_707")
      /// Juan Valdez
      public static let JuanValdez2479: String = CommonStrings.tr("Common", "AR.partners.juan_valdez_2479")
      /// Lawson
      public static let LawsonName1054: String = CommonStrings.tr("Common", "AR.partners.lawson_name_1054")
      /// Vouchers up to $15
      public static let ParticipatingPartnersVoucherValue722: String = CommonStrings.tr("Common", "AR.partners.participating_partners_voucher_value_722")
      /// Reward
      public static let RewardTitle1058: String = CommonStrings.tr("Common", "AR.partners.reward_title_1058")
      /// Softbank
      public static let SoftbankName1054: String = CommonStrings.tr("Common", "AR.partners.softbank_name_1054")
      /// Starbucks
      public static let Starbucks1417: String = CommonStrings.tr("Common", "AR.partners.starbucks_1417")
      /// Accept
      public static let StarbucksDataPrivacyAcceptButton1066: String = CommonStrings.tr("Common", "AR.partners.starbucks_data_privacy_accept_button_1066")
      /// Starbucks
      public static let StarbucksName734: String = CommonStrings.tr("Common", "AR.partners.starbucks_name_734")
      /// Confirm that ‘%1$@’ is your correct Starbucks Reward™ Email. You will not receive your free drink if your email is incorrect.
      public static func StarbucksRewardAlertMessage1064(_ p1: String) -> String {
        return CommonStrings.tr("Common", "AR.partners.starbucks_reward_alert_message_1064", p1)
      }
      /// Confirm Starbucks Reward™ Email address
      public static let StarbucksRewardAlertTitle1063: String = CommonStrings.tr("Common", "AR.partners.starbucks_reward_alert_title_1063")
      /// Starbucks Reward™ Email
      public static let StarbucksRewardEmailHint1061: String = CommonStrings.tr("Common", "AR.partners.starbucks_reward_email_hint_1061")
      /// Make sure that the correct Starbucks Reward™ Email is provided to receive your free drink. Need an account? %1$@
      public static func StarbucksRewardEmailMessage1059(_ p1: String) -> String {
        return CommonStrings.tr("Common", "AR.partners.starbucks_reward_email_message_1059", p1)
      }
      /// The merchants represented are not sponsors of Vitality or otherwise affiliated with Vitality. The logos and other identifying marks attached are trademarks of and are marked by each represented company and/or its affiliates. Please visits each company’s website for additional terms and conditions.
      public static let StarbucksRewardFootnote1062: String = CommonStrings.tr("Common", "AR.partners.starbucks_reward_footnote_1062")
      /// Register account
      public static let StarbucksRewardRegisterAccount1060: String = CommonStrings.tr("Common", "AR.partners.starbucks_reward_register_account_1060")
      /// https://www.starbucks.com/account/create
      public static let StarbucksRewardRegisterURL1060: String = CommonStrings.tr("Common", "AR.partners.starbucks_reward_register_URL_1060")
      /// Free Drink
      public static let StarbucksVoucherValue705: String = CommonStrings.tr("Common", "AR.partners.starbucks_voucher_value_705")
      /// Vue
      public static let VueName1055: String = CommonStrings.tr("Common", "AR.partners.vue_name_1055")
      /// Free Movie Tickets
      public static let VueVoucherValue1057: String = CommonStrings.tr("Common", "AR.partners.vue_voucher_value_1057")
    }

    public enum PointsEarningMetrics {
      /// These metrics are used to track your healthy habits to earn you points.
      public static let ContentShort436: String = CommonStrings.tr("Common", "AR.points_earning_metrics.content_short_436")
    }

    public enum PointsEventDetail {
      /// Event
      public static let CellTitle684: String = CommonStrings.tr("Common", "AR.points_event_detail.cell_title_684")
      /// DETAILS
      public static let DetailsSectionHeaderTitle735: String = CommonStrings.tr("Common", "AR.points_event_detail.details_section_header_title_735")
      /// DEVICE USED
      public static let DeviceSectionHeaderTitle700: String = CommonStrings.tr("Common", "AR.points_event_detail.device_section_header_title_700")
      /// POINTS
      public static let PointsSectionHeaderTitle716: String = CommonStrings.tr("Common", "AR.points_event_detail.points_section_header_title_716")
      /// Event detail
      public static let ViewTitle685: String = CommonStrings.tr("Common", "AR.points_event_detail.view_title_685")
    }

    public enum Rewards {
      /// AVAILABLE SPINS
      public static let AvailableSpinsSectionTitle721: String = CommonStrings.tr("Common", "AR.rewards.available_spins_section_title_721")
      /// Available Reward Swaps
      public static let AvailableSwapsTitle667: String = CommonStrings.tr("Common", "AR.rewards.available_swaps_title_667")
      /// Available
      public static let AvailableTitle1086: String = CommonStrings.tr("Common", "ar.rewards.available_title_1086")
      /// Way to go, you did it!
      public static let ChooseRewardBodyTitle674: String = CommonStrings.tr("Common", "AR.rewards.choose_reward_body_title_674")
      /// Choose
      public static let ChooseRewardButtonTitle686: String = CommonStrings.tr("Common", "AR.rewards.choose_reward_button_title_686")
      /// You reached your target and earned a reward. Choose your reward below.
      public static let ChooseRewardInstruction646: String = CommonStrings.tr("Common", "AR.rewards.choose_reward_instruction_646")
      /// Choose Reward
      public static let ChooseRewardTitle724: String = CommonStrings.tr("Common", "AR.rewards.choose_reward_title_724")
      /// Choose Rewards
      public static let ChooseRewardsSectionTitle2629: String = CommonStrings.tr("Common", "AR.rewards.choose_rewards_section_title_2629")
      /// Chosen Reward
      public static let ChosenRewardTitle736: String = CommonStrings.tr("Common", "AR.rewards.chosen_reward_title_736")
      /// Voucher Code %1$@
      public static func CinemaMultipleVoucherCodesTitle1111(_ p1: String) -> String {
        return CommonStrings.tr("Common", "AR.rewards.cinema__multiple_voucher_codes_title_1111", p1)
      }
      /// Free Ticket Pending
      public static let CinemaFreeTicketPending2133: String = CommonStrings.tr("Common", "AR.rewards.cinema_free_ticket_pending_2133")
      /// Your Voucher/Promo Code will be issued to you on Monday %1$@. When your code is issued you'll need to choose your preferred cinema.
      public static func CinemaPendingFooter1115(_ p1: String) -> String {
        return CommonStrings.tr("Common", "AR.rewards.cinema_pending_footer_1115", p1)
      }
      /// Your Reward
      public static let CinemaPendingTitle2081: String = CommonStrings.tr("Common", "AR.rewards.cinema_pending_title_2081")
      /// Choose Your Cinema
      public static let CinemaRedeemChooseTitle1109: String = CommonStrings.tr("Common", "AR.rewards.cinema_redeem_choose_title_1109")
      /// Your Cinema reward is available for you to redeem.
      public static let CinemaRedeemDescription1110: String = CommonStrings.tr("Common", "AR.rewards.cinema_redeem_description_1110")
      /// Redeem Reward
      public static let CinemaRedeemRewardTitle1108: String = CommonStrings.tr("Common", "AR.rewards.cinema_redeem_reward_title_1108")
      /// Cinema reward
      public static let CinemaReward1570: String = CommonStrings.tr("Common", "AR.rewards.cinema_reward_1570")
      /// You will not be able to swap your reward once confirmed.
      public static let CinemaSelectionDialogDescription1117: String = CommonStrings.tr("Common", "AR.rewards.cinema_selection_dialog_description_1117")
      /// Are you sure you would like to select this cinema partner?
      public static let CinemaSelectionDialogTitle1116: String = CommonStrings.tr("Common", "AR.rewards.cinema_selection_dialog_title_1116")
      /// %1$@ Cinema ticket
      public static func CinemaTicket1327(_ p1: String) -> String {
        return CommonStrings.tr("Common", "AR.rewards.cinema_ticket_1327", p1)
      }
      /// Use the above Voucher code when booking your movie. Please note, you'll need to show your digital Vitality Card at the cinema.
      public static let CinemaVoucherFooter1112: String = CommonStrings.tr("Common", "AR.rewards.cinema_voucher_footer_1112")
      /// Voucher / Promo Code
      public static let CinemaVoucherPromoCodeTitle1114: String = CommonStrings.tr("Common", "AR.rewards.cinema_voucher_promo_code_title_1114")
      /// Way to go! You've won %1$@ at either Cineworld or Vue.
      public static func CinemaWayToGoMessage1113(_ p1: String) -> String {
        return CommonStrings.tr("Common", "AR.rewards.cinema_way_to_go_message_1113", p1)
      }
      /// 2D Movie Ticket
      public static let CinemarkAppDescription2131: String = CommonStrings.tr("Common", "AR.rewards.cinemark_app_description_2131")
      /// Use this code when you book your movie (in 2D format) at any Cinemark nationwide.
      public static let CinemarkVoucherCodeInstruction2132: String = CommonStrings.tr("Common", "AR.rewards.cinemark_voucher_code_instruction_2132")
      /// Select Voucher
      public static let ConfirmRewardButtonConfirm725: String = CommonStrings.tr("Common", "AR.rewards.confirm_reward_button_confirm_725")
      /// Swap Voucher
      public static let ConfirmRewardSwapButton675: String = CommonStrings.tr("Common", "AR.rewards.confirm_reward_swap_button_675")
      /// Congratulations,\nyou've won!
      public static let ConfirmRewardTitle690: String = CommonStrings.tr("Common", "AR.Rewards.confirm_reward_title_690")
      /// Copy Voucher Code
      public static let CopyVoucherCode1418: String = CommonStrings.tr("Common", "AR.rewards.copy_voucher_code_1418")
      /// %1$@ x %2$@
      public static func CounterMultiply2134(_ p1: String, _ p2: String) -> String {
        return CommonStrings.tr("Common", "AR.rewards.counter_multiply_2134", p1, p2)
      }
      /// 
      public static let CounterMultiply9999: String = CommonStrings.tr("Common", "AR.rewards.counter_multiply_9999")
      /// Target Achieved %1$@ \nExpires %2$@
      public static func CreditInfo691(_ p1: String, _ p2: String) -> String {
        return CommonStrings.tr("Common", "AR.rewards.credit_info_691", p1, p2)
      }
      /// Current
      public static let CurrentSegmentTitle688: String = CommonStrings.tr("Common", "AR.rewards.current_segment_title_688")
      /// Current Reward
      public static let CurrentTitle687: String = CommonStrings.tr("Common", "AR.rewards.current_title_687")
      /// Reach your weekly target to earn great rewards
      public static let EarnedCellDescription717: String = CommonStrings.tr("Common", "AR.rewards.earned_cell_description_717")
      /// Reward Earned
      public static let EarnedCellTitle668: String = CommonStrings.tr("Common", "AR.rewards.earned_cell_title_668")
      /// EasyTickets - Buy Movie, Bus & Event Tickets
      public static let EasyticketsAppDescription1956: String = CommonStrings.tr("Common", "AR.rewards.easytickets_app_description_1956")
      /// Use the above code when booking your movie tickets on the EasyTickets app
      public static let EasyticketsVoucherCodeInstruction1955: String = CommonStrings.tr("Common", "AR.rewards.easytickets_voucher_code_instruction_1955")
      /// foodpanda - Local Food Delivery
      public static let FoodpandaAppDescription1958: String = CommonStrings.tr("Common", "AR.rewards.foodpanda_app_description_1958")
      /// Use the above code when ordering your next meal on the foodpanda app
      public static let FoodpandaVoucherCodeInstruction1957: String = CommonStrings.tr("Common", "AR.rewards.foodpanda_voucher_code_instruction_1957")
      /// History
      public static let HistorySegmentTitle670: String = CommonStrings.tr("Common", "AR.rewards.history_segment_title_670")
      /// FREE medium or small drink
      public static let JuanValdezAppDescription2579: String = CommonStrings.tr("Common", "AR.rewards.juan_valdez_app_description_2579")
      /// Voucher code to exchange your prize in Juan Valdez
      public static let JuanValdezVoucherCodeInstruction2135: String = CommonStrings.tr("Common", "AR.rewards.juan_valdez_voucher_code_instruction_2135")
      /// Reach your weekly target to earn great rewards
      public static let NoAvailableSpinsCellDescription681: String = CommonStrings.tr("Common", "AR.rewards.no_available_spins_cell_description_681")
      /// No Available Spins
      public static let NoAvailableSpinsCellTitle730: String = CommonStrings.tr("Common", "AR.rewards.no_available_spins_cell_title_730")
      /// Unfortunately you did not win a reward. Reach your next target and try again.
      public static let NoRewardDescription1120: String = CommonStrings.tr("Common", "AR.rewards.no_reward_description_1120")
      /// No reward
      public static let NoRewardTitle1119: String = CommonStrings.tr("Common", "AR.rewards.no_reward_title_1119")
      /// No Rewards to Choose
      public static let NoRewardsChooseTitle788: String = CommonStrings.tr("Common", "AR.rewards.no_rewards_choose_title_788")
      /// Spin the wheel in 30days after goal achievement
      public static let OnboardingContent12433: String = CommonStrings.tr("Common", "AR.rewards.onboarding_content1_2433")
      /// You can mark a reward as used by yourself. Please note that the Radio button will not be handled automatically, but for your reminder
      public static let OnboardingContent22435: String = CommonStrings.tr("Common", "AR.rewards.onboarding_content2_2435")
      /// Check the status of your benefit
      public static let OnboardingHeader2431: String = CommonStrings.tr("Common", "AR.rewards.onboarding_header_2431")
      /// Spin the wheel
      public static let OnboardingSubheader12432: String = CommonStrings.tr("Common", "AR.rewards.onboarding_subheader1_2432")
      /// You can mark a reward as used
      public static let OnboardingSubheader22434: String = CommonStrings.tr("Common", "AR.rewards.onboarding_subheader2_2434")
      /// PARTICIPATING PARTNERS
      public static let ParticipatingPartnersSectionTitle702: String = CommonStrings.tr("Common", "AR.rewards.participating_partners_section_title_702")
      /// Partner logo
      public static let PartnerLogoContentDescription761: String = CommonStrings.tr("Common", "AR.rewards.partner_logo_content_description_761")
      /// Your %1$@ has been requested from Starbucks. You can collect your %2$@ on Thursday.
      public static func PendingStarbucksRewardFooter1079(_ p1: String, _ p2: String) -> String {
        return CommonStrings.tr("Common", "ar.rewards.pending_starbucks_reward_footer_1079", p1, p2)
      }
      /// Your %1$@ has been requested from Starbucks. You can collect your %2$@ on Thursday.
      public static func PendingStarbucksRewardFooterHtml1079(_ p1: String, _ p2: String) -> String {
        return CommonStrings.tr("Common", "ar.rewards.pending_starbucks_reward_footer_html_1079", p1, p2)
      }
      /// Way to go! You've won a %1$@ at Starbucks.
      public static func PendingStarbucksRewardMesage1078(_ p1: String) -> String {
        return CommonStrings.tr("Common", "ar.rewards.pending_starbucks_reward_mesage_1078", p1)
      }
      /// Your Vitality Status Points will reflect on the Vitality Points Monitor within 48 hours.
      public static let PointsDescription2580: String = CommonStrings.tr("Common", "AR.rewards.points_description_2580")
      /// T's & C's Apply
      public static let PointsRewardFootnote2581: String = CommonStrings.tr("Common", "AR.rewards.points_reward_footnote_2581")
      /// %1$@ Discount
      public static func RewardAmountDiscountKeyword2136(_ p1: String) -> String {
        return CommonStrings.tr("Common", "AR.rewards.reward_amount_discount_keyword_2136", p1)
      }
      /// 
      public static let RewardAmountDiscountKeyword9999: String = CommonStrings.tr("Common", "AR.rewards.reward_amount_discount_keyword_9999")
      /// Your Starbucks Rewards™ Email was either incorrect or does not exist. \n\n Please provide the correct Email address if you win a Starbucks Rewards again.
      public static let RewardCouldNotAwardedContent1084: String = CommonStrings.tr("Common", "ar.rewards.reward_could_not_awarded_content_1084")
      /// %1$@ could not be rewarded
      public static func RewardCouldNotAwardedTitle1083(_ p1: String) -> String {
        return CommonStrings.tr("Common", "ar.rewards.reward_could_not_awarded_title_1083", p1)
      }
      /// %1$@%% Discount
      public static func RewardPercentageDiscountKeyword2137(_ p1: String) -> String {
        return CommonStrings.tr("Common", "AR.rewards.reward_percentage_discount_keyword_2137", p1)
      }
      /// 
      public static let RewardPercentageDiscountKeyword9999: String = CommonStrings.tr("Common", "AR.rewards.reward_percentage_discount_keyword_9999")
      /// %1$@%% Discount Voucher
      public static func RewardPercentageDiscountVoucherKeyword2138(_ p1: String) -> String {
        return CommonStrings.tr("Common", "AR.rewards.reward_percentage_discount_voucher_keyword_2138", p1)
      }
      /// 
      public static let RewardPercentageDiscountVoucherKeyword9999: String = CommonStrings.tr("Common", "AR.rewards.reward_percentage_discount_voucher_keyword_9999")
      /// %1$@ Voucher
      public static func RewardVoucherPlaceholder2139(_ p1: String) -> String {
        return CommonStrings.tr("Common", "AR.rewards.reward_voucher_placeholder_2139", p1)
      }
      /// Select Voucher
      public static let SelectRewardTitle703: String = CommonStrings.tr("Common", "AR.rewards.select_reward_title_703")
      /// Way to go, you did it!
      public static let SpinBodyTitle701: String = CommonStrings.tr("Common", "AR.rewards.spin_body_title_701")
      /// Swipe quickly to spin or drag slowly to view rewards.
      public static let SpinInstruction718: String = CommonStrings.tr("Common", "AR.rewards.spin_instruction_718")
      /// Spin Now
      public static let SpinNowTitle704: String = CommonStrings.tr("Common", "AR.rewards.spin_now_title_704")
      /// Reward Spin
      public static let SpinTitle719: String = CommonStrings.tr("Common", "AR.rewards.spin_title_719")
      /// Get
      public static let StarbucksGet1090: String = CommonStrings.tr("Common", "ar.rewards.starbucks_get_1090")
      /// Install
      public static let StarbucksInstall1089: String = CommonStrings.tr("Common", "ar.rewards.starbucks_install_1089")
      /// Your %1$@ can be collected at any Starbucks. Make sure you have your Starbucks app ready on collection.
      public static func StarbucksIssuedRewardReadyForCollection1085(_ p1: String) -> String {
        return CommonStrings.tr("Common", "Ar.Rewards.starbucks_issued_reward_ready_for_collection_1085", p1)
      }
      /// Your %1$@ can be collected at any Starbucks. Make sure you have your Starbucks app ready on collection.
      public static func StarbucksIssuedRewardReadyForCollectionHtml1085(_ p1: String) -> String {
        return CommonStrings.tr("Common", "Ar.Rewards.starbucks_issued_reward_ready_for_collection_html_1085", p1)
      }
      /// Open
      public static let StarbucksOpen1088: String = CommonStrings.tr("Common", "ar.rewards.starbucks_open_1088")
      /// Starbucks®
      public static let StarbucksRegisteredTrademark1087: String = CommonStrings.tr("Common", "ar.rewards.starbucks_registered_trademark_1087")
      /// View reward
      public static let StarbucksViewReward2140: String = CommonStrings.tr("Common", "AR.rewards.starbucks_view_reward_2140")
      /// 
      public static let StarbucksViewReward999: String = CommonStrings.tr("Common", "AR.rewards.starbucks_view_reward_999")
      /// Swap Reward
      public static let SwapTitle669: String = CommonStrings.tr("Common", "AR.rewards.swap_title_669")
      /// Trophy icon
      public static let TrophyIconContentDescription760: String = CommonStrings.tr("Common", "AR.rewards.trophy_icon_content_description_760")
      /// Use Reward
      public static let UseRewardTitle720: String = CommonStrings.tr("Common", "AR.rewards.use_reward_title_720")
      /// AVAILABLE REWARDS
      public static let UseableRewardsSectionTitle706: String = CommonStrings.tr("Common", "AR.rewards.useable_rewards_section_title_706")
      /// Voucher Code Copied
      public static let VoucherCodeCopied1419: String = CommonStrings.tr("Common", "AR.rewards.voucher_code_copied_1419")
      /// Use the above Voucher code when booking your movie. Please note, you'll need to show your digital Vitality Card at the cinema.
      public static let VoucherCodeInstruction1329: String = CommonStrings.tr("Common", "ar.rewards.voucher_code_instruction_1329")
      /// Code pending
      public static let VoucherCodePending1044: String = CommonStrings.tr("Common", "AR.rewards.voucher_code_pending_1044")
    }

    public enum Skillstest {
      /// Are you sure you want to submit your answer?
      public static let ConfirmMessage2614: String = CommonStrings.tr("Common", "AR.skillstest.confirm_message_2614")
      /// Your final answer is %1$@
      public static func ConfirmTitle2613(_ p1: String) -> String {
        return CommonStrings.tr("Common", "AR.skillstest.confirm_title_2613", p1)
      }
      /// The correct answer is %1$@
      public static func CorrectAnswer2617(_ p1: String) -> String {
        return CommonStrings.tr("Common", "AR.skillstest.correct_answer_2617", p1)
      }
      /// Congratulations! Your answer is correct!
      public static let CorrectMessage2603: String = CommonStrings.tr("Common", "AR.skillstest.correct_message_2603")
      /// You'll have another chance to win a reward next time you meet your weekly target.
      public static let IncorrectFooter2602: String = CommonStrings.tr("Common", "AR.skillstest.incorrect_footer_2602")
      /// We're sorry, your answer is incorrect.
      public static let IncorrectMessage2601: String = CommonStrings.tr("Common", "AR.skillstest.incorrect_message_2601")
      /// Redeem Reward
      public static let Redeem2604: String = CommonStrings.tr("Common", "AR.skillstest.redeem_2604")
      /// Skills Question
      public static let Title6212: String = CommonStrings.tr("Common", "AR.skillstest.title_6212")
    }

    public enum Voucher {
      /// Cineplex
      public static let CineplexTitle2503: String = CommonStrings.tr("Common", "AR.voucher.cineplex_title_2503")
      /// Voucher
      public static let CineplexVoucherField2506: String = CommonStrings.tr("Common", "AR.voucher.cineplex_voucher_field_2506")
      /// Indigo
      public static let IndigoTitle2502: String = CommonStrings.tr("Common", "AR.voucher.indigo_title_2502")
      /// Voucher
      public static let IndigoVoucherField2505: String = CommonStrings.tr("Common", "AR.voucher.indigo_voucher_field_2505")
      /// There has been a delay in issuing your reward voucher. Please check back in a few hours.
      public static let NotReadyDescription2501: String = CommonStrings.tr("Common", "AR.voucher.not_ready_description_2501")
      /// Your Voucher is Almost Ready!
      public static let NotReadyTitle2500: String = CommonStrings.tr("Common", "AR.voucher.not_ready_title_2500")
      /// Please note that this functionality is for your reminder
      public static let PopupBody2428: String = CommonStrings.tr("Common", "AR.voucher.popup_body_2428")
      /// Mark as unused
      public static let PopupButtonUnused2430: String = CommonStrings.tr("Common", "AR.voucher.popup_button_unused_2430")
      /// Mark as used
      public static let PopupButtonUsed2429: String = CommonStrings.tr("Common", "AR.voucher.popup_button_used_2429")
      /// Will you mark this reward as used?
      public static let PopupHeader2427: String = CommonStrings.tr("Common", "AR.voucher.popup_header_2427")
      /// Used
      public static let RadioButtonHeader2426: String = CommonStrings.tr("Common", "AR.voucher.radio_button_header_2426")
      /// Tim Hortons
      public static let TimhortonsTitle2504: String = CommonStrings.tr("Common", "AR.voucher.timhortons_title_2504")
      /// Voucher
      public static let TimhortonsVoucherField2507: String = CommonStrings.tr("Common", "AR.voucher.timhortons_voucher_field_2507")
    }

    public enum Vouchersolution {
      /// The reward will be emailed to you by the %1$@.\n\nIf you do not receive the voucher by this time please call 011 123 4567.
      public static func RewardAvailability2619(_ p1: String) -> String {
        return CommonStrings.tr("Common", "AR.vouchersolution.reward_availability_2619", p1)
      }
      /// The merchants represented are not sponsors of Vitality or otherwise affiliated with Vitality. The logos and other identifying marks attached are trademarks of and are marked by each represented company and/or its affiliates. Please visits each company’s website for additional terms and conditions.
      public static let RewardNotice2621: String = CommonStrings.tr("Common", "AR.vouchersolution.reward_notice_2621")
      /// Congratulations! You have won a voucher for %1$@ for the value of %2$@. Your voucher will be available for use from next Monday.
      public static func RewardTitle2620(_ p1: String, _ p2: String) -> String {
        return CommonStrings.tr("Common", "AR.vouchersolution.reward_title_2620", p1, p2)
      }
      /// To redeem, add this voucher during checkout on %1$@.
      public static func RewardVoucherRedeemMessage2631(_ p1: String) -> String {
        return CommonStrings.tr("Common", "AR.vouchersolution.reward_voucher_redeem_message_2631", p1)
      }
      /// Voucher Code
      public static let RewardVoucherTitle2630: String = CommonStrings.tr("Common", "AR.vouchersolution.reward_voucher_title_2630")
    }
  }

  public enum Assessment {
    /// Enter Details
    public static let EnterDetails352: String = CommonStrings.tr("Common", "assessment.enter_details_352")
    /// Enter
    public static let GenericInputPlaceholder507: String = CommonStrings.tr("Common", "assessment.generic_input_placeholder_507")
    /// Remove image
    public static let RemoveImage2463: String = CommonStrings.tr("Common", "assessment.remove_image_2463")
    /// View uploaded image
    public static let ViewUploadImage2462: String = CommonStrings.tr("Common", "assessment.view_upload_image_2462")

    public enum DateTested {
      /// Please select
      public static let DefaultValue2149: String = CommonStrings.tr("Common", "assessment.date_tested.default_value_2149")
      /// 
      public static let DefaultValue9999: String = CommonStrings.tr("Common", "assessment.date_tested.default_value_9999")
    }

    public enum Source {
      /// device
      public static let Device574: String = CommonStrings.tr("Common", "assessment.source.device_574")
      /// Vitality Health Check
      public static let Vhc571: String = CommonStrings.tr("Common", "assessment.source.vhc_571")
      /// Vitality Health Review
      public static let Vhr572: String = CommonStrings.tr("Common", "assessment.source.vhr_572")
      /// Vitality Nutrition Assessment
      public static let Vna573: String = CommonStrings.tr("Common", "assessment.source.vna_573")
    }

    public enum UnitOfMeasure {
      /// cm
      public static let CentimeterAbbreviation2150: String = CommonStrings.tr("Common", "assessment.unit_of_measure.centimeter_abbreviation_2150")
      /// Centimeter
      public static let CentimeterText2151: String = CommonStrings.tr("Common", "assessment.unit_of_measure.centimeter_text_2151")
      /// Days
      public static let DaysAbbreviation641: String = CommonStrings.tr("Common", "assessment.unit_of_measure.days_abbreviation_641")
      /// Days
      public static let DaysText642: String = CommonStrings.tr("Common", "assessment.unit_of_measure.days_text_642")
      /// ft
      public static let FootAbbreviation2152: String = CommonStrings.tr("Common", "assessment.unit_of_measure.foot_abbreviation_2152")
      /// 
      public static let FootAbbreviation9999: String = CommonStrings.tr("Common", "assessment.unit_of_measure.foot_abbreviation_9999")
      /// ft. in.
      public static let FootInchAbbreviation2153: String = CommonStrings.tr("Common", "assessment.unit_of_measure.foot_inch_abbreviation_2153")
      /// FootInch
      public static let FootInchText2154: String = CommonStrings.tr("Common", "assessment.unit_of_measure.foot_inch_text_2154")
      /// Foot
      public static let FootText2155: String = CommonStrings.tr("Common", "assessment.unit_of_measure.foot_text_2155")
      /// g
      public static let GramAbbreviation2156: String = CommonStrings.tr("Common", "assessment.unit_of_measure.gram_abbreviation_2156")
      /// Gram
      public static let GramText2157: String = CommonStrings.tr("Common", "assessment.unit_of_measure.gram_text_2157")
      /// in
      public static let InchAbbreviation2158: String = CommonStrings.tr("Common", "assessment.unit_of_measure.inch_abbreviation_2158")
      /// Inch
      public static let InchText2159: String = CommonStrings.tr("Common", "assessment.unit_of_measure.inch_text_2159")
      /// kg
      public static let KilogramAbbreviation2160: String = CommonStrings.tr("Common", "assessment.unit_of_measure.kilogram_abbreviation_2160")
      /// Kilogram
      public static let KilogramText2161: String = CommonStrings.tr("Common", "assessment.unit_of_measure.kilogram_text_2161")
      /// km
      public static let KilometerAbbreviation2162: String = CommonStrings.tr("Common", "assessment.unit_of_measure.kilometer_abbreviation_2162")
      /// Kilometer per hour
      public static let KilometerPerHourText2163: String = CommonStrings.tr("Common", "assessment.unit_of_measure.kilometer_per_hour_text_2163")
      /// Kilometer
      public static let KilometerText2164: String = CommonStrings.tr("Common", "assessment.unit_of_measure.kilometer_text_2164")
      /// kPa
      public static let KilopascalAbbreviation2165: String = CommonStrings.tr("Common", "assessment.unit_of_measure.kilopascal_abbreviation_2165")
      /// KiloPascal
      public static let KilopascalText2166: String = CommonStrings.tr("Common", "assessment.unit_of_measure.kilopascal_text_2166")
      /// m
      public static let MeterAbbreviation2167: String = CommonStrings.tr("Common", "assessment.unit_of_measure.meter_abbreviation_2167")
      /// Meter
      public static let MeterText2168: String = CommonStrings.tr("Common", "assessment.unit_of_measure.meter_text_2168")
      /// mi
      public static let MileAbbreviation2169: String = CommonStrings.tr("Common", "assessment.unit_of_measure.mile_abbreviation_2169")
      /// Mile
      public static let MileText2170: String = CommonStrings.tr("Common", "assessment.unit_of_measure.mile_text_2170")
      /// mg/dL
      public static let MilligramsPerDeciliterAbbreviation2171: String = CommonStrings.tr("Common", "assessment.unit_of_measure.milligrams_per_deciliter_abbreviation_2171")
      /// 
      public static let MilligramsPerDeciliterAbbreviation9999: String = CommonStrings.tr("Common", "assessment.unit_of_measure.milligrams_per_deciliter_abbreviation_9999")
      /// Milligrams Per Deci Litre
      public static let MilligramsPerDeciliterText562: String = CommonStrings.tr("Common", "assessment.unit_of_measure.milligrams_per_deciliter_text_562")
      /// mmHg
      public static let MillimeterOfMercuryAbbreviation2172: String = CommonStrings.tr("Common", "assessment.unit_of_measure.millimeter_of_mercury_abbreviation_2172")
      /// Millimeter of mercury
      public static let MillimeterOfMercuryText2173: String = CommonStrings.tr("Common", "assessment.unit_of_measure.millimeter_of_mercury_text_2173")
      /// mmol/L
      public static let MillimolesPerLiterAbbreviation2174: String = CommonStrings.tr("Common", "assessment.unit_of_measure.millimoles_per_liter_abbreviation_2174")
      /// Millimoles Per Litre
      public static let MillimolesPerLiterText2175: String = CommonStrings.tr("Common", "assessment.unit_of_measure.millimoles_per_liter_text_2175")
      /// oz
      public static let OunceAbbreviation2176: String = CommonStrings.tr("Common", "assessment.unit_of_measure.ounce_abbreviation_2176")
      /// Ounce
      public static let OunceText2177: String = CommonStrings.tr("Common", "assessment.unit_of_measure.ounce_text_2177")
      /// Per day
      public static let PerDay505: String = CommonStrings.tr("Common", "assessment.unit_of_measure.per_day_505")
      /// Per week
      public static let PerWeek506: String = CommonStrings.tr("Common", "assessment.unit_of_measure.per_week_506")
      /// %
      public static let PercentageAbbreviation2178: String = CommonStrings.tr("Common", "assessment.unit_of_measure.percentage_abbreviation_2178")
      /// Percentage
      public static let PercentageText2179: String = CommonStrings.tr("Common", "assessment.unit_of_measure.percentage_text_2179")
      /// lb
      public static let PoundAbbreviation2180: String = CommonStrings.tr("Common", "assessment.unit_of_measure.pound_abbreviation_2180")
      /// Pound
      public static let PoundText2181: String = CommonStrings.tr("Common", "assessment.unit_of_measure.pound_text_2181")
      /// st
      public static let StoneAbbreviation2182: String = CommonStrings.tr("Common", "assessment.unit_of_measure.stone_abbreviation_2182")
      /// st lb
      public static let StonePoundAbbreviation643: String = CommonStrings.tr("Common", "assessment.unit_of_measure.stone_pound_abbreviation_643")
      /// Stone Pound
      public static let StonePoundText644: String = CommonStrings.tr("Common", "assessment.unit_of_measure.stone_pound_text_644")
      /// Stone
      public static let StoneText2183: String = CommonStrings.tr("Common", "assessment.unit_of_measure.stone_text_2183")
      /// T
      public static let TonAbbreviation2184: String = CommonStrings.tr("Common", "assessment.unit_of_measure.ton_abbreviation_2184")
      /// Ton
      public static let TonText2185: String = CommonStrings.tr("Common", "assessment.unit_of_measure.ton_text_2185")
      /// 
      public static let TonText9999: String = CommonStrings.tr("Common", "assessment.unit_of_measure.ton_text_9999")
    }

    public enum Vhr {
      /// Metric captured from %1$@ on %2$@.
      public static func PrePopulationDetails570(_ p1: String, _ p2: String) -> String {
        return CommonStrings.tr("Common", "assessment.vhr.pre_population_details_570", p1, p2)
      }
    }
  }

  public enum Awc {
    /// Points are earned when exercising for a certain time and reaching an age-related maximum heart rate between 60-70%. \n\nTo calculate your maximum heart rate, take 220 and subtract your age. For example, if you are 30 years old, 190 beats per minute (bpm) is your maximum heart rate (220 - 30 = 190). \n\nCalculate your required heart rate by multiplying 60% or 70% to your maximum heart rate. If you are 30 years old, your target heart rate at 60% is 114bpm and at 70% is 133bpm. \n\nExample: \nHow do I calculate my age-related maximum heart rate? \nJohn is 30 years old. His age-related maximum heart rate is 190bpm (220 - 30). \nJohn must keep his average heart rate above 114bpm (60% of his maximum heart rate) during his 30-minute exercise session to earn 50 Vitality points. \nIf John keeps his heart rate above 133bpm (70% of his maximum heart rate), he will earn 100 points.
    public static let HowToCalculateHeartRate484: String = CommonStrings.tr("Common", "awc.how_to_calculate_heart_rate_484")
    /// In Progress
    public static let LandingActivitySubTitle2026: String = CommonStrings.tr("Common", "awc.landing_activity_sub_title_2026")
    /// Current Vitality Coins
    public static let LandingProgressMonthCurrentCoins2024: String = CommonStrings.tr("Common", "awc.landing_progress_month_current_coins_2024")
    /// You have %1$@ Vitality Points
    public static func LandingProgressMonthCurrentPoints2025(_ p1: String) -> String {
      return CommonStrings.tr("Common", "awc.landing_progress_month_current_points_2025", p1)
    }
    /// Only activities resulting in points will show here.
    public static let MonthActivityMessage2029: String = CommonStrings.tr("Common", "awc.month_activity_message_2029")
    /// No activities logged
    public static let MonthActivityNoActivities2028: String = CommonStrings.tr("Common", "awc.month_activity_no_activities_2028")
    /// %1$@ steps
    public static func MonthSteps2187(_ p1: String) -> String {
      return CommonStrings.tr("Common", "awc.month_steps_2187", p1)
    }
    /// 
    public static let MonthSteps9999: String = CommonStrings.tr("Common", "awc.month_steps_9999")

    public enum CoinsEarned {
      /// Vitality Coins Pending
      public static let CoinsPending2030: String = CommonStrings.tr("Common", "awc.coins_earned.coins_pending_2030")
      /// %1$@ Vitality Coins Earned
      public static func Description2038(_ p1: String) -> String {
        return CommonStrings.tr("Common", "awc.coins_earned.description_2038", p1)
      }
      /// We allow until %1$@th day of the month\nfollowing your target for points to be allocated before closing and processing your Vitality Coins.
      public static func MonthCoinsPendingMessage2032(_ p1: String) -> String {
        return CommonStrings.tr("Common", "awc.coins_earned.month_coins_pending_message_2032", p1)
      }
      /// If you have any issues with your Vitality Coins\nplease contact your Sumitomo Life Vitality Service Center.
      public static let MonthMessage2031: String = CommonStrings.tr("Common", "awc.coins_earned.month_message_2031")
      /// You can track the history of you Vitality Coins earned here.
      public static let NoCoinsEarnedDescription2016: String = CommonStrings.tr("Common", "awc.coins_earned.no_coins_earned_description_2016")
      /// No Vitality Coins Earned
      public static let NoCoinsEarnedTitle2015: String = CommonStrings.tr("Common", "awc.coins_earned.no_coins_earned_title_2015")
    }

    public enum HomeCard {
      /// Get Apple Watch
      public static let Message1992: String = CommonStrings.tr("Common", "awc.home_card.message_1992")
      /// %1$@ points to reach\n%2$@ Vitality Coins
      public static func PointsToReachDescription2027(_ p1: String, _ p2: String) -> String {
        return CommonStrings.tr("Common", "awc.home_card.points_to_reach_description_2027", p1, p2)
      }
      /// %1$@ cash back earned
      public static func Subtitle2611(_ p1: String) -> String {
        return CommonStrings.tr("Common", "awc.home_card.subtitle_2611", p1)
      }
      /// Apple Watch
      public static let Title1991: String = CommonStrings.tr("Common", "awc.home_card.title_1991")
      /// Get cash back on an Apple Watch purchase
      public static let Title2605: String = CommonStrings.tr("Common", "awc.home_card.title_2605")
      /// Awaiting shipment on your Apple Watch
      public static let Title2606: String = CommonStrings.tr("Common", "awc.home_card.title_2606")
      /// Link to Apple Health app to start earning rewards
      public static let Title2607: String = CommonStrings.tr("Common", "awc.home_card.title_2607")
      /// Start earning cash back %1$@
      public static func Title2608(_ p1: String) -> String {
        return CommonStrings.tr("Common", "awc.home_card.title_2608", p1)
      }
      /// %1$@ more points needed to get %2$@ cash back
      public static func Title2609(_ p1: String, _ p2: String) -> String {
        return CommonStrings.tr("Common", "awc.home_card.title_2609", p1, p2)
      }
      /// Well done! You've earned your full cash back for this month
      public static let Title2610: String = CommonStrings.tr("Common", "awc.home_card.title_2610")

      public enum EarningCoins {
        /// Link Health App
        public static let Footer2018: String = CommonStrings.tr("Common", "awc.home_card.earning_coins.footer_2018")
        /// Start Earning Vitality Coins
        public static let Title2017: String = CommonStrings.tr("Common", "awc.home_card.earning_coins.title_2017")
      }

      public enum NewOrder {
        /// Order Now
        public static let Message2035: String = CommonStrings.tr("Common", "awc.home_card.new_order.message_2035")
        /// Order New Apple Watch
        public static let Title2034: String = CommonStrings.tr("Common", "awc.home_card.new_order.title_2034")
      }

      public enum Points {
        /// You've reached %1$@ Vitality Coins.
        public static func Description2033(_ p1: String) -> String {
          return CommonStrings.tr("Common", "awc.home_card.points.description_2033", p1)
        }
      }
    }

    public enum Landing {
      /// Vitality Coins Earned
      public static let ActionMenuTitle1999: String = CommonStrings.tr("Common", "awc.landing.action_menu_title_1999")
      /// How to Track Vitality Coin
      public static let ActionMenuTitle2000: String = CommonStrings.tr("Common", "awc.landing.action_menu_title_2000")
      /// Get Your Apple Watch
      public static let HeaderTitle1997: String = CommonStrings.tr("Common", "awc.landing.header_title_1997")
      /// Earn Vitality Coins by tracking your verified workouts with an Apple Watch purchased through the online store.\n\nIf you have already purchased your Apple Watch, check back shortly to continue your activation, otherwise you can get your Apple Watch now.
      public static let HeaderTitle1998: String = CommonStrings.tr("Common", "awc.landing.header_title_1998")
      /// Be sure that you have allowed the necessary Health app permissions (weight, steps and Active Energy).
      public static let MonthlyTargetReminder2022: String = CommonStrings.tr("Common", "awc.landing.monthly_target_reminder_2022")
      /// When your Apple Watch is delivered, you must link your Apple Watch, give permission to share your workout data with Vitality and complete at least one workout by the end of the 3rd month after you Apple Watch was shipped.
      public static let ShippedDeviceDescription2005: String = CommonStrings.tr("Common", "awc.landing.shipped_device_description_2005")
      /// Your Apple Watch will be shipped on %1$@
      public static func ShippedDeviceTitle2004(_ p1: String) -> String {
        return CommonStrings.tr("Common", "awc.landing.shipped_device_title_2004", p1)
      }
      /// You are eligible to start a new Apple Watch Vitality Coin cycle!\n\nGet your new Apple Watch and start earning points for completing workout activities and earn Vitality coins on your chosen device.
      public static let WellDoneDesc2037: String = CommonStrings.tr("Common", "awc.landing.well_done_desc_2037")
      /// Well Done on Earning %1$@ coins for Your Previous Vitality Coins Cycle!
      public static func WellDoneHeader2036(_ p1: String) -> String {
        return CommonStrings.tr("Common", "awc.landing.well_done_header_2036", p1)
      }

      public enum EarningCoins {
        /// You have received your Apple Watch. Link the Health app to activate your Monthly Targets and start earning Vitality Coins.
        public static let Description2019: String = CommonStrings.tr("Common", "awc.landing.earning_coins.description_2019")
      }
    }

    public enum Learnmore {
      /// Purchase your Apple Watch through the online store designed exclusively for Vitality members\n\n•	Qualifying devices include an Apple Watch Series1 (requires an iPhone 5s or later with iOS 11 or later) and Apple Watch Series 3 (GPS) which requires an iPhone 6 or later with iOS 11 or later.\n\n•	You can use your benefit for one Apple Watch per policy in a 24 month period.\n\n•	You must link your Apple Watch with Vitality and submit one verified workout by the end of the third month from the month your Apple Watch was dispatched.
      public static let SectionBodyTitle2003: String = CommonStrings.tr("Common", "awc.learnmore.section_body_title_2003")
      /// You can get Apple Watch and use it to achieve your montly targets. If you meet your montly targets, we will give Vitality Coins worth up to 1000 yen a month.
      public static let SectionHeaderDescription2002: String = CommonStrings.tr("Common", "awc.learnmore.section_header_description_2002")
      /// Vitality Active Challenge with Apple Watch
      public static let SectionHeaderTitle2001: String = CommonStrings.tr("Common", "awc.learnmore.section_header_title_2001")
    }

    public enum Onboarding {
      /// Purchase your Apple Watch through the exclusive Vitality member online store.
      public static let SectionMessage1994: String = CommonStrings.tr("Common", "awc.onboarding.section_message_1994")
      /// Your Apple Watch will help you stay active. The more active you are, the more Vitality Coins you'll earn (up to  1000 yen a month).
      public static let SectionMessage1995: String = CommonStrings.tr("Common", "awc.onboarding.section_message_1995")
      /// Earn Vitality Points for verified workouts. The number of points you earn determines how many Vitality Coins you are awarded each month.
      public static let SectionMessage1996: String = CommonStrings.tr("Common", "awc.onboarding.section_message_1996")
      /// To earn points you'll need to allow Health app access to Active Energy, Heart rate, Steps and Workouts.
      public static let SectionMessage2020: String = CommonStrings.tr("Common", "awc.onboarding.section_message_2020")
      /// Your Apple Watch Vitality Coins has successfully been activated.
      public static let SectionMessage2021: String = CommonStrings.tr("Common", "awc.onboarding.section_message_2021")
    }

    public enum PotentialPoints {
      /// %1$@ points
      public static func SectionHeader495(_ p1: String) -> String {
        return CommonStrings.tr("Common", "awc.potential_points.section_header_495", p1)
      }
    }

    public enum TrackCoins {
      /// Gym Points Events
      public static let ActionMenuTitle2012: String = CommonStrings.tr("Common", "awc.track_coins.action_menu_title_2012")
      /// Coins
      public static let CoinsTitle2008: String = CommonStrings.tr("Common", "awc.track_coins.coins_title_2008")
      /// You can start to earn Vitality Coins from when your montly target becomes active. The amount of verified physical activity you do determines the number of Vitality coins you can earn.\n\nFor example, if you reach 1200 poinst for qualifying activities, you will earn 1000 Vitality Coins for that month
      public static let HeaderMessage2007: String = CommonStrings.tr("Common", "awc.track_coins.header_message_2007")
      /// How to track your Vitality Coins
      public static let HeaderTitle2006: String = CommonStrings.tr("Common", "awc.track_coins.header_title_2006")
      /// Vitality Coins can be exchanged for electornic money. Exchange to electronic money will be possible from the beginning of January 2019\n\nApple is not a participant in or sponsor of this promotion. Apple is a registered trademark Apple Inc. All right reserved.
      public static let SectionContent2010: String = CommonStrings.tr("Common", "awc.track_coins.section_content_2010")
      /// Only verified physical activity events will count towards targets. These include:\n\n• Step based points events\n• Heart rate-based points events\n• Gym points events
      public static let SectionContent2011: String = CommonStrings.tr("Common", "awc.track_coins.section_content_2011")
      /// To track points earning metrics, you need to allow access ot the below metrics when allowing access to your Health data:\n\n• Heart Rate\n• Steps\n• Workouts\n• Active Energy
      public static let SectionContent2014: String = CommonStrings.tr("Common", "awc.track_coins.section_content_2014")
      /// About Vitality Coins
      public static let SectionTitle2009: String = CommonStrings.tr("Common", "awc.track_coins.section_title_2009")
      /// Health Access Requirements
      public static let SectionTitle2013: String = CommonStrings.tr("Common", "awc.track_coins.section_title_2013")
    }
  }

  public enum CaptureResults {
    /// Points are only awarded for either random or fasting glucose, not both.
    public static let BloodGlucoseMessage154: String = CommonStrings.tr("Common", "capture_results.blood_glucose_message_154")
    /// To earn points for your body mass index you need to capture both your height and weight measurements. Do you want to continue without capturing this?
    public static let BodyMassIndexAlertMessage162: String = CommonStrings.tr("Common", "capture_results.body_mass_index_alert_message_162")
    /// Good cholesterol.
    public static let CholesterolFootnoteMessage158: String = CommonStrings.tr("Common", "capture_results.cholesterol_footnote_message_158")
    /// Bad cholesterol.
    public static let CholesterolFootnoteMessage159: String = CommonStrings.tr("Common", "capture_results.cholesterol_footnote_message_159")
    /// Points are not awarded for HDL, LDL and triglycerides, only for total cholesterol.
    public static let CholesterolMessage155: String = CommonStrings.tr("Common", "capture_results.cholesterol_message_155")
    /// Date Tested
    public static let DateTested144: String = CommonStrings.tr("Common", "capture_results.date_tested_144")
    /// Ensure you have had your health measurements taken by a healthcare provider. You will need to upload proof of those results in order to complete your Vitality Heath Check.
    public static let Intro1Message143: String = CommonStrings.tr("Common", "capture_results.intro_1_message_143")
    /// Only capture measurements that you can upload verified proof for.
    public static let Intro1Title142: String = CommonStrings.tr("Common", "capture_results.intro_1_title_142")
    /// Certain points earning metrics have not been captured. Are you sure you want to continue?
    public static let MissingPointsAlertMessage1172: String = CommonStrings.tr("Common", "capture_results.missing_points_alert_message_1172")
    /// Missing Points Earning Metrics
    public static let MissingPointsAlertTitle1171: String = CommonStrings.tr("Common", "capture_results.missing_points_alert_title_1171")
    /// None
    public static let NoneMessage337: String = CommonStrings.tr("Common", "capture_results.none_message_337")
    /// Result
    public static let ResultsTitle287: String = CommonStrings.tr("Common", "capture_results.results_title_287")
    /// Points are only awarded for waist circumference when your Body Mass Index is out of healthy range.
    public static let WaistCircumferenceMessage153: String = CommonStrings.tr("Common", "capture_results.waist_circumference_message_153")
  }

  public enum Common {
    /// Submit
    public static let ActionSubmit2616: String = CommonStrings.tr("Common", "common.action_submit_2616")
    /// Current Communications Email
    public static let CurrentCommunicationEmail2196: String = CommonStrings.tr("Common", "common.current_communication_email_2196")
    /// 
    public static let CurrentCommunicationEmail9999: String = CommonStrings.tr("Common", "common.current_communication_email_9999")
    /// A connection error has occurred. Please make sure that you are connected to a wi-fi or cellular network and try again.
    public static let DialogConnectionErrorMessage2198: String = CommonStrings.tr("Common", "common.dialog_connection_error_message_2198")
    /// The file you are trying to upload exceeds the maximum upload limit of %1$@.
    public static func DialogUploadLimitErrorMessage2199(_ p1: String) -> String {
      return CommonStrings.tr("Common", "common.dialog_upload_limit_error_message_2199", p1)
    }
    /// 
    public static let DialogUploadLimitErrorMessage9999: String = CommonStrings.tr("Common", "common.dialog_upload_limit_error_message_9999")
    /// NO
    public static let NoAnwer2200: String = CommonStrings.tr("Common", "common.no_anwer_2200")
    /// Previous
    public static let PreviousButtonTitle2461: String = CommonStrings.tr("Common", "common.previous_button_title_2461")
    /// Biometrics authentication was disabled for this app. Please enable it on Settings.
    public static let PromptBiometrics2201: String = CommonStrings.tr("Common", "Common.prompt_biometrics_2201")
    /// 
    public static let PromptBiometrics9999: String = CommonStrings.tr("Common", "Common.prompt_biometrics_9999")
    /// Support
    public static let SupportTitle2203: String = CommonStrings.tr("Common", "common.support_title_2203")
    /// 
    public static let SupportTitle9999: String = CommonStrings.tr("Common", "common.support_title_9999")
    /// Warning
    public static let Warning2207: String = CommonStrings.tr("Common", "common.warning_2207")
    /// 
    public static let Warning9999: String = CommonStrings.tr("Common", "common.warning_9999")
    /// YES
    public static let YesAnswer2208: String = CommonStrings.tr("Common", "common.yes_answer_2208")

    public enum DateFormat {
      /// YYYY/MM/DD
      public static let Yyyymmdd2197: String = CommonStrings.tr("Common", "common.date_format.yyyymmdd_2197")
      /// 
      public static let Yyyymmdd9999: String = CommonStrings.tr("Common", "common.date_format.yyyymmdd_9999")
    }

    public enum Safari {
      /// Please disable content blocking to fully access this site.
      public static let AllowPopupsDescription2202: String = CommonStrings.tr("Common", "common.safari.allow_popups_description_2202")
      /// 
      public static let AllowPopupsDescription9999: String = CommonStrings.tr("Common", "common.safari.allow_popups_description_9999")
    }

    public enum UploadAttachmentSubtitle2204 {
      /// 1 Attachment
      public static let One: String = CommonStrings.tr("Common", "common.upload_attachment_subtitle_2204.one")
      /// %1$@ Attachments
      public static func Other(_ p1: String) -> String {
        return CommonStrings.tr("Common", "common.upload_attachment_subtitle_2204.other", p1)
      }
      /// Upload Attachment
      public static let Zero: String = CommonStrings.tr("Common", "common.upload_attachment_subtitle_2204.zero")
    }

    public enum UploadAttachmentSubtitle9999 {
      /// 
      public static let One: String = CommonStrings.tr("Common", "common.upload_attachment_subtitle_9999.one")
      /// 
      public static let Other: String = CommonStrings.tr("Common", "common.upload_attachment_subtitle_9999.other")
      /// 
      public static let Zero: String = CommonStrings.tr("Common", "common.upload_attachment_subtitle_9999.zero")
    }
  }

  public enum CompleteScreen {
    /// You can resubmit as many times as needed to maximise your points.
    public static let MeasurementsConfirmedMessage189: String = CommonStrings.tr("Common", "complete_screen.measurements_confirmed_message_189")
    /// Measurements Confirmed
    public static let MeasurementsConfirmedTitle188: String = CommonStrings.tr("Common", "complete_screen.measurements_confirmed_title_188")
    /// The Vitality Health Review can only be completed for points once a year.
    public static let SectionCompletedMessage2334: String = CommonStrings.tr("Common", "complete_screen.section_completed_message_2_334")
    /// %1$@ Sections remaining to earn %2$@ points!
    public static func SectionCompletedMessage332(_ p1: String, _ p2: String) -> String {
      return CommonStrings.tr("Common", "complete_screen.section_completed_message_332", p1, p2)
    }
    /// %@ Completed!
    public static func SectionCompletedTitle1331(_ p1: String) -> String {
      return CommonStrings.tr("Common", "complete_screen.section_completed_title_1_331", p1)
    }
    /// All Sections Completed
    public static let SectionCompletedTitle2333: String = CommonStrings.tr("Common", "complete_screen.section_completed_title_2_333")
  }

  public enum Completed {
    /// All Sections Completed!
    public static let AllQuestionnairesCompletedTitle2209: String = CommonStrings.tr("Common", "completed.all_questionnaires_completed_title_2209")
    /// 
    public static let AllQuestionnairesCompletedTitle9991: String = CommonStrings.tr("Common", "completed.all_questionnaires_completed_title_9991")
    /// %1$@ Sections remaining to earn %2$@ points.
    public static func MultipleRemainingQuestionnairesEarnPointsMessage2210(_ p1: String, _ p2: String) -> String {
      return CommonStrings.tr("Common", "completed.multiple_remaining_questionnaires_earn_points_message_2210", p1, p2)
    }
    /// 
    public static let MultipleRemainingQuestionnairesEarnPointsMessage9992: String = CommonStrings.tr("Common", "completed.multiple_remaining_questionnaires_earn_points_message_9992")
    /// %1$@ Completed!
    public static func SingleQuestionnaireCompletedTitle2211(_ p1: String) -> String {
      return CommonStrings.tr("Common", "completed.single_questionnaire_completed_title_2211", p1)
    }
    /// 
    public static let SingleQuestionnaireCompletedTitle9998: String = CommonStrings.tr("Common", "completed.single_questionnaire_completed_title_9998")
    /// %1$@ Section remaining to earn %2$@ points.
    public static func SingleRemainingQuestionnairesEarnPointsMessage2212(_ p1: String, _ p2: String) -> String {
      return CommonStrings.tr("Common", "completed.single_remaining_questionnaires_earn_points_message_2212", p1, p2)
    }
    /// 
    public static let SingleRemainingQuestionnairesEarnPointsMessage9993: String = CommonStrings.tr("Common", "completed.single_remaining_questionnaires_earn_points_message_9993")
  }

  public enum Confirmation {
    /// Points may not be awarded immediately.
    public static let CompletedFootnotePointsMessage119: String = CommonStrings.tr("Common", "confirmation.completed_footnote_points_message_119")
    /// Way to go, Assessment completed! You only earn points for signing a non-smoker’s declaration once a year.
    public static let CompletedMessage118: String = CommonStrings.tr("Common", "confirmation.completed_message_118")
    /// Completed!
    public static let CompletedTitle117: String = CommonStrings.tr("Common", "confirmation.completed_title_117")
    /// Completed!
    public static let CompletedTitle351: String = CommonStrings.tr("Common", "confirmation.completed_title_351")
  }

  public enum Dc {
    /// Invalid Serial Number
    public static let ActivationInvalidSerialNumber1500: String = CommonStrings.tr("Common", "dc.activation_invalid_serial_number_1500")
    /// Invalid Invoice Number
    public static let ActivationInvalidVoiceNumber1499: String = CommonStrings.tr("Common", "dc.activation_invalid_voice_number_1499")
    /// Verified proof of purchase is required to activate your benefit successfully
    public static let LandingActivationDescription1488: String = CommonStrings.tr("Common", "dc.landing_activation_description_1488")
    /// Device Serial Number
    public static let LandingActivationPurchaseDetailDevSerialNumberField1493: String = CommonStrings.tr("Common", "dc.landing_activation_purchase_detail_dev_serial_number_field_1493")
    /// 000000000000
    public static let LandingActivationPurchaseDetailDevSerialNumberHint1494: String = CommonStrings.tr("Common", "dc.landing_activation_purchase_detail_dev_serial_number_hint_1494")
    /// You have 60 days after purchasing your device to link it with Vitality. if you miss the 60-day mark, please contact 123456789.
    public static let LandingActivationPurchaseDetailFooter1498: String = CommonStrings.tr("Common", "dc.landing_activation_purchase_detail_footer_1498")
    /// Invoice Number
    public static let LandingActivationPurchaseDetailInvoiceNumberField1491: String = CommonStrings.tr("Common", "dc.landing_activation_purchase_detail_invoice_number_field_1491")
    /// 000000
    public static let LandingActivationPurchaseDetailInvoiceNumberHint1492: String = CommonStrings.tr("Common", "dc.landing_activation_purchase_detail_invoice_number_hint_1492")
    /// Model
    public static let LandingActivationPurchaseDetailModelField1490: String = CommonStrings.tr("Common", "dc.landing_activation_purchase_detail_model_field_1490")
    /// Purchase Amount
    public static let LandingActivationPurchaseDetailPurchaseAmtField1495: String = CommonStrings.tr("Common", "dc.landing_activation_purchase_detail_purchase_amt_field_1495")
    /// $0.00
    public static let LandingActivationPurchaseDetailPurchaseAmtHint1496: String = CommonStrings.tr("Common", "dc.landing_activation_purchase_detail_purchase_amt_hint_1496")
    /// Purchase Date
    public static let LandingActivationPurchaseDetailPurchaseDteField1497: String = CommonStrings.tr("Common", "dc.landing_activation_purchase_detail_purchase_dte_field_1497")
    /// Purchase Detail
    public static let LandingActivationPurchaseDetailTitle1489: String = CommonStrings.tr("Common", "dc.landing_activation_purchase_detail_title_1489")
    /// Link Samsung Health to start tracking your physical activity.
    public static let LinkSamsungHealthDialogDescription2214: String = CommonStrings.tr("Common", "dc.link_samsung_health_dialog_description_2214")
    /// Please check and confirm that the purchase details and proof of results are correct.
    public static let ProofOfPurchaseSummaryConfirmInfoDescription1507: String = CommonStrings.tr("Common", "dc.proof_of_purchase_summary_confirm_info_description_1507")
    /// Confirm Information
    public static let ProofOfPurchaseSummaryConfirmInfoHeader1506: String = CommonStrings.tr("Common", "dc.proof_of_purchase_summary_confirm_info_header_1506")
    /// Image Proof
    public static let ProofOfPurchaseSummaryImageProofHeader1509: String = CommonStrings.tr("Common", "dc.proof_of_purchase_summary_image_proof_header_1509")
    /// Purchase Detail
    public static let ProofOfPurchaseSummaryPurchaseDetailHeader1508: String = CommonStrings.tr("Common", "dc.proof_of_purchase_summary_purchase_detail_header_1508")
    /// Summary
    public static let ProofOfPurchaseSummaryTitle1505: String = CommonStrings.tr("Common", "dc.proof_of_purchase_summary_title_1505")

    public enum Activate {
      /// Only activate your Device Cashback if you have proof of your purchase.
      public static let HeaderTitle1487: String = CommonStrings.tr("Common", "dc.activate.header_title_1487")
    }

    public enum Attention {
      /// To earn points for your activity, please give Vitality access to your %1$@.
      public static func LinkAppDescription1513(_ p1: String) -> String {
        return CommonStrings.tr("Common", "dc.attention.link_app_description_1513", p1)
      }
      /// To earn points for your activity, please give Vitality access to your %1$@ device.
      public static func LinkDeviceDescription1513(_ p1: String) -> String {
        return CommonStrings.tr("Common", "dc.attention.link_device_description_1513", p1)
      }
      /// Link Later
      public static let LinkDeviceLaterButton1515: String = CommonStrings.tr("Common", "dc.attention.link_device_later_button_1515")
      /// Link Now
      public static let LinkDeviceNowButton1514: String = CommonStrings.tr("Common", "dc.attention.link_device_now_button_1514")
      /// Link %1$@
      public static func LinkDeviceTitle1512(_ p1: String) -> String {
        return CommonStrings.tr("Common", "dc.attention.link_device_title_1512", p1)
      }
    }

    public enum CashbackEarned {
      /// Activity
      public static let ActivityHeader1543: String = CommonStrings.tr("Common", "dc.cashback_earned.activity_header_1543")
      /// If you have any issues with your cashback please contract your insurer.
      public static let DeviceDetailsHeaderFooter1542: String = CommonStrings.tr("Common", "dc.cashback_earned.device_details_header_footer_1542")
      /// We allow until the 6th day of the month following your target for points to be allocated before closing and processing your cashback.
      public static let EarnedDetailsHeaderviewFooter1544: String = CommonStrings.tr("Common", "dc.cashback_earned.earned_details_headerview_footer_1544")
      /// There is a cashback limit on this device of %1$@
      public static func FooterAmount1539(_ p1: String) -> String {
        return CommonStrings.tr("Common", "dc.cashback_earned.footer_amount_1539", p1)
      }
      /// Cashback Earned
      public static let FooterItemSubtitle1568: String = CommonStrings.tr("Common", "dc.cashback_earned.footer_item_subtitle_1568")
      /// Cashbacks Earned
      public static let FooterSubHeader1568: String = CommonStrings.tr("Common", "dc.cashback_earned.footer_sub_header_1568")
      /// From %1$@ to %2$@
      public static func PeriodHeader1537(_ p1: String, _ p2: String) -> String {
        return CommonStrings.tr("Common", "dc.cashback_earned.period_header_1537", p1, p2)
      }
      /// %1$@ months remaining
      public static func PeriodRemainingDesc1538(_ p1: String) -> String {
        return CommonStrings.tr("Common", "dc.cashback_earned.period_remaining_desc_1538", p1)
      }
      /// Previous Cashbacks
      public static let PreviousCashbacksTitle1540: String = CommonStrings.tr("Common", "dc.cashback_earned.previous_cashbacks_title_1540")
      /// Devices
      public static let PreviousDevicesHeader1541: String = CommonStrings.tr("Common", "dc.cashback_earned.previous_devices_header_1541")
      /// Previous Cashbacks
      public static let PreviousLink1540: String = CommonStrings.tr("Common", "dc.cashback_earned.previous_link_1540")
    }

    public enum CashbacksEarned {
      /// You can track the history of your cashbacks here.
      public static let EmptyViewMessage1479: String = CommonStrings.tr("Common", "dc.cashbacks_earned.empty_view_message_1479")
      /// No Cashbacks Earned
      public static let EmptyViewTitle1478: String = CommonStrings.tr("Common", "dc.cashbacks_earned.empty_view_title_1478")
    }

    public enum Common {
      /// Activate
      public static let ActivateTitle1466: String = CommonStrings.tr("Common", "dc.common.activate_title_1466")
      /// Your Order
      public static let YourOrder1559: String = CommonStrings.tr("Common", "dc.common.your_order_1559")
    }

    public enum Device {
      /// Change Cashback Device
      public static let ChangeCashbackDevice1547: String = CommonStrings.tr("Common", "dc.device.change_cashback_device_1547")
      /// Only one device can be activated at a time. \n\nUpgrade or activate a new device cashback by following the same activation process. Activating a new device will deactivate your current device cashback.
      public static let FooterMessage1548: String = CommonStrings.tr("Common", "dc.device.footer_message_1548")
      /// Current Device
      public static let SectionTitle1545: String = CommonStrings.tr("Common", "dc.device.section_title_1545")
      /// Get a New Device
      public static let SectionTitle1546: String = CommonStrings.tr("Common", "dc.device.section_title_1546")
    }

    public enum HomeCard {
      /// Learn How
      public static let Footer1457: String = CommonStrings.tr("Common", "dc.home_card.footer_1457")
      /// Activate Your Cashback
      public static let Message1456: String = CommonStrings.tr("Common", "dc.home_card.message_1456")
      /// %1$@ Points to Reach %2$@ Cashback
      public static func PointsToReachDescription1536(_ p1: String, _ p2: String) -> String {
        return CommonStrings.tr("Common", "dc.home_card.points_to_reach_description_1536", p1, p2)
      }
      /// You've reached %1$@ Cashback
      public static func ReachedCashbackDesc1569(_ p1: String) -> String {
        return CommonStrings.tr("Common", "dc.home_card.reached_cashback_desc_1569", p1)
      }
      /// Device Cashback
      public static let Title1455: String = CommonStrings.tr("Common", "dc.home_card.title_1455")
    }

    public enum Landing {
      /// Activate
      public static let ActionButton1466: String = CommonStrings.tr("Common", "dc.landing.action_button_1466")
      /// Qualifying Devices
      public static let ActionButton1467: String = CommonStrings.tr("Common", "dc.landing.action_button_1467")
      /// Learn More
      public static let ActionButton2489: String = CommonStrings.tr("Common", "dc.landing.action_button_2489")
      /// How Cash Back Works
      public static let ActionButton2491: String = CommonStrings.tr("Common", "dc.landing.action_button_2491")
      /// View and Purchase Available Devices
      public static let ActionMainButton2488: String = CommonStrings.tr("Common", "dc.landing.action_main_button_2488")
      /// Link Device Or App
      public static let ActionMainButton2490: String = CommonStrings.tr("Common", "dc.landing.action_main_button_2490")
      /// Cashbacks Earned
      public static let ActionMenuTitle1469: String = CommonStrings.tr("Common", "dc.landing.action_menu_title_1469")
      /// How to Track Cashbacks
      public static let ActionMenuTitle1470: String = CommonStrings.tr("Common", "dc.landing.action_menu_title_1470")
      /// Activate
      public static let ActionTitle1466: String = CommonStrings.tr("Common", "dc.landing.action_title_1466")
      /// Only activate your Device Cashback if you have proof of purchase.
      public static let ActivationHeader1487: String = CommonStrings.tr("Common", "dc.landing.activation_header_1487")
      /// You have submitted the details for your %1$@ device. To complete your activation, your device workout activity needs to sync. Please note that data can take up to 48 hours to sync to Vitality.
      public static func ActivitySyncPendingDescription1522(_ p1: String) -> String {
        return CommonStrings.tr("Common", "dc.landing.activity_sync_pending_description_1522", p1)
      }
      /// Device Activity Sync Pending
      public static let ActivitySyncPendingTitle1521: String = CommonStrings.tr("Common", "dc.landing.activity_sync_pending_title_1521")
      /// You have submitted the details for your %1$@ device. To complete your activation and successfully activate your cashback, link %1$@ to give Vitality access to your activity.
      public static func ContinueCashbackDescription1518(_ p1: String) -> String {
        return CommonStrings.tr("Common", "dc.landing.continue_cashback_description_1518", p1)
      }
      /// Continue your Cashback Activation
      public static let ContinueCashbackTitle1517: String = CommonStrings.tr("Common", "dc.landing.continue_cashback_title_1517")
      /// Device
      public static let DeviceMenuItem1520: String = CommonStrings.tr("Common", "dc.landing.device_menu_item_1520")
      /// 
      public static let FooterActivatedMessage2487: String = CommonStrings.tr("Common", "dc.landing.footer_activated_message_2487")
      /// Only one device can be activated at a time.
      public static let FooterMessage1468: String = CommonStrings.tr("Common", "dc.landing.footer_message_1468")
      /// It may take up to three days for the device to be delivered to you.
      public static let FooterMessage2493: String = CommonStrings.tr("Common", "dc.landing.footer_message_2493")
      /// If you experience any issues with your Garmin device or the delivery of your device, please visit Garmin online support.
      public static let FooterMessage2494: String = CommonStrings.tr("Common", "dc.landing.footer_message_2494")
      /// The cash back you earn will depend on the device you purchased.
      public static let FooterMessage2498: String = CommonStrings.tr("Common", "dc.landing.footer_message_2498")
      /// Garmin online support
      public static let FooterMessageCont2495: String = CommonStrings.tr("Common", "dc.landing.footer_message_cont_2495")
      /// Purchase your device from any retail store and start earning points for completing workout activities and earn cashback on your chosen device.
      public static let HeaderMessage1465: String = CommonStrings.tr("Common", "dc.landing.header_message_1465")
      /// Activate Your Device
      public static let HeaderTitle1464: String = CommonStrings.tr("Common", "dc.landing.header_title_1464")
      /// Link %1$@
      public static func LinkDeviceButton1519(_ p1: String) -> String {
        return CommonStrings.tr("Common", "dc.landing.link_device_button_1519", p1)
      }
      /// You can start earning points to get rewarded with Vitality Coins.
      public static let MonthlyTargetDescription2023: String = CommonStrings.tr("Common", "dc.landing.monthly_target_description_2023")
      /// Need Help?
      public static let MonthlyTargetHelpButtonTitle1553: String = CommonStrings.tr("Common", "dc.landing.monthly_target_help_button_title_1553")
      /// Once your monthly target begins you can start earning points towards your cashback.
      public static let MonthlyTargetMessage1526: String = CommonStrings.tr("Common", "dc.landing.monthly_target_message_1526")
      /// Your Monthly Target will start %1$@
      public static func MonthlyTargetTitle1525(_ p1: String) -> String {
        return CommonStrings.tr("Common", "dc.landing.monthly_target_title_1525", p1)
      }
      /// Samsung Device Benefit Activated
      public static let MonthlyTargetTitleActivated2213: String = CommonStrings.tr("Common", "dc.landing.monthly_target_title_activated_2213")
      /// You are eligible to start a new cashback cycle!\n\nPurchase your device from any retail store and start earning points for completing your workout activities and earn cash back on your chosen device.
      public static let WellDoneDesc1567: String = CommonStrings.tr("Common", "dc.landing.well_done_desc_1567")
      /// Well Done On Earning %1$@ for Your Previous Cashback Cycle!
      public static func WellDoneHeader1566(_ p1: String) -> String {
        return CommonStrings.tr("Common", "dc.landing.well_done_header_1566", p1)
      }
    }

    public enum NewTrackCashbacks {
      /// Once your monthly target begins you can start earning points  towards your cashback through Samsung Health. As you earn points you earn more cashback. Below is an example of how you will track your cashbacks.
      public static let HeaderMessage2215: String = CommonStrings.tr("Common", "dc.new_track_cashbacks.header_message_2215")
      /// Once your monthly target begins you can start earning points through Samsung Health towards your cashback. As you earn points you earn more cashback. Below is an example of how you will track your cashbacks.
      public static let HeaderMessageStateActivated2216: String = CommonStrings.tr("Common", "dc.new_track_cashbacks.header_message_state_activated_2216")
      /// To track points earning metrics, you need to allow access to the below metrics when allowing access to your activity data:\n\n• Steps
      public static let SectionContent2217: String = CommonStrings.tr("Common", "dc.new_track_cashbacks.section_content_2217")
    }

    public enum Onboarding {
      /// Got It
      public static let GotIt131: String = CommonStrings.tr("Common", "dc.onboarding.got_it_131")
      /// Device Cashback
      public static let HeaderTitle2499: String = CommonStrings.tr("Common", "dc.onboarding.header_title_2499")
      /// Get your device from any retail store of your choice and keep the proof of payment.
      public static let SectionMessage1459: String = CommonStrings.tr("Common", "dc.onboarding.section_message_1459")
      /// Complete qualifying fitness activities to earn points towards your cashback goal.
      public static let SectionMessage1461: String = CommonStrings.tr("Common", "dc.onboarding.section_message_1461")
      /// Earn cashbacks as you earn points and work towards getting your device for free.
      public static let SectionMessage1463: String = CommonStrings.tr("Common", "dc.onboarding.section_message_1463")
      /// Get Your Device
      public static let SectionTitle1458: String = CommonStrings.tr("Common", "dc.onboarding.section_title_1458")
      /// Get Active
      public static let SectionTitle1460: String = CommonStrings.tr("Common", "dc.onboarding.section_title_1460")
      /// Get Cashbacks
      public static let SectionTitle1462: String = CommonStrings.tr("Common", "dc.onboarding.section_title_1462")
    }

    public enum ProofOfPurchase {
      /// Add
      public static let AddButton1504: String = CommonStrings.tr("Common", "dc.proof_of_purchase.add_button_1504")
      /// Add a photo of the proof you received when purchasing the device
      public static let AddProofDescription1503: String = CommonStrings.tr("Common", "dc.proof_of_purchase.add_proof_description_1503")
      /// Add Proof of Purchase
      public static let AddProofHeader1502: String = CommonStrings.tr("Common", "dc.proof_of_purchase.add_proof_header_1502")
      /// Proof of Purchase
      public static let Title1501: String = CommonStrings.tr("Common", "dc.proof_of_purchase.title_1501")
    }

    public enum QualifyingDevices {
      /// Only one device can be activated at a time. The above list is determined by our provider and may be update at any given time.
      public static let FooterMessage1471: String = CommonStrings.tr("Common", "dc.qualifying_devices.footer_message_1471")
    }

    public enum Success {
      /// Your device cashback has successfully been activated.
      public static let ActivatedDescription1524: String = CommonStrings.tr("Common", "dc.success.activated_description_1524")
      /// Activated!
      public static let ActivatedTitle1523: String = CommonStrings.tr("Common", "dc.success.activated_title_1523")
      /// Your device details have been submitted. To complete your activation you need to link your %1$@ device.
      public static func DetailsSubmittedDescription1511(_ p1: String) -> String {
        return CommonStrings.tr("Common", "dc.success.details_submitted_description_1511", p1)
      }
      /// Details Submitted
      public static let DetailsSubmittedTitle1510: String = CommonStrings.tr("Common", "dc.success.details_submitted_title_1510")
    }

    public enum TrackCashbacks {
      /// Once your monthly target begins you can start earning points towards your cashback. As you earn points you earn more cashback. Below is an example of how you will track your cashbacks.
      public static let HeaderMessage1480: String = CommonStrings.tr("Common", "dc.track_cashbacks.header_message_1480")
      /// How to Track Cashbacks
      public static let HeaderTitle2492: String = CommonStrings.tr("Common", "dc.track_cashbacks.header_title_2492")
      /// 0 - 1499
      public static let PointsLevel2480: String = CommonStrings.tr("Common", "dc.track_cashbacks.points_level_2480")
      /// 1500 - 1999
      public static let PointsLevel2481: String = CommonStrings.tr("Common", "dc.track_cashbacks.points_level_2481")
      /// 2000 - 2999
      public static let PointsLevel2482: String = CommonStrings.tr("Common", "dc.track_cashbacks.points_level_2482")
      /// 3000
      public static let PointsLevel2483: String = CommonStrings.tr("Common", "dc.track_cashbacks.points_level_2483")
      /// $0 for that month
      public static let PointsLevelAmount2517: String = CommonStrings.tr("Common", "dc.track_cashbacks.points_level_amount_2517")
      /// $5 for that month
      public static let PointsLevelAmount2518: String = CommonStrings.tr("Common", "dc.track_cashbacks.points_level_amount_2518")
      /// $10 for that month
      public static let PointsLevelAmount2519: String = CommonStrings.tr("Common", "dc.track_cashbacks.points_level_amount_2519")
      /// $20 for that month
      public static let PointsLevelAmount2520: String = CommonStrings.tr("Common", "dc.track_cashbacks.points_level_amount_2520")
      /// %1$@ Points
      public static func PointsSubtitle1482(_ p1: String) -> String {
        return CommonStrings.tr("Common", "dc.track_cashbacks.points_subtitle_1482", p1)
      }
      /// Reach %1$@ Points
      public static func PointsSubtitle1483(_ p1: String) -> String {
        return CommonStrings.tr("Common", "dc.track_cashbacks.points_subtitle_1483", p1)
      }
      /// %1$@
      public static func PointsTitle1481(_ p1: String) -> String {
        return CommonStrings.tr("Common", "dc.track_cashbacks.points_title_1481", p1)
      }
      /// To track points earning metrics, you need to allow access to the below metrics when allowing access to your activity data:\n\n• Heart Rate\n• Steps\n• Workouts\n• Active Energy
      public static let SectionContent1486: String = CommonStrings.tr("Common", "dc.track_cashbacks.section_content_1486")
      /// Points Earning Activities
      public static let SectionTitle1484: String = CommonStrings.tr("Common", "dc.track_cashbacks.section_title_1484")
      /// Access Requirements
      public static let SectionTitle1485: String = CommonStrings.tr("Common", "dc.track_cashbacks.section_title_1485")
      /// If you purchase a Garmin device worth $600 for example, you will only pay $480 after the 20% discount. The illustration below shows how cash back would work for that device.
      public static let SubHeaderMessage2497: String = CommonStrings.tr("Common", "dc.track_cashbacks.sub_header_message_2497")
      /// Cash Back Example
      public static let SubHeaderTitle2496: String = CommonStrings.tr("Common", "dc.track_cashbacks.sub_header_title_2496")
    }
  }

  public enum DetailScreen {
    /// A new cycle has begun for %1$@ and you can now earn up to another %2$@ points when submitting your verified results.
    public static func BmiPointsMessage280(_ p1: String, _ p2: String) -> String {
      return CommonStrings.tr("Common", "detail_screen.bmi_points_message_280", p1, p2)
    }
    /// Capture Result
    public static let CaptureResultTitle1074: String = CommonStrings.tr("Common", "detail_screen.capture_result_title_1074")
    /// Points are only earned for capturing your total cholesterol.
    public static let CholesterolPointsMessage279: String = CommonStrings.tr("Common", "detail_screen.cholesterol_points_message_279")
    /// You can earn another %@ points if you submit verified measurements that are in a healthy range.
    public static func EarnMorePointsMessage201(_ p1: String) -> String {
      return CommonStrings.tr("Common", "detail_screen.earn_more_points_message_201", p1)
    }
    /// A healthy glucose range is 5,50 mmol/L after not eating for at least 8 hours.
    public static let GlucoseOutHealthyRangeMessage200: String = CommonStrings.tr("Common", "detail_screen.glucose_out_healthy_range_message_200")
    /// Points are only awarded for either random or fasting glucose, not both.
    public static let NoPointsMessage349: String = CommonStrings.tr("Common", "detail_screen.no_points_message_349")
    /// Recommended Result
    public static let RecommendedResultTitle1076: String = CommonStrings.tr("Common", "detail_screen.recommended_result_title_1076")
    /// Self Submitted
    public static let SourceSelfSubmittedMessage202: String = CommonStrings.tr("Common", "detail_screen.source_self_submitted_message_202")
    /// Source
    public static let SourceTitle198: String = CommonStrings.tr("Common", "detail_screen.source_title_198")
    /// Points are not awarded for waist circumference when your body mass index is in healthy range.
    public static let WaistCircumferenceBmiMessage199: String = CommonStrings.tr("Common", "detail_screen.waist_circumference_bmi_message_199")
    /// Why is this important?
    public static let WhyIsThisImportantTitle1077: String = CommonStrings.tr("Common", "detail_screen.why_is_this_important_title_1077")
    /// You have earned the maximum amount of points for this cycle.
    public static let YouEarnedMaxPointsMessage197: String = CommonStrings.tr("Common", "detail_screen.you_earned_max_points_message_197")
  }

  public enum Device {
    /// Device Serial Number
    public static let DeviceSerialNumber2065: String = CommonStrings.tr("Common", "device.device_serial_number_2065")
    /// Invoice Number
    public static let InvoiceNumber2064: String = CommonStrings.tr("Common", "device.invoice_number_2064")
    /// Model
    public static let Model2063: String = CommonStrings.tr("Common", "device.model_2063")
    /// Purchase Amount
    public static let PurchaseAmount2066: String = CommonStrings.tr("Common", "device.purchase_amount_2066")
    /// Purchase Date
    public static let PurchaseDate2067: String = CommonStrings.tr("Common", "device.purchase_date_2067")
  }

  public enum DeviceCashbackLearnMore {
    /// How Device Cashbacks Works
    public static let GroupHeader1472: String = CommonStrings.tr("Common", "device_cashback_learn_more.group_header_1472")
    /// Follow the process below to start earning cashbacks towards your device purchase.
    public static let GroupMessage1473: String = CommonStrings.tr("Common", "device_cashback_learn_more.group_message_1473")
    /// Purchases the device of your choice from any retail store of your choice.\n\nKeep the proof of your purchase as you’ll need it as part of the reactivation process.\n\nYou have 60 days after purchasing the device to link it with Vitality.\n\nAlready have a device?\n\nIf you already have a device add more copy here about what to do.
    public static let Section1Message1475: String = CommonStrings.tr("Common", "device_cashback_learn_more.section_1_message_1475")
    /// Get a device
    public static let Section1Title1474: String = CommonStrings.tr("Common", "device_cashback_learn_more.section_1_title_1474")
    /// When activating your Device Cashback, you’ll need to supply the following:\n\n· Purchase date\n· Invoice number\n· Purchase amount\n· Device serial number\n· Model number\n· Proof of purchase\n\nYou can upgrade of activate a new device cashback benefit by following the same activation process.
    public static let Section2Message1477: String = CommonStrings.tr("Common", "device_cashback_learn_more.section_2_message_1477")
    /// Activate
    public static let Section2Title1476: String = CommonStrings.tr("Common", "device_cashback_learn_more.section_2_title_1476")
    /// Earn cashback on your chosen device every month for 24 months by completing workout activities.\n\nYour cashback will be paid to you in the month following your achieved cashback progress.
    public static let Section3Message1568: String = CommonStrings.tr("Common", "device_cashback_learn_more.section_3_message_1568")
    /// Get Cashbacks
    public static let Section3Title1462: String = CommonStrings.tr("Common", "device_cashback_learn_more.section_3_title_1462")
  }

  public enum DeviceLinking {
    /// Successfully Delinked!
    public static let SuccessfullyDelinked470: String = CommonStrings.tr("Common", "device_linking.successfully_delinked_470")
    /// Successfully Linked!
    public static let SuccessfullyLinked454: String = CommonStrings.tr("Common", "device_linking.successfully_linked_454")
  }

  public enum Dialog {
    /// Start Date
    public static let DatePickerStartDateTitle1080: String = CommonStrings.tr("Common", "dialog.date_picker_start_date_title_1080")
  }

  public enum Er {

    public enum HomeCard {
      /// View Reward
      public static let EmployerRewardButtonText2228: String = CommonStrings.tr("Common", "ER.home_card.employer_reward_button_text_2228")
      /// Earn an extra day's leave
      public static let EmployerRewardSubtitle2229: String = CommonStrings.tr("Common", "ER.home_card.employer_reward_subtitle_2229")
    }
  }

  public enum Feedback {
    /// Submit
    public static let ActionSubmit1434: String = CommonStrings.tr("Common", "feedback.action_submit_1434")
    /// Thank you, your feedback has been successfully submitted. Someone will be in contact with you in the next 5 working days.
    public static let CompletionMessage1448: String = CommonStrings.tr("Common", "feedback.completion_message_1448")
    /// Feedback submitted. You will be contacted within 5 working days.
    public static let CompletionMessage1449: String = CommonStrings.tr("Common", "feedback.completion_message_1449")
    /// Feedback Submitted
    public static let CompletionTitle1447: String = CommonStrings.tr("Common", "feedback.completion_title_1447")
    /// Email
    public static let EmailPlaceholder1444: String = CommonStrings.tr("Common", "feedback.email_placeholder_1444")
    /// Information about your device, account and this app will be automatically included in this report.
    public static let FooterMessage1439: String = CommonStrings.tr("Common", "feedback.footer_message_1439")
    /// Changing your contact details above does not affect the details set in your profile.
    public static let FooterMessage1445: String = CommonStrings.tr("Common", "feedback.footer_message_1445")
    /// It didn't answer the question I had
    public static let OptionMessage1436: String = CommonStrings.tr("Common", "feedback.option_message_1436")
    /// It made me more confused
    public static let OptionMessage1437: String = CommonStrings.tr("Common", "feedback.option_message_1437")
    /// Other
    public static let OptionMessage1438: String = CommonStrings.tr("Common", "feedback.option_message_1438")
    /// Feedback
    public static let PageTitle1433: String = CommonStrings.tr("Common", "feedback.page_title_1433")
    /// Would you like us to contact you regarding your feedback?
    public static let PromptMessage1442: String = CommonStrings.tr("Common", "feedback.prompt_message_1442")
    /// An error occurred while trying to submit your Feedback. Please try again.
    public static let PromptMessage1446: String = CommonStrings.tr("Common", "feedback.prompt_message_1446")
    /// Please provide feedback for 'Other' before continuing.
    public static let PromptMessage1451: String = CommonStrings.tr("Common", "feedback.prompt_message_1451")
    /// Provide Feedback
    public static let PromptTitle1450: String = CommonStrings.tr("Common", "feedback.prompt_title_1450")
    /// Provide feedback...
    public static let ProvideFeedbackPlaceholder1440: String = CommonStrings.tr("Common", "feedback.provide_feedback_placeholder_1440")
    /// Write your feedback
    public static let ProvideFeedbackPlaceholder1441: String = CommonStrings.tr("Common", "feedback.provide_feedback_placeholder_1441")
    /// WHY WASN'T THIS HELPFUL?
    public static let SectionHeaderTitle1435: String = CommonStrings.tr("Common", "feedback.section_header_title_1435")
    /// CONTACT DETAILS
    public static let SectionHeaderTitle1443: String = CommonStrings.tr("Common", "feedback.section_header_title_1443")

    public enum Error {
      /// Enter subject, attachment and feedback before sending.
      public static let MissingFieldsError2230: String = CommonStrings.tr("Common", "feedback.error.missing_fields_error_2230")
    }
  }

  public enum ForgotPassword {
    /// The email address you captured is not registered. Please ensure that the email you've captured is the email you chose for communication with Vitality and try again.
    public static let EmailNotRegisteredAlertMessage57: String = CommonStrings.tr("Common", "forgot_password.email_not_registered_alert_message_57")
    /// Email Not Registered
    public static let EmailNotRegisteredAlertTitle56: String = CommonStrings.tr("Common", "forgot_password.email_not_registered_alert_title_56")
    /// Forgotten your password? Enter the the email you’ve chosen to use for communication with Vitality and we’ll send you instructions to reset your password.
    public static let InstructionMessage54: String = CommonStrings.tr("Common", "forgot_password.instruction_message_54")
    /// Forgot Password
    public static let ScreenTitle52: String = CommonStrings.tr("Common", "forgot_password.screen_title_52")
    /// Validating email...
    public static let ValidatingEmailActivityIndicatorLabel55: String = CommonStrings.tr("Common", "forgot_password.validating_email_activity_indicator_label_55")

    public enum ConfirmationScreen {
      /// An email has been sent to you with instructions to reset your password
      public static let EmailSentMessage59: String = CommonStrings.tr("Common", "forgot_password.confirmation_screen.email_sent_message_59")
      /// Email sent
      public static let EmailSentMessageTitle58: String = CommonStrings.tr("Common", "forgot_password.confirmation_screen.email_sent_message_title_58")
    }
  }

  public enum Generic {
    /// Search
    public static let SearchTitle790: String = CommonStrings.tr("Common", "generic.search_title_790")

    public enum Home {
      /// Card icon
      public static let CardIconContentDescription762: String = CommonStrings.tr("Common", "generic.home.card_icon_content_description_762")
    }
  }

  public enum HealthKit {
    /// To earn points for your activity and health information, please give Vitality access to your Health data.
    public static let OnboardingContent452: String = CommonStrings.tr("Common", "health_kit.onboarding_content_452")
    /// Health Access
    public static let OnboardingHeaders451: String = CommonStrings.tr("Common", "health_kit.onboarding_headers_451")

    public enum Onboarding {
      /// Get Started
      public static let ButtonTitle453: String = CommonStrings.tr("Common", "health_kit.onboarding.button_title_453")
    }
  }

  public enum Healthpartners {
    /// Health Partners
    public static let ScreenTitle897: String = CommonStrings.tr("Common", "HealthPartners.screen_title_897")
  }

  public enum Help {
    /// Contact <font color="#ff6e00"><a href="tel:+27860 000 3813">0860 000 3813</a></font>  for more help.
    public static let DetailFooterContact2232: String = CommonStrings.tr("Common", "help.detail_footer_contact_2232")
    /// Suggestions
    public static let EmptyTitle316: String = CommonStrings.tr("Common", "help.empty_title_316")
    /// Thank you for your feedback!
    public static let FeedbackThanksMessage2233: String = CommonStrings.tr("Common", "help.feedback_thanks_message_2233")
    /// Contact Us
    public static let FooterTitle2627: String = CommonStrings.tr("Common", "help.footer_title_2627")
    /// Ask a Question
    public static let PlaceholderMessage263: String = CommonStrings.tr("Common", "help.placeholder_message_263")
    /// Related help
    public static let RelatedHelp2234: String = CommonStrings.tr("Common", "help.related_help_2234")
    /// Solutions
    public static let SolutionsTitle2235: String = CommonStrings.tr("Common", "help.solutions_title_2235")
    /// Was this helpful?
    public static let WasThisHelpful2236: String = CommonStrings.tr("Common", "help.was_this_helpful_2236")

    public enum Contextual {
      /// All Help
      public static let PromptMessage1452: String = CommonStrings.tr("Common", "help.contextual.prompt_message_1452")
    }

    public enum Detailed {
      /// No
      public static let ActionNegative1427: String = CommonStrings.tr("Common", "help.detailed.action_negative_1427")
      /// Yes
      public static let ActionPositive1426: String = CommonStrings.tr("Common", "help.detailed.action_positive_1426")
      /// Contact <font color="#ff6e00"><a href="tel:+27860 000 3813">0860 000 3813</a></font>  for more help.
      public static let FooterContact1428: String = CommonStrings.tr("Common", "help.detailed.footer_contact_1428")
      /// Help
      public static let PageTitle1423: String = CommonStrings.tr("Common", "help.detailed.page_title_1423")
      /// Was this helpful?
      public static let PromptMessage1425: String = CommonStrings.tr("Common", "help.detailed.prompt_message_1425")
      /// Thank you for your feedback!
      public static let PromptSummary1432: String = CommonStrings.tr("Common", "help.detailed.prompt_summary_1432")
      /// RELATED HELP
      public static let SectionHeaderTitle1424: String = CommonStrings.tr("Common", "help.detailed.section_header_title_1424")
    }

    public enum Landing {
      /// There are no results for "%1$@"
      public static func NoResultMessage1430(_ p1: String) -> String {
        return CommonStrings.tr("Common", "help.landing.no_result_message_1430", p1)
      }
      /// No Results
      public static let NoResultTitle1429: String = CommonStrings.tr("Common", "help.landing.no_result_title_1429")
      /// Ask a question...
      public static let SearchFieldPlaceholder1422: String = CommonStrings.tr("Common", "help.landing.search_field_placeholder_1422")
      /// Unable to find results for
      public static let UnableToFindResults1431: String = CommonStrings.tr("Common", "help.landing.unable_to_find_results_1431")
    }
  }

  public enum History {
    /// Keep track of all Vitality Health Check submissions here.
    public static let EmptyStateMessage251: String = CommonStrings.tr("Common", "history.empty_state_message_251")
    /// No History
    public static let EmptyStateTitle250: String = CommonStrings.tr("Common", "history.empty_state_title_250")
    /// Submissions
    public static let SubmissionSectionTitle203: String = CommonStrings.tr("Common", "history.submission_section_title_203")
  }

  public enum HomeCard {
    /// Confirm Smoking Status
    public static let CardActionLinkButtonName98: String = CommonStrings.tr("Common", "home_card.card_action_link_button_name_98")
    /// Completed!
    public static let CardCompletedTitle290: String = CommonStrings.tr("Common", "home_card.card_completed_title_290")
    /// Earn %@ points
    public static func CardEarnTitle292(_ p1: String) -> String {
      return CommonStrings.tr("Common", "home_card.card_earn_title_292", p1)
    }
    /// Earn %@ points
    public static func CardEarnTitleDynamic292(_ p1: String) -> String {
      return CommonStrings.tr("Common", "home_card.card_earn_title_dynamic_292", p1)
    }
    /// Earn up to %@ points
    public static func CardEarnUpToPointsMessage124(_ p1: String) -> String {
      return CommonStrings.tr("Common", "home_card.card_earn_up_to_points_message_124", p1)
    }
    /// You have earned %@ points
    public static func CardEarnedPointsMessage121(_ p1: String) -> String {
      return CommonStrings.tr("Common", "home_card.card_earned_points_message_121", p1)
    }
    /// Earn up to %@ points
    public static func CardPotentialPointsMessage97(_ p1: String) -> String {
      return CommonStrings.tr("Common", "home_card.card_potential_points_message_97", p1)
    }
    /// Samsung Device Cashback
    public static let CardSectionTitle2039: String = CommonStrings.tr("Common", "home_card.card_section_title_2039")
    /// Mental Well Being
    public static let CardSectionTitle2237: String = CommonStrings.tr("Common", "home_card.card_section_title_2237")
    /// Vitality Health Review
    public static let CardSectionTitle291: String = CommonStrings.tr("Common", "home_card.card_section_title_291")
    /// Apple Watch
    public static let CardSectionTitle364: String = CommonStrings.tr("Common", "home_card.card_section_title_364")
    /// Screenings and Vaccinations
    public static let CardSectionTitle365: String = CommonStrings.tr("Common", "home_card.card_section_title_365")
    /// Vitality Nutrition Assessment
    public static let CardSectionTitle388: String = CommonStrings.tr("Common", "home_card.card_section_title_388")
    /// Vitality Active Rewards
    public static let CardSectionTitle609: String = CommonStrings.tr("Common", "home_card.card_section_title_609")
    /// Mental Wellbeing
    public static let CardSectionTitleMwb2238: String = CommonStrings.tr("Common", "home_card.card_section_title_mwb_2238")
    /// Start Assessments
    public static let CardStartButton2239: String = CommonStrings.tr("Common", "home_card.card_start_button_2239")
    /// Start now
    public static let CardStartButton335: String = CommonStrings.tr("Common", "home_card.card_start_button_335")
    /// Vitality Health Check
    public static let CardTitle125: String = CommonStrings.tr("Common", "home_card.card_title_125")
    /// Non-smoker's Declaration
    public static let CardTitle96: String = CommonStrings.tr("Common", "home_card.card_title_96")
    /// Edit and Update
    public static let EditUpdateMessage253: String = CommonStrings.tr("Common", "home_card.edit_update_message_253")
    /// Edit and Update
    public static let EditUpdateMessageV2253: String = CommonStrings.tr("Common", "home_card.edit_update_message_v2_253")
    /// Home Cards are updating
    public static let InformCardsUpdating2240: String = CommonStrings.tr("Common", "home_card.inform_cards_updating_2240")
    /// 
    public static let InformCardsUpdating9999: String = CommonStrings.tr("Common", "home_card.inform_cards_updating_9999")
    /// %1$@ of %2$@ points earned
    public static func PointsEarnedMessage252(_ p1: String, _ p2: String) -> String {
      return CommonStrings.tr("Common", "home_card.points_earned_message_252", p1, p2)
    }

    public enum EarnInsurance {
      /// Earn Insurance Reward
      public static let CardTitle2515: String = CommonStrings.tr("Common", "home_card.earn_insurance.card_title_2515")
    }

    public enum Insurance {
      /// Your Insurance Reward
      public static let CardTitle1964: String = CommonStrings.tr("Common", "home_card.insurance.card_title_1964")
      /// View Reward
      public static let Message1092: String = CommonStrings.tr("Common", "home_card.insurance.message_1092")
      /// Insurance Premium Award
      public static let SectionTitle1963: String = CommonStrings.tr("Common", "home_card.insurance.section_title_1963")
    }

    public enum SectionsRemainingTitle2241 {
      /// 1 Section Remaining
      public static let One: String = CommonStrings.tr("Common", "home_card.sections_remaining_title_2241.one")
      /// %1$@ Sections Remaining
      public static func Other(_ p1: String) -> String {
        return CommonStrings.tr("Common", "home_card.sections_remaining_title_2241.other", p1)
      }
      /// No Section Remaining
      public static let Zero: String = CommonStrings.tr("Common", "home_card.sections_remaining_title_2241.zero")
    }

    public enum SectionsRemainingTitle9995 {
      /// 
      public static let One: String = CommonStrings.tr("Common", "home_card.sections_remaining_title_9995.one")
      /// 
      public static let Other: String = CommonStrings.tr("Common", "home_card.sections_remaining_title_9995.other")
      /// 
      public static let Zero: String = CommonStrings.tr("Common", "home_card.sections_remaining_title_9995.zero")
    }
  }

  public enum HomeV2 {

    public enum Ar {
      /// Earn spins and gift cards
      public static let ActivateRewardsProgramSubtitle2533: String = CommonStrings.tr("Common", "home_v2.ar.activate_rewards_program_subtitle_2533")
      /// Spin earned
      public static let ActivateRewardsProgramSubtitle2586: String = CommonStrings.tr("Common", "home_v2.ar.activate_rewards_program_subtitle_2586")
      /// Gift card earned
      public static let ActivateRewardsProgramSubtitle2587: String = CommonStrings.tr("Common", "home_v2.ar.activate_rewards_program_subtitle_2587")
      /// Activate your weekly physical activity target
      public static let ActivateRewardsProgramTitle2532: String = CommonStrings.tr("Common", "home_v2.ar.activate_rewards_program_title_2532")
      /// Earn rewards for your weekly physical activity
      public static let ActivateRewardsProgramTitle2588: String = CommonStrings.tr("Common", "home_v2.ar.activate_rewards_program_title_2588")
      /// %1$@ more coins to your %2$@ gift card 
      public static func ActivateRewardsProgramTitle2589(_ p1: String, _ p2: String) -> String {
        return CommonStrings.tr("Common", "home_v2.ar.activate_rewards_program_title_2589", p1, p2)
      }
      /// Choose where you’ll spend your %1$@ gift card 
      public static func ActivateRewardsProgramTitle2591(_ p1: String) -> String {
        return CommonStrings.tr("Common", "home_v2.ar.activate_rewards_program_title_2591", p1)
      }
      /// You have a %1$@ gift card to %2$@
      public static func Voucher2594(_ p1: String, _ p2: String) -> String {
        return CommonStrings.tr("Common", "home_v2.AR.voucher_2594", p1, p2)
      }
      /// You have a voucher to %1$@
      public static func Voucher2595(_ p1: String) -> String {
        return CommonStrings.tr("Common", "home_v2.AR.voucher_2595", p1)
      }
      /// Your %1$@ gift card isn't quite ready.
      public static func Voucher2597(_ p1: String) -> String {
        return CommonStrings.tr("Common", "home_v2.AR.voucher_2597", p1)
      }
      /// We'll notify you in a few hours when it is
      public static let Voucher2598: String = CommonStrings.tr("Common", "home_v2.AR.voucher_2598")

      public enum ActivateRewardsProgramTitle2590 {
        /// You did it. Now spin it!\nYou have 1 spin
        public static let One: String = CommonStrings.tr("Common", "home_v2.ar.activate_rewards_program_title_2590.one")
        /// You did it. Now spin it!\nYou have %1$@ spins
        public static func Other(_ p1: String) -> String {
          return CommonStrings.tr("Common", "home_v2.ar.activate_rewards_program_title_2590.other", p1)
        }
        /// You have no spin available
        public static let Zero: String = CommonStrings.tr("Common", "home_v2.ar.activate_rewards_program_title_2590.zero")
      }

      public enum RewardsExpiringSoonTitle2592 {
        /// Expiring soon
        public static let One: String = CommonStrings.tr("Common", "home_v2.AR.rewards_expiring_soon_title_2592.one")
        /// %1$@ spins expiring soon
        public static func Other(_ p1: String) -> String {
          return CommonStrings.tr("Common", "home_v2.AR.rewards_expiring_soon_title_2592.other", p1)
        }
        /// No spin expiring soon
        public static let Zero: String = CommonStrings.tr("Common", "home_v2.AR.rewards_expiring_soon_title_2592.zero")
      }

      public enum RewardsExpiringSoonTitle2593 {
        /// Expiring soon
        public static let One: String = CommonStrings.tr("Common", "home_v2.AR.rewards_expiring_soon_title_2593.one")
        /// %1$@ gift cards expiring soon
        public static func Other(_ p1: String) -> String {
          return CommonStrings.tr("Common", "home_v2.AR.rewards_expiring_soon_title_2593.other", p1)
        }
        /// No gift card expiring soon
        public static let Zero: String = CommonStrings.tr("Common", "home_v2.AR.rewards_expiring_soon_title_2593.zero")
      }
    }

    public enum CardCommon {
      /// Earn %1$@ points
      public static func EarnPointsSubtitle2535(_ p1: String) -> String {
        return CommonStrings.tr("Common", "home_v2.card_common.earn_points_subtitle_2535", p1)
      }
    }

    public enum InsurancePremium {
      /// Within a 12 month period
      public static let InProgressSubtitle2584: String = CommonStrings.tr("Common", "home_v2.insurance_premium.in_progress_subtitle_2584")
      /// Learn how to qualify for your insurance premium reward
      public static let InProgressTitle2583: String = CommonStrings.tr("Common", "home_v2.insurance_premium.in_progress_title_2583")
      /// View your insurance premium reward
      public static let NotStartedTitle2582: String = CommonStrings.tr("Common", "home_v2.insurance_premium.not_started_title_2582")
    }

    public enum Mwb {
      /// You completed the mental well being review
      public static let CompletedTitle2551: String = CommonStrings.tr("Common", "home_v2.mwb.completed_title_2551")
      /// Take the mental well-being review
      public static let StartTitle2541: String = CommonStrings.tr("Common", "home_v2.mwb.start_title_2541")

      public enum PartialRemainingTitle2546 {
        /// 1 section left on your mental well-being review
        public static let One: String = CommonStrings.tr("Common", "home_v2.mwb.partial_remaining_title_2546.one")
        /// %1$@ sections left on your mental well-being review
        public static func Other(_ p1: String) -> String {
          return CommonStrings.tr("Common", "home_v2.mwb.partial_remaining_title_2546.other", p1)
        }
        /// No section left on your mental well-being review
        public static let Zero: String = CommonStrings.tr("Common", "home_v2.mwb.partial_remaining_title_2546.zero")
      }
    }

    public enum Nsd {
      /// You signed a non-smoker declaration
      public static let Completed2575: String = CommonStrings.tr("Common", "home_v2.nsd.completed_2575")
      /// Declare your tobacco-free status
      public static let Notstarted2574: String = CommonStrings.tr("Common", "home_v2.nsd.notstarted_2574")
    }

    public enum Nuffieldhealthservices {
      /// Our Health, Fitness and Wellbeing partner
      public static let Notstarted2577: String = CommonStrings.tr("Common", "home_v2.nuffieldhealthservices.notstarted_2577")
    }

    public enum Ofe {
      /// Claim points
      public static let ClaimPointsSubtitle2538: String = CommonStrings.tr("Common", "home_v2.ofe.claim_points_subtitle_2538")
      /// Participate in a fitness event
      public static let ParticipateTitle2537: String = CommonStrings.tr("Common", "home_v2.ofe.participate_title_2537")
    }

    public enum Policycashback {
      /// View your insurance premium reward
      public static let Notstarted2576: String = CommonStrings.tr("Common", "home_v2.policycashback.notstarted_2576")
    }

    public enum Rewardpartners {
      /// Check out our reward partners
      public static let Notstarted2578: String = CommonStrings.tr("Common", "home_v2.rewardpartners.notstarted_2578")
    }

    public enum Snv {
      /// You completed your vaccinations and prevention screenings
      public static let CompletedTitle2549: String = CommonStrings.tr("Common", "home_v2.snv.completed_title_2549")
      /// Finish your vaccinations and prevention screenings
      public static let PartialRemainingTitle2544: String = CommonStrings.tr("Common", "home_v2.snv.partial_remaining_title_2544")
      /// Go for your vaccinations and prevention screenings
      public static let StartTitle2539: String = CommonStrings.tr("Common", "home_v2.snv.start_title_2539")
    }

    public enum Tab {
      /// Activities
      public static let Activities2482: String = CommonStrings.tr("Common", "home_v2.tab.activities_2482")
      /// Rewards
      public static let Rewards2483: String = CommonStrings.tr("Common", "home_v2.tab.rewards_2483")
    }

    public enum Vhc {
      /// You completed your biometric screening
      public static let CompletedTitle2548: String = CommonStrings.tr("Common", "home_v2.vhc.completed_title_2548")
      /// Finish your biometric screening
      public static let PartialRemainingTitle2543: String = CommonStrings.tr("Common", "home_v2.vhc.partial_remaining_title_2543")
      /// Go for a biometric screening to assess your overall health
      public static let StartTitle2536: String = CommonStrings.tr("Common", "home_v2.vhc.start_title_2536")
    }

    public enum Vhr {
      /// You completed the health review
      public static let CompletedTitle2547: String = CommonStrings.tr("Common", "home_v2.vhr.completed_title_2547")
      /// Take the health review
      public static let StartTitle2534: String = CommonStrings.tr("Common", "home_v2.vhr.start_title_2534")

      public enum PartialRemainingTitle2542 {
        /// 1 section left on your health review
        public static let One: String = CommonStrings.tr("Common", "home_v2.vhr.partial_remaining_title_2542.one")
        /// %1$@ sections left on your health review
        public static func Other(_ p1: String) -> String {
          return CommonStrings.tr("Common", "home_v2.vhr.partial_remaining_title_2542.other", p1)
        }
        /// No section left on your health review
        public static let Zero: String = CommonStrings.tr("Common", "home_v2.vhr.partial_remaining_title_2542.zero")
      }
    }

    public enum Vna {
      /// You completed your nutrition assessment
      public static let CompletedTitle2550: String = CommonStrings.tr("Common", "home_v2.vna.completed_title_2550")
      /// Complete a nutrition assessment
      public static let StartTitle2540: String = CommonStrings.tr("Common", "home_v2.vna.start_title_2540")

      public enum PartialRemainingTitle2545 {
        /// 1 section left on your nutrition assessment
        public static let One: String = CommonStrings.tr("Common", "home_v2.vna.partial_remaining_title_2545.one")
        /// %1$@ sections left on your nutrition assessment
        public static func Other(_ p1: String) -> String {
          return CommonStrings.tr("Common", "home_v2.vna.partial_remaining_title_2545.other", p1)
        }
        /// No section left on your nutrition assessment
        public static let Zero: String = CommonStrings.tr("Common", "home_v2.vna.partial_remaining_title_2545.zero")
      }
    }
  }

  public enum InsurancePremiumReward {
    /// Insurance Premium Reward
    public static let HeaderTitle2244: String = CommonStrings.tr("Common", "insurance_premium_reward.header_title_2244")
    /// Ensure the renewal of your Vitality membership and earn cashback
    public static let Subtitle2245: String = CommonStrings.tr("Common", "insurance_premium_reward.subtitle_2245")
  }

  public enum InsurancePremiumRewards {
    /// You will qualify for a percentage discount on your insurance premium based on the renewal of your insurance policy as well as an upfront payment of your yearly premium.\nThe higher your Vitality Status, the higher the percentage discount will be on your insurance premium.\nThe percentage discount you receive is dependent on your current Vitality Status and Insurance premium.\n\n <a href="http://www.vitalitygroup.com/">Learn More</a>
    public static let DataPrivacyContent2246: String = CommonStrings.tr("Common", "insurance_premium_rewards.data_privacy_content_2246")
    /// How Insurance Premium Rewards work
    public static let DataPrivacyHeader2247: String = CommonStrings.tr("Common", "insurance_premium_rewards.data_privacy_header_2247")
  }

  public enum Insurercode {
    /// The email address you captured is not registered. Please ensure that the email you've captured is the email you chose for communication with Vitality and try again
    public static let EmailAddressNotRegisteredError2248: String = CommonStrings.tr("Common", "insurercode.email_address_not_registered_error_2248")
    /// 
    public static let EmailAddressNotRegisteredError999: String = CommonStrings.tr("Common", "insurercode.email_address_not_registered_error_999")
    /// Email Address
    public static let EmailAddressPlaceholder2249: String = CommonStrings.tr("Common", "insurercode.email_address_placeholder_2249")
    /// 
    public static let EmailAddressPlaceholder999: String = CommonStrings.tr("Common", "insurercode.email_address_placeholder_999")
    /// An email has been sent to you with your Insurer Code
    public static let EmailWithInsurerCode2250: String = CommonStrings.tr("Common", "insurercode.email_with_insurer_code_2250")
    /// 
    public static let EmailWithInsurerCode999: String = CommonStrings.tr("Common", "insurercode.email_with_insurer_code_999")
    /// Email sent with Insurer Code
    public static let EmailWithInsurerCodeSnackbar2251: String = CommonStrings.tr("Common", "insurercode.email_with_insurer_code_snackbar_2251")
    /// 
    public static let EmailWithInsurerCodeSnackbar999: String = CommonStrings.tr("Common", "insurercode.email_with_insurer_code_snackbar_999")
    /// You should have received an email from you insurer with your Insurer Code.
    public static let ResendInsurerCodeFootnoteText2252: String = CommonStrings.tr("Common", "insurercode.resend_insurer_code_footnote_text_2252")
    /// 
    public static let ResendInsurerCodeFootnoteText999: String = CommonStrings.tr("Common", "insurercode.resend_insurer_code_footnote_text_999")
    /// Enter the email you've chosen to use for communication with Vitality and we'll resend your Insurer Code to you
    public static let VitalityEmail2253: String = CommonStrings.tr("Common", "insurercode.vitality_email_2253")
    /// 
    public static let VitalityEmail999: String = CommonStrings.tr("Common", "insurercode.vitality_email_999")
  }

  public enum Ir {
    /// You will qualify for an Integrated Benefit at every fifth policy anniversary, where an additional amount (% of premium) will be paid into the account value as Vitality Integrated Benefit. The percentage of the integrated benefit is dependent on the Vitality Status and the Face Amount multiple at the end of each year in the preceding 5-year period. 
    public static let LandingDescription2254: String = CommonStrings.tr("Common", "IR.landing_description_2254")
    /// 
    public static let LandingDescription9999: String = CommonStrings.tr("Common", "IR.landing_description_9999")
    /// Learn More
    public static let LandingLearnMoreButton2255: String = CommonStrings.tr("Common", "IR.landing_learn_more_button_2255")
    /// 
    public static let LandingLearnMoreButton9999: String = CommonStrings.tr("Common", "IR.landing_learn_more_button_9999")
    /// https://www.vitalitygroup.com/
    public static let LandingLearnMoreUrl2256: String = CommonStrings.tr("Common", "IR.landing_learn_more_url_2256")
    /// 
    public static let LandingLearnMoreUrl9999: String = CommonStrings.tr("Common", "IR.landing_learn_more_url_9999")
    /// How Integrated Benefit reward work
    public static let LandingTitle12257: String = CommonStrings.tr("Common", "IR.landing_title_1_2257")
    /// 
    public static let LandingTitle19999: String = CommonStrings.tr("Common", "IR.landing_title_1_9999")
  }

  public enum LandingHeader {
    /// You've earned %1$@ points! The Vitality Health Review can only be completed for points once a year.
    public static func DescriptionCompleted2258(_ p1: String) -> String {
      return CommonStrings.tr("Common", "landing_header.description_completed_2258", p1)
    }
    /// You've earned %1$@ points! The Vitality Health Review can only be completed for points once a year.
    public static func DescriptionCompleted389(_ p1: String) -> String {
      return CommonStrings.tr("Common", "landing_header.description_completed_389", p1)
    }
    /// 
    public static let DescriptionCompleted9996: String = CommonStrings.tr("Common", "landing_header.description_completed_9996")
  }

  public enum LandingScreen {
    /// Completed!
    public static let CompletedMessage327: String = CommonStrings.tr("Common", "landing_screen.completed_message_327")
    /// Completed!
    public static let CompletedMessage390: String = CommonStrings.tr("Common", "landing_screen.completed_message_390")
    /// Continue
    public static let ContinueButton306: String = CommonStrings.tr("Common", "landing_screen.continue_button_306")
    /// Continue
    public static let ContinueButton391: String = CommonStrings.tr("Common", "landing_screen.continue_button_391")
    /// Edit
    public static let EditButton307: String = CommonStrings.tr("Common", "landing_screen.edit_button_307")
    /// Edit
    public static let EditButton392: String = CommonStrings.tr("Common", "landing_screen.edit_button_392")
    /// Estimated %1$@ minutes to complete all %2$@ sections
    public static func EstimatedTimeMessage1393(_ p1: String, _ p2: String) -> String {
      return CommonStrings.tr("Common", "landing_screen.estimated_time_message_1_393", p1, p2)
    }
    /// Estimated %1$@ minutes to complete the remaining %2$@ sections
    public static func EstimatedTimeMessage2328(_ p1: String, _ p2: String) -> String {
      return CommonStrings.tr("Common", "landing_screen.estimated_time_message_2_328", p1, p2)
    }
    /// Estimated %1$@ minutes to complete the remaining %2$@ sections
    public static func EstimatedTimeMessage2394(_ p1: String, _ p2: String) -> String {
      return CommonStrings.tr("Common", "landing_screen.estimated_time_message_2_394", p1, p2)
    }
    /// Estimated %@ minutes to complete the remaining section
    public static func EstimatedTimeMessage3329(_ p1: String) -> String {
      return CommonStrings.tr("Common", "landing_screen.estimated_time_message_3_329", p1)
    }
    /// Estimated %@ minutes to complete the remaining section
    public static func EstimatedTimeMessage3395(_ p1: String) -> String {
      return CommonStrings.tr("Common", "landing_screen.estimated_time_message_3_395", p1)
    }
    /// Sections
    public static let GroupHeading300: String = CommonStrings.tr("Common", "landing_screen.group_heading_300")
    /// Sections
    public static let GroupHeading396: String = CommonStrings.tr("Common", "landing_screen.group_heading_396")
    /// Capture all the health measurements to earn %@ bonus points
    public static func HealthMeasurementAdditionalMessage247(_ p1: String) -> String {
      return CommonStrings.tr("Common", "landing_screen.health_measurement_additional_message_247", p1)
    }
    /// Be sure to have your verified results from your healthcare professional on hand when capturing your results.
    public static let HealthMeasurementMessage133: String = CommonStrings.tr("Common", "landing_screen.health_measurement_message_133")
    /// Health Measurements
    public static let HealthMeasurementsTitle132: String = CommonStrings.tr("Common", "landing_screen.health_measurements_title_132")
    /// Healthcare PDF
    public static let HealthcarePdfLabel248: String = CommonStrings.tr("Common", "landing_screen.healthcare_pdf_label_248")
    /// Only non-qualifying points measurements have been captured.
    public static let PointsMessage249: String = CommonStrings.tr("Common", "landing_screen.points_message_249")
    /// Start
    public static let StartButton305: String = CommonStrings.tr("Common", "landing_screen.start_button_305")
    /// Start
    public static let StartButton397: String = CommonStrings.tr("Common", "landing_screen.start_button_397")
  }

  public enum LearnMore {
    /// What is Blood Pressure?
    public static let BloodPressureDetailSection1Title214: String = CommonStrings.tr("Common", "learn_more.blood_pressure_detail_section_1_title_214")
    /// Blood pressure is the pressure of circulating blood on the walls of your blood vessels. Blood pressure is usually described as your systolic blood pressure over your diastolic blood pressure, for example, 140/90. Systolic blood pressure is the pressure in the arteries during a maximum of one heartbeat. Diastolic blood pressure is the pressure in the arteries between a minimum of two heartbeats. This is measured in millimetres of mercury (mmHg). An optimal blood pressure reading is less than 140/90.
    public static let BloodPressureSection1Message234: String = CommonStrings.tr("Common", "learn_more.blood_pressure_section_1_message_234")
    /// High blood pressure or hypertension (140/90 mmHg or higher) can weaken blood vessels and damage organs. Untreated high blood pressure can lead to conditions such as stroke, heart failure, or kidney failure.
    public static let BloodPressureSection2Message235: String = CommonStrings.tr("Common", "learn_more.blood_pressure_section_2_message_235")
    /// Get a healthcare professional to measure your blood pressure. Then submit the results to Vitality as part of your Vitality Health Check.\n\nYou earn %1$@ points for submitting your results, and an extra %2$@ if those results are in the healthy range.\n\nThese points are valid for each year’s membership only.
    public static func BloodPressureSection3DynamicMessage236(_ p1: String, _ p2: String) -> String {
      return CommonStrings.tr("Common", "learn_more.blood_pressure_section_3_dynamic_message_236", p1, p2)
    }
    /// Get a healthcare professional to measure your blood pressure. Then submit the results to Vitality as part of your Vitality Health Check. \n\nYou earn 1 000 points for submitting your results, and an extra 1 000 if those results are in the healthy range. \n\nThese points are valid for each year’s membership only.
    public static let BloodPressureSection3Message236: String = CommonStrings.tr("Common", "learn_more.blood_pressure_section_3_message_236")
    /// Get a healthcare professional to measure your blood pressure. Then submit the results to Vitality as part of your Vitality Health Check.\n\nYou earn %1$@ points for submitting your results, and an extra %2$@ if those results are in the healthy range.\n\nThese points are valid for each year’s membership only.
    public static func BloodPressureSection3MessageDynamic236(_ p1: String, _ p2: String) -> String {
      return CommonStrings.tr("Common", "learn_more.blood_pressure_section_3_message_dynamic_236", p1, p2)
    }
    /// How do I earn points for my Blood Pressure?
    public static let BloodPressureSection3Title221: String = CommonStrings.tr("Common", "learn_more.blood_pressure_section_3_title_221")
    /// What is Body Mass Index?
    public static let BmiDetailSection1Titile211: String = CommonStrings.tr("Common", "learn_more.bmi_detail_section_1_titile_211")
    /// Your Body Mass Index is a calculation that takes into account both your height and your weight. It is more accurate than simply considering your weight. Yet it may inaccurately indicate body composition in populations with lean muscle mass that is more than average, for example, athletes. This is because muscle is heavier than fat, and this may throw off the calculation.
    public static let BmiSection1Message225: String = CommonStrings.tr("Common", "learn_more.bmi_section_1_message_225")
    /// A Body Mass Index indicates how your weight could be considered healthy or unhealthy: 	\n •	Below 18.5: underweight\n	•	Between 18.5 and 24.9: healthy weight \n	•	Above 24.9: overweight\n	•	Above 30: obese (in normal populations)\n
    public static let BmiSection2Message226: String = CommonStrings.tr("Common", "learn_more.bmi_section_2_message_226")
    /// Get your height and weight measured at a healthcare professional. Then submit the results to Vitality for your Vitality Health Check. \n \nYou earn %1$@ points for submitting your results, and an extra %2$@ points if those results are in the healthy range. \n \nThese points are valid for each year of membership only.
    public static func BmiSection3DynamicMessage227(_ p1: String, _ p2: String) -> String {
      return CommonStrings.tr("Common", "learn_more.bmi_section_3_dynamic_message_227", p1, p2)
    }
    /// Get your height and weight measured at a healthcare professional. Then submit the results to Vitality for your Vitality Health Check. \n\nYou earn 1 000 points for submitting your results, and an extra 1 000 points if those results are in the healthy range. \n\nThese points are valid for each year of membership only.
    public static let BmiSection3Message227: String = CommonStrings.tr("Common", "learn_more.bmi_section_3_message_227")
    /// How do I earn points for my Body Mass Index?
    public static let BmiSection3Title218: String = CommonStrings.tr("Common", "learn_more.bmi_section_3_title_218")
    /// What is Cholesterol
    public static let CholesterolDetailSection1Title215: String = CommonStrings.tr("Common", "learn_more.cholesterol_detail_section_1_title_215")
    /// Cholesterol is a waxy, fat-like substance your liver produces. You need some cholesterol to form cell membranes, certain hormones, and Vitamin D. Your body absorbs cholesterol from some foods, shellfish, eggs, and dairy products. Your total cholesterol level is made up of reading the levels of HDL, LDL, and triglycerides in your blood.\n\nAlso known as "good" cholesterol, high-density lipoprotein cholesterol (HDL cholesterol) is an indicator of risk for heart attack or stroke. HDL carries cholesterol out of the bloodstream to the liver, preventing cholesterol and plaque build-up in the arteries. An HDL measure of 1.0mmol/L and above is considered healthy.\n\nAlso known as "bad" cholesterol, low-density lipoprotein cholesterol, (LDL cholesterol) is an indicator of risk for heart attack or stroke. LDL is a carrier of cholesterol in the blood. Therefore, an optimal reading of LDL cholesterol is less than 2.6mmol/L.\n\nTriglycerides are the most common type of fat in your body and are found in your blood and in fat tissue. A major source of energy, they form from extra calories your body does not use immediately for energy and are stored in fat cells for later needs.
    public static let CholesterolSection1Message237: String = CommonStrings.tr("Common", "learn_more.cholesterol_section_1_message_237")
    /// Too much cholesterol can be harmful as it may increase one’s risk of developing heart disease and stroke. It leads to a build-up of fatty deposits (plaque) in the blood vessels that then blocks blood flowing to the heart, brain, and other important organs. An optimal total cholesterol reading is less than 5.2mmol/L.\n\nHigh levels of LDL cholesterol combine with other substances to form the waxy plaque deposits that can eventually clog arteries leading to the heart and brain, not giving them oxygen. This could then lead to a heart attack or stroke.\n\nIn normal amounts (below 1.7mmol/L), triglycerides are important for good health. High levels of triglycerides in the bloodstream (1.7mmol/L and higher) have been linked to atherosclerosis (the hardening and narrowing of arteries), high blood pressure, high blood sugar, and increased waist circumference.
    public static let CholesterolSection2Message238: String = CommonStrings.tr("Common", "learn_more.cholesterol_section_2_message_238")
    /// Get a healthcare professional to measure your cholesterol levels. Then submit the results to Vitality as part of your Vitality Health Check.  \n\nYou earn %1$@ points for submitting your results, and an extra %2$@ if those results are in the healthy range.  \n\nThese points are valid for each year’s membership only.
    public static func CholesterolSection3DynamicMessage239(_ p1: String, _ p2: String) -> String {
      return CommonStrings.tr("Common", "learn_more.cholesterol_section_3_dynamic_message_239", p1, p2)
    }
    /// Get a healthcare professional to measure your cholesterol levels. Then submit the results to Vitality as part of your Vitality Health Check. \n\nYou earn 1 000 points for submitting your results, and an extra 1 000 if those results are in the healthy range. \n\nThese points are valid for each year’s membership only.
    public static let CholesterolSection3Message239: String = CommonStrings.tr("Common", "learn_more.cholesterol_section_3_message_239")
    /// Get a healthcare professional to measure your cholesterol levels. Then submit the results to Vitality as part of your Vitality Health Check. \n\nYou earn %1$@ points for submitting your results, and an extra %2$@ if those results are in the healthy range. \n\nThese points are valid for each year’s membership only.
    public static func CholesterolSection3MessageDynamic239(_ p1: String, _ p2: String) -> String {
      return CommonStrings.tr("Common", "learn_more.cholesterol_section_3_message_dynamic_239", p1, p2)
    }
    /// How do I earn points for my Cholesterol?
    public static let CholesterolSection3Title222: String = CommonStrings.tr("Common", "learn_more.cholesterol_section_3_title_222")
    /// What is Glucose?
    public static let GlucoseDetailSection1Title213: String = CommonStrings.tr("Common", "learn_more.glucose_detail_section_1_title_213")
    /// Glucose, commonly called sugar, is an important energy source all the cells and organs of our bodies need. \n\nThe random blood glucose test: This blood test measures the level of blood sugar (also called blood glucose in your blood). A random glucose reading of less than 7.8mmol/L is optimal.\n\nThe fasting blood glucose level test: This blood test measures the level of blood sugar in your plasma. It’s also called a fasting plasma glucose (FPG) test or a fasting blood sugar test. A fasting glucose reading of less than 6.1mmol/L is optimal.
    public static let GlucoseSection1Message231: String = CommonStrings.tr("Common", "learn_more.glucose_section_1_message_231")
    /// The fasting blood glucose level test detects if you have diabetes or if you have pre-diabetes. Pre-diabetes is a condition in which blood glucose levels are higher than normal, but not high enough to be classified as full-blown diabetes. If you have pre-diabetes, you’re at high risk of developing type 2 diabetes. You are also at increased risk of developing heart disease, unless you adopt a healthier lifestyle that includes weight loss and more physical activity.
    public static let GlucoseSection2Message232: String = CommonStrings.tr("Common", "learn_more.glucose_section_2_message_232")
    /// Get a healthcare professional to measure your fasting glucose or HbA1c. Then submit the results to Vitality as part of your Vitality Health Check.   \n\nYou earn %1$@ points for submitting your results, and an extra %2$@ if those results are in the healthy range. \n\n  These points are valid for each year’s membership only.
    public static func GlucoseSection3DynamicMessage233(_ p1: String, _ p2: String) -> String {
      return CommonStrings.tr("Common", "learn_more.glucose_section_3_dynamic_message_233", p1, p2)
    }
    /// Get a healthcare professional to measure your glucose. Do both a random blood glucose test and a fasting blood glucose level test. Then submit the results to Vitality as part of your Vitality Health Check. \n\nYou earn 1 000 points for submitting your results, and an extra 1 000 if those results are in the healthy range. \n\nThese points are valid for each year’s membership only.
    public static let GlucoseSection3Message233: String = CommonStrings.tr("Common", "learn_more.glucose_section_3_message_233")
    /// Get a healthcare professional to measure your glucose. Do both a random blood glucose test and a fasting blood glucose level test. Then submit the results to Vitality as part of your Vitality Health Check. \n\nYou earn %1$@ points for submitting your results, and an extra %2$@ if those results are in the healthy range. \n\nThese points are valid for each year’s membership only.
    public static func GlucoseSection3MessageDynamic233(_ p1: String, _ p2: String) -> String {
      return CommonStrings.tr("Common", "learn_more.glucose_section_3_message_dynamic_233", p1, p2)
    }
    /// How do I earn points for my Glucose?
    public static let GlucoseSection3Title220: String = CommonStrings.tr("Common", "learn_more.glucose_section_3_title_220")
    /// Confirm if you’re a non-smoker or a smoker and earn points towards your Vitality status.
    public static let GroupMessage105: String = CommonStrings.tr("Common", "learn_more.group_message_105")
    /// What is HbA1c?
    public static let Hba1cDetailSection1Title216: String = CommonStrings.tr("Common", "learn_more.hba1c_detail_section_1_title_216")
    /// Content on what HbA1c is. (required)
    public static let Hba1cSection1Message240: String = CommonStrings.tr("Common", "learn_more.hba1c_section_1_message_240")
    /// Content on why HbA1c is important. (required)
    public static let Hba1cSection2Message241: String = CommonStrings.tr("Common", "learn_more.hba1c_section_2_message_241")
    /// Get a healthcare professional to measure your glucose or HbA1c. Then submit the results to Vitality as part of your Vitality Health Check. \n\nYou earn %1$@ points for submitting your results, and an extra %2$@ if those results are in the healthy range. \n\nThese points are valid for each year’s membership only.
    public static func Hba1cSection3DynamicMessage242(_ p1: String, _ p2: String) -> String {
      return CommonStrings.tr("Common", "learn_more.hba1c_section_3_dynamic_message_242", p1, p2)
    }
    /// Content on how you earn points for HbA1c. (required)
    public static let Hba1cSection3Message242: String = CommonStrings.tr("Common", "learn_more.hba1c_section_3_message_242")
    /// Content on how you earn points for HbA1c. (required)
    public static let Hba1cSection3MessageDynamic242: String = CommonStrings.tr("Common", "learn_more.hba1c_section_3_message_dynamic_242")
    /// How do I earn points for my HbA1c?
    public static let Hba1cSection3Title223: String = CommonStrings.tr("Common", "learn_more.hba1c_section_3_title_223")
    /// Complete the Vitality Health Check to earn points, assess your health and improve your Vitality Age.
    public static let Heading1Message205: String = CommonStrings.tr("Common", "learn_more.heading_1_message_205")
    /// Vitality Health Review consists of basic questions and guidance about your general health.
    public static let Heading1Message309: String = CommonStrings.tr("Common", "learn_more.heading_1_message_309")
    /// Vitality Health Review consists of basic questions and guidance about your general health.
    public static let Heading1Message399: String = CommonStrings.tr("Common", "learn_more.heading_1_message_399")
    /// How Vitality Health Check works
    public static let Heading1Title204: String = CommonStrings.tr("Common", "learn_more.heading_1_title_204")
    /// What is the Vitality Health Review?
    public static let Heading1Title308: String = CommonStrings.tr("Common", "learn_more.heading_1_title_308")
    /// What is the Vitality Health Review?
    public static let Heading1Title400: String = CommonStrings.tr("Common", "learn_more.heading_1_title_400")
    /// Why is it important?
    public static let ImportantSection2Title217: String = CommonStrings.tr("Common", "learn_more.important_section_2_title_217")
    /// Cholesterol is a waxy, fat-like substance your liver produces. You need some cholesterol to form cell membranes, certain hormones, and Vitamin D. Your body absorbs cholesterol from some foods, shellfish, eggs, and dairy products. Your total cholesterol level is made up of reading the levels of HDL, LDL, and triglycerides in your blood.\n\n \n\nAlso known as "good" cholesterol, high-density lipoprotein cholesterol (HDL cholesterol) is an indicator of risk for heart attack or stroke. HDL carries cholesterol out of the bloodstream to the liver, preventing cholesterol and plaque build-up in the arteries. An HDL measure of above 1.0mmol/L is considered healthy for men, and above 1.3mmol/L is considered healthy for women. When looked at in conjunction with total cholesterol, a ratio of Total Cholesterol:HDL below 5.0 is considered healthy for men; women should aim for a ratio below 3.9.\n\n \n\nAlso known as "bad" cholesterol, low-density lipoprotein cholesterol, (LDL cholesterol) is an indicator of risk for heart attack or stroke. LDL is a carrier of cholesterol in the blood. Therefore, an optimal reading of LDL cholesterol is less than 3.0mmol/L.\n\n \n\nTriglycerides are the most common type of fat in your body and are found in your blood and in fat tissue. A major source of energy, they form from extra calories your body does not use immediately for energy and are stored in fat cells for later needs.
    public static let LipidProfileSection1Content1455: String = CommonStrings.tr("Common", "learn_more.lipid_profile_section1_content_1455")
    /// What are Other Blood Lipids?
    public static let LipidProfileSection1Title1454: String = CommonStrings.tr("Common", "learn_more.lipid_profile_section1_title_1454")
    /// Too much cholesterol can be harmful as it may increase your risk of developing heart disease and stroke. It leads to a build-up of fatty deposits (plaque) in the blood vessels that then blocks blood flowing to the heart, brain, and other important organs. An optimal total cholesterol reading is less than 5.0mmol/L.\n\n \n\nHigh levels of LDL cholesterol combine with other substances to form the waxy plaque deposits that can eventually clog arteries leading to the heart and brain, not giving them oxygen. This could then lead to a heart attack or stroke.\n\n \n\nIn normal amounts (below 1.7mmol/L), triglycerides are important for good health. High levels of triglycerides in the bloodstream (1.7mmol/L and higher) have been linked to atherosclerosis (the hardening and narrowing of arteries), high blood pressure, high blood sugar, and increased waist circumference.
    public static let LipidProfileSection2Content1456: String = CommonStrings.tr("Common", "learn_more.lipid_profile_section2_content_1456")
    /// Why is it important?
    public static let LipidProfileSection2Title217: String = CommonStrings.tr("Common", "learn_more.lipid_profile_section2_title_217")
    /// Get a healthcare professional to measure your cholesterol levels. Then submit the results as part of your Vitality Healthcheck.\n\n \n\nYou earn 60 points for submitting your results, and an extra 60 if your ratio of Total Cholesterol:HDL is in the healthy range (below 5.0 for men, below 3.0 for women).\n\n \n\nThese points are valid for each year of membership only.
    public static let LipidProfileSection3Content1458: String = CommonStrings.tr("Common", "learn_more.lipid_profile_section3_content_1458")
    /// How do I earn points for Other Blood Lipids?
    public static let LipidProfileSection3Title1457: String = CommonStrings.tr("Common", "learn_more.lipid_profile_section3_title_1457")
    /// Download the healthcare PDF and get your Body Mass Index, glucose, cholesterol and blood pressure checked by a healthcare professional.
    public static let Section1AlternativeMessage245: String = CommonStrings.tr("Common", "learn_more.section_1_alternative_message_245")
    /// By confirming your smoking status, you agree to take a clinical test if Vitality asks for it. If your test result shows you’ve been smoking, you may risk losing or altering your policy with your insurer.
    public static let Section1Message107: String = CommonStrings.tr("Common", "learn_more.section_1_message_107")
    /// Get your verified health measurements for Body Mass Index, Glucose, Blood Pressure and Cholesterol.
    public static let Section1Message207: String = CommonStrings.tr("Common", "learn_more.section_1_message_207")
    /// Earn 4 000 points towards your status for completing all sections in the Vitality Health Review.
    public static let Section1Message311: String = CommonStrings.tr("Common", "learn_more.section_1_message_311")
    /// Earn 4 000 points towards your status for completing all sections in the Vitality Health Review.
    public static let Section1Message401: String = CommonStrings.tr("Common", "learn_more.section_1_message_401")
    /// Nicotine testing
    public static let Section1Title106: String = CommonStrings.tr("Common", "learn_more.section_1_title_106")
    /// Visit a Healthcare Professional
    public static let Section1Title206: String = CommonStrings.tr("Common", "learn_more.section_1_title_206")
    /// Earn Points
    public static let Section1Title310: String = CommonStrings.tr("Common", "learn_more.section_1_title_310")
    /// Earn Points
    public static let Section1Title402: String = CommonStrings.tr("Common", "learn_more.section_1_title_402")
    /// You can still sign the non-smoker declaration as long as you haven’t smoked any tobacco or e-cigarettes occasionally or regularly over the previous 6 months
    public static let Section2Message109: String = CommonStrings.tr("Common", "learn_more.section_2_message_109")
    /// Enter your verified results from a healthcare professional manually or have them updated automatically by linking an approved device.
    public static let Section2Message209: String = CommonStrings.tr("Common", "learn_more.section_2_message_209")
    /// Your Vitality Age is a measure of how healthy you are relative to your actual age. It measures all aspects of your wellness like exercise, nutrition and stress.
    public static let Section2Message313: String = CommonStrings.tr("Common", "learn_more.section_2_message_313")
    /// Your Vitality Age is a measure of how healthy you are relative to your actual age. It measures all aspects of your wellness like exercise, nutrition and stress.
    public static let Section2Message403: String = CommonStrings.tr("Common", "learn_more.section_2_message_403")
    /// Ex-smokers are now non-smokers
    public static let Section2Title108: String = CommonStrings.tr("Common", "learn_more.section_2_title_108")
    /// Complete the Vitality Health Check
    public static let Section2Title208: String = CommonStrings.tr("Common", "learn_more.section_2_title_208")
    /// Vitality Age
    public static let Section2Title312: String = CommonStrings.tr("Common", "learn_more.section_2_title_312")
    /// Vitality Age
    public static let Section2Title404: String = CommonStrings.tr("Common", "learn_more.section_2_title_404")
    /// Earn points for completing your Vitality Health Check. Bonus points are awarded for completing all your health measurements.
    public static let Section3AlternativeMessage246: String = CommonStrings.tr("Common", "learn_more.section_3_alternative_message_246")
    /// Sign up with our stop-smoking partner to help you quit your smoking habit at discounted rates
    public static let Section3Message111: String = CommonStrings.tr("Common", "learn_more.section_3_message_111")
    /// Earn points upon completion all health measurements and earn bonus points for having test results in the healthy range.
    public static let Section3Message210: String = CommonStrings.tr("Common", "learn_more.section_3_message_210")
    /// Each section has been timed to help you do help you complete small sections when it suits you best. The entire Vitality Health Review should take you an estimated 25 minutes to complete.
    public static let Section3Message315: String = CommonStrings.tr("Common", "learn_more.section_3_message_315")
    /// Each section has been timed to help you do help you complete small sections when it suits you best. The entire Vitality Health Review should take you an estimated 25 minutes to complete.
    public static let Section3Message405: String = CommonStrings.tr("Common", "learn_more.section_3_message_405")
    /// Are you a smoker?
    public static let Section3Title110: String = CommonStrings.tr("Common", "learn_more.section_3_title_110")
    /// Time to Complete
    public static let Section3Title314: String = CommonStrings.tr("Common", "learn_more.section_3_title_314")
    /// Time to Complete
    public static let Section3Title406: String = CommonStrings.tr("Common", "learn_more.section_3_title_406")
    /// Partners that can help
    public static let TitlePartners350: String = CommonStrings.tr("Common", "learn_more.title_partners_350")
    /// Content on what Urine Protein is.
    public static let UrineProteinSection1Message345: String = CommonStrings.tr("Common", "learn_more.urine_protein_section_1_message_345")
    /// What is Urine Protein?
    public static let UrineProteinSection1Title344: String = CommonStrings.tr("Common", "learn_more.urine_protein_section_1_title_344")
    /// Content on why Urine Protein is important.
    public static let UrineProteinSection2Message346: String = CommonStrings.tr("Common", "learn_more.urine_protein_section_2_message_346")
    /// Get a healthcare professional to measure your urine protein. Then submit the results to Vitality as part of your Vitality Health Check. \n\nYou earn %1$@ points for submitting your results, and an extra %2$@ if those results are in the healthy range. \n\nThese points are valid for each year’s membership only.
    public static func UrineProteinSection3DynamicMessage348(_ p1: String, _ p2: String) -> String {
      return CommonStrings.tr("Common", "learn_more.urine_protein_section_3_dynamic_message_348", p1, p2)
    }
    /// Content on how you earn points for Urine Protein.
    public static let UrineProteinSection3Message348: String = CommonStrings.tr("Common", "learn_more.urine_protein_section_3_message_348")
    /// Content on how you earn points for Urine Protein.
    public static let UrineProteinSection3MessageDynamic348: String = CommonStrings.tr("Common", "learn_more.urine_protein_section_3_message_dynamic_348")
    /// How do I earn points for my Urine Protein?
    public static let UrineProteinSection3Title347: String = CommonStrings.tr("Common", "learn_more.urine_protein_section_3_title_347")
    /// Waist measurement is a measure of the circumference or the body at the narrowest part of the torso above the navel and below the lowest portion of the ribs or chest bone.\n\nOptimal waist measurements are:\n	• 	Women: Less than 80 cm or 31.5 inches\n	• 	Men: Less than 90 cm or 35.4 inches. 
    public static let WaistCicumferneceSection1Message228: String = CommonStrings.tr("Common", "learn_more.waist_cicumfernece_section_1_message_228")
    /// Excessive abdominal fat may be serious because it places you at greater risk for developing obesity-related conditions, such as type 2 diabetes, high blood pressure, and coronary artery disease. \n\nTo measure your waist circumference:\n	• 	Stand and remove any heavy outer clothing such as jackets, jumpers or sweaters. \n	• 	If the narrowest part of the waist level is not visible, you can take the measurement at the midpoint between the lowest rib and the top of the hipbone.\n	• 	If someone else does the measurement, they should stand beside you.
    public static let WaistCicumferneceSection2Message229: String = CommonStrings.tr("Common", "learn_more.waist_cicumfernece_section_2_message_229")
    /// Get a healthcare professional to measure your waist. Then submit the results to Vitality as part of your Vitality Health Check. \n\nYou earn 1 000 points for submitting your results, and an extra 1 000 if those results are in the healthy range. \n\nThese points are valid for each year’s membership only.
    public static let WaistCicumferneceSection3Message230: String = CommonStrings.tr("Common", "learn_more.waist_cicumfernece_section_3_message_230")
    /// Get a healthcare professional to measure your waist. Then submit the results to Vitality as part of your Vitality Health Check.\n\nYou earn %1$@ points for submitting your results, and an extra %2$@ if those results are in the healthy range.\n\nThese points are valid for each year’s membership only.
    public static func WaistCicumferneceSection3MessageDynamic230(_ p1: String, _ p2: String) -> String {
      return CommonStrings.tr("Common", "learn_more.waist_cicumfernece_section_3_message_dynamic_230", p1, p2)
    }
    /// Get a healthcare professional to measure your waist. Then submit the results to Vitality as part of your Vitality Health Check.\n\nYou earn %1$@ points for submitting your results, and an extra %2$@ if those results are in the healthy range.\n\nThese points are valid for each year’s membership only.
    public static func WaistCircumferenceSection3DynamicMessage230(_ p1: String, _ p2: String) -> String {
      return CommonStrings.tr("Common", "learn_more.waist_circumference_section_3_dynamic_message_230", p1, p2)
    }
    /// How do I earn points for my Waist Circumference?
    public static let WaistCircumfereneceSection3Title219: String = CommonStrings.tr("Common", "learn_more.waist_circumferenece_section_3_title_219")
    /// What is Waist Circumference?
    public static let WaistCircumferneceDetailSection1Title212: String = CommonStrings.tr("Common", "learn_more.waist_circumfernece_detail_section_1_title_212")
  }

  public enum Login {
    /// Account Locked
    public static let AccountLockedAlertTitle737: String = CommonStrings.tr("Common", "login.account_locked_alert_title_737")
    /// Your account has been locked.
    public static let AccountLockedErrorAlertMessage738: String = CommonStrings.tr("Common", "login.account_locked_error_alert_message_738")
    /// Email field missing
    public static let EmailFieldMissing224: String = CommonStrings.tr("Common", "login.email_field_missing_224")
    /// Register
    public static let EmailNotRegisteredErrorAlertButton340: String = CommonStrings.tr("Common", "login.email_not_registered_error_alert_button_340")
    /// The email address you entered is not registered. Please register or enter a registered email address to continue.
    public static let EmailNotRegisteredErrorAlertMessage339: String = CommonStrings.tr("Common", "login.email_not_registered_error_alert_message_339")
    /// Email Address Not Registered
    public static let EmailNotRegisteredErrorAlertTitle338: String = CommonStrings.tr("Common", "login.email_not_registered_error_alert_title_338")
    /// An error occurred while trying to log in. Please try again.
    public static let ErrorGenericMessage2259: String = CommonStrings.tr("Common", "login.error_generic_message_2259")
    /// 
    public static let ErrorGenericMessage9999: String = CommonStrings.tr("Common", "login.error_generic_message_9999")
    /// Use fingerprint in the future 
    public static let FingerprintFuturePrompt2260: String = CommonStrings.tr("Common", "login.fingerprint_future_prompt_2260")
    /// 
    public static let FingerprintFuturePrompt9999: String = CommonStrings.tr("Common", "login.fingerprint_future_prompt_9999")
    /// Forgot Password
    public static let ForgotPasswordButtonTitle22: String = CommonStrings.tr("Common", "login.forgot_password_button_title_22")
    /// An incorrect email or password has been entered. Please enter your details and try again.
    public static let IncorrectEmailPasswordErrorMessage48: String = CommonStrings.tr("Common", "login.incorrect_email_password_error_message_48")
    /// Unable to login
    public static let IncorrectEmailPasswordErrorTitle47: String = CommonStrings.tr("Common", "login.incorrect_email_password_error_title_47")
    /// To enjoy the benefits of Manulife Vitality Program you must first activate it on the Manulife website, visit www.manulife.ca/vitality on your laptop or desktop machine to do so.
    public static let LearnMoreDescription2283: String = CommonStrings.tr("Common", "login.learn_more_description_2283")
    /// Activate the Manulife Vitality Program
    public static let LearnMoreTitle2284: String = CommonStrings.tr("Common", "login.learn_more_title_2284")
    /// Logging in...
    public static let LoggingInButtonTitle21: String = CommonStrings.tr("Common", "login.logging_in_button_title_21")
    /// Login
    public static let LoginAlertTitle2261: String = CommonStrings.tr("Common", "login.login_alert_title_2261")
    /// 
    public static let LoginAlertTitle9999: String = CommonStrings.tr("Common", "login.login_alert_title_9999")
    /// Login
    public static let LoginButtonTitle20: String = CommonStrings.tr("Common", "login.login_button_title_20")
    /// I have activated Manulife Vitality Program
    public static let LoginButtonTitle2285: String = CommonStrings.tr("Common", "login.login_button_title_2285")
    /// Activate
    public static let LoginLearnMoreBarTitle2287: String = CommonStrings.tr("Common", "login.login_learn_more_bar_title_2287")
    /// %2$@ can only start using the features of this app once your membership is activated on  %1$@. Please contact your insurer for more information.
    public static func LoginRestrictionGeneralErrorMessage2262(_ p1: String, _ p2: String) -> String {
      return CommonStrings.tr("Common", "login.login_restriction_general_error_message_2262", p1, p2)
    }
    /// 
    public static let LoginRestrictionGeneralErrorMessage9999: String = CommonStrings.tr("Common", "login.login_restriction_general_error_message_9999")
    /// Membership Not Yet Activated
    public static let LoginRestrictionGeneralErrorMessageTitle2263: String = CommonStrings.tr("Common", "login.login_restriction_general_error_message_title_2263")
    /// 
    public static let LoginRestrictionGeneralErrorMessageTitle9999: String = CommonStrings.tr("Common", "login.login_restriction_general_error_message_title_9999")
    /// Learn About Vitality
    public static let LoginRestrictionLearnAboutVitalityLink2264: String = CommonStrings.tr("Common", "login.login_restriction_learn_about_vitality_link_2264")
    /// 
    public static let LoginRestrictionLearnAboutVitalityLink9999: String = CommonStrings.tr("Common", "login.login_restriction_learn_about_vitality_link_9999")
    /// Register
    public static let RegisterButtonTitle23: String = CommonStrings.tr("Common", "login.register_button_title_23")
    /// Reset
    public static let ResetButtonText2270: String = CommonStrings.tr("Common", "login.reset_button_text_2270")
    /// Login to Vitality app with your new password
    public static let ResetNewPasswordTitle2271: String = CommonStrings.tr("Common", "login.reset_new_password_title_2271")
    /// Reset Password
    public static let ResetPasswordButtonTitle22: String = CommonStrings.tr("Common", "login.reset_password_button_title_22")
    /// Password Successfully Reset
    public static let ResetPasswordSuccessTitle2272: String = CommonStrings.tr("Common", "login.reset_password_success_title_2272")
    /// Session has expired. Please login again.
    public static let SessionExpirationMessage2273: String = CommonStrings.tr("Common", "login.session_expiration_message_2273")
    /// 
    public static let SessionExpirationMessage9999: String = CommonStrings.tr("Common", "login.session_expiration_message_9999")
    /// Session Error
    public static let SessionExpirationTitle2274: String = CommonStrings.tr("Common", "login.session_expiration_title_2274")
    /// 
    public static let SessionExpirationTitle9999: String = CommonStrings.tr("Common", "login.session_expiration_title_9999")
    ///  For purposes of these terms and conditions "I," "you" and "your" and...
    public static let TermsConditionSection1Preview2458: String = CommonStrings.tr("Common", "login.terms_condition_section1_preview_2458")
    /// PLEASE READ THIS TERMS OF USE AGREEMENT CAREFULLY BEFORE DOWNLOADING...
    public static let TermsConditionSection2Preview2459: String = CommonStrings.tr("Common", "login.terms_condition_section2_preview_2459")
    /// Users of The Vitality Group website or application (“app”) or the Manu...
    public static let TermsConditionSection3Preview2460: String = CommonStrings.tr("Common", "login.terms_condition_section3_preview_2460")
    /// Terms of Agreement
    public static let TermsConditionsHeading2456: String = CommonStrings.tr("Common", "login.terms_conditions_heading_2456")
    /// Please agree to the following conditions in order to continue with the log in process.
    public static let TermsConditionsHeading2457: String = CommonStrings.tr("Common", "login.terms_conditions_heading_2457")
    /// Use Password
    public static let UsePasswordText2278: String = CommonStrings.tr("Common", "login.use_password_text_2278")
    /// 
    public static let UsePasswordText9999: String = CommonStrings.tr("Common", "login.use_password_text_9999")
    /// Every time you take a step to understand or improve your health, like going for a preventive screening, buying healthy foods and getting physically active, you can earn Vitality points and increase your Vitality status. You’ll enjoy a variety of rewards at each status level and the healthier you get, the higher your Vitality status.
    public static let VitalityStatusMessage850: String = CommonStrings.tr("Common", "login.vitality_status_message_850")
    /// Learn More About Vitality Status
    public static let VitalityStatusTitle849: String = CommonStrings.tr("Common", "login.vitality_status_title_849")
    /// To enjoy the Manulife Vitality Program you first need to activate it on the Manulife website
    public static let WelcomeMessage2286: String = CommonStrings.tr("Common", "login.welcome_message_2286")

    public enum AndroidFingerprint {
      /// A fingerprint has been added from this device.  To continue using fingerprint authentication, check "Use Fingerprint Authentication" and tap "Next".
      public static let FingerprintChangeMessage1141: String = CommonStrings.tr("Common", "login.android_fingerprint.fingerprint_change_message_1141")
    }

    public enum Membership {
      /// We are sorry to see you go.\n\nShould you want to re-activate your membership, you have until %1$@ before any points and rewards are removed.\nPlease contact the support centre below if you need any assistance.\n\nA membership cancellation email has been sent to you upon cancellation with any other relevant information as well as the status of, and access to, any remaining benefits and rewards.\n\nFor any further assistance or queries regarding the above, please contact our support centre.
      public static func CancellationMessage2265(_ p1: String) -> String {
        return CommonStrings.tr("Common", "login.membership.cancellation_message_2265", p1)
      }
      /// 
      public static let CancellationMessage9999: String = CommonStrings.tr("Common", "login.membership.cancellation_message_9999")
      /// Your Membership has been cancelled
      public static let CancelledNotice2266: String = CommonStrings.tr("Common", "login.membership.cancelled_notice_2266")
      /// 
      public static let CancelledNotice9999: String = CommonStrings.tr("Common", "login.membership.cancelled_notice_9999")

      public enum Support {
        /// supportemail@vitality.com
        public static let ContactEmail2267: String = CommonStrings.tr("Common", "login.membership.support.contact_email_2267")
        /// 
        public static let ContactEmail9999: String = CommonStrings.tr("Common", "login.membership.support.contact_email_9999")
        /// 0800-12-13-14
        public static let PhoneNumber2268: String = CommonStrings.tr("Common", "login.membership.support.phone_number_2268")
        /// 
        public static let PhoneNumber9999: String = CommonStrings.tr("Common", "login.membership.support.phone_number_9999")
      }
    }

    public enum RememberMe {
      /// Enter the email id
      public static let EnterEmailTitle2269: String = CommonStrings.tr("Common", "login.remember_me.enter_email_title_2269")
      /// 
      public static let EnterEmailTitle9999: String = CommonStrings.tr("Common", "login.remember_me.enter_email_title_9999")
      /// Enter the password for
      public static let EnterPasswordTitle61: String = CommonStrings.tr("Common", "login.remember_me.enter_password_title_61")
      /// The password you entered is incorrect. Please try again.
      public static let IncorrectPasswordErrorAlertMessage63: String = CommonStrings.tr("Common", "login.remember_me.incorrect_password_error_alert_message_63")
      /// Incorrect Password
      public static let IncorrectPasswordErrorAlertTitle62: String = CommonStrings.tr("Common", "login.remember_me.incorrect_password_error_alert_title_62")
      /// Not you
      public static let NotYouButtonTitle60: String = CommonStrings.tr("Common", "login.remember_me.not_you_button_title_60")
    }

    public enum Touchid {
      /// A fingerprint has been added or removed from this device. Would you like to continue using Touch ID? If yes, you'll need to enter your password to verify your account.
      public static let FingerprintChangeMessage1141: String = CommonStrings.tr("Common", "login.touchId.fingerprint_change_message_1141")
      /// Fingerprint Changes
      public static let FingerprintChangeTitle1140: String = CommonStrings.tr("Common", "login.touchId.fingerprint_change_title_1140")
      /// Use your fingerprint for faster, easier access to your account.
      public static let FingerprintPromptMessage2275: String = CommonStrings.tr("Common", "login.touchId.fingerprint_prompt_message_2275")
      /// Fingerprint login
      public static let FingerprintPromptTitle2276: String = CommonStrings.tr("Common", "login.touchId.fingerprint_prompt_title_2276")
      /// Touch sensor
      public static let FingerprintSensorArea2277: String = CommonStrings.tr("Common", "login.touchId.fingerprint_sensor_area_2277")
      /// The use of Touch ID for this app has been disabled and can be enabled again from Settings.
      public static let TouchIdDisabledMessage1143: String = CommonStrings.tr("Common", "login.touchId.touchId_disabled_message_1143")
      /// Touch ID Disabled
      public static let TouchIdDisabledTitle1142: String = CommonStrings.tr("Common", "login.touchId.touchId_disabled_title_1142")

      public enum AuthenticateTouchid {
        /// Authenticate with Touch ID
        public static let Prompt2440: String = CommonStrings.tr("Common", "login.touchId.authenticate_touchId.prompt_2440")

        public enum Prompt {
          /// 
          public static let _9999: String = CommonStrings.tr("Common", "login.touchId.authenticate_touchId.prompt.9999")
        }
      }
    }
  }

  public enum LoginRegistration {

    public enum Userprefs {
      /// Login Email
      public static let LoginEmailTitle2280: String = CommonStrings.tr("Common", "login_registration.userprefs.login_email_title_2280")
      /// 
      public static let LoginEmailTitle9999: String = CommonStrings.tr("Common", "login_registration.userprefs.login_email_title_9999")
    }
  }

  public enum Loginregistration {

    public enum Userprefs {
      /// Your current login email address is %1$@. To change this email, please contact 0860 000 3813.
      public static func LoginEmailMessage2279(_ p1: String) -> String {
        return CommonStrings.tr("Common", "LoginRegistration.UserPrefs.LoginEmailMessage_2279", p1)
      }
      /// 
      public static let LoginEmailMessage9999: String = CommonStrings.tr("Common", "LoginRegistration.UserPrefs.LoginEmailMessage_9999")
    }
  }

  public enum Measurement {
    /// Blood Pressure
    public static let BloodPressureTitle137: String = CommonStrings.tr("Common", "measurement.blood_pressure_title_137")
    /// Body Mass Index
    public static let BodyMassIndexTitle134: String = CommonStrings.tr("Common", "measurement.body_mass_index_title_134")
    /// Centimetres
    public static let CentimetresTitle254: String = CommonStrings.tr("Common", "measurement.centimetres_title_254")
    /// Cholesterol Ratio
    public static let CholesterolRatioTitle1173: String = CommonStrings.tr("Common", "measurement.cholesterol_ratio_title_1173")
    /// Cholesterol
    public static let CholesterolTitle138: String = CommonStrings.tr("Common", "measurement.cholesterol_title_138")
    /// Diastolic
    public static let DiastolicTitle156: String = CommonStrings.tr("Common", "measurement.diastolic_title_156")
    /// Fasting Glucose
    public static let FastingGlucoseTitle148: String = CommonStrings.tr("Common", "measurement.fasting_glucose_title_148")
    /// Feet
    public static let FeetTitle160: String = CommonStrings.tr("Common", "measurement.feet_title_160")
    /// Glucose
    public static let GlucoseTitle136: String = CommonStrings.tr("Common", "measurement.glucose_title_136")
    /// HbA1c
    public static let Hba1cTitle139: String = CommonStrings.tr("Common", "measurement.hba1c_title_139")
    /// HDL
    public static let HdlTitle150: String = CommonStrings.tr("Common", "measurement.hdl_title_150")
    /// Height
    public static let HeightTitle145: String = CommonStrings.tr("Common", "measurement.height_title_145")
    /// Hours
    public static let Hours565: String = CommonStrings.tr("Common", "measurement.hours_565")
    /// LDL
    public static let LdlTitle151: String = CommonStrings.tr("Common", "measurement.ldl_title_151")
    /// Lipid Ratio
    public static let LipidRatioTitle848: String = CommonStrings.tr("Common", "measurement.lipid_ratio_title_848")
    /// Metres
    public static let MetresTitle255: String = CommonStrings.tr("Common", "measurement.metres_title_255")
    /// Millimoles Per Litre
    public static let Millimoles564: String = CommonStrings.tr("Common", "measurement.millimoles_564")
    /// Minutes
    public static let Minutes566: String = CommonStrings.tr("Common", "measurement.minutes_566")
    /// Other Blood Lipids
    public static let OtherBloodLipidsTitle848: String = CommonStrings.tr("Common", "measurement.other_blood_lipids_title_848")
    /// Random Glucose
    public static let RandomGlucoseTitle147: String = CommonStrings.tr("Common", "measurement.random_glucose_title_147")
    /// Systolic
    public static let SystolicTitle157: String = CommonStrings.tr("Common", "measurement.systolic_title_157")
    /// Total Cholesterol
    public static let TotalCholesterolTitle149: String = CommonStrings.tr("Common", "measurement.total_cholesterol_title_149")
    /// Triglycerides
    public static let TriglyceridesTitle152: String = CommonStrings.tr("Common", "measurement.triglycerides_title_152")
    /// Urine Protein
    public static let UrineProteinTitle283: String = CommonStrings.tr("Common", "measurement.urine_protein_title_283")
    /// Waist Circumference
    public static let WaistCircumferenceTitle135: String = CommonStrings.tr("Common", "measurement.waist_circumference_title_135")
    /// Weight
    public static let WeightTitle146: String = CommonStrings.tr("Common", "measurement.weight_title_146")
  }

  public enum Measurment {
    /// Inches
    public static let Inches161: String = CommonStrings.tr("Common", "measurment.inches_161")
  }

  public enum Mwb {
    /// Points may not be awarded immediately.
    public static let CompletedPointsMessage2291: String = CommonStrings.tr("Common", "mwb.completed_points_message_2291")
    /// An error occurred while trying to submit your Stressor Assessment. Please try again.
    public static let ErrorPromptMessage1220: String = CommonStrings.tr("Common", "mwb.error_prompt_message_1220")
    /// Unable to Submit
    public static let ErrorPromptTitle1219: String = CommonStrings.tr("Common", "mwb.error_prompt_title_1219")
    /// Great!
    public static let GreatButtonTitle120: String = CommonStrings.tr("Common", "mwb.great_button_title_120")
    /// CONTINUE
    public static let LandingContinue1217: String = CommonStrings.tr("Common", "mwb.landing_continue_1217")
    /// START
    public static let LandingEditTitle1215: String = CommonStrings.tr("Common", "mwb.landing_edit_title_1215")
    /// EDIT
    public static let LandingEditTitle1223: String = CommonStrings.tr("Common", "mwb.landing_edit_title_1223")
    /// Estimated 25 minutes to complete all 3 assessments
    public static let LandingHeader1214: String = CommonStrings.tr("Common", "mwb.landing_header_1214")
    /// Estimated 4 minutes to complete the remaining assessment
    public static let LandingHeader1224: String = CommonStrings.tr("Common", "mwb.landing_header_1224")
    /// You've earned %1$@ points!\nMental Wellbeing assessments can only be completed for points once a year.
    public static func LandingHeader1225(_ p1: String) -> String {
      return CommonStrings.tr("Common", "mwb.landing_header_1225", p1)
    }
    /// Unlock information about your mental wellbeing when you complete the above assessments.
    public static let LandingSectionFooter1216: String = CommonStrings.tr("Common", "mwb.landing_section_footer_1216")
    /// Assessments
    public static let LandingSectionHeader1210: String = CommonStrings.tr("Common", "mwb.landing_section_header_1210")
    /// Mental wellbeing is a complex issue, with many inter-related factors. Stressors are challenges that place real or perceived demands on you. Stressors can lead to strain, which can manifest as anxiety, depression or physical symptoms such as low energy.\n\nThe Vitality Mental Wellbeing Assessments have been developed to assess the areas of the stress models described above. This will help you to assess what is causing your stress; your current level of mental wellbeing; and what social support you have to help improve your mental wellbeing.
    public static let LearnMoreHeading1Message1203: String = CommonStrings.tr("Common", "mwb.learn_more_heading_1_message_1203")
    /// How Mental Wellbeing works
    public static let LearnMoreHeading1Title1202: String = CommonStrings.tr("Common", "mwb.learn_more_heading_1_title_1202")
    /// Psychological
    public static let LearnMorePsychological1Title1211: String = CommonStrings.tr("Common", "mwb.learn_more_psychological_1_title_1211")
    /// Each assessment has been timed to help you complete small sections when it suits you best. The entire Mental Wellbeing should take you an estimated 16 minutes to complete.
    public static let LearnMoreSection1Message1205: String = CommonStrings.tr("Common", "mwb.learn_more_section_1_message_1205")
    /// Time to complete
    public static let LearnMoreSection1Title1204: String = CommonStrings.tr("Common", "mwb.learn_more_section_1_title_1204")
    /// There are 3 assessments to complete. You will get a personalised report and earn 250 Vitality points after completing each assessment.\n\nYou can complete these assessments once a year, and earn up to 750 Vitality points each membership year.
    public static let LearnMoreSection2Message1207: String = CommonStrings.tr("Common", "mwb.learn_more_section_2_message_1207")
    /// Earn points
    public static let LearnMoreSection2Title1206: String = CommonStrings.tr("Common", "mwb.learn_more_section_2_title_1206")
    /// Complete the Mental Wellbeing assessments to understand your emotional health and mental wellbeing.
    public static let LearnMoreSection3Message1209: String = CommonStrings.tr("Common", "mwb.learn_more_section_3_message_1209")
    /// Mental Wellbeing Status
    public static let LearnMoreSection3Title1208: String = CommonStrings.tr("Common", "mwb.learn_more_section_3_title_1208")
    /// Social
    public static let LearnMoreSocial1Title1213: String = CommonStrings.tr("Common", "mwb.learn_more_social_1_title_1213")
    /// Stressor
    public static let LearnMoreStressor1Title1212: String = CommonStrings.tr("Common", "mwb.learn_more_stressor_1_title_1212")
    /// There are 3 assessments consisting of questions about your emotional health and mental wellbeing.
    public static let OnboardingSection1Message1197: String = CommonStrings.tr("Common", "MWB.onboarding_section_1_message_1197")
    /// There are 3 assessments consisting of questions about your emotional health and mental wellbeing.
    public static let OnboardingSection1Message2293: String = CommonStrings.tr("Common", "mwb.onboarding_section_1_message_2293")
    /// Complete all assessments
    public static let OnboardingSection1Title1196: String = CommonStrings.tr("Common", "MWB.onboarding_section_1_title_1196")
    /// Complete all assessments
    public static let OnboardingSection1Title2294: String = CommonStrings.tr("Common", "mwb.onboarding_section_1_title_2294")
    /// Earn points by completing the assessments.
    public static let OnboardingSection2Message1199: String = CommonStrings.tr("Common", "MWB.onboarding_section_2_message_1199")
    /// Earn points by completing the assessments.
    public static let OnboardingSection2Message2295: String = CommonStrings.tr("Common", "mwb.onboarding_section_2_message_2295")
    /// Earn points
    public static let OnboardingSection2Title1198: String = CommonStrings.tr("Common", "MWB.onboarding_section_2_title_1198")
    /// Earn points
    public static let OnboardingSection2Title2296: String = CommonStrings.tr("Common", "mwb.onboarding_section_2_title_2296")
    /// Upon completing the assessments you'll receive information about your emotional health and mental wellbeing.
    public static let OnboardingSection3Message1201: String = CommonStrings.tr("Common", "MWB.onboarding_section_3_message_1201")
    /// Upon completing the assessments you'll receive information about your emotional health and mental wellbeing.
    public static let OnboardingSection3Message2297: String = CommonStrings.tr("Common", "mwb.onboarding_section_3_message_2297")
    /// Understand your Mental Wellbeing
    public static let OnboardingSection3Title1200: String = CommonStrings.tr("Common", "MWB.onboarding_section_3_title_1200")
    /// Understand your Mental Wellbeing
    public static let OnboardingSection3Title2298: String = CommonStrings.tr("Common", "MWB.onboarding_section_3_title_2298")
    /// Psychological Assessment completed!
    public static let PsychologicalAssessmentCompletedCompletionDescription2299: String = CommonStrings.tr("Common", "mwb.psychological_assessment_completed_completion_description_2299")
    /// Psychological Assessment updated!
    public static let PsychologicalAssessmentUpdatedCompletionDescription2300: String = CommonStrings.tr("Common", "mwb.psychological_assessment_updated_completion_description_2300")
    /// None of the above
    public static let QuestionnaireNoneOfAbove2301: String = CommonStrings.tr("Common", "mwb.questionnaire_none_of_above_2301")
    /// Stressor Result
    public static let ResultHeaderTitle1229: String = CommonStrings.tr("Common", "mwb.result_header_title_1229")
    /// Your Mental Wellbeing Information is now available from the 'My Health' section.
    public static let ResultMessage1231: String = CommonStrings.tr("Common", "mwb.result_message_1231")
    /// My Health Updated
    public static let ResultTitle1230: String = CommonStrings.tr("Common", "mwb.result_title_1230")
    /// An error occurred while trying to submit your Mental Well-being Assessment. Please try again.
    public static let UnableToSubmitErrorAlertMessage: String = CommonStrings.tr("Common", "mwb.unable_to_submit_error_alert_message")

    public enum Completed {
      /// The information you've captured can be seen in your Health section.
      public static let UpdatedQuestionnaireMessage2289: String = CommonStrings.tr("Common", "mwb.completed.updated_questionnaire_message_2289")
      /// %@ updated!
      public static func UpdatedQuestionnaireTitle2290(_ p1: String) -> String {
        return CommonStrings.tr("Common", "mwb.completed.updated_questionnaire_title_2290", p1)
      }
    }

    public enum Completion {
      /// The information you've captured can be seen in your Health section.
      public static let Message1227: String = CommonStrings.tr("Common", "mwb.completion.message_1227")
      /// %1$@ Assessment completed!
      public static func Title1221(_ p1: String) -> String {
        return CommonStrings.tr("Common", "mwb.completion.title_1221", p1)
      }
      /// %1$@ Assessment updated!
      public static func Title1226(_ p1: String) -> String {
        return CommonStrings.tr("Common", "mwb.completion.title_1226", p1)
      }
    }

    public enum Feedback {

      public enum Stressor {
        /// Your safety and security##Too many demands on your time##Serious accident##Life-threatening disaster
        public static let CauseHighHeader1258: String = CommonStrings.tr("Common", "mwb.feedback.stressor.cause_high_header_1258")
        /// Your job or work environment##Managing the home##Relationship with your partner
        public static let CauseLowHeader1258: String = CommonStrings.tr("Common", "mwb.feedback.stressor.cause_low_header_1258")
        /// Your social life##You or someone has experienced health problems##Your work-life balanceyour social life##you or someone has experienced health problems##your work-life balance
        public static let CauseMedHeader1258: String = CommonStrings.tr("Common", "mwb.feedback.stressor.cause_med_header_1258")
        /// There is nothing causing you high levels of stress at the moment
        public static let CauseNoStress1258: String = CommonStrings.tr("Common", "mwb.feedback.stressor.cause_no_stress_1258")
        /// Contact Number
        public static let LabelContactNumber1250: String = CommonStrings.tr("Common", "mwb.feedback.stressor.label_contact_number_1250")
        /// Email
        public static let LabelEmail1251: String = CommonStrings.tr("Common", "mwb.feedback.stressor.label_email_1251")
        /// Website
        public static let LabelWebsite1249: String = CommonStrings.tr("Common", "mwb.feedback.stressor.label_website_1249")
        /// If you are concerned about your mental wellbeing, it's a good idea to speak with your General Practitioner or another healthcare professional, or get in touch with the resources provided above.
        public static let SectionFooter1252: String = CommonStrings.tr("Common", "mwb.feedback.stressor.section_footer_1252")
        /// Stressors
        public static let SectionHeaderTitle1246: String = CommonStrings.tr("Common", "mwb.feedback.stressor.section_header_title_1246")
        /// Traumatic Events
        public static let SectionHeaderTitle1247: String = CommonStrings.tr("Common", "mwb.feedback.stressor.section_header_title_1247")
        /// Need to speak someone?
        public static let SectionHeaderTitle1248: String = CommonStrings.tr("Common", "mwb.feedback.stressor.section_header_title_1248")
      }
    }

    public enum HomeCard {
      /// %1$@ Assessments Remaining
      public static func Subtitle1218(_ p1: String) -> String {
        return CommonStrings.tr("Common", "mwb.home_card.subtitle_1218", p1)
      }
      /// %1$@ Assessment Remaining
      public static func SubtitleSingle1218(_ p1: String) -> String {
        return CommonStrings.tr("Common", "mwb.home_card.subtitle_single_1218", p1)
      }
    }

    public enum Landing {
      /// Completed!
      public static let CompletedTitle1222: String = CommonStrings.tr("Common", "mwb.landing.completed_title_1222")
      /// Your results show that you have elevated levels of stress in your life at the moment.
      public static let HeaderDescription2585: String = CommonStrings.tr("Common", "mwb.landing.header_description_2585")
      /// Assessments
      public static let SectionHeader1228: String = CommonStrings.tr("Common", "mwb.landing.section_header_1228")
    }

    public enum LearnMore {
      /// Understand your mental wellbeing 
      public static let Section3TitleUnderstand2292: String = CommonStrings.tr("Common", "mwb.learn_more.section_3_title_understand_2292")
    }

    public enum Learnmore {

      public enum Landing {
        /// It's helpful to identify and understand the sources of stress (stressors) in your life so that you can develop techniques to reduce or cope with them.\n\nThis section of the assessment will help you identify the main short- and long-term stressors in your life.
        public static let HeaderDescription1239: String = CommonStrings.tr("Common", "mwb.learnmore.landing.header_description_1239")
        /// Your results show that you have elevated levels of stress in your life at the moment. Perhaps this is normal for you or you may have recently been through some changes in your life that have increased your stress levels. You may benefit from learning some tips to manage the stressors in your life to help you cope during these times.\n\nThis Assessment will help you to identify your main short- and long-term stressors.\n\nIt's worth doing this assessment again in six months to see how you're managing the stressors in your life.
        public static let HeaderDescription1254: String = CommonStrings.tr("Common", "mwb.learnmore.landing.header_description_1254")
        /// Psychological wellbeing is not just the absence of mental illness. It's an overall state of health with high levels of positive emotions, such as happiness and energy; and low levels of negative emotions such as depression and anxiety.\n\nThis section of the assessment gives a picture of your overall mental health.
        public static let HeaderDescription1266: String = CommonStrings.tr("Common", "mwb.learnmore.landing.header_description_1266")
        /// Your overall psychological wellbeing result is in the average range. There may be some aspects that could improve.\n\nThis assessment gives you an idea of your current state of psychological wellbeing.\n\nIt's worth doing this assessment again in six months to see if there've been changes in your results.
        public static let HeaderDescription1276: String = CommonStrings.tr("Common", "mwb.learnmore.landing.header_description_1276")
        /// Social support is the physical and emotional comfort and help we get from family, friends, co-workers and others. It's also one of our best defences against the negative effects of stress.\n\nThis assessment will help you understand the strength and sources of the social support you may have, and how you can maintain and develop these sources to help manage stress.
        public static let HeaderDescription1283: String = CommonStrings.tr("Common", "mwb.learnmore.landing.header_description_1283")
        /// Your Social Support result is high. Your answers show that you have at least one strong support network and can rely on other's support. Social support is a strong tool to manage stress. Continue building and strengthening strong relationships with the people in your life.\n\nThis assessment gives you a picture of your current Social Support network.\n\nIt's worth doing this assessment again in six months to see if there've been changes in your results.
        public static let HeaderDescription1289: String = CommonStrings.tr("Common", "mwb.learnmore.landing.header_description_1289")
        /// Your Stressor results are out of date. Please retake the Stressor Assessment to update your result.\n\nThis assessment will help you to identify your main short- and long-term stressors.\n\nIt's worth doing this assessment again in six months to see how you're managing the stressors in your life.
        public static let HeaderDescription1291: String = CommonStrings.tr("Common", "mwb.learnmore.landing.header_description_1291")
        /// Your results show that you have an average level of stress in your life at the moment. It's worth doing this assessment in six months to see how you're managing the stressors in your life.\n\nThis assessment will help you to identify your main short- and long-term stressors.\n\nIt's worth doing this assessment again in six months to see how you're managing the stressors in your life.
        public static let HeaderDescription1293: String = CommonStrings.tr("Common", "mwb.learnmore.landing.header_description_1293")
        /// Your results show that you have a low level of stress in your life at the moment. This is excellent news. Remember though that challenges and stress can come and go, so it will be useful to do this assessment again in a couple of months to track stressor trend in your life.\n\nThis assessment will help you to identify your main short- and long-term stressors.\n\nIt's worth doing this assessment again in six months to see how you're managing the stressors in your life.
        public static let HeaderDescription1295: String = CommonStrings.tr("Common", "mwb.learnmore.landing.header_description_1295")
        /// Please retake the Psychological Wellbeing assessment to update your result.\n\nThis assessment gives you an idea of your current state of psychological wellbeing.\n\nIt's worth doing this assessment again in six months to see if there've been changes in your results.
        public static let HeaderDescription1297: String = CommonStrings.tr("Common", "mwb.learnmore.landing.header_description_1297")
        /// Your overall Psychological Wellbeing result can be improved. This indicates that your psychological wellbeing could benefit from some attention.\n\nThis assessment gives you an idea of your current state of psychological wellbeing.\n\nIt's worth doing this assessment again in six months to see if there've been changes in your results.
        public static let HeaderDescription1299: String = CommonStrings.tr("Common", "mwb.learnmore.landing.header_description_1299")
        /// Your overall psychological wellbeing result is high and that's a good thing.\n\nThis assessment gives you an idea of your current state of psychological wellbeing.\n\nIt's worth doing this assessment again in six months to see if there've been changes in your results.
        public static let HeaderDescription1301: String = CommonStrings.tr("Common", "mwb.learnmore.landing.header_description_1301")
        /// Your Social Support results are out of date. Please retake the Social Support Assessment to update your result.\n\nThis assessment gives you a picture of your current Social Support network.\n\nIt's worth doing this assessment again in six months to see if there've been changes in your results.
        public static let HeaderDescription1303: String = CommonStrings.tr("Common", "mwb.learnmore.landing.header_description_1303")
        /// Your Social Support result can be improved. Your answers show that you feel that you are unable to rely on other's support and you may be finding it difficult to find people to turn to when you need support.\n\nThis assessment gives you a picture of your current Social Support network.\n\nIt's worth doing this assessment again in six months to see if there've been changes in your results.
        public static let HeaderDescription1305: String = CommonStrings.tr("Common", "mwb.learnmore.landing.header_description_1305")
        /// Your Social Support result is in the average range. Your answers show that you feel your are able to rely on other's support to some degree, but that you could benefit from a stronger support system.\n\nThis assessment gives you a picture of your current Social Support network.\n\nIt's worth doing this assessment again in six months to see if there've been changes in your results.
        public static let HeaderDescription1307: String = CommonStrings.tr("Common", "mwb.learnmore.landing.header_description_1307")
        /// How the Stressor Assessment works
        public static let HeaderTitle1238: String = CommonStrings.tr("Common", "mwb.learnmore.landing.header_title_1238")
        /// Let's work together on your stressor results
        public static let HeaderTitle1253: String = CommonStrings.tr("Common", "mwb.learnmore.landing.header_title_1253")
        /// How the Psychological Assessment works
        public static let HeaderTitle1265: String = CommonStrings.tr("Common", "mwb.learnmore.landing.header_title_1265")
        /// Your Psychological results are within the average range.
        public static let HeaderTitle1275: String = CommonStrings.tr("Common", "mwb.learnmore.landing.header_title_1275")
        /// How the Social Support assessment works
        public static let HeaderTitle1282: String = CommonStrings.tr("Common", "mwb.learnmore.landing.header_title_1282")
        /// Your Social Support results suggest that you're on the right track
        public static let HeaderTitle1288: String = CommonStrings.tr("Common", "mwb.learnmore.landing.header_title_1288")
        /// Your Stressor results are out of date
        public static let HeaderTitle1290: String = CommonStrings.tr("Common", "mwb.learnmore.landing.header_title_1290")
        /// Your Stressor results are within the average range
        public static let HeaderTitle1292: String = CommonStrings.tr("Common", "mwb.learnmore.landing.header_title_1292")
        /// Your Stressor results suggest that you're on the right track
        public static let HeaderTitle1294: String = CommonStrings.tr("Common", "mwb.learnmore.landing.header_title_1294")
        /// Your Psychological results are out of date
        public static let HeaderTitle1296: String = CommonStrings.tr("Common", "mwb.learnmore.landing.header_title_1296")
        /// Let's work together on your Psychological results
        public static let HeaderTitle1298: String = CommonStrings.tr("Common", "mwb.learnmore.landing.header_title_1298")
        /// Your Psychological results suggest that you're on the right track
        public static let HeaderTitle1300: String = CommonStrings.tr("Common", "mwb.learnmore.landing.header_title_1300")
        /// Your Social Support results are out of date
        public static let HeaderTitle1302: String = CommonStrings.tr("Common", "mwb.learnmore.landing.header_title_1302")
        /// Let's work together on your Social Support results
        public static let HeaderTitle1304: String = CommonStrings.tr("Common", "mwb.learnmore.landing.header_title_1304")
        /// Your Social Support results are within the average range
        public static let HeaderTitle1306: String = CommonStrings.tr("Common", "mwb.learnmore.landing.header_title_1306")
        /// Take note of what stressors you can work on, and try using the tips provided to help guide you on your journey to improved mental wellbeing.
        public static let SectionDescription1256: String = CommonStrings.tr("Common", "mwb.learnmore.landing.section_description_1256")
        /// Your Psychological results are calculated by evaluating your responses to the questions in this assessment that focus on your mental state in the last month.
        public static let SectionDescription1267: String = CommonStrings.tr("Common", "mwb.learnmore.landing.section_description_1267")
        /// Complete the Psychological assessment to find out your result.
        public static let SectionDescription1269: String = CommonStrings.tr("Common", "mwb.learnmore.landing.section_description_1269")
        /// Once you know your Psychological results, we'll give you the tips to help guide you on your journey to improved mental wellbeing.
        public static let SectionDescription1270: String = CommonStrings.tr("Common", "mwb.learnmore.landing.section_description_1270")
        /// Your Social Support results are calculated by evaluating your responses to the questions in this assessment that focus on your social support networks.
        public static let SectionDescription1284: String = CommonStrings.tr("Common", "mwb.learnmore.landing.section_description_1284")
        /// Complete the Social assessment to find out your result.
        public static let SectionDescription1286: String = CommonStrings.tr("Common", "mwb.learnmore.landing.section_description_1286")
        /// Once you know your Social Support results, we'll give you tips to help guide you on your journey to improved mental wellbeing.
        public static let SectionDescription1287: String = CommonStrings.tr("Common", "mwb.learnmore.landing.section_description_1287")
        /// Take note of what areas you can work on, and try use the tips provided to guide you to improved mental wellbeing
        public static let SectionDescription1308: String = CommonStrings.tr("Common", "mwb.learnmore.landing.section_description_1308")
        /// Your Stressor results are calculated by evaluating your responses to the questions in this assessment that focus on factors that may influence your current stress levels.
        public static let SectionHeaderDescription1241: String = CommonStrings.tr("Common", "mwb.learnmore.landing.section_header_description_1241")
        /// Complete the Stressor Assessment to find out your result.
        public static let SectionHeaderDescription1243: String = CommonStrings.tr("Common", "mwb.learnmore.landing.section_header_description_1243")
        /// Once you get your Stressor result, we'll give you tips to help guide you on your path to improved mental wellbeing.
        public static let SectionHeaderDescription1245: String = CommonStrings.tr("Common", "mwb.learnmore.landing.section_header_description_1245")
        /// How is this calculated?
        public static let SectionHeaderTitle1240: String = CommonStrings.tr("Common", "mwb.learnmore.landing.section_header_title_1240")
        /// Find out what your Stressor results are
        public static let SectionHeaderTitle1242: String = CommonStrings.tr("Common", "mwb.learnmore.landing.section_header_title_1242")
        /// Understand your stressors
        public static let SectionHeaderTitle1244: String = CommonStrings.tr("Common", "mwb.learnmore.landing.section_header_title_1244")
        /// Next Steps
        public static let SectionTitle1255: String = CommonStrings.tr("Common", "mwb.learnmore.landing.section_title_1255")
        /// What your results suggest
        public static let SectionTitle1257: String = CommonStrings.tr("Common", "mwb.learnmore.landing.section_title_1257")
        /// Likely Causes of Stress
        public static let SectionTitle1258: String = CommonStrings.tr("Common", "mwb.learnmore.landing.section_title_1258")
        /// Tips
        public static let SectionTitle1259: String = CommonStrings.tr("Common", "mwb.learnmore.landing.section_title_1259")
        /// Find out what your Psychological results are
        public static let SectionTitle1268: String = CommonStrings.tr("Common", "mwb.learnmore.landing.section_title_1268")
        /// Positive Emotion
        public static let SectionTitle1271: String = CommonStrings.tr("Common", "mwb.learnmore.landing.section_title_1271")
        /// Energy
        public static let SectionTitle1272: String = CommonStrings.tr("Common", "mwb.learnmore.landing.section_title_1272")
        /// Negative Emotion
        public static let SectionTitle1273: String = CommonStrings.tr("Common", "mwb.learnmore.landing.section_title_1273")
        /// Anxiety
        public static let SectionTitle1274: String = CommonStrings.tr("Common", "mwb.learnmore.landing.section_title_1274")
        /// Find out what your Social results are
        public static let SectionTitle1285: String = CommonStrings.tr("Common", "mwb.learnmore.landing.section_title_1285")
      }
    }

    public enum Result {
      /// Your results show that you have a high level of stress in your life at the moment. Perhaps this is normal for you or you may have recently been through some changes in your life that have increased the stress you are under. You could benefit from learning to manage the stressors in your life and cope during these times.
      public static let CardDescription1231: String = CommonStrings.tr("Common", "mwb.result.card_description_1231")
      /// Your results show that you have a moderate level of stress in your life at the moment.
      public static let CardDescription1233: String = CommonStrings.tr("Common", "mwb.result.card_description_1233")
      /// Your results show you have a low level of stress in your life at the moment. This is great news. Remember that challenges and stress can come and go in your life, so it will be useful to take this assessment again a couple of months to track trends in your stressors.
      public static let CardDescription1235: String = CommonStrings.tr("Common", "mwb.result.card_description_1235")
      /// Please retake the Social Support assessment to update your result.
      public static let CardDescription1237: String = CommonStrings.tr("Common", "mwb.result.card_description_1237")
      /// Your overall psychological well-being result is low. This indicates that your psychological well-being could benefit from some attention.
      public static let CardDescription1261: String = CommonStrings.tr("Common", "mwb.result.card_description_1261")
      /// Your overall psychological well-being result is moderate. There may be some aspects that could stand to improve.
      public static let CardDescription1262: String = CommonStrings.tr("Common", "mwb.result.card_description_1262")
      /// Your overall psychological well-being result is high and that's a good thing.
      public static let CardDescription1263: String = CommonStrings.tr("Common", "mwb.result.card_description_1263")
      /// Please retake the Social Support assessment to update your result.
      public static let CardDescription1264: String = CommonStrings.tr("Common", "mwb.result.card_description_1264")
      /// Your social support result is low. Your answers show…
      public static let CardDescription1278: String = CommonStrings.tr("Common", "mwb.result.card_description_1278")
      /// Your Social Support result is in the average range. Your answers...
      public static let CardDescription1279: String = CommonStrings.tr("Common", "mwb.result.card_description_1279")
      /// Your social support result is high. Your answers show that you...
      public static let CardDescription1280: String = CommonStrings.tr("Common", "mwb.result.card_description_1280")
      /// Please retake the Social Support assessment to update your result.
      public static let CardDescription1281: String = CommonStrings.tr("Common", "mwb.result.card_description_1281")
      /// Stressor Feedback
      public static let CardTitle1228: String = CommonStrings.tr("Common", "mwb.result.card_title_1228")
      /// Unknown
      public static let CardTitle1229: String = CommonStrings.tr("Common", "mwb.result.card_title_1229")
      /// Let's work together
      public static let CardTitle1230: String = CommonStrings.tr("Common", "mwb.result.card_title_1230")
      /// You're getting there
      public static let CardTitle1232: String = CommonStrings.tr("Common", "mwb.result.card_title_1232")
      /// You're on the right track
      public static let CardTitle1234: String = CommonStrings.tr("Common", "mwb.result.card_title_1234")
      /// Out of date
      public static let CardTitle1236: String = CommonStrings.tr("Common", "mwb.result.card_title_1236")
      /// Psychological Feedback
      public static let CardTitle1260: String = CommonStrings.tr("Common", "mwb.result.card_title_1260")
      /// Social Feedback
      public static let CardTitle1277: String = CommonStrings.tr("Common", "mwb.result.card_title_1277")
    }
  }

  public enum MyHealth {
    /// Way to go! Your Vitality Age is %@ years lower than your actual age. This means you're at a lower risk of heart disease and other cardiovascular diseases.\n\nKeep your VHC up to date to maintain the most accurate Vitality Age.
    public static func LearnMoreGoodContent757(_ p1: String) -> String {
      return CommonStrings.tr("Common", "my_health._learn_more_good_content_757", p1)
    }
    /// Our Health, Fitness & Wellbeing partner
    public static let CardHeadingNuffieldTitle2076: String = CommonStrings.tr("Common", "my_health.card_heading_nuffield_title_2076")
    /// View Your Discounts
    public static let CardNuffieldSubtitle2078: String = CommonStrings.tr("Common", "my_health.card_nuffield_subtitle_2078")
    /// Nuffield Health
    public static let CardNuffieldTitle2077: String = CommonStrings.tr("Common", "my_health.card_nuffield_title_2077")
    /// Recent Result
    public static let DescriptionHeadingRecentResult1144: String = CommonStrings.tr("Common", "my_health.description_heading_recent_result_1144")
    /// More Tips
    public static let DetailMainTitleMoreTips1067: String = CommonStrings.tr("Common", "my_health.detail_main_title_more_tips_1067")
    /// Tip
    public static let DetailMainTitleTip1068: String = CommonStrings.tr("Common", "my_health.detail_main_title_tip_1068")
    /// Completing your Vitality Health Review will give you an indication of your Vitality Age.\n\nFor a more accurate result, complete the Vitality Health Check as well.\n\nCompleting only your Vitality Health Check will not give you your Vitality Age.
    public static let LearnMoreFindYourVitalityAgeContent749: String = CommonStrings.tr("Common", "my_health.learn_more_find_your_vitality_age_content_749")
    /// Find out what your Vitality Age is
    public static let LearnMoreFindYourVitalityAgeTitle748: String = CommonStrings.tr("Common", "my_health.learn_more_find_your_vitality_age_title_748")
    /// Your Vitality Age is looking good!
    public static let LearnMoreGoodTitle756: String = CommonStrings.tr("Common", "my_health.learn_more_good_title_756")
    /// Take note of the improvements your health needs based on the results you've provided from the Vitality Health Review and Vitality Health Check assessments.
    public static let LearnMoreHowDoILowerContent755: String = CommonStrings.tr("Common", "my_health.learn_more_how_do_I_lower_content_755")
    /// How do I lower my Vitality Age?
    public static let LearnMoreHowDoILowerTitle754: String = CommonStrings.tr("Common", "my_health.learn_more_how_do_I_lower_title_754")
    /// Keep up the good work! It's important to maintain all aspects of your health. Use our tips to guide you through your health journey.
    public static let LearnMoreHowDoIMaintainContent759: String = CommonStrings.tr("Common", "my_health.learn_more_how_do_I_maintain_content_759")
    /// How do I maintain my Vitality Age?
    public static let LearnMoreHowDoIMaintainTitle758: String = CommonStrings.tr("Common", "my_health.learn_more_how_do_I_maintain_title_758")
    /// Your Vitality Age is based on more than 10 risk factors. We reviewed over 234 studies that examined health risk in over 23,000 people to create Vitality Age.
    public static let LearnMoreHowIsThisCalculatedContent747: String = CommonStrings.tr("Common", "my_health.learn_more_how_is_this_calculated_content_747")
    /// How is this calculated?
    public static let LearnMoreHowIsThisCalculatedTitle746: String = CommonStrings.tr("Common", "my_health.learn_more_how_is_this_calculated_title_746")
    /// Your Vitality Age is an overall measure of your personal health and might not match your chronological age.\n\nLet the Vitality Age serve as a tool to motivate you to being active and participating in healthy lifestyle activities.
    public static let LearnMoreSubtitle745: String = CommonStrings.tr("Common", "my_health.learn_more_subtitle_745")
    /// What is Vitality Age?
    public static let LearnMoreTitle744: String = CommonStrings.tr("Common", "my_health.learn_more_title_744")
    /// Your Vitality Age is %@ years older than your chronological age. This means you could be at a higher risk of heart disease and other cardiovascular issues.\n\nKeep your VHC up to date to maintain the most accurate Vitality age.
    public static func LearnMoreTooHighContent753(_ p1: String) -> String {
      return CommonStrings.tr("Common", "my_health.learn_more_too_high_content_753", p1)
    }
    /// Your Vitality Age is too high
    public static let LearnMoreTooHighTitle752: String = CommonStrings.tr("Common", "my_health.learn_more_too_high_title_752")
    /// Once you know your Vitality Age, you’ll be guided through your results and receive tips to improve where necessary.
    public static let LearnMoreUnderstandYourHealthContent751: String = CommonStrings.tr("Common", "my_health.learn_more_understand_your_health_content_751")
    /// Understand your health
    public static let LearnMoreUnderstandYourHealthTitle750: String = CommonStrings.tr("Common", "my_health.learn_more_understand_your_health_title_750")
    /// Your previous Vitality Age of %@ was in good range, however it is currently outdated. You need to retake the Vitality Health Review assessment every membership year to ensure that your Vitality Age is updated and correct.
    public static func LearnMoreVitalityAgeOutdatedDescription1101(_ p1: String) -> String {
      return CommonStrings.tr("Common", "my_health.learn_more_vitality_age_outdated_description_1101", p1)
    }
    /// Your Vitality Age is outdated
    public static let LearnMoreYourVitalityAgeIsOutdated1100: String = CommonStrings.tr("Common", "my_health.learn_more_your_vitality_age_is_outdated_1100")
    /// Tips of Importance
    public static let MetricDetailTipsToImportance1123: String = CommonStrings.tr("Common", "my_health.metric_detail_tips_to_importance_1123")
    /// Tips to Improve
    public static let MetricDetailTipsToImprove1121: String = CommonStrings.tr("Common", "my_health.metric_detail_tips_to_improve_1121")
    /// Tips to Maintain
    public static let MetricDetailTipsToMaintain1122: String = CommonStrings.tr("Common", "my_health.metric_detail_tips_to_maintain_1122")
    /// More Results
    public static let MoreResultsMenuItemTitle792: String = CommonStrings.tr("Common", "my_health.more_results_menu_item_title_792")
    /// More Tips
    public static let MoreTipsMenuItemTitle791: String = CommonStrings.tr("Common", "my_health.more_tips_menu_item_title_791")
    /// 
    public static let MwbSocialRecommendations9999: String = CommonStrings.tr("Common", "my_health.mwb_social_recommendations_9999")
    /// Great! Looks like there's no improvements needed at the moment.
    public static let NoImprovementsNeeded1069: String = CommonStrings.tr("Common", "my_health.no_improvements_needed_1069")
    /// Looks like there are no results in healthy range. Make use of the tips and results in the ‘what you can improve’ section.
    public static let NoResultsInHealthyRange1070: String = CommonStrings.tr("Common", "my_health.no_results_in_healthy_range_1070")
    /// Way to go! You’ve provided all health results!
    public static let NoUnknownHealthResults1071: String = CommonStrings.tr("Common", "my_health.no_unknown_health_results_1071")
    /// Enjoy discounts and rewards to help you get healthier and stay motivated.
    public static let NuffieldHealthServicesContent2079: String = CommonStrings.tr("Common", "my_health.nuffield_health_services_content_2079")
    /// Our Exclusive Discounts
    public static let NuffieldHealthServicesSubtitle2080: String = CommonStrings.tr("Common", "my_health.nuffield_health_services_subtitle_2080")
    /// My Health is a place to view results and feedback from assessments you complete.
    public static let OnboardingSection1Description629: String = CommonStrings.tr("Common", "my_health.onboarding_section1_description_629")
    /// What is My Health?
    public static let OnboardingSection1Title628: String = CommonStrings.tr("Common", "my_health.onboarding_section1_title_628")
    /// Track, interact and monitor your health in the My Health section.
    public static let OnboardingSection2Description633: String = CommonStrings.tr("Common", "my_health.onboarding_section2_description_633")
    /// Manage your health
    public static let OnboardingSection2Title632: String = CommonStrings.tr("Common", "my_health.onboarding_section2_title_632")
    /// Get tips on improving and maintaining your health.
    public static let OnboardingSection3Description635: String = CommonStrings.tr("Common", "my_health.onboarding_section3_description_635")
    /// Health Tips
    public static let OnboardingSection3Title634: String = CommonStrings.tr("Common", "my_health.onboarding_section3_title_634")
    /// Complete the Lifestyle Habits section in the Vitality Health Review to provide information about your physical activity.
    public static let PhysicalActivityCaptureResultMessage1075: String = CommonStrings.tr("Common", "my_health.physical_activity_capture_result_message_1075")
    /// Your Vitality Age is the same as your actual age. This means you probably have a healthy lifestyle and most of your measures are under control. However, there may be a few areas where you can improve.
    public static let SameVitalityAgeAsRealAge1151: String = CommonStrings.tr("Common", "my_health.same_vitality_age_as_real_age_1151")
    /// What you can improve
    public static let TipsScreenWhatYouCanImprove640: String = CommonStrings.tr("Common", "my_health.tips_screen_what_you_can_improve_640")
    /// Too High
    public static let TooHigh1386: String = CommonStrings.tr("Common", "my_health.too_high_1386")
    /// Too Low
    public static let TooLow1387: String = CommonStrings.tr("Common", "my_health.too_low_1387")
    /// Have a look at your results to see where you can improve.
    public static let VhcCompletedIn12months1154: String = CommonStrings.tr("Common", "my_health.VHC_completed_in_12months_1154")
    /// We recommend completing a Vitality Healthcheck or Nuffield Health MOT once a year for the most accurate result.
    public static let VhcNotCompletedIn12months1153: String = CommonStrings.tr("Common", "my_health.VHC_not_completed_in_12months_1153")
    /// You've submitted results for the Vitality Health Check, now complete the Vitality Health Review to unlock your Vitality Age.
    public static let VitalityAgeAfterVhcSubmissionResultUnknownDescription616: String = CommonStrings.tr("Common", "my_health.vitality_age_after_vhc_submission_result_unknown_description_616")
    /// Result Unknown
    public static let VitalityAgeAfterVhcSubmissionResultUnknownTitle615: String = CommonStrings.tr("Common", "my_health.vitality_age_after_vhc_submission_result_unknown_title_615")
    /// Your Vitality Age is equal to your actual age. Keep up the good work and stay healthy.
    public static let VitalityAgeHealthyDescriptionLong2102: String = CommonStrings.tr("Common", "my_health.vitality_age_healthy_description_long_2102")
    /// Way to go! Your Vitality Age is the same as your actual age. You are probably practicing healthy lifestyle habits and have most measures under control.  There may be a few areas in your life where you can improve even more..
    public static let VitalityAgeHealthyDescriptionLong2103: String = CommonStrings.tr("Common", "my_health.vitality_age_healthy_description_long_2103")
    /// Your Vitality Age is the same as your actual age. You are probably practicing healthy lifestyle habits and have most measures under control.  There may be a few areas in your life where you can improve even more.
    public static let VitalityAgeHealthyDescriptionLong625: String = CommonStrings.tr("Common", "my_health.vitality_age_healthy_description_long_625")
    /// Your Vitality Age is the same as your actual age.
    public static let VitalityAgeHealthyDescriptionShort2101: String = CommonStrings.tr("Common", "my_health.vitality_age_healthy_description_short_2101")
    /// Your Vitality Age is the same as your actual age.
    public static let VitalityAgeHealthyDescriptionShort624: String = CommonStrings.tr("Common", "my_health.vitality_age_healthy_description_short_624")
    /// Your Vitality Age is %1$@ years lower than your actual age. Keep up the good work while you explore other areas you may be able to improve.
    public static func VitalityAgeLowerThanActualAge1152(_ p1: String) -> String {
      return CommonStrings.tr("Common", "my_health.vitality_age_lower_than_actual_age_1152", p1)
    }
    /// Your Vitality Age information is now available from the My Health section.
    public static let VitalityAgeMyHealthUpdatedDescription620: String = CommonStrings.tr("Common", "my_health.vitality_age_my_health_updated_description_620")
    /// My Health updated
    public static let VitalityAgeMyHealthUpdatedTitle619: String = CommonStrings.tr("Common", "my_health.vitality_age_my_health_updated_title_619")
    /// Your Vitality Age is %@ year older than your actual age. We can work with you to improve your lifestyle habits so that you can enjoy better health and wellbeing.
    public static func VitalityAgeOlderDescriptionLong622(_ p1: String) -> String {
      return CommonStrings.tr("Common", "my_health.vitality_age_older_description_long_622", p1)
    }
    /// Your Vitality Age is %@ years older than your actual age.
    public static func VitalityAgeOlderDescriptionShort621(_ p1: String) -> String {
      return CommonStrings.tr("Common", "my_health.vitality_age_older_description_short_621", p1)
    }
    /// Outdated
    public static let VitalityAgeOutdated1072: String = CommonStrings.tr("Common", "my_health.vitality_age_outdated_1072")
    /// Retake the Vitality Health Review Assessment to updated your result.
    public static let VitalityAgeOutdatedDescriptionShort1073: String = CommonStrings.tr("Common", "my_health.vitality_age_outdated_description_short_1073")
    /// Vitality Age
    public static let VitalityAgeScreenTitle613: String = CommonStrings.tr("Common", "my_health.vitality_age_screen_title_613")
    /// Unknown
    public static let VitalityAgeUnknown627: String = CommonStrings.tr("Common", "my_health.vitality_age_unknown_627")
    /// Complete the Vitality Health Review to unlock your Vitality Age. Keep your VHC up to date to maintain the most accurate Vitality Age.
    public static let VitalityAgeVhcCompletedVhrNotCompletedNotEnoughDataDescriptionLong623: String = CommonStrings.tr("Common", "my_health.vitality_age_vhc_completed_vhr_not_completed_not_enough_data_description_long_623")
    /// Complete the Vitality Health Review to unlock your Vitality Age.
    public static let VitalityAgeVhcCompletedVhrNotCompletedNotEnoughDataDescriptionShort618: String = CommonStrings.tr("Common", "my_health.vitality_age_vhc_completed_vhr_not_completed_not_enough_data_description_short_618")
    /// Not enough data
    public static let VitalityAgeVhcCompletedVhrNotCompletedNotEnoughDataTitle617: String = CommonStrings.tr("Common", "my_health.vitality_age_vhc_completed_vhr_not_completed_not_enough_data_title_617")
    /// You're one step closer
    public static let VitalityAgeVhcDoneVhrPendingTitle614: String = CommonStrings.tr("Common", "my_health.vitality_age_vhc_done_vhr_pending_title_614")
    /// Way to go! Your Vitality Age is %@ years lower than your actual age. You are probably engaged in a healthy lifestyle. Keep up the good work while you explore other areas where you may be able to improve even further.\nKeep your VHC up to date to maintain the most accurate Vitality age.
    public static func VitalityAgeYoungerDescriptionLong631(_ p1: String) -> String {
      return CommonStrings.tr("Common", "my_health.vitality_age_younger_description_long_631", p1)
    }
    /// Your Vitality Age is %@ years younger than your actual age.
    public static func VitalityAgeYoungerDescriptionShort630(_ p1: String) -> String {
      return CommonStrings.tr("Common", "my_health.vitality_age_younger_description_short_630", p1)
    }
    /// What you are doing well
    public static let WhatYouAreDoingWell1174: String = CommonStrings.tr("Common", "my_health.what_you_are_doing_well_1174")
    /// What you haven’t provided
    public static let WhatYouHaventProvided1175: String = CommonStrings.tr("Common", "my_health.what_you_havent_provided_1175")
  }

  public enum Myhealth {
    /// Check out the tips provided to help guide you to further strengthen your support system.
    public static let MwbSocialRecommendations2302: String = CommonStrings.tr("Common", "myhealth.mwb_social_recommendations_2302")
    /// Tips to help
    public static let MwbSocialTips2303: String = CommonStrings.tr("Common", "myhealth.mwb_social_tips_2303")
  }

  public enum Myrewards {

    public enum Insurance {
      /// Insurance Reward
      public static let RewardHeaderTitle2304: String = CommonStrings.tr("Common", "myrewards.insurance.reward_header_title_2304")
    }
  }

  public enum Notification {
    /// Remember to sync your Health activity before your goal cycle ends.
    public static let HealthappSyncingReminder2305: String = CommonStrings.tr("Common", "notification.healthapp_syncing_reminder_2305")
    /// 
    public static let HealthappSyncingReminder9999: String = CommonStrings.tr("Common", "notification.healthapp_syncing_reminder_9999")
    /// %@ points earned for completing your Non-smoker’s Declaration!
    public static func PointsEarnedMessage122(_ p1: String) -> String {
      return CommonStrings.tr("Common", "notification.points_earned_message_122", p1)
    }
    /// %@ points earned for capturing your Vitality Health Check Measurements!
    public static func PointsEarnedMessageAlert195(_ p1: String) -> String {
      return CommonStrings.tr("Common", "notification.points_earned_message_alert_195", p1)
    }
  }

  public enum Nsd {
    /// Get up to 80%% discount on standard rates from our participating partners.
    public static let PartnerDetailMessage1046: String = CommonStrings.tr("Common", "NSD.partner_detail_message_1046")
    /// Join a Partner
    public static let PartnerDetailTitle1045: String = CommonStrings.tr("Common", "NSD.partner_detail_title_1045")
  }

  public enum NuffieldHealthServices {
    /// Our Services
    public static let OurServices2306: String = CommonStrings.tr("Common", "nuffield_health_services.our_services_2306")
  }

  public enum Ofe {
    /// Enter a valid web link
    public static let EnterValidWeblink1381: String = CommonStrings.tr("Common", "OFE.enter_valid_weblink_1381")
    /// These details will be used to contact you if we need any clarification regarding your suggested fitness events.
    public static let Footer1360: String = CommonStrings.tr("Common", "ofe.footer_1360")
    /// E.g. "Windsurfing" or "Running"
    public static let Footer1401: String = CommonStrings.tr("Common", "ofe.footer_1401")
    /// Great!
    public static let GreatButtonTitle120: String = CommonStrings.tr("Common", "OFE.great_button_title_120")
    /// What distance did you complete?
    public static let Header1406: String = CommonStrings.tr("Common", "ofe.header_1406")

    public enum Action {
      /// Claim Points
      public static let Title1337: String = CommonStrings.tr("Common", "ofe.action.title_1337")
    }

    public enum ClaimPoints {
      /// Proof of you results are required to verify your participation in the event and to award you points.\n\nProof can be in the form of a race certificate, a screenshot of the results or a link sent to your results provided by the event.
      public static let Description1364: String = CommonStrings.tr("Common", "ofe.claim_points.description_1364")
      /// Distance
      public static let Distance1371: String = CommonStrings.tr("Common", "ofe.claim_points.distance_1371")
      /// %1$@ points
      public static func DistanceRangePoints2307(_ p1: String) -> String {
        return CommonStrings.tr("Common", "ofe.claim_points.distance_range_points_2307", p1)
      }
      /// 
      public static let DistanceRangePoints9999: String = CommonStrings.tr("Common", "ofe.claim_points.distance_range_points_9999")
      /// Enter Link
      public static let EnterLink2308: String = CommonStrings.tr("Common", "ofe.claim_points.enter_link_2308")
      /// 
      public static let EnterLink9999: String = CommonStrings.tr("Common", "ofe.claim_points.enter_link_9999")
      /// Event Date
      public static let EventDate1373: String = CommonStrings.tr("Common", "ofe.claim_points.event_date_1373")
      /// Event Details
      public static let EventDetails2309: String = CommonStrings.tr("Common", "ofe.claim_points.event_details_2309")
      /// 
      public static let EventDetails9999: String = CommonStrings.tr("Common", "ofe.claim_points.event_details_9999")
      /// Event Name
      public static let EventName1356: String = CommonStrings.tr("Common", "ofe.claim_points.event_name_1356")
      /// Finish Time
      public static let FinishTime1372: String = CommonStrings.tr("Common", "ofe.claim_points.finish_time_1372")
      /// Only submit a fitness event if you have verified proof of your participation.
      public static let Header1363: String = CommonStrings.tr("Common", "ofe.claim_points.header_1363")
      /// Event Type
      public static let Placeholder1366: String = CommonStrings.tr("Common", "ofe.claim_points.placeholder_1366")
      /// 12345678
      public static let Placeholder1405: String = CommonStrings.tr("Common", "ofe.claim_points.placeholder_1405")
      /// Race Number
      public static let RaceNumber1374: String = CommonStrings.tr("Common", "ofe.claim_points.race_number_1374")

      public enum Header1365 {
        /// 
        public static let One: String = CommonStrings.tr("Common", "ofe.claim_points.header_1365.one")
        /// 
        public static let Other: String = CommonStrings.tr("Common", "ofe.claim_points.header_1365.other")
        /// 
        public static let Zero: String = CommonStrings.tr("Common", "ofe.claim_points.header_1365.zero")
      }
    }

    public enum Common {
      /// CONTACT DETAILS
      public static let ContactDetails1357: String = CommonStrings.tr("Common", "ofe.common.contact_details_1357")
      /// Email
      public static let Email1358: String = CommonStrings.tr("Common", "ofe.common.email_1358")
      /// Event Type
      public static let EventType1356: String = CommonStrings.tr("Common", "ofe.common.event_type_1356")
      /// Metrics
      public static let Metrics1376: String = CommonStrings.tr("Common", "ofe.common.metrics_1376")
      /// Mobile
      public static let Mobile1359: String = CommonStrings.tr("Common", "ofe.common.mobile_1359")
      /// No History
      public static let NoHistory1361: String = CommonStrings.tr("Common", "ofe.common.no_history_1361")
      /// Keep track of organized fitness events submissions here.
      public static let NoHistoryMessage1362: String = CommonStrings.tr("Common", "ofe.common.no_history_message_1362")
      /// Enter Event Type
      public static let Placeholder1402: String = CommonStrings.tr("Common", "ofe.common.placeholder_1402")
      /// POINTS
      public static let Points1353: String = CommonStrings.tr("Common", "ofe.common.points_1353")
      /// Submit
      public static let Submit1400: String = CommonStrings.tr("Common", "ofe.common.submit_1400")
      /// Suggest Event Type
      public static let SuggestEventType1354: String = CommonStrings.tr("Common", "ofe.common.suggest_event_type_1354")
      /// UNIT
      public static let Unit1407: String = CommonStrings.tr("Common", "ofe.common.unit_1407")
      /// Units of measure
      public static let UnitsOfMeasure1375: String = CommonStrings.tr("Common", "ofe.common.units_of_measure_1375")
      /// Weblink Proof
      public static let WebLinkProof1381: String = CommonStrings.tr("Common", "ofe.common.web_link_proof_1381")
    }

    public enum Completed {
      /// Way to go, your fitness event has been submitted.
      public static let Description1395: String = CommonStrings.tr("Common", "ofe.completed.description_1395")
      /// Thank you, your suggested fitness event has been submitted successfully.
      public static let Description1404: String = CommonStrings.tr("Common", "ofe.completed.description_1404")
      /// Fitness Event Submitted
      public static let Header1394: String = CommonStrings.tr("Common", "ofe.completed.header_1394")
      /// Suggestion Submitted
      public static let Header1403: String = CommonStrings.tr("Common", "ofe.completed.header_1403")
    }

    public enum Events {
      /// Suggest Event Type
      public static let ActionTitle1347: String = CommonStrings.tr("Common", "ofe.events.action_title_1347")
      /// Not seeing your event type?
      public static let Content1345: String = CommonStrings.tr("Common", "ofe.events.content_1345")
      /// Let us know what event you take part in so we can consider including it in the list of fitness events.
      public static let Content1346: String = CommonStrings.tr("Common", "ofe.events.content_1346")
    }

    public enum Heading {
      /// Organised Fitness Events
      public static let Title1335: String = CommonStrings.tr("Common", "ofe.heading.title_1335")
    }

    public enum History {
      /// EVENT DATES
      public static let EventDetails1397: String = CommonStrings.tr("Common", "ofe.history.event_details_1397")
      /// SUBMISSION DATE
      public static let SubmissionDate1398: String = CommonStrings.tr("Common", "ofe.history.submission_date_1398")
    }

    public enum Landing {
      /// Events and Points
      public static let ActionTitle1344: String = CommonStrings.tr("Common", "ofe.landing.action_title_1344")
      /// Fitness Events
      public static let ActionTitleAndroid1344: String = CommonStrings.tr("Common", "ofe.landing.action_title_android_1344")
      /// Take part in fitness events and earn points for your hard work.
      public static let Description1343: String = CommonStrings.tr("Common", "ofe.landing.description_1343")
      /// Earn Points for Fitness Events
      public static let Header1342: String = CommonStrings.tr("Common", "ofe.landing.header_1342")
      /// LAST CLAIMED EVENT
      public static let SectionHeader1396: String = CommonStrings.tr("Common", "ofe.landing.section_header_1396")
    }

    public enum LearnMore {
      /// Organised Fitness Events gives you the opportunity to earn Vitality points for participating in sports events such as walking, swimming, running, cycling, mountain biking, canoeing, triathlons and more.
      public static let Description1349: String = CommonStrings.tr("Common", "ofe.learn_more.description_1349")
      /// Earn points for submitting a fitness event that you have completed. Be sure to have proof of your participation in the event nearby when submitting to earn points. Points earned only go towards your Vitality Status and not Active Rewards or Apple Watch goals.
      public static let Description1350: String = CommonStrings.tr("Common", "ofe.learn_more.description_1350")
      /// Only events completed within the last six months can qualify for claiming points.
      public static let Description1352: String = CommonStrings.tr("Common", "ofe.learn_more.description_1352")
      /// How Organised Fitness Events Works
      public static let Header1348: String = CommonStrings.tr("Common", "ofe.learn_more.header_1348")
      /// Validation Period
      public static let Header1351: String = CommonStrings.tr("Common", "ofe.learn_more.header_1351")
    }

    public enum Notification {
      /// You've earned 200 points for %1$@.
      public static func Message1410(_ p1: String) -> String {
        return CommonStrings.tr("Common", "ofe.notification.message_1410", p1)
      }
    }

    public enum Onboarding {
      /// Earn points for taking part in your favourite fitness events.
      public static let Description1339: String = CommonStrings.tr("Common", "ofe.onboarding.description_1339")
      /// Earn points for qualifying fitness events such as walking, swimming, running and more.
      public static let Description1341: String = CommonStrings.tr("Common", "ofe.onboarding.description_1341")
      /// Get rewarded for completing events
      public static let Header1338: String = CommonStrings.tr("Common", "ofe.onboarding.header_1338")
      /// Earn Points
      public static let Heading1340: String = CommonStrings.tr("Common", "ofe.onboarding.heading_1340")
    }

    public enum Privacy {
      /// Data Privacy Consent
      public static let HeaderTitle1399: String = CommonStrings.tr("Common", "ofe.privacy.header_title_1399")
    }

    public enum Proof {
      /// Add
      public static let AddTitle1385: String = CommonStrings.tr("Common", "ofe.proof.add_title_1385")
      /// Upload a screenshot or photograph of your race certificate or result list.
      public static let Description1384: String = CommonStrings.tr("Common", "ofe.proof.description_1384")
      /// What proof of participation are you going to provide?
      public static let Header1378: String = CommonStrings.tr("Common", "ofe.proof.header_1378")
      /// Add Proof of fitness results
      public static let Header1383: String = CommonStrings.tr("Common", "ofe.proof.header_1383")
      /// Upload or take a photo of the race certificate or take a screenshot confirming the event and your results.
      public static let SectionDescription1380: String = CommonStrings.tr("Common", "ofe.proof.section_description_1380")
      /// Provide a website link containing your race results.
      public static let SectionDescription1382: String = CommonStrings.tr("Common", "ofe.proof.section_description_1382")
      /// Image Proof
      public static let SectionHeader1379: String = CommonStrings.tr("Common", "ofe.proof.section_header_1379")
      /// Select Proof
      public static let Title1377: String = CommonStrings.tr("Common", "ofe.proof.title_1377")
      /// Image Proof
      public static let Title1379: String = CommonStrings.tr("Common", "ofe.proof.title_1379")
      /// Website Link
      public static let Title1388: String = CommonStrings.tr("Common", "ofe.proof.title_1388")
    }

    public enum Search {
      /// There are no results for "%1$@"
      public static func NoResultDescription1369(_ p1: String) -> String {
        return CommonStrings.tr("Common", "ofe.search.no_result_description_1369", p1)
      }
      /// No Results
      public static let NoResults1368: String = CommonStrings.tr("Common", "ofe.search.no_results_1368")
      /// Suggest Fitness Event
      public static let SuggestedFitnessEvent1370: String = CommonStrings.tr("Common", "ofe.search.suggested_fitness_event_1370")
    }

    public enum Status {
      /// Take Part in Fitness Events
      public static let Title1336: String = CommonStrings.tr("Common", "ofe.status.title_1336")
    }

    public enum Suggestion {
      /// We will review your suggested event and consider adding it to the list of Fitness Events.
      public static let SectionHeaderTitle1355: String = CommonStrings.tr("Common", "ofe.suggestion.section_header_title_1355")
    }

    public enum Summary {
      /// An error occurred while trying to complete your Organised Fitness Event. Please try again.
      public static let CompleteErrorMessage2310: String = CommonStrings.tr("Common", "ofe.summary.complete_error_message_2310")
      /// 
      public static let CompleteErrorMessage9999: String = CommonStrings.tr("Common", "ofe.summary.complete_error_message_9999")
      /// Confirmation information
      public static let Header1390: String = CommonStrings.tr("Common", "ofe.summary.header_1390")
      /// Please confirm that the fitness event details and proof of results are correct.
      public static let HeaderDescription1391: String = CommonStrings.tr("Common", "ofe.summary.header_description_1391")
      /// Link
      public static let LinkTitle1393: String = CommonStrings.tr("Common", "ofe.summary.link_title_1393")
      /// PROOF
      public static let ProofTitle1392: String = CommonStrings.tr("Common", "ofe.summary.proof_title_1392")
      /// Summary
      public static let Title1389: String = CommonStrings.tr("Common", "ofe.summary.title_1389")
    }
  }

  public enum Onboarding {
    /// Achieve your weekly targets with Active Rewards and earn great rewards from our partners.
    public static let EnjoyWeeklyRewardsDescription17: String = CommonStrings.tr("Common", "onboarding.enjoy_weekly_rewards_description_17")
    /// ENJOY WEEKLY REWARDS
    public static let EnjoyWeeklyRewardsTitle16: String = CommonStrings.tr("Common", "onboarding.enjoy_weekly_rewards_title_16")
    /// Activate Vitality Active Rewards and track your exercise to meet your weekly target.
    public static let ImproveYourHealthDescription15: String = CommonStrings.tr("Common", "onboarding.improve_your_health_description_15")
    /// IMPROVE YOUR HEALTH
    public static let ImproveYourHealthTitle14: String = CommonStrings.tr("Common", "onboarding.improve_your_health_title_14")
    /// Complete assessments and tasks to get an overview of your health status.
    public static let KnowYourHealthDescription13: String = CommonStrings.tr("Common", "onboarding.know_your_health_description_13")
    /// KNOW YOUR HEALTH
    public static let KnowYourHealthTitle12: String = CommonStrings.tr("Common", "onboarding.know_your_health_title_12")
    /// Get Assessed
    public static let Section1Title2311: String = CommonStrings.tr("Common", "onboarding.section1_title_2311")
    /// Earn points for confirming your smoking status.
    public static let Section1Message100: String = CommonStrings.tr("Common", "onboarding.section_1_message_100")
    /// Visit a healthcare professional near you and get your Body Mass Index, glucose, cholesterol and blood pressure checked.
    public static let Section1Message127: String = CommonStrings.tr("Common", "onboarding.section_1_message_127")
    /// Earn points for completing the Vitality Health Review.
    public static let Section1Message294: String = CommonStrings.tr("Common", "onboarding.section_1_message_294")
    /// Earn points for completing the Vitality Health Review.
    public static let Section1Message407: String = CommonStrings.tr("Common", "onboarding.section_1_message_407")
    /// Get Assessed
    public static let Section1Title126: String = CommonStrings.tr("Common", "onboarding.section_1_title_126")
    /// Earn Points
    public static let Section1Title293: String = CommonStrings.tr("Common", "onboarding.section_1_title_293")
    /// Earn Points
    public static let Section1Title408: String = CommonStrings.tr("Common", "onboarding.section_1_title_408")
    /// Earn Points
    public static let Section1Title99: String = CommonStrings.tr("Common", "onboarding.section_1_title_99")
    /// If you have not smoked (tobacco or e-cigarettes) occasionally or regularly over the past 6 months you qualify as a non-smoker.
    public static let Section2Message102: String = CommonStrings.tr("Common", "onboarding.section_2_message_102")
    /// Capture and upload proof of your measurements from the healthcare professional.
    public static let Section2Message129: String = CommonStrings.tr("Common", "onboarding.section_2_message_129")
    /// Your Vitality Age is a measurement of how healthy you are relative to your current age. You need to complete the Vitality Health Review to unlock your Vitality Age.
    public static let Section2Message296: String = CommonStrings.tr("Common", "onboarding.section_2_message_296")
    /// Your Vitality Age is a measurement of how healthy you are relative to your current age. You need to complete the Vitality Health Review to unlock your Vitality Age.
    public static let Section2Message409: String = CommonStrings.tr("Common", "onboarding.section_2_message_409")
    /// Smoking Status
    public static let Section2Title101: String = CommonStrings.tr("Common", "onboarding.section_2_title_101")
    /// Capture Results
    public static let Section2Title128: String = CommonStrings.tr("Common", "onboarding.section_2_title_128")
    /// Vitality Age
    public static let Section2Title295: String = CommonStrings.tr("Common", "onboarding.section_2_title_295")
    /// Vitality Age
    public static let Section2Title410: String = CommonStrings.tr("Common", "onboarding.section_2_title_410")
    /// This assessment is based on 3 sections consisting of questions about your health and lifestyle habits.
    public static let Section3Message298: String = CommonStrings.tr("Common", "onboarding.section_3_message_298")
    /// Earn points for completing your Vitality Health Check.
    public static let Section3Message341: String = CommonStrings.tr("Common", "onboarding.section_3_message_341")
    /// This assessment is based on %@ sections consisting of questions about your health and lifestyle habits.
    public static func Section3Message411(_ p1: String) -> String {
      return CommonStrings.tr("Common", "onboarding.section_3_message_411", p1)
    }
    /// Earn Points
    public static let Section3Title130: String = CommonStrings.tr("Common", "onboarding.section_3_title_130")
    /// Complete All Sections
    public static let Section3Title297: String = CommonStrings.tr("Common", "onboarding.section_3_title_297")
    /// Complete All Sections
    public static let Section3Title412: String = CommonStrings.tr("Common", "onboarding.section_3_title_412")
    ///  Mental Wellbeing
    public static let Title1195: String = CommonStrings.tr("Common", "onboarding.title_1195")
    /// VITALITY HEALTH REVIEW
    public static let Title2312: String = CommonStrings.tr("Common", "onboarding.title_2312")
    /// VITALITY NUTRITION ASSESSMENT
    public static let Title413: String = CommonStrings.tr("Common", "onboarding.title_413")
    /// 
    public static let Title9997: String = CommonStrings.tr("Common", "onboarding.title_9997")
  }

  public enum Partner {
    /// partners
    public static let VisitPartnerMessage244: String = CommonStrings.tr("Common", "partner.visit_partner_message_244")
    /// Visit a Partner
    public static let VisitPartnerTitle243: String = CommonStrings.tr("Common", "partner.visit_partner_title_243")
  }

  public enum Partners {
    /// Partner rewards are dependent on your Vitality status. Increase your status to increase your rewards.
    public static let LandingHeader607: String = CommonStrings.tr("Common", "Partners.landing_header_607")
  }

  public enum Password {

    public enum Error {
      /// Invalid Password
      public static let InvalidPassword19: String = CommonStrings.tr("Common", "password.error.invalid_password_19")
    }
  }

  public enum PhysicalActivity {
    /// Active Calories
    public static let ItemActiveCaloriesText444: String = CommonStrings.tr("Common", "physical_activity.item_activeCalories_text_444")
    /// Calories Burned
    public static let ItemCalorliesBurnedText443: String = CommonStrings.tr("Common", "physical_activity.item_calorlies_burned_text_443")
    /// Distance
    public static let ItemDistanceText440: String = CommonStrings.tr("Common", "physical_activity.item_distance_text_440")
    /// Heart Rate
    public static let ItemHeartRateText439: String = CommonStrings.tr("Common", "physical_activity.item_heart_rate_text_439")
    /// Steps
    public static let ItemStepsText442: String = CommonStrings.tr("Common", "physical_activity.item_steps_text_442")
  }

  public enum Pm {
    /// Please note that points may not be awarded immediately.
    public static let ActivityDetailNotAwarded2509: String = CommonStrings.tr("Common", "PM.activity_detail_not_awarded_2509")
    /// Please note that the points may not be awarded immediately.
    public static let ActivityDetailNotAwarded2510: String = CommonStrings.tr("Common", "PM.activity_detail_not_awarded_2510")
    /// Points
    public static let ActivityDetailSectionHeading1560: String = CommonStrings.tr("Common", "PM.activity_detail_section_heading_1_560")
    /// Device Used
    public static let ActivityDetailSectionHeading2561: String = CommonStrings.tr("Common", "PM.activity_detail_section_heading_2_561")
    /// Detail
    public static let ActivityDetailSectionHeading3526: String = CommonStrings.tr("Common", "PM.activity_detail_section_heading_3_526")
    /// Activity
    public static let ActivityDetailSectionSubheading527: String = CommonStrings.tr("Common", "PM.activity_detail_section_subheading_527")
    /// Activity Detail
    public static let ActivityDetailSectionTitle559: String = CommonStrings.tr("Common", "PM.activity_detail_section_title_559")
    /// Even though you may complete this activity more than once a year, you only earn points for this assessment every 12 months
    public static let ActivityDetailText22313: String = CommonStrings.tr("Common", "pm.activity_detail_text_2_2313")
    /// 
    public static let ActivityDetailText2999: String = CommonStrings.tr("Common", "pm.activity_detail_text_2_999")
    /// A connection error has occurred. Please make sure that you are connected to a Wi-Fi or cellular network and try again.
    public static let AlertUnableToUpdateConnectionMessage558: String = CommonStrings.tr("Common", "PM.alert_unable_to_update_connection_message_558")
    /// An error occurred while trying to update your points. Please try again.
    public static let AlertUnableToUpdateMessage558: String = CommonStrings.tr("Common", "PM.alert_unable_to_update_message_558")
    /// An error occurred while trying to update your points. Please try again.
    public static let AlertUnableToUpdatePointsMessage558: String = CommonStrings.tr("Common", "PM.alert_unable_to_update_points_message_558")
    /// Unable to update
    public static let AlertUnableToUpdateTitle557: String = CommonStrings.tr("Common", "PM.alert_unable_to_update_title_557")
    /// All Points
    public static let CategoryFilterAllPointsTitle515: String = CommonStrings.tr("Common", "PM.category_filter_all_points_title_515")
    /// Assessments
    public static let CategoryFilterAssessmentTitle516: String = CommonStrings.tr("Common", "PM.category_filter_assessment_title_516")
    /// Booster Points
    public static let CategoryFilterBoosterPointsTitle520: String = CommonStrings.tr("Common", "PM.category_filter_booster_points_title_520")
    /// Fitness
    public static let CategoryFilterFitnessTitle519: String = CommonStrings.tr("Common", "PM.category_filter_fitness_title_519")
    /// Choose Points Activity
    public static let CategoryFilterHeader514: String = CommonStrings.tr("Common", "PM.category_filter_header_514")
    /// Nutrition
    public static let CategoryFilterNutritionTitle517: String = CommonStrings.tr("Common", "PM.category_filter_nutrition_title_517")
    /// Points Adjustments
    public static let CategoryFilterPointsAdjustmentsTitle521: String = CommonStrings.tr("Common", "PM.category_filter_points_adjustments_title_521")
    /// Other
    public static let CategoryFilterPotherTitle521: String = CommonStrings.tr("Common", "PM.category_filter_pother_title_521")
    /// Screening
    public static let CategoryFilterScreeningTitle518: String = CommonStrings.tr("Common", "PM.category_filter_screening_title_518")
    /// Current Membership Year
    public static let CurrentMembershipYearTitle611: String = CommonStrings.tr("Common", "PM.current_membership_year_title_611")
    /// No activities have been completed or synced for the selected month.
    public static let EmptyStateAllPointsMessage525: String = CommonStrings.tr("Common", "PM.empty_state_all_points_message_525")
    /// No Points Activity
    public static let EmptyStateAllPointsTitle524: String = CommonStrings.tr("Common", "PM.empty_state_all_points_title_524")
    /// There is no assessment activity for the selected month. Complete assessments in order to earn points.
    public static let EmptyStateAssessmentMessage529: String = CommonStrings.tr("Common", "PM.empty_state_assessment_message_529")
    /// No Assessment Activity
    public static let EmptyStateAssessmentTitle528: String = CommonStrings.tr("Common", "PM.empty_state_assessment_title_528")
    /// There is no Booster Points activity for the selected month.
    public static let EmptyStateBoosterPointsMessage537: String = CommonStrings.tr("Common", "PM.empty_state_booster_points_message_537")
    /// No Booster Points Activity
    public static let EmptyStateBoosterPointsTitle536: String = CommonStrings.tr("Common", "PM.empty_state_booster_points_title_536")
    /// There is no fitness activity for the selected month. Complete physical activities in order to earn points.
    public static let EmptyStateFitnessMessage535: String = CommonStrings.tr("Common", "PM.empty_state_fitness_message_535")
    /// No Fitness Activity
    public static let EmptyStateFitnessTitle534: String = CommonStrings.tr("Common", "PM.empty_state_fitness_title_534")
    /// There is no nutrition activity for the selected month. Complete nutrition activities in order to earn points.
    public static let EmptyStateNutritionMessage531: String = CommonStrings.tr("Common", "PM.empty_state_nutrition_message_531")
    /// No Nutrition Activity
    public static let EmptyStateNutritionTitle530: String = CommonStrings.tr("Common", "PM.empty_state_nutrition_title_530")
    /// There is no Other activity for the selected month.
    public static let EmptyStateOtherMessage541: String = CommonStrings.tr("Common", "PM.empty_state_other_message_541")
    /// No Other Activity
    public static let EmptyStateOtherTitle540: String = CommonStrings.tr("Common", "PM.empty_state_other_title_540")
    /// There is no Points Adjustments activity for the selected month.
    public static let EmptyStatePointsAdjustmentMessage539: String = CommonStrings.tr("Common", "PM.empty_state_points_adjustment_message_539")
    /// No Points Adjustment Activity
    public static let EmptyStatePointsAdjustmentTitle538: String = CommonStrings.tr("Common", "PM.empty_state_points_adjustment_title_538")
    /// There is no screening activity for the selected month. Complete screenings in order to earn points
    public static let EmptyStateScreeningMessage533: String = CommonStrings.tr("Common", "PM.empty_state_screening_message_533")
    /// No Screening Activity
    public static let EmptyStateScreeningTitle532: String = CommonStrings.tr("Common", "PM.empty_state_screening_title_532")
    /// No more points activity for %1$@
    public static func FootnoteEmptyStateAllPointsMessage544(_ p1: String) -> String {
      return CommonStrings.tr("Common", "PM.footnote_empty_state_all_points_message_544", p1)
    }
    /// No more Assessment activity for %1$@
    public static func FootnoteEmptyStateAssessmentMessage545(_ p1: String) -> String {
      return CommonStrings.tr("Common", "PM.footnote_empty_state_assessment_message_545", p1)
    }
    /// No more Booster Points for %1$@
    public static func FootnoteEmptyStateBoosterPointsMessage549(_ p1: String) -> String {
      return CommonStrings.tr("Common", "PM.footnote_empty_state_booster_points_message_549", p1)
    }
    /// No more Fitness activity for %1$@
    public static func FootnoteEmptyStateFitnessMessage548(_ p1: String) -> String {
      return CommonStrings.tr("Common", "PM.footnote_empty_state_fitness_message_548", p1)
    }
    /// No more Nutrition activity for %1$@
    public static func FootnoteEmptyStateNutritionMessage546(_ p1: String) -> String {
      return CommonStrings.tr("Common", "PM.footnote_empty_state_nutrition_message_546", p1)
    }
    /// No more Other activity for %1$@
    public static func FootnoteEmptyStateOtherMessage551(_ p1: String) -> String {
      return CommonStrings.tr("Common", "PM.footnote_empty_state_other_message_551", p1)
    }
    /// No more Points Adjustment for %1$@
    public static func FootnoteEmptyStatePointsAdjustmentMessage550(_ p1: String) -> String {
      return CommonStrings.tr("Common", "PM.footnote_empty_state_points_adjustment_message_550", p1)
    }
    /// No more Screening activity for %1$@
    public static func FootnoteEmptyStateScreeningMessage547(_ p1: String) -> String {
      return CommonStrings.tr("Common", "PM.footnote_empty_state_screening_message_547", p1)
    }
    /// No more activity for %1$@
    public static func FootnoteStateNoMoreActivityMessage610(_ p1: String) -> String {
      return CommonStrings.tr("Common", "PM.footnote_state_no_more_activity_message_610", p1)
    }
    /// No More Events for %1$@
    public static func FootnoteStateNoMoreEventsMessage1102(_ p1: String) -> String {
      return CommonStrings.tr("Common", "PM.footnote_state_no_more_events_message_1102", p1)
    }
    /// Loading…
    public static let LoadingIndicator523: String = CommonStrings.tr("Common", "PM.loading_indicator_523")
    /// Next month
    public static let MonthviewNextMonth2465: String = CommonStrings.tr("Common", "PM.monthview_next_month_2465")
    /// Previous month
    public static let MonthviewPreviousMonth2464: String = CommonStrings.tr("Common", "PM.monthview_previous_month_2464")
    /// Information is unavailable
    public static let PointsDetailNoInformation569: String = CommonStrings.tr("Common", "PM.points_detail_no_information_569")
    /// %1$@ towards Active Rewards target
    public static func PointsEarningActiveRewards567(_ p1: String) -> String {
      return CommonStrings.tr("Common", "PM.points_earning_active_rewards_567", p1)
    }
    /// %1$@ towards Apple Watch target
    public static func PointsEarningAppleWatch568(_ p1: String) -> String {
      return CommonStrings.tr("Common", "PM.points_earning_apple_watch_568", p1)
    }
    /// Previous Membership Year
    public static let PreviousMembershipYearTitle612: String = CommonStrings.tr("Common", "PM.previous_membership_year_title_612")
    /// Last updated today at %1$@
    public static func RefreshUpdateMessage1552(_ p1: String) -> String {
      return CommonStrings.tr("Common", "PM.refresh_update_message_1_552", p1)
    }
    /// Last updated yesterday at %1$@
    public static func RefreshUpdateMessage2553(_ p1: String) -> String {
      return CommonStrings.tr("Common", "PM.refresh_update_message_2_553", p1)
    }
    /// Last updated %1$@
    public static func RefreshUpdateMessage3554(_ p1: String) -> String {
      return CommonStrings.tr("Common", "PM.refresh_update_message_3_554", p1)
    }
    /// Checking for updates…
    public static let RefreshUpdateMessage4555: String = CommonStrings.tr("Common", "PM.refresh_update_message_4_555")
    /// Updated just now
    public static let RefreshUpdateMessage5556: String = CommonStrings.tr("Common", "PM.refresh_update_message_5_556")

    public enum ActivityDetail {
      /// Even though you may complete this activity more than once a year, you only earn points for this assessment every 12 months
      public static let ContentDescription1990: String = CommonStrings.tr("Common", "pm.activity_detail.content_description_1990")
      /// Points have already been awarded for this activity
      public static let ContentTitle1989: String = CommonStrings.tr("Common", "pm.activity_detail.content_title_1989")
    }
  }

  public enum Points {
    /// No Points Earned
    public static let NoPointsEarnedTitle194: String = CommonStrings.tr("Common", "points.no_points_earned_title_194")
    /// Points Pending
    public static let PointsPendingTitle192: String = CommonStrings.tr("Common", "points.points_pending_title_192")
    /// %1$@ of %2$@ points
    public static func XOfYPointsTitle193(_ p1: String, _ p2: String) -> String {
      return CommonStrings.tr("Common", "points.x_of_y_points_title_193", p1, p2)
    }
  }

  public enum PotentialPoints {
    /// BMI ranging between %1$@ to %2$@
    public static func BmiRangingBetween2314(_ p1: String, _ p2: String) -> String {
      return CommonStrings.tr("Common", "potential_points.bmi_ranging_between_2314", p1, p2)
    }
    /// 
    public static let BmiRangingBetween9999: String = CommonStrings.tr("Common", "potential_points.bmi_ranging_between_9999")
    /// A minimum of %1$@ burned in one exercise session a day
    public static func CalorieCountGreaterOrEqualThanBound593(_ p1: String) -> String {
      return CommonStrings.tr("Common", "potential_points.calorie_count_greater_or_equal_than_bound_593", p1)
    }
    /// Between %1$@ and %2$@ burned in one exercise session a day
    public static func CalorieCountLessAndGreaterThanBound596(_ p1: String, _ p2: String) -> String {
      return CommonStrings.tr("Common", "potential_points.calorie_count_less_and_greater_than_bound_596", p1, p2)
    }
    /// Between %1$@ and %2$@ burned in one exercise session a day
    public static func CalorieCountLessOrEqualAndGreaterOrEqualBound595(_ p1: String, _ p2: String) -> String {
      return CommonStrings.tr("Common", "potential_points.calorie_count_less_or_equal_and_greater_or_equal_bound_595", p1, p2)
    }
    /// Between %1$@ and %2$@ burned in one exercise session a day
    public static func CalorieCountLessOrEqualAndGreaterThanBound594(_ p1: String, _ p2: String) -> String {
      return CommonStrings.tr("Common", "potential_points.calorie_count_less_or_equal_and_greater_than_bound_594", p1, p2)
    }
    /// A minimum of %1$@ burned in one exercise session a day
    public static func CalorieCountLessOrEqualThanBound592(_ p1: String) -> String {
      return CommonStrings.tr("Common", "potential_points.calorie_count_less_or_equal_than_bound_592", p1)
    }
    /// A minimum of %1$@ burned in one exercise session a day
    public static func CalorieCountLowerBound490(_ p1: String) -> String {
      return CommonStrings.tr("Common", "potential_points.calorie_count_lower_bound_490", p1)
    }
    /// Between %1$@ and %2$@ burned in one exercise session a day
    public static func CalorieCountLowerBoundUpperBound491(_ p1: String, _ p2: String) -> String {
      return CommonStrings.tr("Common", "potential_points.calorie_count_lower_bound_upper_bound_491", p1, p2)
    }
    /// A maximum of %1$@ burned in one exercise session a day
    public static func CalorieCountUpperBound492(_ p1: String) -> String {
      return CommonStrings.tr("Common", "potential_points.calorie_count_upper_bound_492", p1)
    }
    /// Steps are calculated on a daily basis. get moving to earn points.
    public static let HeaderMessage2315: String = CommonStrings.tr("Common", "potential_points.header_message_2315")
    /// 
    public static let HeaderMessage9991: String = CommonStrings.tr("Common", "potential_points.header_message_9991")
    /// How to Earn Points
    public static let HeaderTitle476: String = CommonStrings.tr("Common", "potential_points.header_title_476")
    /// How to Earn Points
    public static let HeaderTitleSoftbank476: String = CommonStrings.tr("Common", "potential_points.header_title_softbank_476")
    /// An average heart rate of more than %1$@ of your age-related maximum heart rate
    public static func HeartrateGreaterOrEqualThanBound588(_ p1: String) -> String {
      return CommonStrings.tr("Common", "potential_points.heartrate_greater_or_equal_than_bound_588", p1)
    }
    /// An average heart rate of between %1$@ and %2$@ of your age-related maximum heart rate
    public static func HeartrateLessAndGreaterThanBound591(_ p1: String, _ p2: String) -> String {
      return CommonStrings.tr("Common", "potential_points.heartrate_less_and_greater_than_bound_591", p1, p2)
    }
    /// An average heart rate of between %1$@ and %2$@ of your age-related maximum heart rate
    public static func HeartrateLessOrEqualAndGreaterOrEqualBound590(_ p1: String, _ p2: String) -> String {
      return CommonStrings.tr("Common", "potential_points.heartrate_less_or_equal_and_greater_or_equal_bound_590", p1, p2)
    }
    /// An average heart rate of between %1$@ %2$@ of your age-related maximum heart rate
    public static func HeartrateLessOrEqualAndGreaterThanBound589(_ p1: String, _ p2: String) -> String {
      return CommonStrings.tr("Common", "potential_points.heartrate_less_or_equal_and_greater_than_bound_589", p1, p2)
    }
    /// An average heart rate of more than %1$@ of your age-related maximum heart rate
    public static func HeartrateLessOrEqualThanBound587(_ p1: String) -> String {
      return CommonStrings.tr("Common", "potential_points.heartrate_less_or_equal_than_bound_587", p1)
    }
    /// An average heart rate of more than %1$@ of your age-related maximum heart rate
    public static func HeartrateLowerBound482(_ p1: String) -> String {
      return CommonStrings.tr("Common", "potential_points.heartrate_lower_bound_482", p1)
    }
    /// An average heart rate between %1$@ and %2$@ of your age-related maximum heart rate
    public static func HeartrateLowerBoundUpperBound481(_ p1: String, _ p2: String) -> String {
      return CommonStrings.tr("Common", "potential_points.heartrate_lower_bound_upper_bound_481", p1, p2)
    }
    /// An average heart rate of less than %1$@ of your age-related maximum heart rate
    public static func HeartrateUpperBound480(_ p1: String) -> String {
      return CommonStrings.tr("Common", "potential_points.heartrate_upper_bound_480", p1)
    }
    /// Points are earned when exercising for a certain time and burning between 100 and 500 calories.
    public static let HowToCalculateCaloriesBurnedMessage502: String = CommonStrings.tr("Common", "potential_points.how_to_calculate_calories_burned_message_502")
    /// How to Determine Your Required Calories Burned
    public static let HowToCalculateCaloriesBurnedTitle499: String = CommonStrings.tr("Common", "potential_points.how_to_calculate_calories_burned_title_499")
    /// Points are earned when exercising for a certain time and reaching an age-related maximum heart rate between 60-70%%.\n\nTo calculate your maximum heart rate, take 220 and subtract your age. For example, if you are 30 years old, 190 beats per minute (bpm) is your maximum heart rate (220 - 30 = 190). \n\nCalculate your required heart rate by multiplying 60%% or 70%% to your maximum heart rate. If you are 30 years old, your target heart rate at 60%% is 114bpm and at 70%% is 133bpm.\n\nExample:\nHow do I calculate my age-related maximum heart rate?\nJohn is 30 years old. His age-related maximum heart rate is 190bpm (220 - 30).\nJohn must keep his average heart rate above 114bpm (60%% of his maximum heart rate) during his 30-minute exercise session to earn 50 Vitality points.\nIf John keeps his heart rate above 133bpm (70%% of his maximum heart rate), he will earn 100 points.
    public static let HowToCalculateHeartRate484: String = CommonStrings.tr("Common", "potential_points.how_to_calculate_heart_rate_484")
    /// How to Determine Your Required Heart Rate
    public static let HowToCalculateHeartRateTitle483: String = CommonStrings.tr("Common", "potential_points.how_to_calculate_heart_rate_title_483")
    /// Points are earned when exercising for a certain time and reaching an average speed between 4km/h  and 9km/h.
    public static let HowToCalculateSpeedMessage501: String = CommonStrings.tr("Common", "potential_points.how_to_calculate_speed_message_501")
    /// How to Determine Your Required Speed
    public static let HowToCalculateSpeedTitle498: String = CommonStrings.tr("Common", "potential_points.how_to_calculate_speed_title_498")
    /// Points are earned when tracking your steps each day.
    public static let HowToCalculateStepsMessage500: String = CommonStrings.tr("Common", "potential_points.how_to_calculate_steps_message_500")
    /// How to Determine Your Required Steps
    public static let HowToCalculateStepsTitle497: String = CommonStrings.tr("Common", "potential_points.how_to_calculate_steps_title_497")
    /// points
    public static let SectionHeader495: String = CommonStrings.tr("Common", "potential_points.section_header_495")
    /// An average speed of %1$@ an hour
    public static func SpeedAverageSpeedGreaterOrEqualBound597(_ p1: String) -> String {
      return CommonStrings.tr("Common", "potential_points.speed_average_speed_greater_or_equal_bound_597", p1)
    }
    /// An average speed between %1$@ and %2$@ an hour
    public static func SpeedAverageSpeedLessOrEqualAndGreaterBound598(_ p1: String, _ p2: String) -> String {
      return CommonStrings.tr("Common", "potential_points.speed_average_speed_less_or_equal_and_greater_bound_598", p1, p2)
    }
    /// An average speed between %1$@ and %2$@ an hour
    public static func SpeedAverageSpeedLessOrEqualAndGreaterOrEqualBound599(_ p1: String, _ p2: String) -> String {
      return CommonStrings.tr("Common", "potential_points.speed_average_speed_less_or_equal_and_greater_or_Equal_bound_599", p1, p2)
    }
    /// An average speed of %1$@ an hour
    public static func SpeedAverageSpeedLessOrEqualBound601(_ p1: String) -> String {
      return CommonStrings.tr("Common", "potential_points.speed_average_speed_less_or_equal_bound_601", p1)
    }
    /// An average speed between %1$@ and %2$@ an hour
    public static func SpeedAverageSpeedLessThanAndGreaterOrEqualBound600(_ p1: String, _ p2: String) -> String {
      return CommonStrings.tr("Common", "potential_points.speed_average_speed_less_than_and_greater_or_equal_bound_600", p1, p2)
    }
    /// An average speed between %1$@ and %2$@ an hour
    public static func SpeedAverageSpeedLowerAndUpperBound488(_ p1: String, _ p2: String) -> String {
      return CommonStrings.tr("Common", "potential_points.speed_average_speed_lower_and_upper_bound_488", p1, p2)
    }
    /// A minimum average speed of %1$@ an hour
    public static func SpeedAverageSpeedLowerBound487(_ p1: String) -> String {
      return CommonStrings.tr("Common", "potential_points.speed_average_speed_lower_bound_487", p1)
    }
    /// A maximum average speed of %1$@ an hour
    public static func SpeedAverageSpeedUpperBound489(_ p1: String) -> String {
      return CommonStrings.tr("Common", "potential_points.speed_average_speed_upper_bound_489", p1)
    }
    /// A maximum of %1$@ of physical activity in one exercise session a day
    public static func SpeedDurationGreaterOrEqualBound582(_ p1: String) -> String {
      return CommonStrings.tr("Common", "potential_points.speed_duration_greater_or_equal_bound_582", p1)
    }
    /// A duration between %1$@  and %2$@ of physical activity in one exercise session a day
    public static func SpeedDurationLessAndGreaterOrEqualThanBound585(_ p1: String, _ p2: String) -> String {
      return CommonStrings.tr("Common", "potential_points.speed_duration_less_and_greater_or_equal_than_bound_585", p1, p2)
    }
    /// Between %1$@ and %2$@of physical activity in one exercise session a day
    public static func SpeedDurationLessOrEqualAndGreaterOrEqualThanBound586(_ p1: String, _ p2: String) -> String {
      return CommonStrings.tr("Common", "potential_points.speed_duration_less_or_equal_and_greater_or_equal_than_bound_586", p1, p2)
    }
    /// Between %1$@ and %2$@ of physical activity in one exercise session a day
    public static func SpeedDurationLessOrEqualAndGreaterThanBound584(_ p1: String, _ p2: String) -> String {
      return CommonStrings.tr("Common", "potential_points.speed_duration_less_or_equal_and_greater_than_bound_584", p1, p2)
    }
    /// A maximum of %1$@ of physical activity in one exercise session a day
    public static func SpeedDurationLessOrEqualThanBound583(_ p1: String) -> String {
      return CommonStrings.tr("Common", "potential_points.speed_duration_less_or_equal_than_bound_583", p1)
    }
    /// Between %1$@ and %2$@ of physical activity in one exercise session a day
    public static func SpeedDurationLowerAndUpperBound478(_ p1: String, _ p2: String) -> String {
      return CommonStrings.tr("Common", "potential_points.speed_duration_lower_and_upper_bound_478", p1, p2)
    }
    /// At least %1$@ of physical activity in one exercise session a day
    public static func SpeedDurationLowerBound479(_ p1: String) -> String {
      return CommonStrings.tr("Common", "potential_points.speed_duration_lower_bound_479", p1)
    }
    /// A maximum of %1$@ of physical activity in one exercise session a day
    public static func SpeedDurationUpperBound477(_ p1: String) -> String {
      return CommonStrings.tr("Common", "potential_points.speed_duration_upper_bound_477", p1)
    }
    /// Taking %1$@ steps or more
    public static func StepsGreaterThanOrEqualBound578(_ p1: String) -> String {
      return CommonStrings.tr("Common", "potential_points.steps_greater_than_or_equal_bound_578", p1)
    }
    /// Taking %1$@ - %2$@ steps
    public static func StepsLessAndGreaterThanOrEqualThanBound581(_ p1: String, _ p2: String) -> String {
      return CommonStrings.tr("Common", "potential_points.steps_less_and_greater_than_or_equal_than_bound_581", p1, p2)
    }
    /// Taking %1$@ - %2$@ steps
    public static func StepsLessThanOrEqualAndGreaterThanBound580(_ p1: String, _ p2: String) -> String {
      return CommonStrings.tr("Common", "potential_points.steps_less_than_or_equal_and_greater_than_bound_580", p1, p2)
    }
    /// Taking %1$@ - %2$@ steps
    public static func StepsLessThanOrEqualAndGreaterThanOrEqualBound579(_ p1: String, _ p2: String) -> String {
      return CommonStrings.tr("Common", "potential_points.steps_less_than_or_equal_and_greater_than_or_equal_bound_579", p1, p2)
    }
    /// Taking %1$@ steps or less
    public static func StepsLessThanOrEqualBound577(_ p1: String) -> String {
      return CommonStrings.tr("Common", "potential_points.steps_less_than_or_equal_bound_577", p1)
    }
    /// Taking %1$@ - %2$@ steps
    public static func StepsLowerAndUpperBound485(_ p1: String, _ p2: String) -> String {
      return CommonStrings.tr("Common", "potential_points.steps_lower_and_upper_bound_485", p1, p2)
    }
    /// Taking more than %1$@ steps
    public static func StepsLowerBoundOnly486(_ p1: String) -> String {
      return CommonStrings.tr("Common", "potential_points.steps_lower_bound_only_486", p1)
    }
    /// Taking %1$@ steps or less
    public static func StepsUpperBoundOnly508(_ p1: String) -> String {
      return CommonStrings.tr("Common", "potential_points.steps_upper_bound_only_508", p1)
    }
  }

  public enum PrivacyPolicy {
    /// An error occurred while trying to complete your Non-smoker's Declaration.  Please try again.
    public static let UnableToCompleteAlertMessage116: String = CommonStrings.tr("Common", "privacy_policy.unable_to_complete_alert_message_116")
    /// Unable to Complete
    public static let UnableToCompleteAlertTitle115: String = CommonStrings.tr("Common", "privacy_policy.unable_to_complete_alert_title_115")
  }

  public enum Profile {
    /// This email will be used for all Vitality Communications.
    public static let EmailCommunicationsDisclaimer32316: String = CommonStrings.tr("Common", "profile.email_communications_disclaimer_3_2316")
    /// 
    public static let EmailCommunicationsDisclaimer39999: String = CommonStrings.tr("Common", "profile.email_communications_disclaimer_3_9999")
    /// Enter Email Address
    public static let EnterEmailAddressPlaceholder1169: String = CommonStrings.tr("Common", "profile.enter_email_address_placeholder_1169")
    /// Enter Mobile Number
    public static let EnterMobileNumberPlaceholder1168: String = CommonStrings.tr("Common", "profile.enter_mobile_number_placeholder_1168")
    /// No Entity Number
    public static let FooterEntityNumberTitle2317: String = CommonStrings.tr("Common", "profile.footer_entity_number_title_2317")
    /// Entity Number
    public static let LandingEntityNumberTitle2320: String = CommonStrings.tr("Common", "profile.landing_entity_number_title_2320")

    public enum Landing {
      /// This information comes from your insurer. If it's incorrect please contact 0860 000 3813.
      public static let DetailsFootnoteMessageWithContact2318: String = CommonStrings.tr("Common", "profile.landing.details_footnote_message_with_contact_2318")
      /// 
      public static let DetailsFootnoteMessageWithContact9999: String = CommonStrings.tr("Common", "profile.landing.details_footnote_message_with_contact_9999")
      /// This information comes from your insurer and will be used for Vitality communications and marketing. To update this information, please contact 0860 000 3813.
      public static let EmailFootnoteMessageWithContact2319: String = CommonStrings.tr("Common", "profile.landing.email_footnote_message_with_contact_2319")
      /// 
      public static let EmailFootnoteMessageWithContact9999: String = CommonStrings.tr("Common", "profile.landing.email_footnote_message_with_contact_9999")
    }

    public enum Settings {
      /// This email will be used for all Vitality Communications. Please note that this will not change your login username or security related communications email.
      public static let ChangeCommunicationEmailDisclaimer2322: String = CommonStrings.tr("Common", "profile.settings.change_communication_email_disclaimer_2322")
      /// 
      public static let ChangeCommunicationEmailDisclaimer9999: String = CommonStrings.tr("Common", "profile.settings.change_communication_email_disclaimer_9999")
      /// Change Communications Email
      public static let ChangeCommunicationsEmailTitle2321: String = CommonStrings.tr("Common", "profile.settings.change_communications_email_title_2321")
      /// 
      public static let ChangeCommunicationsEmailTitle9999: String = CommonStrings.tr("Common", "profile.settings.change_communications_email_title_9999")
      /// You have successfully changed your login email. A confirmation email has been sent to the updated address.
      public static let ChangeEmailConfirmation2323: String = CommonStrings.tr("Common", "profile.settings.change_email_confirmation_2323")
      /// 
      public static let ChangeEmailConfirmation9999: String = CommonStrings.tr("Common", "profile.settings.change_email_confirmation_9999")
      /// This email will be used for all security related communications as well as login. Please note that this will not change the contact email linked to your insurer.
      public static let ChangeEmailDisclaimer2325: String = CommonStrings.tr("Common", "profile.settings.change_email_disclaimer_2325")
      /// This email will be used for your login username. Please note that this will not change the contact email linked to your insurer or Vitality Communications
      public static let ChangeEmailDisclaimer22324: String = CommonStrings.tr("Common", "profile.settings.change_email_disclaimer_2_2324")
      /// 
      public static let ChangeEmailDisclaimer29999: String = CommonStrings.tr("Common", "profile.settings.change_email_disclaimer_2_9999")
      /// 
      public static let ChangeEmailDisclaimer9999: String = CommonStrings.tr("Common", "profile.settings.change_email_disclaimer_9999")
      /// This email will be used for all Vitality communications.\n\nChanging this email will not affect your login username and security related communications email, contact <font color="#ff6e00"><a href="tel:+278600003813">08600003813</a></font> if you would like to update it.
      public static let ChangeEmailFooter2326: String = CommonStrings.tr("Common", "profile.settings.change_email_footer_2326")
      /// 
      public static let ChangeEmailFooter9999: String = CommonStrings.tr("Common", "profile.settings.change_email_footer_9999")
      /// Communication Email Changed
      public static let CommunicationChangeEmailHeader2328: String = CommonStrings.tr("Common", "profile.settings.communication_change_email_header_2328")
      /// 
      public static let CommunicationChangeEmailHeader9999: String = CommonStrings.tr("Common", "profile.settings.communication_change_email_header_9999")
      /// You have successfully changed your communications email.
      public static let CommunicationChangeEmailMessage2329: String = CommonStrings.tr("Common", "profile.settings.communication_change_email_message_2329")
      /// 
      public static let CommunicationChangeEmailMessage9999: String = CommonStrings.tr("Common", "profile.settings.communication_change_email_message_9999")
      /// Communications Email Changed
      public static let CommunicationsEmailChangedTitle2327: String = CommonStrings.tr("Common", "profile.settings.communications_email_changed_title_2327")
      /// 
      public static let CommunicationsEmailChangedTitle9999: String = CommonStrings.tr("Common", "profile.settings.communications_email_changed_title_9999")
      /// Are you sure you want to Change your Communications Email?
      public static let ConfirmChangeCommunicationEmail2330: String = CommonStrings.tr("Common", "profile.settings.confirm_change_communication_email_2330")
      /// 
      public static let ConfirmChangeCommunicationEmail9999: String = CommonStrings.tr("Common", "profile.settings.confirm_change_communication_email_9999")
      /// Are you sure you want to Change your Login Email?
      public static let ConfirmChangeLoginEmail2331: String = CommonStrings.tr("Common", "profile.settings.confirm_change_login_email_2331")
      /// 
      public static let ConfirmChangeLoginEmail9999: String = CommonStrings.tr("Common", "profile.settings.confirm_change_login_email_9999")
      /// Login Email Changed
      public static let LoginEmailChanged2332: String = CommonStrings.tr("Common", "profile.settings.login_email_changed_2332")
      /// 
      public static let LoginEmailChanged9999: String = CommonStrings.tr("Common", "profile.settings.login_email_changed_9999")
      /// New Communications Email
      public static let NewCommunicationsEmailPlaceholder2333: String = CommonStrings.tr("Common", "profile.settings.new_communications_email_placeholder_2333")
      /// 
      public static let NewCommunicationsEmailPlaceholder9999: String = CommonStrings.tr("Common", "profile.settings.new_communications_email_placeholder_9999")
      /// Change
      public static let ProfileChangeEmailActionButtonTitle2334: String = CommonStrings.tr("Common", "profile.settings.profile_change_email_action_button_title_2334")
      /// 
      public static let ProfileChangeEmailActionButtonTitle9999: String = CommonStrings.tr("Common", "profile.settings.profile_change_email_action_button_title_9999")
      /// Changing this email will also change your login email
      public static let ProfileChangeEmailAlertMessage2335: String = CommonStrings.tr("Common", "profile.settings.profile_change_email_alert_message_2335")
      /// 
      public static let ProfileChangeEmailAlertMessage9999: String = CommonStrings.tr("Common", "profile.settings.profile_change_email_alert_message_9999")
      /// Change Login Email
      public static let ProfileChangeLoginEmailTitle2336: String = CommonStrings.tr("Common", "profile.settings.profile_change_login_email_title_2336")
      /// 
      public static let ProfileChangeLoginEmailTitle9999: String = CommonStrings.tr("Common", "profile.settings.profile_change_login_email_title_9999")
      /// 0860 000 3813
      public static let ProfileCommunicationsEmailContactNumber2337: String = CommonStrings.tr("Common", "profile.settings.profile_communications_email_contact_number_2337")
      /// 
      public static let ProfileCommunicationsEmailContactNumber9999: String = CommonStrings.tr("Common", "profile.settings.profile_communications_email_contact_number_9999")
      /// Current Login Email
      public static let ProfileCurrentLoginEmailTitle2338: String = CommonStrings.tr("Common", "profile.settings.profile_current_login_email_title_2338")
      /// 
      public static let ProfileCurrentLoginEmailTitle9999: String = CommonStrings.tr("Common", "profile.settings.profile_current_login_email_title_9999")
      /// text
      public static let ProfileEmailMessageForCommunicationPreferenceHyperlink2625: String = CommonStrings.tr("Common", "profile.settings.profile_email_message_for_communication_preference_hyperlink_2625")
      /// Text Link
      public static let ProfileEmailMessageForCommunicationPreferenceTextlink2626: String = CommonStrings.tr("Common", "profile.settings.profile_email_message_for_communication_preference_textlink_2626")
      /// Enable email communication to %1$@ to receive important information and updates.
      public static func ProfileEmailToggleMessageForOption1And32339(_ p1: String) -> String {
        return CommonStrings.tr("Common", "profile.settings.profile_email_toggle_message_for_option_1_and_3_2339", p1)
      }
      /// 
      public static let ProfileEmailToggleMessageForOption1And39999: String = CommonStrings.tr("Common", "profile.settings.profile_email_toggle_message_for_option_1_and_3_9999")
      ///  Enable email communication to %1$@ to receive important information and updates.\n\nTo change this email please contact %2$@.
      public static func ProfileEmailToggleMessageForOption2And42340(_ p1: String, _ p2: String) -> String {
        return CommonStrings.tr("Common", "profile.settings.profile_email_toggle_message_for_option_2_and_4_2340", p1, p2)
      }
      /// 
      public static let ProfileEmailToggleMessageForOption2And49999: String = CommonStrings.tr("Common", "profile.settings.profile_email_toggle_message_for_option_2_and_4_9999")
      /// Communications Email
      public static let ProfileEmailToggleTitle2341: String = CommonStrings.tr("Common", "profile.settings.profile_email_toggle_title_2341")
      /// 
      public static let ProfileEmailToggleTitle9999: String = CommonStrings.tr("Common", "profile.settings.profile_email_toggle_title_9999")
      /// This email will be used for your login username as well as all security related communications. E.g. Forgot password emails.\n\nChanging this email will not affect your email linked to your insurer or Vitality communications, contact <font color="#ff6e00"><a href="tel:+278600003813">0860 000 3813</a></font> if you would like to update it.
      public static let ProfileNewEmailFooter2342: String = CommonStrings.tr("Common", "profile.settings.profile_new_email_footer_2342")
      /// 
      public static let ProfileNewEmailFooter9999: String = CommonStrings.tr("Common", "profile.settings.profile_new_email_footer_9999")
      /// New Login Email
      public static let ProfileNewLoginEmailTitle2343: String = CommonStrings.tr("Common", "profile.settings.profile_new_login_email_title_2343")
      /// 
      public static let ProfileNewLoginEmailTitle9999: String = CommonStrings.tr("Common", "profile.settings.profile_new_login_email_title_9999")
      /// Notifications can be enabled or disabled in Settings
      public static let PushNotificationMessageAndroid2344: String = CommonStrings.tr("Common", "profile.settings.push_notification_message_android_2344")
      /// 
      public static let PushNotificationMessageAndroid9999: String = CommonStrings.tr("Common", "profile.settings.push_notification_message_android_9999")
      /// Manage notifications in the Settings app
      public static let PushNotificationMessageIos2345: String = CommonStrings.tr("Common", "profile.settings.push_notification_message_ios_2345")
      /// 
      public static let PushNotificationMessageIos9999: String = CommonStrings.tr("Common", "profile.settings.push_notification_message_ios_9999")
      /// App Notifications Preferences
      public static let PushNotificationTitle2346: String = CommonStrings.tr("Common", "profile.settings.push_notification_title_2346")
      /// 
      public static let PushNotificationTitle9999: String = CommonStrings.tr("Common", "profile.settings.push_notification_title_9999")
      /// Unable to Change Communications Email
      public static let UnableToChangeCommunicationEmail2347: String = CommonStrings.tr("Common", "profile.settings.unable_to_change_communication_email_2347")
      /// 
      public static let UnableToChangeCommunicationEmail9999: String = CommonStrings.tr("Common", "profile.settings.unable_to_change_communication_email_9999")
      /// Unable to Change Login Email
      public static let UnableToChangeLoginEmail2348: String = CommonStrings.tr("Common", "profile.settings.unable_to_change_login_email_2348")
      /// 
      public static let UnableToChangeLoginEmail9999: String = CommonStrings.tr("Common", "profile.settings.unable_to_change_login_email_9999")
    }
  }

  public enum Proof {
    /// Choose From Library
    public static let ActionChooseFromLibraryAlertTitle168: String = CommonStrings.tr("Common", "proof.action_choose__from_library_alert_title_168")
    /// Choose from Gallery
    public static let ActionChooseGalleryDialogTitle257: String = CommonStrings.tr("Common", "proof.action_choose_gallery_dialog_title_257")
    /// Take Photo
    public static let ActionTakePhotoAlertTitle167: String = CommonStrings.tr("Common", "proof.action_take_photo_alert_title_167")
    /// Take a photo
    public static let ActionTakePhotoDialogTitle256: String = CommonStrings.tr("Common", "proof.action_take_photo_dialog_title_256")
    /// Add
    public static let AddButton166: String = CommonStrings.tr("Common", "proof.add_button_166")
    /// Upload the proof of the measurements you’ve capture from your healthcare professional.
    public static let AddProofEmptyMessage165: String = CommonStrings.tr("Common", "proof.add_proof_empty_message_165")
    /// Add Proof of Measurements
    public static let AddProofEmptyTitle164: String = CommonStrings.tr("Common", "proof.add_proof_empty_title_164")
    /// Add Proof
    public static let AddProofScreenTitle163: String = CommonStrings.tr("Common", "proof.add_proof_screen_title_163")
    /// Delete
    public static let AttachmentsDeleteButton2349: String = CommonStrings.tr("Common", "proof.attachments_delete_button_2349")
    /// 
    public static let AttachmentsDeleteButton9999: String = CommonStrings.tr("Common", "proof.attachments_delete_button_9999")
    /// %1$@ items will be deleted.
    public static func AttachmentsDeleteMessage2350(_ p1: String) -> String {
      return CommonStrings.tr("Common", "proof.attachments_delete_message_2350", p1)
    }
    /// %1$@ of %2$@ attachments
    public static func AttachmentsFootnoteMessage177(_ p1: String, _ p2: String) -> String {
      return CommonStrings.tr("Common", "proof.attachments_footnote_message_177", p1, p2)
    }
    /// You have reached the maximum amount of attachments. Delete some attachments to add more.
    public static let AttachmentsLimitMessageBody2351: String = CommonStrings.tr("Common", "proof.attachments_limit_message_body_2351")
    /// 
    public static let AttachmentsLimitMessageBody9999: String = CommonStrings.tr("Common", "proof.attachments_limit_message_body_9999")
    /// Attachment limit reached
    public static let AttachmentsLimitMessageTitle2352: String = CommonStrings.tr("Common", "proof.attachments_limit_message_title_2352")
    /// 
    public static let AttachmentsLimitMessageTitle9999: String = CommonStrings.tr("Common", "proof.attachments_limit_message_title_9999")
    /// %1$@ of %2$@ attachments uploaded
    public static func AttachmentsUploadedFootnoteMessage2353(_ p1: String, _ p2: String) -> String {
      return CommonStrings.tr("Common", "proof.attachments_uploaded_footnote_message_2353", p1, p2)
    }
    /// Vitality Would Like to Access the Camera
    public static let CameraAccessAlertTitle171: String = CommonStrings.tr("Common", "proof.camera_access_alert_title_171")
    /// Allow Vitality to access your Camera
    public static let CameraMessage272: String = CommonStrings.tr("Common", "proof.camera_message_272")
    /// Vitality does not have access to your camera. Enable access through the Settings app.
    public static let CannotAccessCameraAlertMessage176: String = CommonStrings.tr("Common", "proof.cannot_access_camera_alert_message_176")
    /// Cannot Access Camera
    public static let CannotAccessCameraAlertTitle175: String = CommonStrings.tr("Common", "proof.cannot_access_camera_alert_title_175")
    /// Vitality does not have access to your photo library. Enable access through the Settings app.
    public static let CannotAccessLibraryAlertMessage173: String = CommonStrings.tr("Common", "proof.cannot_access_library_alert_message_173")
    /// Cannot Access Library
    public static let CannotAccessLibraryAlertTitle172: String = CommonStrings.tr("Common", "proof.cannot_access_library_alert_title_172")
    /// Don't Allow
    public static let DontAllowButton170: String = CommonStrings.tr("Common", "proof.dont_allow_button_170")
    /// Allow Vitality to access your Gallery
    public static let GalleryMessage258: String = CommonStrings.tr("Common", "proof.gallery_message_258")
    /// Never ask again
    public static let NeverAskButtonMessage259: String = CommonStrings.tr("Common", "proof.never_ask_button_message_259")
    /// Vitality Would Like to Access the Photo Library
    public static let PhotoAccessAlertTitle169: String = CommonStrings.tr("Common", "proof.photo_access_alert_title_169")
    /// Remove Attachment
    public static let RemoveButton179: String = CommonStrings.tr("Common", "proof.remove_button_179")
    /// Remove
    public static let RemoveButton275: String = CommonStrings.tr("Common", "proof.remove_button_275")
    /// Are you sure you want to remove this attachment?
    public static let RemoveMessage178: String = CommonStrings.tr("Common", "proof.remove_message_178")
    /// 1 item will be removed.
    public static let RemoveMessage274: String = CommonStrings.tr("Common", "proof.remove_message_274")
    /// Remove
    public static let RemoveTitle273: String = CommonStrings.tr("Common", "proof.remove_title_273")
    /// Settings
    public static let SettingsAlertButton174: String = CommonStrings.tr("Common", "proof.settings_alert_button_174")
  }

  public enum PushNotification {
    /// Dismiss
    public static let ActionDismiss2354: String = CommonStrings.tr("Common", "push_notification.action_dismiss_2354")
    /// Go To App
    public static let ActionGoToApp2628: String = CommonStrings.tr("Common", "push_notification.action_go_to_app_2628")
    /// Assessments
    public static let AndroidNotificationTypeAssessments2355: String = CommonStrings.tr("Common", "push_notification.android_notification_type_assessments_2355")
    /// Vitality Active Notifications
    public static let AndroidNotificationTypeDefault2356: String = CommonStrings.tr("Common", "push_notification.android_notification_type_default_2356")

    public enum Local {
      /// You can now check your Wellness Assessment on My Health.
      public static let QuestionnaireCompletedMwb2357: String = CommonStrings.tr("Common", "push_notification.local.questionnaire_completed_mwb_2357")
      /// You can now check your Vitality Age on My Health.
      public static let QuestionnaireCompletedVhr2358: String = CommonStrings.tr("Common", "push_notification.local.questionnaire_completed_vhr_2358")
    }
  }

  public enum Range {
    /// In Healthy Range
    public static let InHealthyTitle190: String = CommonStrings.tr("Common", "range.in_healthy_title_190")
    /// Out of Healthy Range
    public static let OutOfHealthyTitle191: String = CommonStrings.tr("Common", "range.out_of_healthy_title_191")
  }

  public enum Registration {
    /// Resend Insurer Code
    public static let CodeFieldFootnoteResendText34: String = CommonStrings.tr("Common", "registration.code_field_footnote_resend_text_34")
    /// Retype Password
    public static let ConfirmPasswordFieldPlaceholder31: String = CommonStrings.tr("Common", "registration.confirm_password_field_placeholder_31")
    /// Confirm Password
    public static let ConfirmPasswordFieldTitle30: String = CommonStrings.tr("Common", "registration.confirm_password_field_title_30")
    /// The date of birth you supplied does not match our records.
    public static let DateOfBirthDoesNotMatch2070: String = CommonStrings.tr("Common", "registration.date_of_birth_does_not_match_2070")
    /// name@email.com
    public static let EmailFieldPlaceholder27: String = CommonStrings.tr("Common", "registration.email_field_placeholder_27")
    /// Email Address
    public static let EmailFieldTitle26: String = CommonStrings.tr("Common", "registration.email_field_title_26")
    /// Invalid date format
    public static let InvalidDateFormat2359: String = CommonStrings.tr("Common", "registration.invalid_date_format_2359")
    /// 
    public static let InvalidDateFormat9999: String = CommonStrings.tr("Common", "registration.invalid_date_format_9999")
    /// Enter a valid email address
    public static let InvalidEmailFootnoteError35: String = CommonStrings.tr("Common", "registration.invalid_email_footnote_error_35")
    /// The email and insurer code do not match. Please try again
    public static let InvalidEmailOrRegistrationCodeAlertMessage39: String = CommonStrings.tr("Common", "registration.invalid_email_or_registration_code_alert_message_39")
    /// Incorrect Email or Insurer Code Captured
    public static let InvalidEmailOrRegistrationCodeAlertTitle38: String = CommonStrings.tr("Common", "registration.invalid_email_or_registration_code_alert_title_38")
    /// Confirm Password does not match
    public static let MismatchedPasswordFootnoteError1137: String = CommonStrings.tr("Common", "registration.mismatched_password_footnote_error_1137")
    /// Passwords do not match
    public static let MismatchedPasswordFootnoteError36: String = CommonStrings.tr("Common", "registration.mismatched_password_footnote_error_36")
    /// Must be at least 7 characters containing numbers, uppercase and lowercase letters
    public static let PasswordFieldFootnote29: String = CommonStrings.tr("Common", "registration.password_field_footnote_29")
    /// Create Password
    public static let PasswordFieldPlaceholder28: String = CommonStrings.tr("Common", "registration.password_field_placeholder_28")
    /// Registering...
    public static let RegisterActivityIndicatorLabel37: String = CommonStrings.tr("Common", "registration.register_activity_indicator_label_37")
    /// You should have received an email from your insurer with your Insurer Code. Please contact your insurer if you did not receive a code.
    public static let RegistrationCodeFieldFootnote34: String = CommonStrings.tr("Common", "registration.registration_code_field_footnote_34")
    /// Enter Code
    public static let RegistrationCodeFieldPlaceholder33: String = CommonStrings.tr("Common", "registration.registration_code_field_placeholder_33")
    /// Insurer Code
    public static let RegistrationCodeFieldTitle32: String = CommonStrings.tr("Common", "registration.registration_code_field_title_32")
    /// Register
    public static let ScreenTitle25: String = CommonStrings.tr("Common", "registration.screen_title_25")
    /// An error occurred while trying to register your details. Please try again.
    public static let UnableToRegisterAlertMessage42: String = CommonStrings.tr("Common", "registration.unable_to_register_alert_message_42")
    /// User already exists.
    public static let UnableToRegisterAlertMessage46: String = CommonStrings.tr("Common", "registration.unable_to_register_alert_message_46")
    /// Unable to Register
    public static let UnableToRegisterAlertTitle41: String = CommonStrings.tr("Common", "registration.unable_to_register_alert_title_41")
  }

  public enum Result {
    /// Negative
    public static let NegativeTitle286: String = CommonStrings.tr("Common", "result.negative_title_286")
    /// Neutral
    public static let NeutralTitle285: String = CommonStrings.tr("Common", "result.neutral_title_285")
    /// Positive
    public static let PositiveTitle284: String = CommonStrings.tr("Common", "result.positive_title_284")
  }

  public enum Review {

    public enum Intro {
      /// Complete it Now
      public static let ButtonTitle2360: String = CommonStrings.tr("Common", "review.intro.button_title_2360")
      /// 
      public static let ButtonTitle9991: String = CommonStrings.tr("Common", "review.intro.button_title_9991")
      /// Complete your Vitality Health Review
      public static let Header2361: String = CommonStrings.tr("Common", "review.intro.header_2361")
      /// 
      public static let Header9991: String = CommonStrings.tr("Common", "review.intro.header_9991")
      /// To start earning great rewards, you need to complete the Vitality Health Review in order to Activate Rewards.
      public static let Message2362: String = CommonStrings.tr("Common", "review.intro.message_2362")
      /// 
      public static let Message9991: String = CommonStrings.tr("Common", "review.intro.message_9991")
      /// Get Started
      public static let Title2363: String = CommonStrings.tr("Common", "review.intro.title_2363")
      /// 
      public static let Title9991: String = CommonStrings.tr("Common", "review.intro.title_9991")
    }
  }

  public enum Rewardpartners {
    /// Reward Partners
    public static let ScreenTitle898: String = CommonStrings.tr("Common", "RewardPartners.screen_title_898")
  }

  public enum Rewards {

    public enum Landing {
      /// Getting fit and healthy has become even more enjoyable with benefits and rewards from our partners
      public static let HeadingTitle2596: String = CommonStrings.tr("Common", "rewards.landing.heading_title_2596")
    }
  }

  public enum Sdc {
    /// Samsung Device Cashback
    public static let HomeCardTitle2368: String = CommonStrings.tr("Common", "sdc.home_card_title_2368")
    /// Qualifying Samsung Devices
    public static let LandingActionButton2369: String = CommonStrings.tr("Common", "sdc.landing_action_button_2369")
    /// To complete your activation, your device workout activity needs to sync. Please note that data can take up to 48 hours to sync to Vitality.
    public static let LandingActivitySyncPendingDescription2370: String = CommonStrings.tr("Common", "sdc.landing_activity_sync_pending_description_2370")
    /// Samsung Device Activity Sync Pending
    public static let LandingActivitySyncPendingTitle2371: String = CommonStrings.tr("Common", "sdc.landing_activity_sync_pending_title_2371")
    /// You have your device, now you need to give Vitality access to your activity by linking Samsung Health to complete your activation.
    public static let LandingContinueCashbackDescription2372: String = CommonStrings.tr("Common", "sdc.landing_continue_cashback_description_2372")
    /// Link Samsung Health to complete the cashback activation
    public static let LandingContinueCashbackTitle2373: String = CommonStrings.tr("Common", "sdc.landing_continue_cashback_title_2373")
    /// To activate your benefit, purchase a qualifying Samsung device from Airlink and provide proof of membership.\n\nPlease note that automatic activation will take up to 7 days from device purchase.
    public static let LandingHeaderMessage2374: String = CommonStrings.tr("Common", "sdc.landing_header_message_2374")
    /// Get Your Samsung Device from Airlink
    public static let LandingHeaderTitle2375: String = CommonStrings.tr("Common", "sdc.landing_header_title_2375")
    /// Order Device
    public static let NewLandingActionButton2376: String = CommonStrings.tr("Common", "sdc.new_landing_action_button_2376")
    /// To activate your benefit, login to the Muller & Phipps online store and order your Samsung device.\n\nPlease note that the cashback activation may take up to 10 days from date of purchase.
    public static let NewLandingHeaderMessage2377: String = CommonStrings.tr("Common", "sdc.new_landing_header_message_2377")
    /// Get Your Samsung Galaxy 1.3 Watch from Muller & Phipps
    public static let NewLandingHeaderTitle2378: String = CommonStrings.tr("Common", "sdc.new_landing_header_title_2378")
    /// Order your device at the Muller & Phipps online store. Only cash on delivery will be accepted, and can take up to 10 days from date of purchase.
    public static let NewOnboardingSection1Message2379: String = CommonStrings.tr("Common", "sdc.new_onboarding_section1_message_2379")
    /// Link Samsung Health to activate your benefit and earn points towards your cashback goal.
    public static let NewOnboardingSection2Message2380: String = CommonStrings.tr("Common", "sdc.new_onboarding_section2_message_2380")
    /// Earn cashbacks, paid in to your IGI Life Investment Fund Account, as you earn points and work towards getting your Samsung device for free.
    public static let NewOnboardingSection3Message2381: String = CommonStrings.tr("Common", "sdc.new_onboarding_section3_message_2381")
    /// Provide proof of your Vitality membership at an Airlink store and purchase an eligible, discounted Samsung device which will be automatically activated within 7 days.
    public static let OnboardingSection1Message2382: String = CommonStrings.tr("Common", "sdc.onboarding_section1_message_2382")
    /// Get Your Samsung Device
    public static let OnboardingSection1Title2383: String = CommonStrings.tr("Common", "sdc.onboarding_section1_title_2383")
    /// Link Samsung Health and complete a qualifying fitness activity before your benefit has been activated to earn points towards your cashback goal.
    public static let OnboardingSection2Message2384: String = CommonStrings.tr("Common", "sdc.onboarding_section2_message_2384")
    /// Earn cashbacks, paid in to your IGI Life Investment Fund Account, as you earn points and work towards getting your Samsung device for free.
    public static let OnboardingSection3Message2385: String = CommonStrings.tr("Common", "sdc.onboarding_section3_message_2385")

    public enum Activation {
      /// To earn points for your activity, please give Vitality access to Samsung Health.
      public static let AllowAccessMessage2060: String = CommonStrings.tr("Common", "SDC.activation.allow_access_message_2060")
    }

    public enum Cashback {
      /// Your cashback will be paid to your IGI Life Investment Fund Account. If you have any issues with your cashbacks please contact your insurer.
      public static let FooterMessage2061: String = CommonStrings.tr("Common", "SDC.cashback.footer_message_2061")
      /// We allow until the 6th day of the month following your target for points to be allocated before closing and processing your cashback. Your cashback will be paid to your IGI Life Investment Fund Account.
      public static let FooterMessage2062: String = CommonStrings.tr("Common", "SDC.cashback.footer_message_2062")
    }

    public enum Landing {
      /// To activate your benefit, purchase a qualifying Samsung device from Airlink and provide proof of membership.\n\nPlease note that automatic activation will take up to 7 days from device purchase.
      public static let SectionHeaderDescription2045: String = CommonStrings.tr("Common", "SDC.landing.section_header_description_2045")
      /// To complete your activation, your device workout activity needs to sync. Please note that data can take up to 48 hours to sync to Vitality.
      public static let SectionHeaderDescription2056: String = CommonStrings.tr("Common", "SDC.landing.section_header_description_2056")
      /// You have your device, you need to give Vitality access to your activity by linking Samsung Health to complete your activation.
      public static let SectionHeaderDescription2059: String = CommonStrings.tr("Common", "SDC.landing.section_header_description_2059")
      /// You are eligible to start a new Samsung Device Cashback cycle!\n\nPurchase a qualifying Samsung device and provide proof of membership. Please note that automatic activation will take up to 7 days.
      public static let SectionHeaderDescription2069: String = CommonStrings.tr("Common", "SDC.landing.section_header_description_2069")
      /// Get Your Samsung Device from Airlink
      public static let SectionHeaderTitle2044: String = CommonStrings.tr("Common", "SDC.landing.section_header_title_2044")
      /// Samsung Device Activity Sync Pending
      public static let SectionHeaderTitle2055: String = CommonStrings.tr("Common", "SDC.landing.section_header_title_2055")
      /// Link Samsung Health to complete the cashback activation
      public static let SectionHeaderTitle2058: String = CommonStrings.tr("Common", "SDC.landing.section_header_title_2058")
    }

    public enum Learnmore {
      /// Follow the process below to start earning cashback towards your Samsung device purchase.
      public static let SectionHeaderDescription2048: String = CommonStrings.tr("Common", "SDC.learnmore.section_header_description_2048")
      /// Purchase a Samsung Gear S3 classic, Gear S3 frontier, Gear Fit2 PRO or Gear Sport device from any retail store of your choice.\n\nKeep the proof of your purchase as you'll need it as part of the activation process.\n\nYou have 60 days after purchasing the device to link it with Vitality.\n\nAlready have a Samsung device?\n\nIf you already have a Samsung device add more copy here about what to do.
      public static let SectionHeaderDescription2050: String = CommonStrings.tr("Common", "SDC.learnmore.section_header_description_2050")
      /// To activate your Samsung Device Cashback, you will need to provide proof of your Vitality membership at an Airlink store.\n\nIt will take up to 7 days for your Samsung device benefit to be activated.\n\nMake sure to link Samsung Health and sync a fitness activity before your benefit has been fully activated.
      public static let SectionHeaderDescription2051: String = CommonStrings.tr("Common", "SDC.learnmore.section_header_description_2051")
      /// Earn cashback on your chosen Samsung device every month for 24 months by completing workout activities.\n\nYour cashback will be paid to your IGI Life Investment Fund Account in the month following your achieved cashback progress.
      public static let SectionHeaderDescription2052: String = CommonStrings.tr("Common", "SDC.learnmore.section_header_description_2052")
      /// How Samsung Device Cashbacks Work
      public static let SectionHeaderTitle2047: String = CommonStrings.tr("Common", "SDC.learnmore.section_header_title_2047")
      /// Get a Samsung Device
      public static let SectionHeaderTitle2049: String = CommonStrings.tr("Common", "SDC.learnmore.section_header_title_2049")
    }

    public enum NewLearnmore {
      /// Activation will take up to 10 days from date of purchase.\n\nMake sure to link Samsung Health to fully activate your cashback benefit.
      public static let SectionActivateHeaderDescription2364: String = CommonStrings.tr("Common", "sdc.new_learnmore.section_activate_header_description_2364")
      /// Order your Samsung device on the Muller & Phipps online store.\n\nDelivery will take 4 to 5 days and only cash on delivery will be accepted for your device.\n\nYou have 60 days after purchasing the device to link it with Vitality.
      public static let SectionDeviceHeaderDescription2365: String = CommonStrings.tr("Common", "sdc.new_learnmore.section_device_header_description_2365")
      /// Earn cashback on your chosen Samsung device every month for 24 months by completing workout activities.\n\nYour cashback will be paid to your IGI Life Investment Fund Account in the month following your achieved cashback progress.\n\nOnly one device will qualify for this benefit during the 24 month period.
      public static let SectionGetcashbackHeaderDescription2366: String = CommonStrings.tr("Common", "sdc.new_learnmore.section_getcashback_header_description_2366")
      /// Get Cashbacks for Being Active
      public static let SectionHeaderTitle2367: String = CommonStrings.tr("Common", "sdc.new_learnmore.section_header_title_2367")
    }

    public enum Onboarding {
      /// Provide proof of your Vitality membership at an Airlink store and purchase an eligible, discounted Samsung device which will be automatically activated within 7 days.
      public static let SectionHeaderDescription2041: String = CommonStrings.tr("Common", "SDC.Onboarding.section_header_description_2041")
      /// Link Samsung Health and complete a qualifying fitness activity before your benefit has been activated to earn points towards your cashback goal.
      public static let SectionHeaderDescription2042: String = CommonStrings.tr("Common", "SDC.Onboarding.section_header_description_2042")
      /// Earn cashbacks, paid in to your IGI Life Investment Fund Account, as you earn points and work towards getting your Samsung device for free.
      public static let SectionHeaderDescription2043: String = CommonStrings.tr("Common", "SDC.Onboarding.section_header_description_2043")
      /// Get Your Samsung Device
      public static let SectionHeaderTitle2040: String = CommonStrings.tr("Common", "SDC.Onboarding.section_header_title_2040")
    }
  }

  public enum Settings {
    /// Remove Pass
    public static let ActionRemovePassButton993: String = CommonStrings.tr("Common", "Settings.action_remove_pass_button_993")
    /// Show Pass in Wallet
    public static let ActionShowWalletButton994: String = CommonStrings.tr("Common", "Settings.action_show_wallet_button_994")
    /// Add Profile Image Later
    public static let AlertAddImageLaterButton979: String = CommonStrings.tr("Common", "Settings.alert_add_image_later_button_979")
    /// Add Profile Image Now
    public static let AlertAddImageNowButton978: String = CommonStrings.tr("Common", "Settings.alert_add_image_now_button_978")
    /// This email will be used for all Vitality communications as well as login. Please note that this will not change the contact email linked to your insurer
    public static let AlertChangeConfirmationMessage930: String = CommonStrings.tr("Common", "Settings.alert_change_confirmation_message_930")
    /// Are You Sure You Want To Change Your Email
    public static let AlertChangeConfirmationTitle929: String = CommonStrings.tr("Common", "Settings.alert_change_confirmation_title_929")
    /// Change Profile Image
    public static let AlertChangeImageButton977: String = CommonStrings.tr("Common", "Settings.alert_change_image_button_977")
    /// The current password you entered is incorrect. Please try again.
    public static let AlertChangePasswordIncorrectMessage2386: String = CommonStrings.tr("Common", "settings.alert_change_password_incorrect_message_2386")
    /// 
    public static let AlertChangePasswordIncorrectMessage9999: String = CommonStrings.tr("Common", "Settings.alert_change_password_incorrect_message_9999")
    /// An error occurred while downloading your digital pass. Please try again.
    public static let AlertDownloadIncompleteMessage982: String = CommonStrings.tr("Common", "settings.alert_download_incomplete_message_982")
    /// Unable to Complete
    public static let AlertDownloadIncompleteTitle981: String = CommonStrings.tr("Common", "Settings.alert_download_incomplete_title_981")
    /// Please ensure that your fingerprint is enrolled on this device and try again. You can enroll your fingerprint from the Settings app.
    public static let AlertFingerprintNotRecognisedMessage941: String = CommonStrings.tr("Common", "Settings.alert_fingerprint_not_recognised_message_941")
    /// Fingerprint Not Recognised
    public static let AlertFingerprintNotRecognisedTitle940: String = CommonStrings.tr("Common", "Settings.alert_fingerprint_not_recognised_title_940")
    /// Forgot Password?
    public static let AlertForgotPasswordButton935: String = CommonStrings.tr("Common", "Settings.alert_forgot_password_button_935")
    /// The password you entered is incorrect. Please try again.
    public static let AlertIncorrectPasswordMessage934: String = CommonStrings.tr("Common", "Settings.alert_incorrect_password_message_934")
    /// Incorrect Password
    public static let AlertIncorrectPasswordTitle933: String = CommonStrings.tr("Common", "Settings.alert_incorrect_password_title_933")
    /// Are you sure you want to log out?
    public static let AlertLogoutMessage910: String = CommonStrings.tr("Common", "Settings.alert_logout_message_910")
    /// Log Out
    public static let AlertLogoutTitle909: String = CommonStrings.tr("Common", "Settings.alert_logout_title_909")
    /// Enter your Vitality password to confirm the change of your email to %1$@
    public static func AlertPasswordRequiredMessage2624(_ p1: String) -> String {
      return CommonStrings.tr("Common", "settings.alert_password_required_message_2624", p1)
    }
    /// Enter your Vitality password to confirm the change of your email to access this information.
    public static let AlertPasswordRequiredMessage932: String = CommonStrings.tr("Common", "Settings.alert_password_required_message_932")
    /// Enter your Vitality password to access this information.
    public static let AlertPasswordRequiredMessage942: String = CommonStrings.tr("Common", "Settings.alert_password_required_message_942")
    /// Password Required
    public static let AlertPasswordRequiredTitle931: String = CommonStrings.tr("Common", "Settings.alert_password_required_title_931")
    /// Your profile image will be used for verification purposes with qualifying partners.
    public static let AlertProvideProfileImageMessage975: String = CommonStrings.tr("Common", "Settings.alert_provide_profile_image_message_975")
    /// Provide a Profile Image for your Vitality Digital Pass
    public static let AlertProvideProfileImageTitle974: String = CommonStrings.tr("Common", "Settings.alert_provide_profile_image_title_974")
    /// Remove
    public static let AlertRemovePassButton992: String = CommonStrings.tr("Common", "Settings.alert_remove_pass_button_992")
    /// Are you sure you want to remove this pass?
    public static let AlertRemovePassMessage991: String = CommonStrings.tr("Common", "Settings.alert_remove_pass_message_991")
    /// Remove Pass
    public static let AlertRemovePassTitle990: String = CommonStrings.tr("Common", "Settings.alert_remove_pass_title_990")
    /// The email address you've entered is already in use. Please enter a different email address.
    public static let AlertUnableToChangeMessage928: String = CommonStrings.tr("Common", "Settings.alert_unable_to_change_message_928")
    /// Unable to Change Password
    public static let AlertUnableToChangePasswordTitle1162: String = CommonStrings.tr("Common", "Settings.alert_unable_to_change_password_title_1162")
    /// Unable to Change Email
    public static let AlertUnableToChangeTitle927: String = CommonStrings.tr("Common", "Settings.alert_unable_to_change_title_927")
    /// Use Existing Profile Image
    public static let AlertUseExistingButton976: String = CommonStrings.tr("Common", "Settings.alert_use_existing_button_976")
    /// App Notification Preferences
    public static let AppNotificationsTitle1165: String = CommonStrings.tr("Common", "settings.app_notifications_title_1165")
    /// Upload
    public static let AttachmentEmptyButton962: String = CommonStrings.tr("Common", "Settings.attachment_empty_button_962")
    /// Upload an attachment of the issue you would like to provide feedback on.
    public static let AttachmentEmptyMessage961: String = CommonStrings.tr("Common", "Settings.attachment_empty_message_961")
    /// Upload Attachment
    public static let AttachmentEmptyTitle960: String = CommonStrings.tr("Common", "Settings.attachment_empty_title_960")
    /// If this email address is incorrect, you can edit it from your profile.
    public static let ChangeEmailIncorrect1164: String = CommonStrings.tr("Common", "settings.change_email_incorrect_1164")
    /// Current Password
    public static let ChangePassword1Placeholder996: String = CommonStrings.tr("Common", "Settings.change_password_1_placeholder_996")
    /// Password
    public static let ChangePassword1Title995: String = CommonStrings.tr("Common", "Settings.change_password_1_title_995")
    /// At least 7 characters
    public static let ChangePassword2Placeholder998: String = CommonStrings.tr("Common", "Settings.change_password_2_placeholder_998")
    /// New Password
    public static let ChangePassword2Title997: String = CommonStrings.tr("Common", "Settings.change_password_2_title_997")
    /// Confirm Password
    public static let ChangePassword3Placeholder1000: String = CommonStrings.tr("Common", "Settings.change_password_3_placeholder_1000")
    /// Confirm Password
    public static let ChangePassword3Title2387: String = CommonStrings.tr("Common", "settings.change_password_3_title_2387")
    /// 
    public static let ChangePassword3Title999: String = CommonStrings.tr("Common", "Settings.change_password_3_title_999")
    /// Communication Preferences
    public static let CommunicationTitle2443: String = CommonStrings.tr("Common", "settings.communication_title_2443")
    /// Communication Preferences
    public static let CommunicationTitle902: String = CommonStrings.tr("Common", "Settings.communication_title_902")
    /// Data Sharing Consent
    public static let DataSharingConsentTitle2442: String = CommonStrings.tr("Common", "settings.data_sharing_consent_title_2442")
    /// Downloading digital pass...
    public static let DownloadingPassTitle980: String = CommonStrings.tr("Common", "Settings.downloading_pass_title_980")
    /// Enable email communication to retrieve important information and updates.\n\nEmails will be sent to %1$@. If this address is incorrect please contact your insurer .
    public static func EmailCommunication985(_ p1: String) -> String {
      return CommonStrings.tr("Common", "Settings.email_communication_985", p1)
    }
    /// Your email preference could not be updated
    public static let EmailPreferenceError2388: String = CommonStrings.tr("Common", "settings.email_preference_error_2388")
    /// Emails will be sent to %1$@. If this address is incorrect please contact your insurer .
    public static func EmailWillBeSentTo1167(_ p1: String) -> String {
      return CommonStrings.tr("Common", "settings.email_will_be_sent_to_1167", p1)
    }
    /// All Events
    public static let EventsAllTitle936: String = CommonStrings.tr("Common", "Settings.events_all_title_936")
    /// There is no events activity recorded for the selected month.
    public static let EventsEmptyErrorMessage938: String = CommonStrings.tr("Common", "Settings.events_empty_error_message_938")
    /// No Events Activity
    public static let EventsEmptyErrorTitle937: String = CommonStrings.tr("Common", "Settings.events_empty_error_title_937")
    /// Choose Events Activity
    public static let EventsPopoverTitle939: String = CommonStrings.tr("Common", "Settings.events_popover_title_939")
    /// Upload Attachment
    public static let FeedbackAttachmentsPlaceholder948: String = CommonStrings.tr("Common", "Settings.feedback_attachments_placeholder_948")
    /// Attachments
    public static let FeedbackAttachmentsTitle947: String = CommonStrings.tr("Common", "Settings.feedback_attachments_title_947")
    /// These details will be used to contact you regarding the feedback we receive.
    public static let FeedbackContactDetailsFootnote953: String = CommonStrings.tr("Common", "Settings.feedback_contact_details_footnote_953")
    /// Contact Details
    public static let FeedbackContactDetailsSectionHeading952: String = CommonStrings.tr("Common", "Settings.feedback_contact_details_section_heading_952")
    /// Information about your device, account and this app will be automatically included in this report.
    public static let FeedbackFormFootnoteMessage951: String = CommonStrings.tr("Common", "Settings.feedback_form_footnote_message_951")
    /// Your feedback is important to us
    public static let FeedbackGroupHeaderTitle1106: String = CommonStrings.tr("Common", "settings.feedback_group_header_title_1106")
    /// We always want to ensure that our members get the best experience, If you have any feedback, contact us at feedback@essentials.co.uk.\n\nIt typically takes 24 hours to respond to feedback.
    public static let FeedbackMessage1107: String = CommonStrings.tr("Common", "settings.feedback_message_1107")
    /// We always want to ensure that our members get the best experience, If you have any feedback, contact us at <a href="mailto:feedback@essentials.co.uk">feedback@essentials.co.uk</a>.\n\nIt typically takes 24 hours to respond to feedback.
    public static let FeedbackMessageWithURL1107: String = CommonStrings.tr("Common", "settings.feedback_message_with_URL_1107")
    /// Write your feedback
    public static let FeedbackPlaceholder987: String = CommonStrings.tr("Common", "Settings.feedback_placeholder_987")
    /// Provide Feedback...
    public static let FeedbackSectionPlaceholder950: String = CommonStrings.tr("Common", "Settings.feedback_section_placeholder_950")
    /// Feedback
    public static let FeedbackSectionTitle949: String = CommonStrings.tr("Common", "Settings.feedback_section_title_949")
    /// Select All
    public static let FeedbackSelectAll2093: String = CommonStrings.tr("Common", "Settings.feedback_select_all_2093")
    /// Send
    public static let FeedbackSendButton986: String = CommonStrings.tr("Common", "Settings.feedback_send_button_986")
    /// Choose Feedback Type
    public static let FeedbackSubjectPlaceholder946: String = CommonStrings.tr("Common", "Settings.feedback_subject_placeholder_946")
    /// Subject
    public static let FeedbackSubjectTitle945: String = CommonStrings.tr("Common", "Settings.feedback_subject_title_945")
    /// Thank you, your feedback has been submitted successfully.
    public static let FeedbackSubmittedMessage964: String = CommonStrings.tr("Common", "Settings.feedback_submitted_message_964")
    /// Feedback submitted successfully
    public static let FeedbackSubmittedSuccessfullyMessage988: String = CommonStrings.tr("Common", "Settings.feedback_submitted_successfully_message_988")
    /// Feedback Submitted
    public static let FeedbackSubmittedTitle963: String = CommonStrings.tr("Common", "Settings.feedback_submitted_title_963")
    /// Provide Feedback
    public static let FeedbackTitle2444: String = CommonStrings.tr("Common", "settings.feedback_title_2444")
    /// Provide Feedback
    public static let FeedbackTitle906: String = CommonStrings.tr("Common", "Settings.feedback_title_906")
    /// Technical Assistance
    public static let FeedbackType1Heading954: String = CommonStrings.tr("Common", "Settings.feedback_type_1_heading_954")
    /// Tell us if you are experiencing any technical issues like bugs, app crashes etc.
    public static let FeedbackType1Message955: String = CommonStrings.tr("Common", "Settings.feedback_type_1_message_955")
    /// General Assistance
    public static let FeedbackType2Heading956: String = CommonStrings.tr("Common", "Settings.feedback_type_2_heading_956")
    /// Provide general feedback regarding the application and suggest improvements.
    public static let FeedbackType2Message957: String = CommonStrings.tr("Common", "Settings.feedback_type_2_message_957")
    /// App Experience User Feedback
    public static let FeedbackType3Heading958: String = CommonStrings.tr("Common", "Settings.feedback_type_3_heading_958")
    /// Provide general feedback regarding the overall Vitality app experience.
    public static let FeedbackType3Message959: String = CommonStrings.tr("Common", "Settings.feedback_type_3_message_959")
    /// The use of fingerprint for this app has been disabled and can be enabled again from settings.
    public static let FingerprintDisabledMessage2389: String = CommonStrings.tr("Common", "settings.fingerprint_disabled_message_2389")
    /// 
    public static let FingerprintDisabledMessage9999: String = CommonStrings.tr("Common", "Settings.fingerprint_disabled_message_9999")
    /// General
    public static let GeneralSectionHeading983: String = CommonStrings.tr("Common", "Settings.general_section_heading_983")
    /// Events Feed
    public static let LandingEventsTitle2445: String = CommonStrings.tr("Common", "settings.landing_events_title_2445")
    /// Events Feed
    public static let LandingEventsTitle900: String = CommonStrings.tr("Common", "Settings.landing_events_title_900")
    /// Membership Pass
    public static let LandingMembershipPassTitle2446: String = CommonStrings.tr("Common", "settings.landing_membership_pass_title_2446")
    /// Settings
    public static let LandingSettingsTitle2447: String = CommonStrings.tr("Common", "settings.landing_settings_title_2447")
    /// Settings
    public static let LandingSettingsTitle899: String = CommonStrings.tr("Common", "Settings.landing_settings_title_899")
    /// Profile
    public static let LandingTitle2448: String = CommonStrings.tr("Common", "settings.landing_title_2448")
    /// Profile
    public static let LandingTitle901: String = CommonStrings.tr("Common", "Settings.landing_title_901")
    /// Are you sure you want to log out?
    public static let LogoutPrompt2449: String = CommonStrings.tr("Common", "settings.logout_prompt_2449")
    /// Log Out
    public static let LogoutTitle2550: String = CommonStrings.tr("Common", "settings.logout_title_2550")
    /// Log Out
    public static let LogoutTitle908: String = CommonStrings.tr("Common", "Settings.logout_title_908")
    /// Add to Apple Wallet
    public static let MembershipAddButton971: String = CommonStrings.tr("Common", "Settings.membership_add_button_971")
    /// Membership Details
    public static let MembershipDetailsSectionHeading965: String = CommonStrings.tr("Common", "Settings.membership_details_section_heading_965")
    /// Use your Vitality Pass to claim your rewards at all qualifying partners.
    public static let MembershipDigitalPassFootnote989: String = CommonStrings.tr("Common", "Settings.membership_digital_pass_footnote_989")
    /// Add your digital pass to your Apple Wallet for use at qualifying partners.
    public static let MembershipDigitalPassMessage972: String = CommonStrings.tr("Common", "Settings.membership_digital_pass_message_972")
    /// Digital Pass
    public static let MembershipDigitalPassSectionHeading970: String = CommonStrings.tr("Common", "Settings.membership_digital_pass_section_heading_970")
    /// More Information
    public static let MembershipMoreInformation973: String = CommonStrings.tr("Common", "Settings.membership_more_information_973")
    /// Party ID
    public static let MembershipPartyIdTitle1105: String = CommonStrings.tr("Common", "settings.membership_party_id_title_1105")
    /// Membership Pass
    public static let MembershipPass911: String = CommonStrings.tr("Common", "Settings.membership_pass_911")
    /// Membership Start Date
    public static let MembershipStartDateTitle968: String = CommonStrings.tr("Common", "Settings.membership_start_date_title_968")
    /// Membership Status
    public static let MembershipStatus969: String = CommonStrings.tr("Common", "Settings.membership_status_969")
    /// Vitality Number
    public static let MembershipVitalityNumberTitle966: String = CommonStrings.tr("Common", "Settings.membership_vitality_number_title_966")
    /// Vitality Status
    public static let MembershipVitalityStatusTitle967: String = CommonStrings.tr("Common", "Settings.membership_vitality_status_title_967")
    /// Mobile Terms and Conditions
    public static let MobileTermsConditionsTitle2441: String = CommonStrings.tr("Common", "settings.mobile_terms_conditions_title_2441")
    /// Must not contain special characters.
    public static let NoSpecialCharactersValidationMessage1166: String = CommonStrings.tr("Common", "settings.no_special_characters_validation_message_1166")
    /// Nuffield Health
    public static let NuffieldHealthTitle2390: String = CommonStrings.tr("Common", "settings.nuffield_health_title_2390")
    /// You have successfully changed your password. A confirmation email has been sent to your account. \n
    public static let PasswordChangedMessage944: String = CommonStrings.tr("Common", "Settings.password_changed_message_944")
    /// Password Changed
    public static let PasswordChangedTitle943: String = CommonStrings.tr("Common", "Settings.password_changed_title_943")
    /// Privacy
    public static let PrivacyTitle2551: String = CommonStrings.tr("Common", "settings.privacy_title_2551")
    /// Privacy
    public static let PrivacyTitle903: String = CommonStrings.tr("Common", "Settings.privacy_title_903")
    /// Change Email
    public static let ProfileChangeEmailTitle921: String = CommonStrings.tr("Common", "Settings.profile_change_email_title_921")
    /// Done
    public static let ProfileChangePasswordConfirmationDoneButtonTitle2394: String = CommonStrings.tr("Common", "settings.profile_change_password_confirmation_done_button_title_2394")
    /// Current Email
    public static let ProfileCurrentEmailTitle922: String = CommonStrings.tr("Common", "Settings.profile_current_email_title_922")
    /// Maximum %1$@ character limit reached
    public static func ProfileEmailErrorCharacterLimit926(_ p1: String) -> String {
      return CommonStrings.tr("Common", "Settings.profile_email_error_character_limit_926", p1)
    }
    /// Enter a valid email address
    public static let ProfileEmailErrorMessage925: String = CommonStrings.tr("Common", "Settings.profile_email_error_message_925")
    /// This email will be used for all Vitality communications as well as login.\n\n  Changing this email will not affect your email with your insurer, contact them if you would like to update it.
    public static let ProfileEmailFootnote924: String = CommonStrings.tr("Common", "Settings.profile_email_footnote_924")
    /// Entity Number
    public static let ProfileEntityNumberTitle1104: String = CommonStrings.tr("Common", "settings.profile_entity_number_title_1104")
    /// Date of Birth
    public static let ProfileLandingDateOfBirth916: String = CommonStrings.tr("Common", "Settings.profile_landing_date_of_birth_916")
    /// This information comes from your insurer. If it's incorrect please contact them.
    public static let ProfileLandingDetailsFootnoteMessage918: String = CommonStrings.tr("Common", "Settings.profile_landing_details_footnote_message_918")
    /// This email will be used for all Vitality communication as well as login.
    public static let ProfileLandingEmailFootnote920: String = CommonStrings.tr("Common", "Settings.profile_landing_email_footnote_920")
    /// Email
    public static let ProfileLandingEmailTitle919: String = CommonStrings.tr("Common", "Settings.profile_landing_email_title_919")
    /// Gender
    public static let ProfileLandingGenderTitle917: String = CommonStrings.tr("Common", "Settings.profile_landing_gender_title_917")
    /// Mobile
    public static let ProfileLandingMobileTitle915: String = CommonStrings.tr("Common", "Settings.profile_landing_mobile_title_915")
    /// Personal Details
    public static let ProfileLandingPersonalDetailsTitle912: String = CommonStrings.tr("Common", "Settings.profile_landing_personal_details_title_912")
    /// Edit
    public static let ProfileLandingProfilePictureEdit913: String = CommonStrings.tr("Common", "Settings.profile_landing_profile_picture_edit_913")
    /// Personal Details
    public static let ProfileLandingProfilePictureSectionHeading914: String = CommonStrings.tr("Common", "Settings.profile_landing_profile_picture_section_heading_914")
    /// New Email
    public static let ProfileNewEmailTitle923: String = CommonStrings.tr("Common", "Settings.profile_new_email_title_923")
    /// Notifications may include alerts, sounds and icon badges. These can be configured in Settings
    public static let ProfileNotificationAlertTitle2395: String = CommonStrings.tr("Common", "Settings.profile_notification_alert_title_2395")
    /// Use the email address your registration code was sent to when you joined Vitality.
    public static let ProfileRegistrationCodeSentVitality2396: String = CommonStrings.tr("Common", "settings.profile_registration_code_sent_vitality_2396")
    /// Rate Us
    public static let RateTitle2552: String = CommonStrings.tr("Common", "settings.rate_title_2552")
    /// Rate Us
    public static let RateUsTitle907: String = CommonStrings.tr("Common", "Settings.rate_us_title_907")
    /// Change Password
    public static let SecurityChangePasswordTitle827: String = CommonStrings.tr("Common", "Settings.security_change_password_title_827")
    /// Please enable the fingerprint permission
    public static let SecurityFingerPrintEnablePermission1161: String = CommonStrings.tr("Common", "settings.security_finger_print_enable_permission_1161")
    /// Your device doesn't support fingerprint authentication.
    public static let SecurityFingerPrintNotAvailable1163: String = CommonStrings.tr("Common", "settings.security_finger_print_not_available_1163")
    /// Fingerprint not recognized. Try again.
    public static let SecurityFingerPrintNotRecognized1159: String = CommonStrings.tr("Common", "settings.security_finger_print_not_recognized_1159")
    /// Confirm fingerprint to continue
    public static let SecurityFingerPrintPromptConfirm1157: String = CommonStrings.tr("Common", "settings.security_finger_print_prompt_confirm_1157")
    /// Fingerprint recognized
    public static let SecurityFingerPrintRecognized1158: String = CommonStrings.tr("Common", "settings.security_finger_print_recognized_1158")
    /// Register at least one fingerprint in Settings
    public static let SecurityFingerPrintRegisterSettings1160: String = CommonStrings.tr("Common", "settings.security_finger_print_register_settings_1160")
    /// Using Fingerprint allows you to securely verify your identity so Vitality can access your private information. Fingerprint means that you don't have to type in your password each time to access the app.
    public static let SecurityFingerPrintTextSettingSli1156: String = CommonStrings.tr("Common", "settings.security_finger_print_text_setting_sli_1156")
    /// Security
    public static let SecurityTitle2553: String = CommonStrings.tr("Common", "settings.security_title_2553")
    /// Security
    public static let SecurityTitle904: String = CommonStrings.tr("Common", "Settings.security_title_904")
    /// Send Feedback
    public static let SendFeedbackTitle984: String = CommonStrings.tr("Common", "Settings.send_feedback_title_984")
    /// Terms and Conditions
    public static let TermsConditionsTitle905: String = CommonStrings.tr("Common", "Settings.terms_conditions_title_905")
    /// Terms and Conditions
    public static let TermsTitle2554: String = CommonStrings.tr("Common", "settings.terms_title_2554")
    /// Unit Preferences
    public static let UnitsTitle2555: String = CommonStrings.tr("Common", "settings.units_title_2555")

    public enum Profile {
      /// This email will be used for all Vitality communications as well as login. \n\nChanging this email will not affect your email with your insurer, contact %1$@ if you would like to update it.
      public static func ChangeEmailFooterOption12391(_ p1: String) -> String {
        return CommonStrings.tr("Common", "settings.profile.change_email_footer_option_1_2391", p1)
      }
      /// 
      public static let ChangeEmailFooterOption19999: String = CommonStrings.tr("Common", "settings.profile.change_email_footer_option_1_9999")
      /// This email will be used for all Vitality communications. \n\nChanging this email will not affect your login username and security related communications email, contact %1$@ if you would like to update it.
      public static func ChangeEmailFooterOption32392(_ p1: String) -> String {
        return CommonStrings.tr("Common", "settings.profile.change_email_footer_option_3_2392", p1)
      }
      /// 
      public static let ChangeEmailFooterOption39999: String = CommonStrings.tr("Common", "settings.profile.change_email_footer_option_3_9999")
      /// This information comes from your insurer. To update, please contact %1$@.
      public static func PersonalDetailsFooterOption42393(_ p1: String) -> String {
        return CommonStrings.tr("Common", "settings.profile.personal_details_footer_option_4_2393", p1)
      }
      /// 
      public static let PersonalDetailsFooterOption49999: String = CommonStrings.tr("Common", "settings.profile.personal_details_footer_option_4_9999")
    }
  }

  public enum Softbank {
    /// All members can issue a discount blood test coupon (5-10% off), and those who purchase smartphones can also issue free coupons.
    public static let PartnersCardHeaderContent2397: String = CommonStrings.tr("Common", "softbank.partners_card_header_content_2397")
    /// Visit a partner
    public static let PartnersCardHeaderTitle2398: String = CommonStrings.tr("Common", "softbank.partners_card_header_title_2398")
    /// Our partners
    public static let PartnersPageTitle2399: String = CommonStrings.tr("Common", "softbank.partners_page_title_2399")
  }

  public enum Splash {
    /// Vitality Logo
    public static let LogoDescriptionAoda2486: String = CommonStrings.tr("Common", "splash.logo_description_aoda_2486")
  }

  public enum Statu {
    /// You're on %1$@. You can enjoy %2$@ rewards this year and next year.
    public static func LandingTotalPointsFinalMessage825(_ p1: String, _ p2: String) -> String {
      return CommonStrings.tr("Common", "Statu.landing_total_points_final_message_825", p1, p2)
    }
  }

  public enum Status {
    /// Advanced Screening
    public static let AdvancedScreeningTitle2104: String = CommonStrings.tr("Common", "Status.advanced_screening_title_2104")
    /// VIEW CASHBACK
    public static let CardActionTitleViewCashback2511: String = CommonStrings.tr("Common", "Status.card_action_title_view_cashback_2511")
    /// Way to go! You've reach %1$@ status again which you can enjoy the same rewards next year.
    public static func IncreaseStatusAgainMessage832(_ p1: String) -> String {
      return CommonStrings.tr("Common", "Status.increase_status_again_message_832", p1)
    }
    /// Way to go, you've reached the final status! You can now enjoy %1$@ rewards this year and next year.
    public static func IncreasedStatusFinalMessage823(_ p1: String) -> String {
      return CommonStrings.tr("Common", "Status.increased_status_final_message_823", p1)
    }
    /// Next Status
    public static let IncreasedStatusGroupHeading812: String = CommonStrings.tr("Common", "Status.increased_status_group_heading_812")
    /// Status Increased
    public static let IncreasedStatusMainTitle809: String = CommonStrings.tr("Common", "Status.increased_status_main_title_809")
    /// Way to go! You've moved onto %1$@ status which means that your rewards have improved as well.
    public static func IncreasedStatusMessage811(_ p1: String) -> String {
      return CommonStrings.tr("Common", "Status.increased_status_message_811", p1)
    }
    /// Earn another %1$@ points
    public static func IncreasedStatusPointsNeeded813(_ p1: String) -> String {
      return CommonStrings.tr("Common", "Status.increased_status_points_needed_813", p1)
    }
    /// %1$@ Status Reached!
    public static func IncreasedStatusTitle810(_ p1: String) -> String {
      return CommonStrings.tr("Common", "Status.increased_status_title_810", p1)
    }
    /// This Year's Starting Status
    public static let InformationGroupHeader817: String = CommonStrings.tr("Common", "Status.information_group_header_817")
    /// Status Information
    public static let InformationMainTitle814: String = CommonStrings.tr("Common", "Status.information_main_title_814")
    /// You reached %1$@ status last membership year which means you can enjoy those rewards right from the beginning of this year.\n\nReach %2$@ again this year to enjoy the same rewards next year.
    public static func InformationMessage816(_ p1: String, _ p2: String) -> String {
      return CommonStrings.tr("Common", "Status.information_message_816", p1, p2)
    }
    /// Every year your points are reset. Start earning points to get to %1$@ status.
    public static func InformationStartingMessage818(_ p1: String) -> String {
      return CommonStrings.tr("Common", "Status.information_starting_message_818", p1)
    }
    /// Well Done on Reaching %1$@ Status Last Year!
    public static func InformationTitle815(_ p1: String) -> String {
      return CommonStrings.tr("Common", "Status.information_title_815", p1)
    }
    /// My Rewards
    public static let LandingButtonRewardsTitle798: String = CommonStrings.tr("Common", "Status.landing_button_rewards_title_798")
    /// Status carried over from last year
    public static let LandingCarryOverMessage820: String = CommonStrings.tr("Common", "Status.landing_carry_over_message_820")
    /// Earning Points
    public static let LandingEarningPointsGroupHeader803: String = CommonStrings.tr("Common", "Status.landing_earning_points_group_header_803")
    /// Points Limit Reached
    public static let LandingEarningPointsLimitMessage822: String = CommonStrings.tr("Common", "Status.landing_earning_points_limit_message_822")
    /// Status
    public static let LandingMainTitleStatus796: String = CommonStrings.tr("Common", "Status.landing_main_title_status_796")
    /// Well done!
    public static let LandingPointsFinalMessage824: String = CommonStrings.tr("Common", "Status.landing_points_final_message_824")
    /// %1$@ points to reach %2$@ again
    public static func LandingPointsMaintainMessage819(_ p1: String, _ p2: String) -> String {
      return CommonStrings.tr("Common", "Status.landing_points_maintain_message_819", p1, p2)
    }
    /// You're currently on %1$@ status. You have %2$@ to reach the next status. The status you reach this year will determine the rewards you enjoy this year and next year.
    public static func LandingPointsMessageNextStatus831(_ p1: String, _ p2: String) -> String {
      return CommonStrings.tr("Common", "Status.landing_points_message_next_status_831", p1, p2)
    }
    /// %1$@ Points to Reach %2$@
    public static func LandingPointsTargetMessage797(_ p1: String, _ p2: String) -> String {
      return CommonStrings.tr("Common", "Status.landing_points_target_message_797", p1, p2)
    }
    /// You're currently on %1$@ status. You have %2$@ days to improve your status level to %3$@. The status you reach this year will determine the rewards you enjoy this year and next year.
    public static func LandingPointsTotalMessage800(_ p1: String, _ p2: String, _ p3: String) -> String {
      return CommonStrings.tr("Common", "Status.landing_points_total_message_800", p1, p2, p3)
    }
    /// This Year's Total Points
    public static let LandingPointsTotalTitle799: String = CommonStrings.tr("Common", "Status.landing_points_total_title_799")
    /// Starting Status
    public static let LandingStatusTracking1Message801: String = CommonStrings.tr("Common", "Status.landing_status_tracking_1_message_801")
    /// Every year your points are reset. You're currently on %1$@ status. You have %2$@ days to increase your status. The status you reach this year will determine the rewards you enjoy next year.
    public static func LandingTotalPointsMessage821(_ p1: String, _ p2: String) -> String {
      return CommonStrings.tr("Common", "Status.landing_total_points_message_821", p1, p2)
    }
    /// More Information
    public static let LandingTrackingInfoIcon802: String = CommonStrings.tr("Common", "Status.landing_tracking_info_icon_802")
    /// Blue
    public static let LevelBlueTitle833: String = CommonStrings.tr("Common", "Status.level_blue_title_833")
    /// Bronze
    public static let LevelBronzeTitle834: String = CommonStrings.tr("Common", "Status.level_bronze_title_834")
    /// Gold
    public static let LevelGoldTitle836: String = CommonStrings.tr("Common", "Status.level_gold_title_836")
    /// Platinum
    public static let LevelPlatinumTitle837: String = CommonStrings.tr("Common", "Status.level_platinum_title_837")
    /// Silver
    public static let LevelSilverTitle835: String = CommonStrings.tr("Common", "Status.level_silver_title_835")
    /// Your current rewards are based on your %1$@ status level. The higher your status, the greater your rewards.
    public static func MyRewardsStatus1Message805(_ p1: String) -> String {
      return CommonStrings.tr("Common", "Status.my_rewards_status_1_message_805", p1)
    }
    /// Reach %1$@ status to enjoy these rewards.
    public static func MyRewardsStatus2Message806(_ p1: String) -> String {
      return CommonStrings.tr("Common", "Status.my_rewards_status_2_message_806", p1)
    }
    /// Your current rewards are based on your %1$@ status level.
    public static func MyRewardsStatus2Message826(_ p1: String) -> String {
      return CommonStrings.tr("Common", "Status.my_rewards_status_2_message_826", p1)
    }
    /// %1$@ Rewards
    public static func MyRewardsStatusTitle804(_ p1: String) -> String {
      return CommonStrings.tr("Common", "Status.my_rewards_status_title_804", p1)
    }
    /// Vitality Status is an indication of your progress, health and fitness.
    public static let OnboardingSection1Message604: String = CommonStrings.tr("Common", "Status.onboarding_section_1_message_604")
    /// What is Vitality Status?
    public static let OnboardingSection1Title603: String = CommonStrings.tr("Common", "Status.onboarding_section_1_title_603")
    /// Increasing your status increases your rewards. You can increase your status level by earning points for being active and completing assessments about your health.
    public static let OnboardingSection2Message606: String = CommonStrings.tr("Common", "Status.onboarding_section_2_message_606")
    /// Increase Your Status
    public static let OnboardingSection2Title605: String = CommonStrings.tr("Common", "Status.onboarding_section_2_title_605")
    /// Vitality Status
    public static let OnboardingTitle602: String = CommonStrings.tr("Common", "Status.onboarding_title_602")
    /// Earning points and points limits for activities
    public static let PointsDetailTitle807: String = CommonStrings.tr("Common", "Status.points_detail_title_807")
    /// You can earn a maximum of %1$@ points per membership year for %2$@ activities.\n\nOnce your %3$@ limit is reached, qualifying points will still go towards Vitality Active Rewards and Apple Watch Benefit.
    public static func PointsDetailTitle808(_ p1: String, _ p2: String, _ p3: String) -> String {
      return CommonStrings.tr("Common", "Status.points_detail_title_808", p1, p2, p3)
    }
    /// %1$@ points earned
    public static func PointsEarnedIndicationMessage896(_ p1: String) -> String {
      return CommonStrings.tr("Common", "Status.points_earned_indication_message_896", p1)
    }
    /// Earn %1$@ points
    public static func PointsIndicationMessage829(_ p1: String) -> String {
      return CommonStrings.tr("Common", "Status.points_indication_message_829", p1)
    }
    /// Earn up to %1$@ points limit
    public static func PointsIndicationMessageLimit830(_ p1: String) -> String {
      return CommonStrings.tr("Common", "Status.points_indication_message_limit_830", p1)
    }
    /// Earn up to %1$@ points
    public static func PointsIndicationMessageUpTo828(_ p1: String) -> String {
      return CommonStrings.tr("Common", "Status.points_indication_message_up_to_828", p1)
    }
    /// Earn %1$@ Vitality points for participating in a parkrun.
    public static func PointsParticipatingParkrun2096(_ p1: String) -> String {
      return CommonStrings.tr("Common", "status.points_participating_parkrun_2096", p1)
    }
    /// %1$@ points
    public static func PointsProgress838(_ p1: String) -> String {
      return CommonStrings.tr("Common", "Status.points_progress_838", p1)
    }
    /// Earn %1$@  Vitality points for volunteering in a parkrun.
    public static func PointsVolunteeringParkrun2097(_ p1: String) -> String {
      return CommonStrings.tr("Common", "status.points_volunteering_parkrun_2097", p1)
    }
    /// Vaccination
    public static let VaccinationTitle2105: String = CommonStrings.tr("Common", "Status.vaccination_title_2105")

    public enum EarnPoints {
      /// Abdominal aortic ultrasound
      public static let AbdominalAorticUltrasound1178: String = CommonStrings.tr("Common", "Status.earn_points.abdominal_aortic_ultrasound_1178")
      /// Cotinine test pass
      public static let CotinineTestPass1186: String = CommonStrings.tr("Common", "Status.earn_points.cotinine_test_pass_1186")
      /// Dental check-up
      public static let DentalCheckup1180: String = CommonStrings.tr("Common", "Status.earn_points.dental_checkup_1180")
      /// Eye Test
      public static let EyeTest1177: String = CommonStrings.tr("Common", "Status.earn_points.eye_test_1177")
      /// Faecal occult blood test
      public static let FaecalOccultBloodTest1183: String = CommonStrings.tr("Common", "Status.earn_points.faecal_occult_blood_test_1183")
      /// Fitness assessment healthy range
      public static let FitnessAssessmentHealthyRange1187: String = CommonStrings.tr("Common", "Status.earn_points.fitness_assessment_healthy_range_1187")
      /// Influenza vaccination
      public static let InfluenzaVaccination1181: String = CommonStrings.tr("Common", "Status.earn_points.influenza_vaccination_1181")
      /// Mammogram
      public static let Mammogram1182: String = CommonStrings.tr("Common", "Status.earn_points.mammogram_1182")
      /// Pap smear
      public static let PapSmear1185: String = CommonStrings.tr("Common", "Status.earn_points.pap_smear_1185")
      /// Pneumococcal vaccination
      public static let PneumococcalVaccination1179: String = CommonStrings.tr("Common", "Status.earn_points.pneumococcal_vaccination_1179")
      /// Shingles vaccination (Zoster)
      public static let ShinglesVaccinationZoster1184: String = CommonStrings.tr("Common", "Status.earn_points.shingles_vaccination_zoster_1184")
    }

    public enum Landing {
      /// Loading...
      public static let LoadingText894: String = CommonStrings.tr("Common", "Status.Landing.LoadingText_894")
    }

    public enum LearnMore {
      /// The higher your Vitality Status, the greater your rewards will be from our participating partners.\n\nThe status you achieve each year will carry over into the next year.
      public static let Section1Content743: String = CommonStrings.tr("Common", "Status.learn_more.section1_content_743")
      /// Increase Your Status. Earn Greater Rewards.
      public static let Section1Title742: String = CommonStrings.tr("Common", "Status.learn_more.section1_title_742")
      /// How Vitality Status Works
      public static let Section1Title794: String = CommonStrings.tr("Common", "Status.learn_more.section1_title_794")
      /// Increase Your Status. Earn Greater Rewards.
      public static let Section2Title795: String = CommonStrings.tr("Common", "Status.learn_more.section2_title_795")
      /// Vitality Status is an indication of your progress, health and fitness. You can increase your status level by earning points for being active and completing assessments about your health.
      public static let Subtitle741: String = CommonStrings.tr("Common", "Status.learn_more.subtitle_741")
    }

    public enum MyRewards {

      public enum Insurance {
        /// No Insurance Reward
        public static let NoRewardHeaderTitle2514: String = CommonStrings.tr("Common", "Status.my_rewards.insurance.no_reward_header_title_2514")
        /// You have not yet earned any Vitality points. Earn points to qualify for an insurance reward. Increase your Status or Insurance premium to increase your cashback.
        public static let NoRewardMessage2513: String = CommonStrings.tr("Common", "Status.my_rewards.insurance.no_reward_message_2513")
        /// The following insurance reward is based on %1$@ Status level. Increase your Status or Insurance premium to increase your cashback.
        public static func RewardMessage2512(_ p1: String) -> String {
          return CommonStrings.tr("Common", "Status.my_rewards.insurance.reward_message_2512", p1)
        }
      }
    }

    public enum Pointsentry {
      /// Earn points for capturing your blood glucose
      public static let BloodGlucose865: String = CommonStrings.tr("Common", "Status.PointsEntry.BloodGlucose865")
      /// Earn points for capturing your blood pressure
      public static let BloodPressure859: String = CommonStrings.tr("Common", "Status.PointsEntry.BloodPressure859")
      /// Earn points for having blood pressure within healthy range
      public static let BloodPressureHR860: String = CommonStrings.tr("Common", "Status.PointsEntry.BloodPressureHR860")
      /// Earn points for capturing your BMI
      public static let Bmi857: String = CommonStrings.tr("Common", "Status.PointsEntry.BMI857")
      /// Earn points for having a BMI within your healthy range
      public static let Bmihr858: String = CommonStrings.tr("Common", "Status.PointsEntry.BMIHR858")
      /// Earn points for having cholesterol within healthy range
      public static let CholesterolHR861: String = CommonStrings.tr("Common", "Status.PointsEntry.CholesterolHR861")
      /// Earn points for having a colon cancer screen.
      public static let ColonCancer2106: String = CommonStrings.tr("Common", "Status.PointsEntry.ColonCancer_2106")
      /// Earn points for cycling events
      public static let Cycling862: String = CommonStrings.tr("Common", "Status.PointsEntry.Cycling_862")
      /// Earn points for linking a device
      public static let DeviceLinking895: String = CommonStrings.tr("Common", "Status.PointsEntry.DeviceLinking895")
      /// Earn points for traveling a certain distance
      public static let Distance863: String = CommonStrings.tr("Common", "Status.PointsEntry.Distance863")
      /// Earn points by burning a certain amount of energy
      public static let EnergyExpend864: String = CommonStrings.tr("Common", "Status.PointsEntry.EnergyExpend864")
      /// Earn points for having your blood glucose within healthy range
      public static let GlucoseHR866: String = CommonStrings.tr("Common", "Status.PointsEntry.GlucoseHR866")
      /// Earn points for gym visits
      public static let GymVisit867: String = CommonStrings.tr("Common", "Status.PointsEntry.GymVisit867")
      /// Earn points for capturing HbA1c
      public static let HbA1c868: String = CommonStrings.tr("Common", "Status.PointsEntry.HbA1c868")
      /// Earn points for having HbA1c within healthy range
      public static let HbA1cHR869: String = CommonStrings.tr("Common", "Status.PointsEntry.HbA1cHR869")
      /// Earn points for buying healthy food
      public static let HealthyFoodSpend870: String = CommonStrings.tr("Common", "Status.PointsEntry.HealthyFoodSpend_870")
      /// Earn points for capturing your heart rate
      public static let Heartrate871: String = CommonStrings.tr("Common", "Status.PointsEntry.Heartrate871")
      /// Earn points for capturing your LDL cholesterol
      public static let LdlCholesterol872: String = CommonStrings.tr("Common", "Status.PointsEntry.LDLCholesterol872")
      /// Earn points for having your LDL cholesteorl within healthy range
      public static let LdlCholesterolHR873: String = CommonStrings.tr("Common", "Status.PointsEntry.LDLCholesterolHR873")
      /// Earn points for capturing your Lipid Ratio
      public static let LipidRatio874: String = CommonStrings.tr("Common", "Status.PointsEntry.LipidRatio874")
      /// Earn points for having a healthy lipid ratio
      public static let LipidRatioHR875: String = CommonStrings.tr("Common", "Status.PointsEntry.LipidRatioHR875")
      /// Earn points for having a lung cancer screen.
      public static let LungCancer2107: String = CommonStrings.tr("Common", "Status.PointsEntry.LungCancer_2107")
      /// Earn points for having a mammogram.
      public static let Mammogram2109: String = CommonStrings.tr("Common", "Status.PointsEntry.Mammogram_2109")
      /// Earn points for manual points allocation
      public static let ManualPointsAlloction879: String = CommonStrings.tr("Common", "Status.PointsEntry.ManualPointsAlloction879")
      /// Earn points for capturing fitness measurements
      public static let MeasuredFitComplete880: String = CommonStrings.tr("Common", "Status.PointsEntry.MeasuredFitComplete880")
      /// Earn points each membership year for having your fitness level measured.
      public static let MeasuredFitHealthyRange880: String = CommonStrings.tr("Common", "Status.PointsEntry.MeasuredFitHealthyRange880")
      /// Earn points for completing your MWB phych evaluation
      public static let MwbPsychological876: String = CommonStrings.tr("Common", "Status.PointsEntry.MWBPsychological876")
      /// Earn points for completing your MWB social evaluation
      public static let MwbSocial877: String = CommonStrings.tr("Common", "Status.PointsEntry.MWBSocial877")
      /// Earn points for capturing your MWB stressor
      public static let MwbStressor878: String = CommonStrings.tr("Common", "Status.PointsEntry.MWBStressor878")
      /// Earn points for completing your non smokers declaration
      public static let NonsmokersDeclarationn881: String = CommonStrings.tr("Common", "Status.PointsEntry.NonsmokersDeclarationn881")
      /// Earn points for OFE activities
      public static let Ofe882: String = CommonStrings.tr("Common", "Status.PointsEntry.OFE882")
      /// Earn points for having a pap smear.
      public static let PapSmear2110: String = CommonStrings.tr("Common", "Status.PointsEntry.PapSmear_2110")
      /// Earn points for having a pneumococcus vaccine.
      public static let Pneumococcus2111: String = CommonStrings.tr("Common", "Status.PointsEntry.Pneumococcus_2111")
      /// Earn points for running
      public static let Running883: String = CommonStrings.tr("Common", "Status.PointsEntry.Running883")
      /// Earn points for maintaining a certain speed
      public static let Speed884: String = CommonStrings.tr("Common", "Status.PointsEntry.Speed884")
      /// Earn points for taking a certain amount of steps
      public static let Steps885: String = CommonStrings.tr("Common", "Status.PointsEntry.Steps885")
      /// Earn points for having a stomach cancer screen.
      public static let StomachCancer2108: String = CommonStrings.tr("Common", "Status.PointsEntry.StomachCancer_2108")
      /// Earn points swimming
      public static let Swimming886: String = CommonStrings.tr("Common", "Status.PointsEntry.Swimming_886")
      /// Earn points for capturing your total cholesterol
      public static let TotCholesterol887: String = CommonStrings.tr("Common", "Status.PointsEntry.TotCholesterol887")
      /// Earn points for partaking in triathlons
      public static let Triathlon888: String = CommonStrings.tr("Common", "Status.PointsEntry.Triathlon888")
      /// Earn points for capturing your urine protein
      public static let UrineProtien889: String = CommonStrings.tr("Common", "Status.PointsEntry.UrineProtien889")
      /// Earn points for having urine protein within healthy range
      public static let UrineProtienHR890: String = CommonStrings.tr("Common", "Status.PointsEntry.UrineProtienHR_890")
      /// Earn points for completing the Vitality Health Review
      public static let VhrAssmntCompleted891: String = CommonStrings.tr("Common", "Status.PointsEntry.VHRAssmntCompleted891")
      /// Earn points for completing the vitality nutrition assessment
      public static let VnaAssmntCompleted892: String = CommonStrings.tr("Common", "Status.PointsEntry.VNAAssmntCompleted892")
      /// Earn points for walking
      public static let Walking893: String = CommonStrings.tr("Common", "Status.PointsEntry.Walking893")
    }
  }

  public enum SummaryScreen {
    /// Please confirm that the measurements and proof of results are correct before confirming.
    public static let ConfirmMeasurementsMessage184: String = CommonStrings.tr("Common", "summary_screen.confirm_measurements_message_184")
    /// Confirm the below
    public static let ConfirmMeasurementsTitle183: String = CommonStrings.tr("Common", "summary_screen.confirm_measurements_title_183")
    /// Tested on %@
    public static func DateTestedTitle185(_ p1: String) -> String {
      return CommonStrings.tr("Common", "summary_screen.date_tested_title_185", p1)
    }
    /// Summary
    public static let SummaryTitle181: String = CommonStrings.tr("Common", "summary_screen.summary_title_181")
    /// Uploaded Proof
    public static let UploadedProofTitle186: String = CommonStrings.tr("Common", "summary_screen.uploaded_proof_title_186")
    /// An error occurred while trying to complete your Vitality Health Check. Please try again.
    public static let VhcCompleteErrorMessage187: String = CommonStrings.tr("Common", "summary_screen.vhc_complete_error_message_187")
  }

  public enum Sv {
    /// Upload the proof of the Screening claims and Vaccinations you’ve completed from your healthcare professional.
    public static let AddProofEmptyMessage1019: String = CommonStrings.tr("Common", "SV.add_proof_empty_message_1019")
    /// Add Proof of Completion
    public static let AddProofEmptyTitle1018: String = CommonStrings.tr("Common", "SV.add_proof_empty_title_1018")
    /// An error occurred while trying to complete Screenings and Vaccinations. Please try again.
    public static let AlertUnableToCompleteMessage1036: String = CommonStrings.tr("Common", "SV.alert_unable_to_complete_message_1036")
    /// %1$@ of %2$@ attachments
    public static func AttachmentsFootnoteMessage1042(_ p1: String, _ p2: String) -> String {
      return CommonStrings.tr("Common", "SV.attachments_footnote_message_1042", p1, p2)
    }
    /// You have confirmed and submitted proof of completion for Screenings and Vaccinations which could earn you up to %1$@ points.
    public static func CompletedMessage1022(_ p1: String) -> String {
      return CommonStrings.tr("Common", "SV.completed_message_1022", p1)
    }
    /// Points may not be awarded immediately.
    public static let CompletedPointsMessage1023: String = CommonStrings.tr("Common", "SV.completed_points_message_1023")
    /// Health Actions Confirmed
    public static let CompletedTitle1021: String = CommonStrings.tr("Common", "SV.completed_title_1021")
    /// Please confirm that the health actions and proof of screenings and vaccinations are correct before confirming.
    public static let ConfirmMessage1024: String = CommonStrings.tr("Common", "SV.confirm_message_1024")
    /// Be sure to have your Screening claims and proof of Vaccinations completed from your healthcare professional on hand when confirming completion.
    public static let ConfirmSubmitIntroMessage1016: String = CommonStrings.tr("Common", "SV.confirm_submit_intro_message_1016")
    /// Only confirm completion for Screenings and Vaccinations that you can upload verified proof for.
    public static let ConfirmSubmitIntroTitle1015: String = CommonStrings.tr("Common", "SV.confirm_submit_intro_title_1015")
    /// Be sure to have your Screenings claims completed from your healthcare professional on hand when confirming completion.
    public static let ConfirmSubmitScreeningsIntroMessage1147: String = CommonStrings.tr("Common", "SV.confirm_submit_screenings_intro_message_1147")
    /// Only confirm completion for Screenings that you can upload verified proof for.
    public static let ConfirmSubmitScreeningsIntroTitle1145: String = CommonStrings.tr("Common", "SV.confirm_submit_screenings_intro_title_1145")
    /// Tested Date
    public static let ConfirmSubmitTestedDate2403: String = CommonStrings.tr("Common", "sv.confirm_submit_tested_date_2403")
    /// Be sure to have your Vaccinations claims completed from your healthcare professional on hand when confirming completion.
    public static let ConfirmSubmitVaccinationsIntroMessage1148: String = CommonStrings.tr("Common", "SV.confirm_submit_vaccinations_intro_message_1148")
    /// Only confirm completion for Vaccinations that you can upload verified proof for.
    public static let ConfirmSubmitVaccinationsIntroTitle1146: String = CommonStrings.tr("Common", "SV.confirm_submit_vaccinations_intro_title_1146")
    /// Points will be awarded after the completion of each vaccination schedule.
    public static let ConfirmSubmitVaccinationsMessage1017: String = CommonStrings.tr("Common", "SV.confirm_submit_vaccinations_message_1017")
    /// Confirm the Below
    public static let ConfirmTitle1041: String = CommonStrings.tr("Common", "SV.confirm_title_1041")
    /// Keep track of all Screenings and Vaccinations submissions here.
    public static let HistoryEmptyMessage1038: String = CommonStrings.tr("Common", "SV.history_empty_message_1038")
    /// No History
    public static let HistoryEmptyTitle1037: String = CommonStrings.tr("Common", "SV.history_empty_title_1037")
    /// Submissions
    public static let HistorySubmissionTitle1043: String = CommonStrings.tr("Common", "SV.history_submission_title_1043")
    /// Be sure to have your Screening claims and proof of Vaccinations completed from your healthcare professional on hand when confirming completion.
    public static let LandingHealthActionMessage1009: String = CommonStrings.tr("Common", "SV.landing_health_action_message_1009")
    /// Confirm and submit proof of completion for all health actions to earn 2 000 bonus points.
    public static let LandingHealthActionPointsMessage1039: String = CommonStrings.tr("Common", "SV.landing_health_action_points_message_1039")
    /// Health Action
    public static let LandingHealthActionTitle1008: String = CommonStrings.tr("Common", "SV.landing_health_action_title_1008")
    /// Healthcare PDF
    public static let LandingHealthcarePdf1040: String = CommonStrings.tr("Common", "SV.landing_healthcare_pdf_1040")
    /// Health Action
    public static let LandingSectionHeading1011: String = CommonStrings.tr("Common", "SV.landing_section_heading_1011")
    /// Confirm and Submit
    public static let LandingSubmitButton1010: String = CommonStrings.tr("Common", "SV.landing_submit_button_1010")
    /// About
    public static let LearnMoreDetail1Title1047: String = CommonStrings.tr("Common", "SV.learn_more_detail_1_title_1047")
    /// What Do I Need to Know Before the Test?
    public static let LearnMoreDetail2Title1048: String = CommonStrings.tr("Common", "SV.learn_more_detail_2_title_1048")
    /// What can I expect?
    public static let LearnMoreDetail3Title1049: String = CommonStrings.tr("Common", "SV.learn_more_detail_3_title_1049")
    /// How do I earn points for getting a Mammogram?
    public static let LearnMoreDetail4Title1050: String = CommonStrings.tr("Common", "SV.learn_more_detail_4_title_1050")
    /// Getting Started
    public static let LearnMoreDetail5Title1051: String = CommonStrings.tr("Common", "SV.learn_more_detail_5_title_1051")
    /// It is best to have regular Screenings and Vaccinations as prevention is always better than cure. Confirm completion of gender, age, and lifestyle-specific Screenings and Vaccinations to earn points and assess your health.
    public static let LearnMoreMainMessage1026: String = CommonStrings.tr("Common", "SV.learn_more_main_message_1026")
    /// How Screenings and Vaccinations Works
    public static let LearnMoreMainTitle1025: String = CommonStrings.tr("Common", "SV.learn_more_main_title_1025")
    /// Visit a healthcare professional near you for gender, age, and lifestyle-specific Screenings and Vaccinations.
    public static let LearnMoreSection1Message1028: String = CommonStrings.tr("Common", "SV.learn_more_section1_message_1028")
    /// Visit a Healthcare Professional
    public static let LearnMoreSection1Title1027: String = CommonStrings.tr("Common", "SV.learn_more_section_1_title_1027")
    /// Confirm Completion of Screenings and Vaccinations
    public static let LearnMoreSection2Title1030: String = CommonStrings.tr("Common", "SV.learn_more_section_2_title_1030")
    /// Earn points for confirming completion of each section for Screenings and Vaccinations.
    public static let LearnMoreSection3Message1033: String = CommonStrings.tr("Common", "SV.learn_more_section_3_message_1033")
    /// Earn Points
    public static let LearnMoreSection3Title1032: String = CommonStrings.tr("Common", "SV.learn_more_section_3_title_1032")
    /// Learn More
    public static let LearnMoreTitle1082: String = CommonStrings.tr("Common", "sv.learn_more_title_1082")
    /// %1$@ points earned for confirming and submitting proof of completion for Screenings and Vaccinations
    public static func NotificationPointsEarnedForCompletion1081(_ p1: String) -> String {
      return CommonStrings.tr("Common", "sv.notification_points_earned_for_completion_1081", p1)
    }
    /// Download the healthcare PDF and get your Screenings and Vaccinations done by a healthcare professional.
    public static let OnboardingSection1AltMessage1001: String = CommonStrings.tr("Common", "SV.onboarding_section_1_alt_message_1001")
    /// Visit a healthcare professional  near you for screenings and vaccinations.
    public static let OnboardingSection1Message1003: String = CommonStrings.tr("Common", "SV.onboarding_section_1_message_1003")
    /// Get  Assessed
    public static let OnboardingSection1Title1002: String = CommonStrings.tr("Common", "SV.onboarding_section_1_title_1002")
    /// Confirm and upload proof of completion for Screenings and Vaccinations from the healthcare professional.
    public static let OnboardingSection2Message1005: String = CommonStrings.tr("Common", "SV.onboarding_section_2_message_1005")
    /// Confirm and Submit
    public static let OnboardingSection2Title1004: String = CommonStrings.tr("Common", "SV.onboarding_section_2_title_1004")
    /// Earn points for confirming and submitting proof of completion for each section of Screenings and Vaccinations.
    public static let OnboardingSection3Message1007: String = CommonStrings.tr("Common", "SV.onboarding_section_3_message_1007")
    /// Earn Points
    public static let OnboardingSection3Title1006: String = CommonStrings.tr("Common", "SV.onboarding_section_3_title_1006")
    /// Participating Partners
    public static let PartnerTitle1029: String = CommonStrings.tr("Common", "SV.partner_title_1029")
    /// An error occurred while trying to fetch Participating Partners details. Please try again.
    public static let PartnersErrorMessage2404: String = CommonStrings.tr("Common", "sv.partners_error_message_2404")
    /// It is best to have regular Screenings as prevention is always better than cure. Earn points for doing gender, age, and lifestyle-specific Screenings.
    public static let ScreeningsDetailMessage1014: String = CommonStrings.tr("Common", "SV.screenings_detail_message_1014")
    /// Your health profile doesn't require any vaccination results.
    public static let ScreeningsFootnoteMessage1149: String = CommonStrings.tr("Common", "SV.screenings_footnote_message_1149")
    /// Screenings
    public static let ScreeningsTitle1012: String = CommonStrings.tr("Common", "SV.screenings_title_1012")
    /// It is best to have regular vaccinations as prevention is always better than cure. Earn points when getting your vaccinations done.
    public static let VaccinationsDetailMessage1020: String = CommonStrings.tr("Common", "SV.vaccinations_detail_message_1020")
    /// Your health profile doesn't require any screenings results.
    public static let VaccinationsFootnoteMessage1150: String = CommonStrings.tr("Common", "SV.vaccinations_footnote_message_1150")
    /// Vaccinations
    public static let VaccinationsTitle1013: String = CommonStrings.tr("Common", "SV.vaccinations_title_1013")
    /// Get one free gender, age, and lifestyle-specific Screenings and Vaccinations per year if you visit any of the participating partners below.
    public static let VisitPartnerMessage1035: String = CommonStrings.tr("Common", "SV.visit_partner_message_1035")
    /// Visit a Partner
    public static let VisitPartnerTitle1034: String = CommonStrings.tr("Common", "SV.visit_partner_title_1034")

    public enum Onboarding {
      /// Got it
      public static let GetStartedButton131: String = CommonStrings.tr("Common", "SV.onboarding.get_started_button_131")
    }
  }

  public enum TermsAndConditions {
    /// Terms and Conditions
    public static let DisagreeAlertMessage51: String = CommonStrings.tr("Common", "terms_and_conditions.disagree_alert_message_51")
    /// More
    public static let MoreButtonTitle95: String = CommonStrings.tr("Common", "terms_and_conditions.more_button_title_95")
    /// Terms and Conditions
    public static let ScreenTitle94: String = CommonStrings.tr("Common", "terms_and_conditions.screen_title_94")
  }

  public enum Uke {
    /// 
    public static let ActivationLoadingIndicatorTitle369: String = CommonStrings.tr("Common", "uke.activation_loading_indicator_title_369")
    /// 
    public static let AlertDateOfBirthMessage1053: String = CommonStrings.tr("Common", "uke.alert_date_of_birth_message_1053")
    /// 
    public static let AlertDateOfBirthTitle1052: String = CommonStrings.tr("Common", "uke.alert_date_of_birth_title_1052")
    /// 
    public static let AlertIncorrectCodeMessage371: String = CommonStrings.tr("Common", "uke.alert_incorrect_code_message_371")
    /// 
    public static let AlertIncorrectCodeTitle370: String = CommonStrings.tr("Common", "uke.alert_incorrect_code_title_370")
    /// 
    public static let AlertIncorrectNumberMessage373: String = CommonStrings.tr("Common", "uke.alert_incorrect_number_message_373")
    /// 
    public static let AlertIncorrectNumberTitle372: String = CommonStrings.tr("Common", "uke.alert_incorrect_number_title_372")
    /// 
    public static let AlertInvalidAccountTitle384: String = CommonStrings.tr("Common", "uke.alert_invalid_account_title_384")
    /// 
    public static let AlertInvalidAccountTitle385: String = CommonStrings.tr("Common", "uke.alert_invalid_account_title_385")
    /// 
    public static let CommunicationPrefButtonStatus377: String = CommonStrings.tr("Common", "uke.communication_pref_button_status_377")
    /// 
    public static let CommunicationPrefMessageKeepLoggedIn387: String = CommonStrings.tr("Common", "uke.communication_pref_message_keep_logged_in_387")
    /// 
    public static let CommunicationPrefMessageStatus376: String = CommonStrings.tr("Common", "uke.communication_pref_message_status_376")
    /// 
    public static let CommunicationPrefTitleKeepLoggedIn386: String = CommonStrings.tr("Common", "uke.communication_pref_title_keep_logged_in_386")
    /// 
    public static let CommunicationPrefTitleStatus375: String = CommonStrings.tr("Common", "uke.communication_pref_title_status_375")
    /// 
    public static let CommunicationPrefUpdateError9999: String = CommonStrings.tr("Common", "uke.communication_pref_update_error_9999")
    /// 
    public static let OnboardingEnjoyWeeklyRewardsMessage383: String = CommonStrings.tr("Common", "uke.onboarding_enjoy_weekly_rewards_message_383")
    /// 
    public static let OnboardingEnjoyWeeklyRewardsTitle382: String = CommonStrings.tr("Common", "uke.onboarding_enjoy_weekly_rewards_title_382")
    /// 
    public static let OnboardingImproveYourHealthMessage381: String = CommonStrings.tr("Common", "uke.onboarding_improve_your_health_message_381")
    /// 
    public static let OnboardingImproveYourHealthTitle380: String = CommonStrings.tr("Common", "uke.onboarding_improve_your_health_title_380")
    /// 
    public static let OnboardingKnowYourHealthMessage379: String = CommonStrings.tr("Common", "uke.onboarding_know_your_health_message_379")
    /// 
    public static let OnboardingKnowYourHealthTitle378: String = CommonStrings.tr("Common", "uke.onboarding_know_your_health_title_378")
    /// 
    public static let WelcomeButtonTitle368: String = CommonStrings.tr("Common", "uke.welcome_button_title_368")

    public enum Activate {
      /// 
      public static let ActivateTitle353: String = CommonStrings.tr("Common", "UKE.Activate.activate_title_353")
      /// 
      public static let AuthenticationCode355: String = CommonStrings.tr("Common", "UKE.activate.authentication_code_355")
      /// 
      public static let AuthenticationCodeFooter357: String = CommonStrings.tr("Common", "UKE.activate.authentication_code_footer_357")
      /// 
      public static let AuthenticationCodePlaceholder356: String = CommonStrings.tr("Common", "UKE.activate.authentication_code_placeholder_356")
      /// 
      public static let DateOfBirth363: String = CommonStrings.tr("Common", "UKE.activate.date_of_birth_363")
      /// 
      public static let EnterDate354: String = CommonStrings.tr("Common", "UKE.activate.enter_date_354")
      /// 
      public static let EntityNumber358: String = CommonStrings.tr("Common", "UKE.activate.entity_number_358")
      /// 
      public static let EntityNumberFooter360: String = CommonStrings.tr("Common", "UKE.activate.entity_number_footer_360")
      /// 
      public static let EntityNumberPlaceholder359: String = CommonStrings.tr("Common", "UKE.activate.entity_number_placeholder_359")
    }
  }

  public enum UnitOfMeasure {
    /// bpm
    public static let BeatsPerMinuteAbbreviation855: String = CommonStrings.tr("Common", "unit_of_measure.beats_per_minute_abbreviation_855")
    /// kcal
    public static let KilocaloriesAbbreviation852: String = CommonStrings.tr("Common", "unit_of_measure.kilocalories_abbreviation_852")
    /// kcal/h
    public static let KilocaloriesPerHourAbbreviation856: String = CommonStrings.tr("Common", "unit_of_measure.kilocalories_per_hour_abbreviation_856")
    /// m/s
    public static let MeterPerSecondAbbreviation853: String = CommonStrings.tr("Common", "unit_of_measure.meter_per_second_abbreviation_853")
    /// min/km
    public static let MinutesPerKilometerAbbreviation854: String = CommonStrings.tr("Common", "unit_of_measure.minutes_per_kilometer_abbreviation_854")
  }

  public enum UserPrefs {
    /// Enable analytics to help us continue improving the app.
    public static let AnalyticsToggleMessage74: String = CommonStrings.tr("Common", "user_prefs.analytics_toggle_message_74")
    /// Analytics
    public static let AnalyticsToggleTitle73: String = CommonStrings.tr("Common", "user_prefs.analytics_toggle_title_73")
    /// Enable the following permissions to enjoy a full app experience.
    public static let CommunicationGroupHeaderMessage65: String = CommonStrings.tr("Common", "user_prefs.communication_group_header_message_65")
    /// Communication Preferences
    public static let CommunicationGroupHeaderTitle64: String = CommonStrings.tr("Common", "user_prefs.communication_group_header_title_64")
    /// Enable crash reports to help us continue improving the app.
    public static let CrashReportsToggleMessage76: String = CommonStrings.tr("Common", "user_prefs.crash_reports_toggle_message_76")
    /// Crash Reports
    public static let CrashReportsToggleTitle75: String = CommonStrings.tr("Common", "user_prefs.crash_reports_toggle_title_75")
    /// Enable email communication to receive important information and updates.
    public static let EmailToggleMessage66: String = CommonStrings.tr("Common", "user_prefs.email_toggle_message_66")
    /// Fingerprint is a secure way to avoid having to type in your password every time Vitality needs to verify your identity to access private information
    public static let FingerprintMessage93: String = CommonStrings.tr("Common", "user_prefs.fingerprint_message_93")
    /// Use Fingerprint
    public static let FingerprintTitle92: String = CommonStrings.tr("Common", "user_prefs.fingerprint_title_92")
    /// Manage in settings
    public static let ManageInSettingsButtonTitle91: String = CommonStrings.tr("Common", "user_prefs.manage_in_settings_button_title_91")
    /// Allowing permissions gives us the ability to communicate important information with you. Are you sure you want to continue without allowing these?
    public static let NoPreferencesSetAlertMessage86: String = CommonStrings.tr("Common", "user_prefs.no_preferences_set_alert_message_86")
    /// You Haven't Allowed Any Permissions
    public static let NoPreferencesSetAlertTitle85: String = CommonStrings.tr("Common", "user_prefs.no_preferences_set_alert_title_85")
    /// Allow app notifications so that you can be notified about important information.
    public static let NotificationsMessage90: String = CommonStrings.tr("Common", "user_prefs.notifications_message_90")
    /// App Notifications
    public static let NotificationsTitle89: String = CommonStrings.tr("Common", "user_prefs.notifications_title_89")
    /// Privacy Statement
    public static let PrivacyGroupHeaderLinkButtonTitle72: String = CommonStrings.tr("Common", "user_prefs.privacy_group_header_link_button_title_72")
    /// Your privacy matters to us. Here’s the information we’ll be using from you.
    public static let PrivacyGroupHeaderMessage71: String = CommonStrings.tr("Common", "user_prefs.privacy_group_header_message_71")
    /// Privacy
    public static let PrivacyGroupHeaderTitle70: String = CommonStrings.tr("Common", "user_prefs.privacy_group_header_title_70")
    /// Allow push notifications so that you can be notified about important information.
    public static let PushMessageToggleMessage68: String = CommonStrings.tr("Common", "user_prefs.push_message_toggle_message_68")
    /// Manage in the Settings app
    public static let PushMessageToggleSettingsLinkButtonTitle69: String = CommonStrings.tr("Common", "user_prefs.push_message_toggle_settings_link_button_title_69")
    /// Push Notifications
    public static let PushMessageToggleTitle67: String = CommonStrings.tr("Common", "user_prefs.push_message_toggle_title_67")
    /// Turn off ‘Remember Me’ if you prefer to enter your email every time you access the app.
    public static let RememberMeToggleMessage82: String = CommonStrings.tr("Common", "user_prefs.remember_me_toggle_message_82")
    /// Remember Me
    public static let RememberMeToggleTitle81: String = CommonStrings.tr("Common", "user_prefs.remember_me_toggle_title_81")
    /// Keep all your information safe.
    public static let SecurityGroupHeaderMessage78: String = CommonStrings.tr("Common", "user_prefs.security_group_header_message_78")
    /// Security
    public static let SecurityGroupHeaderTitle77: String = CommonStrings.tr("Common", "user_prefs.security_group_header_title_77")
    /// Setup
    public static let SetupTite374: String = CommonStrings.tr("Common", "user_prefs.setup_tite_374")
    /// Touch ID is a secure way to avoid having to type in your password every time Vitality needs to verify your identity to access private information.
    public static let TouchidToggleMessage80: String = CommonStrings.tr("Common", "user_prefs.touchid_toggle_message_80")
    /// Touch ID
    public static let TouchidToggleTitle79: String = CommonStrings.tr("Common", "user_prefs.touchid_toggle_title_79")
    /// Manage Privacy and Security from inside the app settings.
    public static let UserPrefsScreenFootnoteMessage83: String = CommonStrings.tr("Common", "user_prefs.user_prefs_screen_footnote_message_83")
  }

  public enum V {
    /// Confirm and upload proof of completion for Screenings and Vaccinations from the healthcare professional.
    public static let LearnMoreSection2Message1031: String = CommonStrings.tr("Common", "V.learn_more_section_2_message_1031")
  }

  public enum Vhc {

    public enum LearnMore {
      /// Content on what Other blood lipids is.
      public static let OtherBloodLipidsSection1Message2407: String = CommonStrings.tr("Common", "vhc.learn_more.other_blood_lipids_section_1_message_2407")
      /// Content on why Other blood lipids is.
      public static let OtherBloodLipidsSection2Message2408: String = CommonStrings.tr("Common", "vhc.learn_more.other_blood_lipids_section_2_message_2408")
      /// Content on how you earn points for Other blood lipids.
      public static let OtherBloodLipidsSection3Message2409: String = CommonStrings.tr("Common", "vhc.learn_more.other_blood_lipids_section_3_message_2409")
      /// How do I earn points for my Other Blood lipids?
      public static let OtherBloodLipidsSection4Title2410: String = CommonStrings.tr("Common", "vhc.learn_more.other_blood_lipids_section_4_title_2410")
      /// Other Blood Lipids
      public static let OtherBloodLipidsTitle1453: String = CommonStrings.tr("Common", "vhc.learn_more.other_blood_lipids_title_1453")
      /// Participating Partners
      public static let ParticipatingPartners262: String = CommonStrings.tr("Common", "vhc.learn_more.participating_partners_262")
    }
  }

  public enum Vhr {
    /// The sum of the values entered exceeds 24 hours. Please edit one or more of the values that you entered
    public static let AssessmentSittingProblemPrompt2412: String = CommonStrings.tr("Common", "vhr.assessment_sitting_problem_prompt_2412")
    /// 
    public static let AssessmentSittingProblemPrompt9999: String = CommonStrings.tr("Common", "vhr.assessment_sitting_problem_prompt_9999")
    /// None of above
    public static let MedHistoryNoneOfAbove2413: String = CommonStrings.tr("Common", "vhr.med_history_none_of_above_2413")
    /// Start Assessment
    public static let ModalButtonText2439: String = CommonStrings.tr("Common", "vhr.modal_button_text_2439")
    /// Welcome to Vitality!
    public static let ModalHeading2436: String = CommonStrings.tr("Common", "vhr.modal_heading_2436")
    /// To start your health programme, take the Vitality Health Review to find out your Vitality Age.
    public static let ModalSection12437: String = CommonStrings.tr("Common", "vhr.modal_section1_2437")
    /// Bonus points available for a limited time.
    public static let ModalSection22438: String = CommonStrings.tr("Common", "vhr.modal_section2_2438")
    /// An error occurred while trying to submit your Vitality Health Review. Please try again.
    public static let UnableToSubmitErrorAlertMessage1330: String = CommonStrings.tr("Common", "vhr.unable_to_submit_error_alert_message_1330")

    public enum Assessment {
      /// Diastolic blood pressure can not be higher than or equal to systolic blood pressure. Please enter the correct values.
      public static let BloodPressurePrompt2411: String = CommonStrings.tr("Common", "vhr.assessment.blood_pressure_prompt_2411")
      /// 
      public static let BloodPressurePrompt9999: String = CommonStrings.tr("Common", "vhr.assessment.blood_pressure_prompt_9999")
    }

    public enum BonusPoints {
      /// By completing Vitality Health Review you earned 1500 bonus points
      public static let Completed2485: String = CommonStrings.tr("Common", "vhr.bonus_points.completed_2485")
      /// Complete within %1$@ days and earn 1500 bonus points
      public static func Incomplete2484(_ p1: String) -> String {
        return CommonStrings.tr("Common", "vhr.bonus_points.incomplete_2484", p1)
      }
    }

    public enum Common {
      /// Unable to submit
      public static let ErrorPrompt1328: String = CommonStrings.tr("Common", "VHR.common.error_prompt_1328")
    }

    public enum Landing {
      /// What you are doing well
      public static let SectionHeaderTitle1174: String = CommonStrings.tr("Common", "vhr.landing.section_header_title_1174")
      /// What you haven't provided
      public static let SectionHeaderTitle1175: String = CommonStrings.tr("Common", "vhr.landing.section_header_title_1175")

      public enum Header {
        /// You've earned %1$@ points! The Vitality Health Review can only be completed for points once a year.
        public static func DescriptionCompleted389(_ p1: String) -> String {
          return CommonStrings.tr("Common", "VHR.landing.header.description_completed_389", p1)
        }
      }
    }
  }

  public enum Vna {
    /// Neither
    public static let DietaryPreferenceNeither2414: String = CommonStrings.tr("Common", "vna.dietary_preference_neither_2414")
    /// 
    public static let DietaryPreferenceNeither9999: String = CommonStrings.tr("Common", "vna.dietary_preference_neither_9999")
    /// %@ updated!
    public static func FoodChoicesUpdateCompletionTitle1192(_ p1: String) -> String {
      return CommonStrings.tr("Common", "vna.food_choices_update_completion_title_1192", p1)
    }
    /// The information you’ve captured can be seen in your Health section.
    public static let FoodChoicesUpdatedCompletionDescription1193: String = CommonStrings.tr("Common", "vna.food_choices_updated_completion_description_1193")
    /// Great!
    public static let GreatButtonTitle120: String = CommonStrings.tr("Common", "vna.great_button_title_120")
    /// Nutrition Results
    public static let Label1965: String = CommonStrings.tr("Common", "vna.label_1965")
    /// None
    public static let QuestionnaireNone2415: String = CommonStrings.tr("Common", "vna.questionnaire_none_2415")
    /// 
    public static let QuestionnaireNone9999: String = CommonStrings.tr("Common", "vna.questionnaire_none_9999")
    /// An error occurred while trying to submit your Vitality Nutrition Assessment. Please try again.
    public static let UnableToSubmitErrorAlertMessage2416: String = CommonStrings.tr("Common", "vna.unable_to_submit_error_alert_message_2416")
    /// 
    public static let UnableToSubmitErrorAlertMessage9999: String = CommonStrings.tr("Common", "VNA.unable_to_submit_error_alert_message_9999")

    public enum Assessment {

      public enum Landing {
        /// Unlock your nutrition results and earn points by completing all of the above sections.
        public static let FooterDescription1199: String = CommonStrings.tr("Common", "vna.assessment.landing.footer_description_1199")
      }
    }

    public enum Card {
      /// Unknown
      public static let Label1966: String = CommonStrings.tr("Common", "vna.card.label_1966")
      /// View Your Results
      public static let Label1967: String = CommonStrings.tr("Common", "vna.card.label_1967")
      /// Take a look at what's going well and what needs improvement.
      public static let Label1968: String = CommonStrings.tr("Common", "vna.card.label_1968")
      /// Outdated
      public static let Label1969: String = CommonStrings.tr("Common", "vna.card.label_1969")
      /// Retake the Vitality Nutrition Assessment to update your result.
      public static let Label1970: String = CommonStrings.tr("Common", "vna.card.label_1970")
    }

    public enum Completed {
      /// The Vitality Nutrition Assessment can only be completed for points once a year.
      public static let Message1191: String = CommonStrings.tr("Common", "vna.completed.message_1191")
      /// All sections completed!
      public static let Title1190: String = CommonStrings.tr("Common", "vna.completed.title_1190")
    }

    public enum DailyMealsCompletion {
      /// %1$@ completed!
      public static func Title1190(_ p1: String) -> String {
        return CommonStrings.tr("Common", "vna.daily_meals_completion.title_1190", p1)
      }
    }

    public enum Landing {
      /// Lifestyle choices
      public static let Section1187: String = CommonStrings.tr("Common", "vna.landing.section_1187")
      /// Daily meals
      public static let Section1188: String = CommonStrings.tr("Common", "vna.landing.section_1188")
      /// Food choices
      public static let Section1189: String = CommonStrings.tr("Common", "vna.landing.section_1189")

      public enum Header {
        /// You've earned %1$@ points! The Vitality Nutrition Assessment can only be completed for points once a year.
        public static func DescriptionCompleted389(_ p1: String) -> String {
          return CommonStrings.tr("Common", "vna.landing.header.description_completed_389", p1)
        }
      }
    }

    public enum LandingScreen {
      /// Completed!
      public static let CompletedMessage390: String = CommonStrings.tr("Common", "VNA.landing_screen.completed_message_390")
    }

    public enum LearnMore {
      /// How Vitality Nutrition results work.
      public static let Content1971: String = CommonStrings.tr("Common", "vna.learn_more.content_1971")
      /// You’ll be given your Vitality nutrition results once you have completed your Vitality Nutrition Assessment. This will give you an idea of how well you manage your nutrition.
      public static let Content1972: String = CommonStrings.tr("Common", "vna.learn_more.content_1972")
      /// Find out your nutrition results
      public static let Content1973: String = CommonStrings.tr("Common", "vna.learn_more.content_1973")
      /// Complete the Vitality Nutrition Assessment to unlock feedback on your nutrition results.
      public static let Content1974: String = CommonStrings.tr("Common", "vna.learn_more.content_1974")
      /// Understand your nutritional health
      public static let Content1975: String = CommonStrings.tr("Common", "vna.learn_more.content_1975")
      /// Once you have completed your Vitality Nutrition assessment you’ll be guided through the results and get tips on where you can improve, if necessary.
      public static let Content1976: String = CommonStrings.tr("Common", "vna.learn_more.content_1976")
      /// Your nutrition results are outdated
      public static let Content1980: String = CommonStrings.tr("Common", "vna.learn_more.content_1980")
      /// You need to retake the Vitality Nutrition Assessment every 12 months to ensure that your nutrition results are updated and accurate.
      public static let Content1981: String = CommonStrings.tr("Common", "vna.learn_more.content_1981")
      /// Vitality Nutrition Assessment consists of basic questions and guidance about your nutrition.
      public static let Heading1Message399: String = CommonStrings.tr("Common", "vna.learn_more.heading_1_message_399")
      /// What is the Vitality Nutrition Assessment?
      public static let Heading1Title400: String = CommonStrings.tr("Common", "vna.learn_more.heading_1_title_400")
      /// Each section has been timed to help you do help you complete small sections when it suits you best. The entire Vitality Nutrition Assessment should take you an estimated 16 minutes to complete.
      public static let Section1Message401: String = CommonStrings.tr("Common", "vna.learn_more.section_1_message_401")
      /// Time to Complete
      public static let Section1Title402: String = CommonStrings.tr("Common", "vna.learn_more.section_1_title_402")
      /// Earn 4 000 points towards your status for completing all sections in the Vitality Nutrition Assessment.
      public static let Section2Message403: String = CommonStrings.tr("Common", "vna.learn_more.section_2_message_403")
      /// Earn Points
      public static let Section2Title404: String = CommonStrings.tr("Common", "vna.learn_more.section_2_title_404")
      /// Complete the Vitality Nutrition Assessment to unlock your Nutrition results and to get an understanding of your overall nutrition and lower your risk of chronic diseases.
      public static let Section3Message405: String = CommonStrings.tr("Common", "vna.learn_more.section_3_message_405")
      /// Nutrition Results
      public static let Section3Title406: String = CommonStrings.tr("Common", "vna.learn_more.section_3_title_406")
    }

    public enum LifestyleChoicesCompletion {
      /// %1$@ completed!
      public static func Title1190(_ p1: String) -> String {
        return CommonStrings.tr("Common", "vna.lifestyle_choices_completion.title_1190", p1)
      }
    }

    public enum NutritionScore {

      public enum Landing {
        /// More Results
        public static let ActionItem1436: String = CommonStrings.tr("Common", "vna.nutrition_score.landing.action_item_1436")
        /// What you can improve
        public static let SectionTitle1435: String = CommonStrings.tr("Common", "vna.nutrition_score.landing.section_title_1435")
        /// What you are doing well
        public static let SectionTitle1438: String = CommonStrings.tr("Common", "vna.nutrition_score.landing.section_title_1438")
      }

      public enum LearnMore {
        /// Once you have completed your Vitality Nutrition Assessment, you will be given results back on what you're doing well and what needs to improve.
        public static let HeaderDescription1430: String = CommonStrings.tr("Common", "vna.nutrition_score.learn_more.header_description_1430")
        /// You need to retake the Vitality Nutrition Assessment every 12 months to ensure that your nutrition results are updated and accurate.
        public static let HeaderDescription1445: String = CommonStrings.tr("Common", "vna.nutrition_score.learn_more.header_description_1445")
        /// How nutrition results work
        public static let HeaderTitle1429: String = CommonStrings.tr("Common", "vna.nutrition_score.learn_more.header_title_1429")
        /// Your nutrition results are outdated
        public static let HeaderTitle1444: String = CommonStrings.tr("Common", "vna.nutrition_score.learn_more.header_title_1444")
        /// Learn More
        public static let LandingTitle1082: String = CommonStrings.tr("Common", "vna.nutrition_score.learn_more.landing_title_1082")
        /// Complete the Vitality Nutrition Assessment to unlock feedback on your nutrition results.
        public static let SectionDescription1432: String = CommonStrings.tr("Common", "vna.nutrition_score.learn_more.section_description_1432")
        /// Once you know your unlocked your nutrition results, you will be guided through your results and receive tips to improve where necessary.
        public static let SectionDescription1434: String = CommonStrings.tr("Common", "vna.nutrition_score.learn_more.section_description_1434")
        /// Get your Nutrition Results
        public static let SectionTitle1431: String = CommonStrings.tr("Common", "vna.nutrition_score.learn_more.section_title_1431")
        /// Understand your health
        public static let SectionTitle1433: String = CommonStrings.tr("Common", "vna.nutrition_score.learn_more.section_title_1433")
      }

      public enum MoreResults {
        /// Recent Result
        public static let ResultTitle1439: String = CommonStrings.tr("Common", "vna.nutrition_score.more_results.result_title_1439")
        /// Recommended Result
        public static let ResultTitle1440: String = CommonStrings.tr("Common", "vna.nutrition_score.more_results.result_title_1440")
        /// Why is this important?
        public static let ResultTitle1441: String = CommonStrings.tr("Common", "vna.nutrition_score.more_results.result_title_1441")
        /// Source
        public static let ResultTitle1442: String = CommonStrings.tr("Common", "vna.nutrition_score.more_results.result_title_1442")
        /// These results need improvement
        public static let SectionHeaderTitle1437: String = CommonStrings.tr("Common", "vna.nutrition_score.more_results.section_header_title_1437")
        /// These results are looking good
        public static let SectionHeaderTitle1438: String = CommonStrings.tr("Common", "vna.nutrition_score.more_results.section_header_title_1438")
        /// Tips to Improve
        public static let SectionHeaderTitle1443: String = CommonStrings.tr("Common", "vna.nutrition_score.more_results.section_header_title_1443")
        /// Tips to Maintain
        public static let SectionHeaderTitle1448: String = CommonStrings.tr("Common", "vna.nutrition_score.more_results.section_header_title_1448")

        public enum Landing {
          /// More Results
          public static let HeaderTitle1437: String = CommonStrings.tr("Common", "vna.nutrition_score.more_results.landing.header_title_1437")
          /// Focus on improving these first
          public static let SectionHeaderTitle1446: String = CommonStrings.tr("Common", "vna.nutrition_score.more_results.landing.section_header_title_1446")
          /// Have a go at these improvements
          public static let SectionHeaderTitle1447: String = CommonStrings.tr("Common", "vna.nutrition_score.more_results.landing.section_header_title_1447")
        }
      }

      public enum Notification {
        /// Your Nutrition Results have been added to My Health.
        public static let Message1450: String = CommonStrings.tr("Common", "vna.nutrition_score.notification.message_1450")
        /// New results have been added to your Nutrition Results.
        public static let Message1452: String = CommonStrings.tr("Common", "vna.nutrition_score.notification.message_1452")
        /// New Results Added
        public static let Title1451: String = CommonStrings.tr("Common", "vna.nutrition_score.notification.title_1451")
        /// Nutrition Results now available
        public static let Title1452: String = CommonStrings.tr("Common", "vna.nutrition_score.notification.title_1452")
      }

      public enum Result {
        /// Learn More
        public static let CardActionItem1424: String = CommonStrings.tr("Common", "vna.nutrition_score.result.card_action_item_1424")
        /// View Your Results
        public static let CardActionItem1425: String = CommonStrings.tr("Common", "vna.nutrition_score.result.card_action_item_1425")
        /// Outdated
        public static let CardActionItem1427: String = CommonStrings.tr("Common", "vna.nutrition_score.result.card_action_item_1427")
        /// Take a look at what's going well and what needs improvement.
        public static let CardDescription1426: String = CommonStrings.tr("Common", "vna.nutrition_score.result.card_description_1426")
        /// Retake the Vitality Nutrition Assessment to update your result.
        public static let CardDescription1428: String = CommonStrings.tr("Common", "vna.nutrition_score.result.card_description_1428")
        /// Unknown
        public static let CardStatus1423: String = CommonStrings.tr("Common", "vna.nutrition_score.result.card_status_1423")
        /// Nutrition Results
        public static let CardTitle1422: String = CommonStrings.tr("Common", "vna.nutrition_score.result.card_title_1422")
      }
    }

    public enum Onboarding {
      /// This assessment is based on %@ sections consisting of questions about your nutrition.
      public static func Section1Message407(_ p1: String) -> String {
        return CommonStrings.tr("Common", "vna.onboarding.section_1_message_407", p1)
      }
      /// Complete All Sections
      public static let Section1Title408: String = CommonStrings.tr("Common", "vna.onboarding.section_1_title_408")
      /// Earn points for completing the Vitality Nutrition Assessment.
      public static let Section2Message409: String = CommonStrings.tr("Common", "vna.onboarding.section_2_message_409")
      /// Earn Points
      public static let Section2Title410: String = CommonStrings.tr("Common", "vna.onboarding.section_2_title_410")
      /// Complete the Vitality Nutrition Assessment to unlock your Nutrition results and to get an understanding of your overall nutrition and lower your risk of chronic diseases.
      public static let Section3Message411: String = CommonStrings.tr("Common", "vna.onboarding.section_3_message_411")
      /// Unlock your Nutrition Results
      public static let Section3Title412: String = CommonStrings.tr("Common", "vna.onboarding.section_3_title_412")
    }

    public enum Result {
      /// What you are doing well
      public static let Content1977: String = CommonStrings.tr("Common", "vna.result.content_1977")
      /// These results need improvement
      public static let Content1978: String = CommonStrings.tr("Common", "vna.result.content_1978")
      /// These results are looking good
      public static let Content1979: String = CommonStrings.tr("Common", "vna.result.content_1979")
      /// Nutrition Results now available
      public static let Content1982: String = CommonStrings.tr("Common", "vna.result.content_1982")
      /// Your Nutrition Results have been added to My Health.
      public static let Content1983: String = CommonStrings.tr("Common", "vna.result.content_1983")
      /// New Results Added
      public static let Content1984: String = CommonStrings.tr("Common", "vna.result.content_1984")
      /// New results have been added to your Nutrition Results.
      public static let Content1985: String = CommonStrings.tr("Common", "vna.result.content_1985")
      /// Focus on improving these first
      public static let Content1987: String = CommonStrings.tr("Common", "vna.result.content_1987")
      /// Have a go at these improvements 
      public static let Content1988: String = CommonStrings.tr("Common", "vna.result.content_1988")
    }
  }

  public enum Wd {

    public enum DeviceDetails {
      /// 
      public static let ManagePermissions9999: String = CommonStrings.tr("Common", "WD.device_details.manage_permissions_9999")
    }
  }

  public enum Wda {
    /// About Softbank
    public static let AboutTextSoftbank450: String = CommonStrings.tr("Common", "WDA.about_text_softbank_450")
    /// Delink
    public static let DelinkDialogTitle464: String = CommonStrings.tr("Common", "WDA.delink_dialog_title_464")
    /// Delink
    public static let DelinkDialogTitleSoftbank464: String = CommonStrings.tr("Common", "WDA.delink_dialog_title_softbank_464")
    /// Health app
    public static let HealthApp455: String = CommonStrings.tr("Common", "WDA.health_app_455")
    /// To help you improve your health.
    public static let HealthAppPermissionMessage563: String = CommonStrings.tr("Common", "WDA.health_app_permission_message_563")
    /// Apple
    public static let Healthapp2900: String = CommonStrings.tr("Common", "wda.healthapp_2900")
    /// Last synced %1$@
    public static func LastSynced457(_ p1: String) -> String {
      return CommonStrings.tr("Common", "WDA.last_synced_457", p1)
    }
    /// Last synced today at %1$@
    public static func LastSyncedToday458(_ p1: String) -> String {
      return CommonStrings.tr("Common", "WDA.last_synced_today_458", p1)
    }
    /// Last synced yesterday at %@
    public static func LastSyncedYesterday459(_ p1: String) -> String {
      return CommonStrings.tr("Common", "WDA.last_synced_yesterday_459", p1)
    }
    /// No New Data Found
    public static let NoDataFoundTitle542: String = CommonStrings.tr("Common", "WDA.no_data_found_title_542")
    /// Please make sure all your permissions are turned on in the Health app and that you have new data entered.
    public static let NoDataFoundTitle543: String = CommonStrings.tr("Common", "WDA.no_data_found_title_543")
    /// Samsung Health
    public static let SamsungHealthApp2425: String = CommonStrings.tr("Common", "wda.samsung_health_app_2425")
    /// 
    public static let SamsungHealthApp9999: String = CommonStrings.tr("Common", "WDA.samsung_health_app_9999")
    /// Wellness Devices and Apps
    public static let Title414: String = CommonStrings.tr("Common", "WDA.title_414")

    public enum Device {
      /// Google Fit
      public static let TitleGooglefit2523: String = CommonStrings.tr("Common", "WDA.device.title_googlefit_2523")
      /// Softbank
      public static let TitleSoftbank2418: String = CommonStrings.tr("Common", "wda.device.title_softbank_2418")
      /// 
      public static let TitleSoftbank9999: String = CommonStrings.tr("Common", "WDA.device.title_softbank_9999")
    }

    public enum DeviceDetail {
      /// Delink
      public static let DelinkButtonText462: String = CommonStrings.tr("Common", "WDA.device_detail.delink_button_text_462")
      /// Delink
      public static let DelinkButtonTextSoftbank462: String = CommonStrings.tr("Common", "WDA.device_detail.delink_button_text_softbank_462")
      /// device
      public static let DelinkDialogDevice467: String = CommonStrings.tr("Common", "WDA.device_detail.delink_dialog_device_467")
      /// Delink
      public static let DelinkDialogDeviceSoftbank467: String = CommonStrings.tr("Common", "WDA.device_detail.delink_dialog_device_softbank_467")
      /// Unable to Delink
      public static let DelinkFailedDialogTitle468: String = CommonStrings.tr("Common", "WDA.device_detail.delink_failed_dialog_title_468")
      /// Earn points for your healthy habits, which contribute to your Vitality status and Active Rewards target.
      public static let EarnMessage433: String = CommonStrings.tr("Common", "WDA.device_detail.earn_message_433")
      /// Earn points for your healthy habits, which contribute to your Vitality status and Active Rewards target.
      public static let EarnMessageSoftbank433: String = CommonStrings.tr("Common", "WDA.device_detail.earn_message_softbank_433")
      /// Last Synced
      public static let LastSyncedHeader456: String = CommonStrings.tr("Common", "WDA.device_detail.last_synced_header_456")
      /// Today, %1$@
      public static func LastSyncedToday474(_ p1: String) -> String {
        return CommonStrings.tr("Common", "WDA.device_detail.last_synced_today_474", p1)
      }
      /// Not Yet Synced
      public static let NotSynced493: String = CommonStrings.tr("Common", "WDA.device_detail.not_synced_493")
      /// An error occurred while trying to sync %1$@. Please try again.
      public static func SyncFailedDialogMessage510(_ p1: String) -> String {
        return CommonStrings.tr("Common", "WDA.device_detail.sync_failed_dialog_message_510", p1)
      }
      /// An error occurred while trying to link %1$@. Please try again.
      public static func SyncFailedDialogMessage513(_ p1: String) -> String {
        return CommonStrings.tr("Common", "WDA.device_detail.sync_failed_dialog_message_513", p1)
      }
      /// An error occurred while trying to link %1$@. Please try again.
      public static func SyncFailedDialogMessageDynamic513(_ p1: String) -> String {
        return CommonStrings.tr("Common", "WDA.device_detail.sync_failed_dialog_message_dynamic_513", p1)
      }
      /// Unable to Sync
      public static let SyncFailedDialogTitle509: String = CommonStrings.tr("Common", "WDA.device_detail.sync_failed_dialog_title_509")
      /// Unable to Link
      public static let SyncFailedDialogTitle512: String = CommonStrings.tr("Common", "WDA.device_detail.sync_failed_dialog_title_512")
      /// Sync Now
      public static let SyncNowButtonText460: String = CommonStrings.tr("Common", "WDA.device_detail.sync_now_button_text_460")
      /// Yesterday, %1$@
      public static func SyncedYesterday475(_ p1: String) -> String {
        return CommonStrings.tr("Common", "WDA.device_detail.synced_yesterday_475", p1)
      }
      /// Syncing...
      public static let SyncingButtonText511: String = CommonStrings.tr("Common", "WDA.device_detail.syncing_button_text_511")

      public enum Other {
        /// Other
        public static let SectionHeader448: String = CommonStrings.tr("Common", "WDA.device_detail.other.section_header_448")
      }
    }

    public enum DeviceDetails {
      /// Manage Permissions
      public static let ManagePermissions2417: String = CommonStrings.tr("Common", "wda.device_details.manage_permissions_2417")
      /// No more data from Samsung Health
      public static let SamsungReadDataEmptyMessage2419: String = CommonStrings.tr("Common", "wda.device_details.samsung_read_data_empty_message_2419")
      /// 
      public static let SamsungReadDataEmptyMessage9999: String = CommonStrings.tr("Common", "WDA.device_details.samsung_read_data_empty_message_9999")
      /// Error encountered while reading data from Samsung Health.
      public static let SamsungReadDataFailedMessage2420: String = CommonStrings.tr("Common", "wda.device_details.samsung_read_data_failed_message_2420")
      /// 
      public static let SamsungReadDataFailedMessage9999: String = CommonStrings.tr("Common", "WDA.device_details.samsung_read_data_failed_message_9999")
      /// Samsung Health Read Data Failed
      public static let SamsungReadDataFailedTitle2421: String = CommonStrings.tr("Common", "wda.device_details.samsung_read_data_failed_title_2421")
      /// 
      public static let SamsungReadDataFailedTitle9999: String = CommonStrings.tr("Common", "WDA.device_details.samsung_read_data_failed_title_9999")
    }

    public enum DeviceSync {
      /// Successfully Sync!
      public static let SuccessfullySynced2422: String = CommonStrings.tr("Common", "wda.device_sync.successfully_synced_2422")
      /// 
      public static let SuccessfullySynced9999: String = CommonStrings.tr("Common", "WDA.device_sync.successfully_synced_9999")
    }

    public enum HealthMeasurements {
      /// Body Mass Index
      public static let ItemBmiText496: String = CommonStrings.tr("Common", "WDA.health_measurements.item_bmi_text_496")
      /// Height
      public static let ItemHeightText447: String = CommonStrings.tr("Common", "WDA.health_measurements.item_height_text_447")
      /// Weight
      public static let ItemWeightText446: String = CommonStrings.tr("Common", "WDA.health_measurements.item_weight_text_446")
      /// Health Measurements
      public static let SectionHeader445: String = CommonStrings.tr("Common", "WDA.health_measurements.section_header_445")
      /// Health Measurements
      public static let SectionHeaderSoftbank445: String = CommonStrings.tr("Common", "WDA.health_measurements.section_header_softbank_445")
    }

    public enum HomeCard {
      /// Manage devices and apps
      public static let Title415: String = CommonStrings.tr("Common", "WDA.home_card.title_415")
    }

    public enum Landing {
      /// Available to Link
      public static let AvailableLinkSectionTitle426: String = CommonStrings.tr("Common", "WDA.landing.available_link_section_title_426")
      /// You don't have any devices or apps linked yet. Choose what you want to link from the list below and start earning points for your daily habits.
      public static let HeaderContent425: String = CommonStrings.tr("Common", "WDA.landing.header_content_425")
      /// Get Linking
      public static let HeaderTitle424: String = CommonStrings.tr("Common", "WDA.landing.header_title_424")
      /// Linked
      public static let LinkedSectionTitle472: String = CommonStrings.tr("Common", "WDA.landing.linked_section_title_472")
      /// Not yet synced
      public static let NotSynced494: String = CommonStrings.tr("Common", "WDA.landing.not_synced_494")
      /// Data can take up to 2 days to sync.
      public static let SyncFooterText473: String = CommonStrings.tr("Common", "WDA.landing.sync_footer_text_473")
    }

    public enum LearnMore {
      /// Linking a wellness device or app allows you track your health habits and earn points from those results
      public static let Item1428: String = CommonStrings.tr("Common", "WDA.learn_more.item1_428")
      /// How Wellness Devices and Apps Work
      public static let Item1Heading427: String = CommonStrings.tr("Common", "WDA.learn_more.item1_heading_427")
      /// Linking a wellness device or app allows you track your health habits and earn points from those results.
      public static let Item2430: String = CommonStrings.tr("Common", "WDA.learn_more.item2_430")
      /// Why is this important from a wellness perspective?
      public static let Item2Heading429: String = CommonStrings.tr("Common", "WDA.learn_more.item2_heading_429")
      /// Vitality offers you savings on a range of fitness devices that help you to improve your health and achieve your health goals. What's more, you earn Vitality points as you use any of the supported devices to track your workouts and work towards a healthier life.
      public static let Item3432: String = CommonStrings.tr("Common", "WDA.learn_more.item3_432")
      /// Why is this important from a Vitality perspective?
      public static let Item3Heading431: String = CommonStrings.tr("Common", "WDA.learn_more.item3_heading_431")
    }

    public enum Onboarding {
      /// Got it
      public static let GotItTitle2423: String = CommonStrings.tr("Common", "wda.onboarding.got_it_title_2423")
      /// 
      public static let GotItTitle9991: String = CommonStrings.tr("Common", "WDA.onboarding.got_it_title_9991")
      /// Wellness Devices and Apps
      public static let Heading416: String = CommonStrings.tr("Common", "WDA.onboarding.heading_416")
      /// I need a device or app
      public static let INeedADeviceOrAppTitle423: String = CommonStrings.tr("Common", "WDA.onboarding.i_need_a_device_or_app_title_423")
      /// Choose from a list of devices and apps to link.
      public static let Item1418: String = CommonStrings.tr("Common", "WDA.onboarding.item1_418")
      /// Link
      public static let Item1Heading417: String = CommonStrings.tr("Common", "WDA.onboarding.item1_heading_417")
      /// Track your daily health habits.
      public static let Item2420: String = CommonStrings.tr("Common", "WDA.onboarding.item2_420")
      /// Track
      public static let Item2Heading419: String = CommonStrings.tr("Common", "WDA.onboarding.item2_heading_419")
      /// Earn points for your healthy habits, which contribute to your Vitality status and Active Rewards target.
      public static let Item3422: String = CommonStrings.tr("Common", "WDA.onboarding.item3_422")
      /// Earn
      public static let Item3Heading421: String = CommonStrings.tr("Common", "WDA.onboarding.item3_heading_421")
      /// Earn points for your healthy habits, which then contribute to your Vitality status and Vitality Active Rewards target.
      public static let Item3Softbank422: String = CommonStrings.tr("Common", "WDA.onboarding.item3_softbank_422")
      /// Link Later
      public static let LinkLaterTitle778: String = CommonStrings.tr("Common", "WDA.onboarding.link_later_title_778")
      /// Link Now
      public static let LinkNowTitle777: String = CommonStrings.tr("Common", "WDA.onboarding.link_now_title_777")
    }

    public enum Other {

      public enum Item {
        /// Sleep
        public static let SleepText449: String = CommonStrings.tr("Common", "WDA.other.item.sleep_text_449")
      }
    }

    public enum PhysicalActivity {
      /// Speed
      public static let ItemSpeedText441: String = CommonStrings.tr("Common", "WDA.physical_activity.item_speed_text_441")
      /// Physical Activity
      public static let SectionHeader438: String = CommonStrings.tr("Common", "WDA.physical_activity.section_header_438")
    }

    public enum PointsEarning {
      /// Body Mass Index
      public static let BmiTitleSoftbank2424: String = CommonStrings.tr("Common", "wda.points_earning.bmi_title_softbank_2424")
      /// 
      public static let BmiTitleSoftbank9999: String = CommonStrings.tr("Common", "WDA.points_earning.bmi_title_softbank_9999")
      /// Points Earning Metrics
      public static let MetricsHeader435: String = CommonStrings.tr("Common", "WDA.points_earning.metrics_header_435")
      /// Points earning measures
      public static let MetricsHeaderSoftbank435: String = CommonStrings.tr("Common", "WDA.points_earning.metrics_header_softbank_435")
    }

    public enum PointsEarningMetrics {
      /// These metrics are used to track your healthy habits to earn you points.\n\nYour device or app may be capable of tracking some or all of these metrics. Please ensure you verify its capabilities. %1$@
      public static func Content436(_ p1: String) -> String {
        return CommonStrings.tr("Common", "WDA.points_earning_metrics.content_436", p1)
      }
      /// We use these measures to track your healthy habits, which earns you points.\n\nDevice or app may be capable of tracking some or all of the above metrics. Please ensure you verify its capabilities. %1$@
      public static func ContentSoftbank436(_ p1: String) -> String {
        return CommonStrings.tr("Common", "WDA.points_earning_metrics.content_softbank_436", p1)
      }
      /// Learn more...
      public static let LearnMoreLinkText437: String = CommonStrings.tr("Common", "WDA.points_earning_metrics.learn_more_link_text_437")
    }
  }

  public enum Welcome {
    /// You will need the authentication code sent to you by your employer to register if you are new to the app
    public static let MessageText367: String = CommonStrings.tr("Common", "welcome.message_text_367")
  }

  public enum Wellnesspartners {
    /// Wellness Partners
    public static let ScreenTitle608: String = CommonStrings.tr("Common", "WellnessPartners.screen_title_608")
  }

  public enum Ww {
    /// Activate Benefit
    public static let ActivateBenefitButtonTitle1326: String = CommonStrings.tr("Common", "ww.activate_benefit_button_title_1326")
  }
}
// swiftlint:enable explicit_type_interface identifier_name line_length nesting type_body_length type_name

extension CommonStrings {
    fileprivate static func tr(_ table: String, _ key: String, _ args: CVarArg...) -> String {
        // check main bundle first for the string,
        // otherwise fall back to what is available in VitalityKit
        var bundle = Bundle.main
        var result = bundle.localizedString(forKey: key, value: nil, table: table)
        // if the result is the key, it means there isn't a localized value 
        // in the application's main bundle, so we fall back to the VitalityKit
        // framework to get the value for the key.
        if result.lowercased() == key.lowercased() {
            bundle = Bundle(for: CommonStringsBundleToken.self)
            result = bundle.localizedString(forKey: key, value: nil, table: table)
        }
        if args.count > 0 {
          result = String(format: result, arguments: args)
        } else {
          // function call added to allow escaping of % characters, 
          // strings not sent through String(format:) 
          // print out as string litterals
          result = String(format: result)
        }

        #if DEBUG
            if ProcessInfo.processInfo.arguments.contains("ALWAYS_RETURN_LOCALIZATION_KEYS") {
                return key
            }
        #endif
        return result
    }
}

internal final class CommonStringsBundleToken {}
