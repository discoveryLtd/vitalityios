import VIACommon
import VIAUIKit
import VIACore
import UIKit

extension AppDelegate: AppearanceColorDataSource {
    class func ecuadorColor() -> UIColor {
        return UIColor(red: 200.0 / 255.0, green: 16.0 / 255.0, blue: 46.0 / 255.0, alpha: 1.0) // hex - #C8102E
    }
    
    // MARK: AppearanceDataSource
    
    public var primaryColor: UIColor {
        return AppDelegate.ecuadorColor()
    }
    
    public var tabBarBackgroundColor: UIColor {
        return .white
    }
    
    public var tabBarBarTintColor: UIColor {
        return .white
    }
    
    public var tabBarTintColor: UIColor? {
        return UIColor.primaryColor()
    }
    
    public var unselectedItemTintColor: UIColor? {
        return nil
    }
    
    public var getInsurerGlobalTint: UIColor? {
        // TODO: valtomol use this once BE is returning correct tint color:
        // return VIAAppearance.default.primaryColorFromServer
        
        return VIAAppearance.default.dataSource?.primaryColor
    }
    
    public func getSplashScreenGradientTop() -> UIColor{
        // TODO: valtomol use this once BE is returning correct splashscreen color:
        // return UIColor(hexString: AppConfigFeature.insurerGradientColor1Hex())
        
        return UIColor.white
    }
    
    public func getSplashScreenGradientBottom() -> UIColor{
        // TODO: valtomol use this once BE is returning correct splashscreen color:
        // return UIColor(hexString: AppConfigFeature.insurerGradientColor2Hex())
        
        return UIColor.white
    }
}

extension AppDelegate: AppearanceIconographyDataSource {
    
    public func homeLogo(for locale: Locale) -> UIImage {
        return EcuadorAsset.homeLogo.image
    }
    
    public func loginLogo(for locale: Locale) -> UIImage {
        return EcuadorAsset.homeLogo.image
    }
    
    public func splashLogo(for locale: Locale = Locale.current) -> UIImage {
        return EcuadorAsset.loginLogo.image
    }
}
