//
//  VAARActivityHistoryViewModel.swift
//  Ecuador
//
//  Created by Val Tomol on 19/03/2019.
//  Copyright © 2019 Glucode. All rights reserved.
//

import Foundation
import VIAUtilities
import VitalityKit
import RealmSwift
import SwiftDate
import VIAActiveRewards

public class VAARActivityHistoryViewModel: VIAARActivityHistoryViewModel {
    public override func configureSectionsAndGoalItemData() {
        arRealm.refresh()
        
        let goalTrackers = arRealm.allARGoalTrackersExcludingFutureGoals()
        
        var unsortedDates: Set<Date> = []
        for goalTracker in goalTrackers {
            if isMonthInYearAlreadySaved(monthDate: goalTracker.effectiveTo, dateSet: unsortedDates) == false {
                unsortedDates.insert(goalTracker.effectiveTo)
            }
        }
        
        monthSections = unsortedDates.sorted(by: {$0 > $1})
        
        var ObjectsBySection = [Results<ARGoalTracker>]()
        for section in monthSections {
            // Sort activites based on the start date. 
            let unsortedObjectsForSection = goalTrackers.filterForElementsBetweenStartAndEnd(month: section, fieldName: "effectiveFrom")
            let sortedObjectsForSection = unsortedObjectsForSection.sorted(byKeyPath: "effectiveFrom", ascending: false)
            ObjectsBySection.append(sortedObjectsForSection)
        }
        goalTrackersBySection = ObjectsBySection
        configureGoalItemsData()
    }
}
