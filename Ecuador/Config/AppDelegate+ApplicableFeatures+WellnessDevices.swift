//
//  AppDelegate+ApplicableFeatures+WellnessDevices.swift
//  VitalityActive
//
//  Created by Dexter Anthony Ambrad on 6/6/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import VIACommon
import VIAUIKit
import VitalityKit

extension AppDelegate{
    public var shouldDisplayWellnessDevicesAndDataSharingConsent: Bool? {
        get {
            return true
        }
    }
    
    public var earnPointsByBMI: Bool? {
        get {
            return false
        }
    }
    
    public func getThirdOnboardingItem() -> OnboardingContentItem {
        
        return OnboardingContentItem(heading: CommonStrings.Wda.Onboarding.Item3Heading421, content:  CommonStrings.Wda.Onboarding.Item3422, image: VIAUIKitAsset.WellnessDevices.wellnessDevicesPoints.image)
    }
}
