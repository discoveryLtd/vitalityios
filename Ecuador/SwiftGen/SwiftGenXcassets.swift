// Generated using SwiftGen, by O.Halligon — https://github.com/SwiftGen/SwiftGen

#if os(OSX)
import AppKit.NSImage
public typealias EcuadorColor = NSColor
public typealias EcuadorImage = NSImage
#elseif os(iOS) || os(tvOS) || os(watchOS)
import UIKit.UIImage
public typealias EcuadorColor = UIColor
public typealias EcuadorImage = UIImage
#endif

// swiftlint:disable file_length

public typealias EcuadorAssetType = EcuadorImageAsset

public struct EcuadorImageAsset: Equatable {
	fileprivate var name: String

	public var templateImage: EcuadorImage {
		let bundle = Bundle(for: BundleToken.self)
		#if os(iOS) || os(tvOS)
		let image = EcuadorImage(named: name, in: bundle, compatibleWith: nil)?.withRenderingMode(.alwaysTemplate)
		#elseif os(OSX)
		let image = bundle.image(forResource: name).withRenderingMode(.alwaysTemplate)
		#elseif os(watchOS)
		let image = EcuadorImage(named: name).withRenderingMode(.alwaysTemplate)
		#endif
		guard let result = image else { fatalError("Unable to load template image named \(name).") }
		return result
	}

	public var image: EcuadorImage {
		let bundle = Bundle(for: BundleToken.self)
		#if os(iOS) || os(tvOS)
		let image = EcuadorImage(named: name, in: bundle, compatibleWith: nil)
		#elseif os(OSX)
		let image = bundle.image(forResource: name)
		#elseif os(watchOS)
		let image = EcuadorImage(named: name)
		#endif
		guard let result = image else { fatalError("Unable to load image named \(name).") }
		return result
	}

	public static func ==(lhs: EcuadorImageAsset, rhs: EcuadorImageAsset) -> Bool {
        return lhs.image == rhs.image
    }
}

public struct EcuadorColorAsset {
fileprivate var name: String

#if swift(>=3.2)
@available(iOS 11.0, tvOS 11.0, watchOS 4.0, OSX 10.13, *)
var color: EcuadorColor {
return EcuadorColor(asset: self)
}
#endif
}

// swiftlint:disable identifier_name line_length nesting type_body_length type_name
public enum EcuadorAsset {
  public static let homeLogoOld = EcuadorImageAsset(name: "homeLogo-old")
  public enum Rewards {
    public static let cinemarkAppIcon = EcuadorImageAsset(name: "cinemarkAppIcon")
    public static let juanvaldezAppIcon = EcuadorImageAsset(name: "juanvaldezAppIcon")
    public static let juanvaldez = EcuadorImageAsset(name: "juanvaldez")
    public static let sampleQrcode = EcuadorImageAsset(name: "sample-qrcode")
    public static let cinemark = EcuadorImageAsset(name: "cinemark")
  }
  public static let loginLogoOld = EcuadorImageAsset(name: "loginLogo-old")
  public static let membershipBanner = EcuadorImageAsset(name: "membershipBanner")
  public static let loginLogo = EcuadorImageAsset(name: "loginLogo")
  public static let homeLogo = EcuadorImageAsset(name: "homeLogo")

  // swiftlint:disable trailing_comma
  public static let allColors: [EcuadorColorAsset] = [
  ]
  public static let allImages: [EcuadorImageAsset] = [
    homeLogoOld,
    Rewards.cinemarkAppIcon,
    Rewards.juanvaldezAppIcon,
    Rewards.juanvaldez,
    Rewards.sampleQrcode,
    Rewards.cinemark,
    loginLogoOld,
    membershipBanner,
    loginLogo,
    homeLogo,
  ]
  // swiftlint:enable trailing_comma
  @available(*, deprecated, renamed: "allImages")
  public static let allValues: [EcuadorAssetType] = allImages
}
// swiftlint:enable identifier_name line_length nesting type_body_length type_name

extension EcuadorImage {
@available(iOS 1.0, tvOS 1.0, watchOS 1.0, *)
@available(OSX, deprecated,
message: "This initializer is unsafe on macOS, please use the EcuadorImageAsset.image property")
convenience init!(asset: EcuadorAssetType) {
#if os(iOS) || os(tvOS)
let bundle = Bundle(for: BundleToken.self)
self.init(named: asset.name, in: bundle, compatibleWith: nil)
#elseif os(OSX) || os(watchOS)
self.init(named: asset.name)
#endif
}
}

extension EcuadorColor {
#if swift(>=3.2)
@available(iOS 11.0, tvOS 11.0, watchOS 4.0, OSX 10.13, *)
convenience init!(asset: EcuadorColorAsset) {
let bundle = Bundle(for: BundleToken.self)
#if os(iOS) || os(tvOS)
self.init(named: asset.name, in: bundle, compatibleWith: nil)
#elseif os(OSX)
self.init(named: asset.name, bundle: bundle)
#elseif os(watchOS)
self.init(named: asset.name)
#endif
}
#endif
}

private final class BundleToken {}
