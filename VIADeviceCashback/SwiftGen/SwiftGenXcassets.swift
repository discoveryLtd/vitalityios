// Generated using SwiftGen, by O.Halligon — https://github.com/SwiftGen/SwiftGen

#if os(OSX)
import AppKit.NSImage
public typealias VIADeviceCashbackColor = NSColor
public typealias VIADeviceCashbackImage = NSImage
#elseif os(iOS) || os(tvOS) || os(watchOS)
import UIKit.UIImage
public typealias VIADeviceCashbackColor = UIColor
public typealias VIADeviceCashbackImage = UIImage
#endif

// swiftlint:disable file_length

public typealias VIADeviceCashbackAssetType = VIADeviceCashbackImageAsset

public struct VIADeviceCashbackImageAsset: Equatable {
	fileprivate var name: String

	public var templateImage: VIADeviceCashbackImage {
		let bundle = Bundle(for: BundleToken.self)
		#if os(iOS) || os(tvOS)
		let image = VIADeviceCashbackImage(named: name, in: bundle, compatibleWith: nil)?.withRenderingMode(.alwaysTemplate)
		#elseif os(OSX)
		let image = bundle.image(forResource: name).withRenderingMode(.alwaysTemplate)
		#elseif os(watchOS)
		let image = VIADeviceCashbackImage(named: name).withRenderingMode(.alwaysTemplate)
		#endif
		guard let result = image else { fatalError("Unable to load template image named \(name).") }
		return result
	}

	public var image: VIADeviceCashbackImage {
		let bundle = Bundle(for: BundleToken.self)
		#if os(iOS) || os(tvOS)
		let image = VIADeviceCashbackImage(named: name, in: bundle, compatibleWith: nil)
		#elseif os(OSX)
		let image = bundle.image(forResource: name)
		#elseif os(watchOS)
		let image = VIADeviceCashbackImage(named: name)
		#endif
		guard let result = image else { fatalError("Unable to load image named \(name).") }
		return result
	}

	public static func ==(lhs: VIADeviceCashbackImageAsset, rhs: VIADeviceCashbackImageAsset) -> Bool {
        return lhs.image == rhs.image
    }
}

public struct VIADeviceCashbackColorAsset {
fileprivate var name: String

#if swift(>=3.2)
@available(iOS 11.0, tvOS 11.0, watchOS 4.0, OSX 10.13, *)
var color: VIADeviceCashbackColor {
return VIADeviceCashbackColor(asset: self)
}
#endif
}

// swiftlint:disable identifier_name line_length nesting type_body_length type_name
public enum VIADeviceCashbackAsset {
  public enum Cells {
    public static let arrowDrill = VIADeviceCashbackImageAsset(name: "arrowDrill")
  }
  public enum HowToTrackCashbacks {
    public static let verticalDashLine = VIADeviceCashbackImageAsset(name: "vertical_dash_line")
    public static let steps = VIADeviceCashbackImageAsset(name: "steps")
    public static let pinkCoin = VIADeviceCashbackImageAsset(name: "pinkCoin")
    public static let heartRate = VIADeviceCashbackImageAsset(name: "heart_rate")
    public static let orangeCoin = VIADeviceCashbackImageAsset(name: "orangeCoin")
    public static let blueCoin = VIADeviceCashbackImageAsset(name: "blueCoin")
    public static let greenCoin = VIADeviceCashbackImageAsset(name: "greenCoin")
  }
  public enum ActivateDeviceCashback {
    public static let deviceCashbackDetailsSubmitted = VIADeviceCashbackImageAsset(name: "device_cashback_details_submitted")
    public enum Attachment {
      public static let addPhoto = VIADeviceCashbackImageAsset(name: "addPhoto")
      public static let cameraLarge = VIADeviceCashbackImageAsset(name: "cameraLarge")
      public static let trashSmall = VIADeviceCashbackImageAsset(name: "trashSmall")
    }
    public enum Activated {
      public static let deviceCashbackActivated = VIADeviceCashbackImageAsset(name: "device_cashback_activated")
    }
  }
  public enum DeviceCashback {
    public enum CashbackEarned {
      public static let greenCoinHome = VIADeviceCashbackImageAsset(name: "greenCoinHome")
      public static let cashbackEarnedGraphic = VIADeviceCashbackImageAsset(name: "cashbackEarnedGraphic")
      public static let blueCoinHome = VIADeviceCashbackImageAsset(name: "blueCoinHome")
      public static let pinkCoinHome = VIADeviceCashbackImageAsset(name: "pinkCoinHome")
      public static let orangeCoinHome = VIADeviceCashbackImageAsset(name: "orangeCoinHome")
    }
    public enum Landing {
      public static let calendar = VIADeviceCashbackImageAsset(name: "calendar")
      public static let howToTrack = VIADeviceCashbackImageAsset(name: "howToTrack")
      public static let questionMarkSmall = VIADeviceCashbackImageAsset(name: "questionMarkSmall")
      public static let learnMoreSmall = VIADeviceCashbackImageAsset(name: "learnMoreSmall")
      public static let cashbackCoinGraphic = VIADeviceCashbackImageAsset(name: "cashbackCoinGraphic")
      public static let illustrationRunning = VIADeviceCashbackImageAsset(name: "illustrationRunning")
      public static let devicesSmall = VIADeviceCashbackImageAsset(name: "devicesSmall")
      public static let cashbacksEarned = VIADeviceCashbackImageAsset(name: "cashbacksEarned")
      public static let illustrationDelivery = VIADeviceCashbackImageAsset(name: "illustrationDelivery")
    }
    public enum LearnMore {
      public static let learnMoreActivate = VIADeviceCashbackImageAsset(name: "learnMoreActivate")
      public static let learnMoreGetADevice = VIADeviceCashbackImageAsset(name: "learnMoreGetADevice")
      public static let learnMoreCashbacks = VIADeviceCashbackImageAsset(name: "learnMoreCashbacks")
    }
    public enum Onboarding {
      public static let getCashback = VIADeviceCashbackImageAsset(name: "getCashback")
      public static let getYourDevice = VIADeviceCashbackImageAsset(name: "getYourDevice")
      public static let getActive = VIADeviceCashbackImageAsset(name: "getActive")
    }
  }

  // swiftlint:disable trailing_comma
  public static let allColors: [VIADeviceCashbackColorAsset] = [
  ]
  public static let allImages: [VIADeviceCashbackImageAsset] = [
    Cells.arrowDrill,
    HowToTrackCashbacks.verticalDashLine,
    HowToTrackCashbacks.steps,
    HowToTrackCashbacks.pinkCoin,
    HowToTrackCashbacks.heartRate,
    HowToTrackCashbacks.orangeCoin,
    HowToTrackCashbacks.blueCoin,
    HowToTrackCashbacks.greenCoin,
    ActivateDeviceCashback.deviceCashbackDetailsSubmitted,
    ActivateDeviceCashback.Attachment.addPhoto,
    ActivateDeviceCashback.Attachment.cameraLarge,
    ActivateDeviceCashback.Attachment.trashSmall,
    ActivateDeviceCashback.Activated.deviceCashbackActivated,
    DeviceCashback.CashbackEarned.greenCoinHome,
    DeviceCashback.CashbackEarned.cashbackEarnedGraphic,
    DeviceCashback.CashbackEarned.blueCoinHome,
    DeviceCashback.CashbackEarned.pinkCoinHome,
    DeviceCashback.CashbackEarned.orangeCoinHome,
    DeviceCashback.Landing.calendar,
    DeviceCashback.Landing.howToTrack,
    DeviceCashback.Landing.questionMarkSmall,
    DeviceCashback.Landing.learnMoreSmall,
    DeviceCashback.Landing.cashbackCoinGraphic,
    DeviceCashback.Landing.illustrationRunning,
    DeviceCashback.Landing.devicesSmall,
    DeviceCashback.Landing.cashbacksEarned,
    DeviceCashback.Landing.illustrationDelivery,
    DeviceCashback.LearnMore.learnMoreActivate,
    DeviceCashback.LearnMore.learnMoreGetADevice,
    DeviceCashback.LearnMore.learnMoreCashbacks,
    DeviceCashback.Onboarding.getCashback,
    DeviceCashback.Onboarding.getYourDevice,
    DeviceCashback.Onboarding.getActive,
  ]
  // swiftlint:enable trailing_comma
  @available(*, deprecated, renamed: "allImages")
  public static let allValues: [VIADeviceCashbackAssetType] = allImages
}
// swiftlint:enable identifier_name line_length nesting type_body_length type_name

extension VIADeviceCashbackImage {
@available(iOS 1.0, tvOS 1.0, watchOS 1.0, *)
@available(OSX, deprecated,
message: "This initializer is unsafe on macOS, please use the VIADeviceCashbackImageAsset.image property")
convenience init!(asset: VIADeviceCashbackAssetType) {
#if os(iOS) || os(tvOS)
let bundle = Bundle(for: BundleToken.self)
self.init(named: asset.name, in: bundle, compatibleWith: nil)
#elseif os(OSX) || os(watchOS)
self.init(named: asset.name)
#endif
}
}

extension VIADeviceCashbackColor {
#if swift(>=3.2)
@available(iOS 11.0, tvOS 11.0, watchOS 4.0, OSX 10.13, *)
convenience init!(asset: VIADeviceCashbackColorAsset) {
let bundle = Bundle(for: BundleToken.self)
#if os(iOS) || os(tvOS)
self.init(named: asset.name, in: bundle, compatibleWith: nil)
#elseif os(OSX)
self.init(named: asset.name, bundle: bundle)
#elseif os(watchOS)
self.init(named: asset.name)
#endif
}
#endif
}

private final class BundleToken {}
