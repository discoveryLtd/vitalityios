//
//  DCLandingViewController.swift
//  VitalityActive
//
//  Created by Von Kervin R. Tuico on 22/03/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import VIAUIKit
import VIACommon
import VIAUtilities
import VitalityKit

class DCLandingViewController: VIATableViewController, ImproveYourHealthTintable {
    @IBAction func unwindToDCLandingViewController(segue:UIStoryboardSegue) { }
    
    var hasDoneInitialLoad: Bool = false
    
    let viewModel = DCLandingViewModel()
    let globalRealm = DataProvider.newRealm()
    var shouldShowDevicesRow: Bool? = false
    var shouldShowCashbackHistoryRow: Bool? = false
    var cardStatus: CardStatusTypeRef?
    weak var dataPrivacyViewController: DeviceCashbackDataPrivacyViewController?
    
    // MARK: View Lifecycles
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = CommonStrings.Dc.HomeCard.Title1455
        initUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.tableView.reloadData()
        VIAAppearance.setGlobalTintColorToImproveYourHealth()
        
        if cardStatus == nil {
            let cards = globalRealm.allHomeCards()
            
            // Get card status to determine the content of the header view.
            for card in cards {
                if card.type == CardTypeRef.DeviceCashback {
                    cardStatus = card.status
                }
            }
            
            initMenuItems()
        } else {
            initMenuItems()
            self.tableView.reloadData()
        }
        
        if AppSettings.hasShownDeviceCashbackOnboarding() && !hasDoneInitialLoad {
            // Get Device Benefit details.
            initAPICall()
        }
        
        showOnboardingIfNeeded()
    }
    
    func initUI() {
        configureAppearance()
        configureTableView()
    }
    
    func configureAppearance() {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.makeNavigationBarTransparent()
        
        self.hideBackButtonTitle()
    }
    
    func configureTableView() {
        self.tableView.register(VIATableViewSectionHeaderFooterView.nib(), forHeaderFooterViewReuseIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier)
        self.tableView.register(DCHeaderTableViewCell.nib(), forCellReuseIdentifier: DCHeaderTableViewCell.defaultReuseIdentifier)
        self.tableView.register(VIATableViewCell.nib(), forCellReuseIdentifier: VIATableViewCell.defaultReuseIdentifier)
        self.tableView.register(DCHeaderStatusCell.nib(), forCellReuseIdentifier: DCHeaderStatusCell.defaultReuseIdentifier)
        self.tableView.register(DCEnabledPointsTableViewCell.nib(), forCellReuseIdentifier: DCEnabledPointsTableViewCell.defaultReuseIdentifier)
        self.tableView.register(DCDisabledPointsTableViewCell.nib(), forCellReuseIdentifier: DCDisabledPointsTableViewCell.defaultReuseIdentifier)
        self.tableView.register(DCCashbackActivityCell.nib(), forCellReuseIdentifier: DCCashbackActivityCell.defaultReuseIdentifier)
        self.tableView.estimatedRowHeight = 96
        self.tableView.tableFooterView = UIView()
        self.tableView.backgroundColor = UIColor.tableViewBackground()
        
        self.tableView.refreshControl = UIRefreshControl()
        self.tableView.refreshControl?.tintColor = UIColor.cellSeperator()
        self.tableView.refreshControl?.addTarget(self, action: #selector(performRefresh), for: .valueChanged)
    }
    
    func initMenuItems() {
        if cardStatus == CardStatusTypeRef(rawValue: CardStatusTypeRef.NotStarted.rawValue) {
            shouldShowDevicesRow = false
            shouldShowCashbackHistoryRow = false
        } else if cardStatus == CardStatusTypeRef(rawValue: CardStatusTypeRef.InProgress.rawValue) ||
            cardStatus == CardStatusTypeRef(rawValue: CardStatusTypeRef.Achieved.rawValue) {
            shouldShowDevicesRow = true
            shouldShowCashbackHistoryRow = true
        } else {
            shouldShowDevicesRow = true
            shouldShowCashbackHistoryRow = false 
        }
        
        // Set segue identifier of header view buttons.
        viewModel.setHeaderButtonInfo = { [weak self] (token) in
            if token == "showDCWKWebView" {
                self?.purchaseButtonTapped()
            } else {
                self?.performSegue(withIdentifier: token, sender: self)
            }
        }

        viewModel.setSubButtonInfo = { [weak self] (token) in
            self?.performSegue(withIdentifier: token, sender: self)
        }
    }
    
    func performRefresh() {
        initAPICall()
        self.tableView.refreshControl?.endRefreshing()
    }
    
    // MARK: Networking
    func initAPICall() {
        self.showHUDOnView(view: self.navigationController?.view)
        viewModel.getDeviceBenefit { [weak self] (error) in
            DispatchQueue.main.async {
                self?.hasDoneInitialLoad = true
                self?.hideHUDFromView(view: self?.navigationController?.view)
                self?.viewModel.userHasPoints = self?.viewModel.showPointsHeader()
                guard error == nil else {
                    self?.tableView.refreshControl?.endRefreshing()
                    self?.initErrorHandler(error!)
                    return
                }
                
                self?.tableView.reloadData()
            }
        }
    }
    
    func purchaseButtonTapped() {
        showDCDataSharingConsentViewController()
    }
    
    func showDCDataSharingConsentViewController() {
        self.performSegue(withIdentifier:"showDCDataConsent", sender: nil)
    }
    
    func getPartnerRedirectUrl(completion: @escaping (Error?) -> Void) {
        self.showHUDOnView(view: self.navigationController?.view)
        viewModel.partnerRedirect { [weak self] error in
            self?.hideHUDFromView(view: self?.navigationController?.view)
            guard error == nil else {
                // self?.initErrorHandler(error!)
                completion(error)
                return
            }
            
            completion(nil)
        }
    }
    
    func performPartnerRedirect(segueIdentifier: String) {
        presentWebView(withSegueIdentifier: segueIdentifier)
    }
    
    public func presentWebView(withSegueIdentifier identifier: String) {
        performSegue(withIdentifier: identifier, sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDCWKWebView" {
            guard let redirectURL = viewModel.partnerRedirectUrl else { return }
            guard let redirectCommonAuthId = viewModel.partnerCommonAuthId else { return }
            if let webVC = segue.destination as? DCWKWebViewController {
                webVC.url = redirectURL
                webVC.commonAuthId = redirectCommonAuthId
            }
        } else if segue.identifier == "showDCDataConsent" {
            if let viewController = segue.destination as? DeviceCashbackDataPrivacyViewController {
                dataPrivacyViewController = viewController
                dataPrivacyViewController?.completion = { [weak self] in
                    if let _ = self?.viewModel.partnerRedirectUrl {
                        self?.performPartnerRedirect(segueIdentifier: "showDCWKWebView")
                    } else {
                        self?.getPartnerRedirectUrl { [weak self] error in
                            guard error == nil else { return }
                            self?.performPartnerRedirect(segueIdentifier: "showDCWKWebView")
                        }
                    }
                }
            }
        }
    }
    
    func initErrorHandler(_ error: Error) {
        let backendError = error as? BackendError ?? BackendError.other
        self.handleBackendErrorWithAlert(backendError)
    }
    
    func showOnboardingIfNeeded() {
        if !AppSettings.hasShownDeviceCashbackOnboarding() {
            self.performSegue(withIdentifier: "showOnBoarding", sender: self)
        }
    }
    
    // MARK: TableView Datasource and Delegate
    override func numberOfSections(in tableView: UITableView) -> Int {
        return Sections.allValues.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let hasPoints = viewModel.userHasPoints else {
            return 0
        }
        
        switch Sections.allValues[section] {
        case .header:
            return hasPoints ? StatusHeader.allValues.count : 0
        case .activity:
            return hasPoints ? viewModel.getActivityCount() : 0
        case .menu:
            return menuRows.count
        }
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let hasPoints = viewModel.userHasPoints else {
            return nil
        }
        
        switch Sections.allValues[section] {
        case .header:
            return viewModel.setMainHeaderView(cardStatus: cardStatus ?? CardStatusTypeRef.NotStarted, tableView: tableView)
        case .activity:
            return hasPoints ? sectionHeaderView(at: section) : nil
        default:
            break
        }
        
        return nil
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        guard let hasPoints = viewModel.userHasPoints else {
            return UITableViewAutomaticDimension
        }
        
        switch Sections.allValues[section] {
        case .header:
            return hasPoints ? 0 : UITableViewAutomaticDimension
        case .menu:
            return 0
        default:
            return UITableViewAutomaticDimension
        }
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        switch Sections.allValues[section] {
        case .header:
            return viewModel.setFooterView(cardStatus: cardStatus ?? CardStatusTypeRef.NotStarted, tableView: tableView)
        default:
            break
        }
        
        return nil
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let hasPoints = viewModel.userHasPoints else {
            return self.tableView.defaultTableViewCell()
        }
        
        if hasPoints {
            switch Sections.allValues[indexPath.section] {
            case .header:
                return statusCell(at: indexPath)
            case .activity:
                return activityCell(at: indexPath)
            case .menu:
                return menuCell(at: indexPath)
            }
        } else {
            switch Sections.allValues[indexPath.section] {
            case .menu:
                return menuCell(at: indexPath)
            default:
                break
            }
        }
        
        return self.tableView.defaultTableViewCell()
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if let hasPoints = viewModel.userHasPoints, hasPoints {
            if Sections.allValues[indexPath.section] == .header && StatusHeader(rawValue: indexPath.row) == .spacer {
                return 25
            }
        }
        
        return UITableViewAutomaticDimension
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let section = Sections(rawValue: indexPath.section) {
            switch section {
            case .menu:
                menuAction(at: indexPath)
            default:
                break
            }
        }
    }
    
    func sectionHeaderView(at section: Int) -> UIView? {
        let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier)
        let footerView = view as! VIATableViewSectionHeaderFooterView
        
        footerView.labelText = viewModel.activitySectionHeaderTitle()
        
        return footerView
    }
    
    func statusCell(at indexPath: IndexPath) -> UITableViewCell {
        let row = StatusHeader(rawValue: indexPath.row)
        
        if row == .month {
            let cell = self.tableView.dequeueReusableCell(withIdentifier: DCHeaderStatusCell.defaultReuseIdentifier, for: indexPath)
            let monthCell = cell as! DCHeaderStatusCell
            
            let currentCountText = viewModel.getCurrentBenefitPeriod()
            let benefitTermMonthsText = viewModel.getBenefitTermMonths()
            
            monthCell.monthCountLabelText = viewModel.monthCountTitle(currentCount: currentCountText, totalCount: benefitTermMonthsText)
            monthCell.monthLabelText = viewModel.getEffectiveGoalMonth()
            
            cell.selectionStyle = .none
            cell.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0)
            return cell
        } else if row == .spacer {
            let cell = self.tableView.defaultTableViewCell()
            
            cell.selectionStyle = .none
            cell.separatorInset = UIEdgeInsetsMake(0, self.view.bounds.width, 0, 0)
            return cell
        } else {
            let rowTierlLevel = StatusHeader.allValues[indexPath.row].tierLevel()
            var cell: UITableViewCell
            
            if viewModel.isTierRowActivated(at: rowTierlLevel) {
                let activeCell = self.tableView.dequeueReusableCell(withIdentifier: DCEnabledPointsTableViewCell.defaultReuseIdentifier, for: indexPath) as! DCEnabledPointsTableViewCell
                cell = configureActiveCell(with: activeCell, at: indexPath)
            } else {
                let inActiveCell = self.tableView.dequeueReusableCell(withIdentifier: DCDisabledPointsTableViewCell.defaultReuseIdentifier, for: indexPath) as! DCDisabledPointsTableViewCell
                cell = configureInActiveCell(with: inActiveCell, at: indexPath)
            }
            
            cell.selectionStyle = .none
            cell.separatorInset = UIEdgeInsetsMake(0, cell.bounds.size.width, 0, 0)
            return cell
        }
    }
    
    func configureActiveCell(with activeCell: DCEnabledPointsTableViewCell, at indexPath: IndexPath) -> DCEnabledPointsTableViewCell {
        let rowTierlLevel = StatusHeader.allValues[indexPath.row].tierLevel()
        
        activeCell.coinsUIImageView.image = viewModel.getCoinImage(at: rowTierlLevel)
        activeCell.verticalDashLineUIImageView.image = VIADeviceCashbackAsset.HowToTrackCashbacks.verticalDashLine.image
        activeCell.setPercentageLabel = viewModel.getAccumulatedAmount(at: rowTierlLevel)
        activeCell.setReachPointsLabel = viewModel.getReachPointsText(at: rowTierlLevel)
        
        let lastRowIndex = self.tableView.numberOfRows(inSection: indexPath.section)
        if indexPath.row == lastRowIndex - 1 {
            activeCell.verticalDashLineUIImageView.isHidden = true
        } else {
            activeCell.verticalDashLineUIImageView.isHidden = false
        }
        
        return activeCell
    }
    
    func configureInActiveCell(with inActiveCell: DCDisabledPointsTableViewCell, at indexPath: IndexPath) -> DCDisabledPointsTableViewCell {
        let rowTierlLevel = StatusHeader.allValues[indexPath.row].tierLevel()
        
        inActiveCell.coinsUIImageView.image = viewModel.getCoinImage(at: rowTierlLevel)
        inActiveCell.verticalDashLineUIImageView.image = VIADeviceCashbackAsset.HowToTrackCashbacks.verticalDashLine.image
        inActiveCell.setPercentageLabel = viewModel.getAccumulatedAmount(at: rowTierlLevel)
        inActiveCell.setReachPointsLabel = viewModel.getReachPointsText(at: rowTierlLevel)
        
        let lastRowIndex = self.tableView.numberOfRows(inSection: indexPath.section)
        if indexPath.row == lastRowIndex - 1 {
            inActiveCell.verticalDashLineUIImageView.isHidden = true
        } else {
            inActiveCell.verticalDashLineUIImageView.isHidden = false
        }
        
        return inActiveCell
    }
    
    func activityCell(at indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: DCCashbackActivityCell.defaultReuseIdentifier, for: indexPath)
        let activityCell = cell as! DCCashbackActivityCell
        
        let hasActivity = viewModel.hasActivityItems()
        let row = indexPath.row
        
        activityCell.pointsLabelText = hasActivity ? viewModel.getActivityPoints(at: row) : nil
        activityCell.stepsLabelText = hasActivity ? viewModel.getActivityType(at: row) : viewModel.emptyActivityTitle()
        activityCell.deviceLabelText = hasActivity ? viewModel.getActivityDevice(at: row) : viewModel.emptyActivitySubtitle()
        activityCell.dateLabelText = hasActivity ? viewModel.getActivityDate(at: row) : nil
        
        cell.separatorInset = UIEdgeInsets(top: 0, left: 17, bottom: 0, right: 0)
        return activityCell
    }
    
    func menuCell(at indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: VIATableViewCell.defaultReuseIdentifier, for: indexPath)
        let menuCell = cell as! VIATableViewCell
        
        if let menuItem = Menu(rawValue: menuRows.integer(at: indexPath.row)) {
            switch menuItem {
            case .cashbacksEarned:
                menuCell.labelText = viewModel.cashbackEarnedRowTitle()
                menuCell.cellImage = viewModel.cashbackEarnedRowImage()?.templatedImage
            case .devices:
                menuCell.labelText = viewModel.devicesRowTitle()
                menuCell.cellImage = viewModel.devicesRowImage()?.templatedImage
            case .howToTrackCashbacks:
                menuCell.labelText = viewModel.howToTrackCashbacksRowTitle()
                menuCell.cellImage = viewModel.howToTrackCashbacksRowImage()?.templatedImage
            case .learnMore:
                menuCell.labelText = viewModel.learnMoreRowTitle()
                menuCell.cellImage = viewModel.learnMoreRowImage()?.templatedImage
            case .help:
                menuCell.labelText = viewModel.helpRowTitle()
                menuCell.cellImage = viewModel.helpRowImage()?.templatedImage
            }
        }
        
        cell.separatorInset = UIEdgeInsets(top: 0, left: 52, bottom: 0, right: 0)
        cell.accessoryType = .disclosureIndicator
        return cell
    }
    
    func menuAction(at indexPath: IndexPath) {
        if let menuItem = Menu(rawValue: menuRows.integer(at: indexPath.row)) {
            switch menuItem {
            case .cashbacksEarned:
                self.performSegue(withIdentifier: viewModel.cashbackEarnedRowSegueIdentifier(), sender: nil)
            case .devices:
                self.performSegue(withIdentifier: viewModel.devicesRowSegueIdentifier(), sender: nil)
            case .howToTrackCashbacks:
                self.performSegue(withIdentifier: viewModel.howToTrackCashbacksRowRowSegueIdentifier(), sender: nil)
            case .learnMore:
                self.performSegue(withIdentifier: viewModel.learnMoreRowSegueIdentifier(), sender: nil)
            case .help:
                self.performSegue(withIdentifier: viewModel.helpRowSegueIdentifier(), sender: nil)
            }
        }
    }
}
