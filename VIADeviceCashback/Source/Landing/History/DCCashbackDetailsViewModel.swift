//
//  DCCashbackDetailsViewModel.swift
//  VIADeviceCashback
//
//  Created by Val Tomol on 26/04/2019.
//  Copyright © 2019 Glucode. All rights reserved.
//

import VitalityKit
import RealmSwift

class DCCashbackDetailsViewModel {
    let realm = DataProvider.newRealm()
    internal let cashbackRealm = DataProvider.newGDCRealm()
    
    func getBenefitID() -> Int? {
        let benefits = cashbackRealm.allGDCBenefit()
        return benefits.first?.benefitId
    }
    
    func getGoalTrackerOuts(benefitID: Int, date: String) -> GDCGoalTrackerOuts? {
        let allGetGoalProgressAndDetails = cashbackRealm.allGDCGetGoalProgressAndDetails()
        
        var goalTrackerOuts: List<GDCGoalTrackerOuts> = List<GDCGoalTrackerOuts>()
        for details in allGetGoalProgressAndDetails {
            goalTrackerOuts = details.goalTrackerOuts
            break
        }
        
        var goalTrackerOutValue: GDCGoalTrackerOuts?
        for goalTrackerOut in goalTrackerOuts {
            if goalTrackerOut.agreementId == benefitID && goalTrackerOut.effectiveFrom == date {
                goalTrackerOutValue = goalTrackerOut
                break
            }
        }
        
        return goalTrackerOutValue
    }
    
    func getObjectivePointsEntries(goalTrackerOut: GDCGoalTrackerOuts) -> [GDCObjectivePointsEntries]? {
        var objectiveTrackerValue: [GDCObjectiveTrackers]? = []
        for objectiveTracker in goalTrackerOut.objectiveTrackers {
            objectiveTrackerValue?.append(objectiveTracker)
        }
        
        let sortedObjectiveTrackerValue = objectiveTrackerValue?.sorted(by: { $0.objectiveCode?.compare($1.objectiveCode!) == .orderedDescending})
        
        var objectivePointsEntriesValue: [GDCObjectivePointsEntries]? = []
        if let objectivePointsEntries = sortedObjectiveTrackerValue?.first?.objectivePointsEntries {
            for objectivePointsEntry in objectivePointsEntries {
                objectivePointsEntriesValue?.append(objectivePointsEntry)
            }
        }
        
        return objectivePointsEntriesValue?.sorted(by: { $0.effectiveDate?.compare($1.effectiveDate!) == .orderedDescending}) ?? []
    }
    
    func getGoalProgressAndDetails(completion: @escaping (_ error: Error?) -> Void) {
        let tenantID = realm.getTenantId()
        let partyID = realm.getPartyId()
        let benefits = cashbackRealm.allGDCBenefit()
        let goals = cashbackRealm.allGDCGoal()
        
        var effectiveFrom = ""
        var effectiveTo = ""
        for benefit in benefits {
            let state = benefit.state
            
            guard let dateFrom = state?.effectiveFrom else { return }
            guard let dateTo = state?.effectiveTo else { return }
            
            effectiveFrom = dateFrom
            effectiveTo = dateTo
        }
        
        let effectiveFromDateType = DateFormatter.yearMonthDayFormatter().date(from: effectiveFrom)
        let effectiveToDateType = DateFormatter.yearMonthDayFormatter().date(from: effectiveTo)
        
        var goalKeys: [Int] = []
        for goal in goals {
            goalKeys.append(goal.key)
        }
        
        let request = GetGoalProgressAndDetailsParameters(effectiveDateFrom: effectiveFromDateType, effectiveDateTo: effectiveToDateType, goalKeys: goalKeys, goalStatusTypeKeys: [], partyId: partyID)
        
        Wire.Events.getGDCGoalProgressAndDetails(tenantId: tenantID, request: request) { (error, response) in
            completion(error)
        }
    }
}
