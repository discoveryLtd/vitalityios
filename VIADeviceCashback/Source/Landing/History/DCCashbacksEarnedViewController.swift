//
//  DCCashbacksEarnedViewController.swift
//  VitalityActive
//
//  Created by Steven Layug on 4/11/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import VIAUIKit
import SnapKit
import VitalityKit
import VIAUtilities
import VIACommon

class DCCashbacksEarnedViewController: VIATableViewController {
    var hasDoneInitialLoad: Bool = false
    
    let viewModel = DCCashbacksEarnedViewModel()
    
    var headerImage: UIImage? = nil
    var amountAwarded: String? = ""
    var pointsAwarded: String? = ""
    var dateValue: String? = ""
    var selectedMonth: String? = ""
    
    func initErrorHandler(_ error: Error) {
        let backendError = error as? BackendError ?? BackendError.other
        self.handleBackendErrorWithAlert(backendError)
    }
    
    // MARK: View Lifecycles
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = CommonStrings.Dc.Landing.ActionMenuTitle1469
        initUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if !hasDoneInitialLoad {
            initAPICall()
        }
    }
    
    func initUI() {
        configureAppearance()
        configureTableView()
    }
    
    func configureAppearance() {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.makeNavigationBarTransparent()
        
        self.hideBackButtonTitle()
        
        self.configureEmptyStatusView()
    }
    
    func configureEmptyStatusView() {
        let view = VIAStatusView.viewFromNib(owner: self)!
        
        view.heading = CommonStrings.Dc.CashbacksEarned.EmptyViewTitle1478
        view.message = CommonStrings.Dc.CashbacksEarned.EmptyViewMessage1479
        
        self.view.addSubview(view)
        self.view.bringSubview(toFront: view)
        
        view.snp.makeConstraints { (make) in
            make.width.equalToSuperview()
            make.left.right.top.equalToSuperview()
            make.height.equalToSuperview().offset(-62)
            make.bottom.equalToSuperview().offset(-62)
        }
    }
    
    func removeEmptyStatusView() {
        self.view.subviews.forEach { (view) in
            if view is VIAStatusView {
                view.removeFromSuperview()
            }
        }
    }
    
    func configureTableView() {
        self.tableView.register(VIATableViewSectionHeaderFooterView.nib(), forHeaderFooterViewReuseIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier)
        self.tableView.register(DCHeaderTableViewWithTextCell.nib(), forCellReuseIdentifier: DCHeaderTableViewWithTextCell.defaultReuseIdentifier)
        self.tableView.register(DCEarnedCashbackCell.nib(), forCellReuseIdentifier: DCEarnedCashbackCell.defaultReuseIdentifier)
        self.tableView.register(VIATableViewCell.nib(), forCellReuseIdentifier: VIATableViewCell.defaultReuseIdentifier)
        self.tableView.estimatedRowHeight = 75
        self.tableView.backgroundColor = UIColor.tableViewBackground()
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.makeNavigationBarTransparent()
        self.navigationController?.hideBackButtonTitle()
        
        self.tableView.refreshControl = UIRefreshControl()
        self.tableView.refreshControl?.tintColor = UIColor.cellSeperator()
        self.tableView.refreshControl?.addTarget(self, action: #selector(performRefresh), for: .valueChanged)
    }
    
    func performRefresh() {
        initAPICall()
        self.tableView.refreshControl?.endRefreshing()
    }
    
    func initAPICall() {
        self.showHUDOnView(view: self.view)
        viewModel.loadData(forceUpdate: true) {[weak self] (error) in
            DispatchQueue.main.async {
                self?.hasDoneInitialLoad = true
                self?.hideHUDFromView(view: self?.view)
                guard error == nil else {
                    guard error?.localizedDescription != BackendError.other.localizedDescription else {
                        self?.configureEmptyStatusView()
                        return
                    }
                    
                    self?.initErrorHandler(error!)
                    self?.configureEmptyStatusView()
                    self?.hideHUDFromView(view: self?.view)
                    return
                }
                
                if self?.viewModel.goalTrackersCount() == 0 {
                    self?.configureEmptyStatusView()
                } else {
                    self?.removeEmptyStatusView()
                    self?.tableView.reloadData()
                }
            }
        }
    }

    // MARK: UITableView Datasource and Delegate
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        let section = Sections(rawValue: section)
        if section == .header {
            return 0
        }
        
        return UITableViewAutomaticDimension
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return hasDoneInitialLoad ? Sections.allValues.count : 0
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (section == Sections.cashbacks.rawValue) {
            return viewModel.goalTrackersCount()
        } else if viewModel.goalTrackersCount() == 0 {
            return 0
        } else {
            return 1
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: VIATableViewCell.defaultReuseIdentifier, for: indexPath)
        
        switch Sections.allValues[indexPath.section] {
        case .header:
            return headerCell(at: indexPath)
        case .cashbacks:
            return cashbackCell(at: indexPath)
        case .previousCashbacks:
            cell.textLabel?.text = CommonStrings.Dc.CashbackEarned.PreviousLink1540
            cell.accessoryType = .disclosureIndicator
            
            return cell
        }
    }
    
    func headerCell(at indexPath: IndexPath) -> UITableViewCell {
        let view = self.tableView.dequeueReusableCell(withIdentifier: DCHeaderTableViewWithTextCell.defaultReuseIdentifier, for: indexPath)
        let headerView = view as! DCHeaderTableViewWithTextCell
        
        headerView.setHeaderTitle = viewModel.headerCoinsEarnedValue()
        headerView.setHeaderText = viewModel.headerDateRange()
        headerView.setHeaderSubText = viewModel.headerGoalsRemaining()
        headerView.headerUIImageView.image = UIImage(asset: VIADeviceCashbackAsset.DeviceCashback.CashbackEarned.cashbackEarnedGraphic)
        
        return view
    }
    
    func cashbackCell(at indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: DCEarnedCashbackCell.defaultReuseIdentifier, for: indexPath)
        let cashbackCell = cell as! DCEarnedCashbackCell
        
        cashbackCell.statusLabelText = CommonStrings.CashbacksEarned1568
        cashbackCell.cellImageView.contentMode = .scaleAspectFit

        cashbackCell.cashValueLabelText = viewModel.earnedCashValue(index: indexPath.row)
        cashbackCell.dateLabelText = viewModel.validFromDate(index: indexPath.row)
        cashbackCell.pointsLabelText = viewModel.pointsValue(index: indexPath.row)
        cashbackCell.cellImage = viewModel.cellImage(index: indexPath.row)
        
        return cashbackCell
    }

    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let sections = Sections(rawValue: section)
        if viewModel.goalTrackersCount() != 0 {
            if sections == .header {
                let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier) as!    VIATableViewSectionHeaderFooterView
                    view.labelText = CommonStrings.Dc.CashbackEarned.FooterAmount1539(viewModel.getCashLimit())
                    view.backgroundColor = UIColor.tableViewBackground()
                
                return view
            }
        }
        
        return nil
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let sections = Sections(rawValue: section)
        if viewModel.goalTrackersCount() != 0 {
            if sections == .cashbacks {
                let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier) as! VIATableViewSectionHeaderFooterView
                view.labelText = CommonStrings.Dc.CashbackEarned.FooterItemSubtitle1568
                view.backgroundColor = UIColor.tableViewBackground()
                
                return view
            }
        }
        
        return nil
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch Sections.allValues[indexPath.section] {
        case .previousCashbacks:
            return 50
        default:
            break
        }
        
        return UITableViewAutomaticDimension
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showCashbackDetails" {
            if let cashbackDetailsVC = segue.destination as? DCCashbackDetailsViewController {
                cashbackDetailsVC.headerImage = headerImage
                cashbackDetailsVC.amountAwarderd = amountAwarded
                cashbackDetailsVC.pointsAwarded = pointsAwarded
                cashbackDetailsVC.dateTitle = dateValue
                cashbackDetailsVC.selectedMonth = selectedMonth
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let sections = Sections(rawValue: indexPath.section) {
            switch sections {
            case .cashbacks:
                let cashbackCell = tableView.cellForRow(at: indexPath) as! DCEarnedCashbackCell

                headerImage = cashbackCell.cellImage
                amountAwarded = cashbackCell.cashValueLabelText
                pointsAwarded = cashbackCell.pointsLabelText
                dateValue = cashbackCell.dateLabelText
                selectedMonth = viewModel.selectedYearMonthDateString(index: indexPath.row)

                self.performSegue(withIdentifier: "showCashbackDetails", sender: nil)
            case .previousCashbacks:
                self.performSegue(withIdentifier: "showPreviousCashbacks", sender: nil)
            default:
                break
            }
        }
    }
}
