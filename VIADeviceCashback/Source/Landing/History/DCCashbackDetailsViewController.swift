//
//  DCCashbackDetailsViewController.swift
//  VitalityActive
//
//  Created by Steven Layug on 4/16/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import VIAUIKit
import VitalityKit
import VIAUtilities

class DCCashbackDetailsViewController: VIATableViewController {
    var hasDoneInitialLoad: Bool = false
    
    let viewModel = DCCashbackDetailsViewModel()
    var goalTrackerOut: GDCGoalTrackerOuts?
    var objectivePointsEntries: [GDCObjectivePointsEntries]? = []
    var hasActivities = false
    
    public var headerImage: UIImage? = nil
    public var amountAwarderd: String? = ""
    public var pointsAwarded: String? = ""
    public var dateTitle: String? = ""
    public var selectedMonth: String? = ""
    
    func initErrorHandler(_ error: Error) {
        let backendError = error as? BackendError ?? BackendError.other
        self.handleBackendErrorWithAlert(backendError)
    }
    
    // MARK: View Lifecycles
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = self.dateTitle
        initUI()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if !hasDoneInitialLoad {
            initAPICall()
            hasDoneInitialLoad = true
        }
    }
    
    func initUI() {
        configureAppearance()
        configureTableView()
    }
    
    func configureAppearance() {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.makeNavigationBarTransparent()
        
        self.hideBackButtonTitle()
    }
    
    func configureTableView() {
        self.tableView.register(VIATableViewSectionHeaderFooterView.nib(), forHeaderFooterViewReuseIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier)
        self.tableView.register(DCCashbackDetailsHeaderCell.nib(), forCellReuseIdentifier: DCCashbackDetailsHeaderCell.defaultReuseIdentifier)
        self.tableView.register(DCCashbackActivityCell.nib(), forCellReuseIdentifier: DCCashbackActivityCell.defaultReuseIdentifier)
        self.tableView.register(VIATableViewCell.nib(), forCellReuseIdentifier: VIATableViewCell.defaultReuseIdentifier)
        self.tableView.estimatedRowHeight = 75
        self.tableView.backgroundColor = UIColor.tableViewBackground()
        
        self.tableView.refreshControl = UIRefreshControl()
        self.tableView.refreshControl?.tintColor = UIColor.cellSeperator()
        self.tableView.refreshControl?.addTarget(self, action: #selector(performRefresh), for: .valueChanged)
    }
    
    func performRefresh() {
        initAPICall()
        self.tableView.refreshControl?.endRefreshing()
    }
    
    func initAPICall() {
        self.showHUDOnView(view: self.view)
        viewModel.getGoalProgressAndDetails(completion: { [weak self] (error) in
            DispatchQueue.main.async {
                self?.hideHUDFromView(view: self?.view)
                guard error == nil else {
                    self?.initErrorHandler(error!)
                    return
                }
                
                guard self?.viewModel.getBenefitID() != nil else {
                    self?.tableView.reloadData()
                    return
                }
                
                self?.hasActivities = true
                self?.initRequiredDetails()
                self?.tableView.reloadData()
            }
        })
    }
    
    func initRequiredDetails() {
        if let benefitID = viewModel.getBenefitID() {
            guard let selectedMonth = selectedMonth else { return }
            goalTrackerOut = viewModel.getGoalTrackerOuts(benefitID: benefitID, date: selectedMonth)
            
            guard let goalTrackerOut = goalTrackerOut else { return }
            objectivePointsEntries = viewModel.getObjectivePointsEntries(goalTrackerOut: goalTrackerOut)
        }
    }
    
    enum Sections: Int, EnumCollection, Equatable {
        case header = 0
        case activities = 1
    }
    
    // MARK: TableView Datasource and Delegate
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == Sections.activities.rawValue {
            if let pointsEntries = self.objectivePointsEntries, !pointsEntries.isEmpty {
                return pointsEntries.count
            }
            
            return 1
        } else {
            return 1
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        let section = Sections(rawValue: section)
        if section == .header {
            return 0
        }
        
        return UITableViewAutomaticDimension
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return Sections.allValues.count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if hasActivities {
            return UITableViewAutomaticDimension
        } else {
            let section = Sections(rawValue: indexPath.section)
            if section == .activities {
                return 50
            }
        }
        return UITableViewAutomaticDimension
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch Sections.allValues[indexPath.section] {
        case .header:
            return headerCell(at: indexPath)
        case .activities:
            if hasActivities {
                return activityCell(at: indexPath)
            }
            
            return noActivityCell(at: indexPath)
        }
    }
    
    func headerCell(at indexPath: IndexPath) -> UITableViewCell {
        let view = self.tableView.dequeueReusableCell(withIdentifier: DCCashbackDetailsHeaderCell.defaultReuseIdentifier, for: indexPath)
        let headerView = view as! DCCashbackDetailsHeaderCell
        
        headerView.setHeaderTitle = self.amountAwarderd
        headerView.setHeaderText = CommonStrings.Dc.CashbackEarned.FooterItemSubtitle1568
        headerView.setHeaderSubText = self.pointsAwarded
        headerView.headerUIImageView.image = self.headerImage
        
        return view
    }
    
    func activityCell(at indexPath: IndexPath) -> UITableViewCell {
        if hasActivities {
            let cell = self.tableView.dequeueReusableCell(withIdentifier: DCCashbackActivityCell.defaultReuseIdentifier, for: indexPath)
            let activityCell = cell as! DCCashbackActivityCell
            
            if let pointsEntries = self.objectivePointsEntries, !pointsEntries.isEmpty {
                let rowDetails = pointsEntries[indexPath.row]
                activityCell.pointsLabelText = "\(rowDetails.pointsContributed)"
                
                if let effectiveDate = rowDetails.effectiveDate, let date = DateFormatter.apiManagerFormatter().date(from: effectiveDate) {
                    activityCell.dateLabelText = Localization.dayDateShortMonthFormatter.string(from: date)
                } else {
                    activityCell.dateLabelText = ""
                }
                
                if rowDetails.typeKey == .GymVisit {
                    activityCell.stepsLabelText = rowDetails.typeName
                    activityCell.deviceLabelText = ""
                } else {
                    let pointsEntryMetadatas = rowDetails.pointsEntryMetadatas
                    for pointsEntryMetadata in pointsEntryMetadatas {
                        switch EventMetaDataTypeRef(rawValue: pointsEntryMetadata.typeKey.rawValue) {
                        case .Manufacturer?:
                            activityCell.deviceLabelText = pointsEntryMetadata.value
                            break
                        case .TotalSteps?, .AverageHeartRate?:
                            activityCell.stepsLabelText = rowDetails.typeName
                            break
                        default:
                            break
                        }
                    }
                }
            }
            
            return activityCell
        }
        
        let cell = self.tableView.dequeueReusableCell(withIdentifier: VIATableViewCell.defaultReuseIdentifier, for: indexPath)
        cell.textLabel?.text = CommonStrings.Awc.MonthActivityNoActivities2028 // TODO: valtomol-BYOD Update to DC string.
        return cell
    }
    
    func noActivityCell(at indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: VIATableViewCell.defaultReuseIdentifier, for: indexPath)
        cell.textLabel?.text = CommonStrings.Awc.MonthActivityNoActivities2028 // TODO: valtomol-BYOD Update to DC string.
        return cell
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let sections = Sections(rawValue: section)
        if sections == .header {
            let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier) as! VIATableViewSectionHeaderFooterView
            view.labelText = CommonStrings.Dc.CashbackEarned.DeviceDetailsHeaderFooter1542
            view.backgroundColor = UIColor.tableViewBackground()
            
            return view
        } else {
            let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier) as! VIATableViewSectionHeaderFooterView
            view.labelText = CommonStrings.DcLandingNoActivitySubIntro1535
            view.backgroundColor = UIColor.tableViewBackground()
            
            return view
        }
     }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let sections = Sections(rawValue: section)
        if sections == .activities {
            let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier) as! VIATableViewSectionHeaderFooterView
            view.labelText = CommonStrings.Dc.CashbackEarned.ActivityHeader1543
            view.backgroundColor = UIColor.tableViewBackground()
            
            return view
        }
        
        return nil
    }
}
