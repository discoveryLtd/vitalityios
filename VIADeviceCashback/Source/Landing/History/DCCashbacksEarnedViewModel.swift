//
//  DCCashbacksEarnedViewModel.swift
//  VitalityActive
//
//  Created by Steven Layug on 5/14/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import UIKit

import Foundation
import VitalityKit
import VIAUtilities
import VIAUIKit
import RealmSwift

extension DCCashbacksEarnedViewController {
    enum Sections: Int, EnumCollection, Equatable {
        case header = 0
        case cashbacks = 1
        case previousCashbacks = 2
    }
}

class DCCashbacksEarnedViewModel {
    let realm = DataProvider.newGDCRealm()
    let globalRealm = DataProvider.newRealm()

    enum Objectives: String, EnumCollection, Equatable {
        case pinkIcon = "0"
        case orangeIcon = "1"
        case blueIcon = "2"
        case greenIcon = "3"
    }
    
    private func getCurrentBenefit() -> GDCBGRBenefit? {
        let currentBenefitData = self.realm.allGDCBGRBenefits()
        var currentBenefit: GDCBGRBenefit?
        for data in currentBenefitData {
            currentBenefit = data
        }
        
        return currentBenefit
    }
    
    func headerCoinsEarnedValue () -> String {
        guard let benefit = getCurrentBenefit() else {
            return "0"
        }
        
        return benefit.totalGoalsRewardAmount.isEmpty ? "0" : benefit.totalGoalsRewardAmount
    }
    
    func headerDateRange() -> String {
        return self.dateFormatter(startDate: getCurrentBenefit()?.goalTrackers.first?.validFrom, endDate: getCurrentBenefit()?.goalTrackers.last?.validFrom)
    }
    
    func dateFormatter (startDate: String?, endDate: String?) -> String{
        let dateResponseFormatter = DateFormatter()
        dateResponseFormatter.dateFormat = "yyyy-MM-dd"
        let dateStringFormatter = DateFormatter()
        dateStringFormatter.dateFormat = "MMMM yyyy"
        
        guard let firstStartDate = startDate, let lastStartDate = endDate else {
            return ""
        }
        let fromDate: Date? = dateResponseFormatter.date(from: (firstStartDate))
        let toDate: Date? = dateResponseFormatter.date(from: (lastStartDate))
        
        return CommonStrings.Dc.CashbackEarned.PeriodHeader1537(dateStringFormatter .string(from: fromDate!), dateStringFormatter .string(from: toDate!))
    }
    
    func headerGoalsRemaining() -> String {
        guard let benefit = getCurrentBenefit() else {
            return ""
        }
        
        return CommonStrings.Dc.CashbackEarned.PeriodRemainingDesc1538("\(benefit.goalsRemaining)")
    }
    
    func goalTrackersCount() -> Int {
        return getCurrentBenefit()?.goalTrackers.count ?? 0
    }
    
    func earnedCashValue(index: Int) -> String {
        guard let amount = getCurrentBenefit()?.goalTrackers[index].rewardValues.first?.totalRewardValueAmount else {
            return ""
        }
        
        return amount.isEmpty ? "0" : amount
    }
    
    func pointsValue(index: Int) -> String {
        guard let points = getCurrentBenefit()?.goalTrackers[index].pointsAchievedTowardsGoal else {
            return ""
        }
        
        return CommonStrings.DcCashbackEarnedPointsSubtitle2219("\(points)")
    }
    
    func validFromDate(index: Int) -> String {
        let dateResponseFormatter = DateFormatter()
        dateResponseFormatter.dateFormat = "yyyy-MM-dd"
        let dateStringFormatter = DateFormatter()
        dateStringFormatter.dateFormat = "MMMM yyyy"
        
        guard let validToDate = getCurrentBenefit()?.goalTrackers[index].validFrom else {
             return ""
        }
        
        let stringToDate = dateResponseFormatter.date(from: validToDate)
        
        return dateStringFormatter .string(from: stringToDate!)
    }
    
    func selectedYearMonthDateString(index: Int) -> String {
        let dateResponseFormatter = DateFormatter()
        dateResponseFormatter.dateFormat = "yyyy-MM-dd"
        let dateStringFormatter = DateFormatter()
        dateStringFormatter.dateFormat = "yyyy-MM-dd"
        
        guard let validToDate = getCurrentBenefit()?.goalTrackers[index].validFrom else {
            return ""
        }
        
        let stringToDate = dateResponseFormatter.date(from: validToDate)
        
        return dateStringFormatter .string(from: stringToDate!)
    }
    
    func cellImage(index: Int) -> UIImage {
        let objectiveLevel = getCurrentBenefit()?.goalTrackers[index].highestObjectiveAchievedName
        
        if (objectiveLevel?.range(of: Objectives.pinkIcon.rawValue) != nil) {
            return VIADeviceCashbackAsset.DeviceCashback.CashbackEarned.pinkCoinHome.image
        } else if (objectiveLevel?.range(of: Objectives.orangeIcon.rawValue) != nil) {
            return VIADeviceCashbackAsset.DeviceCashback.CashbackEarned.orangeCoinHome.image
        } else if (objectiveLevel?.range(of: Objectives.blueIcon.rawValue) != nil) {
            return VIADeviceCashbackAsset.DeviceCashback.CashbackEarned.blueCoinHome.image
        } else if (objectiveLevel?.range(of: Objectives.greenIcon.rawValue) != nil) {
            return VIADeviceCashbackAsset.DeviceCashback.CashbackEarned.greenCoinHome.image
        } else {
            return VIADeviceCashbackAsset.DeviceCashback.CashbackEarned.pinkCoinHome.image
        }
    }
    
    func getCashLimit() -> String {
        if let amounts = getCurrentBenefit()?.purchase?.amounts {
            for amount in amounts {
                if amount.typeKey == 4 || amount.typeCode == "TotalRewardQualify" {
                    return amount.value
                }
            }
        }
        
        return ""
    }
    
    // API Call
    func loadData(forceUpdate: Bool = false, completion: @escaping (_ error: Error?) -> Void)  {
        let partyId = globalRealm.getPartyId()
        let productKey = 34
        let tenantId = globalRealm.getTenantId()
        
        guard let benefitId = realm.allGDCBenefit().first?.benefitId, realm.allGDCBenefit().first?.benefitId != 0 else {
            completion(BackendError.other)
            return
        }
        
        // TO DO: Left for debugging purposes
        // let jsonParameters = GetBenefitGoalsAndRewardsParameters(benefitId: 58156781, partyId: 22104344, productKey: 34)
        let jsonParameters = GetBenefitGoalsAndRewardsParameters(benefitId: benefitId, partyId: partyId, productKey: productKey)
        Wire.Events.getBenefitGoalsAndRewardsFeature(tenantId: tenantId, request: jsonParameters) { (error) in
            completion(error)
        }
    }
}

