//
//  DCLandingViewModel.swift
//  VitalityActive
//
//  Created by Von Kervin R. Tuico on 02/05/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import VitalityKit
import VIAUtilities
import VIAUIKit
import RealmSwift

protocol DCCardStatusChecker {
    func setMainHeaderView(cardStatus: CardStatusTypeRef, tableView: UITableView) -> UIView?
}

extension DCLandingViewController {
    enum Sections: Int, EnumCollection, Equatable {
        case header = 0
        case activity = 1
        case menu = 2
    }
    
    enum StatusHeader: Int, EnumCollection, Equatable {
        case month = 0
        case spacer = 1
        case tier0 = 2
        case tier1 = 3
        case tier2 = 4
        case tier3 = 5
        
        // This corresponds to the level values indicated on the BE response.
        func tierLevel() -> Int {
            switch self {
            case .tier0:
                return 1
            case .tier1:
                return 2
            case .tier2:
                return 3
            case .tier3:
                return 4
            default:
                return 0
            }
        }
    }
    
    enum Menu: Int {
        case cashbacksEarned = 0
        case devices = 1
        case howToTrackCashbacks = 2
        case learnMore = 3
        case help = 4
    }
    
    var menuRows: NSMutableIndexSet {
        let indexSet = NSMutableIndexSet()
        
        if let showCashbackHistory = self.shouldShowCashbackHistoryRow, showCashbackHistory {
            indexSet.add(Menu.cashbacksEarned.rawValue)
        }
        if let showDevices = self.shouldShowDevicesRow, showDevices {
            indexSet.add(Menu.devices.rawValue)
        }
        indexSet.add(Menu.howToTrackCashbacks.rawValue)
        indexSet.add(Menu.learnMore.rawValue)
        indexSet.add(Menu.help.rawValue)
        
        return indexSet
    }
}

class DCLandingViewModel: DCCardStatusChecker {
    var userHasPoints: Bool? = false
    lazy var cardStatus = DataProvider.newRealm().getCardStatus(ofType: CardTypeRef.DeviceCashback)
    
    var setHeaderButtonInfo: ((_ token: String) -> Void)?
    var headerIdentifier: String?
    var setSubButtonInfo: ((_ token: String) -> Void)?
    var subIdentifier: String?
    let currentDate = Date()
    let activeStatus = "ACTIVE"
    let pendingStatus = "PENDING"
    
    let coreRealm = DataProvider.newRealm()
    let realm = DataProvider.newGDCRealm()
    
    var partnerRedirectUrl: URL?
    var partnerCommonAuthId: String?
    
    func getDeviceBenefit(completion: @escaping (_ error: Error?) -> Void) {
        let globalRealm = DataProvider.newRealm()
        let tenantID = globalRealm.getTenantId()
        let partyID = globalRealm.getPartyId()
        
        let productFeatureKey = getCardMetaData(realm: globalRealm) ?? "222"
        let getDeviceBenefit = GetDeviceBenefit(productFeatureKey: Int(productFeatureKey)!, swap: false, partyId: partyID)
        let getDeviceBenefitRequest = GetDeviceBenefitParameters(getDeviceBenefit)
        Wire.Events.getDeviceBenefit(tenantId: tenantID, request: getDeviceBenefitRequest) { (error) in
            completion(error)
        }
    }
    
    func getCardMetaData(realm: Realm) -> String? {
        // Get device cashback home card details.
        let deviceCashbackCard = realm.homeCard(by: CardTypeRef.DeviceCashback)
        
        // Get 'productFeatureKey' value.
        if let cardMetaDatas = deviceCashbackCard?.cardMetadatas {
            for cardMetaData in cardMetaDatas {
                if cardMetaData.type.rawValue == CardMetadataTypeRef.CardFeatureDeviceCashback.rawValue {
                    return cardMetaData.value
                }
            }
        }
        
        return nil
    }
    
    func showPointsHeader() -> Bool {
        if let statusTypeCode = self.realm.allGDCState().first?.statusTypeCode, statusTypeCode == activeStatus {
            if let effectiveFrom = self.realm.allGDCGoalTracker().first?.effectiveFrom {
                if let effectiveFromValue = DateFormatter.yearMonthDayFormatter().date(from: effectiveFrom),
                    effectiveFromValue <= currentDate {
                    userHasPoints = true
                    return true
                }
            }
        }
        
        return false
    }
    
    func setMainHeaderView(cardStatus: CardStatusTypeRef, tableView: UITableView) -> UIView? {
        let viewDetails = DeviceBenefit.self
        
        switch cardStatus {
        case .NotStarted:
            return mainHeaderView(tableView: tableView, details: viewDetails.activateYourDevice)!
        default:
            return mainHeaderView(tableView: tableView, details: viewDetails.activated)!
        }
    }
    
    func setFooterView(cardStatus: CardStatusTypeRef, tableView: UITableView) -> UIView? {
        let viewDetails = DeviceBenefit.self
        
        switch cardStatus {
        case .NotStarted:
            return footerView(tableView: tableView, details: viewDetails.activateYourDevice)!
        default:
            return footerView(tableView: tableView, details: viewDetails.activated)!
        }
    }
    
    func mainHeaderView(tableView: UITableView, details: DeviceBenefit) -> UIView? {
        let view = tableView.dequeueReusableCell(withIdentifier: DCHeaderTableViewCell.defaultReuseIdentifier)
        let mainHeaderView = view as! DCHeaderTableViewCell
        
        mainHeaderView.setHeaderTitle = details.headerTitle()
        mainHeaderView.setHeaderDescription = details.headerDescription()
        
        if let title = details.headerButtonTitle(), !title.isEmpty {
            mainHeaderView.headerButton.isHidden = false
            mainHeaderView.setHeaderButtonTitle = title
        } else {
            mainHeaderView.headerButton.isHidden = true
        }
        
        if let subTitle = details.subButtonTitle(), !subTitle.isEmpty {
            mainHeaderView.subButton.isHidden = false
            mainHeaderView.setSubButtonTitle = subTitle
            mainHeaderView.subButton.hidesBorder = true
        } else {
            mainHeaderView.subButton.isHidden = true
        }
        
        mainHeaderView.headerUIImageView.image = details.headerBanner()
        
        headerIdentifier = details.headerIdentifier()
        subIdentifier = details.subIdentifier()
        mainHeaderView.headerButton.addTarget(self, action: #selector(headerButtonAction), for: .touchUpInside)
        mainHeaderView.subButton.addTarget(self, action: #selector(subButtonAction), for: .touchUpInside)
        
        return view
    }
    
    func footerView(tableView: UITableView, details: DeviceBenefit) -> UIView? {
        let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier)
        let footerView = view as! VIATableViewSectionHeaderFooterView
        
        footerView.labelText = details.sectionFooterText()
        
        return footerView
    }
    
    @objc func headerButtonAction() {
        if let token = headerIdentifier, let setHeaderButtonInfo = setHeaderButtonInfo {
            setHeaderButtonInfo(token)
        }
    }
    
    @objc func subButtonAction() {
        if let token = subIdentifier, let setSubButtonInfo = setSubButtonInfo {
            setSubButtonInfo(token)
        }
    }
    
    func partnerRedirect(completion: @escaping (Error?) -> Void) {
        let coreRealm = DataProvider.newRealm()
        let tenantId = coreRealm.getTenantId()
        
        let requestParams = configurePartnerRedirectRequest(tenantId: tenantId)
        guard let completeRequestParams = requestParams else { return }
        
        Wire.Party.partnerRedirect(tenantId: tenantId, request: completeRequestParams) { [weak self] redirectLocation, commonAuthId, error in
            DispatchQueue.main.async {
                if let redirect = redirectLocation, let commonAuthId = commonAuthId {
                    self?.partnerRedirectUrl = URL(string: redirect.replacingOccurrences(of: " ", with: "%20"))
                    self?.partnerCommonAuthId = commonAuthId
                    completion(nil)
                } else if let serviceError = error {
                    completion(serviceError)
                }
            }
        }
    }
    
    func configurePartnerRedirectRequest(tenantId: Int) -> PartnerRedirectParameters? {
        // TODO: valtomol-BYOD Hardcoded? hmmm... To be confirmed
        let realm = DataProvider.newRealm()
        let party = realm.currentVitalityParty()
        
        var nationalIdKey: String {
            guard let references = party?.references else { return "" }
            for reference in references {
                if reference.type == String(PartyReferenceTypeRef.NationalID.rawValue) {
                    return reference.type
                }
            }
            
            return ""
        }
        
        var nationalIdValue: String {
            guard let references = party?.references else { return "" }
            for reference in references {
                if reference.type == String(PartyReferenceTypeRef.NationalID.rawValue) {
                    return reference.value
                }
            }
            
            return ""
        }
        
        var partnerRedirectUrl: String {
            guard let urlString = Bundle.main.object(forInfoDictionaryKey: "VAPartnerURL") as? String else {
                return ""
            }
            
            return urlString
        }
        
        let partyReferenceKey: String = nationalIdKey
        let partyReferenceValue: String = nationalIdValue
        
        let requestParams = PartnerRedirectParameters(partnerRedirectUrl: partnerRedirectUrl,
                                                      partyReferenceKey: partyReferenceKey,
                                                      partyReferenceValue: partyReferenceValue)
        
        return requestParams
    }
}

extension DCLandingViewModel {
    enum DeviceBenefit {
        case activateYourDevice
        case activated
        
        func deviceModel() -> String {
            let allPurchaseDetailSummaryItems = DataProvider.newGDCRealm().allPurchaseDetailSummaryItems()
            var model = ""
            for summaryItem in allPurchaseDetailSummaryItems {
                model = summaryItem.model
            }
            return model
        }
        
        func headerTitle() -> String? {
            switch self {
            case .activateYourDevice:
                return CommonStrings.Dc.Landing.HeaderTitle1464
            case .activated:
                guard let date = monthlyTarget() else { return "" }
                return CommonStrings.Dc.Landing.MonthlyTargetTitle1525(" \(date)")
            }
        }
        
        func headerDescription() -> String? {
            switch self {
            case .activateYourDevice:
                return CommonStrings.Dc.Landing.HeaderMessage1465
            case .activated:
                return CommonStrings.Dc.Landing.MonthlyTargetMessage1526
            }
        }
        
        func headerButtonTitle() -> String? {
            switch self {
            case .activateYourDevice:
                return CommonStrings.Dc.Landing.ActionMainButton2488
            case .activated:
                return CommonStrings.Dc.Landing.ActionMainButton2490
            }
        }
        
        func subButtonTitle() -> String? {
            switch self {
            case .activateYourDevice:
                return CommonStrings.LearnMoreButtonTitle104
            case .activated:
                return CommonStrings.Dc.Landing.ActionButton2491
            }
        }
        
        
        func headerIdentifier() -> String? {
            switch self {
            case .activateYourDevice:
                return "showDCWKWebView"
            case .activated:
                return "showWellnessDevices"
            }
        }
        
        func subIdentifier() -> String? {
            switch self {
            case .activateYourDevice:
                return "showLearnMore"
            case .activated:
                return "showHowToTrackCashbacks"
            }
        }
        
        func headerBanner() -> UIImage {
            switch self {
            case .activateYourDevice:
                return VIADeviceCashbackAsset.DeviceCashback.Landing.cashbackCoinGraphic.image
            case .activated:
                return VIADeviceCashbackAsset.DeviceCashback.Landing.calendar.image
            }
        }
        
        func sectionFooterText() -> String? {
            switch self {
            case .activateYourDevice:
                return CommonStrings.Dc.Landing.FooterMessage2493
            case .activated:
                return CommonStrings.Dc.Landing.FooterMessage2494 + CommonStrings.Dc.Landing.FooterMessageCont2495
            }
        }
        
        func monthlyTarget() -> String? {
            let realm = DataProvider.newGDCRealm()
            
            guard let date = realm.allGDCGoalTracker().first?.effectiveFrom else {
                return ""
            }
            
            let dateFromResponse = Localization.yearMonthDayFormatter.date(from: date)
            let monthlyTarget = Localization.dayDateMonthYearFormatter.string(from: dateFromResponse!)
            return monthlyTarget
        }
    }
}

extension DCLandingViewModel {
    func cashbackEarnedRowTitle() -> String? {
        return CommonStrings.Dc.Landing.ActionMenuTitle1469
    }
    func devicesRowTitle() -> String? {
        return CommonStrings.Dc.Landing.DeviceMenuItem1520
    }
    func howToTrackCashbacksRowTitle() -> String? {
        return CommonStrings.Dc.Landing.ActionMenuTitle1470
    }
    func learnMoreRowTitle() -> String? {
        return CommonStrings.LearnMoreButtonTitle104
    }
    func helpRowTitle() -> String? {
        return CommonStrings.HelpButtonTitle141
    }
    
    func devicesRowImage() -> UIImage? {
        return UIImage(asset: VIADeviceCashbackAsset.DeviceCashback.Landing.devicesSmall)
    }
    func cashbackEarnedRowImage() -> UIImage? {
        return UIImage(asset: VIADeviceCashbackAsset.DeviceCashback.Landing.cashbacksEarned)
    }
    func howToTrackCashbacksRowImage() -> UIImage? {
        return UIImage(asset: VIADeviceCashbackAsset.DeviceCashback.Landing.howToTrack)
    }
    func learnMoreRowImage() -> UIImage? {
        return UIImage(asset: VIADeviceCashbackAsset.DeviceCashback.Landing.learnMoreSmall)
    }
    func helpRowImage() -> UIImage? {
        return UIImage(asset: VIADeviceCashbackAsset.DeviceCashback.Landing.questionMarkSmall)
    }
    
    func cashbackEarnedRowSegueIdentifier() -> String {
        return "showCashbacksEarned"
    }
    func devicesRowSegueIdentifier() -> String {
        return "showDevices"
    }
    func howToTrackCashbacksRowRowSegueIdentifier() -> String {
        return "showHowToTrackCashbacks"
    }
    func learnMoreRowSegueIdentifier() -> String {
        return "showLearnMore"
    }
    func helpRowSegueIdentifier() -> String {
        return "showHelp"
    }
    
    func activitySectionHeaderTitle() -> String? {
        return CommonStrings.DcLandingActivityTitle1533
    }
    
    func emptyActivityTitle() -> String? {
        return CommonStrings.DcLandingNoActivityIntro1534
    }
    
    func emptyActivitySubtitle() -> String? {
        return CommonStrings.DcLandingNoActivitySubIntro1535
    }
    
    func monthCountTitle(currentCount: String, totalCount: String) -> String? {
        return CommonStrings.DcLandingProgressMonth1531(currentCount, totalCount)
    }
}

extension DCLandingViewModel {
    private func getCurrentTierLevel() -> GDCLevels? {
        let levelData = realm.allGDCLevels().filter { $0.isHighestLevelAchieved == true }
        var level: GDCLevels?
        for data in levelData {
            level = data
        }
        
        return level
    }
    
    private func getDeviceGoal() -> GDCGoal? {
        let goalData = self.realm.allGDCGoal()
        var goal: GDCGoal?
        for data in goalData {
            goal = data
        }
        
        return goal
    }
    
    private func getDeviceBenefit() -> GDCBenefit? {
        let benefitData = self.realm.allGDCBenefit()
        var benefit: GDCBenefit?
        for data in benefitData {
            benefit = data
        }
        
        return benefit
    }
    
    private func getLevelData(at level: Int) -> GDCLevels? {
        guard let levelsData = getDeviceGoal()?.levels else {
            return nil
        }
        
        var levelData: GDCLevels?
        for data in levelsData {
            if data.level == level {
                levelData = data
            }
        }
        
        return levelData
    }
    
    func getActivities() -> List<GDCPointsEntry>? {
        guard let pointsEntries = getDeviceGoal()?.pointsEntry else {
            return nil
        }
        
        return pointsEntries
    }
    
    func getActivityCount() -> Int {
        var count: Int
        if let activities = getActivities(), activities.count > 0 {
            // Activity rows.
            count = activities.count
        } else {
            // 'No Activity' row.
            count = 1
        }
        
        return count
    }
    
    func hasActivityItems() -> Bool {
        var hasActivity: Bool
        if let activities = getActivities(), activities.count > 0 {
            hasActivity = true
        } else {
            hasActivity = false
        }
        
        return hasActivity
    }
    
    func getActivity(at index: Int) -> GDCPointsEntry? {
        guard let pointsEntries = getDeviceGoal()?.pointsEntry else {
            return nil
        }
        
        let entry: GDCPointsEntry
        entry = pointsEntries[index]
        
        return entry
    }
    
    func getActivityPoints(at index: Int) -> String? {
        guard let pointsEntries = getDeviceGoal()?.pointsEntry else {
            return nil
        }
        
        let activityPoints = pointsEntries[index].pointsContributed
        
        return String(activityPoints)
    }
    
    func getActivityDate(at index: Int) -> String? {
        guard let pointsEntries = getDeviceGoal()?.pointsEntry else {
            return nil
        }
        
        guard let effectiveDate = pointsEntries[index].effectiveDate else { return nil }
        guard let date = Localization.yearMonthDayFormatter.date(from: effectiveDate) else { return nil }
        let dateString = Localization.dayDateShortMonthFormatter.string(from: date)
        
        return dateString
    }
    
    func getActivityAction(at index: Int) -> String? {
        guard let pointsEntries = getDeviceGoal()?.pointsEntry else {
            return nil
        }
        
        for metaData in pointsEntries[index].pointsEntryMetadatas {
            if metaData.typeKey == EventMetaDataTypeRef.TotalSteps.rawValue {
                return metaData.value
            }
        }
        
        return nil
    }
    
    func getActivityType(at index: Int) -> String? {
        guard let pointsEntries = getDeviceGoal()?.pointsEntry else {
            return nil
        }
        
        return pointsEntries[index].typeName
    }
    
    func getActivityDevice(at index: Int) -> String? {
        guard let pointsEntries = getDeviceGoal()?.pointsEntry else {
            return nil
        }
        
        for metaData in pointsEntries[index].pointsEntryMetadatas {
            if metaData.typeKey == EventMetaDataTypeRef.Manufacturer.rawValue {
                return metaData.value
            }
        }
        
        return nil
    }
    
    func getCurrentBenefitPeriod() -> String {
        guard let benefitPeriod = getDeviceBenefit()?.currentBenefitPeriod else {
            return ""
        }
        
        return String(benefitPeriod)
    }
    
    func getBenefitTermMonths() -> String {
        guard let benefitProductAttributes = getDeviceBenefit()?.productAttributes else {
            return ""
        }
        
        for attribute in benefitProductAttributes {
            // TODO: valtomol Reference these items to RefData enum.
            if attribute.typeKey == 3 || attribute.typeCode == "BenefitTermMonths" {
                return attribute.value ?? ""
            }
        }
        
        return ""
    }
    
    func getEffectiveGoalMonth() -> String? {
        guard let goalTracker = getDeviceGoal()?.goalTracker else {
            return nil
        }
        
        guard let effectiveFromDate = goalTracker.effectiveFrom else { return nil }
        guard let date = Localization.yearMonthDayFormatter.date(from: effectiveFromDate) else { return nil }
        let dateString = Localization.fullMonthAndYearFormatter.string(from: date)
        
        return dateString
    }
    
    func isTierRowActivated(at tierLevel: Int) -> Bool {
        guard let currentTier = getCurrentTierLevel()?.level else {
            return false
        }
        
        if tierLevel == currentTier {
            return true
        }
        
        return false
    }
    
    private func isTierRowCompleted(at tierLevel: Int) -> Bool {
        guard let isAcheved = getLevelData(at: tierLevel)?.isAchieved else {
            return false
        }
        
        if isTierRowActivated(at: tierLevel) == false && isAcheved == true {
            return true
        }
        
        return false
    }
    
    func getCoinImage(at level: Int) -> UIImage {
        switch level {
        case 1:
            return VIADeviceCashbackAsset.HowToTrackCashbacks.pinkCoin.image
        case 2:
            return VIADeviceCashbackAsset.HowToTrackCashbacks.orangeCoin.image
        case 3:
            return VIADeviceCashbackAsset.HowToTrackCashbacks.blueCoin.image
        case 4:
            return VIADeviceCashbackAsset.HowToTrackCashbacks.greenCoin.image
        default:
            return UIImage()
        }
    }
    
    func getAccumulatedAmount(at tierLevel: Int) -> String? {
        guard let accumulatedAmount = getLevelData(at: tierLevel)?.reward?.accumulatedAmount else {
            return ""
        }
        
        return accumulatedAmount
    }
    
    private func getMinimumPoints(at tierLevel: Int) -> Int? {
        guard let minimumPoints = getLevelData(at: tierLevel)?.minimumPoints else {
            return 0
        }
        
        return minimumPoints
    }
    
    private func getPointsAchieved(at tierLevel: Int) -> Int? {
        guard let pointsAchieved = getLevelData(at: tierLevel)?.pointsAchieved else {
            return 0
        }
        
        return pointsAchieved
    }
    
    private func getMaximumPoints(at tierLevel: Int) -> Int? {
        guard let maximumPoints = getLevelData(at: tierLevel)?.maximumPoints else {
            return 0
        }
        
        return maximumPoints
    }
    
    private func getPoints(at tierLevel: Int) -> Int? {
        if isTierRowActivated(at: tierLevel) {
            guard let current = getPointsAchieved(at: tierLevel) else { return 0 }
            return current
        } else if isTierRowCompleted(at: tierLevel) {
            guard let max = getMaximumPoints(at: tierLevel) else { return 0 }
            return max
        } else {
            guard let min = getMinimumPoints(at: tierLevel) else {return 0 }
            return min
        }
    }
    
    func getReachPointsText(at tierLevel: Int) -> String {
        guard let points = getPoints(at: tierLevel) else {
            return ""
        }
        
        let pointsString = String(points)
        
        switch tierLevel {
        case 1:
            return buildReachPointsText(pointsString, at: tierLevel)
        case 2:
            return buildReachPointsText(pointsString, at: tierLevel)
        case 3:
            return buildReachPointsText(pointsString, at: tierLevel)
        case 4:
            return buildReachPointsText(pointsString + "+", at: tierLevel)
        default:
            return ""
        }
    }
    
    private func buildReachPointsText(_ text: String, at tierLevel: Int) -> String {
        if isTierRowActivated(at: tierLevel) {
            return CommonStrings.Dc.TrackCashbacks.PointsSubtitle1482(text)
        } else if isTierRowCompleted(at: tierLevel) {
            return CommonStrings.DcLandingProgressPointsReached2225(text)
        } else {
            return CommonStrings.Dc.TrackCashbacks.PointsSubtitle1483(text)
        }
    }
}

