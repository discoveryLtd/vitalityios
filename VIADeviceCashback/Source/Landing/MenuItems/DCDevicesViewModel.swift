//
//  DCDevicesViewModel.swift
//  VIADeviceCashback
//
//  Created by Val Tomol on 07/03/2019.
//  Copyright © 2019 Glucode. All rights reserved.
//

import Foundation
import VitalityKit
import VIAUtilities
import VIAUIKit
import RealmSwift

class DCDevicesViewModel {
    let realm = DataProvider.newGDCRealm()
    
    private func getDevicePurchase() -> GDCPurchase? {
        let purchaseData = self.realm.allGDCPurchase()
        var purchase: GDCPurchase?
        for data in purchaseData {
            purchase = data
        }
        
        return purchase
    }
    
    func getDeviceModel() -> String? {
        guard let purchaseItem = getDevicePurchase()?.purchaseItem else {
            return ""
        }
        
        return purchaseItem.itemDescription
    }
    
    func getDeviceInvoiceNumber() -> String? {
        guard let purchaseItem = getDevicePurchase()?.purchaseItem else {
            return ""
        }
        
        let purchaseReferences = purchaseItem.purchaseReference
        for reference in purchaseReferences {
            if reference.typeKey == 3 || reference.typeCode == "InvoiceRef" {
                return reference.value
            }
        }
        
        return ""
    }
    
    func getDeviceSerialNumber() -> String? {
        guard let purchaseItem = getDevicePurchase()?.purchaseItem else {
            return ""
        }
        
        let purchaseReferences = purchaseItem.purchaseReference
        for reference in purchaseReferences {
            // TODO: valtomol Reference these items to RefData enum.
            if reference.typeKey == 4 || reference.typeCode == "DeviceSerialNumber" {
                return reference.value
            }
        }
        
        return ""
    }
    
    func getDevicePurchaseAmount() -> String? {
        guard let purchaseItem = getDevicePurchase()?.purchaseItem else {
            return ""
        }
        
        let purchaseAmounts = purchaseItem.purchaseAmount
        for amount in purchaseAmounts {
            // TODO: valtomol Reference these items to RefData enum.
            if amount.typeKey == 8 || amount.typeCode == "RetailAmount" {
                return amount.value
            }
        }
        
        return ""
    }
    
    func getDevicePurchaseDate() -> String? {
        guard let purchaseDate = getDevicePurchase()?.purchaseDate else { return "" }
        guard let date = Localization.yearMonthDayFormatter.date(from: purchaseDate) else { return "" }
        
        let dateString = Localization.dayDateShortMonthyearFormatter.string(from: date)
        
        return dateString
    }
}
