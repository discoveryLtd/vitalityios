//
//  DCDevicesViewController.swift
//  VitalityActive
//
//  Created by Von Kervin R. Tuico on 19/04/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import VIAUIKit
import VIAUtilities
import VitalityKit
import VIACommon

class DCDevicesViewController: VIATableViewController {
    static let viewModel = DCDevicesViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = CommonStrings.Dc.Landing.DeviceMenuItem1520
        initUI()
    }
    
    func initUI() {
        initNavigationBar()
        initTableView()
    }
    
    func initNavigationBar() {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.makeNavigationBarTransparent()
        
        self.hideBackButtonTitle()
    }
    
    func initTableView() {
        self.tableView.register(VIATextFieldTableViewCell.nib(), forCellReuseIdentifier: VIATextFieldTableViewCell.defaultReuseIdentifier)
        self.tableView.register(DCLabelButtonTableViewCell.nib(), forCellReuseIdentifier: DCLabelButtonTableViewCell.defaultReuseIdentifier)
        self.tableView.register(VIATableViewSectionHeaderFooterView.nib(), forHeaderFooterViewReuseIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier)
        self.tableView.register(VIATableViewCell.nib(), forCellReuseIdentifier: VIATableViewCell.defaultReuseIdentifier)
        self.tableView.estimatedRowHeight = 96
        self.tableView.backgroundColor = UIColor.tableViewBackground()
    }
    
    enum Sections: Int, EnumCollection, Equatable {
        case currentDevice = 0
        case newDevice = 1
        
        func title() -> String? {
            switch self {
            case .currentDevice:
                return CommonStrings.Dc.Device.SectionTitle1545
            case .newDevice:
                return CommonStrings.Dc.Device.SectionTitle1546
            }
        }
    }
    
    enum CurrentDeviceFields: Int, EnumCollection, Equatable {
        case model = 0
        case invoiceNumber = 1
        case deviceSerialNumber = 2
        case purchaseAmount = 3
        case purchaseDate = 4
        
        func title() -> String? {
            switch self {
            case .model:
                return CommonStrings.Dc.LandingActivationPurchaseDetailModelField1490
            case .invoiceNumber:
                return CommonStrings.Dc.LandingActivationPurchaseDetailInvoiceNumberField1491
            case .deviceSerialNumber:
                return CommonStrings.Dc.LandingActivationPurchaseDetailDevSerialNumberField1493
            case .purchaseAmount:
                return CommonStrings.Dc.LandingActivationPurchaseDetailPurchaseAmtField1495
            case .purchaseDate:
                return CommonStrings.Dc.LandingActivationPurchaseDetailPurchaseDteField1497
            }
        }
    
        func value() -> String? {
            switch self {
            case .model:
                return viewModel.getDeviceModel()
            case .invoiceNumber:
                return viewModel.getDeviceInvoiceNumber()
            case .deviceSerialNumber:
                return viewModel.getDeviceSerialNumber()
            case .purchaseAmount:
                return viewModel.getDevicePurchaseAmount()
            case .purchaseDate:
                return viewModel.getDevicePurchaseDate()
            }
        }
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
}

extension DCDevicesViewController {
    override func numberOfSections(in tableView: UITableView) -> Int {
        // Removed new device section for now.
        //return Sections.allValues.count
        
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch Sections.allValues[section] {
        case .currentDevice:
            return CurrentDeviceFields.allValues.count
        case .newDevice:
            return 1
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch Sections.allValues[indexPath.section] {
        case .currentDevice:
            return currentDeviceCell(at: indexPath)
        case .newDevice:
            return getNewDeviceCell(at: indexPath)
        }
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let sections = Sections(rawValue: section)
        
        let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier) as! VIATableViewSectionHeaderFooterView
        view.set(font: UIFont.footnoteFont())
        view.set(textColor: UIColor.tableViewSectionHeaderFooter())
        
        if sections == .currentDevice {
            view.labelText = sections?.title()
            return view
        } else if sections == .newDevice {
            view.labelText = sections?.title()
            return view
        }
        
        return nil
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let sections = Sections(rawValue: section)
        
        let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier) as! VIATableViewSectionHeaderFooterView
        
        if sections == .newDevice {
            view.labelText = CommonStrings.Dc.Device.FooterMessage1548
            view.set(textColor: .mediumGrey())
            return view
        } else {
            let view = UIView()
            view.backgroundColor = self.tableView.backgroundColor
            return view
        }
    }

    func currentDeviceCell(at indexPath: IndexPath) -> UITableViewCell {
        let labels = CurrentDeviceFields.allValues[indexPath.row]
        
        let cell = self.tableView.dequeueReusableCell(withIdentifier: VIATextFieldTableViewCell.defaultReuseIdentifier, for: indexPath)
        let inputFieldCell = cell as! VIATextFieldTableViewCell
        inputFieldCell.setHeadingLabelText(text: labels.title())
        inputFieldCell.setTextFieldText(text: labels.value())
        inputFieldCell.hideCellImage()
        inputFieldCell.textField.isUserInteractionEnabled = false
        inputFieldCell.selectionStyle = .none
        
        return cell
    }
    
    func getNewDeviceCell(at indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: DCLabelButtonTableViewCell.defaultReuseIdentifier, for: indexPath)
        let newDeviceCell = cell as! DCLabelButtonTableViewCell
        
        newDeviceCell.setTitle = CommonStrings.Dc.Device.ChangeCashbackDevice1547
        newDeviceCell.setButtonTitle = CommonStrings.Dc.Common.ActivateTitle1466
        newDeviceCell.button.hidesBorder = true
        newDeviceCell.selectionStyle = .none
        return cell
    }
}
