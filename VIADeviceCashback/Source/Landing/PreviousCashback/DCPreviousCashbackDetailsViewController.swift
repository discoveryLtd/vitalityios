//
//  DCPreviousCashbackDetailsViewController.swift
//  VitalityActive
//
//  Created by Steven Layug on 4/17/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import UIKit

import Foundation
import VitalityKit
import VIAUtilities
import VIAUIKit
import RealmSwift

class DCPreviousCashbackDetailsViewController: VIATableViewController {
    let previousCashbackDetailsViewModel = DCPreviousCashbackDetailsViewModel()
    // NOTE: Set default value to zero instead of setting an optional value to avoid crashes if value is nil
    var selectedBenefitId: Int = 0
    
    enum Sections: Int, EnumCollection, Equatable {
        case header = 0
        case cashbacks = 1
        case previousCashbacks = 2
    }
    
    // MARK: View Lifecycles
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = CommonStrings.Dc.Landing.ActionMenuTitle1469
        initUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        guard self.selectedBenefitId != 0 else {
            return
        }
        
        self.previousCashbackDetailsViewModel.setSelectedBenefitId(benefitId: self.selectedBenefitId)
    }
    
    func initUI() {
        configureAppearance()
        configureTableView()
    }
    
    func configureAppearance() {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.makeNavigationBarTransparent()
        
        self.hideBackButtonTitle()
    }
    
    func configureTableView() {
        self.tableView.register(VIATableViewSectionHeaderFooterView.nib(), forHeaderFooterViewReuseIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier)
        self.tableView.register(DCHeaderTableViewWithTextCell.nib(), forCellReuseIdentifier: DCHeaderTableViewWithTextCell.defaultReuseIdentifier)
        self.tableView.register(DCEarnedCashbackCell.nib(), forCellReuseIdentifier: DCEarnedCashbackCell.defaultReuseIdentifier)
        self.tableView.register(VIATableViewCell.nib(), forCellReuseIdentifier: VIATableViewCell.defaultReuseIdentifier)
        self.tableView.estimatedRowHeight = 75
        self.tableView.backgroundColor = UIColor.tableViewBackground()
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.makeNavigationBarTransparent()
        self.navigationController?.hideBackButtonTitle()
        tableView.contentInset = UIEdgeInsets(top: -1, left: 0, bottom: 0, right: 0)
    }
    
    // MARK: TableView Datasource and Delegate
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        let section = Sections(rawValue: section)
        if section == .header {
            return 0
        }
        
        return UITableViewAutomaticDimension
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (section == Sections.cashbacks.rawValue){
            return previousCashbackDetailsViewModel.goalTrackersCount()
        } else if previousCashbackDetailsViewModel.goalTrackersCount() == 0 {
            return 0
        } else {
            return 1
        }
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return Sections.allValues.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: VIATableViewCell.defaultReuseIdentifier, for: indexPath)
        
        switch Sections.allValues[indexPath.section] {
        case .header:
            return headerCell(at: indexPath)
        case .cashbacks:
            return cashbackCell(at: indexPath)
        case .previousCashbacks:
            cell.textLabel?.text = CommonStrings.Dc.CashbackEarned.PreviousLink1540
            cell.accessoryType = .disclosureIndicator
            
            return cell
        }
    }
    
    func headerCell(at indexPath: IndexPath) -> UITableViewCell {
        let view = self.tableView.dequeueReusableCell(withIdentifier: DCHeaderTableViewWithTextCell.defaultReuseIdentifier, for: indexPath)
        let headerView = view as! DCHeaderTableViewWithTextCell
        
        headerView.setHeaderTitle = previousCashbackDetailsViewModel.headerCoinsEarnedValue()
        headerView.setHeaderText = previousCashbackDetailsViewModel.headerDateRange()
        headerView.setHeaderSubText = previousCashbackDetailsViewModel.headerGoalsRemaining()
        
        headerView.headerUIImageView.image = UIImage(asset: VIADeviceCashbackAsset.DeviceCashback.CashbackEarned.cashbackEarnedGraphic)
        
        return view
    }
    
    func cashbackCell(at indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: DCEarnedCashbackCell.defaultReuseIdentifier, for: indexPath)
        let cashbackCell = cell as! DCEarnedCashbackCell
        
        cashbackCell.statusLabelText = CommonStrings.Dc.CashbackEarned.FooterItemSubtitle1568
        cashbackCell.cellImageView.contentMode = .scaleAspectFit
        cashbackCell.cashValueLabelText = previousCashbackDetailsViewModel.earnedCashValue(index: indexPath.row)
        cashbackCell.dateLabelText = previousCashbackDetailsViewModel.validFromDate(index: indexPath.row)
        cashbackCell.pointsLabelText = previousCashbackDetailsViewModel.pointsValue(index: indexPath.row)
        cashbackCell.cellImage = previousCashbackDetailsViewModel.cellImage(index: indexPath.row)
        
        return cashbackCell
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let sections = Sections(rawValue: section)
        if previousCashbackDetailsViewModel.goalTrackersCount() != 0 {
            if sections == .header {
                let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier) as!    VIATableViewSectionHeaderFooterView
                
                view.labelText = CommonStrings.Dc.CashbackEarned.FooterAmount1539(previousCashbackDetailsViewModel.getCashLimit())
                view.backgroundColor = UIColor.tableViewBackground()
                
                return view
            }
        }
        
        return nil
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let sections = Sections(rawValue: section)
        if previousCashbackDetailsViewModel.goalTrackersCount() != 0 {
            if sections == .cashbacks {
                let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier) as! VIATableViewSectionHeaderFooterView
                view.labelText = CommonStrings.Dc.CashbackEarned.FooterItemSubtitle1568
                view.backgroundColor = UIColor.tableViewBackground()
                
                return view
            }
        }
        
        return nil
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch Sections.allValues[indexPath.section] {
        case .previousCashbacks:
            return 50
        default:
            break
        }
        
        return UITableViewAutomaticDimension
    }
}
