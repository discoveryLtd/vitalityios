//
//  DCPreviousCashbacksViewController.swift
//  VitalityActive
//
//  Created by Steven Layug on 4/17/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import VIAUIKit
import VitalityKit
import VIAUtilities

class DCPreviousCashbacksViewController: VIATableViewController {
    let previousCashbacksViewModel = DCPreviousCashbacksViewModel()
    var selectedBenefitId: Int?
    
    // MARK: View Lifecycles
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = CommonStrings.Dc.CashbackEarned.PreviousLink1540
        initUI()
    }
    
    func initUI() {
        configureAppearance()
        configureTableView()
    }
    
    func configureAppearance() {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.makeNavigationBarTransparent()
        
        self.hideBackButtonTitle()
    }
    
    func configureTableView() {
        self.tableView.register(VIATableViewSectionHeaderFooterView.nib(), forHeaderFooterViewReuseIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier)
        self.tableView.register(DCPreviousCashbackCell.nib(), forCellReuseIdentifier: DCPreviousCashbackCell.defaultReuseIdentifier)
        self.tableView.register(VIATableViewCell.nib(), forCellReuseIdentifier: VIATableViewCell.defaultReuseIdentifier)
        self.tableView.estimatedRowHeight = 75
        self.tableView.backgroundColor = UIColor.tableViewBackground()
        self.tableView.tableFooterView = UIView()
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
    }
    
    // MARK: TableView Datasouce and Delegate
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return previousCashbacksViewModel.pastBenefitsCount()
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override public func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.1
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: DCPreviousCashbackCell.defaultReuseIdentifier, for: indexPath)
        let previousCashbackCell = cell as! DCPreviousCashbackCell
        
        previousCashbackCell.deviceLabelText = previousCashbacksViewModel.pastBenefit(index: indexPath.row).purchase?.purchaseItemProduct?.description
        previousCashbackCell.cashbackEarnedLabelText = "$\(previousCashbacksViewModel.pastBenefit(index: indexPath.row).totalGoalsRewardQuantity) \(CommonStrings.Dc.CashbackEarned.FooterItemSubtitle1568)"
        previousCashbackCell.dateRangeLabelText = previousCashbacksViewModel.headerDateRange(index: indexPath.row)
        
        return previousCashbackCell
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier) as! VIATableViewSectionHeaderFooterView
        view.labelText = CommonStrings.Dc.CashbackEarned.PreviousDevicesHeader1541
        view.backgroundColor = UIColor.tableViewBackground()
        
        return view
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showPreviousCashbackDetails" {
            if let previousCashbackDetailsVC = segue.destination as? DCPreviousCashbackDetailsViewController {
                previousCashbackDetailsVC.selectedBenefitId = self.selectedBenefitId!
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedBenefitId = previousCashbacksViewModel.pastBenefit(index: indexPath.row).id
        self.performSegue(withIdentifier: "showPreviousCashbackDetails", sender: nil)
    }
}
