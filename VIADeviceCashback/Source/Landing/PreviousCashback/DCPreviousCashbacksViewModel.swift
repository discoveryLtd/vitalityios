//
//  DCPreviousCashbacksViewModel.swift
//  VitalityActive
//
//  Created by Steven Layug on 5/16/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import VIAUIKit
import VIAUtilities
import VitalityKit
import VIACommon
import RealmSwift

class DCPreviousCashbacksViewModel {
    let realm = DataProvider.newGDCRealm()
    // NOTE: Created an array because having issues creating a Results<GDCBGRBenefit> object type, will update
    var pastBenefits: [GDCBGRBenefit] = []
    
    func pastBenefit(index: Int) -> GDCBGRBenefit {
        return pastBenefits[index]
    }
    
    func pastBenefitsCount() -> Int {
        let benefits = (realm.allGDCBGRBenefits().filter { $0.id != self.realm.allGDCBenefit().first?.benefitId })
        for benefit in benefits {
            // NOTE: Noticed that is method is call several times during load time, for now checking if array is empty to avoid duplicate item, will update this
            if self.pastBenefits.isEmpty {
            pastBenefits.append(benefit)
            }
        }
        
        return pastBenefits.count
    }
    
    func headerDateRange(index: Int) -> String {
        return self.dateFormatter(startDate: self.pastBenefits[index].goalTrackers.first?.validFrom, endDate: self.pastBenefits[index].goalTrackers.last?.validFrom)
    }

    func dateFormatter (startDate: String?, endDate: String?) -> String{
        let dateResponseFormatter = DateFormatter()
        dateResponseFormatter.dateFormat = "yyyy-MM-dd"
        let dateStringFormatter = DateFormatter()
        dateStringFormatter.dateFormat = "MMMM yyyy"
        
        guard let firstStartDate = startDate, let lastStartDate = endDate else {
            return ""
        }
        let fromDate: Date? = dateResponseFormatter.date(from: (firstStartDate))
        let toDate: Date? = dateResponseFormatter.date(from: (lastStartDate))
        
        // OJ: Don't force unwrap the dates. This can lead to NPE.
        return CommonStrings.Dc.CashbackEarned.PeriodHeader1537(dateStringFormatter .string(from: fromDate!), dateStringFormatter .string(from: toDate!))
    }
}
