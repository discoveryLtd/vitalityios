//
//  DCPreviousCashbackDetailsViewModel.swift
//  VitalityActive
//
//  Created by Steven Layug on 5/17/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import UIKit

import Foundation
import VitalityKit
import VIAUtilities
import VIAUIKit
import RealmSwift

class DCPreviousCashbackDetailsViewModel {
    let realm = DataProvider.newGDCRealm()
    let globalRealm = DataProvider.newRealm()
    var selectedBenefitId: Int?
    
    enum Objectives: String, EnumCollection, Equatable {
        case pinkIcon = "0"
        case orangeIcon = "1"
        case blueIcon = "2"
        case greenIcon = "3"
    }
    
    func setSelectedBenefitId(benefitId: Int) {
        self.selectedBenefitId = benefitId
    }
    
    func selectedBenefit() -> GDCBGRBenefit? {
        let benefit = (realm.allGDCBGRBenefits().filter { $0.id == self.selectedBenefitId})
        return benefit.first
    }
    
    func headerCoinsEarnedValue () -> String {
        if let benefit = self.selectedBenefit() {
            return "$\(benefit.totalGoalsRewardQuantity)"
        }
        return ""
    }
    
    func headerDateRange() -> String {
        return self.dateFormatter(startDate: self.selectedBenefit()?.goalTrackers.first?.validFrom, endDate: self.selectedBenefit()?.goalTrackers.last?.validFrom)
    }
    
    func dateFormatter (startDate: String?, endDate: String?) -> String{
        let dateResponseFormatter = DateFormatter()
        dateResponseFormatter.dateFormat = "yyyy-MM-dd"
        let dateStringFormatter = DateFormatter()
        dateStringFormatter.dateFormat = "MMMM yyyy"
        
        guard let firstStartDate = startDate, let lastStartDate = endDate else {
            return ""
        }
        let fromDate: Date? = dateResponseFormatter.date(from: (firstStartDate))
        let toDate: Date? = dateResponseFormatter.date(from: (lastStartDate))
        
        return CommonStrings.Dc.CashbackEarned.PeriodHeader1537(dateStringFormatter .string(from: fromDate!), dateStringFormatter .string(from: toDate!))
    }
    
    func headerGoalsRemaining() -> String {
        if let benefit = self.selectedBenefit() {
            return CommonStrings.Dc.CashbackEarned.PeriodRemainingDesc1538("\(benefit.goalsRemaining)")
        }
        return ""
    }
    
    func goalTrackersCount() -> Int {
        return self.selectedBenefit()?.goalTrackers.count ?? 0
    }
    
    func earnedCashValue(index: Int) -> String{
        return "\(self.selectedBenefit()?.goalTrackers[index].rewardValues.first?.totalRewardValueQuantity ?? 0)"
    }
    
    func validFromDate(index: Int) -> String {
        let dateResponseFormatter = DateFormatter()
        dateResponseFormatter.dateFormat = "yyyy-MM-dd"
        let dateStringFormatter = DateFormatter()
        dateStringFormatter.dateFormat = "MMMM yyyy"
        
        guard let validToDate = self.selectedBenefit()?.goalTrackers[index].validFrom else {
            return ""
        }
        let stringToDate = dateResponseFormatter.date(from: validToDate)
        return dateStringFormatter .string(from: stringToDate!)
    }
    
    func pointsValue(index: Int) -> String {
        return "\(self.selectedBenefit()?.goalTrackers[index].pointsAchievedTowardsGoal ?? 0) points"
    }
    
    func cellImage(index: Int) -> UIImage {
        let objectiveLevel = self.selectedBenefit()?.goalTrackers[index].highestObjectiveAchievedName
        
        if (objectiveLevel?.range(of: Objectives.pinkIcon.rawValue) != nil){
            return VIADeviceCashbackAsset.DeviceCashback.CashbackEarned.pinkCoinHome.image
        } else if (objectiveLevel?.range(of: Objectives.orangeIcon.rawValue) != nil){
            return VIADeviceCashbackAsset.DeviceCashback.CashbackEarned.orangeCoinHome.image
        } else if (objectiveLevel?.range(of: Objectives.blueIcon.rawValue) != nil){
            return VIADeviceCashbackAsset.DeviceCashback.CashbackEarned.blueCoinHome.image
        } else if (objectiveLevel?.range(of: Objectives.greenIcon.rawValue) != nil){
            return VIADeviceCashbackAsset.DeviceCashback.CashbackEarned.greenCoinHome.image
        } else {
            return VIADeviceCashbackAsset.DeviceCashback.CashbackEarned.pinkCoinHome.image
        }
        return UIImage()
    }
    
    func getCashLimit() -> String {
        if let amount = (selectedBenefit()?.purchase?.amounts.filter {$0.typeKey == 4}) {
            //TODO: Will be updated to Regex if applicable
            guard let cashLimit = amount.first?.value.replacingOccurrences(of: "ZAR ", with: "$") else {
                return ""
            }
            return cashLimit
        }  else {
            return ""
        }
    }
}
