//
//  DCYourOrderTableViewController.swift
//  VitalityActive
//
//  Created by Von Kervin R. Tuico on 23/04/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import VIAUIKit
import VitalityKit
import VIAUtilities

class DCYourOrderTableViewController: VIATableViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initUI()
    }
    
    func initUI() {
        initNavigationBar()
        initTableView()
    }
    
    func initNavigationBar() {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        //        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.backgroundColor = .white
        
        self.hideBackButtonTitle()
    }
    
    func initTableView() {
        /* For Input TextFields in Purchase Detail */
        self.tableView.register(VIATextFieldTableViewCell.nib(), forCellReuseIdentifier: VIATextFieldTableViewCell.defaultReuseIdentifier)
        
        /* For Header Cell */
        self.tableView.register(VIAGenericContentCell.nib(), forCellReuseIdentifier: VIAGenericContentCell.defaultReuseIdentifier)
        
        /* For View in Header and Footer */
        self.tableView.register(VIATableViewSectionHeaderFooterView.nib(), forHeaderFooterViewReuseIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier)
        
        self.tableView.estimatedRowHeight = 96
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.separatorInset = UIEdgeInsets(top: 0, left: 52, bottom: 0, right: 0)
        self.tableView.tableFooterView = UIView()
        self.tableView.backgroundColor = .white
        self.tableView.tableFooterView = UIView()
    }
    
    enum Sections: Int, EnumCollection, Equatable {
        case header = 0
        case purchaseDetail = 1
        
        func title() -> String? {
            switch self {
            case .purchaseDetail:
                return CommonStrings.Dc.LandingActivationPurchaseDetailTitle1489
            default: return nil
            }
        }
    }
    
    enum PurchaseDetailFields: Int, EnumCollection, Equatable {
        case model = 0
        case invoiceNumber = 1
        case deviceSerialNumber = 2
        case purchaseAmount = 3
        
        func title() -> String? {
            switch self {
            case .model:
                return CommonStrings.Dc.LandingActivationPurchaseDetailModelField1490
            case .invoiceNumber:
                return CommonStrings.Dc.LandingActivationPurchaseDetailInvoiceNumberField1491
            case .deviceSerialNumber:
                return CommonStrings.Dc.LandingActivationPurchaseDetailDevSerialNumberField1493
            case .purchaseAmount:
                return CommonStrings.Dc.LandingActivationPurchaseDetailPurchaseAmtField1495
            }
        }
        
        func hint() -> String? {
            switch self {
            case .model:
                return ""
            case .invoiceNumber:
                return CommonStrings.Dc.LandingActivationPurchaseDetailInvoiceNumberHint1492
            case .deviceSerialNumber:
                return CommonStrings.Dc.LandingActivationPurchaseDetailDevSerialNumberHint1494
            case .purchaseAmount:
                return "$0.00"
            }
        }
    }
    
}

extension DCYourOrderTableViewController {
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        } else {
            return PurchaseDetailFields.allValues.count
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch Sections.allValues[indexPath.section] {
        case .header:
            return headerCell(at: indexPath)
        case .purchaseDetail:
            return purchaseDetailCell(at: indexPath)
        default:
            return self.tableView.defaultTableViewCell()
        }
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let sections = Sections(rawValue: section)
        
        let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier) as! VIATableViewSectionHeaderFooterView
        
        if sections == .purchaseDetail {
            view.labelText = sections?.title()
            view.aLabel.backgroundColor = .white
            view.set(font: UIFont.title2Font())
            view.set(textColor: .black)
            view.contentView.backgroundColor = UIColor.white
            return view
        }
        return UIView()
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView()
        view.backgroundColor = .white
        return view
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        switch Sections.allValues[section] {
        case .purchaseDetail:
            return 0
        default:
            return UITableViewAutomaticDimension
        }
    }
    
    func headerCell(at indexPath: IndexPath) -> UITableViewCell {
        
        let cell = self.tableView.dequeueReusableCell(withIdentifier: VIAGenericContentCell.defaultReuseIdentifier, for: indexPath)
        let headerCell = cell as! VIAGenericContentCell
        headerCell.cellImageView.isHidden = true
        headerCell.header = CommonStrings.Dc.Common.YourOrder1559
        headerCell.content = "Once your device is delivered, look for information on when your monthly target begins so you can start earning points towards your cashback."
        headerCell.selectionStyle = .none
        return cell
    }
    
    func purchaseDetailCell(at indexPath: IndexPath) -> UITableViewCell {
        
        let labels = PurchaseDetailFields.allValues[indexPath.row]
        
        let cell = self.tableView.dequeueReusableCell(withIdentifier: VIATextFieldTableViewCell.defaultReuseIdentifier, for: indexPath)
        let inputFieldCell = cell as! VIATextFieldTableViewCell
        inputFieldCell.setHeadingLabelText(text: labels.title())
        inputFieldCell.setTextFieldText(text: labels.hint())
        inputFieldCell.hideCellImage()
        inputFieldCell.textField.isUserInteractionEnabled = false
        inputFieldCell.selectionStyle = .none
        return cell
    }
}
