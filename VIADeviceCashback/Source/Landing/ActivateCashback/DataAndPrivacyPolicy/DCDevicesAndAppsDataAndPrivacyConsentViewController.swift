//
//  DCDevicesAndAppsDataAndPrivacyConsentViewController.swift
//  VitalityActive
//
//  Created by Von Kervin R. Tuico on 17/04/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import VitalityKit
import VIAUtilities
import VIAUIKit
import VIACommon

class DCDevicesAndAppsDataAndPrivacyConsentViewController: CustomTermsConditionsViewController, KnowYourHealthTintable {
    
    // MARK: View Lifecycles
    override open func viewDidLoad() {
        super.viewDidLoad()
        
        // Get content model.
        self.viewModel = CustomTermsConditionsViewModel(articleId: AppConfigFeature.contentId(for: .SVDSConsentContent))
    }
    
    override func configureAppearance() {
        super.configureAppearance()
        
        navigationController?.makeNavigationBarInvisible()
        navigationController?.navigationBar.isHidden = true
    }
    
    // MARK: Action Buttons
    override func leftButtonTapped(_ sender: UIBarButtonItem?) {
        self.performSegue(withIdentifier: "unwindToDCLinkDeviceViewController", sender: self)
    }
    
    override func rightButtonTapped(_ sender: UIBarButtonItem?) {
        self.showHUDOnWindow()
        
        // TODO: Question on what to use.
        let events = [EventTypeRef.DataPrivacyAccept]
        viewModel?.agreeToTermsAndConditions(with: events, completion: { [weak self] (error) in
            self?.hideHUDFromWindow()
            guard error == nil else {
                self?.handleErrorOccurred(error)
                return
            }
            self?.showActivatedScreen()
        })
    }
    
    func showActivatedScreen() {
        self.performSegue(withIdentifier: "showCompletion", sender: nil)
    }
    
    func handleErrorOccurred(_ error: Error?) {
        guard error == nil else {
            debugPrint(error as Any)
            switch error {
            case is BackendError:
                self.handleBackendErrorWithAlert(error as! BackendError, tryAgainAction: { [weak self] in
                    self?.rightButtonTapped(nil)
                })
            default:
                self.displayUnknownServiceErrorOccurredAlert()
            }
            self.hideHUDFromWindow()
            return
        }
    }
}


