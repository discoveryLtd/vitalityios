//
//  ActivateDeviceCashbackViewController.swift
//  VitalityActive
//
//  Created by Von Kervin R. Tuico on 11/04/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import VIAUIKit
import VitalityKit
import VIAUtilities
import RealmSwift

public struct ActivationRequestDataHolder {
    var model: String = ""
    var modelDetails: GDCQualifyingDeviceses?
    var invoiceNumber: String = ""
    var deviceSerialNumber: String = ""
    var purchaseAmount: String = ""
    var purchaseDateForDisplay: String = ""
    var purchaseDateWithTimezoneAPI: String = ""
    var purchaseDate: String = ""
}

class ActivateDeviceCashbackViewController: VIATableViewController {
    
    /* For persistent of Selected Images */
    var imageDataList: Array<Dictionary<String, Any>> = []
    
    var isCollapsed: Bool? = true
    var purchaseDateRow = 1
    
    public var dataHolder: ActivationRequestDataHolder?
    
    /* For Model Information Holder */
    var qualifyingDevice: GDCQualifyingDeviceses?
    var qualifyingDeviceIndex: Int?
    
    var model: String?
    var invoiceNumber: String?
    var deviceSerialNumber: String?
    var purchaseAmount: String?
    var purchaseDate: String?
    var purchaseDateWithTimezoneAPI: String?
    var purchaseDateForDisplay : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initUI()
    }
    
    func initUI() {
        initNavigationBar()
        initTableView()
    }
    
    func initNavigationBar() {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.makeNavigationBarTransparent()
        
        self.addLeftBarButtonItem(CommonStrings.CancelButtonTitle24, target: self, selector: #selector(onCancel))
        self.addRightBarButtonItem(CommonStrings.NextButtonTitle84, target: self, selector: #selector(onNext))
        //        self.navigationItem.rightBarButtonItem?.isEnabled = false
        
        self.hideBackButtonTitle()
    }
    
    func initTableView() {
        /* For Header View */
        self.tableView.register(VIASubtitleTableViewCell.nib(), forCellReuseIdentifier: VIASubtitleTableViewCell.defaultReuseIdentifier)
        
        /* For Value Holder in Purchase Detail */
        self.tableView.register(VIARightDetailTableViewCell.nib(), forCellReuseIdentifier: VIARightDetailTableViewCell.defaultReuseIdentifier)
        
        /* For Input TextFields in Purchase Detail */
        self.tableView.register(VIATextFieldTableViewCell.nib(), forCellReuseIdentifier: VIATextFieldTableViewCell.defaultReuseIdentifier)
        
        /* For View in Header and Footer */
        self.tableView.register(VIATableViewSectionHeaderFooterView.nib(), forHeaderFooterViewReuseIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier)
        
        /* For Collapsible Date Picker */
        self.tableView.register(VIADatePickerTableViewCell.nib(), forCellReuseIdentifier: VIADatePickerTableViewCell.defaultReuseIdentifier)
        
        self.tableView.register(VIATableViewCell.nib(), forCellReuseIdentifier: VIATableViewCell.defaultReuseIdentifier)
        self.tableView.estimatedRowHeight = 96
        self.tableView.separatorInset = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 0)
        self.tableView.tableFooterView = UIView()
    }
    
    enum Sections: Int, EnumCollection, Equatable {
        case header = 0
        case purchaseDetails = 1
        case purchaseDate = 2
        
        func title() -> String? {
            switch self {
            case .purchaseDetails:
                return CommonStrings.Dc.LandingActivationPurchaseDetailTitle1489
            default: return nil
            }
        }
    }
    
    enum PurchaseDetailFields: Int, EnumCollection, Equatable {
        case model = 0
        case invoiceNumber = 1
        case deviceSerialNumber = 2
        case purchaseAmount = 3
        
        func title() -> String? {
            switch self {
            case .model:
                return CommonStrings.Dc.LandingActivationPurchaseDetailModelField1490
            case .invoiceNumber:
                return CommonStrings.Dc.LandingActivationPurchaseDetailInvoiceNumberField1491
            case .deviceSerialNumber:
                return CommonStrings.Dc.LandingActivationPurchaseDetailDevSerialNumberField1493
            case .purchaseAmount:
                return CommonStrings.Dc.LandingActivationPurchaseDetailPurchaseAmtField1495
            }
        }
        
        func defaultValue() -> String? {
            switch self {
            case .model:
                return ""
            case .invoiceNumber:
                return CommonStrings.Dc.LandingActivationPurchaseDetailInvoiceNumberHint1492
            case .deviceSerialNumber:
                return CommonStrings.Dc.LandingActivationPurchaseDetailDevSerialNumberHint1494
            case .purchaseAmount:
                return "$0.00"
            }
        }
    }
    
    func onNext() {
        guard let model = self.model else { return }
        guard let modelDetails = self.qualifyingDevice else { return }
        guard let invoiceNumber = self.invoiceNumber else { return }
        guard let deviceSerialNumber = self.deviceSerialNumber else { return }
        guard let purchaseAmount = self.purchaseAmount else { return }
        guard let purchaseDate = self.purchaseDate else { return }
        guard let purchaseDateWithTimezoneAPI = self.purchaseDateWithTimezoneAPI else { return }
        guard let purchaseDateForDisplay = self.purchaseDateForDisplay else { return }
        
        dataHolder = ActivationRequestDataHolder(model: model, modelDetails: modelDetails, invoiceNumber: invoiceNumber, deviceSerialNumber: deviceSerialNumber, purchaseAmount: purchaseAmount, purchaseDateForDisplay: purchaseDateForDisplay, purchaseDateWithTimezoneAPI: purchaseDateWithTimezoneAPI, purchaseDate: purchaseDate)
        
        if let data = dataHolder {
            let realm = DataProvider.newGDCRealm()
            do {
                try realm.write {
                    realm.delete(realm.allPurchaseDetailSummaryItems())
                    
                    // Map action values to Purchase Detail Items
                    let items = GDCPurchaseDetailSummary()
                    items.model = data.model
                    items.modelDetails = data.modelDetails
                    items.invoiceNumber = data.invoiceNumber
                    items.deviceSerialNumber = data.deviceSerialNumber
                    items.purchaseAmount = data.purchaseAmount
                    items.purchaseDate = data.purchaseDate
                    items.purchaseDateWithTimezoneAPI = data.purchaseDateWithTimezoneAPI
                    items.purchaseDateForDisplay = data.purchaseDateForDisplay
                    
                    // Add Provide Feedback Items to local database
                    realm.add(items)
                }
            } catch let error {
                debugPrint("\(error)")
            }
            
        }
        self.performSegue(withIdentifier: "showAddAttachment", sender: self)
    }
    
    func onCancel() {
        _ = navigationController?.popViewController(animated: true)
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if let addAttachmentCollectionViewController = segue.destination as? AddAttachmentCollectionViewController {
            addAttachmentCollectionViewController.delegate = self
            addAttachmentCollectionViewController.totalAttachments = self.imageDataList
        }
        
        if let deviceModelViewController = segue.destination as? DeviceModelViewController {
            deviceModelViewController.delegate = self
            deviceModelViewController.qualifyingDevice = self.qualifyingDevice
        }
    }
}

extension ActivateDeviceCashbackViewController {
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return Sections.allValues.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch Sections.allValues[section] {
        case .header:
            return 1
        case .purchaseDetails:
            return PurchaseDetailFields.allValues.count
        case .purchaseDate:
            if isCollapsed == true {
                return 1
            } else {
                return 2
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch Sections.allValues[indexPath.section] {
        case .header:
            return headerCell(at: indexPath)
        case .purchaseDetails:
            return purchaseDetailCell(at: indexPath)
        case .purchaseDate:
            return purchaseDateCell(at: indexPath)
        }
        return self.tableView.defaultTableViewCell()
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch Sections.allValues[indexPath.section] {
        case .purchaseDetails:
            if indexPath.row == 0 {
                self.performSegue(withIdentifier: "showDeviceModel", sender: self)
            }
            break
        case .purchaseDate:
            if indexPath.row == 0 {
                self.isCollapsed = !(isCollapsed ?? false)
                self.tableView.reloadSections([indexPath.section], with: .automatic)
            }
            break
        default: break
        }
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let sections = Sections(rawValue: section)
        
        let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier) as! VIATableViewSectionHeaderFooterView
        
        if sections == .purchaseDetails {
            view.labelText = sections?.title()
            view.set(font: UIFont.title2Font())
            view.set(textColor: .black)
            return view
        } else if sections == .purchaseDate {
            view.labelText = " "
            view.set(font: UIFont.title2Font())
            return view
        } else {
            let view = UIView()
            view.backgroundColor = self.tableView.backgroundColor
            self.tableView.tableHeaderView = view
            return self.tableView.tableHeaderView
        }
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let sections = Sections(rawValue: section)
        
        let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier) as! VIATableViewSectionHeaderFooterView
        
        if sections == .purchaseDate {
            view.labelText = CommonStrings.Dc.LandingActivationPurchaseDetailFooter1498
            view.set(textColor: .mediumGrey())
            return view
        } else {
            let view = UIView()
            view.backgroundColor = self.tableView.backgroundColor
            return view
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch Sections.allValues[section] {
        case .header:
            return 0
        default:
            return UITableViewAutomaticDimension
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        switch Sections.allValues[section] {
        case .header:
            return 0
        case .purchaseDetails:
            return 0
        default:
            return UITableViewAutomaticDimension
        }
    }
    
    func headerCell(at indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: VIASubtitleTableViewCell.defaultReuseIdentifier, for: indexPath)
        let headerCell = cell as! VIASubtitleTableViewCell
        
        headerCell.headingText = CommonStrings.Dc.Activate.HeaderTitle1487
        headerCell.customHeaderLabelFont = UIFont.headlineFont()
        headerCell.contentText = CommonStrings.Dc.LandingActivationDescription1488
        headerCell.customContentLabelFont = UIFont.tableViewSectionHeaderFooter()
        headerCell.setContentLabelColor(textColor: .mediumGrey())
        headerCell.backgroundColor = self.tableView.backgroundColor
        return headerCell
    }
    
    func purchaseDetailCell(at indexPath: IndexPath) -> UITableViewCell {
        var cell: UITableViewCell?
        
        let labels = PurchaseDetailFields.allValues[indexPath.row]
        
        if indexPath.row == 0 {
            cell = self.tableView.dequeueReusableCell(withIdentifier: VIARightDetailTableViewCell.defaultReuseIdentifier, for: indexPath)
            let modelCell = cell as! VIARightDetailTableViewCell
            modelCell.setTitle(labels.title())
            
            if let model = self.qualifyingDevice?.productName, !model.isEmpty {
                modelCell.setValue(model)
                self.model = model
            } else {
                modelCell.setPlaceholder("")
            }
            
            modelCell.accessoryType = .disclosureIndicator
        } else {
            cell = self.tableView.dequeueReusableCell(withIdentifier: VIATextFieldTableViewCell.defaultReuseIdentifier, for: indexPath)
            let inputFieldCell = cell as! VIATextFieldTableViewCell
            inputFieldCell.setHeadingLabelText(text: labels.title())
            
            switch labels {
            case .invoiceNumber:
                if let invoiceNumber = self.invoiceNumber, !invoiceNumber.isEmpty {
                    inputFieldCell.setTextFieldText(text: invoiceNumber)
                } else {
                    inputFieldCell.setTextFieldPlaceholder(placeholder: labels.defaultValue())
                }
            case .deviceSerialNumber:
                if let deviceSerialNumber = self.deviceSerialNumber, !deviceSerialNumber.isEmpty {
                    inputFieldCell.setTextFieldText(text: deviceSerialNumber)
                } else {
                    inputFieldCell.setTextFieldPlaceholder(placeholder: labels.defaultValue())
                }
            case .purchaseAmount:
                if let purchaseAmount = self.purchaseAmount, !purchaseAmount.isEmpty {
                    inputFieldCell.setTextFieldText(text: purchaseAmount)
                } else {
                    inputFieldCell.setTextFieldPlaceholder(placeholder: labels.defaultValue())
                }
                inputFieldCell.textField.addTarget(self, action: #selector(currencyInput(_:)), for: .editingChanged)
                inputFieldCell.setTextFieldKeyboardType(type: .numberPad)
            default: break
            }
            
            inputFieldCell.hideCellImage()
            inputFieldCell.textField.delegate = self
            cell?.selectionStyle = .none
        }
        
        return cell!
    }
    
    func purchaseDateCell(at indexPath: IndexPath) -> UITableViewCell {
        
        var cell: UITableViewCell?
        
        if indexPath.row == 0 {
            cell = self.tableView.dequeueReusableCell(withIdentifier: VIARightDetailTableViewCell.defaultReuseIdentifier, for: indexPath)
            let dateCell = cell as! VIARightDetailTableViewCell
            let date = Date()
            
            if let date = self.purchaseDateForDisplay, !date.isEmpty {
                dateCell.setValue(date)
            } else {
                let defaultDateFormat = DateFormatter.yearMonthDayFormatter().string(from: date)
                let apiDateFormat = DateFormatter.apiManagerFormatterWithMilliseconds().string(from: date)
                let displayDateFormat = DateFormatter.dayDateShortMonthyearFormatter().string(from: date)
                
                dateCell.setValue(displayDateFormat)
                self.purchaseDate = defaultDateFormat
                self.purchaseDateWithTimezoneAPI = apiDateFormat
                self.purchaseDateForDisplay = displayDateFormat
            }
            
            dateCell.setTitle(CommonStrings.Dc.LandingActivationPurchaseDetailPurchaseDteField1497)
        } else {
            cell = self.tableView.dequeueReusableCell(withIdentifier: VIADatePickerTableViewCell.defaultReuseIdentifier, for: indexPath)
            let datePickerCell = cell as! VIADatePickerTableViewCell
            datePickerCell.delegate = self
            //            self.tableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
        }
        
        return cell!
    }
    
}

extension ActivateDeviceCashbackViewController: DatePickerDelegate {
    
    func didPickDate(_ date: Date) {
        let datePicked = date
        let defaultDateFormat = DateFormatter.yearMonthDayFormatter().string(from: date)
        let apiDateFormat = DateFormatter.apiManagerFormatterWithMilliseconds().string(from: datePicked)
        let displayDateFormat = DateFormatter.dayDateShortMonthyearFormatter().string(from: datePicked)
        self.purchaseDate = defaultDateFormat
        self.purchaseDateWithTimezoneAPI = apiDateFormat
        self.purchaseDateForDisplay = displayDateFormat
        
        guard let purchaseDateCell = self.tableView.cellForRow(at: IndexPath(row: 0, section: 2))
            as? VIARightDetailTableViewCell else { return }
        
        purchaseDateCell.setValue(self.purchaseDateForDisplay)
    }
}

extension ActivateDeviceCashbackViewController: AddAttachmentCollectionViewControllerProtocol {
    
    func persistImageDataList(totalImageDataAttachments: Array<Dictionary<String, Any>>) {
        self.imageDataList = totalImageDataAttachments
    }
}

extension ActivateDeviceCashbackViewController: DeviceModelViewControllerProtocol {
    
    func updateSelectedDeviceModel(qualifyingDevice: GDCQualifyingDeviceses, index: Int) {
        self.qualifyingDevice = qualifyingDevice
        self.qualifyingDeviceIndex = index
        self.tableView.reloadData()
    }
}

extension ActivateDeviceCashbackViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        var newText = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        let numberOfChars = newText.characters.count // for Swift use count(newText)
        
        var view : UIView = textField
        repeat { view = view.superview! } while !(view is UITableViewCell)
        let cell = view as! VIATextFieldTableViewCell // or UITableViewCell or whatever
        let indexPath = self.tableView.indexPath(for:cell)!
        
        /* Determine the indexPath of the TextField */
        switch indexPath.row {
        case 1:
            self.invoiceNumber = newText
            if numberOfChars == 0 || numberOfChars > 9 {
                refreshUI(message: "Invalid Invoice Number", cell: cell)
            } else {
                refreshUI(cell: cell)
            }
        case 2:
            self.deviceSerialNumber = newText
            if numberOfChars == 0 || numberOfChars > 11 {
                refreshUI(message: "Invalid Serial Number", cell: cell)
            } else {
                refreshUI(cell: cell)
            }
        case 3:
            if numberOfChars == 0 || numberOfChars > 11 {
                refreshUI(message: "Invalid Amount", cell: cell)
            } else {
                refreshUI(cell: cell)
            }
        default: break
        }
        
        return true
    }
}

extension ActivateDeviceCashbackViewController {
    
    func refreshUI(message: String? = nil, cell: VIATextFieldTableViewCell) {
        
        /* Refresh the error message label in UI */
        UIView.performWithoutAnimation {
            self.tableView.beginUpdates()
            cell.setErrorMessage(message: message)
            self.tableView.endUpdates()
        }
    }
    
    func currencyInput(_ textField: UITextField) {
        
        if let amountString = textField.text?.currencyInputFormatting() {
            textField.text = amountString
            self.purchaseAmount = amountString
        }
    }
}
