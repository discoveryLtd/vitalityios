//
//  SummaryViewModel.swift
//  VitalityActive
//
//  Created by Von Kervin R. Tuico on 15/04/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import VIAUIKit
import VitalityKit
import VIAUtilities

public protocol SummaryViewModelDelegate: class {
    func displaySVSubmissionFailure(completion: @escaping (_ resubmit: Bool) -> Void)
}

class SummaryViewModel: NSObject, ActivateDeviceCashbackSummaryViewModelDelegate {
    // MARK: Variables
    let processActivation: SubmissionProcessor = SubmissionHelper()
    internal weak var delegate: SummaryViewModelDelegate?
    let imagesRealm = DataProvider.newGDCRealm()
    
    func registerProductPurchased(completion: @escaping (_ error: Error?) -> Void) {
        processActivation.processDeviceCashbackActivation(completion: completion)
    }
    
    func submittedImages() -> Array<UIImage> {
        var imagesArray: Array<UIImage> = Array<UIImage>()
        let images = imagesRealm.allPhotos()
        if let collectedImages = images {
            for collectedImage in collectedImages {
                if let assetURL = collectedImage.assetURL {
                    if let asset = PhotoAssetHelper.PhotosAssetForFileURL(url: assetURL) {
                        imagesArray.append(PhotoAssetHelper.getAssetThumbnail(asset: asset))
                    }
                } else {
                    if let imageData = collectedImage.assetData {
                        if let assetImage = UIImage(data: imageData) {
                            /*
                             Assets saved to realm as data using the UIImagePNGRepresentation method
                             If you save a UIImage as a JPEG, it will set the rotation flag.
                             PNGs do not support a rotation flag, so if you save a UIImage as a PNG,
                             it will be rotated incorrectly and not have a flag set to fix it.
                             So if you want PNGs you must rotate them yourself.
                             */
                            if let cgAsset = assetImage.cgImage {
                                let uprightImage = UIImage(cgImage: cgAsset, scale: 1, orientation: UIImageOrientation.right)
                                imagesArray.append(uprightImage)
                            } else {
                                imagesArray.append(assetImage)
                            }
                        }
                    }
                }
            }
        }
        return imagesArray
    }
    
    func summaryViewTitle() -> String {
        return CommonStrings.Dc.ProofOfPurchaseSummaryTitle1505
    }
    
    func summaryHeaderTitle() -> String {
        return CommonStrings.Dc.ProofOfPurchaseSummaryConfirmInfoHeader1506
    }
    
    func summaryHeaderMessage() -> String {
        return CommonStrings.Dc.ProofOfPurchaseSummaryConfirmInfoDescription1507
    }
    
    // MARK: Photo Upload Delegate
    func displayImageUploadErrorMessage(with completion: @escaping (_ resubmit: Bool) -> Void) {
        self.delegate?.displaySVSubmissionFailure(completion: { resubmit in
            completion(resubmit)
        })
    }
}
