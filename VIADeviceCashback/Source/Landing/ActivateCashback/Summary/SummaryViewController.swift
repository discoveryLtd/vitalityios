//
//  SummaryViewController.swift
//  VitalityActive
//
//  Created by Von Kervin R. Tuico on 14/04/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import VIAUIKit
import VIAUtilities
import VIACommon
import VitalityKit

public protocol ActivateDeviceCashbackSummaryViewModelDelegate {
    func submittedImages() -> Array<UIImage>
    func registerProductPurchased(completion: @escaping (_ error: Error?) -> Void)
    func summaryViewTitle() -> String
    func summaryHeaderTitle() -> String
    func summaryHeaderMessage() -> String
}

public class SummaryViewController: VIATableViewController, ImproveYourHealthTintable {
    
    @IBAction func unwindToDCSummaryViewController(segue:UIStoryboardSegue) { }
    
    internal var viewModel: ActivateDeviceCashbackSummaryViewModelDelegate?
    internal var heightForCell: CGFloat = 0.0
    
    let deviceCashbackRealm = DataProvider.newGDCRealm()
    var sections = Array<Any>()
    
    var summaryItems: GDCPurchaseDetailSummary?
    
    override public func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        
        self.viewModel = SummaryViewModel()
        self.title = self.viewModel?.summaryViewTitle()
        initUI()
        
        /* Fetch Stored Attachments */
        let images = deviceCashbackRealm.objects(PhotoAsset.self)
        print("-----> images [item count: \(images.count)]: \(images)")
        
        /* Fetch Purchase Detail Items */
        let summaryItems = self.deviceCashbackRealm.allPurchaseDetailSummaryItems()
        for item in summaryItems {
            self.summaryItems = item
            debugPrint("Stored Items: \(item)")
        }
    }
    
    public override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        // Reflect the latest size in tableHeaderView
        if self.tableView.shouldUpdateHeaderViewFrame() {
            
            // **This is where table view's content (tableHeaderView, section headers, cells)
            // frames are updated to account for the new table header size.
            self.tableView.beginUpdates()
            self.tableView.endUpdates()
        }
    }
    
    func initUI() {
        initNavigationBar()
        initTableView()
    }
    
    func initTableView() {
        self.tableView.register(GenericImageCollectionTableViewCell.nib(), forCellReuseIdentifier: GenericImageCollectionTableViewCell.defaultReuseIdentifier)
        self.tableView.register(HeaderValueSubtitleTableViewCell.nib(), forCellReuseIdentifier: HeaderValueSubtitleTableViewCell.defaultReuseIdentifier)
        self.tableView.register(VIATableViewSectionHeaderFooterView.nib(), forHeaderFooterViewReuseIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier)
        
        /* For Value Holder in Purchase Detail */
        self.tableView.register(VIARightDetailTableViewCell.nib(), forCellReuseIdentifier: VIARightDetailTableViewCell.defaultReuseIdentifier)
        
        /*  */
        self.tableView.register(VIAProfileDetailCell.nib(), forCellReuseIdentifier: VIAProfileDetailCell.defaultReuseIdentifier)
        
        self.tableView.estimatedRowHeight = 44
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.setupTableViewHeader()
    }
    
    func initNavigationBar() {
        navigationController?.navigationBar.isHidden = false
        navigationController?.makeNavigationBarTransparent()
        self.navigationController?.setToolbarHidden(true, animated: false)
        
        let next = UIBarButtonItem(title: CommonStrings.ConfirmTitleButton182, style: .plain, target: self, action: #selector(confirm))
        self.navigationItem.rightBarButtonItem = next
    }
    
    func setupTableViewHeader() {
        let header = GenericSummaryTitleMessageHeaderFooterView.viewFromNib(owner: self)
        let headerView = header as! GenericSummaryTitleMessageHeaderFooterView
        headerView.title = self.viewModel?.summaryHeaderTitle()
        headerView.content = self.viewModel?.summaryHeaderMessage()
        
        self.tableView.setTableHeaderView(headerView: headerView)
    }
    
    // MARK: Navigation
    
    func confirm() {
        if VitalityProductFeature.isEnabled(.SVDSConsent) {
            self.performSegue(withIdentifier: "showDataAndPrivacyPolicy", sender: nil)
        } else {
            submitActivationRequest()
        }
    }
    
    func submitActivationRequest() {
        self.showHUDOnWindow()
        self.viewModel?.registerProductPurchased(completion: { [weak self] error in
            DispatchQueue.main.async {
                self?.hideHUDFromWindow()
                if error == nil {
                    self?.performSegue(withIdentifier: "showDetailsSubmitted", sender: nil)
                } else {
                    debugPrint("Error")
                    self?.displaySubmissionFailure{ [weak self] (resubmit) in
                        if resubmit{
                            self?.submitActivationRequest()
                        }
                    }
                }
            }
        })
    }
    
    public func displaySubmissionFailure(completion: @escaping (_ resubmit: Bool) -> Void) {
        let cancelAction = UIAlertAction(title: CommonStrings.CancelButtonTitle24, style: UIAlertActionStyle.cancel) { (alertAction) in
            completion(false)
        }
        let tryAgain = UIAlertAction(title: CommonStrings.TryAgainButtonTitle43, style: UIAlertActionStyle.default) { (alertAction) in
            completion(true)
        }
        
        let failedToSubmitVHCAlert = UIAlertController(title: CommonStrings.PrivacyPolicy.UnableToCompleteAlertTitle115, message: CommonStrings.SummaryScreen.VhcCompleteErrorMessage187, preferredStyle: .alert)
        
        failedToSubmitVHCAlert.addAction(cancelAction)
        failedToSubmitVHCAlert.addAction(tryAgain)
        self.present(failedToSubmitVHCAlert, animated: true, completion: nil)
    }
    
    @IBAction public func unwindFromDCDataAndPrivacyPolicyAgreementToSummary(segue: UIStoryboardSegue) {
        debugPrint("unwindFromDCDataAndPrivacyPolicyAgreementToSummary")
    }
}

/**
 *  Tableview Delegate
 */
extension SummaryViewController {
    
    public override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    public override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let imagesRowCount = 1
        
        if section == 0 {
            return 5
        } else {
            return imagesRowCount
        }
        return 0
    }
    
    public override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = UITableViewCell()
        
        switch indexPath.section {
        case 0:
            cell = setUpPurchaseDetailResults(at: indexPath)
        case 1:
            cell = showImageCollectionForRow()
        default:
            cell = UITableViewCell()
        }
        return cell
    }
    
    public override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: false)
    }
    
    public override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if let header = self.tableView.dequeueReusableHeaderFooterView(withIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier) as? VIATableViewSectionHeaderFooterView {
            self.setup(header: header, for: section)
            return header
        }
        return nil
    }
    
    public override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
}

/**
 *  Tableview DataSource
 */
extension SummaryViewController {
    
    enum PurchaseDetailFields: Int, EnumCollection, Equatable {
        case model = 0
        case invoiceNumber = 1
        case deviceSerialNumber = 2
        case purchaseAmount = 3
        case purchaseDate = 4
        
        func title() -> String? {
            switch self {
            case .model:
                return CommonStrings.Dc.LandingActivationPurchaseDetailModelField1490
            case .invoiceNumber:
                return CommonStrings.Dc.LandingActivationPurchaseDetailInvoiceNumberField1491
            case .deviceSerialNumber:
                return CommonStrings.Dc.LandingActivationPurchaseDetailDevSerialNumberField1493
            case .purchaseAmount:
                return CommonStrings.Dc.LandingActivationPurchaseDetailPurchaseAmtField1495
            case .purchaseDate:
                return CommonStrings.Dc.LandingActivationPurchaseDetailPurchaseDteField1497
            }
        }
        
        func defaultValue() -> String? {
            switch self {
            case .model:
                return ""
            case .invoiceNumber:
                return CommonStrings.Dc.LandingActivationPurchaseDetailInvoiceNumberHint1492
            case .deviceSerialNumber:
                return CommonStrings.Dc.LandingActivationPurchaseDetailDevSerialNumberHint1494
            case .purchaseAmount:
                return "$0.00"
            case .purchaseDate:
                return ""
            }
        }
    }
    
    /* Header */
    func setup(header: VIATableViewSectionHeaderFooterView, for group: Int) {
        header.labelText = ""
        header.set(font: UIFont.subheadlineFont())
        header.set(textColor: UIColor.tableViewSectionHeaderFooter())
        
        switch group {
        case 0:
            header.labelText = CommonStrings.Dc.ProofOfPurchaseSummaryPurchaseDetailHeader1508
        case 1:
            header.labelText = CommonStrings.Dc.ProofOfPurchaseSummaryImageProofHeader1509
        default:
            header.labelText = ""
        }
    }
    
    func setUpPurchaseDetailResults(at indexPath: IndexPath) -> UITableViewCell {
        var cell: UITableViewCell?
        
        cell = self.tableView.dequeueReusableCell(withIdentifier: VIAProfileDetailCell.defaultReuseIdentifier, for: indexPath)
        let inputFieldCell = cell as! VIAProfileDetailCell
        
        let labels = PurchaseDetailFields.allValues[indexPath.row]
        inputFieldCell.labelText = labels.title()
        
        if let storedValues = summaryItems {
            
            if indexPath.row == 0 {
                inputFieldCell.valueText = storedValues.model
            } else if indexPath.row == 1 {
                inputFieldCell.valueText = storedValues.invoiceNumber
            } else if indexPath.row == 2 {
                inputFieldCell.valueText = storedValues.deviceSerialNumber
            } else if indexPath.row == 3 {
                inputFieldCell.valueText = storedValues.purchaseAmount
            } else if indexPath.row == 4 {
                inputFieldCell.valueText = storedValues.purchaseDateForDisplay
            }

        }
        cell?.selectionStyle = .none
        return cell!
    }
    
    func showImageCollectionForRow() -> UITableViewCell {
        if let imageCollectionCell = self.tableView.dequeueReusableCell(withIdentifier: GenericImageCollectionTableViewCell.defaultReuseIdentifier) {
            let collectionviewCell = self.setUpCaptureImages(cell: imageCollectionCell)
            self.heightForImageCollection(cell: collectionviewCell)
            return collectionviewCell
        }
        return UITableViewCell()
    }
    
    func setUpCaptureImages(cell: UITableViewCell) -> GenericImageCollectionTableViewCell {
        if let imageCollectionViewCell = cell as? GenericImageCollectionTableViewCell {
            if let capturedImages = self.viewModel?.submittedImages() {
                imageCollectionViewCell.setImageCollection(images: capturedImages)
            }
            imageCollectionViewCell.imageCollectionView?.bounces = false
            imageCollectionViewCell.viewWidth = self.view.frame.width
            imageCollectionViewCell.setupCollectionFlowLayoutSize()
            return imageCollectionViewCell
        }
        return GenericImageCollectionTableViewCell()
    }
    
    func heightForImageCollection(cell: GenericImageCollectionTableViewCell) {
        let collectionViewFlowLayout = cell.imageCollectionView?.collectionViewLayout
        let cellPadding = collectionViewFlowLayout?.collectionViewContentSize
        let height = cellPadding?.height
        self.heightForCell = height ?? 0
        cell.updateImageCollectionViewSize(with:height)
    }
    
}
