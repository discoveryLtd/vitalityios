//
//  PhotoUploader.swift
//  VitalityActive
//
//  Created by Von Kervin R. Tuico on 16/04/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import UIKit
import Photos
import RealmSwift
import VitalityKit

public protocol PhotoUploaderDelegate: class {
    func imagesUploaded(references: Array<String>)
}

class PhotoUploader: NSObject {
    var imagesUnableToUpload = Array<UploadableImage>()
    var imagesToBeUploaded = Array<UploadableImage>()
    weak var delegate: PhotoUploaderDelegate?
    
    func submitCapturedVHCImages(_ assets: Array<PhotoAsset>) {
        var uploadableImages = Array<UploadableImage>()
        for asset in assets {
            if let imageData = asset.assetData {
                let image = UploadableImage()
                image.setup(with: imageData)
                uploadableImages.append(image)
            } else {
                if let imageURL = asset.assetURL {
                    let image = UploadableImage()
                    image.setup(assetURLString: imageURL)
                    uploadableImages.append(image)
                }
            }
        }
        self.submitUploadableImages(images: uploadableImages)
    }
    
    func getAllUploadableImages(_ assets: Array<PhotoAsset>) {
        var uploadableImages = Array<UploadableImage>()
        for asset in assets {
            if let imageData = asset.assetData {
                let image = UploadableImage()
                image.setup(with: imageData)
                uploadableImages.append(image)
            } else {
                if let imageURL = asset.assetURL {
                    let image = UploadableImage()
                    image.setup(assetURLString: imageURL)
                    uploadableImages.append(image)
                }
            }
        }
        self.imagesToBeUploaded = uploadableImages
    }
    
    func allImagesSuccesfullyUploaded() -> Bool {
        return self.imagesUnableToUpload.count == 0
    }
    
    func reSubmitFailedImages() {
        self.submitUploadableImages(images: self.imagesUnableToUpload)
    }
    
    func submitUploadableImages(images: Array<UploadableImage>) {
        DispatchQueue.global(qos: .userInitiated).async {
            let group = DispatchGroup()
            let coreRealm = DataProvider.newRealm()
            let partyID = coreRealm.getPartyId()
            var imageReferences = Array<String>()
            
            let completion: (String?, UploadableImage?) -> Void = {
                (reference, assetFailedToUpload) in
                if let uploadedImageReference = reference {
                    imageReferences.append(uploadedImageReference)
                }
                
                if let failedPhotosAsset = assetFailedToUpload {
                    self.imagesUnableToUpload.append(failedPhotosAsset)
                }
                
                group.leave()
            }
            
            self.imagesUnableToUpload = Array<UploadableImage>()
            for image in images {
                group.enter()
                Wire.Content.uploadFile(partyId: partyID, fileContents: image.data, completion: { (fileUploadReference, error) in
                    if (error != nil) {
                        completion(nil, image)
                    } else {
                        completion(fileUploadReference, nil)
                    }
                })
            }
            _ = group.wait(timeout: .distantFuture)
            DispatchQueue.main.async {
                self.delegate?.imagesUploaded(references: imageReferences)
            }
        }
    }
}

class UploadableImage {
    var name: String
    var data: Data
    
    init() {
        name = ""
        data = Data()
    }
    
    func setup(with assetData: Data) {
        self.data = assetData
    }
    
    func setup(assetURLString: String) {
        if let asset = PhotoAssetHelper.PhotosAssetForFileURL(url: assetURLString) {
            let imageRequestOptions = PHImageRequestOptions()
            imageRequestOptions.isSynchronous = true
            let mainDispatch = DispatchGroup()
            mainDispatch.enter()
            PHImageManager.default().requestImageData(for: asset, options: imageRequestOptions, resultHandler: { (imageData, dataUTI, orientation, info) in
                if let data = imageData {
                    self.data = data
                }
                mainDispatch.leave()
            })
            _ = mainDispatch.wait(timeout: .distantFuture)
        }
    }
}

