//
//  DCCompletionViewController.swift
//  VitalityActive
//
//  Created by Von Kervin R. Tuico on 16/04/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import UIKit
import VIAUIKit
import VitalityKit
import VIAUtilities

import RealmSwift

class DCCompletionViewController: VIACirclesViewController {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.viewModel = DCCompletionViewModel()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func buttonTapped(_ sender: UIButton) {
        self.performSegue(withIdentifier: "unwindToDCLandingViewController", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "unwindToDCLandingViewController",
            let destination = segue.destination as? DCLandingViewController {
            destination.cardStatus = CardStatusTypeRef(rawValue: CardStatusTypeRef.Activated.rawValue)
        }
    }
}

public class DCCompletionViewModel: AnyObject, CirclesViewModel {
    
    public var image: UIImage? {
        return VIADeviceCashbackAsset.ActivateDeviceCashback.Activated.deviceCashbackActivated.image
    }
    
    public var heading: String? {
        return CommonStrings.Dc.Success.ActivatedTitle1523
    }
    
    public var message: String? {
        return CommonStrings.Dc.Success.ActivatedDescription1524
    }
    
    public var footnote: String? {
        return ""
    }
    
    public var buttonTitle: String? {
        return CommonStrings.GreatButtonTitle120
    }
    
    public var buttonHighlightedTextColor: UIColor? {
        return UIColor.improveYourHealthBlue()
    }
    
    public var gradientColor: Color {
        return .blue
    }
}
