//
//  DCDetailsSubmittedViewModel.swift
//  VitalityActive
//
//  Created by Von Kervin R. Tuico on 14/05/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import VIAUIKit
import VitalityKit
import VIAUtilities
import RealmSwift

public class DCDetailsSubmittedViewModel: AnyObject, CirclesViewModel {
    var realm = DataProvider.newRealm()
    public lazy var homeScreenData: Results<HomeScreenData> = {
        return self.realm.allHomeScreenData()
    }()
    
    public var chosenDeviceModel: String? {
        let allPurchaseDetailSummaryItems = DataProvider.newGDCRealm().allPurchaseDetailSummaryItems()
        var model = ""
        for summaryItem in allPurchaseDetailSummaryItems {
            model = summaryItem.model
        }
        return model
    }
    
    public var image: UIImage? {
        return VIADeviceCashbackAsset.ActivateDeviceCashback.deviceCashbackDetailsSubmitted.image
    }
    
    public var heading: String? {
        return CommonStrings.Dc.Success.DetailsSubmittedTitle1510
    }
    
    public var message: String? {
        return CommonStrings.Dc.Success.DetailsSubmittedDescription1511(chosenDeviceModel!)
    }
    
    public var footnote: String? {
        return ""
    }
    
    public var buttonTitle: String? {
        return CommonStrings.NextButtonTitle84
    }
    
    public var buttonHighlightedTextColor: UIColor? {
        return UIColor.improveYourHealthBlue()
    }
    
    public var gradientColor: Color {
        return .blue
    }
    
    func latestHomeScreenData() -> HomeScreenData? {
        return homeScreenData.first
    }
    
    func finaliseSubmission() {
        /* Delete Purchase Detail Summary */
        let realm = DataProvider.newGDCRealm()
        try! realm.write {
            realm.delete(realm.allPurchaseDetailSummaryItems())
        }
        
        // post notification
        NotificationCenter.default.post(name: .VIAVHCCompletedNotification, object: nil)
    }
}

