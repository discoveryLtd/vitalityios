//
//  DCDetailsSubmittedViewController.swift
//  VitalityActive
//
//  Created by Von Kervin R. Tuico on 17/04/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import VIAUIKit
import VitalityKit
import VIAUtilities
import RealmSwift

class DCDetailsSubmittedViewController: VIACirclesViewController {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.viewModel = DCDetailsSubmittedViewModel()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        // TODO: Uncomment this once the integration is being commenced.
        /*
         if let model = self.viewModel as? DCDetailsSubmittedViewModel {
         model.finaliseSubmission()
         }*/
    }
    
    override func buttonTapped(_ sender: UIButton) {
        self.performSegue(withIdentifier: "showLinkDevice", sender: self)
    }
}
