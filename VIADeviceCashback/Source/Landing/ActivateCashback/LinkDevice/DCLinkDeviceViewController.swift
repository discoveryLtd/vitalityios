//
//  DCLinkDeviceViewController.swift
//  VitalityActive
//
//  Created by Von Kervin R. Tuico on 17/04/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import VIAUIKit
import VitalityKit
import VIAUtilities
import RealmSwift

class DCLinkDeviceViewController: VIACirclesViewController {
    
    @IBAction func unwindToDCLinkDeviceViewController(segue:UIStoryboardSegue) { }
    
    public lazy var linkLabel: UIButton = {
        let view = VIAButton(title: CommonStrings.Dc.Attention.LinkDeviceLaterButton1515)
        view.tintColor = UIColor.day()
        view.hidesBorder = true
        if let highlightedTextColor = self.viewModel?.buttonHighlightedTextColor {
            view.highlightedTextColor = highlightedTextColor
        }
        return view
    }()
    
    public lazy var linkButton: UIButton = {
        let view = VIAButton(title: CommonStrings.Dc.Attention.LinkDeviceNowButton1514)
        view.tintColor = UIColor.day()
        if let highlightedTextColor = self.viewModel?.buttonHighlightedTextColor {
            view.highlightedTextColor = highlightedTextColor
        }
        return view
    }()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.viewModel = DCLinkDeviceViewModel()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        /* Hide default bottom button */
        button.isHidden = true
        
        /* Add the two new buttons */
        addLinkButton()
        addLinkLabel()
    }
    
    func addLinkButton() {
        gradientView.addSubview(linkButton)
        linkButton.addTarget(self, action: #selector(linkButtonTapped(_:)), for: .touchUpInside)
        linkButton.snp.makeConstraints { make in
            make.width.equalTo(gradientView.snp.width).multipliedBy(0.5)
            make.height.equalTo(44)
            make.bottom.equalTo(gradientView.snp.bottom).offset(-85)
            make.centerX.equalTo(gradientView.snp.centerX)
        }
    }
    
    func addLinkLabel() {
        gradientView.addSubview(linkLabel)
        linkLabel.addTarget(self, action: #selector(linkLabelTapped(_:)), for: .touchUpInside)
        linkLabel.snp.makeConstraints { make in
            make.width.equalTo(gradientView.snp.width).multipliedBy(0.5)
            make.height.equalTo(44)
            make.bottom.equalTo(gradientView.snp.bottom).offset(-35)
            make.centerX.equalTo(gradientView.snp.centerX)
        }
        
    }
    
    open func linkLabelTapped(_ sender: UIButton) {
        self.performSegue(withIdentifier: "unwindToDCLandingViewController", sender: self)
    }
    
    open func linkButtonTapped(_ sender: UIButton) {
        self.performSegue(withIdentifier: "showDevicesAndAppsDataAndPrivacyConsent", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "unwindToDCLandingViewController",
            let destination = segue.destination as? DCLandingViewController {
            destination.cardStatus = CardStatusTypeRef(rawValue: CardStatusTypeRef.ContinueActivation.rawValue)
        }
    }
}
