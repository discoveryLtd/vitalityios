//
//  DCLinkDeviceViewModel.swift
//  VitalityActive
//
//  Created by Von Kervin R. Tuico on 14/05/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import VIAUIKit
import VitalityKit
import VIAUtilities
import RealmSwift

public class DCLinkDeviceViewModel: AnyObject, CirclesViewModel {
    
    public var chosenDeviceModel: String? {
        let allPurchaseDetailSummaryItems = DataProvider.newGDCRealm().allPurchaseDetailSummaryItems()
        var model = ""
        for summaryItem in allPurchaseDetailSummaryItems {
            model = summaryItem.model
        }
        return model
    }
    
    public var image: UIImage? {
        return VIADeviceCashbackAsset.DeviceCashback.Onboarding.getActive.image
    }
    
    public var heading: String? {
        return CommonStrings.Dc.Attention.LinkDeviceTitle1512(chosenDeviceModel!)
    }
    
    public var message: String? {
        return CommonStrings.Dc.Attention.LinkAppDescription1513(chosenDeviceModel!)
    }
    
    public var footnote: String? {
        return ""
    }
    
    public var buttonTitle: String? {
        return ""
    }
    
    public var buttonHighlightedTextColor: UIColor? {
        return UIColor.improveYourHealthBlue()
    }
    
    public var gradientColor: Color {
        return .blue
    }
}

