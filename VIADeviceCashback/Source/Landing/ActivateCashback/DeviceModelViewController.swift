//
//  DeviceModelViewController.swift
//  VitalityActive
//
//  Created by Von Kervin R. Tuico on 12/04/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import VIAUIKit
import VitalityKit

protocol DeviceModelViewControllerProtocol {
    func updateSelectedDeviceModel(qualifyingDevice: GDCQualifyingDeviceses, index: Int)
}

class DeviceModelViewController: VIATableViewController {
    
    var delegate: DeviceModelViewControllerProtocol!
    
    let deviceCashback = DataProvider.newGDCRealm()
    var devices: [GDCQualifyingDeviceses]? = []
    var qualifyingDevice: GDCQualifyingDeviceses?
    
    var selectedDeviceModelIndex: Int?
    
    override func viewWillAppear(_ animated: Bool) {
        if let productName = self.qualifyingDevice?.productName, !productName.isEmpty {
            self.tableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let storedQualifyingDevices = deviceCashback.allGDCQualifyingDeviceses()
        
        for device in storedQualifyingDevices {
            self.devices?.append(device)
        }
        
        self.title = CommonStrings.Dc.LandingActivationPurchaseDetailModelField1490
        initUI()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if isMovingFromParentViewController {
            if let qualifyingDevice = self.qualifyingDevice, let index = self.selectedDeviceModelIndex {
                delegate.updateSelectedDeviceModel(qualifyingDevice: qualifyingDevice, index: index)
                self.navigationController?.setToolbarHidden(true, animated: false)
                delegate = nil
            }
        }
    }
    
    func initUI() {
        initNavigationBar()
        initTableView()
    }
    
    func initTableView() {
        self.tableView.register(VIATableViewCell.nib(), forCellReuseIdentifier: VIATableViewCell.defaultReuseIdentifier)
        self.tableView.estimatedRowHeight = 96
        self.tableView.separatorInset = UIEdgeInsets(top: 0, left: 52, bottom: 0, right: 0)
        self.tableView.tableFooterView = UIView()
        self.tableView.backgroundColor = UIColor.tableViewBackground()
    }
    
    func initNavigationBar() {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.makeNavigationBarTransparent()
        self.hideBackButtonTitle()
    }

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    

}

extension DeviceModelViewController {
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let qualifyingDevices = self.devices, !qualifyingDevices.isEmpty {
            return qualifyingDevices.count
        }
        return 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return deviceModelCell(at: indexPath)
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let index = self.selectedDeviceModelIndex, index == indexPath.row {
            self.tableView.selectRow(at: indexPath, animated: false, scrollPosition: .none)
            cell.accessoryType = .checkmark
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.cellForRow(at: indexPath)?.accessoryType = .checkmark
        
        self.selectedDeviceModelIndex = indexPath.row
        self.qualifyingDevice = self.devices?[indexPath.row]
    }
    
    override func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        self.tableView.cellForRow(at: indexPath)?.accessoryType = .none
    }
    
    func deviceModelCell(at indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: VIATableViewCell.defaultReuseIdentifier, for: indexPath)
        let detailCell = cell as! VIATableViewCell
        
        let rows = self.devices?[indexPath.row]
        
        detailCell.labelText = rows?.productName
        detailCell.cellImageView.isHidden = true
        cell.selectionStyle = .none
        return cell
    }
}
