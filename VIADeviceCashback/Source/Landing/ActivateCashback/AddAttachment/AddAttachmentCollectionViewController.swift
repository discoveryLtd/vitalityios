//
//  AddAttachmentCollectionViewController.swift
//  VitalityActive
//
//  Created by Von Kervin R. Tuico on 12/04/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Photos

import VIAUIKit
import VitalityKit
import VIACommon

protocol AddAttachmentCollectionViewControllerProtocol {
    func persistImageDataList(totalImageDataAttachments: Array<Dictionary<String, Any>>)
}

class AddAttachmentCollectionViewController: UICollectionViewController {
    
    @IBOutlet weak var selectPhotosFlowLayout: UICollectionViewFlowLayout!
    @IBAction func unwindToAddAttachmentCollectionViewController(segue:UIStoryboardSegue) {
        self.collectionView?.reloadData()
    }
    
    var delegate: AddAttachmentCollectionViewControllerProtocol!
    
    // MARK: Total number of selected attachments
    var totalAttachments: Array<Dictionary<String, Any>> = []
    
    // MARK: Access SelectImageController contents
    var imagePicker: SelectImageController?
    
    // MARK: Access AddPhotoView contents
    var emptyStateAddPhotoView: AddPhotoView?
    
    // MARK: Variable for the UIBarButtonItem
    var selectBarButton: UIBarButtonItem?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if (isCollectionEmpty()) {
            emptyStateAddPhotoView = AddPhotoView.viewFromNib(owner: self)
            
            emptyStateAddPhotoView?.setHeader(title: CommonStrings.Dc.ProofOfPurchase.AddProofHeader1502)
            emptyStateAddPhotoView?.setDetail(message: CommonStrings.Dc.ProofOfPurchase.AddProofDescription1503)
            emptyStateAddPhotoView?.setAction(title: CommonStrings.Dc.ProofOfPurchase.AddButton1504)
            emptyStateAddPhotoView?.setHeader(image: VIADeviceCashbackAsset.ActivateDeviceCashback.Attachment.cameraLarge.templateImage)
            emptyStateAddPhotoView?.set(photoViewDelegate: self)
            emptyStateAddPhotoView?.frame = self.collectionView?.frame ?? CGRect(x: 0, y: 0, width: 0, height: 0)
            
            if emptyStateAddPhotoView != nil {
                self.collectionView?.addSubview(emptyStateAddPhotoView!)
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = CommonStrings.Dc.ProofOfPurchase.Title1501
        
        initToolBar()
        initCollectionView()
        imagePicker = SelectImageController(pickerDelegate: self)
        
        self.clearsSelectionOnViewWillAppear = false
    }
    
    /* This method will also update the imagesInfo in ProfileProvideFeedbackViewController */
    override func viewWillDisappear(_ animated: Bool) {
        if isMovingFromParentViewController {
            
            /* Pass the values of totalAttachments */
            delegate.persistImageDataList(totalImageDataAttachments: self.totalAttachments)
            self.navigationController?.setToolbarHidden(true, animated: false)
            delegate = nil
            
        }
    }
    
    /* Setup UI */
    
    func initToolBar() {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.barTintColor = .white
        self.navigationController?.makeNavigationBarTransparent()
        self.hideBackButtonTitle()
        
        selectBarButton = UIBarButtonItem(title: CommonStrings.NextButtonTitle84 , style: .done, target: self, action: #selector(showSummary))
        self.navigationItem.rightBarButtonItem = selectBarButton
        updateSelectBarButtonStatus()
    }
    
    func initCollectionView() {
        self.collectionView?.alwaysBounceVertical = true
        self.view.backgroundColor = UIColor.day()
        registerCollectionViewCells()
        setCollectionViewCellSize()
    }
    
    func registerCollectionViewCells() {
        self.collectionView!.register(ImageCollectionViewCell.nib(), forCellWithReuseIdentifier: ImageCollectionViewCell.defaultReuseIdentifier)
        self.collectionView!.register(LabelSupplimentaryView.nib(), forSupplementaryViewOfKind: UICollectionElementKindSectionFooter, withReuseIdentifier: "Footer")
    }
    
    func setCollectionViewCellSize() {
        let leftAndRightSpaceingTotal: CGFloat = 15
        let cellPaddingTotal: CGFloat = 15
        let totalWidthPadding: CGFloat = leftAndRightSpaceingTotal + cellPaddingTotal
        let itemWidth = (((self.collectionView?.frame.size.width) ?? totalWidthPadding) - totalWidthPadding) / 2
        self.selectPhotosFlowLayout.itemSize = CGSize(width: itemWidth,
                                                      height: itemWidth)
        self.selectPhotosFlowLayout.minimumLineSpacing = 10
    }
    
    // MARK: On Click Listener Functions
    
    func updateSelectBarButtonStatus() {
        if totalAttachments.count == 0 {
            selectBarButton?.isEnabled = false
        } else {
            selectBarButton?.isEnabled = true
        }
    }
    
    func showSummary() {
        /* Store all selected attachments on Realm */
        if self.totalAttachments.count != 0 {
            self.showHUDOnWindow()
            self.beginURLWrite(completion: {
                self.hideHUDFromWindow()
                self.performSegue(withIdentifier: "showSummary", sender: self)
            })
        }
    }
    
    func isCollectionEmpty() -> Bool {
        return self.totalAttachments.count == 0
    }
    
    func beginURLWrite(completion: @escaping () -> Void) {
        DispatchQueue.global(qos: .background).async {
            let realm = DataProvider.newGDCRealm()
            var realmAssets = Array<PhotoAsset>()
            
            for image in self.totalAttachments {
                let realmAsset = PhotoAsset()
                if let uiImageURL = image[UIImagePickerControllerReferenceURL] as? URL {
                    let photosAsset = PHAsset.fetchAssets(withALAssetURLs: [uiImageURL], options: nil).firstObject
                    let localIdentifier = photosAsset?.localIdentifier
                    realmAsset.assetURL = localIdentifier
                } else {
                    if let uiImage = image[UIImagePickerControllerOriginalImage] as? UIImage {
                        if let imageData = PhotoAsset.compress(image:uiImage) {
                            realmAsset.assetData = imageData
                        }
                    }
                }
                realmAssets.append(realmAsset)
            }
            
            try! realm.write {
                realm.delete(realm.allPhotosAssets())
                realm.add(realmAssets)
            }
            
            DispatchQueue.main.async {
                completion()
            }
        }
    }
    
    func updateEditSelectedAttachments(attachments: Array<Dictionary<String, Any>>){
        self.totalAttachments.removeAll()
        self.totalAttachments = attachments
        updateSelectBarButtonStatus()
        self.collectionView?.reloadData()
    }
    
    public func remove(imageInfo: [String: Any]) {
        if let pickedImage = imageInfo[UIImagePickerControllerOriginalImage] as? UIImage {
            var index = 0
            for helpImageInfo in self.totalAttachments {
                if let heldImage = helpImageInfo[UIImagePickerControllerOriginalImage] as? UIImage {
                    if (pickedImage.isEqual(heldImage)) {
                        self.totalAttachments.remove(at: index)
                    }
                }
                index += 1
            }
        }
        updateSelectBarButtonStatus()
    }
    
    public func add(imageInfo: [String : Any]) {
        self.totalAttachments.append(imageInfo)
        updateSelectBarButtonStatus()
    }
    
    func showAlertWhenLimitReached(okAction: (() -> Void)? = {}) {
        let controller = UIAlertController(title: CommonStrings.Proof.AttachmentsLimitMessageTitle2352,
                                           message: CommonStrings.Proof.AttachmentsLimitMessageBody2351,
                                           preferredStyle: .alert)
        
        let ok = UIAlertAction(title: CommonStrings.OkButtonTitle40, style: .default) { action in
            if let okAction = okAction {
                okAction()
            }
        }
        controller.addAction(ok)
        controller.view.tintColor = UIColor.genericErrorAlertButtonText()
        self.present(controller, animated: true, completion: nil)
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
//        if segue.identifier == "showEditProof" {
//            // Pass the updated imagesInfo to the SelectedPhotosCollectionViewController, for persistence
//            let profileEditSelectedPhotosCollectionViewController = segue.destination as! ProfileEditSelectedPhotosCollectionViewController
//            profileEditSelectedPhotosCollectionViewController.delegate = self
//            profileEditSelectedPhotosCollectionViewController.totalAttachments = self.totalAttachments
//        }
        
        guard let selectedImage = sender as? [String : Any] else {
            return
        }
        
        guard let detailImageViewController = segue.destination as? ImageDetailViewController else {
            return
        }
        
        detailImageViewController.set(selectedImage: selectedImage)
    }
    
    // MARK: UICollectionViewDataSource
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let addMoreImagesCellCount = 1
        var itemCount = self.totalAttachments.count
        if self.totalAttachments.count < 11 {
            itemCount += addMoreImagesCellCount
        }
        return itemCount
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ImageCollectionViewCell.defaultReuseIdentifier, for: indexPath) as? ImageCollectionViewCell
        cell?.imageView.image = nil
        cell?.resetCell()
        
        if (indexPath.item == self.totalAttachments.count) {
            let plusImage = VIADeviceCashbackAsset.ActivateDeviceCashback.Attachment.addPhoto.templateImage
            cell?.setSmall(image: plusImage)
            
            cell?.selectionClosure = {
                self.imagePicker?.showImageDirectly()
            }
        } else {
            if let pickedImage = self.totalAttachments[indexPath.row][UIImagePickerControllerOriginalImage] as? UIImage {
                cell?.imageView.image = pickedImage
                if self.totalAttachments.count == 5 {
                    self.showAlertWhenLimitReached()
                }
            }
        }
        guard let returnCell = cell else { return UICollectionViewCell() }
        return returnCell
    }
    
    // MARK: Set the content of Footer
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        var supplimentaryView: UICollectionReusableView = UICollectionReusableView()
        if (kind == UICollectionElementKindSectionFooter) {
            guard let footerView = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionFooter, withReuseIdentifier: "Footer", for: indexPath) as? LabelSupplimentaryView else {
                return UICollectionReusableView()
            }
            footerView.setView(text: CommonStrings.Proof.AttachmentsFootnoteMessage177(String(self.totalAttachments.count), "11")) // TODO: If the 5 should come from a service
            supplimentaryView = footerView
        }
        return supplimentaryView
    }
    
    
    // MARK: UICollectionViewDelegate
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: false)
        
        if (indexPath.item < self.totalAttachments.count) {
            let pickedImage = self.totalAttachments[indexPath.row]
            self.performSegue(withIdentifier: "showImageDetailView", sender: pickedImage)
        } else {
            if let cell = collectionView.cellForItem(at: indexPath) as? ImageCollectionViewCell {
                cell.selectionClosure()
            }
        }
    }
}

extension AddAttachmentCollectionViewController: ImagePickerDelegate {
    func pass(imageInfo: [String: Any]) {
        self.emptyStateAddPhotoView?.performSelector(onMainThread: #selector(self.emptyStateAddPhotoView?.removeFromSuperview), with: nil, waitUntilDone: false)
        
        if (self.navigationItem.rightBarButtonItem?.isEnabled == false) {
            self.navigationItem.rightBarButtonItem?.isEnabled = true
        }
        self.totalAttachments.append(imageInfo)
        
        self.collectionView?.reloadData()
        self.imagePicker?.dismissImagePicker()
        
        updateSelectBarButtonStatus()
    }
    
    func show(imageSouceSelector: UIAlertController) {
        self.present(imageSouceSelector, animated: true, completion: nil)
    }
    
    func present(imagePicker: UIImagePickerController) {
        self.present(imagePicker, animated: true, completion: nil)
    }
}

extension AddAttachmentCollectionViewController: AddPhotoViewDelegate {
    
    func showPhotoPicker() {
        self.imagePicker?.selectImage(view: emptyStateAddPhotoView?.getAddPhotoButton())
    }
}


