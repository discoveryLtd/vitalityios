//
//  SubmissionHelper.swift
//  VitalityActive
//
//  Created by Von Kervin R. Tuico on 16/04/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import VIAUIKit
import VIAUtilities
import VitalityKit
import Photos

public protocol PhotoUploadDelegate: class {
    func displayImageUploadErrorMessage(with completion: @escaping (_ resubmit: Bool) -> Void)
}

protocol SubmissionProcessor {
    func processDeviceCashbackActivation(completion: @escaping (_ error: Error?) -> Void)
}

class SubmissionHelper: SubmissionProcessor, PhotoUploaderDelegate {
    internal weak var photoUploadDelegate: PhotoUploadDelegate?
    internal let coreRealm = DataProvider.newRealm()
    internal let imagesRealm = DataProvider.newGDCRealm()
    internal let gdcRealm = DataProvider.newGDCRealm()
    internal let photoUploader = PhotoUploader()
    
    internal var summaryData: GDCPurchaseDetailSummary?
    
    private var completion: ((Error?) -> (Void)) = { error in
        debugPrint("Complete")
    }
    
    func setupDelegate(_ delegate: PhotoUploadDelegate) {
        photoUploadDelegate = delegate
    }
    
    func processDeviceCashbackActivation(completion: @escaping (_ error: Error?) -> Void) {
        self.completion = completion
        submitImagesRetreiveReferences()
    }
    
    func imagesUploaded(references: Array<String>) {
        registerProductPurchased(with: references)
    }
    
    internal func submitImagesRetreiveReferences() {
        guard let realmCapturedAssets = imagesRealm.allPhotos() else {
            debugPrint("No images to upload")
            return
        }
        
        photoUploader.delegate = self
        photoUploader.submitCapturedVHCImages(realmCapturedAssets)
        while !photoUploader.allImagesSuccesfullyUploaded() &&
            self.photoUploadDelegate != nil {
                self.photoUploadDelegate?.displayImageUploadErrorMessage(with: { [weak self] tryAgain in
                    if tryAgain {
                        self?.photoUploader.reSubmitFailedImages()
                    }
                })
        }
    }
    
    internal func registerProductPurchased(with associateReferences: Array<String>?) {
        
        /* */
        var amounts = [Amounts]()
        var parties = [Parties]()
        var purchaseItems = [PurchaseItems]()
        var purchaseItemAmounts = [PIAmounts]()
        var purchaseItemsPurchaseItemReferences = [PIPurchaseItemReferences]()
        var purchaseReferences = [PurchaseReferences]()
        
        /* Fetch Purchase Detail Items */
        let summaryItems = self.gdcRealm.allPurchaseDetailSummaryItems()
        for item in summaryItems {
            self.summaryData = item
            debugPrint("Stored Data: \(item)")
        }
        
        let tenantID = coreRealm.getTenantId()
        let partyID = coreRealm.getPartyId()
        
        guard let model = self.summaryData?.model else { return }
        guard let modelKey = self.summaryData?.modelDetails?.productKey else { return }
        guard var purchaseAmount = self.summaryData?.purchaseAmount else { return }
        guard let purchaseDate = self.summaryData?.purchaseDate else { return }
        guard let purchaseDateWithTimeZone = self.summaryData?.purchaseDateWithTimezoneAPI else { return }
        guard let invoiceNumber = self.summaryData?.invoiceNumber else { return }
        guard let deviceSerialNumber = self.summaryData?.deviceSerialNumber else { return }
        
        /* Amounts */
        let amountData = Amounts(typeCode: "TotalRetailAmount", typeKey: 1, typeName: "", value: String(purchaseAmount.characters.dropFirst()).toCurrencyISOFormat())
        amounts.append(amountData)
        
        /* Parties */
        let partyData = Parties(partyRoleTypeKey: 1, referenceTypeKey: 1, referenceValue: "\(partyID)")
        let supplierRolePartyData = Parties(partyRoleTypeKey: 2, referenceTypeKey: 11, referenceValue: "Vitality")
        parties.append(partyData)
        parties.append(supplierRolePartyData)
        
        /* Purchase Items Amount */
        let piAmountData = PIAmounts(typeCode: "RetailAmount", typeKey: 8, typeName: "", value: String(purchaseAmount.characters.dropFirst()).toCurrencyISOFormat())
        purchaseItemAmounts.append(piAmountData)
        
        /* Purchase Items Purchase Item References */
        let piInvoiceNumberData = PIPurchaseItemReferences(purchaseItemReference: invoiceNumber, typeKey: 0)
        let piDeviceSerialNumberData = PIPurchaseItemReferences(purchaseItemReference: deviceSerialNumber, typeKey: 0)
        purchaseItemsPurchaseItemReferences.append(piInvoiceNumberData)
        purchaseItemsPurchaseItemReferences.append(piDeviceSerialNumberData)
        
        /* Purchase Items */
        let purchaseItemData = PurchaseItems(actionOn: purchaseDate, amounts: purchaseItemAmounts, categoryKey: 0, description: model, instructionReference: 6, instructionTypeKey: 1, parties: [], productKey: modelKey, purchaseItemDates: [], purchaseItemDiscounts: [], purchaseItemId: 0, purchaseItemMetaDatas: [], purchaseItemReferences: purchaseItemsPurchaseItemReferences, qualifiesForDiscount: true, quantity: 1)
        purchaseItems.append(purchaseItemData)
        
        /* Purchase References */
        for reference in associateReferences! {
            let proofOfPurchase = PurchaseReferences(purchaseReference: reference, purchaseReferenceTypeKey: 0)
            purchaseReferences.append(proofOfPurchase)
        }
        
        let invoiceNumberData = PurchaseReferences(purchaseReference: invoiceNumber, purchaseReferenceTypeKey: 2)
        let deviceSerialNumberData  = PurchaseReferences(purchaseReference: deviceSerialNumber, purchaseReferenceTypeKey: 4)
        purchaseReferences.append(invoiceNumberData)
        purchaseReferences.append(deviceSerialNumberData)
        
        let purchaseDetails = PurchaseDetails(amounts: amounts, parties: parties, productKey: 35, purchaseId: 0, purchaseItems: purchaseItems, purchaseReferences: purchaseReferences)
        
        let request = RegisterProductPurchasedRequest(actionOn: purchaseDateWithTimeZone, instructionReference: 2, instructionTypeKey: 1, purchaseDetails: purchaseDetails)
        
        let parameters = RegisterProductPurchasedParameters(request)
        Wire.Events.registerProductPurchased(tenantId: tenantID, request: parameters) { (error) in
            if (error != nil) {
                self.completion(error)
            } else {
                self.completion(nil)
            }
        }
    }
}

