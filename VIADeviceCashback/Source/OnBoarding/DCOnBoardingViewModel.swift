//
//  DCOnBoardingViewModel.swift
//  VitalityActive
//
//  Created by Von Kervin R. Tuico on 22/03/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import VitalityKit
import VIAUIKit
import VIAUtilities

public class DCOnBoardingViewModel: AnyObject, OnboardingViewModel {
    public var heading: String {
        return CommonStrings.Dc.Onboarding.HeaderTitle2499
    }
    
    public var contentItems: [OnboardingContentItem] {
        var items = [OnboardingContentItem]()
        
        items.append(OnboardingContentItem(
            heading: CommonStrings.Dc.Onboarding.SectionTitle1458,
            content: CommonStrings.Dc.Onboarding.SectionMessage1459,
            image: UIImage(asset: VIADeviceCashbackAsset.DeviceCashback.Onboarding.getYourDevice)
        ))
        
        items.append(OnboardingContentItem(
            heading: CommonStrings.Dc.Onboarding.SectionTitle1460,
            content: CommonStrings.Dc.Onboarding.SectionMessage1461,
            image: UIImage(asset: VIADeviceCashbackAsset.DeviceCashback.Onboarding.getActive)
        ))
        
        items.append(OnboardingContentItem(
            heading: CommonStrings.Dc.Onboarding.SectionTitle1462,
            content: CommonStrings.Dc.Onboarding.SectionMessage1463,
            image: UIImage(asset: VIADeviceCashbackAsset.DeviceCashback.Onboarding.getCashback)
        ))
        
        return items
    }
    
    public var buttonTitle: String {
        return CommonStrings.Dc.Onboarding.GotIt131
    }
    
    public var alternateButtonTitle: String? {
        return CommonStrings.LearnMoreButtonTitle104
    }
    
    public var gradientColor: Color {
        return .blue
    }
    
    public var mainButtonHighlightedTextColor: UIColor? {
        return .improveYourHealthBlue()
    }
}
