//
//  DCOnBoardingViewController.swift
//  VitalityActive
//
//  Created by Von Kervin R. Tuico on 22/03/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import VIACommon
import VitalityKit
import VIAUIKit

class DCOnBoardingViewController: VIAOnboardingViewController {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.viewModel = DCOnBoardingViewModel()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initNavigationBar()
        VIAAppearance.setGlobalTintColorToImproveYourHealth()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.navigationBar.isHidden = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.hideBackButtonTitle()
    }
    
    override func mainButtonTapped(_ sender: UIButton) {
        AppSettings.setHasShownDeviceCashbackOnboarding()
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: Learn More button
    
    override func alternateButtonTapped(_ sender: UIButton) {
        self.performSegue(withIdentifier: "showLearnMore", sender: self)
    }
    
    func initNavigationBar() {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.hideBackButtonTitle()
        self.navigationItem.setHidesBackButton(true, animated:true);
    }
    
    public override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        if let vc = segue.destination as? SVLearnMoreViewController{
//            vc.fromOnboarding           = true
//            vc.landingCellsGroupData    = SAVLandingDataController().allScreeningsAndVaccinations()
//        }
    }
    
}
