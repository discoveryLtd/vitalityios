//
//  DCLearnMoreViewModel.swift
//  VitalityActive
//
//  Created by Steven Layug on 4/6/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import VIAUIKit
import VitalityKit
import VIACommon

class DCLearnMoreViewModel: AnyObject, VIALearnMoreViewModel {
    
    public var title: String {
        return CommonStrings.LearnMoreButtonTitle104
    }
    
    public var contentItems: [LearnMoreContentItem] {
        var items = [LearnMoreContentItem]()
        
        items.append(LearnMoreContentItem(
            header: CommonStrings.DeviceCashbackLearnMore.GroupHeader1472,
            content: CommonStrings.DeviceCashbackLearnMore.GroupMessage1473
        ))
        
        items.append(LearnMoreContentItem(
            header: CommonStrings.DeviceCashbackLearnMore.Section1Title1474,
            content: CommonStrings.DeviceCashbackLearnMore.Section1Message1475,
            image: UIImage(asset: VIADeviceCashbackAsset.DeviceCashback.LearnMore.learnMoreGetADevice)
        ))
        
        items.append(LearnMoreContentItem(
            header: CommonStrings.DeviceCashbackLearnMore.Section2Title1476,
            content: CommonStrings.DeviceCashbackLearnMore.Section2Message1477,
            image: UIImage(asset: VIADeviceCashbackAsset.DeviceCashback.LearnMore.learnMoreActivate)
        ))
        
        items.append(LearnMoreContentItem(
            header: CommonStrings.DeviceCashbackLearnMore.Section3Title1462,
            content: CommonStrings.DeviceCashbackLearnMore.Section3Message1568,
            image: UIImage(asset: VIADeviceCashbackAsset.DeviceCashback.LearnMore.learnMoreCashbacks)
        ))
            
        return items
    }
    
    public var imageTint: UIColor {
        return .knowYourHealthGreen()
    }
}
