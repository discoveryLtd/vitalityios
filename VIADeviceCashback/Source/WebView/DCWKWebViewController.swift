//
//  DCWKWebViewController.swift
//  VIADeviceCashback
//
//  Created by Val Tomol on 22/03/2019.
//  Copyright © 2019 Glucode. All rights reserved.
//

import Foundation
import WebKit

import VIAUIKit
import VIAUtilities
import VitalityKit
import VIACommon

public class DCWKWebViewController: VIAViewController, WKUIDelegate, WKNavigationDelegate {
    public var url: URL?
    public var commonAuthId: String?
    
    @IBOutlet var webView: WKWebView!
    
    let progressView = UIProgressView(progressViewStyle: .default)
    private var estimatedProgressObserver: NSKeyValueObservation?
    
    // MARK: View Lifecycles
    public override func viewDidLoad() {
        super.viewDidLoad()
        
        title = CommonStrings.Dc.Landing.ActionMainButton2488
        
        configureAppearance()
        configureWebView()
    }
    
    public override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        loadHTML(webView: webView, url: url)
    }
    
    public override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        progressView.isHidden = true
    }
    
    func configureAppearance() {
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.makeNavigationBarTransparent()
        
        hideBackButtonTitle()
    }
    
    func configureWebView() {
        let pref = WKPreferences()
        pref.javaScriptEnabled = true
        pref.javaScriptCanOpenWindowsAutomatically = true
        
        let configuration = WKWebViewConfiguration()
        configuration.preferences = pref
        
        webView = WKWebView(frame: .zero, configuration: configuration)
        webView.uiDelegate = self
        webView.navigationDelegate = self
        view = webView
        
        setupProgressView()
        setupEstimatedProgressObserver()
    }
    
    func setupProgressView() {
        guard let navigationBar = navigationController?.navigationBar else { return }
        
        progressView.translatesAutoresizingMaskIntoConstraints = false
        navigationBar.addSubview(progressView)
        
        progressView.isHidden = true
        
        NSLayoutConstraint.activate([
            progressView.leadingAnchor.constraint(equalTo: navigationBar.leadingAnchor),
            progressView.trailingAnchor.constraint(equalTo: navigationBar.trailingAnchor),
            progressView.bottomAnchor.constraint(equalTo: navigationBar.bottomAnchor),
            progressView.heightAnchor.constraint(equalToConstant: 2.0)
            ])
    }
    
    func setupEstimatedProgressObserver() {
        estimatedProgressObserver = webView.observe(\.estimatedProgress, options: [.new]) { [weak self] webView, _ in
            self?.progressView.progress = Float(webView.estimatedProgress)
        }
    }
}

extension DCWKWebViewController {
    fileprivate func loadHTML(webView: WKWebView, url: URL?) {
        if let url = url {
            print("GARMIN URL \(url.absoluteURL)")
            print("GARMIN COOKIE COMMONAUTHID \(String(describing: commonAuthId))")
            
            var urlRequest = URLRequest(url: url)
            urlRequest.httpShouldHandleCookies = true
            urlRequest.setValue(commonAuthId, forHTTPHeaderField: "Cookie")
            
            webView.load(urlRequest)
        }
    }
}

extension DCWKWebViewController {
    public func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        UIView.animate(withDuration: 0.33, animations: {
            self.progressView.alpha = 0.0
        }, completion: { isFinished in
            self.progressView.isHidden = isFinished
        })
    }
    
    public func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        if progressView.isHidden {
            progressView.isHidden = false
        }
        
        UIView.animate(withDuration: 0.33, animations: {
            self.progressView.alpha = 1.0
        })
    }
    
    public func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        UIView.animate(withDuration: 0.33, animations: {
            self.progressView.alpha = 0.0
        }, completion: { isFinished in
            self.progressView.isHidden = isFinished
        })
        
        webView.loadHTMLString(error.localizedDescription, baseURL: nil)
    }
}
