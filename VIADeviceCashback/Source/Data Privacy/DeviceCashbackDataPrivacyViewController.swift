//
//  DeviceCashbackDataPrivacyViewController.swift
//  VIADeviceCashback
//
//  Created by Val Tomol on 04/04/2019.
//  Copyright © 2019 Glucode. All rights reserved.
//

import UIKit
import VitalityKit
import VIAUIKit
import VIACommon
import VIAUtilities

class DeviceCashbackDataPrivacyViewController: CustomTermsConditionsViewController {
    // MARK: Properties
    var completion: () -> Void = {
        debugPrint("Lets detail screen know that user accepted the privacy policy")
    }
    
    // MARK: View Lifecycles
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // TODO: valtomol-BYOD Hardcoded for now.
        viewModel = CustomTermsConditionsViewModel(articleId: "byod-consent-privacy")
    }
    
    override func configureAppearance() {
        super.configureAppearance()
        navigationController?.makeNavigationBarInvisible()
        navigationController?.navigationBar.isHidden = true
    }
    
    // MARK: Action Buttons
    override func leftButtonTapped(_ sender: UIBarButtonItem?) {
        performSegue(withIdentifier: "unwindToDCLandingViewController", sender: nil)
    }
    
    override func rightButtonTapped(_ sender: UIBarButtonItem?) {
        performSegue(withIdentifier: "unwindToDCLandingViewController", sender: nil)
        
        self.completion()
        
        // TODO: valtomol-BYOD Discuss with David on how to handle the 'agree' part.
    }
}
