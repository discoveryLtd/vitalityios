//
//  DCHowToTrackCashbacksViewController.swift
//  VitalityActive
//
//  Created by Von Kervin R. Tuico on 26/03/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import VIAUIKit
import VitalityKit
import VIAUtilities

class DCHowToTrackCashbacksViewController: VIATableViewController {
    // MARK: View Lifecycles
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = CommonStrings.Dc.Landing.ActionMenuTitle1470
        initUI()
    }
    
    func initUI() {
        configureAppearance()
        configureTableView()
    }
    
    func configureAppearance() {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.makeNavigationBarTransparent()
        
        self.hideBackButtonTitle()
    }
    
    func configureTableView() {
        self.tableView.register(ECPointsTableViewCell.nib(), forCellReuseIdentifier: ECPointsTableViewCell.defaultReuseIdentifier)
        self.tableView.register(VIAGenericContentCell.nib(), forCellReuseIdentifier: VIAGenericContentCell.defaultReuseIdentifier)
        self.tableView.register(VIATableViewCell.nib(), forCellReuseIdentifier: VIATableViewCell.defaultReuseIdentifier)
        self.tableView.register(VIATableViewSectionHeaderFooterView.nib(), forHeaderFooterViewReuseIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier)
        self.tableView.estimatedRowHeight = 96
        self.tableView.separatorInset = UIEdgeInsets(top: 0, left: 52, bottom: 0, right: 0)
        self.tableView.backgroundColor = UIColor.white
        self.tableView.separatorColor = UIColor.white.withAlphaComponent(0.5)
    }

    // MARK: TableView Data source and Delegate
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let section = Sections(rawValue: section) else {
            return nil
        }
        
        if section == .pointsEarningActivities {
            return pointsEarningActivitiesView(on: section)
        } else if section == .accessRequirements {
            return accessRequirementsView(on: section)
        }
        
        return nil
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        guard let section = Sections(rawValue: section) else {
            return nil
        }
        
        if section == .header {
            return sectionFooterView(on: section)
        }
        
        return nil
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return Sections.allValues.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch Sections.allValues[section] {
        case .header:
            return 6
        case .pointsEarningActivities:
            return 2
        case .accessRequirements:
            return 0
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch Sections.allValues[indexPath.section] {
        case .header:
            return headerCell(at: indexPath)
        case .pointsEarningActivities:
            return pointsEarningActivitiesCell(at: indexPath)
        case .accessRequirements:
            break
        }
        
        return self.tableView.defaultTableViewCell()
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let section = Sections(rawValue: indexPath.section)
        
        if section == .header && indexPath.row != 0 {
            cell.separatorInset = UIEdgeInsetsMake(0, 0, 0, tableView.bounds.width)
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        let section = Sections(rawValue: section)
        
        if section == .header {
            return 0
        }
        
        return UITableViewAutomaticDimension
    }
    
    func headerCell(at indexPath: IndexPath) -> UITableViewCell {
        var cell: UITableViewCell?
        
        let headerRow = Header.allValues[indexPath.row]
        if headerRow == .mainHeader || headerRow == .subHeader {
            cell = self.tableView.dequeueReusableCell(withIdentifier: VIAGenericContentCell.defaultReuseIdentifier, for: indexPath)
            let headerView = cell as! VIAGenericContentCell
            
            headerView.header = headerRow.header() ?? ""
            headerView.content = headerRow.content() ?? ""
            headerView.customHeaderFont = UIFont.title1Font()
            headerView.customContentFont = UIFont.subheadlineFont()
        } else {
            cell = self.tableView.dequeueReusableCell(withIdentifier: ECPointsTableViewCell.defaultReuseIdentifier, for: indexPath)
            let headerCell = cell as! ECPointsTableViewCell
            
            headerCell.coinsUIImageView.image = coinImages[indexPath.row]
            headerCell.verticalDashLineUIImageView.image = VIADeviceCashbackAsset.HowToTrackCashbacks.verticalDashLine.image
            headerCell.setPercentageLabel = amountValueList[indexPath.row]
            headerCell.setReachPointsLabel = pointsList[indexPath.row]
            
            let lastRowIndex = self.tableView.numberOfRows(inSection: indexPath.section)
            if indexPath.row == lastRowIndex - 1 {
                headerCell.verticalDashLineUIImageView.isHidden = true
            } else {
                headerCell.verticalDashLineUIImageView.isHidden = false
            }
        }
        
        cell?.selectionStyle = .none
        
        return cell!
    }
    
    func pointsEarningActivitiesCell(at indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: VIATableViewCell.defaultReuseIdentifier, for: indexPath)
        let otherCell = cell as! VIATableViewCell
        let rowValues = PointsEarningActivities.allValues[indexPath.row]
        
        otherCell.labelText = rowValues.title()
        otherCell.cellImage = activityImages[indexPath.row]
        
        cell.selectionStyle = .none
        
        return cell
    }
    
    func sectionFooterView(on section: Sections) -> UIView? {
        let view = self.tableView.dequeueReusableHeaderFooterView(withIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier)
        let footerView = view as! VIATableViewSectionHeaderFooterView
        
        footerView.labelText = section.footerText()
        footerView.set(backgroundColor: self.tableView.backgroundColor ?? UIColor.white)
        
        return footerView
    }
    
    func pointsEarningActivitiesView(on section: Sections) -> UIView? {
        let view = self.tableView.dequeueReusableCell(withIdentifier: VIATableViewCell.defaultReuseIdentifier)
        let pointsEarningActivitiesView = view as! VIATableViewCell
        
        pointsEarningActivitiesView.labelText = section.title()
        pointsEarningActivitiesView.label.font = UIFont.title2Font()
        pointsEarningActivitiesView.cellImage = nil
        
        return pointsEarningActivitiesView
    }
    
    func accessRequirementsView(on section: Sections) -> UIView? {
        let view = self.tableView.dequeueReusableCell(withIdentifier: VIAGenericContentCell.defaultReuseIdentifier)
        let accessRequirementsView = view as! VIAGenericContentCell
        
        accessRequirementsView.header = section.header() ?? ""
        accessRequirementsView.content = section.content() ?? ""
        accessRequirementsView.customHeaderFont = UIFont.title1Font()
        accessRequirementsView.customContentFont = UIFont.subheadlineFont()
        
        return accessRequirementsView
    }
}
