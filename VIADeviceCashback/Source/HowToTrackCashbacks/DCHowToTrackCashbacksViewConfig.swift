//
//  DCHowToTrackCashbacksViewConfig.swift
//  VIADeviceCashback
//
//  Created by Val Tomol on 28/02/2019.
//  Copyright © 2019 Glucode. All rights reserved.
//

import Foundation
import VitalityKit
import VIAUtilities

extension DCHowToTrackCashbacksViewController {
    var coinImages: [UIImage] {
        get {
            return [UIImage(), UIImage(),VIADeviceCashbackAsset.HowToTrackCashbacks.pinkCoin.image,
                    VIADeviceCashbackAsset.HowToTrackCashbacks.orangeCoin.image,
                    VIADeviceCashbackAsset.HowToTrackCashbacks.blueCoin.image,
                    VIADeviceCashbackAsset.HowToTrackCashbacks.greenCoin.image]
        }
    }
    
    var amountValueList: [String] {
        get {
            return ["", "", CommonStrings.Dc.TrackCashbacks.PointsLevelAmount2517,
                    CommonStrings.Dc.TrackCashbacks.PointsLevelAmount2518,
                    CommonStrings.Dc.TrackCashbacks.PointsLevelAmount2519,
                    CommonStrings.Dc.TrackCashbacks.PointsLevelAmount2520]
        }
    }
    
    var pointsList: [String] {
        get {
            return ["", "", CommonStrings.Dc.TrackCashbacks.PointsSubtitle1482(CommonStrings.Dc.TrackCashbacks.PointsLevel2480),
                    CommonStrings.Dc.TrackCashbacks.PointsSubtitle1482(CommonStrings.Dc.TrackCashbacks.PointsLevel2481),
                    CommonStrings.Dc.TrackCashbacks.PointsSubtitle1482(CommonStrings.Dc.TrackCashbacks.PointsLevel2482),
                    CommonStrings.Dc.TrackCashbacks.PointsSubtitle1482(CommonStrings.Dc.TrackCashbacks.PointsLevel2483)]
        }
    }
    
    var activityImages: [UIImage] {
        get {
            return[VIADeviceCashbackAsset.HowToTrackCashbacks.heartRate.image,
                   VIADeviceCashbackAsset.HowToTrackCashbacks.steps.image]
        }
    }
    
    enum Sections: Int, EnumCollection, Equatable {
        case header = 0
        case pointsEarningActivities = 1
        case accessRequirements = 2
        
        func title() -> String? {
            switch self {
            case .pointsEarningActivities:
                return CommonStrings.Dc.TrackCashbacks.SectionTitle1484
            case .accessRequirements:
                return CommonStrings.Dc.TrackCashbacks.SectionTitle1485
            default:
                return ""
            }
        }
        
        func footerText() -> String? {
            switch self {
            case .header:
                return CommonStrings.Dc.Landing.FooterMessage2498
            default:
                return ""
            }
        }
        
        func header() -> String? {
            switch self {
            case .accessRequirements:
                return CommonStrings.Dc.TrackCashbacks.SectionTitle1485
            default:
                return ""
            }
        }
        
        func content() -> String? {
            switch self {
            case .accessRequirements:
                return CommonStrings.Dc.TrackCashbacks.SectionContent1486
            default:
                return ""
            }
        }
    }
    
    enum Header: Int, EnumCollection, Equatable {
        case mainHeader = 0
        case subHeader = 1
        case tier0 = 2
        case tier1 = 3
        case tier2 = 4
        case tier3 = 5
        
        func header() -> String? {
            switch self {
            case .mainHeader:
                return CommonStrings.Dc.TrackCashbacks.HeaderTitle2492
            case .subHeader:
                return CommonStrings.Dc.TrackCashbacks.SubHeaderTitle2496
            default:
                return ""
            }
        }
        
        func content() -> String? {
            switch self {
            case .mainHeader:
                return CommonStrings.Dc.TrackCashbacks.HeaderMessage1480
            case .subHeader:
                return CommonStrings.Dc.TrackCashbacks.SubHeaderMessage2497
            default:
                return ""
            }
        }
    }
    
    enum PointsEarningActivities: Int, EnumCollection, Equatable {
        case steps = 0
        case heartRate = 1
        
        func title() -> String? {
            switch self {
            case .steps:
                return CommonStrings.PhysicalActivity.ItemStepsText442
            case .heartRate:
                return CommonStrings.PhysicalActivity.ItemHeartRateText439
            }
        }
    }
}
