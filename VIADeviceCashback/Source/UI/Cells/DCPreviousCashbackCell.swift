//
//  DCPreviousCashbackCell.swift
//  VitalityActive
//
//  Created by Steven Layug on 4/17/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import UIKit
import VIAUIKit

public class DCPreviousCashbackCell: UITableViewCell, Nibloadable {
    
    @IBOutlet var deviceLabel: UILabel!
    public var deviceLabelText: String? {
        set {
            self.deviceLabel.text = newValue
        }
        get {
            return self.deviceLabel.text
        }
    }
    
    @IBOutlet weak var arrowImageView: UIImageView!
    public var arrowImage: UIImage? {
        set {
            self.arrowImageView.image = newValue
        }
        get {
            return self.arrowImageView.image
        }
    }
    
    @IBOutlet var cashbackEarnedLabel: UILabel!
    public var cashbackEarnedLabelText: String? {
        set {
            self.cashbackEarnedLabel.text = newValue
        }
        get {
            return self.cashbackEarnedLabel.text
        }
    }
    
    @IBOutlet var dateRangeLabel: UILabel!
    public var dateRangeLabelText: String? {
        set {
            self.dateRangeLabel.text = newValue
        }
        get {
            return self.dateRangeLabel.text
        }
    }
    
    override public func awakeFromNib() {
        super.awakeFromNib()
        self.arrowImage = UIImage(asset: VIADeviceCashbackAsset.Cells.arrowDrill)
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        let labelFrame = self.convert(deviceLabel.frame, from: deviceLabel.superview)
        self.separatorInset = UIEdgeInsets(top: 0, left: labelFrame.origin.x, bottom: 0, right: 0)
    }
}
