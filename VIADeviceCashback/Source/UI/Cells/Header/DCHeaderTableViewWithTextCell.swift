//
//  DCHeaderTableViewWithTextCell
//  VitalityActive
//
//  Created by Steven F. Layug on 4/12/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import VIAUIKit

class DCHeaderTableViewWithTextCell: UITableViewCell, Nibloadable {
    
    
    @IBOutlet weak var headerUIImageView: UIImageView!
    
    @IBOutlet weak var headerTitleLabel: UILabel!
    public var setHeaderTitle: String? {
        set {
            self.headerTitleLabel.text = newValue
        }
        get {
            return self.headerTitleLabel.text
        }
    }
    
    @IBOutlet weak var headerTextLabel: UILabel!
    public var setHeaderText: String? {
        set {
            self.headerTextLabel.text = newValue
        }
        get {
            return self.headerTextLabel.text
        }
    }
    
    @IBOutlet weak var headerSubTextLabel: UILabel!
    public var setHeaderSubText: String? {
        set {
            self.headerSubTextLabel.text = newValue
        }
        get {
            return self.headerSubTextLabel.text
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
//        headerTitleLabel.text = nil
//        headerTitleLabel.font = UIFont.headlineFont()
//        headerTitleLabel.textColor = UIColor.night()
        
        headerTextLabel.text = nil
        headerTextLabel.font = UIFont.subheadlineFont()
        headerTextLabel.textColor = UIColor.darkGrey()
        
        headerSubTextLabel.tintColor = UIColor.currentGlobalTintColor()
    }

}
