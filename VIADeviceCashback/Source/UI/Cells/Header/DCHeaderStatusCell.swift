//
//  DCHeaderStatusCell.swift
//  VIADeviceCashback
//
//  Created by Val Tomol on 04/03/2019.
//  Copyright © 2019 Glucode. All rights reserved.
//

import VIAUIKit

class DCHeaderStatusCell: UITableViewCell, Nibloadable {
    @IBOutlet weak var monthCountLabel: UILabel!
    @IBOutlet weak var monthLabel: UILabel!
    
    public var monthCountLabelText: String? {
        set {
            self.monthCountLabel.text = newValue
        }
        get {
            return self.monthCountLabel.text
        }
    }
    
    public var monthLabelText: String? {
        set {
            self.monthLabel.text = newValue
        }
        get {
            return self.monthLabel.text
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        monthCountLabel.text = nil
        monthCountLabel.font = UIFont.subheadlineFont()
        monthCountLabel.textColor = UIColor.darkGrey()
        
        monthLabel.text = nil
        monthLabel.font = UIFont.title3Font()
        monthLabel.textColor = UIColor.night()
    }
}
