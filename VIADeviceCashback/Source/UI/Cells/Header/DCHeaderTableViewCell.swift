//
//  DCHeaderTableViewCell.swift
//  VitalityActive
//
//  Created by Von Kervin R. Tuico on 22/03/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import VIAUIKit

class DCHeaderTableViewCell: UITableViewCell, Nibloadable {
    
    
    @IBOutlet weak var headerUIImageView: UIImageView!
    
    @IBOutlet weak var headerTitleLabel: UILabel!
    public var setHeaderTitle: String? {
        set {
            self.headerTitleLabel.text = newValue
        }
        get {
            return self.headerTitleLabel.text
        }
    }
    
    @IBOutlet weak var headerDescriptionLabel: UILabel!
    public var setHeaderDescription: String? {
        set {
            self.headerDescriptionLabel.text = newValue
        }
        get {
            return self.headerDescriptionLabel.text
        }
    }
    
    @IBOutlet weak var headerButton: VIAButton!
    public var setHeaderButtonTitle: String? {
        set {
            self.headerButton.setTitle(newValue, for: .normal)
        }
        get {
            return self.headerButton.currentTitle
        }
    }
    
    @IBOutlet weak var subButton: VIAButton!
    public var setSubButtonTitle: String? {
        set {
            self.subButton.setTitle(newValue, for: .normal)
        }
        get {
            return self.subButton.currentTitle
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        headerTitleLabel.text = nil
        headerTitleLabel.font = UIFont.title3Font()
        headerTitleLabel.textColor = UIColor.night()
        
        headerDescriptionLabel.text = nil
        headerDescriptionLabel.font = UIFont.subheadlineFont()
        headerDescriptionLabel.textColor = UIColor.darkGrey()
        
        headerButton.tintColor = UIColor.currentGlobalTintColor()
        headerButton.setTitle(nil, for: .normal)
        headerButton.titleLabel?.font = UIFont.bodyFont()
        headerButton.isHidden = false
        
        subButton.tintColor = UIColor.currentGlobalTintColor()
        subButton.setTitle(nil, for: .normal)
        subButton.titleLabel?.font = UIFont.bodyFont()
        subButton.isHidden = false
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    func initDCHeaderCell() {
        
    }

}
