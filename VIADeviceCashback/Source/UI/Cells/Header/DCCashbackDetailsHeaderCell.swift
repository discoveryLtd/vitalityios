//
//  DCCashbackDetailsHeaderCell.swift
//  VitalityActive
//
//  Created by Steven Layug on 4/16/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import VIAUIKit

class DCCashbackDetailsHeaderCell: UITableViewCell, Nibloadable {

    @IBOutlet weak var headerUIImageView: UIImageView!
    public var headerUIImage: UIImage? {
        set {
            self.headerUIImageView.image = newValue
        }
        get {
            return self.headerUIImageView.image
        }
    }
    
    @IBOutlet weak var headerTitleLabel: UILabel!
    public var setHeaderTitle: String? {
        set {
            self.headerTitleLabel.text = newValue
        }
        get {
            return self.headerTitleLabel.text
        }
    }
    
    @IBOutlet weak var headerTextLabel: UILabel!
    public var setHeaderText: String? {
        set {
            self.headerTextLabel.text = newValue
        }
        get {
            return self.headerTextLabel.text
        }
    }
    
    @IBOutlet weak var headerSubTextLabel: UILabel!
    public var setHeaderSubText: String? {
        set {
            self.headerSubTextLabel.text = newValue
        }
        get {
            return self.headerSubTextLabel.text
        }
    }

}
