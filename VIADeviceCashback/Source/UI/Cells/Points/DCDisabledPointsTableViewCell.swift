//
//  DCDisabledPointsTableViewCell.swift
//  VitalityActive
//
//  Created by Von Kervin R. Tuico on 10/04/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import VIAUIKit

class DCDisabledPointsTableViewCell: UITableViewCell, Nibloadable {
    
    @IBOutlet weak var coinsUIImageView: UIImageView!
    @IBOutlet weak var verticalDashLineUIImageView: UIImageView!
    
    @IBOutlet weak var percentageLabel: UILabel!
    public var setPercentageLabel: String? {
        set {
            self.percentageLabel.text = newValue
        }
        get {
            return self.percentageLabel.text
        }
    }
    
    @IBOutlet weak var reachPointsLabel: UILabel!
    public var setReachPointsLabel: String? {
        set {
            self.reachPointsLabel.text = newValue
        }
        get {
            return self.reachPointsLabel.text
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        initCellUI()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func initCellUI() {
        percentageLabel.text = nil
        percentageLabel.font = UIFont.headlineFont()
        percentageLabel.textColor = UIColor.darkGrey()
        
        reachPointsLabel.text = nil
        reachPointsLabel.font = UIFont.subheadlineFont()
        reachPointsLabel.textColor = UIColor.darkGrey()
    }
}
