//
//  ECPointsTableViewCell.swift
//  VIADeviceCashback
//
//  Created by Val Tomol on 04/03/2019.
//  Copyright © 2019 Glucode. All rights reserved.
//

import VIAUIKit

class ECPointsTableViewCell: UITableViewCell, Nibloadable {
    
    @IBOutlet weak var coinsUIImageView: UIImageView!
    @IBOutlet weak var verticalDashLineUIImageView: UIImageView!
    
    @IBOutlet weak var percentageLabel: UILabel!
    public var setPercentageLabel: String? {
        set {
            self.percentageLabel.text = newValue
        }
        get {
            return self.percentageLabel.text
        }
    }
    
    @IBOutlet weak var reachPointsLabel: UILabel!
    public var setReachPointsLabel: String? {
        set {
            self.reachPointsLabel.text = newValue
        }
        get {
            return self.reachPointsLabel.text
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        initCellUI()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func initCellUI() {
        percentageLabel.text = nil
        percentageLabel.font = UIFont.headlineFont()
        percentageLabel.textColor = UIColor.night()
        
        reachPointsLabel.text = nil
        reachPointsLabel.font = UIFont.subheadlineFont()
        reachPointsLabel.textColor = UIColor.night()
    }
}
