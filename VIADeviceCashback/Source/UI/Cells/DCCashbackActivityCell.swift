//
//  DCCashbackActivityCell.swift
//  VitalityActive
//
//  Created by Steven Layug on 4/16/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import VIAUIKit

class DCCashbackActivityCell: UITableViewCell, Nibloadable {
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBOutlet var stepsLabel: UILabel!
    public var stepsLabelText: String? {
        set {
            self.stepsLabel.text = newValue
        }
        get {
            return self.stepsLabel.text
        }
    }
    
    @IBOutlet var deviceLabel: UILabel!
    public var deviceLabelText: String? {
        set {
            self.deviceLabel.text = newValue
        }
        get {
            return self.deviceLabel.text
        }
    }
    
    @IBOutlet var pointsLabel: UILabel!
    public var pointsLabelText: String? {
        set {
            self.pointsLabel.text = newValue
        }
        get {
            return self.pointsLabel.text
        }
    }
    
    @IBOutlet var dateLabel: UILabel!
    public var dateLabelText: String? {
        set {
            self.dateLabel.text = newValue
        }
        get {
            return self.dateLabel.text
        }
    }
}
