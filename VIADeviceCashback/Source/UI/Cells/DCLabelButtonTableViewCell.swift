//
//  VIAGenericLabelButtonTableViewCell.swift
//  VitalityActive
//
//  Created by Von Kervin R. Tuico on 23/04/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import VIAUIKit

class DCLabelButtonTableViewCell: UITableViewCell, Nibloadable {
    
    @IBOutlet weak var titleLabel: UILabel!
    public var setTitle: String? {
        set {
            self.titleLabel.text = newValue
        }
        get {
            return self.titleLabel.text
        }
    }
    
    @IBOutlet weak var button: VIAButton!
    public var setButtonTitle: String? {
        set {
            self.button.setTitle(newValue, for: .normal)
        }
        get {
            return self.button.currentTitle
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        titleLabel.text = nil
        titleLabel.font = UIFont.headlineFont()
        titleLabel.textColor = UIColor.night()
        
        button.tintColor = UIColor.currentGlobalTintColor()
        button.setTitle(nil, for: .normal)
        button.titleLabel?.font = UIFont.bodyFont()
        button.isHidden = false
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
}
