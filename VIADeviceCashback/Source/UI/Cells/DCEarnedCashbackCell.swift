//
//  VIAARAvailableRewardCell.swift
//  VitalityActive
//
//  Created by Wilmar van Heerden on 2016/11/16.
//  Copyright © 2016 Glucode. All rights reserved.
//

import UIKit
import VIAUIKit

public class DCEarnedCashbackCell: UITableViewCell, Nibloadable {

    @IBOutlet var cellImageView: UIImageView!
    public var cellImage: UIImage? {
        set {
            self.cellImageView.image = newValue
        }
        get {
            return self.cellImageView.image
        }
    }

    @IBOutlet var cashValueLabel: UILabel!
    public var cashValueLabelText: String? {
        set {
            self.cashValueLabel.text = newValue
        }
        get {
            return self.cashValueLabel.text
        }
    }

    @IBOutlet weak var arrowImageView: UIImageView!
    public var arrowImage: UIImage? {
        set {
            self.arrowImageView.image = newValue
        }
        get {
            return self.arrowImageView.image
        }
    }
    
    @IBOutlet var statusLabel: UILabel!
    public var statusLabelText: String? {
        set {
            self.statusLabel.text = newValue
        }
        get {
            return self.statusLabel.text
        }
    }

    @IBOutlet var pointsLabel: UILabel!
    public var pointsLabelText: String? {
        set {
            self.pointsLabel.text = newValue
        }
        get {
            return self.pointsLabel.text
        }
    }
    
    @IBOutlet var dateLabel: UILabel!
    public var dateLabelText: String? {
        set {
            self.dateLabel.text = newValue
        }
        get {
            return self.dateLabel.text
        }
    }
    
    override public func awakeFromNib() {
        super.awakeFromNib()
        self.arrowImage = UIImage(asset: VIADeviceCashbackAsset.Cells.arrowDrill)
    }

	public override func layoutSubviews() {
		super.layoutSubviews()
		let labelFrame = self.convert(cashValueLabel.frame, from: cashValueLabel.superview)
		self.separatorInset = UIEdgeInsets(top: 0, left: labelFrame.origin.x, bottom: 0, right: 0)
	}
}
