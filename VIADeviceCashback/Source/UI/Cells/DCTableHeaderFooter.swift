//
//  DCTableHeaderFooter.swift
//  VitalityActive
//
//  Created by Steven Layug on 4/16/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import UIKit
import VIAUIKit

public class DCTableHeaderFooter: UITableViewHeaderFooterView, Nibloadable {
  
    @IBOutlet weak var firstLabel: UILabel!
    public var firstLabelText: String? {
        set {
            self.firstLabel.text = newValue
        }
        get {
            return self.firstLabel.text
        }
    }
    @IBOutlet weak var secondLabel: UILabel!
    public var secondLabelText: String? {
        set {
            self.secondLabel.text = newValue
        }
        get {
            return self.secondLabel.text
        }
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
//        self.separatorInset = UIEdgeInsets(top: 0, left: labelFrame.origin.x, bottom: 0, right: 0)
    }
}
