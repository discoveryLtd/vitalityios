//
//  EmailViewModel.swift
//  VitalityActive
//
//  Created by Joshua Ryan on 7/14/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation

class EmailViewModel: EditableProfileFieldModel {

	var address: String = ""
	var typeKey: Int = 0
	var typeName: String = ""

	var existingValue: String {
		get {
			return address
		}
	}
	var updatedValue: String = ""

	var type: EditProfileFieldType {
		get {
			return .Email
		}
	}
}
