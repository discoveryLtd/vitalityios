//
//  UploadProfileImageHelper.swift
//  VIACore
//
//  Created by OJ Garde on 8/14/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation

import VitalityKit

public class ProfileImageHelper{
    
    public class func upload(partyID: Int, data: Data, completion: @escaping (_ success:Bool)->Void){
        Wire.Content.uploadProfileImage(partyId: partyID, fileContents: data, completion: { (fileUploadReference, error) in
            completion(error != nil)
        })
    }
    
    public class func getFile(partyId: Int, completion: ((_: UIImage?) -> ())?){
        guard let imageURL = AppSettings.getProfileImageUrl() else {
            retrieveImage(partyId: partyId, completion: completion)
            return
        }
        guard let data = FileManager.default.contents(atPath: imageURL) else {
            retrieveImage(partyId: partyId, completion: completion)
            return
        }
        guard let image = UIImage(data: data) else {
            retrieveImage(partyId: partyId, completion: completion)
            return
        }
        debugPrint("Profile image retrieved from cache")
        completion?(image)
    }
    
    private class func retrieveImage(partyId: Int, completion: ((_: UIImage?) -> ())?){
        let fileName = "\(partyId)_profile.jpg"
        Wire.Content.getFileByPartyId(partyId: partyId) { (data, error) in
            if nil == error{
                saveImage(fileName: fileName, data: data, completion: completion)
            }else{
                completion?(nil)
            }
        }
    }
    
    private class func saveImage(fileName: String, data: Data?, completion: ((_:UIImage?) -> ())?){
        guard let imageData = data, let image = UIImage(data: imageData) else { return }
        if var fileURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            fileURL.appendPathComponent("\(fileName)")
            
            do {
                if let jpgImageData = UIImageJPEGRepresentation(image, 1.0) {
                    try jpgImageData.write(to: fileURL, options: .atomic)
                    AppSettings.setProfileImage(url: fileURL.path)
                    completion?(image)
                }else{
                    completion?(nil)
                }
            } catch {
                completion?(nil)
            }
        }
    }
}
