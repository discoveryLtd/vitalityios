import AVFoundation
import Photos

import VitalityKit
import VIAUIKit
import VIAUtilities
import VIACommon
import VIAImageManager


protocol EditProfileDelegate: VIAFlowDelegate {
    
    func selectField(field: EditableProfileFieldModel)
}

class EditProfileViewController: VIACoordinatedTableViewController, PrimaryColorTintable {
    
    // MARK: Public (internal)
    private var flowController = ProfileFlowCoordinator()
    var viewModel: ProfileViewModel?
    
    // MARK: fileprivate
    
    fileprivate var imagePicker: VIASelectImageController?
    
    fileprivate func getImageConfiguration() -> UIImagePickerController.ImageControllerConfiguration{
        /* Get Market's configuration. */
        if let config = VIAApplicableFeatures.default.getImageControllerConfiguration() as? UIImagePickerController.ImageControllerConfiguration{            
            return config.initWith(allowCropping: true)
        }
        /* Else, use default configuration. */
        return UIImagePickerController.ImageControllerConfiguration()
    }
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        
        self.configureTableView()
        
        imagePicker = VIASelectImageController(pickerDelegate: self, configuration: getImageConfiguration())
        flowController.configureController(vc: self)
    }
    
    override public func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.title = CommonStrings.Settings.ProfileLandingPersonalDetailsTitle912
        self.configureAppearance()

    }
    
    override public func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.tableView.reloadData()
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return Sections.allValues.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return defineRowsInSection(section)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Let's check the identifier first
        if segue.identifier == "showEditEntityNumber" {
            prepareEditEntityNumber(segue)
        }
        
        if segue.identifier == "showEditEmail" {
            prepareEditEmailSegue(segue)
        }
    }
}

// MARK: Segue Helpers
extension EditProfileViewController{
    
    fileprivate func prepareEditEntityNumber(_ segue: UIStoryboardSegue) {
        if let destination = segue.destination as? EditEntityNumberViewController, let data = viewModel?.currentPerson {
            for reference in data.references {
                if reference.type == String(PartyReferenceTypeRef.EntityNumber.rawValue) {
                    destination.currentEntityNumber = reference.value
                    break
                } else {
                    destination.currentEntityNumber = ""
                }
            }
        }
    }
    
    fileprivate func prepareEditEmailSegue(_ segue: UIStoryboardSegue) {
        
        // Verify if the destination view controller is for EditMailViewController
        // Validate the current person view model
        // Validate if email address exists
        if let destination = segue.destination as? EditEmailViewController {
            let email = AppSettings.getLastLoginUsername()
            destination.currentEmail = email
        }
        
    }
}


// MARK: UITableViewDataSource
extension EditProfileViewController{
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if isBasicInfoSection(indexPath.section) {
            return configureBasicCell(tableView, cellForRowAt: indexPath)
        } else if isPersonalDetailsSection(indexPath.section){
            return configureDetailCell(tableView, cellForRowAt: indexPath)
        }else if isAccountDetailsSection(indexPath.section){
            return configureAccountDetailsViewCell(tableView, cellForRowAt: indexPath)
        }else{
            return UITableViewCell()
        }
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return viewForHeaderInSection(tableView, section: section)
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return viewForFooterInSection(tableView, section: section)
    }
}


// MARK: UITableViewDelegate
extension EditProfileViewController{
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        //Just change this later if we have more than 2 options.
        return !isPersonalDetailsSection(section) ? CGFloat.leastNormalMagnitude : UITableViewAutomaticDimension
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        //Just change this later if we have more than 2 options.
        return isBasicInfoSection(section) ? CGFloat.leastNormalMagnitude : UITableViewAutomaticDimension
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if let section = Sections(rawValue: indexPath.section),
            let row = SettingsItems(rawValue: indexPath.row),
            let showMobileNumber = VIAApplicableFeatures.default.enableMobileNumber{
            if section == .PersonalDetails && row == .Mobile && !showMobileNumber{
                return 0
            }
        }
        if let section = Sections(rawValue: indexPath.section),
            let row = AccountDetailItems(rawValue: indexPath.row),
            let showEntityNumber = VIAApplicableFeatures.default.enableEntityNumber{
            if section == .AccountDetails && row == .Entity && !showEntityNumber{
                return 0
            }
        }
        return UITableViewAutomaticDimension
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let section = Sections(rawValue: indexPath.section),
            let accountDetailsRow = AccountDetailItems(rawValue: indexPath.row){
            switch(section){
            case .BasicInfo: break
            case .PersonalDetails: break
            case .AccountDetails:
                switch(accountDetailsRow){
                case .Entity:
                    debugPrint("[EditProfileViewController] Entity row was selected.")
                    self.performSegue(withIdentifier: "showEditEntityNumber", sender: nil)
                    break
                case .Email:
                    debugPrint("[EditProfileViewController] Email row was selected.")
                    self.performSegue(withIdentifier: "showEditEmail", sender: nil)
                    break
                }
            }
        }
    }
}

// MARK: Table Helpers
extension EditProfileViewController{
    
    func defineRowsInSection(_ section:Int) -> Int{
        let section = Sections(rawValue: section)
        
        if section == .BasicInfo{
            return 1
        }else if section == .PersonalDetails {
            return SettingsItems.allValues.count
        }else if section == .AccountDetails{
            return isUpdateEmailEnable() ? AccountDetailItems.allValues.count : AccountDetailItems.allValues.count - 1
        }
        return 0
    }
    
    func viewForHeaderInSection(_ tableView: UITableView, section:Int) -> UIView?{
        //If this is a basic info, collapse the header
        if !isBasicInfoSection(section) {
            let view = getHeaderFooterView().addPadding(labelText: isPersonalDetailsSection(section) ? title : nil, bottom: 0)
            view.set(font: .title3Font())
            view.set(textColor: .night())
            
            return view
        }
        
        return nil
    }
    
    func viewForFooterInSection(_ tableView: UITableView, section:Int) -> UIView?{
        //If this is a basic info, collapse the footer
        //If the section is personal details, use text for insurance. If not, use email footer
        return isBasicInfoSection(section) ? nil : getHeaderFooterView().addPadding(labelText: isPersonalDetailsSection(section) ? CommonStrings.Settings.ProfileLandingDetailsFootnoteMessage918 : isUpdateEmailEnable() ? CommonStrings.Settings.ProfileLandingEmailFootnote920 : "", top: 0)
    }
    
    //Configuration for Detail Cell
    func configureDetailCell(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //Create the Profile Detail cell
        let cell    = getProfileDetailCell(indexPath)
        
        //Check for value of current person if not null,
        //Also let's check if we have a valid value for the SettingsItems
        if let person = viewModel?.currentPerson, let row = SettingsItems(rawValue: indexPath.row){
            switch(row){
            case .Mobile:
                if let showMobileNumber = VIAApplicableFeatures.default.enableMobileNumber,
                    showMobileNumber{
                    configureMobileViewCell(cell, data: person)
                    cell.labelText = row.title()
                }
                break
            case .DOB:
                configureDateOfBirthViewCell(cell, data: person)
                cell.labelText = row.title()
                break
            case .Gender:
                cell.valueText = person.gender
                cell.labelText = row.title()
                break
            }
        }
        return cell
    }
    
    private func configureDateOfBirthViewCell(_ cell:VIAProfileDetailCell, data:PersonViewModel){
        //Let's check if we have a valid data for date of birth
        if let dob = data.dob {
            let dateFormatter = DateFormatter()
            let formattedDate: String
            
            if Locale.current.languageCode == "ja" {
                dateFormatter.locale = Locale(identifier: "ja_JP")
                dateFormatter.dateStyle = .long
                formattedDate = dateFormatter.string(from: dob)
            } else {
                dateFormatter.dateFormat = "MMMM dd, yyyy"
                formattedDate = dateFormatter.string(from: dob)
            }
            
            cell.valueText = formattedDate

        }
    }
    
    private func configureMobileViewCell(_ cell:VIAProfileDetailCell, data:PersonViewModel){
        //Let's check if we have a valid data for phone number
        
        if let phone = data.phoneNumbers.last {
            cell.valueText = phone.display
        } else {
            cell.noValueText = CommonStrings.Settings.ProfileLandingMobileTitle915
        }
    }
    
    // Configuration for Email Cell
    func configureAccountDetailsViewCell(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        //Create the Profile Detail cell
        let cell    = getProfileDetailCell(indexPath)
        
        //Check for value of current person if not null,
        //Also let's check if we have a valid value for the SettingsItems
        if let data = viewModel?.currentPerson, let row = AccountDetailItems(rawValue: indexPath.row){
            //Let's check if we have a valid data for email
            switch row {
            case .Entity:
                self.clearAccountDetails(cell)
                if let showEntityNumber = VIAApplicableFeatures.default.enableEntityNumber,
                    showEntityNumber {
                    configureEntityNumberViewCell(cell, data: data)
                }
                break
            case .Email:
                self.clearAccountDetails(cell)
                if isUpdateEmailEnable(){
                    configureEmailViewCell(cell, data: data)
                }
                break
            }
        }
        
        return cell
    }
    
    private func configureEntityNumberViewCell(_ cell:VIAProfileDetailCell, data:PersonViewModel){
        for reference in data.references {
            if reference.type == String(PartyReferenceTypeRef.EntityNumber.rawValue) {
                cell.valueText = reference.value
                break
            } else {
                cell.noValueText = CommonStrings.EntityNumberFieldPlaceholder1103
            }
        }
        
        cell.labelText = CommonStrings.Activate.EntityNumber358
    }
    
    private func configureEmailViewCell(_ cell:VIAProfileDetailCell, data:PersonViewModel){

        let email = AppSettings.getLastLoginUsername()
        if !(email.isEmpty) {
            cell.valueText = email
            cell.accessoryType = .disclosureIndicator
        } else {
            cell.noValueText = CommonStrings.ProfileMailPlaceholder
        }
        cell.labelText = CommonStrings.Settings.ProfileLandingEmailTitle919
    }
    
    
    // Configuration for Basic Cell
    func configureBasicCell(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = getProfileBasicCell(indexPath)
        
        if let person = viewModel?.currentPerson {
            cell.name = "\(person.givenName) \(person.familyName)"
            
            cell.editClosure = { [weak self] in
                self?.showAvatarPrompt(view: cell.getEditButton())
            }
            
            cell.selectionStyle = .none
            
            if let avatarPath = person.avatarPath, FileManager.default.fileExists(atPath: avatarPath) {
                if let data = FileManager.default.contents(atPath: avatarPath) {
                    cell.photo = UIImage(data: data)
                }
            }else if let imageURL = AppSettings.getProfileImageUrl(){

                if let data = FileManager.default.contents(atPath: imageURL) {
                    cell.photo = UIImage(data: data)
                }
            }
        }
        
        cell.layoutMargins = .zero
        cell.separatorInset = UIEdgeInsetsMake(0, self.tableView.frame.size.width, 0, 0)
        
        return cell
    }
    
    private func getHeaderFooterView() -> VIATableViewSectionHeaderFooterView{
        return tableView.dequeueReusableHeaderFooterView(withIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier) as! VIATableViewSectionHeaderFooterView
    }
    
    private func getProfileBasicCell(_ indexPath: IndexPath, selectionStyle: UITableViewCellSelectionStyle = .none,
                                     accessoryType:UITableViewCellAccessoryType = .none) -> VIAUserBasicInfoLargeCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: VIAUserBasicInfoLargeCell.defaultReuseIdentifier, for: indexPath) as! VIAUserBasicInfoLargeCell
        
        //By default, selectionStyle value is .none
        cell.selectionStyle = selectionStyle
        
        /* Set Edit Button Label*/
        cell.setEditButtonLabel(text: CommonStrings.Settings.ProfileLandingProfilePictureEdit913)
        
        //By default, accessoryType value is .none
        cell.accessoryType = accessoryType
        return cell
    }
    
    private func getProfileDetailCell(_ indexPath: IndexPath, selectionStyle: UITableViewCellSelectionStyle = .none,
                                      accessoryType:UITableViewCellAccessoryType = .none) -> VIAProfileDetailCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: VIAProfileDetailCell.defaultReuseIdentifier, for: indexPath) as! VIAProfileDetailCell
        
        //By default, selectionStyle value is .none
        cell.selectionStyle = selectionStyle
        
        //By default, accessoryType value is .none
        cell.accessoryType = accessoryType
        return cell
    }
}

// MARK: Edit Avatar
extension EditProfileViewController {
    
    func showAvatarPrompt(view: UIView? = nil) {
        imagePicker?.selectImage(view: view)
    }
    
    func takeAvatarPhoto() {
        imagePicker?.selectImage()
    }
    
    func selectLibraryAvatar() {
        imagePicker?.selectImage()
    }
}

// MARK: ImagePickerDelegate
extension EditProfileViewController: VIAImagePickerDelegate{
    
    func pass(imageInfo: [String : Any]) {
        let fileName = UInt(Date().timeIntervalSince1970 * 1000)
        
        /* Let's check first for edited image. */
        var rawImage = imageInfo[UIImagePickerControllerEditedImage] as? UIImage
        
        if nil == rawImage{
            /* let's fallback to the original image. */
            rawImage = imageInfo[UIImagePickerControllerOriginalImage] as? UIImage
        }
        
        guard let image = rawImage else {
            debugPrint("SaveFailed: Can't find valid data from UIImagePickerControllerEditedImage and UIImagePickerControllerOriginalImage.")
            self.imagePicker?.dismissImagePicker()
            return
        }
        
        if var fileURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            fileURL.appendPathComponent("\(fileName)")
            
            do {
                if let jpgImageData = UIImageJPEGRepresentation(image, 1.0) {
                    fileURL.appendPathExtension("jpg")
                    try jpgImageData.write(to: fileURL, options: .atomic)
                    
                    self.uploadProfileImage(data: jpgImageData)
                }
                
                self.imagePicker?.dismissImagePicker()
            } catch {
                
                debugPrint("SaveFailed: \(error)")
                self.imagePicker?.dismissImagePicker()
            }
        }
    }
    
    func show(alert imageSouceSelector: UIAlertController) {
        imageSouceSelector.view.tintColor = UIColor.genericErrorAlertButtonText()
        self.present(imageSouceSelector, animated: true, completion: nil)
    }
    
    func present(imagePicker: UIImagePickerController) {
        imagePicker.view.tintColor = UIColor.genericErrorAlertButtonText()
        self.present(imagePicker, animated: true, completion: nil)
    }
}

extension EditProfileViewController{

    fileprivate func uploadProfileImage(data: Data){
        let partyId = DataProvider.newRealm().getPartyId()
        self.showHUDOnView(view: self.view)
        ProfileImageHelper.upload(partyID: partyId, data: data) { [weak self] (success) in
            self?.hideHUDFromView(view: self?.view)
            if success{
            }else{
                
            }
        }
    }
}

// MARK: View Configuration
extension EditProfileViewController{
    
    func configureAppearance() {
        self.navigationController?.navigationBar.tintColor = UIColor.primaryColor()
        self.tableView.backgroundColor = UIColor.tableViewBackground()
    }
    
    func configureTableView() {
        self.tableView.register(VIATableViewCell.self, forCellReuseIdentifier: VIATableViewCell.defaultReuseIdentifier)
        self.tableView.register(VIAUserBasicInfoLargeCell.nib(), forCellReuseIdentifier: VIAUserBasicInfoLargeCell.defaultReuseIdentifier)
        self.tableView.register(VIAProfileDetailCell.nib(), forCellReuseIdentifier: VIAProfileDetailCell.defaultReuseIdentifier)
        self.tableView.register(VIATableViewSectionHeaderFooterView.nib(), forHeaderFooterViewReuseIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier)
        self.tableView.estimatedRowHeight = 75
        self.tableView.backgroundColor = UIColor.tableViewBackground()
        self.tableView.tableFooterView = UIView()
    }
}


// MARK: Sections+SettingsItems
extension EditProfileViewController{
    
    fileprivate func isBasicInfoSection(_ section: Int) -> Bool{
        return Sections(rawValue: section) == .BasicInfo
    }
    
    fileprivate func isPersonalDetailsSection(_ section: Int) -> Bool{
        return Sections(rawValue: section) == .PersonalDetails
    }
    
    fileprivate func isAccountDetailsSection(_ section: Int) -> Bool{
        return Sections(rawValue: section) == .AccountDetails
    }
    
    fileprivate func isUpdateEmailEnable() -> Bool{
        if let showUpdateEmail = VIAApplicableFeatures.default.enableUpdateEmail,
            showUpdateEmail {
            return true
        }
        return false
    }
    
    enum Sections: Int, EnumCollection {
        case BasicInfo = 0
        case PersonalDetails = 1
        case AccountDetails = 2
    }
    
    enum SettingsItems: Int, EnumCollection {
        case Mobile = 0
        case DOB = 1
        case Gender = 2
        
        func title() -> String? {
            switch self {
            case .Mobile:
                return CommonStrings.Settings.ProfileLandingMobileTitle915
            case .DOB:
                return CommonStrings.Settings.ProfileLandingDateOfBirth916
            case .Gender:
                return CommonStrings.Settings.ProfileLandingGenderTitle917
            }
        }
    }
    
    enum AccountDetailItems: Int, EnumCollection {
        case Entity = 0
        case Email = 1
        
        func title() -> String? {
            switch self {
            case .Entity:
                return CommonStrings.Activate.EntityNumber358
            case .Email:
                return CommonStrings.Settings.ProfileLandingEmailTitle919
            }
        }
    }
}

// MARK: Clear Account Details
extension EditProfileViewController{
    func clearAccountDetails(_ cell:VIAProfileDetailCell) {
        cell.labelText = ""
        cell.valueText = ""
        cell.noValueText = ""
    }
}
