//
//  ReferencesViewModel.swift
//  VitalityActive
//
//  Created by Val Tomol on 12/01/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation

class ReferenceViewModel {
    var effectiveFrom: Date?
    var effectiveTo: Date?
    var comments: String = ""
    var issuedBy: String = ""
    var type: String = ""
    var value: String = ""
}
