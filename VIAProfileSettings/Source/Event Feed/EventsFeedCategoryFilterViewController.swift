//
//  EventsFeedCategoryFilterNavigationViewController.swift
//  VitalityActive
//
//  Created by OJ Garde on 10/11/2017.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import VIAUIKit
import VitalityKit
import VIAUtilities

import SnapKit
import RealmSwift

protocol EventsFeedCategoryFilterDelegate: class {
    func didSelectCategories(categories: [EventCategoryRef])
}

struct EventsFeedCategory {
    let name: String
    var reference: EventCategoryRef
    var image: UIImage
}

class EventsFeedCategoryFilterViewController: VIATableViewController {
    
    public static let defaultReuseIdentifier = "EventsFeedCategoryFilterNavigationViewController"
    
    override var preferredContentSize: CGSize {
        get {
            let height: CGFloat = tableView.contentSize.height + ((self.navigationController?.navigationBar.frame.size.height) ?? 0)
            return CGSize(width: super.preferredContentSize.width, height: height)
        }
        set { super.preferredContentSize = newValue }
    }
    
    // MARK: Properties
    var allPointsCategoryReference = EventCategoryRef.Unknown
    var selectedCategories = Set<EventCategoryRef>()
    var pointsCategories = [EventsFeedCategory]()
    
    weak var delegate: EventsFeedCategoryFilterDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpCategories()
        configureTitle()
        configureTableView()
        tableView.reloadData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        delegate?.didSelectCategories(categories: Array(selectedCategories))
    }
    
    // MARK: UITableViewControllerDataSource
    // We cannot move this to extension due to IDEs restriction
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    // MARK: UITableViewControllerDataSource
    // We cannot move this to extension due to IDEs restriction
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pointsCategories.count
    }
}


// MARK: View Configuration
extension EventsFeedCategoryFilterViewController{
    
    func setUpCategories() {
        let realm = DataProvider.newRealm()
        pointsCategories = []
        
        let eventCategories = realm.allEventCategories()
        
        print("eventCategories --> \(eventCategories)")
        for eventCategory in eventCategories {
            if let categoryImage = EventsFeedCategoryUtil(eventCategory.categoryId).getImage(){
                let category = EventsFeedCategory(name: eventCategory.name,
                                                  reference: EventCategoryRef(rawValue: eventCategory.reference) ?? .Unknown,
                                                  image: categoryImage)
                pointsCategories.append(category)
            }
        }
        pointsCategories.sort(by: sortCategoriesByIndex)
    }
    
    func configureTitle() {
        let tlabel = UILabel(frame: CGRect(x: 0, y: 0, width: 300, height: 37))
        tlabel.text = CommonStrings.EventsChooseCategory
        tlabel.textColor = UIColor.darkGrey()
        tlabel.font = UIFont.footnoteFont()
        self.navigationController?.navigationBar.barTintColor = .white
        self.navigationItem.titleView = tlabel
    }
    
    func configureTableView() {
        self.tableView.register(VIATableViewCell.nib(), forCellReuseIdentifier: VIATableViewCell.defaultReuseIdentifier)
        self.tableView.allowsMultipleSelection = true
        self.tableView.alwaysBounceVertical = false
        self.tableView.isScrollEnabled = true
        self.tableView.backgroundColor = .white
        self.tableView.estimatedRowHeight = 50
        self.tableView.estimatedSectionHeaderHeight = 0
        self.tableView.estimatedSectionFooterHeight = 0
        self.tableView.sectionHeaderHeight = 0
        self.tableView.sectionFooterHeight = 0
    }
}

// MARK: UITableViewControllerDataSource
extension EventsFeedCategoryFilterViewController{
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: VIATableViewCell.defaultReuseIdentifier, for: indexPath) as! VIATableViewCell
        cell.selectionStyle = .none
        //ge20171211 : default to AllEvents
        if(selectedCategories.count < 1) {
            selectedCategories.insert(pointsCategories[indexPath.row].reference)
        }
        let category = pointsCategories[indexPath.row]
        cell.labelText = category.name
        cell.cellImage = category.image
        cell.accessoryType = selectedCategories.contains(category.reference) ? .checkmark : .none
        
        return cell
    }
}

// MARK: UITableViewControllerDelegate
extension EventsFeedCategoryFilterViewController{
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let newSelectedCategoryReference = pointsCategories[indexPath.row].reference
        //ge20171211 : if selected AllEvents, clear all others
        //           : if selected other categories, remove AllEvents
        //           : defaults to AllEvents, cannot be deselected when there's no other selections
        if (newSelectedCategoryReference.rawValue == -1) {
            selectedCategories.removeAll()
            selectedCategories.insert(newSelectedCategoryReference)
        }else{
            if selectedCategories.contains(pointsCategories[0].reference) {
                selectedCategories.remove(pointsCategories[0].reference)
            }
        }
        
        if selectedCategories.contains(newSelectedCategoryReference) {
            selectedCategories.remove(newSelectedCategoryReference)
        } else {
            selectedCategories.insert(newSelectedCategoryReference)
        }
        self.tableView.reloadData()
        return
    }
}


// MARK: Utility
extension EventsFeedCategoryFilterViewController{
    
    func sortCategoriesByIndex(this: EventsFeedCategory, that: EventsFeedCategory) -> Bool {
        return this.reference.rawValue < that.reference.rawValue
    }    
}
