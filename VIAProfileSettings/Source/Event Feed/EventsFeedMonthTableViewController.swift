//
//  EventsFeed.swift
//  VitalityActive
//
//  Created by OJ Garde on 10/11/2017.
//  Copyright © 2017 Glucode. All rights reserved.
//

import UIKit
import VIAUIKit
import VitalityKit
import VIAUtilities

import RealmSwift
import SwiftDate

public protocol EventsFeedMonthRefreshDelegate: class {
    func performRefresh(sender: EventsFeedMonthTableViewController?)
}

struct EventFeedHolder: Equatable, Hashable{
    var categoryName:String
    var category:EventCategoryRef
    var message:String
    
    var hashValue: Int {
        get {
            return category.hashValue << 15 + message.hashValue
        }
    }
    
    static func ==(lhs: EventFeedHolder, rhs: EventFeedHolder) -> Bool {
        return lhs.category == rhs.category && lhs.message == rhs.message
    }
}

public class EventsFeedMonthTableViewController: VIATableViewController {
    
    // MARK: Properties
    public weak var refreshDelegate: EventsFeedMonthRefreshDelegate?
    
    lazy var formatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEE, d MMM yyyy"
        return dateFormatter
    }()
    
    public var date: Date? {
        didSet {
            guard let date = self.date else { return }
            createSectionsAndData(for: date, categories: self.selectedCategories)
            self.reloadData()
        }
    }
    
    public var selectedCategories = [EventCategoryRef]() {
        didSet {
            guard let date = self.date else { return }
            createSectionsAndData(for: date, categories: self.selectedCategories)
            self.reloadData()
        }
    }
    
    public var didSelectItem: (Any?) -> Void = { item in
        debugPrint("EventsFeedMonthTableViewController didSelectItem \(String(describing: item))")
    }
    
    // MARK: Data & sections
    
    var sections: [Date] = []
    
    var sectionsFooter: [String] = []
    
    var eventsBySection = [[EventFeedHolder]]()
    
    var realm = DataProvider.newRealm()
    
    // MARK: View lifecycle
    
    deinit {
        debugPrint("EventsFeedMonthTableViewController Deinit")
    }
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        
        configureTableView()
        configureRefreshControl()
    }
    
    // MARK: UITableViewControllerDataSource
    // We cannot move this to extension due to IDEs restriction
    public override func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    
    // MARK: UITableViewControllerDataSource
    // We cannot move this to extension due to IDEs restriction
    public override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return eventsBySection[section].count
    }
}


// MARK: Sections and Data
extension EventsFeedMonthTableViewController{
    
    fileprivate func createSectionsAndData(for date: Date, categories:[EventCategoryRef]){
        // events for month of date
        let events: Results<EventRO>
        
        if let category = categories.first,
            let eventCategory = realm.eventCategory(with: category),
            categories.count > 0 && eventCategory.name != CommonStrings.EventsAllEvents{
            events = realm.events(for: categories, during: date)
        } else {
            events = realm.events(for: date)
        }
        print("Events count: \(events.count)")
        
        // create sections
        var unsortedDates: Set<Date> = []
        for entry in events {
            if let date = entry.eventDateTime{
                let startOfDay = Calendar.current.startOfDay(for: date)
                unsortedDates.insert(startOfDay)
            }
        }
        sections = unsortedDates.sorted(by: { $0 > $1 })
        
        sectionsFooter = []
        for i in 0..<sections.count{
            if i == sections.count-1{
                sectionsFooter.append(CommonStrings.EventsFooter(date.monthName, String(date.year)))
            }else{
                sectionsFooter.append("")
            }
        }
        
        // create sections for each object
        var objectsBySection = [[EventFeedHolder]]()
        for sectionDate in sections {
            let unsortedObjectsForSection = events.filterEventDateTimeBetweenStartAndEnd(day: sectionDate, fieldName: "eventDateTime")
            let sortedObjectsForSection = unsortedObjectsForSection.sorted(byKeyPath: "eventDateTime", ascending: false)
            objectsBySection.append(toEventsBySection(sortedObjectsForSection))
        }
        
        eventsBySection = objectsBySection
    }
    
    private func toEventsBySection(_ results: Results<EventRO>) -> [EventFeedHolder]{
        let realm = DataProvider.newRealm()
        var dest = [EventFeedHolder]()
        var cat = [Int]()
        for result in results{
            if (cat.contains(result.eventCategoryKey.rawValue)) {
                continue
            }
            cat.append(result.eventCategoryKey.rawValue)
            print("event --> (\(result.eventCategoryKey.rawValue))\n\(result)")
            
            if let name = realm.eventCategory(with: result.eventCategoryKey)?.name{
                dest.append(EventFeedHolder(categoryName:  name,
                                            category: result.eventCategoryKey,
                                            message: result.typeName))
            }else{
                debugPrint("Could not find name for category with ID: \(result.eventCategoryKey.rawValue)")
            }
        }
        return dest
    }
    
}


// MARK: View configuration
extension EventsFeedMonthTableViewController{
    
    func configureEmptyStatusView() {
        removeEmptyStatusView()
        let view = VIAStatusView.viewFromNib(owner: self)!
        
        view.heading = CommonStrings.EventsNoEventsActivity
        view.message = CommonStrings.EventsNoEventsRecorded
        view.buttonTitle = CommonStrings.EventsHelp
        view.hideButtonBorder = false

        
        self.view.addSubview(view)
        
        view.snp.makeConstraints { (make) in
            make.width.equalToSuperview()
            make.left.right.top.equalToSuperview()
            make.height.equalToSuperview().offset(-62)
            make.bottom.equalToSuperview().offset(-62)
        }
    }
    
    func removeEmptyStatusView() {
        self.view.subviews.forEach { (view) in
            if view is VIAStatusView {
                view.removeFromSuperview()
            }
        }
    }
    
    func configureTableView() {
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 58
        self.tableView.estimatedSectionFooterHeight = 40
        self.tableView.sectionFooterHeight = UITableViewAutomaticDimension
        self.tableView.register(VIAEventsFeedCell.nib(), forCellReuseIdentifier: VIAEventsFeedCell.defaultReuseIdentifier)
        self.tableView.register(VIATableViewSectionHeaderFooterView.nib(), forHeaderFooterViewReuseIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier)
    }
    
    func configureRefreshControl() {
        tableView.refreshControl = UIRefreshControl()
        tableView.refreshControl?.tintColor = UIColor.cellSeperator()
        tableView.refreshControl?.addTarget(self, action: #selector(performRefresh), for: .valueChanged)
    }
}

// MARK: UITableViewControllerDataSource
extension EventsFeedMonthTableViewController{
    
    public override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: VIAEventsFeedCell.defaultReuseIdentifier, for: indexPath) as! VIAEventsFeedCell
        
        let event = eventsBySection[indexPath.section][indexPath.row]
        
        cell.eventCategory  = event.categoryName
        cell.eventMessage   = event.message
        cell.eventImage     = EventsFeedCategoryUtil(event.category).getImage()
        
        return cell
    }
    
    public override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier) as! VIATableViewSectionHeaderFooterView
        view.labelText = formatter.string(from: sections[section])
        view.set(alignment: .left)
        return view
    }
    
    public override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier) as! VIATableViewSectionHeaderFooterView
        let footerString = sectionsFooter[section]
        if footerString.isEmpty{
            return nil
        }
        view.set(alignment: .center)
        view.configureWithExtraSpacing(labelText: footerString,
                                       extraTopSpacing: 40,
                                       extraBottomSpacing: 40)
        return view
    }
    
    public override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
}

// MARK: UITableViewControllerDelegate
extension EventsFeedMonthTableViewController{
    
    public override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //        let pointsEntry = dummyMessages[indexPath.section][indexPath.row]
        //        didSelectItem(pointsEntry)
        //        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    override public func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        return false
    }
}

// MARK: Reload
extension EventsFeedMonthTableViewController{
    
    func reloadData() {
        guard self.date != nil else {
            removeEmptyStatusView()
            return
        }
        
        self.tableView.reloadData()
        if eventsBySection.count == 0 {
            configureEmptyStatusView()
        } else {
            removeEmptyStatusView()
        }
    }
}

// MARK: Refresh
extension EventsFeedMonthTableViewController{
    
    func performRefresh() {
        self.refreshDelegate?.performRefresh(sender: self)
    }
}

extension Date{
    
    public static func getDateMinusDaysFromNow(days: Int) -> Date{
        return Calendar.current.date(byAdding: .day, value: days, to: Date())!
    }
}
