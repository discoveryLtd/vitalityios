//
//  EventsFeedCategoryUtil.swift
//  VitalityActive
//
//  Created by OJ Garde on 13/11/2017.
//  Copyright © 2017 Glucode. All rights reserved.
//

import UIKit
import VIAUtilities
import VitalityKit

public class EventsFeedCategoryUtil{
    
    private var category:EventCategoryRef?
    
    required public init(_ category: EventCategoryRef){
        self.category = category
    }
    
    public func getImage() -> UIImage?{
        //TODO REVERT
//        if let category = self.category{
//            switch(category){
////            case EventCategoryRef.Activation: return VIACoreAsset.EventFeeds.activationCategoryLogo.image
//            case EventCategoryRef.Assessment: return VIACoreAsset.EventFeeds.assessmentsCategoryLogo.image
//            case EventCategoryRef.Legal: return VIACoreAsset.EventFeeds.legalCategoryLogo.image
//            case EventCategoryRef.Device: return VIACoreAsset.EventFeeds.deviceCategoryLogo.image
//            case EventCategoryRef.Finance: return VIACoreAsset.EventFeeds.financialsCategoryLogo.image
////            case EventCategoryRef.GetActive: return VIACoreAsset.EventFeeds.getActiveCategoryLogo.image
//            case EventCategoryRef.HealthAttribute: return VIACoreAsset.EventFeeds.heathDataCategoryLogo.image
//            case EventCategoryRef.Nutrition: return VIACoreAsset.EventFeeds.nutritionCategoryLogo.image
////            case EventCategoryRef.Other: return VIACoreAsset.EventFeeds.otherCategoryLogo.image
////            case EventCategoryRef.ProfileManagement: return VIACoreAsset.EventFeeds.profileManagementCategoryLogo.image
//            case EventCategoryRef.Reward: return VIACoreAsset.EventFeeds.rewardCategoryLogo.image
//            case EventCategoryRef.Screenings: return VIACoreAsset.EventFeeds.screeningCategoryLogo.image
//            case EventCategoryRef.Status: return VIACoreAsset.EventFeeds.statusCategoryLogo.image
//            case EventCategoryRef.Unknown: return VIACoreAsset.EventFeeds.allCategoryLogo.image
//            default: return VIACoreAsset.EventFeeds.otherCategoryLogo.image
//            }
//        }
        return nil
    }
    
    static func addCategoriesToRealm() {
        let realm = DataProvider.newRealm()
        try! realm.write {
            let objects = realm.objects(EventCategory.self)
            realm.delete(objects)
        }
        let productFeatureLinkEventCategories = realm.ProductFeatureLink(for: .EventCategory)
        
        EventCategory.setupEventCategory(name: CommonStrings.EventsAllEvents, categoryId: .Unknown)
        
        for category in productFeatureLinkEventCategories {
            let eventCategoryRef: EventCategoryRef = EventCategoryRef(rawValue:category.linkedKey.value ?? EventCategoryRef.Unknown.rawValue) ?? EventCategoryRef.Unknown
            
            switch eventCategoryRef {
            case .Assessment:
                EventCategory.setupEventCategory(name: CommonStrings.EventsCategoryAssessment1129, categoryId: eventCategoryRef)
                break
            case .DataPrivacy,.Legal:
                EventCategory.setupEventCategory(name: CommonStrings.EventsCategoryDataSharingAndLegal1130, categoryId: eventCategoryRef)
                break
            case .Device:
                EventCategory.setupEventCategory(name: CommonStrings.EventsCategoryDevices1131, categoryId: eventCategoryRef)
                break
            case .Finance:
                EventCategory.setupEventCategory(name: CommonStrings.EventsCategoryFinancials1133, categoryId: eventCategoryRef)
                break
            case .HealthAttribute:
                EventCategory.setupEventCategory(name: CommonStrings.EventsCategoryHealthData1135, categoryId: eventCategoryRef)
                break
            case .Nutrition:
                EventCategory.setupEventCategory(name: CommonStrings.EventsCategoryNutrition, categoryId: eventCategoryRef)
                break
            case .Reward:
                EventCategory.setupEventCategory(name: CommonStrings.EventsCategoryRewards, categoryId: eventCategoryRef)
                break
            case .Screenings:
                EventCategory.setupEventCategory(name: CommonStrings.EventsCategoryScreenings, categoryId: eventCategoryRef)
                break
            case .Status:
                EventCategory.setupEventCategory(name: CommonStrings.EventsCategoryStatus, categoryId: eventCategoryRef)
                break
            case .DocumentManagement:
                EventCategory.setupEventCategory(name: CommonStrings.EventsCategoryDocuments1132, categoryId: eventCategoryRef)
                break
            case .Login:
                EventCategory.setupEventCategory(name: CommonStrings.EventsCategoryLogin, categoryId: eventCategoryRef)
                break
            case .Servicing:
                EventCategory.setupEventCategory(name: CommonStrings.EventsCategoryServicing1138, categoryId: eventCategoryRef)
                break
            case .Agreement, .Disclaimer, .Enrollment, .Error, .Fitness, .Integration,
                 .Notification, .Points, .Product, .ScreenAndVacc, .Social, .Target,
                 .TermsAndConditions, .Vaccinations, .VHC, .Unknown, .Other:
                break
            }
        }
        
    }
}
