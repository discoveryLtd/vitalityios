//
//  EditEntityNumberViewController.swift
//  VitalityActive
//
//  Created by OJ Garde on 12/27/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import VIAUIKit
import VIACommon
import VitalityKit
import RealmSwift
import VIAUtilities

struct EntityDisplayHolder{
    var heading:String
    var content:String
    var placeHolder:String
    var footerValue:String
}

class EditEntityNumberViewController: VIACoordinatedTableViewController, PrimaryColorTintable{
    
    var currentEntityNumber:String?
    var newEntityNumber:String?
    var hasEntityNumber: Bool?
    
    fileprivate var displayValues:[EntityDisplayHolder] = []
    fileprivate var footerValues:[String] = []
    fileprivate let MAX_CHARACTER_COUNT = 100
    
    fileprivate var enableNextButton:Bool = false{
        didSet{
            navigationItem.rightBarButtonItem?.isEnabled = enableNextButton
        }
    }
    
    fileprivate var inputAlert:UIAlertController!
    fileprivate var secureEntryButton:UIButton!
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        
        self.configureTableView()
        
        populateData(withCurrentEntityNumber: currentEntityNumber!)
        
        if (currentEntityNumber?.isEmpty)! {
            hasEntityNumber = false
        } else {
            hasEntityNumber = true
        }
    }
    
    override public func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.title = CommonStrings.Activate.EntityNumber358
        self.configureAppearance()
        
        self.tableView.reloadData()
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return displayValues.count
    }
}


// MARK: Data
extension EditEntityNumberViewController{
    
    fileprivate func populateData(withCurrentEntityNumber entityNumber: String) {
        displayValues.append(EntityDisplayHolder(heading: CommonStrings.Activate.EntityNumber358,
                                                 content: entityNumber,
                                                 placeHolder: CommonStrings.Activate.EntityNumberPlaceholder359,
                                                 footerValue: CommonStrings.Activate.EntityNumberFooter360))
    }
}

// MARK: UITableViewControllerDataSource
extension EditEntityNumberViewController{
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = getVIATextFieldTableViewCell(indexPath)
        
        // Retrieve current value from our holder
        let value = displayValues[indexPath.section]
        
        // Initialize cell image configuration
        cell.cellImageConfig = getVIATextFieldCellImageConfig()
        
        //Set value for heading
        cell.setHeadingLabelText(text: value.heading)
        
        // Set initial value for the textfield
        cell.setTextFieldText(text: value.content)
        
        // Set placeholder value for the field in case no input is available
        cell.setTextFieldPlaceholder(placeholder: value.placeHolder)
        
        // Configure UITextField listener for validating of email
        configureTextFieldListener(cell, indexPath: indexPath)
        
        // Set this to .none to disable selection color on the cell row
        cell.selectionStyle = .none
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        // Retrieve current value from our holder
        let value = displayValues[section]
        return value.footerValue.isEmpty ? nil : getFooterView(value.footerValue)
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier) as! VIATableViewSectionHeaderFooterView
        view.labelText = " "
        return view
    }
}

// MARK: Table Helper
extension EditEntityNumberViewController{
    
    fileprivate func configureTextFieldListener(_ cell: VIATextFieldTableViewCell, indexPath: IndexPath){
        cell.textFieldTextDidChange = { [unowned self] textField in
            
            // Holder for our error message
            var errorMessage:String? = nil
            self.newEntityNumber = nil
            self.enableNextButton = true
            
            // Create holder for the value of email
            if let entityNumber = textField.text{
                
                // Let's check for double byte
                if self.checkForDoubleByte(entityNumber) {
                    self.newEntityNumber = entityNumber
                }else{
                    errorMessage = CommonStrings.ProfileEditMailValidation
                }
                
                // Let's check for the character count
                if self.hasExceedCharactersLimit(entityNumber){
                    errorMessage = CommonStrings.Settings.ProfileEmailErrorCharacterLimit926(String(self.MAX_CHARACTER_COUNT))
                }
            }
            
            // Refresh the error message label in UI
            UIView.performWithoutAnimation {
                self.tableView.beginUpdates()
                cell.setErrorMessage(message: errorMessage)
                self.enableNextButton = errorMessage == nil
                self.tableView.endUpdates()
            }
        }
        
        //cell.textFieldDidEndEditing = { [unowned self] textField in
        //    self.tableView.reloadRows(at: [indexPath], with: .automatic)
        //}
    }
    
    fileprivate func getFooterView(_ content:String) -> UIView{
        let view = getHeaderFooterView().addPadding(labelText: content, top: 0)
        return view
    }
}


// MARK: CellView
extension EditEntityNumberViewController{
    
    fileprivate func getHeaderFooterView() -> VIATableViewSectionHeaderFooterView{
        return tableView.dequeueReusableHeaderFooterView(withIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier) as! VIATableViewSectionHeaderFooterView
    }
    
    fileprivate func getVIATextFieldTableViewCell(_ indexPath: IndexPath) -> VIATextFieldTableViewCell{
        return tableView.dequeueReusableCell(withIdentifier: VIATextFieldTableViewCell.defaultReuseIdentifier, for: indexPath) as! VIATextFieldTableViewCell
    }
}


// MARK: VIATextFieldCellImageConfig
extension EditEntityNumberViewController{
    
    fileprivate func getVIATextFieldCellImageConfig() -> VIATextFieldCellImageConfig{
        var config = VIATextFieldCellImageConfig()
        //TODO REVERT
//        config.templateImage = VIACoreAsset.Profile.entityNumberSmall.templateImage
        config.activeTintColor = UIColor.black
        config.inactiveTintColor = UIColor.night()
        return config
    }
}

// MARK: View Configuration
extension EditEntityNumberViewController{
    
    func configureAppearance() {
        self.hideBackButtonTitle()
        self.tableView.backgroundColor = UIColor.tableViewBackground()
        
        // Set left bar button for Cancel
        self.addLeftBarButtonItem(CommonStrings.ProfileChangeEmailCancel, target: self, selector: #selector(onCancel))
        
        // Set right bar button for Next
        self.addRightBarButtonItem(CommonStrings.DoneButtonTitle53, target: self, selector: #selector(onNext))
        
        // This must be disabled after load to prevent user from continuing
        // to the next page with a blank new email.
        self.navigationItem.rightBarButtonItem?.isEnabled = false
    }
    
    // Selector for Cancel navigation button
    func onCancel(sender: UIBarButtonItem){
        self.navigationController?.popViewController(animated: true)
    }
    
    // Selector for Next navigation button
    func onNext(sender: UIBarButtonItem) {
        
        //Show Progress Dialog
        self.showHUDOnView(view: self.view)
        
        if let entityNumber = self.newEntityNumber {
            
            //Update Entity Number
            let coreRealm = DataProvider.newRealm()
            let partyId = coreRealm.getPartyId()
            var party: VitalityParty?
            party = coreRealm.objects(VitalityParty.self).last
            let person = party?.person
            let references = party?.references
            let dateOfBirth = Localization.yearMonthDayFormatter.string(from: (person?.bornOn)!)
            updateEntityNumber(entityNumber: entityNumber, partyId: String(partyId), dateOfBirth: dateOfBirth, completion: { [weak self] (success, error) in
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + .seconds(2)) {
                    self?.hideHUDFromView(view: self?.view)
                    if success {
                        // Pop view.
                        self?.navigationController?.popViewController(animated: true)
                    } else {
                        self?.displayIncorrectEntityNumber()
                        // self?.displayConnectionError()
                    }
                }
            })
        }
    }
    
    func configureTableView() {
        self.tableView.register(VIATextFieldTableViewCell.nib(), forCellReuseIdentifier: VIATextFieldTableViewCell.defaultReuseIdentifier)
        self.tableView.register(VIATableViewSectionHeaderFooterView.nib(), forHeaderFooterViewReuseIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier)
        self.tableView.estimatedRowHeight = 75
        self.tableView.backgroundColor = UIColor.tableViewBackground()
        self.tableView.tableFooterView = UIView()
    }
}


// MARK: Utilities
extension EditEntityNumberViewController{
    
    func hasExceedCharactersLimit(_ token:String) -> Bool{
        return token.characters.count > MAX_CHARACTER_COUNT
    }
    
    func isValidToken(_ token:String?) -> Bool {
        let pattern = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let predicate = NSPredicate(format:"SELF MATCHES %@", pattern)
        return predicate.evaluate(with: token)
    }
    
    func checkForDoubleByte(_ token:String) -> Bool{
        
        let buf = token.utf8.count
        if buf == token.characters.count {
            return true
        }
        
        return false
    }
}

// MARK: UIAlertController
extension EditEntityNumberViewController{
    
    fileprivate func displayIncorrectEntityNumber(){
        let title = CommonStrings.AlertIncorrectNumberTitle372
        let message = CommonStrings.AlertIncorrectNumberMessage373
        
        var actions:[UIAlertAction] = []
        actions.append(UIAlertAction(title: CommonStrings.OkButtonTitle40, style: .default, handler: nil))
        
        showAlertDialog(title: title, message: message, alertActions: actions)
    }
    
    fileprivate func displayConnectionError(){
        let title = CommonStrings.AlertConnectionErrorTitle1139
        let message = CommonStrings.ConnectivityErrorAlertMessage45
        
        var actions:[UIAlertAction] = []
        actions.append(UIAlertAction(title: CommonStrings.CancelButtonTitle24, style: .cancel, handler: nil))
        actions.append(UIAlertAction(title: CommonStrings.TryAgainButtonTitle43, style: .default, handler: nil))
        
        showAlertDialog(title: title, message: message, alertActions: actions)
    }
    
    fileprivate func displayUnknownError(){
        let title = CommonStrings.AlertUnknownTitle266
        let message = CommonStrings.AlertUnknownMessage267
        
        var actions:[UIAlertAction] = []
        actions.append(UIAlertAction(title: CommonStrings.CancelButtonTitle24, style: .cancel, handler: nil))
        actions.append(UIAlertAction(title: CommonStrings.TryAgainButtonTitle43, style: .default, handler: nil))
        
        showAlertDialog(title: title, message: message, alertActions: actions)
    }
    
    fileprivate func showAlertDialog(title:String? = nil, message:String? = nil,
                                     alertActions:[UIAlertAction]){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        
        for alertAction in alertActions{
            alert.addAction(alertAction)
        }
        alert.view.tintColor = UIButton().tintColor
        self.present(alert, animated: true, completion: nil)
    }
}

extension EditEntityNumberViewController {
    fileprivate func updateEntityNumber(entityNumber: String, partyId: String, dateOfBirth: String,
                                        completion:@escaping (_ success:Bool, _ error:String?) -> Void) {
        //Call service to update Entity Number
        let request = ManageEntityNumberParams(entityNumber: entityNumber,
                                               partyId: partyId,
                                               dateOfBirth: dateOfBirth)
        if hasEntityNumber! {
            Wire.Party.updateEntityNumber(request: request) { (success) in
                DispatchQueue.main.async {
                    if success {
                        completion(true, "")
                    } else {
                        completion(false, "")
                    }
                }
            }
        } else {
            Wire.Party.addEntityNumber(request: request) { (success) in
                DispatchQueue.main.async {
                    if success {
                        completion(true, "")
                    } else {
                        completion(false, "")
                    }
                }
            }
        }

    }
}

