//
//  ProfileEditSelectedPhotosCollectionViewController.swift
//  VitalityActive
//
//  Created by Von Kervin R. Tuico on 15/03/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import VIAUIKit
import VitalityKit
import Photos
import RealmSwift
import VIACommon
import VIAImageManager
import VIAScreeningsVaccinations

protocol ProfileEditSelectedPhotosCollectionViewControllerProtocol {
    func updateEditSelectedAttachments(attachments: Array<Dictionary<String, Any>>)
}

class ProfileEditSelectedPhotosCollectionViewController: UICollectionViewController {
    
    var delegate: ProfileEditSelectedPhotosCollectionViewControllerProtocol!
    
    var totalAttachments: Array<Dictionary<String, Any>> = []
    var imagesInformation: Array<ImageInformation> = []
    var selectedImageIndexes = [Int]()
    var imagePicker: VIASelectImageController?
    var emptyStateAddPhotoView: AddPhotoView?
    var realm: Realm?
    
    @IBOutlet weak var selectPhotosFlowLayout: UICollectionViewFlowLayout!
    @IBAction func unwindToPhotosCollection(segue: UIStoryboardSegue) {
        self.collectionView?.reloadData()
    }
    
    public func remove(imageInfo: [String: Any]) {
        if let pickedImage = imageInfo[UIImagePickerControllerOriginalImage] as? UIImage {
            var index = 0
            for helpImageInfo in self.totalAttachments {
                if let heldImage = helpImageInfo[UIImagePickerControllerOriginalImage] as? UIImage {
                    if (pickedImage.isEqual(heldImage)) {
                        self.totalAttachments.remove(at: index)
                    }
                }
                index += 1
            }
        }
        manipulateRightBarButton()
    }
    
    public func add(imageInfo: [String : Any]) {
        self.totalAttachments.append(imageInfo)
        manipulateRightBarButton()
    }
    
    deinit {
    }
    
    // This method will also update the imagesInfo in SVHealthActionViewController
    override func viewWillDisappear(_ animated: Bool) {
        if isMovingFromParentViewController {
            
            totalAttachments.removeAll()
            for imageInformation in imagesInformation{
                totalAttachments.append(imageInformation.imageInfo!)
            }
            self.navigationController?.setToolbarHidden(true, animated: false)
            delegate.updateEditSelectedAttachments(attachments: self.totalAttachments)
            delegate = nil
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.collectionView?.alwaysBounceVertical = true
        self.title = CommonStrings.Proof.AddProofScreenTitle163
        imagePicker = VIASelectImageController(pickerDelegate: self, configuration: UIImagePickerController.ImageControllerConfiguration())
        self.view.backgroundColor = UIColor.day()
        
        self.setCollectionViewCellSize()
        self.registerCollectionViewCells()
        self.setupToolBar()
        self.addImageInfo(imagesInfo: totalAttachments)
        
        self.clearsSelectionOnViewWillAppear = false
    }
    
    func addImageInfo(imagesInfo: Array<Dictionary<String, Any>>){
        for imageInfo in imagesInfo{
            let newImageInfo = ImageInformation(imageInfo: imageInfo, status: false)
            imagesInformation.append(newImageInfo)
            
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.hideBackButtonTitle()
        
        if (isCollectionEmpty()) {
            emptyStateAddPhotoView = AddPhotoView.viewFromNib(owner: self)
            
            emptyStateAddPhotoView?.setHeader(title: CommonStrings.Sv.AddProofEmptyTitle1018)
            emptyStateAddPhotoView?.setDetail(message: CommonStrings.Sv.AddProofEmptyMessage1019)
            emptyStateAddPhotoView?.setAction(title: CommonStrings.Proof.AddButton166)
            //TODO REVERT
//            emptyStateAddPhotoView?.setHeader(image: UIImage.templateImage(asset: .cameraLarge))
            emptyStateAddPhotoView?.set(photoViewDelegate: self)
            emptyStateAddPhotoView?.frame = self.collectionView?.frame ?? CGRect(x: 0, y: 0, width: 0, height: 0)
            
            if emptyStateAddPhotoView != nil {
                self.collectionView?.addSubview(emptyStateAddPhotoView!)
            }
        }
        self.setupNavigationItem()
        manipulateRightBarButton()
    }
    
    func isCollectionEmpty() -> Bool {
        return self.totalAttachments.count == 0
    }
    
    func registerCollectionViewCells() {
        self.collectionView!.register(ImageCollectionViewCell.nib(), forCellWithReuseIdentifier: ImageCollectionViewCell.defaultReuseIdentifier)
        self.collectionView!.register(LabelSupplimentaryView.nib(), forSupplementaryViewOfKind: UICollectionElementKindSectionFooter, withReuseIdentifier: "Footer")
    }
    
    func setupNavigationItem() {
        let cancelBarButton = UIBarButtonItem(title: CommonStrings.CancelButtonTitle24 , style: .done, target: self, action: #selector(onCancel))
        
        let selectAllBarButton = UIBarButtonItem(title: CommonStrings.Settings.FeedbackSelectAll2093 , style: .done, target: self, action: #selector(onSelectAll))
        
        self.navigationItem.rightBarButtonItem = cancelBarButton
        self.navigationItem.leftBarButtonItem = selectAllBarButton
        //self.navigationItem.rightBarButtonItem?.isEnabled = false
    }
    
    func setupToolBar() {
        var toolbarItemsArray = Array<UIBarButtonItem>()
        
        let flexibleSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        toolbarItemsArray.append(flexibleSpace)
        //TODO REVERT
//        let deleteItem = UIBarButtonItem(image: UIImage.templateImage(asset: .trashSmall), style: .plain, target: self, action: #selector(deleteSelectedItem))
//        toolbarItemsArray.append(deleteItem)
        
        self.setToolbarItems(toolbarItemsArray, animated: false)
        self.navigationController?.setToolbarHidden(false, animated: false)
    }
    
    func onCancel() {
        _ = navigationController?.popViewController(animated: true)
    }
    
    func onSelectAll(){
        if imagesInformation.count != 0{
            for index in 0...(imagesInformation.count - 1) {
                imagesInformation[index].status = true
            }
            self.collectionView?.reloadData()
        }
    }
    
    func deleteSelectedItem(sender: UIBarButtonItem) {
        var noItemToBeDeleted = 0
        
        for imageInformation in imagesInformation{
            if imageInformation.status == true{
                noItemToBeDeleted += 1
            }
        }
        
        let alertController = UIAlertController(title: "", message: CommonStrings.Proof.RemoveMessage178, preferredStyle: UIAlertControllerStyle.actionSheet)
        let cancelAction = UIAlertAction(title: CommonStrings.CancelButtonTitle24, style: .cancel) { (UIAlertAction) in
            alertController.dismiss(animated: true, completion: nil)
        }
        
        let removePhotoAction = UIAlertAction(title: VIAApplicableFeatures.default.getSVRemoveButtonLabel(numberOfPhotos: noItemToBeDeleted), style: .destructive) { [weak self] (_) -> Void in
            self?.proceedInDeleting()
        }
        
        alertController.popoverPresentationController?.barButtonItem = sender
        
        alertController.addAction(removePhotoAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func proceedInDeleting(){
        var tempImagesInformation: Array<ImageInformation> = []
        
        if imagesInformation.count != 0{
            for index in 0...(imagesInformation.count - 1) {
                if imagesInformation[index].status == false{
                    tempImagesInformation.append(imagesInformation[index])
                }
            }
            
            imagesInformation.removeAll()
            imagesInformation = tempImagesInformation
            
            if imagesInformation.count == 0{
                _ = navigationController?.popViewController(animated: true)
            }else{
                self.collectionView?.reloadData()
            }
        }
        
    }
    
    func setCollectionViewCellSize() {
        let leftAndRightSpaceingTotal: CGFloat = 15
        let cellPaddingTotal: CGFloat = 15
        let totalWidthPadding: CGFloat = leftAndRightSpaceingTotal + cellPaddingTotal
        let itemWidth = (((self.collectionView?.frame.size.width) ?? totalWidthPadding) - totalWidthPadding) / 2
        self.selectPhotosFlowLayout.itemSize = CGSize(width: itemWidth,
                                                      height: itemWidth)
        self.selectPhotosFlowLayout.minimumLineSpacing = 10
    }
    
    func dequeToCaptureResults() {
        self.performSegue(withIdentifier: "unwindToCaptureResults", sender: self)
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let selectedImage = sender as? [String : Any] else {
            return
        }
        guard let detailImageViewController = segue.destination as? SVImageDetailViewController else {
            return
        }
        
        detailImageViewController.set(selectedImage: selectedImage)
    }
    
    // MARK: UICollectionViewDataSource
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.imagesInformation.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ImageCollectionViewCell.defaultReuseIdentifier, for: indexPath) as? ImageCollectionViewCell
        cell?.imageView.image = nil
        cell?.resetCell()
        
        if let pickedImage = self.imagesInformation[indexPath.row].imageInfo?[UIImagePickerControllerOriginalImage] as? UIImage {
            cell?.imageView.image = pickedImage
            if self.imagesInformation[indexPath.row].status!{
                //TODO REVERT
//                cell?.setStatusIndicatorImage(statusImage: UIImage.templateImage(asset: .statusAchieved)!)
            }
        }
        guard let returnCell = cell else { return UICollectionViewCell() }
        return returnCell
    }
    
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        var supplimentaryView: UICollectionReusableView = UICollectionReusableView()
        if (kind == UICollectionElementKindSectionFooter) {
            guard let footerView = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionFooter, withReuseIdentifier: "Footer", for: indexPath) as? LabelSupplimentaryView else {
                return UICollectionReusableView()
            }
            footerView.setView(text: "")
            supplimentaryView = footerView
        }
        return supplimentaryView
    }
    
    func checkIfImageExist(imageIndex: Int) -> Bool{
        for selectedImageIndex in selectedImageIndexes{
            if selectedImageIndex == imageIndex{
                return true
            }
        }
        return false
    }
    
    // MARK: UICollectionViewDelegate
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: false)
        
        if indexPath.item < self.imagesInformation.count {
            if imagesInformation[indexPath.row].status!{
                imagesInformation[indexPath.row].status = false
            }else{
                imagesInformation[indexPath.row].status = true
            }
            self.collectionView?.reloadData()
        } else {
            if let cell = collectionView.cellForItem(at: indexPath) as? ImageCollectionViewCell {
                cell.selectionClosure()
            }
        }
    }
}

extension ProfileEditSelectedPhotosCollectionViewController: VIAImagePickerDelegate {
    func pass(imageInfo: [String: Any]) {
        self.emptyStateAddPhotoView?.performSelector(onMainThread: #selector(self.emptyStateAddPhotoView?.removeFromSuperview), with: nil, waitUntilDone: false)
    
        self.totalAttachments.append(imageInfo)
        
        self.collectionView?.reloadData()
        self.imagePicker?.dismissImagePicker()
        
        manipulateRightBarButton()
    }
    
    func manipulateRightBarButton() {
        if self.totalAttachments.count > 0 {
            self.navigationItem.rightBarButtonItem?.isEnabled = true
        }
    }
    
    func show(alert imageSouceSelector: UIAlertController) {
        self.present(imageSouceSelector, animated: true, completion: nil)
    }
    
    func present(imagePicker: UIImagePickerController) {
        self.present(imagePicker, animated: true, completion: nil)
    }
}

extension ProfileEditSelectedPhotosCollectionViewController: AddPhotoViewDelegate {
    
    func showPhotoPicker() {
        self.imagePicker?.selectImage(view: emptyStateAddPhotoView?.getAddPhotoButton())
    }
}


