//
//  ProfileProvideFeedbackViewController.swift
//  VitalityActive
//
//  Created by Von Kervin R. Tuico on 12/03/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import VIAUIKit
import VitalityKit
import VIAUtilities
import VIACommon
import RealmSwift

public struct FeedbackRequiredDataHolder {
    var selectedFeebackTypeId: String = ""
    var selectedFeedbackType: String = ""
    var userFeedback: String = ""
    var userEmailAddress: String = ""
    var userContactNumber: String = ""
    var memberFirstName: String = ""
    var memberLastName: String = ""
}

class ProfileProvideFeedbackViewController: VIATableViewController, PrimaryColorTintable {
    
    let profileViewModel = ProfileViewModel()
    let submissionHelper: PFEventsProcessor = ProfilePFSubmissionHelper()
    
    var totalAttachments: Array<Dictionary<String, Any>> = []
    
    public var dataHolder: FeedbackRequiredDataHolder?
    
    var selectedFeedbackType: String?
    var selectedFeedbackTypeIndex: Int?
    var selectedFeedbackTpyeId: String?
    var userFeedback: String?
    var userEmailAddress: String?
    var userContactNumber: String?
    fileprivate let MAX_CHARACTER_COUNT_FOR_EMAIL = 100
    fileprivate let MAX_CHARACTER_COUNT_FOR_MOBILE_NUMBER = 15
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = CommonStrings.Settings.FeedbackTitle906
        
        setupNavBar()
        configureTableView()
    }
    
    override public func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.configureKeyboardAutoToolbar(VIAApplicableFeatures.default.enableKeyboardAutoToolbar ?? false)
    }
    
    override public func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.disableKeyboardAutoToolbar()
    }
    
    func configureTableView() {
        self.tableView.register(VIATextViewProvideFeedbackViewCell.nib(), forCellReuseIdentifier: VIATextViewProvideFeedbackViewCell.defaultReuseIdentifier)
        self.tableView.register(VIAProfileDetailCell.nib(), forCellReuseIdentifier: VIAProfileDetailCell.defaultReuseIdentifier)
        self.tableView.register(VIATableViewSectionHeaderFooterView.nib(), forHeaderFooterViewReuseIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier)
        self.tableView.register(VIATextFieldTableViewCell.nib(), forCellReuseIdentifier: VIATextFieldTableViewCell.defaultReuseIdentifier)
        self.tableView.estimatedRowHeight = 96
        self.tableView.separatorInset = UIEdgeInsets(top: 0, left: 52, bottom: 0, right: 0)
        self.tableView.tableFooterView = UIView()
        self.tableView.backgroundColor = UIColor.tableViewBackground()
    }
    
    func setupNavBar() {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.barTintColor = .white
        self.navigationController?.makeNavigationBarTransparent()
        
        // Configure Submit Button
        self.addRightBarButtonItem(CommonStrings.Feedback.ActionSubmit1434, target: self, selector: #selector(onSubmit))
        self.navigationItem.rightBarButtonItem?.isEnabled = false
        
        self.hideBackButtonTitle()
    }
    
    /* Submit Button Action */
    func onSubmit(sender: UIBarButtonItem){
        self.showHUDOnWindow()
        
        let person = profileViewModel.currentPerson
        
        guard let feedbackTypeId = self.selectedFeedbackTpyeId else {
            return
        }
        
        guard let feedbackType = self.selectedFeedbackType else {
            return
        }
        
        guard let feedbackMessage = self.userFeedback else {
            return
        }
        
        guard let emailAddress = self.userEmailAddress else {
            return
        }
        
        let contactNumber = self.userContactNumber ?? ""
        
        dataHolder = FeedbackRequiredDataHolder(selectedFeebackTypeId: feedbackTypeId, selectedFeedbackType: feedbackType, userFeedback: feedbackMessage, userEmailAddress: emailAddress, userContactNumber: contactNumber, memberFirstName: person.givenName, memberLastName: person.familyName)
        
        /* Save user entered data to Realm */
        if let data = dataHolder {
            let realmList = DataProvider.newProvideFeedbackRealm()
            try! realmList.write {
                realmList.delete(realmList.allProvideFeedbackItems())
                
                // Map action values to Provide Feedback Selected Items
                let items = ProvideFeedbackItems()
                items.selectedFeedbackTypeId = data.selectedFeebackTypeId
                items.selectedFeedbackType = data.selectedFeedbackType
                items.userFeedback = data.userFeedback
                items.userEmailAddress = data.userEmailAddress
                items.userContactNumber = data.userContactNumber
                items.memberFirstName = data.memberFirstName
                items.memberLastName = data.memberLastName
                
                // Add Provide Feedback Items to local database
                realmList.add(items)
            }
            
            self.submissionHelper.sendFeedbackEmail(completion: { [weak self] error in
                DispatchQueue.main.async {
                    self?.hideHUDFromWindow()
                    guard error == nil else {
                        debugPrint("Error Message: \(error!)")
                        self?.handleErrorOccurred(error!)
                        return
                    }
                    self?.performSegue(withIdentifier: "showFeedbackCompletion", sender: nil)
                }
            })
        }
    }
    
    func handleErrorOccurred(_ error: Error) {
        let backendError = error as? BackendError ?? BackendError.other
        self.handleBackendErrorWithAlert(backendError)
    }
    
    // MARK: TableViewController
    
    enum Sections: Int, EnumCollection, Equatable {
        case feedback = 0
        case contact_details = 1
        
        func title() -> String? {
            switch self {
            case .feedback:
                return nil
            case .contact_details:
                return CommonStrings.Settings.FeedbackContactDetailsSectionHeading952
            }
        }
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return Sections.allValues.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch Sections.allValues[section] {
        case .feedback:
            return 3
        case .contact_details:
            return 2
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch Sections.allValues[indexPath.section] {
        case .feedback:
            return feedbackCell(at: indexPath)
        case .contact_details:
            return contactDetailsCell(at: indexPath)
        }
    }
    
    func feedbackCell(at indexPath: IndexPath) -> UITableViewCell {
        
        var cell: UITableViewCell?
        
        let lastRowIndex = self.tableView.numberOfRows(inSection: self.tableView.numberOfSections - 1)
        
        if indexPath.row == lastRowIndex {
            cell = self.tableView.dequeueReusableCell(withIdentifier: VIATextViewProvideFeedbackViewCell.defaultReuseIdentifier, for: indexPath)
            let feedbackCell = cell as! VIATextViewProvideFeedbackViewCell
            feedbackCell.contentTextView.delegate = self
            
            feedbackCell.headingText = CommonStrings.Settings.FeedbackSectionTitle949
            if let feedback = self.userFeedback, !feedback.isEmpty {
                feedbackCell.contentText = feedback
            } else {
                feedbackCell.contentText = CommonStrings.Settings.FeedbackSectionPlaceholder950
            }
            feedbackCell.selectionStyle = .none
        } else {
            cell = self.tableView.dequeueReusableCell(withIdentifier: VIAProfileDetailCell.defaultReuseIdentifier, for: indexPath)
            let contactDetailsCell = cell as! VIAProfileDetailCell
            let headingList = [CommonStrings.Settings.FeedbackSubjectTitle945, CommonStrings.Settings.FeedbackAttachmentsTitle947]
            let heading = headingList[indexPath.row]
            contactDetailsCell.labelText = heading
            
            if indexPath.row == 0, let feedbackType = self.selectedFeedbackType, !feedbackType.isEmpty {
                contactDetailsCell.valueText = feedbackType
            } else if indexPath.row == 1, !self.totalAttachments.isEmpty {
                contactDetailsCell.valueText = "\(self.totalAttachments.count) \(CommonStrings.Settings.FeedbackAttachmentsTitle947)"
            } else {
                let placeHolderList = [CommonStrings.Settings.FeedbackSubjectPlaceholder946, CommonStrings.Settings.FeedbackAttachmentsPlaceholder948]
                let placeHolder = placeHolderList[indexPath.row]
                contactDetailsCell.valueText = placeHolder
            }
            
            cell?.accessoryType = .disclosureIndicator
        }
        
        return cell!
    }
    
    func contactDetailsCell(at indexPath: IndexPath) -> UITableViewCell {
        var cell: UITableViewCell?
        
        let headingList = [CommonStrings.Settings.ProfileLandingEmailTitle919, CommonStrings.Settings.ProfileLandingMobileTitle915]
        let headingText = headingList[indexPath.row]
        
        if indexPath.row == 1 {
            
            if let enableContactNumberField = VIAApplicableFeatures.default.enableContactNumberField,
                enableContactNumberField {
                
                cell = self.tableView.dequeueReusableCell(withIdentifier: VIATextFieldTableViewCell.defaultReuseIdentifier, for: indexPath)
                let contactNumberCell = cell as! VIATextFieldTableViewCell
                contactNumberCell.setHeadingLabelText(text: headingText)
                
                let person = profileViewModel.currentPerson
                if let showMobileNumber = VIAApplicableFeatures.default.enableMobileNumber,
                    showMobileNumber, let phoneNumber = person.phoneNumbers.last {
                    contactNumberCell.setTextFieldText(text: "\(phoneNumber)")
                    self.userContactNumber = "\(phoneNumber)"
                } else {
                    /** TODO : Comment out if the higher peeps wants to show "Mobile"
                     contactNumberCell.setTextFieldPlaceholder(placeholder: ProfileStrings.Settings.ProfileLandingMobileTitle915)
                     */
                    contactNumberCell.setTextFieldPlaceholder(placeholder: "")
                }
                contactNumberCell.hideCellImage()
                contactNumberCell.textField.delegate = self
                contactNumberCell.setTextFieldKeyboardType(type: .phonePad)
            } else {
                cell = self.tableView.dequeueReusableCell(withIdentifier: VIAProfileDetailCell.defaultReuseIdentifier, for: indexPath)
                let contactNumberCell = cell as! VIAProfileDetailCell
                contactNumberCell.labelText = headingText
                
                let person = profileViewModel.currentPerson
                if let showMobileNumber = VIAApplicableFeatures.default.enableMobileNumber,
                    showMobileNumber, let phoneNumber = person.phoneNumbers.last {
                    contactNumberCell.valueText = "\(phoneNumber)"
                    self.userContactNumber = "\(phoneNumber)"
                } else {
                    /** Comment out if the higher peeps wants to show "Mobile" value
                     contactNumberCell.noValueText = ProfileStrings.Settings.ProfileLandingMobileTitle915
                     */
                    contactNumberCell.noValueText = ""
                }
            }
        } else {
            /* Show Editable Email If App is Sumitomo */
            if let enableContactNumberField = VIAApplicableFeatures.default.enableContactNumberField,
                enableContactNumberField {
                
                cell = self.tableView.dequeueReusableCell(withIdentifier: VIATextFieldTableViewCell.defaultReuseIdentifier, for: indexPath)
                let emailCell = cell as! VIATextFieldTableViewCell
                emailCell.setHeadingLabelText(text: headingText)
                
                let email = AppSettings.getLastLoginUsername()
                if !(email.isEmpty) {
                    emailCell.setTextFieldText(text: email)
                    self.userEmailAddress = email
                } else {
                    /** Comment out if the higher peeps wants to show "Email" value
                     emailCell.setTextFieldPlaceholder(placeholder: ProfileStrings.ProfileMailPlaceholder)
                     */
                    emailCell.setTextFieldPlaceholder(placeholder: "")
                }
                emailCell.hideCellImage()
                configureEmailTextFieldListener(emailCell, indexPath: indexPath)
            } else {
                cell = self.tableView.dequeueReusableCell(withIdentifier: VIAProfileDetailCell.defaultReuseIdentifier, for: indexPath)
                let emailCell = cell as! VIAProfileDetailCell
                emailCell.labelText = headingText
                
                let email = AppSettings.getLastLoginUsername()
                if !(email.isEmpty) {
                    emailCell.valueText = email
                    self.userEmailAddress = email
                } else {
                    /** Comment out if the higher peeps wants to show "Email" value
                     emailCell.noValueText = ProfileStrings.ProfileMailPlaceholder
                     */
                    emailCell.noValueText = ""
                }
            }
        }
        
        cell?.selectionStyle = .none
        return cell!
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let section = Sections(rawValue: indexPath.section) {
            
            switch section {
            case .feedback:
                if indexPath.row == 0 {
                    self.performSegue(withIdentifier: "showChooseFeedbackType", sender: nil)
                } else {
                    self.performSegue(withIdentifier: "showUploadAttachment", sender: nil)
                }
                break
            default:
                break
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let sections = Sections(rawValue: section)
        if sections == .contact_details {
            let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier) as! VIATableViewSectionHeaderFooterView
            view.labelText = sections?.title()
            view.set(textColor: .mediumGrey())
            return view
        }
        return nil
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let sections = Sections(rawValue: section)
        if sections == .feedback {
            let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier) as! VIATableViewSectionHeaderFooterView
            view.labelText = CommonStrings.Settings.FeedbackFormFootnoteMessage951
            view.set(textColor: .mediumGrey())
            return view
        } else if sections == .contact_details {
            let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier) as! VIATableViewSectionHeaderFooterView
            view.labelText = CommonStrings.Settings.FeedbackContactDetailsFootnote953
            view.set(textColor: .mediumGrey())
            return view
        }
        return nil
    }
    
    func checkAllRequiredValues() {
        
        guard self.selectedFeedbackType != nil else {
            self.navigationItem.rightBarButtonItem?.isEnabled = false
            return
        }
        
        guard self.userFeedback != nil else {
            self.navigationItem.rightBarButtonItem?.isEnabled = false
            return
        }
        
        guard self.userEmailAddress != nil else {
            self.navigationItem.rightBarButtonItem?.isEnabled = false
            return
        }
        
        if self.totalAttachments.isEmpty {
            self.navigationItem.rightBarButtonItem?.isEnabled = false
        } else {
            self.navigationItem.rightBarButtonItem?.isEnabled = true
        }
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == "showUploadAttachment", let profileUploadAttachmentCollectionViewController = segue.destination as? ProfileUploadAttachmentCollectionViewController {
            profileUploadAttachmentCollectionViewController.delegate = self
            profileUploadAttachmentCollectionViewController.totalAttachments = self.totalAttachments
        } else if segue.identifier == "showChooseFeedbackType", let profileChooseFeedbackTypeViewController = segue.destination as? ProfileChooseFeedbackTypeViewController {
            profileChooseFeedbackTypeViewController.delegate = self
            profileChooseFeedbackTypeViewController.selectedFeedbackType = self.selectedFeedbackType
            profileChooseFeedbackTypeViewController.selectedFeedbackTypeIndex = self.selectedFeedbackTypeIndex
            profileChooseFeedbackTypeViewController.selectedFeedbackTpyeId = self.selectedFeedbackTpyeId
        } else if segue.identifier == "showFeedbackCompletion", let profileFeedbackCompletionViewController = segue.destination as? ProfileFeedbackCompletionViewController {
            
        }
    }
}

extension ProfileProvideFeedbackViewController: ProfileUploadAttachmentCollectionViewControllerProtocol {
    
    // Update imagesInfo variable based on the changes in ProfileUploadAttachmentCollectionViewControllerProtocol, for persistent
    func updateTotalAttachments(totalAttachments: Array<Dictionary<String, Any>>){
        self.totalAttachments = totalAttachments
        checkAllRequiredValues()
        self.tableView.reloadData()
    }
}

extension ProfileProvideFeedbackViewController: ProfileChooseFeedbackTypeViewControllerProtocol {
    
    func updateSelectedFeedbackType(feedbackTypeId: String, feedbackType: String, index: Int) {
        self.selectedFeedbackTpyeId = feedbackTypeId
        self.selectedFeedbackType = feedbackType
        self.selectedFeedbackTypeIndex = index
        checkAllRequiredValues()
        self.tableView.reloadData()
    }
}

extension ProfileProvideFeedbackViewController: UITextViewDelegate {
    
    public func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == CommonStrings.Settings.FeedbackSectionPlaceholder950{
            textView.text = nil
        }else{
            textView.textColor = UIColor.black
        }
    }
    
    public func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = CommonStrings.Settings.FeedbackSectionPlaceholder950
            textView.textColor = UIColor.darkText
        }
    }
    
    public func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        let numberOfChars = newText.characters.count // for Swift use count(newText)
        self.userFeedback = newText
        if numberOfChars == 0 {
            if newText == "" {
                self.userFeedback = nil
            }
            checkAllRequiredValues()
        } else {
            checkAllRequiredValues()
        }
        return true
    }
}

extension ProfileProvideFeedbackViewController: UITextFieldDelegate {
    
    fileprivate func configureEmailTextFieldListener(_ cell: VIATextFieldTableViewCell, indexPath: IndexPath){
        cell.textFieldTextDidChange = { [unowned self] textField in
            
            // Holder for our error message
            var errorMessage: String? = nil
            self.userEmailAddress = nil
            
            // Create holder for the value of email
            if let email = textField.text {
                
                // Let's check if email is invalid
                if self.isValidEmail(email) && self.checkForDoubleByte(email) {
                    self.userEmailAddress = email
                } else {
                    errorMessage = CommonStrings.Settings.ProfileEmailErrorMessage925
                }
                
                // Let's check for the character count
                // This is the same as calling
                // if let hasExceeded = self?.hasExceedCharactersLimit(email) where hasExceeded
                if self.hasExceedCharactersLimit(email){
                    //errorMessage = "Maximum \(self.MAX_CHARACTER_COUNT_FOR_EMAIL) character limit reached"
                    errorMessage = CommonStrings.Settings.ProfileEmailErrorCharacterLimit926(String(self.MAX_CHARACTER_COUNT_FOR_EMAIL))
                }
            }
            
            self.checkAllRequiredValues()
            
            // Refresh the error message label in UI
            UIView.performWithoutAnimation {
                self.tableView.beginUpdates()
                cell.setErrorMessage(message: errorMessage)
                self.tableView.endUpdates()
            }
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let phoneNumber = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        self.userContactNumber = nil
        
        if self.isValidMobileNumber(phoneNumber) {
            
            self.userContactNumber = phoneNumber
            
            if self.hasMobileNumberExceedCharactersLimit(phoneNumber) {
                return false
            }
            
            return true
        }
        return false
    }
}

extension ProfileProvideFeedbackViewController {
    
    func hasExceedCharactersLimit(_ token:String) -> Bool{
        return token.characters.count > MAX_CHARACTER_COUNT_FOR_EMAIL
    }
    
    func hasMobileNumberExceedCharactersLimit(_ phoneNumber:String) -> Bool{
        return phoneNumber.characters.count > MAX_CHARACTER_COUNT_FOR_MOBILE_NUMBER
    }
    
    func isValidEmail(_ token:String?) -> Bool {
        let pattern = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        //TO DO: ASK
        let predicate = NSPredicate(format:"SELF MATCHES %@", pattern)
        return predicate.evaluate(with: token)
    }
    
    func isValidMobileNumber(_ value: String) -> Bool {
        let invalidCharacters = NSCharacterSet.decimalDigits.inverted
        if value.rangeOfCharacter(from: invalidCharacters) == nil {
            return true
        }
        return false
    }
    
    func checkForDoubleByte(_ token:String) -> Bool{
        
        let buf = token.utf8.count
        if buf == token.characters.count {
            return true
        }
        
        return false
    }
}
