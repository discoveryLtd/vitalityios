//
//  ProfileFeedbackCompletionViewController.swift
//  VitalityActive
//
//  Created by Von Kervin R. Tuico on 16/03/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import VIAUIKit
import VitalityKit
import Photos
import VIACommon

class ProfileFeedbackCompletionViewController: UIViewController, PrimaryColorTintable {
    
    var completionView: AddPhotoView?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        initUI()
        initNavigationBar()
    }
    
    func initUI() {
        completionView = AddPhotoView.viewFromNib(owner: self)
        completionView?.setHeader(title: CommonStrings.Settings.FeedbackSubmittedTitle963)
        completionView?.setDetail(message: CommonStrings.Settings.FeedbackSubmittedMessage964)
        completionView?.setAction(title: CommonStrings.DoneButtonTitle53)
        //TODO: REVERT
//        completionView?.setHeader(image: VIACoreAsset.Feedback.feedbackSubmitted.templateImage)
        completionView?.frame = self.view?.frame ?? CGRect(x: 0, y: 0, width: 0, height: 0)
        completionView?.getAddPhotoButton().addTarget(self, action: #selector(onDoneClick), for: .touchUpInside)
        
        if completionView != nil {
            self.view.addSubview(completionView!)
        }
    }
    
    func initNavigationBar() {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.backgroundColor = UIColor.white
        self.hideBackButtonTitle()
        self.navigationItem.setHidesBackButton(true, animated:true);
    }
    
    func onDoneClick() {
        self.performSegue(withIdentifier: "unwindToProfileSettingsViewController", sender: self)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
