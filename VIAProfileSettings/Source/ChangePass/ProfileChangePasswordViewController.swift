//
//  ProfileChangePasswordViewController.swift
//  VitalityActive
//
//  Created by Steven Layug on 11/23/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

import VitalityKit
import VIAUIKit
import VIAUtilities
import VIAActiveRewards
import VIACommon
import SwiftKeychainWrapper
import RNCryptor

public class ProfileChangePasswordViewController: VIATableViewController {
    
    var passwordChangeView: PasswordChangeView?
    var currentPassword = ""
    var newPassword = ""
    var confirmedPassword = ""
    var valid = true
    var newPasswordTextField: VIATextFieldTableViewCell!
    var confirmedPasswordTextField: VIATextFieldTableViewCell!
    var newPasswordError = CommonStrings.Registration.PasswordFieldFootnote29
    var confirmPasswordError = CommonStrings.Registration.MismatchedPasswordFootnoteError1137
    var loginHasFailedAtLeastOnce = false
    
    // TODO: Refactor the implementation on the number of character required.
    let MIN_CHARACTERS = 7
    
    enum Rows: Int, EnumCollection{
        case NewPasswordRow = 0
        case ConfirmPasswordRow = 1
    }
    
    enum Sections: Int, EnumCollection {
        case OldPasswordSec = 0
        case NewPasswordSec = 1
    }
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = CommonStrings.Settings.SecurityChangePasswordTitle827
        setupNavBar()
        configureTableView()
        configureAppearance()
    }
    
    func configureAppearance() {
        self.tableView.backgroundColor = UIColor.tableViewBackground()
    }
    
    func configureTableView() {
        self.tableView.register(VIATableViewSectionHeaderFooterView.nib(), forHeaderFooterViewReuseIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier)
        self.tableView.register(VIATextFieldTableViewCell.nib(), forCellReuseIdentifier: VIATextFieldTableViewCell.defaultReuseIdentifier)
        self.tableView.estimatedRowHeight = 75
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        self.tableView.tableFooterView = UIView()
    }
    
    func setupNavBar() {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.barTintColor = .white
        self.navigationController?.makeNavigationBarTransparent()
        self.hideBackButtonTitle()
        
        //Set bar button items
        self.addLeftBarButtonItem(CommonStrings.CancelButtonTitle24,
                                  target: self, selector: #selector(onCancelPressed))
        
        self.addRightBarButtonItem(CommonStrings.DoneButtonTitle53,
                                   target: self, selector: #selector(onDonePressed))
        
        self.navigationItem.rightBarButtonItem?.isEnabled = false
    }
    
    func resetValues(){
        self.currentPassword = ""
        self.newPassword = ""
        self.confirmedPassword = ""
    }
    
    override public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier) as! VIATableViewSectionHeaderFooterView
        view.labelText = " "
        return view
    }
    
    override public func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
    
    public override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    public override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let section = Sections(rawValue: section)
        
        if section == .OldPasswordSec {
            return 1
        }
            
        else if section == .NewPasswordSec {
            return 2
        }
        
        return 0
    }
    
    public override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let section = Sections(rawValue: indexPath.section)
        let row     = Rows(rawValue: indexPath.row)
        let cell    = tableView.dequeueReusableCell(withIdentifier: VIATextFieldTableViewCell.defaultReuseIdentifier, for: indexPath) as! VIATextFieldTableViewCell
        
        if (section == .OldPasswordSec){
            let _ = self.setupField(cell, heading: CommonStrings.Settings.ChangePassword1Title995,
                                    placeholder: CommonStrings.Settings.ChangePassword1Placeholder996)
            self.setCurrentPasswordListener(cell)
        }
            
        else if (section == .NewPasswordSec){
            
            if (row == .NewPasswordRow) {
                self.newPasswordTextField = self.setupField(cell, heading: CommonStrings.Settings.ChangePassword2Title997,
                                                            placeholder: CommonStrings.Settings.ChangePassword2Placeholder998)
                self.setNewPasswordListener(cell, maxChar: self.MIN_CHARACTERS, error: self.newPasswordError)
            }
            else if (row == .ConfirmPasswordRow){
                self.confirmedPasswordTextField = self.setupField(cell, heading: CommonStrings.Settings.ChangePassword3Placeholder1000,
                                                                  placeholder: CommonStrings.Settings.ChangePassword3Title2387)
                self.setConfirmPasswordListener(cell, error: self.confirmPasswordError)
            }
        }
        
        return cell
        
    }
    
    func onCancelPressed(){
        self.navigationController?.popViewController(animated: true)
    }
    
    func onDonePressed() {
        changePassword()
    }
    
    func showForgotPassword(_ sender: UIButton?) {
        self.performSegue(withIdentifier: "showForgotPassword", sender: nil)
    }
    
}

extension ProfileChangePasswordViewController{

    func changePassword(){
        //let realm = DataProvider.newRealm()
        
        //Retrieve first email from email addresses list
        let email = AppSettings.getLastLoginUsername()
        
        //Prepare Request Param
        let request = ChangePasswordParam(existingPassword: currentPassword, newPassword: newPassword, userName: email)
        
        //Call web service for change password
        self.showHUDOnView(view: self.view)
        Wire.Party.changePassword(request: request) { [weak self] (success, response) in
            DispatchQueue.main.async{
                self?.hideHUDFromView(view: self?.view)
                if success{
                    self?.updatePasswordOnKeychain()
                    self?.openSuccessPage()
                }else{
                    self?.displayEmailChangeConfirmationView(errorCode: response)
                    
                }
            }
        }
    }
    
    
    func updatePasswordOnKeychain() {
        //Convert User Password to Base64 and convert to Data
        let passwordToBase64 = NSData(base64Encoded: (self.newPassword.toBase64()), options: NSData.Base64DecodingOptions.ignoreUnknownCharacters)
        
        //Encrypt User Password
        let encryptedPassword = RNCryptor.encrypt(data: passwordToBase64! as Data, withPassword: KeychainWrapper.standard.string(forKey: "encryptionKey")!)
        
        //Store Encrypted Password
        KeychainWrapper.standard.set(encryptedPassword, forKey: "encryptedPassword")
    }
    
    func openSuccessPage(){
        self.performSegue(withIdentifier: "showChangePasswordConfirmation", sender: self)
    }
    
    override public func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? PasswordChangeConfirmationViewController{
            vc.changePasswordDelegate = self
        }
    }
    
    fileprivate func displayEmailChangeConfirmationView(errorCode: Int){
        enum errorCodes: Int, EnumCollection {
            case failError = 400
            case firstLockoutError  = 40000
            case secondLockoutError = 423
        }
        
        let error = errorCodes(rawValue: errorCode)
        
        switch error {
        case .failError?:
            let title = CommonStrings.Settings.AlertUnableToChangePasswordTitle1162
            let message = CommonStrings.Login.IncorrectEmailPasswordErrorMessage48
            
            let controller = UIAlertController(title: title,
                                               message: message,
                                               preferredStyle: .alert)
            if self.loginHasFailedAtLeastOnce {
                let forgotPassword = UIAlertAction(title: CommonStrings.Login.ForgotPasswordButtonTitle22, style: .default) {
                    [weak self] action in
                    self?.showForgotPassword(nil)
                }
                controller.addAction(forgotPassword)
            }
            
            let okay = UIAlertAction(title: CommonStrings.OkButtonTitle40, style: .cancel, handler: nil)
            controller.addAction(okay)
            self.loginHasFailedAtLeastOnce = true
            self.present(controller, animated: true, completion: nil)
        
        case .firstLockoutError?:
            let title = CommonStrings.Settings.AlertUnableToChangePasswordTitle1162
            let message = CommonStrings.Login.AccountLockedErrorAlertMessage738
            
            let controller = UIAlertController(title: title,
                                               message: message,
                                               preferredStyle: .alert)
            
            let resetPassword = UIAlertAction(title: CommonStrings.Login.ResetPasswordButtonTitle22, style: .default) {
                [weak self] action in
                self?.showForgotPassword(nil)
            }
            controller.addAction(resetPassword)
            
            let okay = UIAlertAction(title: CommonStrings.OkButtonTitle40, style: .cancel, handler: nil)
            controller.addAction(okay)
            self.present(controller, animated: true, completion: nil)
            
        case .secondLockoutError?:
            let title = CommonStrings.Settings.AlertUnableToChangePasswordTitle1162
            let message = CommonStrings.Login.AccountLockedErrorAlertMessage738
            
            let controller = UIAlertController(title: title,
                                               message: message,
                                               preferredStyle: .alert)
            
            let resetPassword = UIAlertAction(title: CommonStrings.Login.ResetPasswordButtonTitle22, style: .default) {
                [weak self] action in
                        self?.showForgotPassword(nil)
                    }
            controller.addAction(resetPassword)
            
            let okay = UIAlertAction(title: CommonStrings.OkButtonTitle40, style: .cancel, handler: nil)
            controller.addAction(okay)
            self.present(controller, animated: true, completion: nil)
            
        default:
            let title = CommonStrings.Settings.AlertUnableToChangePasswordTitle1162
            let message =  CommonStrings.Login.IncorrectEmailPasswordErrorMessage48
            showAlertDialog(title: title, message: message)
        }
    }
    
    fileprivate func showAlertDialog(title:String? = nil, message:String? = nil) {
        //,alertActions:[UIAlertAction]){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let okay = UIAlertAction(title: CommonStrings.OkButtonTitle40, style: .default, handler: nil)
        //for alertAction in alertActions{
        alert.addAction(okay)
        //}
        alert.view.tintColor = UIButton().tintColor
        self.present(alert, animated: true, completion: nil)
    }
}


// Utilities
extension ProfileChangePasswordViewController {
    
    fileprivate func setCurrentPasswordListener(_ cell: VIATextFieldTableViewCell) {
        cell.textFieldTextDidChange = { [weak self] textField in
            textField.clearButtonMode = .whileEditing
            
            if let token = textField.text {
                self?.currentPassword = token
                if (self?.passwordsValidityChecker() ?? false) {
                    self?.enableDoneButton(true)
                } else {
                    self?.enableDoneButton(false)
                }
            }
        }
    }
    
    fileprivate func setNewPasswordListener(_ cell: VIATextFieldTableViewCell, maxChar: Int, error: String){
        cell.textFieldTextDidChange = { [weak self] textField in
            var errorMessage: String? = nil
            textField.clearButtonMode = .whileEditing
            self?.confirmedPassword = ""
            
            /**
             * Clear 'confirm password' textfield everytime there is a change in the 'new password' textfield
             * to reconfirm the updated 'new password' textfield.text
             * https://jira.vitalityservicing.com/browse/VA-32696
             * similar to: https://jira.vitalityservicing.com/browse/VA-31717
             **/
            if let confirmedPasswordString = self?.confirmedPasswordTextField.textField.text {
                if !confirmedPasswordString.isEmpty {
                    self?.confirmedPasswordTextField.textField.text = ""
                    let indexPath = IndexPath(item: Rows.ConfirmPasswordRow.rawValue, section: Sections.NewPasswordSec.rawValue)
                    self?.tableView.reloadRows(at: [indexPath], with: .automatic)
                }
            }

            if let token = textField.text {
                if token.characters.count < maxChar || !(self?.valid(token))! {
                    errorMessage = error
                    self?.enableDoneButton(false)
                } else {
                    errorMessage = nil
                    if (self?.passwordsValidityChecker())! {
                        self?.enableDoneButton(true)
                    } else{
                        self?.enableDoneButton(false)
                    }
                }
                self?.newPassword = token
            } else {
                errorMessage = error
                self?.enableDoneButton(false)
            }
            
            UIView.performWithoutAnimation {
                self?.tableView.beginUpdates()
                cell.setErrorMessage(message: errorMessage)
                self?.confirmedPasswordTextField.setErrorMessage(message: nil)
                //self.confirmedPasswordTextField.setTextFieldText(text: nil)
                self?.tableView.endUpdates()
            }
        }
    }
    
    fileprivate func setConfirmPasswordListener(_ cell: VIATextFieldTableViewCell, error: String){
        cell.textFieldTextDidChange = { [weak self] textField in
            var errorMessage: String? = nil
            textField.clearButtonMode = .whileEditing
            
            if let confirmPassword = textField.text, confirmPassword == self?.newPassword {
                errorMessage = nil
                self?.confirmedPassword = confirmPassword
                if (self?.passwordsValidityChecker())! {
                    self?.enableDoneButton(true)
                } else {
                    self?.enableDoneButton(false)
                }
            } else {
                errorMessage = error
                self?.enableDoneButton(false)
            }
            
            UIView.performWithoutAnimation {
                self?.tableView.beginUpdates()
                cell.setErrorMessage(message: errorMessage)
                self?.tableView.endUpdates()
            }
        }
    }
    
    fileprivate func setupField(_ cell: VIATextFieldTableViewCell, heading:String, placeholder: String) -> VIATextFieldTableViewCell{
        cell.separatorInset = UIEdgeInsetsMake(0, 55, 0, 0);
        //TODO: REVERT
//        cell.cellImageConfig.templateImage = VIACoreAsset.LoginAndRegistration.loginPassword.image
        cell.setTextFieldPlaceholder(placeholder: placeholder)
        cell.setHeadingLabelText(text: heading)
        cell.selectionStyle = .none
        cell.setTextFieldSecureTextEntry(secureTextEntry: true)
        cell.setTextFieldKeyboardType(type: .default)
        
        return cell
    }
    
    fileprivate func enableDoneButton(_ enable:Bool){
        self.navigationItem.rightBarButtonItem?.isEnabled = enable
    }
    func passwordsValidityChecker() -> Bool {
        if valid(currentPassword) && valid(newPassword) && valid(confirmedPassword){
            return true
        }
        return false
    }
    func passwordChecker(newPassword:String, confirmedPassword:String) -> Bool {
        
        if valid(currentPassword) && valid(newPassword) && valid(confirmedPassword){
            return newPassword == confirmedPassword
        }
        
        return false
    }
    
    fileprivate func valid(_ token: String) -> Bool{
        return token.isValidPassword()
            && !token.hasDoubleByte()
            && !token.stringContainsSpecialCharacters()
    }
}

extension ProfileChangePasswordViewController: ChangePasswordDelegate {
    
    func onFinish() {
        self.navigationController?.popViewController(animated: true)
    }
}
