//
//  PasswordChangeConfirmationViewController.swift
//  VitalityActive
//
//  Created by Steven Layug on 11/28/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import VIAUIKit
import VitalityKit
import VIAUtilities
import VitalityKit

protocol ChangePasswordDelegate{
    func onFinish()
}
class PasswordChangeConfirmationViewController: VIAViewController {
    // TODO: to be replaced
    var changePasswordDelegate: ChangePasswordDelegate?
    
    var passwordChangeView: PasswordChangeView?
        
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideBackButtonTitle()
        configureView()
    }
    
    override public func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        configureAppearance()
    }
    
//    override public func viewDidDisappear(_ animated: Bool) {
//        self.removeFromParentViewController()
//    }
    
    func configureAppearance() {
        self.navigationController?.isNavigationBarHidden = true;
        self.navigationController?.makeNavigationBarTransparent()
    }
    
    func configureView(){
        let passwordChangeView = PasswordChangeView.viewFromNib(owner: self)
        // TODO replace this CoreAsset
        //TODO: REVERT
//        passwordChangeView!.setHeader(image: UIImage.templateImage(asset: .loginPassword))
        // TODO replace this static strings
        passwordChangeView!.setHeader(title: CommonStrings.Settings.PasswordChangedTitle943)
        passwordChangeView!.setDetail(message: CommonStrings.Settings.PasswordChangedMessage944)
        passwordChangeView!.setAction(title: CommonStrings.DoneButtonTitle53)
        passwordChangeView!.frame = self.view?.frame ?? CGRect(x: 0, y: 0, width: 0, height: 0)
        passwordChangeView!.set(passwordChangeViewDelegate: self)
        
        self.view.addSubview(passwordChangeView!)
        self.passwordChangeView = passwordChangeView
    }
    
//    override func viewWillDisappear(_ animated: Bool) {
//        super.viewWillDisappear(animated)
//        // Show the navigation bar on other view controllers
//        self.navigationController?.setNavigationBarHidden(false, animated: false)
//    }
    
}

extension PasswordChangeConfirmationViewController: PasswordChangeViewDelegate {
    func doneButtonPressed() {
        self.performSegue(withIdentifier: "unwindToProfileSecurityViewController", sender: self)
    }
}
