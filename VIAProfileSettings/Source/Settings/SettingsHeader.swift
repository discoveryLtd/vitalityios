//
//  SettingsHeader.swift
//  VitalityActive
//
//  Created by Simon Stewart on 10/10/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import VIAUIKit

public class SettingsHeader : UITableViewCell, Nibloadable {
    
    var clickHandler:() -> (Void)? = {
        debugPrint("didSelectButton")
    }

    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var detailsLabel: UILabel!
    
    @IBOutlet weak var actionButton: UIButton!
    override public func awakeFromNib() {
        super.awakeFromNib()
        setupCell()
    }
    public override func prepareForReuse() {
        super.prepareForReuse()
    }
    func setupCell() {
        setupTitleLabel()
        setupDetailsLabel()
        setupButton()
    }
    func setupTitleLabel() {
        self.titleLabel.text = nil
        self.titleLabel.font = UIFont.title1Font()
        self.titleLabel.textColor = UIColor.night()
    }
    
    @IBAction func buttonClicked(_ sender: Any) {
        clickHandler()
    }
    func setupDetailsLabel() {
        self.detailsLabel.text = nil
        self.detailsLabel.font = UIFont.bodyFont()
        self.detailsLabel.textColor = UIColor.night()
    }
    func setupButton() {
        self.actionButton.setTitle(nil, for: .normal)
        self.actionButton.titleLabel?.font = UIFont.bodyFont()
        self.actionButton.titleLabel?.textColor = UIColor.vitalityOrange()
    }
}
extension SettingsHeader {
    
    public func setTitle(_ title: String) {
        self.titleLabel.text = title
    }
    
    public func setDetailText(_ detailText: String) {
        self.detailsLabel.text = detailText
    }
    
    public func setActionButton(title: String) {
        self.bringSubview(toFront: self.actionButton)
        self.actionButton.setTitle(title, for: .normal)
    }
    public func setActionForButton(_ closure: @escaping () -> Void?) {
        self.clickHandler = closure
    }
}
