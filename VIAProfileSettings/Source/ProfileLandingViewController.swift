//
//  ProfileLandingViewController.swift
//  VitalityActive
//
//  Copyright © 2017 Glucode. All rights reserved.
//

import VIAUIKit
import VitalityKit
import VIAUtilities
import VIACommon
import VIALoginRegistration

class ProfileLandingViewController: VIACoordinatedTableViewController, PrimaryColorTintable {

    // MARK: Private

    private var flowController = ProfileFlowCoordinator()

    // MARK: Public

    
    // Mark: Initializers
    var viewModel: ProfileViewModel {
        get {
            return flowController.viewModel
        }
    }

    required init?(coder aDecoder: NSCoder) {
        
        super.init(coder: aDecoder)
        self.setupTabBarTitle()
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        self.setupTabBarTitle()
    }
    
    // MARK: Lifecycle
    override public func viewDidLoad() {
        super.viewDidLoad()

        self.title = CommonStrings.Settings.LandingTitle901
        self.hideBackButtonTitle()

        flowController.configureController(vc: self)
        self.configureAppearance()
        self.configureTableView()
    }

    override public func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        self.tableView.reloadData()
    }
    
    func setupTabBarTitle() {
        
        self.title = CommonStrings.MenuProfileButton9
        self.tabBarItem.title = CommonStrings.MenuProfileButton9
        self.navigationController?.tabBarItem.title = CommonStrings.MenuProfileButton9
        
    }

    func configureAppearance() {
        tableView.tableFooterView = UIView()
    }

    func configureTableView() {
        self.tableView.register(VIAUserBasicInfoCell.nib(), forCellReuseIdentifier: VIAUserBasicInfoCell.defaultReuseIdentifier)
        self.tableView.register(VIATableViewCell.self, forCellReuseIdentifier: VIATableViewCell.defaultReuseIdentifier)
        self.tableView.register(VIATableViewSectionHeaderFooterView.nib(), forHeaderFooterViewReuseIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier)
        self.tableView.estimatedRowHeight = 75
    }

    // MARK: UITableView data source

    enum Sections: Int, EnumCollection {
        case UserInfo = 0
        case Menu = 1

        static func heightForHeader(section: Int) -> CGFloat {
            if Sections(rawValue: section) == .UserInfo {
                return CGFloat.leastNormalMagnitude
            }
            return UITableViewAutomaticDimension
        }
    }

    enum MenuItems: Int, EnumCollection {
        case MembershipPass = 0
        case Settings       = 1
        case Feedback       = 2
        case Help           = 3
        case Terms          = 4
        case Rate           = 5
        case Logout         = 6
        

        func title() -> String? {
            switch self {
            case .MembershipPass:
                if let hideMembershipPassAndUpdateTitle = VIAApplicableFeatures.default.hideMembershipPassAndUpdateTitle,
                    hideMembershipPassAndUpdateTitle{
                        return CommonStrings.ProfileMembershipDetailsTitle
                    }else{
                        return CommonStrings.Settings.MembershipPass911
                }
            case .Settings:
                return CommonStrings.Settings.LandingSettingsTitle899
            case .Feedback:
                return CommonStrings.Settings.FeedbackTitle906
            case .Help:
                return CommonStrings.HelpButtonTitle141
            case .Rate:
                return CommonStrings.Settings.RateUsTitle907
            case .Terms:
                return CommonStrings.Settings.TermsConditionsTitle905
            case .Logout:
                return CommonStrings.Settings.LogoutTitle908
            }
        }
        
        func image() -> UIImage? {
            //TODO REVERT
            return nil
//            switch self {
//            case .MembershipPass:
//                return VIACoreAsset.Profile.profileMembershipPass.templateImage
//            case .Settings:
//                return VIACoreAsset.Profile.profileSettings.templateImage
//            case .Feedback:
//                return UIImage.templateImage(asset: .profileProvideFeedback)
//            case .Help:
//                return UIImage.templateImage(asset: .helpGeneric)
//            case .Rate:
//                return UIImage.templateImage(asset: .profileRateUs)
//            case .Terms:
//                return UIImage.templateImage(asset: .profileTermsAndCondition)
//            case .Logout:
//                return VIACoreAsset.Profile.profileLogout.templateImage
//            }
        }
        
        func segueIdentifier() -> String {
            switch self {
            case .MembershipPass:
                return "showMembershipPass"
            case .Settings:
                return "showSettings"
            case .Terms:
                return "showTermsAndConditions"
            case .Rate:
                return "showRate"
            case .Feedback:
                if VIAApplicableFeatures.default.shouldPullProvideFeedbackContentFromCMS!{
                    return "showProvideFeedbackCMS"
                }
                else{
                    return "showProvideFeedback"
                }
            case .Help:
                return "showHelp"
            case .Logout:
                return ""
            }
        }
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return Sections.allValues.count
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let section = Sections(rawValue: section)
        if section == .Menu {
            if (VIAApplicableFeatures.default.changeMenuItemLocation)!{
                return MenuItems.allValues.count
            }else{
                return MenuItems.allValues.count - 5
            }
            
        } else {
            return 1
        }
    }

    // MARK: UITableView delegate

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let section = Sections(rawValue: indexPath.section)

        var cell = tableView.dequeueReusableCell(withIdentifier: VIATableViewCell.defaultReuseIdentifier, for: indexPath)

        if section == .Menu {
            cell = configureMenuCell(tableView, indexPath: indexPath)
        } else if section == .UserInfo {
            cell = configureUserInfoCell(tableView, indexPath: indexPath)
        }
        cell.accessoryType = .disclosureIndicator

        return cell
    }

    func configureUserInfoCell(_ tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: VIAUserBasicInfoCell.defaultReuseIdentifier, for: indexPath) as! VIAUserBasicInfoCell

        let person = viewModel.currentPerson
        cell.name = "\(person.givenName) \(person.familyName)"

        cell.email = AppSettings.getLastLoginUsername()

        ProfileImageHelper.getFile(partyId: DataProvider.newRealm().getPartyId()) { (image) in
            if nil != image{
                cell.photo = image
            }
        }        
        
        cell.accessoryType = .disclosureIndicator

        return cell
    }

    func configureMenuCell(_ tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: VIATableViewCell.defaultReuseIdentifier, for: indexPath)

        let menuItem = MenuItems(rawValue: indexPath.row)
        cell.textLabel?.text = menuItem?.title()
        cell.imageView?.image = menuItem?.image()
        cell.imageView?.tintColor = UIColor.currentGlobalTintColor()
        cell.accessoryType = .disclosureIndicator

        return cell
    }

    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let section = Sections(rawValue: section)

        if section == .UserInfo {
            return nil
        }

        let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier) as! VIATableViewSectionHeaderFooterView
        view.labelText = " " // Is there a better way of keeping the header height currently?
        return view
    }

    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return Sections.heightForHeader(section: section)
    }

    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)

        let section = Sections(rawValue: indexPath.section)

        if section == .UserInfo {
            self.performSegue(withIdentifier: "showEditProfile", sender: nil)
        } else if section == .Menu {
            let menuItem = MenuItems(rawValue: indexPath.row)
            if menuItem == .Logout {
                logout()
            }
            else if menuItem == .Rate {
                let appId = Bundle.main.object(forInfoDictionaryKey: "VAiTunesIdentifier") as? String ?? "Unknown"
                doRateApp(appId: appId)
            }
            else{
                guard let segueIdentifier = menuItem?.segueIdentifier() else { return }
                self.performSegue(withIdentifier: segueIdentifier, sender: nil)
            }
            
        }
    }

    func logout() {

        let alert = UIAlertController(title: CommonStrings.Settings.AlertLogoutTitle909, message: CommonStrings.Settings.AlertLogoutMessage910, preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: CommonStrings.CancelButtonTitle24, style: UIAlertActionStyle.cancel, handler: { (UIAlertAction) in
            alert.dismiss(animated: false, completion: nil)
        })
        let continueAction = UIAlertAction(title: CommonStrings.Settings.LogoutTitle908, style: UIAlertActionStyle.default, handler: { (UIAlertAction) in
            alert.dismiss(animated: false, completion: nil)
            VIALoginViewController.showLogin()
        })
        alert.view.tintColor = UIButton().tintColor
        alert.addAction(cancelAction)
        alert.addAction(continueAction)
        self.present(alert, animated: true, completion: nil)

    }
    
    func doRateApp(appId: String) {
        
        if let url = URL(string: "https://itunes.apple.com/app/id\(appId)") {
            
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: { (success) in
                    print("User sent to rate \(url): with \(success)")
                })
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == MenuItems.Terms.segueIdentifier() {
            //TODO REVERT
//                        self.setBackButtonTitle(ProfileStrings.SettingsLandingTitle)
                        if let webContentViewController = segue.destination as? VIAWebContentViewController,
                            let articleId = AppConfigFeature.contentId(for: .TermsAndConditionsContent) {
            
                            let webContentViewModel = VIAWebContentViewModel(articleId: articleId)
                            webContentViewController.viewModel = webContentViewModel
                            webContentViewController.title = CommonStrings.Settings.TermsConditionsTitle905
            }
        } else if segue.identifier == MenuItems.Feedback.segueIdentifier() {
            self.hideBackButtonTitle()
            let webContentViewController = segue.destination as? VIAWebContentViewController
            
            let articleId = "profile-settings-feedback"
            
            let webContentViewModel = VIAWebContentViewModel(articleId: articleId)
            webContentViewController?.viewModel = webContentViewModel
            webContentViewController?.title = CommonStrings.Settings.FeedbackSectionTitle949
        } else if segue.identifier == MenuItems.Rate.segueIdentifier() {
            self.hideBackButtonTitle()
        } else {
        
        }
    }
}

extension ProfileLandingViewController{
    
    fileprivate func downloadProfileImage(){
        ProfileImageHelper.getFile(partyId: DataProvider.newRealm().getPartyId()) { [weak self] (image) in
            self?.tableView.reloadData()
        }
    }
}
