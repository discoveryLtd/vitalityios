//
//  ProfileMembershipInfoViewController.swift
//  VitalityActive
//
//  Created by Ma. Catherine Purganan on 10/11/2017.
//  Copyright © 2017 Glucode. All rights reserved.
//

import VitalityKit
import VIAUIKit
import VIAUtilities
import VIACommon

class ProfileMembershipInfoViewController: UIViewController {

    @IBOutlet private weak var membershipInfoLabel: UILabel!
    public var informationText: String = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.membershipInfoLabel.text = informationText
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
