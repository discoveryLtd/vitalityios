//
//  ProfilePrivacyViewController.swift
//  VitalityActive
//
//  Created by Simon Stewart on 10/10/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

import VIAUIKit
import VitalityKit
import VIACommon
import VIAUtilities
import VIALoginRegistration
import VIAPreferences
import VIAFirstTimePreferences

public class ProfilePrivacyViewController : VIATableViewController {
    
    enum PrivacyItems: Int, EnumCollection {
        case Title = 0
        case VitalityStatus = 1
        case Analytics = 2
        case CrashReports = 3
    }
    
    var analyticsPref = AnalyticsPreference()
    var crashPref = CrashReportPreference()
    var vitalityStatusPref = ShareVitalityStatusPreference()
    let realm = DataProvider.newRealm()
    let headerIdentifier = "header"
    let cellIdentifier = "cell"
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        setupNavBar()
        configureTableView()
    }
    func configureTableView() {
        self.tableView.register(SettingsHeader.nib(), forCellReuseIdentifier: headerIdentifier)
        self.tableView.register(FirstTimePreferenceSwitchCell.nib(), forCellReuseIdentifier: cellIdentifier)
        self.tableView.estimatedRowHeight = 200
        self.tableView.separatorInset = UIEdgeInsets(top: 0, left: 52, bottom: 0, right: 0)
        self.tableView.backgroundColor = .white
        self.tableView.tableFooterView = UIView()
        self.removeFooterHeaderSpaces()
    }
    
    func setupNavBar() {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        
        self.hideBackButtonTitle()
    }
    
    func removeFooterHeaderSpaces() {
        self.tableView.estimatedSectionHeaderHeight = 0
        self.tableView.estimatedSectionFooterHeight = 0
    }
    
    public override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    public override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return PrivacyItems.allValues.count
    }
    
    public override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        // Remove Vitality Status for non-UKE builds
        if let showShareVitalityStatus = VIAApplicableFeatures.default.showShareVitalityStatus,!showShareVitalityStatus, indexPath.row == 1 {
            return 0
        } else {
            return UITableViewAutomaticDimension
        }
    }
    
    public override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if (segue.identifier == "showPrivacyStatement"){
            if let webContentViewController = segue.destination as? VIAWebContentViewController,
                
            let articleId = AppConfigFeature.contentId(for: .userPreferencesPrivacyPolicy) {
                
                let webContentViewModel = VIAWebContentViewModel(articleId: articleId)
                webContentViewController.viewModel = webContentViewModel
                // Temporary since token needs to be added on PhraseApp for UKE
                if let appIsUke = VIAApplicableFeatures.default.showShareVitalityStatus, appIsUke{
                    webContentViewController.title = "Privacy statement"
                } else {
                    webContentViewController.title = CommonStrings.UserPrefs.PrivacyGroupHeaderLinkButtonTitle72
                }
           }
        } else {
            if let webContentViewController = segue.destination as? VIAWebContentViewController,
            
            let articleId = AppConfigFeature.contentId(for: .vitalityStatusConsentContent) {
                
                let webContentViewModel = VIAWebContentViewModel(articleId: articleId)
                webContentViewController.viewModel = webContentViewModel
                webContentViewController.title = CommonStrings.CommunicationPrefButtonStatus377
            }
        }
    }
    
    func configSwitchCell(_ switchCell: FirstTimePreferenceSwitchCell, dataObject: PreferenceDataObject, completion: @escaping (_:UISwitch)->Void) -> FirstTimePreferenceSwitchCell {
        switchCell.setupCell()
        switchCell.actionButton.isHidden = true
        if let image = dataObject.preferenceImage {
            switchCell.setImage(asset: image)
        }
        if let title = dataObject.preferenceTitle {
            switchCell.setTitle(title)
        }
        if let detail = dataObject.preferencesDetail {
            switchCell.setDetailText(detail)
        }
        
        switchCell.setActionForSwitch {(sender) -> (Void)?  in
            return completion(sender)
        }
        
        switchCell.selectionStyle = .none
        return switchCell
    }
    func setupHeader(_ cell: SettingsHeader) -> SettingsHeader {
        cell.setTitle(CommonStrings.Settings.PrivacyTitle903)
        cell.setDetailText(CommonStrings.UserPrefs.PrivacyGroupHeaderMessage71)
        cell.setActionButton(title: CommonStrings.UserPrefs.PrivacyGroupHeaderLinkButtonTitle72)
        
        cell.setActionForButton({[weak self] in
            self?.performSegue(withIdentifier: "showPrivacyStatement", sender: nil)
            return nil
        })
        
        // remove separator
        cell.separatorInset = UIEdgeInsetsMake(0, tableView.bounds.width, 0, 0)
        
        cell.selectionStyle = .none
        return cell
    }
    
    private func showHeaderCell() -> UITableViewCell {
        // header
        guard var cell = self.tableView.dequeueReusableCell(withIdentifier: headerIdentifier) as? SettingsHeader else {
            return self.tableView.defaultTableViewCell()
        }
        cell = setupHeader(cell)
        return cell
    }
    
    private func blankCell() -> UITableViewCell {
        // blank
        let cell = self.tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! VIATableViewCell
        cell.selectionStyle = .none
        cell.separatorInset = UIEdgeInsetsMake(0, tableView.bounds.width, 0, 0)
        return cell
    }
    
    private func ShareVitalityStatusCell() -> UITableViewCell {
        // vitality status
        guard var switchCell = self.tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? FirstTimePreferenceSwitchCell else {
            return self.tableView.defaultTableViewCell()
        }
        
        switchCell = configSwitchCell(switchCell, dataObject: vitalityStatusPref) {[weak self] (sender) in
            self?.vitalityStatusPref.toggleVitalityStatusPref(sender: sender)
        }
        switchCell.switchAction.setOn(vitalityStatusPref.preferenceInitialState(), animated: false)
        switchCell.actionButton.isHidden = false
        switchCell.actionButtonHeight.constant = 15
        switchCell.actionButtonSpaceConstraint.constant = 15
        switchCell.setActionButton(title: CommonStrings.CommunicationPrefButtonStatus377)
        switchCell.setActionForButton({[weak self] in
            self?.performSegue(withIdentifier: "showWhatIsVitalityStatus", sender: nil)
            return nil
        })
        return switchCell
    }
    
    private func showAnalyticsCell() -> UITableViewCell {
        // analytics
        guard var switchCell = self.tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? FirstTimePreferenceSwitchCell else {
            return self.tableView.defaultTableViewCell()
        }
        
        switchCell = configSwitchCell(switchCell, dataObject: analyticsPref) {[weak self] (sender) in
            self?.analyticsPref.toggleAnalyticsPreference(sender: sender)
        }
        switchCell.switchAction.setOn(analyticsPref.preferenceInitialState(), animated: false)
        return switchCell
    }
    
    private func showCrashReportsCell() -> UITableViewCell {
        // crash
        guard var switchCell = self.tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? FirstTimePreferenceSwitchCell else {
            return self.tableView.defaultTableViewCell()
        }
        
        switchCell = configSwitchCell(switchCell, dataObject: crashPref) {[weak self] (sender) in
            self?.crashPref.toggleCrashReportPreference(sender: sender)
        }
        switchCell.switchAction.setOn(crashPref.preferenceInitialState(), animated: false)
        
        // remove separator
        switchCell.separatorInset = UIEdgeInsetsMake(0, 0, 0, tableView.bounds.width);
        
        return switchCell
    }
    
    public override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: UITableViewCell = UITableViewCell.init()
        if let row = PrivacyItems(rawValue: indexPath.row){
            switch(row){
            case .Title:
                cell = self.showHeaderCell()
                break
            case .VitalityStatus:
                if let showShareVitalityStatus = VIAApplicableFeatures.default.showShareVitalityStatus,showShareVitalityStatus {
                //Add share vitality status for UKE
                cell = self.ShareVitalityStatusCell()
                    break
                } else {
                    cell.separatorInset = UIEdgeInsetsMake(0, tableView.bounds.width, 0, 0)
                    cell.selectionStyle = .none
                    break
                }
            case .Analytics:
                cell = self.showAnalyticsCell()
                break
            case .CrashReports:
                cell = self.showCrashReportsCell()
                break
            }
        }
        return cell
    }
    
}
