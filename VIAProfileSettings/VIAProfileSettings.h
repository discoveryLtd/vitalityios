//
//  VIAProfileSettings.h
//  VIAProfileSettings
//
//  Created by OJ Garde on 8/31/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for VIAProfileSettings.
FOUNDATION_EXPORT double VIAProfileSettingsVersionNumber;

//! Project version string for VIAProfileSettings.
FOUNDATION_EXPORT const unsigned char VIAProfileSettingsVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <VIAProfileSettings/PublicHeader.h>


