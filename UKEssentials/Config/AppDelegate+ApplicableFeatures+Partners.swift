//
//  AppDelegate+ApplicableFeatures+Partners.swift
//  VitalityActive
//
//  Created by Dexter Anthony Ambrad on 5/10/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import VIACommon
import VitalityKit

extension AppDelegate {
    /**
     * Usage:
     * PartnersDetailTableViewController.swift
     **/
    public var shouldExecuteSSO: Bool? {
        get{
            return false
        }
    }
    
    public var shouldUseGetEligibility: Bool? {
        get{
            return true
        }
    }
}
