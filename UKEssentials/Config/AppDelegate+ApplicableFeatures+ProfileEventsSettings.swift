//
//  AppDelegate+ApplicableFeatures+ProfileEventsSettings.swift
//  VitalityActive
//
//  Created by OJ Garde on 1/14/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import VIACommon



extension AppDelegate{
    
    /**
     * Usage:
     * ProfileLandingViewController.swift & ProfileSettingsViewController.swift
     **/
    public var changeMenuItemLocation: Bool?{
        get{
            return true
        }
    }
}

/**
 * Personal Details
 **/
extension AppDelegate{
    
    /**
     * Usage:
     * EditProfileViewController.swift
     **/
    public var enableEntityNumber: Bool?{
        get{
            return true
        }
    }
    
    /**
     * Usage:
     * EditProfileViewController.swift
     **/
    public var enableMobileNumber: Bool?{
        get{
            return true
        }
    }
    
    /**
     * Usage:
     * EditProfileViewController.swift
     **/
    public func enableGender() -> Bool {
        return true
    }
    
    /**
     * Usage:
     * EditProfileViewController.swift
     **/
    public var enableUpdateEmail: Bool? {
        get{
            return false
        }
    }
}

/**
 * Provide Feedback
 **/
extension AppDelegate {
    
    /**
     * Usage:
     * ProfileProvideFeedbackViewController.swift
     **/
    public var enableContactNumberField: Bool?{
        get{
            return false
        }
    }
}

/**
 * Membership Pass
 **/
extension AppDelegate{
    
    /**
     * Usage:
     * ProfileMembershipPassViewController.swift
     **/
    public var showMembershipBanner: Bool?{
        get{
            return false
        }
    }
    
    public var showBannerInAspectRatio: Bool?{
        get{
            return false
        }
    }
    
    /**
     * Usage:
     * ProfileMembershipPassViewController.swift
     **/
    public func getMembershipBannerImage() -> UIImage? {
        return VIACommonAsset.carrierBrandingAreaGray.image.overlayImage(color: UIColor.currentGlobalTintColor())
    }
    
    /**
     * Usage:
     * ProfileMembershipPassViewController.swift
     **/
    public var userPartyIDAsVitalityNumber: Bool?{
        get{
            return true
        }
    }
    
    /**
     * Usage:
     * ProfileMembershipPassViewController.swift
     **/
    public var showMembershipPassInfoButton: Bool?{
        get{
            return false
        }
    }
    
    /**
     * Usage:
     * ProfileMembershipPassViewController.swift
     **/
    public func showPartyIdDetail() -> Bool {
        return false
    }
}


/**
 * Privacy
 **/
extension AppDelegate{
    
    /**
     * Usage:
     * ProfilePrivacyViewController.swift
     **/
    public var showShareVitalityStatus: Bool?{
        get{
            return true
        }
    }
    
    public var shouldPullProvideFeedbackContentFromCMS: Bool? {
        get{
            return true
        }
    }
    
    public var enableNotificationPreference: Bool? {
        get{
            return false
        }
    }
}


/**
 * Security
 **/
extension AppDelegate{
    /**
     * Usage:
     * ProfileSecurityViewController.swift
     **/
    public var enableChangePassword: Bool?{
        get{
            return false
        }
    }
    
    /**
     * Usage:
     * ProfileSecurityViewController.swift
     **/
    public var enableTouchID: Bool?{
        get{
            return false
        }
    }
    
    /**
     * Usage:
     * RememberMePreference.swift
     **/
    public var rememberMePreferenceDefault: Bool?{
        get{
            return true
        }
    }
    
    /**
     * Usage:
     * TouchIdPreference.swift
     **/
    public func touchIdValidatePassword() -> Bool {
        return false
    }
}

/**
 * Terms and Conditions
 **/
extension AppDelegate{
    
    /**
     * Usage:
     * VIATermsConditionsViewController.swift
     **/
    public var hideTermsAndConditionsDisagreeButton: Bool?{
        get{
            return false
        }
    }
}

/**
 * Log out
 **/
extension AppDelegate{
    
    /**
     * Usage:
     * ProfileSettingsViewController.swift
     **/
    public func getLogoutRedirectURL() -> String?{
        return nil
    }
}
