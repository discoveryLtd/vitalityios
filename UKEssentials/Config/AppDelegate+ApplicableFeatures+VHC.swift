//
//  AppDelegate+ApplicableFeatures+VHC.swift
//  VitalityActive
//
//  Created by OJ Garde on 1/14/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import VIACommon
import VitalityKit
import RealmSwift
import VIAUtilities
import VIAHealthCheck
import VIAUIKit

extension AppDelegate{
    
    /**
     * Usage:
     * VHCLandingViewController.swift
     **/
    public func showVHCHistoryMenu() -> Bool{
        return false
    }
    
    /**
     * Usage:
     * VHCCaptureResultsViewModel.swift
     **/
    public func isCapturedVHCIsValid(bloodPressureValid: Bool, capturedResults: Results<VHCCapturedResult>) -> Bool {
        // nothing captured
        if capturedResults.count == 0 {
            return false
        }
        
        let bloodPressureValid = bloodPressureValid
        
        var allOtherCapturedResultsValid = true
        
        // Let's loop through all the captured result. Once there is an invalid captured result, stop the loop and return false.
        for capturedResult in capturedResults {
            
            if !(capturedResult.isValid)  && capturedResult.unitOfMeasureType != .Unknown{
                allOtherCapturedResultsValid = false
                break
            }
        }
        let count = capturedResults.filter(({ $0.unitOfMeasureType == .Unknown })).count
        if count == capturedResults.count {
            allOtherCapturedResultsValid = false
        }
        return allOtherCapturedResultsValid && bloodPressureValid
    }
    
    /**
     * Usage:
     * VHCCaptureResultsViewModel.swift
     **/
    public func didVHCBloodPressureCapturedCompletely(systolic: VHCCapturedResult?, diastolic: VHCCapturedResult?) -> Bool{
        return (systolic == nil && diastolic == nil) || (systolic != nil &&
            !(systolic?.rawInput ?? "").isEmpty && diastolic != nil &&
            !(diastolic?.rawInput ?? "").isEmpty) || ((systolic?.rawInput ?? "").isEmpty && (diastolic?.rawInput ?? "").isEmpty)
    }
    
    /**
     * Usage:
     * VHCCaptureResultsCollectionViewController
     **/
    public func onValidateCapturedResults(isBMIValid: Bool, isCholesterolValid: Bool, areInputsValid: Bool, onError: () -> Void, onNext: () -> Void){
        /* UKE
         * Add checker if BMI and Cholesterol are valid. Else, disable next.
         * VA-35926 Add checker if all iputs are valid. Else, disable next.
         */
        if isBMIValid && isCholesterolValid && areInputsValid{
            onNext()
        } else {
            onError()
        }
    }
    
    /**
     * Usage:
     * VHCCaptureResultsValidValuesListViewController.swift
     * VHCCaptureResultsCollectionViewController.swift
     **/
    public var reverseValidValuesOptionList: Bool? {
        get{
            return false
        }
    }
    
    /**
     * Usage:
     * VHCHealthAttributeGroup+Extension.swift
     **/
    public var hideCholesterolContent: Bool?{
        get{
            return true
        }
    }
    
    /**
     * Usage:
     * VHCSingleMeasurableInputSectionController.swift
     **/
    public func shouldShowReminderMessageOnVHC(attribute: PartyAttributeTypeRef) -> Bool{
        let attributes: [PartyAttributeTypeRef] = [.TotalCholesterol]
        
        for validAttribute in attributes{
            return attribute == validAttribute
        }
        return false
    }
    
    /**
     * Usage:
     * VHCCaptureResultsViewModel.swift
     **/
    public var hideCholesterolRatio: Bool?{
        get{
            return true
        }
    }
    
    public func getCommonStringsMeasurementCholesterolSectionTitle() -> String{
        
        return CommonStrings.Measurement.CholesterolTitle138
    }
    
    /**
     * VHC Learn More
     * BodyMassIndexLearnMore.swift
     */
    public func getCommonStringsLearnMoreSection3MessageBmi(party: VitalityParty?) -> String{
        return CommonStrings.LearnMore.BmiSection3Message227
    }
    
    /**
     * VHC Learn More
     * WaistCircumferenceLearnMore.swift
     */
    public func getCommonStringsLearnMoreSection3MessageWaistCircumference(party: VitalityParty?) -> String{
        return CommonStrings.LearnMore.WaistCicumferneceSection3Message230
    }
    
    /**
     * VHC Learn More
     * GlucoseLearnMore.swift
     */
    public func getCommonStringsLearnMoreSection3MessageGlucose(party: VitalityParty?) -> String{
        return CommonStrings.LearnMore.GlucoseSection3Message233
    }
    
    /**
     * VHC Learn More
     * BloodPressureLearnMore.swift
     */
    public func getCommonStringsLearnMoreSection3MessageBloodPressure(party: VitalityParty?) -> String{
        return CommonStrings.LearnMore.BloodPressureSection3Message236
    }
    
    /**
     * VHC Learn More
     * CholesterolLearnMore.swift
     */
    public func getCommonStringsLearnMoreSection3MessageCholesterol(party: VitalityParty?) -> String{
        return CommonStrings.LearnMore.CholesterolSection3Message239
    }
    
    /**
     * VHC Learn More
     * HbA1cLearnMore.swift
     */
    public func getCommonStringsLearnMoreSection3MessageFastingGlucose(party: VitalityParty?) -> String{
        return CommonStrings.LearnMore.Hba1cSection3Message242
    }
    
    /**
     * VHC Learn More
     * UrineProteinLearnMore.swift
     */
    public func getCommonStringsLearnMoreSection3MessageUrineProtein(party: VitalityParty?) -> String{
        return CommonStrings.LearnMore.UrineProteinSection3Message348
    }
    
    /**
     * VHC Learn More
     * CholesterolLearnMore.swift
     */
    public func getCholesterolAttributeGroupName() -> (String){
        return CommonStrings.Measurement.CholesterolRatioTitle1173
    }
    
    /**
     * VHC Landing HealthCare pdf
     */
    public func getHealthCarePDFTypeKey() -> AppConfigFeature.AppConfigFeatureType{
        return .VHCHealthcareBenefitUKEIGI
    }
    
    /**
     * SelectedPhotosCollectionViewController
     */
    public var shouldLimitNumberOfAttachProof: Bool?{
        get{
            return true
        }
    }
    
    /**Usage:
     * VHCCaptureSummaryViewModel.swift
     */
    public var removeEmptyCapturedData: Bool?{
        get{
            return false
        }
    }
    
    
    public func persistVHCData() -> Bool {
        return false
    }
    
    public func hideParticipatingPartners() -> Bool {
        return true
    }
    
    /**Usage:
     * VHCOptionListCaptureResultsCollectionViewCell.swift
     * VHCSingleCaptureResultsCollectionViewCell.swift
     * VHCBloodPressureInputSectionController.swift
     * VHCCaptureResultsCollectionViewController.swift
     * VHCCaptureResultsViewModel.swift
     * VHCOptionListInputSectionController.swift
     * VHCSingleMeasurableInputSectionController.swift
     */
    public func withDefaultStringValueForVHCCapturedDate() -> Bool {
        return false
    }
    
    /**
     * VHCBMICaptureResultsCollectionViewCell.swift
     * VHCBMIInputSectionController.swift
     */
    public func getBMIDelimiter() -> String {
        return "##"
    }
    
    public func initMultiUnitField(isValid: Bool?, topInputText: String?, bottomInputText: String?, numberFormatter: NumberFormatter, baseUnit: Double, completion: (String, String) -> Void) {
        var topValue = ""
        var bottomValue = ""
        if let valid = isValid, let input = topInputText, valid || input.components(separatedBy: VIAApplicableFeatures.default.getBMIDelimiter()).count == 1 {
            if let input = topInputText {
                topValue = String(Int((modf(numberFormatter.number(from: input)?.doubleValue ?? 0).0)))
                //topValue = String(getDecimalFormatter().number(from: input)?.doubleValue ?? 0)
            }
            if let input = bottomInputText {
                bottomValue = String(Int(round((modf(numberFormatter.number(from: input)?.doubleValue ?? 0).1 * baseUnit))))
                //bottomBalue = String(getDecimalFormatter().number(from: input)?.doubleValue ?? 0 * baseUnit)
            }
        } else if let input = topInputText {
            var topBotInputArray = input.components(separatedBy: VIAApplicableFeatures.default.getBMIDelimiter())
            topValue = String(numberFormatter.number(from: topBotInputArray[0])?.intValue ?? 0)
            bottomValue = String(numberFormatter.number(from: topBotInputArray[1])?.intValue ?? 0)
        }
        
        return completion(topValue, bottomValue)
    }
    
    public func getPassedValue(top: Double, bottom: Double, baseUnit: Double, selectedUnitOfMeasureType: UnitOfMeasureRef, isTopInputValid: Bool, isBottomInputValid: Bool, completion: (Bool, String) -> Void) {
        var passedValue = ""
        var valid = isTopInputValid
        if (selectedUnitOfMeasureType == .FootInch) {
            valid = (isTopInputValid && isBottomInputValid)
            
            if valid {
                passedValue = "\(top + (bottom/baseUnit))"    // 1 Foot = 12 inches
            } else {
                passedValue = String(top) + VIAApplicableFeatures.default.getBMIDelimiter() + String(bottom)
            }
        } else if (selectedUnitOfMeasureType == .StonePound) {
            valid = (isTopInputValid && isBottomInputValid)
            
            if valid {
                passedValue = "\(top + (bottom/baseUnit))"    // 1 Stone = 14 Pounds
            } else {
                passedValue = String(top) + VIAApplicableFeatures.default.getBMIDelimiter() + String(bottom)
            }
        } else {
            passedValue = String(top)
        }
        
        return completion(valid, passedValue)
    }
    
    public func healthyRangeFeedback(from feedback: VHCHealthAttributeFeedback, isSystolicOrDiastolicHealthy: Bool, explanation: String?) -> VHCFeedback? {
        
        var healthyRangeImage = UIImage()
        var outOfHealthyRangeImage = UIImage()
        
        healthyRangeImage = VIAHealthCheckAsset.vhcGenericInHealthyRangeSmall.templateImage
        outOfHealthyRangeImage = VIAHealthCheckAsset.vhcGenericOutOfHealthyRangeSmall.templateImage
        
        switch feedback.type {
        case .BMIAbove, .BMIBelow, .DiastolicAbove, .FGlucoseOutRange, .HbA1cOutRange, .LDLAbove, .LDLBelow, .SystolicAbove, .TotalCholesterolHigh, .UrinaryProteinOut, .WaistCircumAbove, .WeightAbove, .WeightBelow, .LipidRatioHigh, .RandomGlucoseHigh:
            return VHCFeedback(image: outOfHealthyRangeImage, description: CommonStrings.Range.OutOfHealthyTitle191, explanation: feedback.feedbackTypeName, isInHealthyRange: false)
            
        case .BMIHealthy, .DiastolicHealthy, .FGlucoseInRange, .HbA1cInRange, .LDLHealthy, .SystolicHealthy, .TotalCholesterolGood, .UrinaryProteinIn, .WaistCircumHealthy, .WeightHealthy, .LipidRatioHealthy, .RandomGlucoseHealthy, .RandomGlucoseOk :
            return  VHCFeedback(image: healthyRangeImage, description: CommonStrings.Range.InHealthyTitle190, explanation: feedback.feedbackTypeName, isInHealthyRange: true)
        default:
            return nil
        }
    }
}
