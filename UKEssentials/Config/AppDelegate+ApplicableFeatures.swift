//
//  AppDelegate+ApplicableFeatures.swift
//  UKEssentials
//
//  Created by OJ Garde on 11/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import VIACommon
import VitalityKit
import VIAUIKit
import VIACore

/**
 * Main
 **/
extension AppDelegate: ApplicableFeaturesDataSource{
    
    public var hideHelpTab: Bool?{
        get{
            return false
        }
    }
    
    public var showHelpContactFooter: Bool?{
        get{
            return true
        }
    }
    
    public var showPartnerGetStartedLink: Bool?{
        get{
            return false
        }
    }
    
    public var hideMembershipPassAndUpdateTitle: Bool?{
        get{
            return false
        }
    }
    
    public var showPreviousMembershipYearTitle: Bool?{
        get{
            return true
        }
    }
    
    public var getPointPeriod: PointsPeriod? {
        
        return PointsPeriod.current
    }
    
    public var enableKeyboardAutoToolbar: Bool? {
        get {
            return true
        }
    }
    
    public func getDataSharingLeftBarButtonTitle() -> String{
        return CommonStrings.DisagreeButtonTitle49
    }
    
    public func getDataSharingRightBarButtonTitle() -> String{
        return CommonStrings.AgreeButtonTitle50
    }
    
    public var showPartnersTabItem: Bool? {
        get{
            return true
        }
    }
    
    /* Get the Push notification state from device's settings. */
    public var applyPushNotifToggle: Bool? {
        get{
            return true
        }
    }
    
    public var enableNotificationDialogConfirmation: Bool {
        get{
            return false
        }
    }

    public func shouldDisplayEarningPointsAndPointsLimits(for eventKey: Int) -> Bool{
        return true
    }

    public func showHomeInitialScreen(){
        var storyboard = UIStoryboard(coreStoryboard: .home)
        
        if AppSettings.hasShownUserPreferences(){
            /* Do nothing and show the home screen. */
        }else{
            storyboard = UIStoryboard(name: "UKEFirstTimeOnboarding", bundle: nil)
            AppSettings.setHasShownUserPreferences()
        }
        
        let viewController = storyboard.instantiateInitialViewController()
        UIApplication.shared.keyWindow?.rootViewController = viewController
    }
    
    public func getTextFieldViewCell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        return tableView.dequeueReusableCell(withIdentifier: VIATextFieldTableViewCell.defaultReuseIdentifier, for: indexPath)
    }
    
    public func getImageControllerConfiguration() -> AnyObject?{
        return UIImagePickerController.ImageControllerConfiguration(shouldLimitFileSize: false, maxFileSize: 2000000,
                                                                    shouldLimitNumberOfAttachProof: true,
                                                                    maxAttachmentCount: 11,
                                                                    includeAllProofs: false) as AnyObject
    }
    
    public func setMinimumDate() -> Date? {
        return DataProvider.currentMembershipPeriodStartDate()?.setMinimumDate()
    }
    
    public func navigateToHomeScreen(){
        VIAHomeViewController.showHome()
    }
    
    public func navigateToLoginScreen(){
        VIALoginViewController.showLogin()
    }
    
    public var numberOfDecimalPlaces: Int? {
        get {
            return 1
        }
    }
    
    public func getFirstTimePreferenceViewModel() -> AnyObject? {
        return VIAFirstTimePreferenceViewModel()
    }
}
