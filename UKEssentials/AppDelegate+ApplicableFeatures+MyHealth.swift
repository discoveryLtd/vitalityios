//
//  App.swift
//  VitalityActive
//
//  Created by OJ Garde on 3/17/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import VIACommon

extension AppDelegate{    
    
    public func getMyHealthCards() -> [MyHealthCardItem] {
        return [MyHealthCardItem.VitalityAge]
    }
    
    public var reloadMyHealthOnViewAppear: Bool? {
        get{
            return false
        }
    }
    
    public func shouldSkipMyHealthLandingScreen() -> Bool {
        return true
    }
    
    public var concatenateAgeBlurb: Bool? {
        get {
            return true
        }
    }
}
