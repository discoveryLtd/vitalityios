//
//  AppDelegate+ApplicableFeatures+AWC.swift
//  VitalityActive
//
//  Created by Von Kervin R. Tuico on 28/05/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import VIACommon
import VitalityKit

extension AppDelegate {
    /**
     * Usage:
     * AWCLandingViewModel.swift
     **/
    public func getVMPURL() -> URL? {
        let currentEnvironment = Wire.default.currentEnvironment
        
        switch currentEnvironment {
        case .develop:
            return URL(string: "")!
        case .test:
            return URL(string: "")!
        case .qa:
            return URL(string: "")!
        case .production:
            return URL(string: "")!
        default: break
        }
        return nil
    }
}
