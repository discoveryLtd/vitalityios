import VIACommon
import VIAUIKit
import VIACore
import UIKit
import VitalityKit

extension AppDelegate: AppearanceColorDataSource {

    class func ukeColor() -> UIColor {
        return UIColor(red:0.95686, green:0.10980, blue:0.36863, alpha:1.00000)
    }

    // MARK: AppearanceDataSource

    public var primaryColor: UIColor {
        return AppDelegate.ukeColor()
    }

    public var tabBarBackgroundColor: UIColor {
        return .white
    }

    public var tabBarBarTintColor: UIColor {
        return .white
    }

    public var tabBarTintColor: UIColor? {
        return UIColor.primaryColor()
    }

    public var unselectedItemTintColor: UIColor? {
        return nil
    }
        
    public var getInsurerGlobalTint: UIColor? {
        return VIAAppearance.default.primaryColorFromServer
    }
    
    public func getSplashScreenGradientTop() -> UIColor{
        return UIColor(hexString: AppConfigFeature.insurerGradientColor1Hex())
    }
    
    public func getSplashScreenGradientBottom() -> UIColor{
        return UIColor(hexString: AppConfigFeature.insurerGradientColor2Hex())
    }
}

extension AppDelegate: AppearanceIconographyDataSource {
    
    public func homeLogo(for locale: Locale) -> UIImage {
        return UIImage(asset: .loginLogoEn)
    }

    public func loginLogo(for locale: Locale) -> UIImage {
        return UIImage(asset: .loginLogoEn)
    }
    
    public func splashLogo(for locale: Locale = Locale.current) -> UIImage {
        return UIImage(asset: .loginLogoEn)
    }
}
