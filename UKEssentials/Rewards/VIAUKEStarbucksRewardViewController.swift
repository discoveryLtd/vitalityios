import VIAActiveRewards
import Foundation
import VIAUIKit
import VIAActiveRewards
import VitalityKit
import VIAUtilities
import VIACommon

class VIAUKEStarbucksRewardViewController: VIACoordinatedTableViewController, VIAABespokeRewardConsumer {
	
	// MARK: Public
	internal let starbucksViewModel = VIAUKEStarbucksRewardViewModel()
	var dataProvider: VIAARRewardViewModel?
	var registrationEmail: String?
	var isErrorShowing: Bool = false
	var confirmButton: UIBarButtonItem?
	weak var delegate: VIAABespokeRewardDelegate?
    weak var registrationEmailCell: VIATextFieldTableViewCell?
	public var appUrl: String = "https://itunes.apple.com/gb/app/apple-store/id331177714?mt=8"
	public var launchUrl: String = "starbucks://menu"
	
	// MARK: Private
	private var leftButton = UIBarButtonItem()
	
	// MARK: Lifecycle
	
	override func viewDidLoad() {
		super.viewDidLoad()
		VIAAppearance.setGlobalTintColorToImproveYourHealth()
		configureTableView()
        getEmailCommunicationPreference()
	}
    
    func getEmailCommunicationPreference() {
        let coreRealm = DataProvider.newRealm()
        let tenantId = coreRealm.getTenantId()
        let partyId = coreRealm.getPartyId()
        Wire.Rewards.getPartyById(tenantId: tenantId, partyId: partyId) { [weak self] (error, partyById) in
            if let preferedEmail = PartyPreferenceHelper.getEmailPreference(from: partyById) {
                self?.registrationEmail = preferedEmail
                self?.registrationEmailCell?.setTextFieldText(text: preferedEmail)
                self?.confirmButton?.isEnabled = self?.emailIsValid() ?? false
            }
        }
    }
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		configureBarButtonItems()
	}
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		self.confirmButton?.isEnabled = self.emailIsValid()
	}
	
	func configureTableView() {
		self.tableView.register(VIAARRewardCongratulationsCell.nib(), forCellReuseIdentifier: VIAARRewardCongratulationsCell.defaultReuseIdentifier)
		self.tableView.register(VIATextFieldTableViewCell.nib(), forCellReuseIdentifier: VIATextFieldTableViewCell.defaultReuseIdentifier)
		self.tableView.register(VIALabelTableViewCell.nib(), forCellReuseIdentifier: VIALabelTableViewCell.defaultReuseIdentifier)
		
		self.tableView.register(VIAARRewardWithStatusCell.nib(), forCellReuseIdentifier: VIAARRewardWithStatusCell.defaultReuseIdentifier)
		self.tableView.register(VIAARRewardWithExpirationStatusCell.nib(), forCellReuseIdentifier: VIAARRewardWithExpirationStatusCell.defaultReuseIdentifier)
		self.tableView.register(VIAARRewardDeniedCell.nib(), forCellReuseIdentifier: VIAARRewardDeniedCell.defaultReuseIdentifier)
		self.tableView.register(VIAARPartnerAppCell.nib(), forCellReuseIdentifier: VIAARPartnerAppCell.defaultReuseIdentifier)
		self.tableView.register(VIAARRewardMarkUsedCell.nib(), forCellReuseIdentifier: VIAARRewardMarkUsedCell.defaultReuseIdentifier)
		
		tableView.separatorInset = UIEdgeInsets.zero
		
		self.tableView.allowsSelection = false
		self.tableView.estimatedRowHeight = 600
		self.tableView.backgroundColor = UIColor.tableViewBackground()
	}
	
	// MARK: Action buttons
	
	func configureBarButtonItems() {
		
		let cancel = UIBarButtonItem(title: CommonStrings.GenericCloseButtonTitle10, style: .plain, target: self, action: #selector(cancelButtonTapped(_:)))
		let done = UIBarButtonItem(title: CommonStrings.DoneButtonTitle53, style: .plain, target: self, action: #selector(doneButtonTapped(_:)))
		
		if dataProvider?.rewardStatus == AwardedRewardStatusRef.Allocated, self.emailIsValid() {
			self.confirmButton = UIBarButtonItem(title: CommonStrings.NextButtonTitle84, style: .plain, target: self, action: #selector(nextButtonTapped(_:)))
		} else {
			self.confirmButton = UIBarButtonItem(title: CommonStrings.NextButtonTitle84, style: .plain, target: self, action: #selector(confirmButtonTapped(_:)))
		}
		
		if dataProvider?.rewardStatus == AwardedRewardStatusRef.Canceled
				|| dataProvider?.rewardStatus == AwardedRewardStatusRef.IssueFailed {
			self.navigationItem.leftBarButtonItem = nil
			self.navigationItem.rightBarButtonItem = done
		} else if dataProvider?.rewardStatus == AwardedRewardStatusRef.Acknowledged {
			self.navigationItem.leftBarButtonItem = nil
			self.navigationItem.rightBarButtonItem = done
		} else if let _ = dataProvider?.accepted, let codes = dataProvider?.voucherCodes, codes.first != "" {
			self.navigationItem.leftBarButtonItem = nil
			self.navigationItem.rightBarButtonItem = done
		} else {
			self.navigationItem.leftBarButtonItem = cancel
			self.navigationItem.rightBarButtonItem = confirmButton
		}
		
		self.navigationItem.hidesBackButton = true
		
		self.navigationItem.title = CommonStrings.Ar.Partners.StarbucksName734
	}
	
	// MARK: UITableView datasource
	
	override func numberOfSections(in tableView: UITableView) -> Int {
		return 1
	}
	
	override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		if dataProvider?.rewardStatus == AwardedRewardStatusRef.Canceled
				|| dataProvider?.rewardStatus == AwardedRewardStatusRef.IssueFailed {
			return 1
		} else if dataProvider?.rewardStatus == AwardedRewardStatusRef.Allocated {
			return 1
		} else if dataProvider?.rewardStatus == AwardedRewardStatusRef.Issued {
			return 2
		} else {
			return 3
		}
	}
	
	// MARK: UITableView delegate
	
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if dataProvider?.rewardStatus == AwardedRewardStatusRef.Canceled
            || dataProvider?.rewardStatus == AwardedRewardStatusRef.IssueFailed {
            return self.configureDeniedStatusCell(indexPath)
        } else if dataProvider?.rewardStatus == AwardedRewardStatusRef.Allocated {
            return self.configurePendingStatusCell(indexPath)
        } else if dataProvider?.rewardStatus == AwardedRewardStatusRef.Issued {
            if indexPath.row == 0 {
                return self.configureStatusWithExpirationCell(indexPath)
            } else if indexPath.row == 1 {
                return self.configureStarbucksAppCell(indexPath)
            } else if indexPath.row == 2 {
                return self.configureMarkUsed(indexPath)
            }
        } else {
            if indexPath.row == 0 {
                return self.configureCongratulationsCell(indexPath)
            } else if indexPath.row == 1 {
                return self.configureActionCell(indexPath)
            } else if indexPath.row == 2 {
                return self.configureDisclaimerCell(indexPath)
            }
        }
        
        return tableView.dequeueReusableCell(withIdentifier: VIATableViewCell.defaultReuseIdentifier, for: indexPath)
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView.numberOfSections == 1, tableView.numberOfRows(inSection: indexPath.section) == 1 {
            return tableView.frame.size.height
        } else {
            return UITableViewAutomaticDimension
        }
    }
	
	// MARK: - Status Specific TableCells
	
	// MARK: Available
	func configureStatusWithExpirationCell(_ indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: VIAARRewardWithExpirationStatusCell.defaultReuseIdentifier, for: indexPath) as! VIAARRewardWithExpirationStatusCell
		
		guard let dataProvider = dataProvider else {
			return cell
		}
		
        cell.header = CommonStrings.Ar.Rewards.ConfirmRewardTitle690
		cell.rewardHeader = CommonStrings.Ar.Partners.RewardTitle1058
		cell.rewardContent = dataProvider.subTitle
        if let expiration = dataProvider.expiration {
            let date = Localization.dayMonthAndYearFormatter.string(from: expiration)
            cell.rewardExpiration = CommonStrings.Ar.VoucherExpires658(date)
        }
		cell.rewardImage = dataProvider.image
        cell.status = dataProvider.statusString()
        
        if !((dataProvider.instructions?.isEmpty)!) {
            do {
                let attrStr = try NSMutableAttributedString(
                    data: (dataProvider.instructions?.data(using: String.Encoding.unicode, allowLossyConversion: true)!)!,
                    options: [ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
                    documentAttributes: nil)
                //cell.detailsLink = attrStr
                attrStr.addAttribute(NSFontAttributeName, value: UIFont.systemFont(ofSize: 17), range: NSMakeRange(0, attrStr.length))
                
                let paragraphStyle = NSMutableParagraphStyle()
                paragraphStyle.alignment = .center
                
                attrStr.addAttribute(NSParagraphStyleAttributeName, value: paragraphStyle, range: NSMakeRange(0, attrStr.length))
                cell.detailsLink = attrStr
            } catch let error {
                print(error)
            }
        } else {
            cell.details = dataProvider.instructions ?? ""
        }
		
		return cell
	}
	
	func configureStarbucksAppCell(_ indexPath: IndexPath) -> UITableViewCell {
        //return tableView.defaultTableViewCell()
		let cell = tableView.dequeueReusableCell(withIdentifier: VIAARPartnerAppCell.defaultReuseIdentifier, for: indexPath) as! VIAARPartnerAppCell
		
		cell.appName = CommonStrings.Ar.Rewards.StarbucksRegisteredTrademark1087
		// TODO: Need alternate square / rounded corner starbucks logo asset
		cell.cellImage = UIImage(asset: UKEssentialsAsset.Rewards.starbucks)
		
		if let url = URL(string: launchUrl), UIApplication.shared.canOpenURL(url) {
			cell.targetUrl = launchUrl
			cell.appButtonText = CommonStrings.Ar.Rewards.StarbucksOpen1088
		} else {
			cell.targetUrl = appUrl
			cell.appButtonText = CommonStrings.Ar.Rewards.StarbucksGet1090
		}
		return cell
	}
	
	func configureMarkUsed(_ indexPath: IndexPath) -> UITableViewCell {
        return tableView.defaultTableViewCell()
//TODO: Once in scope add code back to show "Mark as Used" back
//		let cell = tableView.dequeueReusableCell(withIdentifier: VIAARRewardMarkUsedCell.defaultReuseIdentifier, for: indexPath) as! VIAARRewardMarkUsedCell
//		cell.instructions = "Already used this reward? Mark it as used to move it to your reward history."
//		cell.actionText = "Mark as Used"
//		cell.buttonAction = { [weak self] in
//			if let strongSelf = self, let reward = strongSelf.dataProvider {
//				strongSelf.delegate?.consumeReward(reward, completion: nil)
//			}
//		}
//
//		return cell
	}
	
	// MARK: Unsubmitted
	func configureCongratulationsCell(_ indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: VIAARRewardCongratulationsCell.defaultReuseIdentifier, for: indexPath) as! VIAARRewardCongratulationsCell
		
		guard let dataProvider = dataProvider else {
			return cell
		}
		
		cell.header = CommonStrings.Ar.Rewards.ConfirmRewardTitle690
		cell.rewardHeader = CommonStrings.Ar.Partners.RewardTitle1058
		cell.rewardContent = dataProvider.subTitle
        cell.rewardImage = dataProvider.image
		
		let label = UILabel()
		label.numberOfLines = 0
		label.textAlignment = .center
		label.text = CommonStrings.Ar.Partners.StarbucksRewardEmailMessage1059("")
		
		let wrapper = UIView()
		wrapper.addSubview(label)
		
		label.snp.makeConstraints({(make) -> Void in
			make.centerY.equalToSuperview()
			make.top.equalTo(15)
			make.left.equalTo(25)
			make.right.equalTo(-25)
		})
		
		cell.addAdditionalView(view: wrapper)
		
		wrapper.snp.makeConstraints({(make) -> Void in
			make.left.equalToSuperview()
			make.right.equalToSuperview()
		})
		
		let link = UIButton()
		link.setTitleColor(UIColor.cellButton(), for: .normal)
		link.setTitle(CommonStrings.Ar.Partners.StarbucksRewardRegisterAccount1060, for: .normal)
		link.addTarget(self, action: #selector(VIAUKEStarbucksRewardViewController.registerAccount(_:)), for: .touchUpInside)
		
		cell.addAdditionalView(view: link)
		
		return cell
	}
	
	func configureActionCell(_ indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: VIATextFieldTableViewCell.defaultReuseIdentifier, for: indexPath) as! VIATextFieldTableViewCell
		registrationEmailCell = cell
		cell.setTextFieldText(text: registrationEmail)
		cell.setHeadingLabelText(text: CommonStrings.Ar.Partners.StarbucksRewardEmailHint1061)
		cell.setTextFieldPlaceholder(placeholder: "Enter Email Address")
		
		var imageConfig = VIATextFieldCellImageConfig()
		imageConfig.templateImage = UIImage(asset: UKEssentialsAsset.Rewards.registrationEmail)
		cell.cellImageConfig = imageConfig
		cell.setTextFieldKeyboardType(type: .emailAddress)
		
		cell.textFieldDidEndEditing = { [unowned self] textField in
			self.confirmButton?.isEnabled = self.emailIsValid()
			self.tableView.reloadRows(at: [indexPath], with: .automatic)
		}
		
		cell.textFieldTextDidChange = { [unowned self] textField in
			self.registrationEmail = textField.text
			self.confirmButton?.isEnabled = self.emailIsValid()
			
			if self.isErrorShowing && !self.shouldShowErrorMessage() {
				UIView.performWithoutAnimation {
					self.tableView.beginUpdates()
					cell.setErrorMessage(message: nil)
					self.tableView.endUpdates()
				}
			}
		}
		
		isErrorShowing = shouldShowErrorMessage()
		if isErrorShowing {
			cell.setErrorMessage(message: CommonStrings.Registration.InvalidEmailFootnoteError35)
		} else {
			cell.setErrorMessage(message: nil)
		}
		
		return cell
	}
	
	func configureDisclaimerCell(_ indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: VIALabelTableViewCell.defaultReuseIdentifier, for: indexPath) as! VIALabelTableViewCell
		
		// TODO: Pull from datamodel or CMS
		//cell.labelText = CommonStrings.Ar.Partners.StarbucksRewardFootnote1062
		cell.configure(font: .footnoteFont(), color: .gray)
		return cell
	}
	
	// MARK: Pending
    
    func configurePendingStatusCell(_ indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: VIAARRewardWithStatusCell.defaultReuseIdentifier, for: indexPath) as! VIAARRewardWithStatusCell
        
        // TODO: Pull from datamodel or CMS
        cell.rewardHeader = dataProvider?.subTitle ?? ""
        cell.rewardContent = CommonStrings.Ar.Rewards.PendingStarbucksRewardMesage1078(dataProvider?.subTitle ?? "")
        cell.status = dataProvider?.statusString() ?? ""
        if let date = dataProvider?.effectiveFromDate {
            do {
                let attrStr = try NSMutableAttributedString(
                    data: CommonStrings.Ar.Rewards.PendingStarbucksRewardFooterHtml1079(dataProvider?.subTitle ?? "", dataProvider?.subTitle ?? "").data(using: String.Encoding.unicode, allowLossyConversion: true)!,
                    options: [ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
                    documentAttributes: nil)
                //cell.detailsLink = attrStr
                attrStr.addAttribute(NSFontAttributeName, value: UIFont.systemFont(ofSize: 17), range: NSMakeRange(0, attrStr.length))
                
                let paragraphStyle = NSMutableParagraphStyle()
                paragraphStyle.alignment = .center
                
                attrStr.addAttribute(NSParagraphStyleAttributeName, value: paragraphStyle, range: NSMakeRange(0, attrStr.length))
                cell.detailsLink = attrStr
            } catch let error {
                print(error)
            }
            
        }
        cell.rewardImage = dataProvider?.image
        return cell
    }
	
	// MARK: Denied
	
	func configureDeniedStatusCell(_ indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: VIAARRewardDeniedCell.defaultReuseIdentifier, for: indexPath) as! VIAARRewardDeniedCell
		
		// TODO: Pull from datamodel or CMS
        let voucherValue: String = dataProvider?.subTitle ?? ""
        cell.rewardHeader = CommonStrings.Ar.Rewards.RewardCouldNotAwardedTitle1083(voucherValue)
		cell.rewardContent = CommonStrings.Ar.Rewards.RewardCouldNotAwardedContent1084
		cell.rewardImage = dataProvider?.image
		
		return cell
	}

	
	override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
		return nil
	}
	
	override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
		return CGFloat.leastNormalMagnitude
	}
	
	override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
		return CGFloat.leastNormalMagnitude
	}
	
	// MARK: - VIAABespokeRewardConsumer
	
	func swapReward() {
		if let reward = dataProvider {
            delegate?.consumeReward(reward, completion: nil)
		}
	}
	
	// MARK: - IBAction
	
	@IBAction func registerAccount(_ sender: Any) {
		if let url = URL(string: CommonStrings.Ar.Partners.StarbucksRewardRegisterURL1060) {
			UIApplication.shared.open(url, options: [:], completionHandler: nil)
		}
	}
	
	@IBAction func cancelButtonTapped (_ sender: Any) {
            delegate?.cancel(consumer: self)
	}
	
	@IBAction func confirmButtonTapped (_ sender: Any) {
		verifyRegirstionEmail()
	}
	
	@IBAction func doneButtonTapped (_ sender: Any) {
		self.delegate?.cancel(consumer: self)
	}
	
	@IBAction func nextButtonTapped (_ sender: Any) {
		self.configureBarButtonItems()
		self.tableView.reloadData()
	}
	
	// MARK: - VIAABespokeRewardConsumer
	
	func setReward(_ reward: VIAARRewardViewModel) {
		dataProvider = reward
	}
	
	func verifyRegirstionEmail() {
		if let email = registrationEmail {
			let alert = UIAlertController(title: CommonStrings.Ar.Partners.StarbucksRewardAlertTitle1063,
										  message: CommonStrings.Ar.Partners.StarbucksRewardAlertMessage1064(email),
										  preferredStyle: .alert)

			alert.addAction(UIAlertAction(title: CommonStrings.CancelButtonTitle24,
			                              style: UIAlertActionStyle.cancel,
			                              handler: nil
			))

			alert.addAction(UIAlertAction(title: CommonStrings.ConfirmTitleButton182,
			                              style: UIAlertActionStyle.default,
			                              handler: { [unowned self] (UIAlertAction) in
                                            self.confirmReward(with: email)
			}))
			
			alert.view.tintColor = UIColor.improveYourHealthBlue()
			self.present(alert, animated: true, completion: nil)
		}
	}
	
    func confirmReward(with email: String) {
        confirmButton?.isEnabled = false
        guard let dataProvider = dataProvider else {
            return
        }
        if starbucksViewModel.starbucksPrefreredEmailSame(as: email) {
            if shouldShowTermsAndConditions() {
                showTermsAndConditions(with: dataProvider)
            } else {
                selectReward(with: dataProvider)
            }
        } else {
            self.showHUDOnView(view: self.view)
            starbucksViewModel.updateStarbucksEmailPreference(with: email, completion: { [weak self] (error) in
                self?.hideHUDFromView(view: self?.view)
                guard error == nil else {
                    self?.handleBackendErrorWithAlert(error as? BackendError ?? .other)
                    return
                }
                if (self?.shouldShowTermsAndConditions() ?? true) {
                    self?.showTermsAndConditions(with: dataProvider)
                } else {
                    self?.selectReward(with: dataProvider)
                }
            })
        }
    }
    
    func selectReward(with dataProvider: VIAARRewardViewModel) {
        self.showHUDOnView(view: self.view)
        delegate?.confirmReward(dataProvider, consumer: self, completion: { [weak self] (error, newRewardId, voucherNumber) in
            self?.hideHUDFromView(view: self?.view)
            if error != nil {
                self?.handleBackendErrorWithAlert(error as? BackendError ?? .other, tryAgainAction: { 
                    self?.selectReward(with: dataProvider)
                })
            }
            self?.confirmButton?.isEnabled = true
        })
    }
    
    func shouldShowTermsAndConditions() -> Bool {
        let coreRealm = DataProvider.newRealm()
        guard let rewardKey = dataProvider?.rewardkey else {
            return true
        }
        let rewardFeatureIsActive = coreRealm.rewardFeatureActive(with: rewardKey)
        return rewardFeatureIsActive
    }
    
	func postRegistration() {
		self.configureBarButtonItems()
		self.tableView.reloadData()
	}
	
	// MARK: - Private
	
	private func shouldShowErrorMessage() -> Bool {
		if let registrationEmail = registrationEmail {
			return !registrationEmail.isValidEmail()
		} else {
			return false
		}
	}
	
	private func emailIsValid() -> Bool {
		if let registrationEmail = registrationEmail, registrationEmail.isValidEmail() {
			return true
		} else {
			return false
		}
	}
    
    func showTermsAndConditions(with reward: VIAARRewardViewModel) {
        self.performSegue(withIdentifier: "showRewardTerms", sender: reward)
    }
}
