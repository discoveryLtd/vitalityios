import Foundation
import VitalityKit
import VIAUtilities
import VIAActiveRewards

class VIAUKEStarbucksRewardViewModel {
    internal let coreRealm = DataProvider.newRealm()
    
    internal var emailPreference: String? {
        let type = PreferenceTypeRef.StarbucksEmail.rawValue
        return coreRealm.partyGeneralPreference(with: type)?.value
    }
    
    func selectReward(with reward: VIAARRewardViewModel, completion: @escaping (Error?, Int?, String?) -> ()) {
        VIAARRewardHelper.selectReward(with: reward, with: ExchangeTypeRef.Selected, completion: completion)
    }
    
    func starbucksPrefreredEmailSame(as enteredEmail: String) -> Bool {
        if enteredEmail == emailPreference {
            return true
        }
        return false
    }
    
    func updateStarbucksEmailPreference(with enteredEmail: String, completion: @escaping (Error?) -> Void) {
        let partyId = coreRealm.getPartyId()
        let request = UpdatePartyRequest(partyId: partyId)
        request.generalPreference = UpdatePartyGeneralPreference(effectiveFrom: Date(), effectiveTo: Date.distantFuture, typeKey: .StarbucksEmail, value: enteredEmail)
        Wire.Party.update(request: request, completion: { error in
            completion(error)
        })
    }
}
