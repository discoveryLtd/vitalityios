//
//  VIAUKECinemaRewardViewController.swift
//  VitalityActive
//
//  Created by Joshua Ryan on 10/18/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import VIAUIKit
import VIAActiveRewards
import VitalityKit
import VIAUtilities
import VIACommon

class VIAUKECinemaRewardViewController: VIACoordinatedTableViewController, VIAABespokeRewardConsumer, ImproveYourHealthTintable {
	
	// MARK: Public
	var currentReward: VIAARRewardViewModel?
    var availableRewardSelections = [VIAARRewardViewModel]()
	var cinemaPartner: Int?
	var cinemaSelection: Int?
	var isErrorShowing: Bool = false
    var confirmButton: UIBarButtonItem?
	weak var delegate: VIAABespokeRewardDelegate?
    var congratualtionsText: String? = nil
	var pending: Bool = true
	var voucherCodes: [String] = [String]()
	
	// MARK: Private
	
	private var leftButton = UIBarButtonItem()
	
	// MARK: Lifecycle
	
	override func viewDidLoad() {
		super.viewDidLoad()
        
        configureTableView()
	}
    
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
        confirmCinemaReward()
		
        configureAvailableRewardSelections()
        configureVoucherCodes()
        configureTableView()
        configureBarButtonItems()
	}
    
    func confirmCinemaReward() {
        
        // Cinema has no T&C's to accept, confirm reward immediately after spin
        if let reward = currentReward, reward.accepted == nil {
            
            congratualtionsText = CommonStrings.Ar.Rewards.ConfirmRewardTitle690
            
            if self.delegate != nil {
                
                showHUDOnWindow()
                
                self.delegate?.confirmReward(reward, consumer: nil, completion: { [unowned self] error, reward, voucherNumber in
                    
                    self.hideHUDFromWindow()
                    
                    guard error == nil else {
                        
                        self.pending = self.isPending()
                        
                        self.handleBackendErrorWithAlert((error as? BackendError) ?? BackendError.other, tryAgainAction: { [weak self] in
                            self?.confirmCinemaReward()
                        })
                        return
                    }
                    
                    self.pending = self.isPending()
                })
            } else {
                self.pending = self.isPending()
            }
        } else {
            pending = isPending()
        }
    }
    
    func isPending() -> Bool {
        if (currentReward?.rewardStatus == .Allocated || currentReward?.rewardStatus == nil) && currentReward?.rewardkey == 8 {
            return true
        } else {
            return false
        }
    }
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		
		if let _ = self.tableView.indexPathForSelectedRow {
			self.confirmButton?.isEnabled = true
		} else {
			self.confirmButton?.isEnabled = false
		}
	}
    
    func configureAvailableRewardSelections() {
        guard let rewardId = currentReward?.id else {
            return
        }
        if currentReward?.rewardReference != .Cineworld && currentReward?.rewardReference != .Vue {
            let availableRewardViewModel = VIAARAvailableRewardViewModel()
            availableRewardViewModel.rewardId = rewardId
            
            availableRewardSelections = availableRewardViewModel.availableRewards
        }
    }
    
    func configureVoucherCodes() {
        guard let codes = currentReward?.voucherCodes else {
            return
        }
        voucherCodes = codes
        
        if let id = currentReward?.id, voucherCodes.count > 0 {
            cinemaPartner = id
        }

    }
	
	func configureTableView() {
		self.tableView.register(VIAARChooseRewardHeaderCell.nib(), forCellReuseIdentifier: VIAARChooseRewardHeaderCell.defaultReuseIdentifier)
		
		self.tableView.register(VIAUKECinemaRewardHeaderCell.nib(), forCellReuseIdentifier: VIAUKECinemaRewardHeaderCell.defaultReuseIdentifier)
		
		self.tableView.register(VIATextFieldTableViewCell.nib(), forCellReuseIdentifier: VIATextFieldTableViewCell.defaultReuseIdentifier)
		self.tableView.register(VIALabelTableViewCell.nib(), forCellReuseIdentifier: VIALabelTableViewCell.defaultReuseIdentifier)
		self.tableView.register(VIAUKECinemaRewardWithStatusCell.nib(), forCellReuseIdentifier: VIAUKECinemaRewardWithStatusCell.defaultReuseIdentifier)
		self.tableView.register(VIAARRewardWithExpirationStatusCell.nib(), forCellReuseIdentifier: VIAARRewardWithExpirationStatusCell.defaultReuseIdentifier)
		
		self.tableView.register(VIAUKECinemaRewardVoucherCell.nib(), forCellReuseIdentifier: VIAUKECinemaRewardVoucherCell.defaultReuseIdentifier)
		self.tableView.register(VIAARParticipatingPartnerCell.nib(), forCellReuseIdentifier: VIAARParticipatingPartnerCell.defaultReuseIdentifier)
		
		self.tableView.register(VIAARRewardMarkUsedCell.nib(), forCellReuseIdentifier: VIAARRewardMarkUsedCell.defaultReuseIdentifier)
		
		tableView.separatorInset = UIEdgeInsets.zero
		self.tableView.estimatedRowHeight = 400
		self.tableView.backgroundColor = UIColor.tableViewBackground()
	}
	
	// MARK: Action buttons
	
	func configureBarButtonItems() {
		
		let cancel = UIBarButtonItem(title: CommonStrings.CancelButtonTitle24, style: .plain, target: self, action: #selector(cancelButtonTapped(_:)))
		let done = UIBarButtonItem(title: CommonStrings.DoneButtonTitle53, style: .plain, target: self, action: #selector(doneButtonTapped(_:)))
		
		if isPending() {
			self.confirmButton = UIBarButtonItem(title: CommonStrings.NextButtonTitle84, style: .plain, target: self, action: #selector(nextButtonTapped(_:)))
		} else {
			self.confirmButton = UIBarButtonItem(title: CommonStrings.Ar.Rewards.ChooseRewardButtonTitle686, style: .plain, target: self, action: #selector(confirmButtonTapped(_:)))
		}
		
		if isPending() {
			self.navigationItem.leftBarButtonItem = nil
			self.navigationItem.rightBarButtonItem = done
		} else if let _ = cinemaPartner, voucherCodes.first != nil {
			self.navigationItem.leftBarButtonItem = nil
			self.navigationItem.rightBarButtonItem = done
		} else {
			self.navigationItem.leftBarButtonItem = cancel
			self.navigationItem.rightBarButtonItem = confirmButton
		}
		
		self.navigationItem.hidesBackButton = true
        configureScreenTitle()
	}

    func configureScreenTitle() {
        if voucherCodes.count > 0 {
            self.navigationItem.title = CommonStrings.Ar.Rewards.ChosenRewardTitle736
        } else if availableRewardSelections.count > 0  {
            if isPending() {
                self.navigationItem.title = CommonStrings.Ar.Rewards.ChosenRewardTitle736
            } else {
                self.navigationItem.title = CommonStrings.Ar.Rewards.CinemaRedeemRewardTitle1108
            }
            
        } else {
            self.navigationItem.title = CommonStrings.Ar.Partners.StarbucksName734
        }
    }
	
	// MARK: UITableView datasource
	
	override func numberOfSections(in tableView: UITableView) -> Int {
		return 1
	}
	
	override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		if isPending() {
			return 1
		} else if let _ = cinemaPartner, voucherCodes.count > 0 {
			// TODO: increase to 2 + voucherCodes.count when mark as used is rescoped.
			return 2 + voucherCodes.count
		} else {
			return 2 + availableRewardSelections.count
		}
	}
	
	// MARK: UITableView delegate
	
	override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let status: AwardedRewardStatusRef? = currentReward?.rewardStatus
		if isPending() {
			return self.configurePendingStatusCell(indexPath)
		} else if let _ = cinemaPartner, voucherCodes.count > 0 {
			if indexPath.row == 0 {
				return configureRewardHeaderCell(indexPath)
			} else if indexPath.row == voucherCodes.count + 1 {
				return self.configureInstructionCell(indexPath)
			} else if indexPath.row == voucherCodes.count + 2 && status == AwardedRewardStatusRef.Issued  {
				return self.configureMarkUsed(indexPath)
			} else {
				return self.configureVouchersCell(indexPath)
			}
		} else {
			if indexPath.row == 0 {
				return self.configureCongratulationsCell(indexPath)
			} else if indexPath.row == availableRewardSelections.count + 1 {
				return self.configureDisclaimerCell(indexPath)
			} else {
				return self.configurePartnerCell(indexPath)
			}
		}
		
		return tableView.dequeueReusableCell(withIdentifier: VIATableViewCell.defaultReuseIdentifier, for: indexPath)
	}
	
	override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		if tableView.numberOfSections == 1, tableView.numberOfRows(inSection: indexPath.section) == 1 {
			return tableView.frame.size.height
		} else {
			return UITableViewAutomaticDimension
		}
	}
	
	// MARK: - Status Specific TableCells
	
	// MARK: Available
	func configurePartnerCell(_ indexPath: IndexPath) -> UITableViewCell {
        guard let cell = self.tableView.dequeueReusableCell(withIdentifier: VIAARParticipatingPartnerCell.defaultReuseIdentifier, for: indexPath) as? VIAARParticipatingPartnerCell else {
            return tableView.defaultTableViewCell()
        }
        guard let rewardSelection = availableRewardSelections.element(at: indexPath.row - 1) else {
            return cell
        }
        
		cell.partnerName = rewardSelection.title
		cell.descriptionText = rewardSelection.subTitle
		cell.cellImage = rewardSelection.image
		
		if let cinemaSelection = cinemaSelection, indexPath.row - 1 == cinemaSelection {
			cell.accessoryType = .checkmark
		} else {
			cell.accessoryType = .none
		}
		
		if indexPath.row == availableRewardSelections.count {
			cell.separatorInset = UIEdgeInsets.zero
		} else {
			cell.setDefaultInset()
		}
		
		return cell
	}
	
	
	func configureCongratulationsCell(_ indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: VIAARChooseRewardHeaderCell.defaultReuseIdentifier, for: indexPath) as? VIAARChooseRewardHeaderCell else {
            return tableView.defaultTableViewCell()
        }
		
		guard let _ = currentReward else {
			return cell
		}
		
		cell.title = CommonStrings.Ar.Rewards.CinemaRedeemChooseTitle1109
		cell.cellImage = UKEssentialsAsset.Rewards.rewardTrophyBig.image
		cell.descriptionText = CommonStrings.Ar.Rewards.CinemaRedeemDescription1110
		
		cell.selectionStyle = .none
		cell.separatorInset = UIEdgeInsets.zero
		return cell
	}
	
	func configureDisclaimerCell(_ indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: VIALabelTableViewCell.defaultReuseIdentifier, for: indexPath) as? VIALabelTableViewCell else {
            return tableView.defaultTableViewCell()
        }
		
		//cell.labelText = CommonStrings.Ar.Partners.StarbucksRewardFootnote1062
		cell.configure(font: .footnoteFont(), color: .gray)
		cell.selectionStyle = .none
		cell.backgroundColor = UIColor.tableViewBackground()
		cell.separatorInset = UIEdgeInsetsMake(0, self.tableView.frame.size.width, 0, 0)
		
		return cell
	}
	
	// MARK: Usable
	func configureRewardHeaderCell(_ indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: VIAUKECinemaRewardHeaderCell.defaultReuseIdentifier, for: indexPath) as? VIAUKECinemaRewardHeaderCell else {
            return tableView.defaultTableViewCell()
        }
		
		guard let reward = currentReward else {
			return cell
		}

		cell.rewardHeader = reward.title
		cell.rewardContent = reward.subTitle
        
        switch reward.rewardReference {
        case .Cineworld :
            cell.rewardImage = UKEssentialsAsset.Rewards.cineworld.image
        case .Vue :
            cell.rewardImage = UKEssentialsAsset.Rewards.vue.image
        case .CineworldOrVue:
            cell.rewardImage = UKEssentialsAsset.Rewards.cinemas.image
        default:
            cell.rewardImage = reward.image
        }

		
		cell.separatorInset = UIEdgeInsetsMake(0, self.tableView.frame.size.width, 0, 0)
		cell.selectionStyle = .none
		return cell
	}
	
	func configureVouchersCell(_ indexPath: IndexPath) -> UITableViewCell {
        guard let cell = self.tableView.dequeueReusableCell(withIdentifier: VIAUKECinemaRewardVoucherCell.defaultReuseIdentifier, for: indexPath) as? VIAUKECinemaRewardVoucherCell else {
            return tableView.defaultTableViewCell()
        }
		
		guard indexPath.row <= voucherCodes.count else {
			return cell
		}
		
		let code = voucherCodes[indexPath.row - 1]
		
		if voucherCodes.count == 1 {
			cell.title = CommonStrings.Ar.VoucherCodeTitle664
		} else {
			cell.title = CommonStrings.Ar.Rewards.CinemaMultipleVoucherCodesTitle1111(indexPath.row.description)
		}
		cell.voucherCode = code
		
		cell.separatorInset = UIEdgeInsetsMake(0, self.tableView.frame.size.width, 0, 0)
		cell.selectionStyle = .none
		return cell
	}
	
	func configureInstructionCell(_ indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: VIALabelTableViewCell.defaultReuseIdentifier, for: indexPath) as? VIALabelTableViewCell else {
            return tableView.defaultTableViewCell()
        }
        
        guard let reward = currentReward else {
            return cell
        }
        
        switch reward.rewardReference {
        case .Cineworld :
            cell.labelText = CommonStrings.Ar.Rewards.CinemaVoucherFooter1112
        case .Vue :
            cell.labelText = CommonStrings.Ar.Rewards.VoucherCodeInstruction1329
            default:
            cell.labelText = ""
        }
		
        cell.configure(font: .bodyFont(), color: .gray, textAlignment: .center)
		
		cell.backgroundColor = UIColor.day()
		cell.selectionStyle = .none
		cell.separatorInset = UIEdgeInsetsMake(0, self.tableView.frame.size.width, 0, 0)
		
		return cell
	}
	
	func configureMarkUsed(_ indexPath: IndexPath) -> UITableViewCell {
         return tableView.defaultTableViewCell()
//        TODO: Once in scope add code back to show "Mark as Used" back 
//        guard let cell = tableView.dequeueReusableCell(withIdentifier: VIAARRewardMarkUsedCell.defaultReuseIdentifier, for: indexPath) as? VIAARRewardMarkUsedCell else {
//            return tableView.defaultTableViewCell()
//        }
//        cell.actionText = "Mark as Used"
//		cell.instructions = "Already used this reward? Mark it as used to move it to your reward history."
//		cell.buttonAction = { [weak self] in
//			
//			if let strongSelf = self, let reward = strongSelf.currentReward {
//				strongSelf.delegate?.consumeReward(reward, completion: nil)
//			}
//		}
//		
//		cell.selectionStyle = .none
//		cell.separatorInset = UIEdgeInsetsMake(0, self.tableView.frame.size.width, 0, 0)
//		
//		return cell
	}

	// MARK: Pending
	
	func configurePendingStatusCell(_ indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: VIAUKECinemaRewardWithStatusCell.defaultReuseIdentifier, for: indexPath) as? VIAUKECinemaRewardWithStatusCell else {
            return tableView.defaultTableViewCell()
        }
		guard let reward = currentReward else {
            return cell
        }

        let isHidden = congratualtionsText == nil
        cell.configureCongratulationsMessage(isHidden: isHidden, text: congratualtionsText)
        
        cell.rewardImage = UKEssentialsAsset.Rewards.cinemas.image
		cell.rewardHeader = reward.subTitle
		cell.rewardContent = CommonStrings.Ar.Rewards.CinemaWayToGoMessage1113(reward.subTitle.lowercased())
		cell.statusHeader = CommonStrings.Ar.Rewards.CinemaVoucherPromoCodeTitle1114
		cell.status = CommonStrings.Ar.GoalPendingTitle693
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MMMM-yyyy"
        
        if let effectiveTo = reward.effectiveToDate {
            cell.details = CommonStrings.Ar.Rewards.CinemaPendingFooter1115(dateFormatter.string(from: effectiveTo))

        }
        
		cell.selectionStyle = .none
		return cell
	}
	

	override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
		return nil
	}
	
	override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
		return CGFloat.leastNormalMagnitude
	}
	
	override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
		return CGFloat.leastNormalMagnitude
	}
	
	override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		
		let rowOffset = 1 // Number of rows above partner selection.
		if indexPath.row >= rowOffset, indexPath.row - rowOffset < availableRewardSelections.count {
			cinemaSelection = indexPath.row - rowOffset
			confirmButton?.isEnabled = true
			tableView.reloadData()
		}
	}
	
	
	// MARK: - VIAABespokeRewardConsumer
	
	func swapReward() {
		if let reward = currentReward {
			delegate?.consumeReward(reward, completion: nil)
		}
	}
	
	func confirmReward() {
		if let reward = currentReward {
			delegate?.confirmReward(reward, consumer: self, completion: nil)
		}
	}
    
    func confirmRewardSelection(selection: VIAARRewardViewModel) {
        delegate?.confirmReward(selection, consumer: self, completion: nil)
    }
	
	// MARK: - IBAction
	
	@IBAction func cancelButtonTapped (_ sender: Any) {
		delegate?.cancel(consumer: self)
	}
	
	@IBAction func confirmButtonTapped (_ sender: Any) {
		verifyPartnerSelection()
	}
	
	@IBAction func doneButtonTapped (_ sender: Any) {
		self.delegate?.cancel(consumer: self)
	}
	
	@IBAction func nextButtonTapped (_ sender: Any) {
		self.pending = true
		self.configureBarButtonItems()
		self.tableView.reloadData()
	}
	
	// MARK: - VIAABespokeRewardConsumer
	
	func setReward(_ reward: VIAARRewardViewModel) {
		self.currentReward = reward
	}
	
	func verifyPartnerSelection() {
		
		if let _ = cinemaSelection {
			let alert = UIAlertController(title: CommonStrings.Ar.Rewards.CinemaSelectionDialogTitle1116,
			                              message: CommonStrings.Ar.Rewards.CinemaSelectionDialogDescription1117,
				preferredStyle: .alert)
			
			alert.addAction(UIAlertAction(title: CommonStrings.CancelButtonTitle24,
			                              style: UIAlertActionStyle.cancel,
			                              handler: nil
			))
			
			alert.addAction(UIAlertAction(title: CommonStrings.ConfirmTitleButton182,
			                              style: UIAlertActionStyle.default,
			                              handler: { [unowned self] (UIAlertAction) in
											
                                            if let selectionIndex = self.cinemaSelection, let selection = self.availableRewardSelections.element(at: selectionIndex) {
                                                self.confirmRewardSelection(selection: selection)
                                            }
											self.configureBarButtonItems()
											self.tableView.reloadData()

			}))
			
			self.present(alert, animated: true, completion: nil)
		}
	}
}
