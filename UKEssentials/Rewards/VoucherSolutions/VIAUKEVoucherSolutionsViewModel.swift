//
//  VIAUKEVoucherSolutionsViewModel.swift
//  UKEssentials
//
//  Created by Von Kervin R. Tuico on 29/04/2019.
//  Copyright © 2019 Glucode. All rights reserved.
//

import Foundation
import VitalityKit
import VIAUtilities
import VIAActiveRewards

class VIAUKEVoucherSolutionsViewModel {
    
    internal let coreRealm = DataProvider.newRealm()
    
    func getReward(rewardID: Int, completion: @escaping (Error?, VIAARRewardViewModel?) -> Void) {
        VIAARRewardHelper.getReward(rewardID) { (error, response) in
            guard let error = error else {
                completion(nil, response)
                return
            }
            completion(error, nil)
        }
    }
    
    /** FC-26793 : UKE : Change Request
     * Determine the Feature Group of the Account.
     */
    func getFeatureGroupValue() -> String {
        var featureGroupValue = ""
        guard let partyDetails = coreRealm.currentVitalityParty() else {
            return ""
        }
        
        let references = partyDetails.references
        guard !references.isEmpty else {
            return ""
        }
        
        for reference in references {
            /* "15" = Feeature Group */
            if reference.type == "15" {
                featureGroupValue = reference.value
                return featureGroupValue
            }
        }
        return ""
    }
}
