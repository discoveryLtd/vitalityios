//
//  VIAUKEVoucherSolutionsViewController.swift
//  UKEssentials
//
//  Created by Von Kervin R. Tuico on 24/04/2019.
//  Copyright © 2019 Glucode. All rights reserved.
//

import VIAActiveRewards
import Foundation
import VIAUIKit
import VIAActiveRewards
import VitalityKit
import VIAUtilities
import VIACommon

class VIAUKEVoucherSolutionsViewController: VIACoordinatedTableViewController, VIAABespokeRewardConsumer {
    
    internal let voucherSolutionsViewModel = VIAUKEVoucherSolutionsViewModel()
    var delegate: VIAABespokeRewardDelegate?
    var dataProvider: VIAARRewardViewModel?
    private var congratulationsSolutionView: VIAARRewardCongratulationsSolution?
    var hyperlinkTextRange: NSRange!
    let REDEEM_REWARD_STATIC_LINK = URL(string: "https://www.healthy-workplace.com/globalrewards")!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        VIAAppearance.setGlobalTintColorToImproveYourHealth()
        configureTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        configureBarButtonItems()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        configureVoucherView()
    }
    
    func configureTableView() {
        tableView.separatorInset = UIEdgeInsets.zero
        tableView.separatorStyle = .none
        
        self.tableView.allowsSelection = false
        self.tableView.backgroundColor = UIColor.tableViewBackground()
    }
    
    /* Set Navigation Bar Items */
    func configureBarButtonItems() {
        /* Add Navigation Bar Button */
        let done = UIBarButtonItem(title: CommonStrings.DoneButtonTitle53, style: .plain, target: self, action: #selector(doneButtonTapped(_:)))
        self.navigationItem.rightBarButtonItem = done
        
        /* Hide the Back Button */
        self.navigationItem.hidesBackButton = true
        
        /* Set Navigation Bar Title */
        self.navigationItem.title = CommonStrings.Ar.Partners.StarbucksName734
    }
    
    /* Action Trigger when tapping Navigation Bar Done Button */
    @IBAction func doneButtonTapped (_ sender: Any) {
        self.delegate?.cancel(consumer: self)
    }
    
    func configureVoucherView() {
        guard let viewModel = dataProvider else {
            return
        }
        
        self.getReward(rewardID: viewModel.voucherId, model: viewModel)
    }
    
    func configureShortTermSolutionView(viewModel: VIAARRewardViewModel, voucherNumber: String? = nil) {
        guard let solutionView = VIAARRewardCongratulationsSolution.viewFromNib(owner: self) else {
            return
        }
        
        solutionView.frame = .zero
        solutionView.header = CommonStrings.Ar.Vouchersolution.RewardTitle2620(viewModel.title, viewModel.subTitle)
        
        if voucherSolutionsViewModel.getFeatureGroupValue().lowercased() == "india" {
            solutionView.imageViewIsHidden = true
            solutionView.headerVoucherRewardNameViewIsHidden = false
            solutionView.headerVoucherRewardName = viewModel.title
        } else {
            solutionView.imageViewIsHidden = false
            solutionView.headerVoucherRewardNameViewIsHidden = true
            solutionView.image = viewModel.image
        }
        solutionView.rewardHeader = viewModel.title
        solutionView.rewardContent = viewModel.subTitle
        solutionView.rewardAchievedDateIsHidden = true
        
        solutionView.rewardDisclaimer = CommonStrings.Ar.Vouchersolution.RewardNotice2621
        
        /* Short Term Solution Section */
        solutionView.rewardShortTermViewIsHidden = false
        solutionView.rewardShortTermEmailedDate = CommonStrings.Ar.Vouchersolution.RewardAvailability2619("\(self.customDateFormatter(date: viewModel.effectiveFromDate))")
        
        /* Long Term Solution Section */
        solutionView.rewardLongTermViewIsHidden = true
        
        solutionView.layoutMargins = UIEdgeInsets(top: 0, left: 23.0, bottom: 0, right: 23.0)
        self.view.addSubview(solutionView)
        solutionView.snp.makeConstraints({ (make) in
            make.width.equalToSuperview()
            make.left.right.top.equalToSuperview()
            make.height.equalToSuperview().offset(self.tableView.contentOffset.y)
        })
        
        self.congratulationsSolutionView = solutionView
    }
    
    func configureLongTermSolutionView(viewModel: VIAARRewardViewModel, voucherNumber: String? = nil) {
        guard let solutionView = VIAARRewardCongratulationsSolution.viewFromNib(owner: self) else {
            return
        }
        
        solutionView.frame = .zero
        solutionView.header = CommonStrings.Ar.Vouchersolution.RewardTitle2620(viewModel.title, viewModel.subTitle)
        
        if voucherSolutionsViewModel.getFeatureGroupValue().lowercased() == "india" {
            solutionView.imageViewIsHidden = true
            solutionView.headerVoucherRewardNameViewIsHidden = false
            solutionView.headerVoucherRewardName = viewModel.title
        } else {
            solutionView.imageViewIsHidden = false
            solutionView.headerVoucherRewardNameViewIsHidden = true
            solutionView.image = viewModel.image
        }
        solutionView.rewardHeader = viewModel.title
        solutionView.rewardContent = viewModel.subTitle
        solutionView.rewardAchievedDateIsHidden = false
        if let expirationDate = viewModel.expiration {
            let dateFormat = Localization.dayDateLongMonthyearFormatter.string(from: expirationDate)
            solutionView.rewardAchievedDate = CommonStrings.Ar.VoucherExpires658(dateFormat)
        }
        
        solutionView.rewardDisclaimer = CommonStrings.Ar.Vouchersolution.RewardNotice2621
        
        /* Short Term Solution Section */
        solutionView.rewardShortTermViewIsHidden = true
        
        /* Long Term Solution Section */
        solutionView.rewardLongTermViewIsHidden = false
        solutionView.rewardLongTermVoucherTitle = CommonStrings.Ar.Vouchersolution.RewardVoucherTitle2630
        
        /* Get the Voucher Code */
        if let voucherCode = voucherNumber {
            /* Voucher Code is from ExchangeReward api. */
            solutionView.rewardLongTermVoucherCode = voucherCode
        } else {
            /* Voucher Code is from GetAwardedRewardByID api. */
            if let voucherCode = viewModel.awardedRewardReferences?.first {
                solutionView.rewardLongTermVoucherCode = voucherCode
            }
        }
        
        let staticHerelabel = "here"
        let redeemText = "\(CommonStrings.Ar.Vouchersolution.RewardVoucherRedeemMessage2631(staticHerelabel))"
        let hyperlinkTextRange = (redeemText as NSString).range(of: staticHerelabel)
        let attributedString = NSMutableAttributedString(string: redeemText)
        
        /* Set how links should appear: blue and underlined */
        attributedString.addAttribute(NSForegroundColorAttributeName, value: UIColor.primaryColor() , range: hyperlinkTextRange)
        attributedString.addAttribute(NSUnderlineStyleAttributeName, value: NSUnderlineStyle.styleSingle.rawValue, range: hyperlinkTextRange)
        
        /* Set the hyperlinkTextRange to be the link */
        attributedString.addAttribute(NSLinkAttributeName, value: REDEEM_REWARD_STATIC_LINK, range: hyperlinkTextRange)
        solutionView.rewardLongTermRedeemLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(loadHyperlinkURL(_:))))
        solutionView.rewardLongTermRedeemLabel.attributedText = attributedString
        solutionView.rewardLongTermRedeemLabel.isUserInteractionEnabled = true
        self.hyperlinkTextRange = hyperlinkTextRange
        
        solutionView.layoutMargins = UIEdgeInsets(top: 0, left: 23.0, bottom: 0, right: 23.0)
        self.view.addSubview(solutionView)
        solutionView.snp.makeConstraints({ (make) in
            make.width.equalToSuperview()
            make.left.right.top.equalToSuperview()
            make.height.equalToSuperview().offset(self.tableView.contentOffset.y)
        })
        
        self.congratulationsSolutionView = solutionView
    }
    
    func loadHyperlinkURL(_ sender: UITapGestureRecognizer) {
        UIApplication.shared.open(REDEEM_REWARD_STATIC_LINK, options: [:], completionHandler: nil)
    }
    
    func getReward(rewardID: Int, model: VIAARRewardViewModel) {
        /** FC-26793 : UKE : Change Request
         * Always populate first the data of "awardedReward" to determine if "exchangeReward" api has been called.
         */
        self.showHUDOnView(view: self.view)
        self.voucherSolutionsViewModel.getReward(rewardID: rewardID) { [weak self] (error, response) in
            guard error == nil else {
                self?.handleErrorOccurredForGetReward(error!, model)
                return
            }
            
            self?.removeStatusView()
            
            /** FC-26793 : UKE : Change Request
             * Condition below is used to determine if the "exchangeReward" api call has already been triggered.
             * If YES : Show the UI Directly.
             * If NO : Trigger the succeeding api calls.
             */
            guard response?.exchangeOn != nil else {
                /* Call the API Services */
                self?.selectReward(with: model)
                return
            }
            
            guard let reward = response else {
                return
            }
            
            self?.hideHUDFromView(view: self?.view)
            
            if self?.voucherSolutionsViewModel.getFeatureGroupValue().lowercased() == "smallcountries" {
                self?.configureShortTermSolutionView(viewModel: reward)
            } else {
                self?.configureLongTermSolutionView(viewModel: reward)
            }
        }
    }
    
    func handleErrorOccurredForGetReward(_ error: Error, _ dataProvider: VIAARRewardViewModel) {
        let backendError = error as? BackendError ?? BackendError.other
        let statusView = self.handleBackendErrorWithStatusView(backendError, tryAgainAction: { [weak self] in
            self?.getReward(rewardID: dataProvider.voucherId, model: dataProvider)
        })
        configureStatusView(statusView)
    }
    
    func selectReward(with dataProvider: VIAARRewardViewModel) {
        delegate?.confirmReward(dataProvider, consumer: self, completion: { [weak self] (error, newRewardId, voucherNumber) in
            self?.hideHUDFromView(view: self?.view)
            
            guard error == nil else {
                self?.handleErrorOccurred(error!, dataProvider)
                return
            }
            
            self?.removeStatusView()
            
            if self?.voucherSolutionsViewModel.getFeatureGroupValue().lowercased() == "smallcountries" {
                self?.configureShortTermSolutionView(viewModel: dataProvider)
            } else {
                self?.configureLongTermSolutionView(viewModel: dataProvider, voucherNumber: voucherNumber)
            }
        })
    }
    
    func handleErrorOccurred(_ error: Error, _ dataProvider: VIAARRewardViewModel) {
        let backendError = error as? BackendError ?? BackendError.other
        let statusView = self.handleBackendErrorWithStatusView(backendError, tryAgainAction: { [weak self] in
            self?.selectReward(with: dataProvider)
        })
        configureStatusView(statusView)
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    
    
}

extension VIAUKEVoucherSolutionsViewController {
    
    //
    func setReward(_ reward: VIAARRewardViewModel) {
        dataProvider = reward
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 0
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return UITableViewCell()
    }
    
    func customDateFormatter(date: Date?) -> String {
        let calendar = Calendar.current
        let dateValue = date ?? Date()
        let dateComponents = calendar.component(.day, from: dateValue)
        
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .ordinal
        let day = numberFormatter.string(from: dateComponents as NSNumber)
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM yyyy"
        
        return "\(day!) of \(dateFormatter.string(from: dateValue))"
    }
}
