//
//  VIAUKECinemaRewardWithStatusCell.swift
//  UKEssentials
//
//  Created by Joshua Ryan on 11/9/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

import UIKit
import VIAUIKit

public class VIAUKECinemaRewardWithStatusCell: UITableViewCell, Nibloadable {
	
    @IBOutlet weak var logoStackViewTopConstraint: NSLayoutConstraint!
	// MARK: Private
	@IBOutlet weak var rewardImageView: UIImageView!
	
    @IBOutlet weak var congratulationsLabel: UILabel!
	@IBOutlet weak var rewardHeaderLabel: UILabel!
	@IBOutlet weak var rewardContentLabel: UILabel!
	@IBOutlet weak var statusHeaderLabel: UILabel!
	@IBOutlet weak var statusLabel: UILabel!
	@IBOutlet weak var detailsLabel: UILabel!
	
	@IBOutlet weak var stackView: UIStackView!
	
	// MARK: Public
	public var rewardImage: UIImage? {
		didSet {
			self.rewardImageView?.image = self.rewardImage
		}
	}
	
	public var rewardHeader: String = "" {
		didSet {
			self.rewardHeaderLabel?.text = self.rewardHeader
		}
	}
	
	public var rewardContent: String = "" {
		didSet {
			self.rewardContentLabel?.text = self.rewardContent
		}
	}
	
	public var statusHeader: String = "" {
		didSet {
			self.statusHeaderLabel?.text = self.statusHeader
		}
	}
	
	public var status: String = "" {
		didSet {
			self.statusLabel.text = self.status
		}
	}
	
	public var details: String = "" {
		didSet {
			self.detailsLabel.text = self.details
		}
	}
	
	public override func awakeFromNib() {
		super.awakeFromNib()
		setupView()
	}
	
	public func addAdditionalView(view: UIView) {
		self.stackView.addArrangedSubview(view)
	}
	
	func setupView() {
		
		// reset IB values
		self.rewardImageView?.image = nil
		self.rewardHeaderLabel?.text = nil
		self.rewardContentLabel?.text = nil
		self.statusHeaderLabel?.text = nil
		self.detailsLabel?.text = nil
		self.statusLabel?.text = nil
        self.congratulationsLabel.text = nil
        self.congratulationsLabel.font = .title1Font()
        self.congratulationsLabel.isHidden = true
	}
    
    func configureCongratulationsMessage(isHidden: Bool, text: String?) {
        congratulationsLabel.text = text
        congratulationsLabel.isHidden = isHidden
        
        if congratulationsLabel.isHidden {
            logoStackViewTopConstraint.constant = 0
        } else {
            logoStackViewTopConstraint.constant = 30
        }
    }
}
