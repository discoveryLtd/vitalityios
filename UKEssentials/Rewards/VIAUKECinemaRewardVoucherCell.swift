//
//  VIAUKECinemaRewardVoucherCell.swift
//  UKEssentials
//
//  Created by Joshua Ryan on 10/24/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

import UIKit
import VIAUIKit

public class VIAUKECinemaRewardVoucherCell: UITableViewCell, Nibloadable {
	
	@IBOutlet var titleLabel: UILabel!
	public var title: String? {
		set {
			self.titleLabel.text = newValue
		}
		get {
			return self.titleLabel.text
		}
	}
	
	@IBOutlet var voucherLabel: UILabel!
	public var voucherCode: String? {
		set {
			self.voucherLabel.text = newValue
		}
		get {
			return self.voucherLabel.text
		}
	}
	
	public override func awakeFromNib() {
		super.awakeFromNib()

		self.titleLabel.text = nil
		self.titleLabel.font = UIFont.bodyFont()
		self.titleLabel.textColor = UIColor.night()
		
		self.voucherLabel.text = nil
		self.voucherLabel.font = UIFont.title1Font()
		self.voucherLabel.textColor = UIColor.night()

		self.accessoryType = .none
	}
}
