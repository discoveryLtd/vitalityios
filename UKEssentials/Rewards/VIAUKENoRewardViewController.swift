//
//  VIAUKENoRewardViewController.swift
//  UKEssentials
//
//  Created by Joshua Ryan on 11/13/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import VIAUIKit
import VIAActiveRewards
import VitalityKit
import VIAUtilities
import VIACommon

class VIAUKENoRewardViewController: VIACoordinatedViewController, VIAABespokeRewardConsumer {
	
    @IBOutlet weak var noRewardIconImageView: UIImageView!
    @IBOutlet weak var noRewardTitleLabel: UILabel!
    @IBOutlet weak var noRewardDescriptionLabel: UILabel!
	// MARK: Public
	
    let coreRealm = DataProvider.newRealm()
	var viewModel: VIAARRewardViewModel?
    var delegate: VIAABespokeRewardDelegate?
	
	// MARK: Lifecycle
	
    override func viewDidLoad() {
        super.viewDidLoad()
        VIAAppearance.setGlobalTintColorToImproveYourHealth()
        configureBarButtonItems()
        configureContent()
        updateNoRewardStatustoUsed()
    }
    
    func updateNoRewardStatustoUsed() {
        if let reward = viewModel {
            showHUDOnView(view: self.view)
            delegate?.updateRewardStatusToUsed(reward, completion: { [weak self] (error, response) in
                self?.hideHUDFromView(view: self?.view)
                if let serviceError = error {
                    self?.handleErrorOccurred(serviceError)
                    return
                }
                self?.deleteNoRewardData()
                return
            })
        }
    }
	
	// MARK: Action buttons
    
	func configureBarButtonItems() {
		let done = UIBarButtonItem(title: CommonStrings.DoneButtonTitle53, style: .plain, target: self, action: #selector(doneButtonTapped(_:)))
		self.navigationItem.rightBarButtonItem = done
		self.navigationItem.hidesBackButton = true
		self.navigationItem.title = CommonStrings.Ar.Partners.StarbucksName734
	}
    
    func configureContent() {
        noRewardIconImageView.image = VIAActiveRewardsAsset.ActiveRewards.Rewards.UKEPartners.noRewardLarge.image
        noRewardDescriptionLabel.textColor = UIColor.night()
        noRewardTitleLabel.font = UIFont.title1Font()
        noRewardTitleLabel.text = CommonStrings.Ar.Rewards.NoRewardTitle1119
        noRewardDescriptionLabel.textColor = UIColor.night()
        noRewardDescriptionLabel.font = UIFont.bodyFont()
        noRewardDescriptionLabel.text = CommonStrings.Ar.Rewards.NoRewardDescription1120
    }
	
	// MARK: - IBAction
	
	@IBAction func doneButtonTapped (_ sender: Any) {
        self.delegate?.cancel(consumer: self)
	}
    
    func deleteNoRewardData() {
        guard let rewardId = viewModel?.unclaimedRewardId else {
            return
        }
        
        let arRealm = DataProvider.newARRealm()
        arRealm.refresh()
        
        let reward = arRealm.unclaimedRewards(with: rewardId)
        
        do {
            try arRealm.write {
                arRealm.delete(reward)
            }
        } catch {
            print("Unable to delete noReward data")
        }
    }

	// MARK: - VIAABespokeRewardConsumer
	
	func setReward(_ reward: VIAARRewardViewModel) {
		self.viewModel = reward
	}
    
    //MARK: - Error handling

    func handleErrorOccurred(_ error: Error) {
        switch error {
        case is BackendError:
            if let backendError = error as? BackendError {
                handleBackendErrorWithAlert(backendError)
            }
        default:
            displayUnknownServiceErrorOccurredAlert()
        }
    }
}
