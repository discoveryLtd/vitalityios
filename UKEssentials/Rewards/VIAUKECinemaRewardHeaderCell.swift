//
//  VIAUKECinemaRewardHeaderCell.swift
//  UKEssentials
//
//  Created by Joshua Ryan on 11/9/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

import UIKit
import VIAUIKit

public final class VIAUKECinemaRewardHeaderCell: UITableViewCell, Nibloadable {

	// MARK: Private
	@IBOutlet weak var rewardImageView: UIImageView!
	
	@IBOutlet weak var rewardHeaderLabel: UILabel!
	@IBOutlet weak var rewardContentLabel: UILabel!
	
	@IBOutlet weak var stackView: UIStackView!
	
	// MARK: Public
	public var rewardImage: UIImage? {
		didSet {
			self.rewardImageView?.image = self.rewardImage
		}
	}
	
	public var rewardHeader: String = "" {
		didSet {
			self.rewardHeaderLabel?.text = self.rewardHeader
		}
	}
	
	public var rewardContent: String = "" {
		didSet {
			self.rewardContentLabel?.text = self.rewardContent
		}
	}
	
	public override func awakeFromNib() {
		super.awakeFromNib()
		setupView()
	}
	
	public func addAdditionalView(view: UIView) {
		self.stackView.addArrangedSubview(view)
	}
	
	func setupView() {
		
		// reset IB values
		self.rewardImageView?.image = nil
		self.rewardHeaderLabel?.text = nil
		self.rewardContentLabel?.text = nil
	}
}

