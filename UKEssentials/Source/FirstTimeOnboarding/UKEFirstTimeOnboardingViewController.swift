//
//  UKEFirstTimeOnboardingViewController.swift
//  VIACore
//
//  Created by Val Tomol on 12/07/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation

import VIAUIKit
import VitalityKit
import VIACommon

import RazzleDazzle

public class UKEFirstTimeOnboardingViewController: AnimatedPagingScrollViewController {
    // MARK: Properties
    let pageControl = UIPageControl()
    var imageView0 = UIImageView()
    var imageView1 = UIImageView()
    var imageView2 = UIImageView()
    var imageView3 = UIImageView()
    var startButton: VIAButton = VIAButton(title: UKEFirstTimeOnboardingViewController.buttonTitle())
    
    override public func numberOfPages() -> Int {
        return 4
    }
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        
        self.contentView.backgroundColor = UIColor.white
        self.scrollView.backgroundColor = UIColor.white
        self.scrollView.isPagingEnabled = true
        self.scrollView.bounces = false
        
        configureOnboardingViews()
    }
    
    // MARK: View Configurations
    func configureOnboardingViews() {
        addImageViewsToContentView(contentView: contentView)
        addLabelsWith(headingText: UKEFirstTimeOnboardingViewController.heading0(), subtitleText: UKEFirstTimeOnboardingViewController.subtitle0(), page: 0, contentView: contentView)
        addLabelsWith(headingText: UKEFirstTimeOnboardingViewController.heading1(), subtitleText: UKEFirstTimeOnboardingViewController.subtitle1(), page: 1, contentView: contentView)
        addLabelsWith(headingText: UKEFirstTimeOnboardingViewController.heading2(), subtitleText: UKEFirstTimeOnboardingViewController.subtitle2(), page: 2, contentView: contentView)
        addLabelsWith(headingText: UKEFirstTimeOnboardingViewController.heading3(), subtitleText: UKEFirstTimeOnboardingViewController.subtitle3(), page: 3, contentView: contentView)
        addStartButtonToContentView(contentView: contentView)
        addPageControlToContentView(contentView: view)
    }
    
    func addPageControlToContentView(contentView: UIView) {
        configurePageControl()
        contentView.addSubview(pageControl)
        
        let centerConstraint = NSLayoutConstraint(item: pageControl, attribute: .centerX, relatedBy: .equal, toItem: contentView, attribute: .centerX, multiplier: 1, constant: 0)
        let bottomConstraint = NSLayoutConstraint(item: pageControl, attribute: .bottom, relatedBy: .equal, toItem: contentView, attribute: .bottomMargin, multiplier: 1, constant: 0)
        contentView.addConstraints([centerConstraint, bottomConstraint])
    }
    
    func configurePageControl() {
        pageControl.translatesAutoresizingMaskIntoConstraints = false
        pageControl.numberOfPages = 4
        pageControl.pageIndicatorTintColor = UIColor.lightGrey()
        pageControl.currentPageIndicatorTintColor = UIColor.crimsonRed()
    }
    
    // MARK: Labels
    func addLabelsWith(headingText: String, subtitleText: String, page: Int, contentView: UIView) {
        let screenWidth = UIScreen.main.bounds.size.width
        
        let headingLabel = newLabelWithText(text: headingText)
        headingLabel.font = UIFont.onboardingHeading()
        headingLabel.textColor = UIColor.flatBlack()
        let subtitleLabel = newLabelWithText(text: subtitleText)
        subtitleLabel.font = UIFont.onboardingSubtitle()
        subtitleLabel.textColor = UIColor.darkGrey()
        
        let stackView = UIStackView()
        stackView.addArrangedSubview(headingLabel)
        stackView.addArrangedSubview(subtitleLabel)
        stackView.axis = .vertical
        stackView.alignment = .center
        stackView.spacing = 20
        contentView.addSubview(stackView)
        keepView(stackView, onPage: CGFloat(page))
        
        alignStackView(stackView, toImageView: imageView0, inContentView: contentView)
        alignStackView(stackView, toImageView: imageView1, inContentView: contentView)
        alignStackView(stackView, toImageView: imageView2, inContentView: contentView)
        alignStackView(stackView, toImageView: imageView3, inContentView: contentView)
        
        let headingLabelConstraint = NSLayoutConstraint(item: headingLabel, attribute: .width, relatedBy: .lessThanOrEqual, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: screenWidth * 0.8)
        let subtitleLabelConstraint = NSLayoutConstraint(item: subtitleLabel, attribute: .width, relatedBy: .lessThanOrEqual, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: screenWidth * 0.8)
        headingLabel.addConstraint(headingLabelConstraint)
        subtitleLabel.addConstraint(subtitleLabelConstraint)
    }
    
    func alignStackView(_ stackView: UIStackView, toImageView: UIImageView, inContentView: UIView) {
        let topSpace: CGFloat = 60
        let stackViewConstraint = NSLayoutConstraint(item: stackView, attribute: .top, relatedBy: .equal, toItem: toImageView, attribute: .bottom, multiplier: 1, constant: topSpace)
        inContentView.addConstraint(stackViewConstraint)
    }
    
    // MARK: Images
    func addImageViewsToContentView(contentView: UIView) {
        let imageView0 = newImageViewWithImage(image: UKEFirstTimeOnboardingViewController.image0())
        imageView0.alpha = 1
        contentView.addSubview(imageView0)
        keepView(imageView0, onPage: 0)
        self.imageView0 = imageView0
        
        let imageView1 = newImageViewWithImage(image: UKEFirstTimeOnboardingViewController.image1())
        imageView1.alpha = 1
        contentView.addSubview(imageView1)
        keepView(imageView1, onPage: 1)
        self.imageView1 = imageView1
        
        let imageView2 = newImageViewWithImage(image: UKEFirstTimeOnboardingViewController.image2())
        imageView2.alpha = 1
        contentView.addSubview(imageView2)
        keepView(imageView2, onPage: 2)
        self.imageView2 = imageView2
        
        let imageView3 = newImageViewWithImage(image: UKEFirstTimeOnboardingViewController.image3(), contentMode: .left)
        imageView3.alpha = 1
        contentView.addSubview(imageView3)
        keepView(imageView3, onPage: 3)
        self.imageView3 = imageView3
        
        let contentViewTopConstraint = NSLayoutConstraint(item: self.imageView0, attribute: .top, relatedBy: .equal, toItem: contentView, attribute: .top, multiplier: 1, constant: 90)
        contentView.addConstraint(contentViewTopConstraint)
    }
    
    func newImageViewWithImage(image: UIImage, contentMode: UIViewContentMode = .center) -> UIImageView {
        let screenWidth = UIScreen.main.bounds.size.width
        
        let imageView = UIImageView()
        imageView.clipsToBounds = true
        imageView.image = image
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = contentMode
        
        let imageViewWidthConstraint = NSLayoutConstraint(item: imageView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: screenWidth)
        imageView.addConstraint(imageViewWidthConstraint)
        
        return imageView
    }
    
    // MARK: Button
    func addStartButtonToContentView(contentView: UIView) {
        configureStartButton()
        contentView.addSubview(startButton)
        keepView(startButton, onPage: 3)
        
        let buttonHeightConstraint = NSLayoutConstraint(item: startButton, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 44)
        let buttonWidthConstraint = NSLayoutConstraint(item: startButton, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 188)
        startButton.addConstraints([buttonHeightConstraint, buttonWidthConstraint])
        
        let buttonBottomContraint = NSLayoutConstraint(item: startButton, attribute: .bottom, relatedBy: .equal, toItem: contentView, attribute: .bottomMargin, multiplier: 1, constant: -44)
        contentView.addConstraint(buttonBottomContraint)
    }
    
    func configureStartButton() {
        startButton.tintColor = UIColor.crimsonRed()
        startButton.highlightedTextColor = UIColor.flatWhite()
        startButton.addTarget(self, action: #selector(proceedToHomeScreen), for: .touchUpInside)
    }
    
    func proceedToHomeScreen() {
        VIAApplicableFeatures.default.navigateToHomeScreen()
    }
    
    // MARK: UIScrollView
    override public func scrollViewDidScroll(_ scrollView: UIScrollView) {
        super.scrollViewDidScroll(scrollView)
        
        updatePageControlForCurrentPage()
    }
    
    // MARK: Controls
    func updatePageControlForCurrentPage() {
        pageControl.currentPage = Int(roundf(Float(pageOffset)))
        pageControl.pageIndicatorTintColor = UIColor.lightGrey()
        pageControl.currentPageIndicatorTintColor = UIColor.crimsonRed()
    }
    
    // MARK: Helpers
    func newLabelWithText(text: String) -> UILabel {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .center
        label.numberOfLines = 0
        label.text = text
        
        return label
    }
}
