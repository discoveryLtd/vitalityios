//
//  UKEFirstTimeOnboardingContent.swift
//  VIACore
//
//  Created by Val Tomol on 16/07/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import UIKit

import VIAUIKit
import VitalityKit

public extension UKEFirstTimeOnboardingViewController {
    // MARK: Strings
    class func buttonTitle() -> String {
        // TODO: Phrase app token is still in discussion.
        return "Got It"
    }

    class func heading0() -> String {
        return CommonStrings.NewOnboardingHealthReviewTitle2112
    }
    
    class func heading1() -> String {
        return CommonStrings.NewOnboardingTrackActivityTitle2114
    }
    
    class func heading2() -> String {
        return CommonStrings.NewOnboardingEnjoyRewardsTitle2116
    }
    
    class func heading3() -> String {
        return CommonStrings.NewOnboardingExploreTitle2118
    }
    
    class func subtitle0() -> String {
        return CommonStrings.NewOnboardingHealthReviewDescription2113
    }
    
    class func subtitle1() -> String {
        return CommonStrings.NewOnboardingTrackActivityDescription2115
    }
    
    class func subtitle2() -> String {
        return CommonStrings.NewOnboardingEnjoyRewardsDescription2117
    }
    
    class func subtitle3() -> String {
        return CommonStrings.NewOnboardingExploreDescription2119
    }
    
    // MARK: Images
    class func image0() -> UIImage {
        return UKEssentialsAsset.FirstTimeOnboardingUKE.onboardingHeathReview.image
    }
    
    class func image1() -> UIImage {
        return UKEssentialsAsset.FirstTimeOnboardingUKE.onboardingTrackActivity.image
    }
    
    class func image2() -> UIImage {
        return UKEssentialsAsset.FirstTimeOnboardingUKE.onboardingRewards.image
    }
    
    class func image3() -> UIImage {
        return UKEssentialsAsset.FirstTimeOnboardingUKE.onboardingExplore.image
    }
}
