//
//  AppDelegate+ApplicableFeatures+VHR.swift
//  VitalityActive
//
//  Created by Dexter Anthony Ambrad on 4/3/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import VIACommon
import VitalityKit
import VIAUtilities

extension AppDelegate{
    /**
     * Usage:
     * AssessmentNumberRangeQuestionSectionController.swift
     * VHCBMICaptureResultsCollectionViewCell+TextField.swift
     * VHCBloodPressureCaptureResultsCollectionViewCell.swift
     * VHCSingleCaptureResultsCollectionViewCell.swift
     **/
    public func shouldChangeCharactersInAssessmentNumberRangeQuestionSection(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String, selectedUnit: Unit, questionTypeKey: Int, healthAttributeType: PartyAttributeTypeRef) -> Bool {
        if shouldNotAcceptDecimalInputInAssessmentNumberRangeQuestionSection(selectedUnit, questionTypeKey: questionTypeKey, healthAttributeType: .Unknown) {
            return isInputAWholeNumber(string)
        } else {
            return isInputHasCorrectDecimalPlaces(textField.text ?? "", range: range, replacementString: string)
        }
    }
    
    // This will validate on which UoM should have decimal limits. Else, value must be whole number.
    private func shouldNotAcceptDecimalInputInAssessmentNumberRangeQuestionSection(_ selectedUnit: Unit, questionTypeKey: Int, healthAttributeType: PartyAttributeTypeRef) -> Bool {
        return selectedUnit == UnitOfMeasureRef.PerDay.unit() ||
            selectedUnit == UnitOfMeasureRef.FootInch.unit() ||
            selectedUnit == UnitOfMeasureRef.StonePound.unit() ||
            selectedUnit == UnitOfMeasureRef.MillimeterOfMercury.unit() ||
            questionTypeKey == 69 || healthAttributeType == .WaistCircumference ||
            questionTypeKey == 72 || healthAttributeType == .BloodPressureSystol ||
            questionTypeKey == 73 || healthAttributeType == .BloodPressureDiasto
    }
    
    private func isInputAWholeNumber(_ string: String) -> Bool {
        let validInputs = NSCharacterSet(charactersIn:"0123456789").inverted
        let componentsToSeparate = string.components(separatedBy: validInputs)
        let numberFiltered = componentsToSeparate.joined(separator: "")
        return string == numberFiltered
    }
    
    // This will validate the decimal places for other UoMs. In this case, it should be 2 decimal places.
    private func isInputHasCorrectDecimalPlaces(_ text: String, range: NSRange, replacementString string: String) -> Bool {
        return true
    }
    
    public func getVHRSystemFormattedNumber(from localizedString: String?, selectedUnitOfMeasureType: UnitOfMeasureRef) -> String? {
        
        guard let validString = localizedString else { return nil }
        guard let number = NumberFormatter.decimalFormatter().number(from: validString) else { return nil }
        
        return NumberFormatter.serviceFormatter().string(from: number)
    }
    
    /**
     * Usage:
     * AssessmentNumberRangeQuestionSectionController.swift
     **/
    public func shouldAcceptDecimalInput(_ selectedUnit: Unit, questionTypeKey: Int, healthAttributeType: PartyAttributeTypeRef) -> Bool {
        if  selectedUnit == UnitOfMeasureRef.PerDay.unit()   ||
            selectedUnit == UnitOfMeasureRef.FootInch.unit() ||
            selectedUnit == UnitOfMeasureRef.StonePound.unit() ||
            questionTypeKey == 69 || healthAttributeType == .WaistCircumference ||
            questionTypeKey == 72 || healthAttributeType == .BloodPressureSystol ||
            questionTypeKey == 73 || healthAttributeType == .BloodPressureDiasto {
            return false
        }
        return true
    }
    
    public func shouldExecuteGroupValidation() -> Bool {
        return false
    }
    
    /**
     * Usage:
     * AssessmentNumberRangeQuestionSectionController.swift
     * VNAAssessmentNumberRangeQuestionSectionController.swift
     **/
    public var removeAssessmentGuidanceText: Bool?{
        get{
            return false
        }
    }
    
    /**
     * Usage:
     * Assessments -- VHR, VNA, MWB
     **/
    public var showRequiredPrompt: Bool?{
        get{
            return false
        }
    }
    
    /**
     * Usage:
     * Assessments -- VHR AssessmentQuestionnaireSectionViewModel
     **/
    public func isValid(questionnaireSection: VHRQuestionnaireSection) -> Bool {
        
        var capturedResults = [VHRCapturedResult]()
        var questionsWithoutCapturedResults = [VHRQuestion]()
        
        // exclude Label questions from validation
        let questionsToBeValidated = questionnaireSection.questions.filter({ $0.questionType != .Label })
        
        for question in questionsToBeValidated {
            if let results = question.capturedResults(), results.count > 0, question.isVisible {
                    capturedResults.append(contentsOf: results)
            } else if question.isVisible {
                questionsWithoutCapturedResults.append(question)
            }
        }
        
        // validate
        let invalidCapturedResults = capturedResults.filter({ $0.valid == false })
        
        return invalidCapturedResults.count == 0 && questionsWithoutCapturedResults.count == 0
    }
    
    public var vhrDisableKeyboardDecimalInput: Bool? {
        get {
            return false
        }
    }
    
    public func vhrRemoveUnknownUOM() -> Bool {
        return false
    }
    
    public func vhrDisplayUoMAsSymbol() -> Bool {
        return false
    }
    
    public func decimalFormatter(shouldAcceptDecimalInput: Bool) -> NumberFormatter? {
        let formatter = Localization.decimalFormatter
        
        return formatter
    }
    
    public func showVHRDisclaimer() -> Bool {
        return false
    }
}
