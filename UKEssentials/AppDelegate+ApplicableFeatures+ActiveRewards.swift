//
//  AppDelegate+ApplicableFeatures+ActiveRewards.swift
//  VitalityActive
//
//  Created by OJ Garde on 3/17/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import VIAUtilities
import VitalityKit
import VIACommon
import VIAActiveRewards

extension AppDelegate{
    
    /**
     * Usage: VIAARFlowViewModel.swift
     */
    public func shouldIncludeUsedAndExpiredSpinInHistory() -> Bool {
        return true
    }
    
    /* Usage: VIAARLearnMoreViewController */
    public var hideARLearnMoreBenefitGuide: Bool? {
        get{
            return true
        }
    }
    
    public var showSwapRewardsOption: Bool? {
        get{
            return false
        }
    }
    
    public func shouldFilterPartnerGroups() -> Bool {
        return false
    }
    
    public var partnerDetailViewControllerActionImage: UIImage? {
        get{
            return VIACommonAsset.partnersGetStarted.templateImage
        }
    }
    
    public var redirectToWebVoucher: Bool? {
        get{
            return false
        }
    }
    
    public func getPartnerDetailViewControllerActionTitle(with partnerTypeKey: Int? = nil) -> String {
        /*
         * Unwrap the typeKey with default value to Unknown -1.
         * If typeKey is nil or not defined, it will fallback to the generic button title of the market.
         */
        switch PartnerTypeKeyRef(rawValue: partnerTypeKey ?? -1) {
        case .HotelsDotCom?:
            return CommonStrings.BookNowButtonTitle2188
        default:
            /* NOTE: Title may vary on every market */
            return CommonStrings.GetStartedButtonTitle103
        }
    }
    public func getPartnerRewardInstructions(with voucherString: String) -> String {
        return CommonStrings.Ar.Rewards.StarbucksIssuedRewardReadyForCollectionHtml1085(voucherString)
    }
    
    public var convertJoulesToCalories: Bool? {
        get{
            return false
        }
    }
    
    public var filterRewardTypeCode: Bool? {
        get {
            return false
        }
    }
    
    public var filterRewardByStatusOnly: Bool {
        get{
            return true
        }
    }
    
    public func removeAppleWatchOnPartnersData() -> Bool {
        return false
    }
    
    public func navigateToBenefitGuide(segue: UIStoryboardSegue){
        if let webViewController = segue.destination as? VIAWebContentViewController {
            webViewController.showNavBar = true
            webViewController.title = CommonStrings.Ar.LearnMore.BenefitGuideTitle728
            let pdfFilename = AppConfigFeature.getFileName(type: .ActiveRewardsBenefit)
            let webContenViewModel = VIAWebContentViewModel(pdfFilename: pdfFilename)
            webViewController.viewModel = webContenViewModel
        }
    }
    
    public func hideRewardsSubtitle() -> Bool{
        return false
    }
    
    public func enabledEmptyStatusView() -> Bool {
        return false
    }
    
    public func hideVitalityCoinsFromRewards() -> Bool {
        return false
    }
    
    public func getActivityHistoryViewModel() -> AnyObject? {
        return VIAARActivityHistoryViewModel()
    }
    
    public func getAvailableRewardViewModel() -> AnyObject? {
        return VIAARAvailableRewardViewModel()
    }
}
