//
//  AppDelegate+ApplicableFeatures+Status.swift
//  UKEssentials
//
//  Created by Von Kervin R. Tuico on 29/07/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import VIACommon
import VIAStatus

extension AppDelegate{
    
    public func getStatusPointsEarningActivitiesViewCell(tableView: UITableView, indexPath: IndexPath,
                                                         pointsActivityName: String?, potentialPointsString: String?,
                                                         eventKey: Int, activityCode: String?) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: TitleSubtitleTableViewCell.defaultReuseIdentifier, for: indexPath)
        guard let viaCell = cell as? TitleSubtitleTableViewCell,
            let pointsActivityName = pointsActivityName else {
                return tableView.defaultTableViewCell()
        }
        
        viaCell.title.text = pointsActivityName
        if let potentialPointsString = potentialPointsString{
            viaCell.subtitle.text = potentialPointsString
        }
        viaCell.accessoryType = .disclosureIndicator
        
        return viaCell
    }
    
    public func getAnnualStatusViewModel() -> AnyObject?{
        return VAAnnualStatusViewModel()
    }
    
    public func getPointsEarningActivitiesViewModel() -> AnyObject?{
        return VAPointsEarningActivitiesViewModel()
    }
}
