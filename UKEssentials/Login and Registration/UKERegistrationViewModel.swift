import Foundation
import VitalityKit

public class UKERegistrationViewModel: AnyObject {

    public var registrationDetail: UKERegistrationDetail

    init() {
        self.registrationDetail = UKERegistrationDetail()
    }
}

// Mark - UKERegistrationDetail

public struct UKERegistrationDetail {

    public var isAuthenticationCodeDirty = false
    public var isEntityNumberDirty = false
    public var isDateOfBirthDirty = false

    public var isValid: Bool {
        return self.dateOfBirthValid && self.authenticationCodeValid && self.entityNumberValid
    }


    // MARK: - Date of Birth


    public var dateOfBirth: Date? {
        didSet {
            self.isDateOfBirthDirty = self.dateOfBirth != Date()
        }
    }

    public var dateOfBirthValid: Bool {
        if self.dateOfBirth == nil {
            return false
        }
        return self.dateOfBirth! < Date()
    }


    // MARK: - Authentication Code

    public var authenticationCode: String? {
        didSet {
            self.isAuthenticationCodeDirty = !String.isNilOrEmpty(string: self.authenticationCode)
        }
    }

    public var authenticationCodeValid: Bool {
        guard let authCode = self.authenticationCode else {
            return false
        }
        return authCode.characters.count == 10 && authCode.isNumeric
    }


    // MARK: - Entity Number

    public var entityNumber: String? {
        didSet {
            self.isEntityNumberDirty = !String.isNilOrEmpty(string: self.entityNumber)
        }
    }

    public var entityNumberValid: Bool {
        // This is not a required field

        guard let entity = self.entityNumber else {
            return true
        }

        if entity.isEmpty {
            return true
        }
        
        return entity.characters.count == 10 && entity.isNumeric
    }
}
