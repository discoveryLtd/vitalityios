import UIKit
import VitalityKit
import VIACore
import VIAUIKit
import VitalityKit
import SafariServices
import VIAUtilities
import VIACommon
import VIAHealthKit
import HealthKit
import VIAWellnessDevices

class UKELoginViewController: VIAViewController, SFSafariViewControllerDelegate, UKELoginDelegate, VIAAppleHealthHandler {

    // CONTROLLERS
    let authFlowController: UKEAuthenticationFlowController = UKEAuthenticationFlowController()

    // VIEWS
    var containerView: UIView = UIView()
    var headerImage: UIImageView = UIImageView()
    var headingLabel: UILabel = UILabel()
    var messageLabel: UILabel = UILabel()
    var actionButton: UIButton = UIButton(type: .system)

    // CONSTRAINTS
    var spacingAboveHeaderImage: Double = 100
    var spacingBetweenHeaderImageAndTitle: Double = 35
    var spacingBetweenTitleAndMessage: Double = 15
    var spacingBetweenMessageAndButton: Double = 35

    var buttonXContentPadding: Double = 50
    var buttonHeight: Double = 35

    var iconSize: Double = 130

    var buttonTitle: String? {
        didSet {
            actionButton.setTitle(buttonTitle, for: .normal)
        }
    }

    var viewImage: UIImage? {
        didSet {
            headerImage.image = viewImage
        }
    }

    var viewHeading: String? {
        didSet {
            headingLabel.text = viewHeading
        }
    }

    var viewMessage: String? {
        didSet {
            messageLabel.text = viewMessage
        }
    }

    var buttonFillColor: UIColor? {
        didSet {
            actionButton.backgroundColor = buttonFillColor
        }
    }

    var buttonTitleColor: UIColor? {
        didSet {
            actionButton.tintColor = buttonTitleColor
        }
    }

    var buttonBorderRadius: Double = 5.0 {
        didSet {
            actionButton.layer.cornerRadius = CGFloat(buttonBorderRadius)
        }
    }

    var buttonBorderWidth: Double = 1.0 {
        didSet {
            actionButton.layer.borderWidth = CGFloat(buttonBorderWidth)
        }
    }

    var buttonBorderColor: UIColor? {
        didSet {
            actionButton.layer.borderColor = buttonBorderColor?.cgColor ?? UIColor.clear.cgColor
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        Wire.default.currentEnvironment = .production
        
        if let enabled = Bundle.main.object(forInfoDictionaryKey: "VIAEnableBypass") as? Bool, enabled{
            addGestureRecogniser()
        }
        
        configureAppearance()
        addSubviews()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        #if DEBUG
            self.debugLogin()
        #endif
    }

    func configureAppearance() {
        view.backgroundColor = UIColor.white
    }

    func addSubviews() {
        setupSubviews()
        constrainSubview()
        populateSubview()
    }

    func setupSubviews () {
        containerView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(containerView)

        headerImage.translatesAutoresizingMaskIntoConstraints = false
        containerView.addSubview(headerImage)

        headingLabel.translatesAutoresizingMaskIntoConstraints = false
        headingLabel.font = UIFont.title1Font()
        headingLabel.textAlignment = .center
        headingLabel.numberOfLines = 2
        containerView.addSubview(headingLabel)

        messageLabel.translatesAutoresizingMaskIntoConstraints = false
        messageLabel.font = UIFont.footnoteFont()
        messageLabel.textColor = UIColor.mediumGrey()
        messageLabel.textAlignment = .center
        messageLabel.numberOfLines = 3
        containerView.addSubview(messageLabel)

        actionButton.translatesAutoresizingMaskIntoConstraints = false
        actionButton.addTarget(self, action: #selector(getStartedTapped), for: .touchUpInside)
        actionButton.contentEdgeInsets = UIEdgeInsetsMake (10, CGFloat(buttonXContentPadding), 10, CGFloat(buttonXContentPadding))
        containerView.addSubview(actionButton)
    }

    func constrainSubview () {
        let viewMargins = view.layoutMarginsGuide
        containerView.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: -20).isActive = true
        containerView.leadingAnchor.constraint(equalTo: viewMargins.leadingAnchor).isActive = true
        containerView.trailingAnchor.constraint(equalTo: viewMargins.trailingAnchor).isActive = true

        headerImage.centerXAnchor.constraint(equalTo: containerView.centerXAnchor).isActive = true
        headerImage.topAnchor.constraint(equalTo: containerView.topAnchor).isActive = true
        headerImage.widthAnchor.constraint(equalToConstant: CGFloat(iconSize)).isActive = true
        headerImage.heightAnchor.constraint(equalToConstant: CGFloat(iconSize)).isActive = true

        headingLabel.topAnchor.constraint(equalTo: headerImage.bottomAnchor, constant: CGFloat(spacingBetweenHeaderImageAndTitle)).isActive = true
        headingLabel.leadingAnchor.constraint(equalTo: viewMargins.leadingAnchor, constant: 40.0).isActive = true
        headingLabel.trailingAnchor.constraint(equalTo: viewMargins.trailingAnchor, constant: -40.0).isActive = true

        messageLabel.topAnchor.constraint(equalTo: headingLabel.bottomAnchor, constant: CGFloat(spacingBetweenTitleAndMessage)).isActive = true
        messageLabel.widthAnchor.constraint(equalToConstant: 260).isActive = true
        messageLabel.centerXAnchor.constraint(equalTo: containerView.centerXAnchor).isActive = true

        actionButton.centerXAnchor.constraint(equalTo: containerView.centerXAnchor).isActive = true
        actionButton.topAnchor.constraint(equalTo: messageLabel.bottomAnchor, constant: CGFloat(spacingBetweenMessageAndButton)).isActive = true
        actionButton.heightAnchor.constraint(equalToConstant: CGFloat(buttonHeight)).isActive = true
        actionButton.bottomAnchor.constraint(equalTo: containerView.bottomAnchor).isActive = true
    }

    func populateSubview () {
        self.viewImage = UIImage(asset: .loginLogoEn)
        self.viewHeading = CommonStrings.WelcomeTextTitle366
        self.viewMessage = CommonStrings.Welcome.MessageText367
        self.buttonTitle = CommonStrings.GetStartedButtonTitle103
        self.buttonFillColor = UIColor.primaryColor()
        self.buttonTitleColor = UIColor.white
        self.buttonBorderColor = nil
        self.buttonBorderWidth = 0
        self.buttonBorderRadius = 6.0
    }

    func addGestureRecogniser() {
        let hold = UILongPressGestureRecognizer(target: self, action: #selector(environmentSelectionGestureRecognized(_:)))
        hold.numberOfTapsRequired = 2
        hold.numberOfTouchesRequired = 2
        hold.minimumPressDuration = 3
        view.addGestureRecognizer(hold)
    }

    func environmentSelectionGestureRecognized(_ sender: UILongPressGestureRecognizer) {
        guard sender.state == .began else { return }
        let actionSheet = UIAlertController(title: "Select Environment", message: "Please select the environment that you would like to access", preferredStyle: .actionSheet)

        let test = UIAlertAction(title: Wire.Environment.test.name(), style: .default) { (action) in
            Wire.default.currentEnvironment = .test2
        }
        actionSheet.addAction(test)

        let qa = UIAlertAction(title: Wire.Environment.qa.name(), style: .default) { (action) in
            Wire.default.currentEnvironment = .qa
        }
        
        let qaff = UIAlertAction(title: Wire.Environment.qaFrankfurt.name(), style: .default) { (action) in
            Wire.default.currentEnvironment = .qaFrankfurt
        }
        actionSheet.addAction(qaff)

        let production = UIAlertAction(title: Wire.Environment.production.name(), style: .default) { (action) in
            Wire.default.currentEnvironment = .production
        }
        actionSheet.addAction(production)
        
        let bypass = UIAlertAction(title: "BYPASS UKE", style: .default) { [unowned self] (action) in
            self.debugLogin()
        }
        actionSheet.addAction(bypass)

        let cancel = UIAlertAction(title: CommonStrings.CancelButtonTitle24, style: .default, handler: nil)
        actionSheet.addAction(cancel)
        
        if let popoverController = actionSheet.popoverPresentationController {
            popoverController.sourceView = view
            popoverController.sourceRect = CGRect(x: view.bounds.midX, y: view.bounds.midY, width: 0, height: 0)
            popoverController.permittedArrowDirections = []
        }

        present(actionSheet, animated: true, completion: nil)
    }

    @IBAction func getStartedTapped(_ button: UIButton) {
        #if WIREMOCK
            debugLogin()
        #else
            authFlowController.startWebAuthentication(withDelegate: self)
        #endif
    }
    
    func debugLogin() {
        let loginAlert = UIAlertController(title: "Test Vitality Login", message: "Complete Vitality Login Details", preferredStyle: .alert)
        loginAlert.addTextField(configurationHandler: { field in
            field.placeholder = "Email"
            field.autocapitalizationType = .none
            field.autocorrectionType = .no
        })
        loginAlert.addTextField(configurationHandler: { field in
            field.placeholder = "Password"
            field.isSecureTextEntry = true
            field.autocapitalizationType = .none
            field.autocorrectionType = .no
        })
        let okAction = UIAlertAction(title: "OK", style: .default, handler: { [unowned self] action in
            guard
                let email = loginAlert.textFields?.first?.text,
                let password = loginAlert.textFields?.last?.text,
                !email.isEmpty,
                !password.isEmpty
                else { return }

            self.showHUDOnWindow()
            self.authFlowController.loginDelegate = self
            self.authFlowController.startVALogin(withEmail: email, password: password)
        })
        loginAlert.addAction(okAction)
        
        #if DEBUG
            loginAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        #endif
        
        present(loginAlert, animated: true, completion: nil)
    }

    // UKELoginDelegate

    func presentWebView(_ view: UIViewController) {
        present(view, animated: true, completion: nil)
    }

    func returnToWelcomeScreenBecauseOf(error: Error) {
        dismiss(animated: true, completion: nil)
        showUnknownError()
    }
    
    func loginFailedBecauseOf(error: Error) {
        hideHUDFromWindow()
        if let error = error as? BackendError {
            handleBackendErrorWithAlert(error)
        } else if let error = error as? LoginError {
            var dialogTitle = ""
            var dialogMessage = ""
            
            if error == .accountLockedOut {
                dialogTitle = CommonStrings.Login.AccountLockedAlertTitle737
                dialogMessage = CommonStrings.Login.AccountLockedErrorAlertMessage738
            } else {
                dialogTitle = CommonStrings.Login.IncorrectEmailPasswordErrorTitle47
                dialogMessage = CommonStrings.Login.IncorrectEmailPasswordErrorMessage48
            }
            
            let controller = UIAlertController(title: dialogTitle, message: dialogMessage, preferredStyle: .alert)
            
        
            let ok = UIAlertAction(title: CommonStrings.OkButtonTitle40, style: .cancel, handler: nil)
            controller.addAction(ok)
            
            self.present(controller, animated: true, completion: nil)
        } else {
            showUnknownError()
        }
    }
    
    func showUnknownError() {
        let alert = UIAlertController(title: CommonStrings.GenericUnkownErrorTitle266, message: CommonStrings.GenericUnkownErrorMessage267, preferredStyle: .alert)
        let okAction = UIAlertAction(title: CommonStrings.OkButtonTitle40, style: .default, handler: nil)
        alert.addAction(okAction)
        present(alert, animated: true, completion: nil)
    }

    func webAuthenticationSucceeded() {
        dismiss(animated: true, completion: nil)
        showHUDOnWindow()
    }

    func presentNextStep() {
        
        hideHUDFromWindow()
        AppSettings.updateLastLoginDate()
        
        // for Apple Health device auto-sync
        configureAutoSync()
        
        VIASplashScreenViewController.showInsurerSplashScreen(skipAppConfigRequest: true)

    }

    func presentUKERegistrationScreen() {
        hideHUDFromWindow()
        dismiss(animated: true, completion: nil)
        performSegue(withIdentifier: "showAuthenticationScreen", sender: self)
    }

    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showAuthenticationScreen" {
            let authNavController = segue.destination as! UINavigationController
            guard let authVC = authNavController.topViewController as? UKERegistrationViewController else { return }
            authVC.authFlowController = authFlowController
        }
    }

    @IBAction func dismissRegistrationView(withSegue segue: UIStoryboardSegue) {
        presentedViewController?.dismiss(animated: true, completion: nil)
    }
}
