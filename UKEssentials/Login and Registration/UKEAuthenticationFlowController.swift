import UIKit
import VitalityKit
import VIAUtilities
import SafariServices

let tokenKey = "id_token"

protocol UKELoginDelegate: class {
    // FLOW ELEMENT: Web Auth Screen
    func presentWebView(_ view: UIViewController)

    // FLOW ELEMENT: Web Auth Screen > Successful? > N
    func returnToWelcomeScreenBecauseOf(error: Error)
    func loginFailedBecauseOf(error: Error)

    // FLOW ELEMENT: Web Auth Screen > Successful? > Y
    func webAuthenticationSucceeded()

    // FLOW ELEMENT: Web Auth Screen > Successful? > Y > UKE Registration/Login Service > Successful? > Y
    // FLOW ELEMENT: UKE Registration Screen > UKE Registration/Login Service > Successful? > Y
    func presentNextStep()

    // FLOW ELEMENT: Web Auth Screen > Successful? > Y > UKE Registration/Login Service > Successful? > N
    func presentUKERegistrationScreen()
}

protocol UKERegistrationDelegate: class {
    // FLOW ELEMENT: UKE Registration Screen > UKE Registration/Login Service > Successful? > N
    func authorisationCodeValidationFailed()
    func dateOfBirthValidationFailed()

    // FLOW ELEMENT: UKE Registration Screen > UKE Registration/Login Service > Entity # Provided? Y > Valid? > N
    func entityNumberValidationFailed()
    
    func unknownErrorOccured()
}

class UKEAuthenticationFlowController: NSObject, SFSafariViewControllerDelegate {
    weak var loginDelegate: UKELoginDelegate?
    weak var registrationDelegate: UKERegistrationDelegate?
    var webAuthToken: String?
    var awaitingCallback: Bool?

    // MARK: - Get Started -> Web Authentication

    // FLOW ELEMENT: Web Auth Screen
    func startWebAuthentication(withDelegate delegate: UKELoginDelegate?) {
        loginDelegate = delegate
        awaitingCallback = true
        
        guard let url = webAuthenticationURL(for: Wire.default.currentEnvironment) else { return }
        let webAuthViewController = SFSafariViewController(url: url)
        webAuthViewController.delegate = self
        NotificationCenter.default.addObserver(forName: .VIAApplicationDidOpenURL, object: nil, queue: .main, using: webAuthenticationCompleted(withNotification:))

        loginDelegate?.presentWebView(webAuthViewController)
    }

    func webAuthenticationURL(for environment: Wire.Environment) -> URL? {
        let urlString = webAuthenticationURLString(for: environment)
        return URL(string: urlString)
    }

    func webAuthenticationURLString(for environment: Wire.Environment) -> String {
        // TODO: Update when final URLs are provided
        switch environment {
            //        case .develop:
            //            return "https://nuf-b2c-dev.azurewebsites.net/"
        case .test2:
//            return "https://account.nuffieldhealth.com/nuffielddev3.onmicrosoft.com/oauth2/v2.0/authorize?client_id=3dc572c8-ebaa-4933-893e-b456fab97a55&response_type=id_token&redirect_uri=com.vitalityactive.essentials://oauth/redirect&response_mode=query&scope=openid&p=b2c_1a_nuffv2_signinorsignuploa1"
            
            return "https://account.nuffieldhealth.com/nufuat.onmicrosoft.com/oauth2/v2.0/authorize?p=B2C_1A_NUFFV2_SignInOrSignUpLoA1&client_id=7d6c0ce1-f0b4-4ed8-b572-ac26e6e77e1f&redirect_uri=com.vitalityactive.essentials%3A%2F%2Foauth%2Fredirect&scope=openid&response_type=id_token&response_mode=query&branding=healthyu"
        case .qa:
//            return "https://account.nuffieldhealth.com/nufuat.onmicrosoft.com/oauth2/v2.0/authorize?client_id=7d6c0ce1-f0b4-4ed8-b572-ac26e6e77e1f&response_type=id_token&redirect_uri=com.vitalityactive.essentials://oauth/redirect&response_mode=query&scope=openid&p=b2c_1a_nuffv2_signinorsignuploa1"
            
            return "https://account.nuffieldhealth.com/nufuat.onmicrosoft.com/oauth2/v2.0/authorize?p=B2C_1A_NUFFV2_SignInOrSignUpLoA1&client_id=7d6c0ce1-f0b4-4ed8-b572-ac26e6e77e1f&redirect_uri=com.vitalityactive.essentials%3A%2F%2Foauth%2Fredirect&scope=openid&response_type=id_token&response_mode=query&branding=healthyu"
        case .qaFrankfurt:
//            return "https://account.nuffieldhealth.com/nufuat.onmicrosoft.com/oauth2/v2.0/authorize?client_id=7d6c0ce1-f0b4-4ed8-b572-ac26e6e77e1f&response_type=id_token&redirect_uri=com.vitalityactive.essentials://oauth/redirect&response_mode=query&scope=openid&p=b2c_1a_nuffv2_signinorsignuploa1"
            
            return "https://account.nuffieldhealth.com/nufuat.onmicrosoft.com/oauth2/v2.0/authorize?p=B2C_1A_NUFFV2_SignInOrSignUpLoA1&client_id=7d6c0ce1-f0b4-4ed8-b572-ac26e6e77e1f&redirect_uri=com.vitalityactive.essentials%3A%2F%2Foauth%2Fredirect&scope=openid&response_type=id_token&response_mode=query&branding=healthyu"
            //        case .qaCentralAsset:
            //            return "https://nuf-b2c-dev.azurewebsites.net/"
            //        case .eagle:
        //            return "https://nuf-b2c-dev.azurewebsites.net/"
        case .production:
            return "https://account.nuffieldhealth.com/mynuffield.onmicrosoft.com/oauth2/v2.0/authorize?client_id=7626533b-90fc-4f08-8b85-dc4a5df57ceb&response_type=id_token&redirect_uri=com.vitalityactive.essentials://oauth/redirect&response_mode=query&scope=openid&p=b2c_1a_nuffv2_signinorsignuploa1"
            
//            return "https://account.nuffieldhealth.com/mynuffield.onmicrosoft.com/oauth2/v2.0/authorize?p=B2C_1A_NUFFV2_SignInOrSignUpLoA1&client_id=7626533b-90fc-4f08-8b85-dc4a5df57ceb&redirect_uri=com.vitalityactive.essentials%3A%2F%2Foauth%2Fredirect&scope=openid&response_type=id_token&response_mode=query&branding=healthyu"
        default:
            let ukeClientKey = "3dc572c8-ebaa-4933-893e-b456fab97a55"
            
            return "https://account.nuffieldhealth.com/nuffielddev3.onmicrosoft.com/oauth2/v2.0/authorize?client_id=\(ukeClientKey)&response_type=id_token&redirect_uri=com.vitalityactive.essentials://oauth/redirect&response_mode=query&scope=openid&p=b2c_1a_nuffv2_signinorsignuploa1.1"
        }
    }

    // FLOW ELEMENT: Web Auth Screen > Successful?
    func webAuthenticationCompleted(withNotification notification: Notification) {
        guard let awaitingCallback = awaitingCallback, awaitingCallback else { return }
        NotificationCenter.default.removeObserver(self, name: .VIAApplicationDidOpenURL, object: nil)
        self.awaitingCallback = false
        guard let info = notification.object as? NotificationInfo,
            let urlComponents = info.urlComponents,
            let token = urlComponents.queryItems?.first(where: {$0.name == tokenKey})?.value
            else {
                // Successful? -> No
                webAuthenticationFailed()
                return
        }
        
        // Successful? -> Yes
        webAuthenticationSucceeded(with: token)

    }

    // FLOW ELEMENT: Web Auth Screen > Successful? > N
    func webAuthenticationFailed() {
        // This should display some kind of error message
        // It's incredibly unlikely that we'd actually encounter this:
        // A URL callback after a failed login? The web page would likely deal with this
        loginDelegate?.returnToWelcomeScreenBecauseOf(error: UKEAuthenticationError.webAuthFailed)
    }

    // FLOW ELEMENT: Web Auth Screen > Successful? > Y
    func webAuthenticationSucceeded(with token: String) {
        loginDelegate?.webAuthenticationSucceeded()
        startUKELogin(with: token)
    }
    
    // FLOW ELEMENT: Web Auth Screen > Dismissed
    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        awaitingCallback = false
    }

    // MARK: - Web Authentication Successful -> UKE Login

    // FLOW ELEMENT: Web Auth Screen > Successful? > Y > UKE Login Service
    func startUKELogin(with token: String) {
        webAuthToken = token
        // Service Call -> UKE Login
        Wire.Member.ukeLogin(token: token) { [weak self] (username, password, error) in
            
            // FAILURE - Handle error
            if let error = error {
                if let error = error as? UKEAuthenticationError {
                    switch error {
                    case .registrationRequired:
                        self?.ukeRegistrationFailed()
                    default:
                        self?.loginDelegate?.loginFailedBecauseOf(error: error)
                        break
                    }
                } else {
                    self?.loginDelegate?.loginFailedBecauseOf(error: error)
                }
                
                return
            }
            
            // SUCCESS - Login
            guard let username = username else { return }
            guard let password = password else { return }
            self?.ukeRegistrationSucceeded(withEmail: username, password: password)
        }

    }

    // FLOW ELEMENT: Web Auth Screen > Successful? > Y > UKE Login Service > Successful? > Y
    // FLOW ELEMENT: UKE Registration Screen > UKE Registration Service > Successful? > Y
    func ukeRegistrationSucceeded(withEmail email: String, password: String) {
        let prefixedEmail = ensureEnvironmentPrefixIsPresent(for: email)
        startVALogin(withEmail: prefixedEmail, password: password)
    }

    func ensureEnvironmentPrefixIsPresent(for email: String) -> String {
        let prefix = Wire.default.currentEnvironment.prefix()
        guard !(email.contains(prefix)) else { return email }
        return prefix + email
    }

    // FLOW ELEMENT: Web Auth Screen > Successful? > Y > UKE Login Service > Successful? > N
    // FLOW ELEMENT: UKE Registration Screen > UKE Registration Service > Successful? > N
    func ukeRegistrationFailed() {
        // present Authentication Code & DOB screen
        loginDelegate?.presentUKERegistrationScreen()
    }

    // FLOW ELEMENT: Web Auth Screen > Successful? > Y > UKE Login Service > Successful? > N > UKE Registration Screen > UKE Registration Service
    func validateUKERegistrationInfo(withAuthenticationCode authenticationCode: String, dateOfBirth: Date, entityNumber: String?, delegate authDelegate: UKERegistrationDelegate?) {
        registrationDelegate = authDelegate
        
        guard let token = webAuthToken else { return }

        // Service Call -> UKE Registration
        Wire.Member.ukeRegister(token: token, authorizationCode: authenticationCode, dateOfBirth: dateOfBirth, entityNumber: entityNumber) { [weak self] (username, password, error) in
            if let error = error {
                // FAILURE
                if let error = error as? UKEAuthenticationError {
                    switch error {
                    case .invalidDateOfBirthV1, .invalidDateOfBirthVHS:
                        self?.registrationDelegate?.dateOfBirthValidationFailed()
                    case .invalidEntityNumber:
                        self?.registrationDelegate?.entityNumberValidationFailed()
                    case .invalidAuthorizationCode, .unableToDecryptAuthorizationCode:
                        self?.registrationDelegate?.authorisationCodeValidationFailed()
                    default:
                        self?.registrationDelegate?.unknownErrorOccured()
                    }
                    return
                }
                
                self?.registrationDelegate?.unknownErrorOccured()
                return
            }

            // SUCCESS
            guard let username = username else { return }
            guard let password = password else { return }
            self?.ukeRegistrationSucceeded(withEmail: username, password: password)
        }
    }

    // MARK: - UKE Registration/Login Successful -> VA Login

    // FLOW ELEMENT: Web Auth Screen > Successful? > Y > UKE Login Service > Successful? > Y > VA Login
    // FLOW ELEMENT: UKE Registration Screen > UKE Registration Service > Successful? > Y > VA Login
    func startVALogin(withEmail email: String, password: String) {
        Wire.Member.login(email: email, password: password, completion: { [weak self] (error, upgradeUrl) in
            
            /*
             * If Appstore URL is valid and not empty. Notify user to download the latest app.
             */
            if let url = upgradeUrl{
                NotificationCenter.default.post(name: .VIAAppUpdateException, object: url)
            }
            if let error = error {
                self?.vaAuthenticationFailed(error: error)
            } else {
                self?.vaAuthenticationSucceeded()
            }
        })
    }

    // FLOW ELEMENT: VA Login > Successful? > N
    func vaAuthenticationFailed(error: Error) {
        loginDelegate?.loginFailedBecauseOf(error: error)
    }

    // FLOW ELEMENT: VA Login > Successful? > Y
    func vaAuthenticationSucceeded() {
        loginDelegate?.presentNextStep()
    }
}
