import UIKit
import VitalityKit
import VIACore
import VIAUIKit
import VitalityKit

class UKERegistrationViewController: VIATableViewController, UKERegistrationDelegate, DatePickerDelegate {

    enum UKERegistrationSection: Int {
        case dateOfBirth
        case authenticationCode
        case entityNumber

        static var count: Int {
            return self.entityNumber.rawValue + 1
        }
    }

    // CONTROLLERS
    var authFlowController: UKEAuthenticationFlowController?

    var showingDate = false
    var viewModel = UKERegistrationViewModel()
    @IBOutlet weak var cancelButton: UIBarButtonItem!
    @IBOutlet weak var doneButton: UIBarButtonItem!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = CommonStrings.Activate.ActivateTitle353
        setupNavigationbarItemTitles()
        self.configureTableView()
    }

    func setupNavigationbarItemTitles() {
        self.cancelButton.title = CommonStrings.CancelButtonTitle24
        self.doneButton.title = CommonStrings.DoneButtonTitle53
    }

    func configureTableView() {
        self.tableView.register(VIATextFieldTableViewCell.nib(), forCellReuseIdentifier: VIATextFieldTableViewCell.defaultReuseIdentifier)
        self.tableView.register(VIADatePickerTableViewCell.nib(), forCellReuseIdentifier: VIADatePickerTableViewCell.defaultReuseIdentifier)
        self.tableView.register(VIARightDetailTableViewCell.nib(), forCellReuseIdentifier: VIARightDetailTableViewCell.defaultReuseIdentifier)
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        self.tableView.estimatedRowHeight = 75
        self.tableView.separatorInset = UIEdgeInsets(top: 0, left: 52, bottom: 0, right: 0)

        self.tableView.register(VIATableViewSectionHeaderFooterView.nib(), forHeaderFooterViewReuseIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier)
        self.tableView.estimatedSectionFooterHeight = 75
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        self.updateNavigationButtonsEnabled()
    }

    func updateNavigationButtonsEnabled() {
        self.navigationItem.rightBarButtonItem?.isEnabled = self.viewModel.registrationDetail.isValid
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        // make first cell first responder
        let cell = self.tableView.cellForRow(at: IndexPath(row: 0, section: 0))
        if cell != nil { cell!.becomeFirstResponder() }
    }


    // MARK: - UITableView DataSource

    override func numberOfSections(in tableView: UITableView) -> Int {
        return UKERegistrationSection.count
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == UKERegistrationSection.dateOfBirth.rawValue && self.showingDate {
            return 2
        }
        return 1
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        guard let cell = tableView.dequeueReusableCell(withIdentifier: VIATextFieldTableViewCell.defaultReuseIdentifier, for: indexPath) as? VIATextFieldTableViewCell else {
            return UITableViewCell(style: .default, reuseIdentifier: "cell")
        }
        guard let section = UKERegistrationSection(rawValue: indexPath.section) else { return UITableViewCell(style: .default, reuseIdentifier: "cell") }

        switch section {
        case .dateOfBirth:
            if self.showingDate && indexPath.row == 1 {
                let dobCell = tableView.dequeueReusableCell(withIdentifier: VIADatePickerTableViewCell.defaultReuseIdentifier) as! VIADatePickerTableViewCell
                dobCell.maximumDate = Date()
                if let selectedDate = viewModel.registrationDetail.dateOfBirth {
                    dobCell.selectedDate = selectedDate
                }else{
                    let formatter = DateFormatter()
                    formatter.dateFormat = "yyyy/MM/dd"
                    if let selectedDate = formatter.date(from: "1980/01/01") {
                        dobCell.selectedDate = selectedDate
                    }
                }
                dobCell.delegate = self
                return dobCell
            }
            let dobTitleCell = tableView.dequeueReusableCell(withIdentifier: VIARightDetailTableViewCell.defaultReuseIdentifier) as! VIARightDetailTableViewCell
            dobTitleCell.setTitle(CommonStrings.Activate.DateOfBirth363)
            dobTitleCell.setPlaceholder(CommonStrings.Activate.EnterDate354)
            return dobTitleCell
        case .authenticationCode:
            cell.setHeadingLabelText(text: CommonStrings.Activate.AuthenticationCode355)
            cell.setTextFieldPlaceholder(placeholder: CommonStrings.Activate.AuthenticationCodePlaceholder356)
            cell.cellImageConfig = self.registrationImage(asset: UKEssentialsAsset.LoginRegistration.registrationActivationCode.templateImage)
            cell.setTextFieldSecureTextEntry(secureTextEntry: false)
            cell.setTextFieldKeyboardType(type: .default)
            cell.setTextFieldText(text: self.viewModel.registrationDetail.authenticationCode)
            cell.textFieldTextDidChange = { [unowned self] textField in
                self.viewModel.registrationDetail.authenticationCode = textField.text
                self.updateNavigationButtonsEnabled()
                if self.viewModel.registrationDetail.authenticationCodeValid {
                    UIView.performWithoutAnimation {
                        tableView.beginUpdates()
                        cell.setErrorMessage(message: nil)
                        tableView.endUpdates()
                    }
                }
            }
        case .entityNumber:
            cell.setHeadingLabelText(text: CommonStrings.Activate.EntityNumber358)
            cell.setTextFieldPlaceholder(placeholder: CommonStrings.Activate.EntityNumberPlaceholder359)
            cell.cellImageConfig = self.registrationImage(asset: UKEssentialsAsset.LoginRegistration.registrationEntityNumber.templateImage)
            cell.setTextFieldSecureTextEntry(secureTextEntry: false)
            cell.setTextFieldKeyboardType(type: .numberPad)
            cell.textFieldTextDidChange = { [unowned self] textField in
                self.viewModel.registrationDetail.entityNumber = textField.text
                self.updateNavigationButtonsEnabled()
                if self.viewModel.registrationDetail.entityNumberValid {
                    UIView.performWithoutAnimation {
                        tableView.beginUpdates()
                        cell.setErrorMessage(message: nil)
                        tableView.endUpdates()
                    }
                }
            }
        }
        
        return cell
    }

    // MARK: - DatePickerDelegate

    func didPickDate(_ date: Date) {
        self.viewModel.registrationDetail.dateOfBirth = date
        self.updateNavigationButtonsEnabled()
        guard let dobTitleCell = self.tableView.cellForRow(at: IndexPath(row: 0, section: UKERegistrationSection.dateOfBirth.rawValue)) as? VIARightDetailTableViewCell else { return }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMMM dd, yyyy"
        dobTitleCell.setValue(dateFormatter.string(from: date))
    }

    // MARK: - UITableView delegate

    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        guard UKERegistrationSection(rawValue: section) == .dateOfBirth else { return UITableViewAutomaticDimension }
        return 35.0
    }

    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        guard let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier) as? VIATableViewSectionHeaderFooterView else {
            return nil
        }
        guard let section = UKERegistrationSection(rawValue: section) else { return nil }
        switch section {
        case .dateOfBirth:
            return nil
        case .authenticationCode:
            view.labelText = CommonStrings.Activate.AuthenticationCodeFooter357
        case .entityNumber:
            view.labelText = CommonStrings.Activate.EntityNumberFooter360
        }

        return view
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        if indexPath.section == UKERegistrationSection.dateOfBirth.rawValue && indexPath.row == 0 {
            toggleShowingDateOfBirth()
        } else {
            let cell = tableView.cellForRow(at: indexPath) as! VIATextFieldTableViewCell
            _ = cell.becomeFirstResponder()
        }
    }
    
    func showDateOfBirth() {
        if !showingDate {
            toggleShowingDateOfBirth()
        }
    }
    
    func toggleShowingDateOfBirth() {
        showingDate = !showingDate
        let indexPath = IndexPath(row: 1, section: UKERegistrationSection.dateOfBirth.rawValue)
        if self.showingDate {
            tableView.beginUpdates()
            tableView.insertRows(at: [indexPath], with: .automatic)
            tableView.endUpdates()
        } else {
            tableView.beginUpdates()
            tableView.deleteRows(at: [indexPath], with: .automatic)
            tableView.endUpdates()
        }
    }

    // MARK: - Actions


    @IBAction func cancelButtonTapped(_ sender: UIBarButtonItem) {
        self.tableView.endEditing(true)
        self.performSegue(withIdentifier: "unwindToLogin", sender: self)
    }


    @IBAction func doneButtonTapped(_ sender: UIBarButtonItem) {
        self.updateNavigationButtonsEnabled()

        self.tableView.endEditing(true)
        self.showHUDOnWindow()

        self.performRegistration()
    }

    func performRegistration() {
        guard let authCode = self.viewModel.registrationDetail.authenticationCode else { return }
        guard let dateOfBirth = self.viewModel.registrationDetail.dateOfBirth else { return }
        weak var weakSelf = self
        authFlowController?.validateUKERegistrationInfo(withAuthenticationCode: authCode, dateOfBirth: dateOfBirth, entityNumber: self.viewModel.registrationDetail.entityNumber, delegate:self)
    }

    // MARK: - Images

    func registrationImage(asset: UIImage) -> VIATextFieldCellImageConfig {
        var imageConfig = VIATextFieldCellImageConfig()
        imageConfig.templateImage = asset
        return imageConfig
    }

    // MARK: - UKENHAuthenticationDelegate
    func authorisationCodeValidationFailed() {
        hideHUDFromWindow()
        let alert = UIAlertController(title: CommonStrings.AlertIncorrectCodeTitle370, message: CommonStrings.AlertIncorrectCodeMessage371, preferredStyle: .alert)
        let okAction = UIAlertAction(title: CommonStrings.OkButtonTitle40, style: .default) { [unowned self] action in
            guard let cell = self.tableView.cellForRow(at: IndexPath(row: 0, section: UKERegistrationSection.authenticationCode.rawValue)) as? VIATextFieldTableViewCell else { return }
            cell.setTextFieldText(text: nil)
            _ = cell.becomeFirstResponder()
            self.viewModel.registrationDetail.authenticationCode = nil
            self.updateNavigationButtonsEnabled()
        }
        alert.addAction(okAction)
        present(alert, animated: true, completion: nil)
    }

    func dateOfBirthValidationFailed() {
        hideHUDFromWindow()
        let alert = UIAlertController(title: CommonStrings.AlertDateOfBirthTitle1052, message: CommonStrings.AlertDateOfBirthMessage1053, preferredStyle: .alert)
        let okAction = UIAlertAction(title: CommonStrings.OkButtonTitle40, style: .default) { [unowned self] action in
            self.showDateOfBirth()
            self.updateNavigationButtonsEnabled()
        }
        alert.addAction(okAction)
        present(alert, animated: true, completion: nil)
    }

    func entityNumberValidationFailed() {
        hideHUDFromWindow()
        let alert = UIAlertController(title: CommonStrings.AlertIncorrectNumberTitle372, message: CommonStrings.AlertIncorrectNumberMessage373, preferredStyle: .alert)
        let okAction = UIAlertAction(title: CommonStrings.OkButtonTitle40, style: .default) { [unowned self] action in
            guard let cell = self.tableView.cellForRow(at: IndexPath(row: 0, section: UKERegistrationSection.entityNumber.rawValue)) as? VIATextFieldTableViewCell else { return }
            cell.setTextFieldText(text: nil)
            _ = cell.becomeFirstResponder()
            self.viewModel.registrationDetail.entityNumber = nil
            self.updateNavigationButtonsEnabled()
        }
        alert.addAction(okAction)
        present(alert, animated: true, completion: nil)
    }
    
    func unknownErrorOccured() {
        hideHUDFromWindow()
        let alert = UIAlertController(title: CommonStrings.GenericUnkownErrorTitle266, message: CommonStrings.GenericUnkownErrorMessage267, preferredStyle: .alert)
        let okAction = UIAlertAction(title: CommonStrings.OkButtonTitle40, style: .default, handler: nil)
        alert.addAction(okAction)
        present(alert, animated: true, completion: nil)
    }
}
