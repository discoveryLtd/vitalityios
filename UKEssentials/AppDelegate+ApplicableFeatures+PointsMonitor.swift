//
//  AppDelegate+ApplicableFeatures+PointsMonitor.swift
//  UKEssentials
//
//  Created by wenilyn.a.teorica on 10/05/2019.
//  Copyright © 2019 Glucode. All rights reserved.
//

import Foundation
import VIAUtilities
import VitalityKit
import VIACommon
import VIAPointsMonitor

extension AppDelegate {
    
    public func getPointsMonitorActivityDetailViewModel() -> AnyObject? {
        return PointsMonitorActivityDetailViewModel()
    }
}
