import Foundation
import VitalityKit

extension AppDelegate: WireDelegate {

//    public var devBaseURL: URL {
//        return URL(string: "https://dev.vitalityservicing.com/uke/api")!
//    }

    public var testBaseURL: URL {
        return URL(string: "https://test.vitalityservicing.com/uke/api")!
    }

    public var qaBaseURL: URL {
        return URL(string: "https://qa.vitalityactive.com/api")!
    }

    public var qaFrankfurtURL: URL {
        return URL(string: "https://qa.vitalityactive.com/api")!
    }

    public var productionBaseURL: URL {
        
        //OLD PROD URL: ukvitality.vitalityactive.com
        //NEW PROD URL: m.healthy-workplace.com
        
        return URL(string: "https://M.healthy-workplace.com/api")!
    }
    
    public var performanceBaseURL: URL {
        return URL(string: "https://qa.uke.vitalityservicing.com/api")!
    }
    
    public var qaSouthKoreaBaseURL: URL{
        return URL(string: "")!
    }
}

extension AppDelegate{
    
//    public var devAPIManagerIdentifier: String {
//        return "0000000000000000000000000000000000000000000000000000000000000000000000000000"
//    }
    
    public var testAPIManagerIdentifier: String {
        return "Rmw3X3NvcG5RcGZNQnkxcjdURllHSkZyMnBvYTpfb2ZTaGxiWk5OZmJIdFhtYmFMVTdPOUY3R01h"
    }
    
    public var qaAPIManagerIdentifier: String {
//        return "QWJCRUJxQkI5SWcxbExGVFdhZ2ZmemJqN2Z3YTpmWkhHQmZPbk15SW1FTU5zX2VUdDVqczFjVThh"
        return "bnlSWG9nWFJfWjdCY2Z2YWtwa2pyNnVzSWtJYTp6RHZPQllWZzJvc0poZ1hLc0tGNW1YRmpMX3Nh"
    }
    
    public var qaFrankfurtAPIManagerIdentifier: String {
        return "bnlSWG9nWFJfWjdCY2Z2YWtwa2pyNnVzSWtJYTp6RHZPQllWZzJvc0poZ1hLc0tGNW1YRmpMX3Nh"
    }
    
    public var productionAPIManagerIdentifier: String {
        return "eG9WSFZPMHFqOXNFSGZWanM4RHBfZllSRDV3YTpYSnRUd2toZU1fR0xSRjlncU5DYkFkdDRwV1Vh"
    }
    
    public var performanceAPIManagerIdentifier: String {
        return "QWJCRUJxQkI5SWcxbExGVFdhZ2ZmemJqN2Z3YTpmWkhHQmZPbk15SW1FTU5zX2VUdDVqczFjVThh"
    }
    
    public var qaSouthKoreaAPIManagerIdentifier: String {
        return ""
    }
}
