// Generated using SwiftGen, by O.Halligon — https://github.com/SwiftGen/SwiftGen

// swiftlint:disable sorted_imports
import Foundation
import UIKit
import VIAActiveRewards
import VIACore

// swiftlint:disable file_length

protocol StoryboardType {
  static var storyboardName: String { get }
}

extension StoryboardType {
  static var storyboard: UIStoryboard {
    return UIStoryboard(name: self.storyboardName, bundle: Bundle(for: BundleToken.self))
  }
}

struct SceneType<T: Any> {
  let storyboard: StoryboardType.Type
  let identifier: String

  func instantiate() -> T {
    guard let controller = storyboard.storyboard.instantiateViewController(withIdentifier: identifier) as? T else {
      fatalError("ViewController '\(identifier)' is not of the expected class \(T.self).")
    }
    return controller
  }
}

struct InitialSceneType<T: Any> {
  let storyboard: StoryboardType.Type

  func instantiate() -> T {
    guard let controller = storyboard.storyboard.instantiateInitialViewController() as? T else {
      fatalError("ViewController is not of the expected class \(T.self).")
    }
    return controller
  }
}

protocol SegueType: RawRepresentable { }

extension UIViewController {
  func perform<S: SegueType>(segue: S, sender: Any? = nil) where S.RawValue == String {
    performSegue(withIdentifier: segue.rawValue, sender: sender)
  }
}

// swiftlint:disable explicit_type_interface identifier_name line_length type_body_length type_name
enum UKEssentialsStoryboard {
  enum LaunchScreen: StoryboardType {
    static let storyboardName = "LaunchScreen"

    static let initialScene = InitialSceneType<UIViewController>(storyboard: LaunchScreen.self)
  }
  enum LoginRegistration: StoryboardType {
    static let storyboardName = "LoginRegistration"

    static let initialScene = InitialSceneType<VIAFirstTimeOnboardingViewController>(storyboard: LoginRegistration.self)

    static let loginViewController = SceneType<UKEssentials.UKELoginViewController>(storyboard: LoginRegistration.self, identifier: "LoginViewController")

    static let ukEssentialsUKEAuthorisationViewController = SceneType<UKEssentials.UKERegistrationViewController>(storyboard: LoginRegistration.self, identifier: "UKEssentials.UKEAuthorisationViewController")

    static let viaFirstTimeOnboardingViewController = SceneType<VIAFirstTimeOnboardingViewController>(storyboard: LoginRegistration.self, identifier: "VIAFirstTimeOnboardingViewController")

    static let viaRegistrationNavigationController = SceneType<VIACore.VIANavigationViewController>(storyboard: LoginRegistration.self, identifier: "VIARegistrationNavigationController")

    static let viaSplashScreenViewController = SceneType<UKEssentials.VIASplashScreenViewController>(storyboard: LoginRegistration.self, identifier: "VIASplashScreenViewController")
  }
  enum Main: StoryboardType {
    static let storyboardName = "Main"

    static let initialScene = InitialSceneType<ViewController>(storyboard: Main.self)
  }
  enum UKEFirstTimeOnboarding: StoryboardType {
    static let storyboardName = "UKEFirstTimeOnboarding"

    static let initialScene = InitialSceneType<VIACore.UKEFirstTimeOnboardingViewController>(storyboard: UKEFirstTimeOnboarding.self)
  }
  enum VIAUKECinemaRewards: StoryboardType {
    static let storyboardName = "VIAUKECinemaRewards"

    static let initialScene = InitialSceneType<VIAActiveRewards.VIAUKECinemaRewardViewController>(storyboard: VIAUKECinemaRewards.self)
  }
  enum VIAUKENoReward: StoryboardType {
    static let storyboardName = "VIAUKENoReward"

    static let initialScene = InitialSceneType<VIAActiveRewards.VIAUKENoRewardViewController>(storyboard: VIAUKENoReward.self)

    static let viaukeNoRewardViewController = SceneType<VIAActiveRewards.VIAUKENoRewardViewController>(storyboard: VIAUKENoReward.self, identifier: "VIAUKENoRewardViewController")
  }
  enum VIAUKEStarbucksRewards: StoryboardType {
    static let storyboardName = "VIAUKEStarbucksRewards"

    static let initialScene = InitialSceneType<UKEssentials.VIAUKEStarbucksRewardViewController>(storyboard: VIAUKEStarbucksRewards.self)

    static let viaukeStarbucksRewardViewController = SceneType<UKEssentials.VIAUKEStarbucksRewardViewController>(storyboard: VIAUKEStarbucksRewards.self, identifier: "VIAUKEStarbucksRewardViewController")
  }
  enum VIAUKEVoucherSolutionsRewards: StoryboardType {
    static let storyboardName = "VIAUKEVoucherSolutionsRewards"

    static let initialScene = InitialSceneType<UKEssentials.VIAUKEVoucherSolutionsViewController>(storyboard: VIAUKEVoucherSolutionsRewards.self)

    static let viaukeVoucherSolutionsViewController = SceneType<UKEssentials.VIAUKEVoucherSolutionsViewController>(storyboard: VIAUKEVoucherSolutionsRewards.self, identifier: "VIAUKEVoucherSolutionsViewController")
  }
}

enum StoryboardSegue {
  enum LoginRegistration: String, SegueType {
    case showAuthenticationScreen
    case unwindToLogin
    case unwindToLoginAfterSuccessfulRegistration
  }
  enum VIAUKEStarbucksRewards: String, SegueType {
    case showRewardTerms
  }
}
// swiftlint:enable explicit_type_interface identifier_name line_length type_body_length type_name

private final class BundleToken {}
