// Generated using SwiftGen, by O.Halligon — https://github.com/SwiftGen/SwiftGen

#if os(OSX)
import AppKit.NSImage
public typealias UKEssentialsColor = NSColor
public typealias UKEssentialsImage = NSImage
#elseif os(iOS) || os(tvOS) || os(watchOS)
import UIKit.UIImage
public typealias UKEssentialsColor = UIColor
public typealias UKEssentialsImage = UIImage
#endif

// swiftlint:disable file_length

public typealias UKEssentialsAssetType = UKEssentialsImageAsset

public struct UKEssentialsImageAsset: Equatable {
	fileprivate var name: String

	public var templateImage: UKEssentialsImage {
		let bundle = Bundle(for: BundleToken.self)
		#if os(iOS) || os(tvOS)
		let image = UKEssentialsImage(named: name, in: bundle, compatibleWith: nil)?.withRenderingMode(.alwaysTemplate)
		#elseif os(OSX)
		let image = bundle.image(forResource: name).withRenderingMode(.alwaysTemplate)
		#elseif os(watchOS)
		let image = UKEssentialsImage(named: name).withRenderingMode(.alwaysTemplate)
		#endif
		guard let result = image else { fatalError("Unable to load template image named \(name).") }
		return result
	}

	public var image: UKEssentialsImage {
		let bundle = Bundle(for: BundleToken.self)
		#if os(iOS) || os(tvOS)
		let image = UKEssentialsImage(named: name, in: bundle, compatibleWith: nil)
		#elseif os(OSX)
		let image = bundle.image(forResource: name)
		#elseif os(watchOS)
		let image = UKEssentialsImage(named: name)
		#endif
		guard let result = image else { fatalError("Unable to load image named \(name).") }
		return result
	}

	public static func ==(lhs: UKEssentialsImageAsset, rhs: UKEssentialsImageAsset) -> Bool {
        return lhs.image == rhs.image
    }
}

public struct UKEssentialsColorAsset {
fileprivate var name: String

#if swift(>=3.2)
@available(iOS 11.0, tvOS 11.0, watchOS 4.0, OSX 10.13, *)
var color: UKEssentialsColor {
return UKEssentialsColor(asset: self)
}
#endif
}

// swiftlint:disable identifier_name line_length nesting type_body_length type_name
public enum UKEssentialsAsset {
  public enum LoginRegistration {
    public static let registrationActivationCode = UKEssentialsImageAsset(name: "registrationActivationCode")
    public static let loginLogoEn = UKEssentialsImageAsset(name: "loginLogoEn")
    public static let registrationEntityNumber = UKEssentialsImageAsset(name: "registrationEntityNumber")
  }
  public enum Rewards {
    public static let rewardTrophyBig = UKEssentialsImageAsset(name: "rewardTrophyBig")
    public static let registrationEmail = UKEssentialsImageAsset(name: "registrationEmail")
    public static let cinemas = UKEssentialsImageAsset(name: "cinemas")
    public static let noreward = UKEssentialsImageAsset(name: "noreward")
    public static let cineworld = UKEssentialsImageAsset(name: "cineworld")
    public static let starbucks = UKEssentialsImageAsset(name: "starbucks")
    public static let vue = UKEssentialsImageAsset(name: "vue")
  }
  public enum FirstTimeOnboardingUKE {
    public static let onboardingTrackActivity = UKEssentialsImageAsset(name: "onboardingTrackActivity")
    public static let onboardingExplore = UKEssentialsImageAsset(name: "onboardingExplore")
    public static let onboardingHeathReview = UKEssentialsImageAsset(name: "onboardingHeathReview")
    public static let onboardingRewards = UKEssentialsImageAsset(name: "onboardingRewards")
  }

  // swiftlint:disable trailing_comma
  public static let allColors: [UKEssentialsColorAsset] = [
  ]
  public static let allImages: [UKEssentialsImageAsset] = [
    LoginRegistration.registrationActivationCode,
    LoginRegistration.loginLogoEn,
    LoginRegistration.registrationEntityNumber,
    Rewards.rewardTrophyBig,
    Rewards.registrationEmail,
    Rewards.cinemas,
    Rewards.noreward,
    Rewards.cineworld,
    Rewards.starbucks,
    Rewards.vue,
    FirstTimeOnboardingUKE.onboardingTrackActivity,
    FirstTimeOnboardingUKE.onboardingExplore,
    FirstTimeOnboardingUKE.onboardingHeathReview,
    FirstTimeOnboardingUKE.onboardingRewards,
  ]
  // swiftlint:enable trailing_comma
  @available(*, deprecated, renamed: "allImages")
  public static let allValues: [UKEssentialsAssetType] = allImages
}
// swiftlint:enable identifier_name line_length nesting type_body_length type_name

extension UKEssentialsImage {
@available(iOS 1.0, tvOS 1.0, watchOS 1.0, *)
@available(OSX, deprecated,
message: "This initializer is unsafe on macOS, please use the UKEssentialsImageAsset.image property")
convenience init!(asset: UKEssentialsAssetType) {
#if os(iOS) || os(tvOS)
let bundle = Bundle(for: BundleToken.self)
self.init(named: asset.name, in: bundle, compatibleWith: nil)
#elseif os(OSX) || os(watchOS)
self.init(named: asset.name)
#endif
}
}

extension UKEssentialsColor {
#if swift(>=3.2)
@available(iOS 11.0, tvOS 11.0, watchOS 4.0, OSX 10.13, *)
convenience init!(asset: UKEssentialsColorAsset) {
let bundle = Bundle(for: BundleToken.self)
#if os(iOS) || os(tvOS)
self.init(named: asset.name, in: bundle, compatibleWith: nil)
#elseif os(OSX)
self.init(named: asset.name, bundle: bundle)
#elseif os(watchOS)
self.init(named: asset.name)
#endif
}
#endif
}

private final class BundleToken {}
