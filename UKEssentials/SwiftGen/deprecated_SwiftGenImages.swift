// Generated using SwiftGen, by O.Halligon — https://github.com/AliSoftware/SwiftGen

internal class UKEssentialsImagesPlaceholder {
    // Placeholder class to reference lower down
}

#if os(iOS) || os(tvOS) || os(watchOS)
  import UIKit.UIImage
  typealias Image = UIImage
#elseif os(OSX)
  import AppKit.NSImage
  typealias Image = NSImage
#endif

// swiftlint:disable file_length
// swiftlint:disable line_length

// swiftlint:disable type_body_length
@available(*, deprecated, message: "Use UKEssentialsAsset.{image}.image or SumitomoAsseUKEssentialsAsset.{image}.templateImage instead")
internal enum UKEAsset: String {
  case loginLogoEn = "loginLogoEn"
  case registrationActivationCode = "registrationActivationCode"
  case registrationEntityNumber = "registrationEntityNumber"

  var image: Image {
    return Image(asset: self)
  }
}
// swiftlint:enable type_body_length

internal extension UIImage {
    @available(*, deprecated, message: "Use UKEssentialsAsset.{image}.image or SumitomoAsseUKEssentialsAsset.{image}.templateImage instead")
    class func templateImage(asset: UKEAsset) -> UIImage? {
        let bundle = Bundle(for: UKEssentialsImagesPlaceholder.self) // placeholder declared at top of file
        let image = UIImage(named: asset.rawValue, in: bundle, compatibleWith: nil)?.withRenderingMode(.alwaysTemplate)
        return image
    }
}

internal extension Image {
    convenience init!(asset: UKEAsset) {
        @available(*, deprecated, message: "Use UKEssentialsAsset.{image}.image or SumitomoAsseUKEssentialsAsset.{image}.templateImage instead")
        let bundle = Bundle(for: UKEssentialsImagesPlaceholder.self) // placeholder declared at top of file
        self.init(named: asset.rawValue, in: bundle, compatibleWith: nil)
    }
}
