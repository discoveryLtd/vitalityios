import Foundation
import UIKit

import VIACore
import VIAUtilities
import VitalityKit
import VIAPushwoosh
import VIAUIKit

import Firebase
import Pushwoosh

@UIApplicationMain
public class AppDelegate: UIResponder, UIApplicationDelegate, VitalityActiveConfigurer {

    // MARK: Properties

    public var window: UIWindow?

    // MARK: Convenience

    public class func currentDelegate() -> AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }

    // MARK: UIApplicationDelegate

    public func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]? = nil) -> Bool {

        // Analytics
        FirebaseConfiguration.shared.setLoggerLevel(.min)
        FirebaseApp.configure()
        GoogleTagManagerPatch.patchGoogleTagManagerLogging()

        configureVitalityActiveDelegate()

        /*
         * This will disable the logging of Storyboard and XIB files related warnings.
         * Let's enable this during performance testing or refactoring.
         */
        UserDefaults.standard.setValue(false, forKey: "_UIConstraintBasedLayoutLogUnsatisfiable")
        UserDefaults.standard.setValue(false, forKey: "_UIViewAlertForUnsatisfiableConstraints")
        
        if isPushwooshEnabled(){
            let pushDelegate = VIAPushNotificationAppDelegate()            
            pushDelegate.application(application, didFinishLaunchingWithOptions: launchOptions, delegate: self)
        }
        
        return true
    }

    public func applicationWillResignActive(_ application: UIApplication) {
    }

    public func applicationDidEnterBackground(_ application: UIApplication) {
    }

    public func applicationWillEnterForeground(_ application: UIApplication) {
    }

    public func applicationDidBecomeActive(_ application: UIApplication) {
    }

    public func applicationWillTerminate(_ application: UIApplication) {
    }


    // MARK: URL scheme launch

    public func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        NotificationInfo.postNotification(from: url)
        return true
    }

    public func setupLoginStory() {
        let loginStoryboard = UIStoryboard(name: "LoginRegistration", bundle: nil)
        self.window?.rootViewController = loginStoryboard.instantiateInitialViewController()
        self.window?.makeKeyAndVisible()
    }
}

extension AppDelegate: PushNotificationDelegate{
    
    public func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        if isPushwooshEnabled(){
            PushNotificationManager.push().handlePushRegistration(deviceToken as Data!)
            print("didRegisterForRemoteNotificationsWithDeviceToken")
        }
    }
    
    public func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        if isPushwooshEnabled(){
            PushNotificationManager.push().handlePushRegistrationFailure(error)
            print("didFailToRegisterForRemoteNotificationsWithError")
        }
    }
    
    public func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any],
                            fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        if isPushwooshEnabled(){
            PushNotificationManager.push().handlePushReceived(userInfo)
            print("didReceiveRemoteNotification")
            completionHandler(UIBackgroundFetchResult.noData)
        }
    }
    
    public func onPushReceived(_ pushManager: PushNotificationManager!, withNotification pushNotification: [AnyHashable : Any]!, onStart: Bool) {
        if isPushwooshEnabled(){
            print("Push notification received: \(pushNotification)")
            PushNotificationController.sharedManager.pushNotificationParser(pushNotification: pushNotification)
            if UIApplication.shared.applicationState == .active {
                PushNotificationManager.push().showPushnotificationAlert = false
                PushNotificationController.sharedManager.storeInAppNotification()
                PushNotificationController.sharedManager.retrieveInAppNotification(self)
            }
        }
    }
    
    public func onPushAccepted(_ pushManager: PushNotificationManager!, withNotification pushNotification: [AnyHashable : Any]!, onStart: Bool) {
        if isPushwooshEnabled(){
            print("Push notification accepted: \(pushNotification)")
            PushNotificationController.sharedManager.goToSectionWithPushNotification(self)
        }
    }
}

extension AppDelegate{
    
    fileprivate func isPushwooshEnabled() -> Bool{
        return Bundle.main.object(forInfoDictionaryKey: "VAEnablePushwoosh") as? Bool ?? false
    }
}

extension AppDelegate: ViewControllerDelegateDataSource{
    
    public func analyticsLogEvent(from originClass: AnyClass) {
        let screenName = NSStringFromClass(originClass)
        debugPrint("logEvent: \(screenName)")
        Analytics.logEvent("screen_view_dup", parameters: ["screen_name": screenName])
    }
}
