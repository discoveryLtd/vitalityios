//
//  AppDelegate.swift
//  Manulife
//
//  Created by Michelle R. Oratil on 09/11/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import UIKit

import VIACore
import VIAUtilities
import VitalityKit
import VIACommon
import MLILogin
import VIAUIKit

import Firebase
import Pushwoosh

@UIApplicationMain
public class AppDelegate: UIResponder, UIApplicationDelegate, VitalityActiveConfigurer, PushNotificationDelegate {
    
    // MARK: Properties
    
    public var window: UIWindow?
    
    // MARK: Convenience
    
    public class func currentDelegate() -> AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }
    
    // MARK: UIApplicationDelegate
    
    public func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]? = nil) -> Bool {
        
        //PUSHWOOSH
        //        UserDefaults.standard.removeObject(forKey:"applicationBadge")
        //        UserDefaults.standard.removeObject(forKey:"inAppBadges")
        UserDefaults.standard.removeObject(forKey:"inAppMessages")
        UIApplication.shared.applicationIconBadgeNumber = 0
        
        //Change notification name
        NotificationCenter.default.addObserver(self, selector: #selector(setTagsForPushNotification), name: Notification.Name("SetTagsForPushNotification"), object: nil)
        //PUSHWOOSH
        
        // Analytics
        FirebaseConfiguration.shared.setLoggerLevel(.min)
        FirebaseApp.configure()
        GoogleTagManagerPatch.patchGoogleTagManagerLogging()
        
        configureVitalityActiveDelegate()
        
        //PUSHWOOSH
        //self.registerForPushNotication()
        //PUSHWOOSH
        
        /*
         * This will disable the logging of Storyboard and XIB files related warnings.
         * Let's enable this during performance testing or refactoring.
         */
        UserDefaults.standard.setValue(false, forKey: "_UIConstraintBasedLayoutLogUnsatisfiable")
        UserDefaults.standard.setValue(false, forKey: "_UIViewAlertForUnsatisfiableConstraints")
        
        return true
    }
    
    public func applicationWillResignActive(_ application: UIApplication) {
    }
    
    public func applicationDidEnterBackground(_ application: UIApplication) {
    }
    
    public func applicationWillEnterForeground(_ application: UIApplication) {
    }
    
    public func applicationDidBecomeActive(_ application: UIApplication) {
        debugPrint("applicationDidBecomeActive")
        
        if !VitalityParty.accessTokenForCurrentParty().isEmpty{
            VIAUpdaterUtil.checkUpdateForNewAppVersion(shouldLogoutOnNilTenantID: false)
        }
    }
    
    public func applicationWillTerminate(_ application: UIApplication) {
    }
    
    // MARK: URL scheme launch
    
    public func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        NotificationInfo.postNotification(from: url)
        return true
    }
    
    public func setupLoginStory() {
        let storyboardBundle = Bundle(for: MLINavigationController.self)
        let vc = UIStoryboard(name: "Login", bundle: storyboardBundle).instantiateViewController(withIdentifier: "MLINavigationController")
        
        self.window?.rootViewController = vc
        self.window?.makeKeyAndVisible()
    }
    
    //PUSHWOOSH
    //MARK: Push Notification
    
    public func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        PushNotificationManager.push().handlePushRegistration(deviceToken as Data!)
        print("didRegisterForRemoteNotificationsWithDeviceToken")
    }
    
    public func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        PushNotificationManager.push().handlePushRegistrationFailure(error)
        print("didFailToRegisterForRemoteNotificationsWithError")
    }
    
    public func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any],
                            fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        PushNotificationManager.push().handlePushReceived(userInfo)
        print("didReceiveRemoteNotification")
        completionHandler(UIBackgroundFetchResult.noData)
    }
    
    // this event is fired when the push is received in the app
    public func onPushReceived(_ pushManager: PushNotificationManager!, withNotification pushNotification: [AnyHashable : Any]!, onStart: Bool) {
        print("Push notification received: \(pushNotification)")
        
        PushNotificationController.sharedManager.pushNotificationParser(pushNotification: pushNotification)
        if UIApplication.shared.applicationState == .active {
            //hide push
            PushNotificationManager.push().showPushnotificationAlert = false
            PushNotificationController.sharedManager.storeInAppNotification()
            PushNotificationController.sharedManager.retrieveInAppNotification()
        }
        
        // shows a push is received. Implement passive reaction to a push, such as UI update or data download.
    }
    
    // this event is fired when user clicks on notification
    public func onPushAccepted(_ pushManager: PushNotificationManager!, withNotification pushNotification: [AnyHashable : Any]!, onStart: Bool) {
        print("Push notification accepted: \(pushNotification)")
        
        PushNotificationController.sharedManager.goToSectionWithPushNotification()
        
        // shows a user tapped the notification. Implement user interaction, such as showing push details
    }
    
    public func registerForPushNotication() {
        // set custom delegate for push handling, in our case AppDelegate
        PushNotificationManager.push().delegate = self
        
        // set default Pushwoosh delegate for iOS10 foreground push handling
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().delegate = PushNotificationManager.push().notificationCenterDelegate
        }
        
        // make sure we count app open in Pushwoosh stats
        PushNotificationManager.push().sendAppOpen()
        
        // register for push notifications!
        PushNotificationManager.push().registerForPushNotifications()
    }
    
    public func setTagsForPushNotification(notification: NSNotification) {
        // set tags
        let uIDs = notification.userInfo
        let partyID = uIDs!["partyId"]
        let tenantID = uIDs!["tenantId"]
        let environment = Wire.default.currentEnvironment.name().lowercased()
        print (environment)
        
        let tags : [NSObject : AnyObject] = [NSString(string: "Party ID") : partyID as AnyObject, NSString(string: "Tenant ID") : tenantID as AnyObject, NSString(string: "Environment") : environment as AnyObject]
        
        PushNotificationManager.push().setTags(tags)
    }
    
    //PUSHWOOSH
    
}

extension AppDelegate: ViewControllerDelegateDataSource{
    
    public func analyticsLogEvent(from originClass: AnyClass) {
        let screenName = NSStringFromClass(originClass)
        debugPrint("logEvent: \(screenName)")
        Analytics.logEvent("screen_view_dup", parameters: ["screen_name": screenName])
    }
}

