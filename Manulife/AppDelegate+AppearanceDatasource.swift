import VIACommon
import VIAUIKit
import VIACore
import UIKit
import VitalityKit

extension AppDelegate: AppearanceColorDataSource {
    class func manulifeGreenColor() -> UIColor {
        return UIColor(red: 0, green: 136.0/255.0, blue: 83.0/255.0, alpha: 1.0) // Hex code: #008853
    }

    // MARK: AppearanceDataSource

    public var primaryColor: UIColor {
        return AppDelegate.manulifeGreenColor()
    }

    public var tabBarBackgroundColor: UIColor {
        return .white
    }
    
    public var tabBarBarTintColor: UIColor {
        return .white
    }
    
    public var tabBarTintColor: UIColor? {
        return UIColor.primaryColor()
    }
    
    public var unselectedItemTintColor: UIColor? {
        return nil
    }
    
    public var getInsurerGlobalTint: UIColor? {
        // use this once BE is returning correct tint color:
        //return VIAAppearance.default.primaryColorFromServer
        
        return VIAAppearance.default.dataSource?.primaryColor
    }
    
    public func getSplashScreenGradientTop() -> UIColor{
        // use this once BE is returning correct splashscreen color:
        //return UIColor(hexString: AppConfigFeature.insurerGradientColor1Hex())
        
        return .white
    }
    
    public func getSplashScreenGradientBottom() -> UIColor{
        // use this once BE is returning correct splashscreen color:
        //return UIColor(hexString: AppConfigFeature.insurerGradientColor2Hex())
        
        return .white
    }
}

extension AppDelegate: AppearanceIconographyDataSource {
    
    public func homeLogo(for locale: Locale) -> UIImage {
        if locale.languageCode == "fr" {
            return ManulifeAsset.manulifeHomeLogoFr.image
        }
        return ManulifeAsset.manulifeHomeLogo.image
    }

    public func loginLogo(for locale: Locale) -> UIImage {
        return ManulifeAsset.manulifeLoginLogo.image
    }
    
    public func splashLogo(for locale: Locale = Locale.current) -> UIImage {
        if locale.languageCode == "fr" {
            return ManulifeAsset.manulifeSplashLogoFr.image
        }
        return ManulifeAsset.manulifeSplashLogo.image
    }
}
