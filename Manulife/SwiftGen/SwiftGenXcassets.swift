// Generated using SwiftGen, by O.Halligon — https://github.com/SwiftGen/SwiftGen

#if os(OSX)
import AppKit.NSImage
public typealias ManulifeColor = NSColor
public typealias ManulifeImage = NSImage
#elseif os(iOS) || os(tvOS) || os(watchOS)
import UIKit.UIImage
public typealias ManulifeColor = UIColor
public typealias ManulifeImage = UIImage
#endif

// swiftlint:disable file_length

public typealias ManulifeAssetType = ManulifeImageAsset

public struct ManulifeImageAsset: Equatable {
	fileprivate var name: String

	public var templateImage: ManulifeImage {
		let bundle = Bundle(for: BundleToken.self)
		#if os(iOS) || os(tvOS)
		let image = ManulifeImage(named: name, in: bundle, compatibleWith: nil)?.withRenderingMode(.alwaysTemplate)
		#elseif os(OSX)
		let image = bundle.image(forResource: name).withRenderingMode(.alwaysTemplate)
		#elseif os(watchOS)
		let image = ManulifeImage(named: name).withRenderingMode(.alwaysTemplate)
		#endif
		guard let result = image else { fatalError("Unable to load template image named \(name).") }
		return result
	}

	public var image: ManulifeImage {
		let bundle = Bundle(for: BundleToken.self)
		#if os(iOS) || os(tvOS)
		let image = ManulifeImage(named: name, in: bundle, compatibleWith: nil)
		#elseif os(OSX)
		let image = bundle.image(forResource: name)
		#elseif os(watchOS)
		let image = ManulifeImage(named: name)
		#endif
		guard let result = image else { fatalError("Unable to load image named \(name).") }
		return result
	}

	public static func ==(lhs: ManulifeImageAsset, rhs: ManulifeImageAsset) -> Bool {
        return lhs.image == rhs.image
    }
}

public struct ManulifeColorAsset {
fileprivate var name: String

#if swift(>=3.2)
@available(iOS 11.0, tvOS 11.0, watchOS 4.0, OSX 10.13, *)
var color: ManulifeColor {
return ManulifeColor(asset: self)
}
#endif
}

// swiftlint:disable identifier_name line_length nesting type_body_length type_name
public enum ManulifeAsset {
  public static let vitalityLogo = ManulifeImageAsset(name: "vitalityLogo")
  public static let manulifeHomeLogo = ManulifeImageAsset(name: "manulifeHomeLogo")
  public static let manulifeHomeLogoFr = ManulifeImageAsset(name: "manulifeHomeLogo_fr")
  public static let manulifeSplashLogoFr = ManulifeImageAsset(name: "manulifeSplashLogo_fr")
  public static let manulifeSplashLogo = ManulifeImageAsset(name: "manulifeSplashLogo")
  public static let manulifeLogo = ManulifeImageAsset(name: "manulifeLogo")
  public static let manulifeLoginLogo = ManulifeImageAsset(name: "manulifeLoginLogo")

  // swiftlint:disable trailing_comma
  public static let allColors: [ManulifeColorAsset] = [
  ]
  public static let allImages: [ManulifeImageAsset] = [
    vitalityLogo,
    manulifeHomeLogo,
    manulifeHomeLogoFr,
    manulifeSplashLogoFr,
    manulifeSplashLogo,
    manulifeLogo,
    manulifeLoginLogo,
  ]
  // swiftlint:enable trailing_comma
  @available(*, deprecated, renamed: "allImages")
  public static let allValues: [ManulifeAssetType] = allImages
}
// swiftlint:enable identifier_name line_length nesting type_body_length type_name

extension ManulifeImage {
@available(iOS 1.0, tvOS 1.0, watchOS 1.0, *)
@available(OSX, deprecated,
message: "This initializer is unsafe on macOS, please use the ManulifeImageAsset.image property")
convenience init!(asset: ManulifeAssetType) {
#if os(iOS) || os(tvOS)
let bundle = Bundle(for: BundleToken.self)
self.init(named: asset.name, in: bundle, compatibleWith: nil)
#elseif os(OSX) || os(watchOS)
self.init(named: asset.name)
#endif
}
}

extension ManulifeColor {
#if swift(>=3.2)
@available(iOS 11.0, tvOS 11.0, watchOS 4.0, OSX 10.13, *)
convenience init!(asset: ManulifeColorAsset) {
let bundle = Bundle(for: BundleToken.self)
#if os(iOS) || os(tvOS)
self.init(named: asset.name, in: bundle, compatibleWith: nil)
#elseif os(OSX)
self.init(named: asset.name, bundle: bundle)
#elseif os(watchOS)
self.init(named: asset.name)
#endif
}
#endif
}

private final class BundleToken {}
