//
//  PushNotificationController.swift
//  VitalityActive
//
//  Created by sf.wenilyn.a.teorica on 25/01/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import Pushwoosh
import TTGSnackbar

class PushNotificationController {
    
    var snackbar: TTGSnackbar!
    
    var type: Int = 0
    var screen: Int = 0
    var applicationBadge: Int = 0
    var title: String = ""
    var body: String = ""
    var isInApp: Bool = false
    let delegate = UIApplication.shared.delegate as! AppDelegate
    
    class var sharedManager: PushNotificationController {
        struct Static {
            static let instance = PushNotificationController()
        }
        return Static.instance
    }
    
    // MARK: Parsing
    
    func pushNotificationParser(pushNotification: [AnyHashable : Any]!) {
        
        let customPushData = PushNotificationManager.push().getCustomPushData(asNSDict: pushNotification)
        if customPushData != nil {
            screen = customPushData!["screen"] as! Int
            screen -= 1
        }
        
        let apnPayloadData = PushNotificationManager.push().getApnPayload(pushNotification)
        if apnPayloadData != nil {
            
            if apnPayloadData!["badge"] != nil {
                applicationBadge = apnPayloadData!["badge"] as! Int
                UserDefaults.standard.set(applicationBadge, forKey: "applicationBadge")
            }
            
            if apnPayloadData!["alert"] != nil {
                if apnPayloadData!["alert"] is NSDictionary {
                    let alert = apnPayloadData!["alert"] as! NSDictionary
                    
                    if alert["title"] != nil {
                        title = alert["title"] as! String
                    }
                    
                    if alert["body"] != nil {
                        body = alert["body"] as! String
                    }
                } else {
                    body = apnPayloadData!["alert"] as! String
                }
                isInApp = false
            } else {
                if customPushData!["heading"] != nil {
                    title = customPushData!["heading"] as! String
                }
                
                if customPushData!["body"] != nil {
                    body = customPushData!["body"] as! String
                }
                isInApp = true
            }
        }
    }
    
    // MARK: Snackbar
    
    func showSnackBarOnTop() {
        if snackbar == nil {
            snackbar = TTGSnackbar()
        }
                
        snackbar.message = title + "\n" + body
        snackbar.duration =  .forever
        snackbar.actionText = "Close"
        snackbar.actionBlock = { (snackbar) in
            // Dismiss manually after 0 second
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(0 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)) {
                self.retrieveInAppNotification()
                snackbar.dismiss()
            }
        }
        snackbar.onTapBlock = { (snackbar) in
            self.goToSectionWithPushNotification()
        }
        snackbar.animationType = .slideFromTopBackToTop
        snackbar.topMargin = 50.0
        
        snackbar.show()
    }
    
    // MARK: Section
    
    func setSectionWithPushNotification() {
        if UIApplication.shared.applicationState == .active {
            if screen > 0 {
                (delegate.window?.rootViewController as? UITabBarController)?.tabBar.items?[screen].badgeValue = String(getSectionBadge())
                
                if isInApp {
                    if (delegate.window?.rootViewController as? UITabBarController)?.selectedIndex == screen {
                        showMessageForInAppNotification()
                    } else {
                        setMessageForInAppNotification()
                    }
                }
            }
        }
    }
    
    func goToSectionWithPushNotification() {
        if screen > 0 {
            (delegate.window?.rootViewController as? UITabBarController)?.selectedIndex =  screen
//            updateBadges()
        }
    }
    
    // MARK: Push Messages
    
    func storeInAppNotification() {
        
        var loadedInAppMessages: [[String: Any]] = []
        if UserDefaults.standard.object(forKey: "inAppMessages") != nil {
            loadedInAppMessages = (UserDefaults.standard.array(forKey: "inAppMessages") as? [[String: Any]])!
        }
        
        loadedInAppMessages.append(["type": isInApp, "inAppscreen": screen, "inApptitle": title, "inAppbody": body])
        UserDefaults.standard.set(loadedInAppMessages, forKey: "inAppMessages")
    }
    
    func retrieveInAppNotification() {
        if var inAppMessages = UserDefaults.standard.array(forKey: "inAppMessages") as? [[String: Any]] {
            snackbar = TTGSnackbar()

            print(inAppMessages)
            print("InApp Count:", inAppMessages.count)
            
            for index in 0..<inAppMessages.count {
                screen = inAppMessages[index]["inAppscreen"] as! Int
                title = inAppMessages[index]["inApptitle"] as! String
                body = inAppMessages[index]["inAppbody"] as! String
                
                self.showSnackBarOnTop()
                
                //Delete read message
                inAppMessages.remove(at: index)
                UserDefaults.standard.set(inAppMessages, forKey: "inAppMessages")
                break
            }
        }
    }
    
    func showMessageForInAppNotification() {
        let content = UNMutableNotificationContent()
        content.title = title
        content.body = body
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1, repeats: false)
        
//        let dateformatter = DateFormatter()
//        let notificationDate = dateformatter.string(from: NSDate() as Date)

        let request = UNNotificationRequest(identifier: "any", content: content, trigger: trigger)
        UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
        
//        updateBadges()
    }
    
    func setMessageForInAppNotification() {
        let messageDict:[String: Any] = ["screen": screen, "type": isInApp, "title": title, "body": body]
        NotificationCenter.default.post(name: Notification.Name("StoreInAppNotification"), object: nil, userInfo: messageDict)
    }
    
    // MARK: Badges
    
    func getSectionBadge() -> Int {
        var inAppBadges: [[String: Any]] = []
        var badge = 0

        if UserDefaults.standard.object(forKey: "inAppBadges") != nil {
            inAppBadges = (UserDefaults.standard.array(forKey: "inAppBadges") as? [[String: Any]])!

            print(inAppBadges)
            print("InAppBadges Count:", inAppBadges.count)
            
            let predicate = NSPredicate(format: "inAppscreen == %d", screen)
            let filteredArray = (inAppBadges as NSArray).filtered(using: predicate)
            
            if filteredArray.count != 0 {

                for index in 0..<inAppBadges.count {
                    let inAppScreen = inAppBadges[index]["inAppscreen"] as! Int
                    
                    if inAppScreen == screen {
                        badge = (inAppBadges[index]["inAppBadge"] as! Int) + 1
                        inAppBadges[index]["inAppBadge"] = badge
                        UserDefaults.standard.set(inAppBadges, forKey: "inAppBadges")
                        return badge
                    }
                }
            } else {
                badge += 1
                inAppBadges.append(["inAppscreen": screen, "inAppBadge": badge])
                UserDefaults.standard.set(inAppBadges, forKey: "inAppBadges")
            }
        } else {
            badge += 1
            inAppBadges.append(["inAppscreen": screen, "inAppBadge": badge])
            UserDefaults.standard.set(inAppBadges, forKey: "inAppBadges")
        }
        
        return badge
    }
    
    func updateBadges() {
        var inAppBadges: [[String: Any]] = []
        var badge = 0
        
        if UserDefaults.standard.object(forKey: "inAppBadges") != nil {
            inAppBadges = (UserDefaults.standard.array(forKey: "inAppBadges") as? [[String: Any]])!
            
            for index in 0..<inAppBadges.count {
                let inAppscreen = inAppBadges[index]["inAppscreen"] as! Int
                
                if inAppscreen == (delegate.window?.rootViewController as? UITabBarController)?.selectedIndex {
                    badge = (inAppBadges[index]["inAppBadge"] as! Int) - 1
                    inAppBadges[index]["inAppBadge"] = badge
                    UserDefaults.standard.set(inAppBadges, forKey: "inAppBadges")
                    
                    if badge > 0 {
                        (delegate.window?.rootViewController as? UITabBarController)?.tabBar.items?[((delegate.window?.rootViewController as? UITabBarController)?.selectedIndex)!].badgeValue = String(badge)
                    } else {
                        (delegate.window?.rootViewController as? UITabBarController)?.tabBar.items?[((delegate.window?.rootViewController as? UITabBarController)?.selectedIndex)!].badgeValue = nil
                    }
                    
                    updateApplicationBadge()
                    break
                }
            }
        } else {
            updateApplicationBadge()
        }
    }
    
    func updateApplicationBadge() {
        if UserDefaults.standard.object(forKey: "applicationBadge") != nil {
            let badge = (UserDefaults.standard.integer(forKey: "applicationBadge")) - 1
            UserDefaults.standard.set(badge, forKey: "applicationBadge")
            
            UIApplication.shared.applicationIconBadgeNumber = badge
        }
    }
}
