//
//  MLILoginTableViewCell.swift
//  Manulife
//
//  Created by Michelle R. Oratil on 16/01/2019.
//  Copyright © 2019 Glucode. All rights reserved.
//

import UIKit

class MLILoginTableViewCell: UITableViewCell {

    
    @IBOutlet weak var headerTitleLabel: UILabel!
    public var headerTitle: String?{
        set{
            self.headerTitleLabel.text = newValue
        }
        get{
            return self.headerTitleLabel.text
        }
    }
    
    @IBOutlet weak var headerDescriptionLabel: UILabel!
    public var headerDescription: String? {
        set {
            self.headerDescriptionLabel.text = newValue
        }
        get {
            return self.headerDescriptionLabel.text
        }
    }
    
    @IBOutlet weak var activateButton: UIButton!
    public var activateButtonTitle: String? {
        set {
            self.activateButton.setTitle(newValue, for: .normal)
        }
        get {
            return self.activateButton.currentTitle
        }
    }
    
    @IBOutlet weak var learnMoreButton: UIButton!
    public var alearnMoreButtonTitle: String? {
        set {
            self.learnMoreButton.setTitle(newValue, for: .normal)
        }
        get {
            return self.learnMoreButton.currentTitle
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
