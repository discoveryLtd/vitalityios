import Foundation
import HealthKit

public class VIAMeasurementFormatter: MeasurementFormatter {

    public override func string(from unit: Unit) -> String {
        var string = super.string(from: unit)
        if unit == UnitPressure.millimetersOfMercury {
            string = HKUnit.millimeterOfMercury().unitString
        }
        return string
    }

}

extension MeasurementFormatter {

    public class func decimalShortStyle() -> VIAMeasurementFormatter {
        let formatter = VIAMeasurementFormatter()
        formatter.locale = Locale.current
        formatter.numberFormatter = NumberFormatter.decimalFormatter()
        formatter.unitStyle = .short
        return formatter
    }

    public class func integerStyle() -> VIAMeasurementFormatter {
        let formatter = VIAMeasurementFormatter()
        formatter.locale = Locale.current
        formatter.numberFormatter = NumberFormatter.integerFormatter()
        formatter.unitStyle = .short
        return formatter
    }

    public class func minutesString(for value: Int, with unit: Unit) -> String {
        let minutesFormatter = VIAMeasurementFormatter()
        minutesFormatter.unitStyle = .long
        minutesFormatter.unitOptions = [.providedUnit]
        let minutesMeasurement = Measurement(value: Double(value), unit: unit)
        let formattedMinutesMeasurement = minutesFormatter.string(from: minutesMeasurement)
        return formattedMinutesMeasurement
    }
    
    public class func decimalMediumStyleForcingProvidedUnit() -> VIAMeasurementFormatter {
        let formatter = VIAMeasurementFormatter()
        formatter.locale = Locale.current
        formatter.numberFormatter = NumberFormatter.decimalFormatter()
        formatter.unitOptions = [.providedUnit]
        formatter.unitStyle = .medium
        return formatter
    }
}
