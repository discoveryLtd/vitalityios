import Foundation

extension DateFormatter {

    public class func apiManagerFormatter(localeIdentifier: String? = nil) -> DateFormatter {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZZZ'['VV']'"
        if let identifierString = localeIdentifier {
            formatter.locale = Locale(identifier: identifierString)
        }
        return formatter
    }

    public class func apiManagerFormatterWithMilliseconds(localeIdentifier: String? = nil, timezone: TimeZone? = nil) -> DateFormatter {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ'['VV']'"
        if let identifierString = localeIdentifier {
            formatter.locale = Locale(identifier: identifierString)
        }
        if let tz = timezone{
            formatter.timeZone = tz
        }
        return formatter
    }

    public class func newAPIManagerAlternativeDateTimeFormatter() -> DateFormatter {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        return formatter
    }

    public class func newAPIManagerAlternativeTwoDateTimeFormatter() -> DateFormatter {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ"
        return formatter
    }

    public class func yearMonthDayFormatter() -> DateFormatter {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        if let localeID = Bundle.main.object(forInfoDictionaryKey: "VIALocaleID") as? String{
            formatter.locale = Locale(identifier: localeID)
        }
        return formatter
    }

    public class func shortMonthAndYearFormatter() -> DateFormatter {
        let formatter = DateFormatter()
        formatter.setLocalizedDateFormatFromTemplate("MMM yyyy")
        return formatter
    }

    public class func dayMonthAndYearFormatter() -> DateFormatter {
        let formatter = DateFormatter()
        formatter.setLocalizedDateFormatFromTemplate("dd MMM yyyy")
        return formatter
    }

    public class func fullMonthAndYearFormatter() -> DateFormatter {
        let formatter = DateFormatter()
        formatter.setLocalizedDateFormatFromTemplate("MMMM yyyy")
        return formatter
    }

    public class func shortDayShortMonthFormatter() -> DateFormatter {
        let formatter = DateFormatter()
        formatter.setLocalizedDateFormatFromTemplate("d MMM")
        return formatter
    }

    public class func dayDateShortMonthFormatter() -> DateFormatter {
        let formatter = DateFormatter()
        formatter.setLocalizedDateFormatFromTemplate("E, dd MMM")
        return formatter
    }
    
    public class func dayShortMonthDateFormatter() -> DateFormatter {
        let formatter = DateFormatter()
        formatter.setLocalizedDateFormatFromTemplate("E, MMM dd")
        return formatter
    }

    public class func dayDateShortMonthyearFormatter() -> DateFormatter {
        let formatter = DateFormatter()
        formatter.setLocalizedDateFormatFromTemplate("E, dd MMM yyyy")
        return formatter
    }
    
    public class func dayDateMonthYearFormatter() -> DateFormatter {
        let formatter = DateFormatter()
        formatter.setLocalizedDateFormatFromTemplate("EEEE, dd MMM yyyy")
        return formatter
    }

	public class func dayDateLongMonthYearFormatter() -> DateFormatter {
		let formatter = DateFormatter()
		formatter.setLocalizedDateFormatFromTemplate("dd MMMM yyyy")
		return formatter
	}

    public class func dayLongMonthLongYearShortTimeFormatter() -> DateFormatter {
        let formatter = DateFormatter()
        formatter.setLocalizedDateFormatFromTemplate("dd MMMM yyyy, H:mm")
        return formatter
    }

    public class func shortTimeFormatter() -> DateFormatter {
        let formatter = DateFormatter()
        formatter.timeStyle = .short
        return formatter
    }

    public class func fullDateFormatter() -> DateFormatter {
        let formatter = DateFormatter()
        formatter.dateStyle = .full
        return formatter
    }
    
    public class func mediumDateFormatter() -> DateFormatter {
        let formatter = DateFormatter()
        formatter.setLocalizedDateFormatFromTemplate("E, dd MMM")
        return formatter
    }
    
    public class func hoursMinutesSecondsFormatter() -> DateFormatter {
        let formatter = DateFormatter()
        formatter.setLocalizedDateFormatFromTemplate("HH:mm:ss")
        return formatter
    }
}
