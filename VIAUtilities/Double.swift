import Foundation

extension Double {
    
    public func toHoursMinutesSeconds() -> (Int, Int, Int) {
        let r = Int(self.rounded())
        return (r / 3600, (r % 3600) / 60, (r % 3600) % 60)
    }
    
    public func rounded(toPlaces places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}
