import Foundation

extension UnitOfMeasureRef {

    public func unit() -> Unit {
        switch self {
        case .BMI:
            return Unit(symbol: "")
        case .Second:
            return UnitDuration.seconds
        case .KilometersPerHour:
            return UnitSpeed.kilometersPerHour
        case .Gram:
            return UnitMass.grams
        case .Kilogram:
            return UnitMass.kilograms
        case .Meter:
            return UnitLength.meters
        case .Kilometer:
            return UnitLength.kilometers
        case .Inch:
            return UnitLength.inches
        case .Foot:
            return UnitLength.feet
        case .Mile:
            return UnitLength.miles
        case .Ounce:
            return UnitMass.ounces
        case .Pound:
            return UnitMass.pounds
        case .Ton:
            return UnitMass.metricTons
        case .Centimeter:
            return UnitLength.centimeters
        case .MillimolesPerLitre:
            return UnitConcentrationMass.millimolesPerLiter(withGramsPerMole: 0.0) // we don't convert, so this value doesn't really matter
        case .MillimeterOfMercury,
             .Systolicmillimeterofmercury:
            return UnitPressure.millimetersOfMercury
        case .TotalCholestrolMillimolesPerLitre,
             .TriglycerideMillimolesPerLitre,
             .FastingGlucoseMillimolesPerLitre,
             .LDLCholestrolMillimolesPerLitre,
             .HDLCholestrolMillimolesPerLitre,
             .RandomGlucoseMillimolesPerLitre:
            return UnitConcentrationMass.millimolesPerLiter(withGramsPerMole: 0.0) // we don't convert, so this value doesn't really matter
        case .Percentage:
            return Unit(symbol: "%")
        case .SystolicKiloPascal:
            return UnitPressure.kilopascals
        case .Stone:
            return UnitMass.stones
        case .StonePound:
            return Unit(symbol: "st lb")
        case .FootInch:
            return Unit(symbol: "ft in")
        case .TotalCholestrolMilligramsPerDeciLitre,
             .TriglycerideMilligramsPerDeciLitre,
             .FastingGlucoseMilligramsPerDeciLitre,
             .LDLCholestrolMilligramsPerDeciLitre,
             .HDLCholestrolMilligramsPerDeciLitre,
             .RandomGlucoseMilligramsPerDeciLitre:
            //return UnitConcentrationMass.milligramsPerDeciliter
            return UnitMilligramsPerDeciLitre(symbol: "mg/dL")
        case .PerDay:
            return UnitPerDay(symbol: "Per day")
        case .PerWeek:
            return UnitPerWeek(symbol: "Per week")
        case .Minutes:
            return UnitDuration.minutes
        case .Hours:
            return UnitDuration.hours
        case .Days:
            return UnitDays(symbol: "Days")
        case .Kilocalories:
            return UnitEnergy.kilocalories
        case .KiloCaloriesPerHour:
            let symbol = "\(UnitEnergy.kilocalories.symbol)/\(UnitDuration.hours.symbol)"
            return UnitKilocaloriesPerHour(symbol: symbol)
        case .BeatsPerMinute:
            return UnitBeatsPerMinute(symbol: "bpm")
        case .MetersPerSecond:
            return UnitSpeed.metersPerSecond
        case .MinutesPerKilometer:
            let symbol = "\(UnitEnergy.kilocalories.symbol)/\(UnitDuration.hours.symbol)"
            return UnitMinutesPerKilometer(symbol: symbol)
        case .Unknown:
            return Unit(symbol: "")
        }
    }

}

public class UnitPerDay: Unit { }

public class UnitPerWeek: Unit { }

public class UnitDays: Unit { }

public class UnitBeatsPerMinute: Unit { }

public class UnitMinutesPerKilometer: Unit { }

public class UnitKilocaloriesPerHour: Unit { }

public class UnitMilligramsPerDeciLitre: Unit { }
