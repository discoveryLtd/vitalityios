import Foundation

public class VIAUnits: Dimension {
    
    // TODO: this should likely be moved to the other Unit extensions in VitalityKit. Also, typo
    public class func dimention(with string: String) -> Dimension {
        return VIAUnits(symbol: string, converter: UnitConverterLinear(coefficient: 1))
    }

    public static let percentage = VIAUnits(symbol: "%", converter: UnitConverterLinear(coefficient: 1))
}
