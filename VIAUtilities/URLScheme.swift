import Foundation

public struct VIAURLScheme {

    public enum Feature: String {
        case wellnessDevicesApps = "wellnessDevicesApps"
    }

    public static func urlScheme(for feature: Feature) -> URL? {
        return defaultURLScheme(path: feature.rawValue)
    }

    public static func defaultURLScheme(path: String? = nil) -> URL? {
        guard let bundleIdentifier = Bundle.main.object(forInfoDictionaryKey: "CFBundleIdentifier") as? String else {
            return nil
        }

        var urlString = "\(bundleIdentifier)://"
        if let path = path {
            urlString = urlString + path
        }
        let url = URL(string: urlString)
        return url
    }

}
