#import <Foundation/Foundation.h>

@interface GoogleTagManagerPatch : NSObject

+ (void)patchGoogleTagManagerLogging;

@end
