//
//  String.swift
//  VitalityActive
//
//  Created by Wilmar van Heerden on 2017/02/09.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation

extension String {

    public var isAlphanumeric: Bool {
        let unwantedCharacters = NSCharacterSet.alphanumerics.inverted
        return self.rangeOfCharacter(from: unwantedCharacters) == nil
    }

    public var isNumeric: Bool {
        let chars: Array<Character> = Array(self.characters)
        for char: Character in chars {
            if !("0"..."9" ~= char) {
                return false
            }
        }
        return true
    }

    public func isValidPassword() -> Bool {

        let upperCaseCount = self.components(separatedBy: NSCharacterSet.uppercaseLetters).count - 1
        let lowerCaseCount = self.components(separatedBy: NSCharacterSet.lowercaseLetters).count - 1
        let numbersCount = self.components(separatedBy: NSCharacterSet.decimalDigits).count - 1

        return self.characters.count >= 7 && upperCaseCount > 0 && lowerCaseCount > 0 && numbersCount > 0
    }

    public func isValidEmail() -> Bool {
        /*
            The format of email addresses is local-part@domain where the local part may be up to 64 characters long 
            and the domain may have a maximum of 255 characters—but the maximum of 256-character length of a forward 
            or reverse path restricts the entire email address to be no more than 254 characters long.
        */
        //let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}" // more accurate regex

        //let emailRegex = "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])" // original
        //let emailRegex = "^(?=.{2,64}@)(?:[a-zA-Z0-9,!#$%&*+\\/=?^_`{|}~-]+)(?:\\.[a-zA-Z0-9,!#$%&*+\\/=?^_`{|}~-]{1,63})*(?=@.{5,254}$)@(?:[a-zA-Z0-9-]{1,63}\\.){1,125}[a-zA-Z0-9-]{2,63}$" // new one 14 Feb 2017
        //let emailRegex =   "^(?=.{2,64}@)(?:[a-zA-Z0-9,!#$%&*+;\\/=?^_`{|}~-]+)(?:\\.[a-zA-Z0-9,!#$%&*+\\/=?^_`{|}~-]{1,63})*(?=@.{5,254}$)@(?:[a-zA-Z0-9-]{1,63}\\.){1,125}[a-zA-Z0-9-]{2,63}$" // 29 June 2017
        //let emailRegex = "(?=^.{8,254}$)(?=^[\\w-+=)(*&^%$#!`~/?;|{}\\s\".\\[\\]:]{2,64}@)(?=.*@.{5,254}$)(?!^[.])(?!(?=.*\\.@.*))(?=^(([\\w-+=)(*&^%$#!`~/?;|{}]|\"[\\w-+=)(*&^%$#!`~/?;|{}\\s.]+\")\\.?)*@((([\\w-+=)(*&^%$#!`~/?;|{}]{1,63}\\.){1,125}[\\w-+=)(*&^%$#!`~/?;|{}]{2,63})|(\\[((\\d{1,3}\\.){3}\\d{1,3})\\])|(\\[(\\w*:){4}\\w*\\]))$)(?!(?=.*[\\w-+=)(*&^%$#!`~/?;|{}\"]+\"[\\w-+=)(*&^%$#!`~/?;|{}]+\".*@))(?!(?=.*\"[\\w-+=)(*&^%$#!`~/?;|{}]+\"[\\w-+=)(*&^%$#!`~/?;|{}\"]+.*@))(?!(?=.*@-.*$))(?!(?=.*@.*-\\..*$))(?!(?=.*@.*\\.-.*$))(?!(?=.*-$)).+" // 17 July 2017
        //let emailRegex = "(?=^.{8,254}$)(?=^[\\w-+=)(*&^%$#!`~/?;|{}\\s\".\\[\\]:]{2,64}@)(?=.*@.{5,254}$)(?!^[.])(?!(?=.*\\.@.*))(?=^(([\\w-+=)(*&^%$#!`~/?;|{}]|\"[\\w-+=)(*&^%$#!`~/?;|{}\\s.]+\")\\.?)*@((([\\w-+=)(*&^%$#!`~/?;|{}]{1,63}\\.){1,125}[\\w-+=)(*&^%$#!`~/?;|{}]{2,63})|(\\[((\\d{1,3}\\.){3}\\d{1,3})\\])|(\\[(\\w*:){4}\\w*\\]))$)(?!(?=.*[\\w-+=)(*&^%$#!`~/?;|{}\"]+\"[\\w-+=)(*&^%$#!`~/?;|{}]+\".*@))(?!(?=.*\"[\\w-+=)(*&^%$#!`~/?;|{}]+\"[\\w-+=)(*&^%$#!`~/?;|{}\"]+.*@))(?!(?=.*@-.*$))(?!(?=.*@.*-\\..*$))(?!(?=.*@.*\\.-.*$))(?!(?=.*-$))" // 24 July 2017 from Brett
        let emailRegex =   "(?=^.{8,254}$)(?=^[\\w-+=)(*&^%$#!`~/?;|{}\\s\".\\[\\]:]{2,64}@)(?=.*@.{5,254}$)(?!^[.])(?!(?=.*\\.@.*))(?=^(([\\w-+=)(*&^%$#!`~/?;|{}]|\"[\\w-+=)(*&^%$#!`~/?;|{}\\s.]+\")\\.?)*@((([\\w-+=)(*&^%$#!`~/?;|{}]{1,63}\\.){1,125}[\\w-+=)(*&^%$#!`~/?;|{}]{2,63})|(\\[((\\d{1,3}\\.){3}\\d{1,3})\\])|(\\[(\\w*:){4}\\w*\\]))$)(?!(?=.*[\\w-+=)(*&^%$#!`~/?;|{}\"]+\"[\\w-+=)(*&^%$#!`~/?;|{}]+\".*@))(?!(?=.*\"[\\w-+=)(*&^%$#!`~/?;|{}]+\"[\\w-+=)(*&^%$#!`~/?;|{}\"]+.*@))(?!(?=.*@-.*$))(?!(?=.*@.*-\\..*$))(?!(?=.*@.*\\.-.*$))(?!(?=.*-$)).+" // 17 July 2017 10:09 AM from Brett
        let formatResult = NSPredicate(format: "SELF MATCHES %@", emailRegex).evaluate(with: self)

        let lengthResult = self.characters.count >= 7
        return formatResult && lengthResult
    }
    
    public func isURLValid() -> Bool {
        // Taken from: http://urlregex.com/
        // let urlRegex = "((?:http|https)://)?(?:www\\.)?[\\w\\d\\-_]+\\.\\w{2,3}(\\.\\w{2})?(/(?<=/)(?:[\\w\\d\\-./_]+)?)?"
        //
        // // Custom regex to allow simple URL formats.
        // let urlSimpleRegex = "((?:http|https)://)?((\\w|-)+)(([.]|[/])((\\w|-)+))+"
        // let formatResult = NSPredicate(format: "SELF MATCHES %@", urlSimpleRegex).evaluate(with: self)
        //
        // return formatResult
        
        // Make use of native URL checker
        guard let detector = try? NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue) else { return false }
        if let match = detector.firstMatch(in: self, options: [], range: NSRange(location: 0, length: self.endIndex.encodedOffset)) {
            return match.range.length == self.endIndex.encodedOffset
        } else {
            return false
        }
    }

    public func fromBase64() -> String? {
        guard let data = Data(base64Encoded: self) else {
            return nil
        }

        return String(data: data, encoding: .utf8)
    }

    public func toBase64() -> String {
        return Data(self.utf8).base64EncodedString()
    }

    public static func isNilOrEmpty(string: String?) -> Bool {
        return (string ?? "").isEmpty
    }

    public func nsRange(of substring: String) -> NSRange {
        if let range = self.range(of: substring) {
            let nsRange = self.nsRange(from: range)
            return nsRange
        }
        return NSRange()
    }

    public func nsRange(from range: Range<String.Index>) -> NSRange {
        let from = range.lowerBound.samePosition(in: utf16)
        let to = range.upperBound.samePosition(in: utf16)
        return NSRange(location: utf16.distance(from: utf16.startIndex, to: from),
                       length: utf16.distance(from: from, to: to))
    }

    public func range(from nsRange: NSRange) -> Range<String.Index>? {
        guard
            let from16 = utf16.index(utf16.startIndex, offsetBy: nsRange.location, limitedBy: utf16.endIndex),
            let to16 = utf16.index(utf16.startIndex, offsetBy: nsRange.location + nsRange.length, limitedBy: utf16.endIndex),
            let from = from16.samePosition(in: self),
            let to = to16.samePosition(in: self)
            else { return nil }
        return from ..< to
    }
    
    public func currencyInputFormatting() -> String {
        
        var number: NSNumber!
        let formatter = NumberFormatter()
        formatter.maximumFractionDigits = 2
        formatter.minimumFractionDigits = 2
        formatter.numberStyle = .currencyAccounting
        
        var amountWithPrefix = self
        
        // remove from String: "$", ".", ","
        let regex = try! NSRegularExpression(pattern: "[^0-9]", options: .caseInsensitive)
        amountWithPrefix = regex.stringByReplacingMatches(in: amountWithPrefix, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSMakeRange(0, self.characters.count), withTemplate: "")
        
        let double = (amountWithPrefix as NSString).doubleValue
        number = NSNumber(value: (double / 100))
        
        // if first number is 0 or all numbers were deleted
        guard number != 0 as NSNumber else {
            return ""
        }
        
        return formatter.string(from: number)!
    }
    
    public func toCurrencyISOFormat() -> String {
        
        var number: NSNumber!
        
        let currencyFormatter = NumberFormatter()
        currencyFormatter.usesGroupingSeparator = true
        currencyFormatter.numberStyle = .currencyISOCode
        // localize to your grouping and decimal separator
        currencyFormatter.locale = Locale.current
        
        let amountWithPrefix = self
        
        let double = (amountWithPrefix as NSString).doubleValue
        number = NSNumber(value: double)
        
        return currencyFormatter.string(from: number)!
    }
    
    public func hasDoubleByte() -> Bool{
        return self.utf8.count != self.characters.count
    }
    
    public func stringContainsSpecialCharacters() -> Bool{
        
        let regex = try! NSRegularExpression(pattern: ".*[^A-Za-z0-9].*", options: NSRegularExpression.Options())
        return regex.firstMatch(in: self, options: NSRegularExpression.MatchingOptions(),
                                range:NSMakeRange(0, self.characters.count)) != nil
    }

}
