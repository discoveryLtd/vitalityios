public class UIStoryboardSegueWithCompletion: UIStoryboardSegue {

    public var completion: (() -> Void)?

    override public func perform() {
        super.perform()

        if let completion = completion {
            destination.transitionCoordinator?.animate(alongsideTransition: nil, completion: { _ in
                completion()
            })
        }
    }
}
