import Foundation

public protocol CaptureMetric {
    static func configureUnits(for unitsOfMeasure: [UnitOfMeasureRef]) -> [Unit]
    var units: [Unit] { get set }
}

extension CaptureMetric {

    public static func configureUnits(for unitsOfMeasure: [UnitOfMeasureRef]) -> [Unit] {
        var result = unitsOfMeasure.flatMap({ unitOfMeasure -> Unit? in
            let unit = unitOfMeasure.unit()
            if unit.symbol == "" { // don't send through empty units, it creates an empty picker arrow
                return nil
            }
            return unit
        })
        result = result.filterDuplicates { $0.symbol == $1.symbol }
        return result
    }

}
