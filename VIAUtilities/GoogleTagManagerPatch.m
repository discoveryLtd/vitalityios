#import "GoogleTagManagerPatch.h"
#import <objc/runtime.h>

@implementation GoogleTagManagerPatch

+ (void)patchGoogleTagManagerLogging
{
    Class class = NSClassFromString(@"TAGLogger");
    
    SEL originalSelector = NSSelectorFromString(@"info:");
    SEL detourSelector = @selector(detour_info:);
    
    Method originalMethod = class_getClassMethod(class, originalSelector);
    Method detourMethod = class_getClassMethod([self class], detourSelector);
    
    class_addMethod(class,
                    detourSelector,
                    method_getImplementation(detourMethod),
                    method_getTypeEncoding(detourMethod));
    
    method_exchangeImplementations(originalMethod, detourMethod);
}


+ (void)detour_info:(NSString*)message
{
    return; // Disable logging
}

@end
