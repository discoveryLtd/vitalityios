//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum PurchasePartyRoleTypeRef: Int, Hashable { 
    case Unknown = -1
    case Purchaser = 1 // name:Purchaser note: The party that purchases the product or service 
    case Supplier = 2 // name:Supplier note:The party that supplies the product or service 

    public static let allValues: [PurchasePartyRoleTypeRef] = [PurchasePartyRoleTypeRef.Unknown, PurchasePartyRoleTypeRef.Purchaser, PurchasePartyRoleTypeRef.Supplier]

}
