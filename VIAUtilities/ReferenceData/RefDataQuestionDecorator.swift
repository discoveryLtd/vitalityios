//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum QuestionDecoratorRef: Int, Hashable { 
    case Unknown = -1
    case Checkbox = 2 // name:Checkbox note:Checkbox 
    case CollapseUoM = 9 // name:CollapseUoM note:CollapseUoM 
    case DatePicker = 10 // name:DatePicker note:DatePicker 
    case Dropdown = 4 // name:Dropdown note:Dropdown 
    case Icons = 6 // name:Icons note:Icons 
    case Label = 11 // name:Label note:Label 
    case RadioButton = 3 // name:RadioButton note:RadioButton 
    case Slider = 5 // name:Slider note:Slider 
    case Textbox = 1 // name:Textbox note:Textbox 
    case Toggle = 7 // name:Toggle note:Toggle 
    case YesNo = 8 // name:YesNo note:YesNo 

    public static let allValues: [QuestionDecoratorRef] = [QuestionDecoratorRef.Unknown, QuestionDecoratorRef.Checkbox, QuestionDecoratorRef.CollapseUoM, QuestionDecoratorRef.DatePicker, QuestionDecoratorRef.Dropdown, QuestionDecoratorRef.Icons, QuestionDecoratorRef.Label, QuestionDecoratorRef.RadioButton, QuestionDecoratorRef.Slider, QuestionDecoratorRef.Textbox, QuestionDecoratorRef.Toggle, QuestionDecoratorRef.YesNo]

}
