//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum AwardedRewardStatusRef: Int, Hashable { 
    case Unknown = -1
    case Acknowledged = 5 // name:Acknowledged note:Reward has been acknowledged by Member (Member has seen what reward he/she will get) 
    case Allocated = 2 // name:Allocated note:Allocated to member (associated with choice or probabilistic reward) 
    case AvailableToRedeem = 7 // name:Available To Redeem note:Available To Redeem 
    case Canceled = 3 // name:Canceled note:Reward has been canceled 
    case Expired = 6 // name:Expired note:Reward has expired and is no longer valid 
    case IssueFailed = 9 // name:Issue Failed note:Issue of the reward has failed 
    case Issued = 1 // name:Issued note:Issued to member (associated with pre-determined reward) 
    case PartnerRegistration = 8 // name:Partner Registration note:Partner Registration 
    case Used = 4 // name:Used note:Reward has been used by member (e.g. Voucher redeemed) 

    public static let allValues: [AwardedRewardStatusRef] = [AwardedRewardStatusRef.Unknown, AwardedRewardStatusRef.Acknowledged, AwardedRewardStatusRef.Allocated, AwardedRewardStatusRef.AvailableToRedeem, AwardedRewardStatusRef.Canceled, AwardedRewardStatusRef.Expired, AwardedRewardStatusRef.IssueFailed, AwardedRewardStatusRef.Issued, AwardedRewardStatusRef.PartnerRegistration, AwardedRewardStatusRef.Used]

}
