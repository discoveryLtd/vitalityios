//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum ResourceTranslationStatusRef: Int, Hashable { 
    case Unknown = -1
    case A = 1 // name:Approved note:Approved Status 
    case D = 2 // name:Declined note:Declined Status 
    case P = 0 // name:Proposed note:Proposed Status 

    public static let allValues: [ResourceTranslationStatusRef] = [ResourceTranslationStatusRef.Unknown, ResourceTranslationStatusRef.A, ResourceTranslationStatusRef.D, ResourceTranslationStatusRef.P]

}
