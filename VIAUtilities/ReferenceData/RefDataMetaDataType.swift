//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum MetaDataTypeRef: Int, Hashable { 
    case Unknown = -1
    case Boolean = 2 // name:Boolean note:undefined 
    case Date = 5 // name:Date note:undefined 
    case DateTime = 6 // name:DateTime note:undefined 
    case Double = 4 // name:Double note:undefined 
    case Long = 1 // name:Long note:undefined 
    case Money = 7 // name:Money note:undefined 
    case String = 3 // name:String note:undefined 

    public static let allValues: [MetaDataTypeRef] = [MetaDataTypeRef.Unknown, MetaDataTypeRef.Boolean, MetaDataTypeRef.Date, MetaDataTypeRef.DateTime, MetaDataTypeRef.Double, MetaDataTypeRef.Long, MetaDataTypeRef.Money, MetaDataTypeRef.String]

}
