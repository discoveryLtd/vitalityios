//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum ConversionTypeRef: Int, Hashable { 
    case Unknown = -1
    case Complexformula = 2 // name:Complex formula note:Complex formula 
    case Formula = 3 // name:Formula note:Formula 
    case Simpleoperator = 1 // name:Simple operator note:Simple operator 

    public static let allValues: [ConversionTypeRef] = [ConversionTypeRef.Unknown, ConversionTypeRef.Complexformula, ConversionTypeRef.Formula, ConversionTypeRef.Simpleoperator]

}
