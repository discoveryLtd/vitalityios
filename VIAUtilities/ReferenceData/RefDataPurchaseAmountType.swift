//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum PurchaseAmountTypeRef: Int, Hashable { 
    case Unknown = -1
    case DiscountAmount = 9 // name:DiscountAmount note:The discount amount that the member will get for the item 
    case NetAmount = 12 // name:NetAmount note:Amount the member pays 
    case RetailAmount = 8 // name:RetailAmount  note:The Price of an item purchased before any Vitality discounts 
    case RewardQualifying = 11 // name:RewardQualifyingAmount  note:The amount that the discount is calculated on 
    case Tax = 10 // name:Tax  note:Tax amount for the item 
    case TotalDiscountAmount = 2 // name:TotalDiscountAmount  note:The total discount amount that the member will get for the entire purchase 
    case TotalDiscountReturn = 6 // name:TotalDiscountReturned  note:Total discount amount of returned items. Calculated 
    case TotalNetAmount = 7 // name:TotalNetAmount note:Amount that the member pays 
    case TotalRetailAmount = 1 // name:TotalRetailAmount  note:The total Price of items purchased before any Vitality discounts 
    case TotalRetailReturned = 5 // name:TotalRetailReturned  note:Total retail amount of returned items. Calculated 
    case TotalRewardQualify = 4 // name:TotalRewardQualifyingAmount  note:The total amount that the discount is calculated on 
    case TotalTaxAmount = 3 // name:TotalTaxAmount  note:Total taxes for the purchase 

    public static let allValues: [PurchaseAmountTypeRef] = [PurchaseAmountTypeRef.Unknown, PurchaseAmountTypeRef.DiscountAmount, PurchaseAmountTypeRef.NetAmount, PurchaseAmountTypeRef.RetailAmount, PurchaseAmountTypeRef.RewardQualifying, PurchaseAmountTypeRef.Tax, PurchaseAmountTypeRef.TotalDiscountAmount, PurchaseAmountTypeRef.TotalDiscountReturn, PurchaseAmountTypeRef.TotalNetAmount, PurchaseAmountTypeRef.TotalRetailAmount, PurchaseAmountTypeRef.TotalRetailReturned, PurchaseAmountTypeRef.TotalRewardQualify, PurchaseAmountTypeRef.TotalTaxAmount]

}
