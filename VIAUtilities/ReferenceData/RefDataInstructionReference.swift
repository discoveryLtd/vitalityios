//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum InstructionReferenceRef: Int, Hashable { 
    case Unknown = -1
    case Activate = 1 // name:Activate note:Activate -  Gym maintenance 
    case ActivateNew = 21 // name:Activate New note:Activate New  - Used to activate a members Vitality membership. 
    case ActivateExisiting = 22 // name:Activate Exisiting note:Activate Exisiting - If a party already exists for the person for which a Vitality membership activation is requested. Vitality can re-use the existing party and create a new membership for that party where the party have membership in the past 
    case ActivateMembership = 10 // name:Activate Membership note:Activate Membership - Vitality membership 
    case AddPurchaseItem = 6 // name:Add Purchase Item note:Add Purchase Item - Purchase 
    case Cancel = 4 // name:Cancel note:Cancel - Purchase,Gym maintenance,Vitality membership 
    case CancelInception = 25 // name:Cancel Inception note:Cancel Inception - Used to end date a members Vitality Membership to the start date of the membership. 
    case CancelPurchaseItem = 8 // name:Cancel Purchase Item note:Cancel Purchase Item - Purchase 
    case Capture = 2 // name:Capture note:Capture - Purchase 
    case Create = 30 // name:Create note:Create - Used to update a party details. An example of party update would be updates to the Party?s name. 
    case CreateRole = 29 // name:Create Role note:Create Role - Used to create a new membership role 
    case DeleteRole = 28 // name:Delete Role note:Delete Role - Used to delete the membership role 
    case Extend = 27 // name:Extend note:Extend - Used to extend a members Vitality membership beyond the current date. 
    case MaintainMembership = 12 // name:Maintain Membership note:Maintain Membership - Vitality membership 
    case MaintainParty = 20 // name:Maintain Party note:Maintain Party - Vitality membership 
    case Reinstate = 14 // name:Reinstate note:Reinstate - Gym maintenance,Vitality membership 
    case Return = 5 // name:Return note:Return -  Purchase 
    case ReturnPurchaseItem = 9 // name:Return PurchaseItem note:Return PurchaseItem - Purchase 
    case Suspend = 11 // name:Suspend note:Suspend - Gym Maintenace 
    case Unsuspend = 13 // name:Unsuspend note:Unsuspend - Gym maintenance 
    case Update = 3 // name:Update note:Update - Purchase,Gym maintenance,Vitality membership 
    case UpdateProduct = 26 // name:Update Product note:Update Product - Used to update the membership product 
    case UpdatePurchaseItem = 7 // name:Update Purchase Item note:Update Purchase Item - Purchase 
    case UpdateRole = 23 // name:Update Role note:Update Role - Updates the membership party role 
    case UpdateStartDate = 24 // name:Update Start Date note:Update Start Date - Used to update the Vitality Membership details. 

    public static let allValues: [InstructionReferenceRef] = [InstructionReferenceRef.Unknown, InstructionReferenceRef.Activate, InstructionReferenceRef.ActivateNew, InstructionReferenceRef.ActivateExisiting, InstructionReferenceRef.ActivateMembership, InstructionReferenceRef.AddPurchaseItem, InstructionReferenceRef.Cancel, InstructionReferenceRef.CancelInception, InstructionReferenceRef.CancelPurchaseItem, InstructionReferenceRef.Capture, InstructionReferenceRef.Create, InstructionReferenceRef.CreateRole, InstructionReferenceRef.DeleteRole, InstructionReferenceRef.Extend, InstructionReferenceRef.MaintainMembership, InstructionReferenceRef.MaintainParty, InstructionReferenceRef.Reinstate, InstructionReferenceRef.Return, InstructionReferenceRef.ReturnPurchaseItem, InstructionReferenceRef.Suspend, InstructionReferenceRef.Unsuspend, InstructionReferenceRef.Update, InstructionReferenceRef.UpdateProduct, InstructionReferenceRef.UpdatePurchaseItem, InstructionReferenceRef.UpdateRole, InstructionReferenceRef.UpdateStartDate]

}
