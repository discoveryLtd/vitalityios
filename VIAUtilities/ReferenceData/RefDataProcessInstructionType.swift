//  Vitality Active
//
//  Created by Swagger Codegen on 12/07/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum ProcessInstructionTypeRef: Int, Hashable { 
    case Unknown = -1
    case ARAmazonDS = 3 // name:Show Active Rewards Amazon Data Sharing consent note:Show Active Rewards Amazon Data Sharing consent 
    case ARCineworldDS = 5 // name:Show Active Rewards Cineworld Data Sharing consent note:Show Active Rewards Cineworld Data Sharing consent 
    case ARStarbucksDS = 2 // name:Show Active Rewards Starbucks Data Sharing consent note:Show Active Rewards Starbucks Data Sharing consent 
    case ARVueDS = 7 // name:Show Active Rewards Cineworld Data Sharing consent note:Show Active Rewards Cineworld Data Sharing consent 
    case ARZapposDS = 4 // name:Show Active Rewards Zappos Data Sharing consent note:Show Active Rewards Zappos Data Sharing consent 
    case LOGIN_T_AND_C = 1 // name:LOGIN_TERMS_AND_CONDITIONS note:null 
    case ScreenAndVaccDS = 6 // name:Screenings and vaccinations Data Sharing consent note:Show screenings and vaccinations Data Sharing consent 
    case NEXT_STATUS_ACHIEVED = 98 // TODO: Manually added. Replace with correct raw value
    case SHOW_CARRY_OVER_STATUS = 99 // TODO: Manually added. Replace with correct raw value

    public static let allValues: [ProcessInstructionTypeRef] = [ProcessInstructionTypeRef.Unknown, ProcessInstructionTypeRef.ARAmazonDS, ProcessInstructionTypeRef.ARCineworldDS, ProcessInstructionTypeRef.ARStarbucksDS, ProcessInstructionTypeRef.ARVueDS, ProcessInstructionTypeRef.ARZapposDS, ProcessInstructionTypeRef.LOGIN_T_AND_C, ProcessInstructionTypeRef.ScreenAndVaccDS]

}
