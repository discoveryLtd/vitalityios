//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum ObjectiveRef: Int, Hashable { 
    case Unknown = -1
    case ActiveRewardsFitness = 1 // name:Earn active rewards fitness points note:Earn active rewards fitness points 
    case AppleWatchT1 = 2 // name:Apple Watch Tier 1 objective note:Apple Watch Tier 1 objective 
    case AppleWatchT2 = 3 // name:Apple Watch Tier 2 objective note:Apple Watch Tier 2 objective 
    case AppleWatchT3 = 4 // name:Apple Watch Tier 3 objective note:Apple Watch Tier 3 objective 
    case AppleWatchT4 = 5 // name:Apple Watch Tier 4 objective note:Apple Watch Tier 4 objective 

    public static let allValues: [ObjectiveRef] = [ObjectiveRef.Unknown, ObjectiveRef.ActiveRewardsFitness, ObjectiveRef.AppleWatchT1, ObjectiveRef.AppleWatchT2, ObjectiveRef.AppleWatchT3, ObjectiveRef.AppleWatchT4]

}
