//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum PartySegmentRef: Int, Hashable { 
    case Unknown = -1
    case AdidasHGAmt = 42 // name:Amount limit reached for AdidasHG note:Amount limit reached for AdidasHG 
    case AdidasHgLimits = 24 // name:AdidasHgLimits note:Amount Limit Reached for Adidas Heath Gear 
    case BMI = 48 // name:BMI Tested note:BMI Tested 
    case BloodPressure = 49 // name:Blood Pressure Tested note:Blood Pressure Tested 
    case BronzeStatus = 14 // name:Bronze Vitality Status note:Bronze Vitality status has been achieved 
    case DeviceAbcAmtLimits = 11 // name:DeviceAbcAmtLimit note:Amount limits reached for DeviceAbc 
    case DeviceAbcLimits = 10 // name:DeviceAbcLimit note:Limits reached for DeviceAbc 
    case DeviceXyzLimits = 13 // name:DeviceXyzLimits note:Limits reached for product DeviceXyz 
    case FFNational = 41 // name:Fitness First National Active note:Member has an active benefit for Fitness First National 
    case FFRegional = 40 // name:Fitness First Regional Active note:Member has an active benefit for Fitness First Regional 
    case FastingGlucose = 44 // name:Fasting Glucose note:Fasting Glucose Tested 
    case FitnessFirstGM = 39 // name:Fitness First Gym Active note:Member has an active partner membership for Fitness First Gym 
    case GarminDPLimit = 43 // name:Count limit reached for GarminDP note:Count limit reached for GarminDP 
    case GarminPolarShrdUtil = 23 // name:GarminPolarShrdUtil note:Count shared limit reached for Garmin and Polar DP 
    case GoldStatus = 8 // name:GoldStatus note:Gold Vitality status achieved 
    case GymAbcBrnEff = 1 // name:GymAbcBranchEffective note:Gym Abc branch gym contract is effective (Active) 
    case GymAbcEffective = 4 // name:GymAbcEffective note:Gym Abc is effective (Active Status) 
    case GymAbcNationalEff = 2 // name:GymAbcNationalEffective note:Gym Abc national gym contract is effective (Active) 
    case Hba1c = 47 // name:Hba1c tested note:Hba1c Tested 
    case KonamiGB = 29 // name:Konami Gym Benefit Active note:Konami Gym Benefit Active 
    case KonamiGBCAT1 = 30 // name:Category 1 Active note:Category 1 Active 
    case KonamiGBCAT3 = 32 // name:Category 3 Active note:Category 3 Active 
    case KonamiGBCAT4 = 33 // name:Category 4 Active note:Category 4 Active 
    case LdlCholesterol = 45 // name:LDL Cholesterol note:LDL Cholesterol tested 
    case NHGRollingBenefit = 37 // name:Nuffield Health Gym Rolling Monthly Benefit Active note:Member has an active benefit for Nuffield Health Gym Rolling Monthly Benefit 
    case NuffieldHealthGym = 38 // name:Nuffield Health Gym Active note:Member has an active partner membership for Nuffield Health Gym 
    case OisixHFAmt = 22 // name:OisixHFAmt note:Amount Limit reached for OisixHF 
    case PlatinumStatus = 9 // name:PlatinumStatus note:Platinum Vitality Status achieved 
    case PolarDPCnt = 21 // name:PolarDPCnt note:Count Limit reached for PolarDP 
    case RENAISSANCEGB = 35 // name:Renaissance Gym Japan 12 Months note:Renaissance Gym Japan 12 Months 
    case RENAISSANCEGBM = 36 // name:Renaissance Gym Japan 13 Months Plus Active note:Renaissance Gym Japan 13 Months Plus Active 
    case RENAISSANCEGM = 34 // name:Renaissance Gym Benefit note:Renaissance Gym Benefit 
    case SilverStatus = 7 // name:SilverStatus note:Silver Vitality status achieved 
    case TestProduct1Limits = 15 // name:TestProduct1Limits note:Limits reached for test product 1 
    case TestProduct2Limits = 16 // name:TestProduct2Limits note:Limits reached for test product 2 
    case TestProduct3Limits = 17 // name:TestProduct3Limits note:Limits reached for test product 3 
    case TestProduct4Limits = 18 // name:TestProduct4Limits note:Limits reached for test product 4 
    case TestProduct5Limits = 19 // name:TestProduct5Limits note:Limits reached for test product 5 
    case TestProduct6Limits = 20 // name:TestProduct6Limits note:Limits reached for test product 6 
    case TestSharedLimits = 12 // name:TestSharedLimits note:TestSharedLimits 
    case UrinaryTest = 46 // name:Urinary Test note:Urinary Test Completed 
    case VHCCompleted = 5 // name:VHCCompleted note:VHC assessment completed 
    case VHRCompleted = 6 // name:VHRCompleted note:VHR assessment completed 
    case VitalityMemEff = 3 // name:VitalityMembershipEffective note:Vitality Membership is effective (Active status) 

    public static let allValues: [PartySegmentRef] = [PartySegmentRef.Unknown, PartySegmentRef.AdidasHGAmt, PartySegmentRef.AdidasHgLimits, PartySegmentRef.BMI, PartySegmentRef.BloodPressure, PartySegmentRef.BronzeStatus, PartySegmentRef.DeviceAbcAmtLimits, PartySegmentRef.DeviceAbcLimits, PartySegmentRef.DeviceXyzLimits, PartySegmentRef.FFNational, PartySegmentRef.FFRegional, PartySegmentRef.FastingGlucose, PartySegmentRef.FitnessFirstGM, PartySegmentRef.GarminDPLimit, PartySegmentRef.GarminPolarShrdUtil, PartySegmentRef.GoldStatus, PartySegmentRef.GymAbcBrnEff, PartySegmentRef.GymAbcEffective, PartySegmentRef.GymAbcNationalEff, PartySegmentRef.Hba1c, PartySegmentRef.KonamiGB, PartySegmentRef.KonamiGBCAT1, PartySegmentRef.KonamiGBCAT3, PartySegmentRef.KonamiGBCAT4, PartySegmentRef.LdlCholesterol, PartySegmentRef.NHGRollingBenefit, PartySegmentRef.NuffieldHealthGym, PartySegmentRef.OisixHFAmt, PartySegmentRef.PlatinumStatus, PartySegmentRef.PolarDPCnt, PartySegmentRef.RENAISSANCEGB, PartySegmentRef.RENAISSANCEGBM, PartySegmentRef.RENAISSANCEGM, PartySegmentRef.SilverStatus, PartySegmentRef.TestProduct1Limits, PartySegmentRef.TestProduct2Limits, PartySegmentRef.TestProduct3Limits, PartySegmentRef.TestProduct4Limits, PartySegmentRef.TestProduct5Limits, PartySegmentRef.TestProduct6Limits, PartySegmentRef.TestSharedLimits, PartySegmentRef.UrinaryTest, PartySegmentRef.VHCCompleted, PartySegmentRef.VHRCompleted, PartySegmentRef.VitalityMemEff]

}
