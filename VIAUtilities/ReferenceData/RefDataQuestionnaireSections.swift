//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum QuestionnaireSectionsRef: Int, Hashable { 
    case Unknown = -1
    case AgeGender = 2 // name:Age and Gender note:Age and Gender 
    case AlcoholIntake = 14 // name:Your Alcohol Intake note:Your Alcohol Intake 
    case CalciumPotassium = 28 // name:Calcium and Potassium note:Calcium and Potassium 
    case DietaryPreference = 19 // name:Dietary Preferences note:Dietary Preferences 
    case EatHabits = 22 // name:Eating Habits note:Eating Habits 
    case EatingHabits = 15 // name:Your Eating Habits note:Your Eating Habits 
    case ExSmoker = 12 // name:Ex-smoker note:Ex-smoker 
    case FamilyMedicalHistory = 5 // name:Your Family?s Medical History note:Your Family?s Medical History 
    case Fats = 31 // name:Fats note:Fats 
    case FoodAllergies = 20 // name:Food Allergies and Intolerances note:Food Allergies and Intolerances 
    case FoodPreperation = 21 // name:Food Preperation note:Food Preperation 
    case KeyHba1c = 9 // name:Your Key Measurements/HbA1c note:Your Key Measurements/HbA1c 
    case KeyMeasurements = 6 // name:Your Key Measurements note:Your Key Measurements 
    case KeyMeasurementsBP = 7 // name:Your Key Measurements/Blood Pressure note:Your Key Measurements/Blood Pressure 
    case KeyMeasurementsGluc = 8 // name:Your Key Measurements/Glucose note:Your Key Measurements/Glucose 
    case KeyTotalCholesterol = 10 // name:Your Key Measurements/Cholesterol  Total Cholesterol note:Your Key Measurements/Cholesterol  Total Cholesterol 
    case MealPattern = 23 // name:Meal Pattern note:Meal Pattern 
    case Meats = 30 // name:Meats note:Meats 
    case MedicalHistory = 4 // name:Your Medical History note:Your Medical History 
    case Micronutrients = 25 // name:Micronutrients and Fibre note:Micronutrients and Fibre 
    case OverallHealth = 3 // name:Your Overall Health note:Your Overall Health 
    case PhysicalActivityLev = 16 // name:Your Physical Activity Levels note:Your Physical Activity Levels 
    case PsychWellbeing = 35 // name:Psychological Wellbeing note:Psychological Wellbeing 
    case SFA = 29 // name:Saturated Fatty Acids note:Saturated Fatty Acids 
    case SleepingPatterns = 17 // name:Your Sleeping Patterns note:Your Sleeping Patterns 
    case Smoker = 13 // name:Smoker note:Smoker 
    case Smoking = 11 // name:Smoking note:Smoking 
    case Snacks = 24 // name:Snacks note:Snacks 
    case SocialSupport = 36 // name:Social Support note:Social Support 
    case Sodium = 26 // name:Sodium note:Sodium 
    case StressorDiagnostic = 33 // name:Stressor Diagnostic note:Stressor Diagnostic 
    case Sugar = 27 // name:Sugar note:Sugar 
    case TraumaticEvents = 34 // name:Traumatic Events note:Traumatic Events 
    case VNAMedicalHistory = 38 // name:VNA Medical History note:VNA Medical History
    case YourDetails = 1 // name:Your Details note:Your Details 
    case YourWellbeing = 18 // name:Your Wellbeing note:Your Wellbeing 

    public static let allValues: [QuestionnaireSectionsRef] = [QuestionnaireSectionsRef.Unknown, QuestionnaireSectionsRef.AgeGender, QuestionnaireSectionsRef.AlcoholIntake, QuestionnaireSectionsRef.CalciumPotassium, QuestionnaireSectionsRef.DietaryPreference, QuestionnaireSectionsRef.EatHabits, QuestionnaireSectionsRef.EatingHabits, QuestionnaireSectionsRef.ExSmoker, QuestionnaireSectionsRef.FamilyMedicalHistory, QuestionnaireSectionsRef.Fats, QuestionnaireSectionsRef.FoodAllergies, QuestionnaireSectionsRef.FoodPreperation, QuestionnaireSectionsRef.KeyHba1c, QuestionnaireSectionsRef.KeyMeasurements, QuestionnaireSectionsRef.KeyMeasurementsBP, QuestionnaireSectionsRef.KeyMeasurementsGluc, QuestionnaireSectionsRef.KeyTotalCholesterol, QuestionnaireSectionsRef.MealPattern, QuestionnaireSectionsRef.Meats, QuestionnaireSectionsRef.MedicalHistory, QuestionnaireSectionsRef.Micronutrients, QuestionnaireSectionsRef.OverallHealth, QuestionnaireSectionsRef.PhysicalActivityLev, QuestionnaireSectionsRef.PsychWellbeing, QuestionnaireSectionsRef.SFA, QuestionnaireSectionsRef.SleepingPatterns, QuestionnaireSectionsRef.Smoker, QuestionnaireSectionsRef.Smoking, QuestionnaireSectionsRef.Snacks, QuestionnaireSectionsRef.SocialSupport, QuestionnaireSectionsRef.Sodium, QuestionnaireSectionsRef.StressorDiagnostic, QuestionnaireSectionsRef.Sugar, QuestionnaireSectionsRef.TraumaticEvents, QuestionnaireSectionsRef.YourDetails, QuestionnaireSectionsRef.YourWellbeing]

}
