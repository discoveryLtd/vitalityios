//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum CardItemTypeRef: Int, Hashable { 
    case Unknown = -1
    case Device = 2 // name:Device note:Device 
    case RewardChoice = 4 // name:Reward Choice note:Reward Choice 
    case Voucher = 1 // name:Voucher note:Voucher 
    case WheelSpin = 6 // name:Wheel Spin note:Wheel Spin 

    public static let allValues: [CardItemTypeRef] = [CardItemTypeRef.Unknown, CardItemTypeRef.Device, CardItemTypeRef.RewardChoice, CardItemTypeRef.Voucher, CardItemTypeRef.WheelSpin]

}
