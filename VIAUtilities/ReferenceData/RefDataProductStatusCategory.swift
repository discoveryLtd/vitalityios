//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum ProductStatusCategoryRef: Int, Hashable { 
    case Unknown = -1
    case LifeCycle = 1 // name:Product lifecycle status note:Product lifecycle status 
    case MarketableProduct = 2 // name:Marketable product category note:Marketable product category 

    public static let allValues: [ProductStatusCategoryRef] = [ProductStatusCategoryRef.Unknown, ProductStatusCategoryRef.LifeCycle, ProductStatusCategoryRef.MarketableProduct]

}
