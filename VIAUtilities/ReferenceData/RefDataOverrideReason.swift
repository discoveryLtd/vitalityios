//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum OverrideReasonRef: Int, Hashable { 
    case Unknown = -1
    case Correction = 1 // name:Correction note:Correction 
    case False = 3 // name:False Information note:False Information 
    case Invalid = 2 // name:Invalid note:Invalid 

    public static let allValues: [OverrideReasonRef] = [OverrideReasonRef.Unknown, OverrideReasonRef.Correction, OverrideReasonRef.False, OverrideReasonRef.Invalid]

}
