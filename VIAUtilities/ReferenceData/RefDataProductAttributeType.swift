//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum ProductAttributeTypeRef: Int, Hashable { 
    case Unknown = -1
    case AdditionalPassengers = 1 // name:Additional Passengers note:Additional passengers that may be included on a travel booking 

    public static let allValues: [ProductAttributeTypeRef] = [ProductAttributeTypeRef.Unknown, ProductAttributeTypeRef.AdditionalPassengers]

}
