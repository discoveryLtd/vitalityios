//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum PartyPhysicalAddressRoleTypeRef: Int, Hashable { 
    case Unknown = -1
    case CompanyAddress = 3 // name:Company Address note:Company Address 
    case HomeAddress = 1 // name:Home Address note:Home Address 
    case OfficeAddress = 2 // name:Office Address note:Office Address 
    case PostalAddress = 4 // name:Postal Address note:Postal Address 

    public static let allValues: [PartyPhysicalAddressRoleTypeRef] = [PartyPhysicalAddressRoleTypeRef.Unknown, PartyPhysicalAddressRoleTypeRef.CompanyAddress, PartyPhysicalAddressRoleTypeRef.HomeAddress, PartyPhysicalAddressRoleTypeRef.OfficeAddress, PartyPhysicalAddressRoleTypeRef.PostalAddress]

}
