//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum FeedbackRef: Int, Hashable { 
    case Unknown = -1
    case HealthyButatRisk = 3 // name:In Healthy Range But at Risk note:In Healthy Range But at Risk 
    case InHealthyRange = 1 // name:In Healthy Range note:In Healthy Range 
    case NotInhealthyRange = 2 // name:Not In healthy Range note:Not In healthy Range 

    public static let allValues: [FeedbackRef] = [FeedbackRef.Unknown, FeedbackRef.HealthyButatRisk, FeedbackRef.InHealthyRange, FeedbackRef.NotInhealthyRange]

}
