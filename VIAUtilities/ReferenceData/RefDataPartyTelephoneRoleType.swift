//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum PartyTelephoneRoleTypeRef: Int, Hashable { 
    case Unknown = -1
    case AfterHoursNumber = 2 // name:After Hours Number note:After Hours Number 
    case DaytimeNumber = 1 // name:Daytime Number note:Daytime Number 
    case HomePhone = 3 // name:Home Phone note:Home Phone 
    case WorkPhone = 4 // name:Work Phone note:Work Phone 

    public static let allValues: [PartyTelephoneRoleTypeRef] = [PartyTelephoneRoleTypeRef.Unknown, PartyTelephoneRoleTypeRef.AfterHoursNumber, PartyTelephoneRoleTypeRef.DaytimeNumber, PartyTelephoneRoleTypeRef.HomePhone, PartyTelephoneRoleTypeRef.WorkPhone]

}
