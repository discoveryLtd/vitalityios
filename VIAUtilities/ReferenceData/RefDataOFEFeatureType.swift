//
//  RefDataOFEFeatureType.swift
//  VitalityActive
//
//  Created by Val Tomol on 25/04/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation

@objc public enum OFEFeatureTypeRef: Int, Hashable {
    case Unknown = -1
    case OFEMiles = 36
    case OFEKilometers = 37
    case WeblinkProof = 202
    case OFEDistance = 203
    case RaceNumber = 204
    case EventName = 205
    case FinishTime = 206
    case Organiser = 208
    case EventType = 999
    case EventDate = 998
    
    
    public static let allValues: [OFEFeatureTypeRef] = [
        OFEFeatureTypeRef.Unknown,
        OFEFeatureTypeRef.OFEMiles,
        OFEFeatureTypeRef.OFEKilometers,
        OFEFeatureTypeRef.WeblinkProof,
        OFEFeatureTypeRef.OFEDistance,
        OFEFeatureTypeRef.RaceNumber,
        OFEFeatureTypeRef.EventName,
        OFEFeatureTypeRef.FinishTime,
        OFEFeatureTypeRef.Organiser,
        OFEFeatureTypeRef.EventType,
        OFEFeatureTypeRef.EventDate
    ]
}
