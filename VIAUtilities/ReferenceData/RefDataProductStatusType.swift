//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum ProductStatusTypeRef: Int, Hashable { 
    case Unknown = -1
    case Active = 1 // name:Active note:Active 
    case Cancelled = 2 // name:Cancelled note:Cancelled 

    public static let allValues: [ProductStatusTypeRef] = [ProductStatusTypeRef.Unknown, ProductStatusTypeRef.Active, ProductStatusTypeRef.Cancelled]

}
