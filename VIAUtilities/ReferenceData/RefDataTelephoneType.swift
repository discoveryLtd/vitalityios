//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum TelephoneTypeRef: Int, Hashable { 
    case Unknown = -1
    case Fixed = 2 // name:Fixed note:Fixed 
    case Mobile = 1 // name:Mobile note:Mobile 

    public static let allValues: [TelephoneTypeRef] = [TelephoneTypeRef.Unknown, TelephoneTypeRef.Fixed, TelephoneTypeRef.Mobile]

}
