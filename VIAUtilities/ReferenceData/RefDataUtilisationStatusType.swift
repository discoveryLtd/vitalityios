//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum UtilisationStatusTypeRef: Int, Hashable { 
    case Unknown = -1
    case LimitsReached = 2 // name:Limits reached note:Limits have been reached for Utilisation tracker 
    case Tracking = 1 // name:Tracking utilisation note:Tracking of utilisation in progress 

    public static let allValues: [UtilisationStatusTypeRef] = [UtilisationStatusTypeRef.Unknown, UtilisationStatusTypeRef.LimitsReached, UtilisationStatusTypeRef.Tracking]

}
