//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum PartyAttributeFeedbackTypeRef: Int, Hashable { 
    case Unknown = -1
    case Conclusion = 1 // name:Feedback that is giving a conclusion note: 
    case Information = 4 // name:Feedback that is giving an informative description note: 
    case Observation = 3 // name:Feedback that is giving an observation note: 
    case Recommendation = 2 // name:Feedback that is giving a recommendation note: 

    public static let allValues: [PartyAttributeFeedbackTypeRef] = [PartyAttributeFeedbackTypeRef.Unknown, PartyAttributeFeedbackTypeRef.Conclusion, PartyAttributeFeedbackTypeRef.Information, PartyAttributeFeedbackTypeRef.Observation, PartyAttributeFeedbackTypeRef.Recommendation]

}
