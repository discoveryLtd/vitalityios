//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum PartyAttributeMetaDataTypeRef: Int, Hashable { 
    case Unknown = -1
    case SourceValue = 166 // name:Source Value note:Source Value 
    case VAgeVariance = 2 // name:Vitality Age Variance note:Vitality Age Variance 

    public static let allValues: [PartyAttributeMetaDataTypeRef] = [PartyAttributeMetaDataTypeRef.Unknown, PartyAttributeMetaDataTypeRef.SourceValue, PartyAttributeMetaDataTypeRef.VAgeVariance]

}
