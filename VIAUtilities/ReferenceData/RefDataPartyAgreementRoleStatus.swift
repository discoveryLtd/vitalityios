//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum PartyAgreementRoleStatusRef: Int, Hashable { 
    case Unknown = -1
    case ACTIVE = 1 // name: Active note: Active 
    case CANCEL = 2 // name: Cancel note: Cancel 
    case SUSPENDED = 3 // name: Suspended note: Suspended 
    case WAITING_PERIOD = 4 // name: Waiting Period note: Waiting Period 

    public static let allValues: [PartyAgreementRoleStatusRef] = [PartyAgreementRoleStatusRef.Unknown, PartyAgreementRoleStatusRef.ACTIVE, PartyAgreementRoleStatusRef.CANCEL, PartyAgreementRoleStatusRef.SUSPENDED, PartyAgreementRoleStatusRef.WAITING_PERIOD]

}
