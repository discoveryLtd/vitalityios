//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum PartyTypeRef: Int, Hashable { 
    case Unknown = -1
    case INDV = 1 // name:Individual note:Individual Party 
    case ORG = 2 // name:Organisation note:Organisation Party 

    public static let allValues: [PartyTypeRef] = [PartyTypeRef.Unknown, PartyTypeRef.INDV, PartyTypeRef.ORG]

}
