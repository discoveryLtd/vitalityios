//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum UtilisationApplicabilityTypeRef: Int, Hashable { 
    case Unknown = -1
    case Member = 2 // name:Member note:Member 
    case VitalityMembership = 1 // name:Vitality Membership note:VitalityMembership 

    public static let allValues: [UtilisationApplicabilityTypeRef] = [UtilisationApplicabilityTypeRef.Unknown, UtilisationApplicabilityTypeRef.Member, UtilisationApplicabilityTypeRef.VitalityMembership]

}
