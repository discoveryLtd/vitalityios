//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum AwardedRewardReferenceTypeRef: Int, Hashable { 
    case Unknown = -1
    case EventId = 3 // name:Event Id note:Number or code that uniquely identifies an event 
    case GoalTrackerId = 2 // name:Goal Tracker Id note:Number or code that uniquely identifies a goal tracker 
    case PartnerRewardCode = 5 // name:Partner Reward Code note:The reward code that is supplied by the partner when the reward is created in their system and will be used when redeeming the reward 
    case PartnerSysRewardID = 4 // name:Partner System Reward ID note:The reward ID supplied by the partner when the reward is issued. This is an internal id that is used by the partner to identify the reward in their system and is not presented to the member 
    case VoucherNumber = 1 // name:Voucher Number note:Number or code that uniquely identifies a voucher 

    public static let allValues: [AwardedRewardReferenceTypeRef] = [AwardedRewardReferenceTypeRef.Unknown, AwardedRewardReferenceTypeRef.EventId, AwardedRewardReferenceTypeRef.GoalTrackerId, AwardedRewardReferenceTypeRef.PartnerRewardCode, AwardedRewardReferenceTypeRef.PartnerSysRewardID, AwardedRewardReferenceTypeRef.VoucherNumber]

}
