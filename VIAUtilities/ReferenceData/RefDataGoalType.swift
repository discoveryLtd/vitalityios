//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum GoalTypeRef: Int, Hashable { 
    case Unknown = -1
    case Recurring = 1 // name:Recurring goal type note:Recurring goal type 

    public static let allValues: [GoalTypeRef] = [GoalTypeRef.Unknown, GoalTypeRef.Recurring]

}
