//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum InboundFinancialInstructionRoleTypeRef: Int, Hashable { 
    case Unknown = -1
    case Member = 1 // name:Member note:Member 

    public static let allValues: [InboundFinancialInstructionRoleTypeRef] = [InboundFinancialInstructionRoleTypeRef.Unknown, InboundFinancialInstructionRoleTypeRef.Member]

}
