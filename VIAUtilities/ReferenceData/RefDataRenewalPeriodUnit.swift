//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum RenewalPeriodUnitRef: Int, Hashable { 
    case Unknown = -1
    case DAY = 3 // name: DAY note: DAY 
    case MONTH = 2 // name: MONTH note: MONTH 
    case WEEK = 1 // name: WEEK note: WEEK 

    public static let allValues: [RenewalPeriodUnitRef] = [RenewalPeriodUnitRef.Unknown, RenewalPeriodUnitRef.DAY, RenewalPeriodUnitRef.MONTH, RenewalPeriodUnitRef.WEEK]

}
