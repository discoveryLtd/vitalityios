//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum PartyAttributeTypeRef: Int, Hashable { 
//    case Unknown = -1
//    case AddedFatBehavScore = 31 // name:Added Fat Behaviour Score note:Added Fat Behaviour Score calculated 
//    case AddedSaltConsum = 52 // name:Added Salt note:Added Salt Consum 
//    case AlcoholConsumption = 61 // name:Alchohol Consumption note:Whether a member consumes alchohol or not 
//    case AlcoholDrinksPerDay = 44 // name:Alcohol Use note:Alcohol Drinks Per Day 
//    case AlcoholRR = 28 // name:Alcohol Relative Risk note:Alcohol Relative Risk calculated 
//    case AvgHoursSleepDay = 51 // name:Sleep note:Avg Hours Sleep Day 
//    case BMI = 37 // name:Body Mass Index note:Body Mass Index 
//    case BMIRelativeRisk = 14 // name:BMI Relative Risk  note:BMI Relative Risk calculated  
//    case BloodPressure = 42 // name:Blood Pressure note:Blood Pressure 
//    case BloodPressureDiasto = 3 // name:BloodPressureDiastolic note:Blood Pressure Diastolic 
//    case BloodPressureSystol = 2 // name:BloodPressureSystol note:Blood Pressure Systolic 
//    case DailyFruitConsum = 56 // name:Daily Fruit note:Daily Fruit Consum 
//    case DailyVegConsum = 57 // name:Daily Vegetables note:Daily Veg Consum 
//    case DiastolicBP = 39 // name:Diastolic blood pressure note:The diastolic blood pressure reading of the member 
//    case DiastolicBPRR = 16 // name:Diastolic Blood Pressure Relative Risk  note:Diastolic Blood Pressure Relative Risk calculated  
//    case DietBehaviourScore = 23 // name:Dietary Behaviour Score note:Dietary Behaviour Score calculated 
//    case DietBehaviourScoreRR = 22 // name:Dietary Behaviour Score Relative Risk note:Dietary Behaviour Score Relative Risk Calculated 
//    case ExerciseIntensity = 65 // name:ExerciseIntensity note:ExerciseIntensity 
//    case ExerciseMinsPerWeek = 43 // name:Physical Activity note:Exercise Mins Per Week 
//    case FastingGlucose = 8 // name:Blood glucose note:The fasting glucose reading of the member 
//    case FastingGlucoseRR = 18 // name:Fasting Glucose Relative Risk  note:Fasting Glucose Relative Risk calculated  
//    case FlexibilityExercise = 45 // name:FlexibilityExercise note:FlexibilityExercise
//    case FruitVegBehavScore = 29 // name:Fruit and Vegetables Behaviour Score  note:Fruit and Vegetables Behaviour Score calculated  
//    case HDLCholesterol = 6 // name:HDL cholesterol  note:The HDL cholesterol reading of the member 
//    case HbA1c = 10 // name:Diabetes note:The glycated heamoglobin reading  of the member 
//    case Height = 12 // name:Height note:The height measurement of the member 
//    case HighFatConsum = 55 // name:Trans Fats note:High Fat Consum 
//    case HighFatCookMethods = 50 // name:Cooking Methods note:High Fat Cook Methods 
//    case KesslerStressRR = 20 // name:Kessler Stress Relative Risk note:Kessler Stress Relative Risk calculated 
//    case KesslerStressScore = 21 // name:Kessler Stress Score note:Kessler Stress Score calculated 
//    case LDLCholesterol = 5 // name:LDL cholesterol  note:The LDL cholesterol reading of the member 
//    case LeanMeatBehavScore = 32 // name:Lean Meat Behaviour Score note:Lean Meat Behaviour Score calculated 
//    case LeanMeatConsum = 58 // name:Lean Meats note:Lean Meat Consum 
//    case LipidRatio = 41 // name:Lipid Ratio note:Lipid Ratio 
//    case LwFatDairyBehavScore = 33 // name:Low Fat Dairy Behaviour Score note:Low Fat Dairy Behaviour Score calculated 
//    case LwFatDairyConsum = 59 // name:Low Fat Dairy note:Lw Fat Dairy Consum 
//    case MentalWellbeing = 66 // name:Mental Wellbeing note:MentalWellbeing 
//    case OverallNutritionRR = 19 // name:Overall Nutrition Relative Risk  note:Overall Nutrition Relative Risk calculated  
//    case PhysicalActivityRR = 26 // name:Physical Activity Relative Risk note:Physical Activity Relative Risk calculated 
//    case Pregnant = 62 // name:Pregnant note:Whether a member is pregnant or not 
//    case RandomGlucose = 9 // name:Blood glucose note:The random glucose reading of the member 
//    case SaltScore = 34 // name:Salt Score note:Salt Score calculated 
//    case SaltUsageRR = 24 // name:Salt Usage Relative Risk note:Salt Usage Relative Risk calculated 
//    case SaltyFoodConsm = 53 // name:Salty Foods note:Salty Food Consm 
//    case SedentaryBehav = 47 // name:Sedentary Behaviour note:Sedentary Behav 
//    case SmokingHabits = 64 // name:Smoking Habits note:Whether a member smokes or not 
//    case SmokingRR = 27 // name:Smoking Relative Risk note:Smoking Relative Risk calculated 
//    case StrengthExercise = 46 // name:Strength Exercise note:Strength Exercise 
//    case SugarRR = 36 // name:Sugar Relative Risk note:Sugar Relative Risk calculated 
//    case SugaryDrinksConsum = 54 // name:Sugary Drinks note:Sugary Drinks Consum 
//    case SystolicBP = 38 // name:Systolic blood pressure note:The systolic blood pressure reading of the member 
//    case SystolicBPRR = 15 // name:Systolic Blood Pressure Relative Risk  note:Systolic Blood Pressure Relative Risk calculated  
//    case TobaccoUse = 48 // name:Tobacco Use note:Tobacco Use 
//    case TotalCholesterol = 4 // name:Cholesterol note:The total cholesterol reading of the member 
//    case TotalCholesterolRR = 17 // name:Total Cholesterol Relative Risk  note:Total Cholesterol Relative Risk calculated  
//    case TransFatRR = 35 // name:Trans Fat Relative Risk note:Trans Fat Relative Risk calculated 
//    case Triglycerides = 13 // name:Triglycerides note:The Triglyceride cholesterol reading of the member 
//    case UrinaryProtein = 40 // name:Urinary Protein note:Urinary Protein Attribute 
//    case VitalityAge = 25 // name:Vitality Age note:Vitality Age calculated 
//    case WaistCircumference = 1 // name:Waist Circumference note:The waist circumference measurement of the member 
//    case Weight = 11 // name:Weight note:The weight measurement of the member 
//    case WholegrainBehavScore = 30 // name:Wholegrain Behaviour Score note:Wholegrain Behaviour Score calculated 
//    case WholegrainConsum = 60 // name:Wholegrains note:Wholegrain Consum 
//    case YearsSmokeFree = 63 // name:Years Smoke Free note:Number of years a member has not been smoking 
//    case Nutrition = 101 // name:Nutrition
//    case HighProcessedConsum = 80 // name:Highly Processed Foods note:Highly Processed Foods
//    case SugaryFoodConsum = 82 // name:Sugary Foods note:Sugary Foods
//    case AddedSugarConsum = 85 // name:Added Sugar note:Added Sugar
//    case FastFoodConsum = 86 // name:Fast Foods note:Fast Foods
//    case ProcessedMeatConsum = 87 // name:Processed Meats note:Processed Meats
//    case RedMeatsConsum = 88 // name:Red Meats note:Red Meats
//    case Omega3Consum = 89 // name:Omega 3 rich fish note:Omega 3 rich fish
//    case SaturatedFatsConsum = 90 // name:Saturated Fats note:Saturated Fats
//    case Stressor = 68 // name:Stressor note:Stressor
//    case TraumaticEvents = 69 // name:Traumatic Events note:Traumatic Events
//    case PsychoPositive = 72 // name:Positive Emotion note:Positive Emotion
//    case PsychoEnergy = 73 // name:Energy note:Energy
//    case PsycoEmotion = 74 // name:Negative Emotion note:Negative Emotion
//    case PsycoAnxiety = 75 // name:Anxiety note:Anxiety
//    case Psychological = 71 // name:Psychological note:Psychological
//    case Social = 70 // name:Social note:Social
//    case CalciumPotassium = 92 // name:Calcium and Potassium note:Calcium and Potassium
//    case Dinner = 78 // name:Dinner note:Dinner
//
//    public static let allValues: [PartyAttributeTypeRef] = [PartyAttributeTypeRef.Unknown, PartyAttributeTypeRef.AddedFatBehavScore, PartyAttributeTypeRef.AddedSaltConsum, PartyAttributeTypeRef.AlcoholConsumption, PartyAttributeTypeRef.AlcoholDrinksPerDay, PartyAttributeTypeRef.AlcoholRR, PartyAttributeTypeRef.AvgHoursSleepDay, PartyAttributeTypeRef.BMI, PartyAttributeTypeRef.BMIRelativeRisk, PartyAttributeTypeRef.BloodPressure, PartyAttributeTypeRef.BloodPressureDiasto, PartyAttributeTypeRef.BloodPressureSystol, PartyAttributeTypeRef.DailyFruitConsum, PartyAttributeTypeRef.DailyVegConsum, PartyAttributeTypeRef.DiastolicBP, PartyAttributeTypeRef.DiastolicBPRR, PartyAttributeTypeRef.DietBehaviourScore, PartyAttributeTypeRef.DietBehaviourScoreRR, PartyAttributeTypeRef.ExerciseIntensity, PartyAttributeTypeRef.ExerciseMinsPerWeek, PartyAttributeTypeRef.FastingGlucose, PartyAttributeTypeRef.FastingGlucoseRR, PartyAttributeTypeRef.FlexibilityExercise, PartyAttributeTypeRef.FruitVegBehavScore, PartyAttributeTypeRef.HDLCholesterol, PartyAttributeTypeRef.HbA1c, PartyAttributeTypeRef.Height, PartyAttributeTypeRef.HighFatConsum, PartyAttributeTypeRef.HighFatCookMethods, PartyAttributeTypeRef.KesslerStressRR, PartyAttributeTypeRef.KesslerStressScore, PartyAttributeTypeRef.LDLCholesterol, PartyAttributeTypeRef.LeanMeatBehavScore, PartyAttributeTypeRef.LeanMeatConsum, PartyAttributeTypeRef.LipidRatio, PartyAttributeTypeRef.LwFatDairyBehavScore, PartyAttributeTypeRef.LwFatDairyConsum, PartyAttributeTypeRef.MentalWellbeing, PartyAttributeTypeRef.OverallNutritionRR, PartyAttributeTypeRef.PhysicalActivityRR, PartyAttributeTypeRef.Pregnant, PartyAttributeTypeRef.RandomGlucose, PartyAttributeTypeRef.SaltScore, PartyAttributeTypeRef.SaltUsageRR, PartyAttributeTypeRef.SaltyFoodConsm, PartyAttributeTypeRef.SedentaryBehav, PartyAttributeTypeRef.SmokingHabits, PartyAttributeTypeRef.SmokingRR, PartyAttributeTypeRef.StrengthExercise, PartyAttributeTypeRef.SugarRR, PartyAttributeTypeRef.SugaryDrinksConsum, PartyAttributeTypeRef.SystolicBP, PartyAttributeTypeRef.SystolicBPRR, PartyAttributeTypeRef.TobaccoUse, PartyAttributeTypeRef.TotalCholesterol, PartyAttributeTypeRef.TotalCholesterolRR, PartyAttributeTypeRef.TransFatRR, PartyAttributeTypeRef.Triglycerides, PartyAttributeTypeRef.UrinaryProtein, PartyAttributeTypeRef.VitalityAge, PartyAttributeTypeRef.WaistCircumference, PartyAttributeTypeRef.Weight, PartyAttributeTypeRef.WholegrainBehavScore, PartyAttributeTypeRef.WholegrainConsum, PartyAttributeTypeRef.YearsSmokeFree, PartyAttributeTypeRef.Nutrition, PartyAttributeTypeRef.HighProcessedConsum, PartyAttributeTypeRef.SugaryFoodConsum, PartyAttributeTypeRef.AddedSugarConsum, PartyAttributeTypeRef.FastFoodConsum, PartyAttributeTypeRef.ProcessedMeatConsum, PartyAttributeTypeRef.RedMeatsConsum, PartyAttributeTypeRef.Omega3Consum, PartyAttributeTypeRef.SaturatedFatsConsum, PartyAttributeTypeRef.Stressor, PartyAttributeTypeRef.TraumaticEvents, PartyAttributeTypeRef.PsychoPositive, PartyAttributeTypeRef.PsychoEnergy, PartyAttributeTypeRef.PsycoEmotion, PartyAttributeTypeRef.PsycoAnxiety, PartyAttributeTypeRef.Psychological, PartyAttributeTypeRef.Social, PartyAttributeTypeRef.CalciumPotassium, PartyAttributeTypeRef.Dinner]
    
    // Reordered and updated.
    case Unknown = -1
    case WaistCircumference = 1
    case BloodPressureSystol = 2
    case BloodPressureDiasto = 3
    case TotalCholesterol = 4
    case LDLCholesterol = 5
    case HDLCholesterol = 6
    case FastingGlucose = 8
    case RandomGlucose = 9
    case HbA1c = 10
    case Weight = 11
    case Height = 12
    case Triglycerides = 13
    case BmiRelativeRisk = 14
    case SystolicBPRR = 15
    case DiastolicBPRR = 16
    case TotalCholesterolRR = 17
    case FastingGlucoseRR = 18
    case OverallNutritionRR = 19
    case KesslerStressRR = 20
    case KesslerStressScore = 21
    case DietBehaviourScoreRR = 22
    case DietBehaviourScore = 23
    case SaltUsageRR = 24
    case VitalityAge = 25
    case PhysicalActivityRR = 26
    case SmokingRR = 27
    case AlcoholRR = 28
    case FruitVegBehavScore = 29
    case WholegrainBehavScore = 30
    case AddedFatBehavScore = 31
    case LeanMeatBehavScore = 32
    case LwFatDairyBehavScore = 33
    case SaltScore = 34
    case TransFatRR = 35
    case SugarRR = 36
    case BMI = 37
    case SystolicBP = 38
    case DiastolicBP = 39
    case UrinaryProtein = 40
    case LipidRatio = 41
    case BloodPressure = 42
    case ExerciseMinsPerWeek = 43
    case AlcoholDrinksPerDay = 44
    case FlexibilityExercise = 45
    case StrengthExercise = 46
    case SedentaryBehav = 47
    case TobaccoUse = 48
    case HighFatCookMethods = 50
    case AvgHoursSleepDay = 51
    case AddedSaltConsum = 52
    case SaltyFoodConsm = 53
    case SugaryDrinksConsum = 54
    case HighFatConsum = 55
    case DailyFruitConsum = 56
    case DailyVegConsum = 57
    case LeanMeatConsum = 58
    case LwFatDairyConsum = 59
    case WholegrainConsum = 60
    case AlcoholConsumption = 61
    case Pregnant = 62
    case YearsSmokeFree = 63
    case SmokingHabits = 64
    case ExerciseIntensity = 65
    case MentalWellbeing = 66
    case SleepDifficulty = 67
    case Stressor = 68
    case TraumaticEvents = 69
    case Social = 70
    case Psychological = 71
    case PsychoPositive = 72
    case PsychoEnergy = 73
    case PsycoEmotion = 74
    case PsycoAnxiety = 75
    case Breakfast = 76
    case Lunch = 77
    case Dinner = 78
    case Snacking = 79
    case HighProcessedConsum = 80
    case SugaryFoodConsum = 82
    case SaltConsump = 83
    case HighSodiumConsum = 84
    case AddedSugarConsum = 85
    case FastFoodConsum = 86
    case ProcessedMeatConsum = 87
    case RedMeatsConsum = 88
    case Omega3Consum = 89
    case SaturatedFatsConsum = 90
    case UnsaturatedFats = 91
    case CalciumPotassium = 92
    case MealPattern = 93
    case Snacks = 94
    case MicronutrientsFibre = 95
    case Sodium = 96
    case Sugar = 97
    case SaturatedFats = 98
    case Meats = 99
    case Fats = 100
    case Nutrition = 101
    
    public static let allValues: [PartyAttributeTypeRef] = [
        PartyAttributeTypeRef.Unknown,
        PartyAttributeTypeRef.WaistCircumference,
        PartyAttributeTypeRef.BloodPressureSystol,
        PartyAttributeTypeRef.BloodPressureDiasto,
        PartyAttributeTypeRef.TotalCholesterol,
        PartyAttributeTypeRef.LDLCholesterol,
        PartyAttributeTypeRef.HDLCholesterol,
        PartyAttributeTypeRef.FastingGlucose,
        PartyAttributeTypeRef.RandomGlucose,
        PartyAttributeTypeRef.HbA1c,
        PartyAttributeTypeRef.Weight,
        PartyAttributeTypeRef.Height,
        PartyAttributeTypeRef.Triglycerides,
        PartyAttributeTypeRef.BmiRelativeRisk,
        PartyAttributeTypeRef.SystolicBPRR,
        PartyAttributeTypeRef.DiastolicBPRR,
        PartyAttributeTypeRef.TotalCholesterolRR,
        PartyAttributeTypeRef.FastingGlucoseRR,
        PartyAttributeTypeRef.OverallNutritionRR,
        PartyAttributeTypeRef.KesslerStressRR,
        PartyAttributeTypeRef.KesslerStressScore,
        PartyAttributeTypeRef.DietBehaviourScoreRR,
        PartyAttributeTypeRef.DietBehaviourScore,
        PartyAttributeTypeRef.SaltUsageRR,
        PartyAttributeTypeRef.VitalityAge,
        PartyAttributeTypeRef.PhysicalActivityRR,
        PartyAttributeTypeRef.SmokingRR,
        PartyAttributeTypeRef.AlcoholRR,
        PartyAttributeTypeRef.FruitVegBehavScore,
        PartyAttributeTypeRef.WholegrainBehavScore,
        PartyAttributeTypeRef.AddedFatBehavScore,
        PartyAttributeTypeRef.LeanMeatBehavScore,
        PartyAttributeTypeRef.LwFatDairyBehavScore,
        PartyAttributeTypeRef.SaltScore,
        PartyAttributeTypeRef.TransFatRR,
        PartyAttributeTypeRef.SugarRR,
        PartyAttributeTypeRef.BMI,
        PartyAttributeTypeRef.SystolicBP,
        PartyAttributeTypeRef.DiastolicBP,
        PartyAttributeTypeRef.UrinaryProtein,
        PartyAttributeTypeRef.LipidRatio,
        PartyAttributeTypeRef.BloodPressure,
        PartyAttributeTypeRef.ExerciseMinsPerWeek,
        PartyAttributeTypeRef.AlcoholDrinksPerDay,
        PartyAttributeTypeRef.FlexibilityExercise,
        PartyAttributeTypeRef.StrengthExercise,
        PartyAttributeTypeRef.SedentaryBehav,
        PartyAttributeTypeRef.TobaccoUse,
        PartyAttributeTypeRef.HighFatCookMethods,
        PartyAttributeTypeRef.AvgHoursSleepDay,
        PartyAttributeTypeRef.AddedSaltConsum,
        PartyAttributeTypeRef.SaltyFoodConsm,
        PartyAttributeTypeRef.SugaryDrinksConsum,
        PartyAttributeTypeRef.HighFatConsum,
        PartyAttributeTypeRef.DailyFruitConsum,
        PartyAttributeTypeRef.DailyVegConsum,
        PartyAttributeTypeRef.LeanMeatConsum,
        PartyAttributeTypeRef.LwFatDairyConsum,
        PartyAttributeTypeRef.WholegrainConsum,
        PartyAttributeTypeRef.AlcoholConsumption,
        PartyAttributeTypeRef.Pregnant,
        PartyAttributeTypeRef.YearsSmokeFree,
        PartyAttributeTypeRef.SmokingHabits,
        PartyAttributeTypeRef.ExerciseIntensity,
        PartyAttributeTypeRef.MentalWellbeing,
        PartyAttributeTypeRef.SleepDifficulty,
        PartyAttributeTypeRef.Stressor,
        PartyAttributeTypeRef.TraumaticEvents,
        PartyAttributeTypeRef.Social,
        PartyAttributeTypeRef.Psychological,
        PartyAttributeTypeRef.PsychoPositive,
        PartyAttributeTypeRef.PsychoEnergy,
        PartyAttributeTypeRef.PsycoEmotion,
        PartyAttributeTypeRef.PsycoAnxiety,
        PartyAttributeTypeRef.Breakfast,
        PartyAttributeTypeRef.Lunch,
        PartyAttributeTypeRef.Dinner,
        PartyAttributeTypeRef.Snacking,
        PartyAttributeTypeRef.HighProcessedConsum,
        PartyAttributeTypeRef.SugaryFoodConsum,
        PartyAttributeTypeRef.SaltConsump,
        PartyAttributeTypeRef.HighSodiumConsum,
        PartyAttributeTypeRef.AddedSugarConsum,
        PartyAttributeTypeRef.FastFoodConsum,
        PartyAttributeTypeRef.ProcessedMeatConsum,
        PartyAttributeTypeRef.RedMeatsConsum,
        PartyAttributeTypeRef.Omega3Consum,
        PartyAttributeTypeRef.SaturatedFatsConsum,
        PartyAttributeTypeRef.UnsaturatedFats,
        PartyAttributeTypeRef.CalciumPotassium,
        PartyAttributeTypeRef.MealPattern,
        PartyAttributeTypeRef.Snacks,
        PartyAttributeTypeRef.MicronutrientsFibre,
        PartyAttributeTypeRef.Sodium,
        PartyAttributeTypeRef.Sugar,
        PartyAttributeTypeRef.SaturatedFats,
        PartyAttributeTypeRef.Meats,
        PartyAttributeTypeRef.Fats,
        PartyAttributeTypeRef.Nutrition
    ]
}
