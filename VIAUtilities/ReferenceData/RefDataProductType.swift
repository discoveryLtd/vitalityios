//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum ProductTypeRef: Int, Hashable { 
    case Unknown = -1
    case BenefitProduct = 2 // name:Products related to benefits that members can be rewarded on note:Products related to benefits that members can be rewarded on 
    case PartnerMembership = 5 // name:Products related to the Partner Membership Product note:Products related to the Partner Membership Product 
    case ProgrammeProduct = 4 // name:Products related to programmes that a member can participate in note:Products related to programmes that a member can participate in 
    case PurchaseProduct = 1 // name:Products related to purchases that a member can make note:Products related to purchases that a member can make 
    case VitalityProduct = 3 // name:Products related to the Vitality Membership Product note:Products related to the Vitality Membership Product 

    public static let allValues: [ProductTypeRef] = [ProductTypeRef.Unknown, ProductTypeRef.BenefitProduct, ProductTypeRef.PartnerMembership, ProductTypeRef.ProgrammeProduct, ProductTypeRef.PurchaseProduct, ProductTypeRef.VitalityProduct]

}
