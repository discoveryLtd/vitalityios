//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum DiscountTypeRef: Int, Hashable { 
    case Unknown = -1
    case DiscountPercent = 1 // name:DiscountPercent note:DiscountPercent 
    case PassengerDiscount = 2 // name:PassengerDiscount note:PassengerDiscount 

    public static let allValues: [DiscountTypeRef] = [DiscountTypeRef.Unknown, DiscountTypeRef.DiscountPercent, DiscountTypeRef.PassengerDiscount]

}
