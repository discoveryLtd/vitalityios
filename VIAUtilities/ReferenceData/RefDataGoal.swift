//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum GoalRef: Int, Hashable { 
    case Unknown = -1
    case ActiveRewards = 1 // name:Active rewards goal note:Active rewards goal 
    case AppleWatch = 2 // name:Apple Watch goal note:Apple Watch goal 

    public static let allValues: [GoalRef] = [GoalRef.Unknown, GoalRef.ActiveRewards, GoalRef.AppleWatch]

}
