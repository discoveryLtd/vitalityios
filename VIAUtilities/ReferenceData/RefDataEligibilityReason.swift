//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum EligibilityReasonRef: Int, Hashable { 
    case Unknown = -1
    case BenefitActive = 13 // name:Active Benefit note:Active Benefit 
    case GenderNotFemale = 9 // name:Gender not female note:Gender not female 
    case GenderNotMale = 8 // name:Gender not male note:Gender not male 
    case Ineligible = 15 // name:Ineligible note:Ineligible 
    case LimitsReached = 14 // name:Limits reached note:Limits reached 
    case MembershipActive = 1 // name:Active Vitality Membership note:Active Vitality Membership 
    case MembershipNotActive = 2 // name:Vitality Membership Not Active note:Vitality Membership not active 
    case NotApplicable = 11 // name:Not applicable note:Not applicable - None of the eligibility rule conditions were met 
    case OutsideAgeRange = 7 // name:Outside required age range note:Outside required age range 
    case ProductInactive = 12 // name:Product Inactive note:Product not in an Active status on the requested date 
    case VHCCompleted = 3 // name:VHC Completed note:VHC Completed 
    case VHCNotCompleted = 4 // name:VHC not completed note:VHC not completed 
    case VHCandVHRCompleted = 18 // name:VHC and VHR completed note:VHC and VHR completed 
    case VHCandVHRNotComplete = 19 // name:VHC and VHR not completed note:VHC and VHR not completed 
    case VHRCompleted = 16 // name:VHR Completed note:VHR Completed 
    case VHRNotCompleted = 17 // name:VHR not completed note:VHR not completed 
    case VNACompleted = 20 // name:VNA Completed note:VNA Completed 
    case VNANotCompleted = 21 // name:VNA not completed note:VNA not completed 
    case VitStatusAchieved = 10 // name:Vitality status achieved note:Vitality status achieved 
    case VitStatusNotAchieved = 5 // name:Vitality status not achieved note:Vitality status not achieved 
    case WithinAgeRange = 6 // name:Within age range note:Within age range 

    public static let allValues: [EligibilityReasonRef] = [EligibilityReasonRef.Unknown, EligibilityReasonRef.BenefitActive, EligibilityReasonRef.GenderNotFemale, EligibilityReasonRef.GenderNotMale, EligibilityReasonRef.Ineligible, EligibilityReasonRef.LimitsReached, EligibilityReasonRef.MembershipActive, EligibilityReasonRef.MembershipNotActive, EligibilityReasonRef.NotApplicable, EligibilityReasonRef.OutsideAgeRange, EligibilityReasonRef.ProductInactive, EligibilityReasonRef.VHCCompleted, EligibilityReasonRef.VHCNotCompleted, EligibilityReasonRef.VHCandVHRCompleted, EligibilityReasonRef.VHCandVHRNotComplete, EligibilityReasonRef.VHRCompleted, EligibilityReasonRef.VHRNotCompleted, EligibilityReasonRef.VNACompleted, EligibilityReasonRef.VNANotCompleted, EligibilityReasonRef.VitStatusAchieved, EligibilityReasonRef.VitStatusNotAchieved, EligibilityReasonRef.WithinAgeRange]

}
