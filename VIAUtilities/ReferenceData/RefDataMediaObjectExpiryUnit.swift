//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum MediaObjectExpiryUnitRef: Int, Hashable { 
    case Unknown = -1
    case DAYS = 3 // name:DAYS note:undefined 
    case MONTHS = 1 // name:MONTHS note:undefined 
    case WEEKS = 2 // name:WEEKS note:undefined 

    public static let allValues: [MediaObjectExpiryUnitRef] = [MediaObjectExpiryUnitRef.Unknown, MediaObjectExpiryUnitRef.DAYS, MediaObjectExpiryUnitRef.MONTHS, MediaObjectExpiryUnitRef.WEEKS]

}
