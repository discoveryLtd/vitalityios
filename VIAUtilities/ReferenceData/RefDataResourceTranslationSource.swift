//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum ResourceTranslationSourceRef: Int, Hashable { 
    case Unknown = -1
    case G = 0 // name:Google note:Google Source 
    case M = 1 // name:Manual note:Manual Source 
    case T = 2 // name:Tenant note:Tenant Source 

    public static let allValues: [ResourceTranslationSourceRef] = [ResourceTranslationSourceRef.Unknown, ResourceTranslationSourceRef.G, ResourceTranslationSourceRef.M, ResourceTranslationSourceRef.T]

}
