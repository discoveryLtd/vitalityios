//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum CardStatusTypeRef: Int, Hashable {
    case Unknown = -1
    case Achieved = 7 // name:Achieved note:Achieved
    case Activated = 9 // name:Activated note:Activated
    case Available = 10 // name:Available note:Available
    case AvailableToRedeem = 14 // name:Available to Redeem note:Available to Redeem
    case AwaitingShipment = 17 // name:Awaiting Shipment note: Awaiting Shipment
    case ConfirmDetails = 12 // name:Confirm Details note:Confirm Details
    case ContinueActivation = 16 // name:Continue Activation note:Continue Activation
    case Delinked = 5 // name:Delinked note:Delinked
    case Done = 3 // name:Done note:Done
    case InProgress = 2 // name:In Progress note:In Progress
    case Linked = 4 // name:Linked note:Linked
    case NotIssued = 13 // name:Not Issued note:Not Issued
    case NotStarted = 1 // name:Not Started note:Not Started
    case PaymentsNotProcessed = 11 // name:Payments Not Processed note:Payment attempted but could not be completed due to an error
    case Pending = 15 // name:Pending note:Pending
    
    public static let allValues: [CardStatusTypeRef] = [CardStatusTypeRef.Unknown, CardStatusTypeRef.Achieved, CardStatusTypeRef.Activated, CardStatusTypeRef.Available, CardStatusTypeRef.AvailableToRedeem, CardStatusTypeRef.AwaitingShipment, CardStatusTypeRef.ConfirmDetails, CardStatusTypeRef.ContinueActivation, CardStatusTypeRef.Delinked, CardStatusTypeRef.Done, CardStatusTypeRef.InProgress, CardStatusTypeRef.Linked, CardStatusTypeRef.NotIssued, CardStatusTypeRef.NotStarted, CardStatusTypeRef.PaymentsNotProcessed, CardStatusTypeRef.Pending]
    
}
