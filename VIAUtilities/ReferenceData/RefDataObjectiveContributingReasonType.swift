//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum ObjectiveContributingReasonTypeRef: Int, Hashable { 
    case Unknown = -1
    case HigherValueExisting = 2 // name:Higher value points entry exists for the same day note:Higher value points entry exists for the same day 
    case HighestForDay = 1 // name:Highest points contribution for the day of all contributing points entries note:Highest points contribution for the day of all contributing points entries 

    public static let allValues: [ObjectiveContributingReasonTypeRef] = [ObjectiveContributingReasonTypeRef.Unknown, ObjectiveContributingReasonTypeRef.HigherValueExisting, ObjectiveContributingReasonTypeRef.HighestForDay]

}
