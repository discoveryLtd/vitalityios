//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum PurchaseItemStatusCategoryRef: Int, Hashable { 
    case Unknown = -1
    case Order = 1 // name:Order note:Purchase status category relating to the order status of the purchase item 

    public static let allValues: [PurchaseItemStatusCategoryRef] = [PurchaseItemStatusCategoryRef.Unknown, PurchaseItemStatusCategoryRef.Order]

}
