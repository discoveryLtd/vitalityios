//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum EventExternalReferenceTypeRef: Int, Hashable { 
    case Unknown = -1
    case GDLSessionId = 1 // name:The GDL session identifier for the device data  note:The GDL session identifier for the device data  

    public static let allValues: [EventExternalReferenceTypeRef] = [EventExternalReferenceTypeRef.Unknown, EventExternalReferenceTypeRef.GDLSessionId]

}
