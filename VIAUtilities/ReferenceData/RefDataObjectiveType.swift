//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum ObjectiveTypeRef: Int, Hashable { 
    case Unknown = -1
    case EventCompletion = 2 // name:Events Completion objective note:Events Completion objective 
    case EventOutcome = 3 // name:Event Outcome objective note:Event Outcome objective 
    case Points = 1 // name:Points based objective note:Points based objective 

    public static let allValues: [ObjectiveTypeRef] = [ObjectiveTypeRef.Unknown, ObjectiveTypeRef.EventCompletion, ObjectiveTypeRef.EventOutcome, ObjectiveTypeRef.Points]

}
