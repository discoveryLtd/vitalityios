//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum LinkTypeRef: Int, Hashable { 
    case Unknown = -1
    case DSInstructionType = 10 // name:Data Sharing user instruction link to feature note:Data Sharing user instruction link to feature 
    case EventCategory = 2 // name:Event category link to feature note:Event category link to feature 
    case EventType = 1 // name:Event type link to feature note:Event type link to feature 
    case HealthAttributeType = 4 // name:Health attribute type link to feature note:Health attribute type link to feature 
    case PointsCategory = 3 // name:Points category link to feature note:Points category link to feature 
    case PointsEntryType = 8 // name:Points Entry Type note:Points Entry Type 
    case Product = 6 // name:Product link to feature note:Product link to feature 
    case ProductSupplier = 7 // name:Product Supplier link to feature note:Defines which supplier provides the product in the context of the feature 
    case Reward = 9 // name:Reward link to feature note:Reward link to feature 
    case Status = 5 // name:Status link to feature note:Status link to feature 

    public static let allValues: [LinkTypeRef] = [LinkTypeRef.Unknown, LinkTypeRef.DSInstructionType, LinkTypeRef.EventCategory, LinkTypeRef.EventType, LinkTypeRef.HealthAttributeType, LinkTypeRef.PointsCategory, LinkTypeRef.PointsEntryType, LinkTypeRef.Product, LinkTypeRef.ProductSupplier, LinkTypeRef.Reward, LinkTypeRef.Status]

}
