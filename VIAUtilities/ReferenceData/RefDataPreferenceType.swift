//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum PreferenceTypeRef: Int, Hashable { 
    case Unknown = -1
    case EmailCommPref = 1 // name:Email Communication Preference note:Email Communication Preference 
    case Industry = 2 // name:Telecommunications note:Telecommunications 
    case StarbucksEmail = 3 // name:Starbucks email address note:Starbucks email address for active rewards 
    case ShareStatusWithEmp = 4 // name:Share Vitality Status Preferences
    case ProfilePicRef = 5 // name:Profile Picture Reference

    public static let allValues: [PreferenceTypeRef] = [PreferenceTypeRef.Unknown, PreferenceTypeRef.EmailCommPref, PreferenceTypeRef.Industry, PreferenceTypeRef.StarbucksEmail, PreferenceTypeRef.ShareStatusWithEmp, PreferenceTypeRef.ProfilePicRef]

}
