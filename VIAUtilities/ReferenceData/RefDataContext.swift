//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum ContextRef: Int, Hashable { 
    case Unknown = -1
    case ExtIncNum = 1 // name:External Incident Number note:undefined 
    case IVRRefNum = 2 // name:IVR Reference Number note:undefined 

    public static let allValues: [ContextRef] = [ContextRef.Unknown, ContextRef.ExtIncNum, ContextRef.IVRRefNum]

}
