//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum PartyAgreementRoleTypeRef: Int, Hashable { 
    case Unknown = -1
    case ADULTDEPENDENT = 9 // name:Adult Dependent note:Adult Dependent 
    case CHILD = 7 // name:Child note:Child 
    case MAINMEMBER = 6 // name:Main Member note:Main Member 
    case MEMBER = 2 // name: Member note: Member 
    case PARTICIPANT = 1 // name: Participant note: Participant 
    case PARTNER = 3 // name:Partner note:The role that the partner plays on the Partner Membership 
    case PARTNERBRANCH = 4 // name:Partner Branch note:The role that the branch plays on the Partner Membership 
    case PAYER = 5 // name:Payer note:Payer 
    case SPOUSE = 8 // name:Spouse note:Spouse 

    public static let allValues: [PartyAgreementRoleTypeRef] = [PartyAgreementRoleTypeRef.Unknown, PartyAgreementRoleTypeRef.ADULTDEPENDENT, PartyAgreementRoleTypeRef.CHILD, PartyAgreementRoleTypeRef.MAINMEMBER, PartyAgreementRoleTypeRef.MEMBER, PartyAgreementRoleTypeRef.PARTICIPANT, PartyAgreementRoleTypeRef.PARTNER, PartyAgreementRoleTypeRef.PARTNERBRANCH, PartyAgreementRoleTypeRef.PAYER, PartyAgreementRoleTypeRef.SPOUSE]

}
