//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum AssessmentStatusRef: Int, Hashable { 
    case Unknown = -1
    case Assessed = 4 // name:Assessed note:Assessed 
    case Completed = 5 // name:Completed note:Completed 
    case InProgress = 2 // name:In Progress note:In Progress 
    case New = 1 // name:New note:New 
    case Submitted = 3 // name:Submitted note:Submitted 

    public static let allValues: [AssessmentStatusRef] = [AssessmentStatusRef.Unknown, AssessmentStatusRef.Assessed, AssessmentStatusRef.Completed, AssessmentStatusRef.InProgress, AssessmentStatusRef.New, AssessmentStatusRef.Submitted]

}
