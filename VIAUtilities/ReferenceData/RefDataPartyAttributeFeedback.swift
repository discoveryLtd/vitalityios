//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum PartyAttributeFeedbackRef: Int, Hashable { 
//    case Unknown = -1
//    case AddedSaltAbove = 81 // name:Too Much note:null 
//    case AddedSaltHealthy = 80 // name:Looking Good note:null 
//    case AddedSaltPregAbove = 155 // name:Too much note:null 
//    case AddedSaltPregHealthy = 156 // name:Looking Good note:null 
//    case AlcoholAbove = 41 // name:Too High note:null 
//    case AlcoholDoNotConsume = 119 // name:You do not consume alcohol note:null 
//    case AlcoholHealthy = 40 // name:Looking Good note:null 
//    case AlcoholPregAbove = 121 // name:Too high note:null 
//    case AlcoholPregHealthy = 120 // name:Looking Good note:null 
//    case BMIAbove = 1 // name:Your BMI is above the recommended range, consider more physical activities to improve your BMI note: 
//    case BMIBelow = 3 // name:Your BMI is below the recommended range, please contact your general health practitioner note: 
//    case BMIHealthy = 2 // name:Your BMI is healthy, keep up the good work note: 
//    case BMIPregAbove = 122 // name:Too high note:null 
//    case MBIStatusAbove = 42 // name:Too High note:null 
//    case BMIStatusBelow = 44 // name:Too Low note:null 
//    case MBIStatusHealthy = 43 // name:Looking Good note:null 
//    case BMIStatusPregHealthy = 123 // name:Looking Good note:null 
//    case BloodPAbovePhysOOR = 150 // name:Too high note:null 
//    case BloodPHealthyBMIOOR = 153 // name:Looking Good note:null 
//    case BloodPHealthyPhysOOR = 149 // name:Looking Good note:null 
//    case BloodPresPregHealthy = 148 // name:Looking Good note:null 
//    case BloodPressAboveARNA = 111 // name:Too High note:null 
//    case BloodPressBMIAbove = 151 // name:Too high note:null 
//    case BloodPressHealthARNA = 110 // name:Looking Good note:null 
//    case BloodPressHealthyBMI = 152 // name:Looking Good note:null 
//    case BloodPressPregAbove = 147 // name:Too high note:null 
//    case BloodPressureAbove = 65 // name:Too High note:null 
//    case BloodPressureHealthy = 64 // name:Looking Good note:null 
//    case DiastolicAbove = 9 // name:Your Diastolic Blood Pressure is above the recommended range, improve your eating habits note: 
//    case DiastolicHealthy = 10 // name:Your Diastolic Blood Pressure is healthy, keep up the good work note: 
//    case FastGlucHealthyBMIOOR = 141 // name:Looking Good note:null 
//    case FGlucHealthyPhysOOR = 139 // name:Looking Good note:null 
//    case FGlucoseInRange = 20 // name:Your Fasting Glucose is healthy note:Your Fasting Glucose is healthy, keep up the good work 
//    case FGlucoseOutRange = 21 // name:Your Fasting Glucose is outside the recommended healthy range note:Your Fasting Glucose is outside the recommended healthy range 
//    case FastGlucAbovePhysOOR = 142 // name:Too high note:null 
//    case FastGlucBMIHealthy = 140 // name:Looking Good note:null 
//    case FastGlucHealthyARNA = 108 // name:Looking Good note:null 
//    case FastGlucPregAbove = 137 // name:Too high note:null 
//    case FastGlucPregHealthy = 138 // name:Looking Good note:null 
//    case FastingGlucAboveARNA = 109 // name:Too High note:null 
//    case FastingGlucBMIAbove = 143 // name:Too high note:null 
//    case FastingGlucosHealthy = 60 // name:Looking Good note:null 
//    case FastingGlucoseAbove = 61 // name:Too High note:null 
//    case FlexibilityBelow = 51 // name:Too Little note:null 
//    case FlexibilityGood = 50 // name:Looking Good note:null 
//    case FlexibilityPregBelow = 126 // name:Too little note:null 
//    case FlexibilityPregGood = 127 // name:Looking Good note:null 
//    case FruitConsPregBelow = 163 // name:Too low note:null 
//    case FruitConsPregHealthy = 162 // name:Looking Good note:null 
//    case FruitConsumBelow = 89 // name:Too Low note:null 
//    case FruitConsumHealthy = 88 // name:Looking Good note:null 
//    case HDLCholBelow = 72 // name:Too Low note:null 
//    case HDLCholHealthy = 71 // name:Looking Good note:null 
//    case HDLInRange = 32 // name:Congratulations note:Congratulations 
//    case HDLOutRange = 33 // name:#NAME# note:#NOTE# 
//    case HbA1cAbove = 63 // name:Too High note:null 
//    case HbA1cAbovePhysOOR = 146 // name:Too high note:null 
//    case HbA1cHealthy = 62 // name:Looking Good note:null 
//    case HbA1cHealthyPhysOOR = 145 // name:Looking Good note:null 
//    case HbA1cInRange = 18 // name:Your HbA1c reading is healthy! Keep up the good work note:Your HbA1c reading is healthy! Keep up the good work 
//    case HbA1cOutRange = 19 // name:Your HbA1c reading is outside the recommended healthy range note:Your HbA1c reading is outside the recommended healthy range 
//    case HbA1cPregAbove = 144 // name:Too high note:null 
//    case HighFatConPregHealth = 160 // name:Looking Good note:null 
//    case HighFatConsPregAbove = 161 // name:Too high note:null 
//    case HighFatConsumAbove = 87 // name:Too High note:null 
//    case HighFatConsumHealthy = 86 // name:Looking Good note:null 
//    case HighFatCookAbove = 76 // name:Too Frequent note:null 
//    case HighFatCookHealthy = 75 // name:Looking Good note:null 
//    case LDLAbove = 22 // name:Your LDL Cholesterol is above the recommended range note:Your LDL Cholesterol is above the recommended range 
//    case LDLBelow = 24 // name:Your LDL Cholesterol is below the recommended range note:Your LDL Cholesterol is below the recommended range 
//    case LDLCholAbove = 69 // name:Too High note:null 
//    case LDLCholAboveARNA = 113 // name:Too High note:null 
//    case LDLCholBelow = 70 // name:Too Low note:null 
//    case LDLCholHealthy = 68 // name:Looking Good note:null 
//    case LDLCholHealthyARNA = 112 // name:Looking Good note:null 
//    case LDLHealthy = 23 // name:Your LDL Cholesterol is healthy note:Your LDL Cholesterol is healthy 
//    case LeanMeatBelow = 93 // name:Too Low note:null 
//    case LeanMeatDoNotConsume = 168 // name:You do not eat meat note:null 
//    case LeanMeatHealthy = 92 // name:Looking Good note:null 
//    case LeanMeatPregBelow = 167 // name:Too low note:null 
//    case LeanMeatPregHealthy = 166 // name:Looking Good note:null 
//    case LipidRatioHealthy = 30 // name:Lipid Ratio Healthy note:Your lipid ratio is within the healthy range. 
//    case LipidRatioHigh = 31 // name:Lipid Ratio High note:Your lipid ratio is high. 
//    case LwFatBelow = 96 // name:Too Little note:null 
//    case LwFatDoNotConsume = 171 // name:You do not eat or drink dairy products note:null 
//    case LwFatHealthy = 95 // name:Looking Good note:null 
//    case LwFatPregBelow = 169 // name:Too little note:null 
//    case LwFatPregHealthy = 170 // name:Looking Good note:null 
//    case MWBAbove = 176 // name:Stress levels not within healthy range note:null 
//    case MWBHealthy = 175 // name:Looking Good note:null 
//    case PhysActBelowARNA = 107 // name:Too Low note:null 
//    case PhysActBelowDNL = 106 // name:Too Low note:null 
//    case PhysActHealthyARNA = 105 // name:Looking Good note:null 
//    case PhysActHealthyDNL = 104 // name:Looking Good note:null 
//    case PhysActPregBelow = 124 // name:Too low note:null 
//    case PhysActPregHealthy = 125 // name:Looking Good note:null 
//    case PhysActivityBelow = 49 // name:Looking Good note:null 
//    case PhysActivityHealthy = 48 // name:Looking Good note:null 
//    case RandomGlucoseHealthy = 27 // name:Random Glucose Healthy note:Your random glucose value is within the healthy range. 
//    case RandomGlucoseHigh = 29 // name:Random Glucose High note:Your random glucose value is high. 
//    case RandomGlucoseOk = 28 // name:Random Glucose Ok note:Your random glucose is acceptable. 
//    case SaltyFoodAbove = 83 // name:Too Frequent note:null 
//    case SaltyFoodHealthy = 82 // name:Looking Good note:null 
//    case SaltyFoodPregAbove = 157 // name:Too frequent note:null 
//    case SedBehavHealthy = 54 // name:Looking Good note:null 
//    case SedBehavPregAbove = 131 // name:Too high note:null 
//    case SedBehavPregHealthy = 130 // name:Looking Good note:null 
//    case SedentaryBehavAbove = 55 // name:Too High note:null 
//    case SleepAbove = 78 // name:Too Much note:null 
//    case SleepAboveARNA = 115 // name:Too Much note:null 
//    case SleepBelow = 79 // name:Too Little note:null 
//    case SleepBelowARNA = 116 // name:Too Little note:null 
//    case SleepHealthy = 77 // name:Looking Good note:null 
//    case SleepHealthyARNA = 114 // name:Looking Good note:null 
//    case SleepPregAbove = 154 // name:Too much note:null 
//    case SmokeHealthyExSmoker = 135 // name:You do not smoke note:null 
//    case SmokeHealthyNoSmoker = 134 // name:You do not smoke note:null 
//    case SmokingAbove = 57 // name:Too Much note:null 
//    case SmokingAboveWantQuit = 136 // name:Too much note:null 
//    case SmokingHealthy = 56 // name:Looking Good note:null 
//    case SmokingPregAbove = 132 // name:Too much note:null 
//    case SmokingPregHealthy = 133 // name:Looking Good note:null 
//    case StrengthBelow = 53 // name:Too Little note:null 
//    case StrengthGood = 52 // name:Looking Good note:null 
//    case StrengthPregBelow = 128 // name:Too little note:null 
//    case StrengthPregGood = 129 // name:Looking Good note:null 
//    case SugarDrinPregHealthy = 159 // name:Looking Good note:null 
//    case SugaryDrinkPregAbove = 158 // name:Too high note:null 
//    case SugaryDrinksAbove = 85 // name:Too High note:null 
//    case SugaryDrinksHealthy = 84 // name:Looking Good note:null 
//    case SystolicAbove = 7 // name:Your Systolic Blood Pressure is above the recommended range, improve your eating habits note: 
//    case SystolicHealthy = 8 // name:Your Systolic Blood Pressure is healthy, keep up the good work note: 
//    case TotalCholAbove = 67 // name:Too High note:null 
//    case TotalCholHealthy = 66 // name:Looking Good note:null 
//    case TotalCholesterolGood = 12 // name:Your Total Cholesterol is healthy, keep up the good work note: 
//    case TotalCholesterolHigh = 11 // name:Your Total Cholesterol is above the recommended range, improve your eating habits and eat less sugar note: 
//    case TrigCholInRange = 34 // name:Congratulations note:Congratulations 
//    case TrigCholOutRange = 35 // name:#NAME# note:#NOTE# 
//    case TriglycAbove = 74 // name:Too High note:null 
//    case TriglycHealthy = 73 // name:Looking Good note:null 
//    case UrinaryProteinIn = 25 // name:Your Urinary Protein is healthy note:Your Urinary Protein is healthy 
//    case UrinaryProteinOut = 26 // name:Your urinary Protein is outside the recommended healthy range note:Your urinary Protein is outside the recommended healthy range 
//    case UrineAbove = 178 // name:Too high note:null 
//    case UrineHealthy = 177 // name:Looking Good note:null 
//    case UrinePregAbove = 179 // name:Too high note:null 
//    case UrinePregHealthy = 174 // name:Looking Good note:null 
//    case VAgeAbove = 36 // name:Too High note:Your Vitality Age is higher than your actual age. We can work with you to improve your lifestyle habits so that you can enjoy better health and wellbeing. 
//    case VAgeAboveVHCComplete = 100 // name:Too High note:null 
//    case VAgeAboveVHCNotCompl = 101 // name:Too High note:null 
//    case VAgeBelow = 38 // name:Looking great! note:Your Vitality Age is lower than your actual age. You are probably engaged in a healthy lifestyle. Keep up the good work while you explore other areas where you may be able to improve even further. 
//    case VAgeBelowVHCComplete = 117 // name:Looking Great note:null 
//    case VAgeBelowVHCNotCompl = 118 // name:Looking Great note:null 
//    case VAgeHealthy = 37 // name:Looking good! note:Your Vitality Age is the same as your actual age. You are probably practising healthy lifestyle habits and have most measures under control.  There may be a few areas in your life where you can improve even more. 
//    case VAgeHealthyVHCComplete = 102 // name:Looking Good note:null 
//    case VAgeHealthyVHCNotCompl = 103 // name:Looking Good note:null 
//    case VAgeNotEnoughData = 39 // name:Result Unknown note:Complete the Vitality Health Review to unlock your Vitality Age. Keep your VHC up to date to maintain the most accurate Vitality Age. 
//    case VAgeOutdated = 99 // name:outdated note:null 
//    case VegConsPregBelow = 165 // name:Too low note:null 
//    case VegConsPregHealthy = 164 // name:Looking Good note:null 
//    case VegConsumBelow = 91 // name:Too Low note:null 
//    case VegConsumHealthy = 90 // name:Looking Good note:null 
//    case WCircumAbove = 46 // name:Too High note:null 
//    case WCircumBelow = 47 // name:Too High note:null 
//    case WCircumHealthy = 45 // name:Too Low note:null 
//    case WaistCircumAbove = 13 // name:Your Waist Circumference is above the recommended range, improve your physical activities note: 
//    case WaistCircumHealthy = 14 // name:Your Waist Circumference is healthy, keep up the good work note: 
//    case WeightAbove = 4 // name:Your Weight is above the recommended range, consider more physical activities to improve your weight note: 
//    case WeightBelow = 6 // name:Your Weight is below the recommended range, please contact your general health practitioner note: 
//    case WeightHealthy = 5 // name:Your Weight is healthy,keep up the good work note:null 
//    case WholegrPregHealthy = 173 // name:Looking Good note:null 
//    case WholegrainBelow = 98 // name:Too Low note:null 
//    case WholegrainHealthy = 97 // name:Looking Good note:null 
//    case WholegrainPregBelow = 172 // name:Too low note:null 
//    case FruitConsumAbove = 256 // name:Too High note:Too High
//    case Omega3Healthy = 292 // name:Looking Good note:Looking Good
//    case RedMeatUnhealthy = 289 // name:Too Frequent note:Too Frequent
//    case AddedSugarUnhealthy = 269 // name:Too Much note:Too Much
//    case FastFoodsUnhealthy = 281 // name:Too Frequent note:Too Frequent
//    case ProcesedFoodUnhealth = 261 // name:Too Frequent note:Too Frequent
//    case ProcesedMeatUnhealth = 285 // name:Too Frequent note:Too Frequent
//    case SaturatedUnhealthy = 297 // name:Too Frequent note:Too Frequent
//    case SugarFoodsUnhealthy = 265 // name:Too Frequent note:Too Frequent
//    case CalcPotassNoConsump = 272 // name:You do not consume calcium and potassium note:You do not consume calcium and potassium
//    case DinnerUnhealthy = 248 // name:Try to eat dinner everyday note:Try to eat dinner everyday
//    case NutritionUnknown = 304
//    case NutritionResults = 305
//    case NutritionOutdated = 306
//    case StressorRightTrack = 201
//    case StressorGettingThere = 202
//    case StressorWorkTogether = 203
//    case PsychoRightTrack = 198
//    case PsychoGettingThere = 199
//    case PsychoWorkTogether = 200
//    case SocialRightTrack = 195
//    case SocialGettingThere = 196
//    case SocialWorkTogether = 197
//    case MWBOutdated = 237
//    case MWBUnknown = 238
//
//    public static let allValues: [PartyAttributeFeedbackRef] = [PartyAttributeFeedbackRef.Unknown, PartyAttributeFeedbackRef.AddedSaltAbove, PartyAttributeFeedbackRef.AddedSaltHealthy, PartyAttributeFeedbackRef.AddedSaltPregAbove, PartyAttributeFeedbackRef.AddedSaltPregHealthy, PartyAttributeFeedbackRef.AlcoholAbove, PartyAttributeFeedbackRef.AlcoholDoNotConsume, PartyAttributeFeedbackRef.AlcoholHealthy, PartyAttributeFeedbackRef.AlcoholPregAbove, PartyAttributeFeedbackRef.AlcoholPregHealthy, PartyAttributeFeedbackRef.BMIAbove, PartyAttributeFeedbackRef.BMIBelow, PartyAttributeFeedbackRef.BMIHealthy, PartyAttributeFeedbackRef.BMIPregAbove, PartyAttributeFeedbackRef.MBIStatusAbove, PartyAttributeFeedbackRef.BMIStatusBelow, PartyAttributeFeedbackRef.MBIStatusHealthy, PartyAttributeFeedbackRef.BMIStatusPregHealthy, PartyAttributeFeedbackRef.BloodPAbovePhysOOR, PartyAttributeFeedbackRef.BloodPHealthyBMIOOR, PartyAttributeFeedbackRef.BloodPHealthyPhysOOR, PartyAttributeFeedbackRef.BloodPresPregHealthy, PartyAttributeFeedbackRef.BloodPressAboveARNA, PartyAttributeFeedbackRef.BloodPressBMIAbove, PartyAttributeFeedbackRef.BloodPressHealthARNA, PartyAttributeFeedbackRef.BloodPressHealthyBMI, PartyAttributeFeedbackRef.BloodPressPregAbove, PartyAttributeFeedbackRef.BloodPressureAbove, PartyAttributeFeedbackRef.BloodPressureHealthy, PartyAttributeFeedbackRef.DiastolicAbove, PartyAttributeFeedbackRef.DiastolicHealthy, PartyAttributeFeedbackRef.FastGlucHealthyBMIOOR, PartyAttributeFeedbackRef.FGlucHealthyPhysOOR, PartyAttributeFeedbackRef.FGlucoseInRange, PartyAttributeFeedbackRef.FGlucoseOutRange, PartyAttributeFeedbackRef.FastGlucAbovePhysOOR, PartyAttributeFeedbackRef.FastGlucBMIHealthy, PartyAttributeFeedbackRef.FastGlucHealthyARNA, PartyAttributeFeedbackRef.FastGlucPregAbove, PartyAttributeFeedbackRef.FastGlucPregHealthy, PartyAttributeFeedbackRef.FastingGlucAboveARNA, PartyAttributeFeedbackRef.FastingGlucBMIAbove, PartyAttributeFeedbackRef.FastingGlucosHealthy, PartyAttributeFeedbackRef.FastingGlucoseAbove, PartyAttributeFeedbackRef.FlexibilityBelow, PartyAttributeFeedbackRef.FlexibilityGood, PartyAttributeFeedbackRef.FlexibilityPregBelow, PartyAttributeFeedbackRef.FlexibilityPregGood, PartyAttributeFeedbackRef.FruitConsPregBelow, PartyAttributeFeedbackRef.FruitConsPregHealthy, PartyAttributeFeedbackRef.FruitConsumBelow, PartyAttributeFeedbackRef.FruitConsumHealthy, PartyAttributeFeedbackRef.HDLCholBelow, PartyAttributeFeedbackRef.HDLCholHealthy, PartyAttributeFeedbackRef.HDLInRange, PartyAttributeFeedbackRef.HDLOutRange, PartyAttributeFeedbackRef.HbA1cAbove, PartyAttributeFeedbackRef.HbA1cAbovePhysOOR, PartyAttributeFeedbackRef.HbA1cHealthy, PartyAttributeFeedbackRef.HbA1cHealthyPhysOOR, PartyAttributeFeedbackRef.HbA1cInRange, PartyAttributeFeedbackRef.HbA1cOutRange, PartyAttributeFeedbackRef.HbA1cPregAbove, PartyAttributeFeedbackRef.HighFatConPregHealth, PartyAttributeFeedbackRef.HighFatConsPregAbove, PartyAttributeFeedbackRef.HighFatConsumAbove, PartyAttributeFeedbackRef.HighFatConsumHealthy, PartyAttributeFeedbackRef.HighFatCookAbove, PartyAttributeFeedbackRef.HighFatCookHealthy, PartyAttributeFeedbackRef.LDLAbove, PartyAttributeFeedbackRef.LDLBelow, PartyAttributeFeedbackRef.LDLCholAbove, PartyAttributeFeedbackRef.LDLCholAboveARNA, PartyAttributeFeedbackRef.LDLCholBelow, PartyAttributeFeedbackRef.LDLCholHealthy, PartyAttributeFeedbackRef.LDLCholHealthyARNA, PartyAttributeFeedbackRef.LDLHealthy, PartyAttributeFeedbackRef.LeanMeatBelow, PartyAttributeFeedbackRef.LeanMeatDoNotConsume, PartyAttributeFeedbackRef.LeanMeatHealthy, PartyAttributeFeedbackRef.LeanMeatPregBelow, PartyAttributeFeedbackRef.LeanMeatPregHealthy, PartyAttributeFeedbackRef.LipidRatioHealthy, PartyAttributeFeedbackRef.LipidRatioHigh, PartyAttributeFeedbackRef.LwFatBelow, PartyAttributeFeedbackRef.LwFatDoNotConsume, PartyAttributeFeedbackRef.LwFatHealthy, PartyAttributeFeedbackRef.LwFatPregBelow, PartyAttributeFeedbackRef.LwFatPregHealthy, PartyAttributeFeedbackRef.MWBAbove, PartyAttributeFeedbackRef.MWBHealthy, PartyAttributeFeedbackRef.PhysActBelowARNA, PartyAttributeFeedbackRef.PhysActBelowDNL, PartyAttributeFeedbackRef.PhysActHealthyARNA, PartyAttributeFeedbackRef.PhysActHealthyDNL, PartyAttributeFeedbackRef.PhysActPregBelow, PartyAttributeFeedbackRef.PhysActPregHealthy, PartyAttributeFeedbackRef.PhysActivityBelow, PartyAttributeFeedbackRef.PhysActivityHealthy, PartyAttributeFeedbackRef.RandomGlucoseHealthy, PartyAttributeFeedbackRef.RandomGlucoseHigh, PartyAttributeFeedbackRef.RandomGlucoseOk, PartyAttributeFeedbackRef.SaltyFoodAbove, PartyAttributeFeedbackRef.SaltyFoodHealthy, PartyAttributeFeedbackRef.SaltyFoodPregAbove, PartyAttributeFeedbackRef.SedBehavHealthy, PartyAttributeFeedbackRef.SedBehavPregAbove, PartyAttributeFeedbackRef.SedBehavPregHealthy, PartyAttributeFeedbackRef.SedentaryBehavAbove, PartyAttributeFeedbackRef.SleepAbove, PartyAttributeFeedbackRef.SleepAboveARNA, PartyAttributeFeedbackRef.SleepBelow, PartyAttributeFeedbackRef.SleepBelowARNA, PartyAttributeFeedbackRef.SleepHealthy, PartyAttributeFeedbackRef.SleepHealthyARNA, PartyAttributeFeedbackRef.SleepPregAbove, PartyAttributeFeedbackRef.SmokeHealthyExSmoker, PartyAttributeFeedbackRef.SmokeHealthyNoSmoker, PartyAttributeFeedbackRef.SmokingAbove, PartyAttributeFeedbackRef.SmokingAboveWantQuit, PartyAttributeFeedbackRef.SmokingHealthy, PartyAttributeFeedbackRef.SmokingPregAbove, PartyAttributeFeedbackRef.SmokingPregHealthy, PartyAttributeFeedbackRef.StrengthBelow, PartyAttributeFeedbackRef.StrengthGood, PartyAttributeFeedbackRef.StrengthPregBelow, PartyAttributeFeedbackRef.StrengthPregGood, PartyAttributeFeedbackRef.SugarDrinPregHealthy, PartyAttributeFeedbackRef.SugaryDrinkPregAbove, PartyAttributeFeedbackRef.SugaryDrinksAbove, PartyAttributeFeedbackRef.SugaryDrinksHealthy, PartyAttributeFeedbackRef.SystolicAbove, PartyAttributeFeedbackRef.SystolicHealthy, PartyAttributeFeedbackRef.TotalCholAbove, PartyAttributeFeedbackRef.TotalCholHealthy, PartyAttributeFeedbackRef.TotalCholesterolGood, PartyAttributeFeedbackRef.TotalCholesterolHigh, PartyAttributeFeedbackRef.TrigCholInRange, PartyAttributeFeedbackRef.TrigCholOutRange, PartyAttributeFeedbackRef.TriglycAbove, PartyAttributeFeedbackRef.TriglycHealthy, PartyAttributeFeedbackRef.UrinaryProteinIn, PartyAttributeFeedbackRef.UrinaryProteinOut, PartyAttributeFeedbackRef.UrineAbove, PartyAttributeFeedbackRef.UrineHealthy, PartyAttributeFeedbackRef.UrinePregAbove, PartyAttributeFeedbackRef.UrinePregHealthy, PartyAttributeFeedbackRef.VAgeAbove, PartyAttributeFeedbackRef.VAgeAboveVHCComplete, PartyAttributeFeedbackRef.VAgeAboveVHCNotCompl, PartyAttributeFeedbackRef.VAgeBelow, PartyAttributeFeedbackRef.VAgeBelowVHCComplete, PartyAttributeFeedbackRef.VAgeBelowVHCNotCompl, PartyAttributeFeedbackRef.VAgeHealthy, PartyAttributeFeedbackRef.VAgeHealthyVHCComplete, PartyAttributeFeedbackRef.VAgeHealthyVHCNotCompl, PartyAttributeFeedbackRef.VAgeNotEnoughData, PartyAttributeFeedbackRef.VAgeOutdated, PartyAttributeFeedbackRef.VegConsPregBelow, PartyAttributeFeedbackRef.VegConsPregHealthy, PartyAttributeFeedbackRef.VegConsumBelow, PartyAttributeFeedbackRef.VegConsumHealthy, PartyAttributeFeedbackRef.WCircumAbove, PartyAttributeFeedbackRef.WCircumBelow, PartyAttributeFeedbackRef.WCircumHealthy, PartyAttributeFeedbackRef.WaistCircumAbove, PartyAttributeFeedbackRef.WaistCircumHealthy, PartyAttributeFeedbackRef.WeightAbove, PartyAttributeFeedbackRef.WeightBelow, PartyAttributeFeedbackRef.WeightHealthy, PartyAttributeFeedbackRef.WholegrPregHealthy, PartyAttributeFeedbackRef.WholegrainBelow, PartyAttributeFeedbackRef.WholegrainHealthy, PartyAttributeFeedbackRef.WholegrainPregBelow, PartyAttributeFeedbackRef.FruitConsumAbove, PartyAttributeFeedbackRef.Omega3Healthy, PartyAttributeFeedbackRef.RedMeatUnhealthy, PartyAttributeFeedbackRef.AddedSugarUnhealthy, PartyAttributeFeedbackRef.FastFoodsUnhealthy, PartyAttributeFeedbackRef.ProcesedFoodUnhealth, PartyAttributeFeedbackRef.ProcesedMeatUnhealth, PartyAttributeFeedbackRef.SaturatedUnhealthy, PartyAttributeFeedbackRef.SugarFoodsUnhealthy, PartyAttributeFeedbackRef.CalcPotassNoConsump, PartyAttributeFeedbackRef.DinnerUnhealthy, PartyAttributeFeedbackRef.NutritionUnknown, PartyAttributeFeedbackRef.NutritionResults, PartyAttributeFeedbackRef.NutritionOutdated, PartyAttributeFeedbackRef.StressorRightTrack, PartyAttributeFeedbackRef.StressorGettingThere, PartyAttributeFeedbackRef.StressorWorkTogether, PartyAttributeFeedbackRef.PsychoRightTrack, PartyAttributeFeedbackRef.PsychoGettingThere, PartyAttributeFeedbackRef.PsychoWorkTogether, PartyAttributeFeedbackRef.SocialRightTrack, PartyAttributeFeedbackRef.SocialGettingThere, PartyAttributeFeedbackRef.SocialWorkTogether, PartyAttributeFeedbackRef.MWBOutdated, PartyAttributeFeedbackRef.MWBUnknown]

    // Reordered and updated.
    case Unknown = -1
    case BMIAbove = 1
    case BMIHealthy = 2
    case BMIBelow = 3
    case WeightAbove = 4
    case WeightHealthy = 5
    case WeightBelow = 6
    case SystolicAbove = 7
    case SystolicHealthy = 8
    case DiastolicAbove = 9
    case DiastolicHealthy = 10
    case TotalCholesterolHigh = 11
    case TotalCholesterolGood = 12
    case WaistCircumAbove = 13
    case WaistCircumHealthy = 14
    case HbA1cInRange = 18
    case HbA1cOutRange = 19
    case FGlucoseInRange = 20
    case FGlucoseOutRange = 21
    case LDLAbove = 22
    case LDLHealthy = 23
    case LDLBelow = 24
    case UrinaryProteinIn = 25
    case UrinaryProteinOut = 26
    case RandomGlucoseHealthy = 27
    case RandomGlucoseOk = 28
    case RandomGlucoseHigh = 29
    case LipidRatioHealthy = 30
    case LipidRatioHigh = 31
    case HDLInRange = 32
    case HDLOutRange = 33
    case TrigCholInRange = 34
    case TrigCholOutRange = 35
    case VAgeAbove = 36
    case VAgeHealthy = 37
    case VAgeBelow = 38
    case VAgeNotEnoughData = 39
    case AlcoholHealthy = 40
    case AlcoholAbove = 41
    case MBIStatusAbove = 42
    case MBIStatusHealthy = 43
    case BMIStatusBelow = 44
    case WCircumHealthy = 45
    case WCircumAbove = 46
    case WCircumBelow = 47
    case PhysActivityHealthy = 48
    case PhysActivityBelow = 49
    case FlexibilityGood = 50
    case FlexibilityBelow = 51
    case StrengthGood = 52
    case StrengthBelow = 53
    case SedBehavHealthy = 54
    case SedentaryBehavAbove = 55
    case SmokingHealthy = 56
    case SmokingAbove = 57
    case FastingGlucosHealthy = 60
    case FastingGlucoseAbove = 61
    case HbA1cHealthy = 62
    case HbA1cAbove = 63
    case BloodPressureHealthy = 64
    case BloodPressureAbove = 65
    case TotalCholHealthy = 66
    case TotalCholAbove = 67
    case LDLCholHealthy = 68
    case LDLCholAbove = 69
    case LDLCholBelow = 70
    case HDLCholHealthy = 71
    case HDLCholBelow = 72
    case TriglycHealthy = 73
    case TriglycAbove = 74
    case HighFatCookHealthy = 75
    case HighFatCookAbove = 76
    case SleepHealthy = 77
    case SleepAbove = 78
    case SleepBelow = 79
    case AddedSaltHealthy = 80
    case AddedSaltAbove = 81
    case SaltyFoodHealthy = 82
    case SaltyFoodAbove = 83
    case SugaryDrinksHealthy = 84
    case SugaryDrinksAbove = 85
    case HighFatConsumHealthy = 86
    case HighFatConsumAbove = 87
    case FruitConsumHealthy = 88
    case FruitConsumBelow = 89
    case VegConsumHealthy = 90
    case VegConsumBelow = 91
    case LeanMeatHealthy = 92
    case LeanMeatBelow = 93
    case LwFatHealthy = 95
    case LwFatBelow = 96
    case WholegrainHealthy = 97
    case WholegrainBelow = 98
    case VAgeOutdated = 99
    case VAgeAboveVHCComplete = 100
    case VAgeAboveVHCNotCompl = 101
    case VAgeHealthyVHCComplete = 102
    case VAgeHealthyVHCNotCompl = 103
    case PhysActHealthyDNL = 104
    case PhysActHealthyARNA = 105
    case PhysActBelowDNL = 106
    case PhysActBelowARNA = 107
    case FastGlucHealthyARNA = 108
    case FastingGlucAboveARNA = 109
    case BloodPressHealthARNA = 110
    case BloodPressAboveARNA = 111
    case LDLCholHealthyARNA = 112
    case LDLCholAboveARNA = 113
    case SleepHealthyARNA = 114
    case SleepAboveARNA = 115
    case SleepBelowARNA = 116
    case VAgeBelowVHCComplete = 117
    case VAgeBelowVHCNotCompl = 118
    case AlcoholDoNotConsume = 119
    case AlcoholPregHealthy = 120
    case AlcoholPregAbove = 121
    case BMIPregAbove = 122
    case BMIStatusPregHealthy = 123
    case PhysActPregBelow = 124
    case PhysActPregHealthy = 125
    case FlexibilityPregBelow = 126
    case FlexibilityPregGood = 127
    case StrengthPregBelow = 128
    case StrengthPregGood = 129
    case SedBehavPregHealthy = 130
    case SedBehavPregAbove = 131
    case SmokingPregAbove = 132
    case SmokingPregHealthy = 133
    case SmokeHealthyNoSmoker = 134
    case SmokeHealthyExSmoker = 135
    case SmokingAboveWantQuit = 136
    case FastGlucPregAbove = 137
    case FastGlucPregHealthy = 138
    case FGlucHealthyPhysOOR = 139
    case FastGlucBMIHealthy = 140
    case FastGlucHealthyBMIOOR = 141
    case FastGlucAbovePhysOOR = 142
    case FastingGlucBMIAbove = 143
    case HbA1cPregAbove = 144
    case HbA1cHealthyPhysOOR = 145
    case HbA1cAbovePhysOOR = 146
    case BloodPressPregAbove = 147
    case Bloodprespreghealthy = 148
    case BloodPHealthyPhysOOR = 149
    case BloodPAbovePhysOOR = 150
    case BloodPressBMIAbove = 151
    case BloodPressHealthyBMI = 152
    case BloodPHealthyBMIOOR = 153
    case SleepPregAbove = 154
    case AddedSaltPregAbove = 155
    case AddedSaltPregHealthy = 156
    case SaltyFoodPregAbove = 157
    case SugaryDrinkPregAbove = 158
    case SugarDrinPregHealthy = 159
    case HighFatConPregHealth = 160
    case HighFatConsPregAbove = 161
    case FruitConsPregHealthy = 162
    case FruitConsPregBelow = 163
    case VegConsPregHealthy = 164
    case VegConsPregBelow = 165
    case LeanMeatPregHealthy = 166
    case LeanMeatPregBelow = 167
    case LeanMeatDoNotConsume = 168
    case LwFatPregBelow = 169
    case LwFatPregHealthy = 170
    case LwFatDoNotConsume = 171
    case WholegrainPregBelow = 172
    case WholegrPregHealthy = 173
    case UrinePregHealthy = 174
    case MWBHealthy = 175
    case MWBAbove = 176
    case UrineHealthy = 177
    case UrineAbove = 178
    case UrinePregAbove = 179
    case HbA1cPregHealthy = 180
    case LDLCholBelowARNA = 181
    case LDLCholPregHealthy = 182
    case LDLCholPregAbove = 183
    case LDLCholPregBelow = 184
    case SleepPregHealthy = 185
    case SaltyFoodPregHealthy = 186
    case NoData = 187
    case HbA1cHealthyPhysGood = 188
    case SleepPregNoData = 189
    case SleepPregBelow = 190
    case WCircumPreg = 191
    case MWBPregHealthy = 192
    case MWBPregAbove = 193
    case MWBPregNoData = 194
    case SocialGettingThere = 196
    case SocialRightTrack = 195
    case SocialWorkTogether = 197
    case PsychoRightTrack = 198
    case PsychoGettingThere = 199
    case PsychoWorkTogether = 200
    case StressorRightTrack = 201
    case StressorGettingThere = 202
    case StressorWorkTogether = 203
    case MeSingleNotAtAll = 204
    case MeSinToSomeExtent = 205
    case MeSinQuiteSeriously = 206
    case MeSinSeverely = 207
    case MeMultiNotAtAll = 208
    case MeMultiToSomeExtent = 209
    case MeMultiSerious = 210
    case MeMultiSeverely = 211
    case FamilySinNotAtAll = 212
    case FamSinSomeExtent = 213
    case FamSinQuiteSerious = 214
    case FamilySinSeverely = 215
    case FamMultiNotAtAll = 216
    case FamMultiSomeExtent = 217
    case FamMultiQuiteSerious = 218
    case FamilyMultiSeverely = 219
    case MeFamilySinNotAtAll = 220
    case MeFamilyToSomeExtent = 221
    case MeFamQuiteSeriously = 222
    case MeFamSingleSeverely = 223
    case MeSinFamMulNotAtAll = 224
    case MeSinFamMulSomExt = 225
    case MeSinFamMultiSerious = 226
    case MeSinFamMultiSevere = 227
    case MeMulFamSinNotAtAll = 228
    case MeMultiFamSinExtent = 229
    case MeMultiFamSinSerious = 230
    case MeMultiFamSinSevere = 231
    case MeMulFamMulNotAtAll = 232
    case MeMulFamMulSomeExten = 233
    case MeMulFamMulSeriously = 234
    case MeMulFamMulSeverely = 235
    case None = 236
    case MWBOutdated = 237
    case MWBUnknown = 238
    case BreakfastHealthy = 239
    case BreakfastUnhealthy = 240
    case BreakfastPregHealthy = 241
    case BreakPregUnhealthy = 242
    case LunchHealthy = 243
    case LunchUnhealthy = 244
    case LunchPregHealthy = 245
    case LunchPregUnhealthy = 246
    case DinnerHealthy = 247
    case DinnerUnhealthy = 248
    case DinnerPregHealthy = 249
    case DinnerPregUnhealthy = 250
    case SnackingHealthy = 251
    case SnackingUnhealthy = 252
    case SnackingPregHealthy = 253
    case SnackPregUnhealthy = 254
    case SnackNoData = 255
    case FruitConsumAbove = 256
    case FruitConsPregAbove = 257
    case WholegrainAbove = 258
    case WholegrPregAbove = 259
    case ProcessedFoodHealth = 260
    case ProcesedFoodUnhealth = 261
    case PrcsdFoodPregHealth = 262
    case PrsdFoodPregUnhealth = 263
    case SugarFoodsHealthy = 264
    case SugarFoodsUnhealthy = 265
    case SugarFoodPregHealthy = 266
    case SugarFodPregUnhealth = 267
    case AddedSugarHealthy = 268
    case AddedSugarUnhealthy = 269
    case AddedSugarPregHealth = 270
    case AddSugarPregUnhealth = 271
    case CalcPotassNoConsump = 272
    case CalcPotassPregNoCons = 273
    case CalciumPotassHealthy = 274
    case CalciumPotassBelow = 275
    case CalciumPotassAbove = 276
    case CalcPotassPregHealth = 277
    case CalcPotassPregBelow = 278
    case CalcPotassPregAbove = 279
    case FastFoodsHealthy = 280
    case FastFoodsUnhealthy = 281
    case FastFoodsPregHealthy = 282
    case FastFoodPregUnhealth = 283
    case ProcessedMeatHealthy = 284
    case ProcesedMeatUnhealth = 285
    case PrcssdMeatPregHealth = 286
    case PrsdMeatPregUnhealth = 287
    case RedMeatHealthy = 288
    case RedMeatUnhealthy = 289
    case RedMeatPregHealthy = 290
    case RedMeatPregUnhealthy = 291
    case Omega3Healthy = 292
    case Omega3Unhealthy = 293
    case Omega3PregHealthy = 294
    case Omega3PregUnhealthy = 295
    case SaturatedFatsHealthy = 296
    case SaturatedUnhealthy = 297
    case SaturatedPregHealth = 298
    case SaturatdPregUnhealth = 299
    case UnsaturatedHealth = 300
    case UnsaturatedUnhealthy = 301
    case UnsatFatsPregHealth = 302
    case UnsatFatPregUnhealth = 303
    case NutritionUnknown = 304
    case NutritionResults = 305
    case NutritionOutdated = 306
    case PsychOutdated = 307
    case SocialOutdated = 308
    case PositiveLow = 309
    case PositiveMedium = 310
    case PositiveHigh = 311
    case EnergyLow = 312
    case EnergyMedium = 313
    case EnergyHigh = 314
    case NegativeLow = 315
    case NegativeMedium = 316
    case NegativeHigh = 317
    case AnxietyLow = 318
    case AnxietyMedium = 319
    case AnxietyHigh = 320
    
    public static let allValues: [PartyAttributeFeedbackRef] = [
        PartyAttributeFeedbackRef.Unknown,
        PartyAttributeFeedbackRef.BMIAbove,
        PartyAttributeFeedbackRef.BMIHealthy,
        PartyAttributeFeedbackRef.BMIBelow,
        PartyAttributeFeedbackRef.WeightAbove,
        PartyAttributeFeedbackRef.WeightHealthy,
        PartyAttributeFeedbackRef.WeightBelow,
        PartyAttributeFeedbackRef.SystolicAbove,
        PartyAttributeFeedbackRef.SystolicHealthy,
        PartyAttributeFeedbackRef.DiastolicAbove,
        PartyAttributeFeedbackRef.DiastolicHealthy,
        PartyAttributeFeedbackRef.TotalCholesterolHigh,
        PartyAttributeFeedbackRef.TotalCholesterolGood,
        PartyAttributeFeedbackRef.WaistCircumAbove,
        PartyAttributeFeedbackRef.WaistCircumHealthy,
        PartyAttributeFeedbackRef.HbA1cInRange,
        PartyAttributeFeedbackRef.HbA1cOutRange,
        PartyAttributeFeedbackRef.FGlucoseInRange,
        PartyAttributeFeedbackRef.FGlucoseOutRange,
        PartyAttributeFeedbackRef.LDLAbove,
        PartyAttributeFeedbackRef.LDLHealthy,
        PartyAttributeFeedbackRef.LDLBelow,
        PartyAttributeFeedbackRef.UrinaryProteinIn,
        PartyAttributeFeedbackRef.UrinaryProteinOut,
        PartyAttributeFeedbackRef.RandomGlucoseHealthy,
        PartyAttributeFeedbackRef.RandomGlucoseOk,
        PartyAttributeFeedbackRef.RandomGlucoseHigh,
        PartyAttributeFeedbackRef.LipidRatioHealthy,
        PartyAttributeFeedbackRef.LipidRatioHigh,
        PartyAttributeFeedbackRef.HDLInRange,
        PartyAttributeFeedbackRef.HDLOutRange,
        PartyAttributeFeedbackRef.TrigCholInRange,
        PartyAttributeFeedbackRef.TrigCholOutRange,
        PartyAttributeFeedbackRef.VAgeAbove,
        PartyAttributeFeedbackRef.VAgeHealthy,
        PartyAttributeFeedbackRef.VAgeBelow,
        PartyAttributeFeedbackRef.VAgeNotEnoughData,
        PartyAttributeFeedbackRef.AlcoholHealthy,
        PartyAttributeFeedbackRef.AlcoholAbove,
        PartyAttributeFeedbackRef.MBIStatusAbove,
        PartyAttributeFeedbackRef.MBIStatusHealthy,
        PartyAttributeFeedbackRef.BMIStatusBelow,
        PartyAttributeFeedbackRef.WCircumHealthy,
        PartyAttributeFeedbackRef.WCircumAbove,
        PartyAttributeFeedbackRef.WCircumBelow,
        PartyAttributeFeedbackRef.PhysActivityHealthy,
        PartyAttributeFeedbackRef.PhysActivityBelow,
        PartyAttributeFeedbackRef.FlexibilityGood,
        PartyAttributeFeedbackRef.FlexibilityBelow,
        PartyAttributeFeedbackRef.StrengthGood,
        PartyAttributeFeedbackRef.StrengthBelow,
        PartyAttributeFeedbackRef.SedBehavHealthy,
        PartyAttributeFeedbackRef.SedentaryBehavAbove,
        PartyAttributeFeedbackRef.SmokingHealthy,
        PartyAttributeFeedbackRef.SmokingAbove,
        PartyAttributeFeedbackRef.FastingGlucosHealthy,
        PartyAttributeFeedbackRef.FastingGlucoseAbove,
        PartyAttributeFeedbackRef.HbA1cHealthy,
        PartyAttributeFeedbackRef.HbA1cAbove,
        PartyAttributeFeedbackRef.BloodPressureHealthy,
        PartyAttributeFeedbackRef.BloodPressureAbove,
        PartyAttributeFeedbackRef.TotalCholHealthy,
        PartyAttributeFeedbackRef.TotalCholAbove,
        PartyAttributeFeedbackRef.LDLCholHealthy,
        PartyAttributeFeedbackRef.LDLCholAbove,
        PartyAttributeFeedbackRef.LDLCholBelow,
        PartyAttributeFeedbackRef.HDLCholHealthy,
        PartyAttributeFeedbackRef.HDLCholBelow,
        PartyAttributeFeedbackRef.TriglycHealthy,
        PartyAttributeFeedbackRef.TriglycAbove,
        PartyAttributeFeedbackRef.HighFatCookHealthy,
        PartyAttributeFeedbackRef.HighFatCookAbove,
        PartyAttributeFeedbackRef.SleepHealthy,
        PartyAttributeFeedbackRef.SleepAbove,
        PartyAttributeFeedbackRef.SleepBelow,
        PartyAttributeFeedbackRef.AddedSaltHealthy,
        PartyAttributeFeedbackRef.AddedSaltAbove,
        PartyAttributeFeedbackRef.SaltyFoodHealthy,
        PartyAttributeFeedbackRef.SaltyFoodAbove,
        PartyAttributeFeedbackRef.SugaryDrinksHealthy,
        PartyAttributeFeedbackRef.SugaryDrinksAbove,
        PartyAttributeFeedbackRef.HighFatConsumHealthy,
        PartyAttributeFeedbackRef.HighFatConsumAbove,
        PartyAttributeFeedbackRef.FruitConsumHealthy,
        PartyAttributeFeedbackRef.FruitConsumBelow,
        PartyAttributeFeedbackRef.VegConsumHealthy,
        PartyAttributeFeedbackRef.VegConsumBelow,
        PartyAttributeFeedbackRef.LeanMeatHealthy,
        PartyAttributeFeedbackRef.LeanMeatBelow,
        PartyAttributeFeedbackRef.LwFatHealthy,
        PartyAttributeFeedbackRef.LwFatBelow,
        PartyAttributeFeedbackRef.WholegrainHealthy,
        PartyAttributeFeedbackRef.WholegrainBelow,
        PartyAttributeFeedbackRef.VAgeOutdated,
        PartyAttributeFeedbackRef.VAgeAboveVHCComplete,
        PartyAttributeFeedbackRef.VAgeAboveVHCNotCompl,
        PartyAttributeFeedbackRef.VAgeHealthyVHCComplete,
        PartyAttributeFeedbackRef.VAgeHealthyVHCNotCompl,
        PartyAttributeFeedbackRef.PhysActHealthyDNL,
        PartyAttributeFeedbackRef.PhysActHealthyARNA,
        PartyAttributeFeedbackRef.PhysActBelowDNL,
        PartyAttributeFeedbackRef.PhysActBelowARNA,
        PartyAttributeFeedbackRef.FastGlucHealthyARNA,
        PartyAttributeFeedbackRef.FastingGlucAboveARNA,
        PartyAttributeFeedbackRef.BloodPressHealthARNA,
        PartyAttributeFeedbackRef.BloodPressAboveARNA,
        PartyAttributeFeedbackRef.LDLCholHealthyARNA,
        PartyAttributeFeedbackRef.LDLCholAboveARNA,
        PartyAttributeFeedbackRef.SleepHealthyARNA,
        PartyAttributeFeedbackRef.SleepAboveARNA,
        PartyAttributeFeedbackRef.SleepBelowARNA,
        PartyAttributeFeedbackRef.VAgeBelowVHCComplete,
        PartyAttributeFeedbackRef.VAgeBelowVHCNotCompl,
        PartyAttributeFeedbackRef.AlcoholDoNotConsume,
        PartyAttributeFeedbackRef.AlcoholPregHealthy,
        PartyAttributeFeedbackRef.AlcoholPregAbove,
        PartyAttributeFeedbackRef.BMIPregAbove,
        PartyAttributeFeedbackRef.BMIStatusPregHealthy,
        PartyAttributeFeedbackRef.PhysActPregBelow,
        PartyAttributeFeedbackRef.PhysActPregHealthy,
        PartyAttributeFeedbackRef.FlexibilityPregBelow,
        PartyAttributeFeedbackRef.FlexibilityPregGood,
        PartyAttributeFeedbackRef.StrengthPregBelow,
        PartyAttributeFeedbackRef.StrengthPregGood,
        PartyAttributeFeedbackRef.SedBehavPregHealthy,
        PartyAttributeFeedbackRef.SedBehavPregAbove,
        PartyAttributeFeedbackRef.SmokingPregAbove,
        PartyAttributeFeedbackRef.SmokingPregHealthy,
        PartyAttributeFeedbackRef.SmokeHealthyNoSmoker,
        PartyAttributeFeedbackRef.SmokeHealthyExSmoker,
        PartyAttributeFeedbackRef.SmokingAboveWantQuit,
        PartyAttributeFeedbackRef.FastGlucPregAbove,
        PartyAttributeFeedbackRef.FastGlucPregHealthy,
        PartyAttributeFeedbackRef.FGlucHealthyPhysOOR,
        PartyAttributeFeedbackRef.FastGlucBMIHealthy,
        PartyAttributeFeedbackRef.FastGlucHealthyBMIOOR,
        PartyAttributeFeedbackRef.FastGlucAbovePhysOOR,
        PartyAttributeFeedbackRef.FastingGlucBMIAbove,
        PartyAttributeFeedbackRef.HbA1cPregAbove,
        PartyAttributeFeedbackRef.HbA1cHealthyPhysOOR,
        PartyAttributeFeedbackRef.HbA1cAbovePhysOOR,
        PartyAttributeFeedbackRef.BloodPressPregAbove,
        PartyAttributeFeedbackRef.Bloodprespreghealthy,
        PartyAttributeFeedbackRef.BloodPHealthyPhysOOR,
        PartyAttributeFeedbackRef.BloodPAbovePhysOOR,
        PartyAttributeFeedbackRef.BloodPressBMIAbove,
        PartyAttributeFeedbackRef.BloodPressHealthyBMI,
        PartyAttributeFeedbackRef.BloodPHealthyBMIOOR,
        PartyAttributeFeedbackRef.SleepPregAbove,
        PartyAttributeFeedbackRef.AddedSaltPregAbove,
        PartyAttributeFeedbackRef.AddedSaltPregHealthy,
        PartyAttributeFeedbackRef.SaltyFoodPregAbove,
        PartyAttributeFeedbackRef.SugaryDrinkPregAbove,
        PartyAttributeFeedbackRef.SugarDrinPregHealthy,
        PartyAttributeFeedbackRef.HighFatConPregHealth,
        PartyAttributeFeedbackRef.HighFatConsPregAbove,
        PartyAttributeFeedbackRef.FruitConsPregHealthy,
        PartyAttributeFeedbackRef.FruitConsPregBelow,
        PartyAttributeFeedbackRef.VegConsPregHealthy,
        PartyAttributeFeedbackRef.VegConsPregBelow,
        PartyAttributeFeedbackRef.LeanMeatPregHealthy,
        PartyAttributeFeedbackRef.LeanMeatPregBelow,
        PartyAttributeFeedbackRef.LeanMeatDoNotConsume,
        PartyAttributeFeedbackRef.LwFatPregBelow,
        PartyAttributeFeedbackRef.LwFatPregHealthy,
        PartyAttributeFeedbackRef.LwFatDoNotConsume,
        PartyAttributeFeedbackRef.WholegrainPregBelow,
        PartyAttributeFeedbackRef.WholegrPregHealthy,
        PartyAttributeFeedbackRef.UrinePregHealthy,
        PartyAttributeFeedbackRef.MWBHealthy,
        PartyAttributeFeedbackRef.MWBAbove,
        PartyAttributeFeedbackRef.UrineHealthy,
        PartyAttributeFeedbackRef.UrineAbove,
        PartyAttributeFeedbackRef.UrinePregAbove,
        PartyAttributeFeedbackRef.HbA1cPregHealthy,
        PartyAttributeFeedbackRef.LDLCholBelowARNA,
        PartyAttributeFeedbackRef.LDLCholPregHealthy,
        PartyAttributeFeedbackRef.LDLCholPregAbove,
        PartyAttributeFeedbackRef.LDLCholPregBelow,
        PartyAttributeFeedbackRef.SleepPregHealthy,
        PartyAttributeFeedbackRef.SaltyFoodPregHealthy,
        PartyAttributeFeedbackRef.NoData,
        PartyAttributeFeedbackRef.HbA1cHealthyPhysGood,
        PartyAttributeFeedbackRef.SleepPregNoData,
        PartyAttributeFeedbackRef.SleepPregBelow,
        PartyAttributeFeedbackRef.WCircumPreg,
        PartyAttributeFeedbackRef.MWBPregHealthy,
        PartyAttributeFeedbackRef.MWBPregAbove,
        PartyAttributeFeedbackRef.MWBPregNoData,
        PartyAttributeFeedbackRef.SocialGettingThere,
        PartyAttributeFeedbackRef.SocialRightTrack,
        PartyAttributeFeedbackRef.SocialWorkTogether,
        PartyAttributeFeedbackRef.PsychoRightTrack,
        PartyAttributeFeedbackRef.PsychoGettingThere,
        PartyAttributeFeedbackRef.PsychoWorkTogether,
        PartyAttributeFeedbackRef.StressorRightTrack,
        PartyAttributeFeedbackRef.StressorGettingThere,
        PartyAttributeFeedbackRef.StressorWorkTogether,
        PartyAttributeFeedbackRef.MeSingleNotAtAll,
        PartyAttributeFeedbackRef.MeSinToSomeExtent,
        PartyAttributeFeedbackRef.MeSinQuiteSeriously,
        PartyAttributeFeedbackRef.MeSinSeverely,
        PartyAttributeFeedbackRef.MeMultiNotAtAll,
        PartyAttributeFeedbackRef.MeMultiToSomeExtent,
        PartyAttributeFeedbackRef.MeMultiSerious,
        PartyAttributeFeedbackRef.MeMultiSeverely,
        PartyAttributeFeedbackRef.FamilySinNotAtAll,
        PartyAttributeFeedbackRef.FamSinSomeExtent,
        PartyAttributeFeedbackRef.FamSinQuiteSerious,
        PartyAttributeFeedbackRef.FamilySinSeverely,
        PartyAttributeFeedbackRef.FamMultiNotAtAll,
        PartyAttributeFeedbackRef.FamMultiSomeExtent,
        PartyAttributeFeedbackRef.FamMultiQuiteSerious,
        PartyAttributeFeedbackRef.FamilyMultiSeverely,
        PartyAttributeFeedbackRef.MeFamilySinNotAtAll,
        PartyAttributeFeedbackRef.MeFamilyToSomeExtent,
        PartyAttributeFeedbackRef.MeFamQuiteSeriously,
        PartyAttributeFeedbackRef.MeFamSingleSeverely,
        PartyAttributeFeedbackRef.MeSinFamMulNotAtAll,
        PartyAttributeFeedbackRef.MeSinFamMulSomExt,
        PartyAttributeFeedbackRef.MeSinFamMultiSerious,
        PartyAttributeFeedbackRef.MeSinFamMultiSevere,
        PartyAttributeFeedbackRef.MeMulFamSinNotAtAll,
        PartyAttributeFeedbackRef.MeMultiFamSinExtent,
        PartyAttributeFeedbackRef.MeMultiFamSinSerious,
        PartyAttributeFeedbackRef.MeMultiFamSinSevere,
        PartyAttributeFeedbackRef.MeMulFamMulNotAtAll,
        PartyAttributeFeedbackRef.MeMulFamMulSomeExten,
        PartyAttributeFeedbackRef.MeMulFamMulSeriously,
        PartyAttributeFeedbackRef.MeMulFamMulSeverely,
        PartyAttributeFeedbackRef.None,
        PartyAttributeFeedbackRef.MWBOutdated,
        PartyAttributeFeedbackRef.MWBUnknown,
        PartyAttributeFeedbackRef.BreakfastHealthy,
        PartyAttributeFeedbackRef.BreakfastUnhealthy,
        PartyAttributeFeedbackRef.BreakfastPregHealthy,
        PartyAttributeFeedbackRef.BreakPregUnhealthy,
        PartyAttributeFeedbackRef.LunchHealthy,
        PartyAttributeFeedbackRef.LunchUnhealthy,
        PartyAttributeFeedbackRef.LunchPregHealthy,
        PartyAttributeFeedbackRef.LunchPregUnhealthy,
        PartyAttributeFeedbackRef.DinnerHealthy,
        PartyAttributeFeedbackRef.DinnerUnhealthy,
        PartyAttributeFeedbackRef.DinnerPregHealthy,
        PartyAttributeFeedbackRef.DinnerPregUnhealthy,
        PartyAttributeFeedbackRef.SnackingHealthy,
        PartyAttributeFeedbackRef.SnackingUnhealthy,
        PartyAttributeFeedbackRef.SnackingPregHealthy,
        PartyAttributeFeedbackRef.SnackPregUnhealthy,
        PartyAttributeFeedbackRef.SnackNoData,
        PartyAttributeFeedbackRef.FruitConsumAbove,
        PartyAttributeFeedbackRef.FruitConsPregAbove,
        PartyAttributeFeedbackRef.WholegrainAbove,
        PartyAttributeFeedbackRef.WholegrPregAbove,
        PartyAttributeFeedbackRef.ProcessedFoodHealth,
        PartyAttributeFeedbackRef.ProcesedFoodUnhealth,
        PartyAttributeFeedbackRef.PrcsdFoodPregHealth,
        PartyAttributeFeedbackRef.PrsdFoodPregUnhealth,
        PartyAttributeFeedbackRef.SugarFoodsHealthy,
        PartyAttributeFeedbackRef.SugarFoodsUnhealthy,
        PartyAttributeFeedbackRef.SugarFoodPregHealthy,
        PartyAttributeFeedbackRef.SugarFodPregUnhealth,
        PartyAttributeFeedbackRef.AddedSugarHealthy,
        PartyAttributeFeedbackRef.AddedSugarUnhealthy,
        PartyAttributeFeedbackRef.AddedSugarPregHealth,
        PartyAttributeFeedbackRef.AddSugarPregUnhealth,
        PartyAttributeFeedbackRef.CalcPotassNoConsump,
        PartyAttributeFeedbackRef.CalcPotassPregNoCons,
        PartyAttributeFeedbackRef.CalciumPotassHealthy,
        PartyAttributeFeedbackRef.CalciumPotassBelow,
        PartyAttributeFeedbackRef.CalciumPotassAbove,
        PartyAttributeFeedbackRef.CalcPotassPregHealth,
        PartyAttributeFeedbackRef.CalcPotassPregBelow,
        PartyAttributeFeedbackRef.CalcPotassPregAbove,
        PartyAttributeFeedbackRef.FastFoodsHealthy,
        PartyAttributeFeedbackRef.FastFoodsUnhealthy,
        PartyAttributeFeedbackRef.FastFoodsPregHealthy,
        PartyAttributeFeedbackRef.FastFoodPregUnhealth,
        PartyAttributeFeedbackRef.ProcessedMeatHealthy,
        PartyAttributeFeedbackRef.ProcesedMeatUnhealth,
        PartyAttributeFeedbackRef.PrcssdMeatPregHealth,
        PartyAttributeFeedbackRef.PrsdMeatPregUnhealth,
        PartyAttributeFeedbackRef.RedMeatHealthy,
        PartyAttributeFeedbackRef.RedMeatUnhealthy,
        PartyAttributeFeedbackRef.RedMeatPregHealthy,
        PartyAttributeFeedbackRef.RedMeatPregUnhealthy,
        PartyAttributeFeedbackRef.Omega3Healthy,
        PartyAttributeFeedbackRef.Omega3Unhealthy,
        PartyAttributeFeedbackRef.Omega3PregHealthy,
        PartyAttributeFeedbackRef.Omega3PregUnhealthy,
        PartyAttributeFeedbackRef.SaturatedFatsHealthy,
        PartyAttributeFeedbackRef.SaturatedUnhealthy,
        PartyAttributeFeedbackRef.SaturatedPregHealth,
        PartyAttributeFeedbackRef.SaturatdPregUnhealth,
        PartyAttributeFeedbackRef.UnsaturatedHealth,
        PartyAttributeFeedbackRef.UnsaturatedUnhealthy,
        PartyAttributeFeedbackRef.UnsatFatsPregHealth,
        PartyAttributeFeedbackRef.UnsatFatPregUnhealth,
        PartyAttributeFeedbackRef.NutritionUnknown,
        PartyAttributeFeedbackRef.NutritionResults,
        PartyAttributeFeedbackRef.NutritionOutdated,
        PartyAttributeFeedbackRef.PsychOutdated,
        PartyAttributeFeedbackRef.SocialOutdated,
        PartyAttributeFeedbackRef.PositiveLow,
        PartyAttributeFeedbackRef.PositiveMedium,
        PartyAttributeFeedbackRef.PositiveHigh,
        PartyAttributeFeedbackRef.EnergyLow,
        PartyAttributeFeedbackRef.EnergyMedium,
        PartyAttributeFeedbackRef.EnergyHigh,
        PartyAttributeFeedbackRef.NegativeLow,
        PartyAttributeFeedbackRef.NegativeMedium,
        PartyAttributeFeedbackRef.NegativeHigh,
        PartyAttributeFeedbackRef.AnxietyLow,
        PartyAttributeFeedbackRef.AnxietyMedium,
        PartyAttributeFeedbackRef.AnxietyHigh
    ]
}
