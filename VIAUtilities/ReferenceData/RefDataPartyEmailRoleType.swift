//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum PartyEmailRoleTypeRef: Int, Hashable { 
    case Unknown = -1
    case HomeeMailAddress = 1 // name:Home eMail Address note:Home eMail Address 
    case WorkeMailAddress = 2 // name:Work eMail Address note:Work eMail Address 

    public static let allValues: [PartyEmailRoleTypeRef] = [PartyEmailRoleTypeRef.Unknown, PartyEmailRoleTypeRef.HomeeMailAddress, PartyEmailRoleTypeRef.WorkeMailAddress]

}
