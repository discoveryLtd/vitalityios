//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum AgreementReferenceTypeRef: Int, Hashable { 
    case Unknown = -1
    case EXTERNALREFTYPE = 4 // name: External Reference Type note: External Reference Type 
    case MemberCard = 2 // name:Member Card note:Member Card 
    case PARTNERCONTRACTNO = 3 // name:Partner Contract Number note:Partner Contract Number 
    case PartnerMemberNumber = 5 // name:Partner Member Number note:Partner Member Number 
    case PartnerMembershipId = 6 // name:Partner Membership Id note:Partner Membership Id 
    case PolicyNumber = 1 // name:Policy Number note:Policy Number 

    public static let allValues: [AgreementReferenceTypeRef] = [AgreementReferenceTypeRef.Unknown, AgreementReferenceTypeRef.EXTERNALREFTYPE, AgreementReferenceTypeRef.MemberCard, AgreementReferenceTypeRef.PARTNERCONTRACTNO, AgreementReferenceTypeRef.PartnerMemberNumber, AgreementReferenceTypeRef.PartnerMembershipId, AgreementReferenceTypeRef.PolicyNumber]

}
