//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum ObjectiveTrackerStatusRef: Int, Hashable { 
    case Unknown = -1
    case Achieved = 1 // name:Achieved note:Achieved 
    case Cancelled = 3 // name:Cancelled note:Cancelled 
    case NotAchieved = 2 // name:Not Achieved note:Not Achieved 

    public static let allValues: [ObjectiveTrackerStatusRef] = [ObjectiveTrackerStatusRef.Unknown, ObjectiveTrackerStatusRef.Achieved, ObjectiveTrackerStatusRef.Cancelled, ObjectiveTrackerStatusRef.NotAchieved]

}
