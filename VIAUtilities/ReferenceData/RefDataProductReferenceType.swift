//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum ProductReferenceTypeRef: Int, Hashable { 
    case Unknown = -1
    case Barcode = 2 // name:Product Barcode Number note:Product Barcode Number 
    case ISBN = 3 // name:International Standard Book Number note:International Standard Book Number 
    case ModelNumber = 4 // name:Model Number note:Model Number 
    case QRCode = 5 // name:Quick Response Code note:Quick Response Code 
    case SKU = 1 // name:Stock keeping unit number note:Stock keeping unit number 
    case SerialCode = 6 // name:Serial Code note:Serial Code 

    public static let allValues: [ProductReferenceTypeRef] = [ProductReferenceTypeRef.Unknown, ProductReferenceTypeRef.Barcode, ProductReferenceTypeRef.ISBN, ProductReferenceTypeRef.ModelNumber, ProductReferenceTypeRef.QRCode, ProductReferenceTypeRef.SKU, ProductReferenceTypeRef.SerialCode]

}
