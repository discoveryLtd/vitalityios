//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum RewardPeriodTypeRef: Int, Hashable { 
    case Unknown = -1
    case Month = 2 // name:Month note:Month 
    case Week = 1 // name:Week note:Week 
    case Year = 3 // name:Year note:Year 

    public static let allValues: [RewardPeriodTypeRef] = [RewardPeriodTypeRef.Unknown, RewardPeriodTypeRef.Month, RewardPeriodTypeRef.Week, RewardPeriodTypeRef.Year]

}
