//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum GoalStatusRef: Int, Hashable { 
    case Unknown = -1
    case Active = 1 // name:Active Status note:Active Status 
    case Inactive = 2 // name:Inactive Status note:Inactive Status 

    public static let allValues: [GoalStatusRef] = [GoalStatusRef.Unknown, GoalStatusRef.Active, GoalStatusRef.Inactive]

}
