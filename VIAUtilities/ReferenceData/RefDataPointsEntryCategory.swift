//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum PointsEntryCategoryRef: Int, Hashable { 
    case Unknown = -1
    case Adjustment = 2 // name:Adjustment note:Adjustment 
    case Assessment = 5 // name:Assessment note:Adjustment 
    case BoosterPoints = 1 // name:Booster Points note:Booster Points 
    case Fitness = 3 // name:Fitness note:Fitness 
    case GetActive = 8 // name:Get Active note:Get Active 
    case HealthyFood = 4 // name:Healthy Food note:Healthy Food 
    case Nutrition = 7 // name:Nutrition note:Nutrition 
    case Other = 11 // name:Other note:Other 
    case Screening = 6 // name:Screening note:Screening 
    case VHC = 9 // name:VHC note:VHC - Vitality Health Check 
    case VHR = 10 // name:VHR note:VHR - Vitality Health Review 
    case Vaccinations = 12 // name:Vaccinations note:Vaccinations 

    public static let allValues: [PointsEntryCategoryRef] = [PointsEntryCategoryRef.Unknown, PointsEntryCategoryRef.Adjustment, PointsEntryCategoryRef.Assessment, PointsEntryCategoryRef.BoosterPoints, PointsEntryCategoryRef.Fitness, PointsEntryCategoryRef.GetActive, PointsEntryCategoryRef.HealthyFood, PointsEntryCategoryRef.Nutrition, PointsEntryCategoryRef.Other, PointsEntryCategoryRef.Screening, PointsEntryCategoryRef.VHC, PointsEntryCategoryRef.VHR, PointsEntryCategoryRef.Vaccinations]

}
