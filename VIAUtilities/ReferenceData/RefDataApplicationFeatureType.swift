//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum ApplicationFeatureTypeRef: Int, Hashable { 
    case Unknown = -1
    case ApplicationToggle = 2 // name:Application Toggle note:undefined 
    case ProductFeature = 1 // name:Product Feature note:undefined 

    public static let allValues: [ApplicationFeatureTypeRef] = [ApplicationFeatureTypeRef.Unknown, ApplicationFeatureTypeRef.ApplicationToggle, ApplicationFeatureTypeRef.ProductFeature]

}
