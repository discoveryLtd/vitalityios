//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum PartyReferenceTypeRef: Int, Hashable { 
    case Unknown = -1
    case Branch = 7 // name:Branch note:Branch 
    case CarrierUID = 2 // name:CarrierUniqueIdentity note:CarrierUniqueIdentity 
    case CustomerNumber = 8 // name:Customer Number note:Customer Number 
    case EntityNumber = 10 // name:Vitality Entity Number note:The entity number associated with this party on the Legacy Vitality System 
    case NationalID = 1 // name:NationalIdentityNumber note:NationalIdentityNumber 
    case PartnerIdentifier = 3 // name:PartnerIdentifier note:Partner Identifer that uniquely identifies a partner 
    case PartnerMembership = 6 // name:PartnerMembership note:PartnerMembership 
    case Party = 4 // name:Party note:Party 
    case PolicyNumber = 9 // name:Policy Number note:Policy Number 
    case VitalityMembership = 5 // name:Vitality Membership note:Vitality Membership 
    case VitalityNumber = 11 // name:Vitality Number note:Vitality Number 

    public static let allValues: [PartyReferenceTypeRef] = [PartyReferenceTypeRef.Unknown, PartyReferenceTypeRef.Branch, PartyReferenceTypeRef.CarrierUID, PartyReferenceTypeRef.CustomerNumber, PartyReferenceTypeRef.EntityNumber, PartyReferenceTypeRef.NationalID, PartyReferenceTypeRef.PartnerIdentifier, PartyReferenceTypeRef.PartnerMembership, PartyReferenceTypeRef.Party, PartyReferenceTypeRef.PolicyNumber, PartyReferenceTypeRef.VitalityMembership, PartyReferenceTypeRef.VitalityNumber]

}
