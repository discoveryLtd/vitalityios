//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum GoalTrackerStatusRef: Int, Hashable { 
    case Unknown = -1
    case Achieved = 1 // name:Achieved note:Achieved 
    case Cancelled = 3 // name:Cancelled note:Cancelled 
    case InProgress = 7 // name:In Progress Goal tracker note:In Progress Goal tracker 
    case ManualCancellation = 4 // name:Manually Cancelled Goal Tracker note:Manually Cancelled Goal Tracker 
    case ManuallyAchieved = 6 // name:Manually Achieved Goal Tracker note:Manually Achieved Goal Tracker 
    case NotAchieved = 2 // name:Not Achieved note:Not Achieved 
    case Pending = 8 // name:Pending Goal tracker note:Pending Goal tracker 
    case SystemCancellation = 5 // name:System Cancelled Goal Tracker note:System Cancelled Goal Tracker 

    public static let allValues: [GoalTrackerStatusRef] = [GoalTrackerStatusRef.Unknown, GoalTrackerStatusRef.Achieved, GoalTrackerStatusRef.Cancelled, GoalTrackerStatusRef.InProgress, GoalTrackerStatusRef.ManualCancellation, GoalTrackerStatusRef.ManuallyAchieved, GoalTrackerStatusRef.NotAchieved, GoalTrackerStatusRef.Pending, GoalTrackerStatusRef.SystemCancellation]

}
