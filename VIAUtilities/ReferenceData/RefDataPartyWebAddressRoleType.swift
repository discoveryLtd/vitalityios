//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum PartyWebAddressRoleTypeRef: Int, Hashable { 
    case Unknown = -1
    case Facebook = 6 // name:Facebook note:Facebook 
    case GooglePlus = 5 // name:Google Plus note:Google Plus 
    case LinkedIn = 4 // name:LinkedIn note:LinkedIn 
    case PersonalBlog = 2 // name:Personal Blog note:Personal Blog 
    case Twitter = 3 // name:Twitter note:Twitter 
    case WorkBlog = 1 // name:Work Blog note:Work Blog 

    public static let allValues: [PartyWebAddressRoleTypeRef] = [PartyWebAddressRoleTypeRef.Unknown, PartyWebAddressRoleTypeRef.Facebook, PartyWebAddressRoleTypeRef.GooglePlus, PartyWebAddressRoleTypeRef.LinkedIn, PartyWebAddressRoleTypeRef.PersonalBlog, PartyWebAddressRoleTypeRef.Twitter, PartyWebAddressRoleTypeRef.WorkBlog]

}
