//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum CardTypeRef: Int, Hashable { 
    case Unknown = -1
    case ActiveRewards = 7 // name:Active Rewards note:Active Rewards 
    case AppleWatch = 9 // name:Apple Watch note:Apple Watch 
    case AvailableRewards = 27 // name:Available Rewards note:Available Rewards
    case DeviceCashback = 16 // name:Device Cashback note:Device Cashback 
    case HealthHub = 6 // name:Health Hub  note:Health Hub  
    case HealthPartners = 13 // name:Health Partners note:Health Partners 
    case MentalWellbeing = 12 // name:Mental Wellbeing Assessment (MWB) note:Mental Wellbeing Assessment (MWB) 
    case NonSmokersDecl = 3 // name:Non-Smoker's Declaration note:Non-Smoker's Declaration 
    case OrganisedFitnessEvents = 18 // name:Organised Fitness Events note:Organised Fitness Events (OFE) 
    case PolicyCashback = 21 // name:Policy Cashback note:Policy Cashback 
    case RewardPartners = 15 // name:Reward Partners note:Reward Partners 
    case RewardSelection = 22 // name:Reward Selection note:Reward Selection 
	case HealthServices = 23 // name:Your Health Services note:Your Health Services
    case Rewards = 10 // name:Reward/s note:Reward/s 
    case ScreenandVacc = 5 // name:Screenings and Vaccinations (S&V) note:Screenings and Vaccinations (S&V) 
    case VitNutAssessment = 2 // name:Vitality Nutrition Assessment (VNA) note:Vitality Nutrition Assessment (VNA) 
    case VitalityHealthCheck = 4 // name:Vitality Health Check (VHC) note:Vitality Health Check (VHC) 
    case VitalityHealthReview = 1 // name:Vitality Health Review (VHR) note:Vitality Health Review (VHR) 
    case Vouchers = 11 // name:Voucher/s note:Voucher/s 
    case WellDevandApps = 8 // name:Wellness Devices and Apps note:Wellness Devices and Apps 
    case WellnessPartners = 14 // name:Wellness Partners note:Wellness Partners
    case NuffieldHealthServices = 24 // name:Nuffield Health Services
    case ActivationBarcode = 26 // name:Activation Barcode note:Activation Barcode
    case UKEEmployerStatus = 25 // name:Employer Status note:Employer Status
    case EmployerStatus = 28 // name:Employer Status note:Employer Status

    public static let allValues: [CardTypeRef] = [CardTypeRef.Unknown, CardTypeRef.ActiveRewards, CardTypeRef.AppleWatch, CardTypeRef.DeviceCashback, CardTypeRef.HealthHub, CardTypeRef.HealthPartners, CardTypeRef.MentalWellbeing, CardTypeRef.NonSmokersDecl, CardTypeRef.OrganisedFitnessEvents, CardTypeRef.PolicyCashback, CardTypeRef.RewardPartners, CardTypeRef.RewardSelection, CardTypeRef.HealthServices, CardTypeRef.Rewards, CardTypeRef.ScreenandVacc, CardTypeRef.VitNutAssessment, CardTypeRef.VitalityHealthCheck, CardTypeRef.VitalityHealthReview, CardTypeRef.Vouchers, CardTypeRef.WellDevandApps, CardTypeRef.WellnessPartners, CardTypeRef.NuffieldHealthServices, CardTypeRef.ActivationBarcode, CardTypeRef.EmployerStatus]
}
