//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum CardUnitTypeRef: Int, Hashable { 
    case Unknown = -1
    case EarnedExpiring = 3 // name:Earned Expiring note:Earned Expiring 
    case Points = 1 // name:Points note:Points 
    case Tasks = 2 // name:Tasks note:Tasks 

    public static let allValues: [CardUnitTypeRef] = [CardUnitTypeRef.Unknown, CardUnitTypeRef.EarnedExpiring, CardUnitTypeRef.Points, CardUnitTypeRef.Tasks]

}
