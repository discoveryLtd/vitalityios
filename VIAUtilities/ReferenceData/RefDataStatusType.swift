//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum StatusTypeRef: Int, Hashable { 
    case Unknown = -1
    case Blue = 5 // name:Blue note:Blue 
    case Bronze = 1 // name:Bronze note:Bronze 
    case Gold = 3 // name:Gold note:Gold 
    case Platinum = 4 // name:Platinum note:Platinum 
    case Silver = 2 // name:Silver note:Silver 

    public static let allValues: [StatusTypeRef] = [StatusTypeRef.Unknown, StatusTypeRef.Blue, StatusTypeRef.Bronze, StatusTypeRef.Gold, StatusTypeRef.Platinum, StatusTypeRef.Silver]

}
