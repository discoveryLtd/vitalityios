//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum SectionRef: Int, Hashable { 
    case Unknown = -1
    case MWBSection = 3 // name:MentalWellBeing note:Mental Well Being 
    case NutritionSection = 2 // name:Nutrition note:Nutrition 
    case VAgeAreasDoingWell = 10 // name:You are doing well in these areas.  Keep it up! note:You are doing well in these areas.  Keep it up! 
    case VAgeFocusImprovement = 8 // name:Focus on improving these first note:Focus on improving these first 
    case VAgeMoreDoingWell = 11 // name:Good going! Here�s some more note:Good going! Here�s some more 
    case VAgeMoreImprovements = 9 // name:Have a go at these improvements note:Have a go at these improvements 
    case VAgeMoreNeedToKnow = 13 // name:Have a look at these too note:Have a look at these too 
    case VAgeNeedToKnow = 12 // name:We still need to know these things about you note:We still need to know these things about you 
    case VAgeResultBad = 4 // name:What you need to improve note:What you need to improve 
    case VAgeResultGood = 5 // name:What you are doing well note:What you are doing well 
    case VAgeResultUnknown = 6 // name:What you have not provided note:What you have not provided 
    case VAgeSummary = 7 // name:Summary note:Summary 
    case VAgeSection = 1 // name:VitalityAge note:Vitality Age 
    case NutritionWhatWell = 25 // name:What you are doing well note:What you are doing well
    case NutritionWhatImprove = 24 // name:What you can improve note:What you can improve
    case NutritionLookingGood = 27 // name:These results are looking good note:These results are looking good
    case NutritionNeedImprove = 26 // name:These results need improvement note:These results need improvement
    case Stressor = 14 // name:Stressor note:Stressor
    case StressorDiag = 17 // name:Stressor note:Stressor
    case Traumatic = 18 // name:Traumatic Events note:Traumatic Events
    case Psycological = 15 // name:Psycological note:Psycological
    case Positive = 19 // name:Positive Emotion note:Positive Emotion
    case Energy = 20 // name:Energy note: Energy
    case Negative = 21 // name:Negative Emotion note:Negative Emotion
    case Anxiety = 22 // name:Anxiety note:Anxiety
    case Social = 16 // name:Social note:Social

    public static let allValues: [SectionRef] = [SectionRef.Unknown, SectionRef.MWBSection, SectionRef.NutritionSection, SectionRef.VAgeAreasDoingWell, SectionRef.VAgeFocusImprovement, SectionRef.VAgeMoreDoingWell, SectionRef.VAgeMoreImprovements, SectionRef.VAgeMoreNeedToKnow, SectionRef.VAgeNeedToKnow, SectionRef.VAgeResultBad, SectionRef.VAgeResultGood, SectionRef.VAgeResultUnknown, SectionRef.VAgeSummary, SectionRef.VAgeSection, SectionRef.NutritionWhatWell, SectionRef.NutritionWhatImprove, SectionRef.NutritionLookingGood, SectionRef.NutritionNeedImprove, SectionRef.Stressor, SectionRef.StressorDiag, SectionRef.Traumatic, SectionRef.Psycological, SectionRef.Positive, SectionRef.Energy, SectionRef.Negative, SectionRef.Anxiety, SectionRef.Social]
}
