//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum InboundInstructionReferenceTypeRef: Int, Hashable { 
    case Unknown = -1
    case AwardedRewardID = 1 // name:AwardedRewardID note:AwardedRewardID 

    public static let allValues: [InboundInstructionReferenceTypeRef] = [InboundInstructionReferenceTypeRef.Unknown, InboundInstructionReferenceTypeRef.AwardedRewardID]

}
