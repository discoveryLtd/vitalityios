//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum QuestionnaireStatusRef: Int, Hashable { 
    case Unknown = -1
    case Disabled = 2 // name:Disabled note:Disabled 
    case Enabled = 1 // name:Enabled note:Enabled 

    public static let allValues: [QuestionnaireStatusRef] = [QuestionnaireStatusRef.Unknown, QuestionnaireStatusRef.Disabled, QuestionnaireStatusRef.Enabled]

}
