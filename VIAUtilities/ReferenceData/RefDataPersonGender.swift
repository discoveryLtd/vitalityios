//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum PersonGenderRef: Int, Hashable { 
    case Unknown = -1
    case Female = 2 // name:Female note:Female 
    case Male = 1 // name:Male note:Male 

    public static let allValues: [PersonGenderRef] = [PersonGenderRef.Unknown, PersonGenderRef.Female, PersonGenderRef.Male]

}
