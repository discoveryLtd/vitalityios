//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum UpdateInstructionTypeRef: Int, Hashable { 
    case Unknown = -1
    case Custom = 3 // name:Custom note:Custom 
    case LastCapturedValue = 2 // name:Last captured value note:Last captured value 
    case PartyAttributeType = 1 // name:Party Attribute Type note:Party Attribute Type 

    public static let allValues: [UpdateInstructionTypeRef] = [UpdateInstructionTypeRef.Unknown, UpdateInstructionTypeRef.Custom, UpdateInstructionTypeRef.LastCapturedValue, UpdateInstructionTypeRef.PartyAttributeType]

}
