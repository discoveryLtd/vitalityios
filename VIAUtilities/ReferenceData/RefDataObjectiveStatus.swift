//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum ObjectiveStatusRef: Int, Hashable { 
    case Unknown = -1
    case Achieved = 3 // name:Achieved note:Achieved 
    case Active = 1 // name:Active note:Active 
    case Cancelled = 4 // name:Cancelled note:Cancelled 
    case Inactive = 2 // name:Inactive note:Inactive 

    public static let allValues: [ObjectiveStatusRef] = [ObjectiveStatusRef.Unknown, ObjectiveStatusRef.Achieved, ObjectiveStatusRef.Active, ObjectiveStatusRef.Cancelled, ObjectiveStatusRef.Inactive]

}
