//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum ProviderItemTypeRef: Int, Hashable { 
    case Unknown = -1
    case VendorCardTypeId = 1 // name:Voucher Card Type Id note:Voucher Card Type Id 

    public static let allValues: [ProviderItemTypeRef] = [ProviderItemTypeRef.Unknown, ProviderItemTypeRef.VendorCardTypeId]

}
