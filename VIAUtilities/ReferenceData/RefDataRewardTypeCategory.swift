//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum RewardTypeCategoryRef: Int, Hashable { 
    case Unknown = -1
    case AccrualItem = 4 // name:Accrual item note:Reward is a item that accrues towards another reward 
    case FinancialTransaction = 1 // name:Physical financial transaction note:Reward is a physical financial transaction such as a cashback 
    case Provisional = 5 // name:Provisional Reward note:Reward with the possibility to be changed later 
    case Recognition = 3 // name:Recognition note:Reward is a form of recognition such as a badge that is displayed in the mobile app 
    case VirtualTransaction = 2 // name:Virtual financial transaction note:Reward is a virtual financial transaction such as Vitality points,loyalty program points or miles 

    public static let allValues: [RewardTypeCategoryRef] = [RewardTypeCategoryRef.Unknown, RewardTypeCategoryRef.AccrualItem, RewardTypeCategoryRef.FinancialTransaction, RewardTypeCategoryRef.Provisional, RewardTypeCategoryRef.Recognition, RewardTypeCategoryRef.VirtualTransaction]

}
