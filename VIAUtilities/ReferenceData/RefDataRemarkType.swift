//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum RemarkTypeRef: Int, Hashable { 
    case Unknown = -1
    case Complaint = 1 // name:Complaint note:undefined 
    case Compliment = 2 // name:Compliment note:undefined 
    case Grievance = 3 // name:Grievance note:undefined 

    public static let allValues: [RemarkTypeRef] = [RemarkTypeRef.Unknown, RemarkTypeRef.Complaint, RemarkTypeRef.Compliment, RemarkTypeRef.Grievance]

}
