//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum EventAssociationTypeRef: Int, Hashable { 
    case Unknown = -1
    case ConsequentEvent = 1 // name:ConsequentEvent note:ConsequentEvent 
    case DocumentaryProof = 2 // name:Documentary Proof note:null 

    public static let allValues: [EventAssociationTypeRef] = [EventAssociationTypeRef.Unknown, EventAssociationTypeRef.ConsequentEvent, EventAssociationTypeRef.DocumentaryProof]

}
