//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum RewardTypeRef: Int, Hashable { 
    case Unknown = -1
    case Cashback = 4 // name:Cashback note:Cash that is paid to the member as a reward 
    case NoReward = 7 // name:No Reward note:No Reward 
    case Points = 2 // name:Points note:Loyalty program points such as Vitality points. The points can translate into some type of reward for the member. 
    case RewardOpportunity = 5 // name:Reward Opportunity note:The opportunity to receive a reward 
    case Selection = 6 // name:Reward Selection note:The ability to select a reward from a list of rewards 
    case Token = 3 // name:Token note:A token is a type of reward that can be exchanged for some other reward 
    case Voucher = 1 // name:Voucher note:A voucher that may be exchanged for the goods or services specified by the voucher 

    public static let allValues: [RewardTypeRef] = [RewardTypeRef.Unknown, RewardTypeRef.Cashback, RewardTypeRef.NoReward, RewardTypeRef.Points, RewardTypeRef.RewardOpportunity, RewardTypeRef.Selection, RewardTypeRef.Token, RewardTypeRef.Voucher]

}
