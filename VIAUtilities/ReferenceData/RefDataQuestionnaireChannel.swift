//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum QuestionnaireChannelRef: Int, Hashable { 
    case Unknown = -1
    case Mobile = 1 // name:Mobile note:Mobile 
    case Web = 2 // name:Web note:Web 

    public static let allValues: [QuestionnaireChannelRef] = [QuestionnaireChannelRef.Unknown, QuestionnaireChannelRef.Mobile, QuestionnaireChannelRef.Web]

}
