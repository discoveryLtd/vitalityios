//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum PartyAttributeFeedbackTipRef: Int, Hashable { 
//    case Unknown = -1
//    case ActRewardsStress = 55 // name:Activate Active Rewards note:Get moving and earn rewards! 
//    case AlcoholHydrate = 96 // name:Stay hydrated note:Have a glass of water and alternate. 
//    case AlcoholMaintain = 99 // name:Not a problem note:If you have some, keep it in the limit 
//    case BMIDocEatHealthy = 7 // name:Try to reach a healthy weight note:To maintain a  healthy weight pay attention to what you eat 
//    case BMIDocHealthyWeight = 8 // name:Try to reach a healthy weight  note:To maintain a  healthy weight pay attention to what you eat 
//    case BMIPreg = 103 // name:You are Pregnant note:BMI is not an accurate measurement for pregnant women. 
//    case CheckBloodSugar = 128 // name:Check you levels note:Checking blood sugar on your own is still necessary. 
//    case DairyCalPot = 87 // name:Very dairy note:Dairy products are high in calcium and low in sodium (salt). 
//    case DairyNoEat = 88 // name:Very dairy note:Dairy products contain important nutrients such as calcium and B-vitamins which benefit health. 
//    case DietAdvice = 167 // name:Advice note:Take care of your diet. 
//    case DrinkMakeSmallSize = 98 // name:Make it a smaller one note:Literally drink a little less 
//    case DrinkPregNot = 100 // name:You are Pregnant note:Alcohol can be very dangerous for you and your baby 
//    case DrinkSetLimit = 97 // name:Make a plan note:Set a limit and budget 
//    case EatBreakfast1 = 41 // name:Consistent eating habits note:Don't skip meals, especially breakfast. 
//    case EatBreakfast2 = 127 // name:Consistent eating habits note:Don't skip meals, especially breakfast. 
//    case EatDairy = 162 // name:Dear dairy note:Low fat and fat free dairy products are nutritious and may help you lose weight. 
//    case EatDairyIfNot = 164 // name:Very dairy note:Dairy products contain important nutrients such as calcium and B-vitamins which benefit health. 
//    case EatFibre = 34 // name:Add some fibre note:fibre slows carb digestion and sugar absorption. 
//    case EatFruitExtra = 81 // name:Fruit as a snack note:Get extra servings as snacks between meals  
//    case EatPlantProtein = 160 // name:Plant protein in your body note:Try and get the protein your body needs through plant protein sources. 
//    case EatVeg = 83 // name:Go green note:Eating vegetables can reduce your risk for some cancers and heart disease. 
//    case EatVegMaintain = 157 // name:Fight back with veggies note:Vegetables and legumes are an important source of many nutrients to tackle many lifestyle-related medical conditions. 
//    case EatfruitMaintain = 155 // name:Keep variety note:Focus on variety by including different types and colours 
//    case ExecrciseMaintain = 9 // name:Get Moving note:Keep active and get regular exercise. 
//    case ExercisDailyStrength = 22 // name:Do a little bit of exercise every day note:Try to include some strength-training exercise once or twice a week.  
//    case ExerciseDaily = 102 // name:Get Moving note:Keep active and get regular exercise. 
//    case ExerciseFun = 15 // name:Keep it fun note:A variety of activities will help to keep exercising fun 
//    case ExerciseGoal = 12 // name:Start small  note:Start small and build momentum 
//    case ExerciseIncrease = 13 // name:Regular exercise is good for you note:Try to do 30 minutes of moderate activity on 5 days a week or more. 
//    case ExercisePreg = 109 // name:You are Pregnant note:Exercise can have many benefits during pregnancy 
//    case ExercisePregPhys = 117 // name:Do a little bit of exercise every day note: It is recommended that you exercise moderately with your doctor's permission even if you did not exercise regularly before falling pregnant. 
//    case ExercisePregSlow = 111 // name:Do a little bit of exercise every day note:Do slow, controlled stretches without bouncing.  
//    case ExercisePrioritise = 106 // name:Keep it fun note:A variety of activities will help to keep exercising fun 
//    case ExerciseReduceStress = 140 // name:Get Moving note:Keep active and try to reduce stress. 
//    case ExerciseSleep = 64 // name:Get Moving note:Keep active and take care of excessive stress.  
//    case ExerciseSleepPreg = 67 // name:Get Moving note:Keep active and take care of excessive stress.  
//    case ExerciseSocial = 16 // name:Make it social note:Fun time to socialise with friends and family 
//    case ExerciseStressReduc = 58 // name:Get Moving note:Keep active and try to reduce stress.  
//    case FatsMaintain = 56 // name:Keep the fats away note:Cut down the amount of saturated fat in your diet. 
//    case FoodLabelRead = 147 // name:Read food labels note:Reading food labels is important. An ingredient list will not specify sodium alone but rather salt or ingredients containing sodium such as monosodium glutamate, sodium nitrite or soya sauce. 
//    case FruitBuyInSeason = 80 // name:Seasonal fruit note:Buy fresh fruit in season - they will be cheaper and tastier. 
//    case FruitSeasonMaintain = 154 // name:Seasonal fruit note:Buy fresh fruit in season - they will be cheaper and tastier. 
//    case GetAdvice = 40 // name:Get some advice note:Speak to a healthcare professional 
//    case GetRewards = 108 // name:Activate Active Rewards note:Get moving and earn rewards! 
//    case GetRewardsBP = 133 // name:Activate Active Rewards note:Get moving and earn rewards! 
//    case GetRewardsChol = 142 // name:Activate Active Rewards note:Get moving and earn rewards! 
//    case GetRewardsExercise = 145 // name:Activate Active Rewards note:Get moving and earn rewards! 
//    case GetRewardsGlucose = 123 // name:Activate Active Rewards note:Get moving and earn rewards! 
//    case HealthyEatMaintain = 141 // name:Eat Healthy note:Eat plenty vegetables and include fruits every day. 
//    case HealthyFatsMaintain = 152 // name:Favouring healthy fats note:By favouring healthy fats you are lowering your health risks. 
//    case KeepFoodDiaryLowerBP = 131 // name:Record your eating note:Keep a food diary. 
//    case KeepWeightBloodGluc = 125 // name:Try to maintain a healthy weight note:Get advice. 
//    case KeepWeightBloodPress = 135 // name:Try to maintain a healthy weight note:Get advice 
//    case LeanMeatCook = 85 // name:Lean this way note:Choose lean meat over fatty cuts. 
//    case LimitFatCook = 143 // name:Fry using limited amounts of fat note:Adding too much fat whilst cooking increases the energy content of your meals which can lead to weight gain. 
//    case LinkDeviceApp = 107 // name:Link a device or app note:Link a device or app and track your activity 
//    case MoveGlucPhysActOOR = 129 // name:Keep Moving note:Continue doing 150 minutes of physical activity a week 
//    case MoveReduceStress = 28 // name:Get Moving note:Keep active and take care of excessive stress.  
//    case PhysActBPressureOOR = 49 // name:Get Physical note:Try 150 minutes of physical activity a week 
//    case PhysActGlucose = 42 // name:Get Physical note:Try 150 minutes of physical activity a week 
//    case PhysActWeight = 115 // name:Use the stairs note:Becoming physically active has many health benefits and reduces your risk for disease.  
//    case PhysActivityHealth = 116 // name:Go for a walk a brisk walk at lunchtime note:Becoming physically active has many health benefits and reduces your risk for disease.  
//    case PhysActivityProgram = 136 // name:Get Physical note:Try 150 minutes of physical activity a week 
//    case PhysActivityRegular = 130 // name:Get Physical note:Try 150 minutes of physical activity a week 
//    case PhysicalActivityWalk = 14 // name:Go for a walk a brisk walk at lunchtime note:Try to do 30 minutes of moderate activity on 5 days a week or more. 
//    case PregBloodGlucose = 126 // name:You are Pregnant note:Monitor your blood glucose (blood sugar) levels regularly to be in a healthy range 
//    case PregBloodPressure = 137 // name:You are Pregnant note:Monitor your blood pressure regularly. 
//    case PregDietAdvice = 168 // name:You are pregnant note:Take care of your diet. 
//    case PregEatFruit = 156 // name:You are pregnant note:Fruit offers many valuable nutrients and fibre needed during pregnancy. 
//    case PregEatRedMeat = 161 // name:You are pregnant note:You need significantly more iron during pregnancy. Eat plenty of iron-rich foods.  
//    case PregEatVitamins = 158 // name:You are pregnant note:Eating different vitamins and minerals passes on various essential nutrients and increases the chance your baby will accept those flavours later on.   
//    case PregSaltReduce = 148 // name:You are pregnant note:Too much or too little salt intake during pregnancy can negatively affect your sodium levels. 
//    case PregSaltyFoodAdvice = 95 // name:You are pregnant note:Take care of your diet. 
//    case PregSugaryDrinks = 151 // name:You are pregnant note:Continue avoiding sugary drinks as much as possible. 
//    case PregTransfatReduce = 153 // name:You are pregnant note:Avoid food high in trans fats by eatting a balanced diet rich in fruits, vegetables, whole grains, lean protein, and low fat dairy products. 
//    case PregWholegrain = 166 // name:You are pregnant note:Keep on eating whole grains as they are rich in nutrients. 
//    case PregnantBloodGlucose = 43 // name:You are Pregnant note:Monitor your blood glucose (blood sugar) levels regularly to be in a healthy range 
//    case PregnantEatCalcBaby = 163 // name:You are pregnant note:A developing baby needs calcium to build strong bones and teeth, a healthy heart, nerves, and muscles, and to develop a normal heart rhythm and normal blood-clotting. 
//    case PregnantEatIron = 86 // name:You are pregnant note:You need significantly more iron during pregnancy. Eat plenty of iron-rich foods.  
//    case PregnantEatVeg = 84 // name:You are pregnant note:Eating different vitamins and minerals passes on various essential nutrients and increases the chance your baby will accept those flavours later on.   
//    case PregnantSaltModerate = 68 // name:You are pregnant note:Eat salt in moderation by monitoring your intake of salty foods, added salt during cooking and at the table, and reading food labels 
//    case PregnantSaltMonitor = 72 // name:You are pregnant note:Monitor your intake of salt to avoid excess intake 
//    case PregnanteatFruit = 82 // name:You are pregnant note:Fruit offers many valuable nutrients and fibre needed during pregnancy. 
//    case PrgnantSugaryDrinks = 77 // name:You are pregnant note:Try to avoid sugary drinks as much as possible. 
//    case QuitSmokingSave = 29 // name:Choose Your Reward note:How much money will you save? 
//    case RedMeatHealthy = 159 // name:Lean this way note:Choose lean meat over fatty cuts. 
//    case SaltBPMaintain = 149 // name:Assault the salt note:Sodium is in salt but not all foods high in sodium will be obviously salty. 
//    case SaltProcessedFood = 71 // name:Assault the salt note:Avoid a diet high in sodium, which can contribute to high blood pressure. Sodium is in salt. 
//    case SaltyFoodAdvice = 93 // name:Advice note:Take care of your diet. 
//    case SaturatedFatDecrease = 138 // name:Stay away from saturated fats note:Keep the saturated fat in your diet low. 
//    case ShopBPReduce = 46 // name:Be a smart shopper note:Stick to a healthy-eating plan 
//    case SleepSchedule = 144 // name:Stick to a sleep schedule note:Being consistent reinforces your body's sleep-wake cycle 
//    case SleepSchedulePreg = 66 // name:Stick to a sleep schedule note:Being consistent reinforces your body's sleep-wake cycle 
//    case SmokeAvoidDrink = 119 // name:Avoid Triggers note:What makes you want one more? 
//    case SmokeDontStart = 118 // name:Don't start! note:Sometimes all it takes is one 
//    case SmokePreg = 120 // name:You are Pregnant note:Smoking is very dangerous for you and your baby 
//    case SmokeQuitAdvice = 30 // name:Get Advice note:Speak to a healthcare professional. 
//    case SmokeQuitProgramme = 31 // name:Get some Help note:Let <Partner name / Free public program name> help. 
//    case SmokeQuitUnwind = 32 // name:Take a Break note:Find other ways to unwind. 
//    case SodiumReducLessSalt = 146 // name:Reduce Sodium note:Gradually cut back on your salt 
//    case SodiumReduce = 69 // name:Reduce Sodium note:Gradually cut back on your salt. 
//    case SodiumReduceBP = 132 // name:Reduce Sodium note:Less sodium can reduce blood pressure 
//    case StrengthExercise = 112 // name:Always try to incorporate strength training into your exercise schedule note:Muscular strength and endurance has a direct impact on your ability to perform daily living activities.  
//    case StretchIncrease = 20 // name:Try stretching two or three days a week note:Do slow, controlled stretches without bouncing.  
//    case StretchRegular = 110 // name:Regular stretching can prevent injury note:Do slow, controlled stretches without bouncing.  
//    case SugaryDrinkLabel = 76 // name:Drink water instead note:Water is the no kilojoule option 
//    case SugaryDrinks = 73 // name:Drink water instead note:Water is the no kilojoule option 
//    case SugaryDrinksMaintain = 150 // name:Drink water instead note:Water is the no kilojoule option 
//    case SugaryDrinksReduce = 75 // name:Drink water instead note:Water is the no kilojoule option 
//    case SugaryDrinksSoft = 74 // name:Drink water instead note:Water is the no kilojoule option 
//    case TransFatReduce = 78 // name:Trans form your diet note:Choose products with no trans fat. Avoid foods high in artificial trans fat. 
//    case WaistCircumMaintain = 105 // name:Keep the belly away note:Excessive weight around the stomach area increases risk of heart disease, amongst others. 
//    case WaistCircumReduce = 104 // name:Lose the belly! note:Excessive fat around the stomach area increases one's risk of disease. 
//    case WaistCircumfReduce = 11 // name:Lose the belly! note:Excessive fat around the stomach area increases your risk of heart disease, amongst others. 
//    case WalkDriveLess = 114 // name:Drive less and walk more note:Becoming physically active has many health benefits and reduces your risk for disease.  
//    case WeightBloodGlucose = 124 // name:Keep a healthy weight note:Get advice. 
//    case WeightBloodPressure = 134 // name:Keep  a healthy weight note:Get advice 
//    case WeightDecreaseBP = 48 // name:Try to reach a healthy weight note:Get advice. 
//    case WeightLowerBloodGluc = 37 // name:Try to reach a healthy weight note:Get advice. 
//    case WeightMaintain = 101 // name:Try to maintain a healthy weight note:To maintain a  healthy weight pay attention to what you eat 
//    case WeightPregAdvice = 39 // name:Try to maintain a healthy weight note:Get advice. 
//    case WholeGrainsDiet = 90 // name:Grain of truth note:Make half the grains in your diet whole grains. 
//    case WholegrainAddPreg = 91 // name:You are pregnant note:Consult your dietician to find options to increase your wholegrain intake. 
//    case WholegrainChol1 = 57 // name:Choose wholegrains note:Focus on choosing wholegrains instead of refined starches. 
//    case WholegrainChol2 = 139 // name:Choose wholegrains note:Focus on choosing wholegrains instead of refined starches. 
//    case WholegrainMaintain = 165 // name:Whole grain of truth note:Whole grains are the best choice of grain.
//    case BoostImmuneVeg = 169 // name:Boost your immune system
//    case CancerEatVeg = 170 // name:Cut your cancer risk
//    case EatBreakfastGlucose = 171 // name:First things first
//    case EatFruitConvenient = 172 // name:Make it convenient
//    case EatFruitEasy = 173 // name:Make it easy
//    case EatFruitSnack = 174 // name:Fruit as a snack
//    case EatProteinPlants = 175 // name:Plant protein in your body
//    case EatVegIncrease = 176 // name:Keep your heart ticking
//    case FeelAnxious = 177 // name:Determine what causes you to feel anxious
//    case FibreVegIncrease = 178 // name:Fill up on fibre
//    case FruitBuySeasonal = 179 // name:Seasonal fruit
//    case HealthyEatIncr = 180 // name:Eat Healthy
//    case MoveBPPhysActOOR = 181 // name:Keep Moving
//    case PhysActMoveOOR = 182 // name:Keep Moving
//    case PregExistMedCond = 183 // name:You are Pregnant
//    case SaltCookReduce = 184 // name:Assault the salt
//    case SaltFoodTypeReduce = 185 // name:Assault the salt
//    case SaltSodiumReduce = 186 // name:Assault the salt
//    case SodiumLowerGradually = 187 // name:Reduce Sodium
//    case SodiumReduceAddSpice = 188 // name:Reduce Sodium
//    case SodiumReduceIntake = 189 // name:Reduce Sodium
//    case StressLevelslow = 190 // name:Keep your levels of distress and anxiety low
//    case WholeGrainsChoice = 191 // name:Grain of truth
//    case WholeGrainsLabel = 192 // name:Grain of truth
//    case WholeGrainsNutrients = 193 // name:Grain of truth
//    case WholeGrainsRecipe = 194 // name:Grain of truth
//    case SaltyFoodSodium = 195 // name:Assault the salt
//    case SaltyFoodPreg = 196 // name:You are pregnant
//
//    public static let allValues: [PartyAttributeFeedbackTipRef] = [PartyAttributeFeedbackTipRef.Unknown, PartyAttributeFeedbackTipRef.ActRewardsStress, PartyAttributeFeedbackTipRef.AlcoholHydrate, PartyAttributeFeedbackTipRef.AlcoholMaintain, PartyAttributeFeedbackTipRef.BMIDocEatHealthy, PartyAttributeFeedbackTipRef.BMIDocHealthyWeight, PartyAttributeFeedbackTipRef.BMIPreg, PartyAttributeFeedbackTipRef.CheckBloodSugar, PartyAttributeFeedbackTipRef.DairyCalPot, PartyAttributeFeedbackTipRef.DairyNoEat, PartyAttributeFeedbackTipRef.DietAdvice, PartyAttributeFeedbackTipRef.DrinkMakeSmallSize, PartyAttributeFeedbackTipRef.DrinkPregNot, PartyAttributeFeedbackTipRef.DrinkSetLimit, PartyAttributeFeedbackTipRef.EatBreakfast1, PartyAttributeFeedbackTipRef.EatBreakfast2, PartyAttributeFeedbackTipRef.EatDairy, PartyAttributeFeedbackTipRef.EatDairyIfNot, PartyAttributeFeedbackTipRef.EatFibre, PartyAttributeFeedbackTipRef.EatFruitExtra, PartyAttributeFeedbackTipRef.EatPlantProtein, PartyAttributeFeedbackTipRef.EatVeg, PartyAttributeFeedbackTipRef.EatVegMaintain, PartyAttributeFeedbackTipRef.EatfruitMaintain, PartyAttributeFeedbackTipRef.ExecrciseMaintain, PartyAttributeFeedbackTipRef.ExercisDailyStrength, PartyAttributeFeedbackTipRef.ExerciseDaily, PartyAttributeFeedbackTipRef.ExerciseFun, PartyAttributeFeedbackTipRef.ExerciseGoal, PartyAttributeFeedbackTipRef.ExerciseIncrease, PartyAttributeFeedbackTipRef.ExercisePreg, PartyAttributeFeedbackTipRef.ExercisePregPhys, PartyAttributeFeedbackTipRef.ExercisePregSlow, PartyAttributeFeedbackTipRef.ExercisePrioritise, PartyAttributeFeedbackTipRef.ExerciseReduceStress, PartyAttributeFeedbackTipRef.ExerciseSleep, PartyAttributeFeedbackTipRef.ExerciseSleepPreg, PartyAttributeFeedbackTipRef.ExerciseSocial, PartyAttributeFeedbackTipRef.ExerciseStressReduc, PartyAttributeFeedbackTipRef.FatsMaintain, PartyAttributeFeedbackTipRef.FoodLabelRead, PartyAttributeFeedbackTipRef.FruitBuyInSeason, PartyAttributeFeedbackTipRef.FruitSeasonMaintain, PartyAttributeFeedbackTipRef.GetAdvice, PartyAttributeFeedbackTipRef.GetRewards, PartyAttributeFeedbackTipRef.GetRewardsBP, PartyAttributeFeedbackTipRef.GetRewardsChol, PartyAttributeFeedbackTipRef.GetRewardsExercise, PartyAttributeFeedbackTipRef.GetRewardsGlucose, PartyAttributeFeedbackTipRef.HealthyEatMaintain, PartyAttributeFeedbackTipRef.HealthyFatsMaintain, PartyAttributeFeedbackTipRef.KeepFoodDiaryLowerBP, PartyAttributeFeedbackTipRef.KeepWeightBloodGluc, PartyAttributeFeedbackTipRef.KeepWeightBloodPress, PartyAttributeFeedbackTipRef.LeanMeatCook, PartyAttributeFeedbackTipRef.LimitFatCook, PartyAttributeFeedbackTipRef.LinkDeviceApp, PartyAttributeFeedbackTipRef.MoveGlucPhysActOOR, PartyAttributeFeedbackTipRef.MoveReduceStress, PartyAttributeFeedbackTipRef.PhysActBPressureOOR, PartyAttributeFeedbackTipRef.PhysActGlucose, PartyAttributeFeedbackTipRef.PhysActWeight, PartyAttributeFeedbackTipRef.PhysActivityHealth, PartyAttributeFeedbackTipRef.PhysActivityProgram, PartyAttributeFeedbackTipRef.PhysActivityRegular, PartyAttributeFeedbackTipRef.PhysicalActivityWalk, PartyAttributeFeedbackTipRef.PregBloodGlucose, PartyAttributeFeedbackTipRef.PregBloodPressure, PartyAttributeFeedbackTipRef.PregDietAdvice, PartyAttributeFeedbackTipRef.PregEatFruit, PartyAttributeFeedbackTipRef.PregEatRedMeat, PartyAttributeFeedbackTipRef.PregEatVitamins, PartyAttributeFeedbackTipRef.PregSaltReduce, PartyAttributeFeedbackTipRef.PregSaltyFoodAdvice, PartyAttributeFeedbackTipRef.PregSugaryDrinks, PartyAttributeFeedbackTipRef.PregTransfatReduce, PartyAttributeFeedbackTipRef.PregWholegrain, PartyAttributeFeedbackTipRef.PregnantBloodGlucose, PartyAttributeFeedbackTipRef.PregnantEatCalcBaby, PartyAttributeFeedbackTipRef.PregnantEatIron, PartyAttributeFeedbackTipRef.PregnantEatVeg, PartyAttributeFeedbackTipRef.PregnantSaltModerate, PartyAttributeFeedbackTipRef.PregnantSaltMonitor, PartyAttributeFeedbackTipRef.PregnanteatFruit, PartyAttributeFeedbackTipRef.PrgnantSugaryDrinks, PartyAttributeFeedbackTipRef.QuitSmokingSave, PartyAttributeFeedbackTipRef.RedMeatHealthy, PartyAttributeFeedbackTipRef.SaltBPMaintain, PartyAttributeFeedbackTipRef.SaltProcessedFood, PartyAttributeFeedbackTipRef.SaltyFoodAdvice, PartyAttributeFeedbackTipRef.SaturatedFatDecrease, PartyAttributeFeedbackTipRef.ShopBPReduce, PartyAttributeFeedbackTipRef.SleepSchedule, PartyAttributeFeedbackTipRef.SleepSchedulePreg, PartyAttributeFeedbackTipRef.SmokeAvoidDrink, PartyAttributeFeedbackTipRef.SmokeDontStart, PartyAttributeFeedbackTipRef.SmokePreg, PartyAttributeFeedbackTipRef.SmokeQuitAdvice, PartyAttributeFeedbackTipRef.SmokeQuitProgramme, PartyAttributeFeedbackTipRef.SmokeQuitUnwind, PartyAttributeFeedbackTipRef.SodiumReducLessSalt, PartyAttributeFeedbackTipRef.SodiumReduce, PartyAttributeFeedbackTipRef.SodiumReduceBP, PartyAttributeFeedbackTipRef.StrengthExercise, PartyAttributeFeedbackTipRef.StretchIncrease, PartyAttributeFeedbackTipRef.StretchRegular, PartyAttributeFeedbackTipRef.SugaryDrinkLabel, PartyAttributeFeedbackTipRef.SugaryDrinks, PartyAttributeFeedbackTipRef.SugaryDrinksMaintain, PartyAttributeFeedbackTipRef.SugaryDrinksReduce, PartyAttributeFeedbackTipRef.SugaryDrinksSoft, PartyAttributeFeedbackTipRef.TransFatReduce, PartyAttributeFeedbackTipRef.WaistCircumMaintain, PartyAttributeFeedbackTipRef.WaistCircumReduce, PartyAttributeFeedbackTipRef.WaistCircumfReduce, PartyAttributeFeedbackTipRef.WalkDriveLess, PartyAttributeFeedbackTipRef.WeightBloodGlucose, PartyAttributeFeedbackTipRef.WeightBloodPressure, PartyAttributeFeedbackTipRef.WeightDecreaseBP, PartyAttributeFeedbackTipRef.WeightLowerBloodGluc, PartyAttributeFeedbackTipRef.WeightMaintain, PartyAttributeFeedbackTipRef.WeightPregAdvice, PartyAttributeFeedbackTipRef.WholeGrainsDiet, PartyAttributeFeedbackTipRef.WholegrainAddPreg, PartyAttributeFeedbackTipRef.WholegrainChol1, PartyAttributeFeedbackTipRef.WholegrainChol2, PartyAttributeFeedbackTipRef.WholegrainMaintain, PartyAttributeFeedbackTipRef.BoostImmuneVeg,PartyAttributeFeedbackTipRef.CancerEatVeg,PartyAttributeFeedbackTipRef.EatBreakfastGlucose,PartyAttributeFeedbackTipRef.EatFruitConvenient,PartyAttributeFeedbackTipRef.EatFruitEasy,PartyAttributeFeedbackTipRef.EatFruitSnack,PartyAttributeFeedbackTipRef.EatProteinPlants,PartyAttributeFeedbackTipRef.EatVegIncrease,PartyAttributeFeedbackTipRef.FeelAnxious,PartyAttributeFeedbackTipRef.FibreVegIncrease,PartyAttributeFeedbackTipRef.FruitBuySeasonal,PartyAttributeFeedbackTipRef.HealthyEatIncr,PartyAttributeFeedbackTipRef.MoveBPPhysActOOR,PartyAttributeFeedbackTipRef.PhysActMoveOOR,PartyAttributeFeedbackTipRef.PregExistMedCond,PartyAttributeFeedbackTipRef.SaltCookReduce,PartyAttributeFeedbackTipRef.SaltFoodTypeReduce,PartyAttributeFeedbackTipRef.SaltSodiumReduce,PartyAttributeFeedbackTipRef.SodiumLowerGradually,PartyAttributeFeedbackTipRef.SodiumReduceAddSpice,PartyAttributeFeedbackTipRef.SodiumReduceIntake,PartyAttributeFeedbackTipRef.StressLevelslow,PartyAttributeFeedbackTipRef.WholeGrainsChoice,PartyAttributeFeedbackTipRef.WholeGrainsLabel,PartyAttributeFeedbackTipRef.WholeGrainsNutrients,PartyAttributeFeedbackTipRef.WholeGrainsRecipe,PartyAttributeFeedbackTipRef.SaltyFoodSodium,PartyAttributeFeedbackTipRef.SaltyFoodPreg]

    // Reordered and updated.
    case Unknown = -1
    case BMIDocEatHealthy = 7
    case BMIDocHealthyWeight = 8
    case ExecrciseMaintain = 9
    case WaistCircumfReduce = 11
    case ExerciseGoal = 12
    case ExerciseIncrease = 13
    case PhysicalActivityWalk = 14
    case ExerciseFun = 15
    case ExerciseSocial = 16
    case StretchIncrease = 20
    case ExercisDailyStrength = 22
    case MoveReduceStress = 28
    case QuitSmokingSave = 29
    case SmokeQuitAdvice = 30
    case SmokeQuitProgramme = 31
    case SmokeQuitUnwind = 32
    case EatFibre = 34
    case WeightLowerBloodGluc = 37
    case WeightPregAdvice = 39
    case GetAdvice = 40
    case EatBreakfast1 = 41
    case PhysActGlucose = 42
    case PregnantBloodGlucose = 43
    case ShopBPReduce = 46
    case WeightDecreaseBP = 48
    case PhysActBPressureOOR = 49
    case ActRewardsStress = 55
    case FatsMaintain = 56
    case WholegrainChol1 = 57
    case ExerciseStressReduc = 58
    case ExerciseSleep = 64
    case SleepSchedulePreg = 66
    case ExerciseSleepPreg = 67
    case PregnantSaltModerate = 68
    case SodiumReduce = 69
    case SaltProcessedFood = 71
    case PregnantSaltMonitor = 72
    case SugaryDrinks = 73
    case SugaryDrinksSoft = 74
    case SugaryDrinksReduce = 75
    case SugaryDrinkLabel = 76
    case PrgnantSugaryDrinks = 77
    case TransFatReduce = 78
    case FruitBuyInSeason = 80
    case EatFruitExtra = 81
    case PregnanteatFruit = 82
    case EatVeg = 83
    case PregnantEatVeg = 84
    case LeanMeatCook = 85
    case PregnantEatIron = 86
    case DairyCalPot = 87
    case DairyNoEat = 88
    case WholeGrainsDiet = 90
    case WholegrainAddPreg = 91
    case SaltyFoodAdvice = 93
    case PregSaltyFoodAdvice = 95
    case AlcoholHydrate = 96
    case DrinkSetLimit = 97
    case DrinkMakeSmallSize = 98
    case AlcoholMaintain = 99
    case DrinkPregNot = 100
    case WeightMaintain = 101
    case ExerciseDaily = 102
    case BMIPreg = 103
    case WaistCircumReduce = 104
    case WaistCircumMaintain = 105
    case ExercisePrioritise = 106
    case LinkDeviceApp = 107
    case GetRewards = 108
    case ExercisePreg = 109
    case StretchRegular = 110
    case ExercisePregSlow = 111
    case StrengthExercise = 112
    case WalkDriveLess = 114
    case PhysActWeight = 115
    case PhysActivityHealth = 116
    case ExercisePregPhys = 117
    case SmokeDontStart = 118
    case SmokeAvoidDrink = 119
    case SmokePreg = 120
    case GetRewardsGlucose = 123
    case WeightBloodGlucose = 124
    case KeepWeightBloodGluc = 125
    case PregBloodGlucose = 126
    case EatBreakfast2 = 127
    case CheckBloodSugar = 128
    case MoveGlucPhysActOOR = 129
    case PhysActivityRegular = 130
    case KeepFoodDiaryLowerBP = 131
    case SodiumReduceBP = 132
    case GetRewardsBP = 133
    case WeightBloodPressure = 134
    case KeepWeightBloodPress = 135
    case PhysActivityProgram = 136
    case PregBloodPressure = 137
    case SaturatedFatDecrease = 138
    case WholegrainChol2 = 139
    case ExerciseReduceStress = 140
    case HealthyEatMaintain = 141
    case GetRewardsChol = 142
    case LimitFatCook = 143
    case SleepSchedule = 144
    case GetRewardsExercise = 145
    case SodiumReducLessSalt = 146
    case FoodLabelRead = 147
    case PregSaltReduce = 148
    case SaltBPMaintain = 149
    case SugaryDrinksMaintain = 150
    case PregSugaryDrinks = 151
    case HealthyFatsMaintain = 152
    case PregTransfatReduce = 153
    case FruitSeasonMaintain = 154
    case EatfruitMaintain = 155
    case PregEatFruit = 156
    case EatVegMaintain = 157
    case PregEatVitamins = 158
    case RedMeatHealthy = 159
    case EatPlantProtein = 160
    case PregEatRedMeat = 161
    case EatDairy = 162
    case PregnantEatCalcBaby = 163
    case EatDairyIfNot = 164
    case WholegrainMaintain = 165
    case PregWholegrain = 166
    case DietAdvice = 167
    case PregDietAdvice = 168
    case ExerciseSmokeReduce = 169
    case ExercisKeepBloodGluc = 170
    case ExercKeepBloodPress = 171
    case FastFoodLowerPreg = 172
    case MoveBPPhysActOOR = 173
    case PhysActBloodSugar = 174
    case PhysActGlucoseOOR = 175
    case PhysActivityLinkApp = 176
    case PhysActivityPressure = 177
    case PhysActMoveOOR = 178
    case PregBloodPresMonitor = 179
    case PregBMI = 180
    case PregExercise = 181
    case PregExistMedCond = 182
    case PregExistMedCondDoc = 183
    case PregHealthySnacks = 184
    case PregLowerAnimalFat = 185
    case PregModExercise = 186
    case PregnantEatBreakfast = 187
    case PregnantEatCalcium = 188
    case PregnantEatCalDairy = 189
    case PregnantEatRegularly = 190
    case PregnantOmega3 = 191
    case PregnantRegularMeals = 192
    case PregnantSugarReduce = 193
    case PregnantSugaryFoods = 194
    case PregnantWatchFat = 195
    case PregnantWholeFoods = 196
    case PregSmokeDanger = 197
    case PregStretch = 198
    case PregTransFatAvoid = 199
    case ProcMeatReducePreg = 200
    case SmokePregReduceStres = 201
    case SmokeQuit = 202
    case WaistPregNotIndicat = 203
    case PositiveMed = 204
    case PositiveHigh = 205
    case EnergyLow = 206
    case EnergyMed = 207
    case EnergyHigh = 208
    case NegEmotionLow = 209
    case NegEmotionMed = 210
    case NegEmotionHigh = 211
    case AnxietyLow = 212
    case AnxietyMed = 213
    case AnxietyHigh = 214
    case SocialLow = 215
    case SocialMed = 216
    case SocialHigh = 217
    case StressorLow = 218
    case StressorMed = 219
    case StressorHigh = 220
    case TraumaticEvent = 221
    case MWBOutdated = 222
    case MWBUnknown = 223
    case LimitSatFats = 224
    case ReduceSatFats = 225
    case MakeRoomHealthFats = 226
    case LimitSatFatsPregnant = 227
    case LimitFatsPregnantOut = 228
    case UnsatFatsHealthy = 229
    case UnsatFatsEatMore = 230
    case UnsatFatsModeration = 231
    case UnsatFatsBye = 232
    case UnsatFatsModerate = 233
    case UnsatFatPregHealth = 234
    case UnsatFatPregnantOut = 235
    case HighProcessedHealthy = 236
    case HighProcessedOut = 237
    case HighProcessedSugar = 238
    case HighProcessedChoose = 239
    case HighProcessedHidden = 240
    case HighProcessedTrans = 241
    case HighProcesPregHealth = 242
    case HighProcessPregOut = 243
    case FastFoodsHealthy = 244
    case FastFoodsSkip = 245
    case FastFoodsPortion = 246
    case FastFoodsNoneGood = 247
    case FastFoodsSaveMoney = 248
    case FastFoodsStock = 249
    case FastFoodsPregHealthy = 250
    case FastFoodPregOut = 251
    case SugarFoodsHealthy = 252
    case SugarFoodsEmpty = 253
    case SugarFoodsHidden = 254
    case SugarFoodsCravings = 255
    case SugarFoodsPregHealth = 256
    case SugarFoodsPregOut = 257
    case AddedSugarHealthy = 258
    case AddedSugarCutBack = 259
    case AddSugarPregHealthy = 260
    case AddedSugarPregOut = 261
    case ProcessedMeatHealth = 262
    case ProcessedMeatWatch = 263
    case ProcessedMeatKnow = 264
    case ProcessedMeatReduce = 265
    case ProcessedMeatSwop = 266
    case ProcessedMeatFresh = 267
    case PrcssdMeatPregHealth = 268
    case ProcessedMeatPregOut = 269
    case RedMeatHealth = 270
    case RedMeatCookMethod = 271
    case RedMeatTrimFat = 272
    case RedMeatLean = 273
    case RedMeatKeepModerate = 274
    case RedMeatPregHealthy = 275
    case RedMeatPregOut = 276
    case Omega3Healthy = 277
    case Omega3Essential = 278
    case Omega3FattyFish = 279
    case Omega3Think = 280
    case Omega3NoExcuse = 281
    case Omega3CookingMethod = 282
    case Omega3PregHealthy = 283
    case Omega3PregOut = 284
    case CalcPotasHealthy = 285
    case CalcPotasCareBones = 286
    case CalcPotasLowFat = 287
    case CalcPotasFromFood = 288
    case CalcPotasSneakDay = 289
    case CalcPotasManagWeight = 290
    case CalcPotasPregHealthy = 291
    case CalcPotasPregOut = 292
    case CalcPotasNotConsume = 293
    case BreakfastHealthy = 294
    case BreakfastFirstThings = 295
    case BreakfastPriority = 296
    case BreakfastManagWeight = 297
    case BreakfastMakeHealthy = 298
    case BreakfastCreative = 299
    case BreakfastPregHealthy = 300
    case BreakfastPregOut = 301
    case LunchHealthy = 302
    case LunchKeepBalanced = 303
    case LunchScheduleIn = 304
    case LunchUpProductivity = 305
    case LunchPack = 306
    case LunchEatMindfully = 307
    case LunchPregHealthy = 308
    case LunchPregOut = 309
    case DinnerHealthy = 310
    case DinnerSpreadMeals = 311
    case DinnerVeggiesFirst = 312
    case DinnerMakeExtra = 313
    case DinnerPlanAhead = 314
    case DinnerFillUpFibre = 315
    case DinnerPregHealthy = 316
    case DinnerPregOut = 317
    case SnackingHealthy = 318
    case SnackingMindfully = 319
    case SnackingWholesome = 320
    case SnackingSkipBiscuits = 321
    case SnackingHabits = 322
    case SnackingPlan = 323
    case SnackingPregHealthy = 324
    case SnackingPregOut = 325
    case SaltBpReduce = 327
    case WholegrainNotLosses = 329
    case PositiveLow = 330
    
    public static let allValues: [PartyAttributeFeedbackTipRef] = [
        PartyAttributeFeedbackTipRef.Unknown,
        PartyAttributeFeedbackTipRef.BMIDocEatHealthy,
        PartyAttributeFeedbackTipRef.BMIDocHealthyWeight,
        PartyAttributeFeedbackTipRef.ExecrciseMaintain,
        PartyAttributeFeedbackTipRef.WaistCircumfReduce,
        PartyAttributeFeedbackTipRef.ExerciseGoal,
        PartyAttributeFeedbackTipRef.ExerciseIncrease,
        PartyAttributeFeedbackTipRef.PhysicalActivityWalk,
        PartyAttributeFeedbackTipRef.ExerciseFun,
        PartyAttributeFeedbackTipRef.ExerciseSocial,
        PartyAttributeFeedbackTipRef.StretchIncrease,
        PartyAttributeFeedbackTipRef.ExercisDailyStrength,
        PartyAttributeFeedbackTipRef.MoveReduceStress,
        PartyAttributeFeedbackTipRef.QuitSmokingSave,
        PartyAttributeFeedbackTipRef.SmokeQuitAdvice,
        PartyAttributeFeedbackTipRef.SmokeQuitProgramme,
        PartyAttributeFeedbackTipRef.SmokeQuitUnwind,
        PartyAttributeFeedbackTipRef.EatFibre,
        PartyAttributeFeedbackTipRef.WeightLowerBloodGluc,
        PartyAttributeFeedbackTipRef.WeightPregAdvice,
        PartyAttributeFeedbackTipRef.GetAdvice,
        PartyAttributeFeedbackTipRef.EatBreakfast1,
        PartyAttributeFeedbackTipRef.PhysActGlucose,
        PartyAttributeFeedbackTipRef.PregnantBloodGlucose,
        PartyAttributeFeedbackTipRef.ShopBPReduce,
        PartyAttributeFeedbackTipRef.WeightDecreaseBP,
        PartyAttributeFeedbackTipRef.PhysActBPressureOOR,
        PartyAttributeFeedbackTipRef.ActRewardsStress,
        PartyAttributeFeedbackTipRef.FatsMaintain,
        PartyAttributeFeedbackTipRef.WholegrainChol1,
        PartyAttributeFeedbackTipRef.ExerciseStressReduc,
        PartyAttributeFeedbackTipRef.ExerciseSleep,
        PartyAttributeFeedbackTipRef.SleepSchedulePreg,
        PartyAttributeFeedbackTipRef.ExerciseSleepPreg,
        PartyAttributeFeedbackTipRef.PregnantSaltModerate,
        PartyAttributeFeedbackTipRef.SodiumReduce,
        PartyAttributeFeedbackTipRef.SaltProcessedFood,
        PartyAttributeFeedbackTipRef.PregnantSaltMonitor,
        PartyAttributeFeedbackTipRef.SugaryDrinks,
        PartyAttributeFeedbackTipRef.SugaryDrinksSoft,
        PartyAttributeFeedbackTipRef.SugaryDrinksReduce,
        PartyAttributeFeedbackTipRef.SugaryDrinkLabel,
        PartyAttributeFeedbackTipRef.PrgnantSugaryDrinks,
        PartyAttributeFeedbackTipRef.TransFatReduce,
        PartyAttributeFeedbackTipRef.FruitBuyInSeason,
        PartyAttributeFeedbackTipRef.EatFruitExtra,
        PartyAttributeFeedbackTipRef.PregnanteatFruit,
        PartyAttributeFeedbackTipRef.EatVeg,
        PartyAttributeFeedbackTipRef.PregnantEatVeg,
        PartyAttributeFeedbackTipRef.LeanMeatCook,
        PartyAttributeFeedbackTipRef.PregnantEatIron,
        PartyAttributeFeedbackTipRef.DairyCalPot,
        PartyAttributeFeedbackTipRef.DairyNoEat,
        PartyAttributeFeedbackTipRef.WholeGrainsDiet,
        PartyAttributeFeedbackTipRef.WholegrainAddPreg,
        PartyAttributeFeedbackTipRef.SaltyFoodAdvice,
        PartyAttributeFeedbackTipRef.PregSaltyFoodAdvice,
        PartyAttributeFeedbackTipRef.AlcoholHydrate,
        PartyAttributeFeedbackTipRef.DrinkSetLimit,
        PartyAttributeFeedbackTipRef.DrinkMakeSmallSize,
        PartyAttributeFeedbackTipRef.AlcoholMaintain,
        PartyAttributeFeedbackTipRef.DrinkPregNot,
        PartyAttributeFeedbackTipRef.WeightMaintain,
        PartyAttributeFeedbackTipRef.ExerciseDaily,
        PartyAttributeFeedbackTipRef.BMIPreg,
        PartyAttributeFeedbackTipRef.WaistCircumReduce,
        PartyAttributeFeedbackTipRef.WaistCircumMaintain,
        PartyAttributeFeedbackTipRef.ExercisePrioritise,
        PartyAttributeFeedbackTipRef.LinkDeviceApp,
        PartyAttributeFeedbackTipRef.GetRewards,
        PartyAttributeFeedbackTipRef.ExercisePreg,
        PartyAttributeFeedbackTipRef.StretchRegular,
        PartyAttributeFeedbackTipRef.ExercisePregSlow,
        PartyAttributeFeedbackTipRef.StrengthExercise,
        PartyAttributeFeedbackTipRef.WalkDriveLess,
        PartyAttributeFeedbackTipRef.PhysActWeight,
        PartyAttributeFeedbackTipRef.PhysActivityHealth,
        PartyAttributeFeedbackTipRef.ExercisePregPhys,
        PartyAttributeFeedbackTipRef.SmokeDontStart,
        PartyAttributeFeedbackTipRef.SmokeAvoidDrink,
        PartyAttributeFeedbackTipRef.SmokePreg,
        PartyAttributeFeedbackTipRef.GetRewardsGlucose,
        PartyAttributeFeedbackTipRef.WeightBloodGlucose,
        PartyAttributeFeedbackTipRef.KeepWeightBloodGluc,
        PartyAttributeFeedbackTipRef.PregBloodGlucose,
        PartyAttributeFeedbackTipRef.EatBreakfast2,
        PartyAttributeFeedbackTipRef.CheckBloodSugar,
        PartyAttributeFeedbackTipRef.MoveGlucPhysActOOR,
        PartyAttributeFeedbackTipRef.PhysActivityRegular,
        PartyAttributeFeedbackTipRef.KeepFoodDiaryLowerBP,
        PartyAttributeFeedbackTipRef.SodiumReduceBP,
        PartyAttributeFeedbackTipRef.GetRewardsBP,
        PartyAttributeFeedbackTipRef.WeightBloodPressure,
        PartyAttributeFeedbackTipRef.KeepWeightBloodPress,
        PartyAttributeFeedbackTipRef.PhysActivityProgram,
        PartyAttributeFeedbackTipRef.PregBloodPressure,
        PartyAttributeFeedbackTipRef.SaturatedFatDecrease,
        PartyAttributeFeedbackTipRef.WholegrainChol2,
        PartyAttributeFeedbackTipRef.ExerciseReduceStress,
        PartyAttributeFeedbackTipRef.HealthyEatMaintain,
        PartyAttributeFeedbackTipRef.GetRewardsChol,
        PartyAttributeFeedbackTipRef.LimitFatCook,
        PartyAttributeFeedbackTipRef.SleepSchedule,
        PartyAttributeFeedbackTipRef.GetRewardsExercise,
        PartyAttributeFeedbackTipRef.SodiumReducLessSalt,
        PartyAttributeFeedbackTipRef.FoodLabelRead,
        PartyAttributeFeedbackTipRef.PregSaltReduce,
        PartyAttributeFeedbackTipRef.SaltBPMaintain,
        PartyAttributeFeedbackTipRef.SugaryDrinksMaintain,
        PartyAttributeFeedbackTipRef.PregSugaryDrinks,
        PartyAttributeFeedbackTipRef.HealthyFatsMaintain,
        PartyAttributeFeedbackTipRef.PregTransfatReduce,
        PartyAttributeFeedbackTipRef.FruitSeasonMaintain,
        PartyAttributeFeedbackTipRef.EatfruitMaintain,
        PartyAttributeFeedbackTipRef.PregEatFruit,
        PartyAttributeFeedbackTipRef.EatVegMaintain,
        PartyAttributeFeedbackTipRef.PregEatVitamins,
        PartyAttributeFeedbackTipRef.RedMeatHealthy,
        PartyAttributeFeedbackTipRef.EatPlantProtein,
        PartyAttributeFeedbackTipRef.PregEatRedMeat,
        PartyAttributeFeedbackTipRef.EatDairy,
        PartyAttributeFeedbackTipRef.PregnantEatCalcBaby,
        PartyAttributeFeedbackTipRef.EatDairyIfNot,
        PartyAttributeFeedbackTipRef.WholegrainMaintain,
        PartyAttributeFeedbackTipRef.PregWholegrain,
        PartyAttributeFeedbackTipRef.DietAdvice,
        PartyAttributeFeedbackTipRef.PregDietAdvice,
        PartyAttributeFeedbackTipRef.ExerciseSmokeReduce,
        PartyAttributeFeedbackTipRef.ExercisKeepBloodGluc,
        PartyAttributeFeedbackTipRef.ExercKeepBloodPress,
        PartyAttributeFeedbackTipRef.FastFoodLowerPreg,
        PartyAttributeFeedbackTipRef.MoveBPPhysActOOR,
        PartyAttributeFeedbackTipRef.PhysActBloodSugar,
        PartyAttributeFeedbackTipRef.PhysActGlucoseOOR,
        PartyAttributeFeedbackTipRef.PhysActivityLinkApp,
        PartyAttributeFeedbackTipRef.PhysActivityPressure,
        PartyAttributeFeedbackTipRef.PhysActMoveOOR,
        PartyAttributeFeedbackTipRef.PregBloodPresMonitor,
        PartyAttributeFeedbackTipRef.PregBMI,
        PartyAttributeFeedbackTipRef.PregExercise,
        PartyAttributeFeedbackTipRef.PregExistMedCond,
        PartyAttributeFeedbackTipRef.PregExistMedCondDoc,
        PartyAttributeFeedbackTipRef.PregHealthySnacks,
        PartyAttributeFeedbackTipRef.PregLowerAnimalFat,
        PartyAttributeFeedbackTipRef.PregModExercise,
        PartyAttributeFeedbackTipRef.PregnantEatBreakfast,
        PartyAttributeFeedbackTipRef.PregnantEatCalcium,
        PartyAttributeFeedbackTipRef.PregnantEatCalDairy,
        PartyAttributeFeedbackTipRef.PregnantEatRegularly,
        PartyAttributeFeedbackTipRef.PregnantOmega3,
        PartyAttributeFeedbackTipRef.PregnantRegularMeals,
        PartyAttributeFeedbackTipRef.PregnantSugarReduce,
        PartyAttributeFeedbackTipRef.PregnantSugaryFoods,
        PartyAttributeFeedbackTipRef.PregnantWatchFat,
        PartyAttributeFeedbackTipRef.PregnantWholeFoods,
        PartyAttributeFeedbackTipRef.PregSmokeDanger,
        PartyAttributeFeedbackTipRef.PregStretch,
        PartyAttributeFeedbackTipRef.PregTransFatAvoid,
        PartyAttributeFeedbackTipRef.ProcMeatReducePreg,
        PartyAttributeFeedbackTipRef.SmokePregReduceStres,
        PartyAttributeFeedbackTipRef.SmokeQuit,
        PartyAttributeFeedbackTipRef.WaistPregNotIndicat,
        PartyAttributeFeedbackTipRef.PositiveMed,
        PartyAttributeFeedbackTipRef.PositiveHigh,
        PartyAttributeFeedbackTipRef.EnergyLow,
        PartyAttributeFeedbackTipRef.EnergyMed,
        PartyAttributeFeedbackTipRef.EnergyHigh,
        PartyAttributeFeedbackTipRef.NegEmotionLow,
        PartyAttributeFeedbackTipRef.NegEmotionMed,
        PartyAttributeFeedbackTipRef.NegEmotionHigh,
        PartyAttributeFeedbackTipRef.AnxietyLow,
        PartyAttributeFeedbackTipRef.AnxietyMed,
        PartyAttributeFeedbackTipRef.AnxietyHigh,
        PartyAttributeFeedbackTipRef.SocialLow,
        PartyAttributeFeedbackTipRef.SocialMed,
        PartyAttributeFeedbackTipRef.SocialHigh,
        PartyAttributeFeedbackTipRef.StressorLow,
        PartyAttributeFeedbackTipRef.StressorMed,
        PartyAttributeFeedbackTipRef.StressorHigh,
        PartyAttributeFeedbackTipRef.TraumaticEvent,
        PartyAttributeFeedbackTipRef.MWBOutdated,
        PartyAttributeFeedbackTipRef.MWBUnknown,
        PartyAttributeFeedbackTipRef.LimitSatFats,
        PartyAttributeFeedbackTipRef.ReduceSatFats,
        PartyAttributeFeedbackTipRef.MakeRoomHealthFats,
        PartyAttributeFeedbackTipRef.LimitSatFatsPregnant,
        PartyAttributeFeedbackTipRef.LimitFatsPregnantOut,
        PartyAttributeFeedbackTipRef.UnsatFatsHealthy,
        PartyAttributeFeedbackTipRef.UnsatFatsEatMore,
        PartyAttributeFeedbackTipRef.UnsatFatsModeration,
        PartyAttributeFeedbackTipRef.UnsatFatsBye,
        PartyAttributeFeedbackTipRef.UnsatFatsModerate,
        PartyAttributeFeedbackTipRef.UnsatFatPregHealth,
        PartyAttributeFeedbackTipRef.UnsatFatPregnantOut,
        PartyAttributeFeedbackTipRef.HighProcessedHealthy,
        PartyAttributeFeedbackTipRef.HighProcessedOut,
        PartyAttributeFeedbackTipRef.HighProcessedSugar,
        PartyAttributeFeedbackTipRef.HighProcessedChoose,
        PartyAttributeFeedbackTipRef.HighProcessedHidden,
        PartyAttributeFeedbackTipRef.HighProcessedTrans,
        PartyAttributeFeedbackTipRef.HighProcesPregHealth,
        PartyAttributeFeedbackTipRef.HighProcessPregOut,
        PartyAttributeFeedbackTipRef.FastFoodsHealthy,
        PartyAttributeFeedbackTipRef.FastFoodsSkip,
        PartyAttributeFeedbackTipRef.FastFoodsPortion,
        PartyAttributeFeedbackTipRef.FastFoodsNoneGood,
        PartyAttributeFeedbackTipRef.FastFoodsSaveMoney,
        PartyAttributeFeedbackTipRef.FastFoodsStock,
        PartyAttributeFeedbackTipRef.FastFoodsPregHealthy,
        PartyAttributeFeedbackTipRef.FastFoodPregOut,
        PartyAttributeFeedbackTipRef.SugarFoodsHealthy,
        PartyAttributeFeedbackTipRef.SugarFoodsEmpty,
        PartyAttributeFeedbackTipRef.SugarFoodsHidden,
        PartyAttributeFeedbackTipRef.SugarFoodsCravings,
        PartyAttributeFeedbackTipRef.SugarFoodsPregHealth,
        PartyAttributeFeedbackTipRef.SugarFoodsPregOut,
        PartyAttributeFeedbackTipRef.AddedSugarHealthy,
        PartyAttributeFeedbackTipRef.AddedSugarCutBack,
        PartyAttributeFeedbackTipRef.AddSugarPregHealthy,
        PartyAttributeFeedbackTipRef.AddedSugarPregOut,
        PartyAttributeFeedbackTipRef.ProcessedMeatHealth,
        PartyAttributeFeedbackTipRef.ProcessedMeatWatch,
        PartyAttributeFeedbackTipRef.ProcessedMeatKnow,
        PartyAttributeFeedbackTipRef.ProcessedMeatReduce,
        PartyAttributeFeedbackTipRef.ProcessedMeatSwop,
        PartyAttributeFeedbackTipRef.ProcessedMeatFresh,
        PartyAttributeFeedbackTipRef.PrcssdMeatPregHealth,
        PartyAttributeFeedbackTipRef.ProcessedMeatPregOut,
        PartyAttributeFeedbackTipRef.RedMeatHealth,
        PartyAttributeFeedbackTipRef.RedMeatCookMethod,
        PartyAttributeFeedbackTipRef.RedMeatTrimFat,
        PartyAttributeFeedbackTipRef.RedMeatLean,
        PartyAttributeFeedbackTipRef.RedMeatKeepModerate,
        PartyAttributeFeedbackTipRef.RedMeatPregHealthy,
        PartyAttributeFeedbackTipRef.RedMeatPregOut,
        PartyAttributeFeedbackTipRef.Omega3Healthy,
        PartyAttributeFeedbackTipRef.Omega3Essential,
        PartyAttributeFeedbackTipRef.Omega3FattyFish,
        PartyAttributeFeedbackTipRef.Omega3Think,
        PartyAttributeFeedbackTipRef.Omega3NoExcuse,
        PartyAttributeFeedbackTipRef.Omega3CookingMethod,
        PartyAttributeFeedbackTipRef.Omega3PregHealthy,
        PartyAttributeFeedbackTipRef.Omega3PregOut,
        PartyAttributeFeedbackTipRef.CalcPotasHealthy,
        PartyAttributeFeedbackTipRef.CalcPotasCareBones,
        PartyAttributeFeedbackTipRef.CalcPotasLowFat,
        PartyAttributeFeedbackTipRef.CalcPotasFromFood,
        PartyAttributeFeedbackTipRef.CalcPotasSneakDay,
        PartyAttributeFeedbackTipRef.CalcPotasManagWeight,
        PartyAttributeFeedbackTipRef.CalcPotasPregHealthy,
        PartyAttributeFeedbackTipRef.CalcPotasPregOut,
        PartyAttributeFeedbackTipRef.CalcPotasNotConsume,
        PartyAttributeFeedbackTipRef.BreakfastHealthy,
        PartyAttributeFeedbackTipRef.BreakfastFirstThings,
        PartyAttributeFeedbackTipRef.BreakfastPriority,
        PartyAttributeFeedbackTipRef.BreakfastManagWeight,
        PartyAttributeFeedbackTipRef.BreakfastMakeHealthy,
        PartyAttributeFeedbackTipRef.BreakfastCreative,
        PartyAttributeFeedbackTipRef.BreakfastPregHealthy,
        PartyAttributeFeedbackTipRef.BreakfastPregOut,
        PartyAttributeFeedbackTipRef.LunchHealthy,
        PartyAttributeFeedbackTipRef.LunchKeepBalanced,
        PartyAttributeFeedbackTipRef.LunchScheduleIn,
        PartyAttributeFeedbackTipRef.LunchUpProductivity,
        PartyAttributeFeedbackTipRef.LunchPack,
        PartyAttributeFeedbackTipRef.LunchEatMindfully,
        PartyAttributeFeedbackTipRef.LunchPregHealthy,
        PartyAttributeFeedbackTipRef.LunchPregOut,
        PartyAttributeFeedbackTipRef.DinnerHealthy,
        PartyAttributeFeedbackTipRef.DinnerSpreadMeals,
        PartyAttributeFeedbackTipRef.DinnerVeggiesFirst,
        PartyAttributeFeedbackTipRef.DinnerMakeExtra,
        PartyAttributeFeedbackTipRef.DinnerPlanAhead,
        PartyAttributeFeedbackTipRef.DinnerFillUpFibre,
        PartyAttributeFeedbackTipRef.DinnerPregHealthy,
        PartyAttributeFeedbackTipRef.DinnerPregOut,
        PartyAttributeFeedbackTipRef.SnackingHealthy,
        PartyAttributeFeedbackTipRef.SnackingMindfully,
        PartyAttributeFeedbackTipRef.SnackingWholesome,
        PartyAttributeFeedbackTipRef.SnackingSkipBiscuits,
        PartyAttributeFeedbackTipRef.SnackingHabits,
        PartyAttributeFeedbackTipRef.SnackingPlan,
        PartyAttributeFeedbackTipRef.SnackingPregHealthy,
        PartyAttributeFeedbackTipRef.SnackingPregOut,
        PartyAttributeFeedbackTipRef.SaltBpReduce,
        PartyAttributeFeedbackTipRef.WholegrainNotLosses,
        PartyAttributeFeedbackTipRef.PositiveLow
    ]
}
