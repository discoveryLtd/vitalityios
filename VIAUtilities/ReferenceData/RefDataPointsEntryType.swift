//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum PointsEntryTypeRef: Int, Hashable { 
    case Unknown = -1
    case BMI = 11 // name:BMI note:null 
    case BMIHR = 12 // name:BMIHR note:null 
    case BloodPressure = 15 // name:BloodPressure note:null 
    case BloodPressureHR = 16 // name:BloodPressureHR note:null 
    case Cardiovascular = 50 // name:Cardiovascular Screening note:Cardiovascular Screening 
    case CholesterolHR = 9 // name:CholesterolHR note:null 
    case Colonoscopy = 47 // name:Colonoscopy Screening note:Colonoscopy Screening
    case CotinineTest = 59 // name:Cotinine Test note:Cotinine Test
    case AbdomAorticUS = 60 // name:Abdominal aortic ultrasound
    case Cycling = 63 // name:Cycling note:Cycling
    case DentalCheckups = 45 // name:Dental Checkups Screening note:Dental Checkups Screening 
    case DeviceLinking = 30 // name:DeviceLinking note:null 
    case Distance = 7 // name:Distance note:null 
    case EnergyExpend = 5 // name:EnergyExpend note:null
    case EyeTest = 58 // name:Eye Test note:Eye Test
    case FitHR = 75 // name:FitHR note:FitHR
    case FOBT = 46 // name:Faecal Occult Blood Test (FOBT) Screening note:Faecal Occult Blood Test (FOBT) Screening 
    case Flu = 53 // name:Flu Vaccination note:Flu Vaccination 
    case GastricCancer = 41 // name:Gastric Cancer Screening note:Gastric Cancer Screening 
    case Glaucoma = 43 // name:Glaucoma Screening note:Glaucoma Screening 
    case BloodGlucose = 13 // name:BloodGlucose note:null 
    case GlucoseHR = 14 // name:GlucoseHR note:null 
    case GymVisit = 37 // name:GymVisit note:GymVisit 
    case HIV = 44 // name:Human Immunodeficiency Virus (HIV) Screening note:Human Immunodeficiency Virus (HIV) Screening 
    case HPV = 54 // name:Human Papillomavirus (HPV) Vaccination note:Human Papillomavirus (HPV) Vaccination 
    case HbA1c = 17 // name:HbA1c note:null 
    case HbA1cHR = 18 // name:HbA1cHR note:null 
    case HealthyFoodSpend = 38 // name:althy Food Spend note:Healthy Food spend for the member 
    case Heartrate = 4 // name:Heartrate note:null 
    case HepatitisB = 57 // name:Hepatitis-B Vaccination note:Hepatitis-B Vaccination 
    case LDLCholesterol = 21 // name:LDLCholesterol note:null 
    case LDLCholesterolHR = 22 // name:LDLCholesterolHR note:null 
    case LipidRatio = 24 // name:Lipid Ratio note:null 
    case LipidRatioHR = 25 // name:Lipid Ratio Healthy Range note:null 
    case LiverCancer = 52 // name:Liver Cancer Screening note:Liver Cancer Screening 
    case LungCancer = 49 // name:Lung Cancer Screening note:Lung Cancer Screening 
    case MWBPsychological = 28 // name:MWB Psychological Assessment Completed note:null 
    case MWBSocial = 29 // name:MWB Social Assessment Completed note:null 
    case MWBStressor = 27 // name:MWB Stressor Assessment Completed note:null 
    case Mammography = 39 // name:Mammography Screening note:Mammography Screening 
    case ManualPoints = 36 // name:Manual Points note:Points manually allocated 
    case MeasuredFitComplete = 2 // name:MeasuredFitComplete note:null 
    case NonsmokersDeclrtn = 3 // name:NonsmokersDeclrtn note:null 
    case OFE = 23 // name:OFE note:null 
    case OvarianCancer = 51 // name:Ovarian Cancer Screening note:Ovarian Cancer Screening
    case ParkrunParticipation = 76 // name:Parkrun Participation note:Parkrun Participation
    case ParkrunVolunteer = 77 // name:Parkrun Volunteer note:Parkrun Volunteer
    case PSA = 42 // name:Prostate Specific Antigen (PSA) Screening note:Prostate Specific Antigen (PSA) Screening 
    case PapSmear = 40 // name:Pap Smear Screening note:Pap Smear Screening 
    case Pneumococcal = 55 // name:Pneumococcal Vaccination note:Pneumococcal Vaccination 
    case Running = 62 // name:Running note:Running
    case SkinCancer = 48 // name:Skin Cancer Screening note:Skin Cancer Screening 
    case Speed = 6 // name:Speed note:null 
    case Steps = 10 // name:Steps note:null 
    case Swimming = 65 // name:Swimming note:Swimming 
    case TotCholesterol = 8 // name:TotCholesterol note:null 
    case Triathlon = 64 // name:Triathlon note:Triathlon
    case UrineProtien = 19 // name:UrineProtein note:null 
    case UrineProtienHR = 20 // name:UrineProteinHR note:null 
    case VHRAssmntCompleted = 1 // name:VHRAssmntCompleted note:null 
    case VNAAssmntCompleted = 26 // name:VNAAssmntCompleted note:null 
    case Walking = 66 // name:Walking note:Walking 
    case Zoster = 56 // name:Zoster Vaccination note:Zoster Vaccination 

    public static let allValues: [PointsEntryTypeRef] = [PointsEntryTypeRef.Unknown, PointsEntryTypeRef.BMI, PointsEntryTypeRef.BMIHR, PointsEntryTypeRef.BloodPressure, PointsEntryTypeRef.BloodPressureHR, PointsEntryTypeRef.Cardiovascular, PointsEntryTypeRef.CholesterolHR, PointsEntryTypeRef.Colonoscopy, PointsEntryTypeRef.CotinineTest, PointsEntryTypeRef.AbdomAorticUS, PointsEntryTypeRef.Cycling, PointsEntryTypeRef.DentalCheckups, PointsEntryTypeRef.DeviceLinking, PointsEntryTypeRef.Distance, PointsEntryTypeRef.EnergyExpend, PointsEntryTypeRef.EyeTest, PointsEntryTypeRef.FitHR, PointsEntryTypeRef.FOBT, PointsEntryTypeRef.Flu, PointsEntryTypeRef.GastricCancer, PointsEntryTypeRef.Glaucoma, PointsEntryTypeRef.BloodGlucose, PointsEntryTypeRef.GlucoseHR, PointsEntryTypeRef.GymVisit, PointsEntryTypeRef.HIV, PointsEntryTypeRef.HPV, PointsEntryTypeRef.HbA1c, PointsEntryTypeRef.HbA1cHR, PointsEntryTypeRef.HealthyFoodSpend, PointsEntryTypeRef.Heartrate, PointsEntryTypeRef.HepatitisB, PointsEntryTypeRef.LDLCholesterol, PointsEntryTypeRef.LDLCholesterolHR, PointsEntryTypeRef.LipidRatio, PointsEntryTypeRef.LipidRatioHR, PointsEntryTypeRef.LiverCancer, PointsEntryTypeRef.LungCancer, PointsEntryTypeRef.MWBPsychological, PointsEntryTypeRef.MWBSocial, PointsEntryTypeRef.MWBStressor, PointsEntryTypeRef.Mammography, PointsEntryTypeRef.ManualPoints, PointsEntryTypeRef.MeasuredFitComplete, PointsEntryTypeRef.NonsmokersDeclrtn, PointsEntryTypeRef.OFE, PointsEntryTypeRef.OvarianCancer, PointsEntryTypeRef.ParkrunParticipation, PointsEntryTypeRef.ParkrunVolunteer ,PointsEntryTypeRef.PSA, PointsEntryTypeRef.PapSmear, PointsEntryTypeRef.Pneumococcal, PointsEntryTypeRef.Running, PointsEntryTypeRef.SkinCancer, PointsEntryTypeRef.Speed, PointsEntryTypeRef.Steps, PointsEntryTypeRef.Swimming, PointsEntryTypeRef.TotCholesterol, PointsEntryTypeRef.Triathlon, PointsEntryTypeRef.UrineProtien, PointsEntryTypeRef.UrineProtienHR, PointsEntryTypeRef.VHRAssmntCompleted, PointsEntryTypeRef.VNAAssmntCompleted, PointsEntryTypeRef.Walking, PointsEntryTypeRef.Zoster]

}
