import Foundation

extension PartyAttributeTypeRef {
    
    public func vhcBMICaptureSortOrder() -> Int {
        switch self {
        case .Height:
            return 0
        case .Weight:
            return 1
        default:
            return -1
        }
    }
    
}
