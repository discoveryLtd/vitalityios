import Foundation

@objc public enum TenantReference: Int, Hashable {
    
    case Unknown    = -999  /* Unknown Tenant. Fallback purposes */
    case CA         = 9998  /* Central Asset */
    case SLI        = 25    /* Sumitomo Japan */
    case UKE        = 26    /* Healthy U UK */
    case IGI        = 28    /* IGI Vitality Pakistan */
    case ASR        = 34    /* ASR Netherlands */
    case EC         = 31    /* Saludsa Ecuivida Ecuador */
    case MLI        = 103   /* Manulife Canada */
    case GI         = 18    /* Generali Germany */
}
