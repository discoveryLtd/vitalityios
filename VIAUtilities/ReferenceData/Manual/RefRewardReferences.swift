//
//  RewardReferences.swift
//  VitalityActive
//
//  Created by admin on 2017/11/27.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation

@objc public enum RewardReferences: Int, Hashable {
    
    case Unknown =  -1
    case Wheelspin = 299991
    case StarbucksVoucher = 299992
    case VitalityPoints = 299993
    case AmazonVoucher = 299994
    case ZapposVoucher = 299995
    case Token = 299996
    case ChooseReward = 299997
    case CineworldOrVue = 2600003
    case Cineworld = 2600004
    case Vue = 2600005
    case noReward = 2600006
	case EasyTickets = 2899992
	case FoodPanda = 2899993
    case Cinemark = 310000021
    case JuanValdez = 310000020 
    
    case WheelspinSLI = 2599991
    case MachiLawson = 2599992
    case SmoothieLawson = 2599993
    case EGiftStarbucks = 2599994
    case YogurtLawson = 2599995
    
    case AppleWatchVitalityCoins = 250001
    case AppleWatchVitalityCoins2 = 250002
    case AppleWatchVitalityCoins3 = 250003
    
    /* FC-26793 & FC-26012 : UKE CR Interim Voucher Solution & UKE Segmentation */
    case WheelSpinINDIA = 260000048
    case BookMyShow = 260000037
    case CafeCoffeeDay = 260000043
    
    case WheelSpinSWI = 260000041
    case Spotify = 260000038
    
    case WheelSpinPOLAND = 260000046
    case AmazonSmileVoucher = 260000004
    
    case WheelSpinCHANNELISLAND = 260000036
    case WheelSpinUSA = 260000047
    
    case WheelSpinSMALLCOUNTRIES = 260000049
    
    case WheelSpinSP = 2600008
    case WheelSpinIN = 2600009
    case GoalAchievedReward = 2600010
}
