import Foundation

// This file was manually created by Wilmar from a database extract.
// The unit of measure data isn't modelled as reference data, that's
// why it isn't part of the reference data csv dump
@objc public enum UnitOfMeasureRef: Int, EnumCollection, Hashable {
    case Unknown = -1
    case BMI = 100164
    case Centimeter = 100111
    case FastingGlucoseMilligramsPerDeciLitre = 100120
    case FastingGlucoseMillimolesPerLitre = 100122
    case Foot = 100106
    case FootInch = 100117
    case Gram = 100101
    case HDLCholestrolMilligramsPerDeciLitre = 100124
    case HDLCholestrolMillimolesPerLitre = 100126
    case Inch = 100105
    case Kilogram = 100102
    case Kilometer = 100104
    case KilometersPerHour = 101501
    case LDLCholestrolMilligramsPerDeciLitre = 100123
    case LDLCholestrolMillimolesPerLitre = 100125
    case Meter = 100103
    case Mile = 100107
    case MillimeterOfMercury = 100112
    case MillimolesPerLitre = 100113
    case Ounce = 100108
    case Percentage = 100114
    case Pound = 100109
    case RandomGlucoseMilligramsPerDeciLitre = 100128
    case RandomGlucoseMillimolesPerLitre = 100127
    case Second = 101500
    case Stone = 100116
    case SystolicKiloPascal = 100115
    case Systolicmillimeterofmercury = 100129
    case Ton = 100110
    case TotalCholestrolMilligramsPerDeciLitre = 100118
    case TotalCholestrolMillimolesPerLitre = 310000075
    case TriglycerideMilligramsPerDeciLitre = 100119
    case TriglycerideMillimolesPerLitre = 100121
    case PerDay = 101502
    case PerWeek = 101503
    case Minutes = 101504
    case Hours = 101505
    case Days = 101506
    case StonePound = 101507
    case KiloCaloriesPerHour = 101508
    case Kilocalories = 101509
    case MetersPerSecond = 101510
    case MinutesPerKilometer = 101511
    case BeatsPerMinute = 101512
}
