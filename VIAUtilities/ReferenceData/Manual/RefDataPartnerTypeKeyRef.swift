//
//  RefDataPartnerTypeRef.swift
//  VIAUtilities
//
//  Created by Dexter Anthony Ambrad on 6/29/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation

// This file is manually created  by Dexter Anthony Ambrad from the API response getPartnersByCategory

@objc public enum PartnerTypeKeyRef: Int, Hashable {
    case Starbucks = 116
    case HotelsDotCom = 126
    case Lawson = 224
    
}
