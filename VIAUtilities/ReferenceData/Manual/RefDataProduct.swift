import Foundation

// This file was manually created by Wilmar from a database extract.
// The unit of measure data isn't modelled as reference data, that's
// why it isn't part of the reference data csv dump
@objc public enum ProductRef: Int, EnumCollection {
    case Unknown = -1
    case AppleWatch = 1
    case AppleWatchBenefit = 2
    case VitalityActive = 3
    case ActiveRewards = 4
    case DeviceXyz = 5
    case DeviceAbc = 6
    case TestProduct1 = 7
    case TestProduct2 = 8
    case TestProduct3 = 9
    case TestProduct4 = 10
    case TestProduct5 = 11
    case TestProduct6 = 12
}
