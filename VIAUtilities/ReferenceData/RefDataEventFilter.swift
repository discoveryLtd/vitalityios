//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum EventFilterRef: Int, Hashable { 
    case Unknown = -1
    case Mobile = 2 // name:Mobile Front End note:undefined 
    case MobileEventsFeed = 3 // name:Mobile Events Feed note:undefined 
    case VSP = 1 // name:VSP Front End note:undefined 

    public static let allValues: [EventFilterRef] = [EventFilterRef.Unknown, EventFilterRef.Mobile, EventFilterRef.MobileEventsFeed, EventFilterRef.VSP]

}
