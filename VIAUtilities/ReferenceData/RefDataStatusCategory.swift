//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum StatusCategoryRef: Int, Hashable { 
    case Unknown = -1
    case CarryoverStatus = 2 // name:Carryover Status note:Carryover Status 
    case OverallStatus = 4 // name:Overall Status note:Overall Status 
    case OverrideStatus = 3 // name:Manual Override Status note:Manual Override Status 
    case PointsStatus = 1 // name:Points Status note:Points Status 

    public static let allValues: [StatusCategoryRef] = [StatusCategoryRef.Unknown, StatusCategoryRef.CarryoverStatus, StatusCategoryRef.OverallStatus, StatusCategoryRef.OverrideStatus, StatusCategoryRef.PointsStatus]

}
