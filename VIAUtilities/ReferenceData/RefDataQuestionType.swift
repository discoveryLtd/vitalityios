//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum QuestionTypeRef: Int, Hashable { 
    case Unknown = -1
    case CheckList = 4 // name:CheckList note:CheckList 
    case Date = 7 // name:Date note:Date 
    case DecimalRange = 6 // name:DecimalRange note:DecimalRange 
    case FormatText = 2 // name:FormatText note:FormatText 
    case FreeText = 3 // name:FreeText note:FreeText 
    case Label = 13 // name:Label note:Label 
    case MultiDecimalRange = 12 // name:MultiDecimalRange note:MultiDecimalRange 
    case MultiNumberRange = 11 // name:MultiNumberRange note:MultiNumberRange 
    case MultiSelect = 10 // name:MultiSelect note:MultiSelect 
    case NumberRange = 1 // name:NumberRange note:NumberRange 
    case ChildNumberRange = 14
    case OptionList = 5 // name:OptionList note:OptionList 
    case Percentage = 8 // name:Percentage note:Percentage 
    case SingleSelect = 9 // name:SingleSelect note:SingleSelect 

    public static let allValues: [QuestionTypeRef] = [QuestionTypeRef.Unknown, QuestionTypeRef.CheckList, QuestionTypeRef.Date, QuestionTypeRef.DecimalRange, QuestionTypeRef.FormatText, QuestionTypeRef.FreeText, QuestionTypeRef.Label, QuestionTypeRef.MultiDecimalRange, QuestionTypeRef.MultiNumberRange, QuestionTypeRef.MultiSelect, QuestionTypeRef.NumberRange, QuestionTypeRef.OptionList, QuestionTypeRef.Percentage, QuestionTypeRef.SingleSelect, QuestionTypeRef.ChildNumberRange]
}
