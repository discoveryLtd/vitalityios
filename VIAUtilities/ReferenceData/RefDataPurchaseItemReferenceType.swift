//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum PurchaseItemReferenceTypeRef: Int, Hashable { 
    case Unknown = -1
    case PartnerProductCode = 1 // name:PartnerProductCode  note:Code that uniquely identifies the product for the partner 

    public static let allValues: [PurchaseItemReferenceTypeRef] = [PurchaseItemReferenceTypeRef.Unknown, PurchaseItemReferenceTypeRef.PartnerProductCode]

}
