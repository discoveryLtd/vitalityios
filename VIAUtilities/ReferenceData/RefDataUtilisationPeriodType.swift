//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum UtilisationPeriodTypeRef: Int, Hashable { 
    case Unknown = -1
    case CalendarMonth = 2 // name:Calendar Month note:Calendar Month 
    case CalendarWeek = 1 // name:Calendar Week note:Calendar Week 
    case CalendarYear = 3 // name:Calendar Year note:Calendar Year 
    case VitMembershipPeriod = 4 // name:Vitality Membership Period note:Vitality Membership Period 

    public static let allValues: [UtilisationPeriodTypeRef] = [UtilisationPeriodTypeRef.Unknown, UtilisationPeriodTypeRef.CalendarMonth, UtilisationPeriodTypeRef.CalendarWeek, UtilisationPeriodTypeRef.CalendarYear, UtilisationPeriodTypeRef.VitMembershipPeriod]

}
