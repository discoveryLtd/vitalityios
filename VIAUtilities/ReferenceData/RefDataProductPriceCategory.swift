//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum ProductPriceCategoryRef: Int, Hashable { 
    case Unknown = -1
    case AdidasFlatRate = 28 // name:Standard discount for all AdidasDP purchase within limit. note:Standard discount for all AdidasDP purchase within limit. 
    case AdidasHGLim = 16 // name:Discount applicable when the limit is reached for AdidasHG note:Discount applicable when the limit is reached for AdidasHG 
    case AdidasHGLimit = 27 // name:Discount applicable when the limit is reached for AdidasHG note:Discount applicable when the limit is reached for AdidasHG 
    case AdidasHGTSTFlatRate = 17 // name:Flat rate discount applicable for AdidasHGTST note:Flat rate discount applicable for AdidasHGTST 
    case DeviceAbcLimitPrice = 6 // name:DeviceAbc Limit Price note:DeviceAbc Limit Price 
    case DeviceAbcPrice = 1 // name:DeviceAbcPrice note:Pricing associated with DeviceAbc (example) 
    case DeviceXyzBronze = 2 // name:DeviceXyzBronze note:DeviceXyz pricing for bronze member 
    case DeviceXyzGold = 4 // name:DeviceXyzGold note:DeviceXyz pricing for gold members 
    case DeviceXyzPlatinum = 5 // name:DeviceXyzPlatinum note:DeviceXyz pricing for platinum members 
    case DeviceXyzSilver = 3 // name:DeviceXyzSilver note:DeviceXyz pricing for silver members 
    case FFNationalFlat = 26 // name:Fitness First National Flat note:Fitness First National Flat 
    case FFRegionalFlat = 25 // name:Fitness First Regional Flat note:Fitness First Regional Flat 
    case GarminDPLimit = 29 // name:Discount applicable when the limit is reached for GarminDP note:Discount applicable when the limit is reached for GarminDP 
    case GarminFlatRate = 30 // name:Standard discount for all GarminDP purchase within limit. note:Standard discount for all GarminDP purchase within limit. 
    case KonamiCategory1 = 18 // name:Konami Category 1 note:Konami Category 1 
    case KonamiCategory2 = 19 // name:Konami Category 2 note:Konami Category 2 
    case KonamiCategory3 = 20 // name:Konami Category 3 note:Konami Category 3 
    case KonamiCategory4 = 21 // name:Konami Category 4 note:Konami Category 4 
    case NHGEligibleBranch = 24 // name:Nuffield Health Gym Non-Eligible Branch note:null 
    case OisixHFLim = 9 // name:Discount applicable when the limit is reached for OisixHF note:Discount applicable when the limit is reached for OisixHF 
    case OisixSTD = 10 // name:For all members who have not done VHR and VHC or Members whose VHR and VHC has expired note:For all members who have not done VHR and VHC or Members whose VHR and VHC has expired 
    case OisixVHRandVHC = 12 // name:For all members who have done their VHR and VHC note:For all members who have done their VHR and VHC 
    case OisixVHRorVHC = 11 // name:For all members who have done either their VHR or VHC note:For all members who have done either their VHR or VHC 
    case PolarDPLimit = 7 // name:PolarDPLimit note:Discount applicable when the limit is reached for Polar. 
    case PolarFlatRate = 8 // name:PolarFlatRate note:Standard discount for all PolarDP purchase within limit. 
    case RenaissanceGB = 22 // name:Renaissance Gym Benefit note:Renaissance Gym Benefit flat discount 
    case RenaissanceGBM = 23 // name:RenaissanceGBM note:Renaissance Master Gym Benefit flat discount 
    case SOFTBANKDP1_FlatRate = 13 // name:SOFTBANKDP1 - FlatRate Discount note:SOFTBANKDP1 - FlatRate Discount 
    case SOFTBANKDP2_FlatRate = 14 // name:SOFTBANKDP2 - FlatRate Discount note:SOFTBANKDP2 - FlatRate Discount 
    case SOFTBANKDP3_FlatRate = 15 // name:SOFTBANKDP3 - FlatRate Discount note:SOFTBANKDP3 - FlatRate Discount 

    public static let allValues: [ProductPriceCategoryRef] = [ProductPriceCategoryRef.Unknown, ProductPriceCategoryRef.AdidasFlatRate, ProductPriceCategoryRef.AdidasHGLim, ProductPriceCategoryRef.AdidasHGLimit, ProductPriceCategoryRef.AdidasHGTSTFlatRate, ProductPriceCategoryRef.DeviceAbcLimitPrice, ProductPriceCategoryRef.DeviceAbcPrice, ProductPriceCategoryRef.DeviceXyzBronze, ProductPriceCategoryRef.DeviceXyzGold, ProductPriceCategoryRef.DeviceXyzPlatinum, ProductPriceCategoryRef.DeviceXyzSilver, ProductPriceCategoryRef.FFNationalFlat, ProductPriceCategoryRef.FFRegionalFlat, ProductPriceCategoryRef.GarminDPLimit, ProductPriceCategoryRef.GarminFlatRate, ProductPriceCategoryRef.KonamiCategory1, ProductPriceCategoryRef.KonamiCategory2, ProductPriceCategoryRef.KonamiCategory3, ProductPriceCategoryRef.KonamiCategory4, ProductPriceCategoryRef.NHGEligibleBranch, ProductPriceCategoryRef.OisixHFLim, ProductPriceCategoryRef.OisixSTD, ProductPriceCategoryRef.OisixVHRandVHC, ProductPriceCategoryRef.OisixVHRorVHC, ProductPriceCategoryRef.PolarDPLimit, ProductPriceCategoryRef.PolarFlatRate, ProductPriceCategoryRef.RenaissanceGB, ProductPriceCategoryRef.RenaissanceGBM, ProductPriceCategoryRef.SOFTBANKDP1_FlatRate, ProductPriceCategoryRef.SOFTBANKDP2_FlatRate, ProductPriceCategoryRef.SOFTBANKDP3_FlatRate]

}
