//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum PersonTitleRef: Int, Hashable { 
    case Unknown = -1
    case Chancellor = 18 // name:Chancellor note:Chancellor: for the chancellor of a university. 
    case Dame = 10 // name:Dame note:Dame: for women who have been honoured with a British knighthood in their own right. 
    case Dr = 16 // name:Dr note:Dr: for the holder of a doctoral degree (e.g. PhD,or MD in many countries). 
    case Esq = 13 // name:Esq note:Esq: in the UK used postnominally in written addresses for any adult male if no pre-nominal honorific (Mr,Dr,etc.) Is used. 
    case Excellence = 14 // name:Excellence note:Excellence: a title of honor given to certain high officials,as governors,ambassadors,royalty,nobility,and Roman Catholic bishops and archbishops,(preceded by his,your,etc.). 
    case Fr = 21 // name:Fr note:Fr: (Father) for priests in Catholic and Eastern Christianity,as well as some Anglican or Episcopalian groups. 
    case Gentleman = 8 // name:Gentleman note:Gentleman: Originally a social rank,standing below an esquire and above a yeoman. 
    case HerHisHonour = 15 // name:Her/His Honour note:Her/His Honour: Used for judges,mayors and magistrates in some countries. 
    case Imam = 24 // name:Imam note:Imam: for Islamic clergymen,specially the ones who lead prayers and deliver sermons. 
    case Lady = 12 // name:Lady note:Lady: for female peers with the rank of baroness,viscountess,countess,and marchioness,or the wives of men who hold the equivalent titles. 
    case Lord = 11 // name:Lord note:Lord: for male barons,viscounts,earls,and marquesses,as well as some of their children. 
    case Master = 1 // name:Master note:Master: for young boy. 
    case Miss = 3 // name:Miss note:Miss: for girls,unmarried women and (in the UK) married women who continue to use their maiden name (although 'Ms' is often preferred for the last two). 
    case Mr = 2 // name:Mr note:Mr: for men,regardless of marital status,who do not have another professional or academic title. 
    case Mrs = 4 // name:Mrs note:Mrs: for married women who do not have another professional or academic title. 
    case Ms = 5 // name:Ms note:Ms: for women,regardless of marital status or when marital status is unknown. 
    case Mx = 6 // name:Mx note:Mx: used as a gender-neutral honorific or for those who do not identify as male or female. 
    case Pr = 22 // name:Pr note:Pr: (Pastor) used generally for members of the Christian clergy regardless of affiliation,but especially in Protestant denominations. 
    case Professor = 17 // name:Professor note:Professor: for a person who holds the academic rank of professor in a university or other institution. 
    case Rabbi = 23 // name:Rabbi note:Rabbi: (Rabbi) In Judaism,a rabbi is an ordained religious officiant or a teacher of Torah. 
    case Rev = 20 // name:Rev note:Rev: Reverence used generally for members of the Christian clergy regardless of affiliation,but especially in Catholic and Protestant denominations. 
    case Sir = 7 // name:Sir note:Sir: for men,formally if they have a British knighthood or if they are a baronet (used with first name or full name,never surname alone) or generally (used on its own) as a term of general respect 
    case Sire = 9 // name:Sire note:Sire: a term of address for a male monarch. 
    case ViceChancellor = 19 // name:ViceChancellor note:Vice-Chancellor: for the vice-chancellor of a university. 

    public static let allValues: [PersonTitleRef] = [PersonTitleRef.Unknown, PersonTitleRef.Chancellor, PersonTitleRef.Dame, PersonTitleRef.Dr, PersonTitleRef.Esq, PersonTitleRef.Excellence, PersonTitleRef.Fr, PersonTitleRef.Gentleman, PersonTitleRef.HerHisHonour, PersonTitleRef.Imam, PersonTitleRef.Lady, PersonTitleRef.Lord, PersonTitleRef.Master, PersonTitleRef.Miss, PersonTitleRef.Mr, PersonTitleRef.Mrs, PersonTitleRef.Ms, PersonTitleRef.Mx, PersonTitleRef.Pr, PersonTitleRef.Professor, PersonTitleRef.Rabbi, PersonTitleRef.Rev, PersonTitleRef.Sir, PersonTitleRef.Sire, PersonTitleRef.ViceChancellor]

}
