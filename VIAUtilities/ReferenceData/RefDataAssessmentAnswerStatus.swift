//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum AssessmentAnswerStatusRef: Int, Hashable { 
    case Unknown = -1
    case Answered = 3 // name:Answered note:Answered 
    case Completed = 5 // name:Completed note:Completed 
    case Overridden = 4 // name:Overridden note:Overridden 
    case Prepopulated = 2 // name:Prepopulated note:Pre-populated 
    case Unanswered = 1 // name:Unanswered note:Unanswered 

    public static let allValues: [AssessmentAnswerStatusRef] = [AssessmentAnswerStatusRef.Unknown, AssessmentAnswerStatusRef.Answered, AssessmentAnswerStatusRef.Completed, AssessmentAnswerStatusRef.Overridden, AssessmentAnswerStatusRef.Prepopulated, AssessmentAnswerStatusRef.Unanswered]

}
