//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum MeasurementSystemRef: Int, Hashable { 
    case Unknown = -1
    case Imperialsystem = 2 // name:Imperial system note:Imperial system 
    case Metricsystem = 1 // name:Metric system note:Metric system 
    case UScustomary = 3 // name:United States customary units note:United States customary units 

    public static let allValues: [MeasurementSystemRef] = [MeasurementSystemRef.Unknown, MeasurementSystemRef.Imperialsystem, MeasurementSystemRef.Metricsystem, MeasurementSystemRef.UScustomary]

}
