//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum RewardSelectionTypeRef: Int, Hashable { 
    case Unknown = -1
    case Choice = 2 // name:Choice note:This selection type depicts a group of Rewards that are available for a Choice  reward option. This could be used for the Swap/Substitution options as well. 
    case RewardSelection = 3 // name:Reward Selection note:This selection type depicts a group of Rewards that are available for a Choice  reward option. This could be used for the Swap/Substitution options as well. 
    case WheelSpin = 1 // name:WheelSpin note:This selection type depicts a group of Rewards that are available for a WheelSpin probabilistic reward option. 

    public static let allValues: [RewardSelectionTypeRef] = [RewardSelectionTypeRef.Unknown, RewardSelectionTypeRef.Choice, RewardSelectionTypeRef.RewardSelection, RewardSelectionTypeRef.WheelSpin]

}
