//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum ExchangeTypeRef: Int, Hashable { 
    case Unknown = -1
    case Donated = 2 // name:Donated note:Reward has been donated 
    case Selected = 1 // name:Selected note:New reward has been selected. Where the original reward was an opportunity to get another reward 
    case Swapped = 3 // name:Swapped note:An existing reward has been swapped for a new reward 

    public static let allValues: [ExchangeTypeRef] = [ExchangeTypeRef.Unknown, ExchangeTypeRef.Donated, ExchangeTypeRef.Selected, ExchangeTypeRef.Swapped]

}
