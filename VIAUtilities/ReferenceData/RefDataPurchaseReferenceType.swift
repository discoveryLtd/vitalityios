//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum PurchaseReferenceTypeRef: Int, Hashable { 
    case Unknown = -1
    case InvoiceRef = 2 // name:InvoiceRef note:The unique invoice reference number 
    case OrderNumber = 1 // name:Order Number note:The partners number that uniquely identifies the order 
    case PartnerTransRef = 3 // name:PartnerTransRef note:The unique partner transaction number 

    public static let allValues: [PurchaseReferenceTypeRef] = [PurchaseReferenceTypeRef.Unknown, PurchaseReferenceTypeRef.InvoiceRef, PurchaseReferenceTypeRef.OrderNumber, PurchaseReferenceTypeRef.PartnerTransRef]

}
