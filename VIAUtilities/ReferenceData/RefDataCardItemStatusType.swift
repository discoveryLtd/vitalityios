//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum CardItemStatusTypeRef: Int, Hashable { 
    case Unknown = -1
    case Available = 2 // name:Available note:Available 
    case Done = 5 // name:Done note:Done 
    case Expired = 3 // name:Expired note:Expired 
    case Linked = 1 // name:Linked note:Linked 
    case Redeemed = 4 // name:Redeemed note:Redeemed 

    public static let allValues: [CardItemStatusTypeRef] = [CardItemStatusTypeRef.Unknown, CardItemStatusTypeRef.Available, CardItemStatusTypeRef.Done, CardItemStatusTypeRef.Expired, CardItemStatusTypeRef.Linked, CardItemStatusTypeRef.Redeemed]

}
