//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum QuestionValidValueTypeRef: Int, Hashable { 
    case Unknown = -1
    case Boolean = 6 // name:Boolean note:Boolean 
    case LowerLimit = 3 // name:Lower Limit note:Lower Limit 
    case Text = 5 // name:Text note:Text 
    case UpperLimit = 4 // name:Upper Limit note:Upper Limit 
    case ValueMaximum = 1 // name:Value Maximum note:Value Maximum 
    case ValueMinimum = 2 // name:Value Minimum note:Value Minimum 

    public static let allValues: [QuestionValidValueTypeRef] = [QuestionValidValueTypeRef.Unknown, QuestionValidValueTypeRef.Boolean, QuestionValidValueTypeRef.LowerLimit, QuestionValidValueTypeRef.Text, QuestionValidValueTypeRef.UpperLimit, QuestionValidValueTypeRef.ValueMaximum, QuestionValidValueTypeRef.ValueMinimum]

}
