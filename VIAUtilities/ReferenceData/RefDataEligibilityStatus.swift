//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum EligibilityStatusRef: Int, Hashable { 
    case Unknown = -1
    case Eligible = 1 // name:Eligible note:Eligible 
    case Ineligible = 2 // name:Ineligible note:Ineligible 

    public static let allValues: [EligibilityStatusRef] = [EligibilityStatusRef.Unknown, EligibilityStatusRef.Eligible, EligibilityStatusRef.Ineligible]

}
