//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum ApplicationTypeRef: Int, Hashable { 
    case Unknown = -1
    case SU = 2 // name:Sumitomo note:Sumitomo 
    case VA = 1 // name:Vitality Active note:Vitality Active 

    public static let allValues: [ApplicationTypeRef] = [ApplicationTypeRef.Unknown, ApplicationTypeRef.SU, ApplicationTypeRef.VA]

}
