//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum PartyWebAddressRolePurposeTypeRef: Int, Hashable { 
    case Unknown = -1
    case GoalNotification = 2 // name:Goal Notification note:Goal Notification 
    case GroupChallenge = 1 // name:Group Challenge Feedback note:Group Challenge Feedback 
    case InformationOnly = 4 // name:Information Only note:Information Only 
    case RewardNotification = 3 // name:Reward Notification note:Reward Notification 

    public static let allValues: [PartyWebAddressRolePurposeTypeRef] = [PartyWebAddressRolePurposeTypeRef.Unknown, PartyWebAddressRolePurposeTypeRef.GoalNotification, PartyWebAddressRolePurposeTypeRef.GroupChallenge, PartyWebAddressRolePurposeTypeRef.InformationOnly, PartyWebAddressRolePurposeTypeRef.RewardNotification]

}
