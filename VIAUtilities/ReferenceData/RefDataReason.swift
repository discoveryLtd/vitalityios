//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum ReasonRef: Int, Hashable { 
    case Unknown = -1
    case Annual = 104 // name:You have reached the annual membership points limit value for this activity note:null 
    case AnnualCategory = 54 // name:You have reached the annual individual points limit value for this activity category note:null 
    case Daily = 101 // name:You have reached the daily membership points limit value for this activity note:null 
    case DailyCategory = 51 // name:You have reached the daily individual points limit value for this activity category note:null 
    case HghrPtsAwardedGrped = 211 // name:Higher points have already been awarded for an activity in this group note:null 
    case HigherActivity = 216 // name:A higher points earning activity was submitted for this day note:null 
    case IndividualAnnual = 4 // name:You have reached the annual individual points limit value for this activity note:null 
    case IndividualDaily = 1 // name:You have reached the daily individual points limit value for this activity note:null 
    case IndividualMonthly = 3 // name:You have reached the monthly individual points limit value for this activity note:null 
    case IndividualWeekly = 2 // name:You have reached the weekly  individual points limit value for this activity note:null 
    case IndvMembershipPeriod = 5 // name:You have reached the individual points limit value for this activity in the current membership period note:null 
    case InsufficientActivity = 214 // name:Insufficient activity to earn points note:null 
    case MaxGroupedEvents = 210 // name:The maximum number of grouped events has been reached note:null 
    case MaximumEvents = 202 // name:The maximum number of events has been reached note:null 
    case MaximumPoints = 200 // name:You earned the maximum points available for this activity note:null 
    case MembPeriodCategory = 55 // name:You have reached the individual points limit value for this activity category in the current membership period note:null 
    case MembPeriodCategoryLimit = 218 // name:Only <PointsEntry.EarnedPoints> out of a posssible <PointsEntry.PotentialPoints> went towards your status because your physical activity threshold has now been achieved.  note:MembPeriodCategoryLimit 
    case MembPeriodPoints = 217 // name:Even though you may complete this activity more than once a membership period, you only earn points for this activity once every membership period. note:MembPeriodPoints 
    case MembershipAnnual = 154 // name:You have reached the annual membership points limit value for this activity category note:null 
    case MembershipDaily = 151 // name:You have reached the daily membership points limit value for this activity category note:null 
    case MembershipMonthly = 153 // name:You have reached the monthly membership points limit value for this activity category note:null 
    case MembershipPeriod = 155 // name:You have reached the membership points limit value for this activity category for the current membership period note:null 
    case MembershipWeekly = 152 // name:You have reached the weekly membership points limit value for this activity category note:null 
    case Monthly = 103 // name:You have reached the monthly membership points limit value for this activity note:null 
    case MonthlyCategory = 53 // name:You have reached the monthly individual points limit value for this activity category note:null 
    case NotFemale = 206 // name:You are not a female note:null 
    case NotHealthy = 215 // name:The measurement submitted did not fall within the healthy range note:null 
    case NotMale = 205 // name:You are not a male note:null 
    case NotVerified = 212 // name:Source of data is not verified note:null 
    case NotWithinRange = 207 // name:You are not within range note:null 
    case OFE = 213 // name:Organised Fitness Event note:null 
    case Period = 105 // name:You have reached the membership points limit value for this activity in the current membership period note:null 
    case PointsAlreadyAwarded = 201 // name:Points have already been awarded for this activity note:null 
    case TooOld = 204 // name:You are too old note:null 
    case TooSoon = 208 // name:Too soon since last event note:null 
    case TooSoonLastGrpedEvent = 209 // name:Too soon since last event grouped event note:null 
    case TooYoung = 203 // name:You are too young note:null 
    case Weekly = 102 // name:You have reached the weekly membership points limit value for this activity note:null 
    case WeeklyCategory = 52 // name:You have reached the weekly individual points limit value for this activity category note:null 

    public static let allValues: [ReasonRef] = [ReasonRef.Unknown, ReasonRef.Annual, ReasonRef.AnnualCategory, ReasonRef.Daily, ReasonRef.DailyCategory, ReasonRef.HghrPtsAwardedGrped, ReasonRef.HigherActivity, ReasonRef.IndividualAnnual, ReasonRef.IndividualDaily, ReasonRef.IndividualMonthly, ReasonRef.IndividualWeekly, ReasonRef.IndvMembershipPeriod, ReasonRef.InsufficientActivity, ReasonRef.MaxGroupedEvents, ReasonRef.MaximumEvents, ReasonRef.MaximumPoints, ReasonRef.MembPeriodCategory, ReasonRef.MembPeriodCategoryLimit, ReasonRef.MembPeriodPoints, ReasonRef.MembershipAnnual, ReasonRef.MembershipDaily, ReasonRef.MembershipMonthly, ReasonRef.MembershipPeriod, ReasonRef.MembershipWeekly, ReasonRef.Monthly, ReasonRef.MonthlyCategory, ReasonRef.NotFemale, ReasonRef.NotHealthy, ReasonRef.NotMale, ReasonRef.NotVerified, ReasonRef.NotWithinRange, ReasonRef.OFE, ReasonRef.Period, ReasonRef.PointsAlreadyAwarded, ReasonRef.TooOld, ReasonRef.TooSoon, ReasonRef.TooSoonLastGrpedEvent, ReasonRef.TooYoung, ReasonRef.Weekly, ReasonRef.WeeklyCategory]

}
