//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum PartyEmailRolePurposeTypeRef: Int, Hashable { 
    case Unknown = -1
    case AssessmentFeedback = 4 // name:Assessment Feedback note:Assessment Feedback 
    case AssessmentRequests = 3 // name:Assessment Requests note:Assessment Requests 
    case CarrierEmail = 5 // name:Carrier email address note:Email address that is maintained by the carrier. 
    case MarketingInfo = 2 // name:Marketing Information note:Marketing Information 
    case PointsStatements = 1 // name:Points Statements note:Points Statements 
    case VitalityEmail = 6 // name:Vitality email address note:Email address that is used in the Vitality App 

    public static let allValues: [PartyEmailRolePurposeTypeRef] = [PartyEmailRolePurposeTypeRef.Unknown, PartyEmailRolePurposeTypeRef.AssessmentFeedback, PartyEmailRolePurposeTypeRef.AssessmentRequests, PartyEmailRolePurposeTypeRef.CarrierEmail, PartyEmailRolePurposeTypeRef.MarketingInfo, PartyEmailRolePurposeTypeRef.PointsStatements, PartyEmailRolePurposeTypeRef.VitalityEmail]

}
