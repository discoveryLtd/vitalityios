//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum AgreementStatusTypeRef: Int, Hashable { 
    case Unknown = -1
    case ACTIVE = 1 // name: Active note: Active 
    case CANCEL = 2 // name: Cancel note: Cancel 
    case REINSTATE = 4 // name: Reinstate note: Reinstate 
    case SUSPENDED = 6 // name: Suspended note: Suspended 
    case SYNC_CANCEL = 3 // name: Sync Cancel note: Sync Cancel 
    case SYNC_REINSTATE = 5 // name: Sync Reinstate note: Sync Reinstate 

    public static let allValues: [AgreementStatusTypeRef] = [AgreementStatusTypeRef.Unknown, AgreementStatusTypeRef.ACTIVE, AgreementStatusTypeRef.CANCEL, AgreementStatusTypeRef.REINSTATE, AgreementStatusTypeRef.SUSPENDED, AgreementStatusTypeRef.SYNC_CANCEL, AgreementStatusTypeRef.SYNC_REINSTATE]

}
