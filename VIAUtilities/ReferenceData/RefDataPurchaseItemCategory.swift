//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum PurchaseItemCategoryRef: Int, Hashable { 
    case Unknown = -1
    case Healthy = 1 // name:Healthy Food Item note:Indicates the category for the healthy food item purchased 
    case Unhealthy = 2 // name:Unhealthy Food Item note:Indicates the category for the unhealthy food item purchased 

    public static let allValues: [PurchaseItemCategoryRef] = [PurchaseItemCategoryRef.Unknown, PurchaseItemCategoryRef.Healthy, PurchaseItemCategoryRef.Unhealthy]

}
