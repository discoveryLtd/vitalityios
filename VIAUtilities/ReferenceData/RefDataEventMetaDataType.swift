//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum EventMetaDataTypeRef: Int, Hashable { 
    case Unknown = -1
    case ActiveEnergyExpend = 93 // name:Active energy expended that can be distinctly attributed to the users fitness activity note:Active energy expended that can be distinctly attributed to the users fitness activity 
    case AgreementID = 168 // name:The Id of the agreement in the Programme Context note:The Id of the agreement in the Programme Context 
    case AgreementType = 169 // name:The Type of the agreement within the Programme Context note:The Type of the agreement within the Programme Context 
    case AmbTmpHumidity = 156 // name:Measured surrounding humidity for the ambient temperature measurement note:Measured surrounding humidity for the ambient temperature measurement 
    case AmbTmpLocatAccuracy = 160 // name:Accuracy value of the location for the ambient temperature measurement note:Accuracy value of the location for the ambient temperature measurement 
    case AmbTmpLocatAltitude = 159 // name:Altitude value of the place for the ambient temperature measurement note:Altitude value of the place for the ambient temperature measurement 
    case AmbTmpLocatLatitude = 157 // name:Latitude value for ambient temperature measurement note:Latitude value for ambient temperature measurement 
    case AmbTmpLocatLongitude = 158 // name:Longitude value for ambient temperature measurement note:Longitude value for ambient temperature measurement 
    case AverageHeartRate = 66 // name:Average measure of the number of heart beats per minute during a workout session note:Average measure of the number of heart beats per minute during a workout session 
    case AveragePace = 87 // name:Average measure of minutes per kilometer during a workout note:Average measure of minutes per kilometer during a workout 
    case AveragePower = 84 // name:Average measure of power  note:Average measure of power  
    case AverageSpeed = 58 // name:Average measure of speed during a workout session  note:Average measure of speed during a workout session  
    case AverageStrideRate = 75 // name:Average number of steps taken per minute during a workout session note:Average number of steps taken per minute during a workout session 
    case AvgActiveEnergyExpen = 94 // name:Average active energy expended that can be distinctly attributed to the users fitness activity note:Average active energy expended that can be distinctly attributed to the users fitness activity 
    case AvgCadenceCycling = 78 // name:Average measure of the pedaling rate (in revolutions per minute [rpm]) when cycling note:Average measure of the pedaling rate (in revolutions per minute [rpm]) when cycling 
    case AvgEnergyExpenditure = 57 // name:Average energy expended that can be distinctly attributed to the users fitness activity note:Average energy expended that can be distinctly attributed to the users fitness activity 
    case AvgFoodEnergyRate = 24 // name:The average rate that the energy is used note:The average rate that the energy is used 
    case AvgRestEnergyExpend = 96 // name:Average amount of energy expended while minimally active (user's basal metabolic rate) note:Average amount of energy expended while minimally active (user's basal metabolic rate) 
    case AvgStrokeSwimming = 81 // name:Average stroke value at which the user is swimming note:Average stroke value at which the user is swimming 
    case AwardedRewardId = 187 // name:Awarded Reward Id note:Awarded Reward Id 
    case BMI = 107 // name:Body Mass Index measured in kg/square metres note:Body Mass Index measured in kg/square metres 
    case BPLBloodPressure = 100 // name:Blood pressure load note:Blood pressure load 
    case BasalMetabolicRate = 113 // name:BMR in kcal per day note:BMR in kcal per day 
    case BloodCalcium = 138 // name:Amount of Calcium in the blood (measured quantity in mg/dL) note:Amount of Calcium in the blood (measured quantity in mg/dL) 
    case BloodChromium = 139 // name:Amount of Chromium in the blood (measured quantity in ?g/dL) note:Amount of Chromium in the blood (measured quantity in ?g/dL) 
    case BloodFolicAcid = 140 // name:Amount of Folic Acid in the blood (measured quantity in ng/mL) note:Amount of Folic Acid in the blood (measured quantity in ng/mL) 
    case BloodGlucMeasureType = 117 // name:Type of the sample blood for the blood glucose measurement note:Type of the sample blood for the blood glucose measurement 
    case BloodGlucSampleSrc = 118 // name:Source type of the blood vessel where the sample is taken note:Source type of the blood vessel where the sample is taken 
    case BloodGlucose = 114 // name:Amount of glucose in the blood note:Amount of glucose in the blood 
    case BloodMagnesium = 141 // name:Amount of Magnesium in the blood (measured quantity in mg/dL) note:Amount of Magnesium in the blood (measured quantity in mg/dL) 
    case BloodOxygen = 122 // name:Amount of oxygen in the blood note:Amount of oxygen in the blood 
    case BloodOxygenSpo2 = 123 // name:Oxygen saturation value in the blood note:Oxygen saturation value in the blood 
    case BloodPotassium = 142 // name:Amount of Potassium in the blood (measured quantity in mEq/L) note:Amount of Potassium in the blood (measured quantity in mEq/L) 
    case BloodSodium = 143 // name:Amount of Sodium in the blood (measured quantity in mEq/L) note:Amount of Sodium in the blood (measured quantity in mEq/L) 
    case BloodVitaminB12 = 144 // name:Amount of Vitamin B12 in the blood (measured quantity in pg/dL) note:Amount of Vitamin B12 in the blood (measured quantity in pg/dL) 
    case BloodVitaminD = 153 // name:Amount of Vitamin D note:Amount of Vitamin D 
    case BloodZinc = 145 // name:Amount of Zinc in the blood (measured quantity in ?g/dL) note:Amount of Zinc in the blood (measured quantity in ?g/dL) 
    case BodyFatValue = 110 // name:Value of fat weight in the body (kg) note:Value of fat weight in the body (kg) 
    case BodyHeight = 97 // name:Value of body height note:Value of body height 
    case BodyMuscleValue = 111 // name:Value of muscle weight in the body (kg) note:Value of muscle weight in the body (kg) 
    case BodyTemperature = 155 // name:Value of Temperature (measured quantity in Degress Celsius) note:Value of Temperature (measured quantity in Degress Celsius) 
    case BodyWaterValue = 112 // name:Value of water weight in the body note:Value of water weight in the body 
    case BodyWeight = 106 // name:Value of body weight (kg) note:Value of body weight (kg) 
    case BoneValue = 108 // name:Bone value note:Bone value 
    case BrandName = 19 // name:The name of the brand for which the data is received note:The name of the brand for which the data is received 
    case CRP = 147 // name:Amount of C-Reactive Protein in the blood (measured quantity in mg/dL) note:Amount of C-Reactive Protein in the blood (measured quantity in mg/dL) 
    case CreatineKinase = 146 // name:Amount of Creatine Kinase in the blood (measured quantity in U/L) note:Amount of Creatine Kinase in the blood (measured quantity in U/L) 
    case DCI = 109 // name:D-Chiro-Inositol of weight note:D-Chiro-Inositol of weight 
    case DeepSleep = 14 // name:Amount of time in deep sleep (hours) note:Amount of time in deep sleep (hours) 
    case Description = 51 // name:Description of the data recorded note:Description of the data recorded 
    case DeviceDataCategory = 1 // name:The category for the device data uploaded note:The category for the device data uploaded 
    case DiastolicBP = 99 // name:Minimum blood pressure recorded note:Minimum blood pressure recorded 
    case DiscMaraRun = 176 // name:Discovery Marathon Run note:Discovery Marathon Run 
    case Distance = 55 // name:Actual or Calculated distance travelled during a workout session or during the course of the day note:Actual or Calculated distance travelled during a workout session or during the course of the day 
    case DocumentReference = 167 // name:The document reference to the documents  note:The document reference to the documents  
    case DrugTime = 119 // name:The time that the drug was consumed note:The time that the drug was consumed 
    case Duration = 50 // name:Amount of time for which the user was active (measured in seconds) note:Amount of time for which the user was active (measured in seconds) 
    case ECGAverageHeartRate = 129 // name:Average measure of the number of heart beats per minute during a ECG session note:Average measure of the number of heart beats per minute during a ECG session 
    case ECGData = 126 // name:Data for the ECG diagram note:Data for the ECG diagram 
    case ECGDataFormat = 127 // name:Data format for the ECG diagram note:Data format for the ECG diagram 
    case ECGHeartBeatCount = 136 // name:Total heart beat count for a measurement time during a ECG session note:Total heart beat count for a measurement time during a ECG session 
    case ECGHeartRate = 128 // name:The measure of the number of heart beats per minute during a ECG session note:The measure of the number of heart beats per minute during a ECG session 
    case ECGIndexHRZone = 133 // name:ECG Index Heart Rate Zone note:ECG Index Heart Rate Zone 
    case ECGLowerHRZone = 134 // name:ECG Lower Heart Rate Zone note:ECG Lower Heart Rate Zone 
    case ECGMaximumHeartRate = 131 // name:Maximum measure of the number of heart beats per minute during a ECG session note:Maximum measure of the number of heart beats per minute during a ECG session 
    case ECGMeanHeartRate = 137 // name:Mean value of heart rate during a ECG session note:Mean value of heart rate during a ECG session 
    case ECGMinimumHeartRate = 130 // name:Minimum measure of the number of heart beats per minute during a ECG session note:Minimum measure of the number of heart beats per minute during a ECG session 
    case ECGRestingHeartRate = 132 // name:Number of heart beats per minute when resting (measured quantity in bpm) during a ECG session note:Number of heart beats per minute when resting (measured quantity in bpm) during a ECG session 
    case ECGSampleCount = 125 // name:ECG Sample count of heart beats note:ECG Sample count of heart beats 
    case ECGSampleFrequency = 124 // name:ECG Sampling frequency of heart beats note:ECG Sampling frequency of heart beats 
    case ECGUpperHRZone = 135 // name:ECG Upper Heart Rate Zone note:ECG Upper Heart Rate Zone 
    case ElevationGain = 91 // name:Elevation gain note:Elevation gain 
    case ElevationLoss = 90 // name:Elevation loss note:Elevation loss 
    case EndTime = 6 // name:The end date and time that the device stopped recording the data note:The end date and time that the device stopped recording the data 
    case EnergyExpenditure = 56 // name:Energy expended that can be distinctly attributed to the users fitness activity note:Energy expended that can be distinctly attributed to the users fitness activity 
    case Ferritin = 148 // name:Amount of Ferritin (measured quantity ng/mL) note:Amount of Ferritin (measured quantity ng/mL) 
    case FloorsAscended = 53 // name:Number of ascending steps taken during a workout session note:Number of ascending steps taken during a workout session 
    case FoodAmount = 20 // name:The amount of food consumed note:The amount of food consumed 
    case FoodAmountType = 21 // name:The type of serving for the food consumed note:The type of serving for the food consumed 
    case FoodAmtDescription = 22 // name:Full description of the serving amount note:Full description of the serving amount 
    case FoodCalcium = 45 // name:The amount of calcium contained in food measured in grams (micronutrient per 100g) note:The amount of calcium contained in food measured in grams (micronutrient per 100g) 
    case FoodCarbohydrates = 30 // name:The amount of carbohydrates contained in food measured in grams (micronutrient per 100g) note:The amount of carbohydrates contained in food measured in grams (micronutrient per 100g) 
    case FoodEnergy = 23 // name:Amount of energy contained in food measured in calories (cal) and joules (J) note:Amount of energy contained in food measured in calories (cal) and joules (J) 
    case FoodFat = 25 // name:Total amount of fat in food consumedmeasured in grams (macronutrient per 100g) note:Total amount of fat in food consumedmeasured in grams (macronutrient per 100g) 
    case FoodFibre = 32 // name:The amount of soluble and incoluble fibre contained in food measured in grams note:The amount of soluble and incoluble fibre contained in food measured in grams 
    case FoodHDLCholesterol = 44 // name:The amount of high-density lipoprotein contained in food measured in grams (micronutrient per 100g) note:The amount of high-density lipoprotein contained in food measured in grams (micronutrient per 100g) 
    case FoodIron = 33 // name:The amount of iron  contained in food measured in grams (micronutrient per 100g) note:The amount of iron  contained in food measured in grams (micronutrient per 100g) 
    case FoodLDLCholesterol = 43 // name:The amount of low-density lipoprotein contained in food measured in grams (micronutrient per 100g) note:The amount of low-density lipoprotein contained in food measured in grams (micronutrient per 100g) 
    case FoodMagnesium = 49 // name:The amount of magnesium contained in food measured in grams (micronutrient per 100g) note:The amount of magnesium contained in food measured in grams (micronutrient per 100g) 
    case FoodPotassium = 46 // name:The amount of potassium contained in food measured in grams (micronutrient per 100g) note:The amount of potassium contained in food measured in grams (micronutrient per 100g) 
    case FoodProtein = 31 // name:The amount of protein contained in food measured in grams (micronutrient per 100g) note:The amount of protein contained in food measured in grams (micronutrient per 100g) 
    case FoodSodium = 34 // name:The amount of sodium  contained in food measured in grams (micronutrient per 100g) note:The amount of sodium  contained in food measured in grams (micronutrient per 100g) 
    case FoodSugar = 41 // name:The amount of sugar contained in food measured in grams (micronutrient per 100g) note:The amount of sugar contained in food measured in grams (micronutrient per 100g) 
    case FoodTotalCholesterol = 42 // name:The amount of total cholesterol contained in food measured in grams (micronutrient per 100g) note:The amount of total cholesterol contained in food measured in grams (micronutrient per 100g) 
    case FoodVitaminA = 35 // name:The amount of vitamin A contained in food consumed  note:The amount of vitamin A contained in food consumed  
    case FoodVitaminB = 36 // name:The amount of vitamin B contained in food consumed  note:The amount of vitamin B contained in food consumed  
    case FoodVitaminC = 37 // name:The amount of vitamin C contained in food consumed  represented as a percentage of the daily value note:The amount of vitamin C contained in food consumed  represented as a percentage of the daily value 
    case FoodVitaminD = 38 // name:The amount of vitamin D contained in food consumed  note:The amount of vitamin D contained in food consumed  
    case FoodVitaminE = 39 // name:The amount of vitamin E contained in food consumed  note:The amount of vitamin E contained in food consumed  
    case FoodVitaminK = 40 // name:The amount of vitamin K contained in food consumed  note:The amount of vitamin K contained in food consumed  
    case FoodWater = 47 // name:The amount of water consumed measured in millilitres (micronutrient per 100g) note:The amount of water consumed measured in millilitres (micronutrient per 100g) 
    case GoalEndDate = 180 // name:Goal End Date note:Goal End Date 
    case GoalMonitorUntil = 182 // name:Goal Monitor Until note:Goal Monitor Until 
    case GoalStartDate = 179 // name:Goal Start Date note:Goal Start Date 
    case GoalTrackerId = 175 // name:The unique identifier for the goal tracker related to a specific party note:The unique identifier for the goal tracker related to a specific party 
    case HDLCholesterol = 105 // name:The amount of high-density lipoprotein contained in food measured in grams (micronutrient per 100g) note:The amount of high-density lipoprotein contained in food measured in grams (micronutrient per 100g) 
    case HSCRP = 149 // name:Amount of High-Sensitivity C-reactive Protein (measured quantity in mg/dL) note:Amount of High-Sensitivity C-reactive Protein (measured quantity in mg/dL) 
    case HbA1c = 121 // name:Average plasma glucose concentration over prolonged periods of time note:Average plasma glucose concentration over prolonged periods of time 
    case HealthyItemSpend = 195 // name:Healthy Item Spend note:The total Healthy Item spend for a purchase 
    case HeartBeatCount = 73 // name:Total heart beat count for a measurement time note:Total heart beat count for a measurement time 
    case HeartRate = 65 // name:The measure of the number of heart beats per minute during a workout session  note:The measure of the number of heart beats per minute during a workout session  
    case HoursSlept = 10 // name:The number of hours slept note:The number of hours slept 
    case IL6 = 150 // name:Amount of Interleukin 6 (measured quantity in pg/mL) note:Amount of Interleukin 6 (measured quantity in pg/mL) 
    case IndexHeartRateZone = 70 // name:Index Heart Rate Zone note:Index Heart Rate Zone 
    case InstructionTypeKey = 197 // name:Instruction Type Key note:Key of the Instruction Type 
    case Insulin = 120 // name:Amount of insulin (measured in lu/mg) note:Amount of insulin (measured in lu/mg) 
    case Integrity = 7 // name:The identification of the validity of the device data  note:The identification of the validity of the device data  
    case Intensity = 54 // name:Rate at which the activity is being performed note:Rate at which the activity is being performed 
    case LDLCholesterol = 104 // name:The amount of low-density lipoprotein contained in food measured in grams (micronutrient per 100g) note:The amount of low-density lipoprotein contained in food measured in grams (micronutrient per 100g) 
    case LightSleep = 13 // name:Amount of time in light sleep (Hours) note:Amount of time in light sleep (Hours) 
    case LocationAccuracy = 18 // name:Accuracy value of the location note:Accuracy value of the location 
    case LocationAltitude = 17 // name:Altitude value of the place note:Altitude value of the place 
    case LocationLatitude = 15 // name:Latitude value of the location note:Latitude value of the location 
    case LocationLongitude = 16 // name:Longitude value of the location note:Longitude value of the location 
    case LowerHeartRateZone = 71 // name:Lower Heart Rate Zone note:Lower Heart Rate Zone 
    case Manufacturer = 3 // name:The manfucaturer of the device used to upload the data note:The manfucaturer of the device used to upload the data 
    case MaxCadenceCycling = 80 // name:Maximum measure of the pedaling rate (in revolutions per minute [rpm]) when cycling note:Maximum measure of the pedaling rate (in revolutions per minute [rpm]) when cycling 
    case MaxStrokeSwimming = 83 // name:Maximum stroke value at which the user is swimming note:Maximum stroke value at which the user is swimming 
    case MaximumAltitude = 62 // name:Maximum height reached during a workout session in relation to sea level or ground level note:Maximum height reached during a workout session in relation to sea level or ground level 
    case MaximumHeartRate = 68 // name:Maximum measure of the number of heart beats per minute during a workout session note:Maximum measure of the number of heart beats per minute during a workout session 
    case MaximumPace = 89 // name:Maximum measure of minutes per kilometer during a workout note:Maximum measure of minutes per kilometer during a workout 
    case MaximumPower = 86 // name:Maximum measure of power  note:Maximum measure of power  
    case MaximumSpeed = 60 // name:Maximum measure of speed during a workout session  note:Maximum measure of speed during a workout session  
    case MaximumStrideRate = 77 // name:Maximum number of steps taken per minute during a workout session note:Maximum number of steps taken per minute during a workout session 
    case MealTime = 115 // name:The time that the meal was consumed note:The time that the meal was consumed 
    case MealType = 116 // name:Context information for meal E.G. Meal_Type_Before_Breakfast note:Context information for meal E.G. Meal_Type_Before_Breakfast 
    case MeanBloodPressure = 101 // name:Mean blood pressure value note:Mean blood pressure value 
    case MeanHeartRate = 74 // name:Mean value of heart rate note:Mean value of heart rate 
    case MinCadenceCycling = 79 // name:Minimum measure of the pedaling rate (in revolutions per minute [rpm]) when cycling note:Minimum measure of the pedaling rate (in revolutions per minute [rpm]) when cycling 
    case MinStrokeSwimming = 82 // name:Minimum stroke value at which the user is swimming note:Minimum stroke value at which the user is swimming 
    case MinimumAltitude = 63 // name:Minimum height reached during a workout session in relation to sea level or ground level note:Minimum height reached during a workout session in relation to sea level or ground level 
    case MinimumHeartRate = 67 // name:Minimum measure of the number of heart beats per minute during a workout session note:Minimum measure of the number of heart beats per minute during a workout session 
    case MinimumPace = 88 // name:Minimum measure of minutes per kilometer during a workout note:Minimum measure of minutes per kilometer during a workout 
    case MinimumPower = 85 // name:Minimum measure of power  note:Minimum measure of power  
    case MinimumSpeed = 59 // name:Minimum measure of speed during a workout session  note:Minimum measure of speed during a workout session  
    case MinimumStrideRate = 76 // name:Minimum number of steps taken per minute during a workout session note:Minimum number of steps taken per minute during a workout session 
    case Model = 4 // name:The model of the device used to upload the data note:The model of the device used to upload the data 
    case MonounsaturatedFat = 28 // name:Total amount of monounsaturated fat in food consumed measured in grams (micronutrients per 100g) note:Total amount of monounsaturated fat in food consumed measured in grams (micronutrients per 100g) 
    case NumberOfDrinks = 48 // name:The number as an integer representing the number of drinks had during the serving note:The number as an integer representing the number of drinks had during the serving 
    case PartnerBranch = 177 // name:Partner Branch note:Partner branch where the member did the gym workout 
    case PartyRole = 170 // name:The role that a party plays on a Vitality Membership over time note:The role that a party plays on a Vitality Membership over time 
    case PeakFlowRate = 162 // name:Maximum speed of expiration note:Maximum speed of expiration 
    case Points = 174 // name:Points given to the event manually note:Points given to the event manually 
    case PolyunsaturatedFat = 26 // name:Total amount of polyunsaturated fat in food consumed measured in grams (macronutrient per 100g) note:Total amount of polyunsaturated fat in food consumed measured in grams (macronutrient per 100g) 
    case Product = 173 // name:Product associated with the Vitality membership. note:Product associated with the Vitality membership. 
    case PulseRate = 102 // name:Blood pressure pulse rate note:Blood pressure pulse rate 
    case PurchaseId = 193 // name:Purchase Id note:The purchase Id for purchase 
    case PurchaseProduct = 194 // name:Purchase Product note:The purchase product for a purchase 
    case ReadingType = 192 // name:The reading type for the device data upload note:The reading type for the device data upload 
    case RelieverPuffs = 163 // name:Number of puffs taken by user using device note:Number of puffs taken by user using device 
    case Rem = 12 // name:Rapid Eye Movement recorded during sleep (measured in seconds) note:Rapid Eye Movement recorded during sleep (measured in seconds) 
    case Remark = 165 // name:This is a comment that can be added in VSP to a member's profile. note:This is a comment that can be added in VSP to a member's profile. 
    case RenewalPeriodFrom = 171 // name:The effective from date for the Vitality Membership renewal period note:The effective from date for the Vitality Membership renewal period 
    case RenewalPeriodTo = 172 // name:The 'effective to' date for the Vitality Membership renewal period note:The 'effective to' date for the Vitality Membership renewal period 
    case RestingEnergyExpend = 95 // name:Amount of energy expended while minimally active (user's basal metabolic rate) note:Amount of energy expended while minimally active (user's basal metabolic rate) 
    case RestingHeartRate = 69 // name:Number of heart beats per minute when resting (measured quantity in bpm) note:Number of heart beats per minute when resting (measured quantity in bpm) 
    case RewardKey = 190 // name:Reward Key note:Reward Key 
    case RewardName = 189 // name:Reward Name note:Reward Name 
    case RewardPoints = 191 // name:Reward Points note:Reward Points 
    case SaturatedFat = 27 // name:Total amount of saturated fat in food consumed measured in grams (micronutrient per 100g) note:Total amount of saturated fat in food consumed measured in grams (micronutrient per 100g) 
    case SleepDisruptions = 8 // name:The number of disruptions during the sleep period note:The number of disruptions during the sleep period 
    case SleepEfficiency = 11 // name:Ratio of time spent asleep (total sleep time) to the amount of time spent laying in bed note:Ratio of time spent asleep (total sleep time) to the amount of time spent laying in bed 
    case StartAltitude = 61 // name:Height recorded in relation to sea level or ground level at the start of the workout session note:Height recorded in relation to sea level or ground level at the start of the workout session 
    case StartTime = 5 // name:The start date and time that the device started recording the data note:The start date and time that the device started recording the data 
    case SystolicBP = 98 // name:Maximum blood pressure recorded note:Maximum blood pressure recorded 
    case TSH = 152 // name:Amount of Thyroid-Stimulating-Hormone (measured in mIU/L) note:Amount of Thyroid-Stimulating-Hormone (measured in mIU/L) 
    case Testosterone = 151 // name:Testosterone value (measured quantity in ng/d) note:Testosterone value (measured quantity in ng/d) 
    case TotEnergyExpenditure = 92 // name:Total amount of energy (active and resting) expended note:Total amount of energy (active and resting) expended 
    case TotalAscendAltitude = 64 // name:Total altitude ascended during a workout session in relation to sea level or ground level note:Total altitude ascended during a workout session in relation to sea level or ground level 
    case TotalCholesterol = 103 // name:The amount of total cholesterol contained in food measured in grams (micronutrient per 100g) note:The amount of total cholesterol contained in food measured in grams (micronutrient per 100g) 
    case TotalSteps = 52 // name:Number of steps taken during a workout session or during the course of the day note:Number of steps taken during a workout session or during the course of the day 
    case TwilightPeriod = 9 // name:Twilight period note:Twilight period 
    case UnhealthyItemSpend = 196 // name:Unhealthy Item Spend note:The total Unhealthy Item spend for a purchase 
    case UnsaturatedFat = 29 // name:Total amount of unsaturated fat in food consumed measured in grams (micronutrient per 100g) note:Total amount of unsaturated fat in food consumed measured in grams (micronutrient per 100g) 
    case UpperHeartRateZone = 72 // name:Upper Heart Rate Zone note:Upper Heart Rate Zone 
    case UricAcid = 161 // name:Amount of Uric Acid (measured quantity in mg/dL) note:Amount of Uric Acid (measured quantity in mg/dL) 
    case Value = 166 // name:The value associated to the event note:The value associated to the event 
    case Verified = 2 // name:The verification of the device data note:The verification of the device data 
    case VitalityStatus = 164 // name:The vitality status for the member (i.e. Bronze, Silver, Gold) note:The vitality status for the member (i.e. Bronze, Silver, Gold) 
    case WhiteCellCount = 154 // name:Amount of white cells in the blood (measured quantity in cells/?L) note:Amount of white cells in the blood (measured quantity in cells/?L) 

    public static let allValues: [EventMetaDataTypeRef] = [EventMetaDataTypeRef.Unknown, EventMetaDataTypeRef.ActiveEnergyExpend, EventMetaDataTypeRef.AgreementID, EventMetaDataTypeRef.AgreementType, EventMetaDataTypeRef.AmbTmpHumidity, EventMetaDataTypeRef.AmbTmpLocatAccuracy, EventMetaDataTypeRef.AmbTmpLocatAltitude, EventMetaDataTypeRef.AmbTmpLocatLatitude, EventMetaDataTypeRef.AmbTmpLocatLongitude, EventMetaDataTypeRef.AverageHeartRate, EventMetaDataTypeRef.AveragePace, EventMetaDataTypeRef.AveragePower, EventMetaDataTypeRef.AverageSpeed, EventMetaDataTypeRef.AverageStrideRate, EventMetaDataTypeRef.AvgActiveEnergyExpen, EventMetaDataTypeRef.AvgCadenceCycling, EventMetaDataTypeRef.AvgEnergyExpenditure, EventMetaDataTypeRef.AvgFoodEnergyRate, EventMetaDataTypeRef.AvgRestEnergyExpend, EventMetaDataTypeRef.AvgStrokeSwimming, EventMetaDataTypeRef.AwardedRewardId, EventMetaDataTypeRef.BMI, EventMetaDataTypeRef.BPLBloodPressure, EventMetaDataTypeRef.BasalMetabolicRate, EventMetaDataTypeRef.BloodCalcium, EventMetaDataTypeRef.BloodChromium, EventMetaDataTypeRef.BloodFolicAcid, EventMetaDataTypeRef.BloodGlucMeasureType, EventMetaDataTypeRef.BloodGlucSampleSrc, EventMetaDataTypeRef.BloodGlucose, EventMetaDataTypeRef.BloodMagnesium, EventMetaDataTypeRef.BloodOxygen, EventMetaDataTypeRef.BloodOxygenSpo2, EventMetaDataTypeRef.BloodPotassium, EventMetaDataTypeRef.BloodSodium, EventMetaDataTypeRef.BloodVitaminB12, EventMetaDataTypeRef.BloodVitaminD, EventMetaDataTypeRef.BloodZinc, EventMetaDataTypeRef.BodyFatValue, EventMetaDataTypeRef.BodyHeight, EventMetaDataTypeRef.BodyMuscleValue, EventMetaDataTypeRef.BodyTemperature, EventMetaDataTypeRef.BodyWaterValue, EventMetaDataTypeRef.BodyWeight, EventMetaDataTypeRef.BoneValue, EventMetaDataTypeRef.BrandName, EventMetaDataTypeRef.CRP, EventMetaDataTypeRef.CreatineKinase, EventMetaDataTypeRef.DCI, EventMetaDataTypeRef.DeepSleep, EventMetaDataTypeRef.Description, EventMetaDataTypeRef.DeviceDataCategory, EventMetaDataTypeRef.DiastolicBP, EventMetaDataTypeRef.DiscMaraRun, EventMetaDataTypeRef.Distance, EventMetaDataTypeRef.DocumentReference, EventMetaDataTypeRef.DrugTime, EventMetaDataTypeRef.Duration, EventMetaDataTypeRef.ECGAverageHeartRate, EventMetaDataTypeRef.ECGData, EventMetaDataTypeRef.ECGDataFormat, EventMetaDataTypeRef.ECGHeartBeatCount, EventMetaDataTypeRef.ECGHeartRate, EventMetaDataTypeRef.ECGIndexHRZone, EventMetaDataTypeRef.ECGLowerHRZone, EventMetaDataTypeRef.ECGMaximumHeartRate, EventMetaDataTypeRef.ECGMeanHeartRate, EventMetaDataTypeRef.ECGMinimumHeartRate, EventMetaDataTypeRef.ECGRestingHeartRate, EventMetaDataTypeRef.ECGSampleCount, EventMetaDataTypeRef.ECGSampleFrequency, EventMetaDataTypeRef.ECGUpperHRZone, EventMetaDataTypeRef.ElevationGain, EventMetaDataTypeRef.ElevationLoss, EventMetaDataTypeRef.EndTime, EventMetaDataTypeRef.EnergyExpenditure, EventMetaDataTypeRef.Ferritin, EventMetaDataTypeRef.FloorsAscended, EventMetaDataTypeRef.FoodAmount, EventMetaDataTypeRef.FoodAmountType, EventMetaDataTypeRef.FoodAmtDescription, EventMetaDataTypeRef.FoodCalcium, EventMetaDataTypeRef.FoodCarbohydrates, EventMetaDataTypeRef.FoodEnergy, EventMetaDataTypeRef.FoodFat, EventMetaDataTypeRef.FoodFibre, EventMetaDataTypeRef.FoodHDLCholesterol, EventMetaDataTypeRef.FoodIron, EventMetaDataTypeRef.FoodLDLCholesterol, EventMetaDataTypeRef.FoodMagnesium, EventMetaDataTypeRef.FoodPotassium, EventMetaDataTypeRef.FoodProtein, EventMetaDataTypeRef.FoodSodium, EventMetaDataTypeRef.FoodSugar, EventMetaDataTypeRef.FoodTotalCholesterol, EventMetaDataTypeRef.FoodVitaminA, EventMetaDataTypeRef.FoodVitaminB, EventMetaDataTypeRef.FoodVitaminC, EventMetaDataTypeRef.FoodVitaminD, EventMetaDataTypeRef.FoodVitaminE, EventMetaDataTypeRef.FoodVitaminK, EventMetaDataTypeRef.FoodWater, EventMetaDataTypeRef.GoalEndDate, EventMetaDataTypeRef.GoalMonitorUntil, EventMetaDataTypeRef.GoalStartDate, EventMetaDataTypeRef.GoalTrackerId, EventMetaDataTypeRef.HDLCholesterol, EventMetaDataTypeRef.HSCRP, EventMetaDataTypeRef.HbA1c, EventMetaDataTypeRef.HealthyItemSpend, EventMetaDataTypeRef.HeartBeatCount, EventMetaDataTypeRef.HeartRate, EventMetaDataTypeRef.HoursSlept, EventMetaDataTypeRef.IL6, EventMetaDataTypeRef.IndexHeartRateZone, EventMetaDataTypeRef.InstructionTypeKey, EventMetaDataTypeRef.Insulin, EventMetaDataTypeRef.Integrity, EventMetaDataTypeRef.Intensity, EventMetaDataTypeRef.LDLCholesterol, EventMetaDataTypeRef.LightSleep, EventMetaDataTypeRef.LocationAccuracy, EventMetaDataTypeRef.LocationAltitude, EventMetaDataTypeRef.LocationLatitude, EventMetaDataTypeRef.LocationLongitude, EventMetaDataTypeRef.LowerHeartRateZone, EventMetaDataTypeRef.Manufacturer, EventMetaDataTypeRef.MaxCadenceCycling, EventMetaDataTypeRef.MaxStrokeSwimming, EventMetaDataTypeRef.MaximumAltitude, EventMetaDataTypeRef.MaximumHeartRate, EventMetaDataTypeRef.MaximumPace, EventMetaDataTypeRef.MaximumPower, EventMetaDataTypeRef.MaximumSpeed, EventMetaDataTypeRef.MaximumStrideRate, EventMetaDataTypeRef.MealTime, EventMetaDataTypeRef.MealType, EventMetaDataTypeRef.MeanBloodPressure, EventMetaDataTypeRef.MeanHeartRate, EventMetaDataTypeRef.MinCadenceCycling, EventMetaDataTypeRef.MinStrokeSwimming, EventMetaDataTypeRef.MinimumAltitude, EventMetaDataTypeRef.MinimumHeartRate, EventMetaDataTypeRef.MinimumPace, EventMetaDataTypeRef.MinimumPower, EventMetaDataTypeRef.MinimumSpeed, EventMetaDataTypeRef.MinimumStrideRate, EventMetaDataTypeRef.Model, EventMetaDataTypeRef.MonounsaturatedFat, EventMetaDataTypeRef.NumberOfDrinks, EventMetaDataTypeRef.PartnerBranch, EventMetaDataTypeRef.PartyRole, EventMetaDataTypeRef.PeakFlowRate, EventMetaDataTypeRef.Points, EventMetaDataTypeRef.PolyunsaturatedFat, EventMetaDataTypeRef.Product, EventMetaDataTypeRef.PulseRate, EventMetaDataTypeRef.PurchaseId, EventMetaDataTypeRef.PurchaseProduct, EventMetaDataTypeRef.ReadingType, EventMetaDataTypeRef.RelieverPuffs, EventMetaDataTypeRef.Rem, EventMetaDataTypeRef.Remark, EventMetaDataTypeRef.RenewalPeriodFrom, EventMetaDataTypeRef.RenewalPeriodTo, EventMetaDataTypeRef.RestingEnergyExpend, EventMetaDataTypeRef.RestingHeartRate, EventMetaDataTypeRef.RewardKey, EventMetaDataTypeRef.RewardName, EventMetaDataTypeRef.RewardPoints, EventMetaDataTypeRef.SaturatedFat, EventMetaDataTypeRef.SleepDisruptions, EventMetaDataTypeRef.SleepEfficiency, EventMetaDataTypeRef.StartAltitude, EventMetaDataTypeRef.StartTime, EventMetaDataTypeRef.SystolicBP, EventMetaDataTypeRef.TSH, EventMetaDataTypeRef.Testosterone, EventMetaDataTypeRef.TotEnergyExpenditure, EventMetaDataTypeRef.TotalAscendAltitude, EventMetaDataTypeRef.TotalCholesterol, EventMetaDataTypeRef.TotalSteps, EventMetaDataTypeRef.TwilightPeriod, EventMetaDataTypeRef.UnhealthyItemSpend, EventMetaDataTypeRef.UnsaturatedFat, EventMetaDataTypeRef.UpperHeartRateZone, EventMetaDataTypeRef.UricAcid, EventMetaDataTypeRef.Value, EventMetaDataTypeRef.Verified, EventMetaDataTypeRef.VitalityStatus, EventMetaDataTypeRef.WhiteCellCount]

}
