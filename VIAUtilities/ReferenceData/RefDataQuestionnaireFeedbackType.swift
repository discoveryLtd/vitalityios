//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum QuestionnaireFeedbackTypeRef: Int, Hashable { 
    case Unknown = -1
    case Conclusion = 2 // name:Conclusion note:Conclusion 
    case Information = 4 // name:Information note:Information 
    case Observation = 1 // name:Observation note:Observation 
    case Recommendation = 3 // name:Recommendation note:Recommendation 

    public static let allValues: [QuestionnaireFeedbackTypeRef] = [QuestionnaireFeedbackTypeRef.Unknown, QuestionnaireFeedbackTypeRef.Conclusion, QuestionnaireFeedbackTypeRef.Information, QuestionnaireFeedbackTypeRef.Observation, QuestionnaireFeedbackTypeRef.Recommendation]

}
