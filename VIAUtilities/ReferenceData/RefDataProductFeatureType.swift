//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum ProductFeatureTypeRef: Int, Hashable { 
    case Unknown = -1
    case ActiveRewards = 29 // name:ActiveRewards note:ActiveRewards 
    case ActiveRewardsPartner = 38 // name:Active Rewards Partner note:Active Rewards Partner 
    case ActivityTrack = 6 // name:Activity Tracking Devices and App note:Activity Tracking Devices and App 
    case Assessment = 2 // name:Assessment note:Assessment 
    case Benefit = 4 // name:Benefit note:Vitality Active Benefit 
    case DSConsent = 1 // name:Data Sharing Consent note:Data Sharing Consent 
    case Declaration = 33 // name:Declaration note:Declaration 
    case Disclaimer = 21 // name:Disclaimer note:Disclaimer 
    case EventsCategory = 20 // name:Events Category note:Events Category Feature Type 
    case Fitness = 31 // name:Fitness note:Fitness 
    case GymBenefit = 24 // name:Gym Benefit note:Gym Benefit 
    case Habits = 17 // name:Habits note:Habits Declaration 
    case HealthCheck = 3 // name:Health Check Tests note:Health Check Tests 
    case HealthyFoodBenefit = 26 // name:Healthy Food Benefit note:Healthy Food Benefit 
    case KnowYourHealth = 32 // name:Know Your Health note:Know Your Health 
    case MWBSections = 28 // name:MWB Sections note:Mental Wellbeing Assessment (MWB) Section Questionnaires 
    case PartnerAssessment = 27 // name:Partner Assessment note:Partner Assessment 
    case PointsCategory = 19 // name:Points Category note:Points Category Feature Type 
    case Programme = 5 // name:Programme note:Vitality Active Programme 
    case RetailBenefit
 = 25 // name:Retail Benefit note:Retail Benefit 
    case ScreenAndVaccs = 14 // name:Screenings and Vaccinations (S&V) note:Screenings and Vaccinations (S&V) Category 
    case ScreenAndVaccsItems = 15 // name:Screenings and Vaccinations Items note:Screenings and Vaccinations Items 
    case Screenings = 34 // name:Screenings note:Screenings 
    case StopSmokingBenefit = 36 // name:Stop Smoking Benefit note:Stop Smoking Benefit 
    case TravelBenefit = 30 // name:Travel Benefit note:Travel Benefit 
    case VHCBMI = 7 // name:BMI note:Body Mass Index (BMI) Readings 
    case VHCBloodGlucose = 9 // name:Blood Glucose note:Blood Glucose Readings 
    case VHCBloodPressure = 10 // name:Blood Pressure note:Blood Pressure Readings 
    case VHCCholesterol = 11 // name:Cholesterol note:Cholesterol Readings 
    case VHCLipidProfile = 39 //// name:Lipid Profile note:
    case VHCHbA1c = 12 // name:HbA1c note:HbA1c Readings 
    case VHCTemplate = 23 // name:VHCTemplate note:VHC Template for download 
    case VHCUrinaryProtein = 18 // name:Urinary Protein Test note:Urinary Protein Test Readings 
    case VHCWaistCircum = 8 // name:Waist Circumference note:Waist Circumference Readings 
    case VHRSections = 13 // name:VHR Sections note:Vitality Health Review (VHR) Section Questionnaires 
    case VNASections = 16 // name:VNA Sections note:Vitality Nutrition Assessment (VNA) Sections Questionnaires 
    case Vaccinations = 35 // name:Vaccinations note:Vaccinations 
    case VitalityStatus = 22 // name:Vitality Statuses note:The vitality statuses for the product 
    case WeightManageBenefit = 37 // name:Weight Management Benefit note:Weight Management Benefit 

    public static let allValues: [ProductFeatureTypeRef] = [ProductFeatureTypeRef.Unknown, ProductFeatureTypeRef.ActiveRewards, ProductFeatureTypeRef.ActiveRewardsPartner, ProductFeatureTypeRef.ActivityTrack, ProductFeatureTypeRef.Assessment, ProductFeatureTypeRef.Benefit, ProductFeatureTypeRef.DSConsent, ProductFeatureTypeRef.Declaration, ProductFeatureTypeRef.Disclaimer, ProductFeatureTypeRef.EventsCategory, ProductFeatureTypeRef.Fitness, ProductFeatureTypeRef.GymBenefit, ProductFeatureTypeRef.Habits, ProductFeatureTypeRef.HealthCheck, ProductFeatureTypeRef.HealthyFoodBenefit, ProductFeatureTypeRef.KnowYourHealth, ProductFeatureTypeRef.MWBSections, ProductFeatureTypeRef.PartnerAssessment, ProductFeatureTypeRef.PointsCategory, ProductFeatureTypeRef.Programme, ProductFeatureTypeRef.RetailBenefit
, ProductFeatureTypeRef.ScreenAndVaccs, ProductFeatureTypeRef.ScreenAndVaccsItems, ProductFeatureTypeRef.Screenings, ProductFeatureTypeRef.StopSmokingBenefit, ProductFeatureTypeRef.TravelBenefit, ProductFeatureTypeRef.VHCBMI, ProductFeatureTypeRef.VHCBloodGlucose, ProductFeatureTypeRef.VHCBloodPressure, ProductFeatureTypeRef.VHCCholesterol, ProductFeatureTypeRef.VHCLipidProfile, ProductFeatureTypeRef.VHCHbA1c, ProductFeatureTypeRef.VHCTemplate, ProductFeatureTypeRef.VHCUrinaryProtein, ProductFeatureTypeRef.VHCWaistCircum, ProductFeatureTypeRef.VHRSections, ProductFeatureTypeRef.VNASections, ProductFeatureTypeRef.Vaccinations, ProductFeatureTypeRef.VitalityStatus, ProductFeatureTypeRef.WeightManageBenefit]

}
