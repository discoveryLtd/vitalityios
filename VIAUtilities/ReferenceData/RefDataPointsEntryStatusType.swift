//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum PointsEntryStatusTypeRef: Int, Hashable { 
    case Unknown = -1
    case CALCULATED_STATUS = 2 // name:Calculated Points note:undefined 
    case CREATED_STATUS = 1 // name:Calculated Points note:undefined 
    case REVERSED_STATUS = 3 // name:Reversed Points note:undefined 

    public static let allValues: [PointsEntryStatusTypeRef] = [PointsEntryStatusTypeRef.Unknown, PointsEntryStatusTypeRef.CALCULATED_STATUS, PointsEntryStatusTypeRef.CREATED_STATUS, PointsEntryStatusTypeRef.REVERSED_STATUS]

}
