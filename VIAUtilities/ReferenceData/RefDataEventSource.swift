//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum EventSourceRef: Int, Hashable { 
    case Unknown = -1
    case Benefit = 6 // name:Benefits system note:Benefits system 
    case Enrollment = 11 // name:Enrollment service note:Enrollment service 
    case Finance = 5 // name:Finance system note:Finance system 
    case Goals = 2 // name:Goals system note:Goals system 
    case MobileApp = 1 // name:Mobile App note:Mobile App 
    case NuffieldHealthSystem = 14 // name:Nuffield Health System note:Nuffield Health System 
    case Partner = 13 // name:Partner note:Partner 
    case Party = 9 // name:Party system note:Party system 
    case Purchase = 4 // name:Purchase system note:Purchase system 
    case Rewards = 3 // name:Rewards system note:Rewards system 
    case VDP = 8 // name:Vitality Device Platform note:Vitality Device Platform 
    case VMP = 12 // name:Vitality Membership Portal note:Vitality Membership Portal 
    case VSP = 10 // name:Vitality Service Portal note:Vitality Service Portal 
    case VitalityMembership = 7 // name:Vitaltiy Membership system note:Vitaltiy Membership system 

    public static let allValues: [EventSourceRef] = [EventSourceRef.Unknown, EventSourceRef.Benefit, EventSourceRef.Enrollment, EventSourceRef.Finance, EventSourceRef.Goals, EventSourceRef.MobileApp, EventSourceRef.NuffieldHealthSystem, EventSourceRef.Partner, EventSourceRef.Party, EventSourceRef.Purchase, EventSourceRef.Rewards, EventSourceRef.VDP, EventSourceRef.VMP, EventSourceRef.VSP, EventSourceRef.VitalityMembership]

}
