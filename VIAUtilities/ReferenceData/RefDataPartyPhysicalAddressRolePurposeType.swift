//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum PartyPhysicalAddressRolePurposeTypeRef: Int, Hashable { 
    case Unknown = -1
    case BillingAddress = 1 // name:Billing Address note:Billing Address 
    case DeliveryAddress = 3 // name:Delivery Address note:Delivery Address 
    case MarketingAddress = 2 // name:Marketing Address note:Marketing Address 
    case MarketingInfo = 5 // name:Marketing Information note:Marketing Information 
    case ProductDelivery = 4 // name:Product Delivery note:Product Delivery 

    public static let allValues: [PartyPhysicalAddressRolePurposeTypeRef] = [PartyPhysicalAddressRolePurposeTypeRef.Unknown, PartyPhysicalAddressRolePurposeTypeRef.BillingAddress, PartyPhysicalAddressRolePurposeTypeRef.DeliveryAddress, PartyPhysicalAddressRolePurposeTypeRef.MarketingAddress, PartyPhysicalAddressRolePurposeTypeRef.MarketingInfo, PartyPhysicalAddressRolePurposeTypeRef.ProductDelivery]

}
