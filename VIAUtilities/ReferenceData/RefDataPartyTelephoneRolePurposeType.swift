//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum PartyTelephoneRolePurposeTypeRef: Int, Hashable { 
    case Unknown = -1
    case BillingInquiries = 1 // name:Billing Inquiries note:Billing Inquiries 
    case SalesCalls = 3 // name:Sales Calls note:Sales Calls 
    case ServiceCalls = 2 // name:Service Calls note:Service Calls 
    case TechnicalSupport = 4 // name:Technical Support Calls note:Technical Support Calls 
    case VitalityNotification = 5 // name:Vitality Notifications note:Vitality Notifications 

    public static let allValues: [PartyTelephoneRolePurposeTypeRef] = [PartyTelephoneRolePurposeTypeRef.Unknown, PartyTelephoneRolePurposeTypeRef.BillingInquiries, PartyTelephoneRolePurposeTypeRef.SalesCalls, PartyTelephoneRolePurposeTypeRef.ServiceCalls, PartyTelephoneRolePurposeTypeRef.TechnicalSupport, PartyTelephoneRolePurposeTypeRef.VitalityNotification]

}
