//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum RewardOptionRef: Int, Hashable { 
    case Unknown = -1
    case Choice = 2 // name:Choice note:The reward is a choice that the member may select 
    case Predetermined = 1 // name:Predetermined note:The reward is predetermined 
    case Probabilistic = 3 // name:Probabilistic note:The reward is a probabilistic reward where the reward is assigned to the member based on some probability. 

    public static let allValues: [RewardOptionRef] = [RewardOptionRef.Unknown, RewardOptionRef.Choice, RewardOptionRef.Predetermined, RewardOptionRef.Probabilistic]

}
