//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum RewardCategoryRef: Int, Hashable { 
    case Unknown = -1
    case Entertainment = 1 // name:Entertainment note:null 
    case FoodAndBeverage = 2 // name:Food and beverages note:null 
    case Gamification = 5 // name:Gamification note:The opportunity to play a game where the outcome may result in some form of reward 
    case Retail = 3 // name:Retail note:null 
    case RewardsProgram = 4 // name:Rewards Program note:null 

    public static let allValues: [RewardCategoryRef] = [RewardCategoryRef.Unknown, RewardCategoryRef.Entertainment, RewardCategoryRef.FoodAndBeverage, RewardCategoryRef.Gamification, RewardCategoryRef.Retail, RewardCategoryRef.RewardsProgram]

}
