//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum ReasonCategoryRef: Int, Hashable { 
    case Unknown = -1
    case CategoryLimit = 4 // name:Points Entry Category Limit note:undefined 
    case Conditions = 1 // name:Conditions note:undefined 
    case TypeLimit = 3 // name:Points Entry Type Limit note:undefined 
    case Validations = 2 // name:Validations note:undefined 

    public static let allValues: [ReasonCategoryRef] = [ReasonCategoryRef.Unknown, ReasonCategoryRef.CategoryLimit, ReasonCategoryRef.Conditions, ReasonCategoryRef.TypeLimit, ReasonCategoryRef.Validations]

}
