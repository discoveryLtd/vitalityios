//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum OutboundFinancialInstructionTypeRef: Int, Hashable { 
    case Unknown = -1
    case Cashback = 1 // name:Cash back note:Instruction for the carrier/partner to pay a cashback to a member 

    public static let allValues: [OutboundFinancialInstructionTypeRef] = [OutboundFinancialInstructionTypeRef.Unknown, OutboundFinancialInstructionTypeRef.Cashback]

}
