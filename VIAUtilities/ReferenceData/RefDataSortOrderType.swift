//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum SortOrderTypeRef: Int, Hashable { 
    case Unknown = -1
    case Custom = 2 // name:Custom note:A numeric value is specified for each item and the items are sorted numerically ascending. 
    case Random = 1 // name:Random note:The values may be shown in no particular order. 

    public static let allValues: [SortOrderTypeRef] = [SortOrderTypeRef.Unknown, SortOrderTypeRef.Custom, SortOrderTypeRef.Random]

}
