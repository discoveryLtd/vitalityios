//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum EventCategoryRef: Int, Hashable { 
    case Unknown = -1
    case Other = -2
    case Agreement = 2 // name:Enrolment is the process wherein the Person and the Vitality Membership is created or updated. note:Enrolment is the process wherein the Person and the Vitality Membership is created or updated. 
    case Assessment = 6 // name:Defined as a list of questions and answers. note:Defined as a list of questions and answers. 
    case DataPrivacy = 20 // name:Consent to data privacy requirement note:Consent to data privacy requirement 
    case Device = 12 // name:Wareable device management. note:Wareable device management. 
    case Disclaimer = 28 // name:Disclaimer note:Disclaimer 
    case DocumentManagement = 16 // name:Documentation management entails the storage management+tracking of generated material note:Documentation management entails the storage management+tracking of generated material 
    case Enrollment = 21 // name:Deals with the Enrollment process note:Deals with the Enrollment process 
    case Error = 7 // name:Any errors encountered during the front end member journey. These should log an error event. note:Any errors encountered during the front end member journey. These should log an error event. 
    case Finance = 9 // name:Accounting billing and instruction methods. note:Accounting billing and instruction methods. 
    case Fitness = 27 // name:Fitness note:Fitness 
    case HealthAttribute = 18 // name:Defined as a set of measurment for a party. note:Defined as a set of measurment for a party. 
    case Integration = 13 // name:Carrier intergration requirements. note:Carrier intergration requirements. 
    case Legal = 17 // name:Defined as the law or its administration of consents. note:Defined as the law or its administration of consents. 
    case Login = 1 // name:Deals with the logging in process of going through the procedures to begin using the app/site. note:Deals with the logging in process of going through the procedures to begin using the app/site. 
    case Notification = 8 // name:Communication sent to a party. note:Communication sent to a party. 
    case Nutrition = 26 // name:Nutrition assessments or healthyfood events note:Nutrition assessments or healthyfood events 
    case Points = 11 // name:Points maintenance stores the fact that you did a points earning event and how many points you got note:Points maintenance stores the fact that you did a points earning event and how many points you got 
    case Product = 14 // name:The product component stores the details of a product that is to be marketed note:The product component stores the details of a product that is to be marketed 
    case Reward = 5 // name:Generates the reward for the member. note:Generates the reward for the member. 
    case ScreenAndVacc = 25 // name:Screenings and Vaccinations note:Screenings and Vaccinations 
    case Screenings = 23 // name:Tests to screen for a defined list of medical conditions note:Tests to screen for a defined list of medical conditions 
    case Servicing = 3 // name:Servicing entails updating and assisting the party during the member journey process. note:Servicing entails updating and assisting the party during the member journey process. 
    case Social = 10 // name:Friend family and group sharing. note:Friend family and group sharing. 
    case Status = 15 // name:Defined as the Vitality Status i.e. bronze silver gold note:Defined as the Vitality Status i.e. bronze silver gold 
    case Target = 4 // name:Defined as the mechanism of defining and managing the goals for members. note:Defined as the mechanism of defining and managing the goals for members. 
    case TermsAndConditions = 19 // name:Defined as the Terms and Conditions consents. note:Defined as the Terms and Conditions consents. 
    case VHC = 22 // name:Vitality Health Check note:Vitality Health Check 
    case Vaccinations = 24 // name:Vaccinations required as per the defined list note:Vaccinations required as per the defined list 

    public static let allValues: [EventCategoryRef] = [EventCategoryRef.Unknown, EventCategoryRef.Agreement, EventCategoryRef.Assessment, EventCategoryRef.DataPrivacy, EventCategoryRef.Device, EventCategoryRef.Disclaimer, EventCategoryRef.DocumentManagement, EventCategoryRef.Enrollment, EventCategoryRef.Error, EventCategoryRef.Finance, EventCategoryRef.Fitness, EventCategoryRef.HealthAttribute, EventCategoryRef.Integration, EventCategoryRef.Legal, EventCategoryRef.Login, EventCategoryRef.Notification, EventCategoryRef.Nutrition, EventCategoryRef.Points, EventCategoryRef.Product, EventCategoryRef.Reward, EventCategoryRef.ScreenAndVacc, EventCategoryRef.Screenings, EventCategoryRef.Servicing, EventCategoryRef.Social, EventCategoryRef.Status, EventCategoryRef.Target, EventCategoryRef.TermsAndConditions, EventCategoryRef.VHC, EventCategoryRef.Vaccinations, EventCategoryRef.Other]

}
