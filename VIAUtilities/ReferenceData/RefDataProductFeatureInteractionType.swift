//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum ProductFeatureInteractionTypeRef: Int, Hashable { 
    case Unknown = -1
    case AssociatedTo = 3 // name:AssociatedTo note:AssociatedTo 
    case ConsistsOf = 1 // name:ConsistsOf note:ConsistsOf 
    case Includes = 2 // name:Includes note:Includes 
    case LinkedTo = 4 // name:LinkedTo note:LinkedTo 

    public static let allValues: [ProductFeatureInteractionTypeRef] = [ProductFeatureInteractionTypeRef.Unknown, ProductFeatureInteractionTypeRef.AssociatedTo, ProductFeatureInteractionTypeRef.ConsistsOf, ProductFeatureInteractionTypeRef.Includes, ProductFeatureInteractionTypeRef.LinkedTo]

}
