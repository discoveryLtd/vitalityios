//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum AgreementAssociationTypeRef: Int, Hashable { 
    case Unknown = -1
    case PARTNERBENEFIT = 1 // name:Partner Benefit note:Partner Benefit 

    public static let allValues: [AgreementAssociationTypeRef] = [AgreementAssociationTypeRef.Unknown, AgreementAssociationTypeRef.PARTNERBENEFIT]

}
