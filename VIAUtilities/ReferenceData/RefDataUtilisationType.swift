//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum UtilisationTypeRef: Int, Hashable { 
    case Unknown = -1
    case AdidasHGAmt = 17 // name:Amount utilisation for AdidasHG note:Amount utilisation for AdidasHG 
    case Device = 3 // name:Shared Device Utilisation note:Shared Device Utilisation. Per Vitality Membership Period 
    case DeviceAbc = 1 // name:Device Abc Utilisation note:Utilisation for Device Abc. Per Vitality Membership Period 
    case DeviceXyz = 2 // name:Device Xyz Utilisation note:Utilisation for Device Xyz. Per Vitality Membership Period 
    case GarminDPDisc = 14 // name:Flat rate discount for Garmin devices note:Flat rate discount for Garmin devices 
    case GarminDPLim = 13 // name:Discount applicable when the limit is reached for GarminDP note:Discount applicable when the limit is reached for GarminDP 
    case GarminDPLimit = 18 // name:Count utilisation for GarminDP note:Count utilisation for GarminDP 
    case GarminPolarShrdUtil = 10 // name:Utilization shared by Garmin and Polar note:Utilization shared by Garmin and Polar 
    case GarminVivoDPDisc = 16 // name:Flat rate discount for Garmin Vivo devices note:Flat rate discount for Garmin Vivo devices 
    case GarminVivoDPLim = 15 // name:Discount applicable when the limit is reached for GarminVivoDP note:Discount applicable when the limit is reached for GarminVivoDP 
    case OisixHFAmt = 9 // name:Amount utilisation for OisixHF note:Amount utilisation for OisixHF 
    case PolarDPCnt = 8 // name:Count utilisation for PolarDP note:Count utilisation for PolarDP 
    case TestProd1 = 4 // name:Test product 1 utilisation note:Monthly Utilisation for Test Product 1 
    case TestProd2 = 5 // name:Test product 2 utilisation note:Yearly Utilisation for Test product 2 
    case TestProd3 = 6 // name:Test product 3 utilisation note:Yearly Utilisation for Test Product 3. Per Vitality Membership. For Supplier 1 
    case TestProd3S2 = 7 // name:Test product 3 - Supplier2 Utilisation note:Utilisation for Test Product 3 and supplier 2 

    public static let allValues: [UtilisationTypeRef] = [UtilisationTypeRef.Unknown, UtilisationTypeRef.AdidasHGAmt, UtilisationTypeRef.Device, UtilisationTypeRef.DeviceAbc, UtilisationTypeRef.DeviceXyz, UtilisationTypeRef.GarminDPDisc, UtilisationTypeRef.GarminDPLim, UtilisationTypeRef.GarminDPLimit, UtilisationTypeRef.GarminPolarShrdUtil, UtilisationTypeRef.GarminVivoDPDisc, UtilisationTypeRef.GarminVivoDPLim, UtilisationTypeRef.OisixHFAmt, UtilisationTypeRef.PolarDPCnt, UtilisationTypeRef.TestProd1, UtilisationTypeRef.TestProd2, UtilisationTypeRef.TestProd3, UtilisationTypeRef.TestProd3S2]

}
