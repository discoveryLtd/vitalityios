//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum PartyAttributeDataTypeRef: Int, Hashable { 
    case Unknown = -1
    case Boolean_Value = 2 // name:Boolean_Value note:undefined 
    case Date_Value = 5 // name:Date_Value note:undefined 
    case Double_Value = 4 // name:Double_Value note:undefined 
    case Integer_Value = 1 // name:Integer_Value note:undefined 
    case String_Value = 3 // name:String_Vlaue note:undefined 

    public static let allValues: [PartyAttributeDataTypeRef] = [PartyAttributeDataTypeRef.Unknown, PartyAttributeDataTypeRef.Boolean_Value, PartyAttributeDataTypeRef.Date_Value, PartyAttributeDataTypeRef.Double_Value, PartyAttributeDataTypeRef.Integer_Value, PartyAttributeDataTypeRef.String_Value]

}
