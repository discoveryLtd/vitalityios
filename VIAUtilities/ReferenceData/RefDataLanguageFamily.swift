//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum LanguageFamilyRef: Int, Hashable { 
    case Unknown = -1
    case AfroAsiatic = 1 // name:AfroAsiatic languages note:AfroAsiatic languages 
    case Alacalufan = 2 // name:Alacalufan languages note:Alacalufan languages 
    case Algic = 3 // name:Algic languages note:Algic languages 
    case Andamanese = 4 // name:Andamanese languages note:Andamanese languages 
    case Arauan = 5 // name:Arauan languages note:Arauan languages 
    case Araucanian = 6 // name:Araucanian languages note:Araucanian languages 
    case Arawakan = 7 // name:Arawakan languages note:Arawakan languages 
    case ArnhemLand = 8 // name:Arnhem Land languages note:Arnhem Land languages 
    case ArutaniSape = 9 // name:ArutaniSape languages note:ArutaniSape languages 
    case Austroasiatic = 10 // name:Austroasiatic languages note:Austroasiatic languages 
    case Austronesian = 11 // name:Austronesian languages note:Austronesian languages 
    case Aymaran = 12 // name:Aymaran languages note:Aymaran languages 
    case Baining = 13 // name:Baining languages note:Baining languages 
    case Barbacoan = 14 // name:Barbacoan languages note:Barbacoan languages 
    case Border = 15 // name:Border languages note:Border languages 
    case Bunaban = 16 // name:Bunaban languages note:Bunaban languages 
    case Caddoan = 17 // name:Caddoan languages note:Caddoan languages 
    case Cahuapanan = 18 // name:Cahuapanan languages note:Cahuapanan languages 
    case Carib = 19 // name:Carib languages note:Carib languages 
    case Catacaoan = 20 // name:Catacaoan languages note:Catacaoan languages 
    case CentralSolomons = 21 // name:Central Solomons languages note:Central Solomons languages 
    case Chapacuran = 22 // name:Chapacuran languages note:Chapacuran languages 
    case Charruan = 23 // name:Charruan languages note:Charruan languages 
    case Chibchan = 24 // name:Chibchan languages note:Chibchan languages 
    case Chimakuan = 25 // name:Chimakuan languages note:Chimakuan languages 
    case Chimuan = 26 // name:Chimuan languages note:Chimuan languages 
    case Choco = 27 // name:Choco languages note:Choco languages 
    case Chon = 28 // name:Chon languages note:Chon languages 
    case Djeragan = 29 // name:Djeragan languages note:Djeragan languages 
    case Dravidian = 30 // name:Dravidian languages note:Dravidian languages 
    case EastGeelvinkBay = 32 // name:East Geelvink Bay languages note:East Geelvink Bay languages 
    case EastBirdsHeadSen = 31 // name:East Birds Head ? Sentani languages note:East Birds Head ? Sentani languages 
    case EasternTransFly = 33 // name:Eastern TransFly languages note:Eastern TransFly languages 
    case EskimoAleut = 34 // name:EskimoAleut languages note:EskimoAleut languages 
    case EsmereldaYaruro = 35 // name:EsmereldaYaruro languages note:EsmereldaYaruro languages 
    case Fas = 36 // name:Fas languages note:Fas languages 
    case Ge = 37 // name:Ge languages note:Ge languages 
    case Guaicuruan = 38 // name:Guaicuruan languages note:Guaicuruan languages 
    case Gunwinyguan = 39 // name:Gunwinyguan languages note:Gunwinyguan languages 
    case HibitoCholon = 40 // name:HibitoCholon languages note:HibitoCholon languages 
    case Hod = 41 // name:Hod languages note:Hod languages 
    case Hokan = 42 // name:Hokan languages note:Hokan languages 
    case IndoEuropean = 43 // name:IndoEuropean languages note:IndoEuropean languages 
    case Iroquoian = 44 // name:Iroquoian languages note:Iroquoian languages 
    case Japonic = 45 // name:Japonic languages note:Japonic languages 
    case Jicaquean = 46 // name:Jicaquean languages note:Jicaquean languages 
    case Jirajaran = 47 // name:Jirajaran languages note:Jirajaran languages 
    case Jivaroan = 48 // name:Jivaroan languages note:Jivaroan languages 
    case Kadu = 49 // name:Kadu languages note:Kadu languages 
    case Kartvelian = 50 // name:Kartvelian languages note:Kartvelian languages 
    case KatembriTaruma = 51 // name:KatembriTaruma languages note:KatembriTaruma languages 
    case Katukinan = 52 // name:Katukinan languages note:Katukinan languages 
    case Keres = 53 // name:Keres languages note:Keres languages 
    case Khoe = 54 // name:Khoe languages note:Khoe languages 
    case Koman = 55 // name:Koman languages note:Koman languages 
    case Koreanic = 56 // name:Koreanic languages note:Koreanic languages 
    case Kwomtari = 57 // name:Kwomtari languages note:Kwomtari languages 
    case Kxa = 58 // name:Kxa languages note:Kxa languages 
    case LakesPlain = 59 // name:Lakes Plain languages note:Lakes Plain languages 
    case LeftMay = 60 // name:Left May languages note:Left May languages 
    case Lencan = 61 // name:Lencan languages note:Lencan languages 
    case Limilngan = 62 // name:Limilngan languages note:Limilngan languages 
    case LuleVilela = 63 // name:LuleVilela languages note:LuleVilela languages 
    case Luorawetlan = 64 // name:Luorawetlan languages note:Luorawetlan languages 
    case Mairasi = 65 // name:Mairasi languages note:Mairasi languages 
    case Mande = 66 // name:Mande languages note:Mande languages 
    case Mascoian = 67 // name:Mascoian languages note:Mascoian languages 
    case Mashakalian = 68 // name:Mashakalian languages note:Mashakalian languages 
    case Matacoan = 69 // name:Matacoan languages note:Matacoan languages 
    case Mayan = 70 // name:Mayan languages note:Mayan languages 
    case MiaoYao = 71 // name:MiaoYao languages note:MiaoYao languages 
    case Mirndi = 72 // name:Mirndi languages note:Mirndi languages 
    case Misumalpan = 73 // name:Misumalpan languages note:Misumalpan languages 
    case MixeZoquean = 74 // name:MixeZoquean languages note:MixeZoquean languages 
    case Mongolic = 75 // name:Mongolic languages note:Mongolic languages 
    case Mosetenan = 76 // name:Mosetenan languages note:Mosetenan languages 
    case Mura = 77 // name:Mura languages note:Mura languages 
    case Muskogean = 78 // name:Muskogean languages note:Muskogean languages 
    case NaDene = 79 // name:NaDene languages note:NaDene languages 
    case Nadahup = 80 // name:Nadahup languages note:Nadahup languages 
    case Nambiquaran = 81 // name:Nambiquaran languages note:Nambiquaran languages 
    case NigerCongo = 82 // name:NigerCongo languages note:NigerCongo languages 
    case NiloSaharan = 83 // name:NiloSaharan languages note:NiloSaharan languages 
    case Nimboran = 84 // name:Nimboran languages note:Nimboran languages 
    case NorthBougainville = 85 // name:North Bougainville languages note:North Bougainville languages 
    case NortheastCaucasian = 86 // name:Northeast Caucasian languages note:Northeast Caucasian languages 
    case NorthwestCaucasian = 87 // name:Northwest Caucasian languages note:Northwest Caucasian languages 
    case Nyulnyulan = 88 // name:Nyulnyulan languages note:Nyulnyulan languages 
    case Ongan = 89 // name:Ongan languages note:Ongan languages 
    case OtoManguean = 90 // name:OtoManguean languages note:OtoManguean languages 
    case Otomakoan = 91 // name:Otomakoan languages note:Otomakoan languages 
    case PamaNyungan = 92 // name:PamaNyungan languages note:PamaNyungan languages 
    case PanoTacanan = 93 // name:PanoTacanan languages note:PanoTacanan languages 
    case PebaYaguan = 94 // name:PebaYaguan languages note:PebaYaguan languages 
    case Penutian = 95 // name:Penutian note:Penutian 
    case Piawi = 96 // name:Piawi languages note:Piawi languages 
    case Puinavean = 97 // name:Puinavean languages note:Puinavean languages 
    case Quechuan = 98 // name:Quechuan languages note:Quechuan languages 
    case RamuLowerSepik = 99 // name:Ramu Lower Sepik languages note:Ramu Lower Sepik languages 
    case Salishan = 100 // name:Salishan languages note:Salishan languages 
    case Salivan = 101 // name:Salivan languages note:Salivan languages 
    case Senagi = 102 // name:Senagi languages note:Senagi languages 
    case Sepik = 103 // name:Sepik languages note:Sepik languages 
    case Siangic = 104 // name:Siangic note:Siangic 
    case SinoTibetan = 105 // name:SinoTibetan languages note:SinoTibetan languages 
    case SiouanCatawban = 106 // name:SiouanCatawban languages note:SiouanCatawban languages 
    case Skou = 107 // name:Skou languages note:Skou languages 
    case Songhay = 108 // name:Songhay languages note:Songhay languages 
    case SouthBougainville = 109 // name:South Bougainville languages note:South Bougainville languages 
    case SouthernDaly = 110 // name:Southern Daly note:Southern Daly 
    case TaiKadai = 111 // name:TaiKadai languages note:TaiKadai languages 
    case Tanoan = 112 // name:Tanoan languages note:Tanoan languages 
    case TequiracaCanichana = 113 // name:TequiracaCanichana languages note:TequiracaCanichana languages 
    case Timotean = 114 // name:Timotean languages note:Timotean languages 
    case Tiniguan = 115 // name:Tiniguan languages note:Tiniguan languages 
    case TorKwerba = 117 // name:TorKwerba languages note:TorKwerba languages 
    case Torricelli = 116 // name:Torricelli languages note:Torricelli languages 
    case Totonacan = 118 // name:Totonacan languages note:Totonacan languages 
    case TransFlyBulakaRiv = 119 // name:TransFly Bulaka River languages note:TransFly Bulaka River languages 
    case TransNewGuinea = 120 // name:TransNew Guinea note:TransNew Guinea 
    case Tucanoan = 121 // name:Tucanoan languages note:Tucanoan languages 
    case Tungusic = 122 // name:Tungusic languages note:Tungusic languages 
    case Tupian = 123 // name:Tupian languages note:Tupian languages 
    case Turkic = 124 // name:Turkic languages note:Turkic languages 
    case Tuu = 125 // name:Tuu languages note:Tuu languages 
    case Ubangian = 126 // name:Ubangian languages note:Ubangian languages 
    case Uralic = 127 // name:Uralic languages note:Uralic languages 
    case UruChipaya = 128 // name:UruChipaya languages note:UruChipaya languages 
    case UtoAztecan = 129 // name:UtoAztecan languages note:UtoAztecan languages 
    case Wagaydyic = 130 // name:Wagaydyic note:Wagaydyic 
    case Wakashan = 131 // name:Wakashan languages note:Wakashan languages 
    case WestNewBritain = 132 // name:West New Britain languages note:West New Britain languages 
    case WestPapuan = 133 // name:West Papuan languages note:West Papuan languages 
    case WesternDaly = 134 // name:Western Daly note:Western Daly 
    case Wintuan = 135 // name:Wintuan languages note:Wintuan languages 
    case Witotoan = 136 // name:Witotoan languages note:Witotoan languages 
    case Wororan = 137 // name:Wororan languages note:Wororan languages 
    case Xincan = 138 // name:Xincan languages note:Xincan languages 
    case Yabutian = 139 // name:Yabutian languages note:Yabutian languages 
    case Yanomam = 140 // name:Yanomam languages note:Yanomam languages 
    case Yeniseian = 141 // name:Yeniseian languages note:Yeniseian languages 
    case YokUtian = 142 // name:YokUtian languages note:YokUtian languages 
    case Yuat = 143 // name:Yuat languages note:Yuat languages 
    case Yukaghir = 144 // name:Yukaghir languages note:Yukaghir languages 
    case Yukian = 145 // name:Yukian languages note:Yukian languages 
    case Zamucoan = 146 // name:Zamucoan languages note:Zamucoan languages 
    case Zaparoan = 147 // name:Zaparoan languages note:Zaparoan languages 

    public static let allValues: [LanguageFamilyRef] = [LanguageFamilyRef.Unknown, LanguageFamilyRef.AfroAsiatic, LanguageFamilyRef.Alacalufan, LanguageFamilyRef.Algic, LanguageFamilyRef.Andamanese, LanguageFamilyRef.Arauan, LanguageFamilyRef.Araucanian, LanguageFamilyRef.Arawakan, LanguageFamilyRef.ArnhemLand, LanguageFamilyRef.ArutaniSape, LanguageFamilyRef.Austroasiatic, LanguageFamilyRef.Austronesian, LanguageFamilyRef.Aymaran, LanguageFamilyRef.Baining, LanguageFamilyRef.Barbacoan, LanguageFamilyRef.Border, LanguageFamilyRef.Bunaban, LanguageFamilyRef.Caddoan, LanguageFamilyRef.Cahuapanan, LanguageFamilyRef.Carib, LanguageFamilyRef.Catacaoan, LanguageFamilyRef.CentralSolomons, LanguageFamilyRef.Chapacuran, LanguageFamilyRef.Charruan, LanguageFamilyRef.Chibchan, LanguageFamilyRef.Chimakuan, LanguageFamilyRef.Chimuan, LanguageFamilyRef.Choco, LanguageFamilyRef.Chon, LanguageFamilyRef.Djeragan, LanguageFamilyRef.Dravidian, LanguageFamilyRef.EastGeelvinkBay, LanguageFamilyRef.EastBirdsHeadSen, LanguageFamilyRef.EasternTransFly, LanguageFamilyRef.EskimoAleut, LanguageFamilyRef.EsmereldaYaruro, LanguageFamilyRef.Fas, LanguageFamilyRef.Ge, LanguageFamilyRef.Guaicuruan, LanguageFamilyRef.Gunwinyguan, LanguageFamilyRef.HibitoCholon, LanguageFamilyRef.Hod, LanguageFamilyRef.Hokan, LanguageFamilyRef.IndoEuropean, LanguageFamilyRef.Iroquoian, LanguageFamilyRef.Japonic, LanguageFamilyRef.Jicaquean, LanguageFamilyRef.Jirajaran, LanguageFamilyRef.Jivaroan, LanguageFamilyRef.Kadu, LanguageFamilyRef.Kartvelian, LanguageFamilyRef.KatembriTaruma, LanguageFamilyRef.Katukinan, LanguageFamilyRef.Keres, LanguageFamilyRef.Khoe, LanguageFamilyRef.Koman, LanguageFamilyRef.Koreanic, LanguageFamilyRef.Kwomtari, LanguageFamilyRef.Kxa, LanguageFamilyRef.LakesPlain, LanguageFamilyRef.LeftMay, LanguageFamilyRef.Lencan, LanguageFamilyRef.Limilngan, LanguageFamilyRef.LuleVilela, LanguageFamilyRef.Luorawetlan, LanguageFamilyRef.Mairasi, LanguageFamilyRef.Mande, LanguageFamilyRef.Mascoian, LanguageFamilyRef.Mashakalian, LanguageFamilyRef.Matacoan, LanguageFamilyRef.Mayan, LanguageFamilyRef.MiaoYao, LanguageFamilyRef.Mirndi, LanguageFamilyRef.Misumalpan, LanguageFamilyRef.MixeZoquean, LanguageFamilyRef.Mongolic, LanguageFamilyRef.Mosetenan, LanguageFamilyRef.Mura, LanguageFamilyRef.Muskogean, LanguageFamilyRef.NaDene, LanguageFamilyRef.Nadahup, LanguageFamilyRef.Nambiquaran, LanguageFamilyRef.NigerCongo, LanguageFamilyRef.NiloSaharan, LanguageFamilyRef.Nimboran, LanguageFamilyRef.NorthBougainville, LanguageFamilyRef.NortheastCaucasian, LanguageFamilyRef.NorthwestCaucasian, LanguageFamilyRef.Nyulnyulan, LanguageFamilyRef.Ongan, LanguageFamilyRef.OtoManguean, LanguageFamilyRef.Otomakoan, LanguageFamilyRef.PamaNyungan, LanguageFamilyRef.PanoTacanan, LanguageFamilyRef.PebaYaguan, LanguageFamilyRef.Penutian, LanguageFamilyRef.Piawi, LanguageFamilyRef.Puinavean, LanguageFamilyRef.Quechuan, LanguageFamilyRef.RamuLowerSepik, LanguageFamilyRef.Salishan, LanguageFamilyRef.Salivan, LanguageFamilyRef.Senagi, LanguageFamilyRef.Sepik, LanguageFamilyRef.Siangic, LanguageFamilyRef.SinoTibetan, LanguageFamilyRef.SiouanCatawban, LanguageFamilyRef.Skou, LanguageFamilyRef.Songhay, LanguageFamilyRef.SouthBougainville, LanguageFamilyRef.SouthernDaly, LanguageFamilyRef.TaiKadai, LanguageFamilyRef.Tanoan, LanguageFamilyRef.TequiracaCanichana, LanguageFamilyRef.Timotean, LanguageFamilyRef.Tiniguan, LanguageFamilyRef.TorKwerba, LanguageFamilyRef.Torricelli, LanguageFamilyRef.Totonacan, LanguageFamilyRef.TransFlyBulakaRiv, LanguageFamilyRef.TransNewGuinea, LanguageFamilyRef.Tucanoan, LanguageFamilyRef.Tungusic, LanguageFamilyRef.Tupian, LanguageFamilyRef.Turkic, LanguageFamilyRef.Tuu, LanguageFamilyRef.Ubangian, LanguageFamilyRef.Uralic, LanguageFamilyRef.UruChipaya, LanguageFamilyRef.UtoAztecan, LanguageFamilyRef.Wagaydyic, LanguageFamilyRef.Wakashan, LanguageFamilyRef.WestNewBritain, LanguageFamilyRef.WestPapuan, LanguageFamilyRef.WesternDaly, LanguageFamilyRef.Wintuan, LanguageFamilyRef.Witotoan, LanguageFamilyRef.Wororan, LanguageFamilyRef.Xincan, LanguageFamilyRef.Yabutian, LanguageFamilyRef.Yanomam, LanguageFamilyRef.Yeniseian, LanguageFamilyRef.YokUtian, LanguageFamilyRef.Yuat, LanguageFamilyRef.Yukaghir, LanguageFamilyRef.Yukian, LanguageFamilyRef.Zamucoan, LanguageFamilyRef.Zaparoan]

}
