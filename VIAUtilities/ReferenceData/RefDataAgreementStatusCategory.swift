//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum AgreementStatusCategoryRef: Int, Hashable { 
    case Unknown = -1
    case AGREEMENT = 6 // name:Agreement note:The agreement lifecycle status category 
    case BENEFIT = 7 // name:Benefit note:Benefit 
    case CONTRACT = 1 // name: Contract note: Contract 
    case ENGAGEMENT = 5 // name: Engagement note: Engagement 
    case MEMBER_LIABILITY = 2 // name: Member Liability note: Member Liability 
    case REWARD = 4 // name: Reward note: Reward 
    case VITALITY_LIABILITY = 3 // name: Vitality Liability note: Vitality Liability 

    public static let allValues: [AgreementStatusCategoryRef] = [AgreementStatusCategoryRef.Unknown, AgreementStatusCategoryRef.AGREEMENT, AgreementStatusCategoryRef.BENEFIT, AgreementStatusCategoryRef.CONTRACT, AgreementStatusCategoryRef.ENGAGEMENT, AgreementStatusCategoryRef.MEMBER_LIABILITY, AgreementStatusCategoryRef.REWARD, AgreementStatusCategoryRef.VITALITY_LIABILITY]

}
