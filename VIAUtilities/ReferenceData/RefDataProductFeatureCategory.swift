//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum ProductFeatureCategoryRef: Int, Hashable { 
    case Unknown = -1
    case ActiveRewards = 18 // name:ActiveRewards note: 
    case ActiveRewardsPartner = 33 // name:Active Rewards Partners note:Active Rewards Partners 
	case CorporateBenefit = 34 // name:Corporate Benefit note:Corporate Benefit
    case AppleWatch = 17 // name:AppleWatch note: 
    case AssessmentsMobile = 19 // name:Assessments note:Assessments 
    case EarnPointsOnlineWeb = 25 // name:Earn Points Online note:Earn Points Online 
    case EatHealthyWeb = 27 // name:Eat Healthy note:Eat Healthy 
    case EntertainmentPartner = 14 // name:Entertainment Partner note: 
    case FitnessAssessment = 13 // name:Fitness Assessment note: 
    case FitnessDevice = 8 // name:Fitness Device note: 
    case GetActiveMobile = 22 // name:Get Active note:Get Active 
    case GetActiveWeb = 28 // name:Get Active note:Get Active 
    case GymAndFitness = 9 // name:Gym and Fitness note: 
    case HealthAssPrevCareWeb = 26 // name:Health Assessments and Preventative Care note:Health Assessments and Preventative Care 
    case HealthPartners = 4 // name:Health Partners note: 
    case HealthyFood = 7 // name:Healthy Food note: 
    case HealthyGear = 10 // name:Healthy Gear note: 
    case ImproveYourHealth = 2 // name:Improve Your Health note: 
    case ImproveYourHealthWeb = 24 // name:Improve Your Health note:Improve Your Health 
    case KnowYourHealth = 1 // name:Know Your Health note: 
    case KnowYourHealthWeb = 23 // name:Know Your Health note:Know Your Health 
    case NutritionMobile = 20 // name:Nutrition note:Nutrition 
    case OnlineAssessKYHWeb = 29 // name:Online Assessments note:null 
    case OtherAssessKYHWeb = 30 // name:Other Assessments  note:null
    case RewardPartners = 6 // name:Reward Partners note: 
    case Rewards = 3 // name:Rewards note: 
    case ScreenAndVaccPartner = 31 // name:ScreenAndVaccPartner note:Screenings and Vaccinations Partners 
    case ScreeningsMobile = 21 // name:Screenings note:Screenings 
    case StopSmoking = 11 // name:Stop Smoking note: 
    case Travel = 16 // name:Travel note: 
    case WeightManagement = 12 // name:Weight Management note: 
    case WellnessPartners = 5 // name:Wellness Partners note:
    case EmployerReward = 36 // name:Employer Reward
    case NuffieldHealthServices = 15 // name:Nuffield Health Service
    case CorporateProgramme = 39 // name:Corporate Partner note:Corporate Partner

    public static let allValues: [ProductFeatureCategoryRef] = [ProductFeatureCategoryRef.Unknown, ProductFeatureCategoryRef.ActiveRewards, ProductFeatureCategoryRef.ActiveRewardsPartner, ProductFeatureCategoryRef.AppleWatch, ProductFeatureCategoryRef.AssessmentsMobile, ProductFeatureCategoryRef.EarnPointsOnlineWeb, ProductFeatureCategoryRef.EatHealthyWeb, ProductFeatureCategoryRef.EntertainmentPartner, ProductFeatureCategoryRef.FitnessAssessment, ProductFeatureCategoryRef.FitnessDevice, ProductFeatureCategoryRef.GetActiveMobile, ProductFeatureCategoryRef.GetActiveWeb, ProductFeatureCategoryRef.GymAndFitness, ProductFeatureCategoryRef.HealthAssPrevCareWeb, ProductFeatureCategoryRef.HealthPartners, ProductFeatureCategoryRef.HealthyFood, ProductFeatureCategoryRef.HealthyGear, ProductFeatureCategoryRef.ImproveYourHealth, ProductFeatureCategoryRef.ImproveYourHealthWeb, ProductFeatureCategoryRef.KnowYourHealth, ProductFeatureCategoryRef.KnowYourHealthWeb, ProductFeatureCategoryRef.NutritionMobile, ProductFeatureCategoryRef.OnlineAssessKYHWeb, ProductFeatureCategoryRef.OtherAssessKYHWeb, ProductFeatureCategoryRef.RewardPartners, ProductFeatureCategoryRef.Rewards, ProductFeatureCategoryRef.ScreenAndVaccPartner, ProductFeatureCategoryRef.ScreeningsMobile, ProductFeatureCategoryRef.StopSmoking, ProductFeatureCategoryRef.Travel, ProductFeatureCategoryRef.WeightManagement, ProductFeatureCategoryRef.WellnessPartners, ProductFeatureCategoryRef.CorporateBenefit, ProductFeatureCategoryRef.EmployerReward, ProductFeatureCategoryRef.NuffieldHealthServices]

}
