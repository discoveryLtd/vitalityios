//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum CardItemMetadataTypeRef: Int, Hashable { 
    case Unknown = -1
    case AwardedRewardId = 1 // name:Awarded Reward Id note:Awarded Reward Id

    public static let allValues: [CardItemMetadataTypeRef] = [CardItemMetadataTypeRef.Unknown, CardItemMetadataTypeRef.AwardedRewardId]

}
