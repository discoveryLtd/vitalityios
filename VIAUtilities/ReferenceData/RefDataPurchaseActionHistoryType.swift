//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum PurchaseActionHistoryTypeRef: Int, Hashable { 
    case Unknown = -1
    case PurchaseInitiated = 1 // name:PurchaseInitiated  note:Indicates when the purchase occured at the partner 

    public static let allValues: [PurchaseActionHistoryTypeRef] = [PurchaseActionHistoryTypeRef.Unknown, PurchaseActionHistoryTypeRef.PurchaseInitiated]

}
