//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum QuestionnaireSetRef: Int, Hashable { 
    case Unknown = -1
    case MWB = 3 // name:Mental Wellbeing Assessment note:Mental Wellbeing Assessment 
    case VHR = 1 // name:Vitality Health Review note:Vitality Health Review 
    case VNA = 2 // name:Vitality Nutrition Assessment note:Vitality Nutrition Assessment 

    public static let allValues: [QuestionnaireSetRef] = [QuestionnaireSetRef.Unknown, QuestionnaireSetRef.MWB, QuestionnaireSetRef.VHR, QuestionnaireSetRef.VNA]

}
