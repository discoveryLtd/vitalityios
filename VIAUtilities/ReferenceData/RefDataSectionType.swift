//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum SectionTypeRef: Int, Hashable { 
    case Unknown = -1
    case GetRewarded = 3 // name:Get Rewarded note:Get Rewarded 
    case ImproveYourHealth = 2 // name:Improve Your Health note:Improve Your Health 
    case KnowYourHealth = 1 // name:Know Your Health note:Know Your Health 

    public static let allValues: [SectionTypeRef] = [SectionTypeRef.Unknown, SectionTypeRef.GetRewarded, SectionTypeRef.ImproveYourHealth, SectionTypeRef.KnowYourHealth]

}
