//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum AmountTypeRef: Int, Hashable { 
    case Unknown = -1
    case ActivationFee = 1 // name:ActivationFee note:ActivationFee 

    public static let allValues: [AmountTypeRef] = [AmountTypeRef.Unknown, AmountTypeRef.ActivationFee]

}
