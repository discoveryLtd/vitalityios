//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum CardMetadataTypeRef: Int, Hashable { 
    case Unknown = -1
    case AwardedRewardId = 8 // name:Awarded Reward Id note:Awarded Reward Id 
    case CardFeatureDeviceCashback = 19
    case CurrentRewardValue = 22 // name:Current Reward Value note:Current Reward Value
    case DeliverDate = 24 // name:Deliver Date note:Deliver Date
    case EarnedPoints = 3 // name:Earned Points note:Earned Points 
    case GoalEndDate = 11 // name:Goal End Date note:Goal End Date 
    case GoalObjectiveKey = 30 // name:Goal Objective Key note:Goal Objective Key
    case GoalStartDate = 15 // name:Goal Start Date note:Goal Start Date 
    case NextRewardValue = 21 // name:Next Reward Value note:Next Reward Value
    case NumOfLinkedDevices = 4 // name:The number of linked devices a member has note:The number of linked devices a member has 
    case PotentialPoints = 1 // name:Potential Points note:Potential Points 
    case RewardId = 6 // name:Reward Id note:Reward Id 
    case RewardName = 5 // name:Reward Name note:Reward Name 
    case RewardType = 2 // name:Reward Type note:Reward Type 
    case RewardValueAmount = 9 // name:Reward Value Amount note:Reward Value Amount 
    case RewardValueDesc = 13 // name:Reward Value Desc note:Reward Value Desc 
    case RewardValuePercent = 12 // name:Reward Value Percent note:RewardV alue Percent 
    case RewardValueQuantity = 7 // name:Reward Value Quantity note:Reward Value Quantity 
    case RewardValueType = 17 // name:Reward Value Type note:The Type key for reward value. (E.g. Monetary value, Discount, Reard Item) 
    case VitalityStatus = 16 // name:Vitality Status note:Vitality Status
    case DeliveryDate = 29 // Manual add

    public static let allValues: [CardMetadataTypeRef] = [CardMetadataTypeRef.Unknown, CardMetadataTypeRef.AwardedRewardId, CardMetadataTypeRef.CardFeatureDeviceCashback, CardMetadataTypeRef.CurrentRewardValue, CardMetadataTypeRef.DeliverDate, CardMetadataTypeRef.EarnedPoints, CardMetadataTypeRef.GoalEndDate, CardMetadataTypeRef.GoalObjectiveKey,CardMetadataTypeRef.GoalStartDate, CardMetadataTypeRef.NextRewardValue, CardMetadataTypeRef.NumOfLinkedDevices, CardMetadataTypeRef.PotentialPoints, CardMetadataTypeRef.RewardId, CardMetadataTypeRef.RewardName, CardMetadataTypeRef.RewardType, CardMetadataTypeRef.RewardValueAmount, CardMetadataTypeRef.RewardValueDesc, CardMetadataTypeRef.RewardValuePercent, CardMetadataTypeRef.RewardValueQuantity, CardMetadataTypeRef.RewardValueType, CardMetadataTypeRef.VitalityStatus, CardMetadataTypeRef.DeliveryDate]

}
