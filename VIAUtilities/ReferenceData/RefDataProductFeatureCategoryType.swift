//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum ProductFeatureCategoryTypeRef: Int, Hashable { 
    case Unknown = -1
    case ARRewardsPartner = 11 // name:Active Rewards Partner note:Active Rewards Partner 
    case EarnPointsMobile = 7 // name:EarnPointsMobile note:Mobile journey - Earn Points 
    case EarnPointsWeb = 9 // name:EarnPointsWeb note:How to earn points web 
    case KnowYourHealthWeb = 10 // name:KnowYourHealthWeb note:Know your Health web 
    case Level0 = 1 // name:Level0 note:Code and name will be updated? 
    case Level1 = 2 // name:Level1 note:Code and name will be updated? 
    case Level2 = 3 // name:Level2 note:Code and name will be updated? 
    case Level3 = 4 // name:Level3 note:Code and name will be updated? 
    case Level4 = 5 // name:Level4 note:Code and name will be updated? 
    case Level5 = 6 // name:Level5 note:Code and name will be updated? 
    case MaintainStatusWeb = 8 // name:MaintainStatusWeb note:Maintain status web journey 
    case SVPartner = 12 // name:SVPartner note:Screenings and Vaccinations Participating Partners Journey 

    public static let allValues: [ProductFeatureCategoryTypeRef] = [ProductFeatureCategoryTypeRef.Unknown, ProductFeatureCategoryTypeRef.ARRewardsPartner, ProductFeatureCategoryTypeRef.EarnPointsMobile, ProductFeatureCategoryTypeRef.EarnPointsWeb, ProductFeatureCategoryTypeRef.KnowYourHealthWeb, ProductFeatureCategoryTypeRef.Level0, ProductFeatureCategoryTypeRef.Level1, ProductFeatureCategoryTypeRef.Level2, ProductFeatureCategoryTypeRef.Level3, ProductFeatureCategoryTypeRef.Level4, ProductFeatureCategoryTypeRef.Level5, ProductFeatureCategoryTypeRef.MaintainStatusWeb, ProductFeatureCategoryTypeRef.SVPartner]

}
