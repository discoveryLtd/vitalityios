//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum OrganisationTypeRef: Int, Hashable { 
    case Unknown = -1
    case Carrier = 1 // name:Carrier note:Carrier 
    case ClosedCorporation = 2 // name:Closed Corporation note:Closed Corporation 
    case Club = 4 // name:Club note:Club 
    case LimitedCompany = 3 // name:Limited Company note:Limited Company 

    public static let allValues: [OrganisationTypeRef] = [OrganisationTypeRef.Unknown, OrganisationTypeRef.Carrier, OrganisationTypeRef.ClosedCorporation, OrganisationTypeRef.Club, OrganisationTypeRef.LimitedCompany]

}
