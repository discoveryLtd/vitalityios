//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum ReasonTypeRef: Int, Hashable { 
    case Unknown = -1
    case Detail = 2 // name:Detail reason note:Detail reason 
    case Summary = 1 // name:Summary reason note:Summary  

    public static let allValues: [ReasonTypeRef] = [ReasonTypeRef.Unknown, ReasonTypeRef.Detail, ReasonTypeRef.Summary]

}
