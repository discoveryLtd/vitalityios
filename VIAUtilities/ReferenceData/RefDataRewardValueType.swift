//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum RewardValueTypeRef: Int, Hashable { 
    case Unknown = -1
    case Monetary = 1 // name:Monetary note:A monetary value such as Rands or Dollars 
    case Percentage = 2 // name:Percentage note:Percentage discount value 
    case SpecificItem = 3 // name:Specific Item note:A specific item such as a cup of coffee 

    public static let allValues: [RewardValueTypeRef] = [RewardValueTypeRef.Unknown, RewardValueTypeRef.Monetary, RewardValueTypeRef.Percentage, RewardValueTypeRef.SpecificItem]

}
