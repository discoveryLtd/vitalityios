//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum UtilisationLimitValueTypeRef: Int, Hashable { 
    case Unknown = -1
    case Amount = 1 // name:Amount note:The Value Limit Type is a Monetary Amount 
    case Count = 2 // name:Count note:The Value Limit Type is a Count 

    public static let allValues: [UtilisationLimitValueTypeRef] = [UtilisationLimitValueTypeRef.Unknown, UtilisationLimitValueTypeRef.Amount, UtilisationLimitValueTypeRef.Count]

}
