//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum PurchaseItemStatusTypeRef: Int, Hashable { 
    case Unknown = -1
    case Fulfilled = 1 // name:Fulfilled note:The purchase item has been supplied an paid for by the member 
    case Returned = 2 // name:Returned note:The purchase item has been returned to the partner by the member 

    public static let allValues: [PurchaseItemStatusTypeRef] = [PurchaseItemStatusTypeRef.Unknown, PurchaseItemStatusTypeRef.Fulfilled, PurchaseItemStatusTypeRef.Returned]

}
