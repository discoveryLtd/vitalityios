//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum ResolveReferenceTypeRef: Int, Hashable { 
    case Unknown = -1
    case Branch = 7 // name:Branch note:Branch 
    case CarrierUID = 8 // name:Carrier Unique Identity note:Carrier Unique Identity 
    case CustomerNumber = 9 // name:Customer Number note:Customer Number 
    case EXTERNALREFTYPE = 2 // name:External Reference Type note:External Reference Type 
    case MemberCard = 3 // name:Member Card note:Member Card 
    case NationalID = 10 // name:National Identity Number note:National Identity Number 
    case PARTNERCONTRACTNO = 4 // name:Partner Contract Number note:Partner Contract Number 
    case PartnerIdentifier = 11 // name:PartnerIdentifier note:Partner Identifer that uniquely identifies a partner 
    case PartnerMemberNumber = 5 // name:Partner Member Number note:Partner Member Number 
    case PartnerMembership = 12 // name:Partner Membership note:Partner Membership 
    case PartyID = 1 // name:Party ID note:Party ID 
    case PolicyNumber = 6 // name:Policy Number note:Policy Number 
    case VitalityMembership = 13 // name:Vitality Membership note:Vitality Membership 

    public static let allValues: [ResolveReferenceTypeRef] = [ResolveReferenceTypeRef.Unknown, ResolveReferenceTypeRef.Branch, ResolveReferenceTypeRef.CarrierUID, ResolveReferenceTypeRef.CustomerNumber, ResolveReferenceTypeRef.EXTERNALREFTYPE, ResolveReferenceTypeRef.MemberCard, ResolveReferenceTypeRef.NationalID, ResolveReferenceTypeRef.PARTNERCONTRACTNO, ResolveReferenceTypeRef.PartnerIdentifier, ResolveReferenceTypeRef.PartnerMemberNumber, ResolveReferenceTypeRef.PartnerMembership, ResolveReferenceTypeRef.PartyID, ResolveReferenceTypeRef.PolicyNumber, ResolveReferenceTypeRef.VitalityMembership]

}
