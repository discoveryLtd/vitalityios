//
//  RefDataEventActivity.swift
//  VitalityActive
//
//  Created by OJ Garde on 10/11/2017.
//  Copyright © 2017 Glucode. All rights reserved.
//
//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum EventsFeedEntryCategoryRef: Int, Hashable {
    
    case Unknown = -1
    case Assessment = 1 //name:Assessment note:Assessment
    case Nutrition = 2 //name:Nutrition note:Nutrition
    case Screening = 3 //name:Screening note:Screening
    case HealthData = 4 //name:Health Data note:Health Data
    case GetActive = 5 //name:Get Active note:Get Active
    case Devices = 6 //name:Devices note:Devices
    case Activation = 7 //name:Activation note:Activation
    case Rewards = 8 //name:Rewards note:Rewards
    case Status = 9 //name:Status note:Status
    case DataSharingAndLegal = 10 //name:Data Sharing and Legal note:Data Sharing and Legal
    case ProfileManagement = 11 //name:Profile Management note:Profile Management
    case Financials = 12 //name:Financials note:Financials
    case Other = 13 //name:Other note:Other
    
    public static let allValues: [EventsFeedEntryCategoryRef] = [EventsFeedEntryCategoryRef.Unknown,
                                                             EventsFeedEntryCategoryRef.Assessment,
                                                             EventsFeedEntryCategoryRef.Nutrition,
                                                             EventsFeedEntryCategoryRef.Screening,
                                                             EventsFeedEntryCategoryRef.HealthData,
                                                             EventsFeedEntryCategoryRef.GetActive,
                                                             EventsFeedEntryCategoryRef.Devices,
                                                             EventsFeedEntryCategoryRef.Activation,
                                                             EventsFeedEntryCategoryRef.Rewards,
                                                             EventsFeedEntryCategoryRef.Status,
                                                             EventsFeedEntryCategoryRef.DataSharingAndLegal,
                                                             EventsFeedEntryCategoryRef.ProfileManagement,
                                                             EventsFeedEntryCategoryRef.Financials,
                                                             EventsFeedEntryCategoryRef.Other]
    
}
