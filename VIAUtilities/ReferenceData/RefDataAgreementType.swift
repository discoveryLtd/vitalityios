//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum AgreementTypeRef: Int, Hashable { 
    case Unknown = -1
    case Benefit = 2 // name:Vitality Benefit note:undefined 
    case Carrierpolicy = 5 // name:Carrier Policy note:undefined 
    case Vmembership = 4 // name:Vitality Membership note:undefined 
    case Pmembership = 3 // name:Partner Membership note:undefined 
    case Programme = 1 // name:Vitality Programme note:undefined 

    public static let allValues: [AgreementTypeRef] = [AgreementTypeRef.Unknown, AgreementTypeRef.Benefit, AgreementTypeRef.Carrierpolicy, AgreementTypeRef.Vmembership, AgreementTypeRef.Pmembership, AgreementTypeRef.Programme]

}
