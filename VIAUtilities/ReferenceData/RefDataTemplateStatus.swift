//  Vitality Active
//
//  Created by Swagger Codegen on 12/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.

//  Don't modify this file
//
//  swiftlint:disable type_body_length
//  swiftlint:disable file_length
//

import Foundation

@objc public enum TemplateStatusRef: Int, Hashable { 
    case Unknown = -1
    case HTML = 2 // name:HTML note:undefined 
    case PDF = 1 // name:PDF note:undefined 

    public static let allValues: [TemplateStatusRef] = [TemplateStatusRef.Unknown, TemplateStatusRef.HTML, TemplateStatusRef.PDF]

}
