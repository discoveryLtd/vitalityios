import Foundation

extension Array where Element: Equatable {
    
    /// Remove first collection element that is equal to the given `object`:
    public mutating func remove(object: Element) {
        if let index = index(of: object) {
            remove(at: index)
        }
    }
}


extension Array {
    
    public func filterDuplicates(includeElement: (_ lhs: Element, _ rhs: Element) -> Bool) -> [Element] {
        var results = [Element]()
        
        forEach { (element) in
            let existingElements = results.filter {
                return includeElement(element, $0)
            }
            if existingElements.count == 0 {
                results.append(element)
            }
        }
        
        return results
    }
    
    // Safely lookup an index that might be out of bounds and returns nil if index is out of bounds
    public func element(at index: Int) ->Element? {
        if 0 <= index && index < count {
            return self[index]
        } else {
            return nil
        }
    }
}
