public struct NotificationInfo {
    public var url: URL
    public var urlComponents: URLComponents?

    public init(url: URL) {
        self.url = url
        self.urlComponents = URLComponents(string: url.absoluteString)
    }
}


public extension NotificationInfo {

    public static func postNotification(from url: URL) {
        let info = NotificationInfo(url: url)
        NotificationCenter.default.post(name: .VIAApplicationDidOpenURL, object: info)
    }
}
