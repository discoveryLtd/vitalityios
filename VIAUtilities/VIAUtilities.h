//
//  VIAUtilities.h
//  VIAUtilities
//
//  Created by Wilmar van Heerden on 2017/02/09.
//  Copyright © 2017 Glucode. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for VIAUtilities.
FOUNDATION_EXPORT double VIAUtilitiesVersionNumber;

//! Project version string for VIAUtilities.
FOUNDATION_EXPORT const unsigned char VIAUtilitiesVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <VIAUtilities/PublicHeader.h>
#import <VIAUtilities/GoogleTagManagerPatch.h>
