//
//  Date.swift
//  VitalityActive
//
//  Created by Val Tomol on 15/05/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation

extension Date {
    public func setMinimumDate() -> Date? {
        // Check if the membership period date falls between the 6-month gap, then use the membership date as the earliest date.
        let presentDate = Date()
        // let captureBuffer = Bundle.main.object(forInfoDictionaryKey: "VAVHCCaptureLimitBufferDays") as? Int ?? 0
        let captureLimitMonths = Bundle.main.object(forInfoDictionaryKey: "VAVHCCaptureLimitMonths") as? Int ?? 0
        let earliestDate = Calendar.current.date(byAdding: .month, value: -captureLimitMonths, to: presentDate)
        let monthGapComponents = Calendar.current.dateComponents([.month], from: self, to: presentDate)
        
        return monthGapComponents.month! < captureLimitMonths ? self : earliestDate
    }
}
