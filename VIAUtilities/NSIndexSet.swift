extension NSIndexSet {

    public func integer(at anIndex: Int) -> Int {
        if anIndex >= self.count {
            return NSNotFound
        }

        var index = self.firstIndex
        for _ in stride(from: 0, to: anIndex, by: 1) {
            index = self.indexGreaterThanIndex(index)
        }
        return index
    }

    public func index(for anInteger: Int) -> Int {
        var integer = self.firstIndex
        var index = 0

        while integer != NSNotFound && integer != anInteger {
            integer = self.indexGreaterThanIndex(integer)
            index += 1
        }
        if integer == anInteger {
            return index
        }
        return NSNotFound
    }

}
