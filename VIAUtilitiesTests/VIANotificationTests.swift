import XCTest

class VIANotificationTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testNotificationStringsAreCorrect() {
        XCTAssertTrue(NSNotification.Name.VIANonSmokersDeclarationCompletedNotification.rawValue == "VIANonSmokersDeclarationCompletedNotification")
        XCTAssertTrue(NSNotification.Name.VIAVHCCompletedNotification.rawValue == "VIAVHCCompletedNotification")
        XCTAssertTrue(NSNotification.Name.VIAVHRQuestionnaireSubmittedNotification.rawValue == "VIAVHRQuestionnaireSubmittedNotification")
        XCTAssertTrue(NSNotification.Name.VIAVHRQuestionnaireStarted.rawValue == "VIAVHRQuestionnaireStarted")
        XCTAssertTrue(NSNotification.Name.VIAServerSecurityException.rawValue == "VIAServerSecurityException")
        XCTAssertTrue(NSNotification.Name.VIASuccessfullyCapturedVHCHealthAttribute.rawValue == "VIASuccessfullyCapturedVHCHealthAttribute")
        XCTAssertTrue(NSNotification.Name.VIAApplicationWillLogout.rawValue == "VIAApplicationWillLogout")
        XCTAssertTrue(NSNotification.Name.VIAApplicationDidLogout.rawValue == "VIAApplicationDidLogout")
        XCTAssertTrue(NSNotification.Name.VIAApplicationDidOpenURL.rawValue == "VIAApplicationDidOpenURL")
        XCTAssertTrue(NSNotification.Name.VIAPotentialPointsWithTiersReceived.rawValue == "VIAPotentialPointsWithTiersReceived")
        XCTAssertTrue(NSNotification.Name.VIAWDANeedToFetchUpdatedDevices.rawValue == "VIAWDANeedToFetchUpdatedDevices")
        XCTAssertTrue(NSNotification.Name.VIAWDADidFetchDevices.rawValue == "VIAWDADidFetchDevices")
    }
    
}
