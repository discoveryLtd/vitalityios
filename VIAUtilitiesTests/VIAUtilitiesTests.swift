//
//  VIAUtilitiesTests.swift
//  VIAUtilitiesTests
//
//  Created by Wilmar van Heerden on 2017/02/09.
//  Copyright © 2017 Glucode. All rights reserved.
//

import XCTest
@testable import VIAUtilities

// several failures exist as the supplied email regex isn't complete enough.  No action to be taken until client provides a better regex
class VIAUtilitiesTests: XCTestCase {
    func testIsAlphanumeric() {
        XCTAssertTrue("1234567890".isAlphanumeric)
        XCTAssertTrue("qwertyuiop".isAlphanumeric)
        XCTAssertTrue("asdfghjkl".isAlphanumeric)
        XCTAssertTrue("zxcvbnm".isAlphanumeric)
        XCTAssertTrue("1q2w3e4r5t6y7u8i9o0p".isAlphanumeric)
        
        XCTAssertFalse("`zxcvbnm1234567890".isAlphanumeric)
        XCTAssertFalse("_qwertyui1234567890".isAlphanumeric)
        XCTAssertFalse("|234567890".isAlphanumeric)
        XCTAssertFalse("123456.345678".isAlphanumeric)
        XCTAssertFalse("👎🏻".isAlphanumeric)
    }
    
    func testIsNumeric() {
        XCTAssertTrue("1234567890".isNumeric)
        
        XCTAssertFalse("1 2 3 4 5 6 7 8 9 0".isNumeric)
        XCTAssertFalse("qwertyuiop".isNumeric)
        XCTAssertFalse("wert6yuiop".isNumeric)
        XCTAssertFalse("12345t7890".isNumeric)
        XCTAssertFalse("123456789O".isNumeric)
        XCTAssertFalse("12345.67890".isNumeric)
        XCTAssertFalse("ThisIs👎🏻2".isNumeric)
        XCTAssertFalse("👎🏻".isNumeric)
    }
    
    func testLowerCase() {
        XCTAssertTrue("simon@glucode.com".isValidEmail())
        
        func test1() {
            XCTAssertTrue("james@smith.com".isValidEmail())
        }
        func test2() {
            XCTAssertTrue("james01@smith.com".isValidEmail())
        }
        func test3() {
            XCTAssertTrue("John@smith.com".isValidEmail())
        }
        func test4() {
            XCTAssertTrue("JSCAPS@smith.com".isValidEmail())
        }
        func test5() {
            XCTAssertTrue("1234567890@test.com".isValidEmail())
        }
        func test6() {
            XCTAssertTrue("JOHN01@smith.com".isValidEmail())
        }
        func test7() {
            XCTAssertTrue("John01@smith.com".isValidEmail())
        }
        func test8() {
            XCTAssertTrue("john+01@smith.com".isValidEmail())
        }
        func test9() {
            XCTAssertTrue("john#01@smth.com".isValidEmail())
        }
        func test10() {
            XCTAssertTrue("john#01@smth.com".isValidEmail())
        }
        func test11() {
            XCTAssertTrue("john$100@smith.com".isValidEmail())
        }
        func test12() {
            XCTAssertTrue("john%01@smith.com".isValidEmail())
        }
        func test13() {
            XCTAssertTrue("john&lt01@smith.com".isValidEmail())
        }
        func test14() {
            XCTAssertTrue("johnlt;01@smith.com".isValidEmail())
        }
        func test15() {
            XCTAssertTrue("john*01@smith.com".isValidEmail())
        }
        func test16() {
            XCTAssertTrue("john-01@smith.com".isValidEmail())
        }
        func test17() {
            XCTAssertTrue("john/01@smith.com".isValidEmail())
        }
        func test18() {
            XCTAssertTrue("john=01@smith.com".isValidEmail())
        }
        func test19() {
            XCTAssertTrue("john?01@smith.com".isValidEmail())
        }
        func test20() {
            XCTAssertTrue("john^01@smith.com".isValidEmail())
        }
        func test21() {
            XCTAssertTrue("john`01@smith.com".isValidEmail())
        }
        func test22() {
            XCTAssertTrue("john{01}@smith.com".isValidEmail())
        }
        func test23() {
            XCTAssertTrue("john|01@smith.com".isValidEmail())
        }
        func test24() {
            XCTAssertTrue("john~01@smith.com".isValidEmail())
        }
        func test25() {
            XCTAssertTrue("js@smith.com".isValidEmail())
        }
        // fail
        func test26() {
            XCTAssertFalse(".john@smith.com".isValidEmail())
        }
        // fail
        func test27() {
            XCTAssertFalse("john.@smith.com".isValidEmail())
        }
        // fail
        func test28() {
            XCTAssertFalse("john..jones@smith.com".isValidEmail())
        }
        // fail
        func test29() {
            XCTAssertTrue("\".john\"@smith.com".isValidEmail())
        }
        // fail
        func test30() {
            XCTAssertTrue("\"john.\"@smith.com".isValidEmail())
        }
        // fail
        func test31() {
            XCTAssertTrue("\"john..jones\"@smith.com".isValidEmail())
        }
        // fail
        func test32() {
            XCTAssertTrue("\"abcdefg\"@testing.com".isValidEmail())
        }
        // fail
        func test33() {
            XCTAssertTrue("abc.\"def\".ghi@testing.com".isValidEmail())
        }
        
        func test34() {
            XCTAssertFalse("\"abc\"\"def\"ghi@testing.com".isValidEmail())
        }
        func test35() {
            XCTAssertTrue("bugs.\"space jam\".bunny@wb.com".isValidEmail())
        }
        func test36() {
            XCTAssertTrue("ed4DZ3rS3Zul0KfnJFsLh9uyg5BhaoLxxEUzXuXNejL4pGnyvHbYqvKngh4mQSnT@test.com".isValidEmail())
        }
        func test37() {
            XCTAssertFalse("OuLIqoiXMxuOuMyyRMhTia3icDVJ3QZgb3h5nHpdxIzkhQfdGRwP8VHxPsvWDaZud@test.com".isValidEmail())
        }
        func test38() {
            XCTAssertTrue("john@smith.com".isValidEmail())
        }
        func test39() {
            XCTAssertTrue("john@SMITH.com".isValidEmail())
        }
        func test40() {
            XCTAssertTrue("john@12345.net".isValidEmail())
        }
        func test41() {
            XCTAssertTrue("john@Smith21.org".isValidEmail())
        }
        func test42() {
            XCTAssertTrue("".isValidEmail()) // no data from Brett
        }
        func test43() {
            XCTAssertTrue("".isValidEmail()) // no data from Brett
        }
        func test44() {
            XCTAssertFalse("xy@pxIxStlDKhjZiPulrHtAsyHWhrHtfjCpgNeUgAdMTkBLqSzkiRVwrldyEpETIhAYfBXiJXyUfXXzVNfeJsQDbrZtGdXQIQTfcajnJFwHtCeQmfCAxbKUQHcqvNgoPqvu.com".isValidEmail())
        }
        func test45() {
            XCTAssertTrue("user@mail.box.com".isValidEmail())
        }
        func test46() {
            XCTAssertTrue("user@domain.mail.box.com".isValidEmail())
        }
        func test47() {
            XCTAssertTrue("user@sub.domain.mail.box.com".isValidEmail())
        }
        func test48() {
            XCTAssertTrue("user@buried.sub.domain.mail.com".isValidEmail())
        }
        func test49() {
            XCTAssertTrue("ax@aQEnDJDSoIowLwSoDEjCNcmhJJvlgLSZHcmTPLAzJDuXPudvUjsjoZGiryRaKvh.tdyAnxLzIfbvQYxrfRsOwCyLVyoIyTaoarTZtmQQalYHoNcvIUphMETQunehtjZ.YIifpMmxhCItrVfvOUPokpHKMgKwimUsLpMhiajDEkNiHcbXVdwEzWfRUWNEfGS.hZKHoDQxUljYNcBEfEakGReRZeSPkCgIrEFozGmXOehfkuDbVQsMcnz.com".isValidEmail())
        }
        func test50() {
            XCTAssertTrue("user@mail-box.com".isValidEmail())
        }
        func test51() {
            XCTAssertFalse("user@-mailbox.com".isValidEmail())
        }
        func test52() {
            XCTAssertFalse("user@mailbox-.com".isValidEmail())
        }
        func test53() {
            XCTAssertTrue("jsmith@[192.168.2.1]".isValidEmail())
        }
        func test54() {
            XCTAssertTrue("jsmith@[IPv6:2001:db8::1]".isValidEmail())
        }
        func test55() {
            XCTAssertTrue("abc(comment)@abc.com".isValidEmail())
        }
        func test56() {
            XCTAssertTrue("abc@(comment)abc.com".isValidEmail())
        }
        func test57() {
            XCTAssertTrue("abc@abc(comment).com".isValidEmail())
        }
        func test58() {
            XCTAssertTrue("abc@testing.COM".isValidEmail())
        }
        func test59() {
            XCTAssertTrue("abc@testing.za".isValidEmail())
        }
        func test60() {
            XCTAssertTrue("user@test.XN--VERMGENSBERATUNG-PWB".isValidEmail())
        }
        func test61() {
            XCTAssertTrue("abc@testing.(comment)com".isValidEmail())
        }
        func test62() {
            XCTAssertTrue("abc@testing.com(comment)".isValidEmail())
        }
        
        // additional tests
        func test100() {
            XCTAssertTrue("日本人@日人日本人.com".isValidEmail())
        }
        
        // https://en.wikibooks.org/wiki/JavaScript/Best_practices#Examples_valid_according_to_RFC2822
    }
}
