//
//  VIASubtitleTableViewCell.swift
//  VitalityActive
//
//  Created by Wilmar van Heerden on 2016/11/14.
//  Copyright © 2016 Glucode. All rights reserved.
//

import UIKit

public class VIASubtitleTableViewCell: UITableViewCell, Nibloadable {

    @IBOutlet var headingLabel: UILabel!
    public var headingText: String? {
        set {
            self.headingLabel.text = newValue
        }
        get {
            return self.headingLabel.text
        }
    }

    public var attributedHeadingText: NSAttributedString? {
        set {
            self.headingLabel.attributedText = newValue
        }
        get {
            return self.headingLabel.attributedText
        }
    }

    @IBOutlet public var contentLabel: UILabel!
    public var contentText: String? {
        set {
            self.contentLabel.text = newValue
        }
        get {
            return self.contentLabel.text
        }
    }
    public var attributedContentText: NSAttributedString? {
        set {
            self.contentLabel.attributedText = newValue
        }
        get {
            return self.contentLabel.attributedText
        }
    }

    public func fontForContentLabel() -> UIFont {
        return contentLabel.font
    }
    
    public var customHeaderLabelFont: UIFont? {
        didSet {
            self.headingLabel.font = customHeaderLabelFont
        }
    }
    
    public var customContentLabelFont: UIFont? {
        didSet {
            self.contentLabel.font = customContentLabelFont
        }
    }
    
    public func setContentLabelColor(textColor: UIColor) {
        self.contentLabel.textColor = textColor
    }
    
    public override func awakeFromNib() {
        super.awakeFromNib()

        self.headingLabel.text = nil
        self.headingLabel.font = UIFont.title2Font()
        self.headingLabel.textColor = UIColor.night()
        self.headingLabel.numberOfLines = 0
        self.headingLabel.backgroundColor = self.contentView.backgroundColor

        self.contentLabel.text = nil
        self.contentLabel.font = UIFont.cellStyleSubtitleDetailTextLabel()
        self.contentLabel.textColor = UIColor.darkGrey()
        self.contentLabel.numberOfLines = 0
        self.contentLabel.backgroundColor = self.contentView.backgroundColor
    }
}
