//
//  ImageCollectionViewCell.swift
//  VitalityActive
//
//  Created by Marius Janse van Vuuren on 3/27/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

import UIKit

public class ImageCollectionViewCell: UICollectionViewCell, Nibloadable {
    
    @IBOutlet weak var smallImage: UIImageView!
    @IBOutlet weak var statusIndicatorImage: UIImageView!
    
    @IBOutlet public weak var imageView: UIImageView!
    public var selectionClosure:() -> Void = {}
    override public func awakeFromNib() {
        self.backgroundColor = UIColor.mercuryGrey()
        super.awakeFromNib()
    }
    
    public func setSmall(image: UIImage) {
        self.smallImage.image = image
    }
    
    public func setStatusIndicatorImage(statusImage: UIImage){
        self.statusIndicatorImage.image = statusImage
    }
    
    public func resetCell() {
        self.imageView.image = nil
        self.smallImage.image = nil
        self.statusIndicatorImage.image = nil
    }
}
