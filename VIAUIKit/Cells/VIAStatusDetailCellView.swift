//
//  VIAStatusDetailCellView.swift
//  VitalityActive
//
//  Created by Joshua Ryan on 9/5/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

import UIKit

public class VIAStatusDetailCellView: UITableViewCell, Nibloadable {

	@IBOutlet weak var statusImage: UIImageView!
	@IBOutlet weak var titleLabel: UILabel!
	@IBOutlet weak var descriptionLabel: UILabel!
	@IBOutlet weak var progressBar: UIView!
	@IBOutlet weak var progressWidth: NSLayoutConstraint!
	@IBOutlet weak var progressRangeBar: UIView!

	public var badge: UIImage? {
		set {
			self.statusImage.image = newValue
		}
		get {
			return self.statusImage.image
		}
	}

	public var title: String? {
		set {
			self.titleLabel.text = newValue
		}
		get {
			return self.titleLabel.text
		}
	}

	public var cellBody: String? {
		set {
			self.descriptionLabel.text = newValue
		}
		get {
			return self.descriptionLabel.text
		}
	}
    
    public func setProgress(as percentage: Float, of viewWidth: CGFloat) {
        progressWidth.constant = CGFloat(percentage) * (viewWidth - 60.0)
    }

	public override func awakeFromNib() {
		super.awakeFromNib()
		setupView()
	}

	func setupView() {

		self.titleLabel.textColor = UIColor.night()
		self.titleLabel.font = UIFont.title1Font()
		self.titleLabel.backgroundColor = .clear

		self.descriptionLabel.textColor = .darkGray
		self.descriptionLabel.font = UIFont.cellStyleSubtitleDetailTextLabel()
		self.descriptionLabel.backgroundColor = .clear

		self.progressBar.layer.cornerRadius = self.progressBar.frame.size.height / 2
		self.progressBar.layer.masksToBounds = true
		self.progressBar.backgroundColor = UIColor.primaryColor()

		self.progressRangeBar.layer.cornerRadius = self.progressRangeBar.frame.size.height / 2
		self.progressRangeBar.layer.masksToBounds = true
		self.progressRangeBar.backgroundColor = UIColor.tableViewBackground()
	}
}
