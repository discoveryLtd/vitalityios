import UIKit

public class ActivityDetailSubtitleCell: UITableViewCell, Nibloadable {

    @IBOutlet weak var stackView: UIStackView!

    @IBOutlet weak var headingLabel: UILabel!
    public var headingText: String? {
        set {
            self.headingLabel.text = newValue
        }
        get {
            return self.headingLabel.text
        }
    }


    @IBOutlet weak var contentLabel: UILabel!
    public var contentText: String? {
        set {
            self.contentLabel.text = newValue
            self.contentLabel.isHidden = newValue == nil
        }
        get {
            return self.contentLabel.text
        }
    }

    public override func prepareForReuse() {
        super.prepareForReuse()
        setupCell()
    }

    override public func awakeFromNib() {
        super.awakeFromNib()
        setupCell()
    }

    public func configureWithSmallHeadingAndLargeContent() {
        self.headingLabel.font = UIFont.footnoteFont()
        self.headingLabel.textColor = UIColor.mediumGrey()
        self.headingLabel.numberOfLines = 0
        self.headingLabel.backgroundColor = self.contentView.backgroundColor

        self.contentLabel.font = UIFont.bodyFont()
        self.contentLabel.textColor = UIColor.night()
        self.contentLabel.numberOfLines = 0
        self.contentLabel.backgroundColor = self.contentView.backgroundColor
    }

    private func setupCell() {
        self.headingLabel.text = nil
        self.headingLabel.font = UIFont.bodyFont()
        self.headingLabel.textColor = UIColor.night()
        self.headingLabel.numberOfLines = 0
        self.headingLabel.backgroundColor = self.contentView.backgroundColor

        self.contentLabel.isHidden = true
        self.contentLabel.text = nil
        self.contentLabel.font = UIFont.footnoteFont()
        self.contentLabel.textColor = UIColor.darkGrey()
        self.contentLabel.numberOfLines = 0
        self.contentLabel.backgroundColor = self.contentView.backgroundColor
    }

}
