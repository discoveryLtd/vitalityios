//
//  VIALabelTableViewCell.swift
//  VitalityActive
//
//  Created by admin on 2017/07/24.
//  Copyright © 2017 Glucode. All rights reserved.
//

import UIKit

public class VIALabelTableViewCell: UITableViewCell, Nibloadable {
    @IBOutlet weak var label: UILabel!
    public var labelText: String? {
        set {
            guard self.label != nil else { return }
            self.label.text = newValue
        }
        get {
            return self.label.text
        }
    }

    public override func awakeFromNib() {
        super.awakeFromNib()
        setupCell()
    }

    public override func prepareForReuse() {
        super.prepareForReuse()
        setupCell()
    }

    public func configure(font: UIFont, color: UIColor, textAlignment: NSTextAlignment = .left) {
        label.font = font
        label.textColor = color
        label.textAlignment = textAlignment
    }

    func setupCell() {
        self.label.text = nil
        self.label.textColor = UIColor.darkGrey()
        self.label.font = UIFont.cellStyleSubtitleDetailTextLabel()
        self.label.numberOfLines = 0
        self.label.backgroundColor = self.contentView.backgroundColor
    }
}
