//
//  VIAGenericContentCell.swift
//  VitalityActive
//
//  Created by Simon Stewart on 2016/11/15.
//  Copyright © 2016 Glucode. All rights reserved.
//
import UIKit
import TTTAttributedLabel

public final class VIAGenericContentCell: UITableViewCell, Nibloadable {

    @IBOutlet private weak var headerLabel: TTTAttributedLabel!
    @IBOutlet public weak var cellImageView: UIImageView!
    @IBOutlet private weak var contentLabel: TTTAttributedLabel!
    @IBOutlet private weak var parentStackView: UIStackView!
    @IBOutlet weak var contentStackViewWidth: NSLayoutConstraint!

    public var customHeaderFont: UIFont? {
        didSet {
            headerLabel.font = customHeaderFont
        }
    }

    public var customContentFont: UIFont? {
        didSet {
            contentLabel.font = customContentFont
        }
    }

    public var customContentTextColor: UIColor? {
        didSet {
            contentLabel.textColor = customContentTextColor
        }
    }

    public var cellImage: UIImage? {
        didSet {
            cellImageView.image = cellImage
            setNeedsUpdateConstraints()
            updateConstraintsIfNeeded()
        }
    }

    public var cellImageTintColor: UIColor? {
        didSet {
            cellImageView.tintColor = cellImageTintColor
        }
    }

    public var header: String = "" {
        didSet {
            headerLabel.text = header
        }
    }

    public var content: String = "" {
        didSet {
            contentLabel.text = content
        }
    }

    private func setupCell() {
        // clear IB labels
        cellImageView.image = nil
        headerLabel.text = nil
        contentLabel.text = nil

        // set image to hidden, incl resizing
        cellImage = nil

        // top align
        headerLabel.verticalAlignment = .top
        contentLabel.verticalAlignment = .top

        // layout
        headerLabel.font = UIFont.title2Font()
        headerLabel.textColor = UIColor.night()
        headerLabel.numberOfLines = 0

        contentLabel.font = UIFont.bodyFont()
        contentLabel.textColor = UIColor.night()
        contentLabel.numberOfLines = 0
    }

    public override func awakeFromNib() {
        super.awakeFromNib()
        setupCell()
    }

    public override func prepareForReuse() {
        super.prepareForReuse()
        setupCell()
    }

    public override func updateConstraints() {
        //give the stack view a width such that the multiline labels can layout correctly, this width will be overriden however a initial width is required to allow labels to corrctly layout
        contentStackViewWidth.constant = 1
        // override fonts if necessary
        if customHeaderFont != nil {
            headerLabel.font = customHeaderFont
        }
        if customContentFont != nil {
            contentLabel.font = customContentFont
        }

        cellImageView.isHidden = cellImage == nil
        parentStackView.spacing = cellImageView.isHidden ? 0 : CGFloat(12)

        super.updateConstraints()
    }
}
