import UIKit
import VitalityKit

public protocol DatePickerDelegate: class {
    func didPickDate(_ date: Date)
}

public class VIADatePickerTableViewCell: UITableViewCell, Nibloadable {

    // MARK: Outlets

    @IBOutlet weak var datePicker: UIDatePicker!

    // MARK: Properties
    public weak var delegate: DatePickerDelegate?
    public var addBottomBorder = false

    public var selectedDate: Date = Date() {
        didSet {
            self.datePicker.date = self.selectedDate
        }
    }

    public var maximumDate: Date? {
        get {
            return self.datePicker.maximumDate
        }
        set {
            self.datePicker.maximumDate = newValue
        }
    }

    public var minimumDate: Date? {
        get {
            return self.datePicker.minimumDate
        }
        set {
            self.datePicker.minimumDate = newValue
        }
    }

    public var didPickDate: ((_ date: Date) -> Void)?

    // MARK: Init

    override public func awakeFromNib() {
        super.awakeFromNib()

        self.backgroundColor = UIColor.day()
        self.datePicker.backgroundColor = UIColor.day()

        self.datePicker.datePickerMode = .date
        self.datePicker.addTarget(self, action: #selector(datePickerDidPickDate(_:)), for: .valueChanged)
        self.datePicker.maximumDate = Date()
        
        /* FC-24455 : DE
         * Prevents the overlapping issue on Capture S&V when scrolled down and then back to top.
         */
        if let tenantID = AppSettings.getAppTenant(), tenantID == .GI {
            setDatePickerConstraints()
        }
    }

    override public func prepareForReuse() {
        super.prepareForReuse()

        self.datePicker.date = Date()
    }

    override public func layoutSubviews() {
        super.layoutSubviews()

        contentView.layer.removeAllBorderLayers()
        if addBottomBorder {
            contentView.layer.addBorder(edge: .bottom)
        }
    }

    // MARK: DatePicker

    public func datePickerDidPickDate(_ datePicker: UIDatePicker) {
        delegate?.didPickDate(datePicker.date)
    }

    override public func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setDatePickerConstraints() {
        datePicker.translatesAutoresizingMaskIntoConstraints = false
        /* Set the "DatePicker" constraints to superview */
        let attributes: [NSLayoutAttribute] = [.top, .bottom, .right, .left]
        NSLayoutConstraint.activate(attributes.map {
            NSLayoutConstraint(item: datePicker, attribute: $0, relatedBy: .equal, toItem: datePicker.superview, attribute: $0, multiplier: 1, constant: 0)
        })
    }

}
