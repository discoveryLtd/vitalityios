//
//  MLILoginTableViewCell.swift
//  Manulife
//
//  Created by Michelle R. Oratil on 16/01/2019.
//  Copyright © 2019 Glucode. All rights reserved.
//

import UIKit

public class MLILoginTableViewCell: UITableViewCell, Nibloadable {
    
    @IBOutlet weak var headerTitleLabel: UILabel!
    public var headerTitle: String?{
        set{
            self.headerTitleLabel.text = newValue
        }
        get{
            return self.headerTitleLabel.text
        }
    }
    
    @IBOutlet weak var headerDescriptionLabel: UILabel!
    public var headerDescription: String? {
        set {
            self.headerDescriptionLabel.text = newValue
        }
        get {
            return self.headerDescriptionLabel.text
        }
    }
    
    override public func awakeFromNib() {
        headerTitleLabel.text = nil
        headerTitleLabel.textColor = UIColor.night()
        
        headerDescriptionLabel.text = nil
        headerDescriptionLabel.font = UIFont.systemFont(ofSize: 14)
        headerDescriptionLabel.textColor = UIColor.darkGrey()
        
    }
    
    override public func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
}
