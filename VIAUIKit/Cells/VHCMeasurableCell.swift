import UIKit
import SnapKit
import VIAUtilities

public struct VHCFeedback {
    public let image: UIImage
    public let description: String
    public let explanation: String?
    public let isInHealthyRange: Bool?

    public init(image: UIImage, description: String, explanation: String? = nil, isInHealthyRange: Bool? = false) {
        self.image = image
        self.description = description
        self.explanation = explanation
        self.isInHealthyRange = isInHealthyRange
    }
}

public class VHCMeasurableCell: UITableViewCell, Nibloadable {

    @IBOutlet weak var feedbackStackView: UIStackView!


    @IBOutlet weak var messageStackView: UIStackView!

    @IBOutlet weak var contentStackView: UIStackView!

    @IBOutlet weak var titleMeasurementStackView: UIStackView!

    @IBOutlet weak var arrowIcon: UIImageView!
    public var arrowImage: UIImage? {
        set {
            arrowIcon.image = newValue
            arrowIcon.isHidden = newValue == nil
        }
        get {
            return self.arrowIcon.image
        }
    }

    @IBOutlet weak var messageIcon: UIImageView!
    public var messageImage: UIImage? {
        set {
            messageIcon.image = newValue
            messageIcon.isHidden = newValue == nil
        }
        get {
            return self.messageIcon.image
        }
    }


    @IBOutlet public weak var measurableLogo: UIImageView!
    public var measurableImage: UIImage? {
        set {
            measurableLogo.image = newValue
            measurableLogo.isHidden = newValue == nil
        }
        get {
            return self.measurableLogo.image
        }
    }

    @IBOutlet weak var measurableTitleLabel: UILabel!
    public var measurableTitleText: String? {
        set {
            measurableTitleLabel.text = newValue
            measurableTitleLabel.isHidden = newValue == nil
        }
        get {
            return measurableTitleLabel.text
        }
    }

    @IBOutlet weak var messageLabel: UILabel!
    public var messageText: String? {
        set {
            messageLabel.text = newValue
            messageLabel.isHidden = newValue == nil
            messageStackView.isHidden = messageLabel.isHidden
        }
        get {
            return messageLabel.text
        }
    }

    @IBOutlet weak var footerMessageLabel: UILabel!
    public var footerMessageText: String? {
        set {
            footerMessageLabel.text = newValue
            footerMessageLabel.isHidden = newValue == nil
        }
        get {
            return footerMessageLabel.text
        }
    }

    public override func prepareForReuse() {
        super.prepareForReuse()
        setupCell()
    }

    override public func awakeFromNib() {
        super.awakeFromNib()
        setupCell()
    }

    private func setupCell() {
        arrowIcon.image = nil
        measurableLogo.image = nil
        measurableTitleLabel.text = nil
        messageLabel.text = nil
        footerMessageLabel.text = nil
        for view in feedbackStackView.subviews {
            view.removeFromSuperview()
        }

        messageStackView.isHidden = true
        feedbackStackView.isHidden = true
        arrowIcon.isHidden = true
        measurableLogo.isHidden = true
        measurableTitleLabel.isHidden = true
        messageLabel.isHidden = true
        footerMessageLabel.isHidden = true


        measurableTitleLabel.font = UIFont.headlineFont()
        messageLabel.font = .subheadlineFont()
        footerMessageLabel.font = .footnoteFont()

        measurableLogo.tintColor = UIColor.currentGlobalTintColor()
        messageIcon.tintColor = .mediumGrey()
        messageLabel.textColor = .darkGrey()
        measurableTitleLabel.textColor = .night()
        footerMessageLabel.textColor = .mediumGrey()

    }

    public func configureVHCMeasurableMainCell(image: UIImage?, measurableTitle: String, message: String?, feedback: [VHCFeedback]?, footerMessage: String?, isOutOfCurrentMembershipPeriod: Bool, isDisclosureIndicatorHidden: Bool) {
        measurableImage = image
        measurableTitleText = measurableTitle
        messageText = message
        footerMessageText = footerMessage
        arrowImage = VIAUIKitAsset.arrowDrill.image
        arrowIcon.isHidden = isDisclosureIndicatorHidden
        if isOutOfCurrentMembershipPeriod {
            makeCellGrey()
        }
        if let measurableFeedback = feedback {
            add(feedback: measurableFeedback, makeFeedbackMediumGrey: isOutOfCurrentMembershipPeriod)
        }
    }

    public func configureVHCMeasurableNoDataCell(image: UIImage?, measurableTitle: String, messageIcon: UIImage?, message: String?, feedback: [VHCFeedback]?, footerMessage: String?, isDisclosureIndicatorHidden: Bool) {
        makeCellGrey()
        measurableImage = image
        measurableTitleText = measurableTitle
        messageText = message
        messageImage = messageIcon
        footerMessageText = footerMessage
        arrowImage = VIAUIKitAsset.arrowDrill.image
        arrowIcon.isHidden = isDisclosureIndicatorHidden
        if let measurableFeedback = feedback {
            add(feedback: measurableFeedback, makeFeedbackMediumGrey: true)
        }
    }

    func makeCellGrey() {
        messageIcon.tintColor = .mediumGrey()
        measurableLogo.tintColor = .lightGrey()
        measurableTitleLabel.textColor = .darkGrey()
        messageLabel.textColor = .darkGrey()
        footerMessageLabel.textColor = .darkGrey()
    }

    public func add(feedback: [VHCFeedback], makeFeedbackMediumGrey: Bool) {
        guard feedback.count > 0 else { return }
        feedbackStackView.isHidden = false
        for measurableFeedback in feedback {
            let stackView = UIStackView()
            let imageView = UIImageView()
            let label = UILabel()
            stackView.spacing = 5
            stackView.axis = UILayoutConstraintAxis.horizontal
            stackView.distribution = UIStackViewDistribution.fill
            stackView.alignment = UIStackViewAlignment.fill
            imageView.contentMode = UIViewContentMode.top
            imageView.image = measurableFeedback.image
            imageView.tintColor = .mediumGrey()
            label.textColor = .darkGrey()

            stackView.addArrangedSubview(imageView)
            stackView.addArrangedSubview(label)

            imageView.snp.makeConstraints({(make) -> Void in
                make.height.equalTo(label.snp.height)
                make.width.equalTo(15.0)
            })

            label.text = measurableFeedback.description
            label.font = .footnoteFont()
            label.numberOfLines = 0
            label.setContentHuggingPriority(1000, for: .vertical)
            label.setContentCompressionResistancePriority(1000, for: .vertical)

            feedbackStackView.addArrangedSubview(stackView)
            stackView.snp.makeConstraints({(make) -> Void in
                make.right.equalToSuperview()
                make.left.equalToSuperview()
            })
        }
    }
}
