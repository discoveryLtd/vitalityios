//
//  ImageCollectionTableViewCell.swift
//  VitalityActive
//
//  Created by Marius Janse van Vuuren on 4/19/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

import UIKit


public class ImageCollectionTableViewCell: UITableViewCell, Nibloadable {
    public var viewWidth: CGFloat = 0.0
    public var imageCollection: Array<UIImage>?
    public var imageCollectionView: ImageCollection?

    override public func awakeFromNib() {
        super.awakeFromNib()
    }

    override public func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    public func setImageCollection(images: Array<UIImage>) {
        self.imageCollection = images
    }

    public func setupCollectionFlowLayoutSize() {
        setupImageCollection()
        self.imageCollectionView?.imagesAssets = self.imageCollection
        self.imageCollectionView?.register(ImageCollectionViewCell.nib(), forCellWithReuseIdentifier: ImageCollectionViewCell.defaultReuseIdentifier)
        self.imageCollectionView?.setImageCollectionItemWidth(with: viewWidth)
        self.imageCollectionView?.isScrollEnabled = false
    }

    public func setupImageCollection() {
        self.imageCollectionView = ImageCollection.viewFromNib(owner: self) as? ImageCollection
        self.addSubview(self.imageCollectionView!)
        // Set initial width avoiding sizing issues for larger screens
        self.imageCollectionView?.frame = CGRect(x: 0, y: 0, width: self.viewWidth, height: self.frame.height)
        addConstraintToImageCollection(on: .centerX)
        addConstraintToImageCollection(on: .centerY)
        addConstraintToImageCollection(on: .width)
        addConstraintToImageCollection(on: .height)
    }

    public func addConstraintToImageCollection(on relation: NSLayoutAttribute) {
        let constraint = NSLayoutConstraint(item: self.imageCollectionView!,
                               attribute: relation,
                               relatedBy: NSLayoutRelation.equal,
                               toItem: self,
                               attribute: relation,
                               multiplier: 1,
                               constant: 0)
        self.addConstraint(constraint)
    }

    public func updateImageCollectionViewSize(with height: CGFloat?) {
        //Update height to size collection view equal to cell height which should be the same as the collection view content height 
        self.imageCollectionView?.frame = CGRect(x: 0, y: 0, width: self.viewWidth, height: height ?? 0)
    }
}
