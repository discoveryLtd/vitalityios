//
//  OFELastClaimedEventCell.swift
//  VitalityActive
//
//  Created by Val Tomol on 03/05/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import UIKit

public class OFELastClaimedEventCell: UITableViewCell, Nibloadable {
    @IBOutlet weak var cellTitle: UILabel!
    @IBOutlet weak var cellValue: UILabel!
    @IBOutlet weak var cellSubtitle: UILabel!
    
    override public func awakeFromNib() {
        super.awakeFromNib()
        self.setupCellStyling()
    }
    
    override public func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    public func setCell(title: String) {
        self.cellTitle.text = title
    }
    
    public func setCell(value: String) {
        self.cellValue.text = value
    }
    
    public func setCell(subTitle: String) {
        self.cellSubtitle.text = subTitle
    }
    
    func setupCellStyling() {
        self.cellTitle.font = UIFont.title2Font()
        self.cellTitle.textColor = UIColor.night()
        
        self.cellValue.font = UIFont.bodyFont()
        self.cellValue.textColor = UIColor.night()
        
        self.cellSubtitle.font = UIFont.subheadlineFont()
        self.cellSubtitle.textColor = UIColor.mediumGrey()
    }
}
