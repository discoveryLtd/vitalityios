//
//  SAVDatePickerCell.swift
//  VitalityActive
//
//  Created by OJ Garde on 2/8/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import UIKit

public class SVDatePickerCell: UITableViewCell, Nibloadable {
    
    
    @IBOutlet public weak var dateLabel: UILabel!
    @IBOutlet public weak var testedDateLabel: UILabel!
    
    public var testedDate: String? {
        set {
            testedDateLabel.text = newValue
            testedDateLabel.isHidden = newValue == nil
        }
        get {
            return self.testedDateLabel.text
        }
    }
}
