//
//  VIAEventsFeedCell.swift
//  VitalityActive
//
//  Created by OJ Garde on 10/11/2017.
//  Copyright © 2017 Glucode. All rights reserved.
//

import UIKit

public class VIAEventsFeedCell: UITableViewCell, Nibloadable {
    
    @IBOutlet weak var eventImageView: UIImageView!
    public var eventImage: UIImage? {
        set {
            eventImageView.image = newValue
            eventImageView.isHidden = newValue == nil
        }
        get {
            return self.eventImageView.image
        }
    }
    
    @IBOutlet weak var eventCategoryLabel: UILabel!
    public var eventCategory: String? {
        set {
            eventCategoryLabel.text = newValue
            if newValue == nil || eventCategory == "" {
                eventCategoryLabel.isHidden = true
            } else {
                eventCategoryLabel.isHidden = false
            }
        }
        get {
            return eventCategoryLabel.text
        }
    }
    
    @IBOutlet weak var eventMessageLabel: UILabel!
    public var eventMessage: String? {
        set {
            eventMessageLabel.text = newValue
            if newValue == nil || eventMessage == "" {
                eventMessageLabel.isHidden = true
            } else {
                eventMessageLabel.isHidden = false
            }
        }
        get {
            return eventMessageLabel.text
        }
    }
    
    
    public override func prepareForReuse() {
        super.prepareForReuse()
        setupCell()
    }
    
    override public func awakeFromNib() {
        super.awakeFromNib()
        setupCell()
    }
}

extension VIAEventsFeedCell{
    
    fileprivate func setupCell() {
        eventImageView.image = nil
        eventCategoryLabel.text = nil
        eventMessageLabel.text = nil
    }
    
    
}
