//
//  MWBLandingSectionCell.swift
//  VitalityActive
//
//  Created by Harry C. Lingad on 2/6/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import UIKit

public class MWBLandingSectionCell: UITableViewCell, Nibloadable {
    
    
    @IBOutlet weak var feedbackStackView: UIStackView!
    
    @IBOutlet weak var titleLabel: UILabel!
    public var titleText: String? {
        set {
            titleLabel.text = newValue
            titleLabel.isHidden = newValue == nil
        }
        get {
            return titleLabel.text
        }
    }
    
    @IBOutlet weak var potentialPointsLabel: UILabel!
    public var potentialPointsText: String? {
        set {
            potentialPointsLabel.text = newValue
            potentialPointsLabel.isHidden = newValue == nil
            guard newValue?.isEmpty == false else { feedbackStackView.isHidden = true; return }
            feedbackStackView.isHidden = potentialPointsLabel.isHidden
        }
        get {
            return potentialPointsLabel.text
        }
    }
    
    
    @IBOutlet weak var feedbackLabel: UILabel!
    public var feedbackText: String? {
        set {
            feedbackLabel.text = newValue
            feedbackLabel.isHidden = newValue == nil
            guard newValue?.isEmpty == false else { feedbackStackView.isHidden = true; return }
            feedbackStackView.isHidden = feedbackLabel.isHidden
        }
        get {
            return feedbackLabel.text
        }
    }
    
    //earn points
    @IBOutlet weak var feedbackIcon: UIImageView!
    public var feedbackImage: UIImage? {
        set {
            feedbackIcon.image = newValue
            feedbackIcon.isHidden = newValue == nil
        }
        get {
            return self.feedbackIcon.image
        }
    }
    
    @IBOutlet weak var completionIcon: UIImageView!
    public var completionImage: UIImage? {
        set {
            completionIcon.image = newValue
            completionIcon.isHidden = newValue == nil
        }
        get {
            return self.completionIcon.image
        }
    }
    
    
    @IBOutlet public weak var sectionButton: BorderButton!
    
    public override func prepareForReuse() {
        super.prepareForReuse()
        setupCell()
    }
    
    override public func awakeFromNib() {
        super.awakeFromNib()
        setupCell()
    }
    
    private func setupCell() {
        titleLabel.text = nil
        feedbackLabel.text = nil
        feedbackIcon.image = nil
        
        titleLabel.font = .headlineFont()
        feedbackLabel.font = .footnoteFont()
        titleLabel.textColor = .night()
        feedbackLabel.textColor = .darkGrey()
        feedbackIcon.tintColor = .mediumGrey()
        
        sectionButton.setupButton()
        
    }
    
    public func configureCompleteMWBLandingSectionCell(title: String, feedback: String, buttonText: String, completeIcon: UIImage?) {
        titleText = title
        feedbackText = feedback
        feedbackImage = completeIcon
        
        titleLabel.textColor = .darkGrey()
        sectionButton.setTitle(buttonText, for: .normal)
        sectionButton.setTitleColor(UIColor.mediumGrey(), for: .normal)
        sectionButton.layer.borderColor = UIColor.mediumGrey().cgColor
        sectionButton.tintColor = .mediumGrey()
        sectionButton.sizeToFit()
    }
    
    public func configureIncompleteMWBLandingSectionCell(title: String, feedback: String, buttonText: String) {
        titleText = title
        feedbackText = feedback
        feedbackImage = nil
        
        titleLabel.textColor = .night()
        sectionButton.setTitle(buttonText, for: .normal)
        sectionButton.tintColor = UIColor.currentGlobalTintColor()
        sectionButton.layer.borderColor = UIColor.currentGlobalTintColor().cgColor
        sectionButton.sizeToFit()
    }

}
