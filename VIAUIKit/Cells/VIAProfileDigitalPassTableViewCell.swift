//
//  VIAProfileDigitalPassTableViewCell.swift
//  VitalityActive
//
//  Created by Ma. Catherine Purganan on 16/11/2017.
//  Copyright © 2017 Glucode. All rights reserved.
//

import UIKit
import PassKit

public class VIAProfileDigitalPassTableViewCell: UITableViewCell, Nibloadable {

    @IBOutlet weak var label: UILabel!
    let pkButton = PKAddPassButton()
    
    public var digitalPassDescription: String? {
        set {
            guard self.label != nil else { return }
            self.label.text = newValue
        } get {
            return self.label.text
        }
    }
    
    public var hidePKAddPassButton: Bool{
        set{
            pkButton.isHidden = newValue
        }
        get{
            return pkButton.isHidden
        }
    }
    
    
    override public func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        setupCell()
    }

    override public func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupCell() {
        self.addSubview(pkButton)
        let scale = CGFloat(floatLiteral: 0.75)
        pkButton.transform = CGAffineTransform(scaleX: scale, y: scale)
        pkButton.isEnabled = false
        
        
        self.label.text = nil
        self.label.textColor = UIColor.night()
        self.label.font = UIFont.cellStyleMenuItemTextLabel()
        self.label.numberOfLines = 0
        self.label.backgroundColor = self.contentView.backgroundColor
    }

    
}
