//
//  VIATextFieldTableViewCell.swift
//  VitalityActive
//
//  Created by Wilmar van Heerden on 2016/09/16.
//  Copyright © 2016 Glucode. All rights reserved.
//

import UIKit
import TTTAttributedLabel

public struct VIATextFieldCellImageConfig {
    public var templateImage: UIImage?
    public var inactiveTintColor: UIColor = UIColor.genericInactive()
    public var activeTintColor: UIColor = UIColor.genericActive()
    public var errorTintColor: UIColor = UIColor.genericError()

    public init() {}
}

public class VIATextFieldTableViewCell: UITableViewCell, Nibloadable, UITextFieldDelegate {

    let defaultPadding: CGFloat = 10
    var isSecure: Bool = false
    public var isDOBField: Bool = false
    
    // MARK: Outlets
    
    @IBOutlet weak var leftImageView: UIImageView!
    @IBOutlet weak var cellImageView: UIImageView!
    @IBOutlet weak var headingLabel: TTTAttributedLabel!
    @IBOutlet public var textField: VIATextField!
    @IBOutlet weak var showHideButton: UIButton!
    @IBOutlet weak var errorLabel: TTTAttributedLabel!
    @IBOutlet weak var errorStackView: UIStackView!
    @IBOutlet weak var errorLine: UIView!
    @IBOutlet weak var mainStackViewLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var mainStackViewTrailingConstraint: NSLayoutConstraint!
    @IBOutlet weak var imageViewWidthConstraint: NSLayoutConstraint!
    
    // MARK: Closures
    
    public var textFieldShouldBeginEditing: ((_ textField: UITextField) -> Bool)?
    public var textFieldDidBeginEditing: ((_ textField: UITextField) -> Void)?
    public var textFieldShouldEndEditing: ((_ textField: UITextField) -> Bool)?
    public var textFieldDidEndEditing: ((_ textField: UITextField) -> Void)?
    public var textFieldValidate: ((_ textField: UITextField) -> Bool)?
    public var textFieldTextDidChange: ((_ textField: UITextField) -> Void)?
    public var textFieldShouldReturn: ((_ textField: UITextField) -> Bool)?
    
    // MARK: Setters
    
    public var leftImage: UIImage?{
        didSet{
            self.leftImageView.isHidden = leftImage == nil
            self.leftImageView.image = leftImage
            self.leftImageView.isUserInteractionEnabled = true
        }
    }
    
    public var touchIdIconGestureRecognizer: UITapGestureRecognizer?{
        didSet{
            if let gesture = touchIdIconGestureRecognizer{
                self.leftImageView.addGestureRecognizer(gesture)
            }
        }
    }
    
    
    public var cellImageConfig: VIATextFieldCellImageConfig = VIATextFieldCellImageConfig() {
        didSet {
            self.cellImageView.image = cellImageConfig.templateImage
            self.updateCellImageView()
        }
    }
    
    public var extendsEdgesToContentView: Bool = false {
        didSet {
            self.mainStackViewLeadingConstraint.constant = self.extendsEdgesToContentView ? 0 : defaultPadding
            self.mainStackViewTrailingConstraint.constant = self.extendsEdgesToContentView ? 0 : defaultPadding
            self.setNeedsLayout()
            self.layoutIfNeeded()
        }
    }
    
    public func setTextFieldPlaceholder(placeholder: String?) {
        guard let placeholderString = placeholder as String! else {
            self.textField.attributedPlaceholder = nil
            return
        }
        
        let attributes = [NSForegroundColorAttributeName: UIColor.textFieldPlaceholder(), NSFontAttributeName: UIFont.textFieldPlaceholder()]
        let attributedString = NSAttributedString(string: placeholderString, attributes: attributes)
        self.textField.attributedPlaceholder = attributedString
    }
    
    public func setTextFieldText(text: String?) {
        self.textField.text = text
        self.updateCellImageView()
    }
    
    public func setTextFieldKeyboardType(type: UIKeyboardType) {
        self.textField.keyboardType = type
    }
    
    public func setTextFieldEnabled(state: Bool) {
        self.textField.isEnabled = state
    }
    
    public func setTextFieldSecureTextEntry(secureTextEntry: Bool) {
        self.isSecure = secureTextEntry
        self.showHideButton.isHidden = !secureTextEntry
        self.setSecureText(toVisible: !secureTextEntry)
    }
    
    public func setSecureField(asPreviouslyVisible previouslyVisible: Bool) {
        self.setSecureText(toVisible: previouslyVisible)
    }
    
    public func setHeadingLabelText(text: String?) {
        self.headingLabel.isHidden = text == nil
        self.headingLabel.setText(text)
    }
    
    public func hideCellImage() {
        self.cellImageView.isHidden = true
        self.imageViewWidthConstraint.constant = CGFloat.leastNormalMagnitude
        self.setNeedsLayout()
        self.layoutIfNeeded()
    }
    
    // MARK: Methods
    
    public override func awakeFromNib() {
        super.awakeFromNib()
        
        self.leftImageView.isHidden = true
        self.leftImageView.tintColor = UIColor.primaryColor()
        self.leftImageView.backgroundColor = self.contentView.backgroundColor
        self.leftImageView.contentMode = .scaleAspectFit
        self.cellImageView.backgroundColor = self.contentView.backgroundColor
        self.headingLabel.font = UIFont.cellHeadingLabel()
        self.headingLabel.verticalAlignment = .bottom
        self.headingLabel.textColor = UIColor.cellHeadingLabel()
        self.headingLabel.backgroundColor = UIColor.day()
        self.textField.textColor = UIColor.night()
        self.textField.font = UIFont.textField()
        self.textField.backgroundColor = UIColor.day()
        self.showHideButton.isHidden = true
        self.errorLabel.font = UIFont.cellErrorLabel()
        self.errorLabel.textColor = UIColor.cellErrorLabel()
        self.errorLabel.backgroundColor = UIColor.day()
        self.errorLine.backgroundColor = UIColor.genericError()
        
        self.textField.delegate = self
        self.textField.addTarget(self, action: #selector(textFieldEditingChanged(_:)), for: .editingChanged)
        self.textField.keyboardType = .default
    }
    
    // MARK: Getters
    
    public override var canBecomeFirstResponder: Bool {
        return self.textField.canBecomeFirstResponder
    }
    
    public override func becomeFirstResponder() -> Bool {
        return self.textField.becomeFirstResponder()
    }
    
    // MARK: Error label handing
    public func setErrorMessage(message: String?) {
        self.errorStackView.isHidden = message == nil
        self.errorLabel.setText(message)
        self.updateCellImageView()
    }
    
    // MARK: UITextField delegate
    
    public func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        guard self.textFieldShouldBeginEditing != nil else { return true }
        return self.textFieldShouldBeginEditing(textField)
    }
    
    public func textFieldDidBeginEditing(_ textField: UITextField) {
        guard self.textFieldDidBeginEditing != nil else { return }
        self.textFieldDidBeginEditing!(textField)
    }
    
    public func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        guard self.textFieldShouldEndEditing != nil else { return true }
        return self.textFieldShouldEndEditing(textField)
    }
    
    public func textFieldDidEndEditing(_ textField: UITextField) {
        guard self.textFieldDidEndEditing != nil else { return }
        self.textFieldDidEndEditing!(textField)
        self.updateCellImageView()
    }
    
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        guard self.textFieldShouldReturn != nil else { return true }
        textField.resignFirstResponder()
        return true
    }
    
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        // Perform only when fextfield is 'dateofbirth' from registration.
        if self.isDOBField {
            // Place '/' every after 4th and 7th character: YYYY/MM/DD.
            if (textField.text?.count == 4) || (textField.text?.count == 7) {
                // Handle backspace.
                if !(string == "") {
                    if let text = textField.text {
                        textField.text = text + "/"
                    }
                }
            }
            
            // Limit text length to 10 characters.
            if let text = textField.text {
                return !(text.count > 9 && (string.count) > range.length)
            }
        }
        
        //Overridden because native Apple rules does not allow persistent texts when editing secured text fields.
        
        let nsString:NSString? = textField.text as NSString?
        let updatedString = nsString?.replacingCharacters(in:range, with:string);
        
        textField.text = updatedString;
        
        //Setting the cursor at the right place
        let selectedRange = NSMakeRange(range.location + string.characters.count, 0)
        let from = textField.position(from: textField.beginningOfDocument, offset:selectedRange.location)
        let to = textField.position(from: from!, offset:selectedRange.length)
        textField.selectedTextRange = textField.textRange(from: from!, to: to!)
        
        //Sending an action
        textField.sendActions(for: UIControlEvents.editingChanged)
        
        return false;
    }
    
    func textFieldEditingChanged(_ textField: UITextField) {
        self.updateCellImageView()
        
        guard self.textFieldTextDidChange != nil else { return }
        self.textFieldTextDidChange!(textField)
    }
    
    // MARK: Actions
    
    func updateCellImageView() {
        if self.errorStackView.isHidden {
            if self.textField.text?.characters.count == 0 {
                self.cellImageView.tintColor = self.cellImageConfig.inactiveTintColor
            } else {
                self.cellImageView.tintColor = self.cellImageConfig.activeTintColor
            }
        } else {
            self.cellImageView.tintColor = self.cellImageConfig.errorTintColor
        }
    }
    
    
    @IBAction func showHideButtonTapped(_ sender: UIButton) {
        guard self.isSecure else { return }
        self.toggleSecureTextEntry()
    }
    
    func toggleSecureTextEntry() {
        self.setSecureText(toVisible: !self.showHideButton.isSelected)
    }
    
    func setSecureText(toVisible visible: Bool) {
        self.textField.isSecureTextEntry = !visible
        
        if let textRange = self.textField.textRange(from: self.textField.beginningOfDocument, to: self.textField.endOfDocument) {
            self.textField.replace(textRange, withText: self.textField.text!)
        }
        self.showHideButton.isSelected = visible
    }
}
