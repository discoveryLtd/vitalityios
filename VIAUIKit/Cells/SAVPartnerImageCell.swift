//
//  SAVPartnerImageCell.swift
//  VitalityActive
//
//  Created by Steven Layug on 11/20/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
//
//  SAVParticipatingPartnerCell.swift
//  VitalityActive
//∫
//  Created by Steven F. Layug on 2017/11/16.
//  Copyright © 2016 Glucode. All rights reserved.
//
import UIKit

public final class SAVPartnerImageCell: UITableViewCell, Nibloadable {
    
    @IBOutlet public weak var headerLabel: UILabel!
    @IBOutlet public weak var cellImageView: UIImageView!
    
    public var customHeaderFont: UIFont? {
        didSet {
            headerLabel.font = customHeaderFont
        }
    }
    
    public var cellImage: UIImage? {
        didSet {
            cellImageView.image = cellImage
            setNeedsUpdateConstraints()
            updateConstraintsIfNeeded()
        }
    }
    
    public var cellImageTintColor: UIColor? {
        didSet {
            cellImageView.tintColor = cellImageTintColor
        }
    }
    
    public var header: String = "" {
        didSet {
            headerLabel.text = header
        }
    }
    
    private func setupCell() {
        // clear IB labels
        cellImageView.image = nil
        headerLabel.text = nil
        //contentLabel.text = nil
        
        // set image to hidden, incl resizing
        cellImage = nil
        
        // layout
        headerLabel.font = UIFont.headlineFont()
        headerLabel.textColor = UIColor.night()
        headerLabel.numberOfLines = 0
    }
    
    public override func awakeFromNib() {
        super.awakeFromNib()
        setupCell()
    }
    
    public override func prepareForReuse() {
        super.prepareForReuse()
        setupCell()
    }
    
}
