//
//  MLILearnMoreTableViewCell.swift
//  VIAUIKit
//
//  Created by Michelle R. Oratil on 23/01/2019.
//  Copyright © 2019 Glucode. All rights reserved.
//

import UIKit

public class MLILearnMoreTableViewCell: UITableViewCell, Nibloadable {

    
    @IBOutlet weak var headerLabel: UILabel!
    public var headerTitle: String?{
        set{
            self.headerLabel.text = newValue
        }
        get{
            return self.headerLabel.text
        }
    }
    
    @IBOutlet weak var detailLabel: UILabel!
    public var detailTitle: String?{
        set{
            self.detailLabel.text = newValue
        }
        get{
            return self.detailLabel.text
        }
    }
    
    
    override public func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override public func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
