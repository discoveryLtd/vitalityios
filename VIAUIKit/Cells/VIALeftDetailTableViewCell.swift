//
//  VIALeftDetailTableViewCell.swift
//  VitalityActive
//
//  Created by OJ Garde on 11/11/2017.
//  Copyright © 2017 Glucode. All rights reserved.
//

import UIKit

public class VIALeftDetailTableViewCell: UITableViewCell, Nibloadable {
    
    @IBOutlet weak var titleLabel: UILabel!
    public var titleText: String? {
        set {
            titleLabel.text = newValue
            if newValue == nil || titleText == "" {
                titleLabel.isHidden = true
            } else {
                titleLabel.isHidden = false
            }
        }
        get {
            return titleLabel.text
        }
    }
    
    
    @IBOutlet weak var valueLabel: UILabel!
    public var valueText: String? {
        set {
            valueLabel.text = newValue
            if newValue == nil || valueText == "" {
                valueLabel.isHidden = true
            } else {
                valueLabel.isHidden = false
            }
        }
        get {
            return valueLabel.text
        }
    }
    
    override public func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
}
