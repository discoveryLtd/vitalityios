//
//  VIAStatusAnnualDetailCellView.swift
//  VitalityActive
//
//  Created by Joshua Ryan on 9/5/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

import UIKit

public class VIAStatusAnnualDetailCellView: UITableViewCell, Nibloadable {

	@IBOutlet weak var titleLabel: UILabel!
	@IBOutlet weak var descriptionLabel: UILabel!
	@IBOutlet weak var pointsLabel: UILabel!

	public var title: String? {
		set {
			self.titleLabel.text = newValue
		}
		get {
			return self.titleLabel.text
		}
	}

	public var cellBody: String? {
		set {
			self.descriptionLabel.text = newValue
		}
		get {
			return self.descriptionLabel.text
		}
	}

	public var points: String? {
		set {
			self.pointsLabel.text = newValue
		}
		get {
			return self.pointsLabel.text
		}
	}

	public override func awakeFromNib() {
		super.awakeFromNib()
		setupView()
	}

	func setupView() {

		self.titleLabel.textColor = .lightGray
		self.titleLabel.font = UIFont.footnoteFont()
		self.titleLabel.backgroundColor = .clear

		self.descriptionLabel.textColor = UIColor.night()
		self.descriptionLabel.font = UIFont.bodyFont()
		self.descriptionLabel.backgroundColor = .clear

		self.pointsLabel.textColor = UIColor.night()
		self.pointsLabel.font = UIFont.title1Font()
		self.pointsLabel.backgroundColor = .clear
	}
}
