//
//  VIAYesNoQuestionCell.swift
//  VitalityActive
//
//  Created by Val Tomol on 02/02/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import UIKit

public class VIAYesNoQuestionCell: UITableViewCell, Nibloadable {
    @IBOutlet weak var questionTitle: UILabel!
    @IBOutlet weak public var yesButton: UIButton!
    @IBOutlet weak public var noButton: UIButton!
    
    var yesButtonAction: () -> (Void) = {
        debugPrint("PressedYesButton")
    }
    
    var noButtonAction: () -> (Void) = {
        debugPrint("PressedYesButton")
    }
    
    @IBAction func yesButtonTapped(_ sender: Any) {
        yesButtonAction()
    }
    
    @IBAction func noButtonTapped(_ sender: Any) {
        noButtonAction()
    }
    
    public override func awakeFromNib() {
        super.awakeFromNib()
        self.setup()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    public func setup() {
        self.backgroundColor = UIColor.day()
        self.setupQuestionTitle()
        self.setupActionButtons()
        self.setButtonProperties(button: self.yesButton)
        self.setButtonProperties(button: self.noButton)
    }
    
    func setupQuestionTitle() {
        self.questionTitle.numberOfLines = 0
        self.questionTitle.textAlignment = .left
        self.questionTitle.font = UIFont.subheadlineSemiBoldFont()
    }
    
    func setupActionButtons() {
        self.bringSubview(toFront: self.yesButton)
        self.bringSubview(toFront: self.noButton)
    }
    
    public func setButtonProperties(button: UIButton) {
        button.titleLabel?.font = UIFont.systemFont(ofSize: 45, weight: UIFontWeightRegular)
        button.setTitleColor(UIColor.mediumGrey(), for: .normal)
    }
    
    public func setQuestionLabel(text: String) {
        self.questionTitle.text = text
    }
    
    public func setYesButton(title: String) {
        self.yesButton.setTitle(title, for: .normal)
    }
    
    public func setNoButton(title: String) {
        self.noButton.setTitle(title, for: .normal)
    }
    
    public func setYesButtonPressed(action: @escaping () -> (Void)) {
        self.yesButtonAction = action
    }
    
    public func setNoButtonPressed(action: @escaping () -> (Void)) {
        self.noButtonAction = action
    }
    
    public func setYesSelected() {
        self.yesButton.titleLabel?.font = UIFont.systemFont(ofSize: 45, weight: UIFontWeightMedium)
        self.yesButton.setTitleColor(UIColor.currentGlobalTintColor(), for: .normal)
        self.noButton.titleLabel?.font = UIFont.systemFont(ofSize: 45, weight: UIFontWeightRegular)
        self.noButton.setTitleColor(UIColor.mediumGrey(), for: .normal)
    }
    
    public func setNoSelected() {
        self.noButton.titleLabel?.font = UIFont.systemFont(ofSize: 45, weight: UIFontWeightMedium)
        self.noButton.setTitleColor(UIColor.currentGlobalTintColor(), for: .normal)
        self.yesButton.titleLabel?.font = UIFont.systemFont(ofSize: 45, weight: UIFontWeightRegular)
        self.yesButton.setTitleColor(UIColor.mediumGrey(), for: .normal)
    }
}
