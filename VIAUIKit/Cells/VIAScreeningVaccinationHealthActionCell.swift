
//
//  VIAScreeningVaccinationHealthAction.swift
//  VitalityActive
//
//  Created by OJ Garde on 11/11/2017.
//  Copyright © 2017 Glucode. All rights reserved.
//

import UIKit

public class VIAScreeningVaccinationHealthActionCell: UITableViewCell, Nibloadable {
    
    @IBOutlet weak var actionLabel: UILabel!
    public var actionName: String? {
        set {
            actionLabel.text = newValue
            if newValue == nil || actionName == "" {
                actionLabel.isHidden = true
            } else {
                actionLabel.isHidden = false
            }
        }
        get {
            return actionLabel.text
        }
    }
    
    
    @IBOutlet weak var actionSelectionIndicatorView: UIImageView!
    public var actionSelectionIndicatorIcon: UIImage? {
        set {
            actionSelectionIndicatorView.image = newValue
            actionSelectionIndicatorView.isHidden = newValue == nil
        }
        get {
            return self.actionSelectionIndicatorView.image
        }
    }
    
    @IBOutlet weak var testedDateLabel: UILabel!
    public var testedDate: String? {
        set {
            testedDateLabel.text = newValue
            testedDateLabel.isHidden = newValue == nil
        }
        get {
            return self.testedDateLabel.text
        }
    }

}
