//
//  VIAWebViewCell.swift
//  VitalityActive
//
//  Created by Val Tomol on 05/02/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import UIKit

public class VIAWebViewCell: UITableViewCell, Nibloadable {
    @IBOutlet weak public var webView: UIWebView!
    
    public override func awakeFromNib() {
        super.awakeFromNib()
        self.setup()
    }
    
    public func setup() {
        self.webView.scrollView.bounces = false
    }
    
    public func loadHtmlContentWithString(_ string: String) {
        self.webView.loadHTMLString(string, baseURL: nil)
    }
}
