//
//  VIAProfileMembershipPassBannerTableViewCell.swift
//  VitalityActive
//
//  Created by Ma. Catherine Purganan on 10/11/2017.
//  Copyright © 2017 Glucode. All rights reserved.
//
import UIKit

public class VIAProfileMembershipPassBannerTableViewCell: UITableViewCell, Nibloadable {

    @IBOutlet var constraintAspectRatio: NSLayoutConstraint!
    @IBOutlet public var logoImageView: UIImageView!
    public var shouldBeInAspectRatio: Bool = false
    public var logoImage: UIImage? {
        set {
            guard self.logoImageView != nil else { return }
            self.logoImageView.image = newValue
            self.logoImageView.isHidden = self.logoImageView.image == nil
        }
        get {
            return self.logoImageView.image
        }
    }
    
    public var inAspectRatio: Bool {
        set {
            self.shouldBeInAspectRatio = newValue
            self.constraintAspectRatio?.isActive = self.shouldBeInAspectRatio
            self.layoutIfNeeded()
        }
        get {
            return self.shouldBeInAspectRatio
        }
    }
    
    override public func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}
