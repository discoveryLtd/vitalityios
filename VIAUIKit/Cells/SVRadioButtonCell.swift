//
//  SAVRadioButtonCell.swift
//  VitalityActive
//
//  Created by OJ Garde on 2/8/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import UIKit

public class SVRadioButtonCell: UITableViewCell, Nibloadable {
    
    @IBOutlet public weak var actionLabel: UILabel!
    public var actionName: String? {
        set {
            actionLabel.text = newValue
            if newValue == nil || actionName == "" {
                actionLabel.isHidden = true
            } else {
                actionLabel.isHidden = false
            }
        }
        get {
            return actionLabel.text
        }
    }
    
    
    @IBOutlet public weak var actionImageView: UIImageView!
    public var actionSelectionIndicatorIcon: UIImage? {
        set {
            actionImageView.image = newValue
            actionImageView.isHidden = newValue == nil
        }
        get {
            return self.actionImageView.image
        }
    }

}
