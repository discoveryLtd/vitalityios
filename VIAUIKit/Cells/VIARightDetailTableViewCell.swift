import UIKit

public class VIARightDetailTableViewCell: UITableViewCell, Nibloadable {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var valueLabel: UITextField!

    override public func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override public func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    public func setTitle(_ title: String?) {
        self.titleLabel.text = title
    }

    public func setValue(_ value: String?) {
        self.valueLabel.text = value
    }

    public func setPlaceholder(_ placeholder: String?) {
        self.valueLabel.placeholder = placeholder
    }

}
