//
//  VIATextViewProvideFeedbackViewCell.swift
//  VitalityActive
//
//  Created by Von Kervin R. Tuico on 12/03/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import UIKit

public class VIATextViewProvideFeedbackViewCell: UITableViewCell, Nibloadable {
    
    @IBOutlet var headingLabel: UILabel!
    public var headingText: String? {
        set {
            self.headingLabel.text = newValue
        }
        get {
            return self.headingLabel.text
        }
    }
    
    public var attributedHeadingText: NSAttributedString? {
        set {
            self.headingLabel.attributedText = newValue
        }
        get {
            return self.headingLabel.attributedText
        }
    }
    
    @IBOutlet public var contentTextView: UITextView!
    public var contentText: String? {
        set {
            self.contentTextView.text = newValue
            self.contentTextView.font = UIFont.textField()
            self.contentTextView.textColor = self.contentTextView.textColor?.withAlphaComponent(100 / 100)
        }
        get {
            return self.contentTextView.text
        }
    }
    public var attributedContentText: NSAttributedString? {
        set {
            self.contentTextView.attributedText = newValue
        }
        get {
            return self.contentTextView.attributedText
        }
    }
    
    public func fontForContentLabel() -> UIFont {
        return contentTextView.font!
    }
    
    public override func awakeFromNib() {
        super.awakeFromNib()
        
        self.headingLabel.text = nil
        self.headingLabel.font = UIFont.cellHeadingLabel()
        self.headingLabel.textColor = UIColor.tableViewSectionHeaderFooter()
        
//        self.contentTextView.delegate = self
        self.contentTextView.text = nil
        self.contentTextView.textColor = UIColor.darkText
    }
}

//extension VIATextViewProvideFeedbackViewCell: UITextViewDelegate {
//    
//    public func textViewDidBeginEditing(_ textView: UITextView) {
//        if contentTextView.textColor == UIColor.darkText {
//            contentTextView.text = nil
//            contentTextView.textColor = UIColor.black
//        }
//    }
//    
//    public func textViewDidEndEditing(_ textView: UITextView) {
//        if contentTextView.text.isEmpty {
//            contentTextView.text = "Provide Feedback..."
//            contentTextView.textColor = UIColor.darkText
//        }
//    }
//    
//    public func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
//        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
//        let numberOfChars = newText.characters.count // for Swift use count(newText)
//        return numberOfChars <= 15;
//    }
//}
