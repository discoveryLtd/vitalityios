import Foundation
import UIKit
import SnapKit
import VIAUtilities
//import VitalityKit

public class SAVLandingHeaderViewCell: UITableViewCell, Nibloadable {
    
    // MARK: Properties
    
    public var title: String? {
        get {
            return self.titleLabel.text
        }
        set {
            self.titleLabel.text = newValue
        }
    }
    
    public var bonusMessage: String? {
        get {
            return self.bonusMessageLabel.text
        }
        set {
            self.bonusMessageLabel.text = newValue
            self.bonusMessageLabel.isHidden = newValue == nil
        }
    }
    
    public var message: String? {
        get {
            return self.messageLabel.text
        }
        set {
            self.messageLabel.text = newValue
        }
    }
    
    // MARK : Confirm and Submit button
    
    public var didTapButton: () -> Void = {
        debugPrint("VHCLandingHeaderView didTapButton")
    }
    
    
    // MARK: Outlets
    
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var bonusMessageLabel: UILabel!
    @IBOutlet private weak var messageLabel: UILabel!
    @IBOutlet public weak var button: BorderButton!
    
    // MARK:
    
    override public func awakeFromNib() {
        super.awakeFromNib()
        
        titleLabel.text = nil
        titleLabel.numberOfLines = 2
        titleLabel.font = UIFont.title2Font()
        titleLabel.textColor = UIColor.night()
        
        bonusMessageLabel.text = nil
        bonusMessageLabel.isHidden = true
        bonusMessageLabel.numberOfLines = 0
        bonusMessageLabel.font = UIFont.bodyFont()
        bonusMessageLabel.textColor = UIColor.darkGrey()
        
        messageLabel.text = nil
        messageLabel.numberOfLines = 0
        messageLabel.font = UIFont.subheadlineFont()
        messageLabel.textColor = UIColor.darkGrey()
        
        //button.setTitle(SavStrings.Sv.LandingSubmitButton1010, for: .normal)
        button.setupButton()
        button.titleLabel?.font = UIFont.bodyFont()
        button.titleLabel?.textAlignment = .center
    }
    
    override public func layoutSubviews() {
        super.layoutSubviews()
        
        self.layer.removeAllBorderLayers()
        self.layer.addBorder(edge: .top)
        self.layer.addBorder(edge: .bottom)
        
        button.tintColor = UIColor.currentGlobalTintColor()
    }
    
    @IBAction func didTapButton(_ sender: Any) {
        self.didTapButton()
    }
}
