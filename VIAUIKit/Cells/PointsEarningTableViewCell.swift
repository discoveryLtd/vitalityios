import UIKit

public class PointsEarningTableViewCell: UITableViewCell, Nibloadable {

    @IBOutlet public weak var titleLogo: UIImageView!
    @IBOutlet public weak var title: UILabel!
    @IBOutlet public weak var message: UILabel!
    //Points Earned stack
    @IBOutlet weak var totalPotentialPointsEarnedStackView: UIStackView!
    @IBOutlet public weak var totalPotentialPointsEarnedImageView: UIImageView!
    @IBOutlet public weak var totalPotentialPointsEarnedLabel: UILabel!
    
    @IBOutlet public weak var messageImageView: UIImageView!
    
    
    override public func awakeFromNib() {
        super.awakeFromNib()
        title.font = UIFont.headlineFont()
        title.textColor = UIColor.night()
        message.font = UIFont.subheadlineFont()
        message.textColor = UIColor.darkGray
        messageImageView.isHidden = true
        messageImageView.image = nil
        totalPotentialPointsEarnedImageView.image = nil
        totalPotentialPointsEarnedLabel.font = UIFont.subheadlineFont()
        totalPotentialPointsEarnedLabel.textColor = UIColor.darkGray
        totalPotentialPointsEarnedStackView.isHidden = true
    }
    
    public func showTotalPotenialPointsEaerned() {
        self.totalPotentialPointsEarnedStackView.isHidden = false
    }
}
