//
//  VIAGraphTableViewCell.swift
//  VitalityActive
//
//  Created by admin on 2017/08/10.
//  Copyright © 2017 Glucode. All rights reserved.
//

import UIKit

public class VIAGraphTableViewCell: UITableViewCell, Nibloadable {
    @IBOutlet weak public var dateImageView: UIImageView!
    @IBOutlet weak public var dateLabel: UILabel!
    @IBOutlet weak public var graphView: UIView!
    
    @IBOutlet weak public var graphHeight: NSLayoutConstraint!
    @IBOutlet weak public var graphWidth: NSLayoutConstraint!
    
    public var graph: VIAGraphView?
    override public func awakeFromNib() {
        super.awakeFromNib()
        dateLabel.font = UIFont.headlineFont()
    }
    
    override public func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
