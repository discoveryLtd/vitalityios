import UIKit
import VitalityKit

public class VIADatePickerCell: UICollectionViewCell, Nibloadable {

    // MARK: Outlets

    @IBOutlet weak var datePicker: UIDatePicker!

    // MARK: Properties

    public var addBottomBorder = false

    public var selectedDate: Date = Date() {
        didSet {
            self.datePicker.date = self.selectedDate
        }
    }

    public var maximumDate: Date? {
        get {
            return self.datePicker.maximumDate
        }
        set {
            self.datePicker.maximumDate = newValue
        }
    }

    public var minimumDate: Date? {
        get {
            return self.datePicker.minimumDate
        }
        set {
            self.datePicker.minimumDate = newValue
        }
    }

    public var didPickDate: (_ date: Date) -> Void = VIADatePickerCell.defaultDidPickDate

    // MARK: Init

    override public func awakeFromNib() {
        super.awakeFromNib()

        self.backgroundColor = UIColor.day()
        self.datePicker.backgroundColor = UIColor.day()
        self.datePicker.layer.addBorder(edge: .bottom)

        self.datePicker.datePickerMode = .date
        self.datePicker.addTarget(self, action: #selector(datePickerDidPickDate(_:)), for: .valueChanged)
        self.datePicker.maximumDate = Date()
    }

    override public func prepareForReuse() {
        super.prepareForReuse()

        self.datePicker.date = Date()
        self.didPickDate = VIADatePickerCell.defaultDidPickDate
    }

    override public func layoutSubviews() {
        super.layoutSubviews()

        contentView.layer.removeAllBorderLayers()
        if addBottomBorder {
            contentView.layer.addBorder(edge: .bottom)
        }
    }

    // MARK: DatePicker

    class func defaultDidPickDate(_ date: Date) {
        debugPrint("didPickDate: \(date)")
    }

    func datePickerDidPickDate(_ datePicker: UIDatePicker) {
        if AppSettings.getAppTenant() == .SLI {
            let calendar        = Calendar.current
            let currentDate     = Date()
            let selectedDate    = datePicker.date
            
            var dateComponents      = DateComponents()
            dateComponents.year     = calendar.component(.year,     from: selectedDate)
            dateComponents.month    = calendar.component(.month,    from: selectedDate)
            dateComponents.day      = calendar.component(.day,      from: selectedDate)
            dateComponents.hour     = calendar.component(.hour,     from: currentDate)
            dateComponents.minute   = calendar.component(.minute,   from: currentDate)
            dateComponents.second   = calendar.component(.second,   from: currentDate)

            if let formattedDateTime = calendar.date(from: dateComponents){
                self.didPickDate(formattedDateTime)
            }else{
                self.didPickDate(selectedDate)
            }
        } else {
            self.didPickDate(datePicker.date)
        }
    }
}
