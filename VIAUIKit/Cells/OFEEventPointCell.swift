//
//  OFEEventPointCell.swift
//  VitalityActive
//
//  Created by Val Tomol on 03/05/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import UIKit

public class OFEEventPointCell: UITableViewCell, Nibloadable {
    @IBOutlet weak var stackView: UIStackView!
    
    @IBOutlet weak var headingLabel: UILabel!
    public var headingText: String? {
        set {
            self.headingLabel.text = newValue
        }
        get {
            return self.headingLabel.text
        }
    }
    
    @IBOutlet weak var contentLabel: UILabel!
    public var contentText: String? {
        set {
            self.contentLabel.text = newValue
            self.contentLabel.isHidden = newValue == nil
        }
        get {
            return self.contentLabel.text
        }
    }
    
    public override func prepareForReuse() {
        super.prepareForReuse()
        setupCell()
    }
    
    override public func awakeFromNib() {
        super.awakeFromNib()
        setupCell()
    }
    
    public func configureWithLargeHeading() {
        self.headingLabel.font = UIFont.title1Font()
        self.headingLabel.textColor = UIColor.night()
        self.headingLabel.numberOfLines = 0
        self.headingLabel.backgroundColor = self.contentView.backgroundColor
    }
    
    private func setupCell() {
        self.headingLabel.text = nil
        self.headingLabel.font = UIFont.headlineFont()
        self.headingLabel.textColor = UIColor.night()
        self.headingLabel.numberOfLines = 0
        self.headingLabel.backgroundColor = self.contentView.backgroundColor
        
        self.contentLabel.isHidden = true
        self.contentLabel.text = nil
        self.contentLabel.font = UIFont.footnoteFont()
        self.contentLabel.textColor = UIColor.darkGrey()
        self.contentLabel.numberOfLines = 0
        self.contentLabel.backgroundColor = self.contentView.backgroundColor
    }
    
}
