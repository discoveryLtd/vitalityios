//
//  VHRLandingHeaderCell.swift
//  VitalityActive
//
//  Created by Lionel Camacho on 2017/05/09.
//  Copyright © 2017 Glucode. All rights reserved.
//

import UIKit
import VitalityKit

public class VHRLandingHeaderCell: UITableViewCell, Nibloadable {

    enum CellState {
        case incomplete
        case complete
    }

    private var completedImageView = UIImageView()
    private var segmentedCircleView = VIASegmentedCircleView()

    @IBOutlet weak var statusStackView: UIStackView!
    @IBOutlet weak var earnMessageLabel: UILabel!
    public var earnText: String? {
        set {
            earnMessageLabel.text = newValue
            earnMessageLabel.isHidden = newValue == nil
        }
        get {
            return earnMessageLabel.text
        }
    }
    @IBOutlet weak var timeToCompleteMessageLabel: UILabel!
    public var timeToCompleteText: String? {
        set {
            timeToCompleteMessageLabel.text = newValue
            timeToCompleteMessageLabel.isHidden = newValue == nil
        }
        get {
            return timeToCompleteMessageLabel.text
        }
    }

    public override func prepareForReuse() {
        super.prepareForReuse()
        setupCell()
    }

    override public func awakeFromNib() {
        super.awakeFromNib()
        setupCell()
    }

    private func setupCell() {
        earnMessageLabel.text = nil
        timeToCompleteMessageLabel.text = nil

        segmentedCircleView.isHidden = true
        completedImageView.isHidden = true
        completedImageView.tintColor = AppSettings.isAODAEnabled() ? UIColor.knowYourHealthGreen() : UIColor.currentGlobalTintColor()
        earnMessageLabel.font = .title3Font()
        timeToCompleteMessageLabel.font = .footnoteFont()

        earnMessageLabel.textColor = .night()
        timeToCompleteMessageLabel.textColor = .darkGrey()
    }

    public func configureCompleteVHRLandingHeaderCell(title: String, description: String, totalSections: Int, completedSections: Int, completedImage: UIImage?, integerFormatter: NumberFormatter) {
        earnText = title
        timeToCompleteText = description
        completedImageView.image = completedImage
        setCellState(state: .complete)
    }

    public func configureIncompleteVHRLandingHeaderCell(title: String, description: String, totalSections: Int, completedSections: Int, integerFormatter: NumberFormatter) {
        earnText = title
        timeToCompleteText = description
        segmentedCircleView.setupSegmentedCircleForDisplay(totalSections: totalSections, completedSections: completedSections, numberFormatter: integerFormatter)
        setCellState(state: .incomplete)
    }

    func setCellState(state: CellState) {
        for view in statusStackView.arrangedSubviews {
            statusStackView.removeArrangedSubview(view)
        }
        switch state {
        case .complete:
            segmentedCircleView.isHidden = true
            completedImageView.isHidden = false
            statusStackView.addArrangedSubview(completedImageView)
        default:
            segmentedCircleView.isHidden = false
            completedImageView.isHidden = true
            statusStackView.addArrangedSubview(segmentedCircleView)
        }
    }

}
