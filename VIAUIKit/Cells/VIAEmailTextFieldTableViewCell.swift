//
//  VIAEmailTextFieldTableViewCell.swift
//  VIAUIKit
//
//  Created by Erik John T. Alicaya (ADMIN) on 19/07/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import UIKit
import TTTAttributedLabel

public class VIAEmailTextFieldTableViewCell: VIATextFieldTableViewCell {
    
    public override func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return true
    }
}
