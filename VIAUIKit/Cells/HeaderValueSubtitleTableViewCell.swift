//
//  HeaderValueSubtitleTableViewCell.swift
//  VitalityActive
//
//  Created by Marius Janse van Vuuren on 4/19/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

import UIKit

public class HeaderValueSubtitleTableViewCell: UITableViewCell, Nibloadable {
    @IBOutlet weak var cellTitle: UILabel!
    @IBOutlet weak var cellValue: UILabel!
    @IBOutlet weak var cellSubtitle: UILabel!

    override public func awakeFromNib() {
        super.awakeFromNib()
        self.setupCellStyling()
    }

    override public func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    public func setCell(title: String) {
        self.cellTitle.text = title
    }

    public func setCell(value: String) {
        self.cellValue.text = value
    }

    public func setCell(subTitle: String) {
        self.cellSubtitle.text = subTitle
    }

    func setupCellStyling() {
        self.cellTitle.font = UIFont.subheadlineFont()
        self.cellTitle.textColor = UIColor.mediumGrey()

        self.cellValue.font = UIFont.headlineFont()
        self.cellValue.textColor = UIColor.night()

        self.cellSubtitle.font = UIFont.subheadlineFont()
        self.cellSubtitle.textColor = UIColor.darkGray
    }
}
