//
//  PreferencessOnboarding.swift
//  Vitality Preferences
//
//  Created by Marius Janse van Vuuren on 2017/02/14.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import UIKit
import SnapKit

protocol PreferencesSectionCellProtocol {
    func setTitle(_ title: String)
    func setDetailText(_ detailText: String)
    func setActionButton(title: String)
    func setAction(_ closure: @escaping () -> (Void)?)
}

public class PreferencesSectionCell: UITableViewCell, Nibloadable {
    internal var actionButtonTapped: () -> (Void)? = {
        debugPrint("Button Tapped")
    }

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var informationTextLabel: UILabel!
    @IBOutlet weak var actionButton: UIButton!

    override public func awakeFromNib() {
        super.awakeFromNib()
        setupCell()
    }

    public override func prepareForReuse() {
        super.prepareForReuse()
        setupCell()
    }

    func setupCell() {
        self.setupCellView()
        self.setupTitleLabel()
        self.setupDetailsLabel()
        self.setupButton()
    }

    public func resetCell() {
        self.titleLabel.text = nil
        self.informationTextLabel.text = nil
    }

    @IBAction func buttonWithActionTapped(_ sender: Any) {
        self.actionButtonTapped()
    }

    func setupCellView() {
        self.backgroundColor = .clear
        self.backgroundView?.backgroundColor = .clear
        self.contentView.backgroundColor = .clear
    }

    func setupTitleLabel() {
        self.titleLabel.font = UIFont.onboardingHeading()
        self.titleLabel.textColor = UIColor.night()
    }

    func setupDetailsLabel() {
        self.informationTextLabel.font = UIFont.bodyFont()
        self.informationTextLabel.textColor = UIColor.black
    }

    func setupButton() {
        self.actionButton.setTitle(nil, for: .normal)
        self.actionButton.isHidden = true
        self.actionButton.titleLabel?.font = UIFont.subheadlineFont()
        self.actionButton.titleLabel?.textColor = UIColor.primaryColor()
    }
}

extension PreferencesSectionCell :PreferencesSectionCellProtocol {
    public func setTitle(_ title: String) {
        self.titleLabel.text = title
    }

    public func setDetailText(_ informationText: String) {
        self.informationTextLabel.text = informationText
    }

    public func setActionButton(title: String) {
        self.actionButton.isHidden = false
        self.bringSubview(toFront: self.actionButton)
        self.actionButton.setTitle(title, for: .normal)
    }

    public func setAction(_ closure: @escaping () -> (Void)?) {
        self.actionButtonTapped = closure
    }
}
