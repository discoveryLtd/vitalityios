//
//  SAVHealthActionViewCell.swift
//  VitalityActive
//
//  Created by Michelle Oratil on 11/11/2017.
//  Copyright © 2017 Glucode. All rights reserved.
//
import UIKit
import SnapKit
import VIAUtilities

public class SAVHealthActionViewCell: UITableViewCell, Nibloadable {
    
    @IBOutlet weak var feedbackStackView: UIStackView!
    
    
    @IBOutlet weak var messageStackView: UIStackView!
    
    @IBOutlet weak var contentStackView: UIStackView!
    
    @IBOutlet weak var titleMeasurementStackView: UIStackView!
    
    @IBOutlet weak var arrowIcon: UIImageView!
    public var arrowImage: UIImage? {
        set {
            arrowIcon.image = newValue
            arrowIcon.isHidden = newValue == nil
        }
        get {
            return self.arrowIcon.image
        }
    }
    
    @IBOutlet weak var messageIcon: UIImageView!
    public var messageImage: UIImage? {
        set {
            messageIcon.image = newValue
            messageIcon.isHidden = newValue == nil
        }
        get {
            return self.messageIcon.image
        }
    }
    
    
    @IBOutlet public weak var measurableLogo: UIImageView!
    public var measurableImage: UIImage? {
        set {
            measurableLogo.image = newValue
            measurableLogo.isHidden = newValue == nil
        }
        get {
            return self.measurableLogo.image
        }
    }
    
    @IBOutlet weak var measurableTitleLabel: UILabel!
    public var measurableTitleText: String? {
        set {
            measurableTitleLabel.text = newValue
            measurableTitleLabel.isHidden = newValue == nil
        }
        get {
            return measurableTitleLabel.text
        }
    }
    
    @IBOutlet weak var messageLabel: UILabel!
    public var messageText: String? {
        set {
            messageLabel.text = newValue
            messageLabel.isHidden = newValue == nil
            messageStackView.isHidden = messageLabel.isHidden
        }
        get {
            return messageLabel.text
        }
    }
    
    @IBOutlet weak var footerMessageLabel: UILabel!
    public var footerMessageText: String? {
        set {
            footerMessageLabel.text = newValue
            footerMessageLabel.isHidden = newValue == nil
        }
        get {
            return footerMessageLabel.text
        }
    }
    
    public override func prepareForReuse() {
        super.prepareForReuse()
        setupCell()
    }
    
    override public func awakeFromNib() {
        super.awakeFromNib()
        setupCell()
    }
    
    private func setupCell() {
        arrowIcon.image = nil
        footerMessageLabel.text = nil
        for view in feedbackStackView.subviews {
            view.removeFromSuperview()
        }
        
        feedbackStackView.isHidden = true
        arrowIcon.isHidden = true
        footerMessageLabel.isHidden = true
        
        
        measurableTitleLabel.font = UIFont.headlineFont()
        messageLabel.font = .subheadlineFont()
        footerMessageLabel.font = .footnoteFont()
        
        measurableLogo.tintColor = UIColor.currentGlobalTintColor()
        messageIcon.tintColor = .mediumGrey()
        messageLabel.textColor = .darkGrey()
        measurableTitleLabel.textColor = .night()
        footerMessageLabel.textColor = .mediumGrey()
        
    }
    
    public func configureSAVHealthActionNoDataCell(image: UIImage?, measurableTitle: String, message: String?, feedback: [VHCFeedback]?, footerMessage: String?, isOutOfCurrentMembershipPeriod: Bool, isDisclosureIndicatorHidden: Bool) {
        measurableImage = image
        measurableTitleText = measurableTitle
        messageText = message
        footerMessageText = footerMessage
        arrowImage = VIAUIKitAsset.arrowDrill.image
        arrowIcon.isHidden = isDisclosureIndicatorHidden
        makeCellGrey()
        if isOutOfCurrentMembershipPeriod {
            makeCellGrey()
        }
    }
    
    public func configureSAVHealthActionMainCell(image: UIImage?, measurableTitle: String, messageIcon: UIImage?, message: String?, footerMessage: String?, isDisclosureIndicatorHidden: Bool, completed: Bool = false, hasPointsEvents: Bool = false) {
        makeCellGrey()
        measurableImage = image
        measurableTitleText = measurableTitle
        messageText = message
        messageImage = messageIcon
        footerMessageText = footerMessage
        arrowImage = VIAUIKitAsset.arrowDrill.image
        arrowIcon.isHidden = isDisclosureIndicatorHidden
        updateCellAsCompleted(completed, hasPointsEvents)
    }
    
    func makeCellGrey() {
        messageIcon.tintColor = .mediumGrey()
        measurableLogo.tintColor = .lightGrey()
        measurableTitleLabel.textColor = .darkGrey()
        messageLabel.textColor = .darkGrey()
        footerMessageLabel.textColor = .darkGrey()
    }
    
    func updateCellAsCompleted(_ completed:Bool, _ hasPointsEvents: Bool) {
        messageIcon.tintColor = .night()
        measurableLogo.tintColor = .clear
        measurableTitleLabel.textColor = completed || hasPointsEvents ? .night() : .darkGrey()
        messageLabel.textColor = .darkGrey()
        footerMessageLabel.textColor = .darkGrey()
        
        messageIcon.isHidden = !completed
    }
}
