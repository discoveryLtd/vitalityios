//
//  VIAStatusStepCellView.swift
//  VitalityActive
//
//  Created by Joshua Ryan on 9/5/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

import UIKit

public class VIAStatusStepCellView: UITableViewCell, Nibloadable {

	@IBOutlet weak var statusImage: UIImageView!
	@IBOutlet weak var dashView: UIView!
	@IBOutlet weak var titleLabel: UILabel!
	@IBOutlet weak var descriptionLabel: UILabel!
	@IBOutlet weak var infoButton: UIButton!
	@IBOutlet weak var topDash: VIADashedLineView!
	@IBOutlet weak var bottomDash: VIADashedLineView!

	public var badge: UIImage? {
		set {
			self.statusImage.image = newValue
		}
		get {
			return self.statusImage.image
		}
	}

	public var title: String? {
		set {
			self.titleLabel.text = newValue
		}
		get {
			return self.titleLabel.text
		}
	}

	public var cellBody: String? {
		set {
			self.descriptionLabel.text = newValue
		}
		get {
			return self.descriptionLabel.text
		}
	}

	public var first: Bool {
		set {
			self.topDash.isHidden = newValue
		}
		get {
			return self.topDash.isHidden
		}
	}

	public var last: Bool {
		set {
			self.bottomDash.isHidden = newValue
		}
		get {
			return self.bottomDash.isHidden
		}
	}

	public override func awakeFromNib() {
		super.awakeFromNib()
		setupView()
	}

	func setupView() {

		self.titleLabel.textColor = UIColor.night()
		self.titleLabel.font = UIFont.title3Font()
		self.titleLabel.backgroundColor = .clear

		self.descriptionLabel.textColor = .darkGray
		self.descriptionLabel.font = UIFont.subheadlineFont()
		self.descriptionLabel.backgroundColor = .clear

		self.topDash.phase = 3.0
	}
}
