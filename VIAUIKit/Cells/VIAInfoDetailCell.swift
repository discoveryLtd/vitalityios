import UIKit

public class VIAInfoDetailCell: UITableViewCell, Nibloadable {

	@IBOutlet private var titleLabel: UILabel!
	public var labelText: String? {
		set {
			guard self.titleLabel != nil else { return }
			self.titleLabel.text = newValue
            self.titleLabel.font = UIFont.footnoteFont()
            self.titleLabel.textColor = UIColor.fieldLabelColor()
		}
		get {
			return self.titleLabel.text
		}
	}

	@IBOutlet private var valueLabel: UILabel!
	public var valueText: String? {
		set {
			guard self.valueLabel != nil else { return }
			self.valueLabel.text = newValue
			self.valueLabel.font = UIFont.textField()
            self.valueLabel.textColor = self.valueLabel.textColor.withAlphaComponent(100 / 100)
		}
		get {
			return self.valueLabel.text
		}
	}

	public var noValueText: String? {
		set {
			guard self.valueLabel != nil else { return }
			self.valueLabel.text = newValue
			self.valueLabel.font = UIFont.textFieldPlaceholder()
            // change opacity as this is a placeholder value
            self.valueLabel.textColor = self.valueLabel.textColor.withAlphaComponent(26 / 100)
		}
		get {
			return self.valueLabel.text
		}
	}


	override public func awakeFromNib() {
		super.awakeFromNib()

		self.titleLabel.text = nil
		self.titleLabel.font = UIFont.cellHeadingLabel()
		self.titleLabel.textColor = UIColor.tableViewSectionHeaderFooter()

		self.valueLabel.text = nil
		self.valueLabel.textColor = UIColor.darkText
	}
}
