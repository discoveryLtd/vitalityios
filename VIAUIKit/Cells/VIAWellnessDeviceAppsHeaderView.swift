//
//  VIAWellnessDeviceAppsHeader.swift
//  VitalityActive
//
//  Created by Simon Stewart on 11/29/16.
//  Copyright © 2016 Glucode. All rights reserved.
//

import UIKit

public final class VIAWellnessDeviceAppsHeaderView: UIView, Nibloadable {

    @IBOutlet weak var headingLabel: UILabel!
    public var header: String? {
        set {
            guard let lbl = self.headingLabel else { return }
            lbl.text = newValue
        }
        get {
            guard let lbl = self.headingLabel else { return nil }
            return lbl.text
        }
    }

    @IBOutlet weak var contentLabel: UILabel!
    public var content: String? {
        set {
            guard let lbl = self.contentLabel else { return }
            lbl.text = newValue
        }
        get {
            guard let lbl = self.contentLabel else { return nil }
            return lbl.text
        }
    }

    @IBOutlet weak var actionButton: UIButton!
    public var action: String? {
        set {
            guard let btn = self.actionButton else { return }
            btn.isHidden = newValue == nil
            guard btn.isHidden == false else { return }
            btn.setTitle(newValue, for: .normal)

        }
        get {
            guard let btn = self.actionButton else { return nil }
            return btn.currentTitle
        }
    }

    public override func awakeFromNib() {
        super.awakeFromNib()

        self.headingLabel.text = nil
        self.headingLabel.font = UIFont.headlineFont()
        self.headingLabel.textColor = UIColor.night()
        self.headingLabel.numberOfLines = 0
        self.headingLabel.backgroundColor = self.backgroundColor

        self.contentLabel.text = nil
        self.contentLabel.font = UIFont.subheadlineFont()
        self.contentLabel.textColor = UIColor.darkGrey()
        self.contentLabel.numberOfLines = 0
        self.contentLabel.backgroundColor = self.backgroundColor

        self.actionButton.setTitle("", for: .normal)
    }
}
