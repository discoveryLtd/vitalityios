//
//  OFEHistoryEventCell.swift
//  VitalityActive
//
//  Created by Harry C. Lingad on 4/25/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import UIKit

public class OFEHistoryEventCell: UITableViewCell, Nibloadable {
    

    
    @IBOutlet public var label: UILabel!
    public var labelText: String? {
        set {
            guard self.label != nil else { return }
            self.label.text = newValue
        }
        get {
            return self.label.text
        }
    }
    
    public override func awakeFromNib() {
        super.awakeFromNib()
        setupCell()
    }
    
    public override func prepareForReuse() {
        super.prepareForReuse()
        setupCell()
    }
    
    func setupCell() {
        if self.label != nil {
            self.label.text = nil
            self.label.textColor = UIColor.night()
            self.label.font = UIFont.cellStyleMenuItemTextLabel()
            self.label.numberOfLines = 0
            self.label.backgroundColor = self.contentView.backgroundColor
        }
    }

}

