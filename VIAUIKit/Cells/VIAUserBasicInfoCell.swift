//
//  VIAUserBasicInfoCell.swift
//  VitalityActive
//
//  Created by Joshua Ryan on 7/11/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

import UIKit

public class VIAUserBasicInfoCell: UITableViewCell, Nibloadable {

	@IBOutlet weak var nameLabel: UILabel!
	public var name: String? {
		set {
			nameLabel.text = newValue
			nameLabel.isHidden = newValue == nil

			photoView.name = newValue
		}
		get {
			return nameLabel.text
		}
	}

	@IBOutlet weak var emailLabel: UILabel!
	public var email: String? {
		set {
			emailLabel.text = newValue
			emailLabel.isHidden = newValue == nil
		}
		get {
			return emailLabel.text
		}
	}

	@IBOutlet internal weak var photoView: VIAProfileAvatarView!
	public var photo: UIImage? {
		set {
			photoView.image = newValue
			photoView.isHidden = newValue == nil
		}
		get {
			return photoView.image
		}
	}

	override public func awakeFromNib() {
		super.awakeFromNib()

		self.nameLabel.text = nil
		self.nameLabel.font = UIFont.title3Font()
		self.nameLabel.textColor = UIColor.night()

		self.emailLabel.text = nil
		self.emailLabel.font = UIFont.subheadlineFont()
		self.emailLabel.textColor = UIColor.tableViewSectionHeaderFooter()

		self.photoView.image = nil
		self.photoView.backgroundColor = UIColor.lightGrey()

        // exception to the font styling rule as this isn't used anywhere else
		self.photoView.textFont = UIFont.systemFont(ofSize: 27.4, weight: UIFontWeightMedium)
	}
}
