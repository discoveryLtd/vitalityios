//
//  PMPointsEventCell.swift
//  VitalityActive
//
//  Created by Lionel Camacho on 2017/01/26.
//  Copyright © 2017 Glucode. All rights reserved.
//

import UIKit
import SnapKit

public struct Contribution {
    let image: UIImage
    let description: String

    public init(image: UIImage, description: String) {
        self.image = image
        self.description = description
    }
}

public class PMPointsEventCell: UITableViewCell, Nibloadable {
    var pointsValue = 0

    @IBOutlet public weak var contributionsStackView: UIStackView!

    @IBOutlet weak var contentStackView: UIStackView!

    @IBOutlet weak var pointsDateStackView: UIStackView!

    @IBOutlet weak var arrowIcon: UIImageView!
    public var arrowImage: UIImage? {
        set {
            arrowIcon.image = newValue
            arrowIcon.isHidden = newValue == nil
        }
        get {
            return self.arrowIcon.image
        }
    }

    @IBOutlet weak var pointsLogo: UIImageView!
    public var pointsImage: UIImage? {
        set {
            pointsLogo.image = newValue
            pointsLogo.isHidden = newValue == nil
        }
        get {
            return self.pointsLogo.image
        }
    }

    @IBOutlet weak var pointsLabel: UILabel!
    public var pointsText: String? {
        set {
            pointsLabel.text = newValue
            if newValue == nil || pointsValue == 0 {
                 pointsLabel.isHidden = true
            } else {
                pointsLabel.isHidden = false
            }
            pointsDateStackView.isHidden = pointsLabel.isHidden
        }
        get {
            return pointsLabel.text
        }
    }

    @IBOutlet weak var dateLabel: UILabel!
    public var dateText: String? {
        set {
            dateLabel.text = newValue
            dateLabel.isHidden = newValue == nil
        }
        get {
            return dateLabel.text
        }
    }

    @IBOutlet weak var messageLabel: UILabel!
    public var messageText: String? {
        set {
            messageLabel.text = newValue
            messageLabel.isHidden = newValue == nil
        }
        get {
            return messageLabel.text
        }
    }

    @IBOutlet weak var descriptionLabel: UILabel!
    public var descriptionText: String? {
        set {
            descriptionLabel.text = newValue
            descriptionLabel.isHidden = newValue == nil
        }
        get {
            return descriptionLabel.text
        }
    }

    @IBOutlet weak var deviceLabel: UILabel!
    public var deviceText: String? {
        set {
            deviceLabel.text = newValue
            deviceLabel.isHidden = newValue == nil
        }
        get {
            return deviceLabel.text
        }
    }

    public override func prepareForReuse() {
        super.prepareForReuse()
        setupCell()
    }

    override public func awakeFromNib() {
        super.awakeFromNib()
        setupCell()
    }

    private func setupCell() {
        arrowIcon.image = nil
        pointsLogo.image = nil
        pointsLabel.text = nil
        dateLabel.text = nil
        messageLabel.text = nil
        descriptionLabel.text = nil
        deviceLabel.text = nil
        for view in contributionsStackView.subviews {
            view.removeFromSuperview()
        }

        contributionsStackView.isHidden = true
        arrowIcon.isHidden = true
        pointsLogo.isHidden = true
        pointsLabel.isHidden = true
        dateLabel.isHidden = true
        messageLabel.isHidden = true
        descriptionLabel.isHidden = true
        deviceLabel.isHidden = true

        pointsLabel.font = .title1Font()
        messageLabel.font = .headlineFont()
        descriptionLabel.font = .subheadlineFont()
        deviceLabel.font = .footnoteFont()

        pointsLabel.textColor = .night()
        messageLabel.textColor = .darkGrey()
        descriptionLabel.textColor = .darkGrey()
        deviceLabel.textColor = .mediumGrey()
    }

    public func configurePointsMonitorMainCell(image: UIImage?, category: String, pointsValue: Int, pointsText: String?, message: String?, contributions: [Contribution]?, description: String?, device: String?, isDisclosureIndicatorHidden: Bool) {
        self.pointsValue = pointsValue
        self.pointsText = pointsText ?? ""
        messageText = message
        arrowImage = VIAUIKitAsset.arrowDrill.image
        arrowIcon.isHidden = isDisclosureIndicatorHidden
        pointsImage = image
        descriptionText = description
        deviceText = device
        if let pointsContributions = contributions {
            addContributions(contributions: pointsContributions)
        }
    }

    public func configurePointsMonitorDetailCell(pointsValue: Int, pointsText: String?, message: String?, contributions: [Contribution]?) {
        self.pointsValue = pointsValue
        self.pointsText = pointsText ?? ""
        messageText = message
        if let pointsContributions = contributions {
            addContributions(contributions: pointsContributions)
        }
    }

    public func addContributions(contributions: [Contribution]) {
        guard contributions.count > 0 else { return }
        contributionsStackView.isHidden = false
        for contribution in contributions {
            let stackView = UIStackView()
            let imageView = UIImageView()
            let label = UILabel()
            stackView.spacing = 5
            stackView.axis = UILayoutConstraintAxis.horizontal
            stackView.distribution = UIStackViewDistribution.fill
            stackView.alignment = UIStackViewAlignment.fill
            imageView.contentMode = UIViewContentMode.top
            imageView.image = contribution.image
            stackView.addArrangedSubview(imageView)
            stackView.addArrangedSubview(label)

            imageView.snp.makeConstraints({(make) -> Void in
                make.height.equalTo(label.snp.height)
                make.width.equalTo(15.0)
            })

            label.text = contribution.description
            label.textColor = .mediumGrey()
            label.font = .footnoteFont()
            label.numberOfLines = 0
            label.setContentHuggingPriority(1000, for: .vertical)
            label.setContentCompressionResistancePriority(1000, for: .vertical)

            contributionsStackView.addArrangedSubview(stackView)
            stackView.snp.makeConstraints({(make) -> Void in
                make.right.equalToSuperview()
                make.left.equalToSuperview()
            })
        }
    }
}
