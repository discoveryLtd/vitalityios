//
//  VHRLandingSectionCell.swift
//  VitalityActive
//
//  Created by Lionel Camacho on 2017/05/10.
//  Copyright © 2017 Glucode. All rights reserved.
//

import UIKit
import VitalityKit

public class VHRLandingSectionCell: UITableViewCell, Nibloadable {


    @IBOutlet weak var feedbackStackView: UIStackView!
    @IBOutlet weak var mainStackView: UIStackView!
    
    @IBOutlet weak var titleLabel: UILabel!
    public var titleText: String? {
        set {
            titleLabel.text = newValue
            titleLabel.isHidden = newValue == nil
        }
        get {
            return titleLabel.text
        }
    }
    @IBOutlet weak var feedbackLabel: UILabel!
    public var feedbackText: String? {
        set {
            feedbackLabel.text = newValue
            feedbackLabel.isHidden = newValue == nil
            guard newValue?.isEmpty == false else { feedbackStackView.isHidden = true; return }
            feedbackStackView.isHidden = feedbackLabel.isHidden
        }
        get {
            return feedbackLabel.text
        }
    }
    @IBOutlet weak var feedbackIcon: UIImageView!
    public var feedbackImage: UIImage? {
        set {
            feedbackIcon.image = newValue
            feedbackIcon.isHidden = newValue == nil
        }
        get {
            return self.feedbackIcon.image
        }
    }

    @IBOutlet public weak var sectionButton: BorderButton!

    public override func prepareForReuse() {
        super.prepareForReuse()
        setupCell()
    }

    override public func awakeFromNib() {
        super.awakeFromNib()
        setupCell()
    }

    private func setupCell() {
        titleLabel.text = nil
        feedbackLabel.text = nil
        feedbackIcon.image = nil

        titleLabel.font = .headlineFont()
        feedbackLabel.font = .footnoteFont()
        titleLabel.textColor = .night()
        feedbackLabel.textColor = .darkGrey()
        feedbackIcon.tintColor = .mediumGrey()

        sectionButton.setupButton()

        if let tenantID = AppSettings.getAppTenant(), tenantID == .GI {
            setMainStackViewWidthLayoutConstraint()
        }
        
    }

    public func configureCompleteVHRLandingSectionCell(title: String, feedback: String, buttonText: String, completeIcon: UIImage?) {
        titleText = title
        feedbackText = feedback
        feedbackImage = completeIcon

        titleLabel.textColor = .darkGrey()
        sectionButton.setTitle(buttonText, for: .normal)
        sectionButton.setTitleColor(UIColor.mediumGrey(), for: .normal)
        sectionButton.layer.borderColor = UIColor.mediumGrey().cgColor
        sectionButton.tintColor = .mediumGrey()
        sectionButton.sizeToFit()
    }

    public func configureIncompleteVHRLandingSectionCell(title: String, feedback: String, buttonText: String) {
        titleText = title
        feedbackText = feedback
        feedbackImage = nil

        titleLabel.textColor = .night()
        sectionButton.setTitle(buttonText, for: .normal)
        sectionButton.tintColor = UIColor.currentGlobalTintColor()
        sectionButton.layer.borderColor = UIColor.currentGlobalTintColor().cgColor
        sectionButton.sizeToFit()
    }
    
    private func setMainStackViewWidthLayoutConstraint() {
        mainStackView.translatesAutoresizingMaskIntoConstraints = false
        let constW = NSLayoutConstraint(item: mainStackView,
                                        attribute: .width,
                                        relatedBy: .lessThanOrEqual,
                                        toItem: nil,
                                        attribute: .notAnAttribute,
                                        multiplier: 1,
                                        constant: 200)
        mainStackView.addConstraint(constW)
    }
}
