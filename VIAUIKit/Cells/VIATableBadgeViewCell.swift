//
//  VIATableViewCell.swift
//  VitalityActive
//
//  Created by Wilmar van Heerden on 2016/11/14.
//  Copyright © 2016 Glucode. All rights reserved.
//

import UIKit

public class VIATableBadgeViewCell: UITableViewCell, Nibloadable {
    
    @IBOutlet public var cellImageView: UIImageView!
    public var cellImage: UIImage? {
        set {
            guard self.cellImageView != nil else { return }
            self.cellImageView.image = newValue
            self.cellImageView.isHidden = self.cellImageView.image == nil
        }
        get {
            return self.cellImageView.image
        }
    }
    
    @IBOutlet public var label: UILabel!
    public var labelText: String? {
        set {
            guard self.label != nil else { return }
            self.label.text = newValue
        }
        get {
            return self.label.text
        }
    }
    
    @IBOutlet public weak var badgeLabel: UILabel!
        public var badgeLabelText: String? {
            set {
                guard self.badgeLabel != nil else { return }
                self.badgeLabel.text = newValue
                if nil != newValue{
                    self.badgeLabel.layer.masksToBounds = true
                    self.badgeLabel.layer.cornerRadius = self.badgeLabel.bounds.width/2
                    self.badgeLabel.layer.backgroundColor = UIColor.red.cgColor
                }else{
                    self.badgeLabel.layer.backgroundColor = nil
                }
            }
            get {
                return self.badgeLabel.text
            }
        }
    
    public override func awakeFromNib() {
        super.awakeFromNib()
        setupCell()
    }
    
    /**public override func prepareForReuse() {
        super.prepareForReuse()
        setupCell()
    }**/
    
    func setupCell() {
        if self.cellImageView != nil {
            self.cellImageView.isHidden = false
            self.cellImageView.image = nil
        }
        if self.label != nil {
            self.label.text = nil
            self.label.textColor = UIColor.night()
            self.label.font = UIFont.cellStyleMenuItemTextLabel()
            self.label.numberOfLines = 0
            self.label.backgroundColor = self.contentView.backgroundColor
        }
    }
    
    public override func updateConstraints() {
        if self.cellImageView != nil {
            self.cellImageView.isHidden = self.cellImageView.image == nil
        }
        super.updateConstraints()
    }
}
