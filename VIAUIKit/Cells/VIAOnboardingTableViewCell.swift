//
//  VIAOnboardingTableViewCell.swift
//  VitalityActive
//
//  Created by Wilmar van Heerden on 2016/10/14.
//  Copyright © 2016 Glucode. All rights reserved.
//

import UIKit
import TTTAttributedLabel

public class VIAOnboardingTableViewCell: UITableViewCell, Nibloadable {

    // MARK: Outlets

    @IBOutlet private var cellImageView: UIImageView!
    @IBOutlet private var headingLabel: TTTAttributedLabel!
    @IBOutlet private var contentLabel: TTTAttributedLabel!

    // MARK: Properties

    public var heading: String? {
        get {
            return self.headingLabel.text as? String
        }
        set {
            self.headingLabel.text = newValue
        }
    }

    public var content: String? {
        get {
            return self.contentLabel.text as? String
        }
        set {
            self.contentLabel.text = newValue
        }
    }

    public var contentImage: UIImage? {
        get {
            return self.cellImageView.image
        }
        set {
            self.cellImageView.image = newValue
        }
    }

    // MARK: - Setters

    public func setHeadingFont(font: UIFont) {
        self.headingLabel.font = font
    }

    public func contentFont(font: UIFont) {
        self.contentLabel.font = font
    }

    public func setLabelTextColor(color: UIColor) {
        self.contentLabel.textColor = color
        self.headingLabel.textColor = color
    }

    public func setImageTint(color: UIColor ) {
        if let image = self.cellImageView.image {
            self.cellImageView.image = image.withRenderingMode(.alwaysTemplate)
        }
        self.cellImageView.tintColor = color
    }

    // MARK: 

    override public func awakeFromNib() {
        super.awakeFromNib()

        self.headingLabel.text = nil
        self.contentLabel.text = nil

        self.headingLabel.font = UIFont.onboardingCellHeading()
        self.headingLabel.textColor = UIColor.onboardingText()

        self.contentLabel.font = UIFont.onboardingCellContent()
        self.contentLabel.textColor = UIColor.onboardingText()

        self.backgroundColor = .clear
        self.backgroundView?.backgroundColor = .clear
        self.contentView.backgroundColor = .clear
    }

}
