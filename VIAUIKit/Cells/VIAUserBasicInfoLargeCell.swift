//
//  VIAUserBasicInfoLargeCell.swift
//  VitalityActive
//
//  Created by Joshua Ryan on 7/12/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

import UIKit

public class VIAUserBasicInfoLargeCell: VIAUserBasicInfoCell {

	@IBOutlet private weak var editButton: UIButton!
    public var editClosure: () -> (Void)? = {_ in
		debugPrint("editClosure default")
	}
    
    public func getEditButton() -> (UIButton) {
        
        return self.editButton
    }
    
    public func setEditButtonLabel(text: String){
        self.editButton.setTitle(text, for: .normal)
        layoutIfNeeded()
    }

	override public func awakeFromNib() {
		super.awakeFromNib()

        self.nameLabel.font = UIFont.title2Font()
        self.emailLabel.font = UIFont.bodyFont()

		self.editButton.titleLabel?.font = UIFont.subheadlineFont()
		self.editButton.titleLabel?.textColor = UIColor.flatOrange()

        // exception to the font styling rule as this isn't used anywhere else
		self.photoView.textFont = UIFont.systemFont(ofSize: 35.3, weight: UIFontWeightMedium)
	}

	@IBAction func editTapped() {
		self.editClosure()
	}
}
