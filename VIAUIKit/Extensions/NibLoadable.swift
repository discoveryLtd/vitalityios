import Foundation
import UIKit

public protocol Nibloadable: class {
    static func nib() -> UINib
}

extension Nibloadable where Self: UIView {

    public static func nib() -> UINib {
        let bundle = Bundle(for: self)
        let nib = UINib(nibName: String(describing: self), bundle: bundle)
        return nib
    }

    public static func viewFromNib(owner: Any?) -> Self? {
        let bundle = Bundle(for: self)
        let views = bundle.loadNibNamed(String(describing: self), owner: owner, options: nil)
        guard let view = views?.first as? Self else {
            debugPrint("Could not load a view of type \(NSStringFromClass(Self.self)). Is the view class correctly set in interface builder to the type you are expecting?")
            return nil
        }
        return view
    }

    public static func bundle() -> Bundle {
        return Bundle(for: self)
    }
}
