import Foundation
import UIKit

// MARK: ReusableView

public protocol ReusableView: class {}

public extension ReusableView where Self: UITableViewCell {
    public static var defaultReuseIdentifier: String {
        return String(describing: self)
    }
}

public extension ReusableView where Self: UITableViewHeaderFooterView {
    public static var defaultReuseIdentifier: String {
        return String(describing: self)
    }
}

extension UITableViewCell: ReusableView {}
extension UITableViewHeaderFooterView: ReusableView {}

public extension ReusableView where Self: UICollectionViewCell {
    public static var defaultReuseIdentifier: String {
        return String(describing: self)
    }
}

public extension ReusableView where Self: UICollectionReusableView {
    public static var defaultReuseIdentifier: String {
        return String(describing: self)
    }
}

extension UICollectionReusableView: ReusableView {}

// MARK: UITableView

public extension UITableView {
    
    public func setTableHeaderView(headerView: UIView) {
        headerView.translatesAutoresizingMaskIntoConstraints = false
        
        self.tableHeaderView = headerView
        
        // ** Must setup AutoLayout after set tableHeaderView.
        headerView.widthAnchor.constraint(equalTo: self.widthAnchor).isActive = true
        headerView.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        headerView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
    }

    public func shouldUpdateHeaderViewFrame() -> Bool {
        guard let headerView = self.tableHeaderView else { return false }
        let oldSize = headerView.bounds.size
        // Update the size
        headerView.layoutIfNeeded()
        let newSize = headerView.bounds.size
        return oldSize != newSize
    }

    public func sizeHeaderViewToFit() {
        guard let tableHeaderView = self.tableHeaderView else {
            return
        }

        tableHeaderView.setNeedsLayout()
        tableHeaderView.layoutIfNeeded()

        let correctHeight = tableHeaderView.systemLayoutSizeFitting(UILayoutFittingCompressedSize).height

        var frame = tableHeaderView.frame
        frame.size.height = ceil(correctHeight)
        tableHeaderView.frame = frame

        self.tableHeaderView = tableHeaderView
    }

    public func indexPathForCellSubview(subView: UIView) -> IndexPath? {
        var superView: UIView? = nil

        if !(subView is UITableViewCell) {
            superView = subView.superview
            while !(superView is UITableViewCell) {
                superView = superView?.superview
                if superView == nil {
                    return nil
                }
            }
        } else {
            superView = subView
        }

        let cell: UITableViewCell = superView as! UITableViewCell
        let cellIndexPath: IndexPath? = self.indexPath(for: cell)

        return cellIndexPath
    }
}
