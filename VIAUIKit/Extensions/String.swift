//
//  String.swift
//  VitalityActive
//
//  Created by Von Kervin R. Tuico on 15/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.
//

import UIKit

extension String {
    
    func parseDateFromApi() -> Date? {
        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.dateFormat = "yyyy-mm-dd"
        return dayTimePeriodFormatter.date(from: self)
    }
    
}
