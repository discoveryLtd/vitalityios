//
//  UIViewController+VIAAddittions.swift
//  VitalityActive
//
//  Created by Wilmar van Heerden on 2016/11/21.
//  Copyright © 2016 Glucode. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

public extension UIViewController {

    public func resetBackButtonTitle() {
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: self.title, style: .plain, target: nil, action: nil)
    }
    
    public func setBackButtonTitle(_ title: String) {
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: title, style: .plain, target: nil, action: nil)
    }

    public func hideBackButtonTitle(_ vc: UIViewController? = nil) {
        if vc == nil {
            self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        }else{
            vc!.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        }
    }
    
    public func addLeftBarButtonItem(_ title:String, target:Any, selector:Selector){
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: title, style: .plain,
                                                                target: target, action: selector)
    }
    
    public func addRightBarButtonItem(_ title:String, target:Any, selector:Selector){
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: title, style: .plain,
                                                                 target: target, action: selector)
    }
    
    public func removeAllNavigationItems(){
        self.navigationItem.rightBarButtonItem = nil
        self.navigationItem.leftBarButtonItem = nil
        self.navigationItem.backBarButtonItem = nil
    }
    
    public func configureKeyboardAutoToolbar(_ isEnable: Bool) {
        IQKeyboardManager.shared.enable = isEnable
        IQKeyboardManager.shared.enableAutoToolbar = isEnable
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        if isEnable{
            //IQKeyboardManager.shared.preventShowingBottomBlankSpace = true
            IQKeyboardManager.shared.shouldPlayInputClicks = false
            IQKeyboardManager.shared.keyboardDistanceFromTextField = 30
            IQKeyboardManager.shared.shouldShowToolbarPlaceholder = false
        }
    }
    
    public func disableKeyboardAutoToolbar() {
        IQKeyboardManager.shared.enableAutoToolbar = false
    }
    
    public func isKeyboardShowing() -> Bool {
        return IQKeyboardManager.shared.keyboardShowing
    }
}
