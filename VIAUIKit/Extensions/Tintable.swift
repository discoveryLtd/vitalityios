import Foundation
import UIKit

public protocol Tintable {
    func setGlobalTintColor()
}

// MARK: - Tintable protocols

public protocol KnowYourHealthTintable: Tintable { }
public protocol ImproveYourHealthTintable: Tintable { }
public protocol MyHealthTintable: Tintable { }
public protocol GetRewardedTintable: Tintable { }
public protocol PrimaryColorTintable: Tintable { }

// MARK: - Extensions

extension KnowYourHealthTintable {
    public func setGlobalTintColor() {
        VIAAppearance.setGlobalTintColorToKnowYourHealth()
    }
}

extension ImproveYourHealthTintable {
    public func setGlobalTintColor() {
        VIAAppearance.setGlobalTintColorToImproveYourHealth()
    }
}

extension MyHealthTintable {
    public func setGlobalTintColor() {
        VIAAppearance.setGlobalTintColorToMyHealth()
    }
}

extension GetRewardedTintable {
    public func setGlobalTintColor() {
        VIAAppearance.setGlobalTintColorToGetRewarded()
    }
}

extension PrimaryColorTintable {
    public func setGlobalTintColor() {
        VIAAppearance.setGlobalTintColorToPrimaryColor()
    }
}
