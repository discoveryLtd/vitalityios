//
//  UITableViewCell+VIAAdditions.swift
//  VitalityActive
//
//  Created by Wilmar van Heerden on 2016/10/24.
//  Copyright © 2016 Glucode. All rights reserved.
//

import Foundation
import SnapKit

extension VIATableViewCell {

    public func applyDefaultStyling() {
        self.label.font = UIFont.cellStyleMenuItemTextLabel()
    }

    public func applyTitle1Styling() {
        self.label.font = UIFont.cellStyleTitle1Styling()
    }
}
