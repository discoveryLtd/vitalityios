//
//  HUDPresenter.swift
//  VitalityActive
//
//  Created by Wilmar van Heerden on 2016/11/04.
//  Copyright © 2016 Glucode. All rights reserved.
//

import Foundation
import MBProgressHUD

public protocol HUDPresenter {

    func showHUDOnWindow()

    func hideHUDFromWindow()

    func showHUDOnView(view: UIView?)

    func hideHUDFromView(view: UIView?)
    
    func showFullScreenHUD()
    
    func hideFullScreenHUD()
}

public extension HUDPresenter {

    func showHUDOnWindow() {
        guard let window = UIApplication.shared.keyWindow else { return }
        MBProgressHUD.showAdded(to: window, animated: true)
    }

    func hideHUDFromWindow() {
        guard let window = UIApplication.shared.keyWindow else { return }
        MBProgressHUD.hide(for: window, animated: true)
    }

    func showHUDOnView(view: UIView?) {
        guard let view = view else { return }
        view.isUserInteractionEnabled = false
        let progressHUD = MBProgressHUD.showAdded(to: view, animated: true)
        view.bringSubview(toFront: progressHUD)
    }

    func hideHUDFromView(view: UIView?) {
        guard let view = view else { return }
        view.isUserInteractionEnabled = true
        MBProgressHUD.hide(for: view, animated: true)
    }

    func showHud(on view: UIView?, with image: VIAUIKitImage, display text: String) {
        guard let view = view else { return }

        let progressHUD = MBProgressHUD.showAdded(to: view, animated: false)
        progressHUD.label.text = text
        progressHUD.bezelView.color = UIColor.white
        progressHUD.backgroundView.color = UIColor.night()
        progressHUD.backgroundView.alpha = 0.4
        progressHUD.mode = MBProgressHUDMode.customView
        let customView = UIImageView(image: image)
        progressHUD.customView = customView
        view.bringSubview(toFront: progressHUD)
        progressHUD.hide(animated: true, afterDelay: TimeInterval(2))
    }
    
    func showHUDOnWindow(with text: String) {
        guard let window = UIApplication.shared.keyWindow else { return }
        let progressHUD = MBProgressHUD.showAdded(to: window, animated: true)
        progressHUD.label.text = text
    }
}

public extension HUDPresenter where Self:UIViewController {
    func showFullScreenHUD() {
        guard let view = view else { return }
        let screenBounds = UIScreen.main.bounds
        let hudOrigin = CGPoint(x: 0, y: 0)
        var hudSize = CGSize(width: screenBounds.size.width, height: screenBounds.size.height)
        
        hudSize.height -= 20 // Status bar
        
        if let navBarHeight = navigationController?.navigationBar.frame.size.height {
            hudSize.height -= navBarHeight
        }
        
        if let tabBarHeight = tabBarController?.tabBar.frame.size.height {
            hudSize.height -= tabBarHeight
        }
    
        let hudBounds = CGRect(origin: hudOrigin, size: hudSize)
        
        let coveringView = UIView(frame: hudBounds)
        coveringView.tag = 483 // 483 = HUD
        coveringView.backgroundColor = view.backgroundColor
        coveringView.layer.zPosition += 1
        view.addSubview(coveringView)
        
        if let tableView = view as? UITableView {
            tableView.scrollRectToVisible(hudBounds, animated: false)
            tableView.isScrollEnabled = false
        }
        
        showHUDOnView(view: coveringView)
    }
    
    func hideFullScreenHUD() {
        guard let view = view else { return }
        guard let coveringView = view.viewWithTag(483) else { return }
        hideHUDFromView(view: coveringView)
        if let tableView = view as? UITableView {
            tableView.isScrollEnabled = true
        }
        coveringView.removeFromSuperview()
    }
}

extension UIViewController: HUDPresenter {}

extension UIView {
    var parentViewController: UIViewController? {
        var parentResponder: UIResponder? = self
        while parentResponder != nil {
            parentResponder = parentResponder!.next
            if let viewController = parentResponder as? UIViewController {
                return viewController
            }
        }
        return nil
    }
}
