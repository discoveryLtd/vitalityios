//
//  UITextField+VIAAdditions.swift
//  VIAUIKit
//
//  Created by Val Tomol on 10/07/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import IQKeyboardManagerSwift

public extension UITextField {
    
    public func reloadKeyboardToolbar() {
        // Only applicable if IQKeyboardManager is enabled for the market.
        // Add a delay to give time for the keyboard to appear.
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.05) {
            // Reload keyboard toolbar button inputs.
            IQKeyboardManager.shared.reloadInputViews()
        }
    }
}
