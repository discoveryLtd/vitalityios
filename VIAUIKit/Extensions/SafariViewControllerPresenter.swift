//
//  SafariViewControllerBreakout.swift
//  VitalityActive
//
//  Created by Lionel Camacho on 2017/07/06.
//  Copyright © 2017 Glucode. All rights reserved.
//

import UIKit
import SafariServices

public protocol SafariViewControllerPresenter: class {
    func presentModally(url: URL, parentViewController: UIViewController, delegate: SFSafariViewControllerDelegate?)
}

extension SafariViewControllerPresenter {
    public func presentModally(url: URL, parentViewController: UIViewController, delegate: SFSafariViewControllerDelegate? = nil) {
        let safariVC = SFSafariViewController(url: url)
    
        if UIDevice.current.userInterfaceIdiom == .phone {
            safariVC.modalPresentationStyle = .popover
        } else {
            safariVC.modalPresentationStyle = .fullScreen
        }
        
        if let safariDelegate = delegate {
            safariVC.delegate = safariDelegate
            safariVC.preferredControlTintColor = UIColor.currentGlobalTintColor()
        }
        
        if let popoverController = safariVC.popoverPresentationController {
            popoverController.sourceView = parentViewController.view
        }
        
        parentViewController.present(safariVC, animated: true, completion: nil)
    }
}
