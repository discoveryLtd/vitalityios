import UIKit

extension UITableView {

    public func defaultTableViewCell() -> UITableViewCell {
        guard let cell = self.dequeueReusableCell(withIdentifier: "cell") else {
            return UITableViewCell(style: .default, reuseIdentifier: "cell")
        }
        return cell
    }
    
    public func defaultTableViewHeaderFooterView() -> UITableViewHeaderFooterView {
        guard let view = self.dequeueReusableHeaderFooterView(withIdentifier: "headerFooterView") else {
            return UITableViewHeaderFooterView(reuseIdentifier: "headerFooterView")
        }
        return view
    }
    
}
