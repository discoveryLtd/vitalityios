import UIKit
import ChameleonFramework
import VitalityKit

public extension UIColor {

    class func primaryColor() -> UIColor {
        //Get value from BE or from market localization
        if let staticColor = VIAAppearance.default.dataSource?.getInsurerGlobalTint{
            return staticColor
        }else if let color = VIAAppearance.default.dataSource?.primaryColor {
            return color
        }
        return UIColor.night()
    }

    class func currentGlobalTintColor() -> UIColor {
        if let color = UIApplication.shared.keyWindow?.tintColor {
            return color
        }
        return UIColor.primaryColor()
    }

    class func onboardingGreenGradient(frame: CGRect, applyAODAColor: Bool = false) -> UIColor {
        let top = UIColor.onboardingGreenTop(applyAODAColor: applyAODAColor)
        let bottom = UIColor.onboardingGreenBottom(applyAODAColor: applyAODAColor)
        return UIColor(gradientStyle: .topToBottom, withFrame: frame, andColors: [top, bottom])
    }

    class func onboardingBlueGradient(frame: CGRect, applyAODAColor: Bool = false) -> UIColor {
        let top = UIColor.onboardingBlueTop(applyAODAColor: applyAODAColor)
        let bottom = UIColor.onboardingBlueBottom(applyAODAColor: applyAODAColor)
        return UIColor(gradientStyle: .topToBottom, withFrame: frame, andColors: [top, bottom])
    }

    class func onboardingPinkGradient(frame: CGRect) -> UIColor {
        let top = UIColor.onboardingPinkTop()
        let bottom = UIColor.onboardingPinkBottom()
        return UIColor(gradientStyle: .topToBottom, withFrame: frame, andColors: [top, bottom])
    }

    class func blankGradient(frame: CGRect) -> UIColor {
        let top = UIColor.day()
        let bottom = UIColor.day()
        return UIColor(gradientStyle: .topToBottom, withFrame: frame, andColors: [top, bottom])
    }

    class func onboardingText() -> UIColor {
        return dayColor()
    }

    class func cellHeadingLabel() -> UIColor {
        return mediumGreyColor()
    }

    class func cellErrorLabel() -> UIColor {
        return crimsonRedColor()
    }

    class func genericError() -> UIColor {
        return crimsonRedColor()
    }
    
    class func genericErrorAlertButtonText() -> UIColor {
        return UIColor(red: 3 / 255, green: 122 / 255, blue: 255 / 255, alpha: 1.00)
    }

    class func genericInactive() -> UIColor {
        return lightGreyColor()
    }

    class func genericActive() -> UIColor {
        return night()
    }

    class func textFieldPlaceholder() -> UIColor {
        return lightGreyColor()
    }

    class func day() -> UIColor {
        return dayColor()
    }

    class func crimsonRed() -> UIColor {
        return crimsonRedColor()
    }

    class func night() -> UIColor {
        return nightColor()
    }

    class func tableViewHeaderFooter() -> UIColor {
        return mediumGreyColor()
    }

    class func tableViewSectionHeaderFooter() -> UIColor {
        return mediumGreyColor()
    }

    class func mainViewBackground() -> UIColor {
        return UIColor(red: 0.96, green: 0.96, blue: 0.96, alpha: 1.00)
    }

    class func tableViewBackground() -> UIColor {
        return UIColor(red: 0.96, green: 0.96, blue: 0.96, alpha: 1.00)
    }
    class func fieldLabelColor() -> UIColor {
        return UIColor(red: 99.0 / 255.0, green: 99.0 / 255.0, blue: 99.0 / 255.0, alpha: 1.0)
    }
    class func feedbackViewHeadingLabel() -> UIColor {
        return nightColor()
    }

    class func feedbackViewBodyLabel() -> UIColor {
        return nightColor()
    }

    class func feedbackViewBackground() -> UIColor {
        return day()
    }

    class func viewBorder() -> UIColor {
        return lightGreyColor()
    }

    class func cellSeperator() -> UIColor {
        return lightGreyColor()
    }

    class func cellStyleSubtitleTextLabel() -> UIColor {
        return mediumGreyColor()
    }

    class func cellStyleSubtitleDetailTextLabel() -> UIColor {
        return night()
    }

    class func mediumGrey() -> UIColor {
        return mediumGreyColor()
    }

    class func cellButton() -> UIColor {
        return waterBlueColor()
    }

    class func helpSuggestionButton() -> UIColor {
        return waterBlueColor()
    }

    class func lightGrey() -> UIColor {
        return lightGreyColor()
    }

    class func darkGrey() -> UIColor {
        return darkGreyColor()
    }

    class func jungleGreen(applyAODAColor: Bool = false) -> UIColor {
        return jungleGreenColor(applyAODAColor: applyAODAColor)
    }

    class func myHealthOrange() -> UIColor {
        return vitalityOrange()
    }
    
    class func knowYourHealthGreen(applyAODAColor: Bool = false) -> UIColor {
        return jungleGreen(applyAODAColor: applyAODAColor)
    }

    class func improveYourHealthBlue(applyAODAColor: Bool = false) -> UIColor {
        return waterBlueColor(applyAODAColor: applyAODAColor)
    }

    class func getRewardedPink() -> UIColor {
        return salmonPinkColor()
    }

	class func rewardBlue() -> UIColor {
		return waterBlueColor()
	}

    class func homeScreenCardTitleBackground() -> UIColor {
        return UIColor(white: 1.0, alpha:1.00)
    }

    class func mercuryGrey() -> UIColor {
        return mercuryGreyColor()
    }

    class func tealBlue() -> UIColor {
        return tealBlueColor()
    }
    class func deepSeaBlue() -> UIColor {
        return deepSeaBlueColor()
    }
    class func waterBlueThree() -> UIColor {
        return waterBlueThreeColor()
    }
    class func vitalityOrange() -> UIColor {
        return vitalityOrangeColor()
    }
    class func lightBronze() -> UIColor {
        return bronzeLight()
    }
    class func darkBronze() -> UIColor {
        return bronzeDark()
    }
    class func lightGold() -> UIColor {
        return goldLight()
    }
    class func darkGold() -> UIColor {
        return goldDark()
    }
    class func lightSilver() -> UIColor {
        return silverLight()
    }
    class func darkSilver() -> UIColor {
        return silverDark()
    }
    class func lightPlatinum() -> UIColor {
        return platinumLight()
    }
    class func nutritionTop() -> UIColor {
        return nutritionTopColor()
    }
    class func nutritionBottom() -> UIColor {
        return nutritionBottomColor()
    }
    class func stressorBlueTop() -> UIColor {
        return stressorBlueTopColor()
    }
    class func stressorBlueBottom() -> UIColor {
        return stressorBlueBottomColor()
    }
    class func psychologicalBlueTop() -> UIColor {
        return psychologicalBlueTopColor()
    }
    class func psychologicalBlueBottom() -> UIColor {
        return psychologicalBlueBottomColor()
    }
    class func socialBlueTop() -> UIColor {
        return socialBlueTopColor()
    }
    class func socialBlueBottom() -> UIColor {
        return socialBlueBottomColor()
    }
}

//////////////////////////

private extension UIColor {

    class func onboardingBlueTop(applyAODAColor: Bool = false) -> UIColor {
        return applyAODAColor ? UIColor(hexString: "0079AB") : onboardingLightblueColor()
        //return onboardingLightblueColor()
    }

    class func onboardingBlueBottom(applyAODAColor: Bool = false) -> UIColor {
        return applyAODAColor ? UIColor(hexString: "004764") : waterBlueColor()
        //return waterBlueColor()
    }

    class func onboardingGreenTop(applyAODAColor: Bool = false) -> UIColor {
        return jungleGreenColor(applyAODAColor: applyAODAColor)
    }

    class func onboardingGreenBottom(applyAODAColor: Bool = false) -> UIColor {
        return applyAODAColor ? UIColor(hexString: "004438") : turqoiseColor()
        //return turqoiseColor()
    }

    class func onboardingPinkTop() -> UIColor {
        //return applyAODAColor ? UIColor(hexString: "C73067") : pigPinkColor()
        return pigPinkColor()
    }

    class func onboardingPinkBottom() -> UIColor {
        //return applyAODAColor ? UIColor(hexString: "9E124F") : lipstickColor()
        return lipstickColor()
    }

    class func nonSmokersBackgroundStartColor() -> UIColor {
        return UIColor(red: 39.0 / 255.0, green: 224.0 / 255.0, blue: 121.0 / 255.0, alpha: 1.0)
    }
    class func nonSmokersBackgroundEndColor() -> UIColor {
        return UIColor(red: 5.0 / 255.0, green: 198.0 / 255.0, blue: 174.0 / 255.0, alpha: 1.0)
    }

    class func jungleGreenColor(applyAODAColor: Bool = AppSettings.isAODAEnabled()) -> UIColor {
        return applyAODAColor ? UIColor(hexString: "008853") : UIColor(red: 43.0 / 255.0, green: 202.0 / 255.0, blue: 109.0 / 255.0, alpha: 1.0)
    }
    
    class func turqoiseColor() -> UIColor {
        return UIColor(red: 9.0 / 255.0, green: 201.0 / 255.0, blue: 167.0 / 255.0, alpha: 1.0)
    }

    class func deepSeaBlueColor() -> UIColor {
        return UIColor(red: 0 / 255.0, green: 101.0 / 255.0, blue: 128.0 / 255.0, alpha: 1.0)
    }

    class func tealBlueColor() -> UIColor {
        return UIColor(red: 0 / 255.0, green: 147.0 / 255.0, blue: 176.0 / 255.0, alpha: 1.0)
    }

    class func waterBlueColor(applyAODAColor: Bool = AppSettings.isAODAEnabled()) -> UIColor {
        return applyAODAColor ? UIColor(hexString: "0079AB") : UIColor(red: 23.0 / 255.0, green: 170.0 / 255.0, blue: 214.0 / 255.0, alpha: 1.0) 
    }

    class func salmonPinkColor() -> UIColor {
        return UIColor(red: 230.0 / 255.0, green: 108.0 / 255.0, blue: 119.0 / 255.0, alpha: 1.0)
    }

    class func mangoColor() -> UIColor {
        return UIColor(red: 249.0 / 255.0, green: 149.0 / 255.0, blue: 51.0 / 255.0, alpha: 1.0)
    }

    class func vitalityOrangeColor() -> UIColor {
        return UIColor(red: 255.0 / 255.0, green: 110.0 / 255.0, blue: 0.0, alpha: 1.0)
    }

    class func crimsonRedColor() -> UIColor {
        return UIColor(red: 212.0 / 255.0, green: 7.0 / 255.0, blue: 7.0 / 255.0, alpha: 1.0)
    }

    class func nightColor() -> UIColor {
        return UIColor(white: 0.0, alpha: 1.0)
    }

    class func waterBlueThreeColor() -> UIColor {
        return UIColor(red: 26.0 / 255.0, green: 151.0 / 255.0, blue: 223.0 / 255.0, alpha: 1.0)
    }

    class func mediumGreyColor() -> UIColor {
        // MLI AODA Medium Grey (#767676)
        return AppSettings.isAODAEnabled() ? UIColor(hexString: "767676") : UIColor(white: 155.0 / 255.0, alpha: 1.0)
    }

    class func dayColor() -> UIColor {
        return UIColor(white: 255.0 / 255.0, alpha: 1.0)
    }

    class func mercuryGreyColor() -> UIColor {
        return UIColor(white: 245.0 / 255.0, alpha: 1.0)
    }

    class func lightGreyColor() -> UIColor {
        // MLI AODA Light Grey (#D2D2D2)
        return AppSettings.isAODAEnabled() ? UIColor(hexString: "D2D2D2") : UIColor(white: 210.0 / 255.0, alpha: 1.0)
    }

	class func lighterGreyColor() -> UIColor {
		return UIColor(white: 221.0 / 255.0, alpha: 1.0)
	}

    class func darkGreyColor() -> UIColor {
        // MLI AODA Dark Grey (#333333)
        return AppSettings.isAODAEnabled() ? UIColor(hexString: "333333") : UIColor(white: 110.0 / 255.0, alpha: 1.0)
    }

    class func onboardingLightblueColor() -> UIColor {
        return UIColor(red: 80.0 / 255.0, green: 204.0 / 255.0, blue: 241.0 / 255.0, alpha: 1.0)
    }

    class func weirdGreenColor() -> UIColor {
        return UIColor(red: 73.0 / 255.0, green: 234.0 / 255.0, blue: 140.0 / 255.0, alpha: 1.0)
    }

    class func tealishColor() -> UIColor {
        return UIColor(red: 43.0 / 255.0, green: 202.0 / 255.0, blue: 187.0 / 255.0, alpha: 1.0)
    }

    class func pigPinkColor() -> UIColor {
        return UIColor(red: 230.0 / 255.0, green: 108.0 / 255.0, blue: 170.0 / 255.0, alpha: 1.0)
    }

    class func lipstickColor() -> UIColor {
        return UIColor(red: 232.0 / 255.0, green: 52.0 / 255.0, blue: 88.0 / 255.0, alpha: 1.0)
    }
    class func bronzeLight() -> UIColor {
        return UIColor(red: 209.0 / 255.0, green: 142.0 / 255.0, blue: 0.0, alpha: 1.0)
    }
    
    class func bronzeDark() -> UIColor {
        return UIColor(red: 168.0 / 255.0, green: 107.0 / 255.0, blue: 9.0 / 255.0, alpha: 1.0)
    }
    
    class func goldLight() -> UIColor {
        return UIColor(red: 255.0 / 255.0, green: 195.0 / 255.0, blue: 13.0 / 255.0, alpha: 1.0)
    }
    
    class func goldDark() -> UIColor {
        return UIColor(red: 226.0 / 255.0, green: 161.0 / 255.0, blue: 16.0 / 255.0, alpha: 1.0)
    }
    
    class func platinumLight() -> UIColor {
        return UIColor(white: 202.0 / 255.0, alpha: 1.0)
    }
    
    class func silverLight() -> UIColor {
        return UIColor(red: 164.0 / 255.0, green: 168.0 / 255.0, blue: 171.0 / 255.0, alpha: 1.0)
    }
    
    class func silverDark() -> UIColor {
        return UIColor(red: 144.0 / 255.0, green: 144.0 / 255.0, blue: 145.0 / 255.0, alpha: 1.0)
    }
    
    class func nutritionTopColor() -> UIColor {
        return UIColor(red: 143.0 / 255.0, green: 44.0 / 255.0, blue: 138.0 / 255.0, alpha: 1.0)
    }
    
    class func nutritionBottomColor() -> UIColor {
        return UIColor(red: 91.0 / 255.0, green: 40.0 / 255.0, blue: 134.0 / 255.0, alpha: 1.0)
    }
    
    class func stressorBlueTopColor() -> UIColor {
        return UIColor(red: 69.0 / 255.0, green: 175.0 / 255.0, blue: 240.0 / 255.0, alpha: 1.0)
    }
    
    class func stressorBlueBottomColor() -> UIColor {
        return UIColor(red: 28.0 / 255.0, green: 119.0 / 255.0, blue: 237.0 / 255.0, alpha: 1.0)
    }
    
    class func psychologicalBlueTopColor() -> UIColor {
        return UIColor(red: 0.0 / 255.0, green: 183.0 / 255.0, blue: 223.0 / 255.0, alpha: 1.0)
    }
    
    class func psychologicalBlueBottomColor() -> UIColor {
        return UIColor(red: 25.0 / 255.0, green: 82.0 / 255.0, blue: 179.0 / 255.0, alpha: 1.0)
    }
    
    class func socialBlueTopColor() -> UIColor {
        return UIColor(red: 21.0 / 255.0, green: 112.0 / 255.0, blue: 218.0 / 255.0, alpha: 1.0)
    }
    
    class func socialBlueBottomColor() -> UIColor {
        return UIColor(red: 7.0 / 255.0, green: 47.0 / 255.0, blue: 138.0 / 255.0, alpha: 1.0)
    }
}

//////////////////////////

public extension UIColor {
    class func RingChartStartBlue () -> UIColor {
        return UIColor(red: 51 / 255, green: 213 / 255, blue: 237 / 255, alpha: 1)
    }
    class func RingChartEndBlue() -> UIColor {
        return UIColor(red: 23 / 255.0, green: 170 / 255.0, blue: 214 / 255.0, alpha: 1.0)
    }
    class func RingCountingLabelTextColor () -> UIColor {
        return UIColor(red: 23 / 255.0, green: 170 / 255.0, blue: 214 / 255.0, alpha: 1.0)
    }

    public func rgb() -> (red: Int, green: Int, blue: Int, alpha: Int)? {
        var fRed: CGFloat = 0
        var fGreen: CGFloat = 0
        var fBlue: CGFloat = 0
        var fAlpha: CGFloat = 0
        if self.getRed(&fRed, green: &fGreen, blue: &fBlue, alpha: &fAlpha) {
            let iRed = Int(fRed * 255.0)
            let iGreen = Int(fGreen * 255.0)
            let iBlue = Int(fBlue * 255.0)
            let iAlpha = Int(fAlpha * 255.0)

            return (red:iRed, green:iGreen, blue:iBlue, alpha:iAlpha)
        } else {
            // Could not extract RGBA components:
            return nil
        }
    }

}
