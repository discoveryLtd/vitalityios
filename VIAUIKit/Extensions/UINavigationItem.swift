import UIKit

extension UINavigationItem {
    
    public func setTitle(title:String, subtitle:String) {
        
        let one = UILabel()
        one.text = title
        one.font = UIFont.headlineFont()
        one.textColor = UIColor.night()
        one.sizeToFit()
        
        let two = UILabel()
        two.text = subtitle
        two.font = UIFont.footnoteFont()
        two.textColor = UIColor.mediumGrey()
        two.textAlignment = .center
        two.sizeToFit()
        
        
        
        let stackView = UIStackView(arrangedSubviews: [one, two])
        stackView.distribution = .equalCentering
        stackView.axis = .vertical
        
        let width = max(one.frame.size.width, two.frame.size.width)
        stackView.frame = CGRect(x: 0, y: 0, width: width, height: 35)
        
        one.sizeToFit()
        two.sizeToFit()
        
        self.titleView = stackView
    }
}
