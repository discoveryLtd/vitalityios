import Foundation
import UIKit

public extension UINavigationController {

    public func makeNavigationBarInvisible() {
        self.navigationBar.barTintColor = nil
        self.makeNavigationBarInvisibleCommonStyle()
        self.navigationBar.tintColor = UIColor.day()
    }
    private func makeNavigationBarInvisibleCommonStyle() {
        self.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationBar.shadowImage = UIImage()

    }
    public func makeNavigationBarInvisibleWithoutTint() {
        self.makeNavigationBarInvisibleCommonStyle()
    }
    
    public func makeNavigationBarTransparent() {
        self.navigationBar.barTintColor = nil
        self.navigationBar.setBackgroundImage(nil, for: .default)
        self.navigationBar.shadowImage = nil

        self.navigationBar.tintColor = UIColor.currentGlobalTintColor()
    }

}
