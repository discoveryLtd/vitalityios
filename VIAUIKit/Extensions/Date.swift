//
//  Date.swift
//  VitalityActive
//
//  Created by Von Kervin R. Tuico on 15/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.
//

import UIKit

extension Date {
    
    func toApiFormat() -> String {
        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.dateFormat = "yyyyMMdd"
        let dateString = dayTimePeriodFormatter.string(from: self)
        return dateString
    }
    
    func toHumanReadableFormat() -> String {
        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.dateFormat = "E, dd MMM yyyy"
        let dateString = dayTimePeriodFormatter.string(from: self)
        return dateString
    }
    
}
