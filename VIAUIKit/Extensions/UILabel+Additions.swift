//
//  UILabel+Additions.swift
//  VitalityActive
//
//  Created by OJ Garde on 4/23/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import UIKit

extension UILabel {
    func setHTML(text: String) {
        let modifiedFont = "<meta charset=\"utf-8\"><span style=\"font-family: \(self.font!.fontName); font-size: \(self.font!.pointSize)\">\(text)</span>"
        do {
            let at : NSMutableAttributedString = try NSMutableAttributedString(data: modifiedFont.data(using: .utf8)!,
                                                                 options: [NSDocumentTypeDocumentAttribute : NSHTMLTextDocumentType],
                                                                 documentAttributes: nil)
            self.attributedText = at
        } catch {
            self.text = modifiedFont
        }
    }
}
