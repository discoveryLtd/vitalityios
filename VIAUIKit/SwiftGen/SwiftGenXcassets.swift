// Generated using SwiftGen, by O.Halligon — https://github.com/SwiftGen/SwiftGen

#if os(OSX)
import AppKit.NSImage
public typealias VIAUIKitColor = NSColor
public typealias VIAUIKitImage = NSImage
#elseif os(iOS) || os(tvOS) || os(watchOS)
import UIKit.UIImage
public typealias VIAUIKitColor = UIColor
public typealias VIAUIKitImage = UIImage
#endif

// swiftlint:disable file_length

public typealias VIAUIKitAssetType = VIAUIKitImageAsset

public struct VIAUIKitImageAsset: Equatable {
	fileprivate var name: String

	public var templateImage: VIAUIKitImage {
		let bundle = Bundle(for: BundleToken.self)
		#if os(iOS) || os(tvOS)
		let image = VIAUIKitImage(named: name, in: bundle, compatibleWith: nil)?.withRenderingMode(.alwaysTemplate)
		#elseif os(OSX)
		let image = bundle.image(forResource: name).withRenderingMode(.alwaysTemplate)
		#elseif os(watchOS)
		let image = VIAUIKitImage(named: name).withRenderingMode(.alwaysTemplate)
		#endif
		guard let result = image else { fatalError("Unable to load template image named \(name).") }
		return result
	}

	public var image: VIAUIKitImage {
		let bundle = Bundle(for: BundleToken.self)
		#if os(iOS) || os(tvOS)
		let image = VIAUIKitImage(named: name, in: bundle, compatibleWith: nil)
		#elseif os(OSX)
		let image = bundle.image(forResource: name)
		#elseif os(watchOS)
		let image = VIAUIKitImage(named: name)
		#endif
		guard let result = image else { fatalError("Unable to load image named \(name).") }
		return result
	}

	public static func ==(lhs: VIAUIKitImageAsset, rhs: VIAUIKitImageAsset) -> Bool {
        return lhs.image == rhs.image
    }
}

public struct VIAUIKitColorAsset {
fileprivate var name: String

#if swift(>=3.2)
@available(iOS 11.0, tvOS 11.0, watchOS 4.0, OSX 10.13, *)
var color: VIAUIKitColor {
return VIAUIKitColor(asset: self)
}
#endif
}

// swiftlint:disable identifier_name line_length nesting type_body_length type_name
public enum VIAUIKitAsset {
  public enum Sv {
    public static let savScreenings = VIAUIKitImageAsset(name: "savScreenings")
    public static let savPointsInfo = VIAUIKitImageAsset(name: "savPointsInfo")
    public static let savVaccinations = VIAUIKitImageAsset(name: "savVaccinations")
  }
  public static let arrowDrill = VIAUIKitImageAsset(name: "arrowDrill")
  public static let eyeOpen = VIAUIKitImageAsset(name: "eyeOpen")
  public enum Status {
    public static let statusPointsSmall = VIAUIKitImageAsset(name: "statusPointsSmall")
    public static let badgePlatinumSmall = VIAUIKitImageAsset(name: "badgePlatinumSmall")
    public static let badgeGoldLarge = VIAUIKitImageAsset(name: "badgeGoldLarge")
    public static let badgeBronzeLarge = VIAUIKitImageAsset(name: "badgeBronzeLarge")
    public static let badgeSilverLarge = VIAUIKitImageAsset(name: "badgeSilverLarge")
    public static let badgeBlueSmall = VIAUIKitImageAsset(name: "badgeBlueSmall")
    public static let questionMarkSmall = VIAUIKitImageAsset(name: "questionMarkSmall")
    public static let statusHelp = VIAUIKitImageAsset(name: "statusHelp")
    public static let badgePlatinumLarge = VIAUIKitImageAsset(name: "badgePlatinumLarge")
    public static let pointsSmall = VIAUIKitImageAsset(name: "pointsSmall")
    public static let badgeSilverSmall = VIAUIKitImageAsset(name: "badgeSilverSmall")
    public static let badgeBlueLarge = VIAUIKitImageAsset(name: "badgeBlueLarge")
    public static let badgeBronzeSmall = VIAUIKitImageAsset(name: "badgeBronzeSmall")
    public static let badgeGoldSmall = VIAUIKitImageAsset(name: "badgeGoldSmall")
  }
  public static let checkMarkCircle = VIAUIKitImageAsset(name: "checkMarkCircle")
  public enum ActiveRewards {
    public static let goalAchievedSmall = VIAUIKitImageAsset(name: "goalAchievedSmall")
    public enum Onboarding {
      public static let arOnboardingActivated = VIAUIKitImageAsset(name: "AROnboardingActivated")
    }
  }
  public static let eyeClosed = VIAUIKitImageAsset(name: "eyeClosed")
  public enum WellnessDevices {
    public static let wellnessDevicesPoints = VIAUIKitImageAsset(name: "wellnessDevicesPoints")
  }

  // swiftlint:disable trailing_comma
  public static let allColors: [VIAUIKitColorAsset] = [
  ]
  public static let allImages: [VIAUIKitImageAsset] = [
    Sv.savScreenings,
    Sv.savPointsInfo,
    Sv.savVaccinations,
    arrowDrill,
    eyeOpen,
    Status.statusPointsSmall,
    Status.badgePlatinumSmall,
    Status.badgeGoldLarge,
    Status.badgeBronzeLarge,
    Status.badgeSilverLarge,
    Status.badgeBlueSmall,
    Status.questionMarkSmall,
    Status.statusHelp,
    Status.badgePlatinumLarge,
    Status.pointsSmall,
    Status.badgeSilverSmall,
    Status.badgeBlueLarge,
    Status.badgeBronzeSmall,
    Status.badgeGoldSmall,
    checkMarkCircle,
    ActiveRewards.goalAchievedSmall,
    ActiveRewards.Onboarding.arOnboardingActivated,
    eyeClosed,
    WellnessDevices.wellnessDevicesPoints,
  ]
  // swiftlint:enable trailing_comma
  @available(*, deprecated, renamed: "allImages")
  public static let allValues: [VIAUIKitAssetType] = allImages
}
// swiftlint:enable identifier_name line_length nesting type_body_length type_name

extension VIAUIKitImage {
@available(iOS 1.0, tvOS 1.0, watchOS 1.0, *)
@available(OSX, deprecated,
message: "This initializer is unsafe on macOS, please use the VIAUIKitImageAsset.image property")
convenience init!(asset: VIAUIKitAssetType) {
#if os(iOS) || os(tvOS)
let bundle = Bundle(for: BundleToken.self)
self.init(named: asset.name, in: bundle, compatibleWith: nil)
#elseif os(OSX) || os(watchOS)
self.init(named: asset.name)
#endif
}
}

extension VIAUIKitColor {
#if swift(>=3.2)
@available(iOS 11.0, tvOS 11.0, watchOS 4.0, OSX 10.13, *)
convenience init!(asset: VIAUIKitColorAsset) {
let bundle = Bundle(for: BundleToken.self)
#if os(iOS) || os(tvOS)
self.init(named: asset.name, in: bundle, compatibleWith: nil)
#elseif os(OSX)
self.init(named: asset.name, bundle: bundle)
#elseif os(watchOS)
self.init(named: asset.name)
#endif
}
#endif
}

private final class BundleToken {}
