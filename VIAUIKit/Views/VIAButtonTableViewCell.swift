//
//  VIAButtonTableViewCell.swift
//  VitalityActive
//
//  Created by Wilmar van Heerden on 2016/09/23.
//  Copyright © 2016 Glucode. All rights reserved.
//

import UIKit

public class VIAButtonTableViewCell: UITableViewCell, Nibloadable {

    public var buttonAction: (() -> Void)?

    @IBOutlet weak var button: VIAButton!

    public override func awakeFromNib() {
        super.awakeFromNib()

        self.button.addTarget(self, action: #selector(buttonTapped(_:)), for: .touchUpInside)
    }

    public func setTitleText(title: String?) {
        self.button.setTitle(title, for: .normal)
    }

    func buttonTapped(_ sender: UIButton) {
        guard let action = self.buttonAction else {
            return
        }
        action()
    }
}
