import UIKit
import TTTAttributedLabel

public enum VIATableViewHeaderFooterTextViewLocation {
    case top
    case bottom
    case neutral
}

public class VIATableViewHeaderFooterTextView: UIView, Nibloadable {

    let defaultPadding: CGFloat = 10

    @IBOutlet weak var label: TTTAttributedLabel!
    @IBOutlet weak var topMarginConstraint: NSLayoutConstraint!
    @IBOutlet weak var bottomMarginConstraint: NSLayoutConstraint!

    public var location: VIATableViewHeaderFooterTextViewLocation = .neutral {
        didSet {
            switch self.location {
            case .neutral:
                self.topMarginConstraint.constant = defaultPadding
                self.bottomMarginConstraint.constant = defaultPadding
                break
            case .top:
                self.topMarginConstraint.constant = defaultPadding * 2
                self.bottomMarginConstraint.constant = defaultPadding
                break
            case .bottom:
                self.topMarginConstraint.constant = defaultPadding
                self.bottomMarginConstraint.constant = defaultPadding * 2
                break
            }
        }
    }

    public override func awakeFromNib() {
        super.awakeFromNib()

        self.setup()
        self.location = .neutral
    }

    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    func setup() {
        self.backgroundColor = UIColor.tableViewBackground()
        self.label.numberOfLines = 0
        self.label.textColor = UIColor.tableViewHeaderFooter()
        self.label.textAlignment = .left
        self.label.backgroundColor = UIColor.tableViewBackground()
        self.label.font = UIFont.tableViewHeaderFooter()
    }

    public override func layoutSubviews() {
        super.layoutSubviews()

        self.label.preferredMaxLayoutWidth = self.label.frame.size.width
    }

    public func setLabelText(text: String?) {
        self.label.text = text
    }
}
