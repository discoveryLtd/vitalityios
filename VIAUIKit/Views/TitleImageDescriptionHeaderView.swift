//
//  TitleImageDescriptionHeaderView.swift
//  VitalityActive
//
//  Created by OJ Garde on 11/11/2017.
//  Copyright © 2017 Glucode. All rights reserved.
//

import UIKit

public class TitleImageDescriptionHeaderView: UITableViewHeaderFooterView, Nibloadable {
    
    @IBOutlet weak var headerImage: UIImageView!
    @IBOutlet weak var headerImageViewWidth: NSLayoutConstraint!
    @IBOutlet weak var headerImageViewTrailing: NSLayoutConstraint!
    
    public var image: UIImage? {
        set {
            headerImage.isHidden = false
            headerImage.image = newValue
        }
        get {
            return headerImage.image
        }
    }
    
    @IBOutlet weak var titleLabel: UILabel!
    public var title: String? {
        set {
            titleLabel.text = newValue
        }
        get {
            return titleLabel.text
        }
    }
    
    @IBOutlet weak var descriptionLabel: UILabel!
    public var descriptionText: String? {
        set {
            descriptionLabel.text = newValue
        }
        get {
            return descriptionLabel.text
        }
    }
    
    public override func awakeFromNib() {
        super.awakeFromNib()
        setupView()
    }
    
    func setupView() {
        //self.contentView.backgroundColor = .clear
        if image == nil {
            headerImage.isHidden = true
            removeHeaderImage()
        } else {
            headerImage.isHidden = false
        }
        
        titleLabel.textColor                = UIColor.night()
        titleLabel.font                     = UIFont.title2Font()
        titleLabel.backgroundColor          = .clear
        
        descriptionLabel.textColor          = UIColor.cellStyleSubtitleTextLabel()
        descriptionLabel.font               = UIFont.subheadlineFont()
        descriptionLabel.backgroundColor    = .clear
    }
    
    func removeHeaderImage() {
        headerImage.image = nil
        headerImageViewWidth.constant = 0
        headerImageViewTrailing.constant = 0
    }
}

