//
//  DataPrivacyHeaderView.swift
//  VitalityActive
//
//  Created by Val Tomol on 14/11/2017.
//  Copyright © 2017 Glucode. All rights reserved.
//

import UIKit
import TTTAttributedLabel

public class DataPrivacyHeaderView: UIView, Nibloadable {
    
    @IBOutlet weak var headerLabel: TTTAttributedLabel!
    @IBOutlet weak var contentLabel: TTTAttributedLabel!
    
    public var header: String? {
        get {
            return self.headerLabel.text as? String
        }
        set {
            self.headerLabel.text = newValue
        }
    }
    
    public var content: String? {
        get {
            return self.contentLabel.text as? String
        }
        set {
            self.contentLabel.text = newValue
        }
    }
    
    public  override func awakeFromNib() {
        super.awakeFromNib()
        self.setup()
    }
    
    func setup () {
        self.headerLabel.text = nil
        self.contentLabel.text = nil
        
        self.backgroundColor = UIColor.tableViewBackground()
        
        self.headerLabel.font = UIFont.title3Font()
        self.headerLabel.numberOfLines = 0
        self.headerLabel.textAlignment = .left
        self.headerLabel.backgroundColor = UIColor.tableViewBackground()
        self.headerLabel.textColor = UIColor.night()
        
        self.contentLabel.font = UIFont.tableViewHeaderFooter()
        self.contentLabel.numberOfLines = 0
        self.contentLabel.textAlignment = .left
        self.contentLabel.backgroundColor = UIColor.tableViewBackground()
        self.contentLabel.textColor = UIColor.tableViewHeaderFooter()
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        
        self.headerLabel.preferredMaxLayoutWidth = self.headerLabel.frame.size.width
        self.contentLabel.preferredMaxLayoutWidth = self.contentLabel.frame.size.width
    }
    
    public func setHeaderTextAlignment(_ alignment: NSTextAlignment){
        self.headerLabel.textAlignment = alignment
    }
    
    public func setContentTextAlignment(_ alignment: NSTextAlignment){
        self.contentLabel.textAlignment = alignment
    }
}
