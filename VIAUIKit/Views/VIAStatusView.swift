//
//  VIAStatusView.swift
//  VitalityActive
//
//  Created by Wilmar van Heerden on 2016/11/21.
//  Copyright © 2016 Glucode. All rights reserved.
//

import Foundation
import UIKit

public class VIAStatusView: UIView, Nibloadable {

    @IBOutlet var headingLabel: UILabel!
    @IBOutlet var messageLabel: UILabel!
    @IBOutlet var button: VIAButton!

    public var hideButtonBorder = true {
        didSet {
            self.button.hidesBorder = hideButtonBorder
        }
    }

    public var heading: String? {
        set {
            self.headingLabel.text = newValue
        }
        get {
            return self.headingLabel.text
        }
    }

    public var message: String? {
        set {
            self.messageLabel.text = newValue
        }
        get {
            return self.messageLabel.text
        }
    }

    public var buttonTitle: String? {
        set {
            self.button.setTitle(newValue, for: .normal)
        }
        get {
            return self.button.title(for: .normal)
        }
    }

    public var actionBlock: (() -> Void) = {
        // no default implementation
    }

    public override func awakeFromNib() {
        super.awakeFromNib()

        self.backgroundColor = UIColor.mainViewBackground()

        self.headingLabel.text = nil
        self.messageLabel.text = nil

        self.headingLabel.font = UIFont.title2Font()
        self.headingLabel.textColor = UIColor.night()
        self.headingLabel.textAlignment = .center
        self.headingLabel.numberOfLines = 0

        self.messageLabel.font = UIFont.bodyFont()
        self.messageLabel.textColor = UIColor.night()
        self.messageLabel.textAlignment = .center
        self.messageLabel.numberOfLines = 0

        self.button.tintColor = UIColor.currentGlobalTintColor()
        self.button.hidesBorder = hideButtonBorder
        self.button.highlightedTextColor = self.button.tintColor
        self.button.titleLabel?.font = UIFont.tableViewFooterLabelButton()
    }

    @IBAction func buttonTapped(_ sender: Any) {
        actionBlock()
    }
}

extension UIViewController {
    open func configureStatusView(_ view: UIView?) {
        removeStatusView()
        
        guard let view = view else { return }
        
        self.view.addSubview(view)
        
        var offset: CGFloat = 0
        if let tableView = self.view as? UITableView {
            offset = tableView.contentOffset.y
        }
        
        view.snp.makeConstraints { (make) in
            make.width.equalToSuperview()
            make.left.right.top.equalToSuperview()
            make.height.equalToSuperview().offset(offset)
        }
    }
    
    open func removeStatusView() {
        self.view.subviews.forEach { (view) in
            if view is VIAStatusView {
                view.removeFromSuperview()
            }
        }
    }
}

