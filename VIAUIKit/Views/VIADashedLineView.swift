//
//  VIADashedLineView.swift
//  VitalityActive
//
//  Created by Joshua Ryan on 9/8/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

import UIKit


public final class VIADashedLineView: UIView {

	var dashLength = CGFloat(8.0)
	var gapLength = CGFloat(5.0)
	var dashColor = UIColor.lightGrey()
	var phase = CGFloat(0.0)

	override public func draw(_ rect: CGRect) {

		dashColor.set()

		let path = UIBezierPath()

		let p0 = CGPoint(x: self.bounds.midX, y: self.bounds.minY)
		path.move(to: p0)

		let p1 = CGPoint(x: self.bounds.midX, y: self.bounds.maxY)
		path.addLine(to: p1)

		path.lineWidth = self.bounds.maxX - self.bounds.minX

		let dashes: [ CGFloat ] = [ dashLength, gapLength ]
		path.setLineDash(dashes, count: dashes.count, phase: phase)
		path.lineCapStyle = .square

		path.stroke()

	}

	required public init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
	}
}
