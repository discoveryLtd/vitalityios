import UIKit

public class AddWeblinkView: UIView, Nibloadable {

    @IBOutlet weak var headerImageView: UIImageView!
    @IBOutlet weak var headerTitleLabel: UILabel!
    @IBOutlet weak var headerMessageLabel: UILabel!

    public override func awakeFromNib() {
        self.headerTitleLabel.font = UIFont.title2Font()
        self.headerMessageLabel.font = UIFont.bodyFont()
    }

    public func setHeader(image: UIImage?) {
        self.headerImageView.tintColor = UIColor.currentGlobalTintColor()
        self.headerImageView.image = image
    }

    public func setHeader(title: String) {
        self.headerTitleLabel.text = title
    }

    public func setDetail(message: String) {
        self.headerMessageLabel.text = message
    }
}
