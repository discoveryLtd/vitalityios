//
//  PreferencesButtonCell.swift
//  VitalityActive
//
//  Created by Marius Janse van Vuuren on 2017/02/15.
//  Copyright © 2017 Glucode. All rights reserved.
//

import UIKit

public class PreferencesButtonCell: UITableViewCell, Nibloadable {
    @IBOutlet weak var informationTitle: UILabel!
    @IBOutlet weak public var actionButton: VIAButton!

    var buttonPressedAction: () -> (Void) = {
        debugPrint("PressedButton")
    }

    @IBAction func actionButtonTapped(_ sender: Any) {
        buttonPressedAction()
    }

    public override func awakeFromNib() {
        super.awakeFromNib()
        self.setup()
    }

    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    public func setup() {
        self.backgroundColor = UIColor.day()
        self.setupInformationTitle()
        self.setupActionButton()
    }

    func setupInformationTitle() {
        self.informationTitle.numberOfLines = 0
        self.informationTitle.textColor = UIColor.darkGray
        self.informationTitle.textAlignment = .left
        self.informationTitle.font = UIFont.subheadlineFont()
        self.informationTitle.textAlignment = .center
    }

    func setupActionButton() {
        self.bringSubview(toFront: self.actionButton)
    }

    public func setActionButton(title: String) {
        self.actionButton.setTitle(title, for: .normal)
    }

    public func setInformationLabel(text: String) {
        self.informationTitle.text = text
    }

    public func setButtonPressed(action: @escaping () -> (Void)) {
        self.buttonPressedAction = action
    }
}
