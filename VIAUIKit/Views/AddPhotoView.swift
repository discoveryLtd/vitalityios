import UIKit
import VitalityKit

public protocol AddPhotoViewDelegate: class {
    func showPhotoPicker()
}

public class AddPhotoView: UIView, Nibloadable {

    @IBOutlet weak var headerImageView: UIImageView!
    @IBOutlet weak var headerTitleLabel: UILabel!
    @IBOutlet weak var headerMessageLabel: UILabel!
    @IBOutlet weak var addPhotoButton: VIAButton!
    weak var delgate: AddPhotoViewDelegate?

    public override func awakeFromNib() {
        self.headerTitleLabel.font = UIFont.title2Font()
        self.headerMessageLabel.font = UIFont.bodyFont()
        self.addPhotoButton.setTitleColor(UIColor.day(), for: UIControlState.selected)
        self.addPhotoButton.tintColor = UIColor.currentGlobalTintColor()
    }

    public func setHeader(image: UIImage?) {
        self.headerImageView.tintColor = AppSettings.isAODAEnabled() ? UIColor.knowYourHealthGreen() : UIColor.currentGlobalTintColor()
        self.headerImageView.image = image
    }

    public func setHeader(title: String) {
        self.headerTitleLabel.text = title
    }

    public func setDetail(message: String) {
        self.headerMessageLabel.text = message
    }

    public func setAction(title: String) {
        self.addPhotoButton.setTitle(title, for: .normal)
    }

    @IBAction func tappedAddPhotoButton(_ sender: Any) {
        self.delgate?.showPhotoPicker()
    }

    public func set(photoViewDelegate: AddPhotoViewDelegate) {
        self.delgate = photoViewDelegate
    }
    
    public func getAddPhotoButton() -> VIAButton{
        return addPhotoButton
    }
}
