//
//  VIAButton.swift
//  VitalityActive
//
//  Created by Wilmar van Heerden on 2016/09/23.
//  Copyright © 2016 Glucode. All rights reserved.
//

import UIKit
import ChameleonFramework
import VitalityKit

public class VIAButton: UIButton {

    public var hidesBorder: Bool = false {
        didSet {
            self.configureBorderWidth()
        }
    }

    public override var tintColor: UIColor! {
        didSet {
            self.configureBorderColor()
            self.configureTitleLabelColor()
        }
    }

    public var highlightedTextColor: UIColor = UIColor.day() {
        didSet {
            self.configureTitleLabelColor()
        }
    }

    // MARK: Init

    public convenience init(title: String?) {
        self.init(frame: .zero)
        self.translatesAutoresizingMaskIntoConstraints = false
        self.setTitle(title, for: .normal)
        self.configure()
    }

    public override init(frame: CGRect) {
        super.init(frame: frame)
        self.configure()
    }

    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.configure()
    }

    public override var intrinsicContentSize: CGSize {
        if let size = self.titleLabel?.intrinsicContentSize {
            return size
        }
        return self.frame.size
    }

    public override func layoutSubviews() {
        super.layoutSubviews()
        self.titleLabel?.preferredMaxLayoutWidth = self.frame.size.width
    }

    // MARK: Colors

    func borderColor(toggled: Bool) -> UIColor? {
        if self.hidesBorder {
            return .clear
        }
        return self.tintColor
    }

    // MARK: Config, called once

    func configure() {
        if AppSettings.getAppTenant() != .MLI{
            self.configureTitleLabel()
        }
        self.configureBorder()
        self.tintColor = UIColor.primaryColor()
    }

    func configureTitleLabel() {
        self.titleLabel?.font = UIFont.roundedButtonTitle()
        self.titleLabel?.lineBreakMode = .byWordWrapping
        self.configureTitleLabelColor()
    }

    func configureBorder() {
        self.layer.cornerRadius = 5
        self.configureBorderColor()
        self.configureBorderWidth()
    }

    // MARK: Config, called more than once

    func configureTitleLabelColor() {
        self.setTitleColor(self.tintColor, for: .normal)
        self.setTitleColor(self.highlightedTextColor, for: .highlighted)
        self.setTitleColor(self.highlightedTextColor, for: .selected)
    }

    func configureBorderColor() {
        self.layer.borderColor = self.borderColor(toggled: false)?.cgColor
    }

    func configureBorderWidth() {
        self.layer.borderWidth = self.hidesBorder ? 0 : 1
    }

    // MARK: Highlighted, selected states

    func toggleHighlightedSelected(toggle isToggled: Bool) {
        // if no border, just fade the button somewhat
        if self.hidesBorder {
            self.toggleHighlightedSelectedForHiddenBorder(toggle: isToggled)
            // otherwise fill and invert
        } else {
            self.toggleHighlightedSelectedForVisibleBorder(toggle: isToggled)
        }
    }

    func toggleHighlightedSelectedForVisibleBorder(toggle: Bool) {
        self.layer.borderColor = self.borderColor(toggled: toggle)?.cgColor
        if toggle {
            self.backgroundColor = self.tintColor
             self.titleLabel?.textColor = self.highlightedTextColor.withAlphaComponent(0.5)
        } else {
            DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(50), execute: {
                self.backgroundColor = .clear
                self.titleLabel?.textColor = self.tintColor
            })
        }
    }

    func toggleHighlightedSelectedForHiddenBorder(toggle: Bool) {
        if toggle {
            self.titleLabel?.textColor = self.highlightedTextColor.withAlphaComponent(0.5)
        } else {
            DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(50), execute: {
                self.titleLabel?.textColor = self.tintColor
            })
        }
    }

    public override var isSelected: Bool {
        didSet {
            self.toggleHighlightedSelected(toggle: isSelected)
        }
    }

    public override var isHighlighted: Bool {
        didSet {
            self.toggleHighlightedSelected(toggle: isHighlighted)
        }
    }

    public override var isEnabled: Bool {
        didSet {
            self.alpha = isEnabled ? 1.0 : 0.5
        }
    }
}
