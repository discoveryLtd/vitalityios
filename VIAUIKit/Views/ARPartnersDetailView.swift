//
//  ARPartnersDetailView.swift
//  VitalityActive
//
//  Created by kevin.e.b.muscat on 03/05/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import UIKit

public class ARPartnersDetailView: UIView, Nibloadable {
    
    @IBOutlet weak var partnerImageView: UIImageView!
    @IBOutlet weak var descriptionTextView: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        
        let view = Bundle.main.loadNibNamed("ARPartnersDetailView", owner: self, options: nil)?[0] as! UIView
        self.addSubview(view)
    }
    
    public override func awakeFromNib() {
        super.awakeFromNib()
        
        self.descriptionTextView?.text = nil
        self.partnerImageView?.image = nil
        
        self.descriptionTextView?.numberOfLines = 0
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        self.frame = bounds
    }
    
    public var partnerDescriptionText: String = "" {
        didSet {
            self.descriptionTextView?.text = self.arPartnerDescriptionText
        }
    }
    public var partnerImage: UIImage? {
        didSet {
            self.partnerImageView?.image = self.arPartnerImage
        }
    }
    
    public var arPartnerImage: UIImage? {
        set {
            partnerImageView.isHidden = false
            partnerImageView.image = newValue
        }
        get {
            return partnerImageView.image
        }
    }
    
    public var arPartnerDescriptionText: String? {
        set {
            descriptionTextView.text = newValue
            descriptionTextView.sizeToFit()
        }
        get {
            return descriptionTextView.text
        }
    }
    
    public func disableScrollAndAdjustViewHeight() {
        self.scrollView.isScrollEnabled = false
        let imageViewHeight = self.partnerImageView.frame.height
        let labelHeight = self.descriptionTextView.frame.height
        let offsetHeight = CGFloat(80)
        self.frame.size.height = imageViewHeight + labelHeight + offsetHeight
    }
}
