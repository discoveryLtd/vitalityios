import UIKit

public protocol PasswordChangeViewDelegate: class {
    func doneButtonPressed()
}

public class PasswordChangeView: UIView, Nibloadable {
    
    @IBOutlet weak var headerImageView: UIImageView!
    @IBOutlet weak var headerTitleLabel: UILabel!
    @IBOutlet weak var headerMessageLabel: UILabel!
    @IBOutlet weak var doneButton : VIAButton!
    weak var delegate: PasswordChangeViewDelegate?
    
    public override func awakeFromNib() {
        self.headerTitleLabel.font = UIFont.title2Font()
        self.headerMessageLabel.font = UIFont.bodyFont()
        self.doneButton.setTitleColor(UIColor.day(), for: UIControlState.selected)
        self.doneButton.tintColor = UIColor.currentGlobalTintColor()
    }
    
    public func setHeader(image: UIImage?) {
        self.headerImageView.image = image
    }
    
    public func setHeader(title: String) {
        self.headerTitleLabel.text = title
    }
    
    public func setDetail(message: String) {
        self.headerMessageLabel.text = message
    }
    
    public func setAction(title: String) {
        self.doneButton.setTitle(title, for: .normal)
    }
    
    @IBAction func tappedDoneButton(_ sender: Any) {
        self.delegate?.doneButtonPressed()
    }
    
    public func set(passwordChangeViewDelegate: PasswordChangeViewDelegate) {
        self.delegate = passwordChangeViewDelegate
    }
}
