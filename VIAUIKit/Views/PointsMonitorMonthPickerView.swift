import SnapKit
import SwiftDate

public enum Direction: Int {
    case left = -1
    case right = 1
    case none = 0
}

public class PointsMonitorMonthPickerView: UIView, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, Nibloadable {

    // MARK: Outlets

    @IBOutlet weak var collectionView: UICollectionView!

    // MARK: Properties

    public var selectedMonth: Date = Date()

    var currentSelectedIndexPath = IndexPath(item: 0, section: 0)

    public var isAtBeginning: Bool {
        return currentSelectedIndexPath.section == 0 && currentSelectedIndexPath.row == 0
    }

    public var totalNumberOfMonthsInPicker: Int = 0

    var latestDate: Date = Date() {
        didSet {
            earliestDate = Date() - 3.years
            updateTotalNumberOfMonths()
        }
    }

    var earliestDate: Date = (Date() - 3.years) {
        didSet {
            updateTotalNumberOfMonths()
        }
    }

    lazy var monthFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "MMM"
        return formatter
    }()

    lazy var yearFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy"
        return formatter
    }()

    public var didSelectDate: ((Date, Direction) -> Void) = { date, direction in
        debugPrint("Selected \(date) in direction \(direction)")
    }

    func updateTotalNumberOfMonths() {
        let months = (latestDate - earliestDate).in(.month)
        totalNumberOfMonthsInPicker = months ?? 0
    }

    // MARK: View lifecycle

    public override func awakeFromNib() {
        super.awakeFromNib()

        configureCollectionView()
    }

    func configureCollectionView() {
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.isPagingEnabled = true
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.backgroundColor = UIColor.day()
        collectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: UICollectionViewCell.defaultReuseIdentifier)
        collectionView.register(YearLabelSupplementaryView.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: YearLabelSupplementaryView.defaultReuseIdentifier)

        if let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.minimumLineSpacing = 0
            layout.minimumInteritemSpacing = 0
            layout.scrollDirection = .horizontal
            layout.sectionHeadersPinToVisibleBounds = true
            collectionView.collectionViewLayout = layout
        }
    }

    public override func layoutSubviews() {
        super.layoutSubviews()

        self.layer.removeAllBorderLayers()
        self.layer.addBorder(edge: .bottom)
    }

    // MARK: UICollectionViewDataSource

    public func numberOfSections(in collectionView: UICollectionView) -> Int {
        guard let years = (earliestDate - latestDate).in(.year) else {
            return 0
        }
        return years
    }

    func numberOfMonthsPerYear() -> Int {
        return (Calendar.current.range(of: .month, in: .year, for: latestDate)?.count)!
    }

    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == 0 { // latest year
            return latestDate.month
        } else if section == (earliestDate - latestDate).in(.year)! - 1 { // earliest year
            return numberOfMonthsPerYear() - latestDate.month
        }
        return numberOfMonthsPerYear() // full year
    }

    func dateForItem(at indexPath: IndexPath) -> Date {
        let year = (latestDate - indexPath.section.years).year
        var month = 0

        if indexPath.section == 0 {
            month = latestDate.month - indexPath.row
        } else {
            month = numberOfMonthsPerYear() - indexPath.row
        }

        var components = DateComponents()
        components.year = year
        components.month = month
        let date = Calendar.current.date(from: components)!
        return date
    }

    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: UICollectionViewCell.defaultReuseIdentifier, for: indexPath)
        cell.contentView.backgroundColor = UIColor.day()
        cell.contentView.subviews.forEach { $0.removeFromSuperview() }

        let label = UILabel(frame: .zero)
        label.textAlignment = .center
        label.font = UIFont.bodyFont()
        label.layer.cornerRadius = 12
        label.layer.masksToBounds = true
        label.text = monthFormatter.string(from: dateForItem(at: indexPath))

        if indexPath == currentSelectedIndexPath {
            label.backgroundColor = UIColor.primaryColor()
            label.textColor = UIColor.day()
        } else {
            label.backgroundColor = .clear
            label.textColor = UIColor.mediumGrey()
        }

        cell.contentView.addSubview(label)
        label.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(10)
            make.centerX.equalToSuperview()
            make.height.equalTo(25)
            make.width.equalToSuperview().multipliedBy(0.75)
        }

        return cell
    }

    // MARK: UICollectionViewDelegateFlowLayout

    let numberOfMonthsVisibleOnScreen: CGFloat = 6

    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width / numberOfMonthsVisibleOnScreen, height: collectionView.frame.size.height)
    }

    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: collectionView.frame.size.width / numberOfMonthsVisibleOnScreen, height: collectionView.frame.size.height)
    }

    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(0, -collectionView.frame.size.width / numberOfMonthsVisibleOnScreen, 0, 0)
    }

    public func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let view = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: YearLabelSupplementaryView.defaultReuseIdentifier, for: indexPath) as! YearLabelSupplementaryView
        view.label.text = yearFormatter.string(from: dateForItem(at: indexPath))
        view.didTapBlock = { indexPath in
            guard indexPath != nil else { return }
            self.collectionView(self.collectionView, didSelectItemAt: indexPath!)
        }
        return view
    }

    // MARK: UICollectionViewDelegate

    public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard currentSelectedIndexPath != indexPath else { return }

        // indexpaths
        let previouslySelectedIndexPath = currentSelectedIndexPath
        currentSelectedIndexPath = indexPath

        // update visual state
        UIView.performWithoutAnimation {
            collectionView.reloadItems(at: [previouslySelectedIndexPath, currentSelectedIndexPath])
        }

        // determine direction
        var direction = Direction.none
        if currentSelectedIndexPath.section < previouslySelectedIndexPath.section {
            direction = .left
        } else if currentSelectedIndexPath.section > previouslySelectedIndexPath.section {
            direction = .right
        } else if currentSelectedIndexPath.section == previouslySelectedIndexPath.section && currentSelectedIndexPath.row < previouslySelectedIndexPath.row {
            direction = .left
        } else if currentSelectedIndexPath.section == previouslySelectedIndexPath.section && currentSelectedIndexPath.row > previouslySelectedIndexPath.row {
            direction = .right
        }

        // item and selection
        let date = dateForItem(at: indexPath)
        self.selectedMonth = date
        didSelectDate(date, direction)
    }

}

class YearLabelSupplementaryView: UICollectionReusableView {

    public let label = UILabel(frame: .zero)
    var containerView: UIView?

    public var didTapBlock: (IndexPath?) -> Void = { indexPath in
        debugPrint("didTapIndexPath")
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }

    func setup() {
        self.backgroundColor = .clear

        // add container
        containerView = UIView(frame: self.bounds)
        containerView?.backgroundColor = .clear
        self.addSubview(containerView!)

        // setup label
        label.text = ""
        label.backgroundColor = .clear
        label.textAlignment = .center
        label.font = UIFont.footnoteFont()
        label.textColor = UIColor.lightGrey()
        label.isUserInteractionEnabled = true
        containerView?.addSubview(label)
        label.snp.makeConstraints { (make) in
            make.height.equalToSuperview().multipliedBy(0.4)
            make.leading.bottom.right.equalToSuperview()
        }

        // add tap gesture to label
        let gesture = UITapGestureRecognizer(target: self, action: #selector(didTap(_:)))
        gesture.numberOfTapsRequired = 1
        self.addGestureRecognizer(gesture)
    }

    func didTap(_ sender: UITapGestureRecognizer) {
        if let collectionView = self.superview as? UICollectionView {
            let point = collectionView.convert(self.center, from: collectionView)
            let indexPath = collectionView.indexPathForItem(at: point)
            didTapBlock(indexPath)
        }
    }

}
