//
//  VIACarouselViewModel.swift
//  VitalityActive
//
//  Created by Simon Stewart on 5/31/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import UIKit

public class VIACarouselViewModel {
    public var subTitle: String
    public var title: String
    public var image: UIImage

    public init(subTitle: String, title: String, image: UIImage) {
        self.subTitle = subTitle
        self.title = title
        self.image = image
    }
}
