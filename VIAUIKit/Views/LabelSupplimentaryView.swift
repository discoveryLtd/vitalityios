//
//  LabelSupplimentaryView.swift
//  VitalityActive
//
//  Created by Marius Janse van Vuuren on 3/31/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

import UIKit

public class LabelSupplimentaryView: UICollectionReusableView, Nibloadable {

    @IBOutlet public weak var supplimentaryViewTitle: UILabel!
    override public func awakeFromNib() {
        self.supplimentaryViewTitle.font = UIFont.bodyFont()
        self.supplimentaryViewTitle.textColor = UIColor.lightGray

    }

    public func setView(text: String) {
        self.supplimentaryViewTitle.text = text
    }

}
