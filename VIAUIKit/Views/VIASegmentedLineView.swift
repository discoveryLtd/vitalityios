//
//  VIASegmentedLineView
//  VitalityActive
//
//  Created by Simon Stewart on 5/13/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

import UIKit


public final class VIASegmentedLineView: UIView {


    convenience init() {
        self.init(frame: CGRect.zero)
    }
    override public init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.clear
    }

    var colorDone = UIColor.knowYourHealthGreen()
    var colorBusy = UIColor.mediumGrey()
    var colorNotDone = UIColor.lightGrey()

    public var items: Int = 4
    public var activeItem: Int = 3 {
        didSet {
            // if too many, default to max
            if activeItem > items {
                activeItem = items
            }
        }
    }

    // spacer: horizontal spacing between segments
    var spacerWidth = CGFloat(1)
    var spacerColor = UIColor.white

    var leftPadding: CGFloat = CGFloat(5)
    var rightPadding = CGFloat(5)

    var height = CGFloat(8) // of segments
    var cornerRadius = 4 // left top/bottom of first segment; right top/bottom of last segment

    open override func draw(_ rect: CGRect) {
        super.draw(rect)

        let fullWidth = bounds.width - (CGFloat(items - 1) * spacerWidth) - (leftPadding + rightPadding)

        let segWidth = CGFloat(fullWidth / CGFloat(items))
        let y = CGFloat(10)

        var spacers = CGFloat(0)


        for item in 0..<items {

            // segment
            let xSegment = CGFloat((segWidth * CGFloat(item))) + CGFloat(spacers * spacerWidth) + leftPadding

            // segment color
            var color: UIColor
            if item + 1 < activeItem {
                color = colorDone
            } else if item + 1 == activeItem {
                color = colorBusy
            } else {
                color = colorNotDone
            }

            let last = (item == (items - 1))
            let first = (item == 0)

            // draw segment
            drawRect(x: xSegment, y: y, width: segWidth, height: height, color: color, first:first, last:last)

            // spacer

            // don't draw last spacer
            if last {
                break
            }
            // shift right
            let xSpacer = xSegment + segWidth
            // draw spacer
            drawRect(x: xSpacer, y: y, width: spacerWidth, height: height, color: spacerColor, first:false, last:false)
            spacers = spacers + 1
        }


    }
    func drawRect(x: CGFloat, y: CGFloat, width: CGFloat, height: CGFloat, color: UIColor, first: Bool, last: Bool) {

        let drect = CGRect(x: x, y: y, width: width, height: height)

        let bpath: UIBezierPath
        if first && last {
            bpath = UIBezierPath(roundedRect: drect, byRoundingCorners: [.topLeft, .bottomLeft, .topRight, .bottomRight], cornerRadii: CGSize(width:cornerRadius, height:cornerRadius))
        } else if first {
            bpath = UIBezierPath(roundedRect: drect, byRoundingCorners: [.topLeft, .bottomLeft], cornerRadii: CGSize(width:cornerRadius, height:cornerRadius))
        } else if last {
            bpath = UIBezierPath(roundedRect: drect, byRoundingCorners: [.topRight, .bottomRight], cornerRadii: CGSize(width:cornerRadius, height:cornerRadius))
        } else {
            bpath = UIBezierPath(rect: drect)
        }

        color.set()
        bpath.fill()
    }
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}
