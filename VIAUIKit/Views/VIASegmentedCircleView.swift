//
//  VIASegmentedCircleView.swift
//  VitalityActive
//
//  Created by Simon Stewart on 4/20/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

import UIKit

public struct Slice {
    public var color: UIColor!
    public var value: CGFloat!

    init() {
        self.color = UIColor.black
        self.value = 0
    }
    init(color: UIColor, value: CGFloat) {
        self.color = color
        self.value = value
    }
}
public final class VIASegmentedCircleView: UIView {

    // should be able to define
    //  1) number of segments
    //  2) space between segments
    //  3) color of incomplete segments
    //  4) color of completed segments
    //  4) label in the middle
    //  5) width of segment
    //  6) should work any size

    private static let DEFAULT_ACTIVE_COLOR = UIColor.knowYourHealthGreen()

    fileprivate var titleLabel = UILabel()

    var width = CGFloat(6) // radius diff between inner & outer
    var spacer = Slice(color: UIColor.white, value: 0.2)
    public var integerFormatter: NumberFormatter?

    var totalSegments = 0
    var totalIncompleteSegments = 0
    var totalCompleteSegments = 0
    var colorIncompleteSegments = UIColor.lightGrey()
    var colorCompleteSegments = DEFAULT_ACTIVE_COLOR

    var total = CGFloat(0)

    var slices = [Slice]() {
        didSet {
            total = 0
            for slice in slices {
                total = slice.value + total
            }
        }
    }

    var title: String? {
        didSet {
            titleLabel.text = title
        }
    }

    var titleColor: UIColor = DEFAULT_ACTIVE_COLOR {
        didSet {
            titleLabel.textColor = self.titleColor
        }
    }

    func render() {
        var preparedSlices = [Slice]()
        for _ in stride(from: 0, to: totalCompleteSegments, by: 1) {
            preparedSlices.append(Slice(color: colorCompleteSegments, value: 10))
            if(totalCompleteSegments + totalIncompleteSegments > 1) {
                preparedSlices.append(spacer)
            }
        }
        for _ in stride(from: 0, to: totalIncompleteSegments, by: 1) {
            preparedSlices.append(Slice(color: colorIncompleteSegments, value: 10))
            if(totalCompleteSegments + totalIncompleteSegments > 1) {
                preparedSlices.append(spacer)
            }
        }
        self.slices = preparedSlices
    }

   public func setupSegmentedCircleForDisplay(totalSections: Int, completedSections: Int, numberFormatter: NumberFormatter) {
        integerFormatter = numberFormatter
        totalSegments = totalSections
        totalCompleteSegments = completedSections
        totalIncompleteSegments = totalSections - completedSections
        if let totalString = integerFormatter?.string(from: totalSegments as NSNumber),
            let completedString = integerFormatter?.string(from: totalCompleteSegments as NSNumber) {
            title = "\(completedString)/\(totalString)"
        }
        initAndConstrainLabel()
        render()
        setNeedsDisplay()
    }


    convenience init() {
        self.init(frame: CGRect.zero)
    }
    override public init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.white
        self.initAndConstrainLabel()
    }
    func initAndConstrainLabel() {
        titleLabel.text = self.title ?? ""
        titleLabel.font = UIFont.title1Font()
        titleLabel.textAlignment = .center
        titleLabel.textColor = VIASegmentedCircleView.DEFAULT_ACTIVE_COLOR
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(titleLabel)

        self.addConstraint(NSLayoutConstraint(item: titleLabel, attribute: .centerX, relatedBy: .equal, toItem: self, attribute: .centerX, multiplier: 1, constant: 0))
        self.addConstraint(NSLayoutConstraint(item: titleLabel, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0))

    }
    open override func draw(_ rect: CGRect) {
        super.draw(rect)
        let dimension = self.bounds.height > self.bounds.width ? self.bounds.width : self.bounds.height // use smaller

        let radiusOuter = CGFloat(dimension - 10) / 2
        let radiusInner = CGFloat(radiusOuter - self.width)


        var totalExSpacer = CGFloat(0)
        var totalNumberSpacers = CGFloat(0)
        for slice in slices {
            if(slice.value == 1) {
                totalNumberSpacers += 1
                continue
            }
            totalExSpacer = slice.value + totalExSpacer
        }

        var total = CGFloat(0)
        for slice in slices {
            total = slice.value + total
        }

        self.backgroundColor = UIColor.white

        let center = CGPoint(x: bounds.width / 2, y: bounds.height / 2)
        var startValue: CGFloat = 0
        var startAngle: CGFloat = 0
        var endValue: CGFloat = 0
        var endAngle: CGFloat = 0

        for slice in slices {

            startAngle = (startValue * 2 * .pi - .pi / 2)
            endValue = startValue + (slice.value / total)
            endAngle = (endValue * 2 * .pi - .pi / 2)

            let path = UIBezierPath()
            path.move(to: center)
            path.addArc(withCenter: center, radius: radiusOuter, startAngle: startAngle, endAngle: endAngle, clockwise: true)

            slice.color.setFill()
            path.fill()

            // increase start value for next slice
            startValue += slice.value / total

        }

        // create center donut hole
        let innerPath = UIBezierPath()
        innerPath.move(to: center)
        innerPath.addArc(withCenter: center, radius: radiusInner, startAngle: 0, endAngle: .pi * 2, clockwise: true)
        UIColor.white.setFill()
        innerPath.fill()

    }
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}
