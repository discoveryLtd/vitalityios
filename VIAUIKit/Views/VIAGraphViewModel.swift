import Foundation

public enum VIAGraphViewDisplayType {
    case InProgress
    case Achieved
}
public class VIAGraphViewModel {

    public private(set) var DisplayType: VIAGraphViewDisplayType = .InProgress
    public private(set) var Total: Int = 0
    public private(set) var StartingValue: Int = 0
    public private(set) var EndingValue: Int = 0

    public init(total: Int, startingValue: Int, endingValue: Int) {
        self.Total = total
        self.StartingValue = startingValue
        self.EndingValue = endingValue

        self.DetermineValues()
    }

    var ANIMATION_DURATION: Double {
        let ANIMATION_DURATION_PER_100_PERCENT = Double(2)

        var fraction = Double(EndingValue) / Double(Total == 0 ? 1 : Total)
        print("fraction \(fraction)")
        print("ANIMATION_DURATION_PER_100_PERCENT * fraction (\(ANIMATION_DURATION_PER_100_PERCENT) * \(fraction))")

        if(fraction < 1) {
            fraction = 1
        } else if(fraction > 3) {
            fraction = 3
        }
        return (ANIMATION_DURATION_PER_100_PERCENT * fraction)
    }

    var Percent100OrMore: Bool {
        return EndingValue >= Total
    }

    private func DetermineValues() {
        if self.Total > self.EndingValue || self.EndingValue == 0 || self.Total == 0 {
            self.DisplayType = .InProgress
        } else {
            self.DisplayType = .Achieved
        }
    }

}
