//
//  TitleImageHeaderView.swift
//  VitalityActive
//
//  Created by admin on 2017/07/06.
//  Copyright © 2017 Glucode. All rights reserved.
//

import UIKit

public class TitleImageHeaderView: UITableViewHeaderFooterView, Nibloadable {

    @IBOutlet weak var headerImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    public var image: UIImage? {
        set {
            headerImage.isHidden = false
            headerImage.image = newValue
        }
        get {
            return headerImage.image
        }
    }

    public var title: String? {
        set {
            titleLabel.text = newValue
        }
        get {
            return titleLabel.text
        }
    }

    public override func awakeFromNib() {
        super.awakeFromNib()
        setupView()
    }

    func setupView() {
        //self.contentView.backgroundColor = .clear
        if image == nil {
            headerImage.isHidden = true
        } else {
            headerImage.isHidden = false
        }
        titleLabel.textColor = UIColor.night()
        titleLabel.font = UIFont.title2Font()
        titleLabel.backgroundColor = .clear
    }
}
