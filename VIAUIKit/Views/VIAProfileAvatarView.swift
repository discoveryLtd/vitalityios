//
//  VIAProfileAvatarView.swift
//  VitalityActive
//
//  Created by Joshua Ryan on 7/24/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

import UIKit

public class VIAProfileAvatarView: UIImageView {

	public var textFont = UIFont.systemFont(ofSize: 80) // TODO check size
	public var textColor = UIColor.white

	public var name: String? {
		didSet {
			if image == nil {
				image = self.buildNameImage()
			}
		}
	}

	override public func layoutSubviews() {
		super.layoutSubviews()
		self.layer.cornerRadius = self.frame.size.height / 2
		self.clipsToBounds = true
	}

	// MARK: Private
	private func buildNameImage() -> UIImage? {

		var image: UIImage?

		if let name = name {
			let formatter = PersonNameComponentsFormatter()
			formatter.style = .abbreviated
			if let components = formatter.personNameComponents(from: name) {
				let initials = formatter.string(from: components)

				let paragraph = NSMutableParagraphStyle()
				paragraph.alignment = .natural

				let attributes = [NSFontAttributeName: textFont,
								  NSForegroundColorAttributeName: UIColor.white,
								  NSParagraphStyleAttributeName: paragraph]
				let attributedString = NSAttributedString(string: initials,
														  attributes: attributes)

				//let initialsSize = self.frame.width / 2
				let canvasSize = self.frame.width
				let size = sizeOfAttributeString(string: attributedString, maxSize: canvasSize)
				UIGraphicsBeginImageContextWithOptions(CGSize(width: canvasSize, height: canvasSize), false, 0.0)

				let x = (self.frame.width - size.width) / 2
				let y = (self.frame.height - size.height) / 2
				attributedString.draw(in: CGRect(x: x, y: y, width: size.width, height: size.height))
				image = UIGraphicsGetImageFromCurrentImageContext()
				UIGraphicsEndImageContext()
			}
		}

		return image
	}

	private func sizeOfAttributeString(string: NSAttributedString, maxSize: CGFloat) -> CGSize {
		let size = string.boundingRect(with: CGSize(width: maxSize, height: maxSize), context: nil).size
		return size
	}
}
