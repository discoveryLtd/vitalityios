//
//  VIAButton.swift
//  VitalityActive
//
//  Created by Wilmar van Heerden on 2016/09/23.
//  Copyright © 2016 Glucode. All rights reserved.
//

import UIKit
import ChameleonFramework

public class BorderButton: UIButton {

    public func setupButton() {
        self.tintColor = UIColor.currentGlobalTintColor()
        self.backgroundColor = .clear
        self.layer.cornerRadius = 5
        self.layer.borderWidth = 1
        self.layer.borderColor = UIColor.currentGlobalTintColor().cgColor
        self.translatesAutoresizingMaskIntoConstraints = false
        self.contentEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
    }

    func borderColor(toggled: Bool) -> UIColor? {
        return self.tintColor
    }

    public var highlightedTextColor: UIColor = UIColor.day() {
        didSet {
            self.configureTitleLabelColor()
        }
    }

    func configureTitleLabelColor() {
        self.setTitleColor(self.tintColor, for: .normal)
        self.setTitleColor(self.highlightedTextColor, for: .highlighted)
        self.setTitleColor(self.highlightedTextColor, for: .selected)
    }

    func toggleHighlightedSelected(toggle: Bool) {
        self.layer.borderColor = self.borderColor(toggled: toggle)?.cgColor
        if toggle {
            self.backgroundColor = self.tintColor
            self.setTitleColor(self.highlightedTextColor, for: .normal)
        } else {
            DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(50), execute: {
                self.backgroundColor = .clear
                self.setTitleColor(self.tintColor, for: .normal)
            })
        }
    }
    
    public func addCustomPadding(top: CGFloat, left: CGFloat, bottom: CGFloat, right: CGFloat) {
        self.contentEdgeInsets = UIEdgeInsets(top: top, left: left, bottom: bottom, right: right)
    }

    public override var isSelected: Bool {
        didSet {
            self.toggleHighlightedSelected(toggle: isSelected)
        }
    }

    public override var isHighlighted: Bool {
        didSet {
            self.toggleHighlightedSelected(toggle: isHighlighted)
        }
    }

}
