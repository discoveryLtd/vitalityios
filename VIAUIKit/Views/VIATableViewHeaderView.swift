//
//  VIATableViewHeaderView.swift
//  VitalityActive
//
//  Created by Lionel Camacho on 2017/01/16.
//  Copyright © 2017 Glucode. All rights reserved.
//

import UIKit

public class VIATableViewHeaderView: UIView, Nibloadable {

    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var contentLabel: UILabel!

    public var header: String? {
        get {
            return self.headerLabel.text as? String
        }
        set {
            self.headerLabel.text = newValue
        }
    }

    public var content: String? {
        get {
            return self.contentLabel.text as? String
        }
        set {
            self.contentLabel.text = newValue
        }
    }

  public  override func awakeFromNib() {
        super.awakeFromNib()
        self.setup()
    }

    func setup () {
        self.headerLabel.text = nil
        self.contentLabel.text = nil

        self.backgroundColor = UIColor.day()

        self.headerLabel.font = UIFont.title2Font()
        self.headerLabel.textColor = UIColor.night()

        self.contentLabel.font = UIFont.subheadlineFont()
        self.contentLabel.textColor = UIColor.darkGrey()
    }
}
