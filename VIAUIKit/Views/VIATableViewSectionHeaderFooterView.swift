//
//  VIATableViewSectionHeaderFooterView.swift
//  VitalityActive
//
//  Created by Wilmar van Heerden on 2016/09/20.
//  Copyright © 2016 Glucode. All rights reserved.
//

import UIKit

public class VIATableViewSectionHeaderFooterView: UITableViewHeaderFooterView, Nibloadable {
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var label: UILabel!
    @IBOutlet var topConstraint: NSLayoutConstraint!
    @IBOutlet var bottomMarginConstraint: NSLayoutConstraint!
    
    private var defaultTopConstraintConstant: CGFloat = 0
    private var defaultBottomMarginConstraintConstant: CGFloat = 0
    
    
    public var htmlString: String? {
        set {
            if let value = newValue{
                self.label.setHTML(text: value)
            }
        }
        get {
            return self.label.text
        }
    }
    
    public var labelText: String? {
        set {
            self.label.text = newValue
        }
        get {
            return self.label.text
        }
    }
    
    public var attributedLabelText: NSAttributedString? {
        set {
            self.label.attributedText = newValue
        }
        get {
            return self.label.attributedText
        }
    }
    
    public var labelWidth: CGFloat {
        get {
            return self.label.frame.width
        }
    }

    public var aLabel: UILabel {
        get {
            return self.label
        }
    }
    
    public override func awakeFromNib() {
        super.awakeFromNib()
        setupView()
    }
    
    public override func prepareForReuse() {
        super.prepareForReuse()
        setupView()
    }
    
    func setupView() {
        self.defaultTopConstraintConstant = 0
        self.defaultBottomMarginConstraintConstant = 0
        updateConstraints()
        
        self.contentView.backgroundColor = UIColor.tableViewBackground()
        
        self.label.text = nil
        self.label.textColor = UIColor.tableViewSectionHeaderFooter()
        self.label.font = UIFont.tableViewSectionHeaderFooter()
        self.label.backgroundColor = UIColor.tableViewBackground()
    }
    
    public override func updateConstraints() {
        self.topConstraint.constant = self.label.text == nil ? 0 : self.defaultTopConstraintConstant
        self.bottomMarginConstraint.constant = self.label.text == nil ? 0 : self.defaultBottomMarginConstraintConstant
        super.updateConstraints()
    }
    
    public func set(alignment: NSTextAlignment) {
        self.label.textAlignment = alignment
    }
    
    public func set(font: UIFont) {
        self.label.font = font
    }
    
    public func set(textColor: UIColor) {
        self.label.textColor = textColor
    }
    
    public func set(backgroundColor: UIColor) {
        self.contentView.backgroundColor = backgroundColor
        self.label.backgroundColor = backgroundColor
    }
    
    public func configureWithExtraSpacing(labelText: String?, extraTopSpacing: Int, extraBottomSpacing: Int){
        self.labelText = labelText
        defaultTopConstraintConstant = CGFloat(extraTopSpacing)
        defaultBottomMarginConstraintConstant = CGFloat(extraBottomSpacing)
        updateConstraints()
    }
    
    public func addPadding(labelText: String?, top: Int = 28, bottom: Int = 50) -> VIATableViewSectionHeaderFooterView{
        self.labelText = labelText
        defaultTopConstraintConstant = CGFloat(top)
        defaultBottomMarginConstraintConstant = CGFloat(bottom)
        updateConstraints()
        
        return self
    }
}
