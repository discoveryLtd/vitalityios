//
//  VIAGraphView.swift
//  glucodewidgets
//
//  Created by Simon Stewart on 11/2/16.
//
import UIKit
import UICountingLabel
import MKRingProgressView

public class VIAGraphView: UIView, Nibloadable {

    @IBOutlet public var view: UIView! // parent view
    @IBOutlet weak var graphView: UIView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var countingLabel100Plus: UICountingLabel!
    @IBOutlet weak var achievedImage: UIImageView!
    @IBOutlet public weak var progressLabel: UILabel!
    
    @IBOutlet weak var achievedImageBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var achievedImageTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var achievedImageLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var achievedImageTrailingConstraint: NSLayoutConstraint!
    
    public var spacing: CGFloat = 5.0
    public var ringWidth: CGFloat = CGFloat(20)
    
    public var model: VIAGraphViewModel? {
        didSet {
            self.setupGraph()
        }
    }
    public func setCountingLabel(font: UIFont) {
        self.countingLabel100Plus.font = font
    }

    let ANIMATION_DELAY_MIN = 0.01 // required for ring chart setters
    let ANIMATION_DELAY_BETWEEN_COMPLETE_AND_SHOWING_ACHIEVED = 0.2
    let ANIMATION_DELAY_INITIAL = 1.0 // between viewLoad and start of animation
    let RING_START_COLOR = UIColor.RingChartStartBlue()
    let RING_END_COLOR = UIColor.RingChartEndBlue()
    private var ringProgressView: MKRingProgressView = MKRingProgressView(frame: .zero)
    
    private func setupGraph() {
        guard let viewModel = model else {
            return
        }
        graphView.isHidden = false
        progressLabel.font = UIFont.bodyFont()
        setupAchievedImage()
        
        // ref to correct counting label (depending on >= 100% of values)
        configCountingLabel(countingLabel: countingLabel100Plus,
                            startingValue: viewModel.StartingValue)
        
        // header text & image
        // if >= 100%, toggle labels & achieve image after animation done
        if viewModel.DisplayType == .Achieved {
            countingLabel100Plus.completionBlock = { [weak self] in
                guard let animationDelay = self?.ANIMATION_DELAY_BETWEEN_COMPLETE_AND_SHOWING_ACHIEVED else {
                    return
                }
                self?.delay(animationDelay) {
                    DispatchQueue.main.async { [weak self] in
                        self?.updateToAchievedState()
                    }
                }
            }
        }
        startAnimation()
    }
    
    private func startAnimation() {
        guard let viewModel = model else {
            return
        }
        configRingProgressView()
        setRingStartValue(startingValue: viewModel.StartingValue, total: viewModel.Total)
        startAnimations(currentCountingLabel: countingLabel100Plus, startingValue: viewModel.StartingValue, endingValue: viewModel.EndingValue,
                        total: viewModel.Total, animationDuration: viewModel.ANIMATION_DURATION)
    }
    
    private func setupAchievedImage() {
        self.achievedImage.image = VIAUIKitAsset.ActiveRewards.goalAchievedSmall.image
        if model?.DisplayType != .Achieved {
            self.achievedImage.isHidden = true
        }
    }
    
    private func updateToAchievedState() {
        self.countingLabel100Plus.isHidden = true
        self.progressLabel.isHidden = true
        self.achievedImage.isHidden = false
        self.updateAchievedImageConstraints()
    }
    
    func updateAchievedImageConstraints() {
        self.achievedImageTopConstraint.constant = ringWidth + spacing
        self.achievedImageBottomConstraint.constant = ringWidth + spacing
        self.achievedImageLeadingConstraint.constant = ringWidth + spacing
        self.achievedImageTrailingConstraint.constant = ringWidth + spacing
    }

    private func configRingProgressView() {
        graphView.addSubview(ringProgressView)
        ringProgressView.startColor = RING_START_COLOR
        ringProgressView.backgroundRingColor = UIColor.tableViewBackground()
        ringProgressView.endColor = RING_END_COLOR
        ringProgressView.ringWidth = ringWidth
        ringProgressView.shadowOpacity = 0.2
       
        self.ringProgressView.snp.makeConstraints { (make) in
            make.width.equalTo(self.graphView.snp.width)
            make.height.equalTo(self.graphView.snp.height)
            make.centerY.equalTo(self.graphView.snp.centerY)
            make.centerX.equalTo(self.graphView.snp.centerX)
        }
    }

    private func configCountingLabel(countingLabel: UICountingLabel, startingValue: Int ) {
        countingLabel.textColor = UIColor.RingCountingLabelTextColor()
        countingLabel.format = "%d"
        countingLabel.method = .linear
        countingLabel.text = "\(startingValue)"
    }

    private func startAnimations(currentCountingLabel: UICountingLabel, startingValue: Int, endingValue: Int, total: Int, animationDuration: Double) {
        delay(self.ANIMATION_DELAY_INITIAL) {
            currentCountingLabel.count(from: CGFloat(startingValue), to: CGFloat(endingValue), withDuration: Double(animationDuration))
            self.delay(self.ANIMATION_DELAY_MIN) {
                CATransaction.begin()
                CATransaction.setAnimationDuration(Double(animationDuration))
                self.ringProgressView.progress = Double(endingValue) / Double(total)
                CATransaction.commit()
            }
        }
    }

    private func setRingStartValue(startingValue: Int, total: Int) {
        self.delay(self.ANIMATION_DELAY_MIN) {
            CATransaction.begin()
            CATransaction.setAnimationDuration(Double(0))
            self.ringProgressView.progress = Double(startingValue) / Double(total) // starting value
            CATransaction.commit()
        }
    }

    // helper function to start closure async after x delay
    func delay(_ delay: Double, closure: @escaping () -> Void) {
        let deadline = DispatchTime.now() + Double(Int64(delay * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: deadline, execute: closure)
    }
}
