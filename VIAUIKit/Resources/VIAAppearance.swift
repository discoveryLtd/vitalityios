import UIKit
import VitalityKit

public protocol AppearanceColorDataSource: class {
    var primaryColor: UIColor { get }
    var tabBarBackgroundColor: UIColor { get }
    var tabBarBarTintColor: UIColor { get }
    var tabBarTintColor: UIColor? { get }
    var unselectedItemTintColor: UIColor? { get }
    var getInsurerGlobalTint: UIColor? { get }
    func getSplashScreenGradientTop() -> UIColor
    func getSplashScreenGradientBottom() -> UIColor
}

public class VIAAppearance: NSObject {

    public var primaryColorFromServer: UIColor?

    public weak var dataSource: AppearanceColorDataSource? {
        didSet {
            VIAAppearance.setGlobalTintColorToPrimaryColor()
        }
    }

    open static let `default` = {
        return VIAAppearance()
    }()
}

extension VIAAppearance {
    
    public class func setGlobalTintColorToMyHealth() {
        UIApplication.shared.keyWindow?.tintColor = UIColor.myHealthOrange()
    }
    
    public class func setGlobalTintColorToKnowYourHealth() {
        UIApplication.shared.keyWindow?.tintColor = UIColor.knowYourHealthGreen(applyAODAColor: AppSettings.isAODAEnabled())
    }

    public class func setGlobalTintColorToImproveYourHealth() {
        UIApplication.shared.keyWindow?.tintColor = UIColor.improveYourHealthBlue(applyAODAColor: AppSettings.isAODAEnabled())
    }

    public class func setGlobalTintColorToGetRewarded() {
        UIApplication.shared.keyWindow?.tintColor = UIColor.getRewardedPink()
    }

    public class func setGlobalTintColorToPrimaryColor() {
        UIApplication.shared.keyWindow?.tintColor = UIColor.primaryColor()
    }
}
