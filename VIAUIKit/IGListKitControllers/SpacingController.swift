import IGListKit

public final class SpacingController: IGListSectionController, IGListSectionType {

    // MARK: Properties

    var detail: SpacerDetail = SpacerDetail(1)

    // MARK: IGListSectionType

    public func numberOfItems() -> Int {
        return 1
    }

    public func didUpdate(to object: Any) {
        if let temp = object as? SpacerDetail {
            detail = temp
        } else {
            debugPrint("Did update to something unknown, defaulting to space of size 1")
        }
    }

    public func cellForItem(at index: Int) -> UICollectionViewCell {
        let cell = collectionContext!.dequeueReusableCell(of: UICollectionViewCell.self, for: self, at: index)
        return cell
    }

    public func sizeForItem(at index: Int) -> CGSize {
        return CGSize(width: collectionContext!.containerSize.width, height: detail.space)
    }

    public func didSelectItem(at index: Int) { }

}
