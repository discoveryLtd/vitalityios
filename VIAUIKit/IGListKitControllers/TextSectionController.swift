import IGListKit
import VIAUtilities

public final class SpacerDetail: NSObject, IGListDiffable {
    public var uuidString = UUID().uuidString
    public var space: CGFloat

    public init(_ space: CGFloat) {
        self.space = space
    }

    public func diffIdentifier() -> NSObjectProtocol {
        return self.uuidString as NSObjectProtocol
    }

    public func isEqual(toDiffableObject object: IGListDiffable?) -> Bool {
        guard self !== object else { return true }
        guard let object = object as? SpacerDetail else { return false }
        return uuidString == object.uuidString
    }
}

public final class AttributedTextDetail: NSObject, IGListDiffable {
    public enum LayoutType {
        case normal
        case footnote
        case header
    }

    public var attributedString: NSAttributedString!

    public var type: LayoutType!

    public init(attributedString: NSAttributedString, type: LayoutType = .normal) {
        self.attributedString = attributedString
        self.type = type
    }

    public func diffIdentifier() -> NSObjectProtocol {
        return self.attributedString.string as NSObjectProtocol
    }

    public func isEqual(toDiffableObject object: IGListDiffable?) -> Bool {
        guard self !== object else { return true }
        guard let object = object as? AttributedTextDetail else { return false }
        return attributedString.string == object.attributedString.string
    }

    // MARK: 

    public static func addAttributesIfNeeded(to attributedString: NSMutableAttributedString, from textDetail: TextDetail?) {
        if let validTextDetail = textDetail {
            let questionAttributes: [String : Any] = [NSForegroundColorAttributeName: validTextDetail.textColor,
                                                      NSFontAttributeName: validTextDetail.font,
                                                      NSParagraphStyleAttributeName: self.paragraphStyle()]
            attributedString.addAttributes(questionAttributes, range: attributedString.string.nsRange(of: validTextDetail.text))
        }
    }
    
    public static func addAttributesToSameTextDescription(to attributedString: NSMutableAttributedString, from textDetail: TextDetail?) {
        
        if let validTextDetail = textDetail {
            
            let questionAttributes: [String : Any] = [NSForegroundColorAttributeName: validTextDetail.textColor,
                                                      NSFontAttributeName: validTextDetail.font,
                                                      NSParagraphStyleAttributeName: self.paragraphStyle()]
            attributedString.addAttributes(questionAttributes, range: attributedString.string.nsRange(of: String(format: "\n%@", validTextDetail.text)))
        }
    }

    static func paragraphStyle() -> NSParagraphStyle {
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.paragraphSpacing = 10
        return paragraphStyle
    }
}

public final class TextDetail: NSObject, IGListDiffable {
    public var text: String!
    public var font: UIFont!
    public var textColor: UIColor!

    public init(text: String, font: UIFont, textColor: UIColor) {
        self.text = text
        self.font = font
        self.textColor = textColor
    }

    public func diffIdentifier() -> NSObjectProtocol {
        return self.text as NSObjectProtocol
    }

    public func isEqual(toDiffableObject object: IGListDiffable?) -> Bool {
        guard self !== object else { return true }
        guard let object = object as? TextDetail else { return false }
        return text == object.text
    }
}

public final class TextSectionController: IGListSectionController, IGListSectionType {

    // MARK: Properties

    var textDetail: TextDetail!

    // MARK: IGListSectionType

    public func numberOfItems() -> Int {
        return 1
    }

    public func didUpdate(to object: Any) {
        self.textDetail = object as! TextDetail
    }

    public func cellForItem(at index: Int) -> UICollectionViewCell {
        let cell = collectionContext!.dequeueReusableCell(of: TextCell.self, for: self, at: index) as! TextCell
        cell.label.text = self.textDetail.text
        cell.label.font = self.textDetail.font
        cell.label.textColor = self.textDetail.textColor
        return cell
    }

    public func sizeForItem(at index: Int) -> CGSize {
        let width = collectionContext!.containerSize.width
        let insets = UIEdgeInsets(top: 0, left: TextCell.inset, bottom: 0, right: TextCell.inset)
        let size = TextSize.size(self.textDetail.text, font: self.textDetail.font, width: width, insets: insets)
        return CGSize(width: width, height: size.height)
    }

    public func didSelectItem(at index: Int) {
        // typically these cells are for display only, no selection allowed
    }

}

public final class TextCell: UICollectionViewCell, Nibloadable {

    public static let inset: CGFloat = 15.0

    // MARK: Views

    public lazy var label: UILabel = {
        let label = UILabel(frame: .zero)
        label.font = UIFont.title2Font()
        label.textColor = UIColor.night()
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    // MARK: Init

    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override init(frame: CGRect) {
        super.init(frame: frame)

        setupSubviews()
    }

    private func setupSubviews() {
        contentView.addSubview(self.label)
        self.label.snp.makeConstraints { (make) in
            make.top.bottom.equalToSuperview()
            make.left.right.equalToSuperview().inset(TextCell.inset)
        }
    }

    override public func preferredLayoutAttributesFitting(_ layoutAttributes: UICollectionViewLayoutAttributes) -> UICollectionViewLayoutAttributes {
        return self.defaultContentViewLayoutSizeFitting(layoutAttributes)
    }

}
