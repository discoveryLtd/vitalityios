//
//  VIACirclesViewController.swift
//  VitalityActive
//
//  Created by Wilmar van Heerden on 2016/10/17.
//  Copyright © 2016 Glucode. All rights reserved.
//

import Foundation
import ChameleonFramework
import SnapKit
import pop
import VitalityKit

// MARK: Protocol
public enum Color {
    case blue, pink, green, none
}

public protocol CirclesViewModel {
    var image: UIImage? { get }
    var heading: String? { get }
    var message: String? { get }
    var footnote: String? {get}
    var buttonTitle: String? { get }
    var buttonHighlightedTextColor: UIColor? { get }
    var gradientColor: Color {get}
}

extension CirclesViewModel {
    public var buttonHighlightedTextColor: UIColor? {
        return UIColor.improveYourHealthBlue()
    }
}

// MARK: ViewController

open class VIACirclesViewController: UIViewController {

    // MARK: Properties
    public var viewModel: CirclesViewModel?

    public lazy var gradientView: UIView  = {
        let view = UIView(frame: self.view.bounds)
        return view
    }()

    public lazy var button: UIButton = {
        let view = VIAButton(title: self.viewModel?.buttonTitle)
        view.tintColor = UIColor.day()
        if let highlightedTextColor = self.viewModel?.buttonHighlightedTextColor {
            view.highlightedTextColor = highlightedTextColor
        }
        return view
    }()

    public lazy var message = UILabel()
    public lazy var footnote = UILabel()

    // MARK: Internal
    private var gradientBackground: UIColor?
    private var circleViews: [UIView] = []
    private var statusBarHeight: CGFloat {
        let size = UIApplication.shared.statusBarFrame.size
        return min(size.width, size.height)
    }

    // MARK: View lifecycle

    open override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    open override func viewDidLoad() {
        super.viewDidLoad()

        self.configureAppearance()
        self.configureViews()
    }
    
    open override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        VIAViewControllerFeatures.default.analyticsLogEvent(from: self.classForCoder)
    }

    open override func viewDidLayoutSubviews() {
        self.applyRoundingToCircleViews()
    }

    func configureAppearance() {
        self.setNeedsStatusBarAppearanceUpdate()

        self.navigationController?.navigationBar.isHidden = true
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }

    // MARK: Configure views

    func applyRoundingToCircleViews() {
        self.circleViews.forEach {
            $0.setNeedsLayout()
            $0.layoutIfNeeded()
        }

        for view in self.circleViews {
            view.layer.cornerRadius = ceil(view.frame.size.width / 2)
        }
    }
    func addBounceAnimation(to view: UIView, delay: TimeInterval) {
        let animation = POPSpringAnimation(propertyNamed: kPOPLayerScaleXY)!
        animation.velocity = NSValue(cgSize: CGSize(width: 3.0, height: 3.0))
        animation.toValue = NSValue(cgSize: CGSize(width: 1.0, height: 1.0))
        animation.dynamicsMass = 5.0
        animation.dynamicsFriction = 50.0
        animation.beginTime = CACurrentMediaTime() + delay
        view.layer.pop_add(animation, forKey: "layerScaleSpringAnimation")
    }
    class func newCircleView(backgroundColor: UIColor) -> UIView {
        let circle = UIView()
        circle.backgroundColor = backgroundColor
        return circle
    }

    func configureViews() {

        let white10 = UIColor(white: 1.0, alpha: 0.1)
        let white20 = UIColor(white: 1.0, alpha: 0.1)

        // gradient
        self.view.addSubview(gradientView)
        if let color = viewModel?.gradientColor {
            switch color {
            case .blue:
                gradientBackground = UIColor.onboardingBlueGradient(frame: gradientView.frame, applyAODAColor: AppSettings.isAODAEnabled())
            case .pink:
                gradientBackground = UIColor.onboardingPinkGradient(frame: gradientView.frame)
            case .green:
                gradientBackground = UIColor.onboardingGreenGradient(frame: gradientView.frame, applyAODAColor: AppSettings.isAODAEnabled())
            case .none:
                gradientBackground = UIColor.blankGradient(frame: gradientView.frame)
            }

            gradientView.backgroundColor = gradientBackground
        }

        // biggest
        let circle1 = VIACirclesViewController.newCircleView(backgroundColor: white10)
        gradientView.addSubview(circle1)
        self.circleViews.append(circle1)
        circle1.snp.makeConstraints { make in
            make.top.equalTo(circle1.superview!.snp.top).offset(self.statusBarHeight)
            make.width.equalTo(gradientView)
            make.height.equalTo(circle1.snp.width)
        }

        // smaller
        let circle2 = VIACirclesViewController.newCircleView(backgroundColor: white10)
        gradientView.addSubview(circle2)
        self.circleViews.append(circle2)
        circle2.snp.makeConstraints { make in
            make.width.equalTo(gradientView).multipliedBy(0.73)
            make.height.equalTo(circle2.snp.width)
            make.center.equalTo(circle1)
        }

        // even smaller
        let circle3 = VIACirclesViewController.newCircleView(backgroundColor: white20)
        gradientView.addSubview(circle3)
        self.circleViews.append(circle3)
        circle3.snp.makeConstraints { make in
            make.width.equalTo(gradientView).multipliedBy(0.45)
            make.height.equalTo(circle3.snp.width)
            make.center.equalTo(circle1)
        }

        // smallest
        let circle4 = VIACirclesViewController.newCircleView(backgroundColor: white20)
        gradientView.addSubview(circle4)
        self.circleViews.append(circle4)
        circle4.snp.makeConstraints { make in
            make.width.equalTo(gradientView).multipliedBy(0.25)
            make.height.equalTo(circle4.snp.width)
            make.center.equalTo(circle1)
        }

        // imageView
        let imageView = UIImageView()
        gradientView.addSubview(imageView)
        imageView.image = self.viewModel?.image
        imageView.snp.makeConstraints { make in
            make.center.equalTo(circle4)
            make.width.equalTo(circle4.snp.width)
            make.height.equalTo(circle4.snp.height)
        }
        imageView.alpha = 0
        imageView.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)

        // set up animation
        let pulsingCirclesAnimationBlock: ((_: Double) -> Void) = {(_ delay: Double) -> Void in
            self.addBounceAnimation(to: circle1, delay: delay + 0.1)
            self.addBounceAnimation(to: circle2, delay: delay + 0.2)
            self.addBounceAnimation(to: circle3, delay: delay + 0.3)
            self.addBounceAnimation(to: circle4, delay: delay + 0.4)
            UIView.animate(withDuration: 0.2, delay: delay + 0.4, options: UIViewAnimationOptions.curveEaseIn, animations: {
                imageView.transform = CGAffineTransform(scaleX: 1, y: 1)
                imageView.alpha = 1
            }, completion: nil)
        }
        // start animation
        pulsingCirclesAnimationBlock(0.3)

        // heading label
        let heading = UILabel()
        gradientView.addSubview(heading)
        heading.numberOfLines = 0
        heading.textAlignment = .center
        heading.font = UIFont.onboardingHeading()
        heading.textColor = UIColor.onboardingText()
        heading.text = self.viewModel?.heading
        heading.snp.makeConstraints { make in
            make.width.equalTo(gradientView.snp.width).inset(UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 15))
            make.centerY.equalTo(gradientView.snp.centerY)
            make.centerX.equalTo(gradientView.snp.centerX)
        }

        // message label
        gradientView.addSubview(message)
        message.numberOfLines = 0
        message.textAlignment = .center
        message.font = UIFont.onboardingSubtitle()
        message.textColor = UIColor.onboardingText()
        message.text = self.viewModel?.message
        message.snp.makeConstraints { make in
            make.width.equalTo(heading.snp.width)
            make.top.equalTo(heading.snp.bottom).offset(15)
            make.centerX.equalTo(heading.snp.centerX)
        }

        // footnote label
        gradientView.addSubview(footnote)
        footnote.numberOfLines = 0
        footnote.textAlignment = .center
        footnote.font = UIFont.headlineFont()
        footnote.textColor = UIColor.onboardingText()
        footnote.text = self.viewModel?.footnote
        footnote.snp.makeConstraints { make in
            make.width.equalTo(heading.snp.width)
            make.top.equalTo(message.snp.bottom).offset(35)
            make.centerX.equalTo(heading.snp.centerX)
        }

        // button
        gradientView.addSubview(button)
        button.addTarget(self, action: #selector(buttonTapped(_:)), for: .touchUpInside)
        button.snp.makeConstraints { make in
            make.width.equalTo(gradientView.snp.width).multipliedBy(0.5)
            make.height.equalTo(44)
            make.bottom.equalTo(gradientView.snp.bottom).offset(-35)
            make.centerX.equalTo(gradientView.snp.centerX)
        }
    }

    open func buttonTapped(_ sender: UIButton) {
    }
}
