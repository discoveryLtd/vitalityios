import Foundation

public protocol ViewControllerDelegateDataSource: class {
    func analyticsLogEvent(from originClass: AnyClass)
}

public class VIAViewControllerFeatures: ViewControllerDelegateDataSource{
    
    public weak var dataSource: ViewControllerDelegateDataSource?
    
    open static let `default` = {
        return VIAViewControllerFeatures()
    }()
    
    public func analyticsLogEvent(from originClass: AnyClass){
        dataSource?.analyticsLogEvent(from: originClass)
    }
}
