//
//  VIAOnboardingViewController.swift
//  VitalityActive
//
//  Created by Wilmar van Heerden on 2016/10/14.
//  Copyright © 2016 Glucode. All rights reserved.
//

import Foundation
import SnapKit
import VitalityKit

// MARK: ViewModel

public struct OnboardingContentItem {
    public var heading: String?
    public var content: String?
    public var image: UIImage?

    public init(heading: String?, content: String?, image: UIImage?) {
        self.heading = heading
        self.content = content
        self.image = image
    }
}

public protocol OnboardingViewModel {
    var heading: String { get }
    var contentItems: [OnboardingContentItem] { get }
    var buttonTitle: String { get }
    var alternateButtonTitle: String? { get }
    var alternateButtonIsVisible: Bool { get }
    var gradientColor: Color {get}
    var mainButtonHighlightedTextColor: UIColor? { get }
    var labelTextColor: UIColor {get}
    var mainButtonTint: UIColor {get}
    var imageTint: UIColor? {get}
}

public extension OnboardingViewModel {
    var alternateButtonIsVisible: Bool {
        return alternateButtonTitle != nil
    }

    var mainButtonHighlightedTextColor: UIColor? {
        return nil
    }

    var labelTextColor: UIColor {
        return UIColor.onboardingText()
    }

    var mainButtonTint: UIColor {
        return UIColor.day()
    }

    var imageTint: UIColor? {
        return nil
    }
}

// MARK: Class

open class VIAOnboardingViewController: VIATableViewController {

    // MARK: Properties

    public var viewModel: OnboardingViewModel?
    var gradientBackground: UIColor?

    // MARK: View lifecycle

    deinit {
        debugPrint("VIAOnboardingViewController deinit")
    }

    open override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    open override func viewDidLoad() {
        super.viewDidLoad()

        configureAppearance()
        configureTableView()
        configureTableViewHeaderView()
        configureTableViewFooterView()
    }

    open override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        configureAppearance()
    }

    open func configureAppearance() {
        setNeedsStatusBarAppearanceUpdate()

        navigationController?.makeNavigationBarInvisible()

        let  view = UIView(frame: self.view.bounds)
        self.tableView.backgroundView = view
        self.tableView.backgroundColor = .clear

        if let color = viewModel?.gradientColor {
            switch color {
            case .green:
                gradientBackground = UIColor.onboardingGreenGradient(frame: self.view.frame, applyAODAColor:AppSettings.isAODAEnabled())
            case .blue:
                gradientBackground = UIColor.onboardingBlueGradient(frame: self.view.frame, applyAODAColor:AppSettings.isAODAEnabled())
            case .pink:
                gradientBackground = UIColor.onboardingPinkGradient(frame: self.view.frame)
            case .none:
                gradientBackground = UIColor.blankGradient(frame: self.view.frame)
            }
            view.backgroundColor = gradientBackground
        }
    }

    func configureTableView() {
        self.tableView.estimatedRowHeight = 75
        self.tableView.separatorColor = .clear

        self.tableView.register(VIAOnboardingTableViewCell.nib(), forCellReuseIdentifier: VIAOnboardingTableViewCell.defaultReuseIdentifier)
    }

    func configureTableViewHeaderView() {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: .leastNonzeroMagnitude))
        self.tableView.tableHeaderView = headerView

        let container = UIView()
        headerView.addSubview(container)
        container.translatesAutoresizingMaskIntoConstraints = false
        container.backgroundColor = .clear
        container.snp.makeConstraints { make in
            make.width.equalTo(headerView)
        }

        let label = UILabel()
        label.font = UIFont.onboardingHeading()
        label.textColor = self.viewModel?.labelTextColor
        label.textAlignment = .left
        label.numberOfLines = 0
        label.text = self.viewModel?.heading
        label.backgroundColor = .clear
        container.addSubview(label)
        label.translatesAutoresizingMaskIntoConstraints = false
        let padding: CGFloat = 25
        label.preferredMaxLayoutWidth = headerView.frame.size.width - padding * 2
        label.snp.makeConstraints { make in
            make.edges.equalTo(container).inset(UIEdgeInsets(top: 10, left: padding, bottom: padding, right: padding))
        }

        // fix the header frame
        var frame = headerView.frame
        frame.size.height = ceil(container.systemLayoutSizeFitting(UILayoutFittingCompressedSize).height)
        headerView.frame = frame
    }

    func configureTableViewFooterView() {
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: .leastNonzeroMagnitude))
        self.tableView.tableFooterView = footerView

        let container = UIView()
        footerView.addSubview(container)
        container.translatesAutoresizingMaskIntoConstraints = false
        container.backgroundColor = .clear
        container.snp.makeConstraints { make in
            make.width.equalTo(footerView)
        }

        // main button
        guard let viewModel = self.viewModel else { return }
        let mainButton = VIAButton(title: viewModel.buttonTitle)
        mainButton.highlightedTextColor = viewModel.mainButtonHighlightedTextColor
            ?? UIColor.improveYourHealthBlue()
        mainButton.tintColor = viewModel.mainButtonTint
        container.addSubview(mainButton)
        mainButton.addTarget(self, action: #selector(mainButtonTapped(_:)), for: .touchUpInside)
        mainButton.snp.makeConstraints { make in
            make.top.equalTo(20)
            make.width.equalTo(mainButton.superview!).multipliedBy(0.5)
            make.height.equalTo(44)
            make.centerX.equalTo(mainButton.superview!.snp.centerX)
            make.bottom.greaterThanOrEqualTo(mainButton.superview!.snp.bottom).offset(15).priority(750)
        }

        // alternate button
        if self.viewModel?.alternateButtonIsVisible ?? false {
            let alternateButton = VIAButton(title: self.viewModel?.alternateButtonTitle)
            alternateButton.hidesBorder = true
            alternateButton.tintColor = UIColor.day()
            alternateButton.highlightedTextColor = UIColor.day()
            container.addSubview(alternateButton)
            alternateButton.addTarget(self, action: #selector(alternateButtonTapped(_:)), for: .touchUpInside)
            alternateButton.snp.makeConstraints { make in
                make.top.equalTo(mainButton.snp.bottom).offset(10).priority(1000)
                make.width.equalTo(alternateButton.superview!).multipliedBy(0.5)
                make.height.equalTo(44)
                make.centerX.equalTo(alternateButton.superview!.snp.centerX)
                make.bottom.equalTo(alternateButton.superview!.snp.bottom).offset(-15).priority(1000)
            }
        } else {
            mainButton.snp.makeConstraints { make in
                make.bottom.equalTo(mainButton.superview!.snp.bottom).offset(-15).priority(1000)
            }
        }

        // fix the header frame
        var frame = footerView.frame
        frame.size.height = ceil(container.systemLayoutSizeFitting(UILayoutFittingCompressedSize).height)
        footerView.frame = frame
    }

    // MARK: UITableView datasource

    open override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    open override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let count = self.viewModel?.contentItems.count else {
            return 0
        }
        return count
    }

    open override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: VIAOnboardingTableViewCell.defaultReuseIdentifier, for: indexPath) as! VIAOnboardingTableViewCell

        cell.setLabelTextColor(color: self.viewModel!.labelTextColor)

        let item = self.viewModel!.contentItems[indexPath.row]
        cell.heading = item.heading
        cell.content = item.content

        cell.contentImage = item.image
        if let tint = self.viewModel!.imageTint {
            cell.setImageTint(color: tint)
        }
        cell.selectionStyle = .none

        return cell
    }

    open override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }

    // MARK: UITableView delegate

    // MARK: Button actions

    open func mainButtonTapped(_ sender: UIButton) {
        debugPrint("mainButtonTapped")
    }

    open func alternateButtonTapped(_ sender: UIButton) {
        debugPrint("alternateButtonTapped")
    }

    @IBAction open func unwindToVIAOnboardingViewControllerFromWebContentViewControllerWithSegue(_ sender: UIStoryboardSegue) {
        debugPrint("unwindToVIAOnboardingViewControllerFromWebContentViewControllerWithSegue")
    }
}
