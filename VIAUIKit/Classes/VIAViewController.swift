import Foundation
import UIKit

import VitalityKit


#if DEBUG
    import DeallocationChecker
#endif
import IQKeyboardManagerSwift

open class VIAViewController: UIViewController {
    
    fileprivate var keyboardIsOn: Bool = false
    
    open override func awakeFromNib() {
        super.awakeFromNib()
        tintViewControllerIfNeeded()
    }

    open override func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundColor = UIColor.mainViewBackground()
        tintViewControllerIfNeeded()

        if AppSettings.getAppTenant() == .SLI {
            if isViewControllerWithScrollingIssue() {
                setUpKeyboardListener()
            }
        } else {
            if AppSettings.getAppTenant() != .UKE{
                if #available(iOS 12.0, *) {
                    if isViewControllerWithScrollingIssue() {
                        setUpKeyboardListener()
                    }
                }
            }
        }
    }
    
    open override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        tintViewControllerIfNeeded()
    }
    
    open override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        VIAViewControllerFeatures.default.analyticsLogEvent(from: self.classForCoder)
    }

    open override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        #if DEBUG
            dch_checkDeallocation()
        #endif
    }
    
    func tintViewControllerIfNeeded() {
        if let viewController = self as? Tintable {
            viewController.setGlobalTintColor()
        }
    }
}

extension VIAViewController {
    
    public func isViewControllerWithScrollingIssue()->Bool {
        
        let className = NSStringFromClass(self.classForCoder)
        var targetName = className.components(separatedBy: ".")
        
        //VHR / VNA
        if targetName[0] == "VIAAssessments" {
            return true
        }
        
        //VHC
        if targetName[0] == "VIAHealthCheck" {
            return true
        }
        
        return false
    }
    
}

extension VIAViewController {
    
    func setUpKeyboardListener(){
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: .UIKeyboardWillHide, object: nil)
        
    }
    
    func keyboardWillShow(notification: NSNotification) {
        if let keyboardFrame: NSValue = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue {
            if !keyboardIsOn {
                let keyboardFrame = keyboardFrame.cgRectValue
                self.view.frame.size.height -= keyboardFrame.height
                keyboardIsOn = true
            }
        }
        
    }
    
    func keyboardWillHide(notification: NSNotification) {
        if let keyboardFrame: NSValue = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue {
            if keyboardIsOn {
                let keyboardFrame = keyboardFrame.cgRectValue
                self.view.frame.size.height += keyboardFrame.height
                keyboardIsOn = false
            }
        }
    }
    
}
