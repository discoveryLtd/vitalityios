import Foundation

import UIKit

import VitalityKit

#if DEBUG
    import DeallocationChecker
#endif

open class VIATableViewController: UITableViewController {

    public let defaultCellSeperatorColor = UIColor.cellSeperator()
    
    fileprivate var keyboardIsOn: Bool = false
    fileprivate var keyboardHeight:CGFloat = 0.0

    open override func awakeFromNib() {
        super.awakeFromNib()
        tintViewControllerIfNeeded()
    }

    open override func viewDidLoad() {
        super.viewDidLoad()

        // colors
        tintViewControllerIfNeeded()
        self.view.backgroundColor = UIColor.mainViewBackground()

        // basic setup
        self.tableView.backgroundColor = UIColor.tableViewBackground()
        self.tableView.separatorColor = self.defaultCellSeperatorColor
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.sectionHeaderHeight = UITableViewAutomaticDimension
        self.tableView.sectionFooterHeight = UITableViewAutomaticDimension
        // the default spacing between sections is designed to be 35, so 25 + the default footer height roughly equals 35
        self.tableView.estimatedSectionHeaderHeight = 25
        self.tableView.estimatedSectionFooterHeight = 25
        
        if AppSettings.getAppTenant() != .UKE{
            if #available(iOS 12.0, *){
                if isViewControllerWithScrollingIssue() {
                    self.tableView.contentInsetAdjustmentBehavior = .never
                    setUpKeyboardListener()
                }
            }
        }
    }

    open override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        tintViewControllerIfNeeded()
        // deselect the cell that was previously selected, typically when navigating Back
        if let indexPath = self.tableView.indexPathForSelectedRow {
            self.tableView.deselectRow(at: indexPath, animated: true)
        }
        
        if AppSettings.getAppTenant() != .UKE{
            if #available(iOS 12.0, *){
                if isViewControllerWithScrollingIssue() {
                    var navBarHeight = self.navigationController?.navigationBar.frame.height ?? 0
                    navBarHeight += 20
                    self.tableView.contentInset = UIEdgeInsetsMake(abs(navBarHeight), 0, 0, 0)
                }
            }
        }
    }
    
    open override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        VIAViewControllerFeatures.default.analyticsLogEvent(from: self.classForCoder)
    }

    open override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)

        #if DEBUG
            dch_checkDeallocation()
        #endif
    }
    
    func tintViewControllerIfNeeded() {
        if let viewController = self as? Tintable {
            viewController.setGlobalTintColor()
        }
    }

    // MARK: Table view data source

    open override func numberOfSections(in tableView: UITableView) -> Int {
        return 0
    }

    open override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 0
    }
}

extension VIATableViewController {
    
    public func isViewControllerWithScrollingIssue()->Bool {
        let className = NSStringFromClass(self.classForCoder)
        
        switch className {
        case "VIACore.VIARegistrationViewController",
             "VIACore.ProfileProvideFeedbackViewController",
             "VIAOrganisedFitnessEvents.OFEClaimPointsViewController",
             "VIAOrganisedFitnessEvents.OFEAddWeblinkProofViewController",
             "VIACore.EditEmailViewController",
             "VIACore.ProfileChangePasswordViewController",
             "VIACore.VIAForgotPasswordViewController",
             "VIACore.VIAResendInsurerCodeViewController":
            return true
        default:
            return false
        }
    }
    
    public func shouldAdjustBottomContentInset()->Bool {
        let className = NSStringFromClass(self.classForCoder)
        
        if AppSettings.getAppTenant() == .SLI {
            return shouldAdjustBottomContentInsetForSLI()
        } else {
            switch className {
            case "VIACore.VIARegistrationViewController",
                 "VIAOrganisedFitnessEvents.OFEAddWeblinkProofViewController",
                 "VIACore.VIAForgotPasswordViewController",
                 "VIACore.VIAResendInsurerCodeViewController":
                return true
            default:
                return false
            }
        }
    }
    
    public func shouldAdjustBottomContentInsetForSLI()->Bool {
        let className = NSStringFromClass(self.classForCoder)
        
        switch className {
        case "VIACore.VIARegistrationViewController",
             "VIACore.ProfileProvideFeedbackViewController",
             "VIAOrganisedFitnessEvents.OFEClaimPointsViewController",
             "VIAOrganisedFitnessEvents.OFEAddWeblinkProofViewController",
             "VIACore.VIAForgotPasswordViewController",
             "VIACore.VIAResendInsurerCodeViewController":
            return true
        default:
            return false
        }
    }

}

extension VIATableViewController {

    func setUpKeyboardListener(){
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: .UIKeyboardWillHide, object: nil)
    }
    
    func keyboardWillShow(notification: NSNotification) {
        if let keyboardFrame: NSValue = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue {
            if shouldAdjustBottomContentInset() {
                let keyboardRectangle = keyboardFrame.cgRectValue
                if AppSettings.getAppTenant() == .SLI {
                    keyboardHeight = keyboardRectangle.height
                } else {
                    keyboardHeight = keyboardRectangle.height + 60
                }
                setBottomContentInset(keyboardHeight)
            } else {
                if !keyboardIsOn {
                    let keyboardFrame = keyboardFrame.cgRectValue
                    self.view.frame.size.height -= keyboardFrame.height
                    keyboardIsOn = true
                }
            }
        }
    }
    
    func keyboardWillHide(notification: NSNotification) {
        if shouldAdjustBottomContentInset() {
            if AppSettings.getAppTenant() == .SLI {
                setBottomContentInset(0)
            } else {
                setBottomContentInset(keyboardHeight)
            }
        } else {
            if let keyboardFrame: NSValue = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue {
                if keyboardIsOn {
                    let keyboardFrame = keyboardFrame.cgRectValue
                    self.view.frame.size.height += keyboardFrame.height
                    keyboardIsOn = false
                }
            }
        }
    }
    
    private func setBottomContentInset(_ inset: CGFloat) {
        let top = self.tableView.contentInset.top
        let left = self.tableView.contentInset.left
        let right = self.tableView.contentInset.right
        debugPrint("\(top), \(left), \(inset), \(right)")
        
        if AppSettings.getAppTenant() == .SLI {
            self.tableView.contentInset = UIEdgeInsetsMake(64, left, inset, right)
        } else {
            self.tableView.contentInset = UIEdgeInsetsMake(64, left, -(inset*2), right)
        }
    }
}
