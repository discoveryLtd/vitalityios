import Foundation
import UIKit

public class VIAOnboardingLandingSwapSegue: UIStoryboardSegue {

    public var customDestinationViewControllerIdentifier: String?

    public override func perform() {
        let destination: UIViewController = self.destination
        let navigationController: UINavigationController? = destination.navigationController

        guard navigationController != nil, let identifier = self.customDestinationViewControllerIdentifier else {
            debugPrint("Objects missing, cannot perform custom segue")
            return
        }

        let viewControllers = navigationController?.viewControllers
        let indexToBeReplaced = viewControllers?.index(of: destination)
        let newDestination = destination.storyboard?.instantiateViewController(withIdentifier: identifier)

        guard var newViewControllers = viewControllers, let index = indexToBeReplaced, newDestination != nil else {
            debugPrint("Cannot determine which viewcontroller to replace during custom segue")
            return
        }

        newViewControllers[index] = newDestination!
        navigationController?.setViewControllers(newViewControllers, animated: false)
        navigationController?.popToViewController(newDestination!, animated: true)
    }
}
