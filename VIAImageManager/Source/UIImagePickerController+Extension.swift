import Foundation

extension UIImagePickerController{
    public struct ImageControllerConfiguration{
        public private(set) var shouldLimitFileSize:Bool
        public private(set) var shouldLimitNumberOfAttachProof:Bool
        public private(set) var maxFileSize:Int /* UoM: bytes */
        public private(set) var maxAttachmentCount:Int
        public private(set) var includeAllProofs:Bool
        public private(set) var allowCropping:Bool
        
        public init(shouldLimitFileSize: Bool = false,
                    maxFileSize: Int = 2000000,
                    shouldLimitNumberOfAttachProof: Bool = true,
                    maxAttachmentCount: Int = 11,
                    includeAllProofs: Bool = false,
                    allowCropping: Bool = false){
            
            self.shouldLimitFileSize = shouldLimitFileSize
            self.maxFileSize = maxFileSize
            self.shouldLimitNumberOfAttachProof = shouldLimitNumberOfAttachProof
            self.maxAttachmentCount = maxAttachmentCount
            self.includeAllProofs = includeAllProofs
            self.allowCropping = allowCropping
        }
        
        public func initWith(allowCropping: Bool) -> ImageControllerConfiguration{
            return ImageControllerConfiguration(shouldLimitFileSize: self.shouldLimitFileSize,
                                                maxFileSize: self.maxFileSize,
                                                shouldLimitNumberOfAttachProof: self.shouldLimitNumberOfAttachProof,
                                                maxAttachmentCount: self.maxAttachmentCount,
                                                includeAllProofs: self.includeAllProofs,
                                                allowCropping: allowCropping)
        }
    }
}
