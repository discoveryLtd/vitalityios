//
//  SelectedPhotosCollectionViewModel.swift
//  VIAImageManager
//
//  Created by OJ Garde on 8/19/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation

import VitalityKit
import VIACommon

open class SelectedPhotosCollectionViewModel{
    
    public init(){        
    }
    
    // MARK: Properties
    public var configuration: UIImagePickerController.ImageControllerConfiguration?
    
    open func getSelectedPhotoViewTitle() -> String{
        return CommonStrings.Proof.AddProofScreenTitle163
    }
    
    open func getHeaderText() -> String{
        return CommonStrings.Proof.AddProofEmptyTitle164
    }
    
    open func getMessageText() -> String{
        return CommonStrings.Proof.AddProofEmptyMessage165
    }
    
    open func getActionTitle() -> String{
        return CommonStrings.Proof.AddButton166
    }
    
    open func getRealmDomain() -> DataProvider.Domain{
        return DataProvider.Domain.vhc
    }
    
    open func getFooterMessageWithLimit(totalImages: String, maxLimit: String) -> String{
        return CommonStrings.Proof.AttachmentsFootnoteMessage177(totalImages, maxLimit)
    }
    
    open func getFooterMessage(totalImages: String, maxLimit: String) -> String{
        return CommonStrings.Proof.AttachmentsFootnoteMessage177(totalImages, maxLimit)
    }
}
