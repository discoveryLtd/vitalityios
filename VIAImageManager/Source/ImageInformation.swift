import Foundation

public struct ImageInformation {
    public var imageInfo : Dictionary<String, Any>?
    public var status: Bool?
    
    public init(imageInfo : Dictionary<String, Any>?, status: Bool?){
        self.imageInfo = imageInfo
        self.status = status
    }
}
