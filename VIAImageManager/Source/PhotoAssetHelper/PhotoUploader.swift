//
//  PhotoUploader.swift
//  VitalityActive
//
//  Created by Von Kervin R. Tuico on 16/04/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import UIKit
import Photos
import RealmSwift
import VitalityKit

public protocol PhotoUploaderDelegate: class {
    func imagesUploaded(references: Array<String>)
    func onImageUploadFailed()
}

public class PhotoUploader: NSObject {
    public var imagesUnableToUpload = Array<UploadableImage>()
    public var imagesToBeUploaded = Array<UploadableImage>()
    public weak var delegate: PhotoUploaderDelegate?
    /* Moved this here to avoid the refresh state when tapping "Try Again" button when user encountered Timeout */
    internal var imageReferences = Array<String>()
    internal var retryCount: Int = 0
    internal let MAX_RETRY_COUNT: Int = 1
    
    public func submitCapturedVHCImages(_ assets: Array<PhotoAsset>) {
        var uploadableImages = Array<UploadableImage>()
        for asset in assets {
            if let imageData = asset.assetData {
                let image = UploadableImage()
                image.setup(with: imageData)
                uploadableImages.append(image)
            } else {
                if let imageURL = asset.assetURL {
                    let image = UploadableImage()
                    image.setup(assetURLString: imageURL)
                    uploadableImages.append(image)
                }
            }
        }
        self.retryCount = 0
        self.submitUploadableImages(images: uploadableImages)
    }
    
    public func getAllUploadableImages(_ assets: Array<PhotoAsset>) {
        var uploadableImages = Array<UploadableImage>()
        for asset in assets {
            if let imageData = asset.assetData {
                let image = UploadableImage()
                image.setup(with: imageData)
                uploadableImages.append(image)
            } else {
                if let imageURL = asset.assetURL {
                    let image = UploadableImage()
                    image.setup(assetURLString: imageURL)
                    uploadableImages.append(image)
                }
            }
        }
        self.imagesToBeUploaded = uploadableImages
    }
    
    public func allImagesSuccesfullyUploaded() -> Bool {
        return self.imagesUnableToUpload.count == 0
    }
    
    public func reSubmitFailedImages() {
        self.submitUploadableImages(images: self.imagesUnableToUpload)
    }
    
    public func submitUploadableImages(images: Array<UploadableImage>) {
        DispatchQueue.global(qos: .userInitiated).async {
            let group = DispatchGroup()
            let coreRealm = DataProvider.newRealm()
            let partyID = coreRealm.getPartyId()
            
            let completion: (String?, UploadableImage?) -> Void = {
                (reference, assetFailedToUpload) in
                if let uploadedImageReference = reference {
                    self.imageReferences.append(uploadedImageReference)
                }
                
                if let failedPhotosAsset = assetFailedToUpload {
                    self.imagesUnableToUpload.append(failedPhotosAsset)
                }
                
                group.leave()
            }
            
            self.imagesUnableToUpload = Array<UploadableImage>()
            for image in images {
                group.enter()
                Wire.Content.uploadFile(partyId: partyID, fileContents: image.data, completion: { (fileUploadReference, error) in
                    if (error != nil) {
                        completion(nil, image)
                    } else {
                        completion(fileUploadReference, nil)
                    }
                })
            }
            _ = group.wait(timeout: .distantFuture)
            DispatchQueue.main.async { [weak self] in
                if self?.allImagesSuccesfullyUploaded() ?? false{
                    guard let imageReferences = self?.imageReferences else {
                        return
                    }
                    self?.delegate?.imagesUploaded(references: imageReferences)
                    /* Refresh the state of imageReferences after successfully uploading all the images. */
                    self?.imageReferences = Array<String>()
                }else{
                    guard let strongSelf = self else {
                        self?.delegate?.onImageUploadFailed()
                        return
                    }
                    if strongSelf.retryCount < strongSelf.MAX_RETRY_COUNT{
                        strongSelf.retryCount += 1
                        strongSelf.reSubmitFailedImages()
                    }else{
                        strongSelf.delegate?.onImageUploadFailed()
                    }
                }
            }
        }
    }
}

public class UploadableImage {
    public var name: String
    public var data: Data
    
    public init() {
        name = ""
        data = Data()
    }
    
    public func setup(with assetData: Data) {
        if let resizedData = self.compressImage(data: assetData) {
            debugPrint("size of image in KB before compressed: %f ", Double(assetData.count) / 1024.0)
            debugPrint("size of image in KB after compressed: %f ", Double(resizedData.count) / 1024.0)
            self.data = resizedData
        }
    }
    
    public func setup(assetURLString: String) {
        if let asset = PhotoAssetHelper.PhotosAssetForFileURL(url: assetURLString) {
            let imageRequestOptions = PHImageRequestOptions()
            imageRequestOptions.isSynchronous = true
            let mainDispatch = DispatchGroup()
            mainDispatch.enter()
            PHImageManager.default().requestImageData(for: asset, options: imageRequestOptions, resultHandler: { [weak self] (imageData, dataUTI, orientation, info) in
                if let data = imageData {
                    self?.setup(with: data)
                }
                mainDispatch.leave()
            })
            _ = mainDispatch.wait(timeout: .distantFuture)
        }
    }
    
    private func compressImage(data: Data) -> Data?{
        if let image = UIImage(data: data), let resizedImage = image.checkImageSize(){
            debugPrint("Image before resizing: \(image.size.width) x \(image.size.height)")
            debugPrint("Image before scaling to 75dpi: \(resizedImage.size.width) x \(resizedImage.size.height)")
            if let scaledImage = resizedImage.scaleImage(){
                debugPrint("Image after scaling to 75dpi: \(scaledImage.size.width) x \(scaledImage.size.height)")
                return UIImageJPEGRepresentation(scaledImage, 0.9)
            }
            return UIImageJPEGRepresentation(resizedImage, 0.9)
        }
        
        debugPrint("Failed to compress image.")
        return data
    }
}

