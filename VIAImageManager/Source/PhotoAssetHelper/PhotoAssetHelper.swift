//
//  PhotoAssetHelper.swift
//  VitalityActive
//
//  Created by Von Kervin R. Tuico on 16/04/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import UIKit
import Photos

public class PhotoAssetHelper: NSObject {
    
    public class func PhotosAssetForFileURL(url: String) -> PHAsset? {
        let imageRequestOptions = PHImageRequestOptions()
        imageRequestOptions.version = .current
        imageRequestOptions.deliveryMode = .fastFormat
        imageRequestOptions.resizeMode = .fast
        imageRequestOptions.isSynchronous = true
        
        let fetchResult = PHAsset.fetchAssets(with: nil)
        for index in 0 ..< fetchResult.count {
            let asset = fetchResult[index]
            
            if asset.localIdentifier == url {
                return asset
            }
        }
        
        return nil
    }
    
    public class func getAssetThumbnail(asset: PHAsset) -> UIImage {
        let manager = PHImageManager.default()
        let option = PHImageRequestOptions()
        var thumbnail = UIImage()
        option.isSynchronous = true
        manager.requestImage(for: asset, targetSize: CGSize(width: 200, height: 200), contentMode: .aspectFit, options: option, resultHandler: {(result, info) -> Void in
            thumbnail = result!
        })
        return thumbnail
    }
    
}

