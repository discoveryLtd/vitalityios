// Generated using SwiftGen, by O.Halligon — https://github.com/SwiftGen/SwiftGen

#if os(OSX)
import AppKit.NSImage
public typealias VIAImageManagerColor = NSColor
public typealias VIAImageManagerImage = NSImage
#elseif os(iOS) || os(tvOS) || os(watchOS)
import UIKit.UIImage
public typealias VIAImageManagerColor = UIColor
public typealias VIAImageManagerImage = UIImage
#endif

// swiftlint:disable file_length

public typealias VIAImageManagerAssetType = VIAImageManagerImageAsset

public struct VIAImageManagerImageAsset: Equatable {
	fileprivate var name: String

	public var templateImage: VIAImageManagerImage {
		let bundle = Bundle(for: BundleToken.self)
		#if os(iOS) || os(tvOS)
		let image = VIAImageManagerImage(named: name, in: bundle, compatibleWith: nil)?.withRenderingMode(.alwaysTemplate)
		#elseif os(OSX)
		let image = bundle.image(forResource: name).withRenderingMode(.alwaysTemplate)
		#elseif os(watchOS)
		let image = VIAImageManagerImage(named: name).withRenderingMode(.alwaysTemplate)
		#endif
		guard let result = image else { fatalError("Unable to load template image named \(name).") }
		return result
	}

	public var image: VIAImageManagerImage {
		let bundle = Bundle(for: BundleToken.self)
		#if os(iOS) || os(tvOS)
		let image = VIAImageManagerImage(named: name, in: bundle, compatibleWith: nil)
		#elseif os(OSX)
		let image = bundle.image(forResource: name)
		#elseif os(watchOS)
		let image = VIAImageManagerImage(named: name)
		#endif
		guard let result = image else { fatalError("Unable to load image named \(name).") }
		return result
	}

	public static func ==(lhs: VIAImageManagerImageAsset, rhs: VIAImageManagerImageAsset) -> Bool {
        return lhs.image == rhs.image
    }
}

public struct VIAImageManagerColorAsset {
fileprivate var name: String

#if swift(>=3.2)
@available(iOS 11.0, tvOS 11.0, watchOS 4.0, OSX 10.13, *)
var color: VIAImageManagerColor {
return VIAImageManagerColor(asset: self)
}
#endif
}

// swiftlint:disable identifier_name line_length nesting type_body_length type_name
public enum VIAImageManagerAsset {
  public enum BulkImageUploader {
    public static let addPhoto = VIAImageManagerImageAsset(name: "addPhoto")
    public static let cameraLarge = VIAImageManagerImageAsset(name: "cameraLarge")
    public static let trashSmall = VIAImageManagerImageAsset(name: "trashSmall")
  }

  // swiftlint:disable trailing_comma
  public static let allColors: [VIAImageManagerColorAsset] = [
  ]
  public static let allImages: [VIAImageManagerImageAsset] = [
    BulkImageUploader.addPhoto,
    BulkImageUploader.cameraLarge,
    BulkImageUploader.trashSmall,
  ]
  // swiftlint:enable trailing_comma
  @available(*, deprecated, renamed: "allImages")
  public static let allValues: [VIAImageManagerAssetType] = allImages
}
// swiftlint:enable identifier_name line_length nesting type_body_length type_name

extension VIAImageManagerImage {
@available(iOS 1.0, tvOS 1.0, watchOS 1.0, *)
@available(OSX, deprecated,
message: "This initializer is unsafe on macOS, please use the VIAImageManagerImageAsset.image property")
convenience init!(asset: VIAImageManagerAssetType) {
#if os(iOS) || os(tvOS)
let bundle = Bundle(for: BundleToken.self)
self.init(named: asset.name, in: bundle, compatibleWith: nil)
#elseif os(OSX) || os(watchOS)
self.init(named: asset.name)
#endif
}
}

extension VIAImageManagerColor {
#if swift(>=3.2)
@available(iOS 11.0, tvOS 11.0, watchOS 4.0, OSX 10.13, *)
convenience init!(asset: VIAImageManagerColorAsset) {
let bundle = Bundle(for: BundleToken.self)
#if os(iOS) || os(tvOS)
self.init(named: asset.name, in: bundle, compatibleWith: nil)
#elseif os(OSX)
self.init(named: asset.name, bundle: bundle)
#elseif os(watchOS)
self.init(named: asset.name)
#endif
}
#endif
}

private final class BundleToken {}
