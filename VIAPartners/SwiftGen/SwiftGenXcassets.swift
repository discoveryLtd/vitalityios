// Generated using SwiftGen, by O.Halligon — https://github.com/SwiftGen/SwiftGen

#if os(OSX)
import AppKit.NSImage
public typealias VIAPartnersColor = NSColor
public typealias VIAPartnersImage = NSImage
#elseif os(iOS) || os(tvOS) || os(watchOS)
import UIKit.UIImage
public typealias VIAPartnersColor = UIColor
public typealias VIAPartnersImage = UIImage
#endif

// swiftlint:disable file_length

public typealias VIAPartnersAssetType = VIAPartnersImageAsset

public struct VIAPartnersImageAsset: Equatable {
	fileprivate var name: String

	public var templateImage: VIAPartnersImage {
		let bundle = Bundle(for: BundleToken.self)
		#if os(iOS) || os(tvOS)
		let image = VIAPartnersImage(named: name, in: bundle, compatibleWith: nil)?.withRenderingMode(.alwaysTemplate)
		#elseif os(OSX)
		let image = bundle.image(forResource: name).withRenderingMode(.alwaysTemplate)
		#elseif os(watchOS)
		let image = VIAPartnersImage(named: name).withRenderingMode(.alwaysTemplate)
		#endif
		guard let result = image else { fatalError("Unable to load template image named \(name).") }
		return result
	}

	public var image: VIAPartnersImage {
		let bundle = Bundle(for: BundleToken.self)
		#if os(iOS) || os(tvOS)
		let image = VIAPartnersImage(named: name, in: bundle, compatibleWith: nil)
		#elseif os(OSX)
		let image = bundle.image(forResource: name)
		#elseif os(watchOS)
		let image = VIAPartnersImage(named: name)
		#endif
		guard let result = image else { fatalError("Unable to load image named \(name).") }
		return result
	}

	public static func ==(lhs: VIAPartnersImageAsset, rhs: VIAPartnersImageAsset) -> Bool {
        return lhs.image == rhs.image
    }
}

public struct VIAPartnersColorAsset {
fileprivate var name: String

#if swift(>=3.2)
@available(iOS 11.0, tvOS 11.0, watchOS 4.0, OSX 10.13, *)
var color: VIAPartnersColor {
return VIAPartnersColor(asset: self)
}
#endif
}

// swiftlint:disable identifier_name line_length nesting type_body_length type_name
public enum VIAPartnersAsset {
  public enum Rewards {
    public enum IGIPartners {
      public static let foodpandaSmall = VIAPartnersImageAsset(name: "foodpandaSmall")
      public static let easyticketsLarge = VIAPartnersImageAsset(name: "easyticketsLarge")
      public static let easyticketsSmall = VIAPartnersImageAsset(name: "easyticketsSmall")
      public static let foodpandaLarge = VIAPartnersImageAsset(name: "foodpandaLarge")
    }
  }
  public enum Partners {
    public static let partnersPlaceholder = VIAPartnersImageAsset(name: "partnersPlaceholder")
    public static let partnersGetStarted = VIAPartnersImageAsset(name: "partnersGetStarted")
  }

  // swiftlint:disable trailing_comma
  public static let allColors: [VIAPartnersColorAsset] = [
  ]
  public static let allImages: [VIAPartnersImageAsset] = [
    Rewards.IGIPartners.foodpandaSmall,
    Rewards.IGIPartners.easyticketsLarge,
    Rewards.IGIPartners.easyticketsSmall,
    Rewards.IGIPartners.foodpandaLarge,
    Partners.partnersPlaceholder,
    Partners.partnersGetStarted,
  ]
  // swiftlint:enable trailing_comma
  @available(*, deprecated, renamed: "allImages")
  public static let allValues: [VIAPartnersAssetType] = allImages
}
// swiftlint:enable identifier_name line_length nesting type_body_length type_name

extension VIAPartnersImage {
@available(iOS 1.0, tvOS 1.0, watchOS 1.0, *)
@available(OSX, deprecated,
message: "This initializer is unsafe on macOS, please use the VIAPartnersImageAsset.image property")
convenience init!(asset: VIAPartnersAssetType) {
#if os(iOS) || os(tvOS)
let bundle = Bundle(for: BundleToken.self)
self.init(named: asset.name, in: bundle, compatibleWith: nil)
#elseif os(OSX) || os(watchOS)
self.init(named: asset.name)
#endif
}
}

extension VIAPartnersColor {
#if swift(>=3.2)
@available(iOS 11.0, tvOS 11.0, watchOS 4.0, OSX 10.13, *)
convenience init!(asset: VIAPartnersColorAsset) {
let bundle = Bundle(for: BundleToken.self)
#if os(iOS) || os(tvOS)
self.init(named: asset.name, in: bundle, compatibleWith: nil)
#elseif os(OSX)
self.init(named: asset.name, bundle: bundle)
#elseif os(watchOS)
self.init(named: asset.name)
#endif
}
#endif
}

private final class BundleToken {}
