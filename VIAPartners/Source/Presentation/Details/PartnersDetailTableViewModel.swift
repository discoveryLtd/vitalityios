import Foundation

import VitalityKit

import RealmSwift

public class PartnersDetailTableViewModel: CMSConsumer {
    let accessTokenPlaceholder = "ACCESSTOKENACCESSTOKENACCESSTOKEN"
    var partnerViewModel: PartnersListTableViewModel?
    var partner: PartnersListTableViewModel.PartnerData
    
    public init(for partner: PartnersListTableViewModel.PartnerData) {
        self.partner = partner
    }

    //MARK: Networking
    
    public func requestEligibilityContent(completion: @escaping (Error?) -> Void) {
        let tenantId = DataProvider.newRealm().getTenantId()
        let partyId = DataProvider.newRealm().getPartyId()
        let membershipId = DataProvider.newRealm().getMembershipId()
        let productFeatureKey = partner.typeKey
        Wire.Member.getEligibilityContent(tenantId: tenantId, partyId: partyId, membershipId: membershipId, productFeatureKey: productFeatureKey) { [weak self] error, urlString, content in
            guard error == nil else {
                completion(error)
                return
            }
            if let rawUrl = urlString {
                self?.configureLearnMoreUrl(rawUrl: rawUrl)
               
            }
            self?.partner.webViewContent = content
            completion(nil)
        }
    }
    
    func configureLearnMoreUrl(rawUrl: String) {
        guard rawUrl.contains(accessTokenPlaceholder) == false else {
            //TODO: Implement networking calls to refresh the access token for SLI
            let tempRefreshAccessToken = ""
            let urlWithAccessToken = rawUrl.replacingOccurrences(of: accessTokenPlaceholder, with: tempRefreshAccessToken)
            partner.learnMoreUrl = URL(string: urlWithAccessToken)
            return
        }
        
         partner.learnMoreUrl = URL(string: rawUrl)
        
    }
}
