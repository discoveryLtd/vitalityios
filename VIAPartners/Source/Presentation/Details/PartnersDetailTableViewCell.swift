import UIKit
import WebKit
import SnapKit

import VitalityKit
import VIACommon

public protocol PartnersDetailTableViewCellDelegate: class {
    func webViewHeightChanged()
    func userTappedWebContent(link: URL)
    func userTappedTermsAndConditions(sender: String)
    func userTappedPhoneNumber(number: String)
}

public class PartnersDetailTableViewCell: UITableViewCell, WKNavigationDelegate, CMSConsumer {
    
    var myContext = 0
    var observing = false
    var webView: WKWebView!
    public weak var delegate: PartnersDetailTableViewCellDelegate?
    var webViewHeightConstraint: NSLayoutConstraint!
    public var intContent: String? {
        didSet {
            guard let content = intContent else { return }
            startObservingHeight()
            let webContent = RewardPartnerWebContentProvider.templateHtml.replacingOccurrences(of: "<body></body>", with: "<body>\(content)</body>")
            webView.loadHTMLString(webContent, baseURL: nil)
        }
    }
    
    public var content: String? {
        get {
            return intContent
        }

        set(newContent) {
            if newContent != intContent {
                intContent = newContent
            }
        }
    }
    
    public override func awakeFromNib() {
        super.awakeFromNib()
        setupCell()
    }
    
    public override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupCell()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupCell()
    }
    
    func setupCell() {
        selectionStyle = .none
        webView = WKWebView()
        webView.scrollView.isScrollEnabled = false
        webView.navigationDelegate = self
        webView.configuration.dataDetectorTypes = .phoneNumber
        webView.isOpaque = false
        contentView.backgroundColor = UIColor.tableViewBackground()
        contentView.addSubview(webView)
        webView.snp.makeConstraints { (make) in
            make.bottom.equalToSuperview()
            make.top.equalToSuperview()
            make.leading.equalTo(40)
            make.trailing.equalTo(-40)
        }
        
        webViewHeightConstraint = webView.heightAnchor.constraint(equalToConstant: 60)
        webViewHeightConstraint.isActive = true
    }
    
    public func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        guard let destinationURL = navigationAction.request.url, let scheme = destinationURL.scheme else {
            decisionHandler(.allow)
            return
        }
        
        if ["http", "https"].contains(scheme.lowercased()) {
            delegate?.userTappedWebContent(link: destinationURL)
            decisionHandler(.cancel)
            return
        } else {
            if destinationURL.scheme == "tel" {
                let phoneNumber = destinationURL.absoluteString.components(separatedBy: ":")[1]
                delegate?.userTappedPhoneNumber(number: phoneNumber)
                decisionHandler(.cancel)
                return
            } else if destinationURL.scheme == "action" {
                if navigationAction.navigationType == .linkActivated {
                    let contentId = destinationURL.absoluteString.components(separatedBy: "://")[1]
                    delegate?.userTappedTermsAndConditions(sender: contentId)
                    decisionHandler(.cancel)
                    return
                }
            }
        }
        decisionHandler(.allow)
    }
    
    public func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        webView.scrollView.contentSize = .zero
    }
    
    /* Make background color Transparent */
    public func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        
        let colour = "#00000000"
        let css = "body { background-color : \(colour) }"
        let js = "var style = document.createElement('style'); style.innerHTML = '\(css)'; document.head.appendChild(style);"
        
        webView.evaluateJavaScript(js, completionHandler: nil)
    }
    
    deinit {
        stopObservingHeight()
    }
    
    func startObservingHeight() {
        if (!observing) {
            let options = NSKeyValueObservingOptions([.new])
            webView.scrollView.addObserver(self, forKeyPath: "contentSize", options: options, context: &myContext)
            observing = true
        }
    }
    
    func stopObservingHeight() {
        if (observing) {
            webView.scrollView.removeObserver(self, forKeyPath: "contentSize", context: &myContext)
            observing = false
        }
    }
    
    public override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        guard let safeKeyPath = keyPath else {
            super.observeValue(forKeyPath: keyPath, of: object, change: change, context: context)
            return
        }
        
        if safeKeyPath == "contentSize" && context == &myContext {
            // This prevents mulpitle, unncessary reloads of the tableView which cause scroll issues
            let height = webView.scrollView.contentSize.height
            if webViewHeightConstraint.constant != height {
                webViewHeightConstraint.constant = height
                delegate?.webViewHeightChanged()
            }
            
        } else {
            super.observeValue(forKeyPath: safeKeyPath, of: object, change: change, context: context)
        }
    }
}
