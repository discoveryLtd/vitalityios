import UIKit

import VIAUIKit
import VitalityKit

import SafariServices

// TODO: Remove after IGI Partner server content works.
import VIACommon

open class PartnersDetailTableViewController: VIATableViewController, PartnersDetailTableViewCellDelegate, SafariViewControllerPresenter, Tintable, UIWebViewDelegate, SFSafariViewControllerDelegate {
    
    public var viewModel: PartnersDetailTableViewModel?
    var shouldOverrideTitleAndContent: Bool = false
    var sourceFeatureType: FeatureType?
    var partnerView: ARPartnersDetailView?
    var partnerImage: UIImage?
    var hasEligibilityContent: Bool? = true
    fileprivate let SHOW_WKWEBVIEW      = "showWKWebView"
    fileprivate let SHOW_SFSAFARIVIEW   = "showSFSafariView"
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        setGlobalTintColor()
        setupTableView()
        title = viewModel?.partner.name
        
        if self.shouldOverrideTitleAndContent ?? false {
            title = viewModel?.partner.heading
        }
    }
    
    open override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        hideBackButtonTitle()
        if viewModel?.partner.learnMoreUrl == nil && viewModel?.partner.webViewContent == nil {
            showFullScreenHUD()
        }
    }
    
    open override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        // TODO: Remove this ugliness after IGI partner info is implemented server side.
        if viewModel?.partner.name == "foodpanda" || viewModel?.partner.name == "EasyTickets" { return }
        
        if viewModel?.partner.logoFileName == "none" { return }
        
        retrievePartnerDetails()
    }
    
    func setupTableView() {
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: UITableViewCell.defaultReuseIdentifier)
        tableView.register(PartnersDetailTableViewCell.self, forCellReuseIdentifier: PartnersDetailTableViewCell.defaultReuseIdentifier)
        tableView.tableFooterView = UIView()
        tableView.separatorInset = .zero
    }
    
    public func setGlobalTintColor() {
        guard let sourceFeature = sourceFeatureType else {
            return
        }
        switch sourceFeature {
        case .knowYourHealth:
            VIAAppearance.setGlobalTintColorToKnowYourHealth()
        case .improveYourHealth:
            VIAAppearance.setGlobalTintColorToImproveYourHealth()
        case .getRewarded:
            VIAAppearance.setGlobalTintColorToGetRewarded()
        }
    }
    
    open func retrievePartnerDetails() {
        viewModel?.requestEligibilityContent(completion: { [weak self] (error) in
            
            guard error == nil else {
                if self?.viewModel?.partner.typeKey == 116 ||
                    self?.viewModel?.partner.typeKey == 224 {
                    self?.hasEligibilityContent = false
                    self?.showOfflinePartnerDetails()
                } else {
                    self?.handleError(error)
                }
                return
            }
            
            self?.tableView.reloadData()
            self?.removeStatusView()
        })
    }
    
    open func handleError(_ error: Error?) {
        hideFullScreenHUD()
        let statusView = handleBackendErrorWithStatusView((error as? BackendError) ?? .other, tryAgainAction: { [weak self] in
            self?.showFullScreenHUD()
            self?.retrievePartnerDetails()
        })
        configureStatusView(statusView)
    }
    
    func showOfflinePartnerDetails() {
        hideFullScreenHUD()
        self.tableView.reloadData()
        
        guard let partnerView = ARPartnersDetailView.viewFromNib(owner: self) else { return }
        guard let partner = self.viewModel?.partner else { return }
        partnerView.arPartnerImage = self.partnerImage
        partnerView.arPartnerDescriptionText = partner.longDescription
        self.view.addSubview(partnerView)
        partnerView.snp.makeConstraints({ (make) in
            make.width.equalToSuperview()
            make.top.bottom.right.left.equalToSuperview()
            make.height.equalToSuperview()
        })
        
        self.partnerView = partnerView
    }
    
    // MARK: - Table view data source
    
    open override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    open override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let hasEligibilityContent = self.hasEligibilityContent, hasEligibilityContent {
            if let showPartnerGetStartedLink = VIAApplicableFeatures.default.showPartnerGetStartedLink,
                showPartnerGetStartedLink {
                return 2
            }else{
                return 1
            }
        } else {
            return 0
        }
    }
    
    open override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: PartnersDetailTableViewCell.defaultReuseIdentifier) as? PartnersDetailTableViewCell else { return UITableViewCell() }
            cell.delegate = self
            // TODO: Remove this ugliness after IGI partner info is implemented server side.
            if viewModel?.partner.name == "foodpanda" {
                cell.content = RewardPartnerWebContentProvider.foodpandaHtml
            } else if viewModel?.partner.name == "EasyTickets" {
                cell.content = RewardPartnerWebContentProvider.easyticketsHtml
            } else if viewModel?.partner.logoFileName == "none" {
                if shouldOverrideTitleAndContent ?? false{
                    let string = viewModel?.partner.longDescription
                    let description = string?.replacingOccurrences(of: "<[^>]*(>|$)|&nbsp;|&zwnj;|&raquo;|&laquo;|&rsquo;", with: "", options: .regularExpression, range: nil)
                    cell.content = "\r\n<div class=main-container>\r\n<div class=content-container>\r\n\r\n\t\t\t<p>\(String(describing: (viewModel?.partner.longDescription)!))</p>\r\n</div></div>"
                } else {
                    cell.content = "\r\n<div class=main-container>\r\n<div class=content-container>\r\n\r\n\t\t\t<p>\(String(describing: (viewModel?.partner.description)!))</p>\r\n</div></div>"
                }
            } else {
                cell.content = viewModel?.partner.webViewContent
            }
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: UITableViewCell.defaultReuseIdentifier, for: indexPath)
            
            cell.imageView?.image = VIAApplicableFeatures.default.partnerDetailViewControllerActionImage
            cell.imageView?.tintColor = sourceFeatureType?.color
            cell.textLabel?.text = VIAApplicableFeatures.default.getPartnerDetailViewControllerActionTitle(with: viewModel?.partner.typeKey)
            cell.accessoryType = .disclosureIndicator
            
            return cell
        }
    }
    
    open override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    
    open override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    open override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 1 {
            tableView.deselectRow(at: indexPath, animated: true)
            removeStatusView()
            
            /* This is a customized Learn More Webview in order to integrate the customized header value. */
            guard let typeKey = self.viewModel?.partner.typeKey else { return }
//            debugPrint("Parnter typeKey: \(typeKey)")
            switch typeKey{
//                /*
//                 * 122: Garmin
//                 * 223: Yodobashi
//                 * 113: Konami
//                 * 114: Renaissance
//                 */
//            case 122,223, 113, 114:
//                guard let serverURL = Wire.default.currentEnvironment.baseURL() else { return }
//                let serverURLString = serverURL.absoluteString.replacingOccurrences(of: "/api", with: "")
//                let stringURL = "\(serverURLString)/\(Locale.current.languageCode ??  "en")/web/vitality-member-portal/login"
//                guard let url = URL(string: stringURL) else {return}
//                self.displayErrorAlertController(title: CommonStrings.Common.Warning9999,
//                                                 message: CommonStrings.Common.Safari.AllowPopupsDescription9999) {
//                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
//                }
                /*
                 * 122: Garmin - because of FC-24364, garmin used safari because the stagebuy page only displays blank on wkwebview
                 * 223: Yodobashi
                 * 126: Hotels.com
                 */
//            case 122, 223, 126:
              case  223, 126:
                self.performSegue(withIdentifier: SHOW_WKWEBVIEW, sender: nil)
            default:
                self.performSegue(withIdentifier: SHOW_SFSAFARIVIEW, sender: nil)
            }
            
        }
        
    }
    
    open override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return .leastNonzeroMagnitude
    }
    
    open override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return .leastNonzeroMagnitude
    }
    
    open override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showPartnerTermsAndConditions" {
            if let termsAndConditionsVC = segue.destination as? PartnerTermsAndConditionsViewController {
                termsAndConditionsVC.articleId = sender as? String
                let termsViewModel = PartnerTermsAndConditionsViewModel(articleId: termsAndConditionsVC.articleId)
                termsAndConditionsVC.viewModel = termsViewModel
            }
        }else if segue.identifier == SHOW_SFSAFARIVIEW{
            guard let learnMoreURL = self.viewModel?.partner.learnMoreUrl else { return }
            if let vc = segue.destination as? PartnersSFSafariViewController {
                vc.url = learnMoreURL
            }
        }else if segue.identifier == SHOW_WKWEBVIEW{
            guard let learnMoreURL = self.viewModel?.partner.learnMoreUrl else { return }
            if let vc = segue.destination as? PartnersWKWebViewController {
                vc.url = learnMoreURL
            }
        }
    }
    
    // MARK: - Detail TVC Delegate
    public func webViewHeightChanged() {
        tableView.reloadData()
        hideFullScreenHUD()
    }
    
    public func userTappedPhoneNumber(number: String) {
        if let phoneURL = URL(string: "tel://\(number)") {
            let application:UIApplication = UIApplication.shared
            if (application.canOpenURL(phoneURL)) {
                application.open(phoneURL, options: [:], completionHandler: nil)
            }
        }
    }
    
    func showGenericError() {
        
        let controller = UIAlertController(title: CommonStrings.GenericServiceErrorTitle268,
                                           message: CommonStrings.GenericServiceErrorMessage269,
                                           preferredStyle: .alert)
        let ok = UIAlertAction(title: CommonStrings.OkButtonTitle40, style: .default) { action in }
        controller.addAction(ok)
        present(controller, animated: true, completion: nil)
    }
    
    public func userTappedWebContent(link: URL) {
        presentModally(url: link, parentViewController: self, delegate: self)
    }
    
    public func userTappedTermsAndConditions(sender: String) {
        self.performSegue(withIdentifier: "showPartnerTermsAndConditions", sender: sender)
    }
}

extension UIViewController{
    
    public func displayErrorAlertController(title : String, message : String, completion: (()->Void)? = nil) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: CommonStrings.ContinueButtonTitle87, style: .cancel) { action in
            completion?()
        })
        self.present(alert, animated: true, completion: nil)
    }

}
