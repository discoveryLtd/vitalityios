import Foundation
import SafariServices
import WebKit

import VIAUIKit
import VIAUtilities
import VitalityKit
import VIACommon

public class PartnersWKWebViewController: VIAViewController {
    public var url:URL?
    
    @IBOutlet var webView: WKWebView!
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        title = ""
        
        configureWebview()
        configureAppearance()
    }
    
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        loadHTMLString(webView: self.webView, url: self.url)
    }
    
    public override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    public override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.hideBackButtonTitle()
    }
    
    func configureAppearance() {
        navigationController?.makeNavigationBarTransparent()
        self.webView.backgroundColor = .white
        self.webView.isOpaque = true
    }
    
    private func configureWebview(){
        let pref = WKPreferences()
        pref.javaScriptEnabled = true
        pref.javaScriptCanOpenWindowsAutomatically = true
        
        /* Create a config using pref*/
        let configuration = WKWebViewConfiguration()
        configuration.preferences = pref
        
        webView = WKWebView(frame: .zero, configuration: configuration)
        webView.uiDelegate = self
        webView.navigationDelegate = self
        view = webView
    }
}

extension PartnersWKWebViewController{
    
    fileprivate func loadHTMLString(webView: WKWebView, url: URL?){
        
        if let url = url{
            
            var stringHtml = "<!DOCTYPE html><html>"
            stringHtml.append("<head>")
            stringHtml.append("<meta charset=\"UTF-8\">")
            stringHtml.append("<script>")
            stringHtml.append("function submitForm() {")
            stringHtml.append("document.mobileForm.submit();")
            stringHtml.append("}")
            stringHtml.append("</script>")
            stringHtml.append("</head>")
            stringHtml.append("<body onload=\"submitForm();\">")
            stringHtml.append("<form name=\"mobileForm\" action =\"\(String(describing: url))\" method=\"POST\">")
            stringHtml.append("<input type=\"hidden\" name=\"mobileAccessToken\" value=\"\(VitalityParty.accessTokenForCurrentParty())\"/>")
            stringHtml.append("<input type=\"hidden\" name=\"mobileLocale\" value=\"\(DeviceLocale.toString())\"/>")
            stringHtml.append("</form>")
            stringHtml.append("</body>")
            stringHtml.append("</html>")
            
            webView.loadHTMLString(stringHtml, baseURL: nil)
        }
    }
}

extension PartnersWKWebViewController: WKUIDelegate, SafariViewControllerPresenter{
    
    public func webView(_ webView: WKWebView, createWebViewWith configuration: WKWebViewConfiguration, for navigationAction: WKNavigationAction, windowFeatures: WKWindowFeatures) -> WKWebView?{
        
        if navigationAction.targetFrame == nil {
            let newWebView = createNewWebView(config: configuration, action: navigationAction, isSourceWKUIDelegate: true)
            view.addSubview(newWebView)
            return newWebView
        } else {
            self.webView.load(navigationAction.request)
        }
        
        return nil
    }
    
    public func webView(_ webView: WKWebView, shouldPreviewElement elementInfo: WKPreviewElementInfo) -> Bool{
        return false
    }
    
    public func webView(_ webView: WKWebView, previewingViewControllerForElement elementInfo: WKPreviewElementInfo, defaultActions previewActions: [WKPreviewActionItem]) -> UIViewController?{
        return self
    }
}

extension PartnersWKWebViewController: WKNavigationDelegate{
    
    public func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        guard let destinationURL = navigationAction.request.url, let scheme = destinationURL.scheme else {
            decisionHandler(.allow)
            return
        }
        
        if destinationURL != self.url{
            if ["http", "https"].contains(scheme.lowercased()) {
                if UIApplication.shared.canOpenURL(destinationURL){
                    if let syntheticClickType = navigationAction.value(forKeyPath: "syntheticClickType") as? Int, syntheticClickType == 1 {
                        
                        self.presentModally(url: destinationURL, parentViewController: self, delegate: self as? SFSafariViewControllerDelegate)
                    } else {
                        view.addSubview(createNewWebView(config: WKWebViewConfiguration(), action: navigationAction, isSourceWKUIDelegate: false))
                    }
                } else {
                    self.webView.load(navigationAction.request)
                }
                decisionHandler(.cancel)
                return
            }
        }
        decisionHandler(.allow)
    }
}

extension PartnersWKWebViewController{
    
    public func createNewWebView(config configuration:WKWebViewConfiguration, action navigationAction:WKNavigationAction, isSourceWKUIDelegate isFromWKUIDelegate:Bool) -> WKWebView {
        
        let configuration = configuration
        let newWebView = WKWebView(frame: self.webView.frame, configuration: configuration)
        newWebView.uiDelegate = self

        if isFromWKUIDelegate {
            newWebView.allowsLinkPreview = false
        } else {
            newWebView.load(navigationAction.request)
        }
        
        return newWebView
    }
    
    
}
