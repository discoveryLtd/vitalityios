import Foundation
import SafariServices
import WebKit

import VIAUIKit
import VIAUtilities
import VitalityKit
import VIACommon
import VIAActivationBarcode

public class PartnersSFSafariViewController: VIAViewController, SafariViewControllerPresenter {
    public var url:URL?
        
    @IBOutlet var webView: WKWebView!
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        title = ""
        
        configureWebview()
        configureAppearance()
    }
    
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        loadHTMLString(webView: self.webView, url: self.url)
    }
    
    public override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    public override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.hideBackButtonTitle()
    }
    
    func configureAppearance() {
        navigationController?.makeNavigationBarTransparent()
        self.webView.backgroundColor = .white
        self.webView.isOpaque = true
    }
    
    private func configureWebview(){
        let pref = WKPreferences()
        pref.javaScriptEnabled = true
        pref.javaScriptCanOpenWindowsAutomatically = true
        
        /* Create a config using pref*/
        let configuration = WKWebViewConfiguration()
        configuration.preferences = pref
        
        webView = WKWebView(frame: .zero, configuration: configuration)
        webView.uiDelegate = self
        webView.navigationDelegate = self
        view = webView
    }
}

extension PartnersSFSafariViewController{
    
    fileprivate func loadHTMLString(webView: WKWebView, url: URL?){
        
        if let url = url{
            
            var stringHtml = "<!DOCTYPE html><html>"
            stringHtml.append("<head>")
            stringHtml.append("<meta charset=\"UTF-8\">")
            stringHtml.append("<script>")
            stringHtml.append("function submitForm() {")
            stringHtml.append("document.mobileForm.submit();")
            stringHtml.append("}")
            stringHtml.append("</script>")
            stringHtml.append("</head>")
            stringHtml.append("<body onload=\"submitForm();\">")
            stringHtml.append("<form name=\"mobileForm\" action =\"\(String(describing: url))\" method=\"POST\">")
            stringHtml.append("<input type=\"hidden\" name=\"mobileAccessToken\" value=\"\(VitalityParty.accessTokenForCurrentParty())\"/>")
            stringHtml.append("<input type=\"hidden\" name=\"mobileLocale\" value=\"\(DeviceLocale.toString())\"/>")
            stringHtml.append("</form>")
            stringHtml.append("</body>")
            stringHtml.append("</html>")
            
            self.showHUDOnView(view: self.webView)
            webView.loadHTMLString(stringHtml, baseURL: nil)
        }
    }
}

extension PartnersSFSafariViewController: WKNavigationDelegate{
    
    public func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        
        guard let destinationURL = navigationAction.request.url, let scheme = destinationURL.scheme else {
            decisionHandler(.allow)
            return
        }
        
        if destinationURL != self.url{
            if ["http", "https"].contains(scheme.lowercased()) {
                if UIApplication.shared.canOpenURL(destinationURL){
                    self.presentModally(url: destinationURL, parentViewController: self, delegate: self)
                }else{
                    self.webView.load(navigationAction.request)
                }
                decisionHandler(.cancel)
                return
            }
        }
        decisionHandler(.allow)
    }
    
    public func webView(_ webView: WKWebView, decidePolicyFor navigationResponse: WKNavigationResponse, decisionHandler: @escaping (WKNavigationResponsePolicy) -> Swift.Void){
        
        guard let response:HTTPURLResponse = navigationResponse.response as? HTTPURLResponse else {return}
        
        /* If authentication is needed, we will load the URL to SFSafariViewController to handle popups. */
        if response.statusCode == 401{
            guard let url = response.url else {return}
            if UIApplication.shared.canOpenURL(url){
                self.presentModally(url: url, parentViewController: self, delegate: self)
            }else{
                self.webView.load(URLRequest(url: url))
            }
        }
        decisionHandler(.allow)
    }
    
    public func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        self.hideHUDFromView(view: self.webView)
    }
}

extension PartnersSFSafariViewController: WKUIDelegate{
    
    public func webView(_ webView: WKWebView, createWebViewWith configuration: WKWebViewConfiguration, for navigationAction: WKNavigationAction, windowFeatures: WKWindowFeatures) -> WKWebView?{
    
        guard let url = navigationAction.request.url else { return nil }
        if url.absoluteString.contains("getActivationBarcode"){

            guard let vc = UIStoryboard(name: "VIAWebActivationBarcodeView", bundle: Bundle.init(for: VIAWebActivationBarcodeViewController.self)).instantiateViewController(withIdentifier: "VIAWebActivationBarcodeViewController") as? VIAWebActivationBarcodeViewController else{
                return nil
            }
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
            if UIApplication.shared.canOpenURL(url){
                self.presentModally(url: url, parentViewController: self, delegate: self)
            }else{
                self.webView.load(navigationAction.request)
            }
        }
        
        return nil
    }
}

extension PartnersSFSafariViewController: SFSafariViewControllerDelegate{
    
    public func safariViewControllerDidFinish(_ controller: SFSafariViewController){
        self.navigationController?.popViewController(animated: true)
    }
}


