import UIKit
import VIAUIKit

public class PartnersListPartnerCell: UITableViewCell, Nibloadable {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var partnerImageView: UIImageView!
    @IBOutlet weak var imageViewWidth: NSLayoutConstraint!
    

    public var title: String? {
        get {
            return titleLabel.text
        }
        set {
            guard let label = titleLabel else {
                titleLabel.isHidden = true
                return
            }
            titleLabel.isHidden = false
            label.text = newValue
        }
    }

    public var content: String? {
        get {
            return contentLabel.text
        }
        set {
            guard let label = contentLabel else { return }
            label.text = newValue
        }
    }

    public var partnerImage: UIImage? {
        get {
            return partnerImageView.image
        }
        set {
            guard let imageView = partnerImageView else { return }
            imageView.image = newValue
            imageView.contentMode = .scaleAspectFit
            if(newValue == nil){
                imageViewWidth.constant = 0
            }
        }
    }

    public override func awakeFromNib() {
        super.awakeFromNib()
        setupCell()
    }

    func setupCell() {
        titleLabel.font = UIFont.subheadlineFont()
        titleLabel.textColor = UIColor.darkGrey()
        contentLabel.font = UIFont.headlineFont()
        contentLabel.textColor = UIColor.black

    }
}
