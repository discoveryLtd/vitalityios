import UIKit

import VitalityKit
import VIAUtilities

import RealmSwift

open class PartnersListTableViewModel: CMSConsumer {
    
    public init(withPartnerType partnerCategoryType: ProductFeatureCategoryRef) {
        self.partnerCategoryType = partnerCategoryType
    }
    
    //MARK: Properties
    
    public struct PartnerData {
        public var name: String
        public var longDescription: String
        public var description: String
        public var logoFileName: String
        public var typeKey: Int
        public var learnMoreUrl: URL?
        public var webViewContent: String?
        public var heading: String?
        
        public init(name: String
            ,longDescription: String
            ,description: String
            ,logoFileName: String
            ,typeKey: Int
            ,learnMoreUrl: URL?
            ,webViewContent: String?
            ,heading: String?){
            self.name = name
            self.longDescription = longDescription
            self.description = description
            self.logoFileName = logoFileName
            self.typeKey = typeKey
            self.learnMoreUrl = learnMoreUrl
            self.webViewContent = webViewContent
            self.heading = heading
        }
    }
    
    public struct GroupData {
        public var key: Int?
        public var name: String?
        public var partners: [PartnerData]
        
        public init(key: Int?, name: String?, partners: [PartnerData]){
            self.key = key
            self.name = name
            self.partners = partners
        }
    }
    
    public let coreRealm = DataProvider.newPartnerRealm()
    
    public var partnerCategoryType: ProductFeatureCategoryRef
    public var partnerGroupsData = [GroupData]()
    
    public var title: String? {
        switch partnerCategoryType {
        case .WellnessPartners:
            return CommonStrings.Wellnesspartners.ScreenTitle608
        case .HealthPartners:
            return CommonStrings.Healthpartners.ScreenTitle897
        case .RewardPartners:
            return CommonStrings.Rewardpartners.ScreenTitle898
        case .FitnessAssessment:
            if let tenantID = AppSettings.getAppTenantId(), let tenant = TenantReference(rawValue: tenantID) {
                if tenant == .UKE {
                    return CommonStrings.CardTitleYourHealthServices1332
                }
            }
            return CommonStrings.HealthServicesTitle1331
        case .Rewards:
            return CommonStrings.CardHeadingEmployerReward2098
        default:
            return nil
        }
    }
    
    //MARK: Configuration
    
    open func configurePartnerData() {
        coreRealm.refresh()
        
        guard let partnerCategory = coreRealm.partnerCategory(with: partnerCategoryType.rawValue) else {
            return
        }
        var groupsData = [GroupData]()
        let groups = partnerCategory.partnerGroups
        for group in groups {
            var groupData = GroupData(key: group.key, name: group.name, partners:[PartnerData]())
            for partner in group.partners {
                let partnerData = PartnerData(name: partner.name, longDescription: partner.longDescription, description: partner.shortDescription, logoFileName: partner.logoFileName, typeKey: partner.typeKey, learnMoreUrl: nil, webViewContent: nil, heading: partner.heading)
                    groupData.partners.append(partnerData)
            }
            groupsData.append(groupData)
        }
        partnerGroupsData = groupsData
    }
    
    //MARK: Partner data functions
    
    public var headerContent: String? {

        if AppSettings.getAppTenant() == .UKE && partnerGroupsData.first?.key == ProductFeatureCategoryRef.CorporateBenefit.rawValue{
            return CommonStrings.HealthServicesLandingHeader1334
        }
        
        return CommonStrings.Partners.LandingHeader607
    }
    
    public func partnerCount(in section: Int) -> Int? {
        if section < partnerGroupsData.count{
            return partnerGroupsData[section].partners.count
        }
        return nil
    }
    
    public func groupCount() -> Int? {
        return partnerGroupsData.count
    }
    
    open func partnerDetails(at indexPath: IndexPath) -> PartnerData? {
        let row = indexPath.row
        let section = indexPath.section
        if section < partnerGroupsData.count{
            if row < partnerGroupsData[section].partners.count{
                return partnerGroupsData[section].partners[row]
            }
        }
        return nil
    }
    
    public func sectionTitle(for section: Int) -> String? {
        if section < partnerGroupsData.count{
            return partnerGroupsData[section].name
        }
        return nil
    }
    
    //MARK: Networking
    
    public func requestPartnersByCategory(completion: @escaping (Error?) -> Void) {
        let requestParameters = configureGetPartnersByCategoryRequestParameters()
        let tenantId = DataProvider.newRealm().getTenantId()
        let partyId = DataProvider.newRealm().getPartyId()
        Wire.Member.getPartnersByCategory(tenantId: tenantId, partyId: partyId, request: requestParameters) { error, result in
            guard error == nil else {
                completion(error)
                return
            }
            completion(nil)
        }
    }
    
    func configureGetPartnersByCategoryRequestParameters() -> GetPartnersByCategoryParameters {
        let effectiveDate = Date()
        let productFeatureCategoryTypeKey = partnerCategoryType.rawValue
        let requestParameters = GetPartnersByCategoryParameters(effectiveDate: effectiveDate, productFeatureCategoryTypeKey: productFeatureCategoryTypeKey)
        
        return requestParameters
    }
    
    open func partnerLogo(partner: PartnerData, indexPath:IndexPath, completion: @escaping ((_: UIImage, _: PartnerData, _: IndexPath) -> Void)) {
        //Case where logo already exists on disk
        let documentsURL = FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask).first!
        let cachedLogoURL = documentsURL.appendingPathComponent(partner.logoFileName)
        if let cachedLogo = UIImage(contentsOfFile: cachedLogoURL.path) {
            debugPrint("Partner image retrieved successfully from disk")
            return completion(cachedLogo, partner, indexPath)
        }
        
        //Otherwise dowload logo from liferay
        self.downloadCMSImageToCache(file: partner.logoFileName) { url, error in
            let defaultLogo = VIAPartnersAsset.Partners.partnersPlaceholder.image
            guard url != nil, error == nil, let filePath = url?.path, FileManager.default.fileExists(atPath: filePath) else {
                debugPrint("Could not retrieve downloaded partner image from disk")
                return completion(defaultLogo, partner, indexPath)
            }
            
            if let logo = UIImage(contentsOfFile: filePath) {
                debugPrint("Partner image retrieved successfully from service")
                return completion(logo, partner, indexPath)
            }
            
            debugPrint("Falied to download partner image")
            return completion(defaultLogo, partner, indexPath)
        }
    }
    
}
