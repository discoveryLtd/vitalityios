import UIKit

import VIAUIKit
import VIACommon
import VitalityKit
import VIAUtilities

public enum FeatureType {
    case knowYourHealth
    case improveYourHealth
    case getRewarded
    
    public var color: UIColor {
        switch self {
        case .knowYourHealth:
            return UIColor.knowYourHealthGreen()
        case .improveYourHealth:
            return UIColor.improveYourHealthBlue()
        case .getRewarded:
            return UIColor.getRewardedPink()
        }
    }
}

public class PartnersListTableViewController: VIATableViewController, Tintable {
    
    public var viewModel: PartnersListTableViewModel?
    public var sourceFeatureType: FeatureType?
    
    public override func awakeFromNib() {
        super.awakeFromNib()
        self.setupTitle()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setupTitle()
    }
    
    public override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        self.setupTitle()
    }
    
    func setupTitle() {
        self.navigationItem.title = viewModel?.title
        self.tabBarItem.title = CommonStrings.MenuMyBenefitsButton2288
        self.navigationController?.tabBarItem.title = CommonStrings.MenuMyBenefitsButton2288
    }
    
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        if viewModel == nil {
            setupModel()
        }
        //title = viewModel?.title
        self.navigationItem.title = viewModel?.title
        configureAppearance()
        setupTableView()
        loadPartners()
    }
    
    // MARK: Configuration
    
    func setupTableView() {
        tableView.register(PartnersListHeaderView.nib(), forHeaderFooterViewReuseIdentifier: PartnersListHeaderView.defaultReuseIdentifier)
        tableView.register(VIATableViewSectionHeaderFooterView.nib(), forHeaderFooterViewReuseIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier)
        tableView.register(PartnersListPartnerCell.nib(), forCellReuseIdentifier: PartnersListPartnerCell.defaultReuseIdentifier)
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 70
        tableView.sectionHeaderHeight = UITableViewAutomaticDimension
        tableView.estimatedSectionHeaderHeight = 100
        tableView.separatorInset = UIEdgeInsetsMake(0, 110, 0, 0)
        tableView.estimatedSectionFooterHeight = 30
        tableView.sectionFooterHeight = 30
    }
    
    public func setGlobalTintColor() {
        guard let sourceFeature = sourceFeatureType else {
            return
        }
        switch sourceFeature {
        case .knowYourHealth:
            VIAAppearance.setGlobalTintColorToKnowYourHealth()
        case .improveYourHealth:
            VIAAppearance.setGlobalTintColorToImproveYourHealth()
        case .getRewarded:
            VIAAppearance.setGlobalTintColorToGetRewarded()
        }
    }
    
    func configureAppearance() {
        navigationController?.makeNavigationBarTransparent()
        hideBackButtonTitle()
        setGlobalTintColor()
    }
    
    // MARK: - Table view data source
    
    public override func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel?.groupCount() ?? 0
    }
    
    public override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel?.partnerCount(in: section) ?? 0
    }
    
    public override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    public override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let tableViewCell = self.tableView.dequeueReusableCell(withIdentifier: PartnersListPartnerCell.defaultReuseIdentifier, for: indexPath) as? PartnersListPartnerCell else { return UITableViewCell() }

        guard let partnerDetails = viewModel?.partnerDetails(at: indexPath) else { return UITableViewCell() }
        
        tableViewCell.title = partnerDetails.name
        tableViewCell.content = partnerDetails.description
		
        /**
         * If it is employer services card or employer reward card,
         * display 'heading' as title and 'longDescription' as content
         **/
        if AppSettings.getAppTenant() == .UKE && (viewModel?.partnerGroupsData.first?.key == ProductFeatureCategoryRef.CorporateBenefit.rawValue || viewModel?.partnerGroupsData.first?.key == ProductFeatureCategoryRef.CorporateProgramme.rawValue){
            tableViewCell.title = partnerDetails.heading
            
            // Override label properties.
            tableViewCell.titleLabel.font = UIFont.headlineFont()
            tableViewCell.titleLabel.textColor = UIColor.black
            tableViewCell.contentLabel.font = UIFont.subheadlineFont()
            tableViewCell.contentLabel.textColor = UIColor.darkGrey()
        }
        
		// TODO: Remove this once server can return IGI partner data.
		if partnerDetails.name == "EasyTickets" {
            tableViewCell.partnerImage = VIAPartnersAsset.Rewards.IGIPartners.easyticketsSmall.image
		} else if partnerDetails.name == "foodpanda" {
            tableViewCell.partnerImage = VIAPartnersAsset.Rewards.IGIPartners.foodpandaSmall.image
        } else if partnerDetails.logoFileName == "none" {
            tableViewCell.partnerImage = nil
        }
			
		else {
			DispatchQueue.global(qos: .background).async { [weak self] in
                self?.viewModel?.partnerLogo(partner: partnerDetails, indexPath: indexPath) { logo, partner, indexPath in
                    /* We need this placeholder to create the reference in the array. */
					DispatchQueue.main.async {
						if tableViewCell.title == partner.name && tableViewCell.content == partner.description {
                                tableViewCell.partnerImage = logo
						}
					}
				}
			}
		}
        
        tableViewCell.accessoryType = .disclosureIndicator
        return tableViewCell
    }
    
    public override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let tableViewCell = tableView.cellForRow(at: indexPath) as? PartnersListPartnerCell else { return }
        let partnerImage = tableViewCell.partnerImage
        performSegue(withIdentifier: "showPartnerDetails", sender: partnerImage)
    }
    
    public override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
            if section == 0 {
                guard let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: PartnersListHeaderView.defaultReuseIdentifier) as? PartnersListHeaderView else {
                    return nil
                }
                guard let headerContent = viewModel?.headerContent else {
                    return nil
                }
                
                //Remove the Header Title and Title Description in Employer reward
                if AppSettings.getAppTenant() == .UKE {
                    if !(viewModel?.partnerCategoryType == ProductFeatureCategoryRef.Rewards || viewModel?.partnerCategoryType ==  ProductFeatureCategoryRef.FitnessAssessment){
                        view.configurePartnersListHeader(contentText: headerContent, sectionTitleText: viewModel?.sectionTitle(for: section))
                    }
                } else {
                    view.configurePartnersListHeader(contentText: headerContent, sectionTitleText: viewModel?.sectionTitle(for: section))
                }
                
                return view
            } else {
                guard let view =  tableView.dequeueReusableHeaderFooterView(withIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier) as? VIATableViewSectionHeaderFooterView else {
                    return nil
                }
                view.configureWithExtraSpacing(labelText: viewModel?.sectionTitle(for: section), extraTopSpacing: 20, extraBottomSpacing: 0)
                view.set(textColor: .night())
                view.set(font: .title2Font())
                
                return view
            }
    }
    
    // MARK: - Navigation
    
    public override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showPartnerDetails" {
            guard let indexPath = tableView.indexPathForSelectedRow else { return }
            guard let partnerDetails = viewModel?.partnerDetails(at: indexPath) else { return }
            guard let detailVC = segue.destination as? PartnersDetailTableViewController else { return }
            let partnerImage = sender as? UIImage
            
            let detailViewModel = PartnersDetailTableViewModel(for: partnerDetails)
            detailVC.viewModel = detailViewModel
            detailVC.sourceFeatureType = sourceFeatureType
            detailVC.partnerImage = partnerImage
            
            if AppSettings.getAppTenant() == .UKE && (viewModel?.partnerGroupsData.first?.key == ProductFeatureCategoryRef.CorporateBenefit.rawValue ||
                viewModel?.partnerGroupsData.first?.key == ProductFeatureCategoryRef.CorporateProgramme.rawValue){
                detailVC.shouldOverrideTitleAndContent = true
            }
        }
    }
    
    //MARK: Networking
    
    public func loadPartners() {
        showFullScreenHUD()
        UIApplication.shared.beginIgnoringInteractionEvents()
        viewModel?.requestPartnersByCategory(completion: { [weak self] (error) in
            self?.hideFullScreenHUD()
            UIApplication.shared.endIgnoringInteractionEvents()
            guard error == nil else {
                self?.handleError(error)
                return
            }
            self?.viewModel?.configurePartnerData()
            self?.tableView.reloadData()
        })
    }
    
    func handleError(_ error: Error?) {
        let statusView = handleBackendErrorWithStatusView((error as? BackendError) ?? .other, tryAgainAction: { [weak self] in
            self?.loadPartners()
        })
        configureStatusView(statusView)
    }
	
	//MARK: Private
	
	func setupModel() {
		// Currenlty naviely defaults to reward partners
		let type = ProductFeatureCategoryRef.RewardPartners
		self.viewModel = PartnersListTableViewModel(withPartnerType: type)
		self.sourceFeatureType = .getRewarded
	}
}
