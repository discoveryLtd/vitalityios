import UIKit
import VIAUIKit
import SnapKit

public class PartnersListHeaderView: UITableViewHeaderFooterView, Nibloadable {

    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var sectionTitleLabel: UILabel!
    
    private var content: String? {
        didSet {
            contentLabel.text = self.content
        }
    }
    
    private var sectionTitle: String? {
        didSet {
            sectionTitleLabel.text = self.sectionTitle
        }
    }
    
    public override func awakeFromNib() {
        super.awakeFromNib()
        setupHeader()
    }
    
    func setupHeader() {
        contentLabel.textColor = UIColor.tableViewSectionHeaderFooter()
        contentLabel.font = UIFont.tableViewSectionHeaderFooter()
        sectionTitleLabel.textColor = UIColor.night()
        sectionTitleLabel.font = UIFont.title2Font()
        sectionTitleLabel.isHidden = true
        
        contentLabel.text = ""
        sectionTitleLabel.text = ""
    }
    
    public func configurePartnersListHeader(contentText: String, sectionTitleText: String?) {
        content = contentText
        
        guard let sectionTitleValue = sectionTitleText else {
            contentLabel.snp.remakeConstraints({ (make) in
                make.bottom.equalToSuperview().offset(-35)
            })
            sectionTitleLabel.isHidden = true
            return
        }
        
        sectionTitle = sectionTitleValue
        sectionTitleLabel.isHidden = false
        contentLabel.snp.remakeConstraints({ (make) in
            make.bottom.equalTo(sectionTitleLabel.snp.top).offset(-35)
        })
    }

}
