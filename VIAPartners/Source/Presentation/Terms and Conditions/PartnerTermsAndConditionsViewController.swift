import Foundation

import VIAUIKit
import VIAUtilities
import VitalityKit
import VIACommon

import SafariServices

import UIKit

public class PartnerTermsAndConditionsViewController: VIAViewController, UIWebViewDelegate, SafariViewControllerEscapable {
    
    public var viewModel: PartnerTermsAndConditionsViewModel?
    public var articleId: String?
    
    @IBOutlet var webView: UIWebView!

    public override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.hideBackButtonTitle()
    }
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        title = CommonStrings.Settings.TermsConditionsTitle905
        configureAppearance()
    }
    
    public override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        retrieveTermsAndConditions()
    }
    
    func configureAppearance() {
        navigationController?.makeNavigationBarTransparent()
        self.webView.backgroundColor = .white
        self.webView.isOpaque = true
    }
    
    // MARK: - Networking
    
    func retrieveTermsAndConditions() {
        self.showHUDOnView(view: self.webView)
        self.viewModel?.getCMSContent(completion: { [weak self] (error, articleContent) in
            self?.hideHUDFromView(view: self?.webView)
            if let error = error as? BackendError {
                self?.handleBackendErrorWithAlert(error)
                return
            }
            if let content = articleContent {
                self?.webView.loadHTMLString(content, baseURL: nil)
            }
        })
    }
    
    func handleError(_ error: Error?) {
        self.hideHUDFromView(view: self.webView)
        let statusView = handleBackendErrorWithStatusView((error as? BackendError) ?? .other, tryAgainAction: { [weak self] in
            self?.showHUDOnView(view: self?.webView)
            self?.retrieveTermsAndConditions()
        })
        configureStatusView(statusView)
    }
    
    // MARK: UIWebView delegate
    
    public func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        return escapeToSafariViewController(navigationType: navigationType, request: request)
    }

}
