import Foundation

import VitalityKit
import VIAUtilities

public class PartnerTermsAndConditionsViewModel: CMSConsumer {
    
    var articleId: String?
    
    public init(articleId: String?) {
        self.articleId = articleId
    }
    
    func getCMSContent(completion: @escaping ((Error?, String?) -> Void)) {
        if let articleId = articleId {
            getCMSArticleContent(articleId: articleId, completion: completion)
        } else {
            completion(nil, nil)
        }
    }

}
