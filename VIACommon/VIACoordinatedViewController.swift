//
//  VIACoordinatedViewController.swift
//  VitalityActive
//
//  Created by Joshua Ryan on 6/30/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

import VIAUIKit

open class VIACoordinatedViewController: VIAViewController, VIACoordinatable {

	// MARK: Public
	open weak var coordinator: VIAFlowDelegate?

	// MARK: UIViewController
 	override open func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		super.prepare(for: segue, sender: sender)

		coordinator?.configureController(vc: segue.destination)
	}
}
