import Foundation
import WebKit
import SafariServices

public protocol SafariViewControllerEscapable: class {
    
    func escapeToSafariViewController(with navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void)
    
    func escapeToSafariViewController(navigationType: UIWebViewNavigationType, request: URLRequest) -> Bool
    
}

extension SafariViewControllerEscapable where Self: UIViewController {
    
    fileprivate func hasValidScheme(_ url: URL) -> Bool {
        let components = URLComponents(url: url, resolvingAgainstBaseURL: false)
        return components?.scheme == "http" || components?.scheme == "https"
    }
    
    fileprivate func present(url: URL) {
        let controller = SFSafariViewController(url: url)
        present(controller, animated: true)
    }
    
    public func escapeToSafariViewController(with navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        if navigationAction.navigationType == .linkActivated, let url = navigationAction.request.url {
            if hasValidScheme(url) {
                present(url: url)
                decisionHandler(.cancel)
                return
            }
        }
        decisionHandler(.allow)
    }
    
    public func escapeToSafariViewController(navigationType: UIWebViewNavigationType, request: URLRequest) -> Bool {
        if navigationType == .linkClicked, let url = request.url {
            if hasValidScheme(url) {
                present(url: url)
                return false
            }
        }
        return true
    }
    
}
