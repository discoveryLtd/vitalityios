// Generated using SwiftGen, by O.Halligon — https://github.com/AliSoftware/SwiftGen

internal class VIACommonImagesPlaceholder {
// Placeholder class to reference lower down
}

#if os(iOS) || os(tvOS) || os(watchOS)
  import UIKit.UIImage
  typealias Image = UIImage
#elseif os(OSX)
  import AppKit.NSImage
  typealias Image = NSImage
#endif

// swiftlint:disable file_length
// swiftlint:disable line_length

// swiftlint:disable type_body_length
@available(*, deprecated, message: "Use VIACommonAsset.{image}.image or VIACommonAsset.{image}.templateImage instead")
enum CommonAsset: String {
  case vhrBackArrow = "vhrBackArrow"

  var image: Image {
    return Image(asset: self)
  }
}
// swiftlint:enable type_body_length

extension UIImage {
    @available(*, deprecated, message: "Use VIACommonAsset.{image}.image or VIACommonAsset.{image}.templateImage instead")
    class func templateImage(asset: CommonAsset) -> UIImage? {
        let bundle = Bundle(for: VIACommonImagesPlaceholder.self) // placeholder declared at top of file
        let image = UIImage(named: asset.rawValue, in: bundle, compatibleWith: nil)?.withRenderingMode(.alwaysTemplate)
        return image
    }
}

extension Image {
    @available(*, deprecated, message: "Use VIACommonAsset.{image}.image or VIACommonAsset.{image}.templateImage instead")
    convenience init!(asset: CommonAsset) {
        let bundle = Bundle(for: VIACommonImagesPlaceholder.self) // placeholder declared at top of file
        self.init(named: asset.rawValue, in: bundle, compatibleWith: nil)
    }
}
