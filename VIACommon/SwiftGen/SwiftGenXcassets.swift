// Generated using SwiftGen, by O.Halligon — https://github.com/SwiftGen/SwiftGen

#if os(OSX)
import AppKit.NSImage
public typealias VIACommonColor = NSColor
public typealias VIACommonImage = NSImage
#elseif os(iOS) || os(tvOS) || os(watchOS)
import UIKit.UIImage
public typealias VIACommonColor = UIColor
public typealias VIACommonImage = UIImage
#endif

// swiftlint:disable file_length

public typealias VIACommonAssetType = VIACommonImageAsset

public struct VIACommonImageAsset: Equatable {
	fileprivate var name: String

	public var templateImage: VIACommonImage {
		let bundle = Bundle(for: BundleToken.self)
		#if os(iOS) || os(tvOS)
		let image = VIACommonImage(named: name, in: bundle, compatibleWith: nil)?.withRenderingMode(.alwaysTemplate)
		#elseif os(OSX)
		let image = bundle.image(forResource: name).withRenderingMode(.alwaysTemplate)
		#elseif os(watchOS)
		let image = VIACommonImage(named: name).withRenderingMode(.alwaysTemplate)
		#endif
		guard let result = image else { fatalError("Unable to load template image named \(name).") }
		return result
	}

	public var image: VIACommonImage {
		let bundle = Bundle(for: BundleToken.self)
		#if os(iOS) || os(tvOS)
		let image = VIACommonImage(named: name, in: bundle, compatibleWith: nil)
		#elseif os(OSX)
		let image = bundle.image(forResource: name)
		#elseif os(watchOS)
		let image = VIACommonImage(named: name)
		#endif
		guard let result = image else { fatalError("Unable to load image named \(name).") }
		return result
	}

	public static func ==(lhs: VIACommonImageAsset, rhs: VIACommonImageAsset) -> Bool {
        return lhs.image == rhs.image
    }
}

public struct VIACommonColorAsset {
fileprivate var name: String

#if swift(>=3.2)
@available(iOS 11.0, tvOS 11.0, watchOS 4.0, OSX 10.13, *)
var color: VIACommonColor {
return VIACommonColor(asset: self)
}
#endif
}

// swiftlint:disable identifier_name line_length nesting type_body_length type_name
public enum VIACommonAsset {
  public static let vhcGenericBloodGlucose = VIACommonImageAsset(name: "vhcGenericBloodGlucose")
  public static let vhrBackArrow = VIACommonImageAsset(name: "vhrBackArrow")
  public static let carrierBrandingAreaGray = VIACommonImageAsset(name: "carrierBrandingAreaGray")
  public static let carrierBrandingArea1 = VIACommonImageAsset(name: "carrierBrandingArea1")
  public static let statusBadgeGoldLarge = VIACommonImageAsset(name: "statusBadgeGoldLarge")
  public static let partnersGetStarted = VIACommonImageAsset(name: "partnersGetStarted")
  public static let partnersLearnMore = VIACommonImageAsset(name: "partnersLearnMore")

  // swiftlint:disable trailing_comma
  public static let allColors: [VIACommonColorAsset] = [
  ]
  public static let allImages: [VIACommonImageAsset] = [
    vhcGenericBloodGlucose,
    vhrBackArrow,
    carrierBrandingAreaGray,
    carrierBrandingArea1,
    statusBadgeGoldLarge,
    partnersGetStarted,
    partnersLearnMore,
  ]
  // swiftlint:enable trailing_comma
  @available(*, deprecated, renamed: "allImages")
  public static let allValues: [VIACommonAssetType] = allImages
}
// swiftlint:enable identifier_name line_length nesting type_body_length type_name

extension VIACommonImage {
@available(iOS 1.0, tvOS 1.0, watchOS 1.0, *)
@available(OSX, deprecated,
message: "This initializer is unsafe on macOS, please use the VIACommonImageAsset.image property")
convenience init!(asset: VIACommonAssetType) {
#if os(iOS) || os(tvOS)
let bundle = Bundle(for: BundleToken.self)
self.init(named: asset.name, in: bundle, compatibleWith: nil)
#elseif os(OSX) || os(watchOS)
self.init(named: asset.name)
#endif
}
}

extension VIACommonColor {
#if swift(>=3.2)
@available(iOS 11.0, tvOS 11.0, watchOS 4.0, OSX 10.13, *)
convenience init!(asset: VIACommonColorAsset) {
let bundle = Bundle(for: BundleToken.self)
#if os(iOS) || os(tvOS)
self.init(named: asset.name, in: bundle, compatibleWith: nil)
#elseif os(OSX)
self.init(named: asset.name, bundle: bundle)
#elseif os(watchOS)
self.init(named: asset.name)
#endif
}
#endif
}

private final class BundleToken {}
