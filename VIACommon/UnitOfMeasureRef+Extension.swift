//
//  UnitOfMeasureRef+Extension.swift
//  VIACommon
//
//  Created by Michelle R. Oratil on 27/06/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import VIAUtilities
import VitalityKit

extension UnitOfMeasureRef{
    
    public func unitName() -> String{
        switch self {
        case .TotalCholestrolMilligramsPerDeciLitre,
             .TriglycerideMilligramsPerDeciLitre,
             .FastingGlucoseMilligramsPerDeciLitre,
             .LDLCholestrolMilligramsPerDeciLitre,
             .HDLCholestrolMilligramsPerDeciLitre,
             .RandomGlucoseMilligramsPerDeciLitre:
            return CommonStrings.Assessment.UnitOfMeasure.MilligramsPerDeciliterAbbreviation2171
        default:
            let formatter = Localization.decimalShortStyle
            return formatter.string(from: self.unit())
        }
    }
    
}
