//
//  Unit+Extension.swift
//  VIACommon
//
//  Created by Michelle R. Oratil on 28/06/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import VIAUtilities
import VitalityKit

extension Unit{
    public func symbolString() -> String{
        switch self{
        case UnitConcentrationMass.milligramsPerDeciliter:
            return CommonStrings.Assessment.UnitOfMeasure.MilligramsPerDeciliterAbbreviation2171
        default:
            return self.symbol
        }
    }
}
