//
//  VIACommon.h
//  VIACommon
//
//  Created by Wilmar van Heerden on 2017/07/27.
//  Copyright © 2017 Glucode. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for VIACommon.
FOUNDATION_EXPORT double VIACommonVersionNumber;

//! Project version string for VIACommon.
FOUNDATION_EXPORT const unsigned char VIACommonVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <VIACommon/PublicHeader.h>


