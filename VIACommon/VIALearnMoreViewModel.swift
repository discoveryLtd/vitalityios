import Foundation

public struct LearnMoreContentItem {
    public var header: String
    public var content: String
    public var image: UIImage?
    
    public init(header: String, content: String, image: UIImage? = nil) {
        self.header = header
        self.content = content
        self.image = image
    }
}

public protocol VIALearnMoreViewModel {
    var title: String { get }
    var contentItems: [LearnMoreContentItem] { get }
    var headingTitleFont: UIFont { get }
    var headingMessageFont: UIFont { get }
    var sectionTitleFont: UIFont { get }
    var sectionMessageFont: UIFont { get }
    var imageTint: UIColor { get }
}

public extension VIALearnMoreViewModel {
    public var headingTitleFont: UIFont {
        return UIFont.title1Font()
    }
    
    public var headingMessageFont: UIFont {
        return UIFont.subheadlineFont()
    }
    
    public var sectionTitleFont: UIFont {
        return UIFont.title2Font()
    }
    
    public var sectionMessageFont: UIFont {
        return UIFont.bodyFont()
    }
}
