//
//  MyHealthCardModel.swift
//  VitalityActive
//
//  Created by OJ Garde on 3/16/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import VitalityKit
import VIAUtilities

public enum MyHealthCardItem: Int, EnumCollection {
    case VitalityAge = 0
    case Nutrition = 1
    case StressorFeedback = 2
    case PsychologicalFeedback = 3
    case SocialFeedback = 4
    
    public func cardHeaderText() -> String? {
        switch self {
        case .Nutrition:
            return CommonStrings.Vna.Label1965
        case .StressorFeedback:
            return CommonStrings.Mwb.Result.CardTitle1228
        case .PsychologicalFeedback:
            return CommonStrings.Mwb.Result.CardTitle1260
        case .SocialFeedback:
            return CommonStrings.Mwb.Result.CardTitle1277
        default:
            return ""
        }
    }
}
