//
//  AppUpdaterUtil.swift
//  VIACommon
//
//  Created by OJ Garde on 9/28/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import VitalityKit

public class VIAUpdaterUtil{
    
    public class func checkUpdateForNewAppVersion(shouldLogoutOnNilTenantID: Bool = true){
        let realm = DataProvider.newRealm()
        Wire.Update.getAppUpdate(tenantId: realm.getTenantId(shouldLogoutOnNilTenantID: shouldLogoutOnNilTenantID)) {
            (error, upgradeUrl) in
            
            /*
             * If Appstore URL is valid and not empty. Notify user to download the latest app.
             */
            if let stringURL = upgradeUrl, let url = URL(string: stringURL){
                NotificationCenter.default.post(name: .VIAAppUpdateException, object: stringURL)
            }
        }
    }
}
