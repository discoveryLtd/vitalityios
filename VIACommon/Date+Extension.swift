//
//  Date+Extension.swift
//  VitalityActive
//
//  Created by OJ Garde on 1/23/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation

extension Date{
    
    public func getAge() -> Int{
        let now = Date()
        let calendar = Calendar.current
        
        let ageComponents = calendar.dateComponents([Calendar.Component.year], from: self, to: now)
        return ageComponents.year!
    }
    
    public func isInSameMonth(date: Date) -> Bool {
        return Calendar.current.isDate(self, equalTo: date, toGranularity: .month)
    }
}
