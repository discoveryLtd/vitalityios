public class ARRewardsWebContentViewController: VIAWebContentViewController {
    override public func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.hideHUDFromView(view: self.webView)
        self.hideHUDFromWindow()
        if let htmlContent = htmlContent {
            self.webView.loadHTMLString(htmlContent, baseURL: nil)
        }
    }
    
    internal var htmlContent: String?
    public func setHTMLContent(as html: String) {
        htmlContent = html
    }
}
