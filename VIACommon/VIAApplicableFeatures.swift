//
//  VIAApplicableFeatures.swift
//  VitalityActive
//
//  Created by OJ Garde on 11/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import VIAUtilities
import VitalityKit
import VIAUIKit
import RealmSwift

public protocol ApplicableFeaturesDataSource: class {
    var enableMobileNumber: Bool? { get }
    var enableNotificationPreference: Bool? { get }
    var hideHelpTab: Bool? { get }
    func shouldShowReminderMessageOnVHC(attribute: PartyAttributeTypeRef) -> Bool
    var hideCholesterolContent: Bool? { get }
    var hideTermsAndConditionsDisagreeButton: Bool? { get }
    var reverseValidValuesOptionList: Bool? { get }
    var hideCholesterolRatio: Bool? { get }
    var showHelpContactFooter: Bool? { get }
    var enableDelayInSplashScreenDisplay: Bool? { get }
    var enableUserToResendInsurerCode: Bool? {get}
    func showLoginRestriction() -> Bool
    var showPartnerGetStartedLink: Bool? {get}
    var removeAssessmentGuidanceText: Bool? {get}
    var showRequiredPrompt: Bool? {get}
    var hideMembershipPassAndUpdateTitle: Bool? { get }
    var shouldExecuteSSO: Bool? { get }
    var showPartnersTabItem: Bool? { get }
    var showDateOfBirthField: Bool? { get }
    var showPreviousMembershipYearTitle: Bool? { get }
    var getPointPeriod: PointsPeriod? { get }
    var shouldLimitNumberOfAttachProof: Bool? { get }
    var enableKeyboardAutoToolbar: Bool? { get }
    var applyPushNotifToggle: Bool? { get }
    var enableNotificationDialogConfirmation: Bool { get }
    func getTextFieldViewCell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell
    func getPartnerRewardInstructions(with voucherString: String) -> String
    func getDataSharingLeftBarButtonTitle() -> String
    func getDataSharingRightBarButtonTitle() -> String
    var removeEmptyCapturedData: Bool? {get}
    func shouldDisplayEarningPointsAndPointsLimits(for eventKey: Int) -> Bool
    var earnPointsByBMI: Bool? {get}
    func persistVHCData() -> Bool
    func removeAppleWatchOnPartnersData() -> Bool
    func setMinimumDate() -> Date?
    func hideParticipatingPartners() -> Bool
    func showHomeInitialScreen()
    func navigateToHomeScreen()
    func navigateToLoginScreen()
    var numberOfDecimalPlaces: Int? { get }

    /**
     * MyHealth
    **/
    var reloadMyHealthOnViewAppear: Bool? { get }
    var concatenateAgeBlurb: Bool? { get }
    func shouldSkipMyHealthLandingScreen() -> Bool
    
    /**
     * Wellness Devices
     **/
    var shouldDisplayWellnessDevicesAndDataSharingConsent: Bool? { get }
    func getThirdOnboardingItem() -> OnboardingContentItem
    
    /**
     * Partners
    **/
    var partnerDetailViewControllerActionImage: UIImage? { get }
    var shouldUseGetEligibility: Bool? { get }
    func getPartnerDetailViewControllerActionTitle(with partnerTypeKey: Int?) -> String

    /**
     * Active Rewards
     **/
    var redirectToWebVoucher: Bool? { get }
    var filterRewardByStatusOnly: Bool { get }
    var convertJoulesToCalories: Bool? { get }
    var showSwapRewardsOption: Bool? { get }
    var hideARLearnMoreBenefitGuide: Bool? { get }
    func shouldFilterPartnerGroups() -> Bool
    var filterRewardTypeCode: Bool? { get }
    func shouldIncludeUsedAndExpiredSpinInHistory() -> Bool
    func enabledEmptyStatusView() -> Bool
    func hideVitalityCoinsFromRewards() -> Bool
    
    /**
     * Profile and Settings
     **/
    var enableTouchID: Bool? { get }
    var enableEntityNumber: Bool? { get }
    var showShareVitalityStatus: Bool? { get }
    var enableChangePassword: Bool? { get }
    var enableUpdateEmail: Bool? {get}
    var userPartyIDAsVitalityNumber: Bool? { get }
    var showMembershipBanner: Bool? { get }
    var showBannerInAspectRatio: Bool? { get }
    var showMembershipPassInfoButton: Bool? { get }
    var changeMenuItemLocation: Bool? { get }
    var rememberMePreferenceDefault: Bool? { get }
    func getMembershipBannerImage() -> UIImage?
    func getLogoutRedirectURL() -> String?
    func touchIdValidatePassword() -> Bool
    func showPartyIdDetail() -> Bool
    func enableGender() -> Bool

    /**
     * Provide Feedback
     **/
    var enableContactNumberField: Bool? { get }
    func getEmailPlaceholder() -> String
    var shouldPullProvideFeedbackContentFromCMS: Bool? {get}
    
    /**
     * Screenings and Vaccinations
     */
    func showSVLearnMorePartnersLink() -> Bool
    func showSVHistoryMenu() -> Bool
    func getSVRemoveButtonLabel(numberOfPhotos:Int) -> String
    func shouldAllowProofGreaterThanSelection() -> Bool
    func getSVCardActionTitle(status: CardStatusTypeRef, metadataEarnedPoints: String) -> String?
    func participatingPartnersLinkHeight() -> CGFloat
    func checkForHealthcarePDFDisplay() -> Bool
    func withDefaultStringValueForSVTestedDate() -> Bool

    /**
     * VHC Learn More
     */
    func getCommonStringsLearnMoreSection3MessageBmi(party: VitalityParty?) -> String
    func getCommonStringsLearnMoreSection3MessageWaistCircumference(party: VitalityParty?) -> String
    func getCommonStringsLearnMoreSection3MessageGlucose(party: VitalityParty?) -> String
    func getCommonStringsLearnMoreSection3MessageBloodPressure(party: VitalityParty?) -> String
    func getCommonStringsLearnMoreSection3MessageCholesterol(party: VitalityParty?) -> String
    func getCommonStringsLearnMoreSection3MessageFastingGlucose(party: VitalityParty?) -> String
    func getCommonStringsLearnMoreSection3MessageUrineProtein(party: VitalityParty?) -> String
    func getCholesterolAttributeGroupName() -> String
    func getHealthCarePDFTypeKey() -> AppConfigFeature.AppConfigFeatureType
    func didVHCBloodPressureCapturedCompletely(systolic: VHCCapturedResult?, diastolic: VHCCapturedResult?) -> Bool
    func isCapturedVHCIsValid(bloodPressureValid: Bool, capturedResults: Results<VHCCapturedResult>) -> Bool
    /**
     * VHC Landing
     */
    func getCommonStringsMeasurementCholesterolSectionTitle() -> String
    func showVHCHistoryMenu() -> Bool
    /**
     * VHC Capture
     */
    func getBMIDelimiter() -> String
    func initMultiUnitField(isValid: Bool?, topInputText: String?, bottomInputText: String?, numberFormatter: NumberFormatter, baseUnit: Double, completion: (_ topValue: String, _ bottomValue: String) -> Void)
    func getPassedValue(top: Double, bottom: Double, baseUnit: Double, selectedUnitOfMeasureType: UnitOfMeasureRef, isTopInputValid: Bool, isBottomInputValid: Bool, completion: (_ valid: Bool, _ passedValue: String) -> Void)
    func healthyRangeFeedback(from feedback: VHCHealthAttributeFeedback, isSystolicOrDiastolicHealthy: Bool, explanation: String?) -> VHCFeedback?
    /**
     * Help
     */
     var shouldLimitSearchResults: Bool? { get }
    
    /* MyHealth Landing */
    func getMyHealthCards() -> [MyHealthCardItem]
    
    /* Screenings and Vaccinations Landing */
    func getSVLandingHeaderMessage() -> String
    
    /* VHC, VHR Height Weight Input fields */
    func shouldChangeCharactersInAssessmentNumberRangeQuestionSection(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String, selectedUnit: Unit, questionTypeKey: Int, healthAttributeType: PartyAttributeTypeRef) -> Bool 
    func onValidateCapturedResults(isBMIValid: Bool, isCholesterolValid: Bool, areInputsValid: Bool, onError: () -> Void, onNext: () -> Void)
    func getVHRSystemFormattedNumber(from localizedString: String?, selectedUnitOfMeasureType: UnitOfMeasureRef) -> String?
    
    /* VHC Assessment */
    func withDefaultStringValueForVHCCapturedDate() -> Bool
    
    /* VHR */
    func isValid(questionnaireSection: VHRQuestionnaireSection) -> Bool
    func shouldAcceptDecimalInput(_ selectedUnit: Unit, questionTypeKey: Int, healthAttributeType: PartyAttributeTypeRef) -> Bool
    func shouldExecuteGroupValidation() -> Bool
    var vhrDisableKeyboardDecimalInput: Bool? { get }
    func vhrRemoveUnknownUOM() -> Bool
    func vhrDisplayUoMAsSymbol() -> Bool
    func decimalFormatter(shouldAcceptDecimalInput: Bool) -> NumberFormatter?
    func showVHRDisclaimer() -> Bool
    
    /* Organised Fitness Events */
    func includeAllProofs() -> Bool
    
    /* Organised Fitness Events */
    func getOFEFormattedDate(dateString: String) -> String
    
    /* Organised Fitness Events */
    func configureOFEHistoryDetailsRow() -> Bool
    
    /* Organised Fitness Events */
    func withDefaultStringValueForOFEEventDate() -> Bool

    /* AWCLandingViewModel */
    func getVMPURL() -> URL?

    /* Insurance Reward */
    var phraseAppImplementation: Bool? { get }
    
    /* Home Screen Card Resizing */
    var resizeHomeCard: Bool? { get }
    
    /* VNA */
    var disableKeyboardDecimalInput: Bool? { get }

    func getSVCompletionViewModel() -> AnyObject?
    
    /* SV */
    func getSVHealthActionCell(at indexPath: IndexPath, tableView: UITableView,
                          screeningsTotalPotentialPoints: Int,
                          screeningsTotalEarnedPoints: Int,
                          vaccinationsTotalPotentialPoints: Int,
                          vaccinationsTotalEarnedPoints: Int) -> SAVHealthActionViewCell
    
    /* Status */
    func getStatusPointsEarningActivitiesViewCell(tableView: UITableView, indexPath: IndexPath,
                                                         pointsActivityName: String?, potentialPointsString: String?,
                                                         eventKey: Int, activityCode: String?) -> UITableViewCell
    
    /* Status */
    func getAnnualStatusViewModel() -> AnyObject?
    func getPointsEarningActivitiesViewModel() -> AnyObject?

    /* OFE */
    func getImageControllerConfiguration() -> AnyObject?

    /* Active Rewards - Benefit Guide*/
    func navigateToBenefitGuide(segue: UIStoryboardSegue)
    
    /* Active Rewards - Starbucks */
    func hideRewardsSubtitle() -> Bool
    
    /* Active Rewards */
    func getActivityHistoryViewModel() -> AnyObject?

    /* Active Rewards - Swapping */
    func getAvailableRewardViewModel() -> AnyObject?

    /* Activation Barcode */
    var useAcceptEncodingDeflateHeader: Bool? { get }
    
    /* Points Monitor - Activity Detail */
    func getPointsMonitorActivityDetailViewModel() -> AnyObject?
    
    func getFirstTimePreferenceViewModel() -> AnyObject?

}

public class VIAApplicableFeatures: ApplicableFeaturesDataSource{
    public func showPartyIdDetail() -> Bool {
        return dataSource?.showPartyIdDetail() ?? false
    }
    
    public func shouldIncludeUsedAndExpiredSpinInHistory() -> Bool {
        return dataSource?.shouldIncludeUsedAndExpiredSpinInHistory() ?? false
    }

    public func isCapturedVHCIsValid(bloodPressureValid: Bool, capturedResults: Results<VHCCapturedResult>) -> Bool {
        return dataSource?.isCapturedVHCIsValid(bloodPressureValid: bloodPressureValid, capturedResults: capturedResults) ?? false
    }
    
    public func didVHCBloodPressureCapturedCompletely(systolic: VHCCapturedResult?, diastolic: VHCCapturedResult?) -> Bool {
        return dataSource?.didVHCBloodPressureCapturedCompletely(systolic:systolic, diastolic:diastolic) ?? false
    }
    
    public weak var dataSource: ApplicableFeaturesDataSource?
    
    open static let `default` = {
        return VIAApplicableFeatures()
    }()
    
    public var enableMobileNumber: Bool?{
        get{
            return dataSource?.enableMobileNumber ?? true
        }
    }
    
    public var enableNotificationPreference: Bool?{
        get{
            return dataSource?.enableNotificationPreference ?? false
        }
    }
    
    
    public var userPartyIDAsVitalityNumber: Bool?{
        get{
            return dataSource?.userPartyIDAsVitalityNumber ?? false
        }
    }
    
    public var enableTouchID: Bool?{
        get{
            return dataSource?.enableTouchID ?? false
        }
    }

    public func touchIdValidatePassword() -> Bool {
        return dataSource?.touchIdValidatePassword() ?? false
    }
    
    public var hideHelpTab: Bool?{
        get{
            return dataSource?.hideHelpTab ?? false
        }
    }
    
    public var showMembershipBanner: Bool?{
        get{
            return dataSource?.showMembershipBanner ?? true
        }
    }
    
    public var showBannerInAspectRatio: Bool?{
        get{
            return dataSource?.showBannerInAspectRatio ?? false
        }
    }

    public var enableEntityNumber: Bool?{
        get{
            return dataSource?.enableEntityNumber ?? false
        }
    }
    
    public func shouldShowReminderMessageOnVHC(attribute: PartyAttributeTypeRef) -> Bool{
      return dataSource?.shouldShowReminderMessageOnVHC(attribute: attribute) ?? false
    }
    
    public var showShareVitalityStatus: Bool?{
        get{
            return dataSource?.showShareVitalityStatus ?? false
        }
    }
    
    public var enableChangePassword: Bool?{
        get{
            return dataSource?.enableChangePassword ?? false
        }
    }
    public var hideCholesterolContent: Bool?{
        get{
            return dataSource?.hideCholesterolContent ?? false
        }
    }

    public var enableUpdateEmail: Bool? {
        get{
            return dataSource?.enableUpdateEmail ?? true
        }
    }
    
    public func enableGender() -> Bool {
        return dataSource?.enableGender() ?? true
    }
    
    public var shouldUseGetEligibility: Bool? {
        get{
            return dataSource?.shouldUseGetEligibility ?? true
        }
    }
    
    public var hideTermsAndConditionsDisagreeButton: Bool?{
        get{
            return dataSource?.hideTermsAndConditionsDisagreeButton ?? false
        }
    }
    
    public var reverseValidValuesOptionList: Bool? {
        get{
            return dataSource?.reverseValidValuesOptionList ?? false
        }
    }
    
    public var shouldLimitNumberOfAttachProof: Bool? {
        get{
            return dataSource?.shouldLimitNumberOfAttachProof ?? true
        }
    }
    
    public var showHelpContactFooter: Bool? {
        get{
            return dataSource?.showHelpContactFooter ?? true
        }
    }
    
    public var enableDelayInSplashScreenDisplay: Bool? {
        get{
            return dataSource?.enableDelayInSplashScreenDisplay ?? true
        }
    }
    
    public var hideMembershipPassAndUpdateTitle: Bool?{
        get{
            return dataSource?.hideMembershipPassAndUpdateTitle ?? false
        }
    }
    
    public var showPreviousMembershipYearTitle: Bool?{
        get{
            return dataSource?.showPreviousMembershipYearTitle ?? false
        }
    }
    
    public var getPointPeriod: PointsPeriod? {
        get{
            return dataSource?.getPointPeriod ?? PointsPeriod.current
        }
    }
    
    public var enableKeyboardAutoToolbar: Bool? {
        get {
            return dataSource?.enableKeyboardAutoToolbar ?? false
        }
    }
    
    public func hideParticipatingPartners() -> Bool {
        return dataSource?.hideParticipatingPartners() ?? false
    }
    
    public func showHomeInitialScreen() {
        dataSource?.showHomeInitialScreen()
    }
    
    public func navigateToHomeScreen() {
        dataSource?.navigateToHomeScreen()
    }
    
    public func navigateToLoginScreen() {
        dataSource?.navigateToLoginScreen() 
    }
    
    public var numberOfDecimalPlaces: Int? {
        get {
            return dataSource?.numberOfDecimalPlaces ?? 1
        }
    }
    
    //TODO: Change such that can be invoked with VIAApplicableFeatures.default.getTabViewControllers()
    public var showPartnersTabItem: Bool?{
        get{
            return dataSource?.showPartnersTabItem ?? false
        }
    }
    
    public var rememberMePreferenceDefault: Bool?{
        get{
            return dataSource?.rememberMePreferenceDefault ?? false
        }
    }
    
    public func getMembershipBannerImage() -> UIImage? {
            return dataSource?.getMembershipBannerImage() ?? VIACommonAsset.carrierBrandingAreaGray.templateImage
    }
    
    /* Get the Push notification state from device's settings. */
    public var applyPushNotifToggle: Bool?{
        get{
            return dataSource?.applyPushNotifToggle ?? false
        }
    }
    
    public var enableNotificationDialogConfirmation: Bool {
        get{
            return dataSource?.enableNotificationDialogConfirmation ?? false
        }
    }
    
    public func getLogoutRedirectURL() -> String?{
        return dataSource?.getLogoutRedirectURL() ?? ""
    }
    
    public func shouldDisplayEarningPointsAndPointsLimits(for eventKey: Int) -> Bool {
        return dataSource?.shouldDisplayEarningPointsAndPointsLimits(for: eventKey) ?? true
    }
    
    public func getTextFieldViewCell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        return dataSource?.getTextFieldViewCell(tableView: tableView, indexPath: indexPath) ?? tableView.dequeueReusableCell(withIdentifier: VIATextFieldTableViewCell.defaultReuseIdentifier, for: indexPath) as! VIATextFieldTableViewCell
    }
    
    public func removeAppleWatchOnPartnersData() -> Bool {
        return dataSource?.removeAppleWatchOnPartnersData() ?? false
    }
    
    public func setMinimumDate() -> Date? {
        return dataSource?.setMinimumDate() ?? Date()
    }
    
    /**
     * Usage:
     * ARRewardsFlowCoordinator.swift
     **/
    public var showSwapRewardsOption: Bool?{
        get {
            return dataSource?.showSwapRewardsOption ?? false
        }
    }
    
    public var partnerDetailViewControllerActionImage: UIImage? {
        get{
            return dataSource?.partnerDetailViewControllerActionImage ?? VIACommonAsset.partnersGetStarted.templateImage
        }
    }

    public var hideARLearnMoreBenefitGuide: Bool?{
        get{
            return dataSource?.hideARLearnMoreBenefitGuide ?? false
        }
    }
    
    public func shouldFilterPartnerGroups() -> Bool {
        return dataSource?.shouldFilterPartnerGroups() ?? true
    }
    
    public var redirectToWebVoucher: Bool? {
        get{
            return dataSource?.redirectToWebVoucher ?? false
        }
    }
    
    public var filterRewardByStatusOnly: Bool {
        get{
            return dataSource?.filterRewardByStatusOnly ?? false
        }
    }
    
    public func getPartnerDetailViewControllerActionTitle(with partnerTypeKey: Int? = nil) -> String {
        return (dataSource?.getPartnerDetailViewControllerActionTitle(with:partnerTypeKey ?? -1))!
    }
    public func getPartnerRewardInstructions(with voucherString: String) -> String {
        return dataSource?.getPartnerRewardInstructions(with:voucherString) ?? CommonStrings.Ar.Rewards.StarbucksIssuedRewardReadyForCollectionHtml1085(voucherString)
    }
    
    public var convertJoulesToCalories: Bool?{
        get{
            return dataSource?.convertJoulesToCalories ?? false
        }
    }
    
    public var filterRewardTypeCode: Bool?{
        get{
            return dataSource?.filterRewardTypeCode ?? false
        }
    }
    
    public var enableUserToResendInsurerCode: Bool? {
        get{
            return dataSource?.enableUserToResendInsurerCode ?? false
        }
    }
    public var shouldPullProvideFeedbackContentFromCMS: Bool? {
        get{
            return dataSource?.shouldPullProvideFeedbackContentFromCMS ?? false
        }
    }
    
    public func showLoginRestriction() -> Bool {
        return dataSource?.showLoginRestriction() ?? false
    }
    
    public var enableContactNumberField: Bool? {
        get {
            return dataSource?.enableContactNumberField ?? true
        }
    }
    
    public var showDateOfBirthField: Bool?{
        get{
            return dataSource?.showDateOfBirthField ?? false
        }
    }
    
    /**
     * Usage:
     *
     **/
    public var changeMenuItemLocation: Bool?{
        get{
            return dataSource?.changeMenuItemLocation ?? false
        }
    }
    
    /**
     * Usage:
     * ProfileMembershipPassViewController.swift
     **/
    public var showMembershipPassInfoButton: Bool?{
        get{
            return dataSource?.showMembershipPassInfoButton ?? true
        }
    }
    
    /**
     * VHC Learn More
     * BodyMassIndexLearnMore.swift
     */
    public func getCommonStringsLearnMoreSection3MessageBmi(party: VitalityParty?) -> String{
        return dataSource?.getCommonStringsLearnMoreSection3MessageBmi(party: party) ?? CommonStrings.LearnMore.BmiSection3Message227
    }
    
    /**
     * VHC Learn More
     * WaistCircumferenceLearnMore.swift
     */
    public func getCommonStringsLearnMoreSection3MessageWaistCircumference(party: VitalityParty?) -> String{
        return dataSource?.getCommonStringsLearnMoreSection3MessageWaistCircumference(party: party) ?? CommonStrings.LearnMore.WaistCicumferneceSection3Message230
    }
    
    /**
     * VHC Learn More
     * GlucoseLearnMore.swift
     */
    public func getCommonStringsLearnMoreSection3MessageGlucose(party: VitalityParty?) -> String{
        return dataSource?.getCommonStringsLearnMoreSection3MessageGlucose(party: party) ?? CommonStrings.LearnMore.GlucoseSection3Message233
    }
    
    /**
     * VHC Learn More
     * BloodPressureLearnMore.swift
     */
    public func getCommonStringsLearnMoreSection3MessageBloodPressure(party: VitalityParty?) -> String{
        return dataSource?.getCommonStringsLearnMoreSection3MessageBloodPressure(party: party) ?? CommonStrings.LearnMore.BloodPressureSection3Message236
    }
    
    /**
     * VHC Learn More
     * CholesterolLearnMore.swift
     */
    public func getCommonStringsLearnMoreSection3MessageCholesterol(party: VitalityParty?) -> String{
        return dataSource?.getCommonStringsLearnMoreSection3MessageCholesterol(party: party) ?? CommonStrings.LearnMore.CholesterolSection3Message239
    }
    
    /**
     * VHC Learn More
     * HbA1cLearnMore.swift
     */
    public func getCommonStringsLearnMoreSection3MessageFastingGlucose(party: VitalityParty?) -> String{
        return dataSource?.getCommonStringsLearnMoreSection3MessageFastingGlucose(party: party) ?? CommonStrings.LearnMore.Hba1cSection3Message242
    }
    
    /**
     * VHC Learn More
     * UrineProteinLearnMore.swift
     */
    public func getCommonStringsLearnMoreSection3MessageUrineProtein(party: VitalityParty?) -> String{
        return dataSource?.getCommonStringsLearnMoreSection3MessageUrineProtein(party: party) ?? CommonStrings.LearnMore.UrineProteinSection3Message348
    }
    
    public var hideCholesterolRatio: Bool?{
        get{
            return dataSource?.hideCholesterolRatio ?? false
        }
    }
    
    public func getCommonStringsMeasurementCholesterolSectionTitle() -> String {
        
        return dataSource?.getCommonStringsMeasurementCholesterolSectionTitle() ?? CommonStrings.Measurement.CholesterolRatioTitle1173
    }
    
    public func getEmailPlaceholder() -> String {
        
        return dataSource?.getEmailPlaceholder() ?? CommonStrings.Registration.EmailFieldPlaceholder27
    }
    
    public func showSVLearnMorePartnersLink() -> Bool{
        return dataSource?.showSVLearnMorePartnersLink() ?? true
    }
    
    public func showSVHistoryMenu() -> Bool{
        return dataSource?.showSVHistoryMenu() ?? false
    }
    
    public func getCholesterolAttributeGroupName() -> String {
        return dataSource?.getCholesterolAttributeGroupName() ?? CommonStrings.Measurement.CholesterolTitle138
    }
    
    public func getSVRemoveButtonLabel(numberOfPhotos:Int) -> String{
        if numberOfPhotos == 1{
            return CommonStrings.Proof.RemoveButton179.replacingOccurrences(of: "(null) Photos", with: String(describing: numberOfPhotos) + " Photo")
        }
        return CommonStrings.Proof.RemoveButton179.replacingOccurrences(of: "(null)", with: String(describing: numberOfPhotos))
    }
    
    public func shouldAllowProofGreaterThanSelection() -> Bool{
        return dataSource?.shouldAllowProofGreaterThanSelection() ?? false
    }
    
    public func onValidateCapturedResults(isBMIValid: Bool, isCholesterolValid: Bool, areInputsValid: Bool, onError: () -> Void, onNext: () -> Void){
        dataSource?.onValidateCapturedResults(isBMIValid: isBMIValid, isCholesterolValid: isCholesterolValid, areInputsValid: areInputsValid, onError: onError, onNext:onNext)
    }
    
    /**
     * VHCCapture
     */
    public func showVHCHistoryMenu() -> Bool {
        return dataSource?.showVHCHistoryMenu() ?? false
    }
    
    public func getBMIDelimiter() -> String {
        return dataSource?.getBMIDelimiter() ?? ""
    }
    
    public func initMultiUnitField(isValid: Bool?, topInputText: String?, bottomInputText: String?, numberFormatter: NumberFormatter, baseUnit: Double, completion: (String, String) -> Void) {
        dataSource?.initMultiUnitField(isValid: isValid, topInputText: topInputText, bottomInputText: bottomInputText, numberFormatter: numberFormatter, baseUnit: baseUnit, completion: completion)
    }
    
    public func getPassedValue(top: Double, bottom: Double, baseUnit: Double, selectedUnitOfMeasureType: UnitOfMeasureRef, isTopInputValid: Bool, isBottomInputValid: Bool, completion: (Bool, String) -> Void) {
        dataSource?.getPassedValue(top: top, bottom: bottom, baseUnit: baseUnit, selectedUnitOfMeasureType: selectedUnitOfMeasureType, isTopInputValid: isTopInputValid, isBottomInputValid: isBottomInputValid, completion: completion)
    }
    
    public func healthyRangeFeedback(from feedback: VHCHealthAttributeFeedback, isSystolicOrDiastolicHealthy: Bool, explanation: String?) -> VHCFeedback? {
        return dataSource?.healthyRangeFeedback(from: feedback, isSystolicOrDiastolicHealthy: isSystolicOrDiastolicHealthy, explanation: explanation)
    }
    
    /* MyHealth Landing */
    public func getMyHealthCards() -> [MyHealthCardItem]{
        return dataSource?.getMyHealthCards() ?? []
    }
    
    public var reloadMyHealthOnViewAppear: Bool?{
        get{
            return dataSource?.reloadMyHealthOnViewAppear ?? false
        }
    }
    
    /**
     * Usage:
     * VIAWellnessDeviceDetailViewController.swift
     **/
    public var shouldDisplayWellnessDevicesAndDataSharingConsent: Bool? {
        get {
            return dataSource?.shouldDisplayWellnessDevicesAndDataSharingConsent ?? true
        }
    }
    
    /**
     * Usage:
     * VIAWellnessDeviceDetailViewController.swift
     **/
    public func getThirdOnboardingItem() -> OnboardingContentItem {
        
        return dataSource?.getThirdOnboardingItem() ?? OnboardingContentItem(heading: CommonStrings.Wda.Onboarding.Item3Heading421, content:  CommonStrings.Wda.Onboarding.Item3422, image: VIAUIKitAsset.WellnessDevices.wellnessDevicesPoints.image)
    }
    
    /** 
     * Usage: 
     * MyHealthCardTableViewCell.swift 
    **/
    public var concatenateAgeBlurb: Bool? {
        get {
            return dataSource?.concatenateAgeBlurb ?? false
        }
    }

    public func shouldSkipMyHealthLandingScreen() -> Bool {
        return dataSource?.shouldSkipMyHealthLandingScreen() ?? false
    }
    
    /* Screenings and Vaccinations Landing */
    public func getSVLandingHeaderMessage() -> String{
        return dataSource?.getSVLandingHeaderMessage() ?? CommonStrings.Sv.LandingHealthActionMessage1009
    }
    
    public func getHealthCarePDFTypeKey() -> AppConfigFeature.AppConfigFeatureType {
        return dataSource?.getHealthCarePDFTypeKey() ?? .VHCHealthcareBenefit
    }
    
    public var showPartnerGetStartedLink: Bool?{
        get{
            return dataSource?.showPartnerGetStartedLink ?? true
        }
    }
    
    public func getSVCardActionTitle(status: CardStatusTypeRef, metadataEarnedPoints: String) -> String?{
        return dataSource?.getSVCardActionTitle(status:status, metadataEarnedPoints: metadataEarnedPoints)
    }
    
    
    public func participatingPartnersLinkHeight() -> CGFloat {
        return dataSource?.participatingPartnersLinkHeight() ?? 50
    }
    
    public func checkForHealthcarePDFDisplay() -> Bool {
        return dataSource?.checkForHealthcarePDFDisplay() ?? false
    }
    
    /**
     * Usage:
     * AssessmentNumberRangeQuestionSectionController.swift
     * VHCBMICaptureResultsCollectionViewCell+TextField.swift
     * VHCSingleCaptureResultsCollectionViewCell.swift
     **/
    public func shouldChangeCharactersInAssessmentNumberRangeQuestionSection(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String, selectedUnit: Unit, questionTypeKey: Int, healthAttributeType: PartyAttributeTypeRef) -> Bool {
        return dataSource?.shouldChangeCharactersInAssessmentNumberRangeQuestionSection(_:textField, shouldChangeCharactersIn: range, replacementString: string, selectedUnit: selectedUnit, questionTypeKey: questionTypeKey, healthAttributeType: healthAttributeType) ?? false
    }
    
    public func getVHRSystemFormattedNumber(from localizedString: String?, selectedUnitOfMeasureType: UnitOfMeasureRef) -> String? {
        
        return dataSource?.getVHRSystemFormattedNumber(from: localizedString, selectedUnitOfMeasureType: selectedUnitOfMeasureType)
    }
    
    public var removeAssessmentGuidanceText: Bool?{
        get{
            return dataSource?.removeAssessmentGuidanceText ?? false
        }
    }
    
    public var showRequiredPrompt: Bool?{
        get{
            return dataSource?.showRequiredPrompt ?? false
        }
    }
    
    public func getDataSharingLeftBarButtonTitle() -> String {
        return dataSource?.getDataSharingLeftBarButtonTitle() ?? CommonStrings.DisagreeButtonTitle49
    }
    
    /**
     * Usage:
     * AssessmentQuestionnaireSectionViewModel.swift
     **/
    public func isValid(questionnaireSection: VHRQuestionnaireSection) -> Bool {
        return dataSource?.isValid(questionnaireSection: questionnaireSection) ?? false
    }
    
    public func shouldAcceptDecimalInput(_ selectedUnit: Unit, questionTypeKey: Int, healthAttributeType: PartyAttributeTypeRef) -> Bool {
        return dataSource?.shouldAcceptDecimalInput(selectedUnit, questionTypeKey: questionTypeKey, healthAttributeType: healthAttributeType) ?? false
    }
    
    public func getDataSharingRightBarButtonTitle() -> String {
        return dataSource?.getDataSharingRightBarButtonTitle() ?? CommonStrings.AgreeButtonTitle50
    }
    
    /**
     * Usage:
     * AssessmentNumberRangeQuestionSectionController.swift
     **/
    public func shouldExecuteGroupValidation() -> Bool {
        return dataSource?.shouldExecuteGroupValidation() ?? false
    }
    
    /* Organised Fitness Events */
    /**
     * Usage:
     * OFEClaimPointsViewController.swift
     * OFESelectedPhotosCollectionViewController.swift
     * OFEAddWeblinkProofViewController.swift
     * OFESummaryViewController.swift
     * Reference CR: https://jira.vitalityservicing.com/browse/VA-20365
     **/
    public func includeAllProofs() -> Bool {
        return dataSource?.includeAllProofs() ?? false
    }
    
    /* Organised Fitness Events */
    /**
     * Usage:
     * OFEHistoryViewController
     **/
    public func getOFEFormattedDate(dateString: String) -> String{
        return dataSource?.getOFEFormattedDate(dateString: dateString) ?? ""
    }
    
    /* Organised Fitness Events */
    /**
     * Usage:
     * OFEHistoryDetailsViewController
     **/
    public func configureOFEHistoryDetailsRow() -> Bool{
        return dataSource?.configureOFEHistoryDetailsRow() ?? false
    }
    
    /*Wellness Partners, Health Partners, Rewards Partners*/
    /**
     * Usage:
     * PartnersDetailTableViewController.swift
     **/
    public var shouldExecuteSSO: Bool? {
        get{
            return dataSource?.shouldExecuteSSO ?? false
        }
    }
    
    /* Apple Watch Cashback */
    /**
     * Usage:
     * AWCLandingViewModel.swift
     */
    public func getVMPURL() -> URL? {
        return dataSource?.getVMPURL()
    }

    /* Insurance Reward */
    public var phraseAppImplementation: Bool? {
        get{
            return dataSource?.phraseAppImplementation ?? false
        }
    }
    
    /* Home Screen Card Resizing */
    public var resizeHomeCard: Bool? {
        get{
            return dataSource?.resizeHomeCard ?? false
        }
    }
    
    /**
     * Usage:
     * VIAHelpViewController.swift
     **/
    public var shouldLimitSearchResults: Bool? {
        get{
            return dataSource?.shouldLimitSearchResults ?? false
        }
    }
    
    /**
     * Usage:
     * VHR
     */
    public var vhrDisableKeyboardDecimalInput: Bool? {
        get {
            return dataSource?.vhrDisableKeyboardDecimalInput ?? false
        }
    }
    
    public func vhrRemoveUnknownUOM() -> Bool {
        return dataSource?.vhrRemoveUnknownUOM() ?? false
    }
    
    public func vhrDisplayUoMAsSymbol() -> Bool {
        return dataSource?.vhrDisplayUoMAsSymbol() ?? false
    }
    
    public func decimalFormatter(shouldAcceptDecimalInput: Bool) -> NumberFormatter? {
        return dataSource?.decimalFormatter(shouldAcceptDecimalInput: shouldAcceptDecimalInput) ?? nil
    }
    
    public func showVHRDisclaimer() -> Bool {
        return dataSource?.showVHRDisclaimer() ?? false
    }

    /**Usage:
     * VHCCaptureSummaryViewModel.swift
     */
    public var removeEmptyCapturedData: Bool?{
        get{
            return dataSource?.removeEmptyCapturedData ?? false
        }
    }
    
    /** Usage:
     *
     */
    public var disableKeyboardDecimalInput: Bool? {
        get {
            return dataSource?.disableKeyboardDecimalInput ?? false
        }
    }

    public func getSVCompletionViewModel() -> AnyObject?{
        return dataSource?.getSVCompletionViewModel()
    }
    
    /* SV */
    public func getSVHealthActionCell(at indexPath: IndexPath, tableView: UITableView,
                          screeningsTotalPotentialPoints: Int,
                          screeningsTotalEarnedPoints: Int,
                          vaccinationsTotalPotentialPoints: Int,
                          vaccinationsTotalEarnedPoints: Int) -> SAVHealthActionViewCell{
        return dataSource?.getSVHealthActionCell(at: indexPath, tableView: tableView,
                                            screeningsTotalPotentialPoints: screeningsTotalPotentialPoints,
                                            screeningsTotalEarnedPoints: screeningsTotalEarnedPoints,
                                            vaccinationsTotalPotentialPoints: vaccinationsTotalPotentialPoints,
                                            vaccinationsTotalEarnedPoints: vaccinationsTotalEarnedPoints) ?? SAVHealthActionViewCell()
    }
    
    /* Status */
    public func getStatusPointsEarningActivitiesViewCell(tableView: UITableView, indexPath: IndexPath,
                                                         pointsActivityName: String?, potentialPointsString: String?,
                                                         eventKey: Int, activityCode: String?) -> UITableViewCell{
        return dataSource?.getStatusPointsEarningActivitiesViewCell(tableView: tableView, indexPath: indexPath,
                                                                    pointsActivityName: pointsActivityName,
                                                                    potentialPointsString: potentialPointsString,
                                                                    eventKey: eventKey, activityCode: activityCode) ?? UITableViewCell()
    }
    
    public func getAnnualStatusViewModel() -> AnyObject?{
        return dataSource?.getAnnualStatusViewModel() ?? nil
    }
    
    public func getPointsEarningActivitiesViewModel() -> AnyObject?{
        return dataSource?.getPointsEarningActivitiesViewModel() ?? nil
    }
    
    /**
     * Usage:
     * OFESelectImageViewController.swift
     **/
    public func getImageControllerConfiguration() -> AnyObject?{
        return dataSource?.getImageControllerConfiguration() ?? nil
    }
    
    /**
     * Usage:
     * VIAARActivityHistoryViewController.swift
     **/
    public func getActivityHistoryViewModel() -> AnyObject? {
        return dataSource?.getActivityHistoryViewModel() ?? nil
    }

    /**
     * Usage:
     * VIAARAvailableRewardViewModel.swift
     **/
    public func getAvailableRewardViewModel() -> AnyObject? {
        return dataSource?.getAvailableRewardViewModel() ?? nil
    }

    /**
     * Usage:
     * ActivationBarcodeRouter.swift
     **/
    
    public var useAcceptEncodingDeflateHeader: Bool? {
        get {
            return dataSource?.useAcceptEncodingDeflateHeader ?? false
        }
    }

    /**
     * Usage:
     * VIAPointsEarningPotentialWithTiersTableViewController.swift
     **/
    public var earnPointsByBMI: Bool? {
        get {
            return dataSource?.earnPointsByBMI ?? false
        }
    }
    
    public func persistVHCData() -> Bool {
        return dataSource?.persistVHCData() ?? false
    }
    
    public func navigateToBenefitGuide(segue: UIStoryboardSegue){
        dataSource?.navigateToBenefitGuide(segue: segue)
    }
    
    public func hideRewardsSubtitle() -> Bool{
        return dataSource?.hideRewardsSubtitle() ??  false
    }
    
    public func enabledEmptyStatusView() -> Bool {
        return dataSource?.enabledEmptyStatusView() ?? false
    }
    
    public func hideVitalityCoinsFromRewards() -> Bool {
        return dataSource?.hideVitalityCoinsFromRewards() ?? false
    }
    
    /**
     * Usage:
     * OFEEventTypeViewController
     * OFEClaimPointsViewController
     **/
    public func withDefaultStringValueForOFEEventDate() -> Bool{
        return dataSource?.withDefaultStringValueForOFEEventDate() ?? false
    }

    /**
     * Usage:
     * SVHealthActionViewController.swift
     **/
    public func withDefaultStringValueForSVTestedDate() -> Bool{
        return dataSource?.withDefaultStringValueForSVTestedDate() ?? false
    }
    
    /**Usage:
     * VHCOptionListCaptureResultsCollectionViewCell.swift
     * VHCSingleCaptureResultsCollectionViewCell.swift
     * VHCBloodPressureInputSectionController.swift
     * VHCCaptureResultsCollectionViewController.swift
     * VHCCaptureResultsViewModel.swift
     * VHCOptionListInputSectionController.swift
     * VHCSingleMeasurableInputSectionController.swift
     */
    public func withDefaultStringValueForVHCCapturedDate() -> Bool {
        return dataSource?.withDefaultStringValueForVHCCapturedDate() ?? false
    }
    
    /**
     * Usage:
     * PointsMonitorActivityDetailViewModel.swift
     **/
    public func getPointsMonitorActivityDetailViewModel() -> AnyObject? {
        return dataSource?.getPointsMonitorActivityDetailViewModel() ?? nil
    }
    
    public func getFirstTimePreferenceViewModel() -> AnyObject? {
        return dataSource?.getFirstTimePreferenceViewModel() ?? nil
    }
}
