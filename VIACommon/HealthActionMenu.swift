//
//  HealthActionMenu.swift
//  VIACommon
//
//  Created by OJ Garde on 7/29/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import VitalityKit

/*
 * This is just an interim location for S&V Classes. It should be transferred once S&V target is created.
 */
public enum HealthActionMenu: Int {
    case screenings = 0
    case vaccinations = 1
    
    public func text() -> String? {
        switch self {
        case .screenings:
            return CommonStrings.Sv.ScreeningsTitle1012
        case .vaccinations:
            return CommonStrings.Sv.VaccinationsTitle1013
        }
    }
}
