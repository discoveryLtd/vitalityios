//
//  VIACoordinatedTableViewController.swift
//  VitalityActive
//
//  Created by Joshua Ryan on 6/30/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

import VIAUIKit

open class VIACoordinatedTableViewController: VIATableViewController, VIACoordinatable {

	// MARK: Public
	open weak var coordinator: VIAFlowDelegate?

	// MARK: UIViewController
    
    open override func awakeFromNib() {
        super.awakeFromNib()
    }
    
	override open func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		super.prepare(for: segue, sender: sender)

		coordinator?.configureController(vc: segue.destination)
	}
}
