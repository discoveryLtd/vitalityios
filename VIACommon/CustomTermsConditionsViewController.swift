import UIKit
import WebKit
import SnapKit
import VitalityKit
import VitalityKit
import VIAUIKit
import SafariServices

public protocol CustomTermsAndConditionsController {
    var rightButtonTitle: String { get }
    var leftButtonTitle: String { get }
    var showLeftButton: Bool { get }
    var showRightButton: Bool { get }
    func rightButtonTapped(_ sender: UIBarButtonItem)
    func leftButtonTapped(_ sender: UIBarButtonItem)
    func returnToPreviousView()
}

open class CustomTermsConditionsViewController: VIAViewController, CustomTermsAndConditionsController, WKNavigationDelegate, CustomTermsAndConditionsViewModelDelegate, SafariViewControllerEscapable {

    private var webView: WKWebView = WKWebView(frame: .zero)
    private var rightButton = UIBarButtonItem()
    private var leftButton = UIBarButtonItem()
    private var toolbar = UIToolbar()
    private var hasLoadedContent = false
    public var viewModel: CustomTermsConditionsViewModel?

    public var showLeftButton = true {
        didSet {
            configureToolbarButtons()
        }
    }

    public var showRightButton = true {
        didSet {
            configureToolbarButtons()
        }
    }

    public var leftButtonTitle = CommonStrings.DisagreeButtonTitle49 {
        didSet {
            leftButton.title = leftButtonTitle
        }
    }

    public var rightButtonTitle = CommonStrings.AgreeButtonTitle50 {
        didSet {
            rightButton.title = rightButtonTitle
        }
    }

    // MARK: View life cycle

    open override func viewDidLoad() {
        super.viewDidLoad()
        viewModel?.delegate = self
        configureAppearance() // make sure this is called before configureViews
        configureViews()
        configureToolbarBorder()
    }

    open func configureAppearance() {
        navigationController?.makeNavigationBarTransparent()
    }

    open override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        loadWebContent()
        configureToolbarBorder()
        configureAppearance()
    }

    open override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        configureToolbarBorder()
    }

    func configureViews() {
		rightButton.action = #selector(rightButtonTapped)
		leftButton.action = #selector(leftButtonTapped)
        configureToolbar()
        configureWebView()
    }
    
    func configureToolbar() {
        self.view.addSubview(toolbar)
        toolbar.backgroundColor = UIColor.tableViewBackground()
        toolbar.snp.makeConstraints({(make) -> Void in
            make.left.equalToSuperview()
            make.right.equalToSuperview()
            // enable for Xcode 9
//            if #available(iOS 11, *) {
//                make.bottom.equalTo(self.view.safeAreaLayoutGuide.snp.bottomMargin)
//            } else {
                make.bottom.equalToSuperview()
//            }
        })
        configureToolbarButtons()
    }

    func configureToolbarBorder() {
        toolbar.layer.addBorder(edge: .top)
    }

    func configureToolbarButtons () {
        toolbar.items = []
        leftButton.title = nil
        rightButton.title = nil
        rightButton.style = UIBarButtonItemStyle.done
        if showLeftButton == true {
            leftButton.title = leftButtonTitle
            toolbar.items?.append(leftButton)
        }

        toolbar.items?.append(UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil))

        if showRightButton == true {
            rightButton.title = rightButtonTitle
            toolbar.items?.append(rightButton)
        }
    }

    func configureWebView() {
        webView.navigationDelegate = self
        var navigationBarHeight: CGFloat = 0
        if let navController = navigationController {
            navigationBarHeight = navController.navigationBar.isHidden ? 0 : navController.navigationBar.frame.height
        }

        let statusBarHeight = UIApplication.shared.statusBarFrame.height
        view.addSubview(webView)
        webView.snp.makeConstraints({(make) -> Void in
            make.top.equalTo(view).offset(navigationBarHeight + statusBarHeight)
            make.left.equalToSuperview()
            make.right.equalToSuperview()
            make.bottom.equalTo(toolbar.snp.top)
        })
    }

    // MARK: Actions

    func loadWebContent() {
        if hasLoadedContent {
            return
        }

        showHUDOnView(view: self.view)
        viewModel?.loadArticleContent(completion: { [weak self] error, content in
            self?.hideHUDFromView(view: self?.view)
            if let error = error {
                self?.handle(error: error)
            }
            if let validContent = content {
                self?.webView.loadHTMLString(validContent, baseURL: nil)
            }
        })
    }

    public func handle(error: Error) {
        rightButton.isEnabled = false
        switch error {
        case is BackendError:
            if let backendError = error as? BackendError {
                self.handleBackendErrorWithAlert(backendError, tryAgainAction: { [weak self] in
                    self?.rightButton.isEnabled = true
                    self?.loadWebContent()
                })
            }
            break
        case is CMSError:
            displayErrorWithUnwindAction()
            break
        default:
            displayErrorWithUnwindAction()
            break
        }
    }

    func displayErrorWithUnwindAction() {
        displayUnknownServiceErrorOccurredAlert(okAction: { [weak self] in
            self?.returnToPreviousView()
        })
    }

    func updateNavigation(rightButtonEnabled: Bool) {
        self.rightButton.isEnabled = rightButtonEnabled
    }

    open func rightButtonTapped(_ sender: UIBarButtonItem) {
        debugPrint("rightButtonTapped")
    }

    open func leftButtonTapped(_ sender: UIBarButtonItem) {
        debugPrint("leftButtonTapped")
    }

    // MARK: WKNavigationDelegate

    public func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        webView.evaluateJavaScript("document.documentElement.outerHTML.toString()", completionHandler: { [weak self] html, error in
            self?.updateNavigation(rightButtonEnabled: html != nil)
            self?.hasLoadedContent = html != nil
        })
    }
    
    public func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        escapeToSafariViewController(with: navigationAction, decisionHandler: decisionHandler)
    }

    open func returnToPreviousView() {
        debugPrint("CMS content error occurred")
    }
}
