//
//  SVCompletionVideModel.swift
//  VIACommon
//
//  Created by OJ Garde on 7/29/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import VIAUIKit
import VitalityKit
import VIAUtilities

import RealmSwift

/*
 * This is just an interim location for S&V Classes. It should be transferred once S&V target is created.
 */
open class SVGenericCompletionViewModel: CirclesViewModel {
    
    private var realm = DataProvider.newRealm()
    public lazy var homeScreenData: Results<HomeScreenData> = {
        return self.realm.allHomeScreenData()
    }()
    
    public required init(){
        
    }
    
    open var image: UIImage? {
        return VIAUIKitAsset.ActiveRewards.Onboarding.arOnboardingActivated.image
    }
    
    open var heading: String? {
        return CommonStrings.Sv.CompletedTitle1021
    }
    
    open var message: String?{
        return CommonStrings.Sv.CompletedMessage1022(getTotalPotentialPoints())
    }
    
    open var footnote: String? {
        return CommonStrings.Confirmation.CompletedFootnotePointsMessage119
    }
    
    open var buttonTitle: String? {
        return CommonStrings.GreatButtonTitle120
    }
    
    open var buttonHighlightedTextColor: UIColor? {
        return UIColor.knowYourHealthGreen()
    }
    
    open var gradientColor: Color {
        return .green
    }
    
    /**
     * getTotalPotentialPoints
     *
     * returns Total Potential Points for Screenings and Vaccinations
     */
    private func getTotalPotentialPoints() -> String{
        guard let data = latestHomeScreenData() else{
            return "0"
        }
        for section in data.sections{
            if section.type == .KnowYourHealth{
                for card in section.cards{
                    if card.type == .ScreenandVacc{
                        return card.potentialPoints()
                    }
                }
            }
        }
        return "0"
    }
    
    private func latestHomeScreenData() -> HomeScreenData? {
        return homeScreenData.first
    }
    
    public func finaliseSubmission() {
        // delete captured results
        let realm = DataProvider.newSAVRealm()
        try! realm.write {
            realm.delete(realm.allSAVItems())
        }
        
        // post notification
        NotificationCenter.default.post(name: .VIAVHCCompletedNotification, object: nil)
    }
}
