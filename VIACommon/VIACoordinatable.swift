//
//  File.swift
//  VitalityActive
//
//  Created by Joshua Ryan on 1/10/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation

public protocol VIAFlowDelegate: class {
	
	func configureController(vc: UIViewController)
}

public protocol VIACoordinatable: class {
	
	weak var coordinator: VIAFlowDelegate? { get set }
}
