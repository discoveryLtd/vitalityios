import Foundation
import VitalityKit
import VIAUIKit
import VitalityKit
import VIAUtilities

public extension HomeScreenData {
    
    func statusTitle() -> String? {
        return self.currentVitalityStatus?.overallVitalityStatusName
    }
    
    func statusIcon() -> UIImage? {
        guard let statusKey: StatusTypeRef = currentVitalityStatus?.overallVitalityStatusKey else {
            return nil
        }
        switch statusKey {
        case .Blue:
            return VIAUIKitAsset.Status.badgeBlueSmall.image
        case .Bronze:
            return VIAUIKitAsset.Status.badgeBronzeSmall.image
        case .Silver:
            return VIAUIKitAsset.Status.badgeSilverSmall.image
        case .Gold:
            return VIAIconography.default.statusLandingGoldStatusIcon
        case .Platinum:
            return VIAUIKitAsset.Status.badgePlatinumSmall.image
        case .Unknown:
            return nil
        }
    }
    
    func configureStatusPointsMessage() -> String? {
        
        guard let currentStatus = self.currentVitalityStatus,
            let pointsToMaintainStatus: Int = currentStatus.pointsToMaintainStatus.value
            else { return nil }
        
        //no carry over status
        //no next status
        guard let nextStatusPointsAccount = nextStatusPointsAccount(),
            let nextStatusName: String = currentStatus.nextVitalityStatusName
            else {
                return CommonStrings.Status.LandingPointsFinalMessage824
        }
        //Carry over status
        //let nextStatusPointsAccountTest = nextStatusPointsAccount()
        if pointsToMaintainStatus > 0, let carryOverStatusName = currentStatus.carryOverVitalityStatusName {
            guard let formattedPointsToMaintainStatus = Localization.integerFormatter.string(from: NSNumber(integerLiteral: pointsToMaintainStatus)) else { return nil }
            return CommonStrings.Status.LandingPointsMaintainMessage819(formattedPointsToMaintainStatus, carryOverStatusName)
        }
        if nextStatusPointsAccount.pointsNeeded == 0 {
            return CommonStrings.Status.LandingPointsFinalMessage824
        } else {
            guard let formattedPointsToNextStatus = Localization.integerFormatter.string(from: NSNumber(integerLiteral: nextStatusPointsAccount.pointsNeeded))  else { return nil }
            return  CommonStrings.Status.LandingPointsTargetMessage797(formattedPointsToNextStatus, nextStatusName)
        }

    }
    
    func calculateStatusProgress() -> Float? {
        guard let currentStatus = self.currentVitalityStatus,
            var currentPoints = currentStatus.totalPoints.value
            else { return nil }
        
        let previousStatusPoints = previousStatusPointsAccount()?.statusPoints ?? 0
        currentPoints = currentPoints - previousStatusPoints
        if currentStatus.pointsStatusKey == currentStatus.highestVitalityStatusKey {
            return 1.0
        } else {
            var percentageComplete: Float = 0.0
            if let nextStatusPoints = nextStatusPointsAccount()?.statusPoints {
                percentageComplete = Float(currentPoints) / Float(nextStatusPoints)
            }
            else if let pointsToMaintainStatus = currentStatus.pointsToMaintainStatus.value {
                percentageComplete = Float(currentPoints) / Float(pointsToMaintainStatus)
            }
            //Design requires that a small red dot appears on the progress bar even if the percentage complete is 0%
            guard percentageComplete > 0.00
                else { return 0.01 }
            
            return percentageComplete
        }
    }
    
    func nextStatusPointsAccount() -> HomePointsAccount? {
        guard let nextStatus = self.currentVitalityStatus?.nextVitalityStatusKey else { return nil }
        for pointsAccount in self.pointsToNextStatuses {
            if pointsAccount.statusKey == nextStatus {
                return pointsAccount
            }
        }
        return nil
    }
    
    func previousStatusPointsAccount() -> HomePointsAccount? {
        let sortedPointsToNextStatuses = pointsToNextStatuses.sorted(by: {
            $0.sortOrder < $1.sortOrder
        })
        let currentStatusKey = self.currentVitalityStatus?.overallVitalityStatusKey
        let currenStatusSortOrder = sortedPointsToNextStatuses.filter({
            $0.statusKey == currentStatusKey
        }).first?.sortOrder
        let previousStatusSortOrder = (currenStatusSortOrder ?? 0 - 1)
        return sortedPointsToNextStatuses.filter({
            $0.sortOrder == previousStatusSortOrder
        }).first
    }
}
