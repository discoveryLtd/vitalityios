//
//  SAVPointsValue.swift
//  VIACommon
//
//  Created by OJ Garde on 7/29/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import VitalityKit

/*
 * This is just an interim location for S&V Classes. It should be transferred once S&V target is created.
 */
public enum SVPointsValue: Int {
    case screenings = 0
    case vaccinations = 1
    
    public func value(_ totalEarnedPoints: Int, _ totalPotentialPoints: Int) -> String? {
        switch self {
        case .screenings:
            let earnedPoints    = formatted(number: totalEarnedPoints)
            let potentialPoints = formatted(number: totalPotentialPoints)
            if totalEarnedPoints > 0{
                return CommonStrings.HomeCard.PointsEarnedMessage252(earnedPoints, potentialPoints)
            }
            return CommonStrings.HomeCard.CardEarnUpToPointsMessage124(potentialPoints)
        case .vaccinations:
            let earnedPoints    = formatted(number: totalEarnedPoints)
            let potentialPoints = formatted(number: totalPotentialPoints)
            if totalEarnedPoints > 0{
                return CommonStrings.HomeCard.PointsEarnedMessage252(earnedPoints, potentialPoints)
            }
            return CommonStrings.HomeCard.CardEarnUpToPointsMessage124(potentialPoints)
        }
    }
    
    private func formatted(number: Int) -> String{
        return Localization.decimalFormatter.number(from: String(number))?.stringValue ?? ""
    }
}
