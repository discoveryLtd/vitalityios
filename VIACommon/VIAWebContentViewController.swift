import Foundation

import VIAUIKit
import VIAUtilities
import VitalityKit

import SafariServices

public class VIAWebContentViewModel: CMSConsumer {
    
    var articleId: String?
    
    var pdfFilename: String?
    
    public init(articleId: String?) {
        self.articleId = articleId
    }
    
    public init(pdfFilename: String?) {
        self.pdfFilename = pdfFilename
    }
    
    public func setArticleId(_ articleId: String?) {
        self.articleId = articleId
    }
    
    func getCMSContent(completion: @escaping ((Error?, String?) -> Void)) {
        if let articleId = articleId {
            if articleId == "vna-data-privacy-policy-content" {
                getVNACMSArticleContent(articleId: articleId, completion: completion)
            } else {
                getCMSArticleContent(articleId: articleId, completion: completion)
            }
        } else {
            completion(nil, nil)
        }
    }
    
    func fileExists(_ fileURL: URL) -> Bool {
        let filePath = fileURL.path
        return FileManager.default.fileExists(atPath: filePath)
    }
    
    func fileURL(for fileName: String) -> URL? {
        guard let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else { return nil }
        
        return documentsURL.appendingPathComponent("\(fileName)")
    }
    
    public enum DownloaderError: Error {
        case couldNotDownload(message: String?, fileName: String?)
        case couldNotSave(message: String?, fileName: String?)
        
        func fileName() -> String? {
            switch self {
            case .couldNotDownload(_, let fileName):
                return fileName
            case .couldNotSave(_, let fileName):
                return fileName
            }
        }
    }
    
    func downloadPDF(completion: @escaping ((String?, Error?) -> Void)) {
        
        guard let fileName = pdfFilename else {
            completion(nil, CMSError.fileNotFound)
            return
        }
        guard let insurerCMSGroupId = AppConfigFeature.insurerCMSGroupId() else {
            completion(nil, CMSError.missingInsurerGroupId)
            return
        }
        
        let escapedFileName = fileName.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) ?? fileName
        
        Wire.Content.getFileByGroupId(fileName: escapedFileName, groupId: insurerCMSGroupId, modifiedSince: nil, completion: { [weak self] data, error in
            if error != nil {
                completion(nil, error)
                return
            }
            guard let responseData = data else {
                completion(nil, error)
                return
            }
            
            do {
                if let pdfURL = self?.fileURL(for: fileName) {
                    try responseData.write(to: pdfURL)
                    return completion(pdfURL.path, nil)
                }
                
            } catch let error as NSError {
                let error = DownloaderError.couldNotSave(message:"File \(fileName).  Error \(error.description)", fileName: fileName)
                return completion(nil, error)
            }
        })
    }
}

public class VIAWebContentViewController: VIAViewController, UIWebViewDelegate, SafariViewControllerEscapable {
    
    public var viewModel: VIAWebContentViewModel?
    open var showNavBar: Bool = false // TODO not sure this param name explains itself properly
    public var isNavBarTransparent = false
    @IBOutlet var webView: UIWebView!
    
    // MARK: View lifecycle
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        self.configureAppearance()
        if isNavBarTransparent {
            self.setupTransparentNavBar()
        }
    }
    
    override public func viewDidAppear(_ animated: Bool) {
        loadContent()
        super.viewDidAppear(animated)
    }
    
    override public func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNavigationItems()
    }
    
    func setupTransparentNavBar() {
        self.navigationController?.makeNavigationBarInvisibleWithoutTint()
    }
    // Mark: - Actions
    
    func share() {
        if let fileName = AppConfigFeature.getFileName(type: VIAApplicableFeatures.default.getHealthCarePDFTypeKey()) {
            guard let filePath = self.viewModel?.fileURL(for: fileName) else { return }
            guard let file = NSData(contentsOf: filePath) else { return }
            let activityViewController = UIActivityViewController(activityItems: [file], applicationActivities: nil)
            activityViewController.popoverPresentationController?.sourceView = self.view
            present(activityViewController, animated: true, completion: nil)
        }
    }
    
    func setupNavigationItems() {
        if(showNavBar) {
            setupHealthcareNavigationItems()
        }
    }
    
    func setupHealthcareNavigationItems() {
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .action, target: self, action: #selector(share))
    }
    
    func configureAppearance() {
        navigationController?.makeNavigationBarTransparent()
        self.webView.backgroundColor = .white
        self.webView.isOpaque = true
        self.webView.scalesPageToFit = true
    }
    
    private func loadContent() {
        if(showNavBar) {
            /** FC-19570
             * Show Temporary Coming Soon Screen.
             **/
            if let tenantID = AppSettings.getAppTenant(), tenantID == .GI {
                configureStatusView(showComingSoonScreen())
            } else {
                self.showHUDOnView(view: self.webView)
                self.viewModel?.downloadPDF(completion: { [weak self] (path, error) in
                    self?.hideHUDFromView(view: self?.webView)
                    
                    guard let filePath = path else { return }
                    guard let fileName = filePath.components(separatedBy: "/").last else {
                        return
                    }
                    let pdfURL = self?.viewModel?.fileURL(for: fileName)
                    let urlRequest = URLRequest(url: pdfURL!)
                    self?.webView.loadRequest(urlRequest)
                    if let webView = self?.webView {
                        self?.view.addSubview(webView)
                    }
                })
            }
        } else {
            self.showHUDOnWindow()
            self.viewModel?.getCMSContent(completion: { [weak self] (error, articleContent) in
                self?.hideHUDFromWindow()
                if let error = error as? BackendError {
                    self?.handleBackendErrorWithAlert(error)
                    return
                }
                if let content = articleContent {
                    self?.webView.loadHTMLString(content, baseURL: nil)
                }
            })
        }
    }
    
    // MARK: UIWebView delegate
    
    public func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        return escapeToSafariViewController(navigationType: navigationType, request: request)
    }
    
    internal func showComingSoonScreen() -> UIView? {
        guard let view = VIAStatusView.viewFromNib(owner: self) else { return nil }
        view.message = "Coming Soon..."
        return view
    }
}
