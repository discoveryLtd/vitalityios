import VitalityKit

import RealmSwift

extension VHCHealthAttributeGroup {
    
    public func groupName() -> String {
        switch self.type {
        case .VHCBMI:
            return CommonStrings.Measurement.BodyMassIndexTitle134
        case .VHCWaistCircum:
            return CommonStrings.Measurement.WaistCircumferenceTitle135
        case .VHCBloodGlucose:
            return CommonStrings.Measurement.GlucoseTitle136
        case .VHCBloodPressure:
            return CommonStrings.Measurement.BloodPressureTitle137
        case .VHCLipidProfile:
            return CommonStrings.Measurement.OtherBloodLipidsTitle848
        case .VHCCholesterol:
            return VIAApplicableFeatures.default.getCommonStringsMeasurementCholesterolSectionTitle()
        case .VHCHbA1c:
            return CommonStrings.Measurement.Hba1cTitle139
        case .VHCUrinaryProtein:
            return CommonStrings.Measurement.UrineProteinTitle283
        default :
            return ""
        }
    }
    
    public class func sortedGroups(from realm: Realm) -> Array<VHCHealthAttributeGroup> {
        let results = Array(realm.allVHCHealthAttributeGroups())
        return results.sorted { ($0 as VHCHealthAttributeGroup).type.rawValue < ($1 as VHCHealthAttributeGroup).type.rawValue }
    }
}
