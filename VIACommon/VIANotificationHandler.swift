//
//  VIANotificationHandler.swift
//  VIAUIKit
//
//  Created by Michelle R. Oratil on 27/10/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import VIAUtilities
import VitalityKit
import VIAUIKit

public protocol NotificationHandler: class{
    func addNotificationWithCompletion(_ completion: @escaping (Notification) -> Void)
}

extension NotificationHandler{
    public func addNotificationWithCompletion(_ completion: @escaping (Notification) -> Void){
        
        if AppSettings.isSelectedNotificationEnabled(){
            NotificationCenter.default.addObserver(forName: .VIAPushNotificationDidTapped, object: nil, queue: nil, using: completion)
        }
    }
}

extension VIAViewController: NotificationHandler{ }

extension VIATableViewController: NotificationHandler{ }
