public protocol AppearanceIconographyDataSource: class {

    func loginLogo(for locale: Locale) -> UIImage
    
    func splashLogo(for locale: Locale) -> UIImage
    
    func homeLogo(for locale: Locale) -> UIImage
    
    var vhcGenericBloodGlucose: UIImage? { get }
    
    var statusLandingGoldStatusIcon: UIImage? { get }
}


public extension AppearanceIconographyDataSource {
    // In the event that the delegate doesn't define it
    var vhcGenericBloodGlucose: UIImage? {
        return UIImage(asset: VIACommonAsset.vhcGenericBloodGlucose)
    }
    
    var statusLandingGoldStatusIcon: UIImage? {
        return UIImage(asset: VIACommonAsset.statusBadgeGoldLarge)
    }
}

public class VIAIconography: AppearanceIconographyDataSource {

    public weak var dataSource: AppearanceIconographyDataSource?

    open static let `default` = {
        return VIAIconography()
    }()

    public func loginLogo(for locale: Locale = Locale.current) -> UIImage {
        return dataSource?.loginLogo(for:locale) ?? UIImage()
    }
    
    public func homeLogo(for locale: Locale) -> UIImage {
        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        let logoURL = documentsURL.appendingPathComponent("logo_home_header.png") // UIImage automatically searches for @2x, @3x etc
        return dataSource?.homeLogo(for: locale) ?? UIImage(contentsOfFile: logoURL.path) ?? loginLogo()
    }
    
    public func splashLogo(for locale: Locale = Locale.current) -> UIImage {
        return dataSource?.splashLogo(for:locale) ?? UIImage()
    }

    public var vhcGenericBloodGlucose: UIImage? {
        // In the event that there is no delegate
        return dataSource?.vhcGenericBloodGlucose ?? UIImage(asset: VIACommonAsset.vhcGenericBloodGlucose)
    }
    
    public var statusLandingGoldStatusIcon: UIImage? {
        return dataSource?.statusLandingGoldStatusIcon ?? UIImage(asset: VIACommonAsset.statusBadgeGoldLarge)
    }
}
