import Foundation
import VIAUIKit
import VitalityKit
import VitalityKit

public protocol BackendErrorHandler: class {

    func handleBackendErrorWithAlert(_ error: BackendError, tryAgainAction: @escaping () -> Void)

    func handleBackendErrorWithAlert(_ error: BackendError)

    func handleBackendErrorWithStatusView(_ error: BackendError, tryAgainAction: @escaping () -> Void) -> UIView?

    func handleBackendErrorWithStatusView(_ error: BackendError) -> UIView?

    func displayGenericConnectivityErrorAlert(tryAgainAction: @escaping (() -> Void))

}

public extension BackendErrorHandler where Self: UIViewController {

    func handleBackendErrorWithStatusView(_ error: BackendError, tryAgainAction: @escaping () -> Void) -> UIView? {
        return self.setupBackendErrorStatusView(error, tryAgainAction: tryAgainAction)
    }

    func handleBackendErrorWithStatusView(_ error: BackendError) -> UIView? {
        return self.setupBackendErrorStatusView(error, tryAgainAction: nil)
    }

    internal func setupBackendErrorStatusView(_ error: BackendError, tryAgainAction: (() -> Void)?) -> UIView? {
        guard let view = VIAStatusView.viewFromNib(owner: self) else { return nil }
        view.buttonTitle = CommonStrings.TryAgainButtonTitle43
        if let tryAgainAction = tryAgainAction {
            view.hideButtonBorder = false
            view.actionBlock = tryAgainAction
        }

        switch error {
        case .network(let networkError):
            if networkError.code == NSURLErrorNotConnectedToInternet {
                view.heading = CommonStrings.ConnectivityErrorAlertTitle44
                view.message = CommonStrings.ConnectivityErrorAlertMessage45
            } else {
                view.heading = CommonStrings.GenericServiceErrorTitle268
                view.message = CommonStrings.GenericServiceErrorMessage269
            }
        default:
            view.heading = CommonStrings.GenericServiceErrorTitle268
            view.message = CommonStrings.GenericServiceErrorMessage269
        }

        return view
    }


    func handleBackendErrorWithAlert(_ error: BackendError, tryAgainAction: @escaping () -> Void) {
        displayBackendErrorAlert(with: error, tryAgainAction: tryAgainAction)
    }

    func handleBackendErrorWithAlert(_ error: BackendError) {
        displayBackendErrorAlert(with: error, tryAgainAction: nil)
    }

    internal func displayBackendErrorAlert(with error: BackendError, tryAgainAction: (() -> Void)?) {
        switch error {
        case .network(let networkError):
            if networkError.code == NSURLErrorNotConnectedToInternet {
                self.displayGenericConnectivityErrorAlert(tryAgainAction: tryAgainAction)
            } else {
                self.displayUnknownServiceErrorOccurredAlert()
            }
        default:
            self.displayUnknownServiceErrorOccurredAlert()
        }
    }

    func displayGenericConnectivityErrorAlert(tryAgainAction: @escaping (() -> Void)) {
        displayGenericConnectivityErrorAlert(tryAgainAction: tryAgainAction)
    }

    internal func displayGenericConnectivityErrorAlert(tryAgainAction: (() -> Void)?) {
        let controller = UIAlertController(title: CommonStrings.ConnectivityErrorAlertTitle44,
                                           message: CommonStrings.ConnectivityErrorAlertMessage45,
                                           preferredStyle: .alert)
        var cancel: UIAlertAction
        if let tryAgainAction = tryAgainAction {
            let tryAgain = UIAlertAction(title: CommonStrings.TryAgainButtonTitle43, style: .default) { action in
                tryAgainAction()
            }
            controller.addAction(tryAgain)
            cancel = UIAlertAction(title: CommonStrings.CancelButtonTitle24, style: .cancel) { action in }
        } else {
            cancel = UIAlertAction(title: CommonStrings.OkButtonTitle40, style: .cancel) { action in }
        }
        controller.addAction(cancel)
        controller.view.tintColor = UIColor.genericErrorAlertButtonText()
        self.present(controller, animated: true, completion: nil)
    }

    func displayUnknownServiceErrorOccurredAlert(okAction: (() -> Void)? = {}) {
        let controller = UIAlertController(title: CommonStrings.GenericServiceErrorTitle268,
                                           message: CommonStrings.GenericServiceErrorMessage269,
                                           preferredStyle: .alert)

        let ok = UIAlertAction(title: CommonStrings.OkButtonTitle40, style: .default) { action in
            if let okAction = okAction {
                okAction()
            }
        }
        controller.addAction(ok)
        controller.view.tintColor = UIColor.genericErrorAlertButtonText()
        self.present(controller, animated: true, completion: nil)
    }

}

extension VIAViewController: BackendErrorHandler {}
extension VIATableViewController: BackendErrorHandler {}
