//
//  URLComponents+Extension.swift
//  VIACommon
//
//  Created by OJ Garde on 6/7/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation

extension URLComponents {
    
    public func getValue(withParam param: String) -> String? {
        return self.queryItems?.first(where: { $0.name == param })?.value
    }
}
