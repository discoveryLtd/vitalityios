import Foundation
import UIKit
import VIAUIKit
import VitalityKit
import VitalityKit
import VIAUtilities

public protocol VitalityAgePresentable {
    func lastDateVitalityAgeShown() -> Date?
    func setLastDateVitalityAgeShown()
    func hasShownDateVitalityAgeShown() -> Bool

    func shouldShowVitalityAge() -> Bool

    func showVitalityAgeIfRequired()
}

extension VitalityAgePresentable {

    internal func dateInBetween(date: Date, from: Date, to: Date) -> Bool {
        return (from.isBefore(date: date, granularity: .hour)) && (to.isAfter(date: date, granularity: .hour))
    }

    public func shownInPeriod() -> Bool {

        if let period = VitalityPartyMembership.currentVitalityMembershipPeriod() {

            if let lastTimeShown = lastDateVitalityAgeShown() {

                if let effectiveFrom = period.effectiveFrom, let effectiveTo = period.effectiveTo {
                    if self.dateInBetween(date: lastTimeShown, from: effectiveFrom, to: effectiveTo) {
                        return true
                    } else {
                        return false
                    }
                }
            }
        }

        return hasShownDateVitalityAgeShown()
    }
}
