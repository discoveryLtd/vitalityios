platform :ios, '10.1'
workspace 'VitalityActive'
project 'VitalityActive'

# ---------------- Pods ---------------------------------------------------------

def app_and_framework_target_pods
	ui_pods
	vitality_kit_pods
	keychain_pods
	rncryptor_pods
end

def test_pods
    pod 'XCTest-Gherkin/Native', '0.10.3'
end

def analytics_pods
    pod 'Firebase', '4.4.0'
    pod 'GoogleTagManager', '6.0'
    pod 'Dynatrace'
    pod 'Fabric', '1.7.7'
    pod 'Crashlytics', '3.10.2'
end

def keychain_pods
    pod 'SwiftKeychainWrapper','~>3.0.1'
end

def rncryptor_pods
   pod 'RNCryptor', '~> 5.0'
end

def ui_pods
    pod 'IQKeyboardManagerSwift', '6.0.4'
    pod 'ChameleonFramework', '2.1.0'
    pod 'JazzHands', '2.0.8'
    pod 'RazzleDazzle', '0.1.5'
    pod 'pop', '1.0.10'
    pod 'TTTAttributedLabel', '2.0.0'
    pod 'SnapKit', '3.1.2'
    pod 'MBProgressHUD', '1.0.0'
    pod 'UICountingLabel', '1.4.0'
    pod 'MKRingProgressView', '1.1.4'
    pod 'SwiftDate', '4.0.12'
    pod 'IGListKit', '2.1.0'
    pod 'KMNavigationBarTransition', '1.1.5'
    pod 'TTGSnackbar', '1.5.3'
    pod 'lottie-ios'
end

def vitality_kit_pods
    pod 'Alamofire', '4.3.0'
    pod 'AlamofireNetworkActivityIndicator', '2.1.0'
    pod 'AlamofireActivityLogger', '2.3.0'
    pod 'RealmSwift', '2.10.2'
    pod 'SwiftDate', '4.0.12'
    pod 'Pushwoosh'
end

# ---------------- App targets ---------------------------------------------------------

target 'VitalityActive' do
    use_frameworks!

    # Pods
    analytics_pods
    app_and_framework_target_pods

    # Tests
    target 'VitalityActiveTests' do
        inherit! :search_paths
        test_pods
    end
    target 'VitalityActiveUITests' do
        inherit! :search_paths
        test_pods
    end

end

target 'VitalityAdvance' do
    use_frameworks!
    
    analytics_pods
    app_and_framework_target_pods
end


target 'Sumitomo' do
    use_frameworks!

    analytics_pods
    app_and_framework_target_pods
end

target 'UKEssentials' do
    use_frameworks!
    
    analytics_pods
    app_and_framework_target_pods
end

target 'IGIVitality' do
    use_frameworks!
    
    analytics_pods
    app_and_framework_target_pods
end

target 'ASRVitality' do
    use_frameworks!
    
    analytics_pods
    app_and_framework_target_pods
end

target 'Ecuador' do
    use_frameworks!
    
    analytics_pods
    app_and_framework_target_pods
end

target 'Generali' do
    use_frameworks!
    
    analytics_pods
    app_and_framework_target_pods
end

target 'Manulife' do
    use_frameworks!
    
    analytics_pods
    app_and_framework_target_pods
end

target 'Maverick' do
    use_frameworks!
    
    analytics_pods
    app_and_framework_target_pods
end

# ---------------- Feature targets -----------------------------------------------------

target 'VIAMyHealth' do
    use_frameworks!
    
    app_and_framework_target_pods
    
    target 'VIAMyHealthTests' do
        inherit! :search_paths
        test_pods
    end
    
end

target 'VIAAssessments' do
    use_frameworks!
    
    app_and_framework_target_pods
    
    target 'VIAAssessmentsTests' do
        inherit! :search_paths
        test_pods
    end
    
end

target 'VIAActiveRewards' do
    use_frameworks!
    
    app_and_framework_target_pods
    
    target 'VIAActiveRewardsTests' do
        inherit! :search_paths
        test_pods
    end
    
end

target 'VIAStatus' do
    use_frameworks!
    
    app_and_framework_target_pods
    
    target 'VIAStatusTests' do
        inherit! :search_paths
        test_pods
    end
    
end

target 'VIAHelpFAQ' do
    use_frameworks!
    
    app_and_framework_target_pods
    
    target 'VIAHelpFAQTests' do
        inherit! :search_paths
        test_pods
    end
    
end

target 'VIADeviceCashback' do
    use_frameworks!
    
    app_and_framework_target_pods
    
    target 'VIADeviceCashbackTests' do
        inherit! :search_paths
        test_pods
    end
    
end

target 'VIAOrganisedFitnessEvents' do
    use_frameworks!
    
    app_and_framework_target_pods
    
    target 'VIAOrganisedFitnessEventsTests' do
        inherit! :search_paths
        test_pods
    end
    
end

target 'VIAAppleWatchCashback' do
    use_frameworks!
    
    app_and_framework_target_pods
    
    target 'VIAAppleWatchCashbackTests' do
        inherit! :search_paths
        test_pods
    end
    
end

target 'VIAInsuranceReward' do
    use_frameworks!
    
    app_and_framework_target_pods
    
    target 'VIAInsuranceRewardTests' do
        inherit! :search_paths
        test_pods
    end
    
end

target 'VIAPointsMonitor' do
    use_frameworks!
    
    app_and_framework_target_pods
    
    target 'VIAPointsMonitorTests' do
        inherit! :search_paths
        test_pods
    end
    
end

target 'VIANonSmokersDeclaration' do
    use_frameworks!
    
    app_and_framework_target_pods
    
    target 'VIANonSmokersDeclarationTests' do
        inherit! :search_paths
        test_pods
    end
    
end

target 'VIAPartners' do
    use_frameworks!
    
    app_and_framework_target_pods
    
    target 'VIAPartnersTests' do
        inherit! :search_paths
        test_pods
    end
    
end

target 'VIAActivationBarcode' do
    use_frameworks!
    
    app_and_framework_target_pods
    
    target 'VIAActivationBarcodeTests' do
        inherit! :search_paths
        test_pods
    end
    
end

target 'VIAImageManager' do
    use_frameworks!
    
    app_and_framework_target_pods
    
    target 'VIAImageManagerTests' do
        inherit! :search_paths
        test_pods
    end
    
end

target 'VIAWellnessDevices' do
    use_frameworks!
    
    app_and_framework_target_pods
    
    target 'VIAWellnessDevicesTests' do
        inherit! :search_paths
        test_pods
    end
    
end

target 'VIAScreeningsVaccinations' do
    use_frameworks!
    
    app_and_framework_target_pods
    
    target 'VIAScreeningsVaccinationsTests' do
        inherit! :search_paths
        test_pods
    end
    
end

target 'VIAHealthCheck' do
    use_frameworks!
    
    app_and_framework_target_pods
    
    target 'VIAHealthCheckTests' do
        inherit! :search_paths
        test_pods
    end
    
end

target 'VIANuffieldServices' do
    use_frameworks!
    
    app_and_framework_target_pods
    
    target 'VIANuffieldServicesTests' do
        inherit! :search_paths
        test_pods
    end
    
end

target 'VIAHomeScreen' do
    use_frameworks!
    
    app_and_framework_target_pods
    
    target 'VIAHomeScreenTests' do
        inherit! :search_paths
        test_pods
    end
    
end

target 'VIAProfileSettings' do
    use_frameworks!
    
    app_and_framework_target_pods
    
    target 'VIAProfileSettingsTests' do
        inherit! :search_paths
        test_pods
    end
    
end

target 'VIALoginRegistration' do
    use_frameworks!
    
    app_and_framework_target_pods
    
    target 'VIALoginRegistrationTests' do
        inherit! :search_paths
        test_pods
    end
    
end

target 'VIAPreferences' do
    use_frameworks!
    
    app_and_framework_target_pods
    
    target 'VIAPreferencesTests' do
        inherit! :search_paths
        test_pods
    end
    
end

target 'VIAFirstTimePreferences' do
    use_frameworks!
    
    app_and_framework_target_pods
    
    target 'VIAFirstTimePreferencesTests' do
        inherit! :search_paths
        test_pods
    end
    
end

target 'MLILogin' do
    use_frameworks!
    
    app_and_framework_target_pods
    
    target 'MLILoginTests' do
        inherit! :search_paths
        test_pods
    end
    
end

target 'VIAHomeV2' do
    use_frameworks!
    
    app_and_framework_target_pods
    
    target 'VIAHomeV2Tests' do
        inherit! :search_paths
        test_pods
    end
    
end

# ---------------- Framework targets ---------------------------------------------------

target 'VIACore' do
    use_frameworks!
    
    app_and_framework_target_pods

    target 'VIACoreTests' do
        inherit! :search_paths
        test_pods
    end

end

target 'VIACommon' do
    use_frameworks!

    app_and_framework_target_pods

    target 'VIACommonTests' do
        inherit! :search_paths
        test_pods
    end

end

target 'VIAUIKit' do
    use_frameworks!

    ui_pods
    pod 'DeallocationChecker', '1.0.2'

    target 'VIAUIKitTests' do
        inherit! :search_paths
        test_pods
    end

end

target 'VitalityKit' do
    use_frameworks!

    vitality_kit_pods

    target 'VitalityKitTests' do
        inherit! :search_paths
        test_pods
    end

end

target 'VIAUtilities' do
    use_frameworks!
    
    target 'VIAUtilitiesTests' do
        inherit! :search_paths
        test_pods
    end
    
end

target 'VIAPushwoosh' do
    use_frameworks!
    
    pod 'TTGSnackbar', '1.5.3'
    pod 'Pushwoosh'
    
    target 'VIAPushwooshTests' do
        inherit! :search_paths
        test_pods
    end
    
end
