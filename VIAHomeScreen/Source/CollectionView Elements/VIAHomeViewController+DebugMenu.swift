import Foundation

extension VIAHomeViewController{
    
    public func openCardsMenu(_ sender: Any?){
        let alert = UIAlertController(title: "Debug Quick Menu", message: "Select a home screen card to open.", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        alert.addAction(UIAlertAction(title: "VHC", style: .default, handler: { [weak self] (action) in
            self?.showVHC(self)
        }))
        
        alert.addAction(UIAlertAction(title: "VHR", style: .default, handler: { [weak self] (action) in
            self?.showVHR(self)
        }))
        
        alert.addAction(UIAlertAction(title: "NSD", style: .default, handler: { [weak self] (action) in
            self?.showNonSmokersDeclaration(self)
        }))
        
        alert.addAction(UIAlertAction(title: "SV", style: .default, handler: { [weak self] (action) in
            self?.showSAV(self)
        }))
        
        alert.addAction(UIAlertAction(title: "MWB", style: .default, handler: { [weak self] (action) in
            self?.showMWB(self)
        }))
        
        alert.addAction(UIAlertAction(title: "VNA", style: .default, handler: { [weak self] (action) in
            self?.showVNA(self)
        }))
        
        alert.addAction(UIAlertAction(title: "Health Partners", style: .default, handler: { [weak self] (action) in
            self?.showHealthPartners(self)
        }))
        
        alert.addAction(UIAlertAction(title: "OFE", style: .default, handler: { [weak self] (action) in
            self?.showOFE(self)
        }))
        
        alert.addAction(UIAlertAction(title: "Device Cashback", style: .default, handler: { [weak self] (action) in
            self?.showDeviceCashback(self)
        }))
        
        alert.addAction(UIAlertAction(title: "Apple Watch Cashback", style: .default, handler: { [weak self] (action) in
            self?.showAppleWatchCashback(self)
        }))
        
        alert.addAction(UIAlertAction(title: "Active Rewards", style: .default, handler: { [weak self] (action) in
            self?.showARLanding(self)
        }))
        
        alert.addAction(UIAlertAction(title: "Wellness Devices", style: .default, handler: { [weak self] (action) in
            self?.showWellnessDevicesAndApps(self)
        }))
        
        alert.addAction(UIAlertAction(title: "Wellness Partners", style: .default, handler: { [weak self] (action) in
            self?.showWellnessPartners(self)
        }))
        
        alert.addAction(UIAlertAction(title: "Reward Partners", style: .default, handler: { [weak self] (action) in
            self?.showRewardPartners(self)
        }))
        
        alert.addAction(UIAlertAction(title: "Policy Cashback", style: .default, handler: { [weak self] (action) in
            self?.showPolicyCashback(self)
        }))
        
        alert.addAction(UIAlertAction(title: "Available Rewards", style: .default, handler: { [weak self] (action) in
            self?.showAvailableRewards(self)
        }))
        
        alert.addAction(UIAlertAction(title: "Activation Barcode", style: .default, handler: { [weak self] (action) in
            self?.showActivationBarcode(self)
        }))
        
        alert.addAction(UIAlertAction(title: "Trigger Crash", style: .destructive, handler: { (action) in
            // For Crashlytics testing purposes
            
            let x: Int? = nil
            let y = 5 + x!
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
}
