import Foundation

class HomeScreenSectionFlowLayout: UICollectionViewFlowLayout {

    var mostRecentOffset: CGPoint = CGPoint()

    override func targetContentOffset(forProposedContentOffset proposedContentOffset: CGPoint, withScrollingVelocity velocity: CGPoint) -> CGPoint {

        if velocity.x == 0 {
            return mostRecentOffset
        }

        if let collectionView = self.collectionView {
            let collectionViewBounds = collectionView.bounds
            let halfWidth = collectionViewBounds.size.width * 0.5

            if let attributesForVisibleCells = self.layoutAttributesForElements(in: collectionViewBounds) {
                var candidateAttributes: UICollectionViewLayoutAttributes?

                for attributes in attributesForVisibleCells {
                    // == Skip comparison with non-cell items (headers and footers) == //
                    if attributes.representedElementCategory != UICollectionElementCategory.cell {
                        continue
                    }

                    if (attributes.center.x == 0) || (attributes.center.x > (collectionView.contentOffset.x + halfWidth) && velocity.x < 0) {
                        continue
                    }
                    candidateAttributes = attributes
                }

                // Beautification step , I don't know why it works!
                if (proposedContentOffset.x == -(collectionView.contentInset.left)) {
                    return proposedContentOffset
                }

                guard let _ = candidateAttributes else {
                    return mostRecentOffset
                }

                let newOffsetX = round(candidateAttributes!.center.x - halfWidth)
                mostRecentOffset = CGPoint(x: newOffsetX, y: proposedContentOffset.y)
                return mostRecentOffset
            }
        }

        // fallback
        mostRecentOffset = super.targetContentOffset(forProposedContentOffset: proposedContentOffset)
        return mostRecentOffset
    }
}
