import Foundation
import UIKit

import VIACommon
import VIAUIKit
import VIAHealthKit
import VitalityKit
import VIAUtilities
import VIAAssessments
import VIAActiveRewards
import VIAStatus
import VIAPartners
import VIAScreeningsVaccinations

import IGListKit
import SnapKit
import TTGSnackbar
import RealmSwift

extension VIAHomeViewController{
    
    public override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showMWB", let viewController = segue.destination as? MWBAssessmentLandingViewController {
            viewController.viewModel = MWBLandingViewModel()
        } else if segue.identifier == "showSAV", let viewController = segue.destination as? SVLandingViewController {
            viewController.viewModel = SAVLandingViewModel()
        } else if segue.identifier == "showVHR", let viewController = segue.destination as? AssessmentLandingViewController {
            viewController.viewModel = VHRLandingViewModel()
        } else if segue.identifier == "showVNA", let viewController = segue.destination as? VNAAssessmentLandingViewController {
            viewController.viewModel = VNALandingViewModel()
        } else if segue.identifier == "showPolicyCashback", let viewController = segue.destination as? VIAWebContentViewController {
            let webContentViewModel = VIAWebContentViewModel(articleId: "ip-sectionarticle1-about")
            viewController.viewModel = webContentViewModel
            viewController.title = CommonStrings.HomeCard.Insurance.SectionTitle1963
        } else if segue.identifier == "showARLanding" {
            debugPrint("showActiveRewards")
        } else if segue.identifier == "showAROnboarding" {
            if let viewController = segue.destination as? VIAOnboardingViewController {
                prepareActiveRewardsOnboarding(for: viewController)
            } else {
                debugPrint("Cannot determine AR product type, can't configure onboarding")
            }
        }
        else if segue.identifier == "showARSpinner", let navController = segue.destination as? UINavigationController {
            if let viewController = navController.topViewController as? VIAARRewardSpinViewController  {
                if let credit = sender as? VIAARCreditViewModel {
                    viewController.credit = credit
                    let flowCoordinator = ARRewardsFlowCoordinator()
                    flowCoordinator.setRewardCredit(as: credit)
                    flowCoordinator.configureController(vc: viewController)
                    viewController.setSpinCompletionDelegate(flowCoordinator)
                    viewController.parentJourney = .HomeCard
                }
            }
        }
        else if segue.identifier == "showARChooseRewards", let navController = segue.destination as? UINavigationController {
            if let viewController = navController.topViewController as? VIAARRewardSelectionViewController  {
                if let credit = sender as? VIAARCreditViewModel {
                    viewController.credit = credit
                    let flowCoordinator = ARRewardsFlowCoordinator()
                    flowCoordinator.setARSelectRewardCredit(as: credit)
                    flowCoordinator.configureController(vc: viewController)
                    viewController.setARSelectCompletionDelegate(flowCoordinator)
                    viewController.parentJourney = .HomeCard
                }
            }
        }
        else if segue.identifier == "showARVoucher", let navController = segue.destination as? UINavigationController {
            if let viewController = navController.topViewController as? VIAARChosenRewardViewController  {
                viewController.parentJourney = .HomeCard
            }
        }
        else if segue.identifier == "showPartnerJourney" {
            
            if let viewController = segue.destination as? PartnersListTableViewController, let type = sender as? ProductFeatureCategoryRef {
                let viewModel = PartnersListTableViewModel(withPartnerType: type)
                viewController.viewModel = viewModel
                if type == ProductFeatureCategoryRef.HealthPartners {
                    viewController.sourceFeatureType = .knowYourHealth
                } else if type == ProductFeatureCategoryRef.WellnessPartners {
                    viewController.sourceFeatureType = .improveYourHealth
                } else if type == ProductFeatureCategoryRef.RewardPartners {
                    viewController.sourceFeatureType = .getRewarded
                }
            }
        }
            
            // TODO: Remove me, quick test link setup
        else if segue.identifier == "showUKEStarbucksSpinner", let navController = segue.destination as? UINavigationController {
            if let viewController = navController.topViewController as? VIAARRewardSpinViewController  {
                // TODO: Set winningVoucherId to match a starbucks reward
                viewController.credit = VIAARCreditViewModel(id: 1,
                                                             earnedDate: Date(),
                                                             expirationDate: nil,
                                                             used: false,
                                                             voucher: nil,
                                                             winningVoucherId: 3,
                                                             rewardSelectionTypeKey: RewardSelectionTypeRef.RewardSelection.rawValue)
            }
        } else if segue.identifier == "showUKECineSpinner", let navController = segue.destination as? UINavigationController {
            if let viewController = navController.topViewController as? VIAARRewardSpinViewController  {
                // TODO: Set winningVoucherId to match a cine reward
                viewController.credit = VIAARCreditViewModel(id: 1,
                                                             earnedDate: Date(),
                                                             expirationDate: nil,
                                                             used: false,
                                                             voucher: nil,
                                                             winningVoucherId: 5,
                                                             rewardSelectionTypeKey: RewardSelectionTypeRef.RewardSelection.rawValue)
            }
        } else if segue.identifier == "showWebVoucher", let webVoucherController = segue.destination as? VIAWebVoucherViewController {
            if let model = sender as? VIAARRewardViewModel {
                webVoucherController.setHTMLContent(as: (model.partnerSysRewardId?.first)!)
                webVoucherController.title = model.title
            }
        }
    }
}

extension VIAHomeViewController: HomeScreenNavigationDelegate {
   
    func showEmployerReward(_ sender: Any?) {
        //TODO: ge20180605 : change ProductFeatureCategoryRef when available
        performSegue(withIdentifier: "showPartnerJourney", sender: ProductFeatureCategoryRef.Rewards)
    }
    
    public static func showHome() {
        VIAApplicableFeatures.default.showHomeInitialScreen()
    }
    
    
    // MARK: Active Rewards
    
    func showActiveRewards(_ sender: Any?) {
        guard let activeRewardsAlreadyActivated = viewModel.activeRewardsIsAlreadyActivated() else {
            debugPrint("Cannot determine Active Rewards activation, likely the user isn't configured to have AR card")
            return
        }
        
        if activeRewardsAlreadyActivated {
            showARLanding(nil)
        } else {
            showHUDOnView(view: view)
            viewModel.vhrIsRequiredBeforeActiveRewardsActivation(completion: { [weak self] error, required in
                self?.hideHUDFromView(view: self?.view)
                if let error = error {
                    self?.handleErrorOccurred(error)
                } else if let required = required, required == true {
                    self?.showVHRRequiredForAR(nil)
                } else if let required = required, required == false {
                    self?.showAROnboarding(nil)
                }
            })
        }
    }
    
    //CA Rewards Journeys
    
    func showVHRRequiredForAR(_ sender: Any?) {
        self.performSegue(withIdentifier: "showVHRRequiredForAR", sender: nil)
    }
    
    func showARLanding(_ sender: Any?) {
        self.performSegue(withIdentifier: "showARLanding", sender: nil)
    }
    
    func showAROnboarding(_ sender: Any?) {
        self.performSegue(withIdentifier: "showAROnboarding", sender: nil)
    }
    
    func showARSpinner(_ sender: Any?) {
        if let rewardIdString = sender as? String, let rewardId = Int(rewardIdString) {
            self.showHUDOnWindow()
            VIAARRewardHelper.getCredit(rewardId, completion: { [weak self] (error, voucherDataProvider) in
                self?.hideHUDFromWindow()
                if let strongSelf = self, let voucherDataProvider = voucherDataProvider {
                    strongSelf.flowController.configureController(vc: strongSelf)
                    self?.performSegue(withIdentifier: "showARSpinner", sender: voucherDataProvider)
                }
            })
        }
    }
    
    func showARChooseRewards(_ sender: Any?) {
        
        //self.performSegue(withIdentifier: "showARChooseRewards", sender: sender)
        
        if let rewardIdString = sender as? String, let rewardId = Int(rewardIdString) {
            self.showHUDOnWindow()
            VIAARRewardHelper.getCredit(rewardId, completion: { [weak self] (error, voucherDataProvider) in
                self?.hideHUDFromWindow()
                if let strongSelf = self, let voucherDataProvider = voucherDataProvider {
                    strongSelf.flowController.configureController(vc: strongSelf)
                    self?.performSegue(withIdentifier: "showARChooseRewards", sender: voucherDataProvider)
                }
            })
        }
    }
    
    func showARVoucher(_ sender: Any?) {
        if let rewardIdString = sender as? String, let rewardId = Int(rewardIdString) {
            self.showHUDOnWindow()
            let tenantId = DataProvider.newRealm().getTenantId()
            let partyId = DataProvider.newRealm().getPartyId()
            
            Wire.Rewards.getAwardedRewardByPartyId(tenantId: tenantId, partyId: partyId, fromDate: Date(), toDate: Date(), completion: { rewardsRequestError, response in
                VIAARRewardHelper.getReward(rewardId, completion: { [weak self] (error, voucherDataProvider) in
                    self?.hideHUDFromWindow()
                    if let error = error as? BackendError {
                        self?.handleBackendErrorWithAlert(error)
                    }
                    if let rewardReference = voucherDataProvider?.rewardReference {
                        switch rewardReference {
                        case .Wheelspin:
                            self?.rewardUKESpinNow(voucherDataProvider)
                        case .ChooseReward:
                            self?.showARChooseRewards(voucherDataProvider)
                        case .StarbucksVoucher:
                            self?.rewardUKEStarbucksViewReward(voucherDataProvider)
                        case .CineworldOrVue:
                            self?.rewardUKECinemaRedeemReward(voucherDataProvider)
                        case .Cineworld:
                            self?.rewardUKECinemaUseReward(voucherDataProvider)
                        case .Vue:
                            self?.rewardUKECinemaUseReward(voucherDataProvider)
                        default:
                            self?.rewardRedeemReward(voucherDataProvider)
                        }
                    }
                })
            })
        }
    }
    
    func showARLearnMore(_ sender: Any?) {
        self.performSegue(withIdentifier: "showARLearnMore", sender: nil)
    }
    
    func showARCurrentRewards(_ sender: Any?) {
        self.performSegue(withIdentifier: "showARCurrentRewards", sender: nil)
    }
    
    //UKE Reward Journeys
    
    func rewardUKESpinNow(_ sender: Any?) {
        performSegue(withIdentifier: "showUKEStarbucksSpinner", sender: nil)
    }
    
    func rewardUKEStarbucksViewReward(_ sender: Any?) {
        flowController.configureController(vc: self)
        if let model = sender as? VIAARRewardViewModel {
            flowController.showReward(model)
        }
    }
    
    func rewardUKEStarbucksUseReward(_ sender: Any?) {
        flowController.configureController(vc: self)
        if let model = sender as? VIAARRewardViewModel {
            flowController.showReward(model)
        }
    }
    
    func rewardUKECinemaUseReward(_ sender: Any?) {
        flowController.configureController(vc: self)
        if let model = sender as? VIAARRewardViewModel {
            flowController.showReward(model)
        }
    }
    
    func rewardUKECinemaRedeemReward(_ sender: Any?) {
        flowController.configureController(vc: self)
        if let model = sender as? VIAARRewardViewModel {
            flowController.showReward(model)
        }
    }
    
    func rewardRedeemReward(_ sender: Any?) {
        
        if let showWebVoucher = VIAApplicableFeatures.default.redirectToWebVoucher, showWebVoucher {
            if let model = sender as? VIAARRewardViewModel {
                self.performSegue(withIdentifier: "showWebVoucher", sender: model)
            }
        }
            
        else {
            flowController.configureController(vc: self)
            if let model = sender as? VIAARRewardViewModel {
                flowController.showReward(model)
            }
        }
    }
    
    func rewardUKECinemaSpin(_ sender: Any?) {
        performSegue(withIdentifier: "showUKECineSpinner", sender: nil)
    }
    
    func showStatus(_ sender: Any?) {
        self.performSegue(withIdentifier: "showStatus", sender: nil)
    }
    
    func showNonSmokersDeclaration(_ sender: Any?) {
        performSegue(withIdentifier: "showNonSmokersDeclaration", sender: nil)
    }
    
    func showVHC(_ sender: Any?) {
        performSegue(withIdentifier: "showVHC", sender: nil)
    }
    
    func showSAV(_ sender: Any?) {
        self.performSegue(withIdentifier: "showSAV", sender: nil)
    }
    
    func showMWB(_ sender: Any?){
        self.performSegue(withIdentifier: "showMWB", sender: nil)
    }
    
    func showVHR(_ sender: Any?) {
        performSegue(withIdentifier: "showVHR", sender: nil)
    }
    
    func showVNA(_ sender: Any?) {
        performSegue(withIdentifier: "showVNA", sender: nil)
    }
    
    func showWellnessDevicesAndApps(_ sender: Any?) {
        performSegue(withIdentifier: "showWellnessDevices", sender: nil)
    }
    
    func showOFE(_ sender: Any?) {
        performSegue(withIdentifier: "showOFE", sender: nil)
    }
    
    func showDeviceCashback(_ sender: Any?) {
        performSegue(withIdentifier: "showDeviceCashback", sender: nil)
    }
    
    func showPolicyCashback(_ sender: Any?) {
        performSegue(withIdentifier: "showPolicyCashback", sender: nil)
    }
    
    func showIntegratedBenefit(_ sender: Any?) {
        performSegue(withIdentifier: "showIntegratedBenefit", sender: nil)
    }
    
    func showStatusModal(_ sender: Any?) {
        performSegue(withIdentifier: "showStatusIncreased", sender: nil)
    }
    
    func showARAvailableRewards(_ sender: Any?) {
        performSegue(withIdentifier: "showARAvailableRewards", sender: nil)
    }
    
    func showRewardPartners(_ sender: Any?) {
        performSegue(withIdentifier: "showPartnerJourney", sender: ProductFeatureCategoryRef.RewardPartners)
    }
    
    func showHealthPartners(_ sender: Any?) {
        performSegue(withIdentifier: "showPartnerJourney", sender: ProductFeatureCategoryRef.HealthPartners)
    }
    
    func showCorporateBenefitPartners(_ sender: Any?) {
        // changing productFeatureCategoryTypeKey to 13
        performSegue(withIdentifier: "showPartnerJourney", sender: ProductFeatureCategoryRef.FitnessAssessment)
    }
    
    func showNuffieldHealthServices(_ sender: Any?){
        performSegue(withIdentifier: "showNuffield", sender: ProductFeatureCategoryRef.NuffieldHealthServices)
    }
    
    func showActivationBarcode(_ sender: Any?) {
        // TODO - update method when next screen is ready
        performSegue(withIdentifier: "showActivationBarcode", sender: nil)
    }
    
    func showWellnessPartners(_ sender: Any?) {
        performSegue(withIdentifier: "showPartnerJourney", sender: ProductFeatureCategoryRef.WellnessPartners)
    }
    
    func showAppleWatchCashback(_ sender: Any?) {
        performSegue(withIdentifier: "showAppleWatchCashback", sender: nil)
    }
    
    func showAvailableRewards(_ sender: Any?) {
        performSegue(withIdentifier: "showARAvailableRewards", sender: nil)
    }
    
    public func unwindToHomeViewControllerFromNonSmokersDeclaration(segue: UIStoryboardSegue) {
        debugPrint("Unwinding from non-smokers declaration")
    }
    // MARK: Error handling
    
    public func handleErrorOccurred(_ error: Error) {
        switch error {
        case is BackendError:
            if let backendError = error as? BackendError {
                handleBackendErrorWithAlert(backendError)
            }
        default:
            displayUnknownServiceErrorOccurredAlert()
        }
    }
}
