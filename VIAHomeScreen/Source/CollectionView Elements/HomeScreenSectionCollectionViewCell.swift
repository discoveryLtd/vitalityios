import Foundation
import IGListKit

class HomeScreenSectionCollectionViewCell: UICollectionViewCell {

    lazy var collectionView: IGListCollectionView = {

        let layout = HomeScreenSectionFlowLayout()
        layout.scrollDirection = .horizontal

        let collectionView = IGListCollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.backgroundColor = UIColor.tableViewBackground()
        collectionView.alwaysBounceVertical = false
        collectionView.alwaysBounceHorizontal = false
        collectionView.showsVerticalScrollIndicator = false
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.isPagingEnabled = false
        collectionView.decelerationRate = UIScrollViewDecelerationRateFast
        collectionView.contentInset = UIEdgeInsets(top: 0,
                                                   left: VIAHomeViewController.getDefaultHomeScreenLeftContentInset(),
                                                   bottom: 0,
                                                   right: VIAHomeViewController.getDefaultHomeScreenRightContentInset())

        self.contentView.addSubview(collectionView)
        collectionView.snp.makeConstraints({ (make) in
            make.top.left.bottom.right.equalToSuperview()
        })

        return collectionView
    }()

}
