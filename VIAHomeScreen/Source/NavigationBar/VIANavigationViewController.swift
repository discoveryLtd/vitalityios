//
//  VIANavigationViewController.swift
//  VitalityActive
//
//  Created by Wilmar van Heerden on 2016/11/21.
//  Copyright © 2016 Glucode. All rights reserved.
//

import Foundation
import VIAUIKit

public class VIANavigationViewController: UINavigationController {
    
    public override var preferredStatusBarStyle: UIStatusBarStyle {
        if let viewController = self.topViewController {
            return viewController.preferredStatusBarStyle
        }
        return .default
    }
}
