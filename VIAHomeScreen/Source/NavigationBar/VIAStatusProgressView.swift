import VIAUIKit
import UIKit

class VIAStatusProgressView: UIView, Nibloadable {

	@IBOutlet weak var iconView: UIImageView!
	@IBOutlet weak var titleLabel: UILabel!
	@IBOutlet weak var messageLabel: UILabel!
	@IBOutlet weak var progressBar: UIView!
	@IBOutlet weak var progressWidth: NSLayoutConstraint!
	@IBOutlet weak var progressRangeBar: UIView!
	@IBOutlet weak var showStatusButton: UIButton!


	// MARK: Public
	var delegate: HomeScreenNavigationDelegate?

	public var image: UIImage? {
		set {
			self.iconView.image = newValue
		}
		get {
			return self.iconView.image
		}
	}

	public var title: String? {
		set {
			self.titleLabel.text = newValue
		}
		get {
			return self.titleLabel.text
		}
	}

	public var message: String? {
		set {
			self.messageLabel.text = newValue
		}
		get {
			return self.messageLabel.text
		}
	}

    public var _progress: Float = 0.0
	public var progress: Float {
		set {
            _progress = newValue
			progressWidth.constant = CGFloat(newValue) * progressRangeBar.frame.size.width
		}
		get {
			return Float(progressWidth.constant)
		}
	}

	public override func awakeFromNib() {
		super.awakeFromNib()
		setupView()
	}

	func setupView() {
        //TODO: Re-enable the gesture recogniser when delivering the rest of the status journey
        addTapGestureRecogniser()

		self.titleLabel.textColor = UIColor.night()
		self.titleLabel.font = UIFont.title3Font()
		self.titleLabel.backgroundColor = .clear

		self.messageLabel.textColor = UIColor.night()
		self.messageLabel.font = UIFont.footnoteFont()
		self.messageLabel.backgroundColor = .clear

		self.progressBar.layer.cornerRadius = self.progressBar.frame.size.height / 2
		self.progressBar.layer.masksToBounds = true
		self.progressBar.backgroundColor = UIColor.primaryColor()

		self.progressRangeBar.layer.cornerRadius = self.progressRangeBar.frame.size.height / 2
		self.progressRangeBar.layer.masksToBounds = true
		self.progressRangeBar.backgroundColor = UIColor.tableViewBackground()
        
		self.showStatusButton.setImage(VIAHomeScreenAsset.Goals.arrowDrill.image, for: .normal)
	}
    
    func addTapGestureRecogniser() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.showStatus(_:)))
        self.addGestureRecognizer(tap)
        self.isUserInteractionEnabled = true
    }

	override func willMove(toWindow newWindow: UIWindow?) {
		super.willMove(toWindow: newWindow)

		// Use the layer shadow to draw a one pixel hairline under this view to appear as part of the navbar.
		layer.shadowOffset = CGSize(width: 0, height: CGFloat(1) / UIScreen.main.scale)
		layer.shadowRadius = 0
		layer.shadowColor = UIColor.night().cgColor
		layer.shadowOpacity = 0.5
	}

	// MARK: Action
	@IBAction func showStatus(_ sender: Any?) {
		delegate?.showStatus(nil)
	}
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.progress = _progress
    }
    
    override func updateConstraints() {
        self.progress = _progress
        super.updateConstraints()
    }
    
}
