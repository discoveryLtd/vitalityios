import Foundation

import IGListKit
import VitalityKit
import VIAUtilities
import VIAOrganisedFitnessEvents
import VIAActiveRewards
import VIACommon
import RealmSwift
import SwiftDate

class HomeSectionValue: NSObject, IGListDiffable {

    let type: SectionTypeRef
    let priority: Bool
    var cards: [HomeCardValue]
	

    // MARK: Init

    init(homeSection: HomeSection) {
        type = homeSection.type
        priority = homeSection.priority
        cards = []
		
        for card in homeSection.cards {
            cards.append(HomeCardValue(homeCard: card))
        }
		
		for rewardCard in homeSection.rewardCards {
			cards.append(HomeCardValue(rewardHomeCard: rewardCard))
		}
    }

    // MARK: IGListDiffable

    public func diffIdentifier() -> NSObjectProtocol {
        return type as! NSObjectProtocol
    }

    public func isEqual(toDiffableObject object: IGListDiffable?) -> Bool {
        guard self !== object else { return true }
        guard let object = object as? HomeSection else { return false }
        return type == object.type
    }

    // MARK: Convenience

    public func title() -> String {
        switch type {
        case .KnowYourHealth:
            return CommonStrings.SectionTitleKnowYourHealth276
        case .ImproveYourHealth:
            return CommonStrings.SectionTitleImproveYourHealth277
        case .GetRewarded:
            return CommonStrings.SectionTitleGetRewarded278
        case .Unknown:
            return CommonStrings.GenericNotAvailable270
        }
    }

    public func tintColor() -> UIColor {
        switch type {
        case .KnowYourHealth:
            return UIColor.knowYourHealthGreen()
        case .ImproveYourHealth:
            return UIColor.improveYourHealthBlue()
        case .GetRewarded:
            return UIColor.getRewardedPink()
        case .Unknown:
            return UIColor.day()
        }
    }

}

public class HomeCardValue: NSObject, IGListDiffable {

    let type: CardTypeRef
    let status: CardStatusTypeRef
    let validFrom: Date?
    let validTo: Date?
    let amountCompleted: Int
    let unit: CardUnitTypeRef
    let priority: Int
    let total: Int
    let metadataPotentialPoints: String
    let metadataEarnedPoints: String
	let awardedRewardId: String
    let cardItems: [HomeCardItemValue]?
    let metaData: [[CardMetadataTypeRef: String]]?
    let viewModel: VIAHomeViewModel = VIAHomeViewModel.sharedInstance

    lazy var isAppleWatchLinked: Bool = {
        return self.viewModel.isDeviceLinked()
    }()

    lazy var goalStartDate: Date? = {
        if let dateString = self.metaData?.filter({ $0.keys.first == CardMetadataTypeRef.GoalStartDate }).first?.values.first {
            let date = Localization.yearMonthDayFormatter.date(from: dateString)
            return date
        }
        return nil
    }()
    
    lazy var goalEndDate: Date? = {
        if let dateString = self.metaData?.filter({ $0.keys.first == CardMetadataTypeRef.GoalEndDate }).first?.values.first {
            let date = Localization.yearMonthDayFormatter.date(from: dateString)
            return date
        }
        return nil
    }()
    
    lazy var deliverDate: Date? = {
        if let dateString = self.metaData?.filter({ $0.keys.first == CardMetadataTypeRef.DeliveryDate }).first?.values.first {
            let date = Localization.yearMonthDayFormatter.date(from: dateString)
            return date
        }
        return nil
    }()
    
    lazy var nextReward: String? = {
        if let nextRewardValue = self.metaData?.filter({ $0.keys.first == CardMetadataTypeRef.NextRewardValue }).first?.values.first {
            return nextRewardValue
        }
        return nil
    }()
    
    lazy var currentReward: String? = {
        if let currentRewardValue = self.metaData?.filter({ $0.keys.first == CardMetadataTypeRef.CurrentRewardValue }).first?.values.first {
            return currentRewardValue
        }
        return nil
    }()

    // MARK: Init

    public init(homeCard: HomeCard) {
		awardedRewardId = ""
        type = homeCard.type
        status = homeCard.status
        validFrom = homeCard.validFrom
        validTo = homeCard.validTo
        amountCompleted = homeCard.amountCompleted
        unit = homeCard.unit
        priority = homeCard.priority
        total = homeCard.total
        metadataPotentialPoints = homeCard.potentialPoints()
        metadataEarnedPoints = homeCard.earnedPoints()
        metaData = CardMetadataTypeRef.allValues.flatMap({ item -> [CardMetadataTypeRef: String]? in
            let key = item
            let value = homeCard.value(for: item)
            if !value.isEmpty {
                return [key: value]
            }
            return nil
        })
        cardItems = homeCard.cardItems.map({ HomeCardItemValue(item: $0) })
    }
	
	public init(rewardHomeCard: RewardHomeCard) {
		awardedRewardId = rewardHomeCard.awardedRewardId
		type = rewardHomeCard.type
		status = rewardHomeCard.status
		validFrom = rewardHomeCard.validFrom
		validTo = rewardHomeCard.validTo
		amountCompleted = rewardHomeCard.amountCompleted
		unit = rewardHomeCard.unit
		priority = rewardHomeCard.priority
		total = rewardHomeCard.total
		metadataPotentialPoints = rewardHomeCard.potentialPoints()
		metadataEarnedPoints = rewardHomeCard.earnedPoints()
		metaData = CardMetadataTypeRef.allValues.flatMap({ item -> [CardMetadataTypeRef: String]? in
			let key = item
			let value = rewardHomeCard.value(for: item)
			if !value.isEmpty {
				return [key: value]
			}
			return nil
		})
		cardItems = rewardHomeCard.cardItems.map({ HomeCardItemValue(item: $0) })
	}

    // MARK: IGListDiffable

    public func diffIdentifier() -> NSObjectProtocol {
		
        return "\(type.rawValue)-\(awardedRewardId)" as NSObjectProtocol
    }

    public func isEqual(toDiffableObject object: IGListDiffable?) -> Bool {
        guard self !== object else { return true }
        guard let object = object as? HomeCardValue else { return false }
		if awardedRewardId != object.awardedRewardId { return false }
        return type == object.type
    }

    // MARK: Convenience

    public func heading() -> String {
        switch type {
        case .NonSmokersDecl:
            return CommonStrings.HomeCard.CardTitle96
        case .VitalityHealthCheck:
            return CommonStrings.HomeCard.CardTitle125
        case .VitalityHealthReview:
            return CommonStrings.HomeCard.CardSectionTitle291
        case .VitNutAssessment:
            return CommonStrings.HomeCard.CardSectionTitle388
        case .WellDevandApps:
            return CommonStrings.Wda.Title414
        case .AppleWatch:
            return CommonStrings.HomeCard.CardSectionTitle364
        case .ScreenandVacc:
            return CommonStrings.HomeCard.CardSectionTitle365
        case .ActiveRewards:
            return CommonStrings.HomeCard.CardSectionTitle609
        case .RewardPartners:
            return CommonStrings.CardHeadingRewardPartners845
        case .HealthPartners:
            return CommonStrings.CardHeadingHealthPartners843
        case .WellnessPartners:
            return CommonStrings.CardHeadingWellnessPartners844
        case .Rewards:
            return CommonStrings.Ar.HomescreenCard.RewardsTitle785
        case .Vouchers:
            return voucherHeading()
        case .RewardSelection:
            return rewardSelectionHeading()
		case .HealthServices:
			return CommonStrings.HealthServicesTitle1331
        case .MentalWellbeing:
            return CommonStrings.Onboarding.Title1195
        case .DeviceCashback:
            return CommonStrings.Dc.HomeCard.Title1455
        case .PolicyCashback:
            return CommonStrings.HomeCard.Insurance.SectionTitle1963
        case .OrganisedFitnessEvents:
            return CommonStrings.Ofe.Heading.Title1335
        case .NuffieldHealthServices:
            // TODO: PHRASE APP
            return "Our Health, Fitness and Wellbeing partner"
        case .AvailableRewards:
            // TODO: PHRASE APP
            return CommonStrings.CardHeadingActiveChallenge2194
        case .ActivationBarcode:
            return CommonStrings.CardHeadingGymActivation2073
        case .EmployerStatus:
            return CommonStrings.CardHeadingEmployerReward2098 //TODO: ge20180530: heading to phrase app
        case .UKEEmployerStatus:
            return CommonStrings.CardHeadingEmployerReward2098
        default:
            return CommonStrings.GenericNotAvailable270
        }
    }
    
    public func headingImage() -> VIAHomeScreenImageAsset {
        switch type {
        case .NonSmokersDecl:
            return VIAHomeScreenAsset.HomeScreenCards.Headings.nonSmokers
        case .VitalityHealthCheck:
            return VIAHomeScreenAsset.HomeScreenCards.Headings.vhc
        case .ActiveRewards:
            return VIAHomeScreenAsset.HomeScreenCards.Headings.activeRewards
        case .ScreenandVacc:
            return VIAHomeScreenAsset.HomeScreenCards.Headings.screeningsVac
        case .WellDevandApps:
            return VIAHomeScreenAsset.HomeScreenCards.Headings.linkDevices
        case .VitalityHealthReview:
            return VIAHomeScreenAsset.HomeScreenCards.Headings.vhr
        case .VitNutAssessment:
            return VIAHomeScreenAsset.HomeScreenCards.Headings.nutrition
        case .AppleWatch:
            return VIAHomeScreenAsset.HomeScreenCards.Headings.appleWatch
        case .HealthHub:
            return VIAHomeScreenAsset.HomeScreenCards.hsCardExclamationBig
        case .Rewards:
            return VIAHomeScreenAsset.HomeScreenCards.Headings.rewards
        case .Vouchers:
            return VIAHomeScreenAsset.HomeScreenCards.Headings.activeRewards
        case .MentalWellbeing:
            return VIAHomeScreenAsset.HomeScreenCards.Headings.mwb
        case .RewardPartners:
            return VIAHomeScreenAsset.HomeScreenCards.Headings.partnersRewards
        case .HealthPartners:
            return VIAHomeScreenAsset.HomeScreenCards.Headings.partnersHealth
        case .WellnessPartners:
            return VIAHomeScreenAsset.HomeScreenCards.Headings.partnersWellness
        case .OrganisedFitnessEvents:
            return VIAHomeScreenAsset.HomeScreenCards.Headings.events
        case .DeviceCashback:
            return VIAHomeScreenAsset.HomeScreenCards.Headings.appleWatch
        case .PolicyCashback:
            return VIAHomeScreenAsset.HomeScreenCards.Headings.policyCashback
        case .RewardSelection:
            return VIAHomeScreenAsset.HomeScreenCards.Headings.activeRewards
        case .HealthServices:
            return VIAHomeScreenAsset.HomeScreenCards.Headings.employerServices
        case .NuffieldHealthServices:
            // TODO: IMAGE REPLACEMENT
            return VIAHomeScreenAsset.HomeScreenCards.Headings.nuffieldLogo
        case .AvailableRewards:
            // TODO: IMAGE REPLACEMENT
            return VIAHomeScreenAsset.HomeScreenCards.Headings.partnersRewards
        case .ActivationBarcode:
            return VIAHomeScreenAsset.HomeScreenCards.Headings.activationBarcode
        case .EmployerStatus:
            return VIAHomeScreenAsset.HomeScreenCards.Headings.rewardPartners
        case .UKEEmployerStatus:
            return VIAHomeScreenAsset.HomeScreenCards.Headings.rewardPartners
        case .Unknown:
            return VIAHomeScreenAsset.HomeScreenCards.hsCardExclamationBig

        }
    }

    public func title() -> String {
        let missingString = CommonStrings.GenericNotAvailable270
        
        switch type {
        case .NonSmokersDecl:
            if status == .Done { return CommonStrings.Confirmation.CompletedTitle117 }
            return CommonStrings.HomeCard.CardPotentialPointsMessage97(metadataPotentialPoints)
        case .VitalityHealthCheck:
            if status == .InProgress { return CommonStrings.HomeCard.PointsEarnedMessage252(metadataEarnedPoints, metadataPotentialPoints) }
            if status == .Done { return CommonStrings.Confirmation.CompletedTitle117 }
            return CommonStrings.HomeCard.CardEarnUpToPointsMessage124(metadataPotentialPoints)
        case .ActiveRewards:
            if status == .NotStarted { return CommonStrings.Ar.Landing.HomeViewTitle710 }
            if status == .Activated { return  CommonStrings.Ar.HomeCard.ActivatedTitle775 }
            if status == .InProgress {
                let formattedAmountCompleted = Localization.integerFormatter.string(from: NSNumber(integerLiteral: amountCompleted)) ?? "\(amountCompleted)"
                let formattedTotal = Localization.integerFormatter.string(from: NSNumber(integerLiteral: total)) ?? "\(total)"
                return CommonStrings.Ar.HomeCard.InProgressTitle773(formattedAmountCompleted, formattedTotal)
            }
            if status == .Achieved { return CommonStrings.Ar.LandingAchievedTitle768 }
            return missingString
        case .ScreenandVacc:
            if status == .InProgress { return CommonStrings.HomeCard.PointsEarnedMessage252(metadataEarnedPoints, metadataPotentialPoints) }
            if status == .Done { return CommonStrings.Confirmation.CompletedTitle117 }
            return CommonStrings.HomeCard.CardEarnUpToPointsMessage124(metadataPotentialPoints)
        case .WellDevandApps:
            return CommonStrings.Wda.HomeCard.Title415
        case .VitNutAssessment:
            if status == .InProgress { return CommonStrings.HomeCard.CardEarnTitle292(metadataPotentialPoints) }
            if status == .Done { return CommonStrings.Confirmation.CompletedTitle117 }
            return CommonStrings.HomeCard.CardEarnTitle292(metadataPotentialPoints)
        case .VitalityHealthReview:
            if status == .InProgress { return CommonStrings.HomeCard.CardEarnTitle292(metadataPotentialPoints) }
            if status == .Done { return CommonStrings.Confirmation.CompletedTitle117 }
            return CommonStrings.HomeCard.CardEarnTitle292(metadataPotentialPoints)
        case .AppleWatch:
            switch status {
                case .NotStarted:
                    return CommonStrings.Awc.HomeCard.Message1992
                case .AwaitingShipment:
                    return CommonStrings.DcHomeCardMessage1561
                case .ContinueActivation:
                    if isAppleWatchLinked {
                        return CommonStrings.CardActionSubtitle2054
                    } else {
                        return CommonStrings.Awc.HomeCard.EarningCoins.Title2017
                    }
                case .Activated:
                    return CommonStrings.DcHomeCardMessage1527
                case .InProgress:
                    return CommonStrings.Dc.TrackCashbacks.PointsSubtitle1482("\(amountCompleted)")
                case .Achieved:
                    return CommonStrings.Dc.TrackCashbacks.PointsSubtitle1482("\(amountCompleted)")
                default: break
            }
            return CommonStrings.Awc.HomeCard.Message1992
        case .HealthHub:
            return missingString
        case .Rewards:
            return rewardsTitle()
        case .Vouchers:
            return voucherTitle()
        case .MentalWellbeing:
            let intPotentialPoints = (metadataPotentialPoints as NSString).integerValue
            let intEarnedPoints = (metadataEarnedPoints as NSString).integerValue
            if status == .InProgress { return CommonStrings.HomeCard.CardEarnTitleDynamic292("\(intPotentialPoints - intEarnedPoints)") }
            if status == .Done { return CommonStrings.Confirmation.CompletedTitle117 }
            return CommonStrings.HomeCard.CardEarnTitle292(metadataPotentialPoints)
        case .RewardPartners:
            return CommonStrings.CardTitleGreatRewards841
        case .HealthPartners:
            return CommonStrings.CardTitleHealthRewards839
        case .WellnessPartners:
            return CommonStrings.CardTitleWellnessRewards840
        case .OrganisedFitnessEvents:
            return CommonStrings.Ofe.Status.Title1336
        case .DeviceCashback:
            // TODO: PHRASE APP
            if status == .NotStarted { return CommonStrings.Dc.HomeCard.Message1456 }
            if status == .Activated { return "Activated" }
            if status == .AwaitingShipment { return CommonStrings.DcHomeCardMessage1561 }
            if status == .Achieved { return CommonStrings.Dc.TrackCashbacks.PointsTitle1481("\(amountCompleted)") }
            if status == .ContinueActivation { return CommonStrings.Dc.HomeCard.Message1456 }
            if status == .InProgress { return CommonStrings.Dc.TrackCashbacks.PointsTitle1481("\(amountCompleted)") }
            return missingString
        case .PolicyCashback:
            return CommonStrings.HomeCard.Insurance.CardTitle1964
        case .RewardSelection:
            return rewardSelectionTitle()
		case .HealthServices:
			return CommonStrings.CardTitleYourHealthServices1332
        case .NuffieldHealthServices:
            return CommonStrings.MyHealth.CardNuffieldTitle2077
        case .AvailableRewards:
            return CommonStrings.CardTitleAvailableRewards2195
        case .ActivationBarcode:
            return CommonStrings.GymActivationHeaderTitle2074
        case .EmployerStatus:
            return CommonStrings.CardTitleEarnRewardsFromYourEmployer2099 //TODO: ge20180530: string to phrase app
        case .UKEEmployerStatus:
            return CommonStrings.CardTitleEarnRewardsFromYourEmployer2099 //TODO: ge20180530: string to phrase app
        case .Unknown:
            return missingString
        }
    }

    public func subtitle() -> String? {
        switch type {
        case .NonSmokersDecl:
            if status == .Done { return CommonStrings.HomeCard.CardEarnedPointsMessage121(metadataEarnedPoints) }
            return nil
        case .VitalityHealthCheck:
            return nil
        case .ActiveRewards:
            return nil
        case .ScreenandVacc:
            return nil
        case .WellDevandApps:
            return nil
        case .VitNutAssessment:
            if status == .InProgress {
                let sectionsRemaining = (total - amountCompleted) as NSNumber
                if let formattedSectionsRemaining = NumberFormatter.integerFormatter().string(from: sectionsRemaining) {
                    if sectionsRemaining.intValue == 0 {
                        return CommonStrings.HomeCard.SectionsRemainingTitle2241.Zero
                    } else if sectionsRemaining.intValue == 1 && Locale.current.languageCode != "ja" {
                        return CommonStrings.HomeCard.SectionsRemainingTitle2241.One
                    } else {
                        return CommonStrings.HomeCard.SectionsRemainingTitle2241.Other(formattedSectionsRemaining)
                    }
                }
            }
            return nil
        case .VitalityHealthReview:
            if status == .InProgress {
                let sectionsRemaining = (total - amountCompleted) as NSNumber
                if let formattedSectionsRemaining = NumberFormatter.integerFormatter().string(from: sectionsRemaining) {
                    if sectionsRemaining.intValue == 0 {
                        return CommonStrings.HomeCard.SectionsRemainingTitle2241.Zero
                    } else if sectionsRemaining.intValue == 1 && Locale.current.languageCode != "ja" {
                        return CommonStrings.HomeCard.SectionsRemainingTitle2241.One
                    } else {
                        return CommonStrings.HomeCard.SectionsRemainingTitle2241.Other(formattedSectionsRemaining)
                    }
                }
            }
            return nil
        case .AppleWatch:
            switch status {
            case .AwaitingShipment:
                guard let date = deliverDate else { return nil }
                return CommonStrings.DcHomeCardFooter1562(Localization.dayDateShortMonthyearFormatter.string(from: date))
            case .InProgress:
                return CommonStrings.Awc.HomeCard.PointsToReachDescription2027("\(total - amountCompleted)", nextReward ?? "")
            case .Activated:
                guard goalStartDate != nil else {
                    return nil
                }
                // TODO: Will check found discrepancies between date formats displayed accross different devices
                return CommonStrings.DcHomeCardFooter1528(Localization.dayDateShortMonthyearFormatter.string(from: goalStartDate!))
            case .Achieved:
                return CommonStrings.Awc.HomeCard.Points.Description2033("\(String(describing: currentReward!))")
            default: break
            }
            return nil
        case .HealthHub:
            return nil
        case .Rewards:
            return rewardsSubtitle()
        case .Vouchers:
            return voucherSubtitle()
        case .MentalWellbeing:
    //            if status == .Done { return CommonStrings.HomeCard.CardEarnedPointsMessage121(metadataEarnedPoints) }
            if status == .InProgress {
                let sectionsRemaining = (total - amountCompleted) as NSNumber
                if let formattedSectionsRemaining = NumberFormatter.integerFormatter().string(from: sectionsRemaining) {
                    if sectionsRemaining.intValue == 0 {
                        return CommonStrings.HomeCard.SectionsRemainingTitle2241.Zero
                    } else if sectionsRemaining.intValue == 1 && Locale.current.languageCode != "ja" {
                        return CommonStrings.HomeCard.SectionsRemainingTitle2241.One
                    } else {
                        return CommonStrings.HomeCard.SectionsRemainingTitle2241.Other(formattedSectionsRemaining)
                    }
                }
            }
            return nil
        case .RewardPartners:
            return nil
        case .HealthPartners:
            return nil
        case .WellnessPartners:
            return nil
        case .OrganisedFitnessEvents:
            return CommonStrings.Ofe.Action.Title1337
        case .DeviceCashback:
            // TODO: PHRASE APP
            if status == .Activated { return "Starts \(String(describing: goalStartDate))" }
            if status == .AwaitingShipment { return "Estimated Delivery \(String(describing: deliverDate))" }
            if status == .Achieved { return CommonStrings.Dc.HomeCard.ReachedCashbackDesc1569("\(amountCompleted)") }
            if status == .InProgress { return CommonStrings.Dc.HomeCard.PointsToReachDescription1536("\(total)", "\(amountCompleted)")}
            return nil
        case .PolicyCashback:
            return CommonStrings.HomeCard.Insurance.Message1092
        case .RewardSelection:
            return rewardSelectionSubtitle()
		case .HealthServices:
			return nil
        case .NuffieldHealthServices:
            // TODO: PHRASE APP
            return nil
        case .AvailableRewards:
            return nil
        case .ActivationBarcode:
            return nil
        case .EmployerStatus:
            return nil
        case .UKEEmployerStatus:
            return nil
        case .Unknown:
            return nil
        
        }
    }
    
    public func imageLabel() -> String? {
        if type == .AppleWatch
        {
            if status == .InProgress || status == .Achieved {
                return currentReward ?? "0"
            }
        }
        return "0"
    }

    public func actionTitle() -> String? {
        switch type {
        case .NonSmokersDecl:
            if status == .NotStarted { return CommonStrings.HomeCard.CardActionLinkButtonName98 }
            return nil
        case .VitalityHealthCheck:
            if status == .NotStarted { return CommonStrings.GetStartedButtonTitle103 }
            if status == .InProgress { return nil }
            if status == .Done { return CommonStrings.HomeCard.EditUpdateMessage253 }
            return nil
        case .ActiveRewards:
            if status == .NotStarted { return CommonStrings.GetStartedButtonTitle103 }
            if status == .InProgress { return CommonStrings.Ar.Home.ViewGoalButtonTitle765 }
            return nil
        case .ScreenandVacc:
            return VIAApplicableFeatures.default.getSVCardActionTitle(status: status, metadataEarnedPoints: metadataEarnedPoints)
        case .WellDevandApps:
            return nil
        case .VitNutAssessment:
            if status == .NotStarted { return CommonStrings.HomeCard.CardStartButton335}
            if status == .Done { return CommonStrings.HomeCard.EditUpdateMessage253 }
            return nil
        case .VitalityHealthReview:
            if status == .NotStarted { return CommonStrings.GetStartedButtonTitle103 }
            if status == .Done { return CommonStrings.HomeCard.EditUpdateMessage253 }
            return nil
        case .AppleWatch:
            switch status {
                case .NotStarted:
                    return CommonStrings.GetStartedButtonTitle103
                case .ContinueActivation:
                    if !isAppleWatchLinked {
                        return CommonStrings.Awc.HomeCard.EarningCoins.Footer2018
                }
                default: break
            }
            return nil
        case .HealthHub:
            return nil
        case .Rewards:
            return rewardsActionTitle()
        case .Vouchers:
            return vouchersActionTitle()
        case .MentalWellbeing:
                if status == .NotStarted { return CommonStrings.GetStartedButtonTitle103 }
                if status == .Done { return CommonStrings.HomeCard.EditUpdateMessage253 }
                return nil
        case .RewardPartners:
            return CommonStrings.CardActionTitleViewPartners842
        case .HealthPartners:
            return CommonStrings.CardActionTitleViewPartners842
        case .WellnessPartners:
            return CommonStrings.CardActionTitleViewPartners842
        case .OrganisedFitnessEvents:
            return nil
        case .DeviceCashback:
            if status == .NotStarted { return CommonStrings.Dc.HomeCard.Footer1457 }
            // TODO: PHRASE APP
            if status == .ContinueActivation { return "Continue Activation" }
            return nil
        case .PolicyCashback:
            return nil
        case .RewardSelection:
            return rewardSelectionActionTitle()
		case .HealthServices:
			return CommonStrings.CardActionTitleViewServices1333
        case .NuffieldHealthServices:
            return CommonStrings.MyHealth.CardNuffieldSubtitle2078
        case .AvailableRewards:
            // TODO: PHRASE APP
            return CommonStrings.CardActionTitleViewVouchers2193
        case .ActivationBarcode:
            return CommonStrings.GymActivationHeaderSubtitle2075
        case .EmployerStatus:
            return CommonStrings.CardActionTitleViewReward1092
        case .UKEEmployerStatus:
            return CommonStrings.CardActionTitleViewReward1092
        case .Unknown:
            return nil
        
        }
    }

    public func mainImageForCurrentStatus() -> VIAHomeScreenImageAsset {
        switch type {
        case .NonSmokersDecl:
            if status == .Done { return VIAHomeScreenAsset.HomeScreenCards.hsCardCheckmarkBig }
            return VIAHomeScreenAsset.HomeScreenCards.hsCardNonSmokersBig
        case .VitalityHealthCheck:
            if status == .Done { return VIAHomeScreenAsset.HomeScreenCards.hsCardCheckmarkBig }
            return VIAHomeScreenAsset.HomeScreenCards.hsVHCBig
        case .ActiveRewards:
            if status == .NotStarted { return VIAHomeScreenAsset.HomeScreenCards.hsCardLockedBig }
            if status == .Activated { return VIAHomeScreenAsset.HomeScreenCards.hsCardCalendarBig }
            return VIAHomeScreenAsset.HomeScreenCards.hsCardLockedBig
        case .ScreenandVacc:
            if status == .Done { return VIAHomeScreenAsset.HomeScreenCards.hsCardCheckmarkBig }
            return VIAHomeScreenAsset.HomeScreenCards.hsCardSAVBig
        case .WellDevandApps:
            return VIAHomeScreenAsset.HomeScreenCards.hsCardLinkDevicesBig
        case .VitalityHealthReview:
            if status == .Done { return VIAHomeScreenAsset.HomeScreenCards.hsCardCheckmarkBig }
            return VIAHomeScreenAsset.HomeScreenCards.hsCardCheckmarkBig
        case .VitNutAssessment:
            if status == .Done { return VIAHomeScreenAsset.HomeScreenCards.hsCardCheckmarkBig }
            return VIAHomeScreenAsset.HomeScreenCards.hsCardCheckmarkBig
        case .AppleWatch:
            if status == .NotStarted { return VIAHomeScreenAsset.HomeScreenCards.hsCardLockedBig }
            if status == .ContinueActivation {
                if isAppleWatchLinked {
                    return VIAHomeScreenAsset.HomeScreenCards.homeCardAWCPending
                } else {
                    return VIAHomeScreenAsset.HomeScreenCards.hsCardLockedBig
                }
            }
            if status == .Activated { return VIAHomeScreenAsset.HomeScreenCards.hsCardCalendarBig }
            if status == .AwaitingShipment { return VIAHomeScreenAsset.HomeScreenCards.hsCardAwaitingShipmentBig }
            if status == .InProgress || status == .Achieved {
                if let tierValue = self.metaData?.filter({ $0.keys.first == CardMetadataTypeRef.GoalObjectiveKey }).first?.values.first {
                    switch tierValue {
                    case "2":
                        return VIAHomeScreenAsset.HomeScreenCards.hsCardOrangeBlock
                    case "3":
                        return VIAHomeScreenAsset.HomeScreenCards.hsCardBlueBlock
                    case "4":
                        return VIAHomeScreenAsset.HomeScreenCards.hsCardGreenBlock
                    default:
                        break
                    }
                }
                return VIAHomeScreenAsset.HomeScreenCards.hsCardPinkBlock
            }
            return VIAHomeScreenAsset.HomeScreenCards.hsCardLockedBig
        case .HealthHub:
            return VIAHomeScreenAsset.HomeScreenCards.hsCardExclamationBig
        case .Rewards:
            return VIAHomeScreenAsset.HomeScreenCards.hsCardRewardAchievedMain
        case .Vouchers:
            return voucherMainImageForCurrentStatus()
        case .MentalWellbeing:
            if status == .Done { return VIAHomeScreenAsset.HomeScreenCards.hsCardCheckmarkBig }
            return VIAHomeScreenAsset.HomeScreenCards.hsCardExclamationBig
        case .RewardPartners:
            return VIAHomeScreenAsset.HomeScreenCards.hsPartnerRewardsGreatBig
        case .HealthPartners:
            return VIAHomeScreenAsset.HomeScreenCards.hsPartnerRewardsHealthBig
        case .WellnessPartners:
            return VIAHomeScreenAsset.HomeScreenCards.hsPartnerRewardsWellnessBig
        case .OrganisedFitnessEvents:
            return VIAHomeScreenAsset.HomeScreenCards.hsOFEBig
        case .DeviceCashback:
            if status == .NotStarted { return VIAHomeScreenAsset.DeviceCashback.homeCardGetStarted }
            if status == .Activated { return VIAHomeScreenAsset.DeviceCashback.homeCardActivated }
            if status == .AwaitingShipment { return VIAHomeScreenAsset.DeviceCashback.homeCardAwaitingShipment }
            if status == .InProgress || status == .Achieved {
                if  let cardItems = self.cardItems {
                    for card in cardItems {
                        /* Read the items inside HomeCardItemMetaData */
                        for item in card.itemMetaData! {
                            /* Check if "TypeCode" is equal to Goal Objective */
                            if item.typeCode == "GoalObjective" {
                                /* Get the current TIER */
                                let currentTier = item.value
                                if currentTier == "DeviceCashbackT1" {
                                    return VIAHomeScreenAsset.DeviceCashback.homeCardOrangeCoin
                                } else if currentTier == "DeviceCashbackT2" {
                                    return VIAHomeScreenAsset.DeviceCashback.homeCardBlueCoin
                                } else if currentTier == "DeviceCashbackT3" {
                                    return VIAHomeScreenAsset.DeviceCashback.homeCardGreenCoin
                                }
                            } else {
                                /* Default for Vitality Coins */
                                return VIAHomeScreenAsset.DeviceCashback.homeCardPinkCoin
                            }
                        }
                    }
                }
            }
            return VIAHomeScreenAsset.DeviceCashback.homeCardGetStarted
        case .PolicyCashback:
            return VIAHomeScreenAsset.HomeScreenCards.hsPolicyCashbackBig
        case .RewardSelection:
            return VIAHomeScreenAsset.HomeScreenCards.hsCardExclamationBig
        case .HealthServices:
            return VIAHomeScreenAsset.HomeScreenCards.yourEmployerServices
        case .NuffieldHealthServices:
            return VIAHomeScreenAsset.HomeScreenCards.nuffieldNewMainLogo
        case .AvailableRewards:
            // TODO: Change once available
            return VIAHomeScreenAsset.HomeScreenCards.hsCardRewardAchievedMain
        case .ActivationBarcode:
            return VIAHomeScreenAsset.HomeScreenCards.hsActivationBarcode
        case .EmployerStatus:
            return VIAHomeScreenAsset.HomeScreenCards.employerReward
        case .UKEEmployerStatus:
            return VIAHomeScreenAsset.HomeScreenCards.employerReward
        case .Unknown:
            return VIAHomeScreenAsset.HomeScreenCards.hsCardExclamationBig
        }
    }
    
    // MARK: Rewards
    
    var unclaimedRewards: [HomeCardItemValue] {
        if VIAApplicableFeatures.default.filterRewardByStatusOnly{
            return cardItems?.filter({ $0.statusType == CardItemStatusTypeRef.Available }) ?? []
        }else{
            return cardItems?.filter({ $0.statusType == CardItemStatusTypeRef.Available }).filter({ !$0.validTo.isInPast }) ?? []
        }
    }
    
    private var unclaimedRewardsExpiringSoon: [HomeCardItemValue] {
        return unclaimedRewards.filter({ ($0.validTo - 3.days).isInPast })
    }
    
    func rewardsIsLearnMoreState() -> Bool {
        return unclaimedRewards.count == 0
    }
    
    func rewardsHasSingleUnclaimedReward() -> Bool {
        return unclaimedRewards.count == 1
    }

    func rewardsHasMultipleUnclaimedRewards() -> Bool {
        return unclaimedRewards.count > 1
    }
    
    private func rewardsTitle() -> String {
        let countString = Localization.integerFormatter.string(from: unclaimedRewards.count as NSNumber) ?? "\(unclaimedRewards.count)"
        if rewardsHasSingleUnclaimedReward() { return CommonStrings.Ar.RewardsEarnedOneRewardTitle782}
        if rewardsHasMultipleUnclaimedRewards() { return CommonStrings.Ar.RewardsEarnedTitle782(countString) }
        if rewardsIsLearnMoreState() { return CommonStrings.Ar.HomescreenCard.EarnRewardsTitle786 }
        return ""
    }
    
    private func rewardsSubtitle() -> String? {
        let count = unclaimedRewardsExpiringSoon.count
        let countString = Localization.integerFormatter.string(from: count as NSNumber) ?? "\(count)"
        if count > 1 { return CommonStrings.Ar.RewardsExpiringSoonTitle783(countString) }
        if count > 0 { return CommonStrings.Ar.RewardsExpiringSoonTitle783(countString) }
        return nil
    }
    
    private func rewardsActionTitle() -> String? {
        if rewardsIsLearnMoreState() { return CommonStrings.LearnMoreButtonTitle104 }
		else if VitalityProductFeature.isEnabled(.ARProbabilistic) {
            if rewardsHasSingleUnclaimedReward() { return CommonStrings.Ar.Rewards.SpinNowTitle704 }
            if rewardsHasMultipleUnclaimedRewards() { return CommonStrings.Ar.HomescreenCard.AvailableSpinsButtonTitle787 }
        } else if VitalityProductFeature.isEnabled(.ARChoice) {
			return CommonStrings.Ar.Rewards.ChooseRewardTitle724
		}
        return nil
    }
    
    // MARK: Vouchers
    
    private func voucherValidToDateString() -> String? {
        if let date = validTo {
            return CommonStrings.Ar.VoucherExpires658(Localization.dayDateLongMonthyearFormatter.string(from: date))
        }
        return nil
    }
    
    func voucherRewardId() -> Int? {
        if let rewardIdString = self.metaData?.filter({ $0.keys.first == CardMetadataTypeRef.RewardId }).first?.values.first, let rewardId = Int(rewardIdString) {
            return rewardId
        }
        return nil
    }
    
    func voucherValueQuantity() -> String {
        let quantityString = self.metaData?.filter({ $0.keys.first == CardMetadataTypeRef.RewardValueQuantity }).first?.values.first
        return quantityString ?? ""
    }
    
    func voucherValueDescription() -> String {
        let description = self.metaData?.filter({ $0.keys.first == CardMetadataTypeRef.RewardValueDesc }).first?.values.first
        return description ?? ""
    }
    
    private func voucherMainImageForCurrentStatus() -> VIAHomeScreenImageAsset {
        if voucherRewardId() == RewardReferences.StarbucksVoucher.rawValue {
            return VIAHomeScreenAsset.ActiveRewards.Rewards.partnerStarbucks
        }
        // generic voucher image
        return VIAHomeScreenAsset.HomeScreenCards.hsCardRewardsPaceholderBig
    }
    
    private func voucherHeading() -> String {
        let heading = metaData?.filter({ $0.keys.first == CardMetadataTypeRef.RewardName }).first?.values.first
        if heading?.lowercased().range(of:"starbucks") != nil {
            return CommonStrings.Ar.Partners.RewardTitle1058
        }
        return heading ?? ""
    }
    
    private func voucherTitle() -> String {
        
        if voucherRewardId() == RewardReferences.StarbucksVoucher.rawValue {
            return starbucksTitle()
        } else if voucherRewardId() == RewardReferences.CineworldOrVue.rawValue {
            return cinemaTitle()
        } else if voucherRewardId() == RewardReferences.Cineworld.rawValue {
            return cinemaTitle()
		} else if voucherRewardId() == RewardReferences.Vue.rawValue {
			return cinemaTitle()
        } else if voucherRewardId() == RewardReferences.EasyTickets.rawValue {
            return easyTicketTitle()
        }
        // TODO: Fix for VA-27958 when test data is available
        else if voucherRewardId() == RewardReferences.FoodPanda.rawValue {
            return foodPandaTitle()
        }
		
        // generic behaviour
        return self.getAmountOfVoucher()
    }
    
    private func getAmountOfVoucher() -> String {
        
        if let amount = metaData?.filter({ $0.keys.first == CardMetadataTypeRef.RewardValueAmount }).first?.values.first {
            return amount
        } else if let quantity = metaData?.filter({ $0.keys.first == CardMetadataTypeRef.RewardValueQuantity }).first?.values.first {
            return quantity
        } else if let percent = metaData?.filter({ $0.keys.first == CardMetadataTypeRef.RewardValuePercent }).first?.values.first {
            return percent
        }
        
        return ""
    }
    
    private func voucherSubtitle() -> String? {
        if voucherRewardId() == RewardReferences.StarbucksVoucher.rawValue {
            return starbucksSubtitle()
        } else if voucherRewardId() == RewardReferences.CineworldOrVue.rawValue {
            return cinemaSubtitle()
        } else if voucherRewardId() == RewardReferences.Cineworld.rawValue {
            return cinemaSubtitle()
		} else if voucherRewardId() == RewardReferences.Vue.rawValue {
			return cinemaSubtitle()
		}
        return voucherValidToDateString()
    }
    
    private func vouchersActionTitle() -> String? {
        if voucherRewardId() == RewardReferences.StarbucksVoucher.rawValue {
            return starbucksActionTitle()
        } else if voucherRewardId() == RewardReferences.CineworldOrVue.rawValue {
            return cinemaActionTitle()
        } else if voucherRewardId() == RewardReferences.Cineworld.rawValue {
            return cinemaActionTitle()
		} else if voucherRewardId() == RewardReferences.Vue.rawValue {
			return cinemaActionTitle()
		}
        return cinemaActionTitle()
    }
    
    // MARK: Starbucks
    
    private func starbucksLogo() -> VIAHomeScreenImageAsset? {
        return VIAHomeScreenAsset.ActiveRewards.Rewards.partnerStarbucks
    }
    
    private func starbucksTitle() -> String {
        if status == .ConfirmDetails {
            return CommonStrings.CardTitleConfirmDetails1091
        } else if status == .Pending {
            return CommonStrings.CardTitleStarbucksFreeDrink1094
        } else if status == .Available {
            return CommonStrings.CardTitleStarbucksFreeDrink1094
        } else if status == .NotIssued {
            return CommonStrings.CardTitleStarbucksFreeDrinkNotAwarded1096
        }
        return ""
    }
    
    private func starbucksSubtitle() -> String? {
        if status == .ConfirmDetails {
            return voucherValidToDateString()
        } else if status == .Pending {
            return CommonStrings.CardTitleStarbucksFreeDrinkPending1095
        } else if status == .Available {
            return voucherValidToDateString()
        } else if status == .NotIssued {
            return nil
        }
        return nil
    }
    
    private func starbucksActionTitle() -> String? {
        if status == .ConfirmDetails {
            return CommonStrings.CardActionTitleViewReward1092
        } else if status == .Pending {
            return CommonStrings.CardActionTitleViewReward1092
        } else if status == .Available {
            return CommonStrings.CardActionTitleUseReward1093
        } else if status == .NotIssued {
            return CommonStrings.CardActionTitleViewReward1092
        }
        return nil
    }
    
    // MARK: EasyTicket
    private func easyTicketTitle() -> String {
        
        let percentString = metaData?.filter({ $0.keys.first == CardMetadataTypeRef.RewardValuePercent }).first?.values.first as NSString? ?? "0"
        return CommonStrings.Ar.Rewards.RewardPercentageDiscountKeyword2137(String(percentString.integerValue))
    }
    
    // MARK: FoodPanda
    private func foodPandaTitle() -> String {
//        let percentString = metaData?.filter({ $0.keys.first == CardMetadataTypeRef.RewardValueAmount }).first?.values.first as NSString? ?? "0"
//        return ActiveRewardsStrings.Ar.Rewards.RewardPercentageDiscountKeyword9999(String(percentString.integerValue))
        /* FC-19112 */
        return "\(getAmountOfVoucher()) \(voucherValueDescription())"
    }
    
    // MARK: Cinema
    private func cinemaTitle() -> String {
        let intQuantity = Int(Float(voucherValueQuantity())!)
        return "\(intQuantity) \(voucherValueDescription())"
    }
    
    private func cinemaSubtitle() -> String? {
        if status == .Pending {
            return CommonStrings.Ar.Rewards.VoucherCodePending1044
        } else if status == .AvailableToRedeem || status == .Available {
            return voucherValidToDateString()
        }
        return nil
    }
    
    private func cinemaActionTitle() -> String {
        if status == .Pending {
            return CommonStrings.CardActionTitleViewReward1092
        } else if status == .AvailableToRedeem {
            return CommonStrings.CardActionTitleRedeemReward1097
        } else if status == .Available {
            return CommonStrings.CardActionTitleUseReward1093
        }
        return ""
    }
    
    // MARK: RewardSelections
    
    private func rewardSelectionHeading() -> String {
        if voucherRewardId() == RewardReferences.CineworldOrVue.rawValue {
            return CommonStrings.Ar.Rewards.CinemaReward1570
        } else if voucherRewardId() == RewardReferences.Cineworld.rawValue {
            return CommonStrings.CardHeadingCineworld1099
        }else if voucherRewardId() == RewardReferences.Vue.rawValue {
            return CommonStrings.CardHeadingVue1118
        }
        return ""
    }
    
    private func rewardSelectionTitle() -> String {
        if voucherRewardId() == RewardReferences.CineworldOrVue.rawValue {
            return cinemaTitle()
        } else if voucherRewardId() == RewardReferences.Cineworld.rawValue {
            return cinemaTitle()
        } else if voucherRewardId() == RewardReferences.Vue.rawValue {
            return cinemaTitle()
        }
        return ""
    }
    
    private func rewardSelectionSubtitle() -> String? {
        if voucherRewardId() == RewardReferences.CineworldOrVue.rawValue {
            return cinemaSubtitle()
        } else if voucherRewardId() == RewardReferences.Cineworld.rawValue {
            return cinemaSubtitle()
        } else if voucherRewardId() == RewardReferences.Vue.rawValue {
            return cinemaSubtitle()
        }
        return nil
    }
    
    private func rewardSelectionActionTitle() -> String {
        if voucherRewardId() == RewardReferences.CineworldOrVue.rawValue {
            return cinemaActionTitle()
        } else if voucherRewardId() == RewardReferences.Cineworld.rawValue {
            return cinemaActionTitle()
        } else if voucherRewardId() == RewardReferences.Vue.rawValue {
            return cinemaActionTitle()
        }
        return ""
    }
}

struct HomeCardItemValue {
    var validFrom: Date
    var validTo: Date
    var statusType: CardItemStatusTypeRef
    var statusTypeCode: String?
    var statusTypeName: String?
    var type: CardItemTypeRef
    var typeCode: String?
    var typeName: String?
    let itemMetaData: [HomeCardItemMetadataValue]?
	let metaData: [[CardItemMetadataTypeRef: String]]?
	
    init(item: HomeCardItem) {
        self.validFrom = item.validFrom
        self.validTo = item.validTo
        self.statusType = item.statusType
        self.statusTypeCode = item.statusTypeCode
        self.statusTypeName = item.statusTypeName
        self.itemMetaData = item.homeCardItemMetadatas.map({ HomeCardItemMetadataValue(data: $0) })
		metaData = CardItemMetadataTypeRef.allValues.flatMap({ metaItem -> [CardItemMetadataTypeRef: String]? in
			let key = metaItem
			let value = item.value(for: metaItem)
			if !value.isEmpty {
				return [key: value]
			}
			return nil
		})
        self.type = item.type
        self.typeCode = item.typeCode
        self.typeName = item.typeName
    }
}

struct HomeCardItemMetadataValue {
    var type: CardItemMetadataTypeRef
    var typeName: String?
    var typeCode: String?
    var value: String?
    
    init(data: HomeCardItemMetadata) {
        self.type = data.type
        self.typeName = data.typeName
        self.typeCode = data.typeCode
        self.value = data.value
    }
}
