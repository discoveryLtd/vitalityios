import Foundation
import IGListKit


class HomeScreenSectionController: IGListSectionController, IGListSectionType, IGListAdapterDataSource, IGListSupplementaryViewSource {

    var detail: HomeSectionValue!

    lazy var adapter: IGListAdapter = {
        let adapter = IGListAdapter(updater: IGListAdapterUpdater(),
                                    viewController: self.viewController,
                                    workingRangeSize: 0)
        adapter.dataSource = self
        return adapter
    }()

    override init() {
        super.init()

        supplementaryViewSource = self
    }

    // MARK: IGListSectionType

    func numberOfItems() -> Int {
        return 1
    }

    func sizeForItem(at index: Int) -> CGSize {
        return CGSize(width: collectionContext!.containerSize.width, height: 165) // extras above 150 is for shadow at the bottom of the card
    }

    func cellForItem(at index: Int) -> UICollectionViewCell {
        let cell = collectionContext!.dequeueReusableCell(of: HomeScreenSectionCollectionViewCell.self, for: self, at: index) as! HomeScreenSectionCollectionViewCell
        adapter.collectionView = cell.collectionView
        return cell
    }

    func didUpdate(to object: Any) {
        detail = object as! HomeSectionValue
    }

    func didSelectItem(at index: Int) {}

    // MARK: IGListAdapterDataSource

    func objects(for listAdapter: IGListAdapter) -> [IGListDiffable] {
        return detail.cards
    }

    func listAdapter(_ listAdapter: IGListAdapter, sectionControllerFor object: Any) -> IGListSectionController {
        return HomeScreenCardController(tintColor: detail.tintColor())
    }

    func emptyView(for listAdapter: IGListAdapter) -> UIView? {
        return nil
    }

    // MARK: IGListSupplementaryViewSource

    public func supportedElementKinds() -> [String] {
        return [UICollectionElementKindSectionHeader]
    }

    public func viewForSupplementaryElement(ofKind elementKind: String, at index: Int) -> UICollectionReusableView {
        let view = collectionContext!.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader,
                                                                       for: self,
                                                                       class: HomeScreenSectionHeaderSupplementaryView.self,
                                                                       at: index) as! HomeScreenSectionHeaderSupplementaryView
        view.text = detail.title()
        return view
    }

    public func sizeForSupplementaryView(ofKind elementKind: String, at index: Int) -> CGSize {
        return CGSize(width: collectionContext!.containerSize.width,
                      height: HomeScreenSectionHeaderSupplementaryView.homeScreenSectionSupplementaryViewHeight)
    }

}

///////////////////////////////////////////////////////////////////////////

class HomeScreenSectionHeaderSupplementaryView: UICollectionReusableView {
    
    static let homeScreenSectionSupplementaryViewHeight: CGFloat = 30.0

    public var text: String? {
        get {
            return self.label.text
        }
        set {
            self.label.text = newValue
        }
    }

    lazy var container: UIView = {
        let view = UIView(frame: .zero)
        view.backgroundColor = UIColor.tableViewBackground()
        return view
    }()

    lazy var label: UILabel = {
        let label = UILabel(frame: .zero)
        label.textAlignment = .left
        label.textColor = UIColor.mediumGrey()
        label.font = UIFont.systemFont(ofSize: 13.0)
        return label
    }()

    func setupSubviews() {
        self.backgroundColor = .clear

        self.addSubview(container)
        container.snp.makeConstraints({ (make) in
            make.top.left.right.bottom.equalToSuperview()
        })

        container.addSubview(label)
        label.snp.makeConstraints({ (make) in
            make.top.right.bottom.equalToSuperview()
            make.left.equalToSuperview().offset(VIAHomeViewController.getDefaultHomeScreenLeftContentInset())
        })
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

        setupSubviews()
    }

    override init(frame: CGRect) {
        super.init(frame: frame)

        setupSubviews()
    }

}
