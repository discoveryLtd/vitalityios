//
//  ActiveRewardsHomeScreenCardCell.swift
//  VitalityActive
//
//  Created by Simon Stewart on 6/12/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

//let cell = collectionContext!.dequeueReusableCell(of: ActiveRewardsHomeScreenCardCell.self, for: self, at: index) as! ActiveRewardsHomeScreenCardCell
//cell.heading = card.heading()
//cell.setHeadingImage(with: self.card.headingImage(), tintColor: self.tintColor)
//cell.title = "Target achieved"
//cell.setSubtitle("400 of 400 points")
//cell.setActionTitle("View Target", tintColor: self.tintColor)
//
//let model = VIAGraphViewModel(total: 100, startingValue: 0, endingValue: 100)
//model.achievedImageName = "ActiveRewardsTrophy" // TODO:
//
//let graph = VIAGraphView.viewFromNib(owner: nil) as! VIAGraphView
//graph.model = model
//graph.render()
//cell.setMainView(with: graph)
//

import Foundation

class ActiveRewardsHomeScreenCardCell: HomeScreenCardBaseCell {

    static let spacing: CGFloat = 15

    // MARK: Properties

    public var title: String? {
        get {
            return self.titleLabel.text
        }
        set {
            self.titleLabel.text = newValue
        }
    }

    public var subtitle: String? {
        get {
            return self.subtitleLabel.text
        }
    }

    public var actionTitle: String? {
        get {
            return self.actionLabel.text
        }
    }

    // MARK: Views

    lazy private var stackView: UIStackView = {
        let view = UIStackView(frame: .zero)
        view.axis = .horizontal
        view.spacing = spacing
        return view
    }()

    lazy private var mainView: UIView = {
        let view = UIView(frame: .zero)
        return view
    }()

    lazy private var labelsStackView: UIStackView = {
        let view = UIStackView(frame: .zero)
        view.axis = .vertical
        return view
    }()

    lazy private var titleLabel: UILabel = {
        let label = UILabel(frame: .zero)
        label.numberOfLines = 3
        label.font = UIFont.systemFont(ofSize: 22, weight: UIFontWeightHeavy)
        label.textColor = UIColor.night()
        label.adjustsFontSizeToFitWidth = true
        label.minimumScaleFactor = 0.5
        return label
    }()

    lazy private var subtitleLabel: UILabel = {
        let label = UILabel(frame: .zero)
        label.numberOfLines = 0
        label.textColor = UIColor.mediumGrey()
        label.font = UIFont.systemFont(ofSize: 15)
        label.snp.makeConstraints({ (make) in
            make.height.equalTo(40)
        })
        return label
    }()

    lazy private var actionLabel: UILabel = {
        let label = UILabel(frame: .zero)
        label.numberOfLines = 1
        label.textColor = UIColor.mediumGrey()
        label.font = UIFont.systemFont(ofSize: 15)
        label.snp.makeConstraints({ (make) in
            make.height.equalTo(20)
        })
        return label
    }()

    // MARK: Setup

    override func setupCardContentView(_ contentView: UIView) {

        contentView.addSubview(stackView)
        stackView.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.top.left.bottom.right.equalToSuperview().inset(HomeScreenCardSubtitleCell.spacing)
        }

        stackView.addArrangedSubview(mainView)
        mainView.snp.makeConstraints { (make) in
            make.height.equalToSuperview()
            make.width.equalTo(stackView.snp.height)
        }

        let labelsContainer = UIView(frame: .zero)
        stackView.addArrangedSubview(labelsContainer)
        labelsContainer.addSubview(labelsStackView)
        labelsStackView.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.left.right.equalToSuperview()
        }

        labelsStackView.addArrangedSubview(titleLabel)
        labelsStackView.addArrangedSubview(subtitleLabel)
        labelsStackView.addArrangedSubview(actionLabel)

        // hidden by default, setters controls visibility
        subtitleLabel.isHidden = true
        actionLabel.isHidden = true
    }

    override func prepareForReuse() {
        super.prepareForReuse()

        self.title = nil
        self.setSubtitle(nil)
        self.setActionTitle(nil, tintColor: nil)
    }

    // MARK: Setters

    public func setMainView(with view: UIView) {
        mainView.addSubview(view)
        view.snp.makeConstraints { (make) in
            make.height.equalTo(mainView.snp.height)
            make.width.equalTo(mainView.snp.width)
            make.centerX.equalTo(mainView.snp.centerX)
            make.centerY.equalTo(mainView.snp.centerY)
        }
    }

    public func setSubtitle(_ title: String?) {
        subtitleLabel.text = title
        subtitleLabel.isHidden = title == nil
    }

    public func setActionTitle(_ title: String?, tintColor: UIColor?) {
        actionLabel.text = title
        actionLabel.textColor = tintColor
        actionLabel.isHidden = title == nil
    }

}
