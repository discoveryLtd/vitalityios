//
//  AWCHomeScreenCardCell.swift
//  VitalityActive
//
//  Created by Michelle R. Oratil on 11/05/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation

class AWCHomeScreenCardCell: HomeScreenCardSubtitleCell {
    
    let spacing: CGFloat = 15
    
    // MARK: Properties
    
    public override var title: String? {
        get {
            return self.titleLabel.text
        }
        set {
            self.titleLabel.text = newValue
        }
    }
    
    public override var subtitle: String? {
        get {
            return self.subtitleLabel.text
        }
    }
    
    public override var actionTitle: String? {
        get {
            return self.actionLabel.text
        }
    }
    
    // MARK: Views
    
    lazy private var stackView: UIStackView = {
        let view = UIStackView(frame: .zero)
        view.axis = .horizontal
        view.spacing = spacing
        return view
    }()
    
    lazy private var mainImageView: UIImageView = {
        let view = UIImageView(frame: .zero)
        view.contentMode = .center
        return view
    }()
    
    lazy private var labelsStackView: UIStackView = {
        let view = UIStackView(frame: .zero)
        view.axis = .vertical
        return view
    }()
    
    lazy private var coinsStackView: UIStackView = {
        let view = UIStackView(frame: .zero)
        view.axis = .vertical
        return view
    }()
    
    lazy private var titleLabel: UILabel = {
        let label = UILabel(frame: .zero)
        label.numberOfLines = 2
        label.font = UIFont.systemFont(ofSize: 22, weight: UIFontWeightHeavy)
        label.textColor = UIColor.night()
        return label
    }()
    
    lazy private var pointsLabel: UILabel = {
        let label = UILabel(frame: .zero)
        label.numberOfLines = 1
        label.font = UIFont.systemFont(ofSize: 22, weight: UIFontWeightHeavy)
        label.textColor = UIColor.white
        label.textAlignment = NSTextAlignment.center
        return label
    }()
    
    lazy private var coinsLabel: UILabel = {
        let label = UILabel(frame: .zero)
        label.numberOfLines = 1
        label.textColor = UIColor.white
        label.font = UIFont.systemFont(ofSize: 17)
        label.snp.makeConstraints({ (make) in
            make.height.equalTo(20)
        })
        label.textAlignment = NSTextAlignment.center
        label.text = "Coins"
        return label
    }()
    
    lazy private var subtitleLabel: UILabel = {
        let label = UILabel(frame: .zero)
        label.numberOfLines = 2
        label.textColor = UIColor.mediumGrey()
        label.font = UIFont.systemFont(ofSize: 15)
        return label
    }()
    
    lazy private var actionLabel: UILabel = {
        let label = UILabel(frame: .zero)
        label.numberOfLines = 1
        label.textColor = UIColor.mediumGrey()
        label.font = UIFont.systemFont(ofSize: 15)
        label.snp.makeConstraints({ (make) in
            make.height.equalTo(20)
        })
        return label
    }()
    
    // MARK: Setup
    
    override func setupCardContentView(_ contentView: UIView) {
        
        contentView.addSubview(stackView)
        stackView.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.top.left.bottom.right.equalToSuperview().inset(HomeScreenCardSubtitleCell.spacing)
        }
        
        stackView.addArrangedSubview(mainImageView)
        mainImageView.snp.makeConstraints { (make) in
            make.height.equalToSuperview()
            make.width.equalTo(stackView.snp.height)
        }
        
        let labelsContainer = UIView(frame: .zero)
        stackView.addArrangedSubview(labelsContainer)
        labelsContainer.addSubview(labelsStackView)
        labelsStackView.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.left.right.equalToSuperview()
        }
        
        labelsStackView.addArrangedSubview(titleLabel)
        labelsStackView.addArrangedSubview(subtitleLabel)
        labelsStackView.addArrangedSubview(actionLabel)
        
        let coinsContainer = UIView(frame: .zero)
        stackView.addArrangedSubview(coinsContainer)
        coinsContainer.addSubview(coinsStackView)
        coinsStackView.snp.makeConstraints { (make) in
            make.centerY.equalTo(mainImageView)
            make.centerX.equalTo(mainImageView)
        }
        
        coinsStackView.addArrangedSubview(pointsLabel)
        coinsStackView.addArrangedSubview(coinsLabel)
        
        // hidden by default, setters controls visibility
        subtitleLabel.isHidden = true
        actionLabel.isHidden = true
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        self.title = nil
        self.setSubtitle(nil)
        self.mainImageView.image = nil
        self.mainImageView.tintColor = nil
        self.setActionTitle(nil, tintColor: nil)
    }
    
    // MARK: Setters
    
    //REVERT
//    public override func setMainImage(with image: VIACoreImageAsset) {
//        mainImageView.image = asset.image
//    }
    
    public func setImageLabel(_ title: String?) {
        pointsLabel.textColor = UIColor.white
        pointsLabel.text = title
        pointsLabel.isHidden = title == nil
        setNeedsLayout()
        layoutIfNeeded()
        setNeedsUpdateConstraints()
        updateConstraintsIfNeeded()
    }
    
    public override func setSubtitle(_ title: String?) {
        subtitleLabel.textColor = UIColor.mediumGrey()
        subtitleLabel.text = title
        subtitleLabel.isHidden = title == nil
        setNeedsLayout()
        layoutIfNeeded()
        setNeedsUpdateConstraints()
        updateConstraintsIfNeeded()
    }
    
    public override func setSubtitle(_ title: String?, tintColor: UIColor?) {
        subtitleLabel.text = title
        subtitleLabel.textColor = tintColor
        subtitleLabel.isHidden = title == nil
        setNeedsLayout()
        layoutIfNeeded()
        setNeedsUpdateConstraints()
        updateConstraintsIfNeeded()
    }
    
    public override func setActionTitle(_ title: String?, tintColor: UIColor?) {
        actionLabel.text = title
        actionLabel.textColor = tintColor
        actionLabel.isHidden = title == nil
        setNeedsLayout()
        layoutIfNeeded()
        setNeedsUpdateConstraints()
        updateConstraintsIfNeeded()
    }
    
}
