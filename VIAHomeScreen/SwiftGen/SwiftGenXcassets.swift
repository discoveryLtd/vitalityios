// Generated using SwiftGen, by O.Halligon — https://github.com/SwiftGen/SwiftGen

#if os(OSX)
import AppKit.NSImage
public typealias VIAHomeScreenColor = NSColor
public typealias VIAHomeScreenImage = NSImage
#elseif os(iOS) || os(tvOS) || os(watchOS)
import UIKit.UIImage
public typealias VIAHomeScreenColor = UIColor
public typealias VIAHomeScreenImage = UIImage
#endif

// swiftlint:disable file_length

public typealias VIAHomeScreenAssetType = VIAHomeScreenImageAsset

public struct VIAHomeScreenImageAsset: Equatable {
	fileprivate var name: String

	public var templateImage: VIAHomeScreenImage {
		let bundle = Bundle(for: BundleToken.self)
		#if os(iOS) || os(tvOS)
		let image = VIAHomeScreenImage(named: name, in: bundle, compatibleWith: nil)?.withRenderingMode(.alwaysTemplate)
		#elseif os(OSX)
		let image = bundle.image(forResource: name).withRenderingMode(.alwaysTemplate)
		#elseif os(watchOS)
		let image = VIAHomeScreenImage(named: name).withRenderingMode(.alwaysTemplate)
		#endif
		guard let result = image else { fatalError("Unable to load template image named \(name).") }
		return result
	}

	public var image: VIAHomeScreenImage {
		let bundle = Bundle(for: BundleToken.self)
		#if os(iOS) || os(tvOS)
		let image = VIAHomeScreenImage(named: name, in: bundle, compatibleWith: nil)
		#elseif os(OSX)
		let image = bundle.image(forResource: name)
		#elseif os(watchOS)
		let image = VIAHomeScreenImage(named: name)
		#endif
		guard let result = image else { fatalError("Unable to load image named \(name).") }
		return result
	}

	public static func ==(lhs: VIAHomeScreenImageAsset, rhs: VIAHomeScreenImageAsset) -> Bool {
        return lhs.image == rhs.image
    }
}

public struct VIAHomeScreenColorAsset {
fileprivate var name: String

#if swift(>=3.2)
@available(iOS 11.0, tvOS 11.0, watchOS 4.0, OSX 10.13, *)
var color: VIAHomeScreenColor {
return VIAHomeScreenColor(asset: self)
}
#endif
}

// swiftlint:disable identifier_name line_length nesting type_body_length type_name
public enum VIAHomeScreenAsset {
  public enum Goals {
    public static let statusInProgress = VIAHomeScreenImageAsset(name: "statusInProgress")
    public static let statusPending = VIAHomeScreenImageAsset(name: "statusPending")
    public static let statusNotAchieved = VIAHomeScreenImageAsset(name: "statusNotAchieved")
    public static let arrowDrill = VIAHomeScreenImageAsset(name: "arrowDrill")
    public static let statusAchieved = VIAHomeScreenImageAsset(name: "statusAchieved")
  }
  public enum HomeScreenCards {
    public static let hsCardPinkBlock = VIAHomeScreenImageAsset(name: "hsCardPinkBlock")
    public static let employerReward = VIAHomeScreenImageAsset(name: "employerReward")
    public static let hsVHCBig = VIAHomeScreenImageAsset(name: "hsVHCBig")
    public static let hsCardSAVBig = VIAHomeScreenImageAsset(name: "hsCardSAVBig")
    public static let hsCardExclamationBig = VIAHomeScreenImageAsset(name: "hsCardExclamationBig")
    public static let hsCardAwaitingShipmentBig = VIAHomeScreenImageAsset(name: "hsCardAwaitingShipmentBig")
    public static let hsNuffieldHealthLarge = VIAHomeScreenImageAsset(name: "hsNuffieldHealthLarge")
    public static let hsCardGreenBlock = VIAHomeScreenImageAsset(name: "hsCardGreenBlock")
    public static let hsCardLinkDevicesBig = VIAHomeScreenImageAsset(name: "hsCardLinkDevicesBig")
    public static let hsOFEBig = VIAHomeScreenImageAsset(name: "hsOFEBig")
    public static let hsCardCheckmarkBig = VIAHomeScreenImageAsset(name: "hsCardCheckmarkBig")
    public static let nuffieldMainLogo = VIAHomeScreenImageAsset(name: "nuffieldMainLogo")
    public static let hsCardLockedBig = VIAHomeScreenImageAsset(name: "hsCardLockedBig")
    public static let nuffieldNewMainLogo = VIAHomeScreenImageAsset(name: "nuffieldNewMainLogo")
    public static let hsPolicyCashbackBig = VIAHomeScreenImageAsset(name: "hsPolicyCashbackBig")
    public static let hsActivationBarcode = VIAHomeScreenImageAsset(name: "hsActivationBarcode")
    public static let hsCardOrangeBlock = VIAHomeScreenImageAsset(name: "hsCardOrangeBlock")
    public static let hsCardNonSmokersBig = VIAHomeScreenImageAsset(name: "hsCardNonSmokersBig")
    public static let hsCardHealthServicesBig = VIAHomeScreenImageAsset(name: "hsCardHealthServicesBig")
    public enum Headings {
      public static let policyCashback = VIAHomeScreenImageAsset(name: "policyCashback")
      public static let activeRewards = VIAHomeScreenImageAsset(name: "activeRewards")
      public static let rewardPartners = VIAHomeScreenImageAsset(name: "rewardPartners")
      public static let partnersRewards = VIAHomeScreenImageAsset(name: "partnersRewards")
      public static let mwb = VIAHomeScreenImageAsset(name: "mwb")
      public static let events = VIAHomeScreenImageAsset(name: "events")
      public static let nuffieldLogo = VIAHomeScreenImageAsset(name: "nuffieldLogo")
      public static let partnersWellness = VIAHomeScreenImageAsset(name: "partnersWellness")
      public static let employerServices = VIAHomeScreenImageAsset(name: "employerServices")
      public static let vhc = VIAHomeScreenImageAsset(name: "vhc")
      public static let nutrition = VIAHomeScreenImageAsset(name: "nutrition")
      public static let linkDevices = VIAHomeScreenImageAsset(name: "linkDevices")
      public static let vhr = VIAHomeScreenImageAsset(name: "vhr")
      public static let rewards = VIAHomeScreenImageAsset(name: "rewards")
      public static let nonSmokers = VIAHomeScreenImageAsset(name: "nonSmokers")
      public static let partnersHealth = VIAHomeScreenImageAsset(name: "partnersHealth")
      public static let screeningsVac = VIAHomeScreenImageAsset(name: "screeningsVac")
      public static let appleWatch = VIAHomeScreenImageAsset(name: "appleWatch")
      public static let activationBarcode = VIAHomeScreenImageAsset(name: "activationBarcode")
    }
    public static let hsPartnerRewardsWellnessBig = VIAHomeScreenImageAsset(name: "hsPartnerRewardsWellnessBig")
    public static let hsPartnerRewardsGreatBig = VIAHomeScreenImageAsset(name: "hsPartnerRewardsGreatBig")
    public static let hsCardWatchBig = VIAHomeScreenImageAsset(name: "hsCardWatchBig")
    public static let hsCardBlueBlock = VIAHomeScreenImageAsset(name: "hsCardBlueBlock")
    public static let hsCardCalendarBig = VIAHomeScreenImageAsset(name: "hsCardCalendarBig")
    public static let hsCardGoalAchievedBig = VIAHomeScreenImageAsset(name: "hsCardGoalAchievedBig")
    public static let yourEmployerServices = VIAHomeScreenImageAsset(name: "yourEmployerServices")
    public static let hsCardRewardAchievedMain = VIAHomeScreenImageAsset(name: "hsCardRewardAchievedMain")
    public static let hsCardPaymentCompleteBig = VIAHomeScreenImageAsset(name: "hsCardPaymentCompleteBig")
    public static let homeCardAWCPending = VIAHomeScreenImageAsset(name: "homeCardAWCPending")
    public static let hsPartnerRewardsHealthBig = VIAHomeScreenImageAsset(name: "hsPartnerRewardsHealthBig")
    public static let hsCardRewardsPaceholderBig = VIAHomeScreenImageAsset(name: "hsCardRewardsPaceholderBig")
  }
  public enum TabBarIcons {
    public static let tabBarHealthInactive = VIAHomeScreenImageAsset(name: "tabBarHealthInactive")
    public static let tabBarProfileActive = VIAHomeScreenImageAsset(name: "tabBarProfileActive")
    public static let tabBarPointsInactive = VIAHomeScreenImageAsset(name: "tabBarPointsInactive")
    public static let tabBarHomeInactive = VIAHomeScreenImageAsset(name: "tabBarHomeInactive")
    public static let mybenefits = VIAHomeScreenImageAsset(name: "mybenefits")
    public static let tabBarHelpActive = VIAHomeScreenImageAsset(name: "tabBarHelpActive")
    public static let tabBarProfileInactive = VIAHomeScreenImageAsset(name: "tabBarProfileInactive")
    public static let tabBarPointsActive = VIAHomeScreenImageAsset(name: "tabBarPointsActive")
    public static let tabBarHomeActive = VIAHomeScreenImageAsset(name: "tabBarHomeActive")
    public static let mybenefitsInactive = VIAHomeScreenImageAsset(name: "mybenefitsInactive")
    public static let tabBarHealthActive = VIAHomeScreenImageAsset(name: "tabBarHealthActive")
    public static let tabBarHelpInactive = VIAHomeScreenImageAsset(name: "tabBarHelpInactive")
  }
  public enum ActiveRewards {
    public static let activeRewardsTerms = VIAHomeScreenImageAsset(name: "ActiveRewardsTerms")
    public enum Rewards {
      public static let noActiveReward = VIAHomeScreenImageAsset(name: "noActiveReward")
      public static let rewardTrophyBig = VIAHomeScreenImageAsset(name: "rewardTrophyBig")
      public static let activeReward = VIAHomeScreenImageAsset(name: "activeReward")
      public static let partnerStarbucks = VIAHomeScreenImageAsset(name: "partnerStarbucks")
    }
    public static let activeRewardsHelp = VIAHomeScreenImageAsset(name: "ActiveRewardsHelp")
    public static let activeRewardsRewards2 = VIAHomeScreenImageAsset(name: "ActiveRewardsRewards2")
    public static let activeRewardsLearnMore = VIAHomeScreenImageAsset(name: "ActiveRewardsLearnMore")
    public enum LearnMore {
      public static let arLearnMoreReward = VIAHomeScreenImageAsset(name: "ARLearnMoreReward")
      public static let arLearnMoreActivate = VIAHomeScreenImageAsset(name: "ARLearnMoreActivate")
      public static let arLearnMoreTrophy = VIAHomeScreenImageAsset(name: "ARLearnMoreTrophy")
    }
    public static let activeRewardsActivity = VIAHomeScreenImageAsset(name: "ActiveRewardsActivity")
    public enum HomeScreen {
      public static let activeRewardsHomeScreenCard = VIAHomeScreenImageAsset(name: "ActiveRewardsHomeScreenCard")
      public static let activeRewardsTrophy = VIAHomeScreenImageAsset(name: "ActiveRewardsTrophy")
    }
    public static let arRangeCalendar = VIAHomeScreenImageAsset(name: "ARRangeCalendar")
    public static let arvhrRequired = VIAHomeScreenImageAsset(name: "ARVHRRequired")
    public static let activeRewardsRewards = VIAHomeScreenImageAsset(name: "ActiveRewardsRewards")
    public static let arLandingCalendarBig = VIAHomeScreenImageAsset(name: "ARLandingCalendarBig")
    public enum Onboarding {
      public static let arOnboardingSpinner = VIAHomeScreenImageAsset(name: "AROnboardingSpinner")
      public static let arOnboardingCalendar = VIAHomeScreenImageAsset(name: "AROnboardingCalendar")
      public static let arOnboardingRings = VIAHomeScreenImageAsset(name: "AROnboardingRings")
      public static let arOnboardingReward = VIAHomeScreenImageAsset(name: "AROnboardingReward")
      public static let arOnboardingTrophy = VIAHomeScreenImageAsset(name: "AROnboardingTrophy")
      public static let arOnboardingActivated = VIAHomeScreenImageAsset(name: "AROnboardingActivated")
    }
  }
  public enum DeviceCashback {
    public static let homeCardOrangeCoin = VIAHomeScreenImageAsset(name: "homeCardOrangeCoin")
    public static let homeCardActivated = VIAHomeScreenImageAsset(name: "homeCardActivated")
    public static let homeCardAwaitingShipment = VIAHomeScreenImageAsset(name: "homeCardAwaitingShipment")
    public static let homeCardGetStarted = VIAHomeScreenImageAsset(name: "homeCardGetStarted")
    public static let homeCardBlueCoin = VIAHomeScreenImageAsset(name: "homeCardBlueCoin")
    public static let homeCardGreenCoin = VIAHomeScreenImageAsset(name: "homeCardGreenCoin")
    public static let homeCardPinkCoin = VIAHomeScreenImageAsset(name: "homeCardPinkCoin")
  }
  public enum NavBar {
    public static let greyPixel = VIAHomeScreenImageAsset(name: "GreyPixel")
    public static let transparentPixel = VIAHomeScreenImageAsset(name: "TransparentPixel")
    public static let pixel = VIAHomeScreenImageAsset(name: "Pixel")
  }

  // swiftlint:disable trailing_comma
  public static let allColors: [VIAHomeScreenColorAsset] = [
  ]
  public static let allImages: [VIAHomeScreenImageAsset] = [
    Goals.statusInProgress,
    Goals.statusPending,
    Goals.statusNotAchieved,
    Goals.arrowDrill,
    Goals.statusAchieved,
    HomeScreenCards.hsCardPinkBlock,
    HomeScreenCards.employerReward,
    HomeScreenCards.hsVHCBig,
    HomeScreenCards.hsCardSAVBig,
    HomeScreenCards.hsCardExclamationBig,
    HomeScreenCards.hsCardAwaitingShipmentBig,
    HomeScreenCards.hsNuffieldHealthLarge,
    HomeScreenCards.hsCardGreenBlock,
    HomeScreenCards.hsCardLinkDevicesBig,
    HomeScreenCards.hsOFEBig,
    HomeScreenCards.hsCardCheckmarkBig,
    HomeScreenCards.nuffieldMainLogo,
    HomeScreenCards.hsCardLockedBig,
    HomeScreenCards.nuffieldNewMainLogo,
    HomeScreenCards.hsPolicyCashbackBig,
    HomeScreenCards.hsActivationBarcode,
    HomeScreenCards.hsCardOrangeBlock,
    HomeScreenCards.hsCardNonSmokersBig,
    HomeScreenCards.hsCardHealthServicesBig,
    HomeScreenCards.Headings.policyCashback,
    HomeScreenCards.Headings.activeRewards,
    HomeScreenCards.Headings.rewardPartners,
    HomeScreenCards.Headings.partnersRewards,
    HomeScreenCards.Headings.mwb,
    HomeScreenCards.Headings.events,
    HomeScreenCards.Headings.nuffieldLogo,
    HomeScreenCards.Headings.partnersWellness,
    HomeScreenCards.Headings.employerServices,
    HomeScreenCards.Headings.vhc,
    HomeScreenCards.Headings.nutrition,
    HomeScreenCards.Headings.linkDevices,
    HomeScreenCards.Headings.vhr,
    HomeScreenCards.Headings.rewards,
    HomeScreenCards.Headings.nonSmokers,
    HomeScreenCards.Headings.partnersHealth,
    HomeScreenCards.Headings.screeningsVac,
    HomeScreenCards.Headings.appleWatch,
    HomeScreenCards.Headings.activationBarcode,
    HomeScreenCards.hsPartnerRewardsWellnessBig,
    HomeScreenCards.hsPartnerRewardsGreatBig,
    HomeScreenCards.hsCardWatchBig,
    HomeScreenCards.hsCardBlueBlock,
    HomeScreenCards.hsCardCalendarBig,
    HomeScreenCards.hsCardGoalAchievedBig,
    HomeScreenCards.yourEmployerServices,
    HomeScreenCards.hsCardRewardAchievedMain,
    HomeScreenCards.hsCardPaymentCompleteBig,
    HomeScreenCards.homeCardAWCPending,
    HomeScreenCards.hsPartnerRewardsHealthBig,
    HomeScreenCards.hsCardRewardsPaceholderBig,
    TabBarIcons.tabBarHealthInactive,
    TabBarIcons.tabBarProfileActive,
    TabBarIcons.tabBarPointsInactive,
    TabBarIcons.tabBarHomeInactive,
    TabBarIcons.mybenefits,
    TabBarIcons.tabBarHelpActive,
    TabBarIcons.tabBarProfileInactive,
    TabBarIcons.tabBarPointsActive,
    TabBarIcons.tabBarHomeActive,
    TabBarIcons.mybenefitsInactive,
    TabBarIcons.tabBarHealthActive,
    TabBarIcons.tabBarHelpInactive,
    ActiveRewards.activeRewardsTerms,
    ActiveRewards.Rewards.noActiveReward,
    ActiveRewards.Rewards.rewardTrophyBig,
    ActiveRewards.Rewards.activeReward,
    ActiveRewards.Rewards.partnerStarbucks,
    ActiveRewards.activeRewardsHelp,
    ActiveRewards.activeRewardsRewards2,
    ActiveRewards.activeRewardsLearnMore,
    ActiveRewards.LearnMore.arLearnMoreReward,
    ActiveRewards.LearnMore.arLearnMoreActivate,
    ActiveRewards.LearnMore.arLearnMoreTrophy,
    ActiveRewards.activeRewardsActivity,
    ActiveRewards.HomeScreen.activeRewardsHomeScreenCard,
    ActiveRewards.HomeScreen.activeRewardsTrophy,
    ActiveRewards.arRangeCalendar,
    ActiveRewards.arvhrRequired,
    ActiveRewards.activeRewardsRewards,
    ActiveRewards.arLandingCalendarBig,
    ActiveRewards.Onboarding.arOnboardingSpinner,
    ActiveRewards.Onboarding.arOnboardingCalendar,
    ActiveRewards.Onboarding.arOnboardingRings,
    ActiveRewards.Onboarding.arOnboardingReward,
    ActiveRewards.Onboarding.arOnboardingTrophy,
    ActiveRewards.Onboarding.arOnboardingActivated,
    DeviceCashback.homeCardOrangeCoin,
    DeviceCashback.homeCardActivated,
    DeviceCashback.homeCardAwaitingShipment,
    DeviceCashback.homeCardGetStarted,
    DeviceCashback.homeCardBlueCoin,
    DeviceCashback.homeCardGreenCoin,
    DeviceCashback.homeCardPinkCoin,
    NavBar.greyPixel,
    NavBar.transparentPixel,
    NavBar.pixel,
  ]
  // swiftlint:enable trailing_comma
  @available(*, deprecated, renamed: "allImages")
  public static let allValues: [VIAHomeScreenAssetType] = allImages
}
// swiftlint:enable identifier_name line_length nesting type_body_length type_name

extension VIAHomeScreenImage {
@available(iOS 1.0, tvOS 1.0, watchOS 1.0, *)
@available(OSX, deprecated,
message: "This initializer is unsafe on macOS, please use the VIAHomeScreenImageAsset.image property")
convenience init!(asset: VIAHomeScreenAssetType) {
#if os(iOS) || os(tvOS)
let bundle = Bundle(for: BundleToken.self)
self.init(named: asset.name, in: bundle, compatibleWith: nil)
#elseif os(OSX) || os(watchOS)
self.init(named: asset.name)
#endif
}
}

extension VIAHomeScreenColor {
#if swift(>=3.2)
@available(iOS 11.0, tvOS 11.0, watchOS 4.0, OSX 10.13, *)
convenience init!(asset: VIAHomeScreenColorAsset) {
let bundle = Bundle(for: BundleToken.self)
#if os(iOS) || os(tvOS)
self.init(named: asset.name, in: bundle, compatibleWith: nil)
#elseif os(OSX)
self.init(named: asset.name, bundle: bundle)
#elseif os(watchOS)
self.init(named: asset.name)
#endif
}
#endif
}

private final class BundleToken {}
