import XCTest
import RealmSwift

@testable import VitalityKit
@testable import VIACore

class EffectiveDateFilterablePointsMonitorTests: XCTestCase {
    var allPointsHistoryResponse: GetAllPointsHistoryResponse?
    
    func response(from file: String) -> GetAllPointsHistoryResponse? {
        var response: GetAllPointsHistoryResponse? = nil
        guard let pathString = Bundle(for: type(of: self)).path(forResource: file, ofType: "json") else {
            fatalError("\(file) not found")
        }
        
        guard let jsonString = try? NSString(contentsOfFile: pathString, encoding: String.Encoding.utf8.rawValue) else {
            fatalError("Unable to convert \(file) to String")
        }
        
        print("The JSON string is: \(jsonString)")
        
        guard let jsonData = jsonString.data(using: String.Encoding.utf8.rawValue) else {
            fatalError("Unable to convert \(file) to NSData")
        }
        
        do {
            guard let jsonDictionary = try JSONSerialization.jsonObject(with: jsonData, options: []) as? [String:Any] else {
                fatalError("Unable to convert \(file) to JSON dictionary")
            }
            response = GetAllPointsHistoryResponse.init(json: jsonDictionary)!
        } catch _ {
            
        }
        
        return response
    }
    
    override func setUp() {
        super.setUp()
        let realm = DataProvider.newRealm()
        try! realm.write {
            realm.deleteAll()
        }
    }
    
    override func tearDown() {
        let realm = DataProvider.newRealm()
        try! realm.write {
            realm.deleteAll()
        }
        super.tearDown()
    }
    
//    func testCallingFilterForElementsBetweenStartAndEndReturnsExpectedPointsEntriesForOctober() {
//        let file = "get_all_points_history_response_october_and_august_response"
//        guard let pointsHistoryResponse = response(from: file) else {
//            fatalError("Unable to generate response from file provided")
//        }
//        guard let startDate = Localization.yearMonthDayFormatter.date(from: "2017-10-01") else {
//            fatalError("Formatter unable to create date from string provided")
//        }
//        guard let endDate = Localization.yearMonthDayFormatter.date(from: "2017-10-31") else {
//            fatalError("Formatter unable to create date from string provided")
//        }
//        
//        _ = PointsAccount.persistPointsHistoryEvents(response: pointsHistoryResponse)
//    
//        let viewController = PointsMonitorMonthTableViewController()
//        // createSectionsAndData() calls testfilterForElementsBetweenStartAndEnd()
//        viewController.createSectionsAndData(for: startDate, categories: [])
//        
//        XCTAssertEqual(viewController.pointsEntriesBySection.count, 6, "Points entries by section count does not match expected value")
//        
//        XCTAssertEqual(viewController.pointsEntriesBySection[0].count, 1, "Number of points entries in first section does not match expected value")
//        XCTAssertEqual(viewController.pointsEntriesBySection[1].count, 1, "Number of points entries in second section does not match expected value")
//        XCTAssertEqual(viewController.pointsEntriesBySection[2].count, 2, "Number of points entries in third section does not match expected value")
//        XCTAssertEqual(viewController.pointsEntriesBySection[3].count, 1, "Number of points entries in fouth section does not match expected value")
//        XCTAssertEqual(viewController.pointsEntriesBySection[4].count, 1, "Number of points entries in fifth section does not match expected value")
//        XCTAssertEqual(viewController.pointsEntriesBySection[5].count, 1, "Number of points entries in  sixth section does not match expected value")
//        
//        XCTAssertGreaterThanOrEqual(viewController.pointsEntriesBySection[0][0].effectiveDate, startDate, "Effective Date is less than start date")
//        XCTAssertGreaterThanOrEqual(viewController.pointsEntriesBySection[1][0].effectiveDate, startDate, "Effective Date is less than start date")
//        XCTAssertGreaterThanOrEqual(viewController.pointsEntriesBySection[2][0].effectiveDate, startDate, "Effective Date is less than start date")
//        XCTAssertGreaterThanOrEqual(viewController.pointsEntriesBySection[2][1].effectiveDate, startDate, "Effective Date is less than start date")
//        XCTAssertGreaterThanOrEqual(viewController.pointsEntriesBySection[3][0].effectiveDate, startDate, "Effective Date is less than start date")
//        XCTAssertGreaterThanOrEqual(viewController.pointsEntriesBySection[4][0].effectiveDate, startDate, "Effective Date is less than start date")
//        XCTAssertGreaterThanOrEqual(viewController.pointsEntriesBySection[5][0].effectiveDate, startDate, "Effective Date is less than start date")
//        
//        XCTAssertLessThanOrEqual(viewController.pointsEntriesBySection[0][0].effectiveDate, endDate, "Effective Date is greater than end date")
//        XCTAssertLessThanOrEqual(viewController.pointsEntriesBySection[1][0].effectiveDate, endDate, "Effective Date is greater than end date")
//        XCTAssertLessThanOrEqual(viewController.pointsEntriesBySection[2][0].effectiveDate, endDate, "Effective Date is greater than end date")
//        XCTAssertLessThanOrEqual(viewController.pointsEntriesBySection[2][1].effectiveDate, endDate, "Effective Date is greater than end date")
//        XCTAssertLessThanOrEqual(viewController.pointsEntriesBySection[3][0].effectiveDate, endDate, "Effective Date is greater than end date")
//        XCTAssertLessThanOrEqual(viewController.pointsEntriesBySection[4][0].effectiveDate, endDate, "Effective Date is greater than end date")
//        XCTAssertLessThanOrEqual(viewController.pointsEntriesBySection[5][0].effectiveDate, endDate, "Effective Date is greater than end date")
//    }
//    
//    func testCallingFilterForElementsBetweenStartAndEndReturnsExpectedPointsEntriesForAugust() {
//        let file = "get_all_points_history_response_october_and_august_response"
//        guard let pointsHistoryResponse = response(from: file) else {
//            fatalError("Unable to generate response from file provided")
//        }
//        guard let startDate = Localization.yearMonthDayFormatter.date(from: "2017-08-01") else {
//            fatalError("Formatter unable to create date from string provided")
//        }
//        guard let endDate = Localization.yearMonthDayFormatter.date(from: "2017-08-31") else {
//            fatalError("Formatter unable to create date from string provided")
//        }
//        
//        _ = PointsAccount.persistPointsHistoryEvents(response: pointsHistoryResponse)
//        
//        let viewController = PointsMonitorMonthTableViewController()
//        // createSectionsAndData() calls testfilterForElementsBetweenStartAndEnd()
//        viewController.createSectionsAndData(for: startDate, categories: [])
//        
//        XCTAssertEqual(viewController.pointsEntriesBySection.count, 4, "Points entries by section count does not match expected value")
//        
//        XCTAssertEqual(viewController.pointsEntriesBySection[0].count, 1, "Number of points entries in first section does not match expected value")
//        XCTAssertEqual(viewController.pointsEntriesBySection[1].count, 2, "Number of points entries in second section does not match expected value")
//        XCTAssertEqual(viewController.pointsEntriesBySection[2].count, 1, "Number of points entries in third section does not match expected value")
//        XCTAssertEqual(viewController.pointsEntriesBySection[3].count, 1, "Number of points entries in fouth section does not match expected value")
//        
//        XCTAssertGreaterThanOrEqual(viewController.pointsEntriesBySection[0][0].effectiveDate, startDate, "Effective Date is less than start date")
//        XCTAssertGreaterThanOrEqual(viewController.pointsEntriesBySection[1][0].effectiveDate, startDate, "Effective Date is less than start date")
//        XCTAssertGreaterThanOrEqual(viewController.pointsEntriesBySection[1][1].effectiveDate, startDate, "Effective Date is less than start date")
//        XCTAssertGreaterThanOrEqual(viewController.pointsEntriesBySection[2][0].effectiveDate, startDate, "Effective Date is less than start date")
//        XCTAssertGreaterThanOrEqual(viewController.pointsEntriesBySection[3][0].effectiveDate, startDate, "Effective Date is less than start date")
//        
//        XCTAssertLessThanOrEqual(viewController.pointsEntriesBySection[0][0].effectiveDate, endDate, "Effective Date is greater than end date")
//        XCTAssertLessThanOrEqual(viewController.pointsEntriesBySection[1][0].effectiveDate, endDate, "Effective Date is greater than end date")
//        XCTAssertLessThanOrEqual(viewController.pointsEntriesBySection[1][1].effectiveDate, endDate, "Effective Date is greater than end date")
//        XCTAssertLessThanOrEqual(viewController.pointsEntriesBySection[2][0].effectiveDate, endDate, "Effective Date is greater than end date")
//        XCTAssertLessThanOrEqual(viewController.pointsEntriesBySection[3][0].effectiveDate, endDate, "Effective Date is greater than end date")
//    }
    
}
