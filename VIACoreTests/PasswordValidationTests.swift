//
//  PasswordValidationTests.swift
//  VitalityActive
//
//  Created by Simon Stewart on 2/23/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import XCTest
@testable import VIACore

class PasswordValidationTests: XCTestCase {
    func testWithLowerCase6() {
        let loginVM = VIALoginViewModel()
        var registrationVM = VIARegistrationDetail()
        loginVM.password = "abcdef"
        registrationVM.password = loginVM.password
        XCTAssertFalse(loginVM.passwordValid && registrationVM.passwordValid)
    }
    func testWithLowerCase7() {
        let loginVM = VIALoginViewModel()
        var registrationVM = VIARegistrationDetail()
        loginVM.password = "abcdefg"
        registrationVM.password = loginVM.password

        XCTAssertFalse(loginVM.passwordValid && registrationVM.passwordValid)
    }
    func testWithUpperCase6() {
        let loginVM = VIALoginViewModel()
        var registrationVM = VIARegistrationDetail()
        loginVM.password = "WOOHOO"
        registrationVM.password = loginVM.password
        XCTAssertFalse(loginVM.passwordValid && registrationVM.passwordValid)
    }
    func testWithUpperCase7() {
        let loginVM = VIALoginViewModel()
        var registrationVM = VIARegistrationDetail()
        loginVM.password = "WOOOOO"
        registrationVM.password = loginVM.password
        XCTAssertFalse(loginVM.passwordValid && registrationVM.passwordValid)
    }
    func testWithUpperLowerCase6() {
        let loginVM = VIALoginViewModel()
        var registrationVM = VIARegistrationDetail()
        loginVM.password = "WOooO"
        registrationVM.password = loginVM.password
        XCTAssertFalse(loginVM.passwordValid && registrationVM.passwordValid)
    }
    func testWithUpperLowerCase7() {
        let loginVM = VIALoginViewModel()
        var registrationVM = VIARegistrationDetail()
        loginVM.password = "WOooOo"
        registrationVM.password = loginVM.password
        XCTAssertFalse(loginVM.passwordValid && registrationVM.passwordValid)
    }
    func testWithNumbers6() {
        let loginVM = VIALoginViewModel()
        var registrationVM = VIARegistrationDetail()
        loginVM.password = "123456"
        registrationVM.password = loginVM.password
        XCTAssertFalse(loginVM.passwordValid && registrationVM.passwordValid)
    }
    func testWithNumbers7() {
        let loginVM = VIALoginViewModel()
        var registrationVM = VIARegistrationDetail()
        loginVM.password = "1234567"
        registrationVM.password = loginVM.password
        XCTAssertFalse(loginVM.passwordValid && registrationVM.passwordValid)
    }
    func testWithGoodCombo6() {
        let loginVM = VIALoginViewModel()
        var registrationVM = VIARegistrationDetail()
        loginVM.password = "Wo7Wo7"
        registrationVM.password = loginVM.password
        XCTAssertFalse(loginVM.passwordValid && registrationVM.passwordValid)
    }
    func testWithGoodCombo7() {
        let loginVM = VIALoginViewModel()
        var registrationVM = VIARegistrationDetail()
        loginVM.password = "Wo7Wo72a"
        registrationVM.password = loginVM.password
        XCTAssertTrue(loginVM.passwordValid && registrationVM.passwordValid)
    }
}
