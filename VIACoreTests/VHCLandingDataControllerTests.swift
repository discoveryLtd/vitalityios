//
//  VHCLandingDataControllerTests.swift
//  VitalityActive
//
//  Created by Lionel Camacho on 2017/04/04.
//  Copyright © 2017 Glucode. All rights reserved.
//

import XCTest
import VitalityKit

import RealmSwift
import VitalityKit
@testable import VIACore

class VHCLandingDataControllerTests: XCTestCase {
    var getPotentialPointsResponse: GetVHCPotentialPointsResponse?
    var dataController: VHCLandingDataController?
    override func setUp() {
        super.setUp()
        let realm = DataProvider.newVHCRealm()
        try! realm.write {
            realm.deleteAll()
        }
        dataController = VHCLandingDataController()
        
        let file = "PotentialPointsResponse"

        guard let pathString = Bundle(for: type(of: self)).path(forResource: file, ofType: "json") else {
            fatalError("PotentialPointsResponse.json not found")
        }

        guard let jsonString = try? NSString(contentsOfFile: pathString, encoding: String.Encoding.utf8.rawValue) else {
            fatalError("Unable to convert PotentialPointsResponse.json to String")
        }

        print("The JSON string is: \(jsonString)")

        guard let jsonData = jsonString.data(using: String.Encoding.utf8.rawValue) else {
            fatalError("Unable to converPotentialPointsResponse.json to NSData")
        }

        do {
            guard let jsonDictionary = try JSONSerialization.jsonObject(with: jsonData, options: []) as? [String:Any] else {
                fatalError("Unable to convert PotentialPointsResponse.json to JSON dictionary")
            }
            self.getPotentialPointsResponse = GetVHCPotentialPointsResponse.init(json: jsonDictionary)!
        } catch _ {

        }
        VHCEvent.persistGetPotentialPoints(response: self.getPotentialPointsResponse!)
    }

    override func tearDown() {
        dataController = nil
        let realm = DataProvider.newVHCRealm()
        try! realm.write {
            realm.deleteAll()
        }
        super.tearDown()

    }

    func testGetDataForGroupWhereNonqualifyingMeasurementsHaveBeenCaptured() {
        if let groupData = dataController?.dataForGroupsFromRealm() {
            XCTAssertNotNil(groupData)
            XCTAssertEqual(7, groupData.count)
            for group in groupData {
                switch group.groupName {
                //Case where only non-qualifying measurements have been captured
                case "measurement.body_mass_index_title_134":
                    XCTAssertEqual(false, group.isDisclosureIndicatorHidden)
                    XCTAssertNotNil(group.groupIcon)
                    XCTAssertEqual(false, group.isOutOfMembershipPeriod)
                    XCTAssertEqual("", group.measurementUnits)
                    XCTAssertEqual("24,69", group.measurementValue)
                    XCTAssertEqual(nil, group.message)
                    XCTAssertEqual(nil, group.footerMessage)
                    XCTAssertEqual(false, group.pointsAndHealthyRangeFeedback?.isEmpty)
                    XCTAssertEqual(2, group.pointsAndHealthyRangeFeedback?.count)
                //Case where no measurements have been captured
                case "measurement.cholesterol_title_138":
                    XCTAssertEqual(false, group.isDisclosureIndicatorHidden)
                    XCTAssertNotNil(group.groupIcon)
                    XCTAssertEqual(false, group.isOutOfMembershipPeriod)
                    XCTAssertEqual("mmol/L", group.measurementUnits)
                    XCTAssertEqual("3", group.measurementValue)
                    XCTAssertEqual(nil, group.message)
                    XCTAssertEqual(nil, group.footerMessage)
                    XCTAssertEqual(false, group.pointsAndHealthyRangeFeedback?.isEmpty)
                //Case where measurements were captured outside of the current measurement period
                case "measurement.hba1c_title_139":
                    XCTAssertEqual(false, group.isDisclosureIndicatorHidden)
                    XCTAssertNotNil(group.groupIcon)
                    XCTAssertEqual(false, group.isOutOfMembershipPeriod)
                    XCTAssertEqual("%", group.measurementUnits)
                    XCTAssertEqual("4", group.measurementValue)
                    XCTAssertEqual(nil, group.message)
                    XCTAssertNil(group.footerMessage)
                    XCTAssertEqual(false, group.pointsAndHealthyRangeFeedback?.isEmpty)
                    XCTAssertEqual(2, group.pointsAndHealthyRangeFeedback?.count)
                //Case where qualifying measurements were captured
                case "measurement.glucose_title_136":
                    XCTAssertEqual(false, group.isDisclosureIndicatorHidden)
                    XCTAssertNotNil(group.groupIcon)
                    XCTAssertEqual(false, group.isOutOfMembershipPeriod)
                    XCTAssertEqual("mg/dL", group.measurementUnits)
                    XCTAssertEqual("40", group.measurementValue)
                    XCTAssertEqual(nil, group.message)
                    XCTAssertNil(group.footerMessage)
                    XCTAssertEqual(false, group.pointsAndHealthyRangeFeedback?.isEmpty)
                    XCTAssertEqual(2, group.pointsAndHealthyRangeFeedback?.count)
                case "measurement.waist_circumference_title_135":
                    XCTAssertEqual(false, group.isDisclosureIndicatorHidden)
                    XCTAssertNotNil(group.groupIcon)
                    XCTAssertEqual(true, group.isOutOfMembershipPeriod)
                    XCTAssertEqual(nil, group.measurementUnits)
                    XCTAssertEqual(nil, group.measurementValue)
                    XCTAssertEqual(nil, group.message)
                    XCTAssertNotNil(group.footerMessage)
                    XCTAssertEqual(nil, group.pointsAndHealthyRangeFeedback?.isEmpty)
                    XCTAssertEqual(nil, group.pointsAndHealthyRangeFeedback?.count)
                case "measurement.blood_pressure_title_137":
                    XCTAssertEqual(true, group.isDisclosureIndicatorHidden)
                    XCTAssertNotNil(group.groupIcon)
                    XCTAssertEqual(true, group.isOutOfMembershipPeriod)
                    XCTAssertEqual(nil, group.measurementUnits)
                    XCTAssertEqual(nil, group.measurementValue)
                    XCTAssertEqual(CommonStrings.HomeCard.CardEarnUpToPointsMessage124(""), group.message)
                    XCTAssertNil(group.footerMessage)
                    XCTAssertEqual(nil, group.pointsAndHealthyRangeFeedback?.isEmpty)
                    XCTAssertEqual(nil, group.pointsAndHealthyRangeFeedback?.count)
                case "measurement.urine_protein_title_283":
                    XCTAssertEqual(true, group.isDisclosureIndicatorHidden)
                    XCTAssertNotNil(group.groupIcon)
                    XCTAssertEqual(true, group.isOutOfMembershipPeriod)
                    XCTAssertEqual(nil, group.measurementUnits)
                    XCTAssertEqual(nil, group.measurementValue)
                    XCTAssertEqual(nil, group.message)
                    XCTAssertNil(group.footerMessage)
                    XCTAssertEqual(nil, group.pointsAndHealthyRangeFeedback?.isEmpty)
                    XCTAssertEqual(nil, group.pointsAndHealthyRangeFeedback?.count)
                default:
                    XCTAssertNil(group.groupName)
                }
            }
        } else {
            XCTAssertTrue(false)
        }
    }

}
