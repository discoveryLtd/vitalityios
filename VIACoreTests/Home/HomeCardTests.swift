import Foundation
import XCTest
@testable import VitalityKit
@testable import VIAUtilities
import SwiftDate
import RealmSwift

class HomeCardTests: XCTestCase {
    
    let realm: Realm = DataProvider.newRealm()
    
    override func setUp() {
        super.setUp()
        
        realm.reset()
    }
    
    override func tearDown() {
        super.tearDown()
        
        try! realm.write {
            realm.deleteAll()
        }
    }
    
    func cardController(`for` card: HomeCard) -> HomeScreenCardController {
        let cardValue: HomeCardValue = HomeCardValue(homeCard: card)
        let controller = HomeScreenCardController()
        controller.didUpdate(to: cardValue)
        return controller
    }
    
    func setProbabilisticActiveRewards() {
        let feature = VitalityProductFeature()
        feature.type = .ARProbabilistic
        try! realm.write {
            realm.add(feature)
        }

    }
    
    // MARK:
    
    func test1RewardEarnedCardNotExpiringSoon() {
        setProbabilisticActiveRewards()
        
        // setup
        let homeCard = HomeCard()
        homeCard.type = .Rewards
        
        let item1 = HomeCardItem()
        item1.type = .WheelSpin
        item1.statusType = .Available
        item1.validTo = Date() + 12.days
        homeCard.cardItems.append(item1)
        
        //
        let controller = cardController(for: homeCard)
        let homeCardValue = controller.card!
        
        // assert
        XCTAssertTrue(homeCardValue.heading() == ActiveRewardsStrings.Ar.HomescreenCard.RewardsTitle785)
        XCTAssertTrue(homeCardValue.title() == ActiveRewardsStrings.Ar.RewardsEarnedTitle782(""))
        XCTAssertTrue(homeCardValue.subtitle() == nil)
        XCTAssertTrue(homeCardValue.actionTitle() == ActiveRewardsStrings.Ar.Rewards.SpinNowTitle704)
    }
    
    func test1RewardEarnedCard1ExpiringSoon() {
        setProbabilisticActiveRewards()
        
        // setup
        let homeCard = HomeCard()
        homeCard.type = .Rewards
        
        let item1 = HomeCardItem()
        item1.type = .WheelSpin
        item1.statusType = .Available
        item1.validTo = Date() + 1.days
        homeCard.cardItems.append(item1)
        
        //
        let controller = cardController(for: homeCard)
        let homeCardValue = controller.card!
        
        // assert
        XCTAssertTrue(homeCardValue.heading() == ActiveRewardsStrings.Ar.HomescreenCard.RewardsTitle785)
        XCTAssertTrue(homeCardValue.title() == ActiveRewardsStrings.Ar.RewardsEarnedTitle782(""))
        XCTAssertTrue(homeCardValue.subtitle() == ActiveRewardsStrings.Ar.RewardsExpiringSoonTitle783(""))
        XCTAssertTrue(homeCardValue.actionTitle() == ActiveRewardsStrings.Ar.Rewards.SpinNowTitle704)
    }
    
    func test2RewardsEarnedCardNotExpiringSoon() {
        setProbabilisticActiveRewards()
        
        // setup
        let homeCard = HomeCard()
        homeCard.type = .Rewards
        
        let item1 = HomeCardItem()
        item1.type = .WheelSpin
        item1.statusType = .Available
        item1.validTo = Date() + 12.days
        homeCard.cardItems.append(item1)
        
        let item2 = HomeCardItem()
        item2.type = .WheelSpin
        item2.statusType = .Available
        item2.validTo = Date() + 8.days
        homeCard.cardItems.append(item2)
        
        //
        let controller = cardController(for: homeCard)
        let homeCardValue = controller.card!
        
        // assert
        XCTAssertTrue(homeCardValue.heading() == ActiveRewardsStrings.Ar.HomescreenCard.RewardsTitle785)
        XCTAssertTrue(homeCardValue.title() == ActiveRewardsStrings.Ar.RewardsEarnedTitle782(""))
        XCTAssertTrue(homeCardValue.subtitle() == nil)
        XCTAssertTrue(homeCardValue.actionTitle() == ActiveRewardsStrings.Ar.HomescreenCard.AvailableSpinsButtonTitle787)
    }
    
    func test2RewardsEarnedCard2ExpiringSoon() {
        setProbabilisticActiveRewards()
        
        // setup
        let homeCard = HomeCard()
        homeCard.type = .Rewards
        
        let item1 = HomeCardItem()
        item1.type = .WheelSpin
        item1.statusType = .Available
        item1.validTo = Date() + 2.days
        homeCard.cardItems.append(item1)
        
        let item2 = HomeCardItem()
        item2.type = .WheelSpin
        item2.statusType = .Available
        item2.validTo = Date() + 1.days
        homeCard.cardItems.append(item2)
        
        //
        let controller = cardController(for: homeCard)
        let homeCardValue = controller.card!
        
        // assert
        XCTAssertTrue(homeCardValue.heading() == ActiveRewardsStrings.Ar.HomescreenCard.RewardsTitle785)
        XCTAssertTrue(homeCardValue.title() == ActiveRewardsStrings.Ar.RewardsEarnedTitle782(""))
        XCTAssertTrue(homeCardValue.subtitle() == ActiveRewardsStrings.Ar.RewardsExpiringSoonTitle783(""))
        XCTAssertTrue(homeCardValue.actionTitle() == ActiveRewardsStrings.Ar.HomescreenCard.AvailableSpinsButtonTitle787)
    }
    
    // MARK: Starbucks
    
    static let starbucksHeading = "Starbucks Reward"
    
    func starbucksVoucherCard() -> HomeCard {
        let homeCard = HomeCard()
        homeCard.type = .Vouchers
        homeCard.validTo = Date() - 2.days
        
        let meta1 = HomeCardMetadata()
        meta1.type = .RewardName
        meta1.value = HomeCardTests.starbucksHeading
        homeCard.cardMetadatas.append(meta1)
        
        let meta2 = HomeCardMetadata()
        meta2.type = .RewardId
        meta2.value = "\(RewardReferences.StarbucksVoucher.rawValue)"
        homeCard.cardMetadatas.append(meta2)
        
        return homeCard
    }
    
    func testStarbucksVoucherConfirmDetailsCard() {
        // setup
        let homeCard = starbucksVoucherCard()
        homeCard.status = .ConfirmDetails
        
        //
        let controller = cardController(for: homeCard)
        let homeCardValue = controller.card!
        
        // assert
        XCTAssertTrue(homeCardValue.heading() == HomeCardTests.starbucksHeading)
        XCTAssertTrue(homeCardValue.title() == HomeStrings.CardTitleConfirmDetails1091)
        let expectedSubtitle = ActiveRewardsStrings.Ar.VoucherExpires658(Localization.dayDateLongMonthyearFormatter.string(from: homeCard.validTo))
        XCTAssertTrue(homeCardValue.subtitle() == expectedSubtitle)
        XCTAssertTrue(homeCardValue.actionTitle() == HomeStrings.CardActionTitleViewReward1092)
        XCTAssertTrue(homeCardValue.voucherRewardId() == RewardReferences.StarbucksVoucher.rawValue)
        XCTAssertTrue(homeCardValue.mainImageForCurrentStatus() == VIACoreAsset.ActiveRewards.Rewards.partnerStarbucks)
    }
    
    func testStarbucksFreeDrinkUseRewardCard() {
        // setup
        let homeCard = starbucksVoucherCard()
        homeCard.status = .Available
        
        let meta3 = HomeCardMetadata()
        meta3.type = .RewardValueDesc
        meta3.value = HomeStrings.CardTitleStarbucksFreeDrink1094
        homeCard.cardMetadatas.append(meta3)
        
        //
        let controller = cardController(for: homeCard)
        let homeCardValue = controller.card!
        
        // assert
        XCTAssertTrue(homeCardValue.heading() == HomeCardTests.starbucksHeading)
        XCTAssertTrue(homeCardValue.title() == meta3.value)
        let expectedSubtitle = ActiveRewardsStrings.Ar.VoucherExpires658(Localization.dayDateLongMonthyearFormatter.string(from: homeCard.validTo))
        XCTAssertTrue(homeCardValue.subtitle() == expectedSubtitle)
        XCTAssertTrue(homeCardValue.actionTitle() == HomeStrings.CardActionTitleUseReward1093)
        XCTAssertTrue(homeCardValue.voucherRewardId() == RewardReferences.StarbucksVoucher.rawValue)
        XCTAssertTrue(homeCardValue.mainImageForCurrentStatus() == VIACoreAsset.ActiveRewards.Rewards.partnerStarbucks)
    }
    
    func testStarbucksFreeDrinkViewRewardCard() {
        // setup
        let homeCard = starbucksVoucherCard()
        homeCard.status = .Pending
        
        //
        let controller = cardController(for: homeCard)
        let homeCardValue = controller.card!
        
        // assert
        XCTAssertTrue(homeCardValue.heading() == HomeCardTests.starbucksHeading)
        XCTAssertTrue(homeCardValue.title() == HomeStrings.CardTitleStarbucksFreeDrink1094)
        XCTAssertTrue(homeCardValue.subtitle() == HomeStrings.CardTitleStarbucksFreeDrinkPending1095)
        XCTAssertTrue(homeCardValue.actionTitle() == HomeStrings.CardActionTitleViewReward1092)
        XCTAssertTrue(homeCardValue.voucherRewardId() == RewardReferences.StarbucksVoucher.rawValue)
        XCTAssertTrue(homeCardValue.mainImageForCurrentStatus() == VIACoreAsset.ActiveRewards.Rewards.partnerStarbucks)
    }
    
    func testStarbucksFreeDrinkNotAwardedCard() {
        // setup
        let homeCard = starbucksVoucherCard()
        homeCard.status = .NotIssued
        
        //
        let controller = cardController(for: homeCard)
        let homeCardValue = controller.card!
        
        // assert
        XCTAssertTrue(homeCardValue.heading() == HomeCardTests.starbucksHeading)
        XCTAssertTrue(homeCardValue.title() == HomeStrings.CardTitleStarbucksFreeDrinkNotAwarded1096)
        XCTAssertTrue(homeCardValue.subtitle() == nil)
        XCTAssertTrue(homeCardValue.actionTitle() == HomeStrings.CardActionTitleViewReward1092)
        XCTAssertTrue(homeCardValue.voucherRewardId() == RewardReferences.StarbucksVoucher.rawValue)
        XCTAssertTrue(homeCardValue.mainImageForCurrentStatus() == VIACoreAsset.ActiveRewards.Rewards.partnerStarbucks)
    }
    
    // MARK: Cineworld
    
    static let cinemaHeading = HomeStrings.CardHeadingCineworldOrVue1098
    
    func cinemaCard(type: RewardReferences, title: String = HomeCardTests.cinemaHeading) -> HomeCard {
        let homeCard = HomeCard()
        homeCard.type = .RewardSelection
        homeCard.validTo = Date() - 2.days
        
        let meta1 = HomeCardMetadata()
        meta1.type = .RewardName
        meta1.value = title
        homeCard.cardMetadatas.append(meta1)
        
        let meta2 = HomeCardMetadata()
        meta2.type = .RewardId
        meta2.value = "\(type.rawValue)"
        homeCard.cardMetadatas.append(meta2)
        
        return homeCard
    }
    
    func testCinemaPendingRewardCard() {
        // setup
        let homeCard = cinemaCard(type: RewardReferences.CineworldOrVue)
        homeCard.status = .Pending
        
        // 3 x Movie Ticket
        let meta3 = HomeCardMetadata()
        meta3.type = .RewardValueQuantity
        meta3.value = "3"
        homeCard.cardMetadatas.append(meta3)
        let meta4 = HomeCardMetadata()
        meta4.type = .RewardValueDesc
        meta4.value = "Movie Ticket"
        homeCard.cardMetadatas.append(meta4)
        
        //
        let controller = cardController(for: homeCard)
        let homeCardValue = controller.card!
        
        // assert
        XCTAssertTrue(homeCardValue.heading() == HomeCardTests.cinemaHeading)
        let expectedTitle = "\(meta3.value) x \(meta4.value)"
        XCTAssertTrue(homeCardValue.title() == expectedTitle)
        XCTAssertTrue(homeCardValue.subtitle() == HomeStrings.CardTitleStarbucksFreeDrinkPending1095)
        XCTAssertTrue(homeCardValue.actionTitle() == HomeStrings.CardActionTitleViewReward1092)
        XCTAssertTrue(homeCardValue.voucherRewardId() == RewardReferences.CineworldOrVue.rawValue)
    }
    
    func testCinemaRedeemRewardCard() {
        // setup
        let homeCard = cinemaCard(type: RewardReferences.CineworldOrVue)
        homeCard.validTo = Date() + 5.days
        homeCard.status = .AvailableToRedeem
        
        // 3 x Movie Ticket
        let meta3 = HomeCardMetadata()
        meta3.type = .RewardValueQuantity
        meta3.value = "3"
        homeCard.cardMetadatas.append(meta3)
        let meta4 = HomeCardMetadata()
        meta4.type = .RewardValueDesc
        meta4.value = "Movie Ticket"
        homeCard.cardMetadatas.append(meta4)
        
        //
        let controller = cardController(for: homeCard)
        let homeCardValue = controller.card!
        
        // assert
        XCTAssertTrue(homeCardValue.heading() == HomeCardTests.cinemaHeading)
        let expectedTitle = "\(meta3.value) x \(meta4.value)"
        XCTAssertTrue(homeCardValue.title() == expectedTitle)
        let expectedSubtitle = ActiveRewardsStrings.Ar.VoucherExpires658(Localization.dayDateLongMonthyearFormatter.string(from: homeCard.validTo))
        XCTAssertTrue(homeCardValue.subtitle() == expectedSubtitle)
        XCTAssertTrue(homeCardValue.actionTitle() == HomeStrings.CardActionTitleRedeemReward1097)
        XCTAssertTrue(homeCardValue.voucherRewardId() == RewardReferences.CineworldOrVue.rawValue)
    }
    
    func testCinemaUseRewardCard() {
        // setup
        let homeCard = cinemaCard(type: RewardReferences.CineworldOrVue)
        homeCard.type = .Vouchers
        homeCard.status = .Available
        
        // 3 x Movie Ticket
        let meta3 = HomeCardMetadata()
        meta3.type = .RewardValueQuantity
        meta3.value = "3"
        homeCard.cardMetadatas.append(meta3)
        let meta4 = HomeCardMetadata()
        meta4.type = .RewardValueDesc
        meta4.value = "Movie Ticket"
        homeCard.cardMetadatas.append(meta4)
        
        //
        let controller = cardController(for: homeCard)
        let homeCardValue = controller.card!
        
        // assert
        XCTAssertTrue(homeCardValue.heading() == HomeCardTests.cinemaHeading)
        let expectedTitle = "\(meta3.value) x \(meta4.value)"
        XCTAssertTrue(homeCardValue.title() == expectedTitle)
        let expectedSubtitle = ActiveRewardsStrings.Ar.VoucherExpires658(Localization.dayDateLongMonthyearFormatter.string(from: homeCard.validTo))
        XCTAssertTrue(homeCardValue.subtitle() == expectedSubtitle)
        XCTAssertTrue(homeCardValue.actionTitle() == HomeStrings.CardActionTitleUseReward1093)
        XCTAssertTrue(homeCardValue.voucherRewardId() == RewardReferences.CineworldOrVue.rawValue)
    }
    
    // MARK:
    
    func testHomeCardHeadingsAndHeadingImages() {
        let card: HomeCard = HomeCard()
        let types: [CardTypeRef] = [.ActiveRewards,
                                    .AppleWatch,
                                    .HealthHub,
                                    .MentalWellbeing,
                                    .NonSmokersDecl,
                                    .Rewards,
                                    .ScreenandVacc,
                                    .VitNutAssessment,
                                    .VitalityHealthCheck,
                                    .VitalityHealthReview,
                                    .Vouchers,
                                    .WellDevandApps,
                                    .HealthServices]
        
        for type in types {
            card.type = type
            let cardValue = HomeCardValue(homeCard: card)
            var heading: String?
            var headingImage: VIACoreImageAsset
            switch cardValue.type {
            case .NonSmokersDecl:
                heading = NSDStrings.HomeCard.CardTitle96
                headingImage = VIACoreAsset.HomeScreenCards.Headings.nonSmokers
                XCTAssertTrue(cardValue.heading() == heading, "Home card heading incorrect")
                XCTAssertTrue(cardValue.headingImage() == headingImage, "Home card heading image")
            case .VitalityHealthCheck:
                heading = CommonStrings.HomeCard.CardTitle125
                headingImage = VIACoreAsset.HomeScreenCards.Headings.vhc
                XCTAssertTrue(cardValue.heading() == heading, "Home card heading incorrect")
                XCTAssertTrue(cardValue.headingImage() == headingImage, "Home card heading image")
            case .VitalityHealthReview:
                heading = CommonStrings.HomeCard.CardSectionTitle291
                headingImage = VIACoreAsset.HomeScreenCards.Headings.vhr
                XCTAssertTrue(cardValue.heading() == heading, "Home card heading incorrect")
                XCTAssertTrue(cardValue.headingImage() == headingImage, "Home card heading image")
            case .VitNutAssessment:
                heading = CommonStrings.HomeCard.CardSectionTitle388
                headingImage = VIACoreAsset.HomeScreenCards.Headings.nutrition
                XCTAssertTrue(cardValue.heading() == heading, "Home card heading incorrect")
                XCTAssertTrue(cardValue.headingImage() == headingImage, "Home card heading image")
            case .WellDevandApps:
                heading = WellnessDevicesStrings.Wda.Title414
                headingImage = VIACoreAsset.HomeScreenCards.Headings.linkDevices
                XCTAssertTrue(cardValue.heading() == heading, "Home card heading incorrect")
                XCTAssertTrue(cardValue.headingImage() == headingImage, "Home card heading image")
            case .AppleWatch:
                heading = CommonStrings.HomeCard.CardSectionTitle364
                headingImage = VIACoreAsset.HomeScreenCards.Headings.appleWatch
                XCTAssertTrue(cardValue.heading() == heading, "Home card heading incorrect")
                XCTAssertTrue(cardValue.headingImage() == headingImage, "Home card heading image")
            case .ScreenandVacc:
                heading = CommonStrings.HomeCard.CardSectionTitle365
                headingImage = VIACoreAsset.HomeScreenCards.Headings.screeningsVac
                XCTAssertTrue(cardValue.heading() == heading, "Home card heading incorrect")
                XCTAssertTrue(cardValue.headingImage() == headingImage, "Home card heading image")
            case .ActiveRewards:
                heading = CommonStrings.HomeCard.CardSectionTitle609
                headingImage = VIACoreAsset.HomeScreenCards.Headings.activeRewards
                XCTAssertTrue(cardValue.heading() == heading, "Home card heading incorrect")
                XCTAssertTrue(cardValue.headingImage() == headingImage, "Home card heading image")
            case .HealthHub:
                heading = CommonStrings.GenericNotAvailable270
                headingImage = VIACoreAsset.HomeScreenCards.hsCardExclamationBig
                XCTAssertTrue(cardValue.heading() == heading, "Home card heading incorrect")
                XCTAssertTrue(cardValue.headingImage() == headingImage, "Home card heading image")
            case .Rewards:
                XCTAssertTrue(true) // should be tested for each reward type
            case .Vouchers:
                XCTAssertTrue(true) // should be tested for each voucher type
            case .MentalWellbeing:
                heading = CommonStrings.GenericNotAvailable270
                headingImage = VIACoreAsset.HomeScreenCards.hsCardExclamationBig
                XCTAssertTrue(cardValue.heading() == heading, "Home card heading incorrect")
                XCTAssertTrue(cardValue.headingImage() == headingImage, "Home card heading image")
            case .RewardPartners:
                heading = HomeStrings.CardHeadingRewardPartners845
                headingImage = VIACoreAsset.HomeScreenCards.Headings.partnersRewards
                XCTAssertTrue(cardValue.heading() == heading, "Home card heading incorrect")
                XCTAssertTrue(cardValue.headingImage() == headingImage, "Home card heading image")
                XCTAssertTrue(cardValue.actionTitle() == HomeStrings.CardActionTitleViewPartners842, "Home card heading image")
            case .HealthPartners:
                heading = HomeStrings.CardHeadingHealthPartners843
                headingImage = VIACoreAsset.HomeScreenCards.Headings.partnersHealth
                XCTAssertTrue(cardValue.heading() == heading, "Home card heading incorrect")
                XCTAssertTrue(cardValue.headingImage() == headingImage, "Home card heading image")
                XCTAssertTrue(cardValue.actionTitle() == HomeStrings.CardActionTitleViewPartners842, "Home card heading image")
            case .WellnessPartners:
                heading = HomeStrings.CardHeadingWellnessPartners844
                headingImage = VIACoreAsset.HomeScreenCards.Headings.partnersWellness
                XCTAssertTrue(cardValue.heading() == heading, "Home card heading incorrect")
                XCTAssertTrue(cardValue.headingImage() == headingImage, "Home card heading image")
                XCTAssertTrue(cardValue.actionTitle() == HomeStrings.CardActionTitleViewPartners842, "Home card heading image")
            case .OrganisedFitnessEvents:
                heading = CommonStrings.GenericNotAvailable270
                headingImage = VIACoreAsset.HomeScreenCards.hsCardExclamationBig
                XCTAssertTrue(cardValue.heading() == heading, "Home card heading incorrect")
                XCTAssertTrue(cardValue.headingImage() == headingImage, "Home card heading image")
            case .DeviceCashback:
                heading = CommonStrings.GenericNotAvailable270
                headingImage = VIACoreAsset.HomeScreenCards.hsCardExclamationBig
                XCTAssertTrue(cardValue.heading() == heading, "Home card heading incorrect")
                XCTAssertTrue(cardValue.headingImage() == headingImage, "Home card heading image")
            case .PolicyCashback:
                heading = CommonStrings.GenericNotAvailable270
                headingImage = VIACoreAsset.HomeScreenCards.hsCardExclamationBig
                XCTAssertTrue(cardValue.heading() == heading, "Home card heading incorrect")
                XCTAssertTrue(cardValue.headingImage() == headingImage, "Home card heading image")
            case .RewardSelection:
                heading = CommonStrings.GenericNotAvailable270
                headingImage = VIACoreAsset.HomeScreenCards.hsCardExclamationBig
                XCTAssertTrue(cardValue.heading() == heading, "Home card heading incorrect")
                XCTAssertTrue(cardValue.headingImage() == headingImage, "Home card heading image")
            case .Unknown:
                heading = CommonStrings.GenericNotAvailable270
                headingImage = VIACoreAsset.HomeScreenCards.hsCardExclamationBig
                XCTAssertTrue(cardValue.heading() == heading, "Home card heading incorrect")
                XCTAssertTrue(cardValue.headingImage() == headingImage, "Home card heading image")
			case .HealthServices:
				heading = HealthServicesStrings.HealthServicesTitle1331
				// TODO need heading image
				// headingImage = VIACoreAsset.HomeScreenCards.hsCardHealthServicesBig
				XCTAssertTrue(cardValue.heading() == heading, "Home card heading incorrect")
				//XCTAssertTrue(cardValue.headingImage() == headingImage, "Home card heading image")
                break
            }
        }
    }
    
//    func testActiveRewardsCard() {
//        let card = HomeCard()
//        card.type = .ActiveRewards
//        
//        // strings
//        card.status = .NotStarted
//        var cardValue = HomeCardValue(homeCard: card)
//        XCTAssertTrue(cardValue.title() == ActiveRewardsStrings.Ar.Landing.HomeViewTitle710, "AR Home card title incorrect for not started status")
//        XCTAssertTrue(cardValue.subtitle() == nil, "VHC Home card subtitle incorrect for not started status")
//        XCTAssertTrue(cardValue.actionTitle() == CommonStrings.GetStartedButtonTitle103, "AR Home card action title incorrect for not started status")
//        XCTAssertTrue(cardValue.mainImageForCurrentStatus() == VIACoreAsset.HomeScreenCards.hsCardLockedBig, "AR Home card main image incorrect")
//        
//        card.status = .Activated
//        cardValue = HomeCardValue(homeCard: card)
//        XCTAssertTrue(cardValue.title() == CommonStrings.HomeCard.CardEarnUpToPointsMessage124(cardValue.metadataPotentialPoints), "VHC Home card title incorrect for not started status")
//        XCTAssertTrue(cardValue.subtitle() == nil, "VHC Home card subtitle incorrect for not started status")
//        XCTAssertTrue(cardValue.actionTitle() == CommonStrings.GetStartedButtonTitle103, "VHC Home card action title incorrect for not started status")
//        XCTAssertTrue(cardValue.mainImageForCurrentStatus() == VIACoreAsset.HomeScreenCards.hsCardLockedBig, "AR Home card main image incorrect")
//        
//        card.status = .InProgress
//        cardValue = HomeCardValue(homeCard: card)
//        XCTAssertTrue(cardValue.title() == CommonStrings.HomeCard.PointsEarnedMessage252(cardValue.metadataEarnedPoints, cardValue.metadataPotentialPoints), "VHC Home card title incorrect for not started status")
//        XCTAssertTrue(cardValue.subtitle() == nil, "VHC Home card subtitle incorrect for not started status")
//        XCTAssertTrue(cardValue.actionTitle() == nil, "VHC Home card action title incorrect for not started status")
//        XCTAssertTrue(cardValue.mainImageForCurrentStatus() == VIACoreAsset.HomeScreenCards.hsCardLockedBig, "AR Home card main image incorrect")
//        
//        card.status = .Achieved
//        cardValue = HomeCardValue(homeCard: card)
//        XCTAssertTrue(cardValue.title() == CommonStrings.HomeCard.CardCompletedTitle290, "VHC Home card title incorrect for not started status")
//        XCTAssertTrue(cardValue.subtitle() == nil, "VHC Home card subtitle incorrect for not started status")
//        XCTAssertTrue(cardValue.actionTitle() == HomeStrings.HomeCard.EditUpdateMessage253, "VHC Home card action title incorrect for not started status")
//        XCTAssertTrue(cardValue.mainImageForCurrentStatus() == VIACoreAsset.HomeScreenCards.hsCardLockedBig, "AR Home card main image incorrect")
//    }
//    
//    func testNonSmokersDeclCard() {
//        let card = HomeCard()
//        card.type = .NonSmokersDecl
//        
//        card.status = .NotStarted
//        card.metadataPotentialPoints = "250"
//        var cardValue = HomeCardValue(homeCard: card)
//        XCTAssertTrue(cardValue.title() == NSDStrings.HomeCard.CardPotentialPointsMessage97(card.metadataPotentialPoints), "Home card title incorrect for not started status")
//        XCTAssertTrue(cardValue.subtitle() == nil, "Home card subtitle incorrect for not started status")
//        XCTAssertTrue(cardValue.actionTitle() == NSDStrings.HomeCard.CardActionLinkButtonName98, "Home card action title incorrect for not started status")
//        XCTAssertTrue(cardValue.mainImageForCurrentStatus() == VIACoreAsset.hsCardNonSmokersBig.image, "Home card action title incorrect for not started status")
//        
//        card.status = .Done
//        card.metadataEarnedPoints = "150"
//        cardValue = HomeCardValue(homeCard: card)
//        XCTAssertTrue(cardValue.title() == CommonStrings.Confirmation.CompletedTitle117, "Home card title incorrect for not started status")
//        XCTAssertTrue(cardValue.subtitle() == NSDStrings.HomeCard.CardEarnedPointsMessage121(card.metadataEarnedPoints), "Home card subtitle incorrect for not started status")
//        XCTAssertTrue(cardValue.actionTitle() == nil, "Home card action title incorrect for not started status")
//        XCTAssertTrue(cardValue.mainImageForCurrentStatus() == VIACoreAsset.hsCardCheckmarkBig.image, "Home card action title incorrect for not started status")
//    }
//    
//    func testVitalityHealthCheckCard() {
//        let card = HomeCard()
//        card.type = .VitalityHealthCheck
//        card.metadataEarnedPoints = "0"
//        card.metadataPotentialPoints = "5000"
//        
//        card.status = .NotStarted
//        var cardValue = HomeCardValue(homeCard: card)
//        XCTAssertTrue(cardValue.title() == CommonStrings.HomeCard.CardEarnUpToPointsMessage124(cardValue.metadataPotentialPoints), "VHC Home card title incorrect for not started status")
//        XCTAssertTrue(cardValue.subtitle() == nil, "VHC Home card subtitle incorrect for not started status")
//        XCTAssertTrue(cardValue.actionTitle() == CommonStrings.GetStartedButtonTitle103, "VHC Home card action title incorrect for not started status")
//        
//        card.status = .InProgress
//        card.metadataEarnedPoints = "1000"
//        cardValue = HomeCardValue(homeCard: card)
//        XCTAssertTrue(cardValue.title() == CommonStrings.HomeCard.PointsEarnedMessage252(cardValue.metadataEarnedPoints, cardValue.metadataPotentialPoints), "VHC Home card title incorrect for not started status")
//        XCTAssertTrue(cardValue.subtitle() == nil, "VHC Home card subtitle incorrect for not started status")
//        XCTAssertTrue(cardValue.actionTitle() == nil, "VHC Home card action title incorrect for not started status")
//        
//        card.status = .Done
//        cardValue = HomeCardValue(homeCard: card)
//        XCTAssertTrue(cardValue.title() == CommonStrings.Confirmation.CompletedTitle117, "VHC Home card title incorrect for not started status")
//        XCTAssertTrue(cardValue.subtitle() == nil, "VHC Home card subtitle incorrect for not started status")
//        XCTAssertTrue(cardValue.actionTitle() == HomeStrings.HomeCard.EditUpdateMessage253, "VHC Home card action title incorrect for not started status")
//    }
//    
//    func testVitalityHealthReviewCard() {
//        let card = HomeCard()
//        card.type = .VitalityHealthReview
//        
//        card.status = .NotStarted
//        var cardValue = HomeCardValue(homeCard: card)
//        XCTAssertTrue(cardValue.title() == CommonStrings.HomeCard.CardEarnTitle292(card.metadataPotentialPoints), "Home card title incorrect for not started status")
//        XCTAssertTrue(cardValue.subtitle() == nil, "Home card subtitle incorrect for not started status")
//        XCTAssertTrue(cardValue.actionTitle() == CommonStrings.GetStartedButtonTitle103, "Home card action title incorrect for not started status")
//        XCTAssertTrue(cardValue.mainImageForCurrentStatus() == CoreAsset.hsCardCheckmarkBig, "Home card action title incorrect for not started status")
//        
//        card.status = .InProgress
//        cardValue = HomeCardValue(homeCard: card)
//        XCTAssertTrue(cardValue.title() == CommonStrings.HomeCard.CardEarnTitle292(card.metadataPotentialPoints), "Home card title incorrect for not started status")
//        XCTAssertTrue(cardValue.subtitle() == nil, "Home card subtitle incorrect for not started status")
//        XCTAssertTrue(cardValue.actionTitle() == CommonStrings.GetStartedButtonTitle103, "Home card action title incorrect for not started status")
//        XCTAssertTrue(cardValue.mainImageForCurrentStatus() == CoreAsset.hsCardCheckmarkBig, "Home card action title incorrect for not started status")
//        
//        card.status = .Done
//        cardValue = HomeCardValue(homeCard: card)
//        XCTAssertTrue(cardValue.title() == CommonStrings.Confirmation.CompletedTitle117, "Home card title incorrect for not started status")
//        XCTAssertTrue(cardValue.subtitle() == HomeStrings.HomeCard.EditUpdateMessage253, "Home card subtitle incorrect for not started status")
//        XCTAssertTrue(cardValue.actionTitle() == CommonStrings.GetStartedButtonTitle103, "Home card action title incorrect for not started status")
//        XCTAssertTrue(cardValue.mainImageForCurrentStatus() == CoreAsset.hsCardCheckmarkBig, "Home card action title incorrect for not started status")
//    }
    
}
