//
//  VHCLandingDataControllerTests.swift
//  VitalityActive
//
//  Created by Lionel Camacho on 2017/04/04.
//  Copyright © 2017 Glucode. All rights reserved.
//

import XCTest
import VitalityKit
import RealmSwift

@testable import VIACore
@testable import VIAUIKit
@testable import VitalityKit

class HomeStatusTests: XCTestCase {
    var getHomeScreenResponse: GetHomeScreenResponse?
    var viewModel: VIAHomeViewModel?
    
    func response(from file: String) -> GetHomeScreenResponse? {
        var response: GetHomeScreenResponse? = nil
        guard let pathString = Bundle(for: type(of: self)).path(forResource: file, ofType: "json") else {
            fatalError("\(file) not found")
        }
        
        guard let jsonString = try? NSString(contentsOfFile: pathString, encoding: String.Encoding.utf8.rawValue) else {
            fatalError("Unable to convert \(file) to String")
        }
        
        print("The JSON string is: \(jsonString)")
        
        guard let jsonData = jsonString.data(using: String.Encoding.utf8.rawValue) else {
            fatalError("Unable to convert \(file) to NSData")
        }
        
        do {
            guard let jsonDictionary = try JSONSerialization.jsonObject(with: jsonData, options: []) as? [String:Any] else {
                fatalError("Unable to convert \(file) to JSON dictionary")
            }
            response = GetHomeScreenResponse.init(json: jsonDictionary)!
        } catch _ {
            
        }
        
        return response
    }
    
    override func setUp() {
        super.setUp()
        let realm = DataProvider.newVHCRealm()
        try! realm.write {
            realm.deleteAll()
        }
        viewModel = VIAHomeViewModel()
    }
    
    override func tearDown() {
        viewModel = nil
        let realm = DataProvider.newVHCRealm()
        try! realm.write {
            realm.deleteAll()
        }
        super.tearDown()
    }
    
    func testBlueStatusHasAllRequiredDataAndDisplaysMessageToReachBlueLevelAgain() {
        let file = "home_blue_status_get_home_screen_response"
        guard let homeViewModel = viewModel
            else { fatalError("viewModel not initialized") }
        guard let blueResponse = response(from: file)
            else { fatalError("Unable to generate response from file provided") }
        
        HomeScreenData.parseAndPersistGetHomeScreenData(response: blueResponse)
        
        guard let progress = homeViewModel.latestHomeScreenData()?.calculateStatusProgress()
            else { fatalError("unable to get status progress") }
        guard let icon = homeViewModel.latestHomeScreenData()?.statusIcon()
            else { fatalError("unable to get status icon") }
        guard let pointsMessage = homeViewModel.latestHomeScreenData()?.configureStatusPointsMessage()
            else { fatalError("unable to get status pointsMessage") }
        guard let title = homeViewModel.latestHomeScreenData()?.statusTitle()
            else { fatalError("unable to get status title") }
        
        XCTAssertEqual("Blue", title)
        XCTAssertEqual(0.01, progress)
        XCTAssertEqual("Status.landing_points_maintain_message_819", pointsMessage)
        XCTAssertEqual(VIAUIKitAsset.Status.badgeBlueSmall.image, icon)
    }
    
    func testBronzeStatusHasAllRequiredDataAndDisplaysMessageToNextLevelSilver() {
        let file = "home_bronze_status_get_home_screen_response"
        guard let homeViewModel = viewModel
            else { fatalError("viewModel not initialized") }
        guard let bronzeResponse = response(from: file)
            else { fatalError("Unable to generate response from file provided") }
        
        HomeScreenData.parseAndPersistGetHomeScreenData(response: bronzeResponse)
        
        guard let progress = homeViewModel.latestHomeScreenData()?.calculateStatusProgress()
            else { fatalError("unable to get status progress") }
        guard let icon = homeViewModel.latestHomeScreenData()?.statusIcon()
            else { fatalError("unable to get status icon") }
        guard let pointsMessage = homeViewModel.latestHomeScreenData()?.configureStatusPointsMessage()
            else { fatalError("unable to get status pointsMessage") }
        guard let title = homeViewModel.latestHomeScreenData()?.statusTitle()
            else { fatalError("unable to get status title") }
        
        XCTAssertEqual("Bronze", title)
        XCTAssertEqual(0.52, progress)
        XCTAssertEqual("Status.landing_points_target_message_797", pointsMessage)
        XCTAssertEqual(VIAUIKitAsset.Status.badgeBronzeSmall.image, icon)
    }
    
    func testSilverStatusHasAllRequiredDataAndDisplaysMessageToReachNextLevelGold() {
        let file = "home_silver_status_get_home_screen_response"
        guard let homeViewModel = viewModel
            else { fatalError("viewModel not initialized") }
        guard let silverResponse = response(from: file)
            else { fatalError("Unable to generate response from file provided") }
        
        HomeScreenData.parseAndPersistGetHomeScreenData(response: silverResponse)
        
        guard let progress = homeViewModel.latestHomeScreenData()?.calculateStatusProgress()
            else { fatalError("unable to get status progress") }
        guard let icon = homeViewModel.latestHomeScreenData()?.statusIcon()
            else { fatalError("unable to get status icon") }
        guard let pointsMessage = homeViewModel.latestHomeScreenData()?.configureStatusPointsMessage()
            else { fatalError("unable to get status pointsMessage") }
        guard let title = homeViewModel.latestHomeScreenData()?.statusTitle()
            else { fatalError("unable to get status title") }
        
        XCTAssertEqual("Silver", title)
        XCTAssertEqual(0.75, progress)
        XCTAssertEqual("Status.landing_points_target_message_797", pointsMessage)
        XCTAssertEqual(VIAUIKitAsset.Status.badgeSilverSmall.image, icon)
    }
    
    func testGoldStatusHasAllRequiredDataAndDisplaysMessageToReachLevelPlatinum() {
        let file = "home_gold_status_get_home_screen_response"
        guard let homeViewModel = viewModel
            else { fatalError("viewModel not initialized") }
        guard let goldResponse = response(from: file)
            else { fatalError("Unable to generate response from file provided") }
        
        HomeScreenData.parseAndPersistGetHomeScreenData(response: goldResponse)
        
        guard let progress = homeViewModel.latestHomeScreenData()?.calculateStatusProgress()
            else { fatalError("unable to get status progress") }
        guard let icon = homeViewModel.latestHomeScreenData()?.statusIcon()
            else { fatalError("unable to get status icon") }
        guard let pointsMessage = homeViewModel.latestHomeScreenData()?.configureStatusPointsMessage()
            else { fatalError("unable to get status pointsMessage") }
        guard let title = homeViewModel.latestHomeScreenData()?.statusTitle()
            else { fatalError("unable to get status title") }
        
        XCTAssertEqual("Gold", title)
        XCTAssertEqual(0.8666667, progress)
        XCTAssertEqual("Status.landing_points_target_message_797", pointsMessage)
        XCTAssertEqual(VIAUIKitAsset.Status.badgeGoldSmall.image, icon)
    }
    
    func testPlatinumStatusHasAllRequiredDataAndDisplaysMessageWellDone() {
        let file = "home_platinum_status_get_home_screen_response"
        guard let homeViewModel = viewModel
            else { fatalError("viewModel not initialized") }
        guard let platinumResponse = response(from: file)
            else { fatalError("Unable to generate response from file provided") }
        
        HomeScreenData.parseAndPersistGetHomeScreenData(response: platinumResponse)
        
        guard let progress = homeViewModel.latestHomeScreenData()?.calculateStatusProgress()
            else { fatalError("unable to get status progress") }
        guard let icon = homeViewModel.latestHomeScreenData()?.statusIcon()
            else { fatalError("unable to get status icon") }
        guard let pointsMessage = homeViewModel.latestHomeScreenData()?.configureStatusPointsMessage()
            else { fatalError("unable to get status pointsMessage") }
        guard let title = homeViewModel.latestHomeScreenData()?.statusTitle()
            else { fatalError("unable to get status title") }
        
        XCTAssertEqual("Platinum", title)
        XCTAssertEqual(1.0, progress)
        XCTAssertEqual("Status.landing_points_final_message_824", pointsMessage)
        XCTAssertEqual(VIAUIKitAsset.Status.badgePlatinumSmall.image, icon)
    }
    
}
