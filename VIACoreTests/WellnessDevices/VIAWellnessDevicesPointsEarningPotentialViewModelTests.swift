//
//  VIAWellnessDevicesPointsEarningPotentialViewModelTests.swift
//  VitalityActive
//
//  Created by admin on 2017/09/20.
//  Copyright © 2017 Glucode. All rights reserved.
//

import XCTest
import XCTest
import VIACore

@testable import VIACore
@testable import VIAUtilities
@testable import VitalityKit
@testable import VitalityKit

class VIAWellnessDevicesPointsEarningPotentialViewModelTests: XCTestCase {
    let wdaRealm = DataProvider.newWDARealm()
    override func setUp() {
        super.setUp()
        try! wdaRealm.write {
            wdaRealm.deleteAll()
        }
        let file = "HowToEarnPoints"
        guard let pathString = Bundle(for: type(of: self)).path(forResource: file, ofType: "json") else {
            fatalError("PotentialPointsResponse.json not found")
        }
        guard let jsonString = try? NSString(contentsOfFile: pathString, encoding: String.Encoding.utf8.rawValue) else {
            fatalError("Unable to convert PotentialPointsResponse.json to String")
        }
        print("The JSON string is: \(jsonString)")
        guard let jsonData = jsonString.data(using: String.Encoding.utf8.rawValue) else {
            fatalError("Unable to converPotentialPointsResponse.json to NSData")
        }
        do {
            guard let jsonDictionary = try JSONSerialization.jsonObject(with: jsonData, options: []) as? [String:Any] else {
                fatalError("Unable to convert HowToEarnPoints.json to JSON dictionary")
            }
            if let response = PotentialPointsByEventType.init(json: jsonDictionary) {
                PotentialPointsWithTiersEventType.persistEventOuts(response: response)
            }
        } catch _ {
            
        }
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testExample() {
        let viewModel:PointsEarningPotentialTableViewModel = VIAWellnessDevicesPointsEarningPotentialViewModel(earningActivityKey: 5)
        
        let condition = viewModel.conditions(for: 1)
        let wellnessString = WellnessDevicesStrings.PotentialPoints.SpeedDurationGreaterOrEqualBound582("30 minutes")
        let description = viewModel.pointEarningPotentialItemDescription(condition: condition[0])?.conditionString
        XCTAssertEqual(wellnessString, description)
        
    }
}
