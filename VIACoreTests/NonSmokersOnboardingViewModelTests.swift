//
//  NonSmokersOnboardingViewModelTests.swift
//  VitalityActive
//
//  Created by Lionel Camacho on 2017/01/24.
//  Copyright © 2017 Glucode. All rights reserved.
//

import XCTest
@testable import VIACore
@testable import VitalityKit
@testable import VIAUIKit

class NonSmokersOnboardingViewModelTests: XCTestCase {

    override func setUp() {
        super.setUp()
    }

    override func tearDown() {
        super.tearDown()
    }

    func testNonSmokersOnboardingScreenUsesCorrectButtonTitle() {
        let viewModel = NonSmokersOnboardingViewModel()
        let onBoardingButtonTitle = CommonStrings.GetStartedButtonTitle103

        XCTAssertEqual(viewModel.buttonTitle, onBoardingButtonTitle)
    }

    func testNonSmokersOnboardingScreenUsesCorrectAlternateButtonTitle() {
        let viewModel = NonSmokersOnboardingViewModel()
        let onBoardingButtonTitle = CommonStrings.LearnMoreButtonTitle104
        XCTAssertEqual(viewModel.alternateButtonTitle, onBoardingButtonTitle)
    }

    func testNonSmokersOnboardingScreenUsesCorrectGradientColor() {
        let viewModel = NonSmokersOnboardingViewModel()
        let onboardingColor = Color.green

        XCTAssertEqual(viewModel.gradientColor.hashValue, onboardingColor.hashValue)
    }
}
