//
//  LocalisationTest.swift
//  VitalityActive
//
//  Created by jacdevos on 2016/10/27.
//  Copyright © 2016 Glucode. All rights reserved.
//

import XCTest
import VIACore
@testable import VitalityKit

class LocalizationForDifferentLanguagesMatchTest: XCTestCase {
    var englishPath: String = ""
    var germanPath: String = ""
    var japanesePath: String = ""

    override func setUp() {
        super.setUp()
        setupLocalizationFolderPaths()
    }

    func setupLocalizationFolderPaths() {
        let bundle = Bundle(for: CommonStringsBundleToken.self)
        let resourcePath = bundle.resourcePath!
        englishPath = resourcePath + "/Base.lproj"
        japanesePath = resourcePath + "/ja.lproj"
    }

    func testAllLocalizationFoldersArePopulated() {
        XCTAssertNotNil(getFilePaths(atPath: englishPath), "Files at expected path for english localisation files not found")
        XCTAssertNotNil(getFilePaths(atPath: japanesePath), "Files at expected path for japanese localisation files not found")
    }

    func testDifferentLocalesShoudHaveSameAmountOfLocaleFiles() {
        let englishLocaleFiles = getFilePaths(atPath: englishPath)!
        let japaneseLocaleFiles = getFilePaths(atPath: japanesePath)!

        XCTAssertEqual(englishLocaleFiles.count, japaneseLocaleFiles.count, "Not the same amount of english and japanese locale files")
    }

    func testDifferentLocalesShoudHaveMatchingLocaleKeys() {
        let englishLocaleFiles = getFilePaths(atPath: englishPath)!

        for localeFileName in englishLocaleFiles {
            let englishLocaleKeys = getLocaleKeysFromBinaryPlistFile(path: "\(englishPath)/\(localeFileName)")
            let japaneseLocaleKeys = getLocaleKeysFromBinaryPlistFile(path: "\(japanesePath)/\(localeFileName)")

            let englishKeysNotInJapanese = englishLocaleKeys.subtracting(japaneseLocaleKeys)
            XCTAssertEqual(0, englishKeysNotInJapanese.count, "The following key(s) were in english locale file \(localeFileName) but not in japanese: \(englishKeysNotInJapanese)")

            let japaneseKeysNotInEnglish = japaneseLocaleKeys.subtracting(englishLocaleKeys)
            XCTAssertEqual(0, japaneseKeysNotInEnglish.count, "The following key(s) were in japanese locale file \(localeFileName) but not in english: \(japaneseKeysNotInEnglish)")
        }
    }

    func getLocaleKeysFromBinaryPlistFile(path: String) -> Set<String> {
        let plistDictionary: Dictionary<String, AnyObject>
        do {
            let binaryPlistData = try Data(contentsOf: URL(fileURLWithPath: path))
            plistDictionary = try PropertyListSerialization.propertyList(from: binaryPlistData, options: PropertyListSerialization.ReadOptions(), format: nil) as! Dictionary<String, AnyObject>
        } catch let error as NSError {
            XCTFail("Failed to load contents of binary plist at path \(path) with error \(error)")
            return Set<String>()
        }

        XCTAssertGreaterThan(plistDictionary.count, 0, "No localisation items found in file \(path) - is this file necessary?")

        var keys = Set<String>()
        for key in plistDictionary.keys {
            XCTAssertNotNil(plistDictionary[key], "No value for localization key \(key) in file \(path)")
            if let stringValue = plistDictionary[key] as? String {
                XCTAssertNotEqual("", stringValue, "Empty value for localization key \(key) in file \(path)")
            }
            keys.insert(key)
        }

        return keys
    }

    func getFilePaths(atPath: String) -> [String]? {
        do {
            return try FileManager.default.contentsOfDirectory(atPath: atPath)
        } catch let error as NSError {
            XCTFail("Did not find files at expected path \(atPath) with error \(error)")
        }
        return nil
    }
}
