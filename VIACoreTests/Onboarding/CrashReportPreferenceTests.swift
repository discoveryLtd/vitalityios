//
//  CrashReportPreferenceTests.swift
//  VitalityActive
//
//  Created by Marius Janse van Vuuren on 2017/03/01.
//  Copyright © 2017 Glucode. All rights reserved.
//

import XCTest
@testable import VitalityKit

class CrashReportPreferenceTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

//    func testCrashReportPreferenceImage() {
//        let preference: CrashReportPreference = CrashReportPreference()
//        XCTAssertEqual(preference.preferenceImage, UIImage.templateImage(asset: CoreAsset.setupReports))
//    }
//
//    func testCrashReportPreferenceTitle() {
//        let preference: CrashReportPreference = CrashReportPreference()
//        XCTAssertEqual(preference.preferenceTitle, LoginStrings.UserPrefs.CrashReportsToggleTitle75)
//    }
//
//    func testCrashReportPreferenceDetail() {
//        let preference: CrashReportPreference = CrashReportPreference()
//        XCTAssertEqual(preference.preferencesDetail, LoginStrings.UserPrefs.CrashReportsToggleMessage76)
//    }
}
