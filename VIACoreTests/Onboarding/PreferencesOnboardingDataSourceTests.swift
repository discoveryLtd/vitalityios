//
//  OnboardingDataSourceTests.swift
//  VitalityActive
//
//  Created by Marius Janse van Vuuren on 2017/02/27.
//  Copyright © 2017 Glucode. All rights reserved.
//

import XCTest
@testable import VitalityKit

class FirstTimePreferenceDataSourceTests: XCTestCase {
    var dataSource: FirstTimePreferenceDataSource?
    override func setUp() {
        super.setUp()
        dataSource = FirstTimePreferenceDataSource()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

//    func testDataSourceCategoryCount() {
//        XCTAssertEqual(self.dataSource?.preferences.count, 3)
//    }
//
//    func testDataSourceCommunicationCategoryCount() {
//        XCTAssertEqual(self.dataSource?.preferences[0].count, 3)
//    }
//
//    func testDataSourcePrivacyCategoryCount() {
//        XCTAssertEqual(self.dataSource?.preferences[0].count, 3)
//    }
//
//    func testDataSourceSecurityCategoryCount() {
//        XCTAssertEqual(self.dataSource?.preferences[0].count, 3)
//    }
//
//    func testDataSourcePrivacyAnalyticsPreferenceImage() {
//        let preference: PreferenceDataObject = self.dataSource?.preferences[1][1] as! PreferenceDataObject
//        XCTAssertEqual(preference.preferenceImage, UIImage.templateImage(asset: CoreAsset.setupAnalytics))
//    }
//
//    func testDataSourcePrivacyAnalyticsPreferenceTitle() {
//        let preference: AnalyticsPreference = (self.dataSource?.preferences[1][1] as! PreferenceDataObject) as! AnalyticsPreference
//        XCTAssertEqual(preference.preferenceTitle, LoginStrings.UserPrefs.AnalyticsToggleTitle73)
//    }
//    //TODO: test headers
}
