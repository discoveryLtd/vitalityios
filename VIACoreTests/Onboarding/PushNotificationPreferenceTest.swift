//
//  PushNotificationPreferenceTest.swift
//  VitalityActive
//
//  Created by Marius Janse van Vuuren on 2017/03/01.
//  Copyright © 2017 Glucode. All rights reserved.
//

import XCTest
@testable import VitalityKit

class PushNotificationPreferenceTest: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testPushNotificationPreferenceImage() {
        let preference: PushNotificationPreferenceObject = PushNotificationPreferenceObject()
        XCTAssertEqual(preference.preferenceImage, UIImage.templateImage(asset: CoreAsset.setupNotifications))
    }

    func testPushNotificationPreferenceTitle() {
        let preference: PushNotificationPreferenceObject = PushNotificationPreferenceObject()
        XCTAssertEqual(preference.preferenceTitle, LoginStrings.UserPrefs.PushMessageToggleTitle67)
    }

    func testPushNotificationPreferenceDetail() {
        let preference: PushNotificationPreferenceObject = PushNotificationPreferenceObject()
        XCTAssertEqual(preference.preferencesDetail, LoginStrings.UserPrefs.PushMessageToggleMessage68)
    }

}
