//
//  RememberMePreferenceTests.swift
//  VitalityActive
//
//  Created by Marius Janse van Vuuren on 2017/03/01.
//  Copyright © 2017 Glucode. All rights reserved.
//

import XCTest
@testable import VitalityKit
@testable import VitalityKit

class RememberMePreferenceTests: XCTestCase {

    override func setUp() {
        super.setUp()
        AppSettings.removeUsernameForRememberMe()
        let realm = DataProvider.newRealm()
        try! realm.write {
            realm.deleteAll()
        }
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        let realm = DataProvider.newRealm()
        try! realm.write {
            realm.deleteAll()
        }
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testRememberMePreferenceImage() {
        let preference: RememberMePreference = RememberMePreference()
        XCTAssertEqual(preference.preferenceImage, UIImage.templateImage(asset: CoreAsset.setupRememberMe))
    }

    func testRememberMePreferenceTitle() {
        let preference: RememberMePreference = RememberMePreference()
        XCTAssertEqual(preference.preferenceTitle, LoginStrings.UserPrefs.RememberMeToggleTitle81)
    }

    func testRememberMePreferenceDetail() {
        let preference: RememberMePreference = RememberMePreference()
        XCTAssertEqual(preference.preferencesDetail, LoginStrings.UserPrefs.RememberMeToggleMessage82)
    }

    func testRememberMeNotSet() {
        XCTAssertNil(AppSettings.usernameForRememberMe())
    }

    func testRemeberMeSet() {
        XCTAssertNil(AppSettings.usernameForRememberMe())
        AppSettings.setUsernameForRememberMe("mail@test.com")
        XCTAssertNotNil(AppSettings.usernameForRememberMe())
        XCTAssertEqual(AppSettings.usernameForRememberMe(), "mail@test.com")
    }

    func testSwitchState() {
        let mail = "mail@test.com"
        XCTAssertNil(AppSettings.usernameForRememberMe())
        AppSettings.setUsernameForRememberMe(mail)
        XCTAssertNotNil(AppSettings.usernameForRememberMe())
        XCTAssertEqual(AppSettings.usernameForRememberMe(), mail)
        let realm = DataProvider.newRealm()
        try! realm.write {
            let user = User()
            user.username = mail
            realm.add(user)
        }
        let preference: RememberMePreference = RememberMePreference()
        XCTAssertTrue(preference.preferenceInitialState())
    }
}
