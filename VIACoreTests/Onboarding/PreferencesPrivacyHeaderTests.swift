//
//  PreferencesPrivacyHeaderTests.swift
//  VitalityActive
//
//  Created by Marius Janse van Vuuren on 2017/03/01.
//  Copyright © 2017 Glucode. All rights reserved.
//

import XCTest
@testable import VitalityKit

class PreferencesPrivacyHeaderTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testHeaderTitle() {
        let preferenceHeader: PreferenceHeader = FirstTimePreferenceDataSource().setupPrivacyHeader()
        XCTAssertEqual(preferenceHeader.preferenceTitle, LoginStrings.UserPrefs.PrivacyGroupHeaderTitle70)
    }

    func testHeaderDetialText() {
        let preferenceHeader: PreferenceHeader = FirstTimePreferenceDataSource().setupPrivacyHeader()
        XCTAssertEqual(preferenceHeader.preferencesDetail, LoginStrings.UserPrefs.PrivacyGroupHeaderMessage71)
    }

    func testHeaderButtonTitle() {
        let preferenceHeader: PreferenceHeader = FirstTimePreferenceDataSource().setupPrivacyHeader()
        XCTAssertEqual(preferenceHeader.preferenceActionButton?.preferencesActionButtonTitle, LoginStrings.UserPrefs.PrivacyGroupHeaderLinkButtonTitle72)
    }
}
