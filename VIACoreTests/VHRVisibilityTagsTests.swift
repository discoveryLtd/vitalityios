import XCTest
@testable import VitalityKit

class VHRVisibilityTagsTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testSingleMalformedTag() {
        let helper = TagHelper()
        helper.visibilityTag = "100;==;Boolean"
        XCTAssertFalse(helper.isVisible)
        helper.updateVisibility(basedOn: 100, answer: "True")
        XCTAssertFalse(helper.isVisible)
    }
    
    func testMultipleMalformedTags() {
        var helper = TagHelper()
        helper.visibilityTag = "100;==;Boolean||100;==;Name"
        XCTAssertFalse(helper.isVisible)
        helper.updateVisibility(basedOn: 100, answer: "True")
        XCTAssertFalse(helper.isVisible)
        
        helper = TagHelper()
        helper.visibilityTag = "100;==;Boolean&&100;==;Name"
        XCTAssertFalse(helper.isVisible)
        helper.updateVisibility(basedOn: 100, answer: "True")
        XCTAssertFalse(helper.isVisible)
        
        helper = TagHelper()
        helper.visibilityTag = "100;==;Boolean&&100;==;True;Name"
        XCTAssertFalse(helper.isVisible)
        helper.updateVisibility(basedOn: 100, answer: "True")
        XCTAssertFalse(helper.isVisible)
    }
    
    func testSingleBooleanTag() {
        let helper = TagHelper()
        helper.visibilityTag = "100;==;True;Boolean"
        XCTAssertFalse(helper.isVisible)
        helper.updateVisibility(basedOn: 100, answer: "True")
        XCTAssertTrue(helper.isVisible)
    }
    
    func testSingleNameTag() {
        var helper = TagHelper()
        helper.visibilityTag = "100;==;BaconSandwich;Name"
        XCTAssertFalse(helper.isVisible)
        helper.updateVisibility(basedOn: 100, answer: "ChickenMayo")
        XCTAssertFalse(helper.isVisible)
        helper.updateVisibility(basedOn: 100, answer: "BaconSandwich")
        XCTAssertTrue(helper.isVisible)
        
        helper = TagHelper()
        helper.visibilityTag = "100;!=;ChickenMayo;Name"
        XCTAssertFalse(helper.isVisible)
        helper.updateVisibility(basedOn: 100, answer: "ChickenMayo")
        XCTAssertFalse(helper.isVisible)
        helper.updateVisibility(basedOn: 100, answer: "Avocado")
        XCTAssertTrue(helper.isVisible)
        
        helper = TagHelper()
        helper.visibilityTag = "103;@#;Apple;Name"
        XCTAssertFalse(helper.isVisible)
        helper.updateVisibility(basedOn: 103, answer: "Banana")
        XCTAssertFalse(helper.isVisible)
    }
    
    func testMultipleOrNameTags() {
        var helper = TagHelper()
        helper.visibilityTag = "100;==;BaconSandwich;Name||100;==;ChickenMayo;Name"
        XCTAssertFalse(helper.isVisible)
        helper.updateVisibility(basedOn: 100, answer: "BaconSandwich")
        XCTAssertTrue(helper.isVisible)
        
        helper = TagHelper()
        helper.visibilityTag = "100;!=;BaconSandwich;Name||100;!=;ChickenMayo;Name"
        XCTAssertFalse(helper.isVisible)
        helper.updateVisibility(basedOn: 100, answer: "ApplePie")
        XCTAssertTrue(helper.isVisible)
        helper.updateVisibility(basedOn: 100, answer: "BaconSandwich")
        XCTAssertTrue(helper.isVisible)
        helper.updateVisibility(basedOn: 100, answer: "ChickenMayo")
        XCTAssertTrue(helper.isVisible)
    }
    
    func testMultipleAndNameTags() {
        var helper = TagHelper()
        helper.visibilityTag = "100;==;BaconSandwich;Name&&100;==;ChickenMayo;Name"
        XCTAssertFalse(helper.isVisible)
        helper.updateVisibility(basedOn: 100, answer: "BaconSandwich")
        XCTAssertFalse(helper.isVisible)
        
        helper = TagHelper()
        helper.visibilityTag = "100;!=;BaconSandwich;Name&&100;!=;ChickenMayo;Name"
        XCTAssertFalse(helper.isVisible)
        helper.updateVisibility(basedOn: 100, answer: "ApplePie")
        XCTAssertTrue(helper.isVisible)
        helper.updateVisibility(basedOn: 100, answer: "BaconSandwich")
        XCTAssertFalse(helper.isVisible)
        helper.updateVisibility(basedOn: 100, answer: "ChickenMayo")
        XCTAssertFalse(helper.isVisible)
    }
    
    func testSingleNumberTag() {
        var helper = TagHelper()
        helper.visibilityTag = "100;==;125;Number"
        XCTAssertFalse(helper.isVisible)
        helper.updateVisibility(basedOn: 100, answer: "111")
        XCTAssertFalse(helper.isVisible)
        helper.updateVisibility(basedOn: 100, answer: "125")
        XCTAssertTrue(helper.isVisible)
        
        helper = TagHelper()
        helper.visibilityTag = "100;!=;125;Number"
        XCTAssertFalse(helper.isVisible)
        helper.updateVisibility(basedOn: 100, answer: "111")
        XCTAssertTrue(helper.isVisible)
        helper.updateVisibility(basedOn: 100, answer: "125")
        XCTAssertFalse(helper.isVisible)
        helper.updateVisibility(basedOn: 100, answer: "126")
        XCTAssertTrue(helper.isVisible)
        
        helper = TagHelper()
        helper.visibilityTag = "100;>;125;Number"
        XCTAssertFalse(helper.isVisible)
        helper.updateVisibility(basedOn: 100, answer: "111")
        XCTAssertFalse(helper.isVisible)
        helper.updateVisibility(basedOn: 100, answer: "125")
        XCTAssertFalse(helper.isVisible)
        helper.updateVisibility(basedOn: 100, answer: "126")
        XCTAssertTrue(helper.isVisible)
        
        helper = TagHelper()
        helper.visibilityTag = "100;<;125;Number"
        XCTAssertFalse(helper.isVisible)
        helper.updateVisibility(basedOn: 100, answer: "111")
        XCTAssertTrue(helper.isVisible)
        helper.updateVisibility(basedOn: 100, answer: "125")
        XCTAssertFalse(helper.isVisible)
        helper.updateVisibility(basedOn: 100, answer: "126")
        XCTAssertFalse(helper.isVisible)
        
        helper = TagHelper()
        helper.visibilityTag = "100;>=;125;Number"
        XCTAssertFalse(helper.isVisible)
        helper.updateVisibility(basedOn: 100, answer: "111")
        XCTAssertFalse(helper.isVisible)
        helper.updateVisibility(basedOn: 100, answer: "125")
        XCTAssertTrue(helper.isVisible)
        helper.updateVisibility(basedOn: 100, answer: "126")
        XCTAssertTrue(helper.isVisible)
        
        helper = TagHelper()
        helper.visibilityTag = "100;<=;125;Number"
        XCTAssertFalse(helper.isVisible)
        helper.updateVisibility(basedOn: 100, answer: "111")
        XCTAssertTrue(helper.isVisible)
        helper.updateVisibility(basedOn: 100, answer: "125")
        XCTAssertTrue(helper.isVisible)
        helper.updateVisibility(basedOn: 100, answer: "126")
        XCTAssertFalse(helper.isVisible)
        
        helper = TagHelper()
        helper.visibilityTag = "100;🍔;125;Number"
        XCTAssertFalse(helper.isVisible)
        helper.updateVisibility(basedOn: 100, answer: "111")
        XCTAssertFalse(helper.isVisible)
        helper.updateVisibility(basedOn: 100, answer: "125")
        XCTAssertFalse(helper.isVisible)
        helper.updateVisibility(basedOn: 100, answer: "126")
        XCTAssertFalse(helper.isVisible)
    }
    
    func testMultipleAndNumberTags() {
        var helper = TagHelper()
        helper.visibilityTag = "100;<;130;Number&&100;>;125;Number"
        XCTAssertFalse(helper.isVisible)
        helper.updateVisibility(basedOn: 100, answer: "111")
        XCTAssertFalse(helper.isVisible)
        helper.updateVisibility(basedOn: 100, answer: "125")
        XCTAssertFalse(helper.isVisible)
        helper.updateVisibility(basedOn: 100, answer: "128")
        XCTAssertTrue(helper.isVisible)
        helper.updateVisibility(basedOn: 100, answer: "150")
        XCTAssertFalse(helper.isVisible)
        
        helper = TagHelper()
        helper.visibilityTag = "100;<=;130;Number&&100;>=;125;Number"
        XCTAssertFalse(helper.isVisible)
        helper.updateVisibility(basedOn: 100, answer: "111")
        XCTAssertFalse(helper.isVisible)
        helper.updateVisibility(basedOn: 100, answer: "125")
        XCTAssertTrue(helper.isVisible)
        helper.updateVisibility(basedOn: 100, answer: "128")
        XCTAssertTrue(helper.isVisible)
        helper.updateVisibility(basedOn: 100, answer: "130")
        XCTAssertTrue(helper.isVisible)
    }
    
    func testMultipleOrNumberTags() {
        var helper = TagHelper()
        helper.visibilityTag = "100;<;130;Number||100;>;135;Number"
        XCTAssertFalse(helper.isVisible)
        helper.updateVisibility(basedOn: 100, answer: "111")
        XCTAssertTrue(helper.isVisible)
        helper.updateVisibility(basedOn: 100, answer: "125")
        XCTAssertTrue(helper.isVisible)
        helper.updateVisibility(basedOn: 100, answer: "128")
        XCTAssertTrue(helper.isVisible)
        helper.updateVisibility(basedOn: 100, answer: "150")
        XCTAssertTrue(helper.isVisible)
        helper.updateVisibility(basedOn: 100, answer: "132")
        XCTAssertFalse(helper.isVisible)
        
        helper = TagHelper()
        helper.visibilityTag = "100;>=;130;Number||100;<=;125;Number"
        XCTAssertFalse(helper.isVisible)
        helper.updateVisibility(basedOn: 100, answer: "111")
        XCTAssertTrue(helper.isVisible)
        helper.updateVisibility(basedOn: 100, answer: "125")
        XCTAssertTrue(helper.isVisible)
        helper.updateVisibility(basedOn: 100, answer: "128")
        XCTAssertFalse(helper.isVisible)
        helper.updateVisibility(basedOn: 100, answer: "150")
        XCTAssertTrue(helper.isVisible)
    }
    
    func testSingleDateTag() {
        var helper = TagHelper()
        helper.visibilityTag = "100;==;2017-08-08;Date"
        XCTAssertFalse(helper.isVisible)
        helper.updateVisibility(basedOn: 100, answer: "2017-04-04")
        XCTAssertFalse(helper.isVisible)
        helper.updateVisibility(basedOn: 100, answer: "2017-08-08")
        XCTAssertTrue(helper.isVisible)
        
        helper = TagHelper()
        helper.visibilityTag = "100;!=;2017-08-08;Date"
        XCTAssertFalse(helper.isVisible)
        helper.updateVisibility(basedOn: 100, answer: "2017-04-04")
        XCTAssertTrue(helper.isVisible)
        helper.updateVisibility(basedOn: 100, answer: "2017-08-08")
        XCTAssertFalse(helper.isVisible)
        helper.updateVisibility(basedOn: 100, answer: "2013-02-16")
        XCTAssertTrue(helper.isVisible)
        
        helper = TagHelper()
        helper.visibilityTag = "100;>=;2017-08-08;Date"
        XCTAssertFalse(helper.isVisible)
        helper.updateVisibility(basedOn: 100, answer: "2017-04-04")
        XCTAssertFalse(helper.isVisible)
        helper.updateVisibility(basedOn: 100, answer: "2017-08-08")
        XCTAssertTrue(helper.isVisible)
        helper.updateVisibility(basedOn: 100, answer: "2017-12-04")
        XCTAssertTrue(helper.isVisible)
        
        helper = TagHelper()
        helper.visibilityTag = "100;<;2017-08-08;Date"
        XCTAssertFalse(helper.isVisible)
        helper.updateVisibility(basedOn: 100, answer: "2017-04-04")
        XCTAssertTrue(helper.isVisible)
        helper.updateVisibility(basedOn: 100, answer: "2017-08-08")
        XCTAssertFalse(helper.isVisible)
        helper.updateVisibility(basedOn: 100, answer: "2017-08-09")
        XCTAssertFalse(helper.isVisible)
        
        helper = TagHelper()
        helper.visibilityTag = "100;>;2017-08-08;Date"
        XCTAssertFalse(helper.isVisible)
        helper.updateVisibility(basedOn: 100, answer: "2017-04-04")
        XCTAssertFalse(helper.isVisible)
        helper.updateVisibility(basedOn: 100, answer: "2017-08-08")
        XCTAssertFalse(helper.isVisible)
        helper.updateVisibility(basedOn: 100, answer: "2017-09-04")
        XCTAssertTrue(helper.isVisible)
        
        helper = TagHelper()
        helper.visibilityTag = "100;<=;2017-08-08;Date"
        XCTAssertFalse(helper.isVisible)
        helper.updateVisibility(basedOn: 100, answer: "2017-04-04")
        XCTAssertTrue(helper.isVisible)
        helper.updateVisibility(basedOn: 100, answer: "2017-08-08")
        XCTAssertTrue(helper.isVisible)
        helper.updateVisibility(basedOn: 100, answer: "2017-10-02")
        XCTAssertFalse(helper.isVisible)
        
        helper = TagHelper()
        helper.visibilityTag = "100;🍔;2017-08-08;Date"
        XCTAssertFalse(helper.isVisible)
        helper.updateVisibility(basedOn: 100, answer: "2017-04-04")
        XCTAssertFalse(helper.isVisible)
        helper.updateVisibility(basedOn: 100, answer: "2017-08-08")
        XCTAssertFalse(helper.isVisible)
        helper.updateVisibility(basedOn: 100, answer: "2017-11-04")
        XCTAssertFalse(helper.isVisible)
    }
    
}

fileprivate class TagHelper: VisibilityTagUpdatable {
    var isVisible: Bool = false
    var visibilityTag: String = ""
    var typeKey: Int = 0
}
