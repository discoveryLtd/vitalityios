//
//  ResourceFileDownloaderTests.swift
//  VitalityActive
//
//  Created by Simon Stewart on 3/14/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import XCTest
@testable import VIACore
@testable import VitalityKit

//vhc_en.json
//vhc_ja.json

public final class ResourceFileDownloaderTests: XCTestCase {

    var goodUserName = "wilmar@glucode.com"
    var goodPassword = "Password1"

    var expectationTimeout: Double {
        get {
            return 60.0
        }
    }

    public override func setUp() {
        self.continueAfterFailure = false
        #if !WIREMOCK
            XCTFail("Don't run unit tests against LIVE")
        #endif
    }

    public func makeExpectation(_ callingFunctionName: String) -> XCTestExpectation {
        return expectation(description: "Expectation failure for \(callingFunctionName)")
    }
    
    func testMultipleDownloads() {

        let expect = makeExpectation(#function)

        Wire.Member.login(email: goodUserName, password: goodPassword, completion: { error in

            guard error == nil else {
                XCTFail("unexpected error of type: \(String(describing: error))")
                return expect.fulfill()
            }
            // "non_smokers_declaration_en_GB.json"
            // "non_smokers_declaration_en_US.json"
            // "non_smokers_declaration_en_ZA.json"
            // "non_smokers_declaration_ja_JP.json"vhc_en.json
            let downloader = ResourceFileDownloader()
            downloader.download(fileNames: ["non_smokers_declaration_ja.json"]) { (errors) in
                XCTAssert(errors.count == 0, "Errors returned")

                return expect.fulfill()
            }
        })

        waitForExpectations(timeout: expectationTimeout) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
        }
    }
    func testSingleDownload() {

        let expect = makeExpectation(#function)

        Wire.Member.login(email: goodUserName, password: goodPassword, completion: { error in

            guard error == nil else {
                XCTFail("unexpected error of type: \(String(describing: error))")
                return expect.fulfill()
            }
            // "non_smokers_declaration_en_GB.json"
            // "non_smokers_declaration_en_US.json"
            // "non_smokers_declaration_en_ZA.json"
            // "non_smokers_declaration_ja_JP.json"
            let downloader = ResourceFileDownloader()
            downloader.download(fileNames: ["non_smokers_declaration_en.json"]) { (errors) in
                XCTAssert(errors.count == 0, "Errors returned")

                return expect.fulfill()
            }
        })

        waitForExpectations(timeout: expectationTimeout) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
        }
    }
    func testMultipleDownloadsWithOneFailure() {

        let expect = makeExpectation(#function)

        Wire.Member.login(email: goodUserName, password: goodPassword, completion: { error in

            guard error == nil else {
                XCTFail("unexpected error of type: \(String(describing: error))")
                return expect.fulfill()
            }
            // "non_smokers_declaration_en_GB.json"
            // "non_smokers_declaration_en_US.json"
            // "non_smokers_declaration_en_ZA.json"
            // "non_smokers_declaration_ja_JP.json"
            let downloader = ResourceFileDownloader()
            downloader.download(fileNames: ["does-not-exist.json", "non_smokers_declaration_en.json", "non_smokers_declaration_ja.json"]) { (errors) in
                XCTAssert(errors.count == 1, "Errors returned")

                return expect.fulfill()
            }
        })

        waitForExpectations(timeout: expectationTimeout) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
        }
    }
    func testSingleDownloadsOneFailure() {

        let expect = makeExpectation(#function)

        Wire.Member.login(email: goodUserName, password: goodPassword, completion: { error in

            guard error == nil else {
                XCTFail("unexpected error of type: \(String(describing: error))")
                return expect.fulfill()
            }
            // "non_smokers_declaration_en_GB.json"
            // "non_smokers_declaration_en_US.json"
            // "non_smokers_declaration_en_ZA.json"
            // "non_smokers_declaration_ja_JP.json"
            let downloader = ResourceFileDownloader()
            downloader.download(fileNames: ["does-not-exist"]) { (errors) in
                XCTAssert(errors.count == 1, "Errors returned")

                return expect.fulfill()
            }
        })

        waitForExpectations(timeout: expectationTimeout) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
        }
    }
    func testMultipleDownloadsWithMultipleFailures() {

        let expect = makeExpectation(#function)

        Wire.Member.login(email: goodUserName, password: goodPassword, completion: { error in

            guard error == nil else {
                XCTFail("unexpected error of type: \(String(describing: error))")
                return expect.fulfill()
            }
            // "non_smokers_declaration_en_GB.json"
            // "non_smokers_declaration_en_US.json"
            // "non_smokers_declaration_en_ZA.json"
            // "non_smokers_declaration_ja_JP.json"
            let downloader = ResourceFileDownloader()
            downloader.download(fileNames: ["does-not-exist-1.json", "non_smokers_declaration_ja.json", "does-not-exist.json"]) { (errors) in
                XCTAssert(errors.count == 2, "Errors returned")

                return expect.fulfill()
            }
        })

        waitForExpectations(timeout: expectationTimeout) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
        }
    }
}
