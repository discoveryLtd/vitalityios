//
//  NonSmokersCompleteViewModelTests.swift
//  VitalityActive
//
//  Created by Lionel Camacho on 2017/01/24.
//  Copyright © 2017 Glucode. All rights reserved.
//

import XCTest
@testable import VitalityKit
@testable import VIAUIKit

class NonSmokersCompleteViewModelTests: XCTestCase {

    override func setUp() {
        super.setUp()

    }

    override func tearDown() {
        super.tearDown()
    }

    func testNonSmokersCompleteScreenUsesCorrectHeading() {
        let viewModel = NonSmokersCompleteViewModel()
        let completeHeading = CommonStrings.Confirmation.CompletedTitle117

        XCTAssertEqual(viewModel.heading, completeHeading)
    }

    func testNonSmokersCompleteScreenUsesCorrectMessage() {
        let viewModel = NonSmokersCompleteViewModel()
        let completeMessage = NSDStrings.Confirmation.CompletedMessage118

        XCTAssertEqual(viewModel.message, completeMessage)
    }

    func testNonSmokersCompleteScreenUsesCorrectFootnote() {
        let viewModel = NonSmokersCompleteViewModel()
        let completeFootnote = CommonStrings.Confirmation.CompletedFootnotePointsMessage119

        XCTAssertEqual(viewModel.footnote, completeFootnote)
    }

    func testNonSmokersCompleteScreenUsesCorrectButtonTitle() {
        let viewModel = NonSmokersCompleteViewModel()
        let completeButtonTitle = CommonStrings.GreatButtonTitle120

         XCTAssertEqual(viewModel.buttonTitle, completeButtonTitle)
    }

    func testNonSmokersCompleteScreenUsesCorrectGradientColor() {
        let viewModel = NonSmokersCompleteViewModel()
        let completeColor = Color.green

        XCTAssertEqual(viewModel.gradientColor.hashValue, completeColor.hashValue)
    }

}
