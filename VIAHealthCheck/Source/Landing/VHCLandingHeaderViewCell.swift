import Foundation
import VIAUIKit
import VitalityKit

public class VHCLandingHeaderViewCell: UITableViewCell, Nibloadable {

    // MARK: Properties

    public var title: String? {
        get {
            return self.titleLabel.text
        }
        set {
            self.titleLabel.text = newValue
        }
    }

    public var bonusMessage: String? {
        get {
            return self.bonusMessageLabel.text
        }
        set {
            self.bonusMessageLabel.text = newValue
            self.bonusMessageLabel.isHidden = newValue == nil
        }
    }

    public var message: String? {
        get {
            return self.messageLabel.text
        }
        set {
            self.messageLabel.text = newValue
        }
    }

    public var didTapButton: () -> Void = {
        debugPrint("VHCLandingHeaderView didTapButton")
    }

    // MARK: Outlets

    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var bonusMessageLabel: UILabel!
    @IBOutlet private weak var messageLabel: UILabel!
    @IBOutlet private weak var button: BorderButton!

    // MARK: 

    public override func awakeFromNib() {
        super.awakeFromNib()

        titleLabel.text = nil
        titleLabel.numberOfLines = 2
        titleLabel.font = UIFont.title2Font()
        titleLabel.textColor = UIColor.night()

        bonusMessageLabel.text = nil
        bonusMessageLabel.isHidden = true
        bonusMessageLabel.numberOfLines = 3
        bonusMessageLabel.font = UIFont.bodyFont()
        bonusMessageLabel.textColor = UIColor.darkGrey()

        messageLabel.text = nil
        messageLabel.numberOfLines = 3
        messageLabel.font = UIFont.subheadlineFont()
        messageLabel.textColor = UIColor.darkGrey()

        button.setTitle(CommonStrings.Onboarding.Section2Title128, for: .normal)
        button.setupButton()
        button.titleLabel?.font = UIFont.bodyFont()
        button.titleLabel?.textAlignment = .center
    }

    public override func layoutSubviews() {
        super.layoutSubviews()

        self.layer.removeAllBorderLayers()
        self.layer.addBorder(edge: .top)
        self.layer.addBorder(edge: .bottom)

        button.tintColor = UIColor.currentGlobalTintColor()
    }

    @IBAction func didTapButton(_ sender: Any) {
        self.didTapButton()
    }
}
