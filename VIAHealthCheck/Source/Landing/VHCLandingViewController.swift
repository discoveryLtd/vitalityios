import UIKit
import VIAUIKit
import VitalityKit
import VIACommon
import VitalityKit
import SnapKit
import RealmSwift
import VIAUtilities
import VIAMyHealth

public final class VHCLandingViewController: VIATableViewController, VitalityAgePresentable, KnowYourHealthTintable {

    let viewModel = VHCLandingViewModel()

    override public func viewDidLoad() {
        super.viewDidLoad()

        self.title = CommonStrings.HomeCard.CardTitle125
        registerForNotifications()
        configureTableView()
        loadData()
        showOnboardingIfNeeded()
    }

    override public func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        configureAppearance()
    }

    public override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        self.showVitalityAgeIfRequired()
    }

    override public func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        self.hideBackButtonTitle()
    }

    // MARK: VitalityAgePresentable

    public func lastDateVitalityAgeShown() -> Date? {
        return AppSettings.lastVitalityAgePromptModelShownDate()
    }

    public func setLastDateVitalityAgeShown() {
        AppSettings.setHasShownVitalityAgePromptModal()
    }

    public func hasShownDateVitalityAgeShown() -> Bool {
        return AppSettings.hasShownVitalityAgePromptModal()
    }

    public func shouldShowVitalityAge() -> Bool {
        if !shownInPeriod() {
            if (AppSettings.hasCompletedVHC()) {
                if !HealthInformationRepository().hasValidVitalityAge() {
                    return true
                }
            }
        }
        return false
    }

    public func showVitalityAgeIfRequired() {
        if shouldShowVitalityAge() {
            setLastDateVitalityAgeShown()
            self.performSegue(withIdentifier: "showVitalityAgePromptModal", sender: nil)
        }
    }

    // MARK: Setup

    func registerForNotifications() {
        NotificationCenter.default.addObserver(forName: .VIAVHCCompletedNotification, object: nil, queue: .main) { [weak self] notification in
            self?.loadData(forceUpdate: true)
        }
    }

    func configureAppearance() {
        navigationController?.makeNavigationBarTransparent()
    }

    func showOnboardingIfNeeded() {
        if !AppSettings.hasShownVHCOnboarding() {
            showFirstTimeOnboarding(self)
        }
    }

    func configureTableView() {
        self.tableView.register(VHCMeasurableCell.nib(), forCellReuseIdentifier: VHCMeasurableCell.defaultReuseIdentifier)
        self.tableView.register(VIATableViewCell.nib(), forCellReuseIdentifier: VIATableViewCell.defaultReuseIdentifier)
        self.tableView.register(VIATableViewSectionHeaderFooterView.nib(), forHeaderFooterViewReuseIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier)
        self.tableView.register(VHCLandingHeaderViewCell.nib(), forCellReuseIdentifier: VHCLandingHeaderViewCell.defaultReuseIdentifier)
        self.tableView.estimatedRowHeight = 75
        self.tableView.separatorInset = UIEdgeInsets(top: 0, left: 55, bottom: 0, right: 0)

        self.tableView.refreshControl = UIRefreshControl()
        self.tableView.refreshControl?.tintColor = UIColor.cellSeperator()
        self.tableView.refreshControl?.addTarget(self, action: #selector(performRefresh), for: .valueChanged)
    }

    enum Menu: Int {
        case healthcarePdf = 0
        case history = 1
        case learnMore = 2
        case help = 3
    }

    var menuRows: NSMutableIndexSet {
        let indexSet = NSMutableIndexSet()
        indexSet.add(Menu.learnMore.rawValue)
        
        if VIAApplicableFeatures.default.showVHCHistoryMenu(){
            indexSet.add(Menu.history.rawValue)
        }
        if (VitalityProductFeature.isEnabled(.VHCTemplate)) {
            indexSet.add(Menu.healthcarePdf.rawValue)
        }
        if !VIAApplicableFeatures.default.hideHelpTab! {
            indexSet.add(Menu.help.rawValue)
        }

        return indexSet
    }

    // MARK: - UITableView DataSource + Delegate

    override public func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }

    override public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        } else if section == 1 {
            return viewModel.landingCellsGroupData.count
        } else {
            return menuRows.count
        }
    }

    override public func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if section == 0 && viewModel.landingCellsGroupData.count > 0 {
            let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier) as? VIATableViewSectionHeaderFooterView
            return view
        } else {
            return nil
        }
    }

    override public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 1 && viewModel.landingCellsGroupData.count > 0 {
            let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier) as? VIATableViewSectionHeaderFooterView
            view?.labelText = CommonStrings.LandingScreen.HealthMeasurementsTitle132
            return view
        } else {
            return nil
        }
    }

    public override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if section == 0 && viewModel.landingCellsGroupData.count > 0 {
            return 20.0
        }
        return UITableViewAutomaticDimension
    }

    public override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return CGFloat.leastNonzeroMagnitude
        }
        return UITableViewAutomaticDimension
    }

    override public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            return headerViewCell(at: indexPath)
        } else if indexPath.section == 1 {
            return healthMeasurableCell(at: indexPath)
        } else {
            return regularCell(at: indexPath)
        }
    }

    override public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let section = indexPath.section
        if section == 1 {
            measurementTapped()
        }
        if section == 2 {
            if let menuItem = Menu(rawValue: menuRows.integer(at: indexPath.row)) {
                switch menuItem {
                case .history:
                    historyTapped()
                case .learnMore:
                    learnMoreTapped()
                case .healthcarePdf:
                    healthcarePdfTapped()
                case .help:
                    helpTapped()
                }
            }
        }
    }

    func headerViewCell(at indexPath: IndexPath) -> VHCLandingHeaderViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: VHCLandingHeaderViewCell.defaultReuseIdentifier, for: indexPath) as! VHCLandingHeaderViewCell
        cell.title = CommonStrings.LandingScreen.HealthMeasurementsTitle132
        cell.message = CommonStrings.LandingScreen.HealthMeasurementMessage133
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        cell.didTapButton = { [weak self] in
            self?.captureResultsTapped()
        }
        return cell
    }

    func healthMeasurableCell(at indexPath: IndexPath) -> VHCMeasurableCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: VHCMeasurableCell.defaultReuseIdentifier, for: indexPath) as! VHCMeasurableCell
        if viewModel.landingCellsGroupData[indexPath.row].measurementValue == nil {
            let logo = viewModel.landingCellsGroupData[indexPath.row].groupIcon
            let title = viewModel.landingCellsGroupData[indexPath.row].groupName
            let messageIcon = viewModel.landingCellsGroupData[indexPath.row].messageIcon
            let message = viewModel.landingCellsGroupData[indexPath.row].message
            let feedback = viewModel.landingCellsGroupData[indexPath.row].pointsAndHealthyRangeFeedback
            let isDisclosureHidden = viewModel.landingCellsGroupData[indexPath.row].isDisclosureIndicatorHidden
            let footerMessage = viewModel.landingCellsGroupData[indexPath.row].footerMessage

            if isDisclosureHidden {
                cell.isUserInteractionEnabled = false
            } else {
                cell.isUserInteractionEnabled = true
            }

            cell.configureVHCMeasurableNoDataCell(image: logo, measurableTitle: title, messageIcon: messageIcon, message: message, feedback: feedback, footerMessage: footerMessage, isDisclosureIndicatorHidden: isDisclosureHidden)
            return cell
        } else {
            let logo = viewModel.landingCellsGroupData[indexPath.row].groupIcon
            let title = viewModel.landingCellsGroupData[indexPath.row].groupName
            let message = viewModel.landingCellsGroupData[indexPath.row].message
            let footerMessage = viewModel.landingCellsGroupData[indexPath.row].footerMessage
            let feedback = viewModel.landingCellsGroupData[indexPath.row].pointsAndHealthyRangeFeedback
            let isOutOfCurrentMembershipPeriod = viewModel.landingCellsGroupData[indexPath.row].isOutOfMembershipPeriod

            cell.isUserInteractionEnabled = true
            cell.configureVHCMeasurableMainCell(image: logo, measurableTitle: title, message: message, feedback: feedback, footerMessage: footerMessage, isOutOfCurrentMembershipPeriod: isOutOfCurrentMembershipPeriod, isDisclosureIndicatorHidden: false)
            cell.layoutIfNeeded()
            return cell
        }
    }

    func regularCell(at indexPath: IndexPath) -> VIATableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: VIATableViewCell.defaultReuseIdentifier, for: indexPath) as! VIATableViewCell

        if let menuItem = Menu(rawValue: menuRows.integer(at: indexPath.row)) {
            switch menuItem {
            case .history:
                cell.labelText = CommonStrings.HistoryButton140
                cell.cellImage = VIAHealthCheckAsset.vhcGenericHistory.templateImage
            case .learnMore:
                cell.labelText = CommonStrings.LearnMoreButtonTitle104
                cell.cellImage = VIAHealthCheckAsset.vhcGenericLearnMore.templateImage
            case .healthcarePdf:
                cell.labelText = CommonStrings.LandingScreen.HealthcarePdfLabel248
                cell.cellImage = VIAHealthCheckAsset.vhcGeneralDocsSmall.templateImage
            case .help:
                cell.labelText = CommonStrings.HelpButtonTitle141
                cell.cellImage = VIAHealthCheckAsset.vhcHelp.templateImage
            }
        }

        cell.cellImageView.tintColor = AppSettings.isAODAEnabled() ? UIColor.knowYourHealthGreen() : UIColor.currentGlobalTintColor()
        cell.accessoryType = .disclosureIndicator
        return cell
    }

    // MARK: - Navigation

    func captureResultsTapped() {
        self.performSegue(withIdentifier: "segueCaptureResults", sender: self)
    }

    func learnMoreTapped() {
        self.performSegue(withIdentifier: "segueLearnMore", sender: self)
    }

    func historyTapped() {
        self.performSegue(withIdentifier: "segueVHCHistory", sender: self)
    }

    func healthcarePdfTapped() {
        self.performSegue(withIdentifier: "segueHealthcarePdf", sender: self)
    }
    
    func helpTapped() {
        self.performSegue(withIdentifier: "showHelp", sender: self)
    }

    func measurementTapped() {
        if let indexPath = self.tableView.indexPathForSelectedRow {
            let group = viewModel.landingCellsGroupData[indexPath.row]
            if !group.isDisclosureIndicatorHidden {
                self.performSegue(withIdentifier: "segueMeasurementDetail", sender: self)
            }
        }
    }

    @IBAction func showFirstTimeOnboarding(_ sender: Any) {
        self.performSegue(withIdentifier: "showOnboardingSegue", sender: self)
    }

    @IBAction func unwindToVHCLanding(segue: UIStoryboardSegue) {
        debugPrint("unwindToVHCLanding")
    }

    public override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segueMeasurementDetail" {
            if let indexPath = self.tableView.indexPathForSelectedRow {
                let collectionView = segue.destination as? VHCMeasurementDetailCollectionViewController
                let detailDataController = VHCMeasurmentDetailDataController()
                let groupTypeId = viewModel.landingCellsGroupData[indexPath.row].groupTypeId
                collectionView?.measurablesData = detailDataController.measurableDetailDataFromRealm(for: groupTypeId)
            }
        }

        if segue.identifier == "segueHealthcarePdf", let webContentViewController = segue.destination as? VIAWebContentViewController {
            webContentViewController.showNavBar = true
            webContentViewController.title = CommonStrings.LandingScreen.HealthcarePdfLabel248
            let pdfFilename = AppConfigFeature.getFileName(type: VIAApplicableFeatures.default.getHealthCarePDFTypeKey())
            let webContenViewModel = VIAWebContentViewModel(pdfFilename: pdfFilename)
            webContentViewController.viewModel = webContenViewModel
        }
        
        if segue.identifier == "showVitalityAgePromptModal" {
            guard let navController = segue.destination as? UINavigationController else { return }
            guard let viewController = navController.topViewController as? VIAVitalityHealthModalTableViewController else { return }
            viewController.showBarButton = true
            viewController.cardIndex = 0
        }
        
        if segue.identifier == "segueLearnMore", let learnMoreViewController = segue.destination as? VHCLearnMoreViewController {
//            learnMoreViewController.partnersViewModel = partnersViewModel
        }

        if segue.identifier == "segueHistory" {
            
        }
    }

    // MARK: Networking

    func performRefresh() {
        loadData(forceUpdate: true)
        self.tableView.refreshControl?.endRefreshing()
    }

    func loadData(forceUpdate: Bool = false) {
        showHUDOnView(view: self.view)
        viewModel.fetchHealthAttributes(forceUpdate: forceUpdate) { [weak self] error, isCacheOutdated in
            self?.hideHUDFromView(view: self?.view)
            self?.tableView.refreshControl?.endRefreshing()

            guard error == nil else {
                self?.handleErrorOccurred(error!, isCacheOutdated: isCacheOutdated)
                return
            }

            self?.removeStatusView()
            self?.tableView.reloadData()
        }
    }

    func handleErrorOccurred(_ error: Error, isCacheOutdated: Bool?) {
        let backendError = error as? BackendError ?? BackendError.other
        if let outdated = isCacheOutdated, outdated == true {
            let statusView = self.handleBackendErrorWithStatusView(backendError, tryAgainAction: { [weak self] in
                self?.loadData(forceUpdate: true)
            })
            configureStatusView(statusView)
        } else {
            self.handleBackendErrorWithAlert(backendError, tryAgainAction: { [weak self] in
                self?.loadData(forceUpdate: true)
            })
        }
    }
}
