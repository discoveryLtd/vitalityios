import VitalityKit
import VitalityKit
import VIAUIKit
import RealmSwift
import VIAUtilities

public class VHCLandingViewModel: AnyObject {

    // MARK: Properties
    let dataController = VHCLandingDataController()
    var landingCellsGroupData = [VHCLandingCellData]()

    // MARK: Functions

    func getCellDataFromDataController() {
        landingCellsGroupData.removeAll()
        landingCellsGroupData = dataController.dataForGroupsFromRealm()
    }

    // MARK: Networking
    func fetchHealthAttributes(forceUpdate: Bool = false, completion: @escaping (Error?, Bool?) -> Void) {
        if VHCCache.isVHCDataOutdated() || forceUpdate == true {
            let partyId = DataProvider.newRealm().getPartyId()
            let tenantId = DataProvider.newRealm().getTenantId()
            let eventTypes = EventTypeRef.configuredTypesForVHC()
            Wire.Events.getVHCPotentialPointsEventsAndHealthyRanges(tenantId: tenantId, partyId: partyId, eventTypes: eventTypes) { (error) in
                guard error == nil else {
                    completion(error, nil)
                    return
                }
                self.getCellDataFromDataController()
                completion(nil, nil)
            }
        } else {
            getCellDataFromDataController()
            completion(nil, nil)
        }
    }

}
