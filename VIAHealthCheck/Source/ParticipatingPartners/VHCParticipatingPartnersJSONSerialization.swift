//
//  VHCParticipatingPartnersJSONSerialization.swift
//  VitalityActive
//
//  Created by Yuson, Alyosha M. on 23/04/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation

class VHCParticipatingPartnersJSONSerialization {
   static func jsonPartner() -> String {
        
        let preferredLanguage = Locale.preferredLanguages[0]
        let langStr = Locale.current.languageCode
        var fileName = ""
        var message = ""
        if preferredLanguage == "ja" || langStr == "ja" {
            fileName = "VHCParticipatingPartnersJP"
        } else {
            fileName = "VHCParticipatingPartners"
        }
    
    
        guard let pathString = Bundle(for: self).path(forResource: fileName, ofType: "json") else {
                    fatalError("VHCParticipatingPartners.json not found")
        }
        guard let jsonString = try? NSString(contentsOfFile: pathString, encoding: String.Encoding.utf8.rawValue) else {
            fatalError("Unable to VHCParticipatingPartners.json to String")
        }
        
        guard let jsonData = jsonString.data(using: String.Encoding.utf8.rawValue) else {
            fatalError("Unable to VHCParticipatingPartners.json to NSData")
        }
        
        do {
            let jsonDictionary = try JSONSerialization.jsonObject(with: jsonData, options: []) as? [String:Any]
            
            message = (jsonDictionary?["partnerMessage"] as? String)!
        } catch { debugPrint("")}
        return message
    }
    
    public static let softbankHtml = "<!DOCTYPE html><html><head><Title>Vitality SoftBank血液検査特典</title></head><body><img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAJYAAABkCAYAAABkW8nwAAAJFklEQVR4nO2afZBVZR3HP3dfLvLi7la4QGJBxPISS4A6VGYyEyihqI2mJmXkTOSMohVFKb0rWijJLFjAiliIqL0gZoP4ghhR2jhZDDCmlOTA8OImb677YtzbH7/n6R7OPffcs7t3t/3j+5m589x7znOe+zvnfM/v+f1+zwEhhBBCCCGEEEIIIYQQQgghhBBCCCGEEEIIIYQQQgghhBBCCCGEEEIIIYQQQgghhBBCCCGEEEIIIYQQQgghhEhMqliHjU9tfhEYAmS73xzOBV4DyGROMGr0WBruXkzDkp9E9b0WmAsMBTJAGVAJvA7cB/wcONwNNn4MGAA8B7Ql6D8HOB84BrwB1ADvJ3ft24AjwE7gSeClEtvbWcqBvsBbnTm4IkGf8dgN6wlO8V+qqqrZ/eorrGpcEe7TH3gWOBvYDCwFdjsbxwI3AXcDaWBRCW0bBzyIXbO3gVHAzcCyIsftAXYB3wls+x1wwn3vA3wY+CzwI+B54EvAjhLZ3RFGAGdi5zoLeyC6TViHgNM7M3gn+I//clrtINYvv4fm5uZwn+ewk/8kJqwgz2BC2w9cTOmENRDYjglqENAMbHL/lQYiXarjSff5F3AvsAT4aoG+N7gxtwP1mBfrScYDt2BCB/OknaKsJOaUmFQqRTaTYdfOvOv6CUxUD5EvqiCLgHNI9uAk4WZs6roEExXATOAoNjV64kKLctf2jemzDLjVjbO0U5Z2jfXABHKCKo/pG0uvFVZrSwtHj+Q9MPWufa3IEI8Aj2GxUCm4xLXB+Kcd8+Sz3e85wB0xY/hwotg1X+fac+nCje0iR7s6QBJh9dQ0CDatkM1mqUynqaquCu/f49orioyzDxNDp115iIGubQltbyYXg8wiJ/yu4GOvVnomYYqiy54+yQC/wC5sd55kyo1/GExYFZWVjKwbxaaNG4P9ngLexILMdVjA2xXSmOcpRitQTb6wPEOwafobXbQH4OOu3YBlu4WoBN5JOGYFgfi1J0girC90uxURZLNZKsrzzGvHYpttwFVYsDkfy7KSUg0sBCZjqX4FdgN/CjwQ6rsEE02N+/1rZ0M5Fgs1Y8HutW7/FcBEN+Zi4M8R/x9XohgPrAL+DcyL2F8GLMC8cSvQz9mzEPhtoF8KeA8wBrgGyzIBVgOTgMHAX4DvAy/E2BNkKha31mCz2IPAo4U6lyq47QZStLREOog/YjevETgLeNx9FgNbigw6yR3fDnwL+BMmtGuANZhorwz03495Be859mLCKMNEVQYcxDLE6ZhH24sJr5B3q8ZuOu74/sAwZ9uVWA1uPiauIEOAp7GSyvfcOdcADVg8eRnwG9d3KPAz4EL3+3FMfHuA24Fad9x0rLSQJPvchs0YYNntvrjOvVZYqTLIZArOBH/F6lhfAb4NXOQ+G4C7gD9EHFOJPZ0VBOplji1u33JMKDe67T927Swsm7spYtwF2E2djnmE++POC/NqF2CiSgFV5AL7ZzDBh0WFG3cs8GlO9hTjsDDiIXdeGeAAcB2W5dW7/tdjXtmzFxPiN7EHK4pgDD7XtVOdnbH0yqwQoPn4cSZOOpNUKnZxYAlwBnZxjmFTxFZMIGHmYaK6rsBYK7BsaC7wrtA+/wCeWuDYIa4dFGesYxNwqfvMxLK/84CrsZt9CCsAjwgd9wDwQ6KnnwOYOL3d77ixXsIKsF/jZFEBvOLa4TG2+tLKbdhD9lESiAp6sbDa29upra0tJiywKWcR5v5vc9u+jBUlg8xy7bMxY21w7YUxfbrKP7EpeJtrXwB+jyUjs7HC7xSsSPqBwHFrsCkwzIew6TRDfnmi2rVPRBzn+0ZpoAzzgsexh3aBs+X5QicVNUCvJYGoghzHlk2mYRdlGrkaE8D7XHssZoxXXTu2I3/cQfoX2b8Zy3b7YclCFNOwh+BFLN4By/oKxQ61Edu8sE5E7Gtzn3XYMhNYYToxSWKsZcC76ZmayjzgQDabZcCAUzly9AjZbIf/9mksU7sD81z3u+19XBuXovuMLRyD9TQPY4voE4DRwMtu+zlY0jIG87xLsThqO7awXSqOYgnFGGyq3gr8EssmDyYZIImwru+kcZ3hduBAZWUlbW1t3LtieVhYQ7HYoRirMWFNCGxrxcSVjjnulEDf/ydZLHNNkyvOXoSVFNqAj3BymaCYF+wovrwyBSsyzwFWYtnl2UkGSDIVxqaVJaYdIJ1O09TUxM4deQv8W8llbHF4z/N2YJs/jxoKU+fa3Qn+ozsZTG45yi8j+aD9dPJrT3GF1M7gHz5fVG3EanJnkcsOY+mVMVZbWxsDBw5k5Mi68K6Xga8nGGKGa4MZlA/Mz4s5bqZrN4a2Z0JtGO/546ZZf5OiYpowfpZ4FMvMRmEx0etElyL82GH7sqE26T5vY3DB/GLXNpAg++2VwspkMvTt25dBQwaHd63Eygufizl8OLDWff9uYPudri301sBsLItajxVGg/gnOO8dHofPMuICQj+9FltCmovV5t7CYkTIrUcOJj/zqwfe676/GdrnPfahiP85ELIriBfoG4FtB8ktWW2gCEmEdVqCPqWiHGw5J92nD/369Qvv90ska7B60NVYFX4cVsNai6Xzx7A4JDiNH3Z9KoAm4HJMhPXYi4GrsfT/8sAxU7B029eH7gQ+Rf5bE74ivdD97xfJlT4mY4VVX0e6EUsoGrHlm0ZsPXYLJoQGbKqbQE4Q+4B7MIHvdec9E6u9rcJeJASrVw13/S5zdoAJdXTA3hFuG9jLfH5R/wwsnvIZ9K2Yt/Tchc0ak905T6cASYL3XeQKgN3N/57mlpYWJkycxK8eeTi4fx+WpVwKfAYrkLZirjuN1bRuwZZ3ojzDY9hFXYTVhJqwgL4Gu/kNof5TsdJDI+aNPoiJ6m+c/GblDuDzwA/IrfX5dcd6rDb1hPu/KjdO0LulMG+4DKuGR9WLbsCKmvOdnfuBf7hr0YR52quw+3UQC/bXunGrsIfPZ5d12LLSSmy6m4G9ajTM2XofNr3WufP/e8COGdj7acOwFYSoGpkQQgghhBBCCCGEEEIIIYQQQgghhBBCCCGEEEIIIYQQQgghhBBCCCGEEEIIIYQQQgghhBBCCNFx/gsyof167u+hKgAAAABJRU5ErkJggg==\" alt=\"Vitality SoftBank血液検査特典\"><h1>Vitality SoftBank血液検査特典</h1><p>Vitality会員は、指定された薬局において利用することのできる血液検査（本特典）のクーポンを受取ることができます。</p></a></body></html>"
}
