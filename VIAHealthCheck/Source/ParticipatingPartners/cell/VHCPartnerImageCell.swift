//
//  VHCPartnerImageCell.swift
//  VitalityActive
//
//  Created by Yuson, Alyosha M. on 20/04/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import UIKit
import VIAUIKit

public class VHCPartnerImageCell: UITableViewCell, Nibloadable {
    
    @IBOutlet public weak var headerLabel: UILabel!
    @IBOutlet public weak var cellImageView: UIImageView!
    @IBOutlet public weak var contentLabel: UILabel!
    
    public var customHeaderFont: UIFont? {
        didSet {
            headerLabel.font = customHeaderFont
        }
    }
    
    public var cellImage: UIImage? {
        didSet {
            cellImageView.image = cellImage
        }
    }
    
    public var cellImageTintColor: UIColor? {
        didSet {
            cellImageView.tintColor = cellImageTintColor
        }
    }
    
    public var header: String = "" {
        didSet {
            headerLabel.text = header
        }
    }
    
    public var content: String = "" {
        didSet {
            contentLabel.text = content
        }
    }
    
    private func setupCell() {
        // clear IB labels
        cellImageView.image = nil
        headerLabel.text = nil
        contentLabel.text = nil
        
        // set image to hidden, incl resizing
        cellImage = nil
        
        // layout
        headerLabel.font = UIFont.headlineFont()
        headerLabel.textColor = UIColor.night()
        headerLabel.numberOfLines = 0
        
        contentLabel.numberOfLines = 0
        
        cellImageView.tintColor = UIColor.gray
    }
    
    public override func awakeFromNib() {
        super.awakeFromNib()
        setupCell()
    }
    
    public override func prepareForReuse() {
        super.prepareForReuse()
        setupCell()
    }
    
}
