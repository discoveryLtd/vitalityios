//
//  VHCParticipatingPartnersListHeaderView.swift
//  VitalityActive
//
//  Created by Yuson, Alyosha M. on 18/04/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import UIKit
import VIAUIKit

public class VHCParticipatingPartnersListHeaderView: UIView, Nibloadable {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var contentLabel: UILabel!
    
    public var title: String? {
        set {
            self.titleLabel.text = newValue
        }
        get {
            return self.titleLabel.text
        }
    }
    
    public var content: String? {
        set {
            self.contentLabel.text = newValue
        }
        get {
            return self.contentLabel.text
        }
    }
    
    public override func awakeFromNib() {
        super.awakeFromNib()
        setupView()
    }
    
    func setupView() {
        self.contentLabel.backgroundColor = .clear
        
        self.titleLabel.textColor = UIColor.night()
        self.titleLabel.font = UIFont.onboardingHeading()
        self.titleLabel.backgroundColor = .clear
        
        self.contentLabel.textColor = UIColor.tableViewSectionHeaderFooter()
        self.contentLabel.font = UIFont.subheadlineFont()
        self.contentLabel.backgroundColor = .clear
    }
}
