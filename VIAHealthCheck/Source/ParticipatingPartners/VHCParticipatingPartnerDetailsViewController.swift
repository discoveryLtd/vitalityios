//
//  VHCParticipatingPartnerDetailsViewController.swift
//  VitalityActive
//
//  Created by Yuson, Alyosha M. on 19/04/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import VIAUIKit
import VitalityKit
import VIAUtilities

public class VHCParticipatingPartnersDetailsViewController: VIATableViewController {
    
    public var detailsTitle = ""
    public var detailsDescription = ""
    public var imageFilename = ""
    
    fileprivate var partnerLogo:UIImage?    
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        self.title = CommonStrings.Ar.LearnMore.ParticipatingPartnersTitle696
        self.setupNavBar()
        self.configureAppearance()
        self.configureTableView()
    }
    
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.loadImage(imageFilename: self.imageFilename)
    }
    
    public override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.hideBackButtonTitle()
    }
    
    func setupNavBar() {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        
        self.hideBackButtonTitle()
    }
    
    func configureAppearance() {
        VIAAppearance.setGlobalTintColorToKnowYourHealth()
        navigationController?.makeNavigationBarTransparent()
        tableView.tableFooterView = UIView()
        navigationController?.navigationBar.barTintColor = UIColor.white
    }
    
    func configureTableView() {
        self.tableView.register(VHCPartnerImageCell.nib(), forCellReuseIdentifier: VHCPartnerImageCell.defaultReuseIdentifier)
        self.tableView.estimatedRowHeight = 75
        self.tableView.sectionHeaderHeight = 0
        self.tableView.sectionFooterHeight = 0
        self.tableView.estimatedSectionHeaderHeight = 0
        self.tableView.estimatedSectionFooterHeight = 0
        self.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        self.tableView.backgroundColor = .white
    }
    
    // MARK: - UITableView datasource
    
    public override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    public override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    // MARK: UITableView delegate
    
    public override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.separatorInset = UIEdgeInsetsMake(0, max(UIScreen.main.bounds.size.width, UIScreen.main.bounds.size.height), 0, 0)
    }
    
    public override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return self.configureContentCell(indexPath)
    }
    
    func configureContentCell(_ indexPath: IndexPath) -> VHCPartnerImageCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: VHCPartnerImageCell.defaultReuseIdentifier, for: indexPath) as! VHCPartnerImageCell
        cell.header = detailsTitle
        cell.content = detailsDescription
        cell.cellImage = self.partnerLogo
        return cell
    }
}

extension VHCParticipatingPartnersDetailsViewController: CMSConsumer{
    
    fileprivate func loadImage(imageFilename:String){
        self.showHUDOnView(view: self.view)
        downloadCMSImage(file: imageFilename) { [weak self] (url, error)  in
            self?.hideHUDFromView(view: self?.view)
            
            if nil != url, error == nil,
                
                let filePath = url?.path,
                
                FileManager.default.fileExists(atPath: filePath),
                
                let logo = UIImage(contentsOfFile: filePath) {
                    self?.partnerLogo = logo
                    self?.tableView.reloadData()
                }
        }
    }
}
