//
//  VHCParticipatingPartnersDataController.swift
//  VitalityActive
//
//  Created by OJ Garde on 5/4/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import VitalityKit

import RealmSwift

public class VHCParticipatingPartnersDataController{
    public var partners: [VHCParticipatingPartnersViewModel]?
    
    public func loadPartners(_ completion: (() -> Void)?){
        getPartnersByCategory{ [weak self] in
            
            let realm = DataProvider.newPartnerRealm()
            let results = realm.allPartners()
            
            self?.partners = self?.toModelArray(src: results)
            
            completion?()
        }
    }
}

extension VHCParticipatingPartnersDataController{
    fileprivate func getPartnersByCategory(_ completion: (() -> Void)?){
        let realm           = DataProvider.newRealm()
        let tenantId        = realm.getTenantId()
        let partyId         = realm.getPartyId()
        let membershipId    = realm.getMembershipId()
        let effectiveDate   =  Date()
        let typeKey         = 4        
        
        let params = GetPartnersByCategoryParameters(effectiveDate: effectiveDate, productFeatureCategoryTypeKey: typeKey)
        
        Wire.Member.getPartnersByCategory(tenantId: tenantId, partyId: partyId, request: params) { (error, response) in
            completion?()
        }
    }
}

extension VHCParticipatingPartnersDataController{
    
    fileprivate func toModelArray(src: Results<Partner>) -> [VHCParticipatingPartnersViewModel]{
        var dest = [VHCParticipatingPartnersViewModel]()
        for item in src{
            dest.append(toModel(src: item))
        }
        
        return dest
    }
    
    fileprivate func toModel(src: Partner) -> VHCParticipatingPartnersViewModel{
        let dest = VHCParticipatingPartnersViewModel()
        dest.shortDescription   = src.shortDescription
        dest.logoFileName       = src.logoFileName
        dest.longDescription    = src.longDescription
        dest.name               = src.name
        
        return dest
    }
}
