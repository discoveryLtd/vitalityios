//
//  VHCParticipatingPartnersViewController.swift
//  VitalityActive
//
//  Created by Yuson, Alyosha M. on 18/04/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation

import VIAUIKit
import VitalityKit
import VIAUtilities
import VIACommon
import VIAPartners


class VHCParticipatingPartnersViewController: VIATableViewController {
    
    var viewModel: PartnersListTableViewModel?
    private var selectedPartner: VHCParticipatingPartnersViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if viewModel == nil {
            setupModel()
        }
        
        self.title = CommonStrings.Ar.LearnMore.ParticipatingPartnersTitle696
        setupNavBar()
        configureAppearance()
        setupTableViewHeader()
        configureTableView()
        
        loadPartners()
    }
    
    override public func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.hideBackButtonTitle()
    }
    
    func configureAppearance() {
        VIAAppearance.setGlobalTintColorToKnowYourHealth()
        navigationController?.makeNavigationBarTransparent()
        navigationController?.navigationBar.barTintColor = UIColor.white
    }
    
    func setupNavBar() {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationItem.setHidesBackButton(true, animated:true);

        self.addRightBarButtonItem(CommonStrings.GenericCloseButtonTitle10, target: self, selector: #selector(back(_:)))
    }
    
    func setupTableViewHeader() {
        let header = VHCParticipatingPartnersListHeaderView.viewFromNib(owner: self)!
        header.contentLabel.lineBreakMode = .byWordWrapping
        header.title = CommonStrings.Partner.VisitPartnerTitle243
        header.content = CommonStrings.Partner.VisitPartnerMessage244
        self.tableView.tableHeaderView = header
        self.tableView.tableHeaderView?.backgroundColor = UIColor.tableViewBackground()
        self.tableView.sizeHeaderViewToFit()
    }
    
    func configureTableView() {
        self.tableView.register(PartnersListHeaderView.nib(), forHeaderFooterViewReuseIdentifier: PartnersListHeaderView.defaultReuseIdentifier)
        self.tableView.register(VIATableViewSectionHeaderFooterView.nib(), forHeaderFooterViewReuseIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier)
        self.tableView.register(PartnersListPartnerCell.nib(), forCellReuseIdentifier: PartnersListPartnerCell.defaultReuseIdentifier)
        self.tableView.register(VIATableViewCell.nib(), forCellReuseIdentifier: VIATableViewCell.defaultReuseIdentifier)
        self.tableView.estimatedRowHeight = 75
        self.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        self.tableView.separatorInset = UIEdgeInsets(top: 0, left: 80, bottom: 0, right: 0)
        self.tableView.backgroundColor = UIColor.tableViewBackground()
        self.tableView.tableFooterView = UIView()
    }
    
    public override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "seguePartnerDetails" {
            guard let indexPath = tableView.indexPathForSelectedRow else { return }
            guard let partnerDetails = viewModel?.partnerDetails(at: indexPath) else { return }
            guard let detailVC = segue.destination as? PartnersDetailTableViewController else { return }
            let detailViewModel = PartnersDetailTableViewModel(for: partnerDetails)
            detailVC.viewModel = detailViewModel
        }
    }
    
    func back(_ sender: Any?) {
        self.navigationController?.popViewController(animated: true)
    }
}

/* For Loading and Storing of Data/s */
extension VHCParticipatingPartnersViewController {
    
    /* API Call */
    func loadPartners() {
        showFullScreenHUD()
        viewModel?.requestPartnersByCategory(completion: { [weak self] (error) in
            self?.hideFullScreenHUD()
            
            guard error == nil else {
                self?.handleError(error)
                return
            }
            self?.viewModel?.configurePartnerData()
            self?.tableView.reloadData()
        })
    }
    
    func handleError(_ error: Error?) {
        let statusView = handleBackendErrorWithStatusView((error as? BackendError) ?? .other, tryAgainAction: { [weak self] in
            self?.loadPartners()
        })
        configureStatusView(statusView)
    }
    
    /* */
    func setupModel() {
        // Currenlty naviely defaults to reward partners
        let type = ProductFeatureCategoryRef.HealthPartners
        self.viewModel = PartnersListTableViewModel(withPartnerType: type)
    }
}

extension VHCParticipatingPartnersViewController {
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel?.groupCount() ?? 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel?.partnerCount(in: section) ?? 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let tableViewCell = self.tableView.dequeueReusableCell(withIdentifier: PartnersListPartnerCell.defaultReuseIdentifier, for: indexPath) as? PartnersListPartnerCell else { return UITableViewCell() }
        
        guard let partnerDetails = viewModel?.partnerDetails(at: indexPath) else { return UITableViewCell() }
        
//        tableViewCell.title = partnerDetails.name
        tableViewCell.title = nil // Set to nil to hide the label
        tableViewCell.content = partnerDetails.description
        
        if partnerDetails.logoFileName == "none" {
            tableViewCell.partnerImage = nil
        } else {
            DispatchQueue.global(qos: .background).async { [weak self] in
                self?.viewModel?.partnerLogo(partner: partnerDetails, indexPath: indexPath) { logo, partner, indexPath in
                    DispatchQueue.main.async {
                        if partnerDetails.name == partner.name && tableViewCell.content == partner.description {
                            tableViewCell.partnerImage = logo
                        }
                    }
                }
            }
        }
        
        tableViewCell.accessoryType = .disclosureIndicator
        return tableViewCell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "seguePartnerDetails", sender: self)
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 0 {
            let cell =  tableView.dequeueReusableCell(withIdentifier: VIATableViewCell.defaultReuseIdentifier) as! VIATableViewCell
            cell.labelText = self.viewModel?.sectionTitle(for: section)
            cell.cellImageView.isHidden = true
            cell.label.textColor = UIColor.night()
            cell.label.font = UIFont.title2Font()
            cell.label.backgroundColor = .clear
            return cell
        }
        return UIView()
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if section == 0 {
            return 0
        }
        return UITableViewAutomaticDimension
    }
}
