//
//  VHCParticipatingPartnersViewModel.swift
//  VitalityActive
//
//  Created by OJ Garde on 5/4/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import VitalityKit

import RealmSwift

public class VHCParticipatingPartnersViewModel{
    public var shortDescription: String = ""
    public var logoFileName: String = ""
    public var longDescription: String = ""
    public var name: String = ""
}
