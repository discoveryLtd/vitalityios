//
//  VHCCaptureSummaryViewController.swift
//  VitalityActive
//
//  Created by Wilmar van Heerden on 2017/04/18.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import VIAUIKit
import VIAUtilities
import VitalityKit

public protocol CapturedResultsSummaryViewModel {
    func setup(with delegate: VHCCaptureSummaryViewModelDelegate)
    func numberOfCapturedItemsGroups() -> Int
    func itemCount(in group: Int) -> Int
    func sectionIsLast(section: Int) -> Bool
    func nameForItem(in group: Int) -> String?
    func headerForItem(in group: Int, with index: Int) -> String?
    func submittedImages() -> Array<UIImage>
    func item(number: Int, in group: Int) -> VHCCapturedItem?
    func submitEvents(completion: @escaping (_ error: Error?) -> Void)
    func summaryViewTitle() -> String
    func summaryHeaderTitle() -> String
    func summaryHeaderMessage() -> String
}

public class VHCCaptureSummaryViewController: VIATableViewController, VHCCaptureSummaryViewModelDelegate, KnowYourHealthTintable {
    // MARK: - Variables 
    internal var viewModel: CapturedResultsSummaryViewModel?
    internal var heightForCell: CGFloat = 0.0

    public override func viewDidLoad() {
        super.viewDidLoad()

        self.viewModel = VHCCaptureSummaryViewModel()
        self.viewModel?.setup(with: self)
        self.title = self.viewModel?.summaryViewTitle()
        self.setupTableView()

        self.addBarButtonItems()
    }

    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        navigationController?.navigationBar.isHidden = false
        navigationController?.makeNavigationBarTransparent()
    }

    func setupTableView() {
        self.registerReuasbleView()
        self.tableView.estimatedRowHeight = 44
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.setupTableViewHeader()
    }

    func registerReuasbleView() {
        self.tableView.register(ImageCollectionTableViewCell.nib(), forCellReuseIdentifier: ImageCollectionTableViewCell.defaultReuseIdentifier)
        self.tableView.register(HeaderValueSubtitleTableViewCell.nib(), forCellReuseIdentifier: HeaderValueSubtitleTableViewCell.defaultReuseIdentifier)
        self.tableView.register(VIATableViewSectionHeaderFooterView.nib(), forHeaderFooterViewReuseIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier)
    }

    func setupTableViewHeader() {
        let header = TitleMessageHeaderFooterView.viewFromNib(owner: self) as! TitleMessageHeaderFooterView
        header.title = self.viewModel?.summaryHeaderTitle()
        header.content = self.viewModel?.summaryHeaderMessage()
        self.tableView.tableHeaderView = header
        self.tableView.sizeHeaderViewToFit()
    }

   // MARK: - Delegate
    public override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if let header = self.tableView.dequeueReusableHeaderFooterView(withIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier) as? VIATableViewSectionHeaderFooterView {
            self.setup(header: header, for: section)
            return header
        }
        return nil
    }

    func setup(header: VIATableViewSectionHeaderFooterView, for group: Int) {
        header.labelText = ""
        header.set(font: UIFont.subheadlineFont())
        header.set(textColor: UIColor.night())
        if let headerTitle = self.viewModel?.nameForItem(in: group) {
            header.labelText = headerTitle
        }
    }

    public override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if ((self.viewModel?.sectionIsLast(section: indexPath.section) ?? false)) {
           return self.heightForCell
        }
        return UITableViewAutomaticDimension
    }

    public override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: false)
    }

   // MARK: - Datasource
    public override func numberOfSections(in tableView: UITableView) -> Int {
        return self.viewModel?.numberOfCapturedItemsGroups() ?? 0
    }

    public override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel?.itemCount(in: section) ?? 0
    }

    public override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (!(self.viewModel?.sectionIsLast(section: indexPath.section) ?? true)) {
            if let resultsCell = tableView.dequeueReusableCell(withIdentifier: HeaderValueSubtitleTableViewCell.defaultReuseIdentifier) {
                let collectionViewCell = self.setUpCapturedResults(cell: resultsCell, at: indexPath)
                return collectionViewCell
            }
        } else {
            if let imageCollectionCell = self.tableView.dequeueReusableCell(withIdentifier: ImageCollectionTableViewCell.defaultReuseIdentifier) {
                let collectionviewCell = self.setUpCaptureImages(cell: imageCollectionCell)
                self.heightForImageCollection(cell: collectionviewCell)
                return collectionviewCell
            }
        }

        return UITableViewCell()
    }


    func setUpCapturedResults(cell: UITableViewCell, at index: IndexPath) -> UITableViewCell {
        if let capturedResultsCell = cell as? HeaderValueSubtitleTableViewCell, let capturedItem = self.viewModel?.item(number: index.row, in: index.section) {
            capturedResultsCell.selectionStyle = .none
            let title = self.viewModel?.headerForItem(in: index.section, with: index.row) ?? ""
            capturedResultsCell.setCell(title: title)
            capturedResultsCell.setCell(subTitle: CommonStrings.SummaryScreen.DateTestedTitle185(capturedItem.date))
            capturedResultsCell.setCell(value: capturedItem.value)
            return capturedResultsCell
        }
        return UITableViewCell()
    }

    func setUpCaptureImages(cell: UITableViewCell) -> ImageCollectionTableViewCell {
        if let imageCollectionViewCell = cell as? ImageCollectionTableViewCell {
            if let capturedImages = self.viewModel?.submittedImages() {
                imageCollectionViewCell.setImageCollection(images: capturedImages)
            }
        imageCollectionViewCell.imageCollectionView?.bounces = false
        imageCollectionViewCell.viewWidth = self.view.frame.width
        imageCollectionViewCell.setupCollectionFlowLayoutSize()
            return imageCollectionViewCell
        }
        return ImageCollectionTableViewCell()
    }

    func heightForImageCollection(cell: ImageCollectionTableViewCell) {
        let collectionViewFlowLayout = cell.imageCollectionView?.collectionViewLayout
        let cellPadding = collectionViewFlowLayout?.collectionViewContentSize
        let height = cellPadding?.height
        self.heightForCell = height ?? 0
        cell.updateImageCollectionViewSize(with:height)
    }

   // MARK: - Navigation

    func addBarButtonItems() {
        let next = UIBarButtonItem(title: CommonStrings.ConfirmTitleButton182, style: .plain, target: self, action: #selector(next(_:)))
        self.navigationItem.rightBarButtonItem = next
    }

    func next(_ sender: Any) {
        if VitalityProductFeature.isEnabled(.VHCDSConsent) {
            self.performSegue(withIdentifier: "showDataSharingConsent", sender: nil)
        } else {
            submitVHC()
        }
    }

    func submitVHC() {
        self.viewModel?.submitEvents(completion: { [weak self] error in
            if error == nil {
                self?.performSegue(withIdentifier: "showCompletion", sender: nil)
            } else {
                debugPrint("Error")
            }
        })
    }

    public func displayVHCSubmissionFailure(completion: @escaping (_ resubmit: Bool) -> Void) {
        let cancelAction = UIAlertAction(title: CommonStrings.CancelButtonTitle24, style: UIAlertActionStyle.cancel) { (alertAction) in
            completion(false)
        }
        let tryAgain = UIAlertAction(title: CommonStrings.TryAgainButtonTitle43, style: UIAlertActionStyle.default) { (alertAction) in
            completion(true)
        }

        let failedToSubmitVHCAlert = UIAlertController(title: CommonStrings.PrivacyPolicy.UnableToCompleteAlertTitle115, message: CommonStrings.SummaryScreen.VhcCompleteErrorMessage187, preferredStyle: .alert)

        failedToSubmitVHCAlert.addAction(cancelAction)
        failedToSubmitVHCAlert.addAction(tryAgain)
        self.present(failedToSubmitVHCAlert, animated: true, completion: nil)
    }

    @IBAction public func unwindFromVHCDataSharingConsentToSummary(segue: UIStoryboardSegue) {
        debugPrint("unwindFromVHCDataSharingConsentToSummary")
    }
}
