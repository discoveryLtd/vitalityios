//
//  VHCCaptureSummaryViewModel.swift
//  VitalityActive
//
//  Created by Marius Janse van Vuuren on 4/24/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

import UIKit
import VIAUIKit
import VitalityKit
import VIAUtilities
import VIACommon
import VIAImageManager

public struct VHCCapturedItem {
    public let name: String
    public let value: String
    public let date: String
    public let headerTitle: String
}

public protocol VHCCaptureSummaryViewModelDelegate: class {
    func displayVHCSubmissionFailure(completion: @escaping (_ resubmit: Bool) -> Void)
}

public class VHCCaptureSummaryViewModel: NSObject, CapturedResultsSummaryViewModel, PhotoUploadDelegate {

    // MARK: Variables

    let networking = VHCSubmissionHelper()
    internal weak var delegate: VHCCaptureSummaryViewModelDelegate?
    internal var vhcCapturedDataGroups: Array<Array<VHCCapturedResult>> = Array<Array<VHCCapturedResult>>()
    internal var vhcCapturedData: Array<VHCCapturedResult> = Array<VHCCapturedResult>()
    internal var vhcCapturedImages: Array<PhotoAsset>?
    internal let vhcRealm = DataProvider.newVHCRealm()

    //  This variable is used for foot-inch unit
    internal let footToInch: Double = 12 // 1 Foot = 12 inch
    
    //  This variable is used for stone-pound unit
    internal let stoneToPound: Double = 14 // 1 Stone = 14 pounds
    
    public func setup(with delegate: VHCCaptureSummaryViewModelDelegate) {
        self.delegate = delegate
        self.networking.setupDelegate(self)
        self.retrieveCapturedVHCData()
        self.setupCapturedItemsGroup()
    }

    internal func setupCapturedItemsGroup() {
        let groups = Array(VHCHealthAttributeGroup.sortedGroups(from: vhcRealm))
        for group: VHCHealthAttributeGroup in groups {
            let healthAtributes = group.healthAttributes
            var dataForGroup = Array<VHCCapturedResult>()
            for healthAttribute in healthAtributes {
                var filteredDataWithGroup = Array<VHCCapturedResult>()
                if(VIAApplicableFeatures.default.removeEmptyCapturedData)!{
                    filteredDataWithGroup = vhcCapturedData.filter {
                        $0.healthAttributeType == healthAttribute.type && $0.isValid == true
                    }
                }else{
                    filteredDataWithGroup = vhcCapturedData.filter {
                        $0.healthAttributeType == healthAttribute.type
                    }
                }
                if (filteredDataWithGroup.count > 0) {
                    if let filteredData = filteredDataWithGroup.first, filteredData.isValid {
                        dataForGroup.append(contentsOf: filteredDataWithGroup)
                    }
                }
            }
            if (dataForGroup.count > 0) {
                vhcCapturedDataGroups.append(dataForGroup)
            }
        }
    }

    public func numberOfCapturedItemsGroups() -> Int {
        let capturedImagesGroupCount = 1
        return vhcCapturedDataGroups.count + capturedImagesGroupCount
    }

    public func itemCount(in group: Int) -> Int {
        if self.sectionIsLast(section: group) {
            let capturedImagesCellCount = 1
            return capturedImagesCellCount
        }
        return vhcCapturedDataGroups[group].count
    }

    public func item(number: Int, in group: Int) -> VHCCapturedItem? {
        let capturedResult = vhcCapturedDataGroups[group][number]
        var value = capturedResult.rawInput ?? CommonStrings.GenericNotAvailable270

        if let measurement = capturedResult.measurement() {
            if capturedResult.healthAttributeType == .BloodPressureDiasto || capturedResult.healthAttributeType == .BloodPressureSystol {
                let measurementIntegerFormatter = Localization.integerStyle
                measurementIntegerFormatter.numberFormatter.roundingMode = .down
                value = measurementIntegerFormatter.string(from: measurement)
            }
            else if(capturedResult.healthAttributeType == .Height && capturedResult.unitOfMeasureType == .FootInch){
                
                // Revert to 'foot' and 'inch'
                let feet = (modf(Localization.decimalFormatter.number(from: capturedResult.rawInput!)?.doubleValue ?? 0).0)
                let inch = ((modf(Localization.decimalFormatter.number(from: capturedResult.rawInput!)?.doubleValue ?? 0).1) * self.footToInch) // Convert foot to inch 1 Foot = 12 inches
                
                value = CommonStrings.FootInchSummary2231(String(Int(round(feet))), String(Int(round(inch))))
            }
            else if(capturedResult.healthAttributeType == .Weight && capturedResult.unitOfMeasureType == .StonePound){
                
                // Revert to 'stone' and 'pounds'
                let stone = (modf(Localization.decimalFormatter.number(from: capturedResult.rawInput!)?.doubleValue ?? 0).0)
                let pound = ((modf(Localization.decimalFormatter.number(from: capturedResult.rawInput!)?.doubleValue ?? 0).1) * self.stoneToPound) // Convert stone to pound 1 Stone = 14 Pounds
                
                value = CommonStrings.StonePoundSummary2402(String(Int(round(stone))), String(Int(round(pound))))
            }
            else {
                let measurementDecimalFormatter = Localization.decimalShortStyle
                value = measurementDecimalFormatter.string(from: measurement)
            }
        }
        let healthAttribute = vhcRealm.vhcHealthAttribute(of: capturedResult.healthAttributeType)
        let dateString = Localization.dayDateShortMonthyearFormatter.string(from:  capturedResult.dateCaptured)
        let item = VHCCapturedItem(name: healthAttribute?.title() ?? "",
                                        value: value,
                                        date: dateString,
                                        headerTitle: healthAttribute?.inputHeading() ?? "")
        return item
    }

    public func submitEvents(completion: @escaping (_ error: Error?) -> Void) {
        networking.processEvents(completion: completion)
    }

    public func submittedImages() -> Array<UIImage> {
        var images: Array<UIImage> = Array<UIImage>()
        if let realmImages = self.vhcCapturedImages {
            for realmImage in realmImages {
                if let assetURL = realmImage.assetURL {
                    if let asset = PhotoAssetHelper.PhotosAssetForFileURL(url: assetURL) {
                        images.append(PhotoAssetHelper.getAssetThumbnail(asset: asset))
                    }
                } else {
                    if let imageData = realmImage.assetData {
                        if let assetImage = UIImage(data: imageData) {
                            /*
                             Assets saved to realm as data using the UIImagePNGRepresentation method
                             If you save a UIImage as a JPEG, it will set the rotation flag.
                             PNGs do not support a rotation flag, so if you save a UIImage as a PNG,
                             it will be rotated incorrectly and not have a flag set to fix it.
                             So if you want PNGs you must rotate them yourself.
                             */
                            if let cgAsset = assetImage.cgImage {
                                let uprightImage = UIImage(cgImage: cgAsset, scale: 1, orientation: UIImageOrientation.right)
                                images.append(uprightImage)
                            } else {
                                images.append(assetImage)
                            }
                        }
                    }
                }
            }
        }
        return images
    }

    public func summaryViewTitle() -> String {
        return CommonStrings.SummaryScreen.SummaryTitle181
    }

    public func summaryHeaderTitle() -> String {
        return CommonStrings.SummaryScreen.ConfirmMeasurementsTitle183
    }

    public func summaryHeaderMessage() -> String {
        return CommonStrings.SummaryScreen.ConfirmMeasurementsMessage184
    }

    public func sectionIsLast(section: Int) -> Bool {
        return section == (self.numberOfCapturedItemsGroups() - 1)
    }

    public func nameForItem(in group: Int) -> String? {
        if self.vhcCapturedDataGroups.count > group {
            if let capturedResult = self.vhcCapturedDataGroups[group].first {
                let healthGroup = vhcRealm.vhcHealthAttributeGroup(from: capturedResult.healthAttributeType)
                return healthGroup?.groupName()
            }
        } else if (self.vhcCapturedDataGroups.count == group) {
            return CommonStrings.SummaryScreen.UploadedProofTitle186
        }
        return  nil
    }

    public func headerForItem(in group: Int, with index: Int) -> String? {
        return self.item(number: index, in: group)?.headerTitle
    }

    internal func retrieveCapturedVHCData() {
        self.vhcCapturedData = Array(vhcRealm.objects(VHCCapturedResult.self))
        if let images = vhcRealm.allPhotos() {
            self.vhcCapturedImages = images
        }
    }

    //Mark: photoUploadDelegate
    public func displayImageUploadErrorMessage(with completion: @escaping (_ resubmit: Bool) -> Void) {
        self.delegate?.displayVHCSubmissionFailure(completion: { resubmit in
            completion(resubmit)
        })
    }
}
