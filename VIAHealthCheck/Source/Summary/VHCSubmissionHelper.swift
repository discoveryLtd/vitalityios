import UIKit
import Foundation
import VIAUtilities
import VitalityKit
import Photos
import VIAImageManager

public protocol PhotoUploadDelegate: class {
    func displayImageUploadErrorMessage(with completion: @escaping (_ resubmit: Bool) -> Void)
}

public protocol EventsProcessor {
    func processEvents(completion: @escaping (_ error: Error?) -> Void)
}

public class VHCSubmissionHelper: EventsProcessor {
    //  This variable is used for foot-inch unit
    public let footToInch: Double = 12 // 1 Foot = 12 inch
    
    //  This variable is used for stone-pound unit
    public let stoneToPound: Double = 14 // 1 Stone = 14 pounds
    
    // For submission of 'ft in' 'st lb' values
    internal let footLabel = "F"
    internal let inchLabel = "INCH"
    
    internal let stoneLabel = "ST"
    internal let poundLabel = "LB"
    
    internal weak var photoUploadDelegate: PhotoUploadDelegate?
    internal let vhcRealm = DataProvider.newVHCRealm()
    internal let photoUploader = PhotoUploader()
    private var completion: ((Error?) -> (Void)) = { error in
        debugPrint("Complete")
    }

    public func setupDelegate(_ delegate: PhotoUploadDelegate) {
        photoUploadDelegate = delegate
    }

    public func processEvents(completion: @escaping (_ error: Error?) -> Void) {
        self.completion = completion
        submitImagesRetreiveReferences()
    }

    internal func submitImagesRetreiveReferences() {
        guard let realmCapturedAssets = vhcRealm.allPhotos() else {
            debugPrint("No images to upload")
            return
        }

        photoUploader.delegate = self
        photoUploader.submitCapturedVHCImages(realmCapturedAssets)
    }

    internal func submitEvents(with associateEventReferences: Array<String>?) {
        let coreRealm = DataProvider.newRealm()
        let tenantID = coreRealm.getTenantId()
        let partyID = coreRealm.getPartyId()
        let submissionDate = Date()
        var associatedEvents = [ProcessEventsAssociatedEvent]()

        for capturedResult in vhcRealm.allVHCCapturedResults() {
            let metadataKey = EventMetaDataTypeRef.Value

            // default to submitting the rawInput (this helps with submitting 
            // health attributes like Urinary protein. otherwise, of there is 
            // a double value available, override the rawInput with that.
            var eventMetaDataValue = capturedResult.rawInput ?? ""
            if let doubleValue = capturedResult.value.value {
                eventMetaDataValue = String(describing: doubleValue)
            }
            //Treat BMI with footinch differently
            if capturedResult.unitOfMeasureType == .FootInch{
                // Revert to 'foot' and 'inch'
                let feet = (modf(Double(eventMetaDataValue) ?? 0).0)
                let inch = ((modf(Double(eventMetaDataValue) ?? 0).1) * self.footToInch) // Convert foot to inch 1 Foot = 12 inches
                
                let inchValue = String(format: "%.1f", (round(inch)))
                eventMetaDataValue = "\(Int(round(feet))) \(footLabel) \(inchValue) \(inchLabel)"
                
            }
            else if capturedResult.unitOfMeasureType == .StonePound{
                // Revert to 'stone' and 'pounds'
                let stone = (modf(Double(eventMetaDataValue) ?? 0).0)
                let pound = ((modf(Double(eventMetaDataValue) ?? 0).1) * self.stoneToPound) // Convert stone to pound 1 Stone = 14 Pounds
                
                let poundValue = String(format: "%.1f", (round(pound)))
                eventMetaDataValue = "\(Int(round(stone))) \(stoneLabel) \(poundValue) \(poundLabel)"
            }
            let associatedEvent = ProcessEventsAssociatedEvent(eventOccuredOn: capturedResult.dateCaptured, eventCapturedOn: submissionDate,
                                                               eventMetaDataTypeKey: metadataKey, eventMetaDataUnitOfMeasure:capturedResult.unitOfMeasureType,
                                                               eventMetaDataValue: eventMetaDataValue,
                                                               associatedEventTypeKey: .DocumentaryProof,
                                                               eventSourceTypeKey: .MobileApp,
                                                               partyId: partyID,
                                                               reportedBy: partyID,
                                                               eventId: nil,
                                                               typeKey: capturedResult.eventType())
            /** FC-22541: Generali
             * This prevents the unanswered questions from being included on the request payload.
             */
            if let tenantID = AppSettings.getAppTenant(), tenantID == .GI {
                if eventMetaDataValue != "" {
                    associatedEvents.append(associatedEvent)
                }
            } else {
                associatedEvents.append(associatedEvent)
            }
        }

        let processEvent = ProcessEventsEvent(date: submissionDate,
                                              eventTypeKey: .DocumentUploadsVHC,
                                              eventSourceTypeKey: .MobileApp,
                                              partyId: partyID,
                                              reportedBy: partyID,
                                              associatedEvents: associatedEvents,
                                              eventMetaDataTypeKey: EventMetaDataTypeRef.DocumentReference,
                                              eventMetaDataUnitOfMeasure: nil,
                                              eventMetaDataValues: associateEventReferences)

        var events = [ProcessEventsEvent]()
        events.append(processEvent)

        Wire.Events.processEvents(tenantId: tenantID, events: events) { [weak self] (response, error) in
            if let error = error {
                self?.completion(error)
                return
            } else {
                AppSettings.setHasCompletedVHC()
                Wire.MyHealth.shared().healthInformationFor(isSummary: false, tenantId: tenantID, partyId: partyID) { (response, error) in
                    /*This was intentional I am afraid and I know that it is not pretty. But the scenario goes something like this:
                     If you have never done a VHC (or VHR) and then get health attributes the backend returns with an error (400 No Attribute with that type), but any other time that you do a get health attribute call it will return with information. In the case of it returning with an error, we are essentially can assume that no health attributes exists for this person.
                     Ideally the backend should simply return an empty array instead of the 400 - but this is not a trivial change as a number of other calls rely on this.*/

                    self?.completion(nil)
                }
            }
        }
    }
}

extension VHCSubmissionHelper: PhotoUploaderDelegate{
    
    public func imagesUploaded(references: Array<String>) {
        submitEvents(with: references)
    }
    
    public func onImageUploadFailed(){
        self.photoUploadDelegate?.displayImageUploadErrorMessage(with: { [weak self] tryAgain in
            if tryAgain {
                self?.photoUploader.reSubmitFailedImages()
            }
        })
    }
}
