////
////  PhotoUploader.swift
////  VitalityActive
////
////  Created by Marius Janse van Vuuren on 5/5/17.
////  Copyright © 2017 Glucode. All rights reserved.
////
//
//import UIKit
//import Photos
//import RealmSwift
//import VitalityKit
//import VIAImageManager
//
//public protocol PhotoUploaderDelegate: class {
//    func imagesUploaded(references: Array<String>)
//    func onImageUploadFailed()
//}
//
//public class PhotoUploader: NSObject {
//    public var imagesUnableToUpload = Array<UploadableImage>()
//    public var imagesToBeUploaded = Array<UploadableImage>()
//    public weak var delegate: PhotoUploaderDelegate?
//    /* Moved this here to avoid the refresh state when tapping "Try Again" button when user encountered Timeout */
//    internal var imageReferences = Array<String>()
//
//    public func submitCapturedVHCImages(_ assets: Array<PhotoAsset>) {
//        var uploadableImages = Array<UploadableImage>()
//        for asset in assets {
//            if let imageData = asset.assetData {
//                let image = UploadableImage()
//                image.setup(with: imageData)
//                uploadableImages.append(image)
//            } else {
//                if let imageURL = asset.assetURL {
//                    let image = UploadableImage()
//                    image.setup(assetURLString: imageURL)
//                    uploadableImages.append(image)
//                }
//            }
//        }
//        self.submitUploadableImages(images: uploadableImages)
//    }
//
//    public func getAllUploadableImages(_ assets: Array<PhotoAsset>) {
//        var uploadableImages = Array<UploadableImage>()
//        for asset in assets {
//            if let imageData = asset.assetData {
//                let image = UploadableImage()
//                image.setup(with: imageData)
//                uploadableImages.append(image)
//            } else {
//                if let imageURL = asset.assetURL {
//                    let image = UploadableImage()
//                    image.setup(assetURLString: imageURL)
//                    uploadableImages.append(image)
//                }
//            }
//        }
//        self.imagesToBeUploaded = uploadableImages
//    }
//
//    public func allImagesSuccesfullyUploaded() -> Bool {
//        return self.imagesUnableToUpload.count == 0
//    }
//
//    public func reSubmitFailedImages() {
//        self.submitUploadableImages(images: self.imagesUnableToUpload)
//    }
//
//    public func submitUploadableImages(images: Array<UploadableImage>) {
//        DispatchQueue.global(qos: .userInitiated).async {
//            let group = DispatchGroup()
//            let coreRealm = DataProvider.newRealm()
//            let partyID = coreRealm.getPartyId()
//
//            let completion: (String?, UploadableImage?) -> Void = {
//                (reference, assetFailedToUpload) in
//                if let uploadedImageReference = reference {
//                    self.imageReferences.append(uploadedImageReference)
//                }
//
//                if let failedPhotosAsset = assetFailedToUpload {
//                    self.imagesUnableToUpload.append(failedPhotosAsset)
//                }
//
//                group.leave()
//            }
//
//            self.imagesUnableToUpload = Array<UploadableImage>()
//            for image in images {
//
//                group.enter()
//
//                Wire.Content.uploadFile(partyId: partyID, fileContents: image.data, completion: { (fileUploadReference, error) in
//                    if (error != nil) {
//                        completion(nil, image)
//                    } else {
//                        completion(fileUploadReference, nil)
//                    }
//                })
//            }
//            _ = group.wait(timeout: .distantFuture)
//            DispatchQueue.main.async { [weak self] in
//                if self?.allImagesSuccesfullyUploaded() ?? false{
//                    guard let imageReferences = self?.imageReferences else {
//                        return
//                    }
//                    self?.delegate?.imagesUploaded(references: imageReferences)
//                    /* Refresh the state of imageReferences after successfully uploading all the images. */
//                    self?.imageReferences = Array<String>()
//                }else{
//                    self?.delegate?.onImageUploadFailed()
//                }
//            }
//        }
//    }
//}
//
//public class UploadableImage {
//    public var name: String
//    public var data: Data
//
//    public init() {
//        name = ""
//        data = Data()
//    }
//
//    public func setup(with assetData: Data) {
//        self.data = assetData
//    }
//
//    public func setup(assetURLString: String) {
//        if let asset = PhotoAssetHelper.PhotosAssetForFileURL(url: assetURLString) {
//            let imageRequestOptions = PHImageRequestOptions()
//            imageRequestOptions.isSynchronous = true
//            let mainDispatch = DispatchGroup()
//            mainDispatch.enter()
//            PHImageManager.default().requestImageData(for: asset, options: imageRequestOptions, resultHandler: { (imageData, dataUTI, orientation, info) in
//                if let data = imageData {
//                    self.data = data
//                }
//                mainDispatch.leave()
//            })
//            _ = mainDispatch.wait(timeout: .distantFuture)
//        }
//    }
//}
