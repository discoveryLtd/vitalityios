//
//  VHCSourceDateCell.swift
//  VitalityActive
//
//  Created by Lionel Camacho on 2017/04/10.
//  Copyright © 2017 Glucode. All rights reserved.
//

import UIKit
import VIAUIKit

class VHCSourceDateCell: UICollectionViewCell, Nibloadable {
    // MARK: - Properties

    var addTopBorder = false
    var addBottomBorder = false
    var addSeparator = false

    @IBOutlet weak var titleLabel: UILabel!

    @IBOutlet weak var valueLabel: UILabel!


    // MARK: - Lifecycle functions

    public override func prepareForReuse() {
        super.prepareForReuse()
        setupCell()
    }

    override public func awakeFromNib() {
        super.awakeFromNib()
        setupCell()
    }

   // MARK: - Cell initialization

    private func setupCell() {
        titleLabel.text = nil
        valueLabel.text = nil

        titleLabel.font = .footnoteFont()
        valueLabel.font = .subheadlineFont()

        titleLabel.textColor = .mediumGrey()
        valueLabel.textColor = .night()
    }

    public override func layoutSubviews() {
        super.layoutSubviews()

        contentView.layer.removeAllBorderLayers()
        if addTopBorder {
            contentView.layer.addBorder(edge: .top)
        }
        if addBottomBorder {
            contentView.layer.addBorder(edge: .bottom)
        }
        if addSeparator {
            contentView.layer.addTopBorder(inset: 15)
        }
    }


  // MARK: - Cell configuration
    public func configureVHCSourceDateCell(titleText: String, valueText: String, isOutOfmembershipPeriod: Bool) {
        if isOutOfmembershipPeriod {
            makeCellGrey()
        }

        titleLabel.text = titleText
        valueLabel.text = valueText
    }

    func makeCellGrey() {
        titleLabel.textColor = UIColor.mediumGrey()
        valueLabel.textColor = UIColor.darkGrey()
    }

}
