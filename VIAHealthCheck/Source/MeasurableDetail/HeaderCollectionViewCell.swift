//
//  HeaderCollectionViewCell.swift
//  VitalityActive
//
//  Created by Lionel Camacho on 2017/04/12.
//  Copyright © 2017 Glucode. All rights reserved.
//

import UIKit
import VIAUIKit

class HeaderCollectionViewCell: UICollectionViewCell, Nibloadable {
    // MARK: - Properties
    var addTopBorder = false
    var addBottomBorder = false

    // MARK: - Lifecycle functions

    public override func prepareForReuse() {
        super.prepareForReuse()
        setupCell()
    }

    override public func awakeFromNib() {
        super.awakeFromNib()
        setupCell()
    }

    @IBOutlet weak var titleLabel: UILabel!

   // MARK: - Cell initialization

    private func setupCell() {
        titleLabel.text = nil

        titleLabel.font = .footnoteFont()

        titleLabel.textColor = .mediumGrey()
    }

    public override func layoutSubviews() {
        super.layoutSubviews()

        contentView.layer.removeAllBorderLayers()
        if addTopBorder {
            contentView.layer.addBorder(edge: .top)
        }
        if addBottomBorder {
            contentView.layer.addBorder(edge: .bottom)
        }
    }

  // MARK: - Cell configuration
    public func configureHeaderCollectionViewCell(titleText: String) {
        titleLabel.text = titleText
    }

}
