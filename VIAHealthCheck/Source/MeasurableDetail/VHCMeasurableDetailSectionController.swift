import UIKit
import VIAUIKit
import IGListKit
import VitalityKit
import VitalityKit
import VIAUtilities

class VHCMeasurableDetailSectionController: IGListSectionController {

    var measurable: VHCDetailCellData?
    var sourceIsEmpty = false
    var sourceAddedToView = false
    var feedbackCount = 0
    let headerHeight: CGFloat = 56
    let dateAndSourceHeight: CGFloat = 72
    var calculatedHeight: CGFloat = 0
    var calculatedWidth: CGFloat = 0

    func shouldAddSource() -> Bool {
        guard let measurable = self.measurable else { return false }
        
        return measurable.sourceText != nil
    }

    enum DetailItems {
        case header(numberOfItems: Int)
        case measurableDetail(numberOfItems: Int)
        case feedback(numberOfItems: Int)
        case source(numberOfItems: Int)
        case dateTested(numberOfItems: Int)

        func numberOfItems() -> Int {
            switch self {
            case .header(let numberOfItems),
                 .measurableDetail(let numberOfItems),
                 .feedback(let numberOfItems),
                 .source(let numberOfItems),
                 .dateTested(let numberOfItems):
                return numberOfItems
            }
        }
    }

    // Returns the section to which a specific index
    // from numberOfItems belongs.
    func section(for index: Int) -> DetailItems {
        var totalNumberOfItems = 0
        for section in self.sections {
            totalNumberOfItems += section.numberOfItems()
            if index <= totalNumberOfItems - 1 {
                return section
            }
        }
        return self.sections.last! // silly default but let's roll with it
    }

    lazy var sections: [DetailItems] = {
        return [.header(numberOfItems: 1),
                .measurableDetail(numberOfItems: 1),
                .feedback(numberOfItems: self.measurable?.pointsAndHealthyRangeFeedback.count ?? 1),
                .source(numberOfItems: self.shouldAddSource() ? 1 : 0),
                .dateTested(numberOfItems: 1)]
    }()
    //FIXME:- Refactor to use multiple section controllers when possible
    // Yes this is ugly, but we've painted ourselves into a corner here on a couple
    // of fronts. This array is basically fluff to give us the indexes of the feedback
    // items. It is required because this controller deals incorrectly with multiple
    // expanding sections of items, which could be better handled by having a sectionController
    // per section in self.sections.
    // Or use BindingSectionController in IGListKit 3.0+
    lazy var allFeedbackItems: [Any] = {
        var items = [Any]()
        for section in self.sections {
            switch section {
            case .feedback(let numberOfItems):
                self.measurable?.pointsAndHealthyRangeFeedback.forEach({ items.append($0) })
                break
            case .header,
                 .measurableDetail,
                 .source,
                 .dateTested:
                items.append("")
                break
            }
        }
        return items
    }()
}

extension VHCMeasurableDetailSectionController: IGListSectionType {

    //MARK IGListSectionType delegate methods

    public func numberOfItems() -> Int {
        var count = 0
        for item in self.sections {
            count += item.numberOfItems()
        }
        return count
    }

    public func sizeForItem(at index: Int) -> CGSize {
        let containerWidth = collectionContext!.containerSize.width

        let section = self.section(for: index)
        switch section {
        case .header:
            return CGSize(width: containerWidth, height: headerHeight)
        case .measurableDetail:
            let isHidden = self.measurable?.measurementDescription?.isEmpty ?? true
            return CGSize(width: containerWidth, height: VHCMeasurableValueCell.height(measurementDescription: isHidden))
        case .feedback:
            if let feedback = self.allFeedbackItems[index] as? VHCFeedback {
                calculatedWidth = widthForFeedbackItem(containerWidth: containerWidth)
                calculatedHeight = totalHeightForFeedbackItem(calculatedWidth: calculatedWidth, feedbackItem: feedback)
                return CGSize(width: containerWidth, height: calculatedHeight)
            }
        case .source:
/**
            If healthAtttributeName = Body Mass Index, do not display Source and Date Tested
            set cell row height to Zero for Source
*/
            if measurable?.HealthAttributeName == CommonStrings.Measurement.BodyMassIndexTitle134 {
                return CGSize.zero
            }
            return CGSize(width: containerWidth, height: dateAndSourceHeight)
        case .dateTested:
/**
            If healthAtttributeName = Body Mass Index, do not display Source and Date Tested
            set cell row height to Zero for Date Tested
*/
            if measurable?.HealthAttributeName == CommonStrings.Measurement.BodyMassIndexTitle134 {
                return CGSize.zero
            }
            return CGSize(width: containerWidth, height: dateAndSourceHeight)
        }

        return CGSize(width: containerWidth, height: CGFloat.leastNonzeroMagnitude)
    }

    public func cellForItem(at index: Int) -> UICollectionViewCell {

        let section = self.section(for: index)
        switch section {
        case .header:
            return headerViewCell(at:index)
        case .measurableDetail:
            return measurableDetailCell(at:index)
        case .feedback:
            if let feedback = self.allFeedbackItems[index] as? VHCFeedback {
                return measurableFeedbackCell(at: index, image: feedback.image, feedback: feedback.description, explanation: feedback.explanation)
            }
        case .source:
            return sourceCell(at:index)
        case .dateTested:
            return dateTestedCell(at:index)
        }

        return (collectionContext?.dequeueReusableCell(of: UICollectionViewCell.self, for: self, at: index))!
    }

    public func didUpdate(to object: Any) {
        measurable = object as? VHCDetailCellData
    }

    public func didSelectItem(at index: Int) {
    }


    // MARK: - Cell creation methods

    func headerViewCell(at index: Int) -> UICollectionViewCell {
        let cell = collectionContext!.dequeueReusableCell(withNibName: HeaderCollectionViewCell.defaultReuseIdentifier, bundle: HeaderCollectionViewCell.bundle(), for: self, at: index) as! HeaderCollectionViewCell
        cell.addBottomBorder = true
        cell.addTopBorder = false
        
        if let measurable = self.measurable {
            if measurable.groupTypeId == ProductFeatureTypeRef.VHCBloodPressure.rawValue {
                cell.configureHeaderCollectionViewCell(titleText: measurable.groupName)
            } else {
                cell.configureHeaderCollectionViewCell(titleText: measurable.HealthAttributeName)
            }
        }
        
        return cell
    }

    func measurableDetailCell(at index: Int) -> UICollectionViewCell {
        let cell = collectionContext!.dequeueReusableCell(withNibName: VHCMeasurableValueCell.defaultReuseIdentifier, bundle: VHCMeasurableValueCell.bundle(), for: self, at: index) as! VHCMeasurableValueCell
        
        if let measurable = self.measurable {
            cell.configureVHCMeasurableDetailCell(value: measurable.measurementValue,
                                                  description: measurable.measurementDescription,
                                                  unitsSymbol: measurable.measurementUnits,
                                                  isOutOfMembershipPeriod: measurable.isOutOfMembershipPeriod)
        }
        
        return cell
    }

    func measurableFeedbackCell(at index: Int, image: UIImage, feedback: String, explanation: String?) -> UICollectionViewCell {
        let cell = collectionContext!.dequeueReusableCell(withNibName: VHCMeasurableFeedbackCell.defaultReuseIdentifier, bundle: VHCMeasurableFeedbackCell.bundle(), for: self, at: index) as! VHCMeasurableFeedbackCell
        
        if let measurable = self.measurable {
            cell.configureVHCMeasurableFeedbackCell(image: image,
                                                    feedback: feedback,
                                                    explanation: explanation,
                                                    isOutOfMembershipPeriod: measurable.isOutOfMembershipPeriod)
        }
        
        return cell
    }

    func sourceCell(at index: Int) -> UICollectionViewCell {
        let cell = collectionContext!.dequeueReusableCell(withNibName: VHCSourceDateCell.defaultReuseIdentifier, bundle: VHCSourceDateCell.bundle(), for: self, at: index) as! VHCSourceDateCell
        cell.addSeparator = true
        
        if let measurable = self.measurable, let value = measurable.sourceText {
            cell.configureVHCSourceDateCell(titleText: CommonStrings.DetailScreen.SourceTitle198,
                                            valueText:value,
                                            isOutOfmembershipPeriod: measurable.isOutOfMembershipPeriod)
        }
        
        return cell
    }

    func dateTestedCell(at index: Int) -> UICollectionViewCell {
        let cell = collectionContext!.dequeueReusableCell(withNibName: VHCSourceDateCell.defaultReuseIdentifier, bundle: VHCSourceDateCell.bundle(), for: self, at: index) as! VHCSourceDateCell
        cell.addSeparator = true
        cell.addBottomBorder = true
        
        if let measurable = self.measurable, let value = measurable.dateTestedText {
            cell.configureVHCSourceDateCell(titleText: CommonStrings.CaptureResults.DateTested144, valueText: value, isOutOfmembershipPeriod: measurable.isOutOfMembershipPeriod)
        }
        
        return cell
    }

   // MARK: - Height and width helper functions

    func widthForFeedbackItem(containerWidth: CGFloat) -> CGFloat {
        let cell = VHCMeasurableFeedbackCell()
        return containerWidth - cell.explanationLeadingConstraint - cell.explanationTrailingConstraint
    }

    func heightForDescriptionText(calculatedWidth: CGFloat, text: String) -> CGFloat {
        let insets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        let size = TextSize.size(text, font: UIFont.subheadlineFont(), width: calculatedWidth, insets: insets)
        return size.height
    }

    func heightForExplanationText(calculatedWidth: CGFloat, text: String) -> CGFloat {
        let insets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        let size = TextSize.size(text, font: UIFont.footnoteFont(), width: calculatedWidth, insets: insets)
        return size.height
    }

    func totalHeightForFeedbackItem(calculatedWidth: CGFloat, feedbackItem: VHCFeedback) -> CGFloat {
        let descriptionHeight = heightForDescriptionText(calculatedWidth: calculatedWidth, text: feedbackItem.description)
        let verticalPaddingBetween: CGFloat = 5
        let verticalPaddingBottom: CGFloat = 25
        var explanationHeight: CGFloat = 0

        if let explanation = feedbackItem.explanation {
            if explanation.characters.count > 1 {
                explanationHeight = heightForExplanationText(calculatedWidth: calculatedWidth, text: explanation)
                return descriptionHeight + explanationHeight + verticalPaddingBetween + verticalPaddingBottom
            } else {
                return descriptionHeight + verticalPaddingBottom
            }
        } else {
            return descriptionHeight + verticalPaddingBottom
        }
    }

}
