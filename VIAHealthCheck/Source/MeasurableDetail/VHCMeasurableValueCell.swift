//
//  VHCMeasurableDetailCell.swift
//  VitalityActive
//
//  Created by Lionel Camacho on 2017/04/07.
//  Copyright © 2017 Glucode. All rights reserved.
//

import UIKit
import VIAUIKit
import VIAUtilities
import VitalityKit

class VHCMeasurableValueCell: UICollectionViewCell, Nibloadable {

    static func height(measurementDescription isHidden: Bool) -> CGFloat {
        return isHidden ? 85 : 110
    }

    // MARK: - Lifecycle functions

    public override func prepareForReuse() {
        super.prepareForReuse()
        setupCell()
    }

    override public func awakeFromNib() {
        super.awakeFromNib()
        setupCell()
    }

   // MARK: - Outlets and variables

    @IBOutlet weak var measurementValueLabel: UILabel!
    public var measurementValueText: String? {
        set {
            measurementValueLabel.text = newValue
        }
        get {
            return measurementValueLabel.text
        }
    }

    @IBOutlet weak var unitsLabel: UILabel!
    public var formattedUnitText: String? {
        set {
            unitsLabel.text = newValue
            unitsLabel.isHidden = newValue == nil
        }
        get {
            return unitsLabel.text
        }
    }

    @IBOutlet weak var measurementDescriptionLabel: UILabel!
    public var measurementDescriptionText: String? {
        set {
            measurementDescriptionLabel.text = newValue
            measurementDescriptionLabel.isHidden = newValue == nil
        }
        get {
            return measurementDescriptionLabel.text
        }
    }

  // MARK: - Cell initialization

    private func setupCell() {
        measurementValueText = nil
        measurementDescriptionText = nil
        formattedUnitText = nil

        measurementValueLabel.font = UIFont.title1Font()
        measurementDescriptionLabel.font = UIFont.subheadlineFont()
        unitsLabel.font = UIFont.title3Font()

        measurementValueLabel.textColor = .night()
        measurementDescriptionLabel.textColor = .mediumGrey()
        unitsLabel.textColor = .night()

        measurementDescriptionLabel.backgroundColor = .clear
    }

  // MARK: - Cell configuration

    public func configureVHCMeasurableDetailCell(value: String, description: String? = nil, unitsSymbol: String?, isOutOfMembershipPeriod: Bool) {
        if isOutOfMembershipPeriod {
            makeCellGrey()
        }

        measurementValueText = value
        
        //if unit symbol is 'ft in' or 'st lb' no need to add units since it is included in the return of the server
        let measurementFormatter = Localization.decimalShortStyle
        let footInchUnit = measurementFormatter.string(from: (UnitOfMeasureRef.FootInch.unit()))
        let stonePoundUnit = measurementFormatter.string(from:(UnitOfMeasureRef.StonePound.unit()))
        
        if ((unitsSymbol == footInchUnit) || (unitsSymbol == stonePoundUnit)){
            formattedUnitText = ""
        }
        else{
            formattedUnitText = unitsSymbol
        }
        
        measurementDescriptionText = description
    }

    func makeCellGrey() {
        measurementValueLabel.textColor = .darkGrey()
        unitsLabel.textColor = .darkGrey()
        measurementDescriptionLabel.textColor = .darkGrey()
    }

}
