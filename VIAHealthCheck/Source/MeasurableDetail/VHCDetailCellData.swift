//
//  VHCDetailCellData.swift
//  VitalityActive
//
//  Created by Lionel Camacho on 2017/04/11.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import IGListKit
import VIAUIKit

public class VHCDetailCellData: NSObject, IGListDiffable {
    var groupName: String = ""
    var groupTypeId: Int = -1
    var potentialPoints: Int = -1
    var earnedPoints: Int = -1
    var HealthAttributeName: String = ""
    var measurementValue: String = ""
    var measurementDescription: String?
    var measurementUnits: String?
    var sourceText: String?
    var dateTestedText: String?
    var isOutOfMembershipPeriod: Bool = false
    var pointsAndHealthyRangeFeedback: Array<VHCFeedback> = [VHCFeedback]()

    // MARK: IGListDiffable
    public func diffIdentifier() -> NSObjectProtocol {
        return HealthAttributeName as NSObjectProtocol
    }

    public func isEqual(toDiffableObject object: IGListDiffable?) -> Bool {
        guard self !== object else { return true }
        guard let object = object as? VHCDetailCellData else { return false }
        return HealthAttributeName == object.HealthAttributeName
    }

}
