//
//  VHCMeasurementViewController.swift
//  VitalityActive
//
//  Created by Simon Stewart on 2/22/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

import VIAUIKit

public final class VHCMeasurementDescriptionViewController: VIATableViewController, KnowYourHealthTintable {
    var learnMoreContent = VHCHealthAttributeGroupLearnMoreContent()

    override public func viewDidLoad() {
        super.viewDidLoad()

        self.title = learnMoreContent.groupName
        self.configureAppearance()
        self.configureTableView()
    }

    func configureAppearance() {
        navigationController?.makeNavigationBarTransparent()
        tableView.tableFooterView = UIView()
        navigationController?.navigationBar.barTintColor = UIColor.white
    }

    func configureTableView() {
        self.tableView.register(VIAGenericContentCell.nib(), forCellReuseIdentifier: VIAGenericContentCell.defaultReuseIdentifier)
        self.tableView.estimatedRowHeight = 150
        self.tableView.sectionHeaderHeight = 0
        self.tableView.sectionFooterHeight = 0
        self.tableView.estimatedSectionHeaderHeight = 0
        self.tableView.estimatedSectionFooterHeight = 0
        self.tableView.contentInset = UIEdgeInsets(top: 25, left: 0, bottom: 0, right: 0)
        self.tableView.backgroundColor = .white
    }

    // MARK: - UITableView datasource

    override public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }

    // MARK: UITableView delegate

    override public func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.separatorInset = UIEdgeInsetsMake(0, max(UIScreen.main.bounds.size.width, UIScreen.main.bounds.size.height), 0, 0)
    }

    override public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return self.configureContentCell(indexPath)
    }

    func configureContentCell(_ indexPath: IndexPath) -> VIAGenericContentCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: VIAGenericContentCell.defaultReuseIdentifier, for: indexPath) as! VIAGenericContentCell
        var header: String = ""
        var content: String = ""
        var icon: UIImage = UIImage()
        cell.isUserInteractionEnabled = false
        cell.cellImageView.tintColor = UIColor.currentGlobalTintColor()
        switch indexPath.row {
        case 0:
            header = learnMoreContent.section1Title
            content = learnMoreContent.section1Content
            icon = learnMoreContent.section1Icon
            break
        case 1:
            header = learnMoreContent.section2Title
            content = learnMoreContent.section2Content
            icon = learnMoreContent.section2Icon
            break
        case 2:
            header = learnMoreContent.section3Title
            content = learnMoreContent.section3Content
            icon = learnMoreContent.section3Icon
            break
        default:
            break
        }
        cell.customHeaderFont = .title3Font()
        cell.customContentFont = .bodyFont()
        cell.header = header
        cell.content = content
        cell.cellImage = icon
        cell.layoutIfNeeded()
        return cell
    }

}
