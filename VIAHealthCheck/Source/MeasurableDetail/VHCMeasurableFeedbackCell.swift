//
//  VHCMeasurableFeedbackCell.swift
//  VitalityActive
//
//  Created by Lionel Camacho on 2017/04/19.
//  Copyright © 2017 Glucode. All rights reserved.
//

import UIKit
import VIAUIKit

class VHCMeasurableFeedbackCell: UICollectionViewCell, Nibloadable {

    // MARK: - Lifecycle functions

    public override func prepareForReuse() {
        super.prepareForReuse()
        setupCell()
    }

    override public func awakeFromNib() {
        super.awakeFromNib()
        setupCell()
    }

   // MARK: - Outlets and variables

    public let explanationLeadingConstraint: CGFloat = 55
    public let explanationTrailingConstraint: CGFloat = 15

    @IBOutlet weak var feedbackIcon: UIImageView!
    public var feedbackImage: UIImage? {
        set {
            feedbackIcon.image = newValue
        }
        get {
            return self.feedbackIcon.image
        }
    }

    @IBOutlet weak var feedbackLabel: UILabel!
    public var feedbackText: String? {
        set {
            feedbackLabel.text = newValue
        }
        get {
            return feedbackLabel.text
        }
    }

    @IBOutlet weak var explanationLabel: UILabel!
    public var explanationText: String? {
        set {
            explanationLabel.text = newValue
            explanationLabel.isHidden = newValue == nil
        }
        get {
            return explanationLabel.text
        }
    }

    @IBOutlet weak var textLeadingConstraint: NSLayoutConstraint!
    public var leadingSpace: CGFloat {
        set {
            return textLeadingConstraint.constant = newValue
        }
        get {
            return textLeadingConstraint.constant
        }
    }

    @IBOutlet weak var textTrailingConstraint: NSLayoutConstraint!
    public var trailingSpace: CGFloat {
        set {
            return textTrailingConstraint.constant = newValue
        }
        get {
            return textTrailingConstraint.constant
        }
    }

  // MARK: - Cell initialization

    private func setupCell() {
        feedbackIcon.image = nil
        feedbackText = nil
        explanationText = nil

        feedbackLabel.font = .subheadlineFont()
        explanationLabel.font = .footnoteFont()

        feedbackIcon.tintColor = UIColor.mediumGrey()
        feedbackLabel.textColor = .night()
        explanationLabel.textColor = .darkGrey()

        leadingSpace = explanationLeadingConstraint
        trailingSpace = explanationTrailingConstraint
    }

    public override func layoutSubviews() {
        super.layoutSubviews()
    }

 // MARK: - Cell configuration

    public func configureVHCMeasurableFeedbackCell(image: UIImage, feedback: String, explanation: String?, isOutOfMembershipPeriod: Bool) {
        if isOutOfMembershipPeriod {
            makeCellGrey()
        }
        feedbackImage = image
        feedbackText = feedback
        explanationText = explanation
    }

    func makeCellGrey() {
        feedbackLabel.textColor = .darkGrey()
        explanationLabel.textColor = .darkGrey()
    }

}
