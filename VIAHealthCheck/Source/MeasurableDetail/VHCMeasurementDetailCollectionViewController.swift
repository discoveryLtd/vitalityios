//
//  VHCMeasurementDetailViewController.swift
//  VitalityActive
//
//  Created by Lionel Camacho on 2017/03/08.
//  Copyright © 2017 Glucode. All rights reserved.
//

import VIAUIKit
import IGListKit

public final class VHCMeasurementDetailCollectionViewController: VIAViewController, KnowYourHealthTintable {

    // MARK: - Properties
    var measurablesData = [VHCDetailCellData]()

    let collectionView: IGListCollectionView = {
        let collectionView = IGListCollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
        collectionView.backgroundColor = UIColor.tableViewBackground()
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        return collectionView
    }()

    lazy var adapter: IGListAdapter = {
        return IGListAdapter(updater: IGListAdapterUpdater(), viewController: self, workingRangeSize: 0)
    }()

    // MARK: View lifecycle

    override public func viewDidLoad() {
        super.viewDidLoad()
        measurablesData.sort { $0.potentialPoints > $1.potentialPoints }
        title = measurablesData.first?.groupName

        configureCollectionView()
        configureAdapter()
    }

    //View configuration

    func configureCollectionView() {
        view.addSubview(collectionView)
        collectionView.snp.makeConstraints { (make) in
            make.top.left.bottom.right.equalToSuperview()
        }
    }

    func configureAdapter() {
        adapter.collectionView = collectionView
        adapter.dataSource = self
    }
}

extension VHCMeasurementDetailCollectionViewController: IGListAdapterDataSource {
    public func objects(for listAdapter: IGListAdapter) -> [IGListDiffable] {
        let data = measurablesData as [IGListDiffable]
        return data
    }

    public func listAdapter(_ listAdapter: IGListAdapter, sectionControllerFor object: Any) -> IGListSectionController {
        return VHCMeasurableDetailSectionController()
    }

    public func emptyView(for listAdapter: IGListAdapter) -> UIView? {
        return nil
    }

}
