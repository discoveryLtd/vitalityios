import Foundation
import VIAUIKit
import VitalityKit
import VIAUtilities
import VIACommon

public class VHCDataSharingConsentViewController: CustomTermsConditionsViewController, KnowYourHealthTintable {

    let eventsProcessor = VHCSubmissionHelper()
    
    // MARK: View lifecycle

    override open func viewDidLoad() {
        super.viewDidLoad()

        self.viewModel = CustomTermsConditionsViewModel(articleId: AppConfigFeature.contentId(for: .VHCDSConsentContent))
        
        rightButtonTitle = VIAApplicableFeatures.default.getDataSharingRightBarButtonTitle()
        leftButtonTitle = VIAApplicableFeatures.default.getDataSharingLeftBarButtonTitle()
    }

    override public func configureAppearance() {
        super.configureAppearance()
        
        navigationController?.makeNavigationBarInvisible()
        navigationController?.navigationBar.isHidden = true
    }

    // MARK: Action buttons

    override public func leftButtonTapped(_ sender: UIBarButtonItem?) {
        self.performSegue(withIdentifier: "unwindFromVHCDataSharingConsentToSummary", sender: nil)
    }

    override public func rightButtonTapped(_ sender: UIBarButtonItem?) {
        self.showHUDOnWindow()

        let events = [EventTypeRef.VHCDataPrivacyAgree]
        viewModel?.agreeToTermsAndConditions(with: events, completion: { [weak self] (error) in
            self?.hideHUDFromWindow()
            guard error == nil else {
                self?.handleErrorOccurred(error)
                return
            }
            self?.submitVHC()
        })
    }

    public func submitVHC() {
        self.showHUDOnWindow()
        eventsProcessor.setupDelegate(self)
        eventsProcessor.processEvents(completion: { [weak self] error in
            self?.hideHUDFromWindow()
            guard error == nil else {
                self?.handleErrorOccurred(error)
                return
            }
            self?.performSegue(withIdentifier: "showCompletion", sender: nil)
        })
    }

    public func handleErrorOccurred(_ error: Error?) {
        guard error == nil else {
            debugPrint(error as Any)
            switch error {
            case is BackendError:
                self.handleBackendErrorWithAlert(error as! BackendError, tryAgainAction: { [weak self] in
                    self?.rightButtonTapped(nil)
                })
            default:
                self.displayUnknownServiceErrorOccurredAlert()
            }
            self.hideHUDFromWindow()
            return
        }
    }

    public override func returnToPreviousView() {
        self.performSegue(withIdentifier: "unwindFromVHCDataSharingConsentToSummary", sender: nil)
    }
}

extension VHCDataSharingConsentViewController: PhotoUploadDelegate{

    public func displayImageUploadErrorMessage(with completion: @escaping (_ resubmit: Bool) -> Void){
        let cancelAction = UIAlertAction(title: CommonStrings.CancelButtonTitle24, style: UIAlertActionStyle.cancel) {
            [weak self] (alertAction) in
            completion(false)
            self?.hideHUDFromWindow()
        }
        let tryAgain = UIAlertAction(title: CommonStrings.TryAgainButtonTitle43, style: UIAlertActionStyle.default) { (alertAction) in
            completion(true)
        }
        
        let failedToSubmitVHCAlert = UIAlertController(title: CommonStrings.PrivacyPolicy.UnableToCompleteAlertTitle115, message: CommonStrings.SummaryScreen.VhcCompleteErrorMessage187, preferredStyle: .alert)
        
        failedToSubmitVHCAlert.addAction(cancelAction)
        failedToSubmitVHCAlert.addAction(tryAgain)
        self.present(failedToSubmitVHCAlert, animated: true, completion: nil)
    }
}
