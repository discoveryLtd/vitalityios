import Foundation
import UIKit
import VIAUIKit
import Foundation
import UIKit
import VIAUIKit
import VitalityKit

import VitalityKit

public class VHCCompletionViewController: VIACirclesViewController {

    public override func awakeFromNib() {
        super.awakeFromNib()

        self.viewModel = VHCCompletionViewModel()
    }

    public override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.setNavigationBarHidden(true, animated: false)

        if let vhcViewModel = self.viewModel as? VHCCompletionViewModel {
            vhcViewModel.finaliseSubmission()
        }
    }

    public override func buttonTapped(_ sender: UIButton) {
        self.performSegue(withIdentifier: "unwindToVHCLanding", sender: nil)
    }
}

public class VHCCompletionViewModel: AnyObject, CirclesViewModel {

    public required init() {
    }

    public var image: UIImage? {
        return VIAHealthCheckAsset.arOnboardingActivated.image
    }

    public var heading: String? {
        return CommonStrings.CompleteScreen.MeasurementsConfirmedTitle188
    }

    public var message: String? {
        return CommonStrings.CompleteScreen.MeasurementsConfirmedMessage189
    }

    public var footnote: String? {
        return CommonStrings.Confirmation.CompletedFootnotePointsMessage119
    }

    public var buttonTitle: String? {
        return CommonStrings.GreatButtonTitle120
    }
    
    public var buttonHighlightedTextColor: UIColor? {
        return UIColor.knowYourHealthGreen()
    }

    public var gradientColor: Color {
        return .green
    }

    public func finaliseSubmission() {
        // delete captured VHC results
        let realm = DataProvider.newVHCRealm()
        try! realm.write {
            realm.delete(realm.allVHCCapturedResults())
        }
        
        if let tenantId = AppSettings.getAppTenant(), tenantId == .EC || tenantId == .ASR {
            // Toggle VHC updated flag on AppSettings.
            AppSettings.setHasUpdatedVHC()
        }

        // post notification
        NotificationCenter.default.post(name: .VIAVHCCompletedNotification, object: nil)
    }
}
