import Foundation
import RealmSwift
import VitalityKit
import VIAUIKit

public class VHCLearnMoreViewModel {
    let vhcRealm = DataProvider.newVHCRealm()
    var learnMoreContent = [VHCHealthAttributeGroupLearnMoreContent]()

    public init() {
        configureMeasurableItems(party: DataProvider.newRealm().currentVitalityParty())
    }

    func configureMeasurableItems(party: VitalityParty?) {
        let allGroups = VHCHealthAttributeGroup.sortedGroups(from: vhcRealm)
        if allGroups.count == 0 {
            VHCEvent.persistHealthAttributeGroups()
        }
        learnMoreContent = allGroups.map({ $0.learnMoreContent(party: party) })
    }
}

extension VHCLearnMoreViewModel {
    
    func setupHeader(cell: VIAGenericContentCell) {
        setupContentCell(header: CommonStrings.LearnMore.Heading1Title204,
                         content: CommonStrings.LearnMore.Heading1Message205,
                         customHeaderFont: UIFont.title1Font(),
                         customContentFont: UIFont.subheadlineFont(),
                         cell: cell)
    }
    
    func setupVisitHealthCareProfessional(cell: VIAGenericContentCell) {
        setupContentCell(header: CommonStrings.LearnMore.Section1Title206,
                         content: CommonStrings.LearnMore.Section1Message207,
                         icon: VIAHealthCheckAsset.vhcHealthcareBig.image,
                         customHeaderFont: UIFont.title2Font(),
                         customContentFont: UIFont.bodyFont(),
                         cell: cell)
    }
    
    func setupParticipatingPartner(cell: VIAGenericContentCell) {
        setupContentCell(content: CommonStrings.Vhc.LearnMore.ParticipatingPartners262,
                         icon: VIAHealthCheckAsset.vhcGenericLearnMore.templateImage,
                         customContentFont: UIFont.headlineFont(),
                         customContentTextColor: UIColor.currentGlobalTintColor(),
                         opacity: 0,
                         isUserInteractionEnabled: true,
                         cell: cell)
    }
    
    func setupCompleteVitalityHealthcheck(cell: VIAGenericContentCell) {
        setupContentCell(header: CommonStrings.LearnMore.Section2Title208,
                         content: CommonStrings.LearnMore.Section2Message209,
                         icon: VIAHealthCheckAsset.vhcCompletedBig.image,
                         customHeaderFont: UIFont.title2Font(),
                         customContentFont: UIFont.bodyFont(),
                         cell: cell)
    }
    
    func setupEarnPoints(cell: VIAGenericContentCell) {
        setupContentCell(header: CommonStrings.Onboarding.Section3Title130,
                         content: CommonStrings.LearnMore.Section3Message210,
                         icon: VIAHealthCheckAsset.vhcEarnPointsBig.image,
                         customHeaderFont: UIFont.title2Font(),
                         customContentFont: UIFont.bodyFont(),
                         cell: cell)
    }
    
    func withoutParticipatingPartnerCell(indexPath: IndexPath, cell: VIAGenericContentCell) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            setupHeader(cell: cell)
            break
        case 1:
            setupVisitHealthCareProfessional(cell: cell)
            break
        case 2:
            setupCompleteVitalityHealthcheck(cell: cell)
            break
        case 3:
            setupEarnPoints(cell: cell)
            break
        default:
            break
        }
        
        return cell
    }
    
    func withParticipatingPartnerCell(indexPath: IndexPath, cell: VIAGenericContentCell) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            setupHeader(cell: cell)
            break
        case 1:
            setupVisitHealthCareProfessional(cell: cell)
            break
        case 2:
            setupParticipatingPartner(cell: cell)
            break
        case 3:
            setupCompleteVitalityHealthcheck(cell: cell)
            break
        case 4:
            setupEarnPoints(cell: cell)
            break
        default:
            break
        }
        
        return cell
    }
    
    func setupContentCell(header: String? = nil, content: String, icon: UIImage? = nil,
                          customHeaderFont: UIFont? = nil, customContentFont: UIFont? = nil,
                          customContentTextColor: UIColor? = nil, opacity: CGFloat? = nil,
                          isUserInteractionEnabled: Bool? = nil,
                          cell: VIAGenericContentCell) {
        
        if let isUserInteractionEnabled = isUserInteractionEnabled {
            cell.isUserInteractionEnabled = isUserInteractionEnabled
        } else {
            cell.isUserInteractionEnabled = false
        }
        
        if let header = header {
            cell.header = header
        }
        
        cell.content = content
        cell.cellImage = icon
        
        if let customHeaderFont = customHeaderFont {
            cell.customHeaderFont = customHeaderFont
        } else {
            cell.customHeaderFont = UIFont.title2Font()
        }
        
        if let customContentFont = customContentFont {
            cell.customContentFont = customContentFont
        } else {
            cell.customContentFont = UIFont.bodyFont()
        }
        
        if let customContentTextColor = customContentTextColor {
            cell.customContentTextColor = customContentTextColor
        }
        
        if let opacity = opacity {
            cell.cellImageView.alpha = opacity
        }
        
        cell.layoutIfNeeded()
    }
}
