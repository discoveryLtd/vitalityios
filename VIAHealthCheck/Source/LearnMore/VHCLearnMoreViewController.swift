import VIAUIKit
import VitalityKit
import VIAUtilities
import VIACommon

public final class VHCLearnMoreViewController: VIATableViewController, KnowYourHealthTintable {

    let viewModel = VHCLearnMoreViewModel()

    override public func viewDidLoad() {
        super.viewDidLoad()

        self.title = CommonStrings.LearnMoreButtonTitle104
        self.configureAppearance()
        self.configureTableView()
    }

    override public func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        self.hideBackButtonTitle()
    }

    func configureAppearance() {
        navigationController?.makeNavigationBarTransparent()
        navigationController?.navigationBar.barTintColor = UIColor.white
    }

    func configureTableView() {
        self.tableView.register(VIAGenericContentCell.nib(), forCellReuseIdentifier: VIAGenericContentCell.defaultReuseIdentifier)
        self.tableView.register(VIATableViewCell.self, forCellReuseIdentifier: VIATableViewCell.defaultReuseIdentifier)
        self.tableView.estimatedRowHeight = 75
        self.tableView.sectionHeaderHeight = 0
        self.tableView.sectionFooterHeight = 0
        self.tableView.estimatedSectionHeaderHeight = 0
        self.tableView.estimatedSectionFooterHeight = 0
        self.tableView.contentInset = UIEdgeInsets(top: 25, left: 0, bottom: 0, right: 0)
        self.tableView.backgroundColor = .white
    }

    // MARK: - UITableView datasource

    enum Sections: Int, EnumCollection {
        case Content = 0
        case Measurables = 1
    }

    override public func numberOfSections(in tableView: UITableView) -> Int {
        return Sections.allValues.count
    }

    override public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let section = Sections(rawValue: section)
        if section == .Content {
            if VIAApplicableFeatures.default.hideParticipatingPartners() {
                return 4
            } else {
                return 5
            }
        } else if section == .Measurables {
            return self.viewModel.learnMoreContent.count
        }
        return 0
    }

    // MARK: UITableView delegate

    override public func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let section = Sections(rawValue: indexPath.section)
        if section == .Content && indexPath.row != 4 {
            cell.separatorInset = UIEdgeInsetsMake(0, max(UIScreen.main.bounds.size.width, UIScreen.main.bounds.size.height), 0, 0)
        } else if section == .Content && indexPath.row == 4 {
            cell.preservesSuperviewLayoutMargins = false
            cell.separatorInset = UIEdgeInsets.zero
            cell.layoutMargins = UIEdgeInsets.zero
        }
    }

    override public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let section = Sections(rawValue: indexPath.section)
        if section == .Content {
            return self.configureContentCell(indexPath)
        } else if section == .Measurables {
            return self.configureMeasurableItemCell(indexPath)
        }

        return tableView.dequeueReusableCell(withIdentifier: VIATableViewCell.defaultReuseIdentifier, for: indexPath)
    }

    override public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let section = Sections(rawValue: indexPath.section)
        if section == .Content && indexPath.row == 2 {
            participatingPartnersClick(self)
        }
        else if section == .Measurables {
            measurementClick(self)
        }
    }

    func configureContentCell(_ indexPath: IndexPath) -> VIAGenericContentCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: VIAGenericContentCell.defaultReuseIdentifier, for: indexPath) as! VIAGenericContentCell
        
        if VIAApplicableFeatures.default.hideParticipatingPartners() {
            /* Hide Participating Partner for UKE, IGI, and EC */
            return self.viewModel.withoutParticipatingPartnerCell(indexPath: indexPath, cell: cell) as! VIAGenericContentCell
        } else {
            return self.viewModel.withParticipatingPartnerCell(indexPath: indexPath, cell: cell) as! VIAGenericContentCell
        }
    }

    func configureMeasurableItemCell(_ indexPath: IndexPath) -> VIATableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: VIATableViewCell.defaultReuseIdentifier, for: indexPath) as! VIATableViewCell
        let content = self.viewModel.learnMoreContent[indexPath.row]
        cell.textLabel?.text = content.groupName
        cell.imageView?.tintColor = AppSettings.isAODAEnabled() ? UIColor.knowYourHealthGreen() : UIColor.currentGlobalTintColor()
        cell.imageView?.image = content.templateImage
        cell.accessoryType = .disclosureIndicator
        return cell
    }

    // MARK: - Navigation

    public override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segueMeasurement", let nextScene = segue.destination as? VHCMeasurementDescriptionViewController, let indexPath = self.tableView.indexPathForSelectedRow {
            let selectedGroup = self.viewModel.learnMoreContent[indexPath.row]
            nextScene.learnMoreContent = selectedGroup
        }
    }

    @IBAction func participatingPartnersClick(_ sender: Any) {
        self.performSegue(withIdentifier: "segueParticipatingPartners", sender: self)
    }

    @IBAction func measurementClick(_ sender: Any) {
        self.performSegue(withIdentifier: "segueMeasurement", sender: self)
    }

}
