//
//  GlucoseLearnMore.swift
//  VitalityActive
//
//  Created by admin on 2017/07/14.
//  Copyright © 2017 Glucode. All rights reserved.
//

import UIKit
import VitalityKit
import VIACommon

class GlucoseLearnMore: VHCLearnMoreContentProtocol {
    func groupName() -> String {
        return CommonStrings.Measurement.GlucoseTitle136
    }

    func section1Title() -> String {
        return CommonStrings.LearnMore.GlucoseDetailSection1Title213
    }

    func section1Content() -> String {
        return CommonStrings.LearnMore.GlucoseSection1Message231
    }

    func section2Title() -> String {
        return CommonStrings.LearnMore.ImportantSection2Title217
    }

    func section2Content() -> String {
        return CommonStrings.LearnMore.GlucoseSection2Message232
    }

    func section3Title() -> String {
        return CommonStrings.LearnMore.GlucoseSection3Title220
    }

    func section3Content(party: VitalityParty?) -> String {
        return VIAApplicableFeatures.default.getCommonStringsLearnMoreSection3MessageGlucose(party: party)
    }
}
