//
//  OtherBloodLipidsLearnMore.swift
//  VitalityActive
//
//  Created by Genie Mae Lorena Edera on 3/7/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import UIKit
import VitalityKit
import VIACommon

class OtherBloodLipidsLearnMore: VHCLearnMoreContentProtocol {
    func groupName() -> String {
        return CommonStrings.Measurement.OtherBloodLipidsTitle848
    }
    
    func section1Title() -> String {
        return CommonStrings.LearnMore.LipidProfileSection1Title1454
    }
    
    func section1Content() -> String {
        return CommonStrings.LearnMore.LipidProfileSection1Content1455
    }
    
    func section2Title() -> String {
        return CommonStrings.LearnMore.LipidProfileSection2Title217
    }
    
    func section2Content() -> String {
        return CommonStrings.LearnMore.LipidProfileSection2Content1456
    }
    
    func section3Title() -> String {
        return CommonStrings.LearnMore.LipidProfileSection3Title1457
    }
    
    func section3Content(party: VitalityParty?) -> String {
        return CommonStrings.LearnMore.LipidProfileSection3Content1458
    }
}
