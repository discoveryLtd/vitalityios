import UIKit
import VitalityKit
import VIACommon

class UrineProteinLearnMore: VHCLearnMoreContentProtocol {
    func groupName() -> String {
        return CommonStrings.Measurement.UrineProteinTitle283
    }

    func section1Title() -> String {
        return CommonStrings.LearnMore.UrineProteinSection1Title344
    }

    func section1Content() -> String {
        return CommonStrings.LearnMore.UrineProteinSection1Message345
    }

    func section2Title() -> String {
        return CommonStrings.LearnMore.ImportantSection2Title217
    }

    func section2Content() -> String {
        return CommonStrings.LearnMore.UrineProteinSection2Message346
    }

    func section3Title() -> String {
        return CommonStrings.LearnMore.UrineProteinSection3Title347
    }

    func section3Content(party: VitalityParty?) -> String {
        return VIAApplicableFeatures.default.getCommonStringsLearnMoreSection3MessageUrineProtein(party: party)
    }
}
