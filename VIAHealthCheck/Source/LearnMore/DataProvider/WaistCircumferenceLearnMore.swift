//
//  WaistCircumferenceLearnMore.swift
//  VitalityActive
//
//  Created by admin on 2017/07/14.
//  Copyright © 2017 Glucode. All rights reserved.
//

import UIKit
import VitalityKit
import VIACommon

class WaistCircumferenceLearnMore: VHCLearnMoreContentProtocol {
    func groupName() -> String {
        return CommonStrings.Measurement.WaistCircumferenceTitle135
    }

    func section1Title() -> String {
        return CommonStrings.LearnMore.WaistCircumferneceDetailSection1Title212
    }

    func section1Content() -> String {
        return CommonStrings.LearnMore.WaistCicumferneceSection1Message228
    }

    func section2Title() -> String {
        return CommonStrings.LearnMore.ImportantSection2Title217
    }

    func section2Content() -> String {
        return CommonStrings.LearnMore.WaistCicumferneceSection2Message229
    }

    func section3Title() -> String {
        return CommonStrings.LearnMore.WaistCircumfereneceSection3Title219
    }

    func section3Content(party: VitalityParty?) -> String {
        return VIAApplicableFeatures.default.getCommonStringsLearnMoreSection3MessageWaistCircumference(party: party)
    }

}
