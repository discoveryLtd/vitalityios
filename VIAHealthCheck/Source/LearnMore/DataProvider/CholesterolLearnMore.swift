//
//  CholesterolLearnMore.swift
//  VitalityActive
//
//  Created by admin on 2017/07/14.
//  Copyright © 2017 Glucode. All rights reserved.
//

import UIKit
import VitalityKit
import VIACommon

class CholesterolLearnMore: VHCLearnMoreContentProtocol {
    func groupName() -> String {
        return VIAApplicableFeatures.default.getCholesterolAttributeGroupName()
    }

    func section1Title() -> String {
        return CommonStrings.LearnMore.CholesterolDetailSection1Title215
    }

    func section1Content() -> String {
        return CommonStrings.LearnMore.CholesterolSection1Message237
    }

    func section2Title() -> String {
        return CommonStrings.LearnMore.ImportantSection2Title217
    }

    func section2Content() -> String {
        return CommonStrings.LearnMore.CholesterolSection2Message238
    }

    func section3Title() -> String {
        return CommonStrings.LearnMore.CholesterolSection3Title222
    }

    func section3Content(party: VitalityParty?) -> String {
        return VIAApplicableFeatures.default.getCommonStringsLearnMoreSection3MessageCholesterol(party: party)
    }
}
