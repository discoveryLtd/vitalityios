//
//  BodyMassIndexLearnMore.swift
//  VitalityActive
//
//  Created by admin on 2017/07/14.
//  Copyright © 2017 Glucode. All rights reserved.
//

import UIKit
import VitalityKit
import VIACommon

class BodyMassIndexLearnMore: VHCLearnMoreContentProtocol {
    func groupName() -> String {
        return CommonStrings.Measurement.BodyMassIndexTitle134
    }

    func section1Title() -> String {
        return CommonStrings.LearnMore.BmiDetailSection1Titile211
    }

    func section1Content() -> String {
        return CommonStrings.LearnMore.BmiSection1Message225
    }

    func section2Title() -> String {
        return CommonStrings.LearnMore.ImportantSection2Title217
    }

    func section2Content() -> String {
        return CommonStrings.LearnMore.BmiSection2Message226
    }

    func section3Title() -> String {
        return CommonStrings.LearnMore.BmiSection3Title218
    }

    func section3Content(party: VitalityParty?) -> String {
        return VIAApplicableFeatures.default.getCommonStringsLearnMoreSection3MessageBmi(party: party)
    }
}
