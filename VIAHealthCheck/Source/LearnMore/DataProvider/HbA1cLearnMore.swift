//
//  HbA1cLearnMore.swift
//  VitalityActive
//
//  Created by admin on 2017/07/14.
//  Copyright © 2017 Glucode. All rights reserved.
//

import UIKit
import VitalityKit
import VIACommon

class HbA1cLearnMore: VHCLearnMoreContentProtocol {
    func groupName() -> String {
        return CommonStrings.Measurement.Hba1cTitle139
    }

    func section1Title() -> String {
        return CommonStrings.LearnMore.Hba1cDetailSection1Title216
    }

    func section1Content() -> String {
        return CommonStrings.LearnMore.Hba1cSection1Message240
    }

    func section2Title() -> String {
        return CommonStrings.LearnMore.ImportantSection2Title217
    }

    func section2Content() -> String {
        return CommonStrings.LearnMore.Hba1cSection2Message241
    }

    func section3Title() -> String {
        return CommonStrings.LearnMore.Hba1cSection3Title223
    }

    func section3Content(party: VitalityParty?) -> String {
        return VIAApplicableFeatures.default.getCommonStringsLearnMoreSection3MessageFastingGlucose(party: party)
    }

}
