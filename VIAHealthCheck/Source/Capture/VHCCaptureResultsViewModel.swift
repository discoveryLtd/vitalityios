import RealmSwift
import IGListKit
import VIAUIKit
import VIAUtilities
import VitalityKit
import SwiftDate
import Foundation
import VIACommon

// MARK: Persist

public func vhcPersistInput(_ input: String?,
                            isValid: Bool?,
                            for healthAttribute: VHCHealthAttribute,
                            on date: Date,
                            unit: Unit?,
                            unitOfMeasureType: UnitOfMeasureRef?) {
    
    let vhcRealm = DataProvider.newVHCRealm()

    if healthAttribute.type == .UrinaryProtein {
        if let inputValue = input, !inputValue.isEmpty {
            vhcRealm.captureResult(type: healthAttribute.type, date: date, unitOfMeasureType: UnitOfMeasureRef.Unknown, value: nil, isValid: isValid ?? true, rawInput: inputValue, inputUnitSymbol: nil) 
        }
        else {
            deleteCapturedResultForHealthAttribute(healthAttribute, from: vhcRealm)
        }
    }
    else {
        guard let validUnit = unit, let validUnitOfMeasureType = unitOfMeasureType else {
            debugPrint("Missing values to persist health measurable reading")
            return
        }
        
        guard let validInput = input?.localizeDecimalFormat() else {
            debugPrint("Invalid decimal input. Setting value and rawValue to nil.")
            vhcRealm.captureResult(type: healthAttribute.type, date: date, unitOfMeasureType: validUnitOfMeasureType, value: nil, isValid: false, rawInput: input, inputUnitSymbol: validUnit.symbol)
            NotificationCenter.default.post(name: .VIAValidateVHCHealthAttribute, object: nil)
            return
        }
        
        let value = Localization.decimalFormatter.number(from: validInput)?.doubleValue

        if let value = value {
            vhcRealm.captureResult(type: healthAttribute.type, date: date, unitOfMeasureType: validUnitOfMeasureType, value: value, isValid: isValid ?? true, rawInput: validInput, inputUnitSymbol: validUnit.symbol)
        } else {
            deleteCapturedResultForHealthAttribute(healthAttribute, from: vhcRealm)
        }
    }
    NotificationCenter.default.post(name: .VIASuccessfullyCapturedVHCHealthAttribute, object: nil)
}

public func vhcPersistInput(_ input: String?, isValid: Bool?, for healthAttribute: VHCHealthAttribute, on date: Date) {
    let vhcRealm = DataProvider.newVHCRealm()
    if let value = input {
        vhcRealm.captureResult(type: healthAttribute.type, date: date, isValid: isValid ?? true, rawInput: value)
    } else {
        deleteCapturedResultForHealthAttribute(healthAttribute, from: vhcRealm)
    }
    NotificationCenter.default.post(name: .VIASuccessfullyCapturedVHCHealthAttribute, object: nil)
}

fileprivate func deleteCapturedResultForHealthAttribute(_ healthAttribute: VHCHealthAttribute, from realm: Realm) {
    try! realm.write {
        if let deletedResult = realm.vhcCapturedResult(of: healthAttribute.type) {
            realm.delete(deletedResult)
        }
    }
}

// MARK: Double input helper - e.g. bloodpressure that is over/under

public class VHCSingleInput: IGListDiffable {
    var id = UUID().uuidString
    var healthAttribute: VHCHealthAttribute

    public init(healthAttribute: VHCHealthAttribute) {
        self.healthAttribute = healthAttribute
    }

    // MARK: IGListDiffable

    public func diffIdentifier() -> NSObjectProtocol {
        return id as NSObjectProtocol
    }

    public func isEqual(toDiffableObject object: IGListDiffable?) -> Bool {
        guard self !== object else { return true }
        guard let object = object as? VHCSingleInput else { return false }
        return id == object.id
    }
}

public class VHCOptionListInput: IGListDiffable {
    var id = UUID().uuidString
    var healthAttribute: VHCHealthAttribute

    public init(healthAttribute: VHCHealthAttribute) {
        self.healthAttribute = healthAttribute
    }

    // MARK: IGListDiffable

    public func diffIdentifier() -> NSObjectProtocol {
        return id as NSObjectProtocol
    }

    public func isEqual(toDiffableObject object: IGListDiffable?) -> Bool {
        guard self !== object else { return true }
        guard let object = object as? VHCOptionListInput else { return false }
        return id == object.id
    }
}

public class VHCBloodPressureInput: IGListDiffable {
    var id = UUID().uuidString
    var top: VHCHealthAttribute
    var bottom: VHCHealthAttribute

    public init(top: VHCHealthAttribute, bottom: VHCHealthAttribute) {
        self.top = top
        self.bottom = bottom
    }

    // MARK: IGListDiffable

    public func diffIdentifier() -> NSObjectProtocol {
        return id as NSObjectProtocol
    }

    public func isEqual(toDiffableObject object: IGListDiffable?) -> Bool {
        guard self !== object else { return true }
        guard let object = object as? VHCBloodPressureInput else { return false }
        return id == object.id
    }
}

public class VHCBMIInput: IGListDiffable {
    var id = UUID().uuidString
    var top: VHCHealthAttribute
    var bottom: VHCHealthAttribute
    
    public init(top: VHCHealthAttribute, bottom: VHCHealthAttribute) {
        self.top = top
        self.bottom = bottom
    }
    
    // MARK: IGListDiffable
    
    public func diffIdentifier() -> NSObjectProtocol {
        return id as NSObjectProtocol
    }
    
    public func isEqual(toDiffableObject object: IGListDiffable?) -> Bool {
        guard self !== object else { return true }
        guard let object = object as? VHCBMIInput else { return false }
        return id == object.id
    }
}

public struct VHCMetric: CaptureMetric {
    var healthAttribute: VHCHealthAttribute
    var validValues = [VHCHealthAttributeValidValues]()
    public var units: [Unit] = [Unit]()

    public init(healthAttribute: VHCHealthAttribute) {
        self.healthAttribute = healthAttribute
        let unitsOfMeasure = self.healthAttribute.validValues.flatMap({ $0.unitOfMeasureType })
        self.units = VHCMetric.configureUnits(for: Array(unitsOfMeasure))
    }

    func validate(_ input: String?,
                  _ unit: Unit?,
                  _ unitOfMeasureType: inout UnitOfMeasureRef,
                  _ min: inout Double?,
                  _ max: inout Double?) -> Bool {

        guard let validString = input?.localizeDecimalFormat(), let validInput = Localization.decimalFormatter.number(from: validString)?.doubleValue else {
            debugPrint("Missing parameters in valid values")
            return false
        }

        guard let parameters = self.healthAttribute.validValues.filter({ $0.unitOfMeasureType.unit().symbol == unit?.symbol }).first else {
            debugPrint("Missing parameters in valid values")
            return false
        }

        var minResult = false
        var maxResult = false
        unitOfMeasureType = parameters.unitOfMeasureType

        var numberOfDecimalPlaces: Int {
            guard let number = VIAApplicableFeatures.default.numberOfDecimalPlaces else {
                return 1
            }
            
            return number
        }
        
        // check min
        if let validMinValue = Double(parameters.minValue)?.rounded(toPlaces: numberOfDecimalPlaces) {
            min = validMinValue
            minResult = validInput >= min!
        } else {
            debugPrint("No min value to validate against")
            minResult = true
        }

        // check max
        if let validMaxValue = Double(parameters.maxValue)?.rounded(toPlaces: numberOfDecimalPlaces) {
            max = validMaxValue
            maxResult = validInput <= max!
        } else {
            debugPrint("No max value to validate against")
            maxResult = true
        }

        return minResult && maxResult
    }

    func validateMultiUnit(_ input: String?,
                  _ unit: Unit?,
                  _ unitToValidate: UnitOfMeasureRef?,
                  _ unitOfMeasureType: inout UnitOfMeasureRef,
                  _ min: inout Double?,
                  _ max: inout Double?) -> Bool {
        
        guard let validString = input?.localizeDecimalFormat(), let validInput = Localization.decimalFormatter.number(from: validString)?.doubleValue else {
            debugPrint("Missing parameters in valid values")
            return false
        }
        
        guard let parameters = self.healthAttribute.validValues.filter({ $0.unitOfMeasureType.unit().symbol == unit?.symbol }).first else {
            debugPrint("Missing parameters in valid values")
            return false
        }
        
        var minResult = false
        var maxResult = false
        unitOfMeasureType = parameters.unitOfMeasureType
        
        // check min
        if let validMinValue = Double(parameters.minValue) {
            if unitToValidate == UnitOfMeasureRef.Foot && (unitOfMeasureType == .FootInch || unitOfMeasureType == .StonePound) {
                let validMinFootValue = String(validMinValue).characters.split(separator: ".")
                if validMinFootValue.count > 0{
                    min = Localization.decimalFormatter.number(from: String(validMinFootValue[0]))?.doubleValue ?? 0
                }
            }else{
                min = validMinValue
            }
            minResult = validInput >= min!
        } else {
            debugPrint("No min value to validate against")
            minResult = true
        }
        
        // check max
        if let validMaxValue = Double(parameters.maxValue) {
            max = validMaxValue
            maxResult = validInput <= max!
        } else {
            debugPrint("No max value to validate against")
            maxResult = true
        }
        
        return minResult && maxResult
    }

}

protocol CaptureResultsViewModel {
    var metrics: [VHCMetric] {get}
    var vhcRealm: Realm {get}
    var healthAttributeGroups: Array<VHCHealthAttributeGroup>? {get}
    var healthAttributes: Results<VHCHealthAttribute>? {get}
    var capturedResults: Results<VHCCapturedResult> { get }

    func configureAllMetrics()
}

public class VHCCaptureResultsViewModel: CaptureResultsViewModel {

    // MARK: Properties

    var metrics = [VHCMetric]()

    let vhcRealm = DataProvider.newVHCRealm()

    var healthAttributeGroups: Array<VHCHealthAttributeGroup>?

    var healthAttributes: Results<VHCHealthAttribute>?

    var BMIValid: Bool = true
    
    var CholesterolValid: Bool = true

    internal lazy var capturedResults: Results<VHCCapturedResult> = {
        return self.vhcRealm.objects(VHCCapturedResult.self)
    }()

    lazy var earliestCaptureDate: Date = {
        return VIAApplicableFeatures.default.setMinimumDate() ?? Date()
    }()

    // MARK: Lifecycle

    init() {
        configureAllMetrics()
    }

    // MARK: Data

    var data: [Any] = {
        let vhcRealm = DataProvider.newVHCRealm()

        let bigPadding: CGFloat = 50
        let mediumPadding: CGFloat = 35
        let smallPadding: CGFloat = 20
        let tinyPadding: CGFloat = 15
        let tinierPadding: CGFloat = 10
        let minutePadding: CGFloat = 5

        let instruction = CommonStrings.CaptureResults.Intro1Title142
        let instructionContent = CommonStrings.CaptureResults.Intro1Message143

        var data: [Any] = [
            SpacerDetail(mediumPadding),
            TextDetail(text: instruction, font: UIFont.headlineFont(), textColor: UIColor.night()),
            SpacerDetail(tinierPadding),
            TextDetail(text: instructionContent, font: UIFont.footnoteFont(), textColor: UIColor.darkGrey())
        ]

        let groups = VHCHealthAttributeGroup.sortedGroups(from: vhcRealm)
        data.append(SpacerDetail(bigPadding)) // padding for first group
        groups.forEach({ group in
            data.append(group)
            data.append(SpacerDetail(smallPadding))
            
            var healthAttributesToAppend = [VHCHealthAttribute]()
            
            if group.type == .VHCBMI {
                healthAttributesToAppend = Array(group.healthAttributes).sorted(by: { $0.type.vhcBMICaptureSortOrder() < $1.type.vhcBMICaptureSortOrder() })
            } else {
                healthAttributesToAppend = Array(group.healthAttributes)
                
                if let hideCholesterolRatio = VIAApplicableFeatures.default.hideCholesterolRatio, hideCholesterolRatio{
                    if let ratio = group.healthAttribute(of: .LipidRatio){
                        healthAttributesToAppend = healthAttributesToAppend.filter({ $0.type != ratio.type} )
                    }
                }
            }

            // treat blood pressure differently
            if group.containsBothBloodPressureMeasurables() {
                if let systolic = group.healthAttribute(of: .BloodPressureSystol), let diastolic = group.healthAttribute(of: .BloodPressureDiasto) {
                    data.append(VHCBloodPressureInput(top: systolic, bottom: diastolic))
                    data.append(SpacerDetail(mediumPadding))
                    healthAttributesToAppend = healthAttributesToAppend.filter({ $0.type != systolic.type && $0.type != diastolic.type })
                }
            }
            
            // treat BMI differently
            let heightValidValues = group.healthAttribute(of: .Height)?.validValues.map({ $0.unitOfMeasureType})
            let weightValidValues = group.healthAttribute(of: .Weight)?.validValues.map({ $0.unitOfMeasureType})

            if(heightValidValues?.contains(UnitOfMeasureRef.FootInch) ?? false){
                if let height = group.healthAttribute(of: .Height){
                    data.append(VHCBMIInput(top: height, bottom: height))
                    data.append(SpacerDetail(mediumPadding))
                    healthAttributesToAppend = healthAttributesToAppend.filter({ $0.type != height.type})
                }
            }
            if(weightValidValues?.contains(UnitOfMeasureRef.StonePound ) ?? false){
                if let weight = group.healthAttribute(of: .Weight){
                    data.append(VHCBMIInput(top: weight, bottom: weight))
                    data.append(SpacerDetail(mediumPadding))
                    healthAttributesToAppend = healthAttributesToAppend.filter({ $0.type != weight.type})
                }
            }
      
            // append health attributes
            for attribute in healthAttributesToAppend {
                // special exception to skip capturing BMI
                if attribute.type == PartyAttributeTypeRef.BMI {
                    continue
                }

                // if the health attribute's validvalues contains an option list
                // add an option list input, otherwise the normal single input
                if attribute.validValues.first?.validValuesList != nil {
                    data.append(VHCOptionListInput(healthAttribute: attribute))
                } else {
                    data.append(VHCSingleInput(healthAttribute: attribute))
                }

                if let footnote = attribute.captureFootnote() {
                    let textDetail = TextDetail(text: footnote, font: UIFont.footnoteFont(), textColor: UIColor.mediumGrey())
                    data.append(SpacerDetail(minutePadding))
                    data.append(textDetail)
                }
                data.append(SpacerDetail(mediumPadding))
            }

            data.append(SpacerDetail(tinyPadding)) // this plus mediumPadding makes the required 50 (bigPadding)
        })
        data.append(SpacerDetail(bigPadding))
        return data
    }()

    // MARK: - Metric configurations

    func configureAllMetrics() {
        let healthAttributeTypes: [PartyAttributeTypeRef] = [.BMI,
                                                             .Weight,
                                                             .Height,
                                                             .WaistCircumference,
                                                             .RandomGlucose,
                                                             .FastingGlucose,
                                                             .BloodPressureSystol,
                                                             .BloodPressureDiasto,
                                                             .HDLCholesterol,
                                                             .LDLCholesterol,
                                                             .Triglycerides,
                                                             .TotalCholesterol,
                                                             .HbA1c,
                                                             .UrinaryProtein]
        healthAttributes = vhcRealm.allVHCHealthAttributes()

        guard let healthAttributes = healthAttributes else {
            debugPrint("No health attributes to configure metrics for")
            return
        }

        healthAttributes.forEach({ healthAttribute in
            if healthAttributeTypes.contains(healthAttribute.type) {
                metrics.append(VHCMetric(healthAttribute: healthAttribute))
            }
        })
    }

    // MARK: CapturedResults

    func capturedResult(for healthAttributeType: PartyAttributeTypeRef) -> VHCCapturedResult? {
        let result = self.capturedResults.filter({ $0.healthAttributeType == healthAttributeType }).first
        return result
    }

    // MARK: VHC captured input validation
    /*
     If either weight or height is entered by the user
     both values are required to be input by the user.
     
     First get the latest input from the user from the
     vhcCapturedResults items to see if the item was
     successfully writen to realm.
     Then check if the item is of health attribute type
     Weight or Height
     if this is true check the vhcCapturedResults items
     for n item with the opposite health attribute type.
     If both items are contained within realm BMI is
     fully inputted by the user
     */

    func checkIfBMIIsValid() {
        var weightCapturedResult = false
        var heightCapturedResult = false
        var weightDateCapturedResult = false
        var heightDateCapturedResult = false

        if VIAApplicableFeatures.default.withDefaultStringValueForVHCCapturedDate() {
            if vhcRealm.vhcCapturedResult(of: .Weight) != nil && vhcRealm.vhcCapturedResult(of: .Weight)?.dateCaptured != NSDate.distantPast {
                weightDateCapturedResult = true
            }
            
            if vhcRealm.vhcCapturedResult(of: .Height) != nil && vhcRealm.vhcCapturedResult(of: .Height)?.dateCaptured != NSDate.distantPast {
                heightDateCapturedResult = true
            }
            
            weightCapturedResult = weightDateCapturedResult ? true : false
            heightCapturedResult = heightDateCapturedResult ? true : false
            
        } else {
        
            if let wCapturedResult = vhcRealm.vhcCapturedResult(of: .Weight) {
                weightCapturedResult = wCapturedResult.isValid
            }
            
            if let hCapturedResult = vhcRealm.vhcCapturedResult(of: .Height) {
                heightCapturedResult = hCapturedResult.isValid
            }
        }
        
        // Only one value captured = invalid
        // Both or none captured = valid
        if (weightCapturedResult && heightCapturedResult) || (!weightCapturedResult && !heightCapturedResult) {
            self.BMIValid = true
        } else {
            self.BMIValid = false
        }
    }
    
    func checkIfCholesterolIsValid() {
        let totalCholesterolCapturedResult = vhcRealm.vhcCapturedResult(of: .TotalCholesterol)
        let HDLCapturedResult = vhcRealm.vhcCapturedResult(of: .HDLCholesterol)
        
        // Only one value captured = invalid
        // Both or none captured = valid
        if ((totalCholesterolCapturedResult != nil && HDLCapturedResult == nil) ||
            (totalCholesterolCapturedResult == nil && HDLCapturedResult != nil)) {
            self.CholesterolValid = false
        } else {
            self.CholesterolValid = true
        }
    }

    func bloodPressureCapturedCompletely() -> Bool {
        let systolic = self.capturedResults.filter({ $0.healthAttributeType == PartyAttributeTypeRef.BloodPressureSystol }).first
        let diastolic = self.capturedResults.filter({ $0.healthAttributeType == PartyAttributeTypeRef.BloodPressureDiasto }).first
        return VIAApplicableFeatures.default.didVHCBloodPressureCapturedCompletely(systolic: systolic ?? VHCCapturedResult(), diastolic: diastolic ?? VHCCapturedResult())
    }
/**
     This method will remove the persisted unit that has a rawValue = 0.0
     rawValue = 0.0 is forced just to persist the unit of measure selected.
     The persisted unit of measure is used as the unit when Height and Weight
     section is reload under BMI
     
*/
    func removePersistedUnitOnly(){
        let realm = DataProvider.newVHCRealm()
        try! realm.write {
            for capturedResult in capturedResults {
                if capturedResult.rawInput == "0.0" &&
                    (capturedResult.healthAttributeType == .Height || capturedResult.healthAttributeType == .Weight){
                    realm.delete(realm.vhcCapturedResult(of: capturedResult.healthAttributeType)!)
                }
            }
            
        }
    }
    func capturedVHCIsValid() -> Bool {
        
        removePersistedUnitOnly()
        
        return VIAApplicableFeatures.default.isCapturedVHCIsValid(bloodPressureValid: bloodPressureCapturedCompletely(), capturedResults: self.capturedResults)
    }

}
