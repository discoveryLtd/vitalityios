import IGListKit
import VitalityKit
import VIAUtilities
import VIAUIKit

public class VHCCaptureResultsGroupHeadingSectionController: IGListSectionController, IGListSectionType {

    // MARK: Properties

    public var detail: VHCHealthAttributeGroup!

    // MARK: IGListSectionType

    public func numberOfItems() -> Int {
        return self.detail.captureInstruction().isEmpty ? 1 : 3 // 1 - only a heading, 3 - heading, spacer, captureInstruction
    }

    public func didUpdate(to object: Any) {
        detail = object as! VHCHealthAttributeGroup
    }

    public func cellForItem(at index: Int) -> UICollectionViewCell {
        if index == 0 {
            return headingCell(at: index)
        }
        if index == 1 {
            return spacingCell(at: index)
        }
        return messageCell(at: index)
    }

    public func headingCell(at index: Int) -> UICollectionViewCell {
        let cell = collectionContext!.dequeueReusableCell(withNibName: VHCCaptureResultsMeasurableHeadingCell.defaultReuseIdentifier, bundle: VHCCaptureResultsMeasurableHeadingCell.bundle(), for: self, at: index) as! VHCCaptureResultsMeasurableHeadingCell
        cell.title = self.detail.groupName()
        cell.image = self.detail.assetImage?.templatedImage
        return cell
    }

    public func spacingCell(at index: Int) -> UICollectionViewCell {
        let cell = collectionContext!.dequeueReusableCell(of: UICollectionViewCell.self, for: self, at: index)
        return cell
    }

    public func messageCell(at index: Int) -> UICollectionViewCell {
        let cell = collectionContext!.dequeueReusableCell(of: TextCell.self, for: self, at: index) as! TextCell
        cell.label.text = self.detail.captureInstruction()
        cell.label.numberOfLines = 0
        cell.label.font = UIFont.footnoteFont()
        cell.label.textColor = UIColor.darkGrey()
        return cell
    }

    public func sizeForItem(at index: Int) -> CGSize {
        let width = collectionContext!.containerSize.width
        var height: CGFloat = 0

        if index == 0 {
            height = 28 // heading is fixed height
        } else if index == 1 {
            height = 10 // fixed space between header and instruction
        } else if index == 2 {
            height = heightForMessageCell(width: width)
        }
        return CGSize(width: collectionContext!.containerSize.width, height: height)
    }

    public func heightForMessageCell(width: CGFloat) -> CGFloat {
        let insets = UIEdgeInsets(top: 0, left: TextCell.inset, bottom: 0, right: TextCell.inset)
        let size = TextSize.size(self.detail.captureInstruction(), font: UIFont.footnoteFont(), width: width, insets: insets)
        return size.height
    }

    public func didSelectItem(at index: Int) { }

}
