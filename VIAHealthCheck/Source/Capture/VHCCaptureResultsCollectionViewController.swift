import UIKit
import VIAUIKit
import VitalityKit
import RealmSwift
import IGListKit
import VIACommon
import VIAImageManager

public extension NSNotification.Name {
    static let VIAVHCInputDidTogglePickerNotification = Notification.Name("VIAVHCInputDidTogglePickerNotification")
}

public protocol VHCCaptureResultsDelegate: class {
    func show(menu: UIAlertController)
    func togglePicker()
    func removeKeyboard()
}

public final class VHCCaptureResultsCollectionViewController: VIAViewController, IGListAdapterDataSource, KnowYourHealthTintable {

    // MARK: Properties

    public private(set) var viewModel = VHCCaptureResultsViewModel()

    let collectionView: IGListCollectionView = {
        let collectionView = IGListCollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
        collectionView.backgroundColor = UIColor.tableViewBackground()
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        return collectionView
    }()

    lazy var adapter: IGListAdapter = {
        return IGListAdapter(updater: IGListAdapterUpdater(), viewController: self, workingRangeSize: 0)
    }()

    var metrics = [VHCMetric]()
    var datePicker: UIDatePicker!
    let realm = DataProvider.newRealm()
    var realmHealthAttributeGroups: Results<VHCHealthAttributeGroup>?
    var realmHealthAttributes: Results<VHCHealthAttribute>?

    // MARK: View lifecycle

    override public func viewDidLoad() {
        super.viewDidLoad()

        title = CommonStrings.Onboarding.Section2Title128
        addObserver()
        configureAppearance()
        configureCollectionView()
        configureAdapter()
        addBarButtonItems()
        configureAppearance()
    }

    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        resetBackButtonTitle()
        setInitialNextButtonState()
        self.configureKeyboardAutoToolbar(VIAApplicableFeatures.default.enableKeyboardAutoToolbar ?? false)
    }

    func setInitialNextButtonState() {
        self.navigationItem.rightBarButtonItem?.isEnabled = self.viewModel.capturedVHCIsValid()
    }

    public override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        self.hideBackButtonTitle()
        self.disableKeyboardAutoToolbar()
    }

    // MARK: View config

    func configureAppearance() {
        navigationController?.makeNavigationBarTransparent()
    }

    func configureCollectionView() {
        view.addSubview(collectionView)
        collectionView.snp.makeConstraints { (make) in
            make.top.left.bottom.right.equalToSuperview()
        }
    }

    func configureAdapter() {
        adapter.collectionView = collectionView
        adapter.dataSource = self
    }

    func addBarButtonItems() {
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: CommonStrings.CancelButtonTitle24, style: .plain, target: self, action: #selector(cancel(_:)))
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: CommonStrings.NextButtonTitle84, style: .plain, target: self, action: #selector(next(_:)))
    }

    // MARK: - Navigation

    func showValidValuesOptionList(data: (String, VHCHealthAttributeValidValues, String?, VHCCaptureResultsValidValuesListViewControllerDelegate)) {
        self.performSegue(withIdentifier: "showValidValuesOptionList", sender: data)
    }

    func cancel(_ sender: Any?) {
//        delete captured results
//        let realm = DataProvider.newVHCRealm()
//        try! realm.write {
//            realm.delete(realm.allVHCCapturedResults())
//        }
        self.performSegue(withIdentifier: "unwindToVHCLanding", sender: nil)
    }

    public override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "addProof", let destination = segue.destination as? SelectedPhotosCollectionViewController {
            guard let configuration = VIAApplicableFeatures.default.getImageControllerConfiguration() as? UIImagePickerController.ImageControllerConfiguration else { return }
            
            var viewModel = SelectedPhotosCollectionViewModel()
            viewModel.configuration = configuration
            
            destination.viewModel = viewModel
            destination.delegate = self
            
        } else if segue.identifier == "showValidValuesOptionList", let destination = segue.destination as? VHCCaptureResultsValidValuesListViewController {
            if let data = sender as? (String, VHCHealthAttributeValidValues, String?, VHCCaptureResultsValidValuesListViewControllerDelegate) {
                let viewModel = VHCCaptureResultsValidValuesListViewModel(items: data.1.attributedStringsSemiColonSeperatedValueList(), initialItem: data.2)
                destination.viewModel = viewModel
                // this tuple is getting ugly
                destination.title = data.0
                destination.delegate = data.3
            }
        }
    }
    
    func next(_ sender: Any?) {
        self.view.endEditing(true)
        
        VIAApplicableFeatures.default.onValidateCapturedResults(isBMIValid: self.viewModel.BMIValid, isCholesterolValid: self.viewModel.CholesterolValid, areInputsValid: self.viewModel.capturedVHCIsValid(), onError:{alertNotFullyCaptured()} , onNext: {gotoAddProof()})
    }

    func alertNotFullyCaptured() {
        let cancelAction = UIAlertAction(title: CommonStrings.CancelButtonTitle24, style: .cancel, handler: nil)
        let continueAction = UIAlertAction(title: CommonStrings.ContinueButtonTitle87, style: .default, handler: { (alert) in
            self.gotoAddProof()
        })
        let alert = UIAlertController(title: CommonStrings.Measurement.BodyMassIndexTitle134, message: CommonStrings.CaptureResults.BodyMassIndexAlertMessage162, preferredStyle: .alert)
        alert.addAction(cancelAction)
        alert.addAction(continueAction)
        self.present(alert, animated: true, completion: nil)
    }

    func gotoAddProof() {
        self.performSegue(withIdentifier: "addProof", sender: self)
    }

    @IBAction public func unwindToCaptureResults(segue: UIStoryboardSegue) {
        debugPrint("unwindToCaptureResults")
    }

    // MARK: IGListAdapterSource

    public func emptyView(for listAdapter: IGListAdapter) -> UIView? {
        return nil
    }

    public func listAdapter(_ listAdapter: IGListAdapter, sectionControllerFor object: Any) -> IGListSectionController {
        if object is VHCHealthAttributeGroup {
            return VHCCaptureResultsGroupHeadingSectionController()
        }
        if object is VHCSingleInput {
            return VHCSingleMeasurableInputSectionController() // With datePickerCell
        }
        if object is VHCOptionListInput {
            return VHCOptionListInputSectionController() // With datePickerCell
        }
        if object is VHCBloodPressureInput {
            return VHCBloodPressureInputSectionController() // With datePickerCell
        }
        if object is TextDetail {
            return TextSectionController()
        }
        if object is VHCBMIInput{
            return VHCBMIInputSectionController() // With datePickerCell
        }
        return SpacingController()
    }

    public func objects(for listAdapter: IGListAdapter) -> [IGListDiffable] {
        return self.viewModel.data as! [IGListDiffable]
    }

    // MARK: Successful health attribute capture

    func addObserver() {
        NotificationCenter.default.addObserver(self, selector: #selector(respondToToSuccessfulHealthAttributeCaptured), name: .VIASuccessfullyCapturedVHCHealthAttribute, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(validateHealthAttributeCaptured), name: .VIAValidateVHCHealthAttribute, object: nil)
    }

    func respondToToSuccessfulHealthAttributeCaptured(from notification: Notification) {
        self.viewModel.checkIfBMIIsValid()
        self.viewModel.checkIfCholesterolIsValid()
        self.updateNextEnabled()
    }
    
    func validateHealthAttributeCaptured(from notification: Notification) {
        self.viewModel.checkIfBMIIsValid()
        self.viewModel.checkIfCholesterolIsValid()
        self.updateNextEnabled()
    }

    func updateNextEnabled() {
        self.navigationItem.rightBarButtonItem?.isEnabled = self.viewModel.capturedVHCIsValid()
    }
}

extension VHCCaptureResultsCollectionViewController: SelectedPhotosCollectionViewDelegate{
    
    public func onPerformSegue(){
        self.performSegue(withIdentifier: "showSummary", sender: self)
    }
}
