import VIACommon
import IGListKit
import VIAUIKit
import VIAUtilities
import VitalityKit
import VitalityKit


public struct VHCHealthAttributeGroupLearnMoreContent {
    var groupName: String = ""
    var templateImage: UIImage? = UIImage()
    var section1Title: String = ""
    var section1Content: String = ""
    var section1Icon: UIImage = UIImage()
    var section2Title: String = ""
    var section2Content: String = ""
    var section2Icon: UIImage = VIAHealthCheckAsset.vhcIcnTipsSmall.image
    var section3Title: String = ""
    var section3Content: String = ""
    var section3Icon: UIImage = VIAHealthCheckAsset.vhcGenericPointsLarge.templateImage
}

protocol VHCLearnMoreContentProtocol {
    func groupName() -> String
    func section1Title() -> String
    func section1Content() -> String
    func section2Title() -> String
    func section2Content() -> String
    func section3Title() -> String
    func section3Content(party: VitalityParty?) -> String
}

extension VHCHealthAttributeGroup: IGListDiffable {

    public func diffIdentifier() -> NSObjectProtocol {
        return self
    }

    public func isEqual(toDiffableObject object: IGListDiffable?) -> Bool {
        guard self !== object else { return true }
        guard let object = object as? VHCHealthAttributeGroup else { return false }
        return type == object.type
    }

    // MARK: Convenience

    public func captureInstruction() -> String {
        var content: String = ""
        switch self.type {
        case .VHCBloodGlucose:
            return CommonStrings.CaptureResults.BloodGlucoseMessage154
        case .VHCCholesterol:
            if let hideCholesterolContent = VIAApplicableFeatures.default.hideCholesterolContent,
                !hideCholesterolContent{
                content = CommonStrings.CaptureResults.CholesterolMessage155
            }
            return content
        case .VHCWaistCircum:
            return CommonStrings.CaptureResults.WaistCircumferenceMessage153
        default:
            return ""
        }
    }

    var assetImage: UIImage? {
        switch self.type {
        case .VHCBMI:
            return VIAHealthCheckAsset.vhcGenericBMI.image
        case .VHCWaistCircum:
            return VIAHealthCheckAsset.vhcGenericWaistCircumference.image
        case .VHCBloodGlucose:
            return VIAIconography.default.vhcGenericBloodGlucose
        case .VHCBloodPressure:
            return VIAHealthCheckAsset.vhcGenericBloodPressure.image
        case .VHCCholesterol:
            return VIAHealthCheckAsset.vhcGenericCholesterol.image
        case .VHCLipidProfile:
            return VIAHealthCheckAsset.vhcGenericCholesterol.image
        case .VHCHbA1c:
            return VIAHealthCheckAsset.vhcGenericHba1c.image
        case .VHCUrinaryProtein:
            return VIAHealthCheckAsset.vhcGenericUrineProtein.image
        default:
            return nil
        }
    }

    func learnMoreContent(party: VitalityParty?) -> VHCHealthAttributeGroupLearnMoreContent {
        let typeKey = self.type
        var content = VHCHealthAttributeGroupLearnMoreContent()
        content.groupName = self.groupName()
        content.templateImage = self.assetImage?.templatedImage

        if let image = content.templateImage {
            content.section1Icon = image
        }

        if let contentProvider = learnMoreContentProvider(for: typeKey) {
            content.groupName = contentProvider.groupName()
            content.section1Title = contentProvider.section1Title()
            content.section1Content = contentProvider.section1Content()
            content.section2Title = contentProvider.section2Title()
            content.section2Content = contentProvider.section2Content()
            content.section3Title = contentProvider.section3Title()
            content.section3Content = contentProvider.section3Content(party: party)
        }
        return content
    }

    func learnMoreContentProvider(for type: ProductFeatureTypeRef) -> VHCLearnMoreContentProtocol? {
        switch type {
        case .VHCBMI:
            return BodyMassIndexLearnMore()
        case .VHCWaistCircum:
            return WaistCircumferenceLearnMore()
        case .VHCBloodGlucose:
            return GlucoseLearnMore()
        case .VHCBloodPressure:
            return BloodPressureLearnMore()
        case .VHCCholesterol:
            return CholesterolLearnMore()
        case .VHCHbA1c:
            return HbA1cLearnMore()
        case .VHCUrinaryProtein:
            return UrineProteinLearnMore()
        case .VHCLipidProfile:
            return OtherBloodLipidsLearnMore()
        default:
            return nil
        }
    }

}

extension VHCHealthAttribute {

    public func captureFootnote() -> String? {
        switch self.type {
        case .LDLCholesterol:
            return CommonStrings.CaptureResults.CholesterolFootnoteMessage159
        case .HDLCholesterol:
            return CommonStrings.CaptureResults.CholesterolFootnoteMessage158
        default:
            return nil
        }
    }

}
