import IGListKit
import VIAUIKit
import VitalityKit
import VitalityKit
import VIAUtilities
import VIACommon

// MARK: 

class VHCOptionListInputSectionController: IGListSectionController, IGListSectionType, VHCCaptureResultsDelegate, VHCOptionListCaptureResultsCollectionViewCellDelegate, VHCCaptureResultsValidValuesListViewControllerDelegate {

    // MARK: Properties

    var showDatePicker = false

    var input: String?

    lazy var viewModel: VHCCaptureResultsViewModel? = {
        if let controller = self.viewController as? VHCCaptureResultsCollectionViewController {
            return controller.viewModel
        }
        return nil
    }()

    var capturedResult: VHCCapturedResult? {
        return self.viewModel?.capturedResult(for: self.detail.type)
    }

    var selectedDate = Date()

    var metric: VHCMetric? {
        didSet {
            debugPrint(self.metric as Any)
        }
    }

    var detail: VHCHealthAttribute! {
        didSet {
            self.metric = self.viewModel?.metrics.filter({ $0.healthAttribute.type == self.detail.type }).first
            if VIAApplicableFeatures.default.withDefaultStringValueForVHCCapturedDate() {
                self.selectedDate = self.capturedResult?.dateCaptured ?? NSDate.distantPast
            } else {
                self.selectedDate = self.capturedResult?.dateCaptured ?? Date()
            }
        }
    }

    weak var inputCell: VHCOptionListCaptureResultsCollectionViewCell?

    // MARK: Init

    public override init() {

    }

    // MARK: IGListSectionType

    enum Cells: Int {
        case heading = 0
        case inputCell = 1
        case datePicker = 2
    }

    var sections: NSMutableIndexSet {
        let indexSet = NSMutableIndexSet()
        indexSet.add(Cells.inputCell.rawValue)
        if self.detail.inputHeading() != nil { indexSet.add(Cells.heading.rawValue) }
        if self.showDatePicker { indexSet.add(Cells.datePicker.rawValue) }
        return indexSet
    }

    public func numberOfItems() -> Int {
        return sections.count
    }

    public func didUpdate(to object: Any) {
        if let input = object as? VHCOptionListInput {
            detail = input.healthAttribute
        }
    }

    public func cellForItem(at index: Int) -> UICollectionViewCell {
        let integer = sections.integer(at: index)
        if integer == Cells.heading.rawValue {
            return headingCell(at: index)
        } else if integer == Cells.inputCell.rawValue {
            return inputCell(at: index)
        } else {
            return datePickerCell(at: index)
        }
    }

    func headingCell(at index: Int) -> UICollectionViewCell {
        let cell = collectionContext!.dequeueReusableCell(of: TextCell.self, for: self, at: index) as! TextCell
        cell.label.text = self.detail.inputHeading()
        cell.label.font = UIFont.subheadlineFont()
        cell.label.textColor = UIColor.night()
        return cell
    }

    func inputCell(at index: Int) -> VHCOptionListCaptureResultsCollectionViewCell {
        let cell = collectionContext!.dequeueReusableCell(withNibName: VHCOptionListCaptureResultsCollectionViewCell.defaultReuseIdentifier, bundle: VHCOptionListCaptureResultsCollectionViewCell.bundle(), for: self, at: index) as! VHCOptionListCaptureResultsCollectionViewCell
        cell.captureResultsDelegate = self

        // configure cell
        cell.configureCaptureResultCell(inputValue: self.capturedResult?.rawInput,
                                        selectedDate: self.selectedDate)

        cell.delegate = self
        self.inputCell = cell
        return cell
    }

    func datePickerCell(at index: Int) -> VIADatePickerCell {
        let cell = collectionContext?.dequeueReusableCell(withNibName: VIADatePickerCell.defaultReuseIdentifier, bundle: VIADatePickerCell.bundle(), for: self, at: index) as! VIADatePickerCell
        cell.didPickDate = self.datePickerDidPick
        cell.selectedDate = self.selectedDate
        cell.minimumDate = self.viewModel?.earliestCaptureDate ?? Date()

        return cell
    }

    public func sizeForItem(at index: Int) -> CGSize {
        let integer = sections.integer(at: index)
        if integer == Cells.heading.rawValue {
            return CGSize(width: collectionContext!.containerSize.width, height: 40)
        } else if integer == Cells.inputCell.rawValue {
            return CGSize(width: collectionContext!.containerSize.width, height: 96)
        } else {
            return CGSize(width: collectionContext!.containerSize.width, height: 200)
        }
    }

    public func didSelectItem(at index: Int) { }

    // MARK: CaptureResultsDelegate

    public func show(menu: UIAlertController) {
        if let controller = self.viewController {
            controller.present(menu, animated: true, completion: nil)
        }
    }

    public func togglePicker() {
        
        if VIAApplicableFeatures.default.withDefaultStringValueForVHCCapturedDate() {
            if self.selectedDate == NSDate.distantPast {
                self.selectedDate = Date()
                self.inputCell?.updateDateButtonTitle(self.selectedDate)
                self.persist()
            }
        }

        self.showDatePicker = !self.showDatePicker
        if self.showDatePicker {
            self.collectionContext?.insert(in: self, at: IndexSet(integer: sections.count - 1))
        } else {
            self.collectionContext?.delete(in: self, at: IndexSet(integer: sections.count))
        }
    }

    public func removeKeyboard() {
        NotificationCenter.default.post(name: .VHCCaptureResultsTextFieldShouldResignFirstResponder, object: nil)
    }

    // MARK: VHCOptionListCaptureResultsCollectionViewCellDelegate

    func shouldDisplayOptionList() {
        guard let controller = self.viewController as? VHCCaptureResultsCollectionViewController else { return }
        guard let validValue = self.detail.validValues.first, validValue.validValuesList != nil else { return }
        let title = self.detail.title() ?? ""
        controller.showValidValuesOptionList(data: (title, validValue, self.capturedResult?.rawInput, self))
    }

    // MARK: DatePicker

    func datePickerDidPick(date: Date) {
        self.selectedDate = date
        self.inputCell?.updateDateButtonTitle(self.selectedDate)
        self.persist()
    }

    // MARK: Persist

    func persist() {
        if VIAApplicableFeatures.default.withDefaultStringValueForVHCCapturedDate() {
            vhcPersistInput(self.input, isValid: true, for: self.detail, on: self.selectedDate)
        } else {
            vhcPersistInput(self.input, isValid: true, for: self.detail, on: self.selectedDate, unit: nil, unitOfMeasureType: nil)
        }
    }

    // MARK: VHCCaptureResultsValidValuesListViewControllerDelegate

    func didSelectionOption(option: VHCValidValueOption?) {
        self.input = option?.value
        vhcPersistInput(self.input, isValid: true, for: self.detail, on: self.selectedDate)
        self.collectionContext?.reload(in: self, at: IndexSet(integer: 0))
    }

}
