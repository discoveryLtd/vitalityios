import IGListKit
import VIAUIKit
import VitalityKit
import VitalityKit
import VIAUtilities
import VIACommon

// MARK: 

class VHCSingleMeasurableInputSectionController: IGListSectionController, IGListSectionType, VHCCaptureResultsDelegate {

    // MARK: Properties

    var showDatePicker = false

    var selectedUnit: Unit?

    var input: String?

    var isValid: Bool = false

    var selectedUnitOfMeasureType: UnitOfMeasureRef?

    lazy var viewModel: VHCCaptureResultsViewModel? = {
        if let controller = self.viewController as? VHCCaptureResultsCollectionViewController {
            return controller.viewModel
        }
        return nil
    }()

    var capturedResult: VHCCapturedResult? {
        return self.viewModel?.capturedResult(for: self.detail.type)
    }

    var selectedDate = Date()

    var metric: VHCMetric? {
        didSet {
            debugPrint(self.metric as Any)
        }
    }

    var detail: VHCHealthAttribute! {
        didSet {
            self.metric = self.viewModel?.metrics.filter({ $0.healthAttribute.type == self.detail.type }).first
            if VIAApplicableFeatures.default.withDefaultStringValueForVHCCapturedDate() {
                self.selectedDate = self.capturedResult?.dateCaptured ?? NSDate.distantPast
            } else {
                self.selectedDate = self.capturedResult?.dateCaptured ?? Date()
            }
        }
    }

    weak var inputCell: VHCSingleCaptureResultsCollectionViewCell?

    // MARK: Init

    public override init() {
        super.init()
        NotificationCenter.default.addObserver(self, selector: #selector(closeDatePicker(_:)), name: .VIAVHCInputDidTogglePickerNotification, object: nil)
    }

    deinit {
        debugPrint("VHCSingleMeasurableInputSectionController deinit")
    }

    // MARK: IGListSectionType

    enum Cells: Int {
        case heading = 0
        case inputCell = 1
        case datePicker = 2
    }

    var sections: NSMutableIndexSet {
        let indexSet = NSMutableIndexSet()
        indexSet.add(Cells.inputCell.rawValue)
        if self.detail.inputHeading() != nil { indexSet.add(Cells.heading.rawValue) }
        if self.showDatePicker { indexSet.add(Cells.datePicker.rawValue) }
        return indexSet
    }

    public func numberOfItems() -> Int {
        return sections.count
    }

    public func didUpdate(to object: Any) {
        if let input = object as? VHCSingleInput {
            detail = input.healthAttribute
        }
    }

    public func cellForItem(at index: Int) -> UICollectionViewCell {
        let integer = sections.integer(at: index)
        if integer == Cells.heading.rawValue {
            return headingCell(at: index)
        } else if integer == Cells.inputCell.rawValue {
            return inputCell(at: index)
        } else {
            return datePickerCell(at: index)
        }
    }

    func headingCell(at index: Int) -> UICollectionViewCell {
        let cell = collectionContext!.dequeueReusableCell(of: TextCell.self, for: self, at: index) as! TextCell
        cell.label.text = self.detail.inputHeading()
        cell.label.font = UIFont.subheadlineFont()
        cell.label.textColor = UIColor.night()
        return cell
    }

    func inputCell(at index: Int) -> VHCSingleCaptureResultsCollectionViewCell {
        let cell = collectionContext!.dequeueReusableCell(withNibName: VHCSingleCaptureResultsCollectionViewCell.defaultReuseIdentifier, bundle: VHCSingleCaptureResultsCollectionViewCell.bundle(), for: self, at: index) as! VHCSingleCaptureResultsCollectionViewCell
        cell.captureResultsDelegate = self
        cell.healthAttributeType = self.detail.type
        
        // textFieldTextDidChange
        cell.textFieldTextDidChange = { [unowned self] textField, unit, min, max in
            var uomType = UnitOfMeasureRef.Unknown
            if let metric = self.metric, metric.validate(textField.text, unit, &uomType, &min, &max) {
                cell.updateInputValid(true)
            }
            
            if VIAApplicableFeatures.default.withDefaultStringValueForVHCCapturedDate() {
                if let isValid = self.metric?.validate(textField.text, unit, &uomType, &min, &max) {
                    self.isValid = isValid
                    self.selectedUnitOfMeasureType = uomType
                    self.selectedUnit = unit
                    self.input = textField.text
                    self.persist()
                }
            }
        }

        // textFieldDidEndEditing
        cell.textFieldDidEndEditing = { [unowned self] textField, unit, min, max in
            var uomType = UnitOfMeasureRef.Unknown
            
            guard let _ = textField.text  else {
                self.persist()
                return
            }
            if let isValid = self.metric?.validate(textField.text, unit, &uomType, &min, &max) {
                self.isValid = isValid
                self.selectedUnitOfMeasureType = uomType
                self.selectedUnit = unit
                self.input = textField.text
                self.persist()
                
                if !(((textField.text ?? "").isEmpty)){
                    cell.updateInputValid(isValid)
                }
            }
        }

        // configure cell
        cell.configureCaptureResultCell(units: self.metric?.units ?? [],
                                        inputPlaceholder: self.detail.title(),
                                        inputText: self.capturedResult?.rawInput,
                                        inputUnitSymbol: self.capturedResult?.inputUnitSymbol,
                                        selectedDate: self.selectedDate)
        
        if self.capturedResult?.rawInput != nil && (self.capturedResult?.isValid ?? false){
            cell.setValidationErrorText("", for: cell.validationErrorLabel)
        }else{
//            if let showReminderMessage = VIAApplicableFeatures.default.showReminderMessage, showReminderMessage {
//                let title = detail.title()
//                if title == "Total cholesterol" || (title == "HDL" && AppSettings.getAppTenant() != .UKE) {
//                    cell.setValidationErrorText(CommonStrings.ErrorRequiredForPoints1170, for: cell.validationErrorLabel)
//                }
//            }
            if VIAApplicableFeatures.default.shouldShowReminderMessageOnVHC(attribute: detail.type) {
                cell.setValidationErrorText(CommonStrings.ErrorRequiredForPoints1170, for: cell.validationErrorLabel)
            }
        }
        
        self.inputCell = cell
        return cell
    }

    func datePickerCell(at index: Int) -> VIADatePickerCell {
        let cell = collectionContext?.dequeueReusableCell(withNibName: VIADatePickerCell.defaultReuseIdentifier, bundle: VIADatePickerCell.bundle(), for: self, at: index) as! VIADatePickerCell
        cell.didPickDate = self.datePickerDidPick
        cell.selectedDate = self.selectedDate
        cell.minimumDate = self.viewModel?.earliestCaptureDate ?? Date()
        return cell
    }

    public func sizeForItem(at index: Int) -> CGSize {
        let integer = sections.integer(at: index)
        if integer == Cells.heading.rawValue {
            return CGSize(width: collectionContext!.containerSize.width, height: 40)
        } else if integer == Cells.inputCell.rawValue {
            return CGSize(width: collectionContext!.containerSize.width, height: 96)
        } else {
            return CGSize(width: collectionContext!.containerSize.width, height: 200)
        }
    }

    public func didSelectItem(at index: Int) { }

    // MARK: CaptureResultsDelegate

    public func show(menu: UIAlertController) {
        if let controller = self.viewController {
            controller.present(menu, animated: true, completion: nil)
        }
    }

    public func togglePicker() {
        NotificationCenter.default.post(name: .VIAVHCInputDidTogglePickerNotification, object: self)
        
        if VIAApplicableFeatures.default.withDefaultStringValueForVHCCapturedDate() {
            if self.selectedDate == NSDate.distantPast {
                self.selectedDate = Date()
                self.inputCell?.updateDateButtonTitle(self.selectedDate)
                if self.isValid {
                    self.persist()
                }
            }
        }
        
        self.showDatePicker = !self.showDatePicker
        updatePickerState()
    }

    func updatePickerState() {
        if self.showDatePicker {
            self.collectionContext?.insert(in: self, at: IndexSet(integer: sections.count - 1))
        } else {
            self.collectionContext?.delete(in: self, at: IndexSet(integer: sections.count))
        }
    }

    public func removeKeyboard() {
        NotificationCenter.default.post(name: .VHCCaptureResultsTextFieldShouldResignFirstResponder, object: nil)
    }

    // MARK: DatePicker

    func datePickerDidPick(date: Date) {
        self.selectedDate = date
        self.inputCell?.updateDateButtonTitle(self.selectedDate)
        self.persist()
    }

    func closeDatePicker(_ notification: Any?) {
        guard let notification = notification as? Notification else { return }
        guard let object = notification.object as? VHCCaptureResultsDelegate else { return }

        if object !== self && self.showDatePicker {
            self.showDatePicker = false
            updatePickerState()
        }
    }

    // MARK: Persist

    func persist() {
        
        if VIAApplicableFeatures.default.persistVHCData(){
            if self.input != "" {
                
                vhcPersistInput(self.input, isValid: self.isValid, for: self.detail, on: self.selectedDate, unit: self.selectedUnit, unitOfMeasureType: self.selectedUnitOfMeasureType)
            }
            else if self.input == "" && self.capturedResult?.rawInput != "" {
                let realm = DataProvider.newVHCRealm()
                try! realm.write {
                    if let deletedResult = realm.vhcCapturedResult(of: self.detail.type) {
                        realm.delete(deletedResult)
                    }
                }
            }
        }else{
            vhcPersistInput(self.input, isValid: self.isValid, for: self.detail, on: self.selectedDate, unit: self.selectedUnit, unitOfMeasureType: self.selectedUnitOfMeasureType)
        }
    }

}
