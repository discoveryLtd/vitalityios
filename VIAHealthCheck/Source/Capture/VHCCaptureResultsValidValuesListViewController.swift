import Foundation
import UIKit
import VIAUIKit
import VitalityKit
import VIAUtilities
import VIACommon

typealias VHCValidValueOption = (value: String, attributedString: NSAttributedString)

protocol VHCCaptureResultsValidValuesListViewControllerDelegate: class {
    func didSelectionOption(option: VHCValidValueOption?)
}

final class VHCCaptureResultsValidValuesListViewController: VIATableViewController, KnowYourHealthTintable {

    // MARK: Properties

    weak var delegate: VHCCaptureResultsValidValuesListViewControllerDelegate?

    var viewModel: VHCCaptureResultsValidValuesListViewModel = VHCCaptureResultsValidValuesListViewModel(items: [], initialItem: nil)

    // MARK: View lifecycle

    override public func viewDidLoad() {
        super.viewDidLoad()

        configureAppearance()
        configureTableView()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)


        if isMovingFromParentViewController {
            delegate?.didSelectionOption(option: viewModel.selectedItem)
        }
    }

    // MARK: View config

    func configureTableView() {
        tableView.register(VHCCaptureResultsValidValueCell.self, forCellReuseIdentifier: VHCCaptureResultsValidValueCell.defaultReuseIdentifier)
        tableView.register(VIATableViewCell.nib(), forCellReuseIdentifier: VIATableViewCell.defaultReuseIdentifier)
        tableView.estimatedRowHeight = 70
    }

    func configureAppearance() {
        navigationController?.makeNavigationBarTransparent()
    }

    // MARK: TableView

    public override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let reversed = VIAApplicableFeatures.default.reverseValidValuesOptionList, reversed{
            return getReversedList(numberOfRowsInSection: section)
        }else{
            return getNormalList(numberOfRowsInSection: section)
        }
    }

    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 35
    }

    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNonzeroMagnitude
    }

    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return UIView()
    }

    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return nil
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let reversed = VIAApplicableFeatures.default.reverseValidValuesOptionList, reversed{
            return getReversedList(cellForRowAt: indexPath)
        }else{
            return getNormalList(cellForRowAt: indexPath)
        }
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let reversed = VIAApplicableFeatures.default.reverseValidValuesOptionList, reversed{
            reversedList(didSelectRowAt: indexPath)
        }else{
            normalList(didSelectRowAt: indexPath)
        }

        tableView.reloadData()
        self.navigationController?.popViewController(animated: true)
    }

}

class VHCCaptureResultsValidValuesListViewModel {

    var initialItem: String?

    var selectedItem: VHCValidValueOption?

    var items: [VHCValidValueOption]

    init(items: [VHCValidValueOption], initialItem: String?) {
        self.items = items
        self.initialItem = initialItem
        self.selectedItem = self.items.filter({ $0.0 == initialItem }).first
    }

}

fileprivate class VHCCaptureResultsValidValueCell: UITableViewCell, Nibloadable {

    fileprivate static let inset: CGFloat = 15.0

    var answerIsSelected = false {
        didSet {
            toggleImageViewSelected()
        }
    }

    // MARK: Views

    lazy var label: UILabel = {
        let label = UILabel(frame: .zero)
        label.font = UIFont.title2Font()
        label.textColor = UIColor.night()
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    lazy var checkImageView: UIImageView = {
        let view = UIImageView(frame: .zero)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.contentMode = .center
        view.image = nil
        view.tintColor = UIColor.currentGlobalTintColor()
        return view
    }()

    // MARK: Init

    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        setupSubviews()
    }

    public override func layoutSubviews() {
        super.layoutSubviews()

        toggleImageViewSelected()
    }

    private func setupSubviews() {
        contentView.backgroundColor = .day()

        let stackView = UIStackView()
        contentView.addSubview(stackView)
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .horizontal
        stackView.distribution = .fill
        stackView.spacing = 10
        stackView.addArrangedSubview(self.label)
        stackView.addArrangedSubview(self.checkImageView)

        stackView.snp.makeConstraints { (make) in
            make.top.bottom.equalToSuperview().inset(VHCCaptureResultsValidValueCell.inset)
            make.left.right.equalToSuperview().inset(VHCCaptureResultsValidValueCell.inset)
            make.height.greaterThanOrEqualTo(22)
            make.height.lessThanOrEqualTo(300)
        }

        checkImageView.snp.makeConstraints { (make) in
            make.width.equalTo(23).priority(1000)
        }
    }

    // MARK: Helpers

    public func toggleImageViewSelected() {
        if answerIsSelected {
            checkImageView.image = VIAHealthCheckAsset.vhcCheckMarkSingle.templateImage
        } else {
            checkImageView.image = nil
        }
    }

    // MARK: Height calculation

    public class func height(with attributedTextDetail: AttributedTextDetail, constrainedTo width: CGFloat) -> CGSize {
        let inset = VHCCaptureResultsValidValueCell.inset
        let insets = UIEdgeInsets(top: 0, left: inset, bottom: 0, right: inset)
        let size = TextSize.size(attributedTextDetail.attributedString, width: width, insets: insets)
        let height = size.height + inset * 2
        return CGSize(width: width, height: height)
    }

}

extension VHCHealthAttributeValidValues {

    func attributedStringsSemiColonSeperatedValueList() -> [VHCValidValueOption] {
        guard var list = self.validValuesList?.components(separatedBy: ";") else { return [] }
        
        if let reversed = VIAApplicableFeatures.default.reverseValidValuesOptionList, reversed{
            list.reverse()
        }

        return list.map({ item -> VHCValidValueOption in
            var string = item
            let description = self.healthAttribute.first?.descriptionForValidValue(item)

            if let validDescription = description {
                string = "\(item)\n\(validDescription)"
            }

            let attributedString = NSMutableAttributedString(string: string)

            // value
            let valueAttributes: [String : Any] = [NSForegroundColorAttributeName: UIColor.night(),
                                                   NSFontAttributeName: UIFont.bodyFont()]
            attributedString.addAttributes(valueAttributes, range: attributedString.string.nsRange(of: item))

            // description
            if let validDescription = description {
                let descriptionAttributes: [String : Any] = [NSForegroundColorAttributeName: UIColor.mediumGrey(),
                                                             NSFontAttributeName: UIFont.subheadlineFont()]
                attributedString.addAttributes(descriptionAttributes, range: attributedString.string.nsRange(of: validDescription))
            }

            return (item, attributedString)
        })
    }

}

extension VHCHealthAttribute {

    public func descriptionForValidValue(_ validValue: String) -> String? {
        switch self.type {
        case .UrinaryProtein:
            return VHCHealthAttribute.descriptionForUrinaryProteinValue(validValue)
        default:
            return nil
        }
    }

    private class func descriptionForUrinaryProteinValue(_ value: String) -> String? {
        switch value {
        case "++++",
             "+++",
             "++",
             "+":
            return CommonStrings.Result.PositiveTitle284
        case "+-":
            return CommonStrings.Result.NeutralTitle285
        case "-":
            return CommonStrings.Result.NegativeTitle286
        default:
            return nil
        }
    }

}

extension VHCCaptureResultsValidValuesListViewController{
   
    fileprivate func getReversedList(numberOfRowsInSection section:Int) -> Int{
        if section == 0 {
            return 1
        }
        return viewModel.items.count
    }
   
    
    fileprivate func getReversedList(cellForRowAt indexPath:IndexPath) -> UITableViewCell{
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: VIATableViewCell.defaultReuseIdentifier, for: indexPath) as! VIATableViewCell
            
            cell.labelText = CommonStrings.CaptureResults.NoneMessage337
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: VHCCaptureResultsValidValueCell.defaultReuseIdentifier, for: indexPath) as! VHCCaptureResultsValidValueCell
        let item = viewModel.items[indexPath.row]
        cell.label.attributedText = item.attributedString
        cell.answerIsSelected = viewModel.initialItem == item.value
        if viewModel.selectedItem != nil {
            cell.answerIsSelected = viewModel.selectedItem?.attributedString == item.attributedString
        }
        return cell
    }
    
    fileprivate func reversedList(didSelectRowAt indexPath:IndexPath){
        if indexPath.section == 0 {
            viewModel.selectedItem = nil
        } else {
            let item = viewModel.items[indexPath.row]
            viewModel.selectedItem = item
        }
    }
    
   
}

extension VHCCaptureResultsValidValuesListViewController{
    
    fileprivate func getNormalList(numberOfRowsInSection section: Int) -> Int{
        if section == 0 {
            return viewModel.items.count
        }
        return 1
    }
    
    fileprivate func normalList(didSelectRowAt indexPath:IndexPath){
        if indexPath.section == 0 {
            let item = viewModel.items[indexPath.row]
            viewModel.selectedItem = item
        } else {
            viewModel.selectedItem = nil
        }
    }
    
    fileprivate func getNormalList(cellForRowAt indexPath:IndexPath) -> UITableViewCell{
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: VHCCaptureResultsValidValueCell.defaultReuseIdentifier, for: indexPath) as! VHCCaptureResultsValidValueCell
            let item = viewModel.items[indexPath.row]
            cell.label.attributedText = item.attributedString
            cell.answerIsSelected = viewModel.initialItem == item.value
            if viewModel.selectedItem != nil {
                cell.answerIsSelected = viewModel.selectedItem?.attributedString == item.attributedString
            }
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: VIATableViewCell.defaultReuseIdentifier, for: indexPath) as! VIATableViewCell
        
        cell.labelText = CommonStrings.CaptureResults.NoneMessage337
        return cell
    }
}
