import IGListKit
import VitalityKit
import VIACommon

extension VHCHealthAttribute: IGListDiffable {

    public func diffIdentifier() -> NSObjectProtocol {
        return self
    }

    public func isEqual(toDiffableObject object: IGListDiffable?) -> Bool {
        guard self !== object else { return true }
        guard let object = object as? VHCHealthAttribute else { return false }
        return type == object.type
    }

    public func title() -> String? {
        switch self.type {
        case .BMI:
            return CommonStrings.Measurement.BodyMassIndexTitle134
        case .Height:
            return CommonStrings.Measurement.HeightTitle145
        case .Weight:
            return CommonStrings.Measurement.WeightTitle146
        case .WaistCircumference:
            return CommonStrings.Measurement.WaistCircumferenceTitle135
        case .BloodPressureSystol:
            return CommonStrings.Measurement.SystolicTitle157
        case .BloodPressureDiasto:
            return CommonStrings.Measurement.DiastolicTitle156
        case .TotalCholesterol:
            return CommonStrings.Measurement.TotalCholesterolTitle149
        case .FastingGlucose:
            return CommonStrings.Measurement.FastingGlucoseTitle148
        case .RandomGlucose:
            return CommonStrings.Measurement.RandomGlucoseTitle147
        case .HDLCholesterol:
            return CommonStrings.Measurement.HdlTitle150
        case .LDLCholesterol:
            return CommonStrings.Measurement.LdlTitle151
        case .Triglycerides:
            return CommonStrings.Measurement.TriglyceridesTitle152
        case .HbA1c:
            return CommonStrings.Measurement.Hba1cTitle139
        case .UrinaryProtein:
            return CommonStrings.Measurement.UrineProteinTitle283
        case .LipidRatio:
            return VIAApplicableFeatures.default.getCommonStringsMeasurementCholesterolSectionTitle()
        default:
            return nil
        }
    }

    public func inputHeading() -> String? {
        // blood pressure is a special snowflake, it never has an input heading since
        // it is in its own special group and input controller
        guard self.type != .BloodPressureSystol, self.type != .BloodPressureDiasto else {
            return nil
        }

        guard let group = self.group.first else { return nil }

        if group.healthAttributes.count > 1 {
            return self.title()
        }
        return nil
    }

}
