import IGListKit
import VIAUIKit
import VitalityKit
import VIAUtilities
import VIACommon

public class VHCBloodPressureInputSectionController: IGListSectionController, IGListSectionType, VHCCaptureResultsDelegate {

    // MARK: Properties

    public lazy var viewModel: VHCCaptureResultsViewModel? = {
        if let controller = self.viewController as? VHCCaptureResultsCollectionViewController {
            return controller.viewModel
        }
        return nil
    }()

    public var isTopInputValid: Bool = false

    public var isBottomInputValid: Bool = false

    public var showDatePicker = false

    public var selectedUnit: Unit?

    public var selectedUnitOfMeasureType: UnitOfMeasureRef?

    public var selectedDate = Date() {
        didSet {
            persistTopAndBottomInput()
        }
    }

    public var topCapturedResult: VHCCapturedResult? {
        return self.viewModel?.capturedResult(for: self.detail.top.type)
    }

    public var bottomCapturedResult: VHCCapturedResult? {
        return self.viewModel?.capturedResult(for: self.detail.bottom.type)
    }

    public var topInput: String? {
        didSet {
            persistTopAndBottomInput()
        }
    }

    public var bottomInput: String? {
        didSet {
            persistTopAndBottomInput()
        }
    }

    public var detail: VHCBloodPressureInput! {
        didSet {
            self.topMetric = self.viewModel?.metrics.filter({ $0.healthAttribute.type == self.detail.top.type }).first
            self.bottomMetric = self.viewModel?.metrics.filter({ $0.healthAttribute.type == self.detail.bottom.type }).first
            if VIAApplicableFeatures.default.withDefaultStringValueForVHCCapturedDate() {
                self.selectedDate = self.topCapturedResult?.dateCaptured ?? self.bottomCapturedResult?.dateCaptured ?? NSDate.distantPast
            } else {
                self.selectedDate = self.topCapturedResult?.dateCaptured ?? self.bottomCapturedResult?.dateCaptured ?? Date()
            }
        }
    }

    public var topMetric: VHCMetric? {
        didSet {
            debugPrint(self.topMetric as Any)
        }
    }

    public var bottomMetric: VHCMetric? {
        didSet {
            debugPrint(self.bottomMetric as Any)
        }
    }

    public weak var inputCell: VHCBloodPressureCaptureResultsCollectionViewCell?

    // MARK: Init

    public override init() {
        super.init()
        NotificationCenter.default.addObserver(self, selector: #selector(closeDatePicker(_:)), name: .VIAVHCInputDidTogglePickerNotification, object: nil)
    }

    // MARK: IGListSectionType

    public func numberOfItems() -> Int {
        return self.showDatePicker ? 2 : 1
    }

    public func didUpdate(to object: Any) {
        detail = object as! VHCBloodPressureInput
    }

    public func cellForItem(at index: Int) -> UICollectionViewCell {
        if index == 0 {
            return inputCell(at: index)
        } else {
            return datePickerCell(at: index)
        }
    }
    
    public func displayErrorMessageIfNecessary(isValid: Bool, cell:VHCBloodPressureCaptureResultsCollectionViewCell, top:Bool){
        if top {
            cell.updateTopInputValidationMessage(isValid, topInput: self.topInput, bottomInput: self.bottomInput)
        } else {
            cell.updateBottomInputValidationMessage(isValid, topInput: self.topInput, bottomInput: self.bottomInput)
        }
    }
    
    public func inputCell(at index: Int) -> UICollectionViewCell {
        let cell = collectionContext!.dequeueReusableCell(withNibName: VHCBloodPressureCaptureResultsCollectionViewCell.defaultReuseIdentifier, bundle: VHCBloodPressureCaptureResultsCollectionViewCell.bundle(), for: self, at: index) as! VHCBloodPressureCaptureResultsCollectionViewCell
        cell.captureResultsDelegate = self

        // top textFieldTextDidChange
        cell.topTextFieldTextDidChange = { [unowned self] textField, unit, min, max in
            var uomType = UnitOfMeasureRef.Unknown
            if let isValid = self.topMetric?.validate(textField.text, unit, &uomType, &min, &max) {
                self.isTopInputValid = isValid
                self.selectedUnitOfMeasureType = uomType
                self.selectedUnit = unit
                self.topInput = textField.text
                
                self.displayErrorMessageIfNecessary(isValid: isValid, cell: cell, top: true)
            }
        }
        // bottom textFieldTextDidChange
        cell.bottomTextFieldTextDidChange = { [unowned self] textField, unit, min, max in
            var uomType = UnitOfMeasureRef.Unknown
            if let isValid = self.bottomMetric?.validate(textField.text, unit, &uomType, &min, &max) {
                self.isBottomInputValid = isValid
                self.selectedUnitOfMeasureType = uomType
                self.selectedUnit = unit
                self.bottomInput = textField.text
                
                self.displayErrorMessageIfNecessary(isValid: isValid, cell: cell, top: false)
            }
        }

        // top textFieldDidEndEditing
        cell.topTextFieldDidEndEditing = { [unowned self] textField, unit, min, max in
            var uomType = UnitOfMeasureRef.Unknown
            if let isValid = self.topMetric?.validate(textField.text, unit, &uomType, &min, &max) {
                self.isTopInputValid = isValid
                self.selectedUnitOfMeasureType = uomType
                self.selectedUnit = unit
                self.topInput = textField.text
                cell.updateTopInputValidationMessage(isValid, topInput: self.topInput, bottomInput: self.bottomInput)
            }
        }
        // bottom textFieldDidEndEditing
        cell.bottomTextFieldDidEndEditing = { [unowned self] textField, unit, min, max in
            var uomType = UnitOfMeasureRef.Unknown
            if let isValid = self.bottomMetric?.validate(textField.text, unit, &uomType, &min, &max) {
                self.isBottomInputValid = isValid
                self.selectedUnitOfMeasureType = uomType
                self.selectedUnit = unit
                self.bottomInput = textField.text
                cell.updateBottomInputValidationMessage(isValid, topInput: self.topInput, bottomInput: self.bottomInput)
            }
        }

        // configure cell
        cell.configureCaptureResultCell(units: self.topMetric?.units, // either top/bottom is fine, they're the same on units
                                        topInputText: self.topCapturedResult?.rawInput,
                                        topInputPlaceholder: self.detail.top.title(),
                                        bottomInputText: self.bottomCapturedResult?.rawInput,
                                        bottomInputPlaceholder: self.detail.bottom.title(),
                                        selectedDate: self.selectedDate)

        self.inputCell = cell
        return cell
    }

    public func datePickerCell(at index: Int) -> UICollectionViewCell {
        let cell = collectionContext?.dequeueReusableCell(withNibName: VIADatePickerCell.defaultReuseIdentifier, bundle: VIADatePickerCell.bundle(), for: self, at: index) as! VIADatePickerCell
        cell.didPickDate = self.datePickerDidPick
        cell.selectedDate = self.selectedDate
        cell.minimumDate = self.viewModel?.earliestCaptureDate ?? Date()
        return cell
    }

    public func sizeForItem(at index: Int) -> CGSize {
        if index == 0 {
            return CGSize(width: collectionContext!.containerSize.width, height: 144)
        }
        return CGSize(width: collectionContext!.containerSize.width, height: 200)
    }

    public func didSelectItem(at index: Int) { }

    // MARK: CaptureResultsDelegate

    public func show(menu: UIAlertController) {
        if let controller = self.viewController {
            controller.present(menu, animated: true, completion: nil)
        }
    }

    public func togglePicker() {
        
        if VIAApplicableFeatures.default.withDefaultStringValueForVHCCapturedDate() {
            if self.selectedDate == NSDate.distantPast {
                self.selectedDate = Date()
                self.inputCell?.updateDateButtonTitle(self.selectedDate)
            }
        }

        NotificationCenter.default.post(name: .VIAVHCInputDidTogglePickerNotification, object: self)
        self.showDatePicker = !self.showDatePicker
        updatePickerState()
    }

    func updatePickerState() {
        if self.showDatePicker {
            self.collectionContext?.insert(in: self, at: IndexSet(integer: 1))
        } else {
            self.collectionContext?.delete(in: self, at: IndexSet(integer: 1))
        }
    }

    public func removeKeyboard() {
        NotificationCenter.default.post(name: .VHCCaptureResultsTextFieldShouldResignFirstResponder, object: nil)
    }

    // MARK: DatePicker

    public func datePickerDidPick(date: Date) {
        self.selectedDate = date
        self.inputCell?.updateDateButtonTitle(self.selectedDate)
    }

    public func closeDatePicker(_ notification: Any?) {
        guard let notification = notification as? Notification else { return }
        guard let object = notification.object as? VHCCaptureResultsDelegate else { return }

        if object !== self && self.showDatePicker {
            self.showDatePicker = false
            updatePickerState()
        }
    }

    // MARK: Persist

    public func persistTopAndBottomInput() {
        persist(input: self.topInput,
                attribute: self.detail.top,
                unit: self.selectedUnit,
                unitOfMeasureType: self.selectedUnitOfMeasureType,
                date: self.selectedDate,
                //isValid: self.isTopInputValid)
                isValid: self.isTopInputValid && (Int(self.topInput ?? "0") ?? 0 > Int(self.bottomInput ?? "0") ?? 0))
        persist(input: self.bottomInput,
                attribute: self.detail.bottom,
                unit: self.selectedUnit,
                unitOfMeasureType: self.selectedUnitOfMeasureType,
                date: self.selectedDate,
                //isValid: self.isBottomInputValid)
                isValid: self.isBottomInputValid && (Int(self.topInput ?? "0") ?? 0 > Int(self.bottomInput ?? "0") ?? 0))
    }

    public func persist(input: String?, attribute: VHCHealthAttribute, unit: Unit?, unitOfMeasureType: UnitOfMeasureRef?, date: Date, isValid: Bool) {
        vhcPersistInput(input, isValid: isValid, for: attribute, on: date, unit: unit, unitOfMeasureType: unitOfMeasureType)
    }

}
