import IGListKit
import VIAUIKit
import VitalityKit
import VitalityKit
import VIAUtilities
import VIACommon

class VHCBMIInputSectionController: IGListSectionController, IGListSectionType, VHCCaptureResultsDelegate,  VHCBMICaptureResultsCollectionViewCellDelegate{
    
    // MARK: Properties
    
    lazy var viewModel: VHCCaptureResultsViewModel? = {
        if let controller = self.viewController as? VHCCaptureResultsCollectionViewController {
            return controller.viewModel
        }
        return nil
    }()
    
    //  This variable is used for dual unit validation
    public let safeInitialValue: String = "99994999399291" // 1 Foot = 12 inch
    
    //  This variable is used for foot-inch unit
    public let footToInch: Double = 12 // 1 Foot = 12 inch
    
    //  This variable is used for stone-pound unit
    public let stoneToPound: Double = 14 // 1 Stone = 14 pounds
    
    var isTopInputValid: Bool = false
    
    var isBottomInputValid: Bool = false
    
    var showDatePicker = false
    
    var selectedUnit: Unit?
    
    var selectedUnitOfMeasureType: UnitOfMeasureRef?
    
    var selectedDate = Date() {
        didSet {
            persistTopAndBottomInput()
        }
    }
    
    var topCapturedResult: VHCCapturedResult? {
        return self.viewModel?.capturedResult(for: self.detail.top.type)
    }
    
    var bottomCapturedResult: VHCCapturedResult? {
        return self.viewModel?.capturedResult(for: self.detail.bottom.type)
    }
    
    var topInput: String? {
        didSet {
            persistTopAndBottomInput()
        }
    }
    
    var bottomInput: String? {
        didSet {
            persistTopAndBottomInput()
        }
    }
    
    var detail: VHCBMIInput! {
        didSet {
            self.topMetric = self.viewModel?.metrics.filter({ $0.healthAttribute.type == self.detail.top.type }).first
            self.bottomMetric = self.viewModel?.metrics.filter({ $0.healthAttribute.type == self.detail.bottom.type }).first
            self.selectedDate = self.topCapturedResult?.dateCaptured ?? self.bottomCapturedResult?.dateCaptured ?? Date()
        }
    }
    
    var topMetric: VHCMetric? {
        didSet {
            debugPrint(self.topMetric as Any)
        }
    }
    
    var bottomMetric: VHCMetric? {
        didSet {
            debugPrint(self.bottomMetric as Any)
        }
    }
    
    weak var inputCell: VHCBMICaptureResultsCollectionViewCell?
    
    // MARK: Init
    
    public override init() {
        super.init()
        NotificationCenter.default.addObserver(self, selector: #selector(closeDatePicker(_:)), name: .VIAVHCInputDidTogglePickerNotification, object: nil)
    }
    
    // MARK: IGListSectionType
    
    public func numberOfItems() -> Int {
        return self.showDatePicker ? 3 : 2
    }
    
    public func didUpdate(to object: Any) {
        detail = object as! VHCBMIInput
    }
    
    public func cellForItem(at index: Int) -> UICollectionViewCell {
        if index == 0 {
            return headingCell(at: index)
        } else if index == 1{
            return inputCell(at: index)
        }else{
            return datePickerCell(at: index)
        }
    }
    
    func headingCell(at index: Int) -> UICollectionViewCell {
        let cell = collectionContext!.dequeueReusableCell(of: TextCell.self, for: self, at: index) as! TextCell
        cell.label.text = self.detail.top.inputHeading()
        cell.label.font = UIFont.subheadlineFont()
        cell.label.textColor = UIColor.night()
        return cell
    }
    
    func inputCell(at index: Int) -> UICollectionViewCell {
        let cell = collectionContext!.dequeueReusableCell(withNibName: VHCBMICaptureResultsCollectionViewCell.defaultReuseIdentifier, bundle: VHCBMICaptureResultsCollectionViewCell.bundle(), for: self, at: index) as! VHCBMICaptureResultsCollectionViewCell
        cell.captureResultsDelegate = self
        cell.delegate = self
        cell.healthAttributeType = self.detail.top.type
            
        // top textFieldTextDidChange
        cell.topTextFieldTextDidChange = { [unowned self] textField, unit, min, max in
            var uomType = UnitOfMeasureRef.Unknown
            self.topInput = textField.text
            let topInputString = (Localization.decimalFormatter.number(from: self.topInput!))?.stringValue
            if let metric = self.topMetric, metric.validateMultiUnit(topInputString, unit, UnitOfMeasureRef.Foot, &uomType, &min, &max) {
                self.isTopInputValid = true
                self.selectedUnitOfMeasureType = uomType
                self.selectedUnit = unit
            }
            else{
                self.isTopInputValid = false
            }
            cell.updateTopInputValidationMessage(self.isTopInputValid, topInput: self.topInput, bottomInput: self.bottomInput)
            self.persistTopAndBottomInput()
        }
        
        // bottom textFieldTextDidChange
        cell.bottomTextFieldTextDidChange = { [unowned self] textField, unit, min, max in
                self.onBottomFieldListener(textField, unit, min, max, cell)
        }
        
        // top textFieldDidEndEditing
        cell.topTextFieldDidEndEditing = { [unowned self] textField, unit, min, max in
            // Clear BMI field captured data.
            self.topInput = textField.text
            if (self.topInput ?? "").isEmpty {
                self.deletePreviousCapturedData()
                if (self.bottomInput ?? "").isEmpty {
                    // Re-validate captured result by posting notification.
                    self.validateCapturedResult()
                }
            }
        }
        
        // bottom textFieldDidEndEditing
        cell.bottomTextFieldDidEndEditing = { [unowned self] textField, unit, min, max in
            // Clear BMI field captured data.
            self.bottomInput = textField.text
            if (self.bottomInput ?? "").isEmpty {
                self.deletePreviousCapturedData()
                if (self.topInput ?? "").isEmpty {
                    // Re-validate captured result by posting notification.
                    self.validateCapturedResult()
                }
            }
        }
        
        var sUnitSymbol = self.selectedUnit?.symbol
        
        if sUnitSymbol == nil{
            sUnitSymbol = self.topCapturedResult?.inputUnitSymbol
            
        }
        
        if sUnitSymbol == nil{
            sUnitSymbol = self.topMetric?.units.first?.symbol
        }
            
        // configure cell
        cell.configureCaptureResultCell(units: self.topMetric?.units, // either top/bottom is fine, they're the same on units
            topInputText: self.topCapturedResult?.rawInput,
            topInputPlaceholder: self.detail.top.title(),
            bottomInputText: self.bottomCapturedResult?.rawInput,
            bottomInputPlaceholder: self.detail.bottom.title(),
//            selectedUnitSymbol: self.topCapturedResult?.inputUnitSymbol,
            selectedUnitSymbol: sUnitSymbol,
            selectedDate: self.selectedDate,
            isValid: (self.isTopInputValid && self.isBottomInputValid))
        
        self.inputCell = cell
        return cell
    }
    
    private func onBottomFieldListener(_ textField: UITextField, _ unit: Unit?, _ min: Double?, _ max: Double?, _ cell: VHCBMICaptureResultsCollectionViewCell){
        self.bottomInput = textField.text
        var isValid = false
        
        if selectedUnitOfMeasureType == .FootInch || selectedUnitOfMeasureType == .StonePound{
            isValid = VHCBMIFieldValidationUtil.getInstance().validate(topInput: self.topInput,
                                                                       bottomInput: (Double(textField.text ?? "0") ?? 0))
        }
        
        
        self.isBottomInputValid = isValid
        cell.updateBottomInputValidationMessage(isValid, topInput: self.topInput, bottomInput: self.bottomInput)
        self.persistTopAndBottomInput()
    }
    
    func datePickerCell(at index: Int) -> UICollectionViewCell {
        let cell = collectionContext?.dequeueReusableCell(withNibName: VIADatePickerCell.defaultReuseIdentifier, bundle: VIADatePickerCell.bundle(), for: self, at: index) as! VIADatePickerCell
        cell.didPickDate = self.datePickerDidPick
        cell.selectedDate = self.selectedDate
        cell.minimumDate = self.viewModel?.earliestCaptureDate ?? Date()
        return cell
    }
    
    public func deletePreviousCapturedData(){
        
        let realm = DataProvider.newVHCRealm()
        if let data = self.topCapturedResult{
            try! realm.write {
                realm.delete(data)
            }
        }
        
    }
    public func didUnitChanged(unit: Unit){
        
        deletePreviousCapturedData()
        
        self.selectedUnit = unit
        
        if self.selectedUnit == UnitOfMeasureRef.Meter.unit() {
            self.selectedUnitOfMeasureType = .Meter
        } else if self.selectedUnit == UnitOfMeasureRef.Centimeter.unit() {
            self.selectedUnitOfMeasureType = .Centimeter
        } else if self.selectedUnit == UnitOfMeasureRef.FootInch.unit() {
            self.selectedUnitOfMeasureType = .FootInch
        }

        self.collectionContext?.reload(in: self, at: IndexSet(integer: 1))
        
    }
    public func sizeForItem(at index: Int) -> CGSize {
        if index == 0 {
            return CGSize(width: collectionContext!.containerSize.width, height: 40)
        }
        if index == 1 {
            
            var sUnitSymbol = self.selectedUnit?.symbol
            
            if sUnitSymbol == nil{
                sUnitSymbol = self.topCapturedResult?.inputUnitSymbol
                
            }
            
            if sUnitSymbol == nil{
                sUnitSymbol = self.topMetric?.units.first?.symbol
            }
            
            if sUnitSymbol == UnitOfMeasureRef.FootInch.unit().symbol || sUnitSymbol == UnitOfMeasureRef.StonePound.unit().symbol{
                return CGSize(width: collectionContext!.containerSize.width, height: 144)
            }
            else{
                return CGSize(width: collectionContext!.containerSize.width, height: 96)
            }
        }else{
            return CGSize(width: collectionContext!.containerSize.width, height: 200)
        }
        
    }
    
    public func didSelectItem(at index: Int) { }
    
    // MARK: CaptureResultsDelegate
    
    public func show(menu: UIAlertController) {
        if let controller = self.viewController {
            controller.present(menu, animated: true, completion: nil)
        }
    }
    
    public func togglePicker() {
        NotificationCenter.default.post(name: .VIAVHCInputDidTogglePickerNotification, object: self)
        self.showDatePicker = !self.showDatePicker
        updatePickerState()
    }
    
    func updatePickerState() {
        if self.showDatePicker {
            self.collectionContext?.insert(in: self, at: IndexSet(integer: 2))
        } else {
            self.collectionContext?.delete(in: self, at: IndexSet(integer: 2))
        }
    }
    
    public func removeKeyboard() {
        NotificationCenter.default.post(name: .VHCCaptureResultsTextFieldShouldResignFirstResponder, object: nil)
    }
    
    // MARK: DatePicker
    
    func datePickerDidPick(date: Date) {
        self.selectedDate = date
        self.inputCell?.updateDateButtonTitle(self.selectedDate)
    }
    
    func closeDatePicker(_ notification: Any?) {
        guard let notification = notification as? Notification else { return }
        guard let object = notification.object as? VHCCaptureResultsDelegate else { return }
        
        if object !== self && self.showDatePicker {
            self.showDatePicker = false
            updatePickerState()
        }
    }
    
    // MARK: Persist
    
    func persistTopAndBottomInput() {
        var passedValue = ""
        var valid = self.isTopInputValid
        let top = Localization.decimalFormatter.number(from: self.topInput ?? safeInitialValue)?.doubleValue ?? Double(safeInitialValue)
        let bottom = Localization.decimalFormatter.number(from: self.bottomInput ?? safeInitialValue)?.doubleValue ?? Double(safeInitialValue)
        var baseUnit: Double = 0
        
        if (self.selectedUnitOfMeasureType == .FootInch) {
            baseUnit = footToInch
        } else if (self.selectedUnitOfMeasureType == .StonePound) {
            baseUnit = stoneToPound
        }
        
        VIAApplicableFeatures.default.getPassedValue(top: top!,
                                                     bottom: bottom!,
                                                     baseUnit: baseUnit,
                                                     selectedUnitOfMeasureType: self.selectedUnitOfMeasureType ?? .Unknown,
                                                     isTopInputValid: self.isTopInputValid,
                                                     isBottomInputValid: self.isBottomInputValid,
                                                     completion: { (vValue, pValue) in
                                                        valid = vValue
                                                        passedValue = pValue
        })
        
        print("==> Persist: \(passedValue)")
        persist(input: "\(passedValue)",
            attribute: self.detail.top,
            unit: self.selectedUnit,
            unitOfMeasureType: self.selectedUnitOfMeasureType,
            date: self.selectedDate,
            isValid: valid)
    }
    
    func persist(input: String?, attribute: VHCHealthAttribute, unit: Unit?, unitOfMeasureType: UnitOfMeasureRef?, date: Date, isValid: Bool) {
        vhcPersistInput(input, isValid: isValid, for: attribute, on: date, unit: unit, unitOfMeasureType: unitOfMeasureType)
    }
    
    func validateCapturedResult() {
        NotificationCenter.default.post(name: .VIASuccessfullyCapturedVHCHealthAttribute, object: nil)
    }
}
