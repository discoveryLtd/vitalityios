//
//  VHCBMICaptureResultsCollectionViewCell+Alert.swift
//  VitalityActive
//
//  Created by OJ Garde on 2/22/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import VIAUtilities
import VitalityKit

extension VHCBMICaptureResultsCollectionViewCell{
    
    public func showUnitsOption(){
        guard measurementUnits.count > 1 else { return }
        
        let unitsMenu = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        unitsMenu.popoverPresentationController?.sourceView = self
        
        for unit in measurementUnits {
            let title = measurementFormatter.string(from: unit)
            let action = UIAlertAction(title: title, style: .default, handler: { [weak self] (alert: UIAlertAction!) -> Void in
                
                self?.updateFieldsOnUoMSelect(unit: unit, title: title)
                self?.delegate.didUnitChanged(unit: (self?.selectedUnit)!)
            })
            unitsMenu.addAction(action)
        }
        
        let cancelAction = UIAlertAction(title: CommonStrings.CancelButtonTitle24, style: .cancel, handler: nil)
        unitsMenu.addAction(cancelAction)
        
        captureResultsDelegate?.show(menu: unitsMenu)
    }
    
    public func updateFieldsOnUoMSelect(unit: Unit, title: String){
        self.selectedUnit = unit
        self.unitsButtonTitle = title
        self.topInputTextField.text = nil
        self.setValidationErrorText(nil, for: self.topValidationErrorLabel)
        self.bottomInputTextField.text = nil
        self.setValidationErrorText(nil, for: self.bottomValidationErrorLabel)
        
        if let selectedUnitSymbol = self.selectedUnit?.symbol,
            (selectedUnitSymbol == footInchUnit || selectedUnitSymbol == stonePoundUnit) {
            self.bottomInputTextField.isUserInteractionEnabled = true
            
            let unitVal = title.characters.split{$0 == " "}.map(String.init)
            
            if unitVal.count > 1{
                self.topInputTextField.placeholder = unitVal[0]
                self.bottomInputTextField.placeholder = unitVal[1]
            }
        }
        else{
            self.bottomInputTextField.isUserInteractionEnabled = false
            self.bottomInputTextField.placeholder = ""
            //self.topInputTextField.placeholder = title
        }
    }
}
