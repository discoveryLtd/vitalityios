//
//  VHCBMICaptureResultsCollectionViewCell+TextField.swift
//  VitalityActive
//
//  Created by OJ Garde on 2/22/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import VIAUtilities
import VIACommon

extension VHCBMICaptureResultsCollectionViewCell{
    // MARK: - Textfield
    
    func setupInputTextFields() {
        self.topInputTextField.delegate = self
        self.topInputTextField.addTarget(self, action: #selector(textFieldEditingChanged(_:)), for: .editingChanged)
        self.topInputTextField.keyboardType = .decimalPad
        self.topInputTextField.placeholder = nil
        self.topInputTextField.font = UIFont.bodyFont()
        
        self.bottomInputTextField.delegate = self
        self.bottomInputTextField.addTarget(self, action: #selector(textFieldEditingChanged(_:)), for: .editingChanged)
        self.bottomInputTextField.keyboardType = .decimalPad
        self.bottomInputTextField.placeholder = nil
        self.bottomInputTextField.font = UIFont.bodyFont()
        self.bottomInputTextField.isUserInteractionEnabled = false
    }
    
    public func textFieldDidBeginEditing(_ textField: UITextField) {
        // Reload IQKeyboard manager toolbar on focus.
        textField.reloadKeyboardToolbar()
        
        if textField == self.topInputTextField {
            //
        } else if textField == self.bottomInputTextField {
            //
        }
    }
    
    public func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == self.topInputTextField{
            guard let action = self.topTextFieldDidEndEditing else { return }
            action(textField, selectedUnit, &topMin, &topMax)
        } else if textField == self.bottomInputTextField {
            guard let action = self.bottomTextFieldDidEndEditing else { return }
            action(textField, selectedUnit, &bottomMin, &bottomMax)
        }
    }
    
    func textFieldEditingChanged(_ textField: UITextField) {
        
        if textField == self.topInputTextField {
            guard self.topTextFieldTextDidChange != nil else { return }
            self.topTextFieldTextDidChange!(textField, selectedUnit, &topMin, &topMax)
        } else if textField == self.bottomInputTextField {
            guard self.bottomTextFieldTextDidChange != nil else { return }
            self.bottomTextFieldTextDidChange!(textField, selectedUnit, &bottomMin, &bottomMax)
        }
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        // TODO: Deprecated. Implementation moved to shouldChangeCharactersInAssessmentNumberRangeQuestionSection().
        //        print("===> VHCBMICaptureResultsCollectionViewCell should change")
        //        if  selectedUnit == UnitOfMeasureRef.FootInch.unit() ||
        //            selectedUnit == UnitOfMeasureRef.StonePound.unit(){
        //            let validInputs = NSCharacterSet(charactersIn:"0123456789").inverted
        //            let componentsToSeparate = string.components(separatedBy: validInputs)
        //            let numberFiltered = componentsToSeparate.joined(separator: "")
        //            return string == numberFiltered
        //        }
        //        /**
        //         * if Vendor == SLI, should conform to Japanese decimal which is 1 decimal place
        //         **/
        //        if (healthAttributeType == .Height || healthAttributeType == .Weight) &&
        //            (selectedUnit != UnitOfMeasureRef.FootInch.unit() || selectedUnit != UnitOfMeasureRef.StonePound.unit()){
        //
        //            return VIAApplicableFeatures.default.shouldChangeCharactersIn(textField.text ?? "", range: range, replacementString: string)
        //        }
        //        return true
        
        return VIAApplicableFeatures.default.shouldChangeCharactersInAssessmentNumberRangeQuestionSection(textField, shouldChangeCharactersIn: range, replacementString: string, selectedUnit: selectedUnit!, questionTypeKey: 0, healthAttributeType: healthAttributeType)
    }
}
