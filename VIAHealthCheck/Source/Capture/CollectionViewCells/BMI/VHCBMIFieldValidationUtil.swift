//
//  VHCBMIFieldValidationUtil.swift
//  VitalityActive
//
//  Created by OJ Garde on 4/18/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import VIAUtilities
import VitalityKit

public class VHCBMIFieldValidationUtil{
    private init(){
        
    }
    
    public static func getInstance() -> VHCBMIFieldValidationUtil{
        return VHCBMIFieldValidationUtil()
    }
    
    public func validate(topInput: String?, bottomInput: Double) -> Bool{
        /* Get the lower and upper limits */
        let limits = getLowerUpperLimits(topInput: topInput)
        
        /* We need to check if we have the valid values for the upper and lower limits. */
        if limits.count > 1{
            /* Compare if bottom input is greater than or equal to lower limit 
             * And bottom input is less than the upper limit. 
             */
            return bottomInput >= limits[0] && bottomInput <= limits[1]
        }
        
        /* If anything is not according to our rule, return false. */
        return false
    }
    
    public func getLowerUpperLimits(topInput: String?) -> [Double]{
        var topInputValue = 0.0
        
        guard let topInput = topInput else{
            return []
        }
        
        topInputValue = Localization.decimalFormatter.number(from: topInput)?.doubleValue ?? 0
        
        /* This is the holder for our bottom field lower limit default to 0.0 */
        var bottomMin   = 0.0
        
        /* This is the holder for our bottom field upper limit default to 0.0 */
        var bottomMax   = 0.0
        
        /* We need to search using the for-loop the valid health attribute value for FOOT INCH or STONE POUND */
        attrib: for attrib in DataProvider.newVHCRealm().allVHCHealthAttributes(){
            
            for validValue in attrib.validValues{
                
                /* Implement filter for FOOT INCH or STONE POUND*/
                if validValue.unitOfMeasureType == .FootInch || validValue.unitOfMeasureType == .StonePound{
                    
                    /* Manipulate bottom field lower limit if top field input is less than or equal to the top field lower limit */
                    if topInputValue <= getTopFieldLowerLimit(validValue){
                        /* If top input is less than or equal to the top field lower limit,
                         * use the bottom field lower limit as bottom field lower limit
                         */
                        bottomMin = getTopFieldUpperLimit(validValue)
                    }else{
                        /* Else, use 0 */
                        bottomMin = 0
                    }
                    
                    /*  If top input value is less than the bottom upper limit, we will use */
                    if topInputValue < getBottomFieldLowerLimit(validValue){
                        if validValue.unitOfMeasureType == .FootInch{
                            /* 1 Foot 12 Inches */
                            bottomMax = 11
                        }else if validValue.unitOfMeasureType == .StonePound{
                            /* 1 Stone = 14 LB */
                            bottomMax = 13
                        }else{
                            /* This is for fallback. */
                            bottomMax = getBottomFieldUpperLimit(validValue)
                        }
                    }else{
                        /* If top input value is equals to the top field upper limit, let's use the bottom field upper limit. */
                        bottomMax = getBottomFieldUpperLimit(validValue)
                    }
                    
                    /* break loop if were done with FOOT INCH or STONE POUND filter */
                    break attrib
                }
            }
        }
        
        return [bottomMin, bottomMax]
    }
}

extension VHCBMIFieldValidationUtil{
    
    fileprivate func getTopFieldLowerLimit(_ validValue: VHCHealthAttributeValidValues) -> Double{
        /* Let's split the values from minValue using the delimiter '.' */
        let minValueRaw = validValue.minValue.components(separatedBy: ".")
        
        /* This is the value of our limit which is default to 0.0 */
        var limit       = 0.0
        
        /* We need to check if we have the valid values for the upper and lower limits. */
        if minValueRaw.count > 1{
            
            /* If valid, return the value from index 0. */
            limit   = Localization.decimalFormatter.number(from: minValueRaw[0])?.doubleValue ?? 0
        }
        
        /* return the value of our limit */
        return limit
    }
    
    fileprivate func getTopFieldUpperLimit(_ validValue: VHCHealthAttributeValidValues) -> Double{
        /* Let's split the values from minValue using the delimiter '.' */
        let minValueRaw = validValue.minValue.components(separatedBy: ".")
        
        /* This is the value of our limit which is default to 0.0 */
        var limit       = 0.0
        
        /* We need to check if we have the valid values for the upper and lower limits. */
        if minValueRaw.count > 1{
            
            /* If valid, return the value from index 1. */
            limit   = Localization.decimalFormatter.number(from: minValueRaw[1])?.doubleValue ?? 0
        }
        
        /* return the value of our limit */
        return limit
    }
    
    fileprivate func getBottomFieldLowerLimit(_ validValue: VHCHealthAttributeValidValues) -> Double{
        /* Let's split the values from maxValue using the delimiter '.' */
        let maxValueRaw = validValue.maxValue.components(separatedBy: ".")
        
        /* This is the value of our limit which is default to 0.0 */
        var limit       = 0.0
        
        /* We need to check if we have the valid values for the upper and lower limits. */
        if maxValueRaw.count > 1{
            
            /* If valid, return the value from index 1. */
            limit   = Localization.decimalFormatter.number(from: maxValueRaw[0])?.doubleValue ?? 0
        }
        
        /* return the value of our limit */
        return limit    }
    
    fileprivate func getBottomFieldUpperLimit(_ validValue: VHCHealthAttributeValidValues) -> Double{
        /* Let's split the values from maxValue using the delimiter '.' */
        let maxValueRaw = validValue.maxValue.components(separatedBy: ".")
        
        /* This is the value of our limit which is default to 0.0 */
        var limit       = 0.0
        
        /* We need to check if we have the valid values for the upper and lower limits. */
        if maxValueRaw.count > 1{
            
            /* If valid, return the value from index 1. */
            limit   = Localization.decimalFormatter.number(from: maxValueRaw[1])?.doubleValue ?? 0
        }
        
        /* return the value of our limit */
        return limit
    }
}
