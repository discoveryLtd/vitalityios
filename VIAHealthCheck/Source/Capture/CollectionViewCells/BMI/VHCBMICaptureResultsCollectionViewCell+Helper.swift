//
//  VHCBMICaptureResultsCollectionViewCell+Helper.swift
//  VitalityActive
//
//  Created by OJ Garde on 2/22/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import VIAUtilities
import VitalityKit
import VIACommon
import VIAUIKit

extension VHCBMICaptureResultsCollectionViewCell{
    
    public func configureCaptureResultCell(units: [Unit]?,
                                           topInputText: String?,
                                           topInputPlaceholder: String?,
                                           bottomInputText: String?,
                                           bottomInputPlaceholder: String?,
                                           selectedUnitSymbol: String?,
                                           selectedDate: Date,
                                           isValid: Bool?) {
        let measurementFormatter        = Localization.decimalShortStyle
        let title                       = measurementFormatter.string(from: Unit.init(symbol: selectedUnitSymbol ?? ""))
        
        /** Initialize field values and presentation */
        initFields(selectedUnitSymbol, title: title,
                   topInputText: topInputText,
                   topInputPlaceholder: topInputPlaceholder)
        
        if let validUnits = units, selectedUnitSymbol != nil{
            
            /** Update Dropdown View */
            updateUnitDropdownView(validUnits)
            
            if title == measurementFormatter.string(from: (UnitOfMeasureRef.FootInch.unit())){
                initFootInchField(topInputText: topInputText, bottomInputText: bottomInputText, isValid: isValid)
            }else if(title == measurementFormatter.string(from: (UnitOfMeasureRef.StonePound.unit()))){
                initStonePound(topInputText: topInputText, bottomInputText: bottomInputText, isValid: isValid)
            }else {
                initOtherUom(topInputText: topInputText)
            }
            
            updateFieldData(topInputText: topInputText, bottomInputText: bottomInputText, selectedDate: selectedDate)
            
        } else {
            debugPrint("Cannot configure cell without valid units")
        }
    }
    fileprivate func initOtherUom(topInputText: String?) {
        if let input = topInputText, topInputText?.removeDecimalFormatter() != safeInitialValue{
            let val = formatNumber(value: input)
            topInputTextField.text = val
        }
    }
    
    fileprivate func initFootInchField(topInputText: String?, bottomInputText: String?, isValid: Bool?) {
        var topValue = ""
        var bottomValue = ""
        
        VIAApplicableFeatures.default.initMultiUnitField(isValid: isValid,
                                                         topInputText: topInputText,
                                                         bottomInputText: bottomInputText,
                                                         numberFormatter: getDecimalFormatter(),
                                                         baseUnit: footToInch,
                                                         completion: { (tValue, bValue) in
                                                            topValue = tValue
                                                            bottomValue = bValue
        })
        
        topInputTextField.text = (topValue != safeInitialValue) ? topValue : nil
        bottomInputTextField.text = (bottomValue != safeInitialValue) ? bottomValue : nil
        self.bottomInputFieldsStackView.isHidden            = false
        self.bottomInputTextField.isUserInteractionEnabled  = true
    }
    
    fileprivate func initStonePound(topInputText: String?, bottomInputText: String?, isValid: Bool?) {
        var topValue = ""
        var bottomValue = ""
        
        VIAApplicableFeatures.default.initMultiUnitField(isValid: isValid,
                                                         topInputText: topInputText,
                                                         bottomInputText: bottomInputText,
                                                         numberFormatter: getDecimalFormatter(),
                                                         baseUnit: stoneToPound,
                                                         completion: { (tValue, bValue) in
                                                            topValue = tValue
                                                            bottomValue = bValue
        })
        
        topInputTextField.text = (topValue != safeInitialValue) ? topValue : nil
        bottomInputTextField.text = (bottomValue != safeInitialValue) ? bottomValue : nil
        self.bottomInputFieldsStackView.isHidden            = false
        self.bottomInputTextField.isUserInteractionEnabled  = true
    }
    
    fileprivate func updateFieldData(topInputText: String?, bottomInputText: String?, selectedDate: Date){
        dateTestedLabel.text = CommonStrings.CaptureResults.DateTested144
        updateDateButtonTitle(selectedDate)
        
        /** only trigger if there was actual input */
        if let _ = topInputText {
            if (topInputText != "0.0"){
                textFieldDidEndEditing(topInputTextField)
                textFieldEditingChanged(topInputTextField)
            }
        }
        if let _ = bottomInputText {
            if (bottomInputText != "0.0"){
                textFieldDidEndEditing(bottomInputTextField)
                textFieldEditingChanged(bottomInputTextField)
            }
        }
    }
    
    fileprivate func initFields(_ selectedUnitSymbol: String?, title: String,
                                topInputText: String?, topInputPlaceholder: String?){
        self.topInputTextField.text                         = nil
        self.bottomInputTextField.text                      = nil
        self.selectedUnit                                   = Unit.init(symbol: selectedUnitSymbol ?? "")
        
        if (topInputText != "0.0" && topInputText != nil){
            topInputTextField.text = topInputText
        }
        
        self.topInputTextField.placeholder                  = topInputPlaceholder
        self.bottomInputFieldsStackView.isHidden            = true
        self.bottomInputTextField.isUserInteractionEnabled  = false
        self.bottomInputTextField.placeholder               = ""
                
        self.updateFieldsOnUoMSelect(unit: self.selectedUnit!, title: title)
    }

    fileprivate func updateUnitDropdownView(_ validUnits: [Unit]){
        if validUnits.count > 1 {
            unitsArrowImageView.image = VIAUIKitAsset.arrowDrill.image
            unitsButton.tintColor = .lightGrey()
        } else {
            unitsArrowImageView.image = nil
        }
        measurementUnits = validUnits
    }
}

extension VHCBMICaptureResultsCollectionViewCell{
    
    public func formatNumber(value: String) -> String{
        guard let number = getDecimalFormatter().number(from: value) else { return value}
        return getDecimalFormatter().string(from: number) ?? value
    }
    
    fileprivate func getDecimalFormatter() -> NumberFormatter {
        let formatter = NumberFormatter()
        formatter.locale = Locale.current
        formatter.decimalSeparator = Locale.current.decimalSeparator ?? "."
        formatter.numberStyle = .decimal
        formatter.maximumFractionDigits = 2
        return formatter
    }
}
