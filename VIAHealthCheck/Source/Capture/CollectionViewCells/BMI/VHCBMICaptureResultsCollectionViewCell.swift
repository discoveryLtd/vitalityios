import VIAUIKit
import VitalityKit
import VIAUtilities
import VIACommon

protocol VHCBMICaptureResultsCollectionViewCellDelegate{
    func didUnitChanged(unit: Unit)
}

class VHCBMICaptureResultsCollectionViewCell: VHCCaptureResultsCollectionViewCell, Nibloadable, UITextFieldDelegate {
    
    var delegate:VHCBMICaptureResultsCollectionViewCellDelegate!
    
    // MARK: Outlets
    
    @IBOutlet weak var topInputFieldsStackView: UIStackView!
    @IBOutlet weak var bottomInputFieldsStackView: UIStackView!
    @IBOutlet weak var userInputStackView: UIStackView!
    @IBOutlet weak var topInputTextField: UITextField!
    @IBOutlet weak var bottomInputTextField: UITextField!
    @IBOutlet weak var topValidationErrorLabel: UILabel!
    @IBOutlet weak var bottomValidationErrorLabel: UILabel!
    
    // MARK: Properties
    
    public var topMin: Double?
    public var topMax: Double?
    public var bottomMin: Double?
    public var bottomMax: Double?
    var healthAttributeType: PartyAttributeTypeRef = .Unknown
    
//  This variable is used for foot-inch unit
    public let minInchValue: Double = 0
    public let maxInchValue: Double = 11
    public let footToInch: Double = 12 // 1 Foot = 12 inch
    
//  This variable is used for dual unit validation
    public let safeInitialValue: String = "99994999399291" // 1 Foot = 12 inch
    
//  This variable is used for stone-pound unit
    public let minPoundValue: Double = 0
    public let maxPoundValue: Double = 13
    public let stoneToPound: Double = 14 // 1 Stone = 14 pounds
    
    let measurementFormatter = Localization.decimalShortStyle
    let footInchUnit = Localization.decimalShortStyle.string(from: (UnitOfMeasureRef.FootInch.unit()))
    let stonePoundUnit = Localization.decimalShortStyle.string(from: (UnitOfMeasureRef.StonePound.unit()))
    
    // MARK: Closures
    
    public var topTextFieldTextDidChange: ((_ textField: UITextField, _ unit: Unit?, _ min: inout Double?, _ max: inout Double?) -> Void)?
    
    public var topTextFieldDidEndEditing: ((_ textField: UITextField, _ unit: Unit?, _ min: inout Double?, _ max: inout Double?) -> Void)?
    
    public var bottomTextFieldTextDidChange: ((_ textField: UITextField, _ unit: Unit?, _ min: inout Double?, _ max: inout Double?) -> Void)?
    
    public var bottomTextFieldDidEndEditing: ((_ textField: UITextField, _ unit: Unit?, _ min: inout Double?, _ max: inout Double?) -> Void)?
    
    public var measurementUnits = [Unit]()
    
    // MARK: Lifecycle
    
    override public func awakeFromNib() {
        super.awakeFromNib()
        
        setupInputTextFields()
    }
    
    public override func prepareForReuse() {
        super.prepareForReuse()
        
        setupCell()
    }
    
    override func setupCell() {
        super.setupCell()
        
        topValidationErrorLabel.isHidden = true
        topValidationErrorLabel.font = .footnoteFont()
        topValidationErrorLabel.textColor = UIColor.cellErrorLabel()
        
        bottomValidationErrorLabel.isHidden = true
        bottomValidationErrorLabel.font = .footnoteFont()
        bottomValidationErrorLabel.textColor = UIColor.cellErrorLabel()
        
        topMax = nil
        topMin = nil
        bottomMax = nil
        bottomMin = nil
        topInputTextField.text = nil
        setValidationErrorText(nil, for: self.topValidationErrorLabel)
        bottomInputTextField.text = nil
        setValidationErrorText(nil, for: self.bottomValidationErrorLabel)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        mainStackView.layer.removeAllBorderLayers()
        mainStackView.layer.addBorder(edge: .top)
        mainStackView.layer.addBorder(edge: .bottom)
        dateStackView.layer.removeAllBorderLayers()
        dateStackView.layer.addBorder(edge: .top)
        unitsArrowImageView.layer.removeAllBorderLayers()
        unitsArrowImageView.layer.addBorder(edge: .right)
        bottomInputFieldsStackView.layer.removeAllBorderLayers()
        bottomInputFieldsStackView.layer.addBorder(edge: .top)
    }
    
    // MARK:
    
    override func stopEditing() {
        self.topInputTextField.endEditing(true)
        self.bottomInputTextField.endEditing(true)
    }
    
    
    // MARK: Autosizing
    
    override open func preferredLayoutAttributesFitting(_ layoutAttributes: UICollectionViewLayoutAttributes) -> UICollectionViewLayoutAttributes {
        return self.defaultContentViewLayoutSizeFitting(layoutAttributes)
    }
    
    
    override func showUnitsActionSheet() {
        // Dismiss keyboard on unit change.
        if AppSettings.getAppTenant() == .EC || AppSettings.getAppTenant() == .ASR {
            self.contentView.endEditing(true)
        }
        
        showUnitsOption()
    }
    
}

extension VHCBMICaptureResultsCollectionViewCell {
    
    func updateTopInputValidationMessage(_ isValid: Bool, topInput: String?, bottomInput: String?) {
        if isValid {
            self.setValidationErrorText(nil, for: self.topValidationErrorLabel)
        } else {
            self.setTopInputOutOfRangeMessage()
        }
        
        self.setRequiredInputMessageIfNeeded(topInput: topInput, bottomInput: bottomInput)
    }
    
    func updateBottomInputValidationMessage(_ isValid: Bool, topInput: String?, bottomInput: String?) {
        if isValid {
            self.setValidationErrorText(nil, for: self.bottomValidationErrorLabel)
        } else {
            self.setBottomInputOutOfRangeMessage()
        }
        
        self.setRequiredInputMessageIfNeeded(topInput: topInput, bottomInput: bottomInput)
    }
    
    private func setRequiredInputMessageIfNeeded(topInput: String?, bottomInput: String?) {
        let text = CommonStrings.ErrorRequired289
        let shouldHandleTop = topInput == nil || topInput!.isEmpty
        let shouldHandleBottom = bottomInput == nil || bottomInput!.isEmpty
        
        // both can't be required, then it means the user hasn't
        // and doesn't have to enter anything
        if shouldHandleTop && shouldHandleBottom {
            self.setValidationErrorText(nil, for: self.topValidationErrorLabel)
            self.setValidationErrorText(nil, for: self.bottomValidationErrorLabel)
            return
        }
        
        if shouldHandleTop {
            self.setValidationErrorText(text, for: self.topValidationErrorLabel)
        }
        if shouldHandleBottom {
            self.setValidationErrorText(text, for: self.bottomValidationErrorLabel)
        }
    }
    
    private func setTopInputOutOfRangeMessage() {
        var min = self.topMin
        var max = self.topMax

        if (self.selectedUnit?.symbol == footInchUnit || self.selectedUnit?.symbol == stonePoundUnit){
            min = (modf(self.topMin ?? 0).0)
            max = (modf(self.topMax ?? 0).0)
        }
        self.setInputOutOfRangeMessage(for: self.topValidationErrorLabel, minValue: min, maxValue: max)
    }
    
    private func setBottomInputOutOfRangeMessage() {
        
        /* Custom validation for FOOT INCH and STONE POUND */
        if self.selectedUnit?.symbol == footInchUnit || self.selectedUnit?.symbol == stonePoundUnit{
           customValidationForFootInchStonePound()
        }
        
        self.setInputOutOfRangeMessage(for: self.bottomValidationErrorLabel, minValue: self.bottomMin, maxValue: self.bottomMax)
    }
    
    private func customValidationForFootInchStonePound(){
        /* Initialize our lower limit global variable to zero. */
        self.bottomMin = 0.0
        
        /* Initialize our upper limit global variable to zero. */
        self.bottomMax = 0.0
        
        /* Get the lower and upper limits */
        let limits = VHCBMIFieldValidationUtil.getInstance().getLowerUpperLimits(topInput: self.topInputTextField.text)
        
        /* We need to check if we have the valid values for the upper and lower limits. */
        if limits.count > 1{
            /* Assign the values of our lower limit */
            self.bottomMin = limits[0]
            
            /* Assign the values of our upper limit */
            self.bottomMax = limits[1]
        }
    }
    
    private func getTopFieldLowerLimit(_ validValue: VHCHealthAttributeValidValues) -> Double{
        /* Let's split the values from minValue using the delimiter '.' */
        let minValueRaw = validValue.minValue.components(separatedBy: ".")
        
        /* This is the value of our limit which is default to 0.0 */
        var limit       = 0.0
        
        /* We need to check if we have the valid values for the upper and lower limits. */
        if minValueRaw.count > 1{
        
            /* If valid, return the value from index 0. */
            limit   = Localization.decimalFormatter.number(from: minValueRaw[0])?.doubleValue ?? 0
        }
        
        /* return the value of our limit */
        return limit
    }
    
    private func getBottomFieldLowerLimit(_ validValue: VHCHealthAttributeValidValues) -> Double{
        /* Let's split the values from minValue using the delimiter '.' */
        let minValueRaw = validValue.minValue.components(separatedBy: ".")
        
        /* This is the value of our limit which is default to 0.0 */
        var limit       = 0.0
        
        /* We need to check if we have the valid values for the upper and lower limits. */
        if minValueRaw.count > 1{
            
            /* If valid, return the value from index 1. */
            limit   = Localization.decimalFormatter.number(from: minValueRaw[1])?.doubleValue ?? 0
        }
        
        /* return the value of our limit */
        return limit
    }
    
    private func getBottomFieldUpperLimit(_ validValue: VHCHealthAttributeValidValues) -> Double{
        /* Let's split the values from maxValue using the delimiter '.' */
        let maxValueRaw = validValue.maxValue.components(separatedBy: ".")
        
        /* This is the value of our limit which is default to 0.0 */
        var limit       = 0.0
        
        /* We need to check if we have the valid values for the upper and lower limits. */
        if maxValueRaw.count > 1{
            
            /* If valid, return the value from index 1. */
            limit   = Localization.decimalFormatter.number(from: maxValueRaw[1])?.doubleValue ?? 0
        }
        
        /* return the value of our limit */
        return limit
    }
    
    private func setInputOutOfRangeMessage(for label: UILabel, minValue: Double?, maxValue: Double?) {
        let decimalFormatter: NumberFormatter = {
            let shouldAcceptDecimalInput = VIAApplicableFeatures.default.shouldAcceptDecimalInput(self.selectedUnit!, questionTypeKey: 0, healthAttributeType: .Unknown)
            
            guard let formatter = VIAApplicableFeatures.default.decimalFormatter(shouldAcceptDecimalInput: shouldAcceptDecimalInput) else {
                return Localization.decimalFormatter
            }
            
            return formatter
        }()
        
        var text: String = ""
        if let min = minValue, let max = maxValue {
            if let formattedMin = decimalFormatter.string(from: min as NSNumber), let formattedMax = decimalFormatter.string(from: max as NSNumber) {
                text = CommonStrings.ErrorRange180(formattedMin, formattedMax)
            }
        } else if let min = minValue, maxValue == nil {
            if let formattedMin = decimalFormatter.string(from: min as NSNumber) {
                text = CommonStrings.ErrorRangeBigger281(formattedMin)
            }
        } else if let max = maxValue, minValue == nil {
            if let formattedMax = decimalFormatter.string(from: max as NSNumber) {
                text = CommonStrings.ErrorRangeSmaller282(formattedMax)
            }
        }
        
        self.setValidationErrorText(text, for: label)
    }
}
