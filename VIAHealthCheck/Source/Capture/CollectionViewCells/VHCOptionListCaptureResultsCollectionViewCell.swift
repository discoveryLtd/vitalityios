import VIAUIKit
import VitalityKit
//import VIAUtilities
//import VIACommon
import VIACommon

protocol VHCOptionListCaptureResultsCollectionViewCellDelegate: class {
    func shouldDisplayOptionList()
}

class VHCOptionListCaptureResultsCollectionViewCell: UICollectionViewCell, Nibloadable {

    // MARK: Properties

    weak var captureResultsDelegate: VHCCaptureResultsDelegate?

    weak var delegate: VHCOptionListCaptureResultsCollectionViewCellDelegate?

    // MARK: - Date button setup

    @IBAction func dateButtonTapped(_ sender: Any) {
        NotificationCenter.default.post(name: .VHCCaptureResultsTextFieldShouldResignFirstResponder, object: nil)
        captureResultsDelegate?.togglePicker()
    }

    public func updateDateButtonTitle(_ date: Date) {
        if VIAApplicableFeatures.default.withDefaultStringValueForVHCCapturedDate() {
            let title: String
            if date == NSDate.distantPast {
                title = CommonStrings.Assessment.DateTested.DefaultValue2149
            } else {
                title = Localization.dayDateShortMonthyearFormatter.string(from: date)
            }
            dateButton.setTitle(title, for: .normal)
        } else {
            let title = Localization.dayDateShortMonthyearFormatter.string(from: date)
            dateButton.setTitle(title, for: .normal)
        }
    }

    // MARK: Outlets

    @IBOutlet weak var userInputStackView: UIStackView!
    @IBOutlet weak var mainButton: UIButton!
    @IBOutlet weak var mainStackView: UIStackView!
    @IBOutlet weak var dateButton: UIButton!
    @IBOutlet weak var dateTestedLabel: UILabel!
    @IBOutlet weak var dateStackView: UIStackView!
    @IBOutlet weak var selectedDateStackView: UIStackView!
    @IBOutlet weak var valueButton: UIButton!

    // MARK: Properties

    public var measurementUnits = [Unit]()

    // MARK: Lifecycle

    override public func awakeFromNib() {
        super.awakeFromNib()

        setupCell()
    }

    public override func prepareForReuse() {
        super.prepareForReuse()

        setupCell()
    }

    func setupCell() {
        mainButton.setTitle(CommonStrings.CaptureResults.ResultsTitle287, for: .normal)
        mainButton.titleLabel?.font = .bodyFont()
        mainButton.setTitleColor(.mediumGrey(), for: .normal)

        valueButton.setTitle(CommonStrings.LabelSelect288, for: .normal)
        valueButton.titleLabel?.font = .bodyFont()
        valueButton.titleLabel?.textAlignment = .right
        valueButton.setTitleColor(.lightGrey(), for: .normal)
        valueButton.contentHorizontalAlignment = .right

        dateTestedLabel.text = nil
        dateTestedLabel.font = .bodyFont()
        dateTestedLabel.textColor = .mediumGrey()

        dateButton.setTitle(nil, for: .normal)
        dateButton.titleLabel?.font =  .bodyFont()
        dateButton.setTitleColor(.night(), for: .normal)
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        mainStackView.layer.removeAllBorderLayers()
        mainStackView.layer.addBorder(edge: .top)
        mainStackView.layer.addBorder(edge: .bottom)
        userInputStackView.layer.removeAllBorderLayers()
        userInputStackView.layer.addBorder(edge: .bottom)
    }

    public func configureCaptureResultCell(inputValue: String?, selectedDate: Date) {
        dateTestedLabel.text = CommonStrings.CaptureResults.DateTested144
        valueButton.setTitle(inputValue ?? CommonStrings.LabelSelect288, for: .normal)
        valueButton.setTitleColor(.lightGrey(), for: .normal)
        if inputValue != nil {
            valueButton.setTitleColor(.night(), for: .normal)
        }
        updateDateButtonTitle(selectedDate)
    }

    // MARK: Actions

    @IBAction func showOptions(_ sender: Any) {
        delegate?.shouldDisplayOptionList()
    }

    // MARK: Autosizing

    override open func preferredLayoutAttributesFitting(_ layoutAttributes: UICollectionViewLayoutAttributes) -> UICollectionViewLayoutAttributes {
        return self.defaultContentViewLayoutSizeFitting(layoutAttributes)
    }

}
