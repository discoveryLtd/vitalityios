import VIAUIKit
import VitalityKit
import VIAUtilities
import VIACommon

public extension NSNotification.Name {
    static let VHCCaptureResultsTextFieldShouldResignFirstResponder = Notification.Name("VHCCaptureResultsTextFieldShouldResignFirstResponder")
}

open class VHCCaptureResultsCollectionViewCell: UICollectionViewCell {

    // MARK: Outlets

    @IBOutlet weak var unitsButton: UIButton!
    @IBOutlet weak var mainStackView: UIStackView!
    @IBOutlet weak var dateButton: UIButton!
    @IBOutlet weak var dateTestedLabel: UILabel!
    @IBOutlet weak var dateStackView: UIStackView!
    @IBOutlet weak var selectedDateStackView: UIStackView!
    @IBOutlet weak var unitsArrowImageView: UIImageView!
    @IBOutlet weak var inputFieldsStackView: UIStackView!

    // MARK: Properties

    var selectedUnit: Unit?
    
    weak var captureResultsDelegate: VHCCaptureResultsDelegate?

    public var unitsButtonTitle: String? {
        set {
            unitsButton.setTitle(newValue, for: .normal)
        }
        get {
            return unitsButton.titleLabel?.text
        }
    }

    // MARK: Lifecycle

    open override func awakeFromNib() {
        super.awakeFromNib()

        setupCell()
        subscribeToNotifications()
    }

    open func setupCell() {
        unitsButton.setTitle(nil, for: .normal)
        unitsButton.titleLabel?.font = .bodyFont()
        unitsButton.setTitleColor(.mediumGrey(), for: .normal)
        let gesture = UITapGestureRecognizer(target: self, action: #selector(unitsButtonTapped(_:)))
        unitsArrowImageView.addGestureRecognizer(gesture)

        dateTestedLabel.text = nil
        dateTestedLabel.font = .bodyFont()
        dateTestedLabel.textColor = .mediumGrey()

        dateButton.setTitle(nil, for: .normal)
        dateButton.titleLabel?.font =  .bodyFont()
        dateButton.setTitleColor(.night(), for: .normal)
    }

    func subscribeToNotifications() {
        NotificationCenter.default.addObserver(forName: .VHCCaptureResultsTextFieldShouldResignFirstResponder, object: nil, queue: nil) { (notification) in
            self.stopEditing()
        }
    }

    // MARK: - Units button setup

    @IBAction func unitsButtonTapped(_ sender: Any) {
        resignFirstResponder()
        showUnitsActionSheet()
    }

    open func showUnitsActionSheet() {
    }

   // MARK: - Date button setup

    @IBAction func dateButtonTapped(_ sender: Any) {
        NotificationCenter.default.post(name: .VHCCaptureResultsTextFieldShouldResignFirstResponder, object: nil)
        captureResultsDelegate?.togglePicker()
    }

    public func updateDateButtonTitle(_ date: Date) {
        if VIAApplicableFeatures.default.withDefaultStringValueForVHCCapturedDate() {
            let title: String
            if date == NSDate.distantPast {
                title = CommonStrings.Assessment.DateTested.DefaultValue2149
            } else {
                title = Localization.dayDateShortMonthyearFormatter.string(from: date)
            }
            dateButton.setTitle(title, for: .normal)
        } else {
            let title = Localization.dayDateShortMonthyearFormatter.string(from: date)
            dateButton.setTitle(title, for: .normal)
        }
    }

    // MARK: Actions

    open func stopEditing() {
    }

    func setValidationErrorText(_ text: String?, for label: UILabel) {
        label.text = text
        if let validText = text {
            label.isHidden = validText.isEmpty
        } else {
            label.isHidden = true
        }
    }

    // MARK: Autosizing

    override open func preferredLayoutAttributesFitting(_ layoutAttributes: UICollectionViewLayoutAttributes) -> UICollectionViewLayoutAttributes {
        return self.defaultContentViewLayoutSizeFitting(layoutAttributes)
    }

}

class VHCSingleCaptureResultsCollectionViewCell: VHCCaptureResultsCollectionViewCell, Nibloadable, UITextFieldDelegate {

    // MARK: Outlets

    @IBOutlet weak var userInputStackView: UIStackView!
    @IBOutlet weak var inputTextField: UITextField!
    @IBOutlet weak var validationErrorLabel: UILabel!

    // MARK: Properties

    var min: Double?
    var max: Double?
    var healthAttributeType: PartyAttributeTypeRef = .Unknown
    
    // MARK: Closures

    public var textFieldTextDidChange: ((_ textField: UITextField, _ unit: Unit?, _ min: inout Double?, _ max: inout Double?) -> Void)?

    public var textFieldDidEndEditing: ((_ textField: UITextField, _ unit: Unit?, _ min: inout Double?, _ max: inout Double?) -> Void)?

    public var measurementUnits = [Unit]()

    // MARK: Lifecycle

    override public func awakeFromNib() {
        super.awakeFromNib()

        setupInputTextField()
    }

    public override func prepareForReuse() {
        super.prepareForReuse()

        setupCell()
    }

    override func setupCell() {
        super.setupCell()

        validationErrorLabel.isHidden = true
        validationErrorLabel.font = .footnoteFont()
        validationErrorLabel.textColor = UIColor.cellErrorLabel()

        min = nil
        max = nil
        inputTextField.text = nil
        setValidationErrorText(nil, for: validationErrorLabel)
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        mainStackView.layer.removeAllBorderLayers()
        mainStackView.layer.addBorder(edge: .top)
        mainStackView.layer.addBorder(edge: .bottom)
        userInputStackView.layer.removeAllBorderLayers()
        userInputStackView.layer.addBorder(edge: .bottom)
        unitsArrowImageView.layer.removeAllBorderLayers()
        unitsArrowImageView.layer.addBorder(edge: .right)
    }

  // MARK: 

    override func stopEditing() {
        self.inputTextField.endEditing(true)
    }

    public func configureCaptureResultCell(units: [Unit], inputPlaceholder: String?, inputText: String?, inputUnitSymbol: String?, selectedDate: Date) {
        // units picker button
        if units.count > 1 {
            unitsArrowImageView.image = VIAUIKitAsset.arrowDrill.image
            unitsButton.tintColor = .lightGrey()
        } else {
            unitsArrowImageView.image = nil
        }

        // select units
        measurementUnits = units
        var selectedUnit: Unit?
        if let validSymbol = inputUnitSymbol {
            selectedUnit = Unit(symbol: validSymbol)
        }
        
        if let selectedUnit = selectedUnit ?? measurementUnits.first {
            self.selectedUnit = selectedUnit
            unitsButtonTitle = selectedUnit.symbolString()
        }

        // other
        dateTestedLabel.text = CommonStrings.CaptureResults.DateTested144
        inputTextField.placeholder = inputPlaceholder
        inputTextField.text = inputText
        updateDateButtonTitle(selectedDate)
        /* 
            Call text field did end editing to allow cell to recall
            input validity check
        */
        textFieldDidEndEditing(inputTextField)
    }

  // MARK: - Units

    override func showUnitsActionSheet() {
        // Dismiss keyboard on unit change.
        if AppSettings.getAppTenant() == .EC || AppSettings.getAppTenant() == .ASR {
            self.contentView.endEditing(true)
        }
        
        guard measurementUnits.count > 1 else { return }

        let unitsMenu = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        unitsMenu.popoverPresentationController?.sourceView = self
        let measurementFormatter = Localization.decimalShortStyle

        for unit in measurementUnits {
            let title = measurementFormatter.string(from: unit)
            let action = UIAlertAction(title: title, style: .default, handler: { (alert: UIAlertAction!) -> Void in
                self.selectedUnit = unit
                self.unitsButtonTitle = title
                self.inputTextField.text = nil
                self.setValidationErrorText(nil, for: self.validationErrorLabel)
            })
            unitsMenu.addAction(action)
        }

        let cancelAction = UIAlertAction(title: CommonStrings.CancelButtonTitle24, style: .cancel, handler: nil)
        unitsMenu.addAction(cancelAction)

        captureResultsDelegate?.show(menu: unitsMenu)
    }

 // MARK: - Textfield

    func setupInputTextField() {
        self.inputTextField.delegate = self
        self.inputTextField.addTarget(self, action: #selector(textFieldEditingChanged(_:)), for: .editingChanged)
        self.inputTextField.keyboardType = .decimalPad
        self.inputTextField.placeholder = nil
        self.inputTextField.font = UIFont.bodyFont()
    }

    public func textFieldDidBeginEditing(_ textField: UITextField) {
        // Reload IQKeyboard manager toolbar on focus.
        textField.reloadKeyboardToolbar()
        
        // Remove grouping separator on textfield focus.
        textField.text = textField.text?.removeNumberGroupingSeparator()
    }

    public func textFieldDidEndEditing(_ textField: UITextField) {
        guard let action = self.textFieldDidEndEditing else { return }
        action(textField, selectedUnit, &min, &max)
    }

    func textFieldEditingChanged(_ textField: UITextField) {
        guard let action = self.textFieldTextDidChange else { return }
        action(textField, selectedUnit, &min, &max)
    }
}

extension VHCSingleCaptureResultsCollectionViewCell {

    public func updateInputValid(_ valid: Bool) {
        if valid {
            self.inputIsValid()
        } else {
            self.inputIsNotValid()
        }
    }

    private func inputIsValid() {
        self.setValidationErrorText(nil, for: self.validationErrorLabel)
    }

    private func inputIsNotValid() {
        let decimalFormatter: NumberFormatter = {
            let shouldAcceptDecimalInput = VIAApplicableFeatures.default.shouldAcceptDecimalInput(self.selectedUnit!, questionTypeKey: 0, healthAttributeType: .Unknown)
            
            guard let formatter = VIAApplicableFeatures.default.decimalFormatter(shouldAcceptDecimalInput: shouldAcceptDecimalInput) else {
                return Localization.decimalFormatter
            }
            
            return formatter
        }()
        
        var text: String = ""
        if let min = self.min, let max = self.max {
            if let formattedMin = decimalFormatter.string(from: min as NSNumber), let formattedMax = decimalFormatter.string(from: max as NSNumber) {
                text = CommonStrings.ErrorRange180(formattedMin, formattedMax)
            }
        } else if let min = self.min, self.max == nil {
            if let formattedMin = decimalFormatter.string(from: min as NSNumber) {
                text = CommonStrings.ErrorRangeBigger281(formattedMin)
            }
        } else if let max = self.max, self.min == nil {
            if let formattedMax = decimalFormatter.string(from: max as NSNumber) {
                text = CommonStrings.ErrorRangeSmaller282(formattedMax)
            }
        }

        self.setValidationErrorText(text, for: self.validationErrorLabel)
    }
    
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        // TODO: Deprecated. Implementation moved to shouldChangeCharactersInAssessmentNumberRangeQuestionSection().
        //        if !(VIAApplicableFeatures.default.shouldAcceptDecimalInput(.init(symbol: ""), questionTypeKey: -1, healthAttributeType: healthAttributeType)) {
        //            let validInputs = NSCharacterSet(charactersIn:"0123456789").inverted
        //            let componentsToSeparate = string.components(separatedBy: validInputs)
        //            let numberFiltered = componentsToSeparate.joined(separator: "")
        //            return string == numberFiltered
        //        }
        //
        //        /**
        //         * if Vendor == SLI, IGI, and CA should conform to decimal format which is 1 decimal place
        //         * reference tickets:
        //         * SLI https://jira.vitalityservicing.com/browse/VA-23215
        //         * IGI https://jira.vitalityservicing.com/browse/VA-37909
        //         * CA https://jira.vitalityservicing.com/browse/FC-12140
        //         **/
        //        if (healthAttributeType == .Height || healthAttributeType == .Weight) {
        //            return VIAApplicableFeatures.default.shouldChangeCharactersIn(textField.text ?? "", range: range, replacementString: string)
        //        }
        //
        //        return VIAApplicableFeatures.default.shouldChangeCharactersInVHCSingleInput(textField.text ?? "", range: range, replacementString: string)
        
        return VIAApplicableFeatures.default.shouldChangeCharactersInAssessmentNumberRangeQuestionSection(textField, shouldChangeCharactersIn: range, replacementString: string, selectedUnit: selectedUnit!, questionTypeKey: 0, healthAttributeType: healthAttributeType)
    }
}
