import VIAUIKit
import VitalityKit
import VIACommon

public class VHCBloodPressureCaptureResultsCollectionViewCell: VHCCaptureResultsCollectionViewCell, Nibloadable, UITextFieldDelegate {

    // MARK: Outlets

    @IBOutlet weak var topInputFieldsStackView: UIStackView!
    @IBOutlet weak var bottomInputFieldsStackView: UIStackView!
    @IBOutlet weak var userInputStackView: UIStackView!
    @IBOutlet weak var topInputTextField: UITextField!
    @IBOutlet weak var bottomInputTextField: UITextField!
    @IBOutlet weak var topValidationErrorLabel: UILabel!
    @IBOutlet weak var bottomValidationErrorLabel: UILabel!

    // MARK: Properties

    public var topMin: Double?
    public var topMax: Double?
    public var bottomMin: Double?
    public var bottomMax: Double?

    // MARK: Closures

    public var topTextFieldTextDidChange: ((_ textField: UITextField, _ unit: Unit?, _ min: inout Double?, _ max: inout Double?) -> Void)?

    public var topTextFieldDidEndEditing: ((_ textField: UITextField, _ unit: Unit?, _ min: inout Double?, _ max: inout Double?) -> Void)?

    public var bottomTextFieldTextDidChange: ((_ textField: UITextField, _ unit: Unit?, _ min: inout Double?, _ max: inout Double?) -> Void)?

    public var bottomTextFieldDidEndEditing: ((_ textField: UITextField, _ unit: Unit?, _ min: inout Double?, _ max: inout Double?) -> Void)?

    public var measurementUnits = [Unit]()

    // MARK: Lifecycle

    override public func awakeFromNib() {
        super.awakeFromNib()

        setupInputTextFields()
    }

    public override func prepareForReuse() {
        super.prepareForReuse()

        setupCell()
    }

    public override func setupCell() {
        super.setupCell()

        topValidationErrorLabel.isHidden = true
        topValidationErrorLabel.font = .footnoteFont()
        topValidationErrorLabel.textColor = UIColor.cellErrorLabel()

        bottomValidationErrorLabel.isHidden = true
        bottomValidationErrorLabel.font = .footnoteFont()
        bottomValidationErrorLabel.textColor = UIColor.cellErrorLabel()

        topMax = nil
        topMin = nil
        bottomMax = nil
        bottomMin = nil
        topInputTextField.text = nil
        setValidationErrorText(nil, for: self.topValidationErrorLabel)
        bottomInputTextField.text = nil
        setValidationErrorText(nil, for: self.bottomValidationErrorLabel)
    }

    public override func layoutSubviews() {
        super.layoutSubviews()

        mainStackView.layer.removeAllBorderLayers()
        mainStackView.layer.addBorder(edge: .top)
        mainStackView.layer.addBorder(edge: .bottom)
        userInputStackView.layer.removeAllBorderLayers()
        userInputStackView.layer.addBorder(edge: .bottom)
        unitsArrowImageView.layer.removeAllBorderLayers()
        unitsArrowImageView.layer.addBorder(edge: .right)
        topInputFieldsStackView.layer.removeAllBorderLayers()
        topInputFieldsStackView.layer.addBorder(edge: .bottom)
    }

    // MARK: 

    public override func stopEditing() {
        self.topInputTextField.endEditing(true)
        self.bottomInputTextField.endEditing(true)
    }

    public func configureCaptureResultCell(units: [Unit]?, topInputText: String?, topInputPlaceholder: String?, bottomInputText: String?, bottomInputPlaceholder: String?, selectedDate: Date) {
        if let validUnits = units {
            if validUnits.count > 1 {
                unitsArrowImageView.image = VIAUIKitAsset.arrowDrill.image
                unitsButton.tintColor = .lightGrey()
            } else {
                unitsArrowImageView.image = nil
            }

            measurementUnits = validUnits
            if let firstUnit = measurementUnits.first {
                selectedUnit = firstUnit
                let measurementFormatter = Localization.decimalShortStyle
                unitsButtonTitle = measurementFormatter.string(from: firstUnit)
            }

            dateTestedLabel.text = CommonStrings.CaptureResults.DateTested144
            updateDateButtonTitle(selectedDate)

            topInputTextField.text = topInputText
            topInputTextField.placeholder = topInputPlaceholder
            bottomInputTextField.placeholder = bottomInputPlaceholder
            bottomInputTextField.text = bottomInputText

            // only trigger if there was actual input
            if let _ = topInputText {
                textFieldDidEndEditing(topInputTextField)
            }
            if let _ = bottomInputText {
                textFieldDidEndEditing(bottomInputTextField)
            }
        } else {
            debugPrint("Cannot configure cell without valid units")
        }
    }

    // MARK: - Units

    public override func showUnitsActionSheet() {
        // Dismiss keyboard on unit change. 
        if AppSettings.getAppTenant() == .EC || AppSettings.getAppTenant() == .ASR {
            self.contentView.endEditing(true)
        }
        
        guard measurementUnits.count > 1 else { return }

        let unitsMenu = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        unitsMenu.popoverPresentationController?.sourceView = self

        for unit in measurementUnits {
            let measurementFormatter = Localization.decimalShortStyle
            let title = measurementFormatter.string(from: unit)
            let action = UIAlertAction(title: title, style: .default, handler: { (alert: UIAlertAction!) -> Void in
                self.selectedUnit = unit
                self.unitsButtonTitle = title
                self.topInputTextField.text = nil
                self.setValidationErrorText(nil, for: self.topValidationErrorLabel)
                self.bottomInputTextField.text = nil
                self.setValidationErrorText(nil, for: self.bottomValidationErrorLabel)
            })
            unitsMenu.addAction(action)
        }

        let cancelAction = UIAlertAction(title: CommonStrings.CancelButtonTitle24, style: .cancel, handler: nil)
        unitsMenu.addAction(cancelAction)

        captureResultsDelegate?.show(menu: unitsMenu)
    }

   // MARK: - Textfield

    func setupInputTextFields() {
        self.topInputTextField.delegate = self
        self.topInputTextField.addTarget(self, action: #selector(textFieldEditingChanged(_:)), for: .editingChanged)
        self.topInputTextField.keyboardType = .decimalPad
        self.topInputTextField.placeholder = nil
        self.topInputTextField.font = UIFont.bodyFont()

        self.bottomInputTextField.delegate = self
        self.bottomInputTextField.addTarget(self, action: #selector(textFieldEditingChanged(_:)), for: .editingChanged)
        self.bottomInputTextField.keyboardType = .decimalPad
        self.bottomInputTextField.placeholder = nil
        self.bottomInputTextField.font = UIFont.bodyFont()
    }

    public func textFieldDidBeginEditing(_ textField: UITextField) {
        // Reload IQKeyboard manager toolbar on focus.
        textField.reloadKeyboardToolbar()
        
        // Remove grouping separator on textfield focus.
        textField.text = textField.text?.removeNumberGroupingSeparator()
        
        if textField == self.topInputTextField {
            //
        } else if textField == self.bottomInputTextField {
            //
        }
    }

    public func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == self.topInputTextField {
            guard let action = self.topTextFieldDidEndEditing else { return }
            action(textField, selectedUnit, &topMin, &topMax)
        } else if textField == self.bottomInputTextField {
            guard let action = self.bottomTextFieldDidEndEditing else { return }
            action(textField, selectedUnit, &bottomMin, &bottomMax)
        }
    }

    func textFieldEditingChanged(_ textField: UITextField) {
        if textField == self.topInputTextField {
            guard self.topTextFieldTextDidChange != nil else { return }
            self.topTextFieldTextDidChange!(textField, selectedUnit, &topMin, &topMax)
        } else if textField == self.bottomInputTextField {
            guard self.bottomTextFieldTextDidChange != nil else { return }
            self.bottomTextFieldTextDidChange!(textField, selectedUnit, &bottomMin, &bottomMax)
        }
    }
    
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if AppSettings.getAppTenant() == .UKE {
            let validInputs = NSCharacterSet(charactersIn:"0123456789").inverted
            let componentsToSeparate = string.components(separatedBy: validInputs)
            let numberFiltered = componentsToSeparate.joined(separator: "")
            return string == numberFiltered
        }
        // TODO: Deprecated. Implementation moved to shouldChangeCharactersInAssessmentNumberRangeQuestionSection().
        //        return VIAApplicableFeatures.default.shouldChangeCharactersInVHCBloodPressureInput(textField.text ?? "", range: range, replacementString: string)
        
        return VIAApplicableFeatures.default.shouldChangeCharactersInAssessmentNumberRangeQuestionSection(textField, shouldChangeCharactersIn: range, replacementString: string, selectedUnit: selectedUnit!, questionTypeKey: 0, healthAttributeType: .Unknown)
    }
    
    // MARK: Autosizing

    override open func preferredLayoutAttributesFitting(_ layoutAttributes: UICollectionViewLayoutAttributes) -> UICollectionViewLayoutAttributes {
        return self.defaultContentViewLayoutSizeFitting(layoutAttributes)
    }

}

extension VHCBloodPressureCaptureResultsCollectionViewCell {

    func updateTopInputValidationMessage(_ isValid: Bool, topInput: String?, bottomInput: String?) {
        if isValid {
            self.setValidationErrorText(nil, for: self.topValidationErrorLabel)
            self.checkDiastolicGreaterThanSystolicError(topValue: topInput, bottomValue: bottomInput)
        } else {
            self.setTopInputOutOfRangeMessage()
        }

        self.setRequiredInputMessageIfNeeded(topInput: topInput, bottomInput: bottomInput)
    }

    func updateBottomInputValidationMessage(_ isValid: Bool, topInput: String?, bottomInput: String?) {
        if isValid {
            self.setValidationErrorText(nil, for: self.bottomValidationErrorLabel)
            self.checkDiastolicGreaterThanSystolicError(topValue: topInput, bottomValue: bottomInput)
        } else {
            self.setBottomInputOutOfRangeMessage()
        }

        self.setRequiredInputMessageIfNeeded(topInput: topInput, bottomInput: bottomInput)
    }
    
    func checkDiastolicGreaterThanSystolicError(topValue: String?, bottomValue: String?) {
        // Compare Distolic vs Systolic values, then display error.
        if Int(topValue ?? "0") ?? 0 <= Int(bottomValue ?? "0") ?? 0 {
            self.setValidationErrorText(CommonStrings.Vhr.Assessment.BloodPressurePrompt2411, for: self.bottomValidationErrorLabel)
        } else {
            self.setValidationErrorText(nil, for: self.bottomValidationErrorLabel)
        }
    }

    private func setRequiredInputMessageIfNeeded(topInput: String?, bottomInput: String?) {
        let text = CommonStrings.ErrorRequired289
        let shouldHandleTop = topInput == nil || topInput!.isEmpty
        let shouldHandleBottom = bottomInput == nil || bottomInput!.isEmpty

        // both can't be required, then it means the user hasn't
        // and doesn't have to enter anything
        if shouldHandleTop && shouldHandleBottom {
            self.setValidationErrorText(nil, for: self.topValidationErrorLabel)
            self.setValidationErrorText(nil, for: self.bottomValidationErrorLabel)
            return
        }

        if shouldHandleTop {
            self.setValidationErrorText(text, for: self.topValidationErrorLabel)
        }
        if shouldHandleBottom {
            self.setValidationErrorText(text, for: self.bottomValidationErrorLabel)
        }
    }

    private func setTopInputOutOfRangeMessage() {
        self.setInputOutOfRangeMessage(for: self.topValidationErrorLabel, minValue: self.topMin, maxValue: self.topMax)
    }

    private func setBottomInputOutOfRangeMessage() {
        self.setInputOutOfRangeMessage(for: self.bottomValidationErrorLabel, minValue: self.bottomMin, maxValue: self.bottomMax)
    }

    private func setInputOutOfRangeMessage(for label: UILabel, minValue: Double?, maxValue: Double?) {
        let decimalFormatter: NumberFormatter = {
            let shouldAcceptDecimalInput = VIAApplicableFeatures.default.shouldAcceptDecimalInput(self.selectedUnit!, questionTypeKey: 0, healthAttributeType: .Unknown)
            
            guard let formatter = VIAApplicableFeatures.default.decimalFormatter(shouldAcceptDecimalInput: shouldAcceptDecimalInput) else {
                return Localization.decimalFormatter
            }
            
            return formatter
        }()
        
        var text: String = ""
        if let min = minValue, let max = maxValue {
            if let formattedMin = decimalFormatter.string(from: min as NSNumber), let formattedMax = decimalFormatter.string(from: max as NSNumber) {
                text = CommonStrings.ErrorRange180(formattedMin, formattedMax)
            }
        } else if let min = minValue, maxValue == nil {
            if let formattedMin = decimalFormatter.string(from: min as NSNumber) {
                text = CommonStrings.ErrorRangeBigger281(formattedMin)
            }
        } else if let max = maxValue, minValue == nil {
            if let formattedMax = decimalFormatter.string(from: max as NSNumber) {
                text = CommonStrings.ErrorRangeSmaller282(formattedMax)
            }
        }

        self.setValidationErrorText(text, for: label)
    }
    
}
