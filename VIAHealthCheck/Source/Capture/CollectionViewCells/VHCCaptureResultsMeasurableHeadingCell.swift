import UIKit
import SnapKit
import VIAUIKit
import VitalityKit

class VHCCaptureResultsMeasurableHeadingCell: UICollectionViewCell, Nibloadable {

    // MARK: Properties

    public var title: String? {
        get {
            return self.titleLabel.text
        }
        set {
            self.titleLabel.text = newValue
        }
    }

    public var image: UIImage? {
        get {
            return self.imageView.image
        }
        set {
            self.imageView.image = newValue
        }
    }

    // MARK: Outlets

    @IBOutlet private weak var container: UIStackView!

    @IBOutlet private weak var headingContainer: UIStackView!

    @IBOutlet private weak var titleLabel: UILabel!

    @IBOutlet private weak var imageView: UIImageView!

    // MARK: Init

    override func awakeFromNib() {
        super.awakeFromNib()

        setupViews()
    }

    func setupViews() {
        self.backgroundColor = UIColor.tableViewBackground()
        contentView.backgroundColor = UIColor.tableViewBackground()
        container.backgroundColor = UIColor.tableViewBackground()
        headingContainer.backgroundColor = UIColor.tableViewBackground()

        titleLabel.text = nil
        titleLabel.font = UIFont.title2Font()
        titleLabel.textColor = UIColor.night()
        titleLabel.backgroundColor = UIColor.tableViewBackground()

        imageView.image = nil
        imageView.contentMode = .center
        imageView.tintColor = AppSettings.isAODAEnabled() ? UIColor.knowYourHealthGreen() : UIColor.currentGlobalTintColor()
        imageView.backgroundColor = UIColor.tableViewBackground()
    }

    override func preferredLayoutAttributesFitting(_ layoutAttributes: UICollectionViewLayoutAttributes) -> UICollectionViewLayoutAttributes {
        return self.defaultContentViewLayoutSizeFitting(layoutAttributes)
    }

}
