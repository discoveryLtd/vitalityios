import VIAUIKit
import VitalityKit
import IGListKit
import VitalityKit
import VIAUtilities
import VIACommon

public class VHCMeasurmentDetailDataController {
    var vhcRealm = DataProvider.newVHCRealm()

    public func measurableDetailDataFromRealm(for groupTypeId: ProductFeatureTypeRef) -> [VHCDetailCellData] {
        vhcRealm.refresh()
        let realmGroup = vhcRealm.vhcHealthAttributeGroup(from: groupTypeId)
        var allGroupData = [VHCDetailCellData]()
        var pointsEarningMeasurables = [VHCDetailCellData]()
        var nonPointsEarningMeasurables = [VHCDetailCellData]()

        if let group = realmGroup {

            if group.type == .VHCBloodPressure {
                if let healthAttribute = group.healthAttributes.first {
                    if let latestEventMetaData = vhcRealm.latestVHCEventMetadata(for: healthAttribute.type) {
                        allGroupData.append(vhcDetailCellData(from: latestEventMetaData, group: group, healthAttribute: healthAttribute))
                    }
                }
            } else {
                for healthAttribute in group.healthAttributes {
                    if let latestEventMetaData = vhcRealm.latestVHCEventMetadata(for: healthAttribute.type) {
                        let healthAttributeData = vhcDetailCellData(from: latestEventMetaData, group: group, healthAttribute: healthAttribute)
                        if let earnedPoints = vhcRealm.earnedPointsFromLatestVHCEvent(for: healthAttribute.type) {
                            if earnedPoints > 0 {
                                pointsEarningMeasurables.append(healthAttributeData)
                            } else {
                                nonPointsEarningMeasurables.append(healthAttributeData)
                            }
                        }

                    }
                }
                allGroupData.append(contentsOf: sortAlphabetically(healthAttributeData: pointsEarningMeasurables))
                allGroupData.append(contentsOf: sortAlphabetically(healthAttributeData: nonPointsEarningMeasurables))
            }
        }

        return allGroupData
    }

    public func sortAlphabetically(healthAttributeData: [VHCDetailCellData]) -> [VHCDetailCellData] {
        return healthAttributeData.sorted { $0.HealthAttributeName < $1.HealthAttributeName }
    }

    public func vhcDetailCellData(from latestEventMetaData: VHCEventMetaData, group: VHCHealthAttributeGroup, healthAttribute: VHCHealthAttribute) -> VHCDetailCellData {
        vhcRealm.refresh()
        let landingDataHelper = VHCLandingDataController()
        let groupName = group.groupName()
        let groupTypeId = group.type
        var healthAtttributeName = ""
        let measurementValue = landingDataHelper.measurement(for: healthAttribute, eventMetaData: latestEventMetaData)
        let measurementUnits = landingDataHelper.unitsSymbol(for: latestEventMetaData.unitOfMeasure, healthAttributeType: healthAttribute.type)
        var healthyRangeFeedbackAdded = false
        var isOutOfMembershipPeriod = false
        var pointsText = ""
        var pointsExplanationText: String? = nil
        var potentialPoints = 0
        var earnedPoints = 0
        var sourceText: String? = nil
        var dateTestedText: String? = nil
        var vhcFeedback = [VHCFeedback]()
        var pointsImage = UIImage()

        if groupTypeId == .VHCBloodPressure{
            var healthFeedback: VHCFeedback?
            var isBPHealthy = true;
            var feedbackTypeName: String? = nil
            
            for healthAttribute in group.healthAttributes {

                if let allFeedback = vhcRealm.latestVHCHealthAttributeReadingFeedback(for: healthAttribute.type) {
                    for feedback in allFeedback {
                        guard let healthRangeFeedback = VIAApplicableFeatures.default.healthyRangeFeedback(from: feedback, isSystolicOrDiastolicHealthy: isBPHealthy, explanation: feedbackTypeName) else { continue }
                        guard let inHealthyRange = healthRangeFeedback.isInHealthyRange else { continue }
                        
                        if feedback.type == PartyAttributeFeedbackRef.SystolicAbove || feedback.type == PartyAttributeFeedbackRef.DiastolicAbove {
                            healthFeedback = healthRangeFeedback
                            guard let tenantID = AppSettings.getAppTenant(), tenantID == .SLI || tenantID == .EC || tenantID == .ASR else { break }
                            feedbackTypeName = feedback.feedbackTypeName
                            isBPHealthy = false
                        } else {
                            if inHealthyRange {
                                healthFeedback = healthRangeFeedback
                                break
                            } else {
                                if healthFeedback == nil {
                                    healthFeedback = healthRangeFeedback
                                }
                            }
                        }
                    }
                }
            }

            if let feedback = healthFeedback {
                vhcFeedback.append(feedback)
            }

        } else {
            if let allFeedback = vhcRealm.latestVHCHealthAttributeReadingFeedback(for: healthAttribute.type) {
                for feedback in allFeedback {
                    if !healthyRangeFeedbackAdded {
                        if let healthyRangeFeedback = VIAApplicableFeatures.default.healthyRangeFeedback(from: feedback, isSystolicOrDiastolicHealthy: true, explanation: nil) {
                            vhcFeedback.append(healthyRangeFeedback)
                            healthyRangeFeedbackAdded = true
                        }
                    }
                }
            }
        }

        dateTestedText = CommonStrings.GenericNotAvailable270
        if let eventDateTime = vhcRealm.latestVHCEventDateTime(for: healthAttribute.type) {
            dateTestedText = Localization.fullDateFormatter.string(from: eventDateTime)
            isOutOfMembershipPeriod = landingDataHelper.isOutOfCurrentMembershipPeriod(date: eventDateTime)
        }

        let integerFormatter = Localization.integerFormatter
        earnedPoints = landingDataHelper.totalEarnedPoints(for: healthAttribute)
        potentialPoints = landingDataHelper.totalPotentialPoints(for: healthAttribute)
        
        if groupTypeId == .VHCBloodPressure {
            if (AppSettings.getAppTenant() == .UKE ||
                AppSettings.getAppTenant() == .IGI ||
                AppSettings.getAppTenant() == .EC ||
                AppSettings.getAppTenant() == .ASR) {
                if let group = healthAttribute.group.first {
                    earnedPoints = landingDataHelper.totalEarnedPoints(for: group)
                    potentialPoints = landingDataHelper.totalPotentialPoints(for: group)
                }
            }
            if AppSettings.getAppTenant() == .SLI {
                earnedPoints = landingDataHelper.totalEarnedPoints(for: group)
                potentialPoints = landingDataHelper.totalPotentialPoints(for: group)
            }
        }
        
        if earnedPoints == 0 {
            pointsText = CommonStrings.Points.NoPointsEarnedTitle194
        } else if earnedPoints > 0 && !isOutOfMembershipPeriod {
            if let formattedEarnedPoints = integerFormatter.string(from: earnedPoints as NSNumber), let formattedPotentialPoints = integerFormatter.string(from:  potentialPoints as NSNumber) {
                pointsText = CommonStrings.Points.XOfYPointsTitle193(formattedEarnedPoints, formattedPotentialPoints)
            }
        } else {
            if potentialPoints > 0 {
                if let formattedPotentialPoints = integerFormatter.string(from: potentialPoints as NSNumber) {
                    pointsText = CommonStrings.HomeCard.CardEarnUpToPointsMessage124(formattedPotentialPoints)
                }
            }
        }

        pointsExplanationText = pointsReason(for: healthAttribute, earnedPoints: earnedPoints, potentialPoints: potentialPoints)

        pointsImage = VIAHealthCheckAsset.vhcGenericPointsLarge.templateImage

        vhcFeedback.append(VHCFeedback(image: pointsImage, description: pointsText, explanation: pointsExplanationText))

        if let name = healthAttribute.title() {
            healthAtttributeName = name
        }

        if let source = vhcRealm.eventSourceFromLatestVHCEvent(for: healthAttribute.type) {
            if source.type == EventSourceRef.MobileApp {
                sourceText = CommonStrings.DetailScreen.SourceSelfSubmittedMessage202
            } else {
                sourceText = source.eventSourceName
            }
        }


        let cellData = VHCDetailCellData()
        cellData.groupName = groupName
        cellData.groupTypeId = groupTypeId.rawValue
        cellData.HealthAttributeName = healthAtttributeName
        cellData.measurementValue = measurementValue
        cellData.measurementDescription = healthAttribute.descriptionForValidValue(measurementValue)
        cellData.measurementUnits = measurementUnits
        cellData.sourceText = sourceText
        cellData.potentialPoints = potentialPoints
        cellData.earnedPoints = earnedPoints
        cellData.dateTestedText = dateTestedText
        cellData.isOutOfMembershipPeriod = isOutOfMembershipPeriod
        cellData.pointsAndHealthyRangeFeedback = vhcFeedback
/**
*       If healthAtttributeName = Height or Weight, do not add Points And Healthy Range Feedback
        If healthAtttributeName = Body Mass Index, do not display Source and Date Tested
*/
        if groupTypeId == .VHCBMI{
            if (healthAtttributeName == CommonStrings.Measurement.HeightTitle145 || healthAtttributeName == CommonStrings.Measurement.WeightTitle146){
                cellData.pointsAndHealthyRangeFeedback = [VHCFeedback]()
            }else if (healthAtttributeName == CommonStrings.Measurement.BodyMassIndexTitle134){
                cellData.dateTestedText = nil
                cellData.sourceText = nil
            }
        }
        
        
        

        return cellData
    }

    // MARK: - Helper functions

    public func pointsReason(for healthAttribute: VHCHealthAttribute, earnedPoints: Int, potentialPoints: Int) -> String? {
        vhcRealm.refresh()

        var pointsReasonText: String? = nil

        if let healthAttributeGroupType = healthAttribute.group.first?.type, healthAttributeGroupType == .VHCBloodPressure {
            let eventTypeRef: EventTypeRef = .BloodPressure
            guard let reasonKey = vhcRealm.pointsReasonKey(for: eventTypeRef) else { return nil }
            pointsReasonText = pointsReason(for: healthAttribute, reasonKey: reasonKey, eventTypeRef: eventTypeRef, earnedPoints: earnedPoints, potentialPoints: potentialPoints)
            return pointsReasonText
        }

        guard let eventTypeRef = healthAttribute.eventType.first?.type else { return nil }
        guard let reasonKey = vhcRealm.pointsReasonKey(for: eventTypeRef) else { return nil }
        pointsReasonText = pointsReason(for: healthAttribute, reasonKey: reasonKey, eventTypeRef: eventTypeRef, earnedPoints: earnedPoints, potentialPoints: potentialPoints)
        return pointsReasonText
    }

    internal func pointsReason(for healthAttribute: VHCHealthAttribute, reasonKey: Int, eventTypeRef: EventTypeRef, earnedPoints: Int, potentialPoints: Int) -> String? {
        let landingDataHelper = VHCLandingDataController()
        var pointsReasonText: String? = nil
        var groupTotalEarnedPoints: Int = -1
        var groupMaxPotentialPoints: Int = -1

        if let group = healthAttribute.group.first {
            groupTotalEarnedPoints = landingDataHelper.totalEarnedPoints(for: group)
            groupMaxPotentialPoints = landingDataHelper.maximumPotentialPoints(for: group)
        }

        pointsReasonText = vhcRealm.pointsReason(for: eventTypeRef)

        if groupTotalEarnedPoints > 0 && groupMaxPotentialPoints > 0 {
            if groupTotalEarnedPoints == groupMaxPotentialPoints || reasonKey == 200 {
                pointsReasonText = CommonStrings.DetailScreen.YouEarnedMaxPointsMessage197
                return pointsReasonText
            } else if reasonKey == 207 {
                let remainingPoints = potentialPoints - earnedPoints
                guard let formattedPoints = Localization.integerFormatter.string(from: remainingPoints as NSNumber) else { return pointsReasonText }
                pointsReasonText = CommonStrings.DetailScreen.EarnMorePointsMessage201(formattedPoints)
                return pointsReasonText
            }
        }

        return pointsReasonText
    }
}
