//
//  VHCLandingDataController.swift
//  VitalityActive
//
//  Created by Lionel Camacho on 2017/03/29.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import RealmSwift
import VitalityKit
import VIAUIKit
import VIAUtilities
import VitalityKit
import VIACommon

public struct VHCLandingCellData {
    public var groupName: String
    public var groupTypeId: ProductFeatureTypeRef
    public var groupIcon: UIImage
    public var messageIcon: UIImage?
    public var message: String?
    public var pointsAndHealthyRangeFeedback: [VHCFeedback]?
    public var measurementValue: String?
    public var measurementUnits: String?
    public var isOutOfMembershipPeriod: Bool
    public var isDisclosureIndicatorHidden: Bool
    public var footerMessage: String?
}

public class VHCLandingDataController {
    var vhcRealm = DataProvider.newVHCRealm()

    public  func dataForGroupsFromRealm() -> [VHCLandingCellData] {
        vhcRealm.refresh()

        let realmGroups = VHCHealthAttributeGroup.sortedGroups(from: vhcRealm)
        var allGroupsData = [VHCLandingCellData]()
        var greyedOutGroups = [VHCLandingCellData]()
        var groupsWithMainAttributeReadings = [VHCHealthAttributeGroup]()
        var groupsWithMainAttributeReadingOutsidePeriod = [VHCHealthAttributeGroup]()
        var groupsWithoutAttributeReadings = [VHCHealthAttributeGroup]()
        var groupsWithNonmainAttributeReadings = [VHCHealthAttributeGroup]()

        for group in realmGroups {
            var groupHasNoReadings = true
            var hasMainAttributeForDisplay = false
            var isOutsideMembershipPeriod = false

            for healthAttribute in group.healthAttributes {
                let isMainAttribute = isMainHealthAttributeForDisplay(healthAttribute: healthAttribute)
                if  vhcRealm.latestVHCEventWithReadingAndEventMetadata(for: healthAttribute.type) != nil {

                    if group.type == .VHCBloodPressure {
                        let systolicEventMetaData = vhcRealm.latestVHCEventMetadata(for: PartyAttributeTypeRef.BloodPressureSystol)
                        let diastolicEventMetaData = vhcRealm.latestVHCEventMetadata(for: PartyAttributeTypeRef.BloodPressureDiasto)
                        if systolicEventMetaData != nil && diastolicEventMetaData != nil {
                            groupHasNoReadings = false
                        } else {
                            groupHasNoReadings = true
                        }
                    } else {
                        groupHasNoReadings = false
                    }

                    if hasMainAttributeForDisplay == false {
                        hasMainAttributeForDisplay = isMainAttribute
                    }

                    if let eventDate = vhcRealm.latestVHCEventDateTime(for: healthAttribute.type) {
                        if isMainAttribute {
                            isOutsideMembershipPeriod = isOutOfCurrentMembershipPeriod(date: eventDate)
                        }
                    }
                }
            }
            
            /**
             Graying out of attribute will not depend on potentialPoints
             (aligning with Android implementation)
             ge20180322 for VA-26853
             */
            if groupHasNoReadings {
                groupsWithoutAttributeReadings.append(group)
            } else if !groupHasNoReadings && !isOutsideMembershipPeriod {
                groupsWithMainAttributeReadings.append(group)
            } else {
                groupsWithMainAttributeReadingOutsidePeriod.append(group)
            }
            
        }

//        groupsWithMainAttributeReadings.sort { $0.groupName() < $1.groupName() }
//        groupsWithMainAttributeReadingOutsidePeriod.sort { $0.groupName() < $1.groupName() }
//        groupsWithoutAttributeReadings.sort { $0.groupName() < $1.groupName() }
//        groupsWithNonmainAttributeReadings.sort { $0.groupName() < $1.groupName() }

        for groupWithoutReadings in groupsWithoutAttributeReadings {
            greyedOutGroups.append(vhcLandingCellDataWithNoReadings(for: groupWithoutReadings))
        }

        for groupWithNonmainAttributeReadings in groupsWithNonmainAttributeReadings {
            greyedOutGroups.append(vhcLandingCellDataWithReadingsNotMainMeasurement(for: groupWithNonmainAttributeReadings))
        }

        for groupWithAttributeReadingOutsidePeriod in groupsWithMainAttributeReadingOutsidePeriod {
            greyedOutGroups.append(vhcLandingCellDataWithMainReadings(for: groupWithAttributeReadingOutsidePeriod, isOutOfMembershipPeriod: true))
        }

        for greyedOutGroup in greyedOutGroups {
            allGroupsData.append(greyedOutGroup)
        }

        for groupWithReadings in groupsWithMainAttributeReadings {
            allGroupsData.append(vhcLandingCellDataWithMainReadings(for: groupWithReadings, isOutOfMembershipPeriod: false))
        }

        return allGroupsData
    }

    // MARK: - Cell data setup functions

    //Handles case where no readings have been recorded
    public func vhcLandingCellDataWithNoReadings(for group: VHCHealthAttributeGroup) -> VHCLandingCellData {
        let groupName = group.groupName()
        let groupTypeId = group.type
        var groupMaxPotentialPoints = 0
        var earnPointsMessage: String?
        var messageIcon: UIImage?
        var groupIcon = UIImage()
        if let image = group.assetImage?.templatedImage {
            groupIcon = image
        }

        let integerFormatter = Localization.integerFormatter
        groupMaxPotentialPoints = maximumPotentialPoints(for: group)
        if groupMaxPotentialPoints > 0 {
            if let formattedPoints = integerFormatter.string(from: groupMaxPotentialPoints as NSNumber) {
                earnPointsMessage = CommonStrings.HomeCard.CardEarnUpToPointsMessage124(formattedPoints)
                messageIcon = VIAHealthCheckAsset.vhcGenericPointsSmall.templateImage
            }
        }

        return VHCLandingCellData(groupName: groupName, groupTypeId: groupTypeId, groupIcon: groupIcon, messageIcon: messageIcon, message: earnPointsMessage, pointsAndHealthyRangeFeedback: nil, measurementValue: nil, measurementUnits: nil, isOutOfMembershipPeriod: true, isDisclosureIndicatorHidden: true, footerMessage: nil)
    }

    //Handles case where qualifying measurements were captured in and out of the current membership period
    public func vhcLandingCellDataWithMainReadings(for group: VHCHealthAttributeGroup, isOutOfMembershipPeriod: Bool) -> VHCLandingCellData {
        var vhcFeedback = [VHCFeedback]()
        let groupName = group.groupName()
        let groupTypeId = group.type
        var pointsText = ""
        var measurementValue = ""
        var measurementUnits = ""
        var groupMaxPotentialPoints = 0
        var groupTotalEarnedPoints = 0
        var groupIcon = UIImage()
        var pointsImage = UIImage()
        if let image = group.assetImage?.templatedImage {
            groupIcon = image
        }

        var healthFeedback: VHCFeedback?
        var isBPHealthy = true;
        var feedbackTypeName: String? = nil
        
        for healthAttribute in group.healthAttributes {
            let isMainAttribute = isMainHealthAttributeForDisplay(healthAttribute: healthAttribute)
            if isMainAttribute {
                if let eventMetaData = vhcRealm.latestVHCEventMetadata(for: healthAttribute.type) {
                    measurementValue = measurement(for: healthAttribute, eventMetaData: eventMetaData)
                    if let unitSymbol = unitsSymbol(for: eventMetaData.unitOfMeasure, healthAttributeType: healthAttribute.type) {
                        measurementUnits = unitSymbol
                    }
                }
            }

            if let allFeedback = vhcRealm.latestVHCHealthAttributeReadingFeedback(for: healthAttribute.type) {
                for feedback in allFeedback {
                    guard let healthRangeFeedback = VIAApplicableFeatures.default.healthyRangeFeedback(from: feedback, isSystolicOrDiastolicHealthy: isBPHealthy, explanation: feedbackTypeName) else { continue }
                    guard let inHealthyRange = healthRangeFeedback.isInHealthyRange else { continue }
                    
                    if feedback.type == PartyAttributeFeedbackRef.SystolicAbove || feedback.type == PartyAttributeFeedbackRef.DiastolicAbove {
                        healthFeedback = healthRangeFeedback
                        guard let tenantID = AppSettings.getAppTenant(), tenantID == .SLI || tenantID == .EC || tenantID == .ASR else { break }
                        feedbackTypeName = feedback.feedbackTypeName
                        isBPHealthy = false
                    } else {
                        if inHealthyRange {
                            healthFeedback = healthRangeFeedback
                            break
                        } else {
                            if healthFeedback == nil {
                                healthFeedback = healthRangeFeedback
                            }
                            
                            /** FC-22535
                             * Status: Out of Healthy Range / Group: BMI
                             * Purpose: Override the stocked value in "healthFeedback" and replaced it with the correct value.
                             */
                            if let tenantID = AppSettings.getAppTenant(), tenantID == .GI {
                                if feedback.type == PartyAttributeFeedbackRef.BMIAbove {
                                    healthFeedback = healthRangeFeedback
                                }
                            }
                        }
                    }
                }
            }
        }

        if let feedback = healthFeedback {
            debugPrint("Health Feedback: \(feedback)")
            vhcFeedback.append(feedback)
        }

        let integerFormatter = Localization.integerFormatter
        groupTotalEarnedPoints = totalEarnedPoints(for: group)
        groupMaxPotentialPoints = maximumPotentialPoints(for: group)
        if groupTotalEarnedPoints == 0 && !isOutOfMembershipPeriod {
            pointsText = CommonStrings.Points.NoPointsEarnedTitle194
        } else if groupTotalEarnedPoints > 0 && !isOutOfMembershipPeriod {
            if let formattedEarnedPoints = integerFormatter.string(from: groupTotalEarnedPoints as NSNumber), let formattedPotentialPoints = integerFormatter.string(from:  groupMaxPotentialPoints as NSNumber) {
                pointsText = CommonStrings.Points.XOfYPointsTitle193(formattedEarnedPoints, formattedPotentialPoints)
            }
        } else {
            if groupMaxPotentialPoints > 0 {
                if let formattedPotentialPoints = integerFormatter.string(from: groupMaxPotentialPoints as NSNumber) {
                    pointsText = CommonStrings.HomeCard.CardEarnUpToPointsMessage124(formattedPotentialPoints)
                }
            }
        }

        pointsImage = VIAHealthCheckAsset.vhcGenericPointsSmall.templateImage

        vhcFeedback.append(VHCFeedback(image: pointsImage, description: pointsText))

        return VHCLandingCellData(groupName: groupName, groupTypeId: groupTypeId, groupIcon: groupIcon, messageIcon: nil, message: nil, pointsAndHealthyRangeFeedback: vhcFeedback, measurementValue: measurementValue, measurementUnits: measurementUnits, isOutOfMembershipPeriod: isOutOfMembershipPeriod, isDisclosureIndicatorHidden: false, footerMessage: nil)

    }

    //Handles case where readings were not for qualifying measurement
    public func vhcLandingCellDataWithReadingsNotMainMeasurement(for group: VHCHealthAttributeGroup) -> VHCLandingCellData {
        let groupName = group.groupName()
        let groupTypeId = group.type
        var vhcFeedback: [VHCFeedback]?
        var groupMaxPotentialPoints = 0
        var footerMessage: String?
        var groupIcon = UIImage()
        var pointsImage = UIImage()
        if let image = group.assetImage?.templatedImage {
            groupIcon = image
        }

        groupMaxPotentialPoints = maximumPotentialPoints(for: group)

        pointsImage = VIAHealthCheckAsset.vhcGenericPointsSmall.templateImage
        
        if groupMaxPotentialPoints > 0 {
            let integerFormatter = Localization.integerFormatter
            if let formattedPotentialPoints = integerFormatter.string(from: groupMaxPotentialPoints as NSNumber) {
                let pointsFeedback = VHCFeedback(image: pointsImage, description: CommonStrings.HomeCard.CardEarnUpToPointsMessage124(formattedPotentialPoints))
                vhcFeedback = [pointsFeedback]
            }
        }
        footerMessage = CommonStrings.LandingScreen.PointsMessage249

        return VHCLandingCellData(groupName: groupName, groupTypeId: groupTypeId, groupIcon: groupIcon, messageIcon: nil, message: nil, pointsAndHealthyRangeFeedback: vhcFeedback, measurementValue: nil, measurementUnits: nil, isOutOfMembershipPeriod: true, isDisclosureIndicatorHidden: false, footerMessage: footerMessage)
    }

    public func unitsSymbol(for unitsKey: String, healthAttributeType: PartyAttributeTypeRef) -> String? {
        guard let unitsKeyInt = Int(unitsKey), let uomType = UnitOfMeasureRef(rawValue: unitsKeyInt) else {
            return nil
        }
        let validValues = VHCHealthAttributeValidValues()
        validValues.unitOfMeasureType = uomType
        return validValues.unitOfMeasureType.unitName()
    }

    public func isMainHealthAttributeForDisplay(healthAttribute: VHCHealthAttribute) -> Bool {
        let potentialPoints = totalPotentialPoints(for: healthAttribute)

        if potentialPoints > 0 {
            return true
        } else {
            return false
        }
    }

    public func isOutOfCurrentMembershipPeriod(date: Date) -> Bool {
        guard let membershipStartDate = DataProvider.currentMembershipPeriodStartDate() else { return true }
        guard let membershipEndDate = DataProvider.currentMembershipPeriodEndDate() else { return true }

        if date < membershipStartDate || date > membershipEndDate {
            return true
        }

        return false
    }

    public func maximumPotentialPoints(for group: VHCHealthAttributeGroup) -> Int {
        var maxPotentialPoints = 0

        switch group.type {
        case .VHCBloodPressure:
            if let latestEventPotentialPoints = vhcRealm.potentialPoints(for: EventTypeRef.BloodPressure) {
                maxPotentialPoints = latestEventPotentialPoints
            }
            return maxPotentialPoints
        case .VHCBMI:
            if let latestEventPotentialPoints = vhcRealm.potentialPoints(for: EventTypeRef.BMI) {
                maxPotentialPoints = latestEventPotentialPoints
            }
            return maxPotentialPoints
        default:
            for healthAttribute in group.healthAttributes {
                let healthAttributePotentialPoints = totalPotentialPoints(for: healthAttribute)
                if healthAttributePotentialPoints > maxPotentialPoints {
                    maxPotentialPoints = healthAttributePotentialPoints
                }
            }
            return maxPotentialPoints
        }

    }

    public func totalEarnedPoints(for group: VHCHealthAttributeGroup) -> Int {
        var points = 0

        switch group.type {
        case .VHCBloodPressure:
            if let latestEventEarnedlPoints = vhcRealm.earnedPoints(for: EventTypeRef.BloodPressure) {
                points = latestEventEarnedlPoints
            }
            return points
        case .VHCBMI:
            if let latestEventEarnedlPoints = vhcRealm.earnedPoints(for: EventTypeRef.BMI) {
                points = latestEventEarnedlPoints
            }
            return points
        default:
            for healthAttribute in group.healthAttributes {
                let healthAttributeEarnedPoints = totalEarnedPoints(for: healthAttribute)
                points += healthAttributeEarnedPoints
            }
            return points
        }
    }
    
    public func totalPotentialPoints(for group: VHCHealthAttributeGroup) -> Int {
        var points = 0
        switch group.type {
        case .VHCBloodPressure:
            if let latestEventEarnedlPoints = vhcRealm.potentialPoints(for: EventTypeRef.BloodPressure) {
                points = latestEventEarnedlPoints
            }
            return points
        default:
            return points
        }
    }
    public func totalPotentialPoints(for healthAttribute: VHCHealthAttribute) -> Int {
        var points = 0
        if let pointsEventType = healthAttribute.eventType.first {
            points = pointsEventType.totalPotentialPoints
        }
        return points
    }

    public func totalEarnedPoints(for healthAttribute: VHCHealthAttribute) -> Int {
        var points = 0
        if let pointsEventType = healthAttribute.eventType.first {
            points = pointsEventType.totalEarnedPoints
        }
        return points
    }

    public func measurement(for healthAttribute: VHCHealthAttribute, eventMetaData: VHCEventMetaData) -> String {
        var measurementValue =  ""
        switch healthAttribute.type {
        case .BloodPressureDiasto,
             .BloodPressureSystol:
            let systolicValue = vhcRealm.latestReadingValueFromEventMetadata(for: .BloodPressureSystol)
            let diastolicValue = vhcRealm.latestReadingValueFromEventMetadata(for: .BloodPressureDiasto)
            if let diastolic = diastolicValue, let systolic = systolicValue {
                let integerFormatter = Localization.integerFormatter
                integerFormatter.roundingMode = .down
                if let diastolicFormattedString = integerFormatter.string(from: diastolic as NSNumber), let systolicFormattedString = integerFormatter.string(from: systolic as NSNumber) {
                    measurementValue = "\(systolicFormattedString) / \(diastolicFormattedString)"
                }
            }
        default:
            if let readingValue = vhcRealm.latestReadingValueFromEventMetadata(for: healthAttribute.type) {
                let decimalFormatter = Localization.decimalFormatter
                if let value = decimalFormatter.string(from: readingValue as NSNumber) {
                    measurementValue = value
                }
            } else {
                //Needed for SLI Urine Protein
                measurementValue = vhcRealm.latestEventMetadataRawValue(for: healthAttribute.type)
            }
        }
        return measurementValue
    }

}
