import VitalityKit
import VitalityKit
import VIAUIKit

public class VHCOnboardingViewModel: AnyObject, OnboardingViewModel {
    public var heading: String {
        return CommonStrings.HomeCard.CardTitle125
    }

    public var contentItems: [OnboardingContentItem] {
        var items = [OnboardingContentItem]()
        items.append(OnboardingContentItem(heading: CommonStrings.Onboarding.Section1Title126,
                                           content: CommonStrings.Onboarding.Section1Message127,
                                           image: VIAHealthCheckAsset.vhcGenericHealthcare.image))
        items.append(OnboardingContentItem(heading: CommonStrings.Onboarding.Section2Title128,
                                           content: CommonStrings.Onboarding.Section2Message129,
                                           image: VIAHealthCheckAsset.vhcGenericBMI.image))
        items.append(OnboardingContentItem(heading: CommonStrings.Onboarding.Section3Title130,
                                           content: CommonStrings.Onboarding.Section3Message341,
                                           image: VIAHealthCheckAsset.vhcGenericPointsLarge.image))
        return items
    }

    public var buttonTitle: String {
        return CommonStrings.GenericGotItButtonTitle131
    }

    public var alternateButtonTitle: String? {
        return CommonStrings.LearnMoreButtonTitle104
    }

    public var gradientColor: Color {
        return .green
    }
    
    public var mainButtonHighlightedTextColor: UIColor? {
        return .knowYourHealthGreen()
    }
}
