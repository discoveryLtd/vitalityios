//
//  VHCLandingViewController.swift
//  VitalityActive
//
//  Created by Simon Stewart on 2/22/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

import VitalityKit
import VitalityKit
import VIAUIKit

public class VHCOnboardingViewController: VIAOnboardingViewController, KnowYourHealthTintable {

    public override func awakeFromNib() {
        super.awakeFromNib()

        self.viewModel = VHCOnboardingViewModel()
    }

    public override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    public override func viewDidLoad() {
        super.viewDidLoad()
    }

    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        navigationController?.navigationBar.isHidden = false
    }

    public override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        self.hideBackButtonTitle()
    }

        // MARK: - Navigation
    override public func mainButtonTapped(_ sender: UIButton) {
        AppSettings.setHasShownVHCOnboarding()
        self.dismiss(animated: true, completion: nil)
    }

    override public func alternateButtonTapped(_ sender: UIButton) {
        self.performSegue(withIdentifier: "segueLearnMore", sender: self)
    }
}
