// Generated using SwiftGen, by O.Halligon — https://github.com/SwiftGen/SwiftGen

#if os(OSX)
import AppKit.NSImage
public typealias VIAHealthCheckColor = NSColor
public typealias VIAHealthCheckImage = NSImage
#elseif os(iOS) || os(tvOS) || os(watchOS)
import UIKit.UIImage
public typealias VIAHealthCheckColor = UIColor
public typealias VIAHealthCheckImage = UIImage
#endif

// swiftlint:disable file_length

public typealias VIAHealthCheckAssetType = VIAHealthCheckImageAsset

public struct VIAHealthCheckImageAsset: Equatable {
	fileprivate var name: String

	public var templateImage: VIAHealthCheckImage {
		let bundle = Bundle(for: BundleToken.self)
		#if os(iOS) || os(tvOS)
		let image = VIAHealthCheckImage(named: name, in: bundle, compatibleWith: nil)?.withRenderingMode(.alwaysTemplate)
		#elseif os(OSX)
		let image = bundle.image(forResource: name).withRenderingMode(.alwaysTemplate)
		#elseif os(watchOS)
		let image = VIAHealthCheckImage(named: name).withRenderingMode(.alwaysTemplate)
		#endif
		guard let result = image else { fatalError("Unable to load template image named \(name).") }
		return result
	}

	public var image: VIAHealthCheckImage {
		let bundle = Bundle(for: BundleToken.self)
		#if os(iOS) || os(tvOS)
		let image = VIAHealthCheckImage(named: name, in: bundle, compatibleWith: nil)
		#elseif os(OSX)
		let image = bundle.image(forResource: name)
		#elseif os(watchOS)
		let image = VIAHealthCheckImage(named: name)
		#endif
		guard let result = image else { fatalError("Unable to load image named \(name).") }
		return result
	}

	public static func ==(lhs: VIAHealthCheckImageAsset, rhs: VIAHealthCheckImageAsset) -> Bool {
        return lhs.image == rhs.image
    }
}

public struct VIAHealthCheckColorAsset {
fileprivate var name: String

#if swift(>=3.2)
@available(iOS 11.0, tvOS 11.0, watchOS 4.0, OSX 10.13, *)
var color: VIAHealthCheckColor {
return VIAHealthCheckColor(asset: self)
}
#endif
}

// swiftlint:disable identifier_name line_length nesting type_body_length type_name
public enum VIAHealthCheckAsset {
  public static let vhcGenericInHealthyRangeSmall = VIAHealthCheckImageAsset(name: "vhcGenericInHealthyRangeSmall")
  public static let vhcGenericBloodGlucose = VIAHealthCheckImageAsset(name: "vhcGenericBloodGlucose")
  public static let vhcGenericWaistCircumference = VIAHealthCheckImageAsset(name: "vhcGenericWaistCircumference")
  public static let vhcGenericOutOfHealthyRangeLarge = VIAHealthCheckImageAsset(name: "vhcGenericOutOfHealthyRangeLarge")
  public static let vhcHelp = VIAHealthCheckImageAsset(name: "vhcHelp")
  public static let arrowDrill = VIAHealthCheckImageAsset(name: "arrowDrill")
  public static let vhcGeneralDocsSmall = VIAHealthCheckImageAsset(name: "vhcGeneralDocsSmall")
  public static let vhcGenericPointsLarge = VIAHealthCheckImageAsset(name: "vhcGenericPointsLarge")
  public static let vhcIcnPointsSmall = VIAHealthCheckImageAsset(name: "vhcIcnPointsSmall")
  public static let vhcGenericUrineProtein = VIAHealthCheckImageAsset(name: "vhcGenericUrineProtein")
  public static let vhcGenericHealthcare = VIAHealthCheckImageAsset(name: "vhcGenericHealthcare")
  public static let vhcCheckMarkSingle = VIAHealthCheckImageAsset(name: "vhcCheckMarkSingle")
  public static let vhcGenericBloodPressure = VIAHealthCheckImageAsset(name: "vhcGenericBloodPressure")
  public static let vhcGenericHistory = VIAHealthCheckImageAsset(name: "vhcGenericHistory")
  public static let vhcGenericBMI = VIAHealthCheckImageAsset(name: "vhcGenericBMI")
  public static let vhcEarnPointsBig = VIAHealthCheckImageAsset(name: "vhcEarnPointsBig")
  public static let vhcPartnerSoftBank = VIAHealthCheckImageAsset(name: "vhcPartnerSoftBank")
  public static let vhcGenericCholesterol = VIAHealthCheckImageAsset(name: "vhcGenericCholesterol")
  public static let vhcGenericUrine = VIAHealthCheckImageAsset(name: "vhcGenericUrine")
  public static let vhcCompletedBig = VIAHealthCheckImageAsset(name: "vhcCompletedBig")
  public static let vhcGenericOutOfHealthyRangeSmall = VIAHealthCheckImageAsset(name: "vhcGenericOutOfHealthyRangeSmall")
  public static let vhcGenericHba1c = VIAHealthCheckImageAsset(name: "vhcGenericHba1c")
  public static let vhcGenericInHealthyRangeLarge = VIAHealthCheckImageAsset(name: "vhcGenericInHealthyRangeLarge")
  public static let vhcIcnTipsSmall = VIAHealthCheckImageAsset(name: "vhcIcnTipsSmall")
  public static let vhcGenericQuestionMark = VIAHealthCheckImageAsset(name: "vhcGenericQuestionMark")
  public static let vhcGenericLearnMore = VIAHealthCheckImageAsset(name: "vhcGenericLearnMore")
  public static let vhcHealthcareBig = VIAHealthCheckImageAsset(name: "vhcHealthcareBig")
  public static let vhcGenericMask = VIAHealthCheckImageAsset(name: "vhcGenericMask")
  public static let vhcGenericPointsSmall = VIAHealthCheckImageAsset(name: "vhcGenericPointsSmall")
  public static let arOnboardingActivated = VIAHealthCheckImageAsset(name: "AROnboardingActivated")

  // swiftlint:disable trailing_comma
  public static let allColors: [VIAHealthCheckColorAsset] = [
  ]
  public static let allImages: [VIAHealthCheckImageAsset] = [
    vhcGenericInHealthyRangeSmall,
    vhcGenericBloodGlucose,
    vhcGenericWaistCircumference,
    vhcGenericOutOfHealthyRangeLarge,
    vhcHelp,
    arrowDrill,
    vhcGeneralDocsSmall,
    vhcGenericPointsLarge,
    vhcIcnPointsSmall,
    vhcGenericUrineProtein,
    vhcGenericHealthcare,
    vhcCheckMarkSingle,
    vhcGenericBloodPressure,
    vhcGenericHistory,
    vhcGenericBMI,
    vhcEarnPointsBig,
    vhcPartnerSoftBank,
    vhcGenericCholesterol,
    vhcGenericUrine,
    vhcCompletedBig,
    vhcGenericOutOfHealthyRangeSmall,
    vhcGenericHba1c,
    vhcGenericInHealthyRangeLarge,
    vhcIcnTipsSmall,
    vhcGenericQuestionMark,
    vhcGenericLearnMore,
    vhcHealthcareBig,
    vhcGenericMask,
    vhcGenericPointsSmall,
    arOnboardingActivated,
  ]
  // swiftlint:enable trailing_comma
  @available(*, deprecated, renamed: "allImages")
  public static let allValues: [VIAHealthCheckAssetType] = allImages
}
// swiftlint:enable identifier_name line_length nesting type_body_length type_name

extension VIAHealthCheckImage {
@available(iOS 1.0, tvOS 1.0, watchOS 1.0, *)
@available(OSX, deprecated,
message: "This initializer is unsafe on macOS, please use the VIAHealthCheckImageAsset.image property")
convenience init!(asset: VIAHealthCheckAssetType) {
#if os(iOS) || os(tvOS)
let bundle = Bundle(for: BundleToken.self)
self.init(named: asset.name, in: bundle, compatibleWith: nil)
#elseif os(OSX) || os(watchOS)
self.init(named: asset.name)
#endif
}
}

extension VIAHealthCheckColor {
#if swift(>=3.2)
@available(iOS 11.0, tvOS 11.0, watchOS 4.0, OSX 10.13, *)
convenience init!(asset: VIAHealthCheckColorAsset) {
let bundle = Bundle(for: BundleToken.self)
#if os(iOS) || os(tvOS)
self.init(named: asset.name, in: bundle, compatibleWith: nil)
#elseif os(OSX)
self.init(named: asset.name, bundle: bundle)
#elseif os(watchOS)
self.init(named: asset.name)
#endif
}
#endif
}

private final class BundleToken {}
