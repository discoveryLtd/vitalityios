//
//  VHCViewHistoryViewModel.swift
//  VIAHealthCheck
//
//  Created by Dexter Anthony Ambrad on 1/17/19.
//  Copyright © 2019 Glucode. All rights reserved.
//

import VitalityKit
import VIAUIKit
import VIAUtilities
import RealmSwift

public struct VHCHistoryItem {
    public var name: String
    public var value: String
    public var date: String
    public let headerTitle: String
    public let groupTypeRef: EventTypeRef
}

public class VHCViewHistoryViewModel: AnyObject {
    
    // MARK: Properties
    let dataController = VHCViewHistoryDataController()
    var cellsGroupData = [VHCViewHistoryCellData]()
    
    internal var vhcHistoryDataGroups: Array<Array<VHCHistoryItem>> = Array<Array<VHCHistoryItem>>()
    var vhcViewHistoryRealm = DataProvider.newVHCRealm()
    
    // MARK: Functions
    
    func getCellDataFromDataController() {
        cellsGroupData.removeAll()
        cellsGroupData = dataController.dataForGroupsFromRealm()
    }
    
    func fetchHistoryAttributes(forceUpdate: Bool = false, completion: @escaping (Error?, Bool?) -> Void) {
        if VHCCache.isVHCDataOutdated() || forceUpdate == true {
            let partyId = DataProvider.newRealm().getPartyId()
            let tenantId = DataProvider.newRealm().getTenantId()
            
            var eventTypes = EventTypeRef.configuredTypesForVHC()
            eventTypes.append(.DocumentUploadsVHC)
            
            guard let effectiveFromDate = DataProvider.currentMembershipPeriodStartDate() else {
                return
            }
            
            guard let effectiveToDate = DataProvider.currentMembershipPeriodEndDate() else {
                return
            }
            
            let parameter = GetVHCEventByPartyParameter(partyId: partyId, effectiveFrom: effectiveFromDate, effectiveTo: effectiveToDate, eventTypesTypeKeys: eventTypes)
            
            Wire.Events.getEventByParty(tenantId: tenantId, request: parameter,
                                        completion: { error, response in
                                            if let error = error {
                                                completion(error, nil)
                                            } else {
                                                self.getCellDataFromDataController()
                                                completion(nil, nil)
                                            }
            })
            
        } else {
            getCellDataFromDataController()
            completion(nil, nil)
        }
    }
    
    func getVHCHistoryItem(associatedEvents: List<VHCAssociatedEvents>, eventUploadID: Int) {
        var sectionItem = [VHCHistoryItem]()
        
        if self.getEventID(typeRef: .WeightCaptured, associatedEvents: associatedEvents) != nil {
            if let item = self.getEventItemInAllEvents(eventId: eventUploadID, typeRef: .BMI) {
                sectionItem.append(item)
            }
        }
        
        if sectionItem.isEmpty {
            if self.getEventID(typeRef: .HeightCaptured, associatedEvents: associatedEvents) != nil {
                if let item = self.getEventItemInAllEvents(eventId: eventUploadID, typeRef: .BMI) {
                    sectionItem.append(item)
                }
            }
        }
        
        if let heightEventId = self.getEventID(typeRef: .HeightCaptured, associatedEvents: associatedEvents){
            if let item = self.getEventItemInAssociatedEvents(eventId: heightEventId, typeRef: .HeightCaptured, associatedEvents: associatedEvents){
                sectionItem.append(item)
            }
        }
        
        if let weightEventId = self.getEventID(typeRef: .WeightCaptured, associatedEvents: associatedEvents){
            if let item = self.getEventItemInAssociatedEvents(eventId: weightEventId, typeRef: .WeightCaptured, associatedEvents: associatedEvents){
                sectionItem.append(item)
            }
        }
        
        if sectionItem.count == 3 {
            vhcHistoryDataGroups.append(sectionItem)
        }
        
        if let waistCircumID = self.getEventID(typeRef: .WaistCircum, associatedEvents: associatedEvents){
            if let item = self.getEventItemInAssociatedEvents(eventId: waistCircumID, typeRef: .WaistCircum, associatedEvents: associatedEvents){
                sectionItem = [VHCHistoryItem]()
                sectionItem.append(item)
                
                vhcHistoryDataGroups.append(sectionItem)
            }
        }
        
        if let systolicPressureID = self.getEventID(typeRef: .SystolicPressure, associatedEvents: associatedEvents) {
            if var item = self.getEventItemInAllEvents(eventId: eventUploadID, typeRef: .BloodPressure), let systolicItem = self.getEventItemInAssociatedEvents(eventId: systolicPressureID, typeRef: .SystolicPressure, associatedEvents: associatedEvents) {
                
                item.date = systolicItem.date
                sectionItem = [VHCHistoryItem]()
                sectionItem.append(item)
                
                vhcHistoryDataGroups.append(sectionItem)
            }
        }
        
        sectionItem = [VHCHistoryItem]()
        if let randomGlucoseID = self.getEventID(typeRef: .RandomGlucose, associatedEvents: associatedEvents){
            if let item = self.getEventItemInAssociatedEvents(eventId: randomGlucoseID, typeRef: .RandomGlucose, associatedEvents: associatedEvents){
                sectionItem.append(item)
            }
        }
        
        if let fastingGlucoseID = self.getEventID(typeRef: .FastingGlucose, associatedEvents: associatedEvents) {
            if let item = self.getEventItemInAssociatedEvents(eventId: fastingGlucoseID, typeRef: .FastingGlucose, associatedEvents: associatedEvents) {
                sectionItem = [VHCHistoryItem]()
                sectionItem.append(item)
            }
        }
        if sectionItem.count > 0 {
            vhcHistoryDataGroups.append(sectionItem)
        }
        
        if let hba1cId = self.getEventID(typeRef: .HbA1c, associatedEvents: associatedEvents){
            if let item = self.getEventItemInAssociatedEvents(eventId: hba1cId, typeRef: .HbA1c, associatedEvents: associatedEvents){
                sectionItem = [VHCHistoryItem]()
                sectionItem.append(item)
                
                vhcHistoryDataGroups.append(sectionItem)
            }
        }
        
        if let urinaryTestID = self.getEventID(typeRef: .UrinaryTest, associatedEvents: associatedEvents) {
            if let item = self.getEventItemInAssociatedEvents(eventId: urinaryTestID, typeRef: .UrinaryTest, associatedEvents: associatedEvents) {
                sectionItem = [VHCHistoryItem]()
                sectionItem.append(item)
                
                vhcHistoryDataGroups.append(sectionItem)
            }
        }
        
        sectionItem = [VHCHistoryItem]()
        if let totCholesterolID = self.getEventID(typeRef: .TotCholesterol, associatedEvents: associatedEvents){
            if let item = self.getEventItemInAssociatedEvents(eventId: totCholesterolID, typeRef: .TotCholesterol, associatedEvents: associatedEvents){
                sectionItem = [VHCHistoryItem]()
                sectionItem.append(item)
            }
        }
        
        if let hdlCholestrolID = self.getEventID(typeRef: .HDLCholesterol, associatedEvents: associatedEvents) {
            if let item = self.getEventItemInAssociatedEvents(eventId: hdlCholestrolID, typeRef: .HDLCholesterol, associatedEvents: associatedEvents) {
                sectionItem = [VHCHistoryItem]()
                sectionItem.append(item)
            }
        }
        
        if let ldlCholestrolID = self.getEventID(typeRef: .LDLCholestrol, associatedEvents: associatedEvents) {
            if let item = self.getEventItemInAssociatedEvents(eventId: ldlCholestrolID, typeRef: .LDLCholestrol, associatedEvents: associatedEvents) {
                sectionItem = [VHCHistoryItem]()
                sectionItem.append(item)
            }
        }
        
        if let triGlycCholestrolID = self.getEventID(typeRef: .TriGlycCholesterol, associatedEvents: associatedEvents) {
            if let item = self.getEventItemInAssociatedEvents(eventId: triGlycCholestrolID, typeRef: .TriGlycCholesterol, associatedEvents: associatedEvents) {
                sectionItem = [VHCHistoryItem]()
                sectionItem.append(item)
            }
        }
        if sectionItem.count > 0 {
            vhcHistoryDataGroups.append(sectionItem)
        }
        
        //dump(vhcHistoryDataGroups)
    }
    
    public func dumpVHCHistoryDataGroups() {
        dump(vhcHistoryDataGroups)
    }
    
    func getEventGroupName(eventTypeRef: EventTypeRef) -> String{
        switch eventTypeRef {
        case .RandomGlucose, .FastingGlucose:
            return CommonStrings.Measurement.GlucoseTitle136
        case .TotCholesterol, .HDLCholesterol, .LDLCholestrol, .TriGlycCholesterol:
            return CommonStrings.Measurement.CholesterolRatioTitle1173
        default:
            return ""
        }
        
    }
    public func nameForItem(in group: Int) -> String? {
        if self.vhcHistoryDataGroups.count > group {
            if let historyItem = self.vhcHistoryDataGroups[group].first{
                return self.getEventGroupName(eventTypeRef: historyItem.groupTypeRef).uppercased()
            }
        } else if (self.vhcHistoryDataGroups.count == group) {
            return CommonStrings.SummaryScreen.UploadedProofTitle186.uppercased()
        }
        return  nil
    }
    
    func getConvertedDate(dateString: String) -> String?{
        if let dateFormat = dataController.stringToDate(stringDate: dateString){
        let toHumanReadableFormat = dataController.dateToString(date: dateFormat)
        
        return toHumanReadableFormat
        }
        return nil
    }
    
    func getEventID (typeRef: EventTypeRef, associatedEvents: List<VHCAssociatedEvents> ) -> Int? {
        if let eventID = associatedEvents.filter({$0.eventTypeKey == typeRef.rawValue}).first?.eventId{
            
            return eventID
        }
        return nil
    }
    
    func getEventItemInAssociatedEvents(eventId: Int, typeRef: EventTypeRef, associatedEvents: List<VHCAssociatedEvents> ) -> VHCHistoryItem?{
        let events = associatedEvents.filter({ $0.eventTypeKey == typeRef.rawValue })
        var groupTypeRef = typeRef
        if typeRef == .WeightCaptured || typeRef == .HeightCaptured {
            groupTypeRef = .BMI
        }
        
        for event in events {
            if event.eventTypeKey == typeRef.rawValue {
                if let cDate = self.getConvertedDate(dateString:event.eventDateTime ?? ""), let submittedValue = self.getSubmissionValue(eventId: eventId) {
                    let item = VHCHistoryItem(name: event.categoryName ?? "", value: submittedValue, date: CommonStrings.SummaryScreen.DateTestedTitle185(cDate), headerTitle: self.getEventName(eventTypeRef: EventTypeRef(rawValue: event.eventTypeKey)!), groupTypeRef: groupTypeRef)
                    return item
                }
            }
        }
        return nil
    }
    
    func getEventItemInAllEvents(eventId: Int, typeRef: EventTypeRef) -> VHCHistoryItem?{
        let vhcEvents = vhcViewHistoryRealm.allVHCHistoryEvents()
        let events = vhcEvents.filter({ ($0 as VHCHistoryEvent).typeKey == typeRef.rawValue })
        
        for event in events {
            for associatedEvent in event.associatedEvents{
                if associatedEvent.eventId == eventId, let eventDate = event.eventDateTime {
                    if let eDate = typeRef == .BMI ? dataController.convertAPIDateToString(apiDate: eventDate) : associatedEvent.eventDateTime, let eventTypeRef = EventTypeRef(rawValue: event.typeKey){
                        if let cDate = self.getConvertedDate(dateString: eDate), let submittedValue = self.getSubmissionValue(eventId: event.id) {
                            let item = VHCHistoryItem(name: event.categoryName ?? "", value: submittedValue, date: CommonStrings.SummaryScreen.DateTestedTitle185(cDate), headerTitle: self.getEventName(eventTypeRef: eventTypeRef), groupTypeRef: typeRef)
                            return item
                        }
                    }
                }
            }
        }
        return nil
    }
    
    public func numberOfCapturedItemsGroups() -> Int {
        let capturedImagesGroupCount = 1
        
        return vhcHistoryDataGroups.count + capturedImagesGroupCount
    }
    
    public func sectionIsLast(section: Int) -> Bool {
        
        return section == (self.numberOfCapturedItemsGroups() - 1)
    }
    
    public func itemCount(in group: Int) -> Int {
        if self.sectionIsLast(section: group) {
            let capturedImagesCellCount = 1
            return capturedImagesCellCount
        }
        
        return vhcHistoryDataGroups[group].count
    }
    
    func getEventName (eventTypeRef: EventTypeRef) -> String{
        switch eventTypeRef {
        case .RandomGlucose:
            return CommonStrings.Measurement.RandomGlucoseTitle147
        case .TotCholesterol:
            return CommonStrings.Measurement.TotalCholesterolTitle149
        case .WaistCircum:
            return CommonStrings.Measurement.WaistCircumferenceTitle135
        case .BloodPressure:
            return CommonStrings.Measurement.BloodPressureTitle137
        case .HbA1c:
            return CommonStrings.Measurement.Hba1cTitle139
        case .FastingGlucose:
            return CommonStrings.Measurement.FastingGlucoseTitle148
        case .UrinaryTest:
            return CommonStrings.Measurement.UrineProteinTitle283
        case .LDLCholestrol:
            return CommonStrings.Measurement.LdlTitle151
        case .BMI:
            return CommonStrings.Measurement.BodyMassIndexTitle134
        case .HeightCaptured:
            return CommonStrings.Measurement.HeightTitle145
        case .WeightCaptured:
            return CommonStrings.Measurement.WeightTitle146
        default:
            return ""
        }
        
    }
    func getSubmissionUnit (eventId : Int) -> String {
        let vhcEvents = vhcViewHistoryRealm.allVHCHistoryEvents()
        
        if let unitOfMeasure = vhcEvents.filter(({ ($0 as VHCHistoryEvent).id == eventId })).first?.eventMetaDataOuts.first?.unitOfMeasure {
            if UnitOfMeasureRef(rawValue: Int(unitOfMeasure) ?? 0) == .FootInch || UnitOfMeasureRef(rawValue: Int(unitOfMeasure) ?? 0) == .StonePound {
                return ""
            } else {
                if let rawValue = Int(unitOfMeasure){
                    let unit = UnitOfMeasureRef(rawValue: rawValue)?.unit().symbol
                    return unit ?? ""
                }
            }
        }
        
        return ""
    }
    func getSubmissionValue (eventId : Int) -> String? {
        let vhcEvents = vhcViewHistoryRealm.allVHCHistoryEvents()
        let value = vhcEvents.filter(({ ($0 as VHCHistoryEvent).id == eventId })).first?.eventMetaDataOuts.first?.value ?? ""
        let unit = self.getSubmissionUnit(eventId: eventId)
        
        if value != "" {
            return ("\(value) \(unit)")
        }
        
        return nil
    }
}

extension VHCViewHistoryViewModel: CMSConsumer {
    
    func getUploadedProof(refId: Int, completion: @escaping ((_: UIImage) -> Void)){
        
        self.downloadHistoryImage(referenceId: refId) { url, error in
            let defaultLogo = VIAHealthCheckAsset.vhcCompletedBig.image
            guard url != nil, error == nil, let filePath = url?.path, FileManager.default.fileExists(atPath: filePath) else {
                debugPrint("Could not retrieve downloaded partner image from disk")
                return completion(defaultLogo)
            }
            
            if let img = UIImage(contentsOfFile: filePath) {
                debugPrint("Partner image retrieved successfully from service")
                return completion(img)
            }
            
            debugPrint("Falied to download partner image")
            return completion(defaultLogo)
        }
        
    }
}
