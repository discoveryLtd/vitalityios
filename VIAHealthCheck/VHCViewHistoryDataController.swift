//
//  VHCViewHistoryDataController.swift
//  VIAHealthCheck
//
//  Created by Dexter Anthony Ambrad on 1/17/19.
//  Copyright © 2019 Glucode. All rights reserved.
//

import RealmSwift

import VitalityKit
import VIAUIKit
import VIAUtilities

public struct VHCViewHistoryCellData {
    var vhcHistoryEventList: [VHCHistoryEvent]
    var historyEventModel: [VHCHistoryEvent]
    var eventDate: String
}

public class VHCViewHistoryDataController {
    
    var vhcViewHistoryRealm = DataProvider.newVHCRealm()
    
    class func sortedGroups(from realm: Realm) -> Array<VHCHealthAttributeGroup> {
        let results = Array(realm.allVHCHealthAttributeGroups())
        return results.sorted { ($0 as VHCHealthAttributeGroup).groupName() < ($1 as VHCHealthAttributeGroup).groupName() }
    }
    
    public func dataForGroupsFromRealm() -> [VHCViewHistoryCellData] {
        vhcViewHistoryRealm.refresh()
        
        var allGroupsData = [VHCViewHistoryCellData]()
        let vhcEvents = vhcViewHistoryRealm.allVHCHistoryEvents()
        
        /* Sort Events in Descending Order */
        let sortedVHCEvents = vhcEvents.sorted{ ($0 as VHCHistoryEvent).eventDateTime! > ($1 as VHCHistoryEvent).eventDateTime! }
        let vhcSortedSubmissions = sortedVHCEvents.filter({ ($0 as VHCHistoryEvent).typeKey == EventTypeRef.DocumentUploadsVHC.rawValue })
        
        for event in vhcSortedSubmissions {
            
            if let date = event.eventDateTime {
                
                var vhcHistoryEventList = [VHCHistoryEvent]()
                vhcHistoryEventList.append(contentsOf: vhcEvents)
                var historyEventModel = [VHCHistoryEvent]()
                historyEventModel.append(event)
                let convertedEventDate = convertAPIDateToString(apiDate: date)
                
                let dateFormat = stringToDate(stringDate: convertedEventDate)
                if let dFormat = dateFormat {
                    let toHumanReadableFormat = dateToString(date: dFormat)
                    let isEventDateExist = allGroupsData.contains(where: { (data) -> Bool in
                        return ((data.eventDate) == toHumanReadableFormat)
                    })
                    
                    if isEventDateExist {
                        if var groupData = allGroupsData.filter( { ($0 as VHCViewHistoryCellData).eventDate == toHumanReadableFormat}).first {
                            allGroupsData.removeAll(where: { (data) -> Bool in
                                return ((data.eventDate) == toHumanReadableFormat)
                            })
                            groupData.historyEventModel.append(event)
                            allGroupsData.append(groupData)
                        }
                    }
                    else {
                        let cellData = VHCViewHistoryCellData(vhcHistoryEventList: vhcHistoryEventList, historyEventModel: historyEventModel, eventDate: toHumanReadableFormat)
                        allGroupsData.append(cellData)
                    }
                }
            }
        }
        return allGroupsData
    }
    
    func convertAPIDateToString(apiDate: Date) -> String {
        let dateFormatter = DateFormatter.apiManagerFormatterWithMilliseconds()
        let date = dateFormatter.string(from: apiDate)
        
        if String.isNilOrEmpty(string: date) {
            let secondsDateFormatter = DateFormatter.apiManagerFormatter()
            
            return secondsDateFormatter.string(from: apiDate)
        }
        
        return date
    }
    
    func stringToDate(stringDate: String) -> Date? {
        let dateFormatter = DateFormatter.apiManagerFormatterWithMilliseconds()
        
        guard let date = dateFormatter.date(from: stringDate) else {
            let secondsDateFormatter = DateFormatter.apiManagerFormatter()
            
            return secondsDateFormatter.date(from: stringDate)
        }
        return date
    }
    
    func dateToString(date: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "E, dd MMM yyyy" //Your New Date format as per requirement change it own
        let stringDate = dateFormatter.string(from: date)
        return stringDate
    }
}
