//
//  VHCViewHistoryViewController.swift
//  VIAHealthCheck
//
//  Created by Dexter Anthony Ambrad on 1/15/19.
//  Copyright © 2019 Glucode. All rights reserved.
//

import VIAUIKit
import VitalityKit
import VIAUtilities
import VIACommon

public final class VHCViewHistoryViewController: VIATableViewController {
    
    let viewModel = VHCViewHistoryViewModel()
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = CommonStrings.HistoryButton140
        
        setupNavBar()
        configureAppearance()
        configureTableView()
        loadData()
    }
    
    func configureAppearance() {
        VIAAppearance.setGlobalTintColorToKnowYourHealth()
        navigationController?.makeNavigationBarTransparent()
    }
    
    func setupNavBar() {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        
        self.hideBackButtonTitle()
    }
    
    func configureTableView() {
        self.tableView.register(VIALabelTableViewCell.nib(), forCellReuseIdentifier: VIALabelTableViewCell.defaultReuseIdentifier)
        self.tableView.register(VIATableViewSectionHeaderFooterView.nib(), forHeaderFooterViewReuseIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier)
        self.tableView.estimatedRowHeight = 75
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.separatorInset = UIEdgeInsets(top: 0, left: 52, bottom: 0, right: 0)
        self.tableView.tableFooterView = UIView()
    }
    
    func performRefresh() {
        loadData(forceUpdate: true)
        self.tableView.refreshControl?.endRefreshing()
    }
    
    func loadData(forceUpdate: Bool = false) {
        showHUDOnView(view: self.view)
        viewModel.fetchHistoryAttributes(forceUpdate: forceUpdate) { [weak self] error, isCacheOutdated in
            self?.hideHUDFromView(view: self?.view)
            self?.tableView.refreshControl?.endRefreshing()
            
            guard error == nil else {
                self?.handleErrorOccurred(error!, isCacheOutdated: isCacheOutdated)
                return
            }
            
            self?.removeStatusView()
            self?.tableView.reloadData()
            
            if (self?.viewModel.cellsGroupData.isEmpty) ?? true{
                // Replace background view
                if let width = self?.tableView.frame.width, let height = self?.tableView.frame.height {
                    let view = VIATableViewHeaderView.viewFromNib(owner: self)!
                    view.header = CommonStrings.History.EmptyStateTitle250
                    view.content = CommonStrings.History.EmptyStateMessage251
                    view.layer.frame = CGRect(x: 0, y: 0, width: view.bounds.size.width, height: view.bounds.size.height)
                    view.center = CGPoint(x: (width) / 2, y: (height) / 2.5)
                    view.backgroundColor = UIColor.tableViewBackground()
                    view.tag = 1
                    self?.view.addSubview(view)
                }
            }
        }
    }
    
    func handleErrorOccurred(_ error: Error, isCacheOutdated: Bool?) {
        let backendError = error as? BackendError ?? BackendError.other
        if let outdated = isCacheOutdated, outdated == true {
            let statusView = self.handleBackendErrorWithStatusView(backendError, tryAgainAction: { [weak self] in
                self?.loadData(forceUpdate: true)
            })
            configureStatusView(statusView)
        } else {
            self.handleBackendErrorWithAlert(backendError, tryAgainAction: { [weak self] in
                self?.loadData(forceUpdate: true)
            })
        }
    }
    
    override public func configureStatusView(_ view: UIView?) {
        removeStatusView()
        
        guard let view = view else { return }
        
        self.view.addSubview(view)
        
        view.snp.makeConstraints { (make) in
            make.width.equalToSuperview()
            make.left.right.top.equalToSuperview()
            make.height.equalToSuperview().offset(self.tableView.contentOffset.y)
        }
    }
    
    override public func removeStatusView() {
        self.view.subviews.forEach { (view) in
            if view is VIAStatusView {
                view.removeFromSuperview()
            }
        }
    }
    
    /*
     *   Tableview Delegates and DataSource.
     */
    
    public override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    public override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0 {
            if !viewModel.cellsGroupData.isEmpty {
                return viewModel.cellsGroupData.count
            }
            return 0
        }
        
        return 0
    }
    
    public override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell: UITableViewCell?
        
        if indexPath.section == 0, !viewModel.cellsGroupData.isEmpty {
            cell = self.tableView.dequeueReusableCell(withIdentifier: VIALabelTableViewCell.defaultReuseIdentifier)
            let cellInformation = cell as! VIALabelTableViewCell
            let items = self.viewModel.cellsGroupData[indexPath.row]
            
            cellInformation.labelText = items.eventDate
            cellInformation.accessoryType = .disclosureIndicator
        }
        
        guard let tableViewCell = cell else {
            return self.tableView.defaultTableViewCell()
        }
        
        return tableViewCell
    }
    
    override public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        detailsClick(self)
    }
    
    override public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if !viewModel.cellsGroupData.isEmpty {
            let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier)
            let footer = view as! VIATableViewSectionHeaderFooterView
            footer.labelText = "\n\(CommonStrings.History.SubmissionSectionTitle203.uppercased())"
            return view
        }
        
        return nil
    }
    
    override public func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    override public func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
    
    // MARK: - Navigation
    
    public override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showVHCHistoryDetails",
            let nextScene = segue.destination as? VHCViewHistoryDetailsViewController,
            let indexPath = self.tableView.indexPathForSelectedRow {
            
            let selectedGroup = self.viewModel.cellsGroupData[indexPath.row]
            nextScene.data = selectedGroup.historyEventModel
            nextScene.vhcHistoryEventList = selectedGroup.vhcHistoryEventList
            nextScene.eventDate = selectedGroup.eventDate
        }
    }
    
    @IBAction func detailsClick(_ sender: Any) {
        self.performSegue(withIdentifier: "showVHCHistoryDetails", sender: self)
    }
    
}
