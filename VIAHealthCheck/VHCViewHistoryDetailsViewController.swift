//
//  VHCHistoryDetailViewController.swift
//  VIAHealthCheck
//
//  Created by Dexter Anthony Ambrad on 1/15/19.
//  Copyright © 2019 Glucode. All rights reserved.
//

import VIAUIKit
import VIACommon
import VIAUtilities
import VitalityKit

class VHCViewHistoryDetailsViewController: VIATableViewController {
    
    let viewModel = VHCViewHistoryViewModel()
    
    internal var heightForCell: CGFloat = 0.0
    
    /* Image & PDF Container Variable */
    var pdfFile = UIImage()
    var logoImage: [UIImage] = []
    
    /* Data */
    var data: [VHCHistoryEvent] = []
    var vhcHistoryEventList = [VHCHistoryEvent]()
    var eventDate: String?
    var vhcCapturedDataGroups: Array<Array<VHCCapturedResult>> = Array<Array<VHCCapturedResult>>()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadDetails()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = eventDate
        self.navigationController?.navigationBar.backgroundImage(for: .default)
        
        setupNavBar()
        configureAppearance()
        self.setupTableView()
        
    }
    
    func loadDetails() {
        /* Check if Data is Empty of Not */
        if data.count > 0 {
            
            /* Check if AssociatedEvents is Empty or Not and Unwrapped its Contents */
            for aData in data {
                debugPrint("Associated Event: \(aData.associatedEvents)")
                self.viewModel.getVHCHistoryItem(associatedEvents: aData.associatedEvents, eventUploadID: aData.id)
            }
            self.viewModel.dumpVHCHistoryDataGroups()
            getImages()
        }
    }
    
    func getImages() {
        
        showFullScreenHUD()
        /* Check if Data is Empty of Not */
        for aVHCHistory in data {
            /* Check if EventMetaDataOuts is Empty or Not and Unwrapped its Contents */
            let eventMetaDataOuts = aVHCHistory.eventMetaDataOuts
            for data in eventMetaDataOuts {
                
                if let referenceId = Int(data.value ?? "") {
                    viewModel.getUploadedProof(refId: referenceId) { img in
                        self.hideFullScreenHUD()
                        self.pdfFile = img
                        self.logoImage.append(self.pdfFile)
                        self.tableView.reloadData()
                    }
                }
            }
        }
    }
    
    func setupTableView() {
        self.registerReusableView()
        self.tableView.estimatedRowHeight = 44
        self.tableView.rowHeight = UITableViewAutomaticDimension
    }
    
    func configureAppearance() {
        VIAAppearance.setGlobalTintColorToKnowYourHealth()
        navigationController?.makeNavigationBarTransparent()
        navigationController?.navigationBar.barTintColor = UIColor.white
    }
    
    func setupNavBar() {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        
        self.hideBackButtonTitle()
    }
    
    
    func registerReusableView() {
        self.tableView.register(HeaderValueSubtitleTableViewCell.nib(), forCellReuseIdentifier: HeaderValueSubtitleTableViewCell.defaultReuseIdentifier)
        self.tableView.register(ImageCollectionTableViewCell.nib(), forCellReuseIdentifier: ImageCollectionTableViewCell.defaultReuseIdentifier)
        self.tableView.register(VIATableViewSectionHeaderFooterView.nib(), forHeaderFooterViewReuseIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier)
    }
    
    public override func numberOfSections(in tableView: UITableView) -> Int {
        return self.viewModel.numberOfCapturedItemsGroups()
        
    }
    
    public override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.itemCount(in: section)
    }
    
    public override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if (!(self.viewModel.sectionIsLast(section: indexPath.section) )) {
            if let resultsCell = tableView.dequeueReusableCell(withIdentifier: HeaderValueSubtitleTableViewCell.defaultReuseIdentifier) {
                let collectionViewCell = self.setUpCapturedResults(cell: resultsCell, at: indexPath)
                return collectionViewCell
            }
        } else {
            if let imageCollectionCell = self.tableView.dequeueReusableCell(withIdentifier: ImageCollectionTableViewCell.defaultReuseIdentifier) {
                let collectionviewCell = self.uploadedProofsItemCell(cell: imageCollectionCell)
                self.heightForImageCollection(cell: collectionviewCell)
                return collectionviewCell
            }
        }
        
        return UITableViewCell()
    }
    
    func setUpCapturedResults(cell: UITableViewCell, at index: IndexPath) -> UITableViewCell {
        if let capturedResultsCell = cell as? HeaderValueSubtitleTableViewCell {
            if self.viewModel.vhcHistoryDataGroups.count > 0 {
                capturedResultsCell.selectionStyle = .none
                let item = self.viewModel.vhcHistoryDataGroups[index.section][index.row]
                capturedResultsCell.setCell(title: item.headerTitle)
                capturedResultsCell.setCell(subTitle: item.date)
                capturedResultsCell.setCell(value: item.value)
                return capturedResultsCell
            }
            
            
        }
        return UITableViewCell()
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if let header = self.tableView.dequeueReusableHeaderFooterView(withIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier) as? VIATableViewSectionHeaderFooterView {
            self.setup(header: header, for: section)
            return header
        }
        return nil
    }
    
    func setup(header: VIATableViewSectionHeaderFooterView, for group: Int) {
        header.labelText = ""
        if let headerTitle = self.viewModel.nameForItem(in: group) {
            header.labelText = headerTitle
        }
    }
    
    override public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: false)
    }
    
    public override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if ((self.viewModel.sectionIsLast(section: indexPath.section) )) {
            return self.heightForCell
        }
        return UITableViewAutomaticDimension
    }
    
    func uploadedProofsItemCell(cell: UITableViewCell) -> ImageCollectionTableViewCell {
        if let imageCollectionViewCell = cell as? ImageCollectionTableViewCell {
            imageCollectionViewCell.setImageCollection(images: self.logoImage)
            imageCollectionViewCell.imageCollectionView?.bounces = false
            imageCollectionViewCell.viewWidth = self.view.frame.width
            imageCollectionViewCell.setupCollectionFlowLayoutSize()
            return imageCollectionViewCell
        }
        return ImageCollectionTableViewCell()
    }
    
    func heightForImageCollection(cell: ImageCollectionTableViewCell) {
        let collectionViewFlowLayout = cell.imageCollectionView?.collectionViewLayout
        let cellPadding = collectionViewFlowLayout?.collectionViewContentSize
        let height = cellPadding?.height
        self.heightForCell = height ?? 0
        cell.updateImageCollectionViewSize(with:height)
    }
    
    func stringToDate(stringDate: String) -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-mm-dd" //Your date format
        let date = dateFormatter.date(from: stringDate) //according to date format your date string
        return date
    }
    
    func dateToString(date: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "E, dd MMM yyyy" //Your New Date format as per requirement change it own
        let stringDate = dateFormatter.string(from: date) //pass Date here
        return stringDate
    }
}
