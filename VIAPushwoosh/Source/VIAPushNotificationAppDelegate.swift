//
//  VIAPushwooshAppDelegate.swift
//  VitalityActive
//
//  Created by OJ Garde on 5/31/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import Pushwoosh
import VitalityKit

public class VIAPushNotificationAppDelegate: PushNotificationDelegate{
    
    public init(){
        NotificationCenter.default.post(name: .VIAPushNotificationSetTagsForPushNotification, object: nil)
    }
    
    public func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]? = nil, delegate: UIApplicationDelegate&PushNotificationDelegate){
        
        UserDefaults.standard.removeObject(forKey:"inAppMessages")
        UIApplication.shared.applicationIconBadgeNumber = 0
        
        NotificationCenter.default.addObserver(forName: .VIAPushNotificationSetTagsForPushNotification, object: nil, queue: nil) { (notification) in
            let uIDs = notification.userInfo
            let partyID = uIDs!["partyId"] as AnyObject
            let tenantID = uIDs!["tenantId"] as AnyObject
            let currentEnvironment = Wire.default.currentEnvironment.name().lowercased().replacingOccurrences(of: "--", with: "") as AnyObject

  
            let tags : [NSObject : AnyObject] = [NSString(string: "Party ID") : partyID ,
                                                 NSString(string: "Tenant ID") : tenantID ,
                                                 NSString(string: "Environment") : currentEnvironment as AnyObject]
            
            PushNotificationManager.push().setTags(tags)

        }
        
        registerForPushNotication(withDelegate: delegate)
    }
    
    private func registerForPushNotication(withDelegate delegate: UIApplicationDelegate&PushNotificationDelegate) {
        // set custom delegate for push handling, in our case AppDelegate
        PushNotificationManager.push().delegate = delegate
        
        // set default Pushwoosh delegate for iOS10 foreground push handling
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().delegate = PushNotificationManager.push().notificationCenterDelegate
        }
        
        // make sure we count app open in Pushwoosh stats
        PushNotificationManager.push().sendAppOpen()
        
        // register for push notifications!
        PushNotificationManager.push().registerForPushNotifications()
    }
 
}
