//
//  Notifications+PushNotifications.swift
//  VitalityActive
//
//  Created by OJ Garde on 5/31/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation

public extension NSNotification.Name {
    static let VIAPushNotificationSetTagsForPushNotification = Notification.Name("SetTagsForPushNotification")
}
