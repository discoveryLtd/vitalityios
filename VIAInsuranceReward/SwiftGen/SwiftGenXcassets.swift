// Generated using SwiftGen, by O.Halligon — https://github.com/SwiftGen/SwiftGen

#if os(OSX)
import AppKit.NSImage
public typealias VIAInsuranceRewardColor = NSColor
public typealias VIAInsuranceRewardImage = NSImage
#elseif os(iOS) || os(tvOS) || os(watchOS)
import UIKit.UIImage
public typealias VIAInsuranceRewardColor = UIColor
public typealias VIAInsuranceRewardImage = UIImage
#endif

// swiftlint:disable file_length

public typealias VIAInsuranceRewardAssetType = VIAInsuranceRewardImageAsset

public struct VIAInsuranceRewardImageAsset: Equatable {
	fileprivate var name: String

	public var templateImage: VIAInsuranceRewardImage {
		let bundle = Bundle(for: BundleToken.self)
		#if os(iOS) || os(tvOS)
		let image = VIAInsuranceRewardImage(named: name, in: bundle, compatibleWith: nil)?.withRenderingMode(.alwaysTemplate)
		#elseif os(OSX)
		let image = bundle.image(forResource: name).withRenderingMode(.alwaysTemplate)
		#elseif os(watchOS)
		let image = VIAInsuranceRewardImage(named: name).withRenderingMode(.alwaysTemplate)
		#endif
		guard let result = image else { fatalError("Unable to load template image named \(name).") }
		return result
	}

	public var image: VIAInsuranceRewardImage {
		let bundle = Bundle(for: BundleToken.self)
		#if os(iOS) || os(tvOS)
		let image = VIAInsuranceRewardImage(named: name, in: bundle, compatibleWith: nil)
		#elseif os(OSX)
		let image = bundle.image(forResource: name)
		#elseif os(watchOS)
		let image = VIAInsuranceRewardImage(named: name)
		#endif
		guard let result = image else { fatalError("Unable to load image named \(name).") }
		return result
	}

	public static func ==(lhs: VIAInsuranceRewardImageAsset, rhs: VIAInsuranceRewardImageAsset) -> Bool {
        return lhs.image == rhs.image
    }
}

public struct VIAInsuranceRewardColorAsset {
fileprivate var name: String

#if swift(>=3.2)
@available(iOS 11.0, tvOS 11.0, watchOS 4.0, OSX 10.13, *)
var color: VIAInsuranceRewardColor {
return VIAInsuranceRewardColor(asset: self)
}
#endif
}

// swiftlint:disable identifier_name line_length nesting type_body_length type_name
public enum VIAInsuranceRewardAsset {
  public static let image = VIAInsuranceRewardImageAsset(name: "Image")

  // swiftlint:disable trailing_comma
  public static let allColors: [VIAInsuranceRewardColorAsset] = [
  ]
  public static let allImages: [VIAInsuranceRewardImageAsset] = [
    image,
  ]
  // swiftlint:enable trailing_comma
  @available(*, deprecated, renamed: "allImages")
  public static let allValues: [VIAInsuranceRewardAssetType] = allImages
}
// swiftlint:enable identifier_name line_length nesting type_body_length type_name

extension VIAInsuranceRewardImage {
@available(iOS 1.0, tvOS 1.0, watchOS 1.0, *)
@available(OSX, deprecated,
message: "This initializer is unsafe on macOS, please use the VIAInsuranceRewardImageAsset.image property")
convenience init!(asset: VIAInsuranceRewardAssetType) {
#if os(iOS) || os(tvOS)
let bundle = Bundle(for: BundleToken.self)
self.init(named: asset.name, in: bundle, compatibleWith: nil)
#elseif os(OSX) || os(watchOS)
self.init(named: asset.name)
#endif
}
}

extension VIAInsuranceRewardColor {
#if swift(>=3.2)
@available(iOS 11.0, tvOS 11.0, watchOS 4.0, OSX 10.13, *)
convenience init!(asset: VIAInsuranceRewardColorAsset) {
let bundle = Bundle(for: BundleToken.self)
#if os(iOS) || os(tvOS)
self.init(named: asset.name, in: bundle, compatibleWith: nil)
#elseif os(OSX)
self.init(named: asset.name, bundle: bundle)
#elseif os(watchOS)
self.init(named: asset.name)
#endif
}
#endif
}

private final class BundleToken {}
