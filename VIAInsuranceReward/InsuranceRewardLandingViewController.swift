//
//  InsuranceRewardLandingViewController.swift
//  VitalityActive
//
//  Created by Erik John T. Alicaya (ADMIN) on 28/05/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import VitalityKit

class InsuranceRewardLandingViewController: UIViewController, UITextViewDelegate {
    
    @IBOutlet var titleL: UILabel!
    @IBOutlet var messageTV: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.title =  CommonStrings.HomeCard.Insurance.SectionTitle1963
        
        messageTV.delegate = self
        
        let attributedString = NSMutableAttributedString(string: CommonStrings.Ir.LandingDescription2254 + "\n" + CommonStrings.Ir.LandingLearnMoreButton2255 + "...")
        
        let fontAttribute = [NSFontAttributeName: UIFont.systemFont(ofSize: 18.0)]
        
        attributedString.addAttributes(fontAttribute, range: NSRange(location: 0, length: attributedString.length))
        
        attributedString.addAttribute(NSLinkAttributeName, value: "https://www.vitalitygroup.com", range: NSRange(location: 348, length: 14))
        
        messageTV.isEditable = false
        messageTV.isScrollEnabled = false
        messageTV.font = UIFont(name: "Arial-Bold", size: 100)
        messageTV.attributedText = attributedString
        
        titleL.text = CommonStrings.Ir.LandingTitle12257
    }
    
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        UIApplication.shared.open(URL, options: [:])
        
        return false
    }
}

