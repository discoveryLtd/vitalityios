//
//  VNAAssessmentLandingViewModel.swift
//  VitalityActive
//
//  Created by Von Kervin R. Tuico on 09/02/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import VitalityKit
import RealmSwift

public struct VNAAssessmentQuestionnaireCellData {
    var title: String
    var buttonText: String
    var timeToComplete: String
    var completed: Bool
    var completeIcon: UIImage?
    var typeKey: Int
}

public struct VNAAssessmentHeaderCellData {
    var title: String
    var description: String
    var totalSections: Int
    var completeSections: Int
    var completedImage: UIImage?
}

public protocol VNAAssessmentLandingViewModel: class {
    
    var vnaRealm: Realm { get }
    var vnaQuestionnaireData: [VNAAssessmentQuestionnaireCellData] { get }
    var vnaHeaderData: VNAAssessmentHeaderCellData? { get }
    var integerFormatter: NumberFormatter { get }
    var questionnairesFooterText: String? { get }
    
    // MARK: Functions
    
    func shouldShowOnboarding() -> Bool
    
    func configureQuestionnaires()
    
    func configureHeader()
    
    func getTotalNumberOfQuestionnaires() -> Int
    
    func getHeaderDescriptionSubtext() -> String?
    
    func getTotalNumberOfCompletedQuestionnaires() -> Int
    
    func getQuestionnairesTotalPotentialPoints() -> Int
    
    func getQuestionnairesTotalEarnedPoints() -> Int
    
    func getQuestionnaireAt(index: Int) -> VNAQuestionnaire?
    
    func getVNAQuestionnaire(using selectedQuestionnaireTypeKey: Int) -> VNAQuestionnaire?
    
    func fetchQuestionnaires(forceUpdate: Bool, completion: @escaping (Error?, Bool?) -> Void)
    
}

