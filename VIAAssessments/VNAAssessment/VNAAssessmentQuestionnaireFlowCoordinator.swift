import Foundation
import VitalityKit
import VIAUIKit
import RealmSwift
import VIACommon
import VIAUtilities

protocol VNAAssessmentSubmitter: class {
    func submit(for viewController: VIAViewController?, with completion:  @escaping (Error?) -> ())
}

class VNAAssessmentQuestionnaireFlowCoordinator {
    
    // MARK: - Properties
    
    var navigationController: VNAAssessmentNavigationController!
    
    var selectedQuestionnaire: VNAQuestionnaire?
    
    var potentialPoints: Int
    
    var currentSectionIndex = 0
    
    var realm = DataProvider.newVNARealm()
    
    var shouldReloadWhenLandingViewIsPresented: Bool = false
    
    var selectedAssessment: String?
    
    var completionFlag: Bool?
    
    var totalVisibleSections: Int {
        var count = 0
        guard let questionnaire = self.selectedQuestionnaire else { return count }
        for section in questionnaire.questionnaireSections {
            if section.isVisible {
                count = count + 1
            }
        }
        return count
    }
    
    var currentSectionTitle: String {
        return self.selectedQuestionnaire?.typeName ?? ""
    }
    
    var currentQuestionnaireSection: VNAQuestionnaireSection? {
        return selectedQuestionnaire?.sortedVisibleQuestionnaireSections()[currentSectionIndex]
    }
    
    // MARK: - Init
    
    deinit {
        debugPrint("VNAQuestionnaireFlowCoordinator deinit")
    }
    
    init(with navigationController: VNAAssessmentNavigationController, selectedQuestionnaire: VNAQuestionnaire?, potentialPoints: Int) {
        self.selectedQuestionnaire = selectedQuestionnaire
        self.navigationController = navigationController
        self.potentialPoints = potentialPoints
        setCurrentSectionIndex()
        configureNavigationBar()
    }
    
    // MARK: - Configuration
    
    func start() {
        configureTopViewController()
    }
    
    func configureTopViewController() {
        if let rootViewController = self.navigationController?.topViewController as? VNAAssessmentQuestionnaireSectionViewController {
            configureQuestionnaireSectionViewController(rootViewController)
            configureNavigationStack(topViewController: rootViewController)
        }
    }
    
    func configureVNADataSharingConsentViewControllerDelegate() {
        if let rootViewController = self.navigationController?.topViewController as? VNAAssessmentDataSharingConsentViewController {
            rootViewController.navigationDelegate = self
            rootViewController.submitter = self
        }
    }
    
    func configureAssessmentCompletionViewControllerDelegate() {
        if let rootViewController = self.navigationController?.topViewController as? VNAAssessmentCompletionViewController {
            rootViewController.navigationDelegate = self
        }
    }
    
    func configureNavigationStack(topViewController: VNAAssessmentQuestionnaireSectionViewController) {
        let preceedingViewControllers = getAllPreceedingSectionsViewControllers()
        
        if preceedingViewControllers.count == 0 {
            self.navigationController.setViewControllers([topViewController], animated: true)
        } else {
            self.navigationController.setViewControllers([], animated: false)
            for viewController in preceedingViewControllers {
                self.navigationController.pushViewController(viewController, animated: false)
            }
            self.navigationController.pushViewController(topViewController, animated: true)
        }
    }
    
    func setCurrentSectionIndex() {
        currentSectionIndex = 0
        if let currentSectionTypeKey = selectedQuestionnaire?.currentSectionTypeKey {
            guard let section = realm.vnaQuestionnaireSection(by: currentSectionTypeKey) else { return }
            guard let sortedVisibleSections = selectedQuestionnaire?.sortedVisibleQuestionnaireSections() else { return }
            if let index = sortedVisibleSections.index(of: section) {
                currentSectionIndex = index
            }
        }
    }
    
    func configureQuestionnaireSectionViewController(_ viewController: VNAAssessmentQuestionnaireSectionViewController) {
        guard let questionnaireSection = currentQuestionnaireSection else { return }
        
        viewController.title = selectedQuestionnaire?.typeName
        viewController.navigationDelegate = self
        viewController.answeringDelegate = self
        
        self.navigationController.sectionTitle = questionnaireSection.typeName
        let isFirstSection = currentSectionIndex == 0
        let isLastSection = currentSectionIndex == totalVisibleSections - 1
        let questionnaireSectionTypeKey = questionnaireSection.typeKey
        viewController.configure(isFirstSection: isFirstSection,
                                 isLastSection: isLastSection,
                                 questionnaireSectionTypeKey: questionnaireSectionTypeKey)
    }
    
    func configureQuestionnaireSectionViewControllerFor(section: VNAQuestionnaireSection, index: Int) -> VNAAssessmentQuestionnaireSectionViewController? {
        guard let storyboard = self.navigationController?.storyboard else { return nil }
        guard let viewController = storyboard.instantiateViewController(withIdentifier: "VNAAssessmentQuestionnaireSectionViewController") as? VNAAssessmentQuestionnaireSectionViewController else { return nil }
        let isFirstSection = index == 0
        let isLastSection = false
        let questionnaireSectionTypeKey = section.typeKey
        viewController.configure(isFirstSection: isFirstSection,
                                 isLastSection: isLastSection,
                                 questionnaireSectionTypeKey: questionnaireSectionTypeKey)
        return viewController
    }
    
    func configureNavigationBar() {
        navigationController.currentSection = currentSectionIndex + 1
        navigationController.totalSections = totalVisibleSections
        if let questionnaireSection = currentQuestionnaireSection {
            navigationController.sectionIcon = questionnaireSection.image()
            navigationController.sectionTitle = questionnaireSection.typeName
        }
        navigationController.configureNavigationBar()
    }
    
    func configureWithUpdatedVisibleSections(vnaQuestionnaireSectionViewController: VNAAssessmentQuestionnaireSectionViewController) {
        configureNavigationBar()
        vnaQuestionnaireSectionViewController.viewModel?.isLastSection = currentSectionIndex == totalVisibleSections - 1
    }
    
    // MARK: - VNA Coordinator Navigation Functions
    
    func goToNextSection() {
        currentSectionIndex += 1
        configureNavigationBar()
        
        guard let storyboard = self.navigationController?.storyboard else { return }
        guard let viewController = storyboard.instantiateViewController(withIdentifier: "VNAAssessmentQuestionnaireSectionViewController") as? VNAAssessmentQuestionnaireSectionViewController else { return }
        
        configureQuestionnaireSectionViewController(viewController)
        navigationController.pushViewController(viewController, animated: true)
    }
    
    func goToNextQuestionnaire() {
        configureNavigationBar()
        guard let storyboard = self.navigationController?.storyboard else { return }
        guard let viewController = storyboard.instantiateViewController(withIdentifier: "VNAAssessmentQuestionnaireSectionViewController") as? VNAAssessmentQuestionnaireSectionViewController else { return }
        configureQuestionnaireSectionViewController(viewController)
        configureNavigationStack(topViewController: viewController)
    }
    
    func getAllPreceedingSectionsViewControllers() -> [VNAAssessmentQuestionnaireSectionViewController] {
        var preceedingViewControllers = [VNAAssessmentQuestionnaireSectionViewController]()
        if currentSectionIndex != 0 {
            let preceedingSections = getAllPreceedingSections()
            for section in preceedingSections {
                if let index = preceedingSections.index(of: section) {
                    if let viewController = configureQuestionnaireSectionViewControllerFor(section: section, index: index) {
                        preceedingViewControllers.append(viewController)
                    }
                }
            }
        }
        return preceedingViewControllers
    }
    
    public func getAllPreceedingSections() -> [VNAQuestionnaireSection] {
        var index = 0
        var preceedingSections = [VNAQuestionnaireSection]()
        if let questionnaire = selectedQuestionnaire {
            let sortedVisibleSections = questionnaire.sortedVisibleQuestionnaireSections()
            while index < currentSectionIndex {
                preceedingSections.append(sortedVisibleSections[index])
                index += 1
            }
        }
        return preceedingSections
    }
    
    func showVNADataSharingConsentViewController() {
        guard let storyboard = self.navigationController?.storyboard else { return }
        guard let viewController: VNAAssessmentDataSharingConsentViewController = storyboard.instantiateViewController(withIdentifier: "VNAAssessmentDataSharingConsentViewController") as? VNAAssessmentDataSharingConsentViewController else { return }
        viewController.questionnaire = self.selectedQuestionnaire
        self.navigationController?.pushViewController(viewController, animated: true)
        configureVNADataSharingConsentViewControllerDelegate()
    }
    
    func showCompleteViewController() {
        guard let storyboard = self.navigationController?.storyboard else { return }
        guard let viewController: VNAAssessmentCompletionViewController = storyboard.instantiateViewController(withIdentifier: "VNAAssessmentCompletionViewController") as? VNAAssessmentCompletionViewController else { return }
        
        viewController.vnaViewModel?.getAssessmentCompletionStatus(completionFlag: self.completionFlag!, assessmentName: self.selectedAssessment!)
        
        let incompleteQuestionnaires = realm.incompleteVNAQuestionnaires()
        viewController.vnaViewModel = VNACompletionViewModel(incompleteQuestionnaires: incompleteQuestionnaires, sectionTitle: currentSectionTitle, potentialPoints: potentialPoints, assessmentName: self.selectedAssessment!, completionFlag: self.completionFlag!)
        
        self.navigationController.setViewControllers([viewController], animated: true)
        configureAssessmentCompletionViewControllerDelegate()
    }
    
    func getAssessment(completionFlag: Bool, assessmentName: String){
        self.selectedAssessment = assessmentName
        self.completionFlag = completionFlag
    }
}

// MARK: - Delegates

extension VNAAssessmentQuestionnaireFlowCoordinator: VNAAssessmentQuestionnaireSectionViewControllerDelegate {
    
    func sectionControllerDidSelectNext(_ vnaQuestionnaireSectionViewController: VNAAssessmentQuestionnaireSectionViewController) {
        if currentSectionIndex < totalVisibleSections - 1 {
            goToNextSection()
        } else {
            sectionControllerDidSelectDone(vnaQuestionnaireSectionViewController)
        }
    }
    
    func sectionControllerDidSelectBack(_ vnaQuestionnaireSectionViewController: VNAAssessmentQuestionnaireSectionViewController) {
        if currentSectionIndex > 0 {
            currentSectionIndex -= 1
            self.navigationController?.popViewController(animated: true)
            if let topView = navigationController.topViewController as? VNAAssessmentQuestionnaireSectionViewController {
                configureQuestionnaireSectionViewController(topView)
            }
            
            configureNavigationBar()
        }
    }
    
    func sectionControllerDidSelectClose(_ vnaQuestionnaireSectionViewController: VNAAssessmentQuestionnaireSectionViewController) {
        self.navigationController?.topViewController?.performSegue(withIdentifier: "unwindFromQuestionnaireSectionViewControllerToAssessmentLanding", sender: nil)
    }
    
    func sectionControllerDidSelectDone(_ vnaQuestionnaireSectionViewController: VNAAssessmentQuestionnaireSectionViewController) {
        let tenantId = DataProvider.newRealm().getTenantId()
        if VitalityProductFeature.isEnabled((tenantId == 25) ? .VHCDSConsent : .VNADSConsent) {
            self.showVNADataSharingConsentViewController()
        } else {
            vnaQuestionnaireSectionViewController.showHUDOnWindow()
            self.submit(for: vnaQuestionnaireSectionViewController, with: { [weak self] error in
                vnaQuestionnaireSectionViewController.hideHUDFromWindow()
                if let error = error {
                    vnaQuestionnaireSectionViewController.handleErrorWithAlert(error: error, tryAgain: { [weak self] error in
                        self?.sectionControllerDidSelectDone(vnaQuestionnaireSectionViewController)
                    })
                    return
                }
                self?.showCompleteViewController()
            })
        }
    }
}

extension VNAAssessmentQuestionnaireFlowCoordinator: VNAAssessmentDataSharingConsentViewControllerDelegate {
    
    func dataSharingConsentControllerDidSelectAgree() {
        showCompleteViewController()
    }
    
    func dataSharingConsentControllerDidSelectDisagree(_ controller: VNAAssessmentDataSharingConsentViewController) {
        self.navigationController?.popViewController(animated: true)
        if (navigationController.topViewController as? VNAAssessmentQuestionnaireSectionViewController) != nil {
            navigationController?.navigationBar.isHidden = false
            navigationController?.makeNavigationBarTransparent()
            configureNavigationBar()
        }
    }
}

extension VNAAssessmentQuestionnaireFlowCoordinator: VNAAssessmentCompletionViewControllerDelegate {
    
    func completionControllerDidSelectDone(_ completionViewController: VNAAssessmentCompletionViewController) {
        self.navigationController?.topViewController?.performSegue(withIdentifier: "unwindFromCompletionViewControllerToAssessmentLanding", sender: nil)
    }
    
    func completionControllerDidSelectNewQuestionnaire(_ completionViewController: VNAAssessmentCompletionViewController, selectedQuestionnaireTypeKey: Int) {
        selectedQuestionnaire = realm.getVNAQuestionnaire(using: selectedQuestionnaireTypeKey)
        setCurrentSectionIndex()
        goToNextQuestionnaire()
    }
}

// MARK : Submitter

extension VNAAssessmentQuestionnaireFlowCoordinator: VNAAssessmentSubmitter {
    func submit(for viewController: VIAViewController?, with completion:  @escaping (Error?) -> ()) {
        guard let questionnaireTypeKey = self.selectedQuestionnaire?.typeKey else {
            viewController?.displayUnknownServiceErrorOccurredAlert()
            return
        }
        
        VNASubmissionHelper().vnaCaptureAssesment(with: questionnaireTypeKey) { [weak self] (error) in
            if let error = error {
                completion(error)
            } else {
                self?.getAssessment(completionFlag: (self?.selectedQuestionnaire?.completionFlag)!, assessmentName: (self?.selectedQuestionnaire?.typeName)!)
                self?.markQuestionnaireAsCompleted()
                self?.selectedQuestionnaire = nil
                let currentSectionViewController = self?.navigationController.topViewController as? VNAAssessmentQuestionnaireSectionViewController
                currentSectionViewController?.tearDownViewModel()
                
                viewController?.hideHUDFromWindow()
                NotificationCenter.default.post(name: .VIAVNAQuestionnaireSubmittedNotification, object: nil)
                completion(error)
            }
        }
    }
    
    func markQuestionnaireAsCompleted() {
        do {
            try self.realm.write {
                self.selectedQuestionnaire?.setCompletedAndSubmitted()
            }
        } catch {
            print("Error occured marking VNA questionnaire as completed and submitted")
        }
    }
    
    func reloadVNA(for viewController: VIAViewController?, completion: @escaping (Error?) -> ()) {
        let coreRealm = DataProvider.newRealm()
        let partyId = coreRealm.getPartyId()
        let tenantId = coreRealm.getTenantId()
        let membershipId = coreRealm.getMembershipId()
        let setTypeKey = QuestionnaireSetRef.VNA
        let prePopulation = true
        let vnaQuestionnaireParameter = VNAQuestionnaireParameter(setTypeKey: setTypeKey, prePopulation: prePopulation)
        
        Wire.Member.VNAQuestionnaireProgressAndPointsTracking(tenantId: tenantId, partyId: partyId, vitalityMembershipId: membershipId, payload: vnaQuestionnaireParameter) { error in
            if let error = error {
                completion(error)
                return
            }
            completion(nil)
        }
    }
}
