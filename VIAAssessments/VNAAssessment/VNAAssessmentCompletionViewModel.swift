//
//  VNAAssessmentCompletionViewModel.swift
//  VitalityActive
//
//  Created by Von Kervin R. Tuico on 09/02/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import VIAUIKit
import VitalityKit

public class VNACompletionViewModel: AnyObject, CirclesViewModel {
    
    public var headingText: String?
    public var messageText: String?
    public var footnoteText: String?
    public var buttonTitleText: String?
    
    var incompleteQuestionnaires: [VNAQuestionnaire]? {
        didSet {
            configureCorrectState()
        }
    }
    
    var currentSectionTitle: String
    var potentialPoints: Int
    var completionFlag: Bool?
    var assessmentName: String?
    
    let integerFormatter = NumberFormatter.integerFormatter()
    
    public var image: UIImage? {
        return UIImage(asset: .arOnboardingActivated)
    }
    
    public var heading: String? {
        return headingText
    }
    
    public var message: String? {
        return messageText
    }
    
    public var footnote: String? {
        return footnoteText
    }
    
    public var buttonTitle: String? {
        return buttonTitleText
    }
    
    public var buttonHighlightedTextColor: UIColor? {
        return UIColor.knowYourHealthGreen()
    }
    
    public var gradientColor: Color {
        return .green
    }
 
    public func getAssessmentCompletionStatus(completionFlag: Bool, assessmentName: String){
        self.completionFlag = completionFlag
        self.assessmentName = assessmentName
    }
    
    init(incompleteQuestionnaires: [VNAQuestionnaire]?, sectionTitle: String, potentialPoints: Int, assessmentName: String, completionFlag: Bool) {
        self.incompleteQuestionnaires = incompleteQuestionnaires
        self.currentSectionTitle = sectionTitle
        self.potentialPoints = potentialPoints
        self.assessmentName = assessmentName
        self.completionFlag = completionFlag
        configureCorrectState()
    }
    
    func configureForCompleteQuestionnaires() {
        self.buttonTitleText = CommonStrings.Vna.GreatButtonTitle120
        self.headingText = CommonStrings.Vna.Completed.Title1190
        // TODO: Will updated once final value is confirmed
        self.messageText = CommonStrings.Vna.DailyMealsCompletion.Title1190(self.assessmentName!)
        self.footnoteText = CommonStrings.Confirmation.CompletedFootnotePointsMessage119
    }

    func configureForUpdatedAssessments() {
        self.buttonTitleText = CommonStrings.Vna.GreatButtonTitle120
        self.headingText = CommonStrings.Vna.FoodChoicesUpdateCompletionTitle1192(self.assessmentName!)
        self.messageText = CommonStrings.Vna.FoodChoicesUpdatedCompletionDescription1193
    }
    
    func configureForIncompleteQuestionnaires() {
        // TODO: Will updated once final value is confirmed
        self.headingText = CommonStrings.Vna.DailyMealsCompletion.Title1190(self.assessmentName!)
        let count = incompleteQuestionnaires?.count
        if let totalRemainingQuestionnaires = count {
            if totalRemainingQuestionnaires == 1 {
                messageText = CommonStrings.Completed.SingleRemainingQuestionnairesEarnPointsMessage2212(integerFormatter.string(from: totalRemainingQuestionnaires as NSNumber) ?? "", integerFormatter.string(from: potentialPoints as NSNumber) ?? "")
            } else {
                messageText = CommonStrings.Completed.MultipleRemainingQuestionnairesEarnPointsMessage2210(integerFormatter.string(from: totalRemainingQuestionnaires as NSNumber) ?? "", integerFormatter.string(from: potentialPoints as NSNumber) ?? "")
            }
        }
    }
    
    public func configureCorrectState() {
        if (self.completionFlag == true){
            configureForUpdatedAssessments()
        } else if let questionnaires = incompleteQuestionnaires, questionnaires.count > 0 {
            configureForIncompleteQuestionnaires()
        } else {
            configureForCompleteQuestionnaires()
        }
    }
}

