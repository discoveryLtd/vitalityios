//
//  VNAAssessmentMultiSelectQuestionSectionController.swift
//  VitalityActive
//
//  Created by Von Kervin R. Tuico on 10/02/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import IGListKit
import VIAUIKit
import VIAUtilities
import VitalityKit
import RealmSwift

public final class VNAAssessmentMultiSelectQuestionSectionController: IGListSectionController, IGListSectionType, FootnoteToggleable {
    
    // MARK: Properties
    
    var realm: Realm
    
    var questionDetail: VNAAssessmentQuestionPresentationDetail! {
        didSet {
            updateData()
            
            if let validAnswers = self.questionDetail.rawQuestion(in: self.realm)?.capturedResults() {
                selectedAnswers = [VNAAssessmentValidValuePresentationDetail]()
                for validAnswer in validAnswers {
                    if let answer = self.questionDetail.validValuesPresentationDetail?.filter({ $0.rawValidValue.name == validAnswer.answer }).first {
                        selectedAnswers.append(answer)
                    }
                }
            }
        }
    }
    
    var selectedAnswers: [VNAAssessmentValidValuePresentationDetail] = []
    
    // MARK: FootnoteToggleable
    
    var isFootnoteExpanded: Bool = false
    
    var indexForFootnoteCell: Int? {
        return shouldAddNote() ? data.count - 1 : nil
    }
    
    // MARK: Lifecycle
    
    public required init(realm: Realm) {
        self.realm = realm
    }
    
    // MARK: Data
    
    var data: [Any] = []
    
    func updateData() {
        // question
        data = [self.questionDetail.textAndDescriptionDetail()]
        // each answer
        questionDetail.validValuesPresentationDetail?.forEach({ data.append($0) })
        // footnote
        if shouldAddNote() {
            data.append(self.questionDetail.noteDetail())
        }
    }
    
    func shouldAddNote() -> Bool {
        return self.questionDetail.textNote != nil
    }
    
    // MARK: IGListSectionType
    
    public func numberOfItems() -> Int {
        return data.count
    }
    
    public func didUpdate(to object: Any) {
        self.questionDetail = object as! VNAAssessmentQuestionPresentationDetail
    }
    
    public func cellForItem(at index: Int) -> UICollectionViewCell {
        let item = data[index]
        if let textDetail = item as? AttributedTextDetail, textDetail.type == .normal {
            return attributedTextCell(for: textDetail, at: index)
        } else if let textDetail = item as? AttributedTextDetail, textDetail.type == .footnote {
            return footnoteAttributedTextCell(for: textDetail, at: index)
        } else if let answerDetail = item as? VNAAssessmentValidValuePresentationDetail {
            return answerCell(for: answerDetail, at: index)
        }
        return collectionContext!.dequeueReusableCell(of: UICollectionViewCell.self, for: self, at: index)
    }
    
    public func sizeForItem(at index: Int) -> CGSize {
        let width = collectionContext!.containerSize.width
        let item = data[index]
        if let textDetail = item as? AttributedTextDetail, textDetail.type == .normal {
            return VNAAssessmentTextCollectionViewCell.height(with: textDetail, constrainedTo: width)
        } else if let textDetail = item as? AttributedTextDetail, textDetail.type == .footnote {
            return VNAAssessmentFootnoteCollectionViewCell.height(with: textDetail, constrainedTo: width, isFootnoteExpanded: self.isFootnoteExpanded)
        } else if let answerDetail = item as? VNAAssessmentValidValuePresentationDetail {
            if selectedAnswers.contains(answerDetail) {
                return VNAAssessmentMultiSelectAnswerCollectionViewCell.height(with: answerDetail.textAndDescriptionAndNoteDetail(), constrainedTo: width)
            } else {
                return VNAAssessmentMultiSelectAnswerCollectionViewCell.height(with: answerDetail.textAndDescriptionDetail(), constrainedTo: width)
            }
        }
        
        return CGSize.zero
    }
    
    public func didSelectItem(at index: Int) {
        let item = data[index]
        
        // tap footnote
        if let textDetail = item as? AttributedTextDetail, textDetail.type == .footnote {
            toggleFootnote()
            return
        }
        
        if let answerDetail = item as? VNAAssessmentValidValuePresentationDetail {
            // manage answer value
            if selectedAnswers.contains(answerDetail) {
                selectedAnswers.remove(object: answerDetail)
                deleteCapturedResult(with: answerDetail.rawValidValue.name)
            } else {
                selectedAnswers.append(answerDetail)
                persistCapturedResult(with: answerDetail.rawValidValue.name)
                let answerValue = ["Neither", "None"]
                
                for value in answerValue {
                    let isValidValue = answerDetail.rawValidValue.name == value
                    for answers in selectedAnswers{
                        
                        if answerDetail.rawValidValue.name == value {
                            if  isValidValue && answers.rawValidValue.name != value {
                                selectedAnswers.remove(object: answers)
                                deleteCapturedResult(with: answers.rawValidValue.name)
                            }
                        } else {
                            if !isValidValue && answers.rawValidValue.name == value {
                                selectedAnswers.remove(object: answers)
                                deleteCapturedResult(with: answers.rawValidValue.name)
                            }
                        }
                    }
                }
            }
        }
    }
    
    func persistCapturedResult(with answerValue: String) {
        guard let type = self.questionDetail.rawQuestion(in: realm)?.questionType else { return }
        
        if let controller = self.viewController as? VNAAssessmentQuestionnaireSectionViewController {
            controller.answeringDelegate?.persist(answerValue,
                                                  unit: nil,
                                                  for: self.questionDetail.rawQuestionTypeKey,
                                                  of: type,
                                                  allowsMultipleAnswers: true,
                                                  answerIsValid: true)
        }
    }
    
    func deleteCapturedResult(with answerValue: String) {
        if let controller = self.viewController as? VNAAssessmentQuestionnaireSectionViewController {
            controller.answeringDelegate?.delete(answerValue, for: self.questionDetail.rawQuestionTypeKey, allowsMultipleAnswers: true)
        }
    }
    
    // MARK: Cells
    
    func attributedTextCell(for textDetail: AttributedTextDetail, at index: Int) -> UICollectionViewCell {
        let cell = collectionContext!.dequeueReusableCell(of: VNAAssessmentTextCollectionViewCell.self, for: self, at: index) as! VNAAssessmentTextCollectionViewCell
        cell.label.attributedText = textDetail.attributedString
        cell.addTopBorder = true
        cell.addBottomBorder = true
        return cell
    }
    
    func footnoteAttributedTextCell(for textDetail: AttributedTextDetail, at index: Int) -> UICollectionViewCell {
        let cell = collectionContext!.dequeueReusableCell(of: VNAAssessmentFootnoteCollectionViewCell.self, for: self, at: index) as! VNAAssessmentFootnoteCollectionViewCell
        cell.setLabelAttributedText(textDetail.attributedString, isFootnoteExpanded: self.isFootnoteExpanded)
        return cell
    }
    
    func answerCell(for answerDetail: VNAAssessmentValidValuePresentationDetail, at index: Int) -> UICollectionViewCell {
        let cell = collectionContext!.dequeueReusableCell(of: VNAAssessmentMultiSelectAnswerCollectionViewCell.self, for: self, at: index) as! VNAAssessmentMultiSelectAnswerCollectionViewCell
        cell.answerIsSelected = selectedAnswers.contains(answerDetail)
        if selectedAnswers.contains(answerDetail) {
            cell.label.attributedText = answerDetail.textAndDescriptionAndNoteDetail().attributedString
        } else {
            cell.label.attributedText = answerDetail.textAndDescriptionDetail().attributedString
        }
        cell.addSeperator = index != data.count - 1
        cell.addBottomBorder = index == data.count - 1
        return cell
    }
    
}


public final class VNAAssessmentMultiSelectAnswerCollectionViewCell: UICollectionViewCell, Nibloadable {
    
    fileprivate static let inset: CGFloat = 15.0
    
    private static let minimumHeight: CGFloat = 26.0
    
    public var addSeperator = false
    
    public var addBottomBorder = false
    
    var answerIsSelected = false {
        didSet {
            toggleImageViewSelected()
        }
    }
    
    // MARK: Views
    
    public lazy var label: UILabel = {
        let label = UILabel(frame: .zero)
        label.font = UIFont.title2Font()
        label.textColor = UIColor.night()
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    public lazy var imageView: UIImageView = {
        let view = UIImageView(frame: .zero)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.contentMode = .top
        view.image = UIImage.templateImage(asset: .vhrCheckMarkInactive)
        view.tintColor = UIColor.mediumGrey()
        return view
    }()
    
    // MARK: Init
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupSubviews()
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        
        contentView.layer.removeAllBorderLayers()
        if addSeperator {
            contentView.layer.addBottomBorder(inset: VNAAssessmentMultiSelectAnswerCollectionViewCell.inset)
        }
        if addBottomBorder {
            contentView.layer.addBorder(edge: .bottom)
        }
        
        toggleImageViewSelected()
    }
    
    private func setupSubviews() {
        contentView.backgroundColor = .day()
        
        let stackView = UIStackView()
        contentView.addSubview(stackView)
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .horizontal
        stackView.distribution = .fill
        stackView.spacing = 10
        stackView.addArrangedSubview(self.label)
        stackView.addArrangedSubview(self.imageView)
        
        stackView.snp.makeConstraints { (make) in
            make.top.bottom.equalToSuperview().inset(VNAAssessmentMultiSelectAnswerCollectionViewCell.inset)
            make.left.right.equalToSuperview().inset(VNAAssessmentMultiSelectAnswerCollectionViewCell.inset)
            make.height.greaterThanOrEqualTo(imageView.snp.height)
        }
        
        imageView.snp.makeConstraints { (make) in
            make.width.equalTo(VNAAssessmentMultiSelectAnswerCollectionViewCell.minimumHeight).priority(1000)
        }
    }
    
    override public func preferredLayoutAttributesFitting(_ layoutAttributes: UICollectionViewLayoutAttributes) -> UICollectionViewLayoutAttributes {
        return self.defaultContentViewLayoutSizeFitting(layoutAttributes)
    }
    
    // MARK: Helpers
    
    public func toggleImageViewSelected() {
        if answerIsSelected {
            imageView.image = UIImage.templateImage(asset: .vhrCheckMarkActive)
            imageView.tintColor = UIColor.currentGlobalTintColor()
        } else {
            imageView.image = UIImage.templateImage(asset: .vhrCheckMarkInactive)
            imageView.tintColor = UIColor.mediumGrey()
        }
    }
    
    // MARK: Height calculation
    
    public class func height(with attributedTextDetail: AttributedTextDetail, constrainedTo width: CGFloat) -> CGSize {
        let inset = VNAAssessmentMultiSelectAnswerCollectionViewCell.inset
        let insets = UIEdgeInsets(top: 0, left: inset, bottom: 0, right: inset)
        let size = TextSize.size(attributedTextDetail.attributedString, width: width, insets: insets)
        let height = max(size.height, VNAAssessmentMultiSelectAnswerCollectionViewCell.minimumHeight) + inset * 2
        return CGSize(width: width, height: height)
    }
    
}

