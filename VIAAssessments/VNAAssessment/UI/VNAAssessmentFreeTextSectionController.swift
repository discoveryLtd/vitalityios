//
//  VNAAssessmentFreeTextSectionController.swift
//  VitalityActive
//
//  Created by Von Kervin R. Tuico on 10/02/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import IGListKit
import VIAUIKit
import VitalityKit
import VIAUtilities
import RealmSwift
import CoreGraphics

public final class VNAAssessmentFreeTextSectionController: IGListSectionController, IGListSectionType, FootnoteToggleable {
    
    // MARK: Properties
    
    var realm: Realm
    
    var questionDetail: VNAAssessmentQuestionPresentationDetail!
    
    lazy var characterLimit: Int = {
        return self.questionDetail.rawQuestion(in: self.realm)?.length.value ?? 0
    }()
    
    private var answerText: String = ""
    
    var footnoteDetail: AttributedTextDetail? {
        if self.questionDetail.textNote != nil {
            return self.questionDetail.noteDetail()
        }
        return nil
    }
    
    // MARK: FootnoteToggleable
    
    var isFootnoteExpanded: Bool = false
    
    var indexForFootnoteCell: Int? {
        return self.footnoteDetail != nil ? 2 : nil
    }
    
    // MARK: Lifecycle
    
    public required init(realm: Realm) {
        self.realm = realm
    }
    
    // MARK: IGListSectionType
    
    public func numberOfItems() -> Int {
        var count = 2
        if self.footnoteDetail != nil {
            count = count + 1
        }
        return count
    }
    
    public func didUpdate(to object: Any) {
        self.questionDetail = object as! VNAAssessmentQuestionPresentationDetail
    }
    
    public func cellForItem(at index: Int) -> UICollectionViewCell {
        if index == 0 {
            return attributedTextCell(for: self.questionDetail.textAndDescriptionDetail(), at: index)
        } else if index == 1 {
            return freeTextInputCell(at: index)
        } else if index == 2, let noteDetail = self.footnoteDetail {
            return footnoteAttributedTextCell(for: noteDetail, at: index)
        }
        return collectionContext!.dequeueReusableCell(of: UICollectionViewCell.self, for: self, at: index)
    }
    
    public func sizeForItem(at index: Int) -> CGSize {
        let width = collectionContext!.containerSize.width
        if index == 0 {
            return VNAAssessmentTextCollectionViewCell.height(with: self.questionDetail.textAndDescriptionDetail(), constrainedTo: width)
        } else if index == 1 {
            return VNAAssessmentFreeTextCollectionViewCell.height(constrainedTo: width, characterLimit: characterLimit)
        } else if index == 2, let noteDetail = self.footnoteDetail {
            return VNAAssessmentFootnoteCollectionViewCell.height(with: noteDetail, constrainedTo: width, isFootnoteExpanded: self.isFootnoteExpanded)
        }
        return CGSize.zero
    }
    
    public func didSelectItem(at index: Int) {
        if index == 2 {
            toggleFootnote()
        }
    }
    
    // MARK: Cells
    
    func attributedTextCell(for textDetail: AttributedTextDetail, at index: Int) -> UICollectionViewCell {
        let cell = collectionContext!.dequeueReusableCell(of: VNAAssessmentTextCollectionViewCell.self, for: self, at: index) as! VNAAssessmentTextCollectionViewCell
        cell.label.attributedText = textDetail.attributedString
        cell.addTopBorder = true
        cell.addBottomBorder = false
        return cell
    }
    
    func footnoteAttributedTextCell(for textDetail: AttributedTextDetail, at index: Int) -> UICollectionViewCell {
        let cell = collectionContext!.dequeueReusableCell(of: VNAAssessmentFootnoteCollectionViewCell.self, for: self, at: index) as! VNAAssessmentFootnoteCollectionViewCell
        cell.setLabelAttributedText(textDetail.attributedString, isFootnoteExpanded: self.isFootnoteExpanded)
        return cell
    }
    
    func freeTextInputCell(at index: Int) -> UICollectionViewCell {
        let cell = collectionContext!.dequeueReusableCell(of: VNAAssessmentFreeTextCollectionViewCell.self, for: self, at: index) as! VNAAssessmentFreeTextCollectionViewCell
//        cell.setTextViewAsFirstResponder()
        cell.characterLimit = characterLimit
        cell.addTopBorder = false
        cell.addBottomBorder = true
        cell.textViewDidChange = self.persistCapturedResult
        if let validAnswer = self.questionDetail.rawQuestion(in: self.realm)?.capturedResult()?.answer {
            cell.setText(validAnswer)
        } else {
            persistCapturedResult(with: "")
        }
        return cell
    }
    
    // MARK: Actions
    
    
    func persistCapturedResult(with text: String) {
        guard let type = self.questionDetail.rawQuestion(in: realm)?.questionType else { return }

        guard !text.isEmpty else {
//            deleteCapturedResult(with: answerText)
            return
        }

        answerText = text
        if let controller = self.viewController as? VNAAssessmentQuestionnaireSectionViewController {
            controller.answeringDelegate?.persist(answerText,
                                                  unit: nil,
                                                  for: self.questionDetail.rawQuestionTypeKey,
                                                  of: type,
                                                  allowsMultipleAnswers: false,
                                                  answerIsValid: true)
        }
    }

    
    
    func deleteCapturedResult(with text: String) {
        if let controller = self.viewController as? VNAAssessmentQuestionnaireSectionViewController {
            controller.answeringDelegate?.delete(text, for: self.questionDetail.rawQuestionTypeKey, allowsMultipleAnswers: true)
        }
    }
    
}


public final class VNAAssessmentFreeTextCollectionViewCell: UICollectionViewCell, Nibloadable, UITextViewDelegate {
    
    fileprivate static let verticalInset: CGFloat = 10.0
    
    fileprivate static let horizontalInset: CGFloat = 15.0
    
    fileprivate static let textViewMinHeight: CGFloat = 88
    
    fileprivate static let textViewMaxHeight: CGFloat = 400
    
    fileprivate static let textViewFont: UIFont = UIFont.bodyFont()
    
    fileprivate static let counterLabelFont: UIFont = UIFont.footnoteFont()
    
    fileprivate static let placeholder: String = CommonStrings.Assessment.EnterDetails352
    
    public var characterLimit: Int = 0 {
        didSet {
            updateTextViewHeight()
            updateTextLimitCount()
        }
    }
    
    fileprivate var addTopBorder = false
    
    fileprivate var addBottomBorder = false
    
    fileprivate var textViewDidChange: ((String) -> Void)?
    
    fileprivate var textViewDidEndEditing: ((String) -> Void)?
    
    // MARK: Views
    
    lazy private var textView: UITextView = {
        let textView = UITextView()
        textView.text = VNAAssessmentFreeTextCollectionViewCell.placeholder
        textView.textColor = UIColor.textFieldPlaceholder()
        textView.textContainer.lineFragmentPadding = 0
        textView.font = VNAAssessmentFreeTextCollectionViewCell.textViewFont
        
        // This is a hotfix for FC-24420 on Ecuador on top of miss Wen's implementation on the scrolling issue.
        // TODO: valtomol Refactor this to adjust the textView height based on the character length content.
        if AppSettings.getAppTenant() == .EC || AppSettings.getAppTenant() == .ASR {
            textView.isScrollEnabled = false
        }
        
        textView.delegate = self
        return textView
    }()
    
    lazy private var counterLabel: UILabel = {
        let label = UILabel(frame: .zero)
        label.font = VNAAssessmentFreeTextCollectionViewCell.counterLabelFont
        label.textColor = UIColor.darkGrey()
        label.textAlignment = .right
        return label
    }()
    
    // MARK: Init
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupSubviews()
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        
        contentView.layer.removeAllBorderLayers()
        if addTopBorder {
            contentView.layer.addBorder(edge: .top)
        }
        if addBottomBorder {
            contentView.layer.addBorder(edge: .bottom)
        }
    }
    
    private func setupSubviews() {
        contentView.backgroundColor = .day()
        
        let stackView = UIStackView()
        contentView.addSubview(stackView)
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.distribution = .fill
        stackView.spacing = 10
        stackView.addArrangedSubview(textView)
        
        // stackview
        stackView.snp.makeConstraints { (make) in
            make.top.bottom.equalToSuperview().inset(VNAAssessmentFreeTextCollectionViewCell.verticalInset)
            make.left.right.equalToSuperview().inset(VNAAssessmentFreeTextCollectionViewCell.horizontalInset)
        }
        
        // counter label
        counterLabel.snp.makeConstraints { (make) in
            make.height.equalTo(counterLabel.font.lineHeight)
        }
        
        updateTextViewHeight()
        
        // initial text setup
        updateTextLimitCount()
    }
    
    func updateTextViewHeight() {
        let height = VNAAssessmentFreeTextCollectionViewCell.textViewHeight(characterLimit: characterLimit, width: textView.frame.width)
        textView.snp.makeConstraints { (make) in
            make.height.equalTo(height)
        }
        
        self.setNeedsLayout()
        self.layoutIfNeeded()
        self.updateConstraints()
        self.updateConstraintsIfNeeded()
    }
    
    private class func textViewHeight(characterLimit: Int, width: CGFloat) -> CGFloat {
        let averageCharWidth = VNAAssessmentFreeTextCollectionViewCell.textViewFont.pointSize / 2 // guesstimate!
        let totalWidthBasedOnCharLimit = averageCharWidth * CGFloat(characterLimit)
        let numberOfLines = ceil(totalWidthBasedOnCharLimit / (width > 0 ? width : 1))
        var textViewHeight = VNAAssessmentFreeTextCollectionViewCell.textViewFont.lineHeight * numberOfLines
        textViewHeight = ceil(textViewHeight)
        textViewHeight = min(textViewMaxHeight, textViewHeight)
        textViewHeight = max(textViewHeight, textViewMinHeight)
        return textViewHeight + VNAAssessmentFreeTextCollectionViewCell.verticalInset * 2
    }
    
    override public func preferredLayoutAttributesFitting(_ layoutAttributes: UICollectionViewLayoutAttributes) -> UICollectionViewLayoutAttributes {
        return self.defaultContentViewLayoutSizeFitting(layoutAttributes)
    }
    
    // MARK: Height calculation
    
    public class func height(constrainedTo width: CGFloat, characterLimit: Int) -> CGSize {
        let height = VNAAssessmentFreeTextCollectionViewCell.textViewHeight(characterLimit: characterLimit, width: width)
        return CGSize(width: width, height: height)
    }
    
    // MARK: UITextViewDelegate
    
    func setTextViewAsFirstResponder() {
        textView.becomeFirstResponder()
    }
    
    func setText(_ text: String?) {
        textView.text = text
        textViewDidBeginEditing(textView)
        updateTextLimitCount()
    }
    
    func updateTextLimitCount() {
        var count = 0
        if textView.text == VNAAssessmentFreeTextCollectionViewCell.placeholder {
            count = self.characterLimit
        } else {
            count = self.characterLimit - textView.text.characters.count
        }
        counterLabel.text = Localization.integerFormatter.string(from: count as NSNumber)
        counterLabel.textColor = count >= 0 ? UIColor.darkGrey() : UIColor.genericError()
    }

    public func textViewDidChange(_ textView: UITextView) {
        updateTextLimitCount()
//        Removed, root cause of issue causing keyboard being dismissed per character input
//        if textView.text != VNAAssessmentFreeTextCollectionViewCell.placeholder && !textView.text.isEmpty {
//            self.textViewDidChange?(textView.text)
//        } else {
//            self.textViewDidChange?("")
//        }
    }
    
    public func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == VNAAssessmentFreeTextCollectionViewCell.placeholder {
            textView.text = nil
        }
        textView.textColor = UIColor.night()
        textView.becomeFirstResponder()
    }
    
    public func textViewDidEndEditing(_ textView: UITextView) {
        updateTextLimitCount()
        if textView.text != VNAAssessmentFreeTextCollectionViewCell.placeholder && !textView.text.isEmpty {
            self.textViewDidChange?(textView.text)
        } else {
            self.textViewDidChange?("")
        }
        if textView.text.isEmpty {
            textView.text = VNAAssessmentFreeTextCollectionViewCell.placeholder
            textView.textColor = UIColor.textFieldPlaceholder()
        }
        textView.resignFirstResponder()
    }
    
    public func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        // TO DO: Will be updated with acceptable japanese characters
        //let acceptableCharacters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz "
        
        //let cs = NSCharacterSet(charactersIn: acceptableCharacters).inverted
        //let filtered = text.components(separatedBy: cs).joined(separator: "")
        
        //return (text == filtered)
        return true
    }
    
}

