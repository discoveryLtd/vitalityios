//
//  VNAAssessmentLabelQuestionSectionController.swift
//  VitalityActive
//
//  Created by Von Kervin R. Tuico on 10/02/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import IGListKit
import VIAUIKit
import VIAUtilities
import VitalityKit
import RealmSwift

public final class VNAAssessmentLabelQuestionSectionController: IGListSectionController, IGListSectionType {
    
    // MARK: Properties
    
    var realm: Realm
    
    var questionDetail: VNAAssessmentQuestionPresentationDetail!
    
    // MARK: Lifecycle
    
    public required init(realm: Realm) {
        self.realm = realm
    }
    
    // MARK: IGListSectionType
    
    public func numberOfItems() -> Int {
        return 1
    }
    
    public func didUpdate(to object: Any) {
        self.questionDetail = object as! VNAAssessmentQuestionPresentationDetail
    }
    
    public func cellForItem(at index: Int) -> UICollectionViewCell {
        return attributedTextCell(for: questionDetail.textAndDescriptionDetail(), at: index)
    }
    
    public func sizeForItem(at index: Int) -> CGSize {
        let width = collectionContext!.containerSize.width
        return VNAAssessmentTextCollectionViewCell.height(with: questionDetail.textAndDescriptionDetail(), constrainedTo: width)
    }
    
    public func didSelectItem(at index: Int) { }
    
    // MARK: Cells
    
    func attributedTextCell(for textDetail: AttributedTextDetail, at index: Int) -> UICollectionViewCell {
        let cell = collectionContext!.dequeueReusableCell(of: VNAAssessmentTextCollectionViewCell.self, for: self, at: index) as! VNAAssessmentTextCollectionViewCell
        cell.label.attributedText = textDetail.attributedString
        cell.addTopBorder = true
        cell.addBottomBorder = true
        return cell
    }
    
}

