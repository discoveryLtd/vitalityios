//
//  VNAAssessmentQuestionnaireHeaderFooterSectionController.swift
//  VitalityActive
//
//  Created by Von Kervin R. Tuico on 10/02/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import IGListKit
import VIAUIKit
import VIAUtilities

public final class VNAAssessmentQuestionnaireHeaderFooterSectionController: IGListSectionController, IGListSectionType {
    
    // MARK: Properties
    
    var headerFooter: VNAAssessmentQuestionnaireSectionHeaderFooterDetail!
    
    // MARK: IGListSectionType
    
    public func numberOfItems() -> Int {
        return 1
    }
    
    public func didUpdate(to object: Any) {
        self.headerFooter = object as! VNAAssessmentQuestionnaireSectionHeaderFooterDetail
    }
    
    public func cellForItem(at index: Int) -> UICollectionViewCell {
        let cell = collectionContext!.dequeueReusableCell(of: AssessmentTextCollectionViewCell.self, for: self, at: index) as! AssessmentTextCollectionViewCell
        cell.label.attributedText = headerFooter.textAndDescriptionDetail().attributedString
        cell.addTopBorder = true
        cell.addBottomBorder = true
        return cell
    }
    
    public func sizeForItem(at index: Int) -> CGSize {
        let width = collectionContext!.containerSize.width
        return AssessmentTextCollectionViewCell.height(with: headerFooter.textAndDescriptionDetail(), constrainedTo: width)
    }
    
    public func didSelectItem(at index: Int) { }
    
}

