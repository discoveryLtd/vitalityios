import IGListKit
import VIAUIKit
import VIAUtilities
import VitalityKit
import VitalityKit
import RealmSwift
import VIACommon

public protocol VNAAssessmentCaptureResultsDelegate: class {
    func show(menu: UIAlertController)
    func didSelectUnitOfMeasure(_ unitOfMeasure: UnitOfMeasureRef)
}

public final class VNAAssessmentNumberRangeQuestionSectionController: IGListSectionController, IGListSectionType, VNAAssessmentCaptureResultsDelegate, FootnoteToggleable {
    
    // MARK: Properties
    
    var realm: Realm
    
    var questionDetail: VNAAssessmentQuestionPresentationDetail! {
        didSet {
            if let validAnswers = self.questionDetail.validValuesPresentationDetail {
                let validValues = (validAnswers.map({ $0.rawValidValue }))
                self.metric = VNAAssessmentMetric(validValues: validValues)
                
                for values in validValues {
                    self.selectedUnitOfMeasureType = UnitOfMeasureRef(rawValue: values.unitOfMeasure.rawValue)!
                }
                debugPrint("Valid Metric Values: \(validValues)")
            } else if let unitsOfMeasure = self.questionDetail.rawQuestion(in: realm)?.unitsOfMeasure {
                debugPrint("No valid values to validate against, using question's units of measure")
                let values = unitsOfMeasure.flatMap({ $0.unitOfMeasureRef() })
                debugPrint("Setting first unit of measure as selected type")
                if let firstUnitOfMeasure = values.first {
                    self.selectedUnitOfMeasureType = firstUnitOfMeasure
                    debugPrint("Selected Unit of Measure: \(firstUnitOfMeasure)")
                }
                self.metric = VNAAssessmentMetric(unitsOfMeasure: Array(values))
            } else {
                debugPrint("No valid values to validate against, nor units of measure on the question")
            }
        }
    }
    
    var decimalFormatter = Localization.decimalFormatter
    
    var serviceFormatter = NumberFormatter.serviceFormatter()
    
    var metric: VNAAssessmentMetric?
    
    var selectedUnit: Unit?
    
    var input: String?
    
    var isValid: Bool = false
    
    var selectedUnitOfMeasureType: UnitOfMeasureRef = UnitOfMeasureRef.Unknown
    
    // MARK: FootnoteToggleable
    
    var isFootnoteExpanded: Bool = false
    
    var indexForFootnoteCell: Int? {
        return self.footnoteDetail != nil ? 2 : nil
    }
    
    fileprivate let DUAL_INPUT: CGFloat = 96
    fileprivate let SINGLE_INPUT: CGFloat = 48
    fileprivate var INPUT_HEIGHT: CGFloat = 48
    
    // MARK: Lifecycle
    
    public required init(realm: Realm) {
        self.realm = realm
    }
    
    // MARK: Data
    
    var capturedResult: VNACapturedResult? {
        return self.questionDetail.rawQuestion(in: self.realm)?.capturedResult()
    }
    
    var footnoteDetail: AttributedTextDetail? {
        if self.questionDetail.textNote != nil {
            return self.questionDetail.noteDetail()
        }
        return nil
    }
    
    // MARK: IGListSectionType
    
    public func numberOfItems() -> Int {
        var count = 2
        if self.footnoteDetail != nil {
            count = count + 1
        }
        return count
    }
    
    public func didUpdate(to object: Any) {
        self.questionDetail = object as! VNAAssessmentQuestionPresentationDetail
    }
    
    public func cellForItem(at index: Int) -> UICollectionViewCell {
        if index == 0 {
            return attributedTextCell(for: self.questionDetail.textAndDescriptionDetail(), at: index)
        } else if index == 1 {
            return inputCell(at: index)
        } else if index == 2, let noteDetail = self.footnoteDetail {
            return footnoteAttributedTextCell(for: noteDetail, at: index)
        }
        
        return collectionContext!.dequeueReusableCell(of: UICollectionViewCell.self, for: self, at: index)
    }
    
    public func sizeForItem(at index: Int) -> CGSize {
        let width = collectionContext!.containerSize.width
        
        if index == 0 {
            return VNAAssessmentTextCollectionViewCell.height(with: self.questionDetail.textAndDescriptionDetail(), constrainedTo: width)
        } else if index == 1 {
            var uom: UnitOfMeasureRef = self.selectedUnitOfMeasureType
            if uom == .Unknown{
                if let unit = self.capturedResult?.unitOfMeasure{
                    uom = unit
                    debugPrint("Unit: \(unit)")
                }
            }
            if uom == .FootInch || uom == .StonePound{
                INPUT_HEIGHT = DUAL_INPUT
            }
            return CGSize(width: width, height: INPUT_HEIGHT)
        } else if index == 2, let noteDetail = self.footnoteDetail {
            return VNAAssessmentFootnoteCollectionViewCell.height(with: noteDetail, constrainedTo: width, isFootnoteExpanded: self.isFootnoteExpanded)
        }
        
        return CGSize.zero
    }
    
    public func didSelectItem(at index: Int) {
        if index == 2 {
            toggleFootnote()
        }
    }
    
    // MARK: Cells
    
    func attributedTextCell(for textDetail: AttributedTextDetail, at index: Int) -> UICollectionViewCell {
        let cell = collectionContext!.dequeueReusableCell(of: VNAAssessmentTextCollectionViewCell.self, for: self, at: index) as! VNAAssessmentTextCollectionViewCell
        cell.label.attributedText = textDetail.attributedString
        cell.addTopBorder = true
        cell.addBottomBorder = true
        return cell
    }
    
    func footnoteAttributedTextCell(for textDetail: AttributedTextDetail, at index: Int) -> UICollectionViewCell {
        let cell = collectionContext!.dequeueReusableCell(of: VNAAssessmentFootnoteCollectionViewCell.self, for: self, at: index) as! VNAAssessmentFootnoteCollectionViewCell
        cell.setLabelAttributedText(textDetail.attributedString, isFootnoteExpanded: self.isFootnoteExpanded)
        return cell
    }
    
    func inputCell(at index: Int) -> UICollectionViewCell {
        let cell = collectionContext!.dequeueReusableCell(withNibName: VNAAssessmentNumberRangeAnswerCollectionViewCell.defaultReuseIdentifier, bundle: VNAAssessmentNumberRangeAnswerCollectionViewCell.bundle(), for: self, at: index) as! VNAAssessmentNumberRangeAnswerCollectionViewCell
        cell.captureResultsDelegate = self
        cell.unitButton.isHidden = true
        cell.questionRawTypeKey = self.questionDetail.rawQuestionTypeKey
        
        /* Check if Decimal Input should be disabled */
        if let shouldDisableDecimalInput = VIAApplicableFeatures.default.disableKeyboardDecimalInput,
            shouldDisableDecimalInput {
            self.disableKeyboardDecimalInput(cell: cell)
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(updateCapturedResult), name: Notification.Name("updateNumberValues"), object: nil)
        
        // textFieldTextDidChange
        cell.textFieldTextDidChange = { [unowned self] textField, unit, min, max in
            var uomType = self.selectedUnitOfMeasureType
            if let metric = self.metric, metric.validate(cell.inputTextField.text, unit, &uomType, &min, &max) {
                cell.updateInputValid(true, nil, nil, cell.validationErrorLabel)
                AppSettings.setHasProvidedValidAssessmentValues(true)
            } else {
                cell.updateInputValid(false, min, max, cell.validationErrorLabel)
                AppSettings.setHasProvidedValidAssessmentValues(false)
            }
            
            if let isValid = self.metric?.validate(cell.inputTextField.text, unit, &uomType, &min, &max){
                if self.selectedUnitOfMeasureType == .FootInch || self.selectedUnitOfMeasureType == .StonePound{
                    if let isValidSecondField = self.metric?.validate(cell.secondInputField.text, unit, &uomType, &min, &max){
                        
                        self.updateValues(isValid: isValid && isValidSecondField,
                                          uomType: self.selectedUnitOfMeasureType, unit: unit,
                                          inputText: self.getInput(cell: cell))
                    }
                }else{
                    self.updateValues(isValid: isValid, uomType: self.selectedUnitOfMeasureType, unit: unit, inputText: cell.inputTextField.text)
                }
                cell.updateInputValid(isValid, min, max, cell.validationErrorLabel)
            }
            
            AppSettings.setHasUpdatedNumberRangeInSection(true)
        }
        
        // textFieldDidEndEditing
        cell.textFieldDidEndEditing = { [unowned self] textField, unit, min, max in
            var uomType = self.selectedUnitOfMeasureType
            if let isValid = self.metric?.validate(cell.inputTextField.text, unit, &uomType, &min, &max){
                if self.selectedUnitOfMeasureType == .FootInch || self.selectedUnitOfMeasureType == .StonePound{
                    if let isValidSecondField = self.metric?.validate(cell.secondInputField.text, unit, &uomType, &min, &max){
                        
                        self.updateValues(isValid: isValid && isValidSecondField,
                                          uomType: self.selectedUnitOfMeasureType, unit: unit,
                                          inputText: self.getInput(cell: cell))
                    }
                }else{
                    self.updateValues(isValid: isValid, uomType: self.selectedUnitOfMeasureType, unit: unit, inputText: cell.inputTextField.text)
                }
                cell.updateInputValid(isValid, min, max, cell.validationErrorLabel)
                self.updateCapturedResult()
            }
        }
        
        // secondTextFieldTextDidChange
        cell.secondTextFieldTextDidChange = { [unowned self] textField, unit, min, max in
            var uomType = self.selectedUnitOfMeasureType
            /**
             * Manually override the UnitOfMeasureTypeRef into Inch for checking in the AssessmentMetric.validate
             * This is to manually override the Upper and Lower Limit for Pound (0-11)
             */
            if self.selectedUnitOfMeasureType == .FootInch{
                uomType = .Inch
            }
            
            /**
             * Manually override the UnitOfMeasureTypeRef into Pound for checking in the AssessmentMetric.validate
             * This is to manually override the Upper and Lower Limit for Pound (0-13)
             */
            if self.selectedUnitOfMeasureType == .StonePound{
                uomType = .Pound
            }
            if let metric = self.metric, metric.validate(textField.text, unit, &uomType, &min, &max) {
                cell.updateInputValid(true, nil, nil, cell.secondValidationErrorLabel)
            }
        }
        
        // secondTextFieldDidEndEditing
        cell.secondTextFieldDidEndEditing = { [unowned self] textField, unit, min, max in
            var uomType = self.selectedUnitOfMeasureType
            /**
             * Manually override the UnitOfMeasureTypeRef into Inch for checking in the AssessmentMetric.validate
             * This is to manually override the Upper and Lower Limit for Pound (0-11)
             */
            if self.selectedUnitOfMeasureType == .FootInch{
                uomType = .Inch
            }
            
            /**
             * Manually override the UnitOfMeasureTypeRef into Pound for checking in the AssessmentMetric.validate
             * This is to manually override the Upper and Lower Limit for Pound (0-13)
             */
            if self.selectedUnitOfMeasureType == .StonePound{
                uomType = .Pound
            }
            if let isValid = self.metric?.validate(textField.text, unit, &uomType, &min, &max) {
                if self.selectedUnitOfMeasureType == .FootInch || self.selectedUnitOfMeasureType == .StonePound{
                    if let isValidFirstField = self.metric?.validate(cell.inputTextField.text, unit, &uomType, &min, &max){
                        
                        self.updateValues(isValid: isValid && isValidFirstField,
                                          uomType: self.selectedUnitOfMeasureType, unit: unit,
                                          inputText: self.getInput(cell: cell))
                    }
                }else{
                    self.updateValues(isValid: isValid, uomType: self.selectedUnitOfMeasureType, unit: unit, inputText: textField.text)
                }
                self.updateCapturedResult()
                cell.updateInputValid(isValid, min, max, cell.secondValidationErrorLabel)
            }
            
        }
        
        // configure the cell
        var inputText = localFormatted(from: self.capturedResult?.answer, selectedUnit: self.capturedResult?.unitOfMeasure.unit())
        if let uom = self.capturedResult?.unitOfMeasure{
            if uom == .FootInch || uom == .StonePound{
                inputText = self.capturedResult?.answer ?? ""
            }
        }
        
        if .Unknown == self.selectedUnitOfMeasureType{
            self.selectedUnitOfMeasureType = (self.capturedResult?.unitOfMeasure ?? self.metric?.rawUnits.first) ?? .Unknown
        }
        
        //Initialize selected unit of measurement type from captureResult value
        cell.configureCaptureResultCell(units: self.metric?.rawUnits ?? [],
                                        inputPlaceholder: CommonStrings.Assessment.GenericInputPlaceholder507,
                                        inputText: inputText,
                                        inputUnitOfMeasure: self.selectedUnitOfMeasureType)
        
        //Collapse second input view if current height is for SINGLE INPUT
        cell.secondUserInputStackView.isHidden = INPUT_HEIGHT == SINGLE_INPUT
        cell.secondInputField.isHidden = cell.secondUserInputStackView.isHidden
        cell.secondValidationErrorLabel.isHidden = cell.secondUserInputStackView.isHidden
        
        // update validation for cell, to be visually correct
        // directly after being displayed
        let unit = selectedUnitOfMeasureType.unit()
        var uomType = selectedUnitOfMeasureType
        var min: Double?
        var max: Double?
        //        var secondUnitMin: Double?
        //        var secondUnitMax: Double?
        
        var value = inputText
        var values = [String]()
        var uomSecondType = UnitOfMeasureRef.Unknown
        if uomType == .FootInch || uomType == .StonePound{
            if uomType == .FootInch{
                values = cell.getValueFromAnswer(toSearch: inputText ?? "", uom1: "FT", uom2: "INCH")
                value = values[0]
                uomSecondType = .Inch
            }else if uomType == .StonePound{
                values = cell.getValueFromAnswer(toSearch: inputText ?? "", uom1: "ST", uom2: "LB")
                value = values[0]
                uomSecondType = .Pound
            }
            if values.count > 1{
                if let metric = self.metric?.validate(values[1], unit, &uomSecondType, &min, &max) {
                    cell.updateInputValid(metric, min, max, cell.secondValidationErrorLabel)
                }
            }
        }
        
        if let isValid = self.metric?.validate(value, unit, &uomType, &min, &max) {
            self.updateValues(isValid: isValid, uomType: uomType, unit: unit, inputText: inputText)
            cell.updateInputValid(isValid, min, max, cell.validationErrorLabel)
        }
        
        return cell
    }
    
    private func getInput(cell: VNAAssessmentNumberRangeAnswerCollectionViewCell) -> String{
        
        var firstValString  = ""
        if let firstVal     = Int(cell.inputTextField.text ?? ""){
            firstValString = String(firstVal)
        }
        
        var secondValString = ""
        if let secondVal    = Double(cell.secondInputField.text ?? ""){
            secondValString = String(secondVal)
        }
        
        var firstUoM    = ""
        var secondUoM    = ""
        if self.selectedUnitOfMeasureType == .FootInch{
            firstUoM    = "FT"
            secondUoM   = "INCH"
        }else if self.selectedUnitOfMeasureType == .StonePound{
            firstUoM    = "ST"
            secondUoM   = "LB"
        }
        
        return "\(firstValString) \(firstUoM) \(secondValString) \(secondUoM)"
    }
    
    func updateValues(isValid: Bool, uomType: UnitOfMeasureRef, unit: Unit?, inputText: String?) {
        self.selectedUnitOfMeasureType  = uomType
        self.isValid                    = isValid
        self.selectedUnit               = unit
        self.input                      = inputText
    }
    
    // MARK: CaptureResultsDelegate
    
    public func show(menu: UIAlertController) {
        if let controller = self.viewController {
            controller.present(menu, animated: true, completion: nil)
        }
    }
    
    public func didSelectUnitOfMeasure(_ unitOfMeasure: UnitOfMeasureRef) {
        self.selectedUnitOfMeasureType = unitOfMeasure
        deletePreviousCapturedData()
        
        //RELOAD Index
        INPUT_HEIGHT = (unitOfMeasure == .FootInch || unitOfMeasure == .StonePound) ? DUAL_INPUT : SINGLE_INPUT
        collectionContext?.reload(self)
        
    }
    
    // MARK: Persist
    
    public func deletePreviousCapturedData(){
        
        if let data = self.capturedResult{
            try! realm.write {
                realm.delete(data)
            }
        }
        
    }
    func updateCapturedResult() {
        // safety checks
        guard let validInput = self.input else {
            return
        }
        
        if selectedUnitOfMeasureType == .FootInch || selectedUnitOfMeasureType == .StonePound{
            guard let type = self.questionDetail.rawQuestion(in: realm)?.questionType else {
                return
            }
            
            // warn and persist
            if self.selectedUnitOfMeasureType == .Unknown {
                debugPrint("Capturing result with Unknown unit of measure. This happens when neither the question nor the valid values have any UOM configured.")
            }
            if let controller = self.viewController as? VNAAssessmentQuestionnaireSectionViewController {
                controller.answeringDelegate?.persist(validInput,
                                                      unit: self.selectedUnitOfMeasureType,
                                                      for: self.questionDetail.rawQuestionTypeKey,
                                                      of: type,
                                                      allowsMultipleAnswers: false,
                                                      answerIsValid: self.isValid)
            }
        }else{
            guard let systemFormattedNumberString = systemFormatted(from: validInput) else {
                deleteCapturedResult(with: validInput)
                return
            }
            
            guard let type = self.questionDetail.rawQuestion(in: realm)?.questionType else {
                return
            }
            
            // warn and persist
            if self.selectedUnitOfMeasureType == .Unknown {
                debugPrint("Capturing result with Unknown unit of measure. This happens when neither the question nor the valid values have any UOM configured.")
            }
            if let controller = self.viewController as? VNAAssessmentQuestionnaireSectionViewController {
                deleteCapturedResult(with: validInput)
                controller.answeringDelegate?.persist(systemFormattedNumberString,
                                                      unit: self.selectedUnitOfMeasureType,
                                                      for: self.questionDetail.rawQuestionTypeKey,
                                                      of: type,
                                                      allowsMultipleAnswers: false,
                                                      answerIsValid: self.isValid)
            }
        }
        print("===>UpdateCapturedResult:\(self.capturedResult)")
    }
    
    func deleteCapturedResult(with text: String) {
        if let controller = self.viewController as? VNAAssessmentQuestionnaireSectionViewController {
            controller.answeringDelegate?.delete(text, for: self.questionDetail.rawQuestionTypeKey, allowsMultipleAnswers: false)
        }
    }
    
    private func systemFormatted(from localizedString: String?) -> String? {
        guard let validString = localizedString else { return nil }
        guard let number = decimalFormatter.number(from: validString) else { return nil }
        return serviceFormatter.string(from: number)
    }
    
    private func localFormatted(from serviceString: String?, selectedUnit: Unit?) -> String? {
        guard let validString = serviceString, let selectedUnit = selectedUnit else { return nil }
        guard let number = serviceFormatter.number(from: validString) else { return nil }
        
        let shouldAcceptDecimalInput = VIAApplicableFeatures.default.shouldAcceptDecimalInput(selectedUnit, questionTypeKey: self.questionDetail.rawQuestionTypeKey, healthAttributeType: .Unknown)
        
        /* Default formatter will be Localization.decimalFormatter */
        var decimalFormatter = Localization.decimalFormatter
        /* If localized formatter is not nil, use the formatter instead. */
        if let formatter = VIAApplicableFeatures.default.decimalFormatter(shouldAcceptDecimalInput: shouldAcceptDecimalInput){
            decimalFormatter = formatter
        }
        
        return decimalFormatter.string(from: number)
    }
    
    func disableKeyboardDecimalInput(cell: VNAAssessmentNumberRangeAnswerCollectionViewCell) {
        /* Vegetables | Fruits | Wholegrains | Beverages | Tea | Milk */
        switch self.questionDetail.rawQuestionTypeKey {
        case 99, 100, 101, 108, 164, 165:
            cell.inputTextField.keyboardType = .numberPad
        default:
            break
        }
    }
}

public struct VNAAssessmentMetric: CaptureMetric {
    
    var validValues = [VNAValidValue]()
    
    public var units: [Unit] = [Unit]()
    
    public var rawUnits: [UnitOfMeasureRef] = [UnitOfMeasureRef]()
    
    public init(validValues: [VNAValidValue]) {
        self.validValues = validValues
        
        self.setRawUnits(self.validValues.flatMap({ $0.unitOfMeasure }))
        self.units = VNAAssessmentMetric.configureUnits(for: self.rawUnits)
    }
    
    public init(unitsOfMeasure: [UnitOfMeasureRef]) {
        self.setRawUnits(unitsOfMeasure)
        self.units = VNAAssessmentMetric.configureUnits(for: self.rawUnits)
    }
    
    private mutating func setRawUnits(_ unitsOfMeasure: [UnitOfMeasureRef]) {
        self.rawUnits = Array(Set(unitsOfMeasure)) // filter duplicates
    }
    
    func validate(_ input: String?,
                  _ unit: Unit?,
                  _ unitOfMeasureType: inout UnitOfMeasureRef,
                  _ min: inout Double?,
                  _ max: inout Double?) -> Bool {
        
        
        // if the input is empty, the user wants to delete the input
        guard let validString = input, !validString.isEmpty else {
            return true
        }
        
        // check that we have a number and not some random garbage
        let localizedFormatter = NumberFormatter.decimalFormatter()
        guard let validInput = localizedFormatter.number(from: validString)?.doubleValue else {
            return false
        }
        
        // if no valid values, then anything is valid!
        if self.validValues.count == 0 {
            debugPrint("No validValues, letting through any value")
            return true
        }
        
        // if we have valid values, filtr and go mad with validation
        let parameters = self.validValues.filter({ $0.unitOfMeasure.unit().symbol == (unit?.symbol ?? "") })
        
        // TODO: Temporarily disabled. What's this for?
        //        guard parameters.count >= 1 else {
        //            debugPrint("Missing parameters in valid values")
        //            return false
        //        }
        
        let serviceFormatter = NumberFormatter.serviceFormatter()
        var minResult: Bool?
        var maxResult: Bool?
        
        for parameter in parameters {
            // doesn't matter that this sits in the loop,
            // they all have the same UOM based on the filter
            // we applied above.
            //            unitOfMeasureType = parameter.unitOfMeasure
            
            if let validValue = serviceFormatter.number(from: parameter.value)?.doubleValue {
                if parameter.type == .LowerLimit {
                    //Let's check here if we are receiving Inch then manually override the lower limit to 0
                    if unitOfMeasureType == .Inch && unit == UnitOfMeasureRef.FootInch.unit(){
                        min = 0
                        minResult = validInput >= min!
                        
                        //Let's check here if we are receiving Pound then manually override the lower limit to 0
                    }else if unitOfMeasureType == .Pound && unit == UnitOfMeasureRef.StonePound.unit(){
                        min = 0
                        minResult = validInput >= min!
                    }else{
                        min = validValue
                        minResult = validInput >= min!
                    }
                } else if parameter.type == .UpperLimit {
                    //Let's check here if we are receiving Inch then manually override the upper limit to 11
                    if unitOfMeasureType == .Inch && unit == UnitOfMeasureRef.FootInch.unit(){
                        max = 11
                        maxResult = validInput <= max!
                        
                        //Let's check here if we are receiving Pound then manually override the upper limit to 13
                    }else if unitOfMeasureType == .Pound && unit == UnitOfMeasureRef.StonePound.unit(){
                        max = 13
                        maxResult = validInput <= max!
                    }else{
                        max = validValue
                        maxResult = validInput <= max!
                    }
                } else {
                    debugPrint("No value to validate against, letting through any value")
                }
            } else {
                debugPrint("No value to validate against, letting through any value")
            }
        }
        
        // some metric's valid values only have a lower or only an upper
        // limit. we track the validation thus with optionals in so that
        // in the case where there's only 1 limit,
        // we default the other to true
        return (minResult ?? true) && (maxResult ?? true)
    }
}

class VNAAssessmentNumberRangeAnswerCollectionViewCell: UICollectionViewCell, Nibloadable, UITextFieldDelegate {
    
    // MARK: Outlets
    
    @IBOutlet weak var userInputStackView: UIStackView!
    @IBOutlet weak var inputTextField: UITextField!
    @IBOutlet weak var secondInputField: UITextField!
    @IBOutlet weak var validationErrorLabel: UILabel!
    @IBOutlet weak var secondValidationErrorLabel: UILabel!
    @IBOutlet weak var unitsButton: UIButton!
    @IBOutlet weak var unitButton: UIButton!
    @IBOutlet weak var mainStackView: UIStackView!
    @IBOutlet weak var unitsArrowImageView: UIImageView!
    @IBOutlet weak var inputFieldsStackView: UIStackView!
    @IBOutlet weak var secondUserInputStackView: UIStackView!
    
    // MARK: Properties
    
    var min: Double?
    var max: Double?
    var questionRawTypeKey: Int = 0
    
    var selectedUnit: Unit?{
        didSet{
            if let currentSelected = selectedUnit{
                debugPrint(measurementFormatter.string(from: currentSelected))
            }
        }
    }
    
    weak var captureResultsDelegate: VNAAssessmentCaptureResultsDelegate?
    
    lazy var measurementFormatter: MeasurementFormatter = {
        return Localization.decimalShortStyle
    }()
    
    lazy var decimalFormatter: NumberFormatter = {
        return Localization.decimalFormatter
    }()
    
    public var unitsButtonTitle: String? {
        set {
            unitsButton.setTitle(newValue, for: .normal)
        }
        get {
            return unitsButton.titleLabel?.text
        }
    }
    
    let footInchUnit = Localization.decimalShortStyle.string(from: (UnitOfMeasureRef.FootInch.unit()))
    let stonePoundUnit = Localization.decimalShortStyle.string(from: (UnitOfMeasureRef.StonePound.unit()))
    
    // MARK: Closures
    
    public var textFieldTextDidChange: ((_ textField: UITextField, _ unit: Unit?, _ min: inout Double?, _ max: inout Double?) -> Void)?
    
    public var secondTextFieldTextDidChange: ((_ textField: UITextField, _ unit: Unit?, _ min: inout Double?, _ max: inout Double?) -> Void)?
    
    public var textFieldDidEndEditing: ((_ textField: UITextField, _ unit: Unit?, _ min: inout Double?, _ max: inout Double?) -> Void)?
    
    public var secondTextFieldDidEndEditing: ((_ textField: UITextField, _ unit: Unit?, _ min: inout Double?, _ max: inout Double?) -> Void)?
    
    public var unitsOfMeasure = [UnitOfMeasureRef]()
    
    // MARK: Lifecycle
    
    override public func awakeFromNib() {
        super.awakeFromNib()
        
        setupInputTextField()
        setupCell()
    }
    
    public override func prepareForReuse() {
        super.prepareForReuse()
        
        setupCell()
    }
    
    func setupCell() {
        unitsButton.setTitle(nil, for: .normal)
        unitsButton.titleLabel?.font = .bodyFont()
        unitsButton.setTitleColor(.mediumGrey(), for: .normal)
        let gesture = UITapGestureRecognizer(target: self, action: #selector(unitsButtonTapped(_:)))
        unitsArrowImageView.gestureRecognizers?.forEach({ unitsArrowImageView.removeGestureRecognizer($0) })
        unitsArrowImageView.addGestureRecognizer(gesture)
        
        validationErrorLabel.isHidden = true
        validationErrorLabel.font = .footnoteFont()
        validationErrorLabel.textColor = UIColor.cellErrorLabel()
        
        secondValidationErrorLabel.isHidden = true
        secondValidationErrorLabel.font = .footnoteFont()
        secondValidationErrorLabel.textColor = UIColor.cellErrorLabel()
        
        min = nil
        max = nil
        inputTextField.text = nil
        secondInputField.text = nil
        setValidationErrorText(nil, for: validationErrorLabel)
        setValidationErrorText(nil, for: secondValidationErrorLabel)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        mainStackView.superview?.layer.removeAllBorderLayers()
        mainStackView.superview?.layer.addBorder(edge: .bottom)
        userInputStackView.layer.removeAllBorderLayers()
        userInputStackView.layer.addBorder(edge: .bottom)
        unitsArrowImageView.layer.removeAllBorderLayers()
        unitsArrowImageView.layer.addBorder(edge: .right)
    }
    
    // MARK: - Units button setup
    
    @IBAction func unitsButtonTapped(_ sender: Any) {
        resignFirstResponder()
        showUnitsActionSheet()
    }
    
    // MARK:
    
    func stopEditing(textField: UITextField) {
        textField.endEditing(true)
    }
    
    public func configureCaptureResultCell(units: [UnitOfMeasureRef], inputPlaceholder: String?, inputText: String?, inputUnitOfMeasure: UnitOfMeasureRef?) {
        // units picker button
        if units.count > 1 {
            unitsArrowImageView.image = UIImage(asset: .arrowDrill)
            unitsButton.tintColor = .lightGrey()
        } else {
            unitsArrowImageView.image = nil
        }
        
        var stringUoM = ""
        
        // select units
        unitsOfMeasure = units
        
        //If there is no current selected UoM, fallback to the user input UoM
        if stringUoM.isEmpty{
            if let inputUnit = inputUnitOfMeasure?.unit(){
                self.selectedUnit   = inputUnit
                stringUoM           = measurementFormatter.string(from: inputUnit)
            }
        }
        
        //If there is no user input UoM, fallback to the first item in the options
        if stringUoM.isEmpty{
            if let availableUnit = unitsOfMeasure.first?.unit(){
                self.selectedUnit   = availableUnit
                stringUoM           = measurementFormatter.string(from: availableUnit)
            }
        }
        
        unitsButtonTitle = stringUoM
        
        self.setUnitsOfMeasureViewVisibility(hidden: self.selectedUnit?.symbol == "")
        
        // other
        
        /**
         *ge20180410 : VACR130 : Remove guidance texts
         */
        if(VIAApplicableFeatures.default.removeAssessmentGuidanceText)!{
            inputTextField.placeholder = nil
        }else{
            inputTextField.placeholder = inputPlaceholder
        }
        
        inputTextField.text = inputText
        var values = [String]()
        if let uom = inputUnitOfMeasure{
            if uom == .FootInch || uom == .StonePound{
                if uom == .FootInch{
                    values = getValueFromAnswer(toSearch: inputText ?? "", uom1: "FT", uom2: "INCH")
                }else if uom == .StonePound{
                    values = getValueFromAnswer(toSearch: inputText ?? "", uom1: "ST", uom2: "LB")
                }
                
                inputTextField.text     = nil
                secondInputField.text   = nil
                if values.count > 0{
                    inputTextField.text     = values[0]
                }
                if values.count > 1{
                    secondInputField.text   = values[1]
                }
            }
        }
        
        if let selectedUnitSymbol = self.selectedUnit?.symbol,
            (selectedUnitSymbol == self.footInchUnit || selectedUnitSymbol == self.stonePoundUnit) {
            
            /**
             *ge20180410 : VACR130 : Remove guidance texts
             */
            if(!VIAApplicableFeatures.default.removeAssessmentGuidanceText!){
                let unitVal = stringUoM.characters.split{$0 == " "}.map(String.init)
                inputTextField.placeholder = unitVal[0]
                if unitVal.count > 1{
                    self.secondInputField.placeholder = unitVal[1]
                }
            }
        }
    }
    
    public func getValueFromAnswer(toSearch: String, uom1: String, uom2: String) -> [String]{
        
        var value = [String]()
        let pattern = "(\\d+(?:\\.\\d+)?)"
        let regex = try! NSRegularExpression(pattern: pattern, options: [])
        let nsString = toSearch as NSString
        let results = regex.matches(in: toSearch, range: NSRange(location: 0, length: nsString.length))
        
        //ge20180210 : Found that IN was moved to FT in FE when there is no FT.
        //           : Left FT empty when only provided IN
        //           : Todo: Clarify expected logic
        let beginningRegEx = try! NSRegularExpression(pattern: "(^\\d+)", options: [])
        let findFt = beginningRegEx.matches(in: toSearch, range: NSRange(location: 0, length: nsString.length)).map { nsString.substring(with: $0.range)}
        if (findFt.isEmpty){
            value.append("")
            return value + results.map { nsString.substring(with: $0.range)}
        }else{
            return results.map { nsString.substring(with: $0.range)}
        }
    }
    
    func setUnitsOfMeasureViewVisibility(hidden: Bool) {
        unitsButton.isHidden = hidden
        unitsArrowImageView.isHidden = hidden
        setNeedsUpdateConstraints()
        setNeedsLayout()
    }
    
    // MARK: Actions
    
    func setValidationErrorText(_ text: String?, for label: UILabel) {
        label.text = text
        label.isHidden = text == nil
    }
    
    // MARK: - Units
    
    func showUnitsActionSheet() {
        guard unitsOfMeasure.count > 1 else { return }
        
        let unitsMenu = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        unitsMenu.popoverPresentationController?.sourceView = self
        
        for rawUnitOfMeasure in unitsOfMeasure {
            let unit = rawUnitOfMeasure.unit()
            let title = measurementFormatter.string(from: unit)
            let action = UIAlertAction(title: title, style: .default, handler: { [weak self] (alert: UIAlertAction!) -> Void in
                
                self?.onSelectUoM(unit: unit, title: title, rawUnitOfMeasure: rawUnitOfMeasure)
                self?.captureResultsDelegate?.didSelectUnitOfMeasure(rawUnitOfMeasure)
            })
            unitsMenu.addAction(action)
        }
        
        let cancelAction = UIAlertAction(title: CommonStrings.CancelButtonTitle24, style: .cancel, handler: nil)
        unitsMenu.addAction(cancelAction)
        
        captureResultsDelegate?.show(menu: unitsMenu)
    }
    
    fileprivate func onSelectUoM(unit: Unit, title: String, rawUnitOfMeasure: UnitOfMeasureRef){
        if self.selectedUnit != rawUnitOfMeasure.unit(){
            self.inputTextField.text = nil
            self.secondInputField.text = nil
        }
        
        self.selectedUnit = unit
        self.unitsButtonTitle = title
        self.inputTextField.text = nil
        self.secondInputField.text = nil
        self.setValidationErrorText(nil, for: self.validationErrorLabel)
        self.setValidationErrorText(nil, for: self.secondValidationErrorLabel)
        
        //        self.captureResultsDelegate?.didSelectUnitOfMeasure(rawUnitOfMeasure)
    }
    
    // MARK: - Textfield
    
    func setupInputTextField() {
        self.inputTextField.delegate = self
        self.inputTextField.addTarget(self, action: #selector(textFieldEditingChanged(_:)), for: .editingChanged)
        self.inputTextField.keyboardType = .decimalPad
        self.inputTextField.placeholder = nil
        self.inputTextField.font = UIFont.bodyFont()
        
        self.secondInputField.delegate = self
        self.secondInputField.addTarget(self, action: #selector(secondTextFieldTextDidChange(_:)), for: .editingChanged)
        self.secondInputField.keyboardType = .decimalPad
        self.secondInputField.placeholder = nil
        self.secondInputField.font = UIFont.bodyFont()
        
        AppSettings.setHasUpdatedNumberRangeInSection(false)
    }
    
    public func textFieldDidBeginEditing(_ textField: UITextField) {
    }
    
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

        guard let selectedUnit = selectedUnit else { return false }

        if !(VIAApplicableFeatures.default.shouldAcceptDecimalInput(selectedUnit, questionTypeKey: questionRawTypeKey, healthAttributeType: .Unknown)) {
            let validInputs = NSCharacterSet(charactersIn:"0123456789").inverted
            let componentsToSeparate = string.components(separatedBy: validInputs)
            let numberFiltered = componentsToSeparate.joined(separator: "")
            return string == numberFiltered
        }
        
        if textField == inputTextField && self.selectedUnit == UnitOfMeasureRef.FootInch.unit(){
            return string != "."
        }
        return true
    }
    
    public func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == inputTextField{
            guard let action = self.textFieldDidEndEditing else { return }
            action(textField, selectedUnit, &min, &max)
        }else if textField == secondInputField{
            guard let action = self.secondTextFieldDidEndEditing else { return }
            action(textField, selectedUnit, &min, &max)
        }
    }
    
    func textFieldEditingChanged(_ textField: UITextField) {
        guard let action = self.textFieldTextDidChange else { return }
        action(textField, selectedUnit, &min, &max)
    }
    
    func secondTextFieldTextDidChange(_ textField: UITextField) {
        guard let action = self.secondTextFieldTextDidChange else { return }
        action(textField, selectedUnit, &min, &max)
    }
    
    
    // MARK: Validation
    
    public func updateInputValid(_ valid: Bool, _ min: Double?, _ max: Double?, _ errorLabel: UILabel) {
        if valid {
            self.inputIsValid(errorLabel: errorLabel)
        } else {
            self.inputIsNotValid(min: min, max: max, errorLabel: errorLabel)
        }
    }
    
    private func inputIsValid(errorLabel: UILabel) {
        self.setValidationErrorText(nil, for: errorLabel)
    }
    
    private func inputIsNotValid(min: Double?, max: Double?, errorLabel: UILabel) {
        var text: String = ""
        self.min = min
        self.max = max
        if let min = self.min, let max = self.max {
            if let formattedMin = decimalFormatter.string(from: min as NSNumber), let formattedMax = decimalFormatter.string(from: max as NSNumber) {
                text = CommonStrings.ErrorRange180(formattedMin, formattedMax)
            }
        } else if let min = self.min, self.max == nil {
            if let formattedMin = decimalFormatter.string(from: min as NSNumber) {
                text = CommonStrings.ErrorRangeBigger281(formattedMin)
            }
        } else if let max = self.max, self.min == nil {
            if let formattedMax = decimalFormatter.string(from: max as NSNumber) {
                text = CommonStrings.ErrorRangeSmaller282(formattedMax)
            }
        }
        self.setValidationErrorText(text, for: errorLabel)
    }
    
}
