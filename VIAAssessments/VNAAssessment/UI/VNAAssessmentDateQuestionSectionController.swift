//
//  VNAAssessmentDateQuestionSectionController.swift
//  VitalityActive
//
//  Created by Von Kervin R. Tuico on 10/02/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import IGListKit
import VIAUIKit
import VIAUtilities
import VitalityKit
import VitalityKit
import RealmSwift

public final class VNAAssessmentDateQuestionSectionController: IGListSectionController, IGListSectionType, FootnoteToggleable {
    
    // MARK: Properties
    
    var realm: Realm
    
    var questionDetail: VNAAssessmentQuestionPresentationDetail!
    
    var footnoteDetail: AttributedTextDetail? {
        if self.questionDetail.textNote != nil {
            return self.questionDetail.noteDetail()
        }
        return nil
    }
    
    var showDatePicker: Bool = false
    
    var dateCell: VNAAssessmentDateCollectionViewCell?
    
    var dateFormatter: DateFormatter {
        return Localization.dayMonthAndYearFormatter
    }
    
    var answerValueFormatter: DateFormatter {
        return Localization.yearMonthDayFormatter
    }
    
    var capturedResult: VNACapturedResult? {
        var result = self.questionDetail.rawQuestion(in: self.realm)?.capturedResult()
        if result == nil {
            self.updateCapturedResult(date: Date())
            result = self.questionDetail.rawQuestion(in: self.realm)?.capturedResult()
        }
        return result
    }
    
    var capturedResultDate: Date {
        var date: Date?
        if let dateString = self.capturedResult?.answer {
            date = answerValueFormatter.date(from: dateString)
        }
        return date ?? Date() // should never come to this
    }
    
    // MARK: FootnoteToggleable
    
    var isFootnoteExpanded: Bool = false
    
    var indexForFootnoteCell: Int? {
        return showDatePicker ? 3 : 2
    }
    
    // MARK: Lifecycle
    
    public required init(realm: Realm) {
        self.realm = realm
    }
    
    // MARK: IGListSectionType
    
    public func numberOfItems() -> Int {
        var count = 2
        if self.showDatePicker {
            count = count + 1
        }
        if self.footnoteDetail != nil {
            count = count + 1
        }
        return count
    }
    
    public func didUpdate(to object: Any) {
        self.questionDetail = object as! VNAAssessmentQuestionPresentationDetail
    }
    
    public func cellForItem(at index: Int) -> UICollectionViewCell {
        if index == 0 {
            let attributedTextDetail = self.questionDetail.textAndDescriptionDetail()
            return attributedTextCell(for: attributedTextDetail, at: index)
        } else if index == 1 {
            return dateCell(at: index)
        } else if index == 2 && self.showDatePicker {
            return datePickerCell(at: index)
        } else if (index == 2 && !self.showDatePicker) || index == 3, let noteDetail = self.footnoteDetail {
            return footnoteAttributedTextCell(for: noteDetail, at: index)
        }
        return collectionContext!.dequeueReusableCell(of: UICollectionViewCell.self, for: self, at: index)
    }
    
    public func sizeForItem(at index: Int) -> CGSize {
        let width = collectionContext!.containerSize.width
        if index == 0 {
            let attributedTextDetail = self.questionDetail.textAndDescriptionDetail()
            return VNAAssessmentTextCollectionViewCell.height(with: attributedTextDetail, constrainedTo: width)
        } else if index == 1 {
            return CGSize(width: width, height: VNAAssessmentDateCollectionViewCell.height)
        } else if index == 2 && self.showDatePicker {
            return CGSize(width: width, height: 200)
        } else if (index == 2 && !self.showDatePicker) || index == 3, let noteDetail = self.footnoteDetail {
            return VNAAssessmentFootnoteCollectionViewCell.height(with: noteDetail, constrainedTo: width, isFootnoteExpanded: self.isFootnoteExpanded)
        }
        return CGSize.zero
    }
    
    public func didSelectItem(at index: Int) {
        // tap on footnote
        if (index == 2 && !self.showDatePicker) || index == 3 {
            toggleFootnote()
            return
        }
        
        if index == 1 {
            togglePicker()
        }
    }
    
    // MARK: Cells
    
    func attributedTextCell(for textDetail: AttributedTextDetail, at index: Int) -> UICollectionViewCell {
        let cell = collectionContext!.dequeueReusableCell(of: VNAAssessmentTextCollectionViewCell.self, for: self, at: index) as! VNAAssessmentTextCollectionViewCell
        cell.label.attributedText = textDetail.attributedString
        cell.addTopBorder = true
        cell.addBottomBorder = true
        return cell
    }
    
    func footnoteAttributedTextCell(for textDetail: AttributedTextDetail, at index: Int) -> UICollectionViewCell {
        let cell = collectionContext!.dequeueReusableCell(of: VNAAssessmentFootnoteCollectionViewCell.self, for: self, at: index) as! VNAAssessmentFootnoteCollectionViewCell
        cell.setLabelAttributedText(textDetail.attributedString, isFootnoteExpanded: self.isFootnoteExpanded)
        return cell
    }
    
    func dateCell(at index: Int) -> UICollectionViewCell {
        let cell = collectionContext!.dequeueReusableCell(of: AssessmentDateCollectionViewCell.self, for: self, at: index) as! VNAAssessmentDateCollectionViewCell
        cell.leftLabel.text = CommonStrings.DateTitle264
        cell.rightLabel.text = self.dateFormatter.string(from: self.capturedResultDate)
        self.dateCell = cell
        cell.addBottomBorder = !self.showDatePicker
        return cell
    }
    
    func datePickerCell(at index: Int) -> VIADatePickerCell {
        let cell = collectionContext?.dequeueReusableCell(withNibName: VIADatePickerCell.defaultReuseIdentifier, bundle: VIADatePickerCell.bundle(), for: self, at: index) as! VIADatePickerCell
        cell.didPickDate = self.datePickerDidPick
        cell.selectedDate = self.capturedResultDate
        cell.addBottomBorder = self.showDatePicker
        return cell
    }
    
    // MARK: Date Picker
    
    public func togglePicker() {
        self.showDatePicker = !self.showDatePicker
        self.collectionContext?.performBatch(animated: true, updates: {
            // reload date cell to get rid of bottom border line
            self.collectionContext?.reload(in: self, at: IndexSet(integer: 1))
            // add/remove picker
            if self.showDatePicker {
                self.collectionContext?.insert(in: self, at: IndexSet(integer: 2))
            } else {
                self.collectionContext?.delete(in: self, at: IndexSet(integer: 2))
            }
        }, completion: { completed in
            //
        })
    }
    
    // MARK: DatePicker
    
    func datePickerDidPick(date: Date) {
        self.dateCell?.rightLabel.text = dateFormatter.string(from: date)
        updateCapturedResult(date: date)
    }
    
    // MARK: Capture result
    
    func updateCapturedResult(date: Date) {
        guard let type = self.questionDetail.rawQuestion(in: realm)?.questionType else { return }
        
        let answerValue = answerValueFormatter.string(from: date)
        
        if let controller = self.viewController as? VNAAssessmentQuestionnaireSectionViewController {
            controller.answeringDelegate?.persist(answerValue,
                                                  unit: nil,
                                                  for: questionDetail.rawQuestionTypeKey,
                                                  of: type,
                                                  allowsMultipleAnswers: false,
                                                  answerIsValid: true)
        }
    }
    
}


public final class VNAAssessmentDateCollectionViewCell: UICollectionViewCell, Nibloadable {
    
    fileprivate static let inset: CGFloat = 15.0
    
    fileprivate static let height: CGFloat = 52.0
    
    public var addSeperator = false
    
    public var addTopBorder = false
    
    public var addBottomBorder = false
    
    // MARK: Views
    
    public lazy var leftLabel: UILabel = {
        let label = UILabel(frame: .zero)
        label.font = UIFont.bodyFont()
        label.textColor = UIColor.night()
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    public lazy var rightLabel: UILabel = {
        let label = UILabel(frame: .zero)
        label.font = UIFont.bodyFont()
        label.textColor = UIColor.night()
        label.textAlignment = .right
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    // MARK: Init
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupSubviews()
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        
        contentView.layer.removeAllBorderLayers()
        if addTopBorder {
            contentView.layer.addBorder(edge: .top)
        }
        if addBottomBorder {
            contentView.layer.addBorder(edge: .bottom)
        }
        if addSeperator {
            contentView.layer.addBottomBorder(inset: VNAAssessmentDateCollectionViewCell.inset)
        }
    }
    
    private func setupSubviews() {
        contentView.backgroundColor = .day()
        
        let stackView = UIStackView()
        contentView.addSubview(stackView)
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .horizontal
        stackView.distribution = .fillEqually
        stackView.addArrangedSubview(self.leftLabel)
        stackView.addArrangedSubview(self.rightLabel)
        
        stackView.snp.makeConstraints { (make) in
            make.top.bottom.equalToSuperview().inset(VNAAssessmentDateCollectionViewCell.inset)
            make.left.right.equalToSuperview().inset(VNAAssessmentDateCollectionViewCell.inset)
            make.height.greaterThanOrEqualTo(22)
            make.height.lessThanOrEqualTo(300)
        }
    }
    
    override public func preferredLayoutAttributesFitting(_ layoutAttributes: UICollectionViewLayoutAttributes) -> UICollectionViewLayoutAttributes {
        return self.defaultContentViewLayoutSizeFitting(layoutAttributes)
    }
    
}

