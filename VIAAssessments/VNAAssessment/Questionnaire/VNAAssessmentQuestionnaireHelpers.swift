import IGListKit
import VIAUtilities
import VIAUIKit
import VitalityKit
import VitalityKit
import RealmSwift
import TTTAttributedLabel

public final class VNAAssessmentQuestionnaireSectionNavigationDetail: NSObject, IGListDiffable {
    public var isLastSection: Bool = false
    public var isEnabled: Bool = false
    public var uuidString: String = UUID().uuidString
    
    public init(isLastSection: Bool = false, isEnabled: Bool = false) {
        self.isLastSection = isLastSection
        self.isEnabled = isEnabled
    }
    
    public func diffIdentifier() -> NSObjectProtocol {
        return uuidString as NSObjectProtocol
    }
    
    public func isEqual(toDiffableObject object: IGListDiffable?) -> Bool {
        guard self !== object else { return true }
        guard let object = object as? VNAAssessmentQuestionnaireSectionNavigationDetail else { return false }
        return uuidString == object.uuidString
    }
}

public final class VNAAssessmentQuestionnaireSectionHeaderFooterDetail: NSObject, IGListDiffable {
    public var text: TextDetail
    public var textDescription: TextDetail?
    public var textNote: TextDetail?
    public var uuidString = UUID().uuidString
    
    public init(text: TextDetail, textDescription: TextDetail? = nil, textNote: TextDetail? = nil) {
        self.text = text
        self.textDescription = textDescription
        self.textNote = textNote
    }
    
    public func diffIdentifier() -> NSObjectProtocol {
        return self.uuidString as NSObjectProtocol
    }
    
    public func isEqual(toDiffableObject object: IGListDiffable?) -> Bool {
        guard self !== object else { return true }
        guard let object = object as? VNAAssessmentQuestionnaireSectionHeaderFooterDetail else { return false }
        return uuidString == object.uuidString
    }
}

public final class VNAAssessmentValidValuePresentationDetail: NSObject, IGListDiffable {
    public var text: TextDetail
    public var textDescription: TextDetail?
    public var textNote: TextDetail?
    public var rawValidValue: VNAValidValue
    public var uuidString = UUID().uuidString
    
    public init(text: TextDetail, textDescription: TextDetail? = nil, textNote: TextDetail? = nil, rawValidValue: VNAValidValue) {
        self.text = text
        self.textDescription = textDescription
        self.textNote = textNote
        self.rawValidValue = rawValidValue
    }
    
    public func diffIdentifier() -> NSObjectProtocol {
        return self.uuidString as NSObjectProtocol
    }
    
    public func isEqual(toDiffableObject object: IGListDiffable?) -> Bool {
        guard self !== object else { return true }
        guard let object = object as? VNAAssessmentValidValuePresentationDetail else { return false }
        return self.uuidString == object.uuidString
    }
    
    static private func == (lhs: VNAAssessmentValidValuePresentationDetail, rhs: VNAAssessmentValidValuePresentationDetail) -> Bool {
        return lhs.uuidString == rhs.uuidString
    }
}

public final class VNAAssessmentQuestionPresentationDetail: NSObject, IGListDiffable {
    public var text: TextDetail
    public var textDescription: TextDetail?
    public var presentationType: QuestionTypeRef
    public var decorator: QuestionDecoratorRef
    public var validValuesPresentationDetail: [VNAAssessmentValidValuePresentationDetail]?
    public var isVisible: Bool
    public var rawQuestionTypeKey: Int
    
    public init(text: TextDetail, textDescription: TextDetail? = nil, presentationType: QuestionTypeRef, decorator: QuestionDecoratorRef, validValues: [VNAAssessmentValidValuePresentationDetail]? = nil, isVisible: Bool, questionTypeKey: Int) {
        self.text = text
        self.textDescription = textDescription
        self.presentationType = presentationType
        self.decorator = decorator
        self.validValuesPresentationDetail = validValues
        self.isVisible = isVisible
        self.rawQuestionTypeKey = questionTypeKey
    }
    
    public func diffIdentifier() -> NSObjectProtocol {
        return self.text as NSObjectProtocol
    }
    
    public func isEqual(toDiffableObject object: IGListDiffable?) -> Bool {
        guard self !== object else { return true }
        guard let object = object as? VNAAssessmentQuestionPresentationDetail else { return false }
        return text.text == object.text.text
    }
    
    public func rawQuestion(in realm: Realm) -> VNAQuestion? {
        return realm.vnaQuestion(by: self.rawQuestionTypeKey)
    }
    
    public func answerDetail(for answer: String) -> VNAAssessmentValidValuePresentationDetail? {
        return self.validValuesPresentationDetail?.filter({ $0.rawValidValue.name == answer }).first
    }
}

public final class VNAAssessmentQuestionFootnotePresentationDetail: NSObject, IGListDiffable {
    public var text: TextDetail
    
    public init(text: TextDetail) {
        self.text = text
    }
    
    public func diffIdentifier() -> NSObjectProtocol {
        return text as NSObjectProtocol
    }
    
    public func isEqual(toDiffableObject object: IGListDiffable?) -> Bool {
        guard self !== object else { return true }
        guard let object = object as? VNAAssessmentQuestionFootnotePresentationDetail else { return false }
        return text.text == object.text.text
    }
}

public final class VNAAssessmentQuestionPrepopulationPresentationDetail: NSObject, IGListDiffable {
    public var text: TextDetail
    
    public init(text: TextDetail) {
        self.text = text
    }
    
    public func diffIdentifier() -> NSObjectProtocol {
        return text as NSObjectProtocol
    }
    
    public func isEqual(toDiffableObject object: IGListDiffable?) -> Bool {
        guard self !== object else { return true }
        guard let object = object as? VNAAssessmentQuestionPrepopulationPresentationDetail else { return false }
        return text.text == object.text.text
    }
}

protocol VNAAttributedTextDetailPresentable {
    var text: TextDetail { get }
    var textDescription: TextDetail? { get }
    var textNote: TextDetail? { get }
    func textAndDescriptionDetail() -> AttributedTextDetail
    func textAndDescriptionAndNoteDetail() -> AttributedTextDetail
}

extension VNAAttributedTextDetailPresentable {
    var textDescription: TextDetail? { return nil }
    var textNote: TextDetail? { return nil }
}

extension VNAAttributedTextDetailPresentable {
    
    public func textDetail() -> AttributedTextDetail {
        let string = "\(self.text.text!)"
        return attributedTextDetail(for: string)
    }
    
    public func textAndDescriptionDetail() -> AttributedTextDetail {
        var string = "\(self.text.text!)"
        if let textDescription = self.textDescription?.text {
            string = string + "\n\(textDescription)"
        }
        return attributedTextDetail(for: string)
    }
    
    public func textAndDescriptionAndNoteDetail() -> AttributedTextDetail {
        var string = "\(self.text.text!)"
        if let textDescription = self.textDescription?.text {
            string = string + "\n\(textDescription)"
        }
        if let textNote = self.textNote?.text {
            string = string + "\n\(textNote)"
        }
        return attributedTextDetail(for: string)
    }
    
    public func noteDetail() -> AttributedTextDetail {
        var string = ""
        if let textNote = self.textNote?.text {
            string = textNote
        }
        return attributedTextDetail(for: string, type: .footnote)
    }
    
    private func attributedTextDetail(for string: String, type: AttributedTextDetail.LayoutType = .normal) -> AttributedTextDetail {
        let attributedString = NSMutableAttributedString(string: string)
        
        if (self.text.text == self.textDescription?.text) {
            AttributedTextDetail.addAttributesIfNeeded(to: attributedString, from: self.text)
            AttributedTextDetail.addAttributesToSameTextDescription(to: attributedString, from: self.textDescription)
            AttributedTextDetail.addAttributesIfNeeded(to: attributedString, from: self.textNote)
            return AttributedTextDetail(attributedString: attributedString, type: type)
        } else {
            AttributedTextDetail.addAttributesIfNeeded(to: attributedString, from: self.text)
            AttributedTextDetail.addAttributesIfNeeded(to: attributedString, from: self.textDescription)
            AttributedTextDetail.addAttributesIfNeeded(to: attributedString, from: self.textNote)
            return AttributedTextDetail(attributedString: attributedString, type: type)
        }
    }
    
}

extension VNAAssessmentQuestionnaireSectionHeaderFooterDetail: VNAAttributedTextDetailPresentable { }
extension VNAAssessmentQuestionPresentationDetail: VNAAttributedTextDetailPresentable { }
extension VNAAssessmentValidValuePresentationDetail: VNAAttributedTextDetailPresentable { }
extension VNAAssessmentQuestionFootnotePresentationDetail: VNAAttributedTextDetailPresentable { }
extension VNAAssessmentQuestionPrepopulationPresentationDetail: VNAAttributedTextDetailPresentable { }


public final class VNAAssessmentTextCollectionViewCell: UICollectionViewCell, Nibloadable {
    
    public static let inset: CGFloat = 15.0
    
    public var addTopBorder = false
    
    public var addBottomBorder = false
    
    // MARK: Views
    
    public lazy var label: UILabel = {
        let label = UILabel(frame: .zero)
        label.font = UIFont.title2Font()
        label.textColor = UIColor.night()
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    // MARK: Init
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupSubviews()
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        
        contentView.layer.removeAllBorderLayers()
        if addTopBorder {
            contentView.layer.addBorder(edge: .top)
        }
        if addBottomBorder {
            contentView.layer.addBorder(edge: .bottom)
        }
    }
    
    private func setupSubviews() {
        contentView.backgroundColor = .day()
        contentView.addSubview(self.label)
        self.label.snp.makeConstraints { (make) in
            make.top.bottom.equalToSuperview().inset(VNAAssessmentTextCollectionViewCell.inset)
            make.left.right.equalToSuperview().inset(VNAAssessmentTextCollectionViewCell.inset)
        }
    }
    
    override public func preferredLayoutAttributesFitting(_ layoutAttributes: UICollectionViewLayoutAttributes) -> UICollectionViewLayoutAttributes {
        return self.defaultContentViewLayoutSizeFitting(layoutAttributes)
    }
    
    // MARK: Height calculation
    
    public class func height(with attributedTextDetail: AttributedTextDetail, constrainedTo width: CGFloat) -> CGSize {
        let inset = VNAAssessmentTextCollectionViewCell.inset
        let insets = UIEdgeInsets(top: 0, left: inset, bottom: 0, right: inset)
        let size = TextSize.size(attributedTextDetail.attributedString, width: width, insets: insets)
        let height = size.height + inset * 2
        return CGSize(width: width, height: ceil(height))
    }
    
}

public final class VNAAssessmentFootnoteCollectionViewCell: UICollectionViewCell, Nibloadable {
    
    public static let topInset: CGFloat = 8.0
    
    public static let sideInset: CGFloat = 15.0
    
    private static let limitedNumberOfLines: Int = 2
    
    var isExpanded: Bool = false
    
    // MARK: Views
    
    private lazy var label: TTTAttributedLabel = {
        let label = TTTAttributedLabel(frame: .zero)
        label.font = UIFont.footnoteFont()
        label.textColor = UIColor.darkGrey()
        label.numberOfLines = VNAAssessmentFootnoteCollectionViewCell.limitedNumberOfLines
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    // MARK: Init
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupSubviews()
    }
    
    private func setupSubviews() {
        contentView.backgroundColor = UIColor.tableViewBackground()
        contentView.addSubview(self.label)
        self.label.snp.makeConstraints { (make) in
            make.top.equalToSuperview().inset(VNAAssessmentFootnoteCollectionViewCell.topInset)
            make.bottom.equalToSuperview()
            make.left.right.equalToSuperview().inset(VNAAssessmentFootnoteCollectionViewCell.sideInset)
        }
    }
    
    override public func preferredLayoutAttributesFitting(_ layoutAttributes: UICollectionViewLayoutAttributes) -> UICollectionViewLayoutAttributes {
        return self.defaultContentViewLayoutSizeFitting(layoutAttributes)
    }
    
    // MARK: Height calculation
    
    func setLabelAttributedText(_ attributedText: NSAttributedString?, isFootnoteExpanded: Bool) {
        if !isFootnoteExpanded {
            // Unicode Character 'HORIZONTAL ELLIPSIS' (U+2026)
            self.label.attributedTruncationToken = attributedText?.addTruncationToken(text: "\u{2026}" + CommonStrings.FootnoteMoreButton342)
            self.label.numberOfLines = VNAAssessmentFootnoteCollectionViewCell.limitedNumberOfLines
            self.label.attributedText = attributedText
        } else {
            self.label.attributedTruncationToken = nil
            self.label.numberOfLines = 0
            let combo = NSMutableAttributedString()
            if let lessString = attributedText?.addTruncationToken(text: " " + CommonStrings.FootnoteLessButton343), let attributedText = attributedText {
                combo.append(attributedText)
                combo.append(lessString)
            }
            self.label.attributedText = combo
        }
    }
    
    public class func height(with attributedTextDetail: AttributedTextDetail, constrainedTo width: CGFloat, isFootnoteExpanded: Bool) -> CGSize {
        let attributedText: NSAttributedString = attributedTextDetail.attributedString
        let topInset = VNAAssessmentFootnoteCollectionViewCell.topInset
        let sideInset = VNAAssessmentFootnoteCollectionViewCell.sideInset
        let sizeLimit = CGSize(width: width - sideInset * 2, height: CGFloat.greatestFiniteMagnitude)
        let numberOfLines = isFootnoteExpanded ? 0 : VNAAssessmentFootnoteCollectionViewCell.limitedNumberOfLines
        let size = TTTAttributedLabel.sizeThatFitsAttributedString(attributedText, withConstraints: sizeLimit, limitedToNumberOfLines: UInt(numberOfLines))
        let height = size.height + topInset
        return CGSize(width: width, height: ceil(height))
    }
    
}

fileprivate extension NSAttributedString {
    
    func addTruncationToken(text: String) -> NSAttributedString {
        let tokenString = text
        let attributedTruncationToken = NSMutableAttributedString(string: tokenString)
        let attributes = self.attributes(at: 0, effectiveRange: nil)
        
        // add current paragraphy style if present
        if let paragraphStyle = attributes[NSParagraphStyleAttributeName] as? NSMutableParagraphStyle {
            attributedTruncationToken.addAttributes([NSParagraphStyleAttributeName: paragraphStyle], range: attributedTruncationToken.string.nsRange(of: attributedTruncationToken.string))
        }
        // reapply foreground color to whole string
        if let foregroundColor = attributes[NSForegroundColorAttributeName] as? UIColor {
            attributedTruncationToken.addAttributes([NSForegroundColorAttributeName: foregroundColor], range: attributedTruncationToken.string.nsRange(of: attributedTruncationToken.string))
        }
        // set custom text color to token string
        attributedTruncationToken.addAttributes([NSForegroundColorAttributeName: UIColor.currentGlobalTintColor()], range: attributedTruncationToken.string.nsRange(of: tokenString))
        
        return attributedTruncationToken
    }
    
}

extension VNAQuestionnaireSection {
    
    func image() -> UIImage? {
        let type = QuestionnaireSectionsRef(rawValue: self.typeKey) ?? .Unknown
        switch type {
        case .DietaryPreference:
            return VIAAssessmentsAsset.SectionIcons.vnaDietaryPreferences.templateImage
        case .FoodAllergies:
            return VIAAssessmentsAsset.SectionIcons.vnaAllergiesAndIntolerances.templateImage
        case .FoodPreperation:
            return VIAAssessmentsAsset.SectionIcons.vnaFoodPrep.templateImage
        case .EatHabits:
            return VIAAssessmentsAsset.SectionIcons.vnaEatingHabits.templateImage
        case .MealPattern:
            return VIAAssessmentsAsset.SectionIcons.vnaMealpattern.templateImage
        case .Snacks:
            return VIAAssessmentsAsset.SectionIcons.vnaSnacks.templateImage
        case .Micronutrients:
            return VIAAssessmentsAsset.SectionIcons.vnaMicronutrientsAndFibre.templateImage
        case .Sodium:
            return VIAAssessmentsAsset.SectionIcons.vnaSodium.templateImage
        case .Sugar:
            return VIAAssessmentsAsset.SectionIcons.vnaSugar.templateImage
        case .CalciumPotassium:
            return VIAAssessmentsAsset.SectionIcons.vnaCalciumAndPotassium.templateImage
        case .SFA:
            return VIAAssessmentsAsset.SectionIcons.vnaSaturatedFattyAcids.templateImage
        case .Meats:
            return VIAAssessmentsAsset.SectionIcons.vnaMeats.templateImage
        case .Fats:
            return VIAAssessmentsAsset.SectionIcons.vnaFats.templateImage
        case .VNAMedicalHistory:
            return VIAAssessmentsAsset.SectionIcons.vhrSectionMedicalHistory.templateImage
        default:
            return nil
        }
    }
}

