import Foundation
import IGListKit
import VitalityKit
import VitalityKit
import VIAUIKit
import RealmSwift

public protocol VNAAssessmentQuestionnaireSectionViewControllerDelegate: class {
    
    func sectionControllerDidSelectNext(_ vnaQuestionnaireSectionViewController: VNAAssessmentQuestionnaireSectionViewController)
    
    func sectionControllerDidSelectBack(_ vnaQuestionnaireSectionViewController: VNAAssessmentQuestionnaireSectionViewController)
    
    func sectionControllerDidSelectClose(_ vnaQuestionnaireSectionViewController: VNAAssessmentQuestionnaireSectionViewController)
    
    func sectionControllerDidSelectDone(_ vnaQuestionnaireSectionViewController: VNAAssessmentQuestionnaireSectionViewController)
    
}

public final class VNAAssessmentQuestionnaireSectionViewController: VIAViewController, IGListAdapterDataSource, VNAAssessmentQuestionnaireSectionViewModelDelegate {
    
    // MARK: Properties
    
    weak var navigationDelegate: VNAAssessmentQuestionnaireSectionViewControllerDelegate?
    
    weak var answeringDelegate: VNAAssessmentQuestionnaireAnsweringDelegate?
    
    private (set) var isFirstSection: Bool = false
    
    private (set) var isLastSection: Bool = false
    
    private (set) var questionnaireSectionTypeKey: Int?
    
    private  var _viewModel: VNAAssessmentQuestionnaireSectionViewModel?
    private (set) var viewModel: VNAAssessmentQuestionnaireSectionViewModel? {
        set {
            self._viewModel = newValue
        }
        get {
            if _viewModel == nil {
                let viewModel = VNAAssessmentQuestionnaireSectionViewModel(questionnaireSectionTypeKey: self.questionnaireSectionTypeKey,
                                                                           isLastSection: self.isLastSection)
                viewModel?.delegate = self
                self._viewModel = viewModel
            }
            return _viewModel
        }
    }
    
    let collectionView: IGListCollectionView = {
        let collectionView = IGListCollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
        collectionView.backgroundColor = UIColor.tableViewBackground()
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        return collectionView
    }()
    
    lazy var adapter: IGListAdapter = {
        return IGListAdapter(updater: IGListAdapterUpdater(), viewController: self, workingRangeSize: 0)
    }()
    
    // MARK: View lifecycle
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        
        configureAppearance()
        configureCollectionView()
        configureAdapter()
        addBarButtonItems()
        
        addNotificationWithCompletion { [weak self] (notification) in
            guard let strongSelf = self else { return }
            strongSelf.navigationDelegate?.sectionControllerDidSelectClose(strongSelf)
        }
    }
    
    func configure(isFirstSection: Bool, isLastSection: Bool, questionnaireSectionTypeKey: Int) {
        self.isFirstSection = isFirstSection
        self.isLastSection = isLastSection
        self.questionnaireSectionTypeKey = questionnaireSectionTypeKey
    }
    
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    func tearDownViewModel() {
        viewModel = nil
    }
    
    public override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        tearDownViewModel()
    }
    
    deinit {
        debugPrint("VNAAssessmentQuestionnaireSectionViewController deinit")
    }
    
    // MARK: View config
    
    func configureAppearance() {
        navigationController?.setNavigationBarHidden(false, animated: false)
        navigationController?.makeNavigationBarTransparent()
        navigationController?.navigationBar.barTintColor = .white
    }
    
    func configureCollectionView() {
        view.addSubview(collectionView)
        collectionView.snp.makeConstraints { (make) in
            //Xcode 9
            if #available(iOS 11, *) {
                let window = UIApplication.shared.keyWindow
                let topPadding = window?.safeAreaInsets.top
                make.top.equalTo(150 + topPadding!)
                make.left.bottom.right.equalToSuperview()
            } else {
                make.top.left.bottom.right.equalToSuperview()
            }
            //Xcode 9
            //make.top.left.bottom.right.equalToSuperview()
        }
    }
    
    func configureAdapter() {
        adapter.collectionView = collectionView
        adapter.dataSource = self
    }
    
    func addBarButtonItems() {
        if !self.isFirstSection {
            self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage.templateImage(asset: ASSAsset.vhrBackArrow), style: .plain, target: self, action: #selector(back(_:)))
        }
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: CommonStrings.GenericCloseButtonTitle10, style: .plain, target: self, action: #selector(close(_:)))
        
    }
    
    // MARK: - Navigation
    
    func close(_ sender: Any?) {
        collectionView.endEditing(true)
        navigationDelegate?.sectionControllerDidSelectClose(self)
    }
    
    func next(_ sender: Any?) {
        navigationDelegate?.sectionControllerDidSelectNext(self)
    }
    
    func back(_ sender: Any?) {
        navigationDelegate?.sectionControllerDidSelectBack(self)
    }
    
    func done(_ sender: Any?) {
        navigationDelegate?.sectionControllerDidSelectDone(self)
    }
    
    // MARK: IGListAdapterSource
    
    public func objects(for listAdapter: IGListAdapter) -> [IGListDiffable] {
        if let data = viewModel?.data {
            return data
        }
        return [] as! [IGListDiffable]
    }
    
    public func listAdapter(_ listAdapter: IGListAdapter, sectionControllerFor object: Any) -> IGListSectionController {
        if let detail = object as? VNAAssessmentQuestionPresentationDetail, let realm = viewModel?.realm {
            if detail.presentationType == .Date && detail.decorator == .DatePicker {
                return VNAAssessmentDateQuestionSectionController(realm: realm)
            } else if detail.presentationType == .DecimalRange && detail.decorator == .Textbox {
                return VNAAssessmentNumberRangeQuestionSectionController(realm: realm)
            } else if detail.presentationType == .FreeText && detail.decorator == .Textbox {
                return VNAAssessmentFreeTextSectionController(realm: realm)
            } else if detail.presentationType == .MultiSelect && detail.decorator == .Checkbox {
                return VNAAssessmentMultiSelectQuestionSectionController(realm: realm)
            } else if detail.presentationType == .NumberRange && detail.decorator == .Textbox {
                return VNAAssessmentNumberRangeQuestionSectionController(realm: realm)
            } else if detail.presentationType == .ChildNumberRange && detail.decorator == .Textbox {
                return VNAAssessmentNumberRangeSingleLineQuestionSectionController(realm: realm)
            } else if detail.presentationType == .SingleSelect && detail.decorator == .RadioButton {
                return VNAAssessmentSingleSelectQuestionSectionController(realm: realm)
            } else if detail.presentationType == .SingleSelect && detail.decorator == .Toggle {
                return VNAAssessmentToggleQuestionSectionController(realm: realm)
            } else if detail.presentationType == .SingleSelect && detail.decorator == .YesNo {
                return VNAAssessmentYesNoQuestionSectionController(realm: realm)
            } else if detail.presentationType == .Label && detail.decorator == .Label {
                return VNAAssessmentLabelQuestionSectionController(realm: realm)
            } else if detail.presentationType == .Label && detail.decorator == .CollapseUoM {
                return VNAAssessmentLabelQuestionSectionController(realm: realm)
            } else {
                debugPrint("We do not cater for this combination: \(detail.presentationType.rawValue) + \(detail.decorator.rawValue)")
                return SpacingController()
            }
        } else if object is VNAAssessmentQuestionnaireSectionHeaderFooterDetail {
            return VNAAssessmentQuestionnaireHeaderFooterSectionController()
        } else if object is VNAAssessmentQuestionPrepopulationPresentationDetail {
            return VNAAssessmentQuestionPrepopulationSectionController()
        } else if object is VNAAssessmentQuestionFootnotePresentationDetail {
            return VNAAssessmentQuestionFootnoteSectionController()
        } else if object is VNAAssessmentQuestionnaireSectionNavigationDetail {
            return VNAAssessmentNavigationButtonSectionController()
        } else if object is VNAAssessmentRequiredPrompt {
            return VNAAssessmentRequiredPromptController()
        }
        return SpacingController()
    }
    
    public func emptyView(for listAdapter: IGListAdapter) -> UIView? { return nil }
    
    // MARK: VNAQuestionnaireSectionViewModelDelegate
    
    public func viewModelDidUpdateData() {
        self.adapter.performUpdates(animated: true, completion: { finished in
            //
        })
    }
    
    public func viewModelDidUpdateCapturedResults() {
        // reload only the navigation detail, not everything.
        // if we reload everything, the date picker freaks out and the
        // number range validation disappears immediately after showing
        if let controller = self.adapter.sectionController(for: self.viewModel?.navigationDetail as Any) {
            controller.collectionContext?.performBatch(animated: true, updates: {
                controller.collectionContext?.reload(controller)
            }, completion: nil)
        }
    }
    public func scrollTo(questionDetail: VNAAssessmentQuestionPresentationDetail){
        print("--->Question Text:\(questionDetail.text.text)")
        if let details = self.adapter.objects().filter({ $0 is VNAAssessmentQuestionPresentationDetail }) as? [VNAAssessmentQuestionPresentationDetail] {
            if let filtered = details.filter({ $0.text.isEqual(toDiffableObject: questionDetail.text) }).first {
                if let controller = self.adapter.sectionController(for: filtered) {
                    controller.collectionContext?.scroll(to: controller, at: 0, scrollPosition: .centeredVertically, animated: false)
                }
            }
        }
    }
    public func reloadSectionController(for questionDetail: VNAAssessmentQuestionPresentationDetail) {
        if let details = self.adapter.objects().filter({ $0 is VNAAssessmentQuestionPresentationDetail }) as? [VNAAssessmentQuestionPresentationDetail] {
            if let filtered = details.filter({ $0.text.isEqual(toDiffableObject: questionDetail.text) }).first {
                if let controller = self.adapter.sectionController(for: filtered) {
                    controller.collectionContext?.performBatch(animated: true, updates: {
                        controller.collectionContext?.reload(controller)
                    }, completion: nil)
                }
            }
        }
    }
}

