import VitalityKit
import VIAUtilities
import VIAUIKit
import VIACommon

public protocol VNAAssessmentDataSharingConsentViewControllerDelegate: class {
    
    func dataSharingConsentControllerDidSelectAgree()
    
    func dataSharingConsentControllerDidSelectDisagree(_ controller: VNAAssessmentDataSharingConsentViewController)
    
}

public class VNAAssessmentDataSharingConsentViewController: CustomTermsConditionsViewController {
    var questionnaire: VNAQuestionnaire?
    weak var submitter: VNAAssessmentSubmitter?
    weak var navigationDelegate: VNAAssessmentDataSharingConsentViewControllerDelegate?
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        
        // Get content model. Original = "vna-data-privacy-policy-content"
        if let tenantID = AppSettings.getAppTenant() {
            if tenantID == .IGI || tenantID == .EC || tenantID == .ASR || tenantID == .GI {
                // Article id was hardcoded because this is non-existent on the login response payload.
                self.viewModel = CustomTermsConditionsViewModel(articleId: "vna-data-privacy-policy-content")
            } else {
                self.viewModel = CustomTermsConditionsViewModel(articleId: AppConfigFeature.contentId(for: .VHCDSConsentContent))
            }
        }
    
        rightButtonTitle = VIAApplicableFeatures.default.getDataSharingRightBarButtonTitle()
        leftButtonTitle = VIAApplicableFeatures.default.getDataSharingLeftBarButtonTitle()
    }
    
    override public func configureAppearance() {
        super.configureAppearance()
        navigationController?.makeNavigationBarInvisible()
        navigationController?.navigationBar.isHidden = true
    }
    
    // MARK: Action buttons
    
    override public func leftButtonTapped(_ sender: UIBarButtonItem?) {
        navigationDelegate?.dataSharingConsentControllerDidSelectDisagree(self)
    }
    
    override public func rightButtonTapped(_ sender: UIBarButtonItem?) {
        self.showHUDOnWindow()
        let events = [EventTypeRef.VNADataSharingAgree]
        viewModel?.agreeToTermsAndConditions(with: events, completion: { [weak self]
            (error) in
            self?.hideHUDFromWindow()
            if let error = error {
                sender?.isEnabled = true
                self?.handleBackendErrorWithAlert(BackendError.network(error: error as NSError))
                return
            }
            // create event to agree to privacy policy
            sender?.isEnabled = false
            self?.submitVNA(from: sender)
        })
    }
    
    func submitVNA(from sender: UIBarButtonItem?) {
        self.showHUDOnWindow()
        weak var weakSelf = self
        submitter?.submit(for: weakSelf, with: { [weak self] error in
            self?.hideHUDFromWindow()
            if let error = error {
                self?.handleErrorWithAlert(error: error, tryAgain: { [weak self] error in
                    self?.submitVNA(from: sender)
                })
                return
            }
            self?.navigationDelegate?.dataSharingConsentControllerDidSelectAgree()
        })
    }
    
    override public func returnToPreviousView() {
        navigationDelegate?.dataSharingConsentControllerDidSelectDisagree(self)
    }
}

