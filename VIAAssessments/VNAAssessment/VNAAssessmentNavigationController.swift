//
//  VNAAssessmentNavigationController.swift
//  VitalityActive
//
//  Created by Von Kervin R. Tuico on 10/02/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import UIKit

class VNAAssessmentNavigationController: UINavigationController {
    
    // MARK: Properties
    
    var sectionTitle: String? {
        didSet {
            if let navBar = self.navigationBar as? AssessmentNavigationBar {
                navBar.sectionLabel.text = sectionTitle
            }
        }
    }
    
    var sectionIcon: UIImage?
    
    var totalSections = 1
    
    var currentSection = 0
    
    // MARK: Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    deinit {
        debugPrint("AssessmentNavigationController deinit")
    }
    
    public func configureNavigationBar() {
        if let navBar = self.navigationBar as? AssessmentNavigationBar {
            navBar.configure(sectionTitle: sectionTitle, sectionIcon: sectionIcon, totalSections: totalSections, currentSection: currentSection)
        }
    }
    
}

