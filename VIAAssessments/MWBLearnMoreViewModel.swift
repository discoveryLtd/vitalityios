//
//  MWBLearnMoreViewModel.swift
//  VitalityActive
//
//  Created by Michelle R. Oratil on 20/02/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import VIAUIKit
import VitalityKit
import VIACommon

public class MWBLearnMoreViewModel: AnyObject, VIALearnMoreViewModel {
    
    public var title: String {
        return CommonStrings.LearnMoreButtonTitle104
    }
    
    public var contentItems: [LearnMoreContentItem] {
        var items = [LearnMoreContentItem]()
        
        items.append(LearnMoreContentItem(
            header: CommonStrings.Mwb.LearnMoreHeading1Title1202,
            content: CommonStrings.Mwb.LearnMoreHeading1Message1203
        ))
        
        items.append(LearnMoreContentItem(
            header: CommonStrings.Mwb.LearnMoreSection1Title1204,
            content: CommonStrings.Mwb.LearnMoreSection1Message1205,
            image: VIAAssessmentsAsset.Mwb.mwbLearnMoreTimeToComplete.image
        ))
        
        items.append(LearnMoreContentItem(
            header: CommonStrings.Mwb.LearnMoreSection2Title1206,
            content: CommonStrings.Mwb.LearnMoreSection2Message1207,
            image: VIAAssessmentsAsset.Mwb.mwbLearnMorePoints.templateImage
        ))
        
        items.append(LearnMoreContentItem(
            header: CommonStrings.Mwb.LearnMoreSection3Title1208,
            content: CommonStrings.Mwb.LearnMoreSection3Message1209,
            image: VIAAssessmentsAsset.Mwb.mwbLearnMoreWellbeing.image
        ))
        
        return items
    }
    
    public var imageTint: UIColor {
        return .knowYourHealthGreen()
    }
}
