import VitalityKit
import VitalityKit
import VIAUIKit
import RealmSwift
import VIAUtilities

public class VHRLandingViewModel: AssessmentLandingViewModel {

    // MARK: Properties

    public var vhrRealm = DataProvider.newVHRRealm()
    public var vhrQuestionnaireData = [AssessmentQuestionnaireCellData]()
    public var vhrHeaderData: AssessmentHeaderCellData?
    public let integerFormatter = NumberFormatter.integerFormatter()
    public var questionnairesFooterText: String? {
        return vhrRealm.allVHRQuestionnaireProgressAndPointsTracking().first?.questionnaireSetTextNote
    }

    // MARK: Lifecycle

    public init() {

    }

    // MARK: Functions

    public func shouldShowOnboarding() -> Bool {
        return !AppSettings.hasShownVHROnboarding()
    }

    public func configureQuestionnaires() {
        vhrRealm.refresh()
        vhrQuestionnaireData.removeAll()
        configureHeader()
        let  questionnaires = vhrRealm.allSortedVHRQuestionnaires()

        for questionnaire in questionnaires {
            let typeKey = questionnaire.typeKey
            let title = questionnaire.typeName ?? ""
            var timeToComplete = questionnaire.textDescription
            var buttonText = CommonStrings.LandingScreen.StartButton305
            let completionState = questionnaire.getCompletionState()
            if completionState == .Complete {
                buttonText = CommonStrings.LandingScreen.EditButton307
                timeToComplete = CommonStrings.LandingScreen.CompletedMessage327
            } else if completionState == .InProgress {
                buttonText = CommonStrings.LandingScreen.ContinueButton306
            }
            let completed = questionnaire.completionFlag
            let completeIcon = UIImage.templateImage(asset: .vhcGenericInHealthyRangeSmall)
            let cellData = AssessmentQuestionnaireCellData(title: title,
                                                    buttonText: buttonText,
                                                    timeToComplete: timeToComplete,
                                                    completed: completed,
                                                    completeIcon: completeIcon,
                                                    typeKey: typeKey)
            vhrQuestionnaireData.append(cellData)
        }
    }

    public func configureHeader() {
        vhrHeaderData = nil
        let totalSections = getTotalNumberOfQuestionnaires()
        let completeSections = getTotalNumberOfCompletedQuestionnaires()
        let incompleteSections = totalSections - completeSections
        let completedImage = UIImage.templateImage(asset: .hsCardCheckmarkBig)
        let earnedPoints = getQuestionnairesTotalEarnedPoints()
        let potentialPoints = getQuestionnairesTotalPotentialPoints()
        var title = ""
        var description = getHeaderDescriptionSubtext() ?? ""

        if incompleteSections >= 1 {
            title = CommonStrings.HomeCard.CardEarnTitle292(integerFormatter.string(from: potentialPoints as NSNumber) ?? "")
        } else if incompleteSections <= 0 {
            title = CommonStrings.LandingScreen.CompletedMessage327
            description = CommonStrings.LandingHeader.DescriptionCompleted2258(integerFormatter.string(from: earnedPoints as NSNumber) ?? "")
        }

        vhrHeaderData = AssessmentHeaderCellData(title: title, description: description, totalSections: totalSections, completeSections: completeSections, completedImage: completedImage)
    }

    public func getTotalNumberOfQuestionnaires() -> Int {
        guard let numberOfQuestionnaires = vhrRealm.allVHRQuestionnaireProgressAndPointsTracking().first?.totalQuestionnaires else { return 0 }
        return numberOfQuestionnaires
    }

    public func getHeaderDescriptionSubtext() -> String? {
        if let description = vhrRealm.allVHRQuestionnaireProgressAndPointsTracking().first?.questionnaireSetTextDescription {
            guard !description.isEmpty else { return nil }
            return description
        }
        return nil
    }

    public func getTotalNumberOfCompletedQuestionnaires() -> Int {
        guard let numberOfCompletedQuestionnaires = vhrRealm.allVHRQuestionnaireProgressAndPointsTracking().first?.totalQuestionnaireCompleted.value else { return 0 }
        return numberOfCompletedQuestionnaires
    }

    public func getQuestionnairesTotalPotentialPoints() -> Int {
        guard let totalPotentialPoints = vhrRealm.allVHRQuestionnaireProgressAndPointsTracking().first?.totalPotentialPoints.value else { return 0 }
        return totalPotentialPoints
    }

    public func getQuestionnairesTotalEarnedPoints() -> Int {
        guard let totalPotentialPoints = vhrRealm.allVHRQuestionnaireProgressAndPointsTracking().first?.totalEarnedPoints.value else { return 0 }
        return totalPotentialPoints
    }

    public func getQuestionnaireAt(index: Int) -> VHRQuestionnaire? {
        let questionnaireTypeKey = vhrQuestionnaireData[index].typeKey
        let questionnaireResult = vhrRealm.allVHRQuestionnaires().filter { $0.typeKey == questionnaireTypeKey }
        let questionnaire = questionnaireResult.first
        return questionnaire
    }

    public func getVHRQuestionnaire(using selectedQuestionnaireTypeKey: Int) -> VHRQuestionnaire? {
        return vhrRealm.getVHRQuestionnaire(using: selectedQuestionnaireTypeKey)
    }

    // MARK: Networking

    public func fetchQuestionnaires(forceUpdate: Bool = false, completion: @escaping (Error?, Bool?) -> Void) {
        if AppSettings.hasUpdatedVHC() {
            // Clear VHR realm and fetch the latest updates.
            try! self.vhrRealm.write {
                self.vhrRealm.deleteAll()
            }
            
            AppSettings.clearHasUpdatedVHC()
        }
        
        if VHRCache.vhrDataIsOutdated() || forceUpdate == true {
            let coreRealm = DataProvider.newRealm()
            let partyId = coreRealm.getPartyId()
            let tenantId = coreRealm.getTenantId()
            let membershipId = coreRealm.getMembershipId()
            let setTypeKey = QuestionnaireSetRef.VHR
            let prePopulation = true
            let vhrQuestionnaireParameter = VHRQuestionnaireParameter(setTypeKey: setTypeKey, prePopulation: prePopulation)

            Wire.Member.VHRQuestionnaireProgressAndPointsTracking(tenantId: tenantId, partyId: partyId, vitalityMembershipId: membershipId, payload: vhrQuestionnaireParameter) { error in
                guard error == nil else {
                    completion(error, VHRCache.vhrDataIsOutdated())
                    return
                }
                
                self.configureQuestionnaires()
                completion(nil, nil)
            }
        } else {
            configureQuestionnaires()
            completion(nil, nil)
        }
    }

}
