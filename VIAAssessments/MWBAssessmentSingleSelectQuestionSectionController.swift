//
//  MWBAssessmentSingleSelectQuestionSectionController.swift
//  VitalityActive
//
//  Created by Michelle R. Oratil on 14/02/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import IGListKit
import VIAUIKit
import VIAUtilities
import VitalityKit
import RealmSwift

public final class MWBAssessmentSingleSelectQuestionSectionController: IGListSectionController, IGListSectionType, FootnoteToggleable {
    
    // MARK: Properties
    
    var realm: Realm
    
    var questionDetail: MWBAssessmentQuestionPresentationDetail! {
        didSet {
            updateData()
            
            if let validAnswer = self.questionDetail.rawQuestion(in: self.realm)?.capturedResult()?.answer {
                selectedAnswer = self.questionDetail.validValuesPresentationDetail?.filter({ $0.rawValidValue.name == validAnswer }).first
            }
        }
    }
    
    var selectedAnswer: MWBAssessmentValidValuePresentationDetail?
    
    // MARK: FootnoteToggleable
    
    var isFootnoteExpanded: Bool = false
    
    var indexForFootnoteCell: Int? {
        return shouldAddNote() ? data.count - 1 : nil
    }
    
    // MARK: Lifecycle
    
    public required init(realm: Realm) {
        self.realm = realm
    }
    
    // MARK: Data
    
    var data: [Any] = []
    
    func updateData() {
        // question
        data = [self.questionDetail.textAndDescriptionDetail()]
        // each answer
        questionDetail.validValuesPresentationDetail?.forEach({ data.append($0) })
        // footnote
        if shouldAddNote() {
            data.append(self.questionDetail.noteDetail())
        }
    }
    
    func shouldAddNote() -> Bool {
        return self.questionDetail.textNote != nil
    }
    
    // MARK: IGListSectionType
    
    public func numberOfItems() -> Int {
        return data.count
    }
    
    public func didUpdate(to object: Any) {
        self.questionDetail = object as! MWBAssessmentQuestionPresentationDetail
    }
    
    public func cellForItem(at index: Int) -> UICollectionViewCell {
        let item = data[index]
        if let textDetail = item as? AttributedTextDetail, textDetail.type == .normal {
            return attributedTextCell(for: textDetail, at: index)
        } else if let textDetail = item as? AttributedTextDetail, textDetail.type == .footnote {
            return footnoteAttributedTextCell(for: textDetail, at: index)
        } else if let answerDetail = item as? MWBAssessmentValidValuePresentationDetail {
            return answerCell(for: answerDetail, at: index)
        }
        return collectionContext!.dequeueReusableCell(of: UICollectionViewCell.self, for: self, at: index)
    }
    
    public func sizeForItem(at index: Int) -> CGSize {
        let width = collectionContext!.containerSize.width
        let item = data[index]
        
        if let textDetail = item as? AttributedTextDetail, textDetail.type == .normal {
            return MWBAssessmentTextCollectionViewCell.height(with: textDetail, constrainedTo: width)
        } else if let textDetail = item as? AttributedTextDetail, textDetail.type == .footnote {
            return MWBAssessmentFootnoteCollectionViewCell.height(with: textDetail, constrainedTo: width, isFootnoteExpanded: self.isFootnoteExpanded)
        } else if let answerDetail = item as? MWBAssessmentValidValuePresentationDetail {
            if answerDetail == selectedAnswer {
                return MWBAssessmentSingleSelectAnswerCollectionViewCell.height(with: answerDetail.textAndDescriptionAndNoteDetail(), constrainedTo: width)
            } else {
                return MWBAssessmentSingleSelectAnswerCollectionViewCell.height(with: answerDetail.textAndDescriptionDetail(), constrainedTo: width)
            }
        }
        
        return CGSize.zero
    }
    
    public func didSelectItem(at index: Int) {
        let item = data[index]
        
        // tapped footnote
        if let textDetail = item as? AttributedTextDetail, textDetail.type == .footnote {
            toggleFootnote()
            return
        }
        
        
        // tapped answer
        if let answerDetail = item as? MWBAssessmentValidValuePresentationDetail {
            // reload the old and new answer cells
            var indexes = [index]
            if let validAnswer = selectedAnswer {
                if let oldIndex = data.index(where: { $0 as? MWBAssessmentValidValuePresentationDetail == validAnswer }) {
                    indexes.append(oldIndex)
                }
            }
            
            // capture answer
            selectedAnswer = answerDetail
            updateCapturedResult()
        }
    }
    
    func updateCapturedResult() {
        guard let answerValue = selectedAnswer?.rawValidValue.name else { return }
        guard let type = self.questionDetail.rawQuestion(in: realm)?.questionType else { return }
        
        if let controller = self.viewController as? MWBAssessmentQuestionnaireSectionViewController {
            controller.answeringDelegate?.persist(answerValue,
                                                  unit: nil,
                                                  for: questionDetail.rawQuestionTypeKey,
                                                  of: type,
                                                  allowsMultipleAnswers: false,
                                                  answerIsValid: true)
        }
    }
    
    // MARK: Cells
    
    func attributedTextCell(for textDetail: AttributedTextDetail, at index: Int) -> UICollectionViewCell {
        let cell = collectionContext!.dequeueReusableCell(of: MWBAssessmentTextCollectionViewCell.self, for: self, at: index) as! MWBAssessmentTextCollectionViewCell
        cell.label.attributedText = textDetail.attributedString
        cell.addTopBorder = true
        cell.addBottomBorder = true
        return cell
    }
    
    func footnoteAttributedTextCell(for textDetail: AttributedTextDetail, at index: Int) -> UICollectionViewCell {
        let cell = collectionContext!.dequeueReusableCell(of: MWBAssessmentFootnoteCollectionViewCell.self, for: self, at: index) as! MWBAssessmentFootnoteCollectionViewCell
        cell.setLabelAttributedText(textDetail.attributedString, isFootnoteExpanded: self.isFootnoteExpanded)
        return cell
    }
    
    func answerCell(for answerDetail: MWBAssessmentValidValuePresentationDetail, at index: Int) -> UICollectionViewCell {
        let cell = collectionContext!.dequeueReusableCell(of: MWBAssessmentSingleSelectAnswerCollectionViewCell.self, for: self, at: index) as! MWBAssessmentSingleSelectAnswerCollectionViewCell
        cell.answerIsSelected = answerDetail == selectedAnswer
        if answerDetail == selectedAnswer {
            cell.label.attributedText = answerDetail.textAndDescriptionAndNoteDetail().attributedString
        } else {
            cell.label.attributedText = answerDetail.textAndDescriptionDetail().attributedString
        }
        cell.addSeperator = index != data.count - 1
        cell.addBottomBorder = index == data.count - 1
        return cell
    }
    
}


public final class MWBAssessmentSingleSelectAnswerCollectionViewCell: UICollectionViewCell, Nibloadable {
    
    fileprivate static let inset: CGFloat = 15.0
    
    private static let minimumHeight: CGFloat = 26.0
    
    public var addSeperator = false
    
    public var addTopBorder = false
    
    public var addBottomBorder = false
    
    var answerIsSelected = false {
        didSet {
            toggleImageViewSelected()
        }
    }
    
    // MARK: Views
    
    public lazy var label: UILabel = {
        let label = UILabel(frame: .zero)
        label.font = UIFont.title2Font()
        label.textColor = UIColor.night()
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    public lazy var imageView: UIImageView = {
        let view = UIImageView(frame: .zero)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.contentMode = .center
        view.image = nil
        view.tintColor = UIColor.currentGlobalTintColor()
        return view
    }()
    
    // MARK: Init
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupSubviews()
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        
        contentView.layer.removeAllBorderLayers()
        if addTopBorder {
            contentView.layer.addBorder(edge: .top)
        }
        if addBottomBorder {
            contentView.layer.addBorder(edge: .bottom)
        }
        if addSeperator {
            contentView.layer.addBottomBorder(inset: MWBAssessmentSingleSelectAnswerCollectionViewCell.inset)
        }
        
        toggleImageViewSelected()
    }
    
    private func setupSubviews() {
        contentView.backgroundColor = .day()
        
        let stackView = UIStackView()
        contentView.addSubview(stackView)
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .horizontal
        stackView.distribution = .fill
        stackView.spacing = 10
        stackView.addArrangedSubview(self.label)
        stackView.addArrangedSubview(self.imageView)
        
        stackView.snp.makeConstraints { (make) in
            make.top.bottom.equalToSuperview().inset(MWBAssessmentSingleSelectAnswerCollectionViewCell.inset)
            make.left.right.equalToSuperview().inset(MWBAssessmentSingleSelectAnswerCollectionViewCell.inset)
            make.height.greaterThanOrEqualTo(imageView.snp.height)
        }
        
        imageView.snp.makeConstraints { (make) in
            make.width.equalTo(MWBAssessmentSingleSelectAnswerCollectionViewCell.minimumHeight).priority(1000)
        }
    }
    
    override public func preferredLayoutAttributesFitting(_ layoutAttributes: UICollectionViewLayoutAttributes) -> UICollectionViewLayoutAttributes {
        return self.defaultContentViewLayoutSizeFitting(layoutAttributes)
    }
    
    // MARK: Helpers
    
    public func toggleImageViewSelected() {
        if answerIsSelected {
            imageView.image = UIImage.templateImage(asset: .vhrCheckMarkSingle)
        } else {
            imageView.image = nil
        }
    }
    
    // MARK: Height calculation
    
    public class func height(with attributedTextDetail: AttributedTextDetail, constrainedTo width: CGFloat) -> CGSize {
        let inset = MWBAssessmentSingleSelectAnswerCollectionViewCell.inset
        let insets = UIEdgeInsets(top: 0, left: inset, bottom: 0, right: inset)
        let size = TextSize.size(attributedTextDetail.attributedString, width: width, insets: insets)
        let height = max(size.height, MWBAssessmentSingleSelectAnswerCollectionViewCell.minimumHeight) + inset * 2
        return CGSize(width: width, height: height)
    }
    
}
