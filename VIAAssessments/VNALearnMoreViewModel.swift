import VIAUIKit
import VitalityKit
import VIACommon

public class VNALearnMoreViewModel: AnyObject, VIALearnMoreViewModel {
    
    public var title: String {
        return CommonStrings.LearnMoreButtonTitle104
    }
    
    public var contentItems: [LearnMoreContentItem] {
        var items = [LearnMoreContentItem]()
        
        items.append(LearnMoreContentItem(
            header: CommonStrings.Vna.LearnMore.Heading1Title400,
            content: CommonStrings.Vna.LearnMore.Heading1Message399
        ))
        
        items.append(LearnMoreContentItem(
            header: CommonStrings.Vna.LearnMore.Section1Title402,
            content: CommonStrings.Vna.LearnMore.Section1Message401,
            image: VIAAssessmentsAsset.vhrLearnCompleteTime.templateImage
        ))
        
        items.append(LearnMoreContentItem(
            header: CommonStrings.Vna.LearnMore.Section2Title404,
            content: CommonStrings.Vna.LearnMore.Section2Message403,
            image: VIAAssessmentsAsset.vhrLearnMorePoints.templateImage
        ))
        
        items.append(LearnMoreContentItem(
            header: CommonStrings.Vna.LearnMore.Section3Title406,
            content: CommonStrings.Vna.LearnMore.Section3Message405,
            image: VIAAssessmentsAsset.Vna.vnaLearnMoreNutritionActive.templateImage
        ))
        
        return items
    }
    
    public var imageTint: UIColor {
        return .knowYourHealthGreen()
    }
}
