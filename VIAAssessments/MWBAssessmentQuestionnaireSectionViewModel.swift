//
//  MWBAssessmentQuestionnaireSectionViewModel.swift
//  VitalityActive
//
//  Created by Michelle R. Oratil on 14/02/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import VitalityKit
import VIAUtilities
import VIAUIKit
import VitalityKit
import RealmSwift
import IGListKit
import VIACommon

public protocol MWBAssessmentQuestionnaireSectionViewModelDelegate: class {
    func viewModelDidUpdateData()
    func viewModelDidUpdateCapturedResults()
    func reloadSectionController(for questionDetail: MWBAssessmentQuestionPresentationDetail)
    func scrollTo(questionDetail: MWBAssessmentQuestionPresentationDetail)
}

public class MWBAssessmentQuestionnaireSectionViewModel {
    
    // MARK: Properties
    
    weak var delegate: MWBAssessmentQuestionnaireSectionViewModelDelegate?
    
    var questionnaireSection: MWBQuestionnaireSection
    
    var isLastSection: Bool = false {
        didSet {
            updateData()
        }
    }
    
    public private(set) var realm = DataProvider.newMWBRealm()
    
    var visibleQuestionsNotificationToken: NotificationToken?
    
    var capturedResultsNotificationToken: NotificationToken?
    
    private lazy var visibleQuestions: Results<MWBQuestion> = {
        return self.questionnaireSection.sortedVisibleQuestions()
    }()
    
    private lazy var capturedResults: Results<MWBCapturedResult> = {
        return self.realm.mwbCapturedResults(for: self.questionnaireSection.typeKey)
    }()
    
    lazy var navigationDetail: MWBAssessmentQuestionnaireSectionNavigationDetail = {
        return MWBAssessmentQuestionnaireSectionNavigationDetail(isLastSection: self.isLastSection, isEnabled: self.isValid())
    }()
    
    var data: [IGListDiffable] = []{
        didSet{
            self.delegate?.viewModelDidUpdateData()
        }
    }
    
    public var nextButtonStatus: Bool = false
    
    var unansweredQuestionsDetail = [MWBAssessmentQuestionPresentationDetail]()
    
    // MARK: Lifecycle
    
    deinit {
        debugPrint("MWBQuestionnaireSectionViewModel deinit")
    }
    
    required public init?(questionnaireSectionTypeKey: Int?, isLastSection lastSection: Bool) {
        guard let section = realm.mwbQuestionnaireSection(by: questionnaireSectionTypeKey ?? -999) else {
            debugPrint("Couldn't find MWB questionnaire section to return")
            return nil
        }
        
        questionnaireSection = section
        isLastSection = lastSection
        updateData()
        registerForNotifications()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.isNextButtonTapped(_:)), name: .VIAMWBQuestionnaireNextNotification, object: nil)
    }
    
    /*
     * Retrieve the status of next button based on
     * MWBAssessmentNavigationButtonSectionController
     * This is useful when the whole section is reloaded
     */
    @objc func isNextButtonTapped(_ notification: NSNotification) {
        if let isTapped = notification.userInfo?["isNextButtonTapped"] as? Bool {
            nextButtonStatus = isTapped
            updateData()
            if let qDetail = unansweredQuestionsDetail.first{
                self.delegate?.scrollTo(questionDetail:qDetail)
            }
        }
    }
    
    func registerForNotifications() {
        visibleQuestionsNotificationToken = visibleQuestions.addNotificationBlock { [weak self] (changes: RealmCollectionChange) in
            self?.visibleQuestionsDidChange(changes)
        }
        
        capturedResultsNotificationToken = capturedResults.addNotificationBlock { [weak self] (changes: RealmCollectionChange) in
            self?.capturedResultsDidChange(changes)
        }
        
        
    }
    
    func visibleQuestionsDidChange(_ changes: RealmCollectionChange<Results<MWBQuestion>>) {
        updateData()
        self.delegate?.viewModelDidUpdateData()
    }
    
    func capturedResultsDidChange(_ changes: RealmCollectionChange<Results<MWBCapturedResult>>) {
        updateNavigation()
        self.delegate?.viewModelDidUpdateCapturedResults()
        
        switch changes {
        case .update(let results, let deletions, let insertions, let modifications):
            updateSelectQuestionControllers(results: results, deletions: deletions, insertions: insertions, modifications: modifications)
            break
        default:
            break
        }
        unansweredQuestionsDetail = [MWBAssessmentQuestionPresentationDetail]()
    }
    
    func updateSelectQuestionControllers(results: Results<MWBCapturedResult>, deletions: [Int], insertions: [Int], modifications: [Int]) {
        // determine which change was made
        var altered: Int?
        if deletions.count > 0 {
            altered = deletions.first
        } else if insertions.count > 0 {
            altered = insertions.first
        } else if modifications.count > 0 {
            altered = modifications.first
        }
        guard let validAltered = altered else { debugPrint("Couldn't figure it out");  return }
        guard results.count > validAltered else { return }
        
        // filter the question controllers down to only questions
        // that have single or multi select decorations
        guard let questionPresentationDetails = self.data.filter({ $0 is MWBAssessmentQuestionPresentationDetail }) as? [MWBAssessmentQuestionPresentationDetail] else { return }
        let singleMultiSelectableQuestions = questionPresentationDetails.filter({ [.Toggle, .Checkbox, .RadioButton].contains($0.decorator) })
        
        // get the onscreen presentation detail for the question that was
        // just answered via its captured result
        let capturedResult: MWBCapturedResult = results[validAltered]
        guard let question = capturedResult.realm?.mwbquestion(by: capturedResult.questionTypeKey) else { return }
        guard let detail = singleMultiSelectableQuestions.filter({ $0.rawQuestionTypeKey == question.typeKey }).first else { return }
        
        self.delegate?.reloadSectionController(for: detail)
    }
    
    func updateNavigation() {
        self.navigationDetail.isLastSection = self.isLastSection
        self.navigationDetail.isEnabled = isValid()
    }
    
    func updateData() {
        print("ge-->MWB: Update data on UI")
        let interQuestionPadding: CGFloat = 30.0
        
        var data = [IGListDiffable]()
        //data.append(SpacerDetail(interQuestionPadding))
        
        // section header textDescription
        // note: due to the `text` property not being part of the design but in the model,
        // we're explicitly omitting it on the frontend. we're rather using the `textDescription`
        // property to replace the `text` property to be in line with Android and the web.
        if !questionnaireSection.textDescription.isEmpty {
            data.append(SpacerDetail(interQuestionPadding))
            let detail = TextDetail(text: questionnaireSection.textDescription, font: .headlineFont(), textColor: .night())
            data.append(MWBAssessmentQuestionnaireSectionHeaderFooterDetail(text: detail, textDescription: nil))
        }
        
        // each section question
        for question in visibleQuestions {
            
            //ge20180113 : if question has a parent question, no need to add padding
            if(question.isChild == false){
                data.append(SpacerDetail(interQuestionPadding))
            }
            
            //ge20180420 : Add required prompt
            if (VIAApplicableFeatures.default.showRequiredPrompt! && question.isChild == false && question.capturedResult() == nil && nextButtonStatus){
                //to handle multipart & toggle questions separately if any
                data.append(AssessmentRequiredPrompt(key: questionnaireSection.typeKey))
            }
            
            
            
            // question
            let questionTextDetail = TextDetail(text: question.text, font: .headlineFont(), textColor: .night())
            // question description
            var questionTextDescriptionDetail: TextDetail?
            if !question.textDescription.isEmpty {
                questionTextDescriptionDetail = TextDetail(text: question.textDescription, font: .subheadlineFont(), textColor: .mediumGrey())
            }
            // put it all together
            let questionDetail = MWBAssessmentQuestionPresentationDetail(text: questionTextDetail,
                                                                      textDescription: questionTextDescriptionDetail,
                                                                      presentationType: question.questionType,
                                                                      decorator: question.questionDecorator?.type ?? .Unknown,
                                                                      isVisible: question.isVisible,
                                                                      questionTypeKey: question.typeKey)
            
            // valid values for question
            if question.validValues.count > 0 {
                var validValuesPresentationDetail = [MWBAssessmentValidValuePresentationDetail]()
                for validValue in question.validValues {
                    // valid value
                    let answerTextDetail = TextDetail(text: validValue.value, font: .bodyFont(), textColor: .night())
                    // valid value description
                    var answerTextDescriptionDetail: TextDetail?
                    if !validValue.valueDescription.isEmpty {
                        answerTextDescriptionDetail = TextDetail(text: validValue.valueDescription, font: .subheadlineFont(), textColor: .mediumGrey())
                    }
                    // valid value note that shows when selected
                    var answerTextNoteDetail: TextDetail?
                    if !validValue.valueNote.isEmpty {
                        answerTextNoteDetail = TextDetail(text: validValue.valueNote, font: .subheadlineFont(), textColor: .mediumGrey())
                    }
                    // put it all together
                    let answer = MWBAssessmentValidValuePresentationDetail(text: answerTextDetail,
                                                                        textDescription: answerTextDescriptionDetail,
                                                                        textNote: answerTextNoteDetail,
                                                                        rawValidValue: validValue)
                    validValuesPresentationDetail.append(answer)
                }
                questionDetail.validValuesPresentationDetail = validValuesPresentationDetail
            } else {
                debugPrint("No valid values for the question")
            }
            
            // append the question detail
            data.append(questionDetail)
            
            // source for prepop if available
            var prepopEventRef: EventTypeRef = .Unknown
            if let capturedResult = question.capturedResult() {
                debugPrint("Have captured result for question: \(question.text)")
                if let prepopulationEventKeyValue = capturedResult.prepopulationEventKey.value {
                    debugPrint("Have prepopulationEventKeyValue: \(prepopulationEventKeyValue)")
                    prepopEventRef = EventTypeRef(rawValue: prepopulationEventKeyValue) ?? .Unknown
                    debugPrint("prepopEventRef set to \(prepopEventRef.rawValue)")
                    debugPrint("capturedResult.dateCaptured: \(capturedResult.dateCaptured)")
                    debugPrint("full capturedResult: \(capturedResult)")
                    if prepopEventRef != .Unknown {
                        debugPrint("prepopEventRef != Unknown, creating prepopTextDetail")
                        let formattedDate = Localization.fullDateFormatter.string(from: capturedResult.dateCaptured)
                        let prepopText = CommonStrings.Assessment.Vhr.PrePopulationDetails570(prepopEventRef.asSource(), formattedDate)
                        let textDetail = TextDetail(text: prepopText, font: .footnoteFont(), textColor: .darkGrey())
                        let prepopulationDetail = MWBAssessmentQuestionPrepopulationPresentationDetail(text: textDetail)
                        debugPrint("prepopTextDetail: \(prepopulationDetail)")
                        data.append(prepopulationDetail)
                    } else {
                        debugPrint("Unknown prepop event, fail")
                    }
                } else {
                    debugPrint("Couldn't obtain prepopulationEventKeyValue")
                }
            } else {
                debugPrint("No capturedResult for question: \(question.text)")
                unansweredQuestionsDetail.append(questionDetail)
            }
            
            // expandable question footnote
            if !question.textNote.isEmpty {
                let textDetail = TextDetail(text: question.textNote, font: .footnoteFont(), textColor: .darkGrey())
                let questionFootNoteDetail = MWBAssessmentQuestionFootnotePresentationDetail(text: textDetail)
                
                // add extra padding if there's a prepop note
                if prepopEventRef != .Unknown {
                    data.append(SpacerDetail(7.0)) // this 7 plus the topInset of the footnote adds to 15, which the design requires
                }
                // append footnote
                data.append(questionFootNoteDetail)
            }
            
            // spacing
            //data.append(SpacerDetail(interQuestionPadding))
        }
        
        // section footer text
        if !questionnaireSection.textNote.isEmpty {
            data.append(SpacerDetail(interQuestionPadding))
            let sectionTextNoteDetail = TextDetail(text: questionnaireSection.textNote, font: .headlineFont(), textColor: .night())
            data.append(MWBAssessmentQuestionnaireSectionHeaderFooterDetail(text: sectionTextNoteDetail))
        }
        
        // navigation button
        data.append(SpacerDetail(interQuestionPadding))
        data.append(navigationDetail)
        data.append(SpacerDetail(interQuestionPadding))
        
        self.data = data
    }
    
    // MARK: Validation
    
    func isValid() -> Bool {
        return questionnaireSection.isValid()
    }
    
}
