//
//  MWBAssessmentOnboardingViewController.swift
//  VitalityActive
//
//  Created by Michelle R. Oratil on 14/02/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import VIACommon
import VitalityKit
import VIAUIKit
import VitalityKit

public class MWBAssessmentOnboardingViewModel: OnboardingViewModel {
    
    public var contentItems: [OnboardingContentItem] {
        return [OnboardingContentItem]()
    }
    
    public var heading: String {
        return ""
    }
    
    public func setOnboardingHasShown() {
        fatalError("Not implemented")
    }
    
    public var buttonTitle: String {
        return CommonStrings.GenericGotItButtonTitle131
    }
    
    public var alternateButtonTitle: String? {
        return nil
    }
    
    public var gradientColor: Color {
        return .green
    }
    
    public var mainButtonHighlightedTextColor: UIColor? {
        return .knowYourHealthGreen()
    }
    
}

class MWBAssessmentOnboardingViewController: VIAOnboardingViewController, KnowYourHealthTintable {
    
    var assessmentViewModel: MWBAssessmentOnboardingViewModel? {
        didSet {
            self.viewModel = assessmentViewModel
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.navigationBar.isHidden = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.hideBackButtonTitle()
    }
    
    // MARK: - Navigation
    
    override func mainButtonTapped(_ sender: UIButton) {
        assessmentViewModel?.setOnboardingHasShown()
        self.dismiss(animated: true, completion: nil)
    }
    
    override func alternateButtonTapped(_ sender: UIButton) {
        self.performSegue(withIdentifier: "showLearnMore", sender: self)
    }
}
