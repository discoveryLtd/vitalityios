//
//  MWBAssessmentCompletionViewModel.swift
//  VitalityActive
//
//  Created by Michelle R. Oratil on 13/02/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import VIAUIKit
import VitalityKit
import VitalityKit

public class MWBCompletionViewModel: AnyObject, CirclesViewModel {
    
    public var headingText: String?
    public var messageText: String?
    public var footnoteText: String?
    public var buttonTitleText: String?
    
    var incompleteQuestionnaires: [MWBQuestionnaire]? {
        didSet {
            configureCorrectState()
        }
    }
    
    var currentSectionTitle: String
    var potentialPoints: Int
    var completionFlag: Bool?
    var assessmentName: String?
    
    let integerFormatter = NumberFormatter.integerFormatter()
    
    public var image: UIImage? {
        return UIImage(asset: .arOnboardingActivated)
    }
    
    public var heading: String? {
        return headingText
    }
    
    public var message: String? {
        return messageText
    }
    
    public var footnote: String? {
        return footnoteText
    }
    
    public var buttonTitle: String? {
        return buttonTitleText
    }
    
    public var buttonHighlightedTextColor: UIColor? {
        return UIColor.knowYourHealthGreen()
    }
    
    public var gradientColor: Color {
        return .green
    }
    
    public func getAssessmentCompletionStatus(completionFlag: Bool, assessmentName: String){
        self.completionFlag = completionFlag
        self.assessmentName = assessmentName
    }
    
    init(incompleteQuestionnaires: [MWBQuestionnaire]?, sectionTitle: String, potentialPoints: Int, assessmentName: String, completionFlag: Bool) {
        self.incompleteQuestionnaires = incompleteQuestionnaires
        self.currentSectionTitle = sectionTitle
        self.potentialPoints = potentialPoints
        self.assessmentName = assessmentName
        self.completionFlag = completionFlag
        configureCorrectState()
    }
    
    func configureForUpdatedAssessment() {
        self.buttonTitleText = CommonStrings.Mwb.GreatButtonTitle120
        self.headingText = CommonStrings.Mwb.Completion.Title1226(self.assessmentName!)
        self.messageText = CommonStrings.Mwb.Completion.Message1227
    }
    
    func configureForCompletedAssessment() {
        self.buttonTitleText = CommonStrings.Mwb.GreatButtonTitle120
        self.headingText = CommonStrings.Mwb.Completion.Title1221(self.assessmentName!)
        self.messageText = CommonStrings.Confirmation.CompletedFootnotePointsMessage119
    }
    
    public func configureCorrectState() {
        if (self.completionFlag == false) {
            configureForCompletedAssessment()
        } else {
            configureForUpdatedAssessment()
        }
    }
}
