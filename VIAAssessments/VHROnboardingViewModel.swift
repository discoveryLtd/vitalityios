import VitalityKit
import VitalityKit
import VIAUIKit

public class VHROnboardingViewModel: AssessmentOnboardingViewModel {

    internal let coreRealm = DataProvider.newRealm()

    public override var heading: String {
        return CommonStrings.Onboarding.Title2312
    }

    public override var alternateButtonTitle: String? {
        if VitalityProductFeature.isEnabled(.VHRDisclaimer) {
            return CommonStrings.DisclaimerTitle265
        }
        return nil
    }

    public override var contentItems: [OnboardingContentItem] {
        var items: [OnboardingContentItem] = [OnboardingContentItem]()
        items.append(OnboardingContentItem(heading: CommonStrings.Onboarding.Section1Title293,
                                           content: CommonStrings.Onboarding.Section1Message294,
                                           image: UIImage(asset: .vhrOnboardingEarnPoints)))
        items.append(OnboardingContentItem(heading: CommonStrings.Onboarding.Section2Title295,
                                           content: CommonStrings.Onboarding.Section2Message296,
                                           image: UIImage(asset: .vhrOnboardingVitalityAge)))
        items.append(OnboardingContentItem(heading: CommonStrings.Onboarding.Section3Title297,
                                           content: CommonStrings.Onboarding.Section3Message298,
                                           image: UIImage(asset: .vhrOnboardingCompleteTime)))
        return items
    }

    public override func setOnboardingHasShown() {
        AppSettings.setHasShownVHROnboarding()
    }
}
