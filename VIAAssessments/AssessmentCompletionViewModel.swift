import Foundation
import VIAUIKit
import VitalityKit
import VitalityKit

public class VHRCompletionViewModel: AnyObject, CirclesViewModel {

    public var headingText: String?
    public var messageText: String?
    public var footnoteText: String?
    public var buttonTitleText: String?

    var incompleteQuestionnaires: [VHRQuestionnaire]? {
        didSet {
            configureCorrectState()
        }
    }

    var currentSectionTitle: String
    var potentialPoints: Int

    let integerFormatter = NumberFormatter.integerFormatter()

    public var image: UIImage? {
        return UIImage(asset: .arOnboardingActivated)
    }

    public var heading: String? {
        return headingText
    }

    public var message: String? {
        return messageText
    }

    public var footnote: String? {
        return footnoteText
    }

    public var buttonTitle: String? {
        return buttonTitleText
    }
    
    public var buttonHighlightedTextColor: UIColor? {
        return UIColor.knowYourHealthGreen()
    }

    public var gradientColor: Color {
        return .green
    }

    init(incompleteQuestionnaires: [VHRQuestionnaire]?, sectionTitle: String, potentialPoints: Int) {
        self.incompleteQuestionnaires = incompleteQuestionnaires
        self.currentSectionTitle = sectionTitle
        self.potentialPoints = potentialPoints
        configureCorrectState()
    }

    func configureForCompleteQuestionnaires() {
        self.buttonTitleText = CommonStrings.GreatButtonTitle120
        self.headingText = CommonStrings.Completed.AllQuestionnairesCompletedTitle2209
        self.messageText = CommonStrings.CompleteScreen.SectionCompletedMessage2334
        self.footnoteText = CommonStrings.Confirmation.CompletedFootnotePointsMessage119
    }

    func configureForIncompleteQuestionnaires() {
        self.headingText = CommonStrings.Completed.SingleQuestionnaireCompletedTitle2211(currentSectionTitle)
        let count = incompleteQuestionnaires?.count
        if let totalRemainingQuestionnaires = count {
            if totalRemainingQuestionnaires == 1 {
                messageText = CommonStrings.Completed.SingleRemainingQuestionnairesEarnPointsMessage2212(integerFormatter.string(from: totalRemainingQuestionnaires as NSNumber) ?? "", integerFormatter.string(from: potentialPoints as NSNumber) ?? "")
            } else {
                messageText = CommonStrings.Completed.MultipleRemainingQuestionnairesEarnPointsMessage2210(integerFormatter.string(from: totalRemainingQuestionnaires as NSNumber) ?? "", integerFormatter.string(from: potentialPoints as NSNumber) ?? "")
            }
        }
    }

    public func configureCorrectState() {
        if let questionnaires = incompleteQuestionnaires, questionnaires.count > 0 {
            configureForIncompleteQuestionnaires()
        } else {
            configureForCompleteQuestionnaires()
        }
    }
}
