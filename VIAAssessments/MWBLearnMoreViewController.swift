//
//  MWBLearnMoreViewController.swift
//  VitalityActive
//
//  Created by Michelle R. Oratil on 20/02/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import VIAUIKit
import VitalityKit
import VIAUtilities
import VIACommon


public final class MWBLearnMoreViewController: VIATableViewController, KnowYourHealthTintable {
    
    let viewModel = MWBLearnMoreViewModel()
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = viewModel.title
        self.configureAppearance()
        self.configureTableView()
    }
    
    override public func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.hideBackButtonTitle()
    }
    
    func configureAppearance() {
        navigationController?.makeNavigationBarTransparent()
        navigationController?.navigationBar.barTintColor = UIColor.white
    }
    
    func configureTableView() {
        self.tableView.register(VIAGenericContentCell.nib(), forCellReuseIdentifier: VIAGenericContentCell.defaultReuseIdentifier)
        self.tableView.register(VIATableViewCell.nib(), forCellReuseIdentifier: VIATableViewCell.defaultReuseIdentifier)
        self.tableView.register(VIATableViewSectionHeaderFooterView.nib(), forHeaderFooterViewReuseIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier)
        self.tableView.estimatedRowHeight = 100
        self.tableView.sectionHeaderHeight = 0
        self.tableView.sectionFooterHeight = 0
        self.tableView.estimatedSectionHeaderHeight = 0
        self.tableView.contentInset = UIEdgeInsets(top: 25, left: 0, bottom: 0, right: 0)
        self.tableView.separatorInset = UIEdgeInsets(top: 0, left: 55, bottom: 0, right: 0)
        self.tableView.backgroundColor = .white
    }
    
    enum MWB_Sections: Int, EnumCollection {
        case Content = 0
        case Assessments = 1
        case Clickable = 2
    }
    
    enum AssessmentItems: Int, EnumCollection {
        case Stressor
        case Psychological
        case Social
        
        func title() -> String? {
            switch self {
            case .Stressor:
                return CommonStrings.Mwb.LearnMoreStressor1Title1212
            case .Psychological:
                return CommonStrings.Mwb.LearnMorePsychological1Title1211
            case .Social:
                return CommonStrings.Mwb.LearnMoreSocial1Title1213
            }
            
        }
        
        func iconName() -> UIImage {
            switch self {
            case .Stressor:
                return VIAAssessmentsAsset.Mwb.mwbLearnMoreStressor.image
            case .Psychological:
                return VIAAssessmentsAsset.Mwb.mwbLearnMorePsychological.templateImage
            case .Social:
                return VIAAssessmentsAsset.Mwb.mwbLearnMoreSocial.templateImage
            }
        }
    }

    enum ClickableItems: Int, EnumCollection {
        case Disclaimer
        
        func title() -> String? {
            switch self {
            case .Disclaimer:
                return CommonStrings.DisclaimerTitle265
            }
        
        }
        
        func iconName() -> UIImage {
            switch self {
            case .Disclaimer:
                return VIAAssessmentsAsset.Mwb.mwbLearnMoreTaC.templateImage
            }
        }
    }
    
    // MARK: - UITableView datasource
    
    override public func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let section = MWB_Sections(rawValue: indexPath.section)
        if section == .Content && indexPath.row != 3 {
            cell.separatorInset = UIEdgeInsetsMake(0, max(UIScreen.main.bounds.size.width, UIScreen.main.bounds.size.height), 0, 0)
        } else if section == .Content && indexPath.row == 3 {
            cell.preservesSuperviewLayoutMargins = false
            cell.separatorInset = UIEdgeInsets.zero
            cell.layoutMargins = UIEdgeInsets.zero
        }
    }
    
    override public func numberOfSections(in tableView: UITableView) -> Int {
        return MWB_Sections.allValues.count
    }
    
    override public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let section = MWB_Sections(rawValue: section)
        if section == .Content {
            return viewModel.contentItems.count
        } else if section == .Assessments {
            return 3
        }  else if section == .Clickable {
            return AppSettings.hideDSConsent() ? 0 : 1
        }
        return 0
    }
    
    // MARK: UITableView delegate
    
    override public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let section = MWB_Sections(rawValue: indexPath.section)
        if section == .Content {
            return self.configureContentCell(indexPath)
        } else if section == .Assessments {
            return self.configureAssessmentItemsCell(indexPath)
        } else if section == .Clickable {
            return self.configureClickableItemCell(indexPath)
        }
        
        return tableView.dequeueReusableCell(withIdentifier: VIATableViewCell.defaultReuseIdentifier, for: indexPath)
    }
    
    override public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let section = MWB_Sections(rawValue: indexPath.section)
        if section == .Assessments{
            switch indexPath.row {
            case 0:
                showStressor(self)
                break
            case 1:
                showPsychological(self)
                break
            case 2:
                showSocial(self)
                break
            default:
                break
            }
        } else if section == .Clickable {
            switch indexPath.row {
            case 0:
                showDisclaimer(self)
                break
            default:
                break
            }
        }
}
    
    func configureContentCell(_ indexPath: IndexPath) -> VIAGenericContentCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: VIAGenericContentCell.defaultReuseIdentifier, for: indexPath) as! VIAGenericContentCell
        cell.isUserInteractionEnabled = false
        
        let item = viewModel.contentItems[indexPath.row]
        
        if indexPath.row == 0 {
            cell.customHeaderFont = viewModel.headingTitleFont
            cell.customContentFont = viewModel.headingMessageFont
        } else {
            cell.customHeaderFont = viewModel.sectionTitleFont
            cell.customContentFont = viewModel.sectionMessageFont
        }
        cell.header = item.header
        cell.content = item.content
        cell.cellImage = item.image
        cell.cellImageTintColor = viewModel.imageTint
        cell.layoutIfNeeded()
        return cell
    }
    
    func configureAssessmentItemsCell(_ indexPath: IndexPath) -> VIATableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: VIATableViewCell.defaultReuseIdentifier, for: indexPath) as! VIATableViewCell
        let assessment = AssessmentItems.allValues[indexPath.row]
        
        cell.textLabel?.text = assessment.title()
        cell.imageView?.image = assessment.iconName()
        cell.accessoryType = .disclosureIndicator
        cell.preservesSuperviewLayoutMargins = false
        cell.separatorInset = UIEdgeInsets.zero
        cell.layoutMargins = UIEdgeInsets.zero
        cell.imageView?.tintColor = viewModel.imageTint
        return cell
    }
    
    func configureClickableItemCell(_ indexPath: IndexPath) -> VIATableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: VIATableViewCell.defaultReuseIdentifier, for: indexPath) as! VIATableViewCell
        let content = ClickableItems.allValues[indexPath.row]
        
        cell.textLabel?.text = content.title()
        cell.imageView?.image = content.iconName()
        cell.accessoryType = .disclosureIndicator
        cell.imageView?.tintColor = viewModel.imageTint
        return cell
    }
    
    public override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        let sectionValue = MWB_Sections(rawValue: section)
        if sectionValue == .Assessments {
            return 40
        }
        return UITableViewAutomaticDimension
    }

    override public func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return UITableViewAutomaticDimension
    }

    public override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let theSection = MWB_Sections(rawValue: section)
        if theSection == .Assessments {
            let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier) as! VIATableViewSectionHeaderFooterView
            view.labelText = CommonStrings.Mwb.LandingSectionHeader1210.localizedUppercase
            return view
        }
        return nil
    }
    
    override public func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let theSection = MWB_Sections(rawValue: section)
        if theSection == .Content {
            let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier) as! VIATableViewSectionHeaderFooterView
            return view
        }
        return nil
    }
    
    public override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let section = MWB_Sections(rawValue: indexPath.section)
        if section == .Assessments || section == .Clickable {
            return 44
        } else {
            return UITableViewAutomaticDimension
        }
    }
    
    func showDisclaimer(_ sender: Any) {
        self.performSegue(withIdentifier: "showDisclaimer", sender: self)
    }
    
    func showStressor(_ sender: Any) {
        self.performSegue(withIdentifier: "showStressor", sender: self)
    }
    
    func showPsychological(_ sender: Any) {
        self.performSegue(withIdentifier: "showPsychological", sender: self)
    }
    
    func showSocial(_ sender: Any) {
        self.performSegue(withIdentifier: "showSocial", sender: self)
    }
    
    override public func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showStressor", let controller = segue.destination as? VIAWebContentViewController {
            let webContentViewModel = VIAWebContentViewModel(articleId: "mwb-stressor-content")
            controller.viewModel = webContentViewModel
            controller.title = CommonStrings.Mwb.LearnMoreStressor1Title1212
        } else if segue.identifier == "showPsychological", let controller = segue.destination as?
            VIAWebContentViewController {
            let webContentViewModel = VIAWebContentViewModel(articleId: "mwb-psychological-content")
            controller.viewModel = webContentViewModel
            controller.title = CommonStrings.Mwb.LearnMorePsychological1Title1211
        } else if segue.identifier == "showSocial", let controller = segue.destination as?
            VIAWebContentViewController {
            let webContentViewModel = VIAWebContentViewModel(articleId: "mwb-social-content")
            controller.viewModel = webContentViewModel
            controller.title = CommonStrings.Mwb.LearnMoreSocial1Title1213
        } else if segue.identifier == "showDisclaimer", let controller = segue.destination as? VIAWebContentViewController {
            let webContentViewModel = VIAWebContentViewModel(articleId: "mwb-disclaimer-content")
            controller.viewModel = webContentViewModel
            controller.title = CommonStrings.DisclaimerTitle265
        }
    }
}
