import IGListKit
import VIAUIKit
import VIAUtilities
import VitalityKit
import VIACommon

public final class AssessmentNavigationButtonSectionController: IGListSectionController, IGListSectionType {

    // MARK: Properties

    var detail: AssessmentQuestionnaireSectionNavigationDetail!

    // MARK: IGListSectionType

    public func numberOfItems() -> Int {
        return 1
    }

    public func didUpdate(to object: Any) {
        self.detail = object as! AssessmentQuestionnaireSectionNavigationDetail
    }

    public func cellForItem(at index: Int) -> UICollectionViewCell {
        let cell = collectionContext!.dequeueReusableCell(of: AssessmentNavigationButtonCollectionViewCell.self, for: self, at: index) as! AssessmentNavigationButtonCollectionViewCell

        cell.button.setTitle(CommonStrings.NextButtonTitle84, for: .normal)
        if detail.isLastSection {
            cell.button.setTitle(CommonStrings.DoneButtonTitle53, for: .normal)
        }

        cell.buttonTapped = self.buttonTapped
        if(VIAApplicableFeatures.default.showRequiredPrompt!){
            cell.button.isEnabled = true
        }else{
            cell.button.isEnabled = self.detail.isEnabled
        }
        return cell
    }

    public func sizeForItem(at index: Int) -> CGSize {
        let width = collectionContext!.containerSize.width
        return AssessmentNavigationButtonCollectionViewCell.height(constrainedTo: width)
    }

    public func didSelectItem(at index: Int) { }

    func buttonTapped() {
        if (VIAApplicableFeatures.default.showRequiredPrompt!) {
            /**
             * For SLI Market, when next is clicked, "Required" header should be displayed
             * if no answer is provided.
             */
            let nextStatus:[String: Bool] = ["isNextButtonTapped": true]
            NotificationCenter.default.post(name: .VIAVHRQuestionnaireNextNotification, object: nil, userInfo: nextStatus)
            
            if (self.detail.isEnabled) {
                if let controller = self.viewController as? AssessmentQuestionnaireSectionViewController {
                    controller.next(nil)
                }
            } else {
                //TODO: Scrollup to unanswered questions
                if let controller = self.viewController as? AssessmentQuestionnaireSectionViewController {
                    //controller.collectionView.scrollToTop(animated: false)
                    //controller.collectionView.scrollToItem(at: 4, at: .centeredHorizontally, animated: false)
                    //.scrollToItem(at: startingIndexPath, at: .centeredHorizontally, animated: false)
                    //controller.collectionView.scrollToItem(at:IndexPath(item: 4, section: 0), at: .centeredHorizontally, animated: false)
                }
            }
        } else {
            if let controller = self.viewController as? AssessmentQuestionnaireSectionViewController {
                controller.next(nil)
            }
        }
    }
}

public final class AssessmentNavigationButtonCollectionViewCell: UICollectionViewCell, Nibloadable {

    private static let buttonHeight: CGFloat = 44.0

    internal var buttonTapped: () -> Void = {
        debugPrint("AssessmentNavigationButtonCollectionViewCell button tapped")
    }

    // MARK: Views

    public lazy var button: VIAButton = {
        let button = VIAButton(title: CommonStrings.NextButtonTitle84)
        button.tintColor = UIColor.currentGlobalTintColor()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(buttonTapped(_:)), for: .touchUpInside)
        return button
    }()

    // MARK: Init

    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override init(frame: CGRect) {
        super.init(frame: frame)

        setupSubviews()
    }

    public override func layoutSubviews() {
        super.layoutSubviews()
    }

    private func setupSubviews() {
        contentView.backgroundColor = UIColor.mainViewBackground()

        contentView.addSubview(button)
        button.snp.makeConstraints { (make) in
            make.top.bottom.equalToSuperview()
            make.width.equalToSuperview().multipliedBy(0.6)
            make.centerX.equalToSuperview()
            make.height.equalTo(AssessmentNavigationButtonCollectionViewCell.buttonHeight)
        }
    }

    override public func preferredLayoutAttributesFitting(_ layoutAttributes: UICollectionViewLayoutAttributes) -> UICollectionViewLayoutAttributes {
        return self.defaultContentViewLayoutSizeFitting(layoutAttributes)
    }

    // MARK: Height calculation

    public class func height(constrainedTo width: CGFloat) -> CGSize {
        let height = AssessmentNavigationButtonCollectionViewCell.buttonHeight
        return CGSize(width: width, height: ceil(height))
    }

    // MARK: Actions

    func buttonTapped(_ sender: Any?) {
        buttonTapped()
    }

}
