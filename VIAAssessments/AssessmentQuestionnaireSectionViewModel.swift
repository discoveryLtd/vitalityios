import Foundation
import VitalityKit
import VIAUtilities
import VIAUIKit
import VitalityKit
import RealmSwift
import IGListKit
import VIACommon

public protocol AssessmentQuestionnaireSectionViewModelDelegate: class {
    func viewModelDidUpdateData()
    func viewModelDidUpdateCapturedResults()
    func reloadSectionController(for questionDetail: AssessmentQuestionPresentationDetail)
    func scrollTo(questionDetail: AssessmentQuestionPresentationDetail)
}

public class AssessmentQuestionnaireSectionViewModel {

    // MARK: Properties

    weak var delegate: AssessmentQuestionnaireSectionViewModelDelegate?

    var questionnaireSection: VHRQuestionnaireSection
    
    var nextButtonStatus: Bool = false
    
    var unansweredQuestionsDetail = [AssessmentQuestionPresentationDetail]()

    var isLastSection: Bool = false {
        didSet {
            updateData()
        }
    }

    public private(set) var realm = DataProvider.newVHRRealm()

    var visibleQuestionsNotificationToken: NotificationToken?

    var capturedResultsNotificationToken: NotificationToken?

    private lazy var visibleQuestions: Results<VHRQuestion> = {
        return self.questionnaireSection.sortedVisibleQuestions()
    }()

    private lazy var capturedResults: Results<VHRCapturedResult> = {
        return self.realm.vhrCapturedResults(for: self.questionnaireSection.typeKey)
    }()

    lazy var navigationDetail: AssessmentQuestionnaireSectionNavigationDetail = {
        return AssessmentQuestionnaireSectionNavigationDetail(isLastSection: self.isLastSection, isEnabled: self.isValid())
    }()

    var data: [IGListDiffable] = []{
        didSet{
            self.delegate?.viewModelDidUpdateData()
        }
    }

    // MARK: Lifecycle
    
    deinit {
        debugPrint("VHRQuestionnaireSectionViewModel deinit")
    }

    required public init?(questionnaireSectionTypeKey: Int?, isLastSection lastSection: Bool) {
        guard let section = realm.vhrQuestionnaireSection(by: questionnaireSectionTypeKey ?? -999) else {
//            debugPrint("Couldn't find VHR questionnaire section to return")
            return nil
        }
        
        questionnaireSection = section
        isLastSection = lastSection
        updateData()
        registerForNotifications()
        
    }

    func registerForNotifications() {
        visibleQuestionsNotificationToken = visibleQuestions.addNotificationBlock { [weak self] (changes: RealmCollectionChange) in
            self?.visibleQuestionsDidChange(changes)
        }

        capturedResultsNotificationToken = capturedResults.addNotificationBlock { [weak self] (changes: RealmCollectionChange) in
            self?.capturedResultsDidChange(changes)
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.isNextButtonTapped(_:)), name: .VIAVHRQuestionnaireNextNotification, object: nil)
    }

    func visibleQuestionsDidChange(_ changes: RealmCollectionChange<Results<VHRQuestion>>) {
        updateData()
        self.delegate?.viewModelDidUpdateData()
    }

    func capturedResultsDidChange(_ changes: RealmCollectionChange<Results<VHRCapturedResult>>) {
        updateNavigation()
        self.delegate?.viewModelDidUpdateCapturedResults()

        switch changes {
        case .update(let results, let deletions, let insertions, let modifications):
            updateSelectQuestionControllers(results: results, deletions: deletions, insertions: insertions, modifications: modifications)
            break
        default:
            break
        }
        
        unansweredQuestionsDetail = [AssessmentQuestionPresentationDetail]()
    }

    func updateSelectQuestionControllers(results: Results<VHRCapturedResult>, deletions: [Int], insertions: [Int], modifications: [Int]) {
        // determine which change was made
        var altered: Int?
        if deletions.count > 0 {
            altered = deletions.first
        } else if insertions.count > 0 {
            altered = insertions.first
        } else if modifications.count > 0 {
            altered = modifications.first
        }
        guard let validAltered = altered else { //debugPrint("Couldn't figure it out");
            return
        }
        guard results.count > validAltered else { return }

        // filter the question controllers down to only questions
        // that have single or multi select decorations
        guard let questionPresentationDetails = self.data.filter({ $0 is AssessmentQuestionPresentationDetail }) as? [AssessmentQuestionPresentationDetail] else { return }
        let singleMultiSelectableQuestions = questionPresentationDetails.filter({ [.Toggle, .Checkbox, .RadioButton].contains($0.decorator) })

        // get the onscreen presentation detail for the question that was
        // just answered via its captured result
        let capturedResult: VHRCapturedResult = results[validAltered]
        guard let question = capturedResult.realm?.question(by: capturedResult.questionTypeKey) else { return }
        guard let detail = singleMultiSelectableQuestions.filter({ $0.rawQuestionTypeKey == question.typeKey }).first else { return }

        self.delegate?.reloadSectionController(for: detail)
    }

    func updateNavigation() {
        self.navigationDetail.isLastSection = self.isLastSection
        self.navigationDetail.isEnabled = isValid()
    }
    
    
    /*
     * Reload collectionContext when Next button is tapped
     * so that "Required" header will appear
     * This will set the nextButtonStatus
     */
    @objc func isNextButtonTapped(_ notification: NSNotification) {
        if let isTapped = notification.userInfo?["isNextButtonTapped"] as? Bool {
            nextButtonStatus = isTapped
            updateData()
            if let qDetail = unansweredQuestionsDetail.first{
                self.delegate?.scrollTo(questionDetail:qDetail)
            }
        }
    }
    
    func updateData() {
//        print("ge-->VHR: Update data on UI")
        let interQuestionPadding: CGFloat = 30.0

        var data = [IGListDiffable]()
        //data.append(SpacerDetail(interQuestionPadding))
        // section header textDescription
        // note: due to the `text` property not being part of the design but in the model,
        // we're explicitly omitting it on the frontend. we're rather using the `textDescription`
        // property to replace the `text` property to be in line with Android and the web.
        
        if !questionnaireSection.textDescription.isEmpty {
            data.append(SpacerDetail(interQuestionPadding))
            if (VIAApplicableFeatures.default.showRequiredPrompt! && !questionnaireSection.isValidExclusive() && questionnaireSection.typeCode == "MedicalHistory" && nextButtonStatus){
                data.append(AssessmentRequiredPrompt(key: questionnaireSection.typeKey))
            }
            let detail = TextDetail(text: questionnaireSection.textDescription, font: .headlineFont(), textColor: .night())
            data.append(AssessmentQuestionnaireSectionHeaderFooterDetail(text: detail, textDescription: nil))
        }

        // each section question
        for question in visibleQuestions {
            
            //ge20180113 : if question has a parent question, no need to add padding
            if(question.isChild == false){
                data.append(SpacerDetail(interQuestionPadding))
                if question.childQuestions.count == 0 && nextButtonStatus  && question.capturedResult()?.answer == nil && VIAApplicableFeatures.default.showRequiredPrompt!{
                    data.append(AssessmentRequiredPrompt(key: questionnaireSection.typeKey))
                }

            }
            
            //ge20180417 : Add requiredPrompt for multipart Q's
            if (VIAApplicableFeatures.default.showRequiredPrompt! && question.childQuestions.count > 0 && !questionnaireSection.isValidMultipart(parentTypeCode: question.typeCode) && nextButtonStatus){
                data.append(AssessmentRequiredPrompt(key: questionnaireSection.typeKey))
            }
            
            // question
            let questionTextDetail = TextDetail(text: question.text, font: .headlineFont(), textColor: .night())
            // question description
            var questionTextDescriptionDetail: TextDetail?
            if !question.textDescription.isEmpty {
                questionTextDescriptionDetail = TextDetail(text: question.textDescription, font: .subheadlineFont(), textColor: .mediumGrey())
            }
            // put it all together
            let questionDetail = AssessmentQuestionPresentationDetail(text: questionTextDetail,
                                                               textDescription: questionTextDescriptionDetail,
                                                               presentationType: question.questionType,
                                                               decorator: question.questionDecorator?.type ?? .Unknown,
                                                               isVisible: question.isVisible,
                                                               questionTypeKey: question.typeKey)

            // valid values for question
            if question.validValues.count > 0 {
                var validValuesPresentationDetail = [AssessmentValidValuePresentationDetail]()
                for validValue in question.validValues {
                    // valid value
                    let answerTextDetail = TextDetail(text: validValue.value, font: .bodyFont(), textColor: .night())
                    // valid value description
                    var answerTextDescriptionDetail: TextDetail?
                    if !validValue.valueDescription.isEmpty {
                        answerTextDescriptionDetail = TextDetail(text: validValue.valueDescription, font: .subheadlineFont(), textColor: .mediumGrey())
                    }
                    // valid value note that shows when selected
                    var answerTextNoteDetail: TextDetail?
                    if !validValue.valueNote.isEmpty {
                        answerTextNoteDetail = TextDetail(text: validValue.valueNote, font: .subheadlineFont(), textColor: .mediumGrey())
                    }
                    // put it all together
                    let answer = AssessmentValidValuePresentationDetail(text: answerTextDetail,
                                                                        textDescription: answerTextDescriptionDetail,
                                                                        textNote: answerTextNoteDetail,
                                                                        rawValidValue: validValue)
                    validValuesPresentationDetail.append(answer)
                }
                questionDetail.validValuesPresentationDetail = validValuesPresentationDetail
            } else {
//                debugPrint("No valid values for the question")
            }

            // append the question detail
            data.append(questionDetail)

            // source for prepop if available
            var prepopEventRef: EventTypeRef = .Unknown
            if let capturedResult = question.capturedResult() {
//                debugPrint("Have captured result for question: \(question.text)")
                if let prepopulationEventKeyValue = capturedResult.prepopulationEventKey.value {
//                    debugPrint("Have prepopulationEventKeyValue: \(prepopulationEventKeyValue)")
                    prepopEventRef = EventTypeRef(rawValue: prepopulationEventKeyValue) ?? .Unknown
//                    debugPrint("prepopEventRef set to \(prepopEventRef.rawValue)")
//                    debugPrint("capturedResult.dateCaptured: \(capturedResult.dateCaptured)")
//                    debugPrint("full capturedResult: \(capturedResult)")
                    if prepopEventRef != .Unknown {
//                        debugPrint("prepopEventRef != Unknown, creating prepopTextDetail")
                        let formattedDate = Localization.fullDateFormatter.string(from: capturedResult.dateCaptured)
                        let prepopText = CommonStrings.Assessment.Vhr.PrePopulationDetails570(prepopEventRef.asSource(), formattedDate)
                        let textDetail = TextDetail(text: prepopText, font: .footnoteFont(), textColor: .darkGrey())
                        let prepopulationDetail = AssessmentQuestionPrepopulationPresentationDetail(text: textDetail)
//                        debugPrint("prepopTextDetail: \(prepopulationDetail)")
                        data.append(prepopulationDetail)
                    } else {
//                        debugPrint("Unknown prepop event, fail")
                    }
                } else {
//                    debugPrint("Couldn't obtain prepopulationEventKeyValue")
                }
            } else {
//                debugPrint("No capturedResult for question: \(question.text)")
                unansweredQuestionsDetail.append(questionDetail)
            }

            // expandable question footnote
            if !question.textNote.isEmpty {
                let textDetail = TextDetail(text: question.textNote, font: .footnoteFont(), textColor: .darkGrey())
                let questionFootNoteDetail = AssessmentQuestionFootnotePresentationDetail(text: textDetail)

                // add extra padding if there's a prepop note
                if prepopEventRef != .Unknown {
                    data.append(SpacerDetail(7.0)) // this 7 plus the topInset of the footnote adds to 15, which the design requires
                }
                // append footnote
                data.append(questionFootNoteDetail)
            }

            // spacing
            //data.append(SpacerDetail(interQuestionPadding))
        }

        // section footer text
        if !questionnaireSection.textNote.isEmpty {
            data.append(SpacerDetail(interQuestionPadding))
            let sectionTextNoteDetail = TextDetail(text: questionnaireSection.textNote, font: .headlineFont(), textColor: .night())
            data.append(AssessmentQuestionnaireSectionHeaderFooterDetail(text: sectionTextNoteDetail))
        }

        // navigation button
        data.append(SpacerDetail(interQuestionPadding))
        self.updateNavigation()
        data.append(navigationDetail)
        data.append(SpacerDetail(interQuestionPadding))

        self.data = data
    }

    // MARK: Validation

    func isValid() -> Bool {
        return VIAApplicableFeatures.default.isValid(questionnaireSection: questionnaireSection)

    }

}
