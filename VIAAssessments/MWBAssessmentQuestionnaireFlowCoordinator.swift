//
//  MWBAssessmentQuestionnaireFlowCoordinator.swift
//  VitalityActive
//
//  Created by Michelle R. Oratil on 14/02/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import VitalityKit
import VIAUIKit
import RealmSwift
import VIACommon
import VIAUtilities

protocol MWBAssessmentSubmitter: class {
    func submit(for viewController: VIAViewController?, with completion:  @escaping (Error?) -> ())
}

class MWBAssessmentQuestionnaireFlowCoordinator {
    
    // MARK: - Properties
    
    var navigationController: MWBAssessmentNavigationController!
    
    var selectedQuestionnaire: MWBQuestionnaire?
    
    var potentialPoints: Int
    
    var currentSectionIndex = 0
    
    var realm = DataProvider.newMWBRealm()
    
    var shouldReloadWhenLandingViewIsPresented: Bool = false
    
    var selectedAssessment: String?
    
    var completionFlag: Bool?
    
    var totalVisibleSections: Int {
        var count = 0
        guard let questionnaire = self.selectedQuestionnaire else { return count }
        for section in questionnaire.questionnaireSections {
            if section.isVisible {
                count = count + 1
            }
        }
        return count
    }
    
    var currentSectionTitle: String {
        return self.selectedQuestionnaire?.typeName ?? ""
    }
    
    var currentQuestionnaireSection: MWBQuestionnaireSection? {
        return selectedQuestionnaire?.sortedVisibleQuestionnaireSections()[currentSectionIndex]
    }
    
    var currentQuestionnaireTypeKey: Int?
    
    // MARK: - Init
    
    deinit {
        debugPrint("MWBQuestionnaireFlowCoordinator deinit")
    }
    
    init(with navigationController: MWBAssessmentNavigationController, selectedQuestionnaire: MWBQuestionnaire?, potentialPoints: Int) {
        self.selectedQuestionnaire = selectedQuestionnaire
        self.navigationController = navigationController
        self.potentialPoints = potentialPoints
        setCurrentSectionIndex()
        configureNavigationBar()
        self.currentQuestionnaireTypeKey = selectedQuestionnaire?.typeKey
    }
    
    // MARK: - Configuration
    
    func start() {
        configureTopViewController()
    }
    
    func configureTopViewController() {
        if let rootViewController = self.navigationController?.topViewController as? MWBAssessmentQuestionnaireSectionViewController {
            configureQuestionnaireSectionViewController(rootViewController)
            configureNavigationStack(topViewController: rootViewController)
        }
    }
    
    func configureMWBDataSharingConsentViewControllerDelegate() {
        if let rootViewController = self.navigationController?.topViewController as? MWBAssessmentDataSharingConsentViewController {
            rootViewController.navigationDelegate = self
            rootViewController.submitter = self
        }
    }
    
    func MWBconfigureAssessmentCompletionViewControllerDelegate() {
        if let rootViewController = self.navigationController?.topViewController as? MWBAssessmentCompletionViewController {
            rootViewController.navigationDelegate = self
            rootViewController.currentQuestionnaireTypeKey = self.currentQuestionnaireTypeKey
        }
    }
    
    func configureNavigationStack(topViewController: MWBAssessmentQuestionnaireSectionViewController) {
        let preceedingViewControllers = getAllPreceedingSectionsViewControllers()
        
        if preceedingViewControllers.count == 0 {
            self.navigationController.setViewControllers([topViewController], animated: true)
        } else {
            self.navigationController.setViewControllers([], animated: false)
            for viewController in preceedingViewControllers {
                self.navigationController.pushViewController(viewController, animated: false)
            }
            self.navigationController.pushViewController(topViewController, animated: true)
        }
    }
    
    func setCurrentSectionIndex() {
        currentSectionIndex = 0
        if let currentSectionTypeKey = selectedQuestionnaire?.currentSectionTypeKey {
            guard let section = realm.mwbQuestionnaireSection(by: currentSectionTypeKey) else { return }
            guard let sortedVisibleSections = selectedQuestionnaire?.sortedVisibleQuestionnaireSections() else { return }
            if let index = sortedVisibleSections.index(of: section) {
                currentSectionIndex = index
            }
        }
    }
    
    func configureQuestionnaireSectionViewController(_ viewController: MWBAssessmentQuestionnaireSectionViewController) {
        guard let questionnaireSection = currentQuestionnaireSection else { return }
        
        viewController.title = selectedQuestionnaire?.typeName
        viewController.navigationDelegate = self
        viewController.answeringDelegate = self
        
        self.navigationController.sectionTitle = questionnaireSection.typeName
        let isFirstSection = currentSectionIndex == 0
        let isLastSection = currentSectionIndex == totalVisibleSections - 1
        let questionnaireSectionTypeKey = questionnaireSection.typeKey
        viewController.configure(isFirstSection: isFirstSection,
                                 isLastSection: isLastSection,
                                 questionnaireSectionTypeKey: questionnaireSectionTypeKey)
    }
    
    func configureQuestionnaireSectionViewControllerFor(section: MWBQuestionnaireSection, index: Int) -> MWBAssessmentQuestionnaireSectionViewController? {
        guard let storyboard = self.navigationController?.storyboard else { return nil }
        guard let viewController = storyboard.instantiateViewController(withIdentifier: "MWBAssessmentQuestionnaireSectionViewController") as? MWBAssessmentQuestionnaireSectionViewController else { return nil }
        let isFirstSection = index == 0
        let isLastSection = false
        let questionnaireSectionTypeKey = section.typeKey
        viewController.configure(isFirstSection: isFirstSection,
                                 isLastSection: isLastSection,
                                 questionnaireSectionTypeKey: questionnaireSectionTypeKey)
        return viewController
    }
    
    func configureNavigationBar() {
        navigationController.currentSection = currentSectionIndex + 1
        navigationController.totalSections = totalVisibleSections
        if let questionnaireSection = currentQuestionnaireSection {
            navigationController.sectionIcon = questionnaireSection.image()
            navigationController.sectionTitle = questionnaireSection.typeName
        }
        navigationController.configureNavigationBar()
    }
    
    func configureWithUpdatedVisibleSections(mwbQuestionnaireSectionViewController: MWBAssessmentQuestionnaireSectionViewController) {
        configureNavigationBar()
        mwbQuestionnaireSectionViewController.viewModel?.isLastSection = currentSectionIndex == totalVisibleSections - 1
    }
    
    // MARK: - MWB Coordinator Navigation Functions
    
    func goToNextSection() {
        currentSectionIndex += 1
        configureNavigationBar()
        
        guard let storyboard = self.navigationController?.storyboard else { return }
        guard let viewController = storyboard.instantiateViewController(withIdentifier: "MWBAssessmentQuestionnaireSectionViewController") as? MWBAssessmentQuestionnaireSectionViewController else { return }
        
        configureQuestionnaireSectionViewController(viewController)
        navigationController.pushViewController(viewController, animated: true)
    }
    
    func goToNextQuestionnaire() {
        configureNavigationBar()
        guard let storyboard = self.navigationController?.storyboard else { return }
        guard let viewController = storyboard.instantiateViewController(withIdentifier: "MWBAssessmentQuestionnaireSectionViewController") as? MWBAssessmentQuestionnaireSectionViewController else { return }
        configureQuestionnaireSectionViewController(viewController)
        configureNavigationStack(topViewController: viewController)
    }
    
    func getAllPreceedingSectionsViewControllers() -> [MWBAssessmentQuestionnaireSectionViewController] {
        var preceedingViewControllers = [MWBAssessmentQuestionnaireSectionViewController]()
        if currentSectionIndex != 0 {
            let preceedingSections = getAllPreceedingSections()
            for section in preceedingSections {
                if let index = preceedingSections.index(of: section) {
                    if let viewController = configureQuestionnaireSectionViewControllerFor(section: section, index: index) {
                        preceedingViewControllers.append(viewController)
                    }
                }
            }
        }
        return preceedingViewControllers
    }
    
    public func getAllPreceedingSections() -> [MWBQuestionnaireSection] {
        var index = 0
        var preceedingSections = [MWBQuestionnaireSection]()
        if let questionnaire = selectedQuestionnaire {
            let sortedVisibleSections = questionnaire.sortedVisibleQuestionnaireSections()
            while index < currentSectionIndex {
                preceedingSections.append(sortedVisibleSections[index])
                index += 1
            }
        }
        return preceedingSections
    }
    
    func showMWBDataSharingConsentViewController() {
        guard let storyboard = self.navigationController?.storyboard else { return }
        guard let viewController: MWBAssessmentDataSharingConsentViewController = storyboard.instantiateViewController(withIdentifier: "MWBAssessmentDataSharingConsentViewController") as? MWBAssessmentDataSharingConsentViewController else { return }
        viewController.questionnaire = self.selectedQuestionnaire
        self.navigationController?.pushViewController(viewController, animated: true)
        configureMWBDataSharingConsentViewControllerDelegate()
    }
    
    func showCompleteViewController() {
        guard let storyboard = self.navigationController?.storyboard else { return }
        guard let viewController: MWBAssessmentCompletionViewController = storyboard.instantiateViewController(withIdentifier: "MWBAssessmentCompletionViewController") as? MWBAssessmentCompletionViewController else { return }
        
        viewController.mwbViewModel?.getAssessmentCompletionStatus(completionFlag: self.completionFlag!, assessmentName: self.selectedAssessment!)
        
        let incompleteQuestionnaires = realm.incompleteMWBQuestionnaires()
            viewController.mwbViewModel = MWBCompletionViewModel(incompleteQuestionnaires: incompleteQuestionnaires, sectionTitle: currentSectionTitle, potentialPoints: potentialPoints, assessmentName: self.selectedAssessment!, completionFlag: self.completionFlag!)

        self.navigationController.setViewControllers([viewController], animated: true)
        MWBconfigureAssessmentCompletionViewControllerDelegate()
    }
    
    func getPsycAssessment(completionFlag: Bool, assessmentName: String){
        self.selectedAssessment = assessmentName
        self.completionFlag = completionFlag
    }
    
}

// MARK: - Delegates

extension MWBAssessmentQuestionnaireFlowCoordinator: MWBAssessmentQuestionnaireSectionViewControllerDelegate {
    
    func sectionControllerDidSelectNext(_ mwbQuestionnaireSectionViewController: MWBAssessmentQuestionnaireSectionViewController) {
        if currentSectionIndex < totalVisibleSections - 1 {
            goToNextSection()
        } else {
            sectionControllerDidSelectDone(mwbQuestionnaireSectionViewController)
        }
    }
    
    func sectionControllerDidSelectBack(_ mwbQuestionnaireSectionViewController: MWBAssessmentQuestionnaireSectionViewController) {
        if currentSectionIndex > 0 {
            currentSectionIndex -= 1
            self.navigationController?.popViewController(animated: true)
            if let topView = navigationController.topViewController as? MWBAssessmentQuestionnaireSectionViewController {
                configureQuestionnaireSectionViewController(topView)
            }
            
            configureNavigationBar()
        }
    }
    
    func sectionControllerDidSelectClose(_ mwbQuestionnaireSectionViewController: MWBAssessmentQuestionnaireSectionViewController) {
        self.navigationController?.topViewController?.performSegue(withIdentifier: "unwindFromQuestionnaireSectionViewControllerToAssessmentLanding", sender: nil)
    }
    
    func sectionControllerDidSelectDone(_ controller: MWBAssessmentQuestionnaireSectionViewController) {
        let tenantId = DataProvider.newRealm().getTenantId()
        if VitalityProductFeature.isEnabled((tenantId == 25) ? .VHCDSConsent : .VHRDSConsent) {
            self.showMWBDataSharingConsentViewController()
        } else {
            controller.showHUDOnWindow()
            self.submit(for: controller, with: { [weak self] error in
                controller.hideHUDFromWindow()
                if let error = error {
                    controller.handleErrorWithAlert(error: error, tryAgain: { [weak self] error in
                        self?.sectionControllerDidSelectDone(controller)
                    })
                    return
                }
                self?.showCompleteViewController()
            })
        }
    }
}

extension MWBAssessmentQuestionnaireFlowCoordinator: MWBAssessmentDataSharingConsentViewControllerDelegate {
    
    func MWBdataSharingConsentControllerDidSelectAgree() {
        showCompleteViewController()
    }
    
    func MWBdataSharingConsentControllerDidSelectDisagree(_ controller: MWBAssessmentDataSharingConsentViewController) {
        self.navigationController?.popViewController(animated: true)
        if (navigationController.topViewController as? MWBAssessmentQuestionnaireSectionViewController) != nil {
            navigationController?.navigationBar.isHidden = false
            navigationController?.makeNavigationBarTransparent()
            configureNavigationBar()
        }
    }
}

extension MWBAssessmentQuestionnaireFlowCoordinator: MWBAssessmentCompletionViewControllerDelegate {
    
    func completionControllerDidSelectDone(_ completionViewController: MWBAssessmentCompletionViewController) {
        /*
         note: If you are to change the segue indentifier string,
         kindly change also the string used in MWBAssessmentCompletionViewController. Thanks!
         */
        self.navigationController?.topViewController?.performSegue(withIdentifier: "unwindFromCompletionViewControllerToAssessmentLanding", sender: nil)
    }
    
    func completionControllerDidSelectNewQuestionnaire(_ completionViewController: MWBAssessmentCompletionViewController, selectedQuestionnaireTypeKey: Int) {
        selectedQuestionnaire = realm.getMWBQuestionnaire(using: selectedQuestionnaireTypeKey)
        setCurrentSectionIndex()
        goToNextQuestionnaire()
    }
}

// MARK : Submitter

extension MWBAssessmentQuestionnaireFlowCoordinator: MWBAssessmentSubmitter {
    func submit(for viewController: VIAViewController?, with completion:  @escaping (Error?) -> ()) {
        guard let questionnaireTypeKey = self.selectedQuestionnaire?.typeKey else {
            viewController?.displayUnknownServiceErrorOccurredAlert()
            return
        }
        
        MWBSubmissionHelper().captureAssesment(with: questionnaireTypeKey) { [weak self] (error) in
            if let error = error {
                completion(error)
            } else {
                self?.getPsycAssessment(completionFlag: (self?.selectedQuestionnaire?.completionFlag)!, assessmentName: (self?.selectedQuestionnaire?.typeName)!)
                self?.markQuestionnaireAsCompleted()
                self?.selectedQuestionnaire = nil
                let currentSectionViewController = self?.navigationController.topViewController as? MWBAssessmentQuestionnaireSectionViewController
                currentSectionViewController?.tearDownViewModel()
                
                viewController?.hideHUDFromWindow()
                NotificationCenter.default.post(name: .VIAMWBQuestionnaireSubmittedNotification, object: nil)
                completion(error)
            }
        }
    }
    
    func markQuestionnaireAsCompleted() {
        do {
            try self.realm.write {
                self.selectedQuestionnaire?.setCompletedAndSubmitted()
            }
        } catch {
            print("Error occured marking MWB questionnaire as completed and submitted")
        }
    }
    
    func reloadMWB(for viewController: VIAViewController?, completion: @escaping (Error?) -> ()) {
        let coreRealm = DataProvider.newRealm()
        let partyId = coreRealm.getPartyId()
        let tenantId = coreRealm.getTenantId()
        let membershipId = coreRealm.getMembershipId()
        let setTypeKey = QuestionnaireSetRef.MWB
        let prePopulation = true
        let mwbQuestionnaireParameter = MWBQuestionnaireParameter(setTypeKey: setTypeKey, prePopulation: prePopulation)
        
        Wire.Member.MWBQuestionnaireProgressAndPointsTracking(tenantId: tenantId, partyId: partyId, vitalityMembershipId: membershipId, payload: mwbQuestionnaireParameter) { error in
            if let error = error {
                completion(error)
                return
            }
            completion(nil)
        }
    }
}

//extension BackendErrorHandler where Self: UIViewController {
//    func handleErrorWithAlert(error: Error, tryAgain: @escaping () -> Void) {
//        switch error {
//        case is BackendError:
//            if let backendError = error as? BackendError {
//                self.handleBackendErrorWithAlert(backendError, tryAgainAction: tryAgain)
//            }
//            return
//        case is FrontendError:
//            self.displayUnknownServiceErrorOccurredAlert()
//            return
//        default:
//            self.displayUnknownServiceErrorOccurredAlert()
//            return
//        }
//    }
//}
