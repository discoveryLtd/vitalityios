import Foundation
import VIAUIKit
import VitalityKit
import VIAUtilities
import VIACommon

public protocol AssessmentDataSharingConsentViewControllerDelegate: class {

    func dataSharingConsentControllerDidSelectAgree()

    func dataSharingConsentControllerDidSelectDisagree(_ controller: AssessmentDataSharingConsentViewController)

}

public class AssessmentDataSharingConsentViewController: CustomTermsConditionsViewController {
    var questionnaire: VHRQuestionnaire?
    weak var submitter: AssessmentSubmitter?
    weak var navigationDelegate: AssessmentDataSharingConsentViewControllerDelegate?

    override public func viewDidLoad() {
        super.viewDidLoad()
        
        // Get content model. Original = VHRDSConsentContent
        if let tenantID = AppSettings.getAppTenant() {
            if tenantID == .IGI || tenantID == .EC || tenantID == .ASR || tenantID == .GI {
                self.viewModel = CustomTermsConditionsViewModel(articleId: AppConfigFeature.contentId(for: .VHRDSConsentContent))
            } else {
                self.viewModel = CustomTermsConditionsViewModel(articleId: AppConfigFeature.contentId(for: .VHCDSConsentContent))
            }
        }
        
        rightButtonTitle = VIAApplicableFeatures.default.getDataSharingRightBarButtonTitle()
        leftButtonTitle = VIAApplicableFeatures.default.getDataSharingLeftBarButtonTitle()
    }

    override public func configureAppearance() {
        super.configureAppearance()
        navigationController?.makeNavigationBarInvisible()
        navigationController?.navigationBar.isHidden = true
    }

    // MARK: Action buttons

    override public func leftButtonTapped(_ sender: UIBarButtonItem?) {
        navigationDelegate?.dataSharingConsentControllerDidSelectDisagree(self)
    }

    override public func rightButtonTapped(_ sender: UIBarButtonItem?) {
        self.showHUDOnWindow()
        let events = [EventTypeRef.VHRDataPrivacyAgree]
        viewModel?.agreeToTermsAndConditions(with: events, completion: { [weak self]
            (error) in
            self?.hideHUDFromWindow()
            if let error = error {
                sender?.isEnabled = true
                self?.handleBackendErrorWithAlert(BackendError.network(error: error as NSError))
                return
            }
            // create event to agree to privacy policy
            sender?.isEnabled = false
            self?.submitVHR(from: sender)
        })
    }

    func submitVHR(from sender: UIBarButtonItem?) {
        self.showHUDOnWindow()
        weak var weakSelf = self
        submitter?.submit(for: weakSelf, with: { [weak self] error in
            self?.hideHUDFromWindow()
            if let error = error {
                self?.handleErrorWithAlert(error: error, tryAgain: { [weak self] error in
                    self?.submitVHR(from: sender)
                })
                return
            }
            self?.navigationDelegate?.dataSharingConsentControllerDidSelectAgree()
        })
    }

    override public func returnToPreviousView() {
        navigationDelegate?.dataSharingConsentControllerDidSelectDisagree(self)
    }
}
