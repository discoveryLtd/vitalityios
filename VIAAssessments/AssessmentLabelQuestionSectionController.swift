import IGListKit
import VIAUIKit
import VIAUtilities
import VitalityKit
import RealmSwift

public final class AssessmentLabelQuestionSectionController: IGListSectionController, IGListSectionType {

    // MARK: Properties

    var realm: Realm

    var questionDetail: AssessmentQuestionPresentationDetail!

    // MARK: Lifecycle

    public required init(realm: Realm) {
        self.realm = realm
    }

    // MARK: IGListSectionType

    public func numberOfItems() -> Int {
        return 1
    }

    public func didUpdate(to object: Any) {
        self.questionDetail = object as! AssessmentQuestionPresentationDetail
    }

    public func cellForItem(at index: Int) -> UICollectionViewCell {
        return attributedTextCell(for: questionDetail.textAndDescriptionDetail(), at: index)
    }

    public func sizeForItem(at index: Int) -> CGSize {
        let width = collectionContext!.containerSize.width
        return AssessmentTextCollectionViewCell.height(with: questionDetail.textAndDescriptionDetail(), constrainedTo: width)
    }

    public func didSelectItem(at index: Int) { }

    // MARK: Cells

    func attributedTextCell(for textDetail: AttributedTextDetail, at index: Int) -> UICollectionViewCell {
        let cell = collectionContext!.dequeueReusableCell(of: AssessmentTextCollectionViewCell.self, for: self, at: index) as! AssessmentTextCollectionViewCell
        cell.label.attributedText = textDetail.attributedString
        cell.addTopBorder = true
        cell.addBottomBorder = true
        return cell
    }

}
