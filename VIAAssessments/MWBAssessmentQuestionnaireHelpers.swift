//
//  MWBAssessmentQuestionnaireHelpers.swift
//  VitalityActive
//
//  Created by Michelle R. Oratil on 14/02/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import IGListKit
import VIAUtilities
import VIAUIKit
import VitalityKit
import VitalityKit
import RealmSwift
import TTTAttributedLabel

public final class MWBAssessmentQuestionnaireSectionNavigationDetail: NSObject, IGListDiffable {
    public var isLastSection: Bool = false
    public var isEnabled: Bool = false
    public var uuidString: String = UUID().uuidString
    
    public init(isLastSection: Bool = false, isEnabled: Bool = false) {
        self.isLastSection = isLastSection
        self.isEnabled = isEnabled
    }
    
    public func diffIdentifier() -> NSObjectProtocol {
        return uuidString as NSObjectProtocol
    }
    
    public func isEqual(toDiffableObject object: IGListDiffable?) -> Bool {
        guard self !== object else { return true }
        guard let object = object as? MWBAssessmentQuestionnaireSectionNavigationDetail else { return false }
        return uuidString == object.uuidString
    }
}

public final class MWBAssessmentQuestionnaireSectionHeaderFooterDetail: NSObject, IGListDiffable {
    public var text: TextDetail
    public var textDescription: TextDetail?
    public var textNote: TextDetail?
    public var uuidString = UUID().uuidString
    
    public init(text: TextDetail, textDescription: TextDetail? = nil, textNote: TextDetail? = nil) {
        self.text = text
        self.textDescription = textDescription
        self.textNote = textNote
    }
    
    public func diffIdentifier() -> NSObjectProtocol {
        return self.uuidString as NSObjectProtocol
    }
    
    public func isEqual(toDiffableObject object: IGListDiffable?) -> Bool {
        guard self !== object else { return true }
        guard let object = object as? MWBAssessmentQuestionnaireSectionHeaderFooterDetail else { return false }
        return uuidString == object.uuidString
    }
}

public final class MWBAssessmentValidValuePresentationDetail: NSObject, IGListDiffable {
    public var text: TextDetail
    public var textDescription: TextDetail?
    public var textNote: TextDetail?
    public var rawValidValue: MWBValidValue
    public var uuidString = UUID().uuidString
    
    public init(text: TextDetail, textDescription: TextDetail? = nil, textNote: TextDetail? = nil, rawValidValue: MWBValidValue) {
        self.text = text
        self.textDescription = textDescription
        self.textNote = textNote
        self.rawValidValue = rawValidValue
    }
    
    public func diffIdentifier() -> NSObjectProtocol {
        return self.uuidString as NSObjectProtocol
    }
    
    public func isEqual(toDiffableObject object: IGListDiffable?) -> Bool {
        guard self !== object else { return true }
        guard let object = object as? MWBAssessmentValidValuePresentationDetail else { return false }
        return self.uuidString == object.uuidString
    }
    
    static private func == (lhs: MWBAssessmentValidValuePresentationDetail, rhs: MWBAssessmentValidValuePresentationDetail) -> Bool {
        return lhs.uuidString == rhs.uuidString
    }
}

public final class MWBAssessmentQuestionPresentationDetail: NSObject, IGListDiffable {
    public var text: TextDetail
    public var textDescription: TextDetail?
    public var presentationType: QuestionTypeRef
    public var decorator: QuestionDecoratorRef
    public var validValuesPresentationDetail: [MWBAssessmentValidValuePresentationDetail]?
    public var isVisible: Bool
    public var rawQuestionTypeKey: Int
    
    public init(text: TextDetail, textDescription: TextDetail? = nil, presentationType: QuestionTypeRef, decorator: QuestionDecoratorRef, validValues: [MWBAssessmentValidValuePresentationDetail]? = nil, isVisible: Bool, questionTypeKey: Int) {
        self.text = text
        self.textDescription = textDescription
        self.presentationType = presentationType
        self.decorator = decorator
        self.validValuesPresentationDetail = validValues
        self.isVisible = isVisible
        self.rawQuestionTypeKey = questionTypeKey
    }
    
    public func diffIdentifier() -> NSObjectProtocol {
        return self.text as NSObjectProtocol
    }
    
    public func isEqual(toDiffableObject object: IGListDiffable?) -> Bool {
        guard self !== object else { return true }
        guard let object = object as? MWBAssessmentQuestionPresentationDetail else { return false }
        return text.text == object.text.text
    }
    
    public func rawQuestion(in realm: Realm) -> MWBQuestion? {
        return realm.mwbquestion(by: self.rawQuestionTypeKey)
    }
    
    public func answerDetail(for answer: String) -> MWBAssessmentValidValuePresentationDetail? {
        return self.validValuesPresentationDetail?.filter({ $0.rawValidValue.name == answer }).first
    }
}

public final class MWBAssessmentQuestionFootnotePresentationDetail: NSObject, IGListDiffable {
    public var text: TextDetail
    
    public init(text: TextDetail) {
        self.text = text
    }
    
    public func diffIdentifier() -> NSObjectProtocol {
        return text as NSObjectProtocol
    }
    
    public func isEqual(toDiffableObject object: IGListDiffable?) -> Bool {
        guard self !== object else { return true }
        guard let object = object as? MWBAssessmentQuestionFootnotePresentationDetail else { return false }
        return text.text == object.text.text
    }
}

public final class MWBAssessmentQuestionPrepopulationPresentationDetail: NSObject, IGListDiffable {
    public var text: TextDetail
    
    public init(text: TextDetail) {
        self.text = text
    }
    
    public func diffIdentifier() -> NSObjectProtocol {
        return text as NSObjectProtocol
    }
    
    public func isEqual(toDiffableObject object: IGListDiffable?) -> Bool {
        guard self !== object else { return true }
        guard let object = object as? MWBAssessmentQuestionPrepopulationPresentationDetail else { return false }
        return text.text == object.text.text
    }
}

protocol MWBAttributedTextDetailPresentable {
    var text: TextDetail { get }
    var textDescription: TextDetail? { get }
    var textNote: TextDetail? { get }
    func textAndDescriptionDetail() -> AttributedTextDetail
    func textAndDescriptionAndNoteDetail() -> AttributedTextDetail
}

extension MWBAttributedTextDetailPresentable {
    var textDescription: TextDetail? { return nil }
    var textNote: TextDetail? { return nil }
}

extension MWBAttributedTextDetailPresentable {
    
    public func textDetail() -> AttributedTextDetail {
        let string = "\(self.text.text!)"
        return attributedTextDetail(for: string)
    }
    
    public func textAndDescriptionDetail() -> AttributedTextDetail {
        var string = "\(self.text.text!)"
        if let textDescription = self.textDescription?.text {
            string = string + "\n\(textDescription)"
        }
        return attributedTextDetail(for: string)
    }
    
    public func textAndDescriptionAndNoteDetail() -> AttributedTextDetail {
        var string = "\(self.text.text!)"
        if let textDescription = self.textDescription?.text {
            string = string + "\n\(textDescription)"
        }
        if let textNote = self.textNote?.text {
            string = string + "\n\(textNote)"
        }
        return attributedTextDetail(for: string)
    }
    
    public func noteDetail() -> AttributedTextDetail {
        var string = ""
        if let textNote = self.textNote?.text {
            string = textNote
        }
        return attributedTextDetail(for: string, type: .footnote)
    }
    
    private func attributedTextDetail(for string: String, type: AttributedTextDetail.LayoutType = .normal) -> AttributedTextDetail {
        let attributedString = NSMutableAttributedString(string: string)
        AttributedTextDetail.addAttributesIfNeeded(to: attributedString, from: self.text)
        AttributedTextDetail.addAttributesIfNeeded(to: attributedString, from: self.textDescription)
        AttributedTextDetail.addAttributesIfNeeded(to: attributedString, from: self.textNote)
        return AttributedTextDetail(attributedString: attributedString, type: type)
    }
    
}

extension MWBAssessmentQuestionnaireSectionHeaderFooterDetail: MWBAttributedTextDetailPresentable { }
extension MWBAssessmentQuestionPresentationDetail: MWBAttributedTextDetailPresentable { }
extension MWBAssessmentValidValuePresentationDetail: MWBAttributedTextDetailPresentable { }
extension MWBAssessmentQuestionFootnotePresentationDetail: MWBAttributedTextDetailPresentable { }
extension MWBAssessmentQuestionPrepopulationPresentationDetail: MWBAttributedTextDetailPresentable { }


public final class MWBAssessmentTextCollectionViewCell: UICollectionViewCell, Nibloadable {
    
    public static let inset: CGFloat = 15.0
    
    public var addTopBorder = false
    
    public var addBottomBorder = false
    
    // MARK: Views
    
    public lazy var label: UILabel = {
        let label = UILabel(frame: .zero)
        label.font = UIFont.title2Font()
        label.textColor = UIColor.night()
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    // MARK: Init
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupSubviews()
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        
        contentView.layer.removeAllBorderLayers()
        if addTopBorder {
            contentView.layer.addBorder(edge: .top)
        }
        if addBottomBorder {
            contentView.layer.addBorder(edge: .bottom)
        }
    }
    
    private func setupSubviews() {
        contentView.backgroundColor = .day()
        contentView.addSubview(self.label)
        self.label.snp.makeConstraints { (make) in
            make.top.bottom.equalToSuperview().inset(MWBAssessmentTextCollectionViewCell.inset)
            make.left.right.equalToSuperview().inset(MWBAssessmentTextCollectionViewCell.inset)
        }
    }
    
    override public func preferredLayoutAttributesFitting(_ layoutAttributes: UICollectionViewLayoutAttributes) -> UICollectionViewLayoutAttributes {
        return self.defaultContentViewLayoutSizeFitting(layoutAttributes)
    }
    
    // MARK: Height calculation
    
    public class func height(with attributedTextDetail: AttributedTextDetail, constrainedTo width: CGFloat) -> CGSize {
        let inset = MWBAssessmentTextCollectionViewCell.inset
        let insets = UIEdgeInsets(top: 0, left: inset, bottom: 0, right: inset)
        let size = TextSize.size(attributedTextDetail.attributedString, width: width, insets: insets)
        let height = size.height + inset * 2
        return CGSize(width: width, height: ceil(height))
    }
    
}

public final class MWBAssessmentFootnoteCollectionViewCell: UICollectionViewCell, Nibloadable {
    
    public static let topInset: CGFloat = 8.0
    
    public static let sideInset: CGFloat = 15.0
    
    private static let limitedNumberOfLines: Int = 2
    
    var isExpanded: Bool = false
    
    // MARK: Views
    
    private lazy var label: TTTAttributedLabel = {
        let label = TTTAttributedLabel(frame: .zero)
        label.font = UIFont.footnoteFont()
        label.textColor = UIColor.darkGrey()
        label.numberOfLines = MWBAssessmentFootnoteCollectionViewCell.limitedNumberOfLines
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    // MARK: Init
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupSubviews()
    }
    
    private func setupSubviews() {
        contentView.backgroundColor = UIColor.tableViewBackground()
        contentView.addSubview(self.label)
        self.label.snp.makeConstraints { (make) in
            make.top.equalToSuperview().inset(MWBAssessmentFootnoteCollectionViewCell.topInset)
            make.bottom.equalToSuperview()
            make.left.right.equalToSuperview().inset(MWBAssessmentFootnoteCollectionViewCell.sideInset)
        }
    }
    
    override public func preferredLayoutAttributesFitting(_ layoutAttributes: UICollectionViewLayoutAttributes) -> UICollectionViewLayoutAttributes {
        return self.defaultContentViewLayoutSizeFitting(layoutAttributes)
    }
    
    // MARK: Height calculation
    
    func setLabelAttributedText(_ attributedText: NSAttributedString?, isFootnoteExpanded: Bool) {
        if !isFootnoteExpanded {
            // Unicode Character 'HORIZONTAL ELLIPSIS' (U+2026)
            self.label.attributedTruncationToken = attributedText?.addTruncationToken(text: "\u{2026}" + CommonStrings.FootnoteMoreButton342)
            self.label.numberOfLines = MWBAssessmentFootnoteCollectionViewCell.limitedNumberOfLines
            self.label.attributedText = attributedText
        } else {
            self.label.attributedTruncationToken = nil
            self.label.numberOfLines = 0
            let combo = NSMutableAttributedString()
            if let lessString = attributedText?.addTruncationToken(text: " " + CommonStrings.FootnoteLessButton343), let attributedText = attributedText {
                combo.append(attributedText)
                combo.append(lessString)
            }
            self.label.attributedText = combo
        }
    }
    
    public class func height(with attributedTextDetail: AttributedTextDetail, constrainedTo width: CGFloat, isFootnoteExpanded: Bool) -> CGSize {
        let attributedText: NSAttributedString = attributedTextDetail.attributedString
        let topInset = MWBAssessmentFootnoteCollectionViewCell.topInset
        let sideInset = MWBAssessmentFootnoteCollectionViewCell.sideInset
        let sizeLimit = CGSize(width: width - sideInset * 2, height: CGFloat.greatestFiniteMagnitude)
        let numberOfLines = isFootnoteExpanded ? 0 : MWBAssessmentFootnoteCollectionViewCell.limitedNumberOfLines
        let size = TTTAttributedLabel.sizeThatFitsAttributedString(attributedText, withConstraints: sizeLimit, limitedToNumberOfLines: UInt(numberOfLines))
        let height = size.height + topInset
        return CGSize(width: width, height: ceil(height))
    }
    
}

fileprivate extension NSAttributedString {
    
    func addTruncationToken(text: String) -> NSAttributedString {
        let tokenString = text
        let attributedTruncationToken = NSMutableAttributedString(string: tokenString)
        let attributes = self.attributes(at: 0, effectiveRange: nil)
        
        // add current paragraphy style if present
        if let paragraphStyle = attributes[NSParagraphStyleAttributeName] as? NSMutableParagraphStyle {
            attributedTruncationToken.addAttributes([NSParagraphStyleAttributeName: paragraphStyle], range: attributedTruncationToken.string.nsRange(of: attributedTruncationToken.string))
        }
        // reapply foreground color to whole string
        if let foregroundColor = attributes[NSForegroundColorAttributeName] as? UIColor {
            attributedTruncationToken.addAttributes([NSForegroundColorAttributeName: foregroundColor], range: attributedTruncationToken.string.nsRange(of: attributedTruncationToken.string))
        }
        // set custom text color to token string
        attributedTruncationToken.addAttributes([NSForegroundColorAttributeName: UIColor.currentGlobalTintColor()], range: attributedTruncationToken.string.nsRange(of: tokenString))
        
        return attributedTruncationToken
    }
    
}

extension MWBQuestionnaireSection {
    
    func image() -> UIImage? {
        let type = QuestionnaireSectionsRef(rawValue: self.typeKey) ?? .Unknown
        switch type {
        case .StressorDiagnostic:
            return VIAAssessmentsAsset.SectionIcons.mwbStressor.image
        case .TraumaticEvents:
            return VIAAssessmentsAsset.SectionIcons.mwbStressor.image
        case .PsychWellbeing:
            return VIAAssessmentsAsset.SectionIcons.mwbPsychological.templateImage
        case .SocialSupport:
            return VIAAssessmentsAsset.SectionIcons.mwbSocial.templateImage
        default:
            return nil
        }
    }
    
}

protocol MWBFootnoteToggleable: class {
    
    var isFootnoteExpanded: Bool { get set }
    
    var indexForFootnoteCell: Int? { get }
    
    func toggleFootnote()
    
}

extension MWBFootnoteToggleable where Self: IGListSectionController {
    
    func toggleFootnote() {
        isFootnoteExpanded = !isFootnoteExpanded
        
        if let validIndex = indexForFootnoteCell {
            self.collectionContext?.performBatch(animated: true, updates: {
                self.collectionContext?.reload(in: self, at: IndexSet(integer: validIndex))
            }, completion: { completed in
                //
            })
        }
    }
    
}
