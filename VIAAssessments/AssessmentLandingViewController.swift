import Foundation
import VIAUIKit
import VIAUtilities
import VitalityKit
import VitalityKit
import VIACommon
import VIAMyHealth

public class AssessmentLandingViewController: VIATableViewController, KnowYourHealthTintable {

    // MARK: - Properties

    public var viewModel: AssessmentLandingViewModel!

    let integerFormatter = NumberFormatter.integerFormatter()

    var currentQuestionnaireFlowCoordinator: AssessmentQuestionnaireFlowCoordinator?

    var selectedQuestionnaireTypeKey = -1
    
    var shouldReloadOnViewWillAppear = false
    
    public var shouldShowVAgeHealthCard = false

    // MARK: Lifecycle

    override public func viewDidLoad() {
        super.viewDidLoad()
        
        configureTitle()
        configureAppearance()
        configureTableView()
        showOnboardingIfNeeded()
        loadData()
        registerForNotifications()
    }

    override public func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        configureAppearance()
        
        if self.shouldReloadOnViewWillAppear {
            self.shouldReloadOnViewWillAppear = false
            self.loadData(forceUpdate: true)
        }
    }

    override public func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        self.hideBackButtonTitle()
    }

    public override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        showVitalityAgeIfRequired()
    }

  

    public func showVitalityAgeIfRequired() {
        /*
         * Task: Mobile Performance Enhancements
         * Only display Vitality Age card when all sections were completed.
         */
        
        if shouldShowVAgeHealthCard{
            shouldShowVAgeHealthCard = false
            self.performSegue(withIdentifier: "showVitalityAgeModal", sender: nil)
        }
    }

    // MARK: Setup

    func loadData(forceUpdate: Bool = false) {
        showHUDOnView(view: self.view)
        viewModel.fetchQuestionnaires(forceUpdate: forceUpdate) { [weak self] error, isCacheOutdated in
            self?.hideHUDFromView(view: self?.view)

            guard error == nil else {
                    self?.handleErrorOccurred(error!, isCacheOutdated: isCacheOutdated)
                    return
            }

            self?.removeStatusView()
            self?.tableView.reloadData()
        }
    }

    func handleErrorOccurred(_ error: Error, isCacheOutdated: Bool?) {
        let backendError = error as? BackendError ?? BackendError.other
        if let outdated = isCacheOutdated, outdated == true {
            let statusView = self.handleBackendErrorWithStatusView(backendError, tryAgainAction: { [weak self] in
                self?.loadData(forceUpdate: true)
            })
            configureStatusView(statusView)
        } else {
            self.handleBackendErrorWithAlert(backendError, tryAgainAction: { [weak self] in
                self?.loadData(forceUpdate: true)
            })
        }
    }
    
    func configureTitle(){
        
        if viewModel is VHRLandingViewModel{
            self.title = CommonStrings.HomeCard.CardSectionTitle291
        }else if viewModel is MWBLandingViewModel{
            self.title = CommonStrings.Onboarding.Title1195
        }
    }

    func configureAppearance() {
        navigationController?.makeNavigationBarTransparent()
    }

    func configureTableView() {
        self.tableView.register(VHRLandingHeaderCell.nib(), forCellReuseIdentifier: VHRLandingHeaderCell.defaultReuseIdentifier)
        self.tableView.register(VHRLandingSectionCell.nib(), forCellReuseIdentifier: VHRLandingSectionCell.defaultReuseIdentifier)
        self.tableView.register(VIATableViewCell.nib(), forCellReuseIdentifier: VIATableViewCell.defaultReuseIdentifier)
        self.tableView.register(VIATableViewSectionHeaderFooterView.nib(), forHeaderFooterViewReuseIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier)
        self.tableView.estimatedRowHeight = 150
        self.tableView.estimatedSectionHeaderHeight = 40
        self.tableView.estimatedSectionFooterHeight = 40
    }

    func showOnboardingIfNeeded() {
        if viewModel.shouldShowOnboarding() {
            showFirstTimeOnboarding()
        }
    }

    func registerForNotifications() {
        /*
         * Task: Mobile Performance Enhancements
         * Disabled all reload calls from other Journeys.
         * Home screen card should only be refreshed on viewWillAppear and on pull-to-refresh
         */
        NotificationCenter.default.addObserver(forName: .VIAVHRQuestionnaireSubmittedNotification, object: nil, queue: .main) { [weak self] notification in
            /*
             * Task: Mobile Performance Enhancements
             * Disable reloading of VHR data from service
             */
            self?.loadData(forceUpdate: false)
            self?.shouldReloadOnViewWillAppear = true

            self?.showVitalityAgeIfRequired()
        }
        
        NotificationCenter.default.addObserver(forName: .VIAVHRQuestionnaireStarted, object: nil, queue: .main) { [weak self] notification in
            self?.loadData(forceUpdate: false)
        }
    }

    // MARK: - UITableView DataSource + Delegate

    enum Sections: Int, EnumCollection, Equatable {
        case header = 0
        case questionnaires = 1
        case other = 2

        func title() -> String? {
            switch self {
            case .header,
                 .other:
                return nil
            case .questionnaires:
                return CommonStrings.LandingScreen.GroupHeading300
            }
        }
    }

    override public func numberOfSections(in tableView: UITableView) -> Int {
        return Sections.allValues.count
    }

    override public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch Sections.allValues[section] {
        case .header:
            return 1
        case .questionnaires:
            return viewModel.vhrQuestionnaireData.count
        case .other:
            if let hideHelpTab = VIAApplicableFeatures.default.hideHelpTab,
                hideHelpTab{
                return 1
            } else{
                return 2
            } //ge20171204: change to 2 to show "Help"
        }
    }

    override public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch Sections.allValues[indexPath.section] {
        case .header:
            return headerCell(at: indexPath)
        case .questionnaires:
            return questionnaireCell(at: indexPath)
        case .other:
            return otherCell(at: indexPath)
        }
    }

    func headerCell(at indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: VHRLandingHeaderCell.defaultReuseIdentifier, for: indexPath) as! VHRLandingHeaderCell
        guard let headerData = viewModel.vhrHeaderData else { return cell }
        guard headerData.totalSections > 0 else { return cell }

        if headerData.totalSections <= headerData.completeSections {
            cell.configureCompleteVHRLandingHeaderCell(title: headerData.title, description: headerData.description, totalSections: headerData.totalSections, completedSections: headerData.completeSections, completedImage: headerData.completedImage, integerFormatter: integerFormatter)
        } else {
            cell.configureIncompleteVHRLandingHeaderCell(title: headerData.title, description: headerData.description, totalSections: headerData.totalSections, completedSections: headerData.completeSections, integerFormatter: integerFormatter)
        }

        cell.isUserInteractionEnabled = false
        return cell
    }

    func questionnaireCell(at indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: VHRLandingSectionCell.defaultReuseIdentifier, for: indexPath) as! VHRLandingSectionCell
        let cellData = viewModel.vhrQuestionnaireData[indexPath.row]
        if cellData.completed {
            cell.configureCompleteVHRLandingSectionCell(title: cellData.title, feedback: cellData.timeToComplete, buttonText: cellData.buttonText, completeIcon: cellData.completeIcon)
        } else {
            cell.configureIncompleteVHRLandingSectionCell(title: cellData.title, feedback: cellData.timeToComplete, buttonText: cellData.buttonText)
        }
        cell.sectionButton.addTarget(self, action: #selector(navigateToQuestionnaire(_:)), for: .touchUpInside)
        cell.selectionStyle = .none
        return cell
    }

    func otherCell(at indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: VIATableViewCell.defaultReuseIdentifier, for: indexPath) as! VIATableViewCell
        if indexPath.row == 0 {
            cell.labelText = CommonStrings.LearnMoreButtonTitle104
            cell.cellImage = VIAAssessmentsAsset.vhrGenericLearnMore.templateImage //UIImage.templateImage(asset: .vhrGenericLearnMore)
        }
        if indexPath.row == 1 {
            cell.labelText = CommonStrings.HelpButtonTitle141
            cell.cellImage = VIAAssessmentsAsset.helpGeneric.templateImage //UIImage.templateImage(asset: .helpGeneric)
        }
        cell.cellImageTintColor = UIColor.knowYourHealthGreen()
        cell.accessoryType = .disclosureIndicator
        return cell
    }

    override public func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch Sections.allValues[section] {
        case .header:
            return CGFloat.leastNormalMagnitude
        case .questionnaires,
             .other:
            return UITableViewAutomaticDimension
        }
    }

    override public func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return UITableViewAutomaticDimension
    }

    override public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let theSection = Sections(rawValue: section)
        if theSection == .questionnaires {
            let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier) as! VIATableViewSectionHeaderFooterView
            view.labelText = theSection?.title()
            view.set(textColor: .mediumGrey())

            return view
        }
        return nil
    }

    override public func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let theSection = Sections(rawValue: section)
        if theSection == .questionnaires {
            let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier) as! VIATableViewSectionHeaderFooterView
            view.labelText = viewModel.questionnairesFooterText
            view.set(textColor: .darkGrey())
            return view
        }
        return nil
    }

    override public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let theSection = Sections(rawValue: indexPath.section)
        if theSection == .other && indexPath.row == 0 {
            self.performSegue(withIdentifier: "showLearnMore", sender: nil)
        }
        if theSection == .other && indexPath.row == 1 {
            GlobalUtil.helpSourceVC = 1 // VHR
            self.performSegue(withIdentifier: "showHelp", sender: nil)
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }

    // MARK: Navigation

    func showFirstTimeOnboarding() {
        self.performSegue(withIdentifier: "showOnboarding", sender: self)
    }

    @IBAction func unwindToAssessmentLanding(segue: UIStoryboardSegue) {
        debugPrint("unwindToAssessmentLanding")
    }

    @IBAction public func unwindFromQuestionnaireSectionViewControllerToAssessmentLanding(segue: UIStoryboardSegue) {
        debugPrint("unwindFromQuestionnaireSectionViewControllerToAssessmentLanding")
        currentQuestionnaireFlowCoordinator = nil
    }

    func navigateToQuestionnaire(_ sender: BorderButton?) {
        guard let button = sender else { return }
        if let cellIndexPath = tableView.indexPathForCellSubview(subView: button) {
            guard let questionnaireTypeKey = viewModel.getQuestionnaireAt(index: cellIndexPath.row)?.typeKey else { return }
            selectedQuestionnaireTypeKey = questionnaireTypeKey
            self.performSegue(withIdentifier: "showQuestionnaire", sender: nil)
        }
    }

    override public func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showOnboarding" {
            prepareForOnboarding(with: segue)
        } else if segue.identifier == "showQuestionnaire" {
            prepareForShowingQuestionnaire(with: segue)
        } else if segue.identifier == "showVitalityAgeModal" {
            prepareForShowingHealthCard(with: segue)
        }
    }

    func prepareForOnboarding(with segue: UIStoryboardSegue) {
        guard let navController = segue.destination as? UINavigationController else { return }
        guard let viewController = navController.topViewController as? AssessmentOnboardingViewController else { return }

        if viewModel is VHRLandingViewModel {
            viewController.assessmentViewModel = VHROnboardingViewModel()
        } else if viewModel is VNALandingViewModel {
            viewController.assessmentViewModel = VNAOnboardingViewModel()
        }
    }

    func prepareForShowingQuestionnaire(with segue: UIStoryboardSegue) {
        if let navController = segue.destination as? AssessmentNavigationController {

            let selectedQuestionnaire = viewModel.getVHRQuestionnaire(using: selectedQuestionnaireTypeKey)
            let potentialPoints = viewModel.getQuestionnairesTotalPotentialPoints()

            currentQuestionnaireFlowCoordinator = AssessmentQuestionnaireFlowCoordinator(with: navController, selectedQuestionnaire: selectedQuestionnaire, potentialPoints: potentialPoints)

            currentQuestionnaireFlowCoordinator?.start()
        }
    }
    
    func prepareForShowingHealthCard(with segue: UIStoryboardSegue) {
        guard let navController = segue.destination as? UINavigationController else { return }
        guard let viewController = navController.topViewController as? VIAVitalityHealthModalTableViewController else { return }
        viewController.showBarButton = true
        viewController.cardIndex = 0 // Default to 0 for Vitality Age card index.
    }
}

extension AssessmentLandingViewController{
    
    public func areAllSectionsCompleted(_ viewModel: AssessmentLandingViewModel) -> Bool{
        if viewModel.getTotalNumberOfQuestionnaires() > 0{
            return viewModel.getTotalNumberOfCompletedQuestionnaires() >= viewModel.getTotalNumberOfQuestionnaires()
        }
        return false
    }
}
