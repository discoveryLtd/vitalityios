import VIAUIKit
import VitalityKit
import VIACommon

public class VHRLearnMoreViewModel: AnyObject, VIALearnMoreViewModel {
    public var title: String {
        return CommonStrings.LearnMoreButtonTitle104
    }
    
    public var contentItems: [LearnMoreContentItem] {
        var items = [LearnMoreContentItem]()
        
        items.append(LearnMoreContentItem(
            header: CommonStrings.LearnMore.Heading1Title308,
            content: CommonStrings.LearnMore.Heading1Message309
        ))
        
        items.append(LearnMoreContentItem(
            header: CommonStrings.LearnMore.Section1Title310,
            content: CommonStrings.LearnMore.Section1Message311,
            image: VIAAssessmentsAsset.vhrLearnMorePoints.templateImage
        ))
        
        items.append(LearnMoreContentItem(
            header: CommonStrings.LearnMore.Section2Title312,
            content: CommonStrings.LearnMore.Section2Message313,
            image: VIAAssessmentsAsset.vhrLearnVitalityAge.templateImage
        ))
        
        items.append(LearnMoreContentItem(
            header: CommonStrings.LearnMore.Section3Title314,
            content: CommonStrings.LearnMore.Section3Message315,
            image: VIAAssessmentsAsset.vhrLearnCompleteTime.templateImage
        ))
        
        return items
    }
    
    public var imageTint: UIColor {
        return .knowYourHealthGreen()
    }
}
