//
//  MWBOnboardingViewModel.swift
//  VitalityActive
//
//  Created by Harry C. Lingad on 1/31/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import VitalityKit
import VitalityKit
import VIAUIKit

public class MWBOnboardingViewModel: MWBAssessmentOnboardingViewModel {
    
    internal let coreRealm = DataProvider.newRealm()
    
    public override var heading: String {
        return CommonStrings.Onboarding.Title1195
    }
    
    public override var alternateButtonTitle: String? {
        return CommonStrings.LearnMoreButtonTitle104
    }
    
    public override var contentItems: [OnboardingContentItem] {
        var items: [OnboardingContentItem] = [OnboardingContentItem]()
        items.append(OnboardingContentItem(heading: CommonStrings.Mwb.OnboardingSection1Title1196,
                                           content: CommonStrings.Mwb.OnboardingSection1Message1197,
                                           image: VIAAssessmentsAsset.onboardingSections.image))
        items.append(OnboardingContentItem(heading: CommonStrings.Mwb.OnboardingSection2Title1198,
                                           content: CommonStrings.Mwb.OnboardingSection2Message1199,
                                           image: VIAAssessmentsAsset.onboardingEarnPoints.image))
        items.append(OnboardingContentItem(heading: CommonStrings.Mwb.OnboardingSection3Title1200,
                                           content: CommonStrings.Mwb.OnboardingSection3Message1201,
                                           image: VIAAssessmentsAsset.mentalwellbeing.image))
        return items
    }
    
    public override func setOnboardingHasShown() {
        AppSettings.setHasShownMWBOnboarding()
    }
    
    
    
}

