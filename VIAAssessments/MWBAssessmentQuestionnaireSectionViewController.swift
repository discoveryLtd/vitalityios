//
//  MWBAssessmentQuestionnaireSectionViewController.swift
//  VitalityActive
//
//  Created by Michelle R. Oratil on 14/02/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import IGListKit
import VitalityKit
import VitalityKit
import VIAUIKit
import RealmSwift

public protocol MWBAssessmentQuestionnaireSectionViewControllerDelegate: class {
    
    func sectionControllerDidSelectNext(_ mwbQuestionnaireSectionViewController: MWBAssessmentQuestionnaireSectionViewController)
    
    func sectionControllerDidSelectBack(_ mwbQuestionnaireSectionViewController: MWBAssessmentQuestionnaireSectionViewController)
    
    func sectionControllerDidSelectClose(_ mwbQuestionnaireSectionViewController: MWBAssessmentQuestionnaireSectionViewController)
    
    func sectionControllerDidSelectDone(_ mwbQuestionnaireSectionViewController: MWBAssessmentQuestionnaireSectionViewController)
    
}

public final class MWBAssessmentQuestionnaireSectionViewController: VIAViewController, IGListAdapterDataSource, MWBAssessmentQuestionnaireSectionViewModelDelegate {
    
    // MARK: Properties
    
    weak var navigationDelegate: MWBAssessmentQuestionnaireSectionViewControllerDelegate?
    
    weak var answeringDelegate: MWBAssessmentQuestionnaireAnsweringDelegate?
    
    private (set) var isFirstSection: Bool = false
    
    private (set) var isLastSection: Bool = false
    
    private (set) var questionnaireSectionTypeKey: Int?
    
    private  var _viewModel: MWBAssessmentQuestionnaireSectionViewModel?
    private (set) var viewModel: MWBAssessmentQuestionnaireSectionViewModel? {
        set {
            self._viewModel = newValue
        }
        get {
            if _viewModel == nil {
                let viewModel = MWBAssessmentQuestionnaireSectionViewModel(questionnaireSectionTypeKey: self.questionnaireSectionTypeKey,
                                                                        isLastSection: self.isLastSection)
                viewModel?.delegate = self
                self._viewModel = viewModel
            }
            return _viewModel
        }
    }
    
    let collectionView: IGListCollectionView = {
        let collectionView = IGListCollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
        collectionView.backgroundColor = UIColor.tableViewBackground()
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        return collectionView
    }()
    
    lazy var adapter: IGListAdapter = {
        return IGListAdapter(updater: IGListAdapterUpdater(), viewController: self, workingRangeSize: 0)
    }()
    
    // MARK: View lifecycle
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        
        configureAppearance()
        configureCollectionView()
        configureAdapter()
        addBarButtonItems()
        
        addNotificationWithCompletion { [weak self] (notification) in
            guard let strongSelf = self else { return }
            strongSelf.navigationDelegate?.sectionControllerDidSelectClose(strongSelf)
        }
    }
    
    func configure(isFirstSection: Bool, isLastSection: Bool, questionnaireSectionTypeKey: Int) {
        self.isFirstSection = isFirstSection
        self.isLastSection = isLastSection
        self.questionnaireSectionTypeKey = questionnaireSectionTypeKey
    }
    
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    func tearDownViewModel() {
        viewModel = nil
    }
    
    public override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        tearDownViewModel()
    }
    
    deinit {
        debugPrint("MWBAssessmentQuestionnaireSectionViewController deinit")
    }
    
    // MARK: View config
    
    func configureAppearance() {
        navigationController?.setNavigationBarHidden(false, animated: false)
        navigationController?.makeNavigationBarTransparent()
        navigationController?.navigationBar.barTintColor = .white
    }
    
    func configureCollectionView() {
        view.addSubview(collectionView)
        collectionView.snp.makeConstraints { (make) in
            //Xcode 9
            if #available(iOS 11, *) {
                let window = UIApplication.shared.keyWindow
                let topPadding = window?.safeAreaInsets.top
                make.top.equalTo(150 + topPadding!)
                make.left.bottom.right.equalToSuperview()
            } else {
                make.top.left.bottom.right.equalToSuperview()
            }
            //Xcode 9
            //make.top.left.bottom.right.equalToSuperview()
        }
    }
    
    func configureAdapter() {
        adapter.collectionView = collectionView
        adapter.dataSource = self
    }
    
    func addBarButtonItems() {
        if !self.isFirstSection {
            self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage.templateImage(asset: ASSAsset.vhrBackArrow), style: .plain, target: self, action: #selector(back(_:)))
        }
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: CommonStrings.GenericCloseButtonTitle10, style: .plain, target: self, action: #selector(close(_:)))
        
    }
    
    // MARK: - Navigation
    
    func close(_ sender: Any?) {
        collectionView.endEditing(true)
        navigationDelegate?.sectionControllerDidSelectClose(self)
    }
    
    func next(_ sender: Any?) {
        navigationDelegate?.sectionControllerDidSelectNext(self)
    }
    
    func back(_ sender: Any?) {
        navigationDelegate?.sectionControllerDidSelectBack(self)
    }
    
    func done(_ sender: Any?) {
        navigationDelegate?.sectionControllerDidSelectDone(self)
    }
    
    // MARK: IGListAdapterSource
    
    public func objects(for listAdapter: IGListAdapter) -> [IGListDiffable] {
        if let data = viewModel?.data {
            return data
        }
        return [] as! [IGListDiffable]
    }
    
    public func listAdapter(_ listAdapter: IGListAdapter, sectionControllerFor object: Any) -> IGListSectionController {
        if let detail = object as? MWBAssessmentQuestionPresentationDetail, let realm = viewModel?.realm {
            if detail.presentationType == .Date && detail.decorator == .DatePicker {
                return MWBAssessmentDateQuestionSectionController(realm: realm)
            } else if detail.presentationType == .DecimalRange && detail.decorator == .Textbox {
                return MWBAssessmentNumberRangeQuestionSectionController(realm: realm)
            } else if detail.presentationType == .FreeText && detail.decorator == .Textbox {
                return MWBAssessmentFreeTextSectionController(realm: realm)
            } else if detail.presentationType == .MultiSelect && detail.decorator == .Checkbox {
                return MWBAssessmentMultiSelectQuestionSectionController(realm: realm)
            } else if detail.presentationType == .NumberRange && detail.decorator == .Textbox {
                return MWBAssessmentNumberRangeQuestionSectionController(realm: realm)
            } else if detail.presentationType == .ChildNumberRange && detail.decorator == .Textbox {
                return MWBAssessmentNumberRangeSingleLineQuestionSectionController(realm: realm)
            } else if detail.presentationType == .SingleSelect && detail.decorator == .RadioButton {
                return MWBAssessmentSingleSelectQuestionSectionController(realm: realm)
            } else if detail.presentationType == .SingleSelect && detail.decorator == .Toggle {
                return MWBAssessmentToggleQuestionSectionController(realm: realm)
            } else if detail.presentationType == .SingleSelect && detail.decorator == .YesNo {
                return MWBAssessmentYesNoQuestionSectionController(realm: realm)
            } else if detail.presentationType == .Label && detail.decorator == .Label {
                return MWBAssessmentLabelQuestionSectionController(realm: realm)
            } else if detail.presentationType == .Label && detail.decorator == .CollapseUoM {
                return MWBAssessmentLabelQuestionSectionController(realm: realm)
            } else {
                debugPrint("We do not cater for this combination: \(detail.presentationType.rawValue) + \(detail.decorator.rawValue)")
                return SpacingController()
            }
        } else if object is MWBAssessmentQuestionnaireSectionHeaderFooterDetail {
            return MWBAssessmentQuestionnaireHeaderFooterSectionController()
        } else if object is MWBAssessmentQuestionPrepopulationPresentationDetail {
            return MWBAssessmentQuestionPrepopulationSectionController()
        } else if object is MWBAssessmentQuestionFootnotePresentationDetail {
            return MWBAssessmentQuestionFootnoteSectionController()
        } else if object is MWBAssessmentQuestionnaireSectionNavigationDetail {
            return MWBAssessmentNavigationButtonSectionController()
        } else if object is AssessmentRequiredPrompt {
            return AssessmentRequiredPromptController()
        }
        return SpacingController()
    }
    
    public func emptyView(for listAdapter: IGListAdapter) -> UIView? { return nil }
    
    // MARK: MWBQuestionnaireSectionViewModelDelegate
    
    public func viewModelDidUpdateData() {
        self.adapter.performUpdates(animated: true, completion: { finished in
            //
        })
    }
    
    public func viewModelDidUpdateCapturedResults() {
        // reload only the navigation detail, not everything.
        // if we reload everything, the date picker freaks out and the
        // number range validation disappears immediately after showing
        if let controller = self.adapter.sectionController(for: self.viewModel?.navigationDetail as Any) {
            controller.collectionContext?.performBatch(animated: true, updates: {
                controller.collectionContext?.reload(controller)
            }, completion: nil)
        }
    }
    
    public func reloadSectionController(for questionDetail: MWBAssessmentQuestionPresentationDetail) {
        if let details = self.adapter.objects().filter({ $0 is MWBAssessmentQuestionPresentationDetail }) as? [MWBAssessmentQuestionPresentationDetail] {
            if let filtered = details.filter({ $0.text.isEqual(toDiffableObject: questionDetail.text) }).first {
                if let controller = self.adapter.sectionController(for: filtered) {
                    controller.collectionContext?.performBatch(animated: true, updates: {
                        controller.collectionContext?.reload(controller)
                    }, completion: nil)
                }
            }
        }
    }
    
    public func scrollTo(questionDetail: MWBAssessmentQuestionPresentationDetail){
        if let details = self.adapter.objects().filter({ $0 is MWBAssessmentQuestionPresentationDetail }) as? [MWBAssessmentQuestionPresentationDetail] {
            if let filtered = details.filter({ $0.text.isEqual(toDiffableObject: questionDetail.text) }).first {
                if let controller = self.adapter.sectionController(for: filtered) {
                    controller.collectionContext?.scroll(to: controller, at: 0, scrollPosition: .centeredVertically, animated: false)
                }
            }
        }
    }
}
