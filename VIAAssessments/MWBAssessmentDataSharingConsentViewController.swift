//
//  MWBAssessmentDataSharingConsentViewController.swift
//  VitalityActive
//
//  Created by Michelle R. Oratil on 14/02/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import VIAUIKit
import VitalityKit
import VIAUtilities
import VIACommon

public protocol MWBAssessmentDataSharingConsentViewControllerDelegate: class {
    
    func MWBdataSharingConsentControllerDidSelectAgree()
    
    func MWBdataSharingConsentControllerDidSelectDisagree(_ controller: MWBAssessmentDataSharingConsentViewController)
    
}

public class MWBAssessmentDataSharingConsentViewController: CustomTermsConditionsViewController {
    var questionnaire: MWBQuestionnaire?
    weak var submitter: MWBAssessmentSubmitter?
    weak var navigationDelegate: MWBAssessmentDataSharingConsentViewControllerDelegate?
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        
        // Get content model. Original Value = VHRDSConsentContent
        self.viewModel = CustomTermsConditionsViewModel(articleId: AppConfigFeature.contentId(for: .VHCDSConsentContent))
        
        rightButtonTitle = VIAApplicableFeatures.default.getDataSharingRightBarButtonTitle()
        leftButtonTitle = VIAApplicableFeatures.default.getDataSharingLeftBarButtonTitle()
        
    }
    
    override public func configureAppearance() {
        super.configureAppearance()
        navigationController?.makeNavigationBarInvisible()
        navigationController?.navigationBar.isHidden = true
    }
    
    // MARK: Action buttons
    
    override public func leftButtonTapped(_ sender: UIBarButtonItem?) {
        navigationDelegate?.MWBdataSharingConsentControllerDidSelectDisagree(self)
    }
    
    override public func rightButtonTapped(_ sender: UIBarButtonItem?) {
        self.showHUDOnWindow()
        let events = [EventTypeRef.VHRDataPrivacyAgree]
        viewModel?.agreeToTermsAndConditions(with: events, completion: { [weak self]
            (error) in
            self?.hideHUDFromWindow()
            if let error = error {
                sender?.isEnabled = true
                self?.handleBackendErrorWithAlert(BackendError.network(error: error as NSError))
                return
            }
            // create event to agree to privacy policy
            sender?.isEnabled = false
            self?.submitMWB(from: sender)
        })
    }
    
    func submitMWB(from sender: UIBarButtonItem?) {
        self.showHUDOnWindow()
        weak var weakSelf = self
        submitter?.submit(for: weakSelf, with: { [weak self] error in
            self?.hideHUDFromWindow()
            if let error = error {
                self?.handleErrorWithAlert(error: error, tryAgain: { [weak self] error in
                    self?.submitMWB(from: sender)
                })
                return
            }
            self?.navigationDelegate?.MWBdataSharingConsentControllerDidSelectAgree()
        })
    }
    
    override public func returnToPreviousView() {
        navigationDelegate?.MWBdataSharingConsentControllerDidSelectDisagree(self)
    }
}
