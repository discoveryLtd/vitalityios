//
//  MWBLandingViewModel.swift
//  VitalityActive
//
//  Created by Harry C. Lingad on 1/31/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import VitalityKit
import VitalityKit
import VIAUIKit
import RealmSwift
import VIAUtilities

public class MWBLandingViewModel: MWBAssessmentLandingViewModel {
    
    // MARK: Properties
    public var mwbRealm = DataProvider.newMWBRealm()
    public var mwbQuestionnaireData = [MWBAssessmentQuestionnaireCellData]()
    public var mwbHeaderData: MWBAssessmentHeaderCellData?
    public let integerFormatter = NumberFormatter.integerFormatter()
    public var questionnairesFooterText: String? {
        return mwbRealm.allMWBQuestionnaireProgressAndPointsTracking().first?.questionnaireSetTextNote
    }
    
    // MARK: Lifecycle
    
    public init() {
        
    }
    
    // MARK: Functions
    
    public func shouldShowOnboarding() -> Bool {
        return !AppSettings.hasShownMWBOnboarding()
    }
    
    public func configureQuestionnaires() {
        mwbRealm.refresh()
        mwbQuestionnaireData.removeAll()
        configureHeader()
        let  questionnaires = mwbRealm.allSortedMWBQuestionnaires()
        
        for questionnaire in questionnaires {
            
            
            let typeKey = questionnaire.typeKey
            let title = questionnaire.typeName ?? ""
            var timeToComplete = questionnaire.textDescription
            var buttonText = CommonStrings.LandingScreen.StartButton305
            let completionState = questionnaire.getCompletionState()
            if completionState == .Complete {
                buttonText = CommonStrings.LandingScreen.EditButton307
                timeToComplete = CommonStrings.LandingScreen.CompletedMessage327
            } else if completionState == .InProgress {
                buttonText = CommonStrings.LandingScreen.ContinueButton306
            }
            let completed = questionnaire.completionFlag
            let completeIcon = UIImage.templateImage(asset: .vhcGenericInHealthyRangeSmall)
            let cellData = MWBAssessmentQuestionnaireCellData(title: title,
                                                              buttonText: buttonText,
                                                              timeToComplete: timeToComplete,
                                                              completed: completed,
                                                              completeIcon: completeIcon,
                                                              typeKey: typeKey)
            mwbQuestionnaireData.append(cellData)
        }
    }
    
    public func configureHeader() {
        mwbHeaderData = nil
        let totalSections = getTotalNumberOfQuestionnaires()
        let completeSections = getTotalNumberOfCompletedQuestionnaires()
        let incompleteSections = totalSections - completeSections
        let completedImage = UIImage.templateImage(asset: .hsCardCheckmarkBig)
        let earnedPoints = getQuestionnairesTotalEarnedPoints()
        let potentialPoints = getQuestionnairesTotalPotentialPoints()
        let remainingPoints = potentialPoints - earnedPoints
        var title = ""
        var description = getHeaderDescriptionSubtext() ?? ""
        
        if incompleteSections >= 1 {
            title = CommonStrings.HomeCard.CardEarnTitleDynamic292(integerFormatter.string(from: remainingPoints as NSNumber) ?? "")
        } else if incompleteSections <= 0 {
            title = CommonStrings.Mwb.Landing.CompletedTitle1222
            description = CommonStrings.Mwb.LandingHeader1225(integerFormatter.string(from: earnedPoints as NSNumber) ?? "")
        }
        
        mwbHeaderData = MWBAssessmentHeaderCellData(title: title, description: description, totalSections: totalSections, completeSections: completeSections, completedImage: completedImage)
    }
    
    public func getTotalNumberOfQuestionnaires() -> Int {
        guard let numberOfQuestionnaires = mwbRealm.allMWBQuestionnaireProgressAndPointsTracking().first?.totalQuestionnaires else { return 0 }
        return numberOfQuestionnaires
    }
    
    public func getHeaderDescriptionSubtext() -> String? {
        if let description = mwbRealm.allMWBQuestionnaireProgressAndPointsTracking().first?.questionnaireSetTextDescription {
            guard !description.isEmpty else { return nil }
            return description
        }
        return nil
    }
    
    public func getTotalNumberOfCompletedQuestionnaires() -> Int {
        guard let numberOfCompletedQuestionnaires = mwbRealm.allMWBQuestionnaireProgressAndPointsTracking().first?.totalQuestionnaireCompleted.value else { return 0 }
        return numberOfCompletedQuestionnaires
    }
    
    public func getQuestionnairesTotalPotentialPoints() -> Int {
        guard let totalPotentialPoints = mwbRealm.allMWBQuestionnaireProgressAndPointsTracking().first?.totalPotentialPoints.value else { return 0 }
        return totalPotentialPoints
    }
    
    public func getQuestionnairesTotalEarnedPoints() -> Int {
        guard let totalPotentialPoints = mwbRealm.allMWBQuestionnaireProgressAndPointsTracking().first?.totalEarnedPoints.value else { return 0 }
        return totalPotentialPoints
    }
    
    public func getQuestionnaireAt(index: Int) -> MWBQuestionnaire? {
        let questionnaireTypeKey = mwbQuestionnaireData[index].typeKey
        let questionnaireResult = mwbRealm.allMWBQuestionnaires().filter { $0.typeKey == questionnaireTypeKey }
        let questionnaire = questionnaireResult.first
        return questionnaire
    }
    
    public func getQuestionnaire(using selectedQuestionnaireTypeKey: Int) -> MWBQuestionnaire? {
        return mwbRealm.getMWBQuestionnaire(using: selectedQuestionnaireTypeKey)
    }
    
    public func getVHRQuestionnaireAt(index: Int) -> VHRQuestionnaire?{return nil}
    
    public func getVHRQuestionnaire(using selectedQuestionnaireTypeKey: Int) -> VHRQuestionnaire?{return nil}
    
    // MARK: Networking
    
    public func fetchQuestionnaires(forceUpdate: Bool = false, completion: @escaping (Error?, Bool?) -> Void) {
        
        if MWBCache.mwbDataIsOutdated() || forceUpdate == true {
            let coreRealm = DataProvider.newRealm()
            let partyId = coreRealm.getPartyId()
            let tenantId = coreRealm.getTenantId()
            let membershipId = coreRealm.getMembershipId()
            let setTypeKey = QuestionnaireSetRef.MWB
            let prePopulation = true
            let mwbQuestionnaireParameter = MWBQuestionnaireParameter(setTypeKey: setTypeKey, prePopulation: prePopulation)
            
            Wire.Member.MWBQuestionnaireProgressAndPointsTracking(tenantId: tenantId, partyId: partyId, vitalityMembershipId: membershipId, payload: mwbQuestionnaireParameter) { error in
                guard error == nil else {
                    completion(error, MWBCache.mwbDataIsOutdated())
                    return
                }
                self.configureQuestionnaires()
                completion(nil, nil)
            }
        } else {
            configureQuestionnaires()
            completion(nil, nil)
        }
    }
    
}

