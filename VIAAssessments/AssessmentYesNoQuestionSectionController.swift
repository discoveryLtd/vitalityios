import IGListKit
import VIAUIKit
import VIAUtilities
import VitalityKit
import RealmSwift
import VitalityKit
import VIACommon

public final class AssessmentYesNoQuestionSectionController: IGListSectionController, IGListSectionType, FootnoteToggleable {
    
    private static let trueString = CommonStrings.TrueTitle361
    private static let falseString = CommonStrings.FalseTitle362
    
    // MARK: Properties
    
    var realm: Realm
    
    var questionDetail: AssessmentQuestionPresentationDetail! {
        didSet {
            if let validAnswer = self.questionDetail.rawQuestion(in: self.realm)?.capturedResult()?.answer {
                self.isYesSelected = validAnswer == yesAnswerName
            }
        }
    }
    
    var footnoteDetail: AttributedTextDetail? {
        if self.questionDetail.textNote != nil {
            return self.questionDetail.noteDetail()
        }
        return nil
    }
    
    private var isYesSelected: Bool?
    
    var yesAnswerValue: String {
        return questionDetail.validValuesPresentationDetail?.first?.rawValidValue.value ?? AssessmentYesNoQuestionSectionController.trueString
    }
    
    var yesAnswerName: String {
        return questionDetail.validValuesPresentationDetail?.first?.rawValidValue.name ?? AssessmentYesNoQuestionSectionController.trueString
    }
    
    var noAnswerValue: String {
        return questionDetail.validValuesPresentationDetail?.last?.rawValidValue.value ?? AssessmentYesNoQuestionSectionController.falseString
    }
    
    var noAnswerName: String {
        return questionDetail.validValuesPresentationDetail?.last?.rawValidValue.name ?? AssessmentYesNoQuestionSectionController.falseString
    }
    
    // MARK: FootnoteToggleable
    
    var isFootnoteExpanded: Bool = false
    
    var indexForFootnoteCell: Int? {
        return self.footnoteDetail != nil ? 2 : nil
    }
    
    // MARK: Lifecycle
    
    public required init(realm: Realm) {
        self.realm = realm
    }
    
    // MARK: IGListSectionType
    
    public func numberOfItems() -> Int {
        var count = 2
        if self.footnoteDetail != nil {
            count = count + 1
        }
        return count
    }
    
    public func didUpdate(to object: Any) {
        self.questionDetail = object as! AssessmentQuestionPresentationDetail
    }
    
    public func cellForItem(at index: Int) -> UICollectionViewCell {
        if index == 0 {
            return attributedTextCell(for: self.questionDetail.textAndDescriptionDetail(), at: index)
        } else if index == 1 {
            return yesNoCell(at: index)
        } else if index == 2, let noteDetail = self.footnoteDetail {
            return footnoteAttributedTextCell(for: noteDetail, at: index)
        }
        return collectionContext!.dequeueReusableCell(of: UICollectionViewCell.self, for: self, at: index)
    }
    
    public func sizeForItem(at index: Int) -> CGSize {
        let width = collectionContext!.containerSize.width
        if index == 0 {
            return AssessmentTextCollectionViewCell.height(with: self.questionDetail.textAndDescriptionDetail(), constrainedTo: width)
        } else if index == 1 {
            return AssessmentYesNoAnswerCollectionViewCell.height(constrainedTo: width)
        } else if index == 2, let noteDetail = self.footnoteDetail {
            return AssessmentFootnoteCollectionViewCell.height(with: noteDetail, constrainedTo: width, isFootnoteExpanded: self.isFootnoteExpanded)
        }
        return CGSize.zero
    }
    
    public func didSelectItem(at index: Int) {
        if index == 2 {
            toggleFootnote()
        }
    }
    
    // MARK: Cells
    
    func attributedTextCell(for textDetail: AttributedTextDetail, at index: Int) -> UICollectionViewCell {
        let cell = collectionContext!.dequeueReusableCell(of: AssessmentTextCollectionViewCell.self, for: self, at: index) as! AssessmentTextCollectionViewCell
        cell.label.attributedText = textDetail.attributedString
        cell.addTopBorder = true
        cell.addBottomBorder = false
        return cell
    }
    
    func footnoteAttributedTextCell(for textDetail: AttributedTextDetail, at index: Int) -> UICollectionViewCell {
        let cell = collectionContext!.dequeueReusableCell(of: AssessmentFootnoteCollectionViewCell.self, for: self, at: index) as! AssessmentFootnoteCollectionViewCell
        cell.setLabelAttributedText(textDetail.attributedString, isFootnoteExpanded: self.isFootnoteExpanded)
        return cell
    }
    
    func yesNoCell(at index: Int) -> UICollectionViewCell {
        let cell = collectionContext!.dequeueReusableCell(of: AssessmentYesNoAnswerCollectionViewCell.self, for: self, at: index) as! AssessmentYesNoAnswerCollectionViewCell
        cell.addTopBorder = false
        cell.addBottomBorder = true
        cell.isYesSelected = isYesSelected
        cell.didAnswer = self.didAnswer
        cell.yesButton.setTitle(self.yesAnswerValue, for: .normal)
        cell.noButton.setTitle(self.noAnswerValue, for: .normal)
        return cell
    }
    
    // MARK: Actions
    
    func didAnswer(answer: Bool) {
        self.isYesSelected = answer
        let answerName = answer ? yesAnswerName : noAnswerName
        self.updateCapturedResult(with: answerName)
    }
    
    func updateCapturedResult(with answer: String) {
        guard let type = self.questionDetail.rawQuestion(in: realm)?.questionType else { return }
        
        if let controller = self.viewController as? AssessmentQuestionnaireSectionViewController {
            controller.answeringDelegate?.persist(answer,
                                                  unit: nil,
                                                  for: self.questionDetail.rawQuestionTypeKey,
                                                  of: type,
                                                  allowsMultipleAnswers: false,
                                                  answerIsValid: true)
        }
    }
    
}


public final class AssessmentYesNoAnswerCollectionViewCell: UICollectionViewCell, Nibloadable {
    
    private static let fontSize: CGFloat = Bundle.main.object(forInfoDictionaryKey: "VIAAssessmentYesNoFontSize") as? CGFloat ?? 45.0
    
    fileprivate static let topInset: CGFloat = 15.0
    
    fileprivate static let bottomInset: CGFloat = 25.0
    
    //  Size of the cell
    fileprivate static let yesNoHeight: CGFloat = 52.0
    
    public var addTopBorder = false
    
    public var addBottomBorder = false
    
    public var didAnswer: (_ answer: Bool) -> Void = { answer in
//        debugPrint("YesNo answer toggled to: \(answer)")
    }
    
    public var isYesSelected: Bool? {
        willSet {
            updateButton(yesButton, isSelected: nil)
            updateButton(noButton, isSelected: nil)
        }
        didSet {
            if let validAnswer = isYesSelected {
                updateButton(yesButton, isSelected: validAnswer)
                updateButton(noButton, isSelected: !validAnswer)
            }
        }
    }
    
    // MARK: Views
    
    private func newButton() -> UIButton {
        let button = UIButton(type: .custom)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.titleLabel?.font = UIFont.systemFont(ofSize: AssessmentYesNoAnswerCollectionViewCell.fontSize, weight: UIFontWeightRegular)
        button.setTitleColor(UIColor.mediumGrey(), for: .normal)
        button.addTarget(self, action: #selector(answerSelected(_:)), for: .touchUpInside)
        return button
    }
    
    lazy fileprivate var yesButton: UIButton = {
        return self.newButton()
    }()
    
    lazy fileprivate var noButton: UIButton = {
        return self.newButton()
    }()
    
    // MARK: Init
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupSubviews()
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        
        contentView.layer.removeAllBorderLayers()
        if addTopBorder {
            contentView.layer.addBorder(edge: .top)
        }
        if addBottomBorder {
            contentView.layer.addBorder(edge: .bottom)
        }
    }
    
    private func setupSubviews() {
        contentView.backgroundColor = .day()
        
        let stackView = UIStackView()
        contentView.addSubview(stackView)
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .horizontal
        stackView.distribution = .fillEqually
        stackView.addArrangedSubview(yesButton)
        stackView.addArrangedSubview(noButton)
        
        stackView.snp.makeConstraints { (make) in
            make.top.equalToSuperview().inset(AssessmentYesNoAnswerCollectionViewCell.topInset)
            make.bottom.equalToSuperview().inset(AssessmentYesNoAnswerCollectionViewCell.bottomInset)
            make.left.right.equalToSuperview()
        }
    }
    
    override public func preferredLayoutAttributesFitting(_ layoutAttributes: UICollectionViewLayoutAttributes) -> UICollectionViewLayoutAttributes {
        return self.defaultContentViewLayoutSizeFitting(layoutAttributes)
    }
    
    // MARK: Height calculation
    
    public class func height(constrainedTo width: CGFloat) -> CGSize {
        let height = yesNoHeight + AssessmentYesNoAnswerCollectionViewCell.topInset + AssessmentYesNoAnswerCollectionViewCell.bottomInset
        return CGSize(width: width, height: height)
    }
    
    // MARK: Actions
    
    func answerSelected(_ sender: UIButton?) {
        if sender === yesButton {
            isYesSelected = true
        } else if sender === noButton {
            isYesSelected = false
        }
        
        if let validAnswer = isYesSelected {
            didAnswer(validAnswer)
        }
    }
    
    func updateButton(_ button: UIButton, isSelected: Bool?) {
        if let validSelected = isSelected, validSelected == true {
            button.titleLabel?.font = UIFont.systemFont(ofSize: AssessmentYesNoAnswerCollectionViewCell.fontSize, weight: UIFontWeightMedium)
            button.setTitleColor(UIColor.currentGlobalTintColor(), for: .normal)
        } else {
            button.titleLabel?.font = UIFont.systemFont(ofSize: AssessmentYesNoAnswerCollectionViewCell.fontSize, weight: UIFontWeightRegular)
            button.setTitleColor(UIColor.mediumGrey(), for: .normal)
        }
    }
    
}
