import Foundation
import VIACommon
import VitalityKit
import VIAUIKit
import VitalityKit

public class AssessmentOnboardingViewModel: OnboardingViewModel {
    
    public var contentItems: [OnboardingContentItem] {
        return [OnboardingContentItem]()
    }
    
    public var heading: String {
        return ""
    }
    
    public func setOnboardingHasShown() {
        fatalError("Not implemented")
    }
    
    public var buttonTitle: String {
        return CommonStrings.GenericGotItButtonTitle131
    }
    
    public var alternateButtonTitle: String? {
        return nil
    }
    
    public var alternateButtonIdentifier: String? {
        return nil
    }
    
    public var gradientColor: Color {
        return .green
    }
    
    public var mainButtonHighlightedTextColor: UIColor? {
        return .knowYourHealthGreen()
    }
    
}

class AssessmentOnboardingViewController: VIAOnboardingViewController, KnowYourHealthTintable {
    
    var assessmentViewModel: AssessmentOnboardingViewModel? {
        didSet {
            self.viewModel = assessmentViewModel
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.navigationBar.isHidden = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.hideBackButtonTitle()
    }
    
    // MARK: - Navigation
    
    override func mainButtonTapped(_ sender: UIButton) {
        assessmentViewModel?.setOnboardingHasShown()
        self.dismiss(animated: true, completion: nil)
    }
    
    override func alternateButtonTapped(_ sender: UIButton) {
        if let identifier = assessmentViewModel?.alternateButtonIdentifier {
            self.performSegue(withIdentifier: identifier, sender: self)
        } else {
            self.performSegue(withIdentifier: "showDisclaimer", sender: self)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDisclaimer", let controller = segue.destination as? VIAWebContentViewController {
            let webContentViewModel = VIAWebContentViewModel(articleId: AppConfigFeature.contentId(for: .VHRDisclaimerContent))
            controller.viewModel = webContentViewModel
            controller.title = CommonStrings.DisclaimerTitle265
        }
    }
}
