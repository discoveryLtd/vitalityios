import VitalityKit
import VitalityKit
import VIAUIKit

public class VNAOnboardingViewModel: AssessmentOnboardingViewModel {

    let realm = DataProvider.newRealm()
    
    public override var heading: String {
        return CommonStrings.Onboarding.Title413
    }
    
    public override var alternateButtonTitle: String? {
        return CommonStrings.LearnMoreButtonTitle104
    }
    
    public override var alternateButtonIdentifier: String? {
        return "showLearnMore"
    }

    public override var contentItems: [OnboardingContentItem] {
        var numberOfQuestionSections = 3 // Number of sections by default.
        if let card = realm.homeCard(by: .VitNutAssessment) {
            // Get the actual number of questionnaire sections from the homescreen card data.
            numberOfQuestionSections = card.total
        }
        
        var items: [OnboardingContentItem] = [OnboardingContentItem]()
        items.append(OnboardingContentItem(
            heading: CommonStrings.Vna.Onboarding.Section1Title408,
            content: CommonStrings.Vna.Onboarding.Section1Message407("\(numberOfQuestionSections)"),
            image: VIAAssessmentsAsset.vhrOnboardingCompleteTime.image))
        items.append(OnboardingContentItem(
            heading: CommonStrings.Vna.Onboarding.Section2Title410,
            content: CommonStrings.Vna.Onboarding.Section2Message409,
            image: VIAAssessmentsAsset.vhrOnboardingEarnPoints.image))
        items.append(OnboardingContentItem(
            heading: CommonStrings.Vna.Onboarding.Section3Title412,
            content: CommonStrings.Vna.Onboarding.Section3Message411,
            image: VIAAssessmentsAsset.Vna.vnaNutritiionActive.image))
        return items
    }

    public override func setOnboardingHasShown() {
        AppSettings.setHasShownVNAOnboarding()
    }
}
