//
//  MWBAssessmentNumberRangeSingleLineQuestionSectionController.swift
//  VitalityActive
//
//  Created by Michelle R. Oratil on 14/02/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import IGListKit
import VIAUIKit
import VIAUtilities
import VitalityKit
import VitalityKit
import RealmSwift


public final class MWBAssessmentNumberRangeSingleLineQuestionSectionController: IGListSectionController, IGListSectionType, MWBAssessmentCaptureResultsDelegate, FootnoteToggleable {
    
    // MARK: Properties
    
    var realm: Realm
    
    var questionDetail: MWBAssessmentQuestionPresentationDetail! {
        didSet {
            if let validAnswers = self.questionDetail.validValuesPresentationDetail {
                let validValues = (validAnswers.map({ $0.rawValidValue }))
                self.metric = MWBAssessmentMetric(validValues: validValues)
            } else if let unitsOfMeasure = self.questionDetail.rawQuestion(in: realm)?.unitsOfMeasure {
                debugPrint("No valid values to validate against, using question's units of measure")
                let values = unitsOfMeasure.flatMap({ $0.unitOfMeasureRef() })
                debugPrint("Setting first unit of measure as selected type")
                if let firstUnitOfMeasure = values.first {
                    self.selectedUnitOfMeasureType = firstUnitOfMeasure
                }
                self.metric = MWBAssessmentMetric(unitsOfMeasure: Array(values))
            } else {
                debugPrint("No valid values to validate against, nor units of measure on the question")
            }
        }
    }
    
    var decimalFormatter = Localization.decimalFormatter
    
    var serviceFormatter = NumberFormatter.serviceFormatter()
    
    var metric: MWBAssessmentMetric?
    
    var selectedUnit: Unit?
    
    var input: String?
    
    var isValid: Bool = false
    
    var selectedUnitOfMeasureType: UnitOfMeasureRef = UnitOfMeasureRef.Unknown
    
    // MARK: FootnoteToggleable
    
    var isFootnoteExpanded: Bool = false
    
    var indexForFootnoteCell: Int? {
        return self.footnoteDetail != nil ? 1 : nil
    }
    
    // MARK: Lifecycle
    
    public required init(realm: Realm) {
        self.realm = realm
    }
    
    // MARK: Data
    
    var capturedResult: MWBCapturedResult? {
        return self.questionDetail.rawQuestion(in: self.realm)?.capturedResult()
    }
    
    var footnoteDetail: AttributedTextDetail? {
        if self.questionDetail.textNote != nil {
            return self.questionDetail.noteDetail()
        }
        return nil
    }
    
    // MARK: IGListSectionType
    
    public func numberOfItems() -> Int {
        var count = 1
        if self.footnoteDetail != nil {
            count = count + 1
        }
        return count
    }
    
    public func didUpdate(to object: Any) {
        self.questionDetail = object as! MWBAssessmentQuestionPresentationDetail
    }
    
    public func cellForItem(at index: Int) -> UICollectionViewCell {
        if index == 0 {
            return inputCell(for: self.questionDetail.text.text, at: index)
        } else if index == 1, let noteDetail = self.footnoteDetail {
            return footnoteAttributedTextCell(for: noteDetail, at: index)
        }
        return collectionContext!.dequeueReusableCell(of: UICollectionViewCell.self, for: self, at: index)
    }
    
    public func sizeForItem(at index: Int) -> CGSize {
        let width = collectionContext!.containerSize.width
        
        //        if index == 0 {
        //            return MWBAssessmentTextCollectionViewCell.height(with: self.questionDetail.textAndDescriptionDetail(), constrainedTo: width)
        //        } else
        if index == 0 {
            return CGSize(width: width, height: 48)
        } else if index == 1, let noteDetail = self.footnoteDetail {
            return MWBAssessmentFootnoteCollectionViewCell.height(with: noteDetail, constrainedTo: width, isFootnoteExpanded: self.isFootnoteExpanded)
        }
        
        return CGSize.zero
    }
    
    public func didSelectItem(at index: Int) {
        if index == 1 {
            toggleFootnote()
        }
    }
    
    // MARK: Cells
    
    func attributedTextCell(for textDetail: AttributedTextDetail, at index: Int) -> UICollectionViewCell {
        let cell = collectionContext!.dequeueReusableCell(of: MWBAssessmentTextCollectionViewCell.self, for: self, at: index) as! MWBAssessmentTextCollectionViewCell
        cell.label.attributedText = textDetail.attributedString
        cell.addTopBorder = true
        cell.addBottomBorder = true
        return cell
    }
    
    func footnoteAttributedTextCell(for textDetail: AttributedTextDetail, at index: Int) -> UICollectionViewCell {
        let cell = collectionContext!.dequeueReusableCell(of: MWBAssessmentFootnoteCollectionViewCell.self, for: self, at: index) as! MWBAssessmentFootnoteCollectionViewCell
        cell.setLabelAttributedText(textDetail.attributedString, isFootnoteExpanded: self.isFootnoteExpanded)
        return cell
    }
    
    func inputCell(for textDetail: String, at index: Int) -> UICollectionViewCell {
        let cell = collectionContext!.dequeueReusableCell(withNibName: MWBAssessmentNumberRangeAnswerCollectionViewCell.defaultReuseIdentifier, bundle: MWBAssessmentNumberRangeAnswerCollectionViewCell.bundle(), for: self, at: index) as! MWBAssessmentNumberRangeAnswerCollectionViewCell
        cell.captureResultsDelegate = self
        cell.unitButton.setTitle(textDetail, for: .normal)
        cell.unitButton.isHidden = false
        cell.unitButton.setTitleColor(UIColor.mediumGrey(), for: .normal)
        cell.inputTextField.keyboardType = .numberPad
        
        // textFieldTextDidChange
        cell.textFieldTextDidChange = { [unowned self] textField, unit, min, max in
            var uomType = self.selectedUnitOfMeasureType
            if let metric = self.metric, metric.validate(textField.text, unit, &uomType, &min, &max) {
                cell.updateInputValid(true, nil, nil, cell.validationErrorLabel)
            }
        }
        
        // textFieldDidEndEditing
        cell.textFieldDidEndEditing = { [unowned self] textField, unit, min, max in
            var uomType = self.selectedUnitOfMeasureType
            
            //ge20180123 : force integer
            if(textField.text != "" && Double(textField.text!) != nil) {
                textField.text = "\(Int(floor(Double(textField.text!)!)))"
            }
            
            if let isValid = self.metric?.validate(textField.text, unit, &uomType, &min, &max) {
                self.updateValues(isValid: isValid, uomType: uomType, unit: unit, inputText: textField.text)
                self.updateCapturedResult()
                cell.updateInputValid(isValid, min, max, cell.validationErrorLabel)
            }
            
        }
        
        // configure the cell
        let inputText = localFormatted(from: self.capturedResult?.answer)
        cell.configureCaptureResultCell(units: self.metric?.rawUnits ?? [],
                                        inputPlaceholder: CommonStrings.Assessment.GenericInputPlaceholder507,
                                        inputText: inputText,
                                        inputUnitOfMeasure: self.capturedResult?.unitOfMeasure)
        
        //Collapse second input view
        cell.secondUserInputStackView.isHidden = true
        cell.secondInputField.isHidden = cell.secondUserInputStackView.isHidden
        cell.secondValidationErrorLabel.isHidden = cell.secondUserInputStackView.isHidden
        
        // update validation for cell, to be visually correct
        // directly after being displayed
        let unit = self.selectedUnit
        var uomType = selectedUnitOfMeasureType
        var min: Double?
        var max: Double?
        if let isValid = self.metric?.validate(inputText, unit, &uomType, &min, &max) {
            self.updateValues(isValid: isValid, uomType: uomType, unit: unit, inputText: inputText)
            cell.updateInputValid(isValid, min, max, cell.validationErrorLabel)
        }
        
        
        return cell
    }
    
    func updateValues(isValid: Bool, uomType: UnitOfMeasureRef, unit: Unit?, inputText: String?) {
        self.isValid = isValid
        self.selectedUnitOfMeasureType = uomType
        self.selectedUnit = unit
        self.input = inputText
    }
    
    // MARK: CaptureResultsDelegate
    
    public func show(menu: UIAlertController) {
        if let controller = self.viewController {
            controller.present(menu, animated: true, completion: nil)
        }
    }
    
    public func didSelectUnitOfMeasure(_ unitOfMeasure: UnitOfMeasureRef) {
        self.selectedUnitOfMeasureType = unitOfMeasure
    }
    
    // MARK: Persist
    
    func updateCapturedResult() {
        // safety checks
        guard let validInput = self.input else {
            return
        }
        guard let systemFormattedNumberString = systemFormatted(from: validInput) else {
            deleteCapturedResult(with: validInput)
            return
        }
        guard let type = self.questionDetail.rawQuestion(in: realm)?.questionType else {
            return
        }
        
        // warn and persist
        if self.selectedUnitOfMeasureType == .Unknown {
            debugPrint("Capturing result with Unknown unit of measure. This happens when neither the question nor the valid values have any UOM configured.")
        }
        if let controller = self.viewController as? MWBAssessmentQuestionnaireSectionViewController {
            controller.answeringDelegate?.persist(systemFormattedNumberString,
                                                  unit: self.selectedUnitOfMeasureType,
                                                  for: self.questionDetail.rawQuestionTypeKey,
                                                  of: type,
                                                  allowsMultipleAnswers: false,
                                                  answerIsValid: self.isValid)
        }
    }
    
    func deleteCapturedResult(with text: String) {
        if let controller = self.viewController as? MWBAssessmentQuestionnaireSectionViewController {
            controller.answeringDelegate?.delete(text, for: self.questionDetail.rawQuestionTypeKey, allowsMultipleAnswers: false)
        }
    }
    
    private func systemFormatted(from localizedString: String?) -> String? {
        guard let validString = localizedString else { return nil }
        guard let number = decimalFormatter.number(from: validString) else { return nil }
        return serviceFormatter.string(from: number)
    }
    
    private func localFormatted(from serviceString: String?) -> String? {
        guard let validString = serviceString else { return nil }
        guard let number = serviceFormatter.number(from: validString) else { return nil }
        return decimalFormatter.string(from: number)
    }
    
}
