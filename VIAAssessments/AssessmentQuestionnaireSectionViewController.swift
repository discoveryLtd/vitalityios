import Foundation
import IGListKit
import VitalityKit
import VIAUtilities
import VIAUIKit
import RealmSwift
import VIACommon

public protocol AssessmentQuestionnaireSectionViewControllerDelegate: class {

    func sectionControllerDidSelectNext(_ vhrQuestionnaireSectionViewController: AssessmentQuestionnaireSectionViewController)

    func sectionControllerDidSelectBack(_ vhrQuestionnaireSectionViewController: AssessmentQuestionnaireSectionViewController)

    func sectionControllerDidSelectClose(_ vhrQuestionnaireSectionViewController: AssessmentQuestionnaireSectionViewController)

    func sectionControllerDidSelectDone(_ vhrQuestionnaireSectionViewController: AssessmentQuestionnaireSectionViewController)

}

public final class AssessmentQuestionnaireSectionViewController: VIAViewController, IGListAdapterDataSource, AssessmentQuestionnaireSectionViewModelDelegate {

    // MARK: Properties

    weak var navigationDelegate: AssessmentQuestionnaireSectionViewControllerDelegate?

    weak var answeringDelegate: AssessmentQuestionnaireAnsweringDelegate?

    private (set) var isFirstSection: Bool = false

    private (set) var isLastSection: Bool = false
    
    private (set) var questionnaireSectionTypeKey: Int?
    
    private  var _viewModel: AssessmentQuestionnaireSectionViewModel?
    private (set) var viewModel: AssessmentQuestionnaireSectionViewModel? {
        set {
            self._viewModel = newValue
        }
        get {
            if _viewModel == nil {
                let viewModel = AssessmentQuestionnaireSectionViewModel(questionnaireSectionTypeKey: self.questionnaireSectionTypeKey,
                                                                    isLastSection: self.isLastSection)
                viewModel?.delegate = self
                self._viewModel = viewModel
            }
            return _viewModel
        }
    }

    let collectionView: IGListCollectionView = {
        let collectionView = IGListCollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
        collectionView.backgroundColor = UIColor.tableViewBackground()
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        return collectionView
    }()

    lazy var adapter: IGListAdapter = {
        return IGListAdapter(updater: IGListAdapterUpdater(), viewController: self, workingRangeSize: 0)
    }()

    // MARK: View lifecycle

    override public func viewDidLoad() {
        super.viewDidLoad()

        configureAppearance()
        configureCollectionView()
        configureAdapter()
        addBarButtonItems()

        addNotificationWithCompletion { [weak self] (notification) in
            guard let strongSelf = self else { return }
            strongSelf.navigationDelegate?.sectionControllerDidSelectClose(strongSelf)
        }
    }
    
    
    func configure(isFirstSection: Bool, isLastSection: Bool, questionnaireSectionTypeKey: Int) {
        self.isFirstSection = isFirstSection
        self.isLastSection = isLastSection
        self.questionnaireSectionTypeKey = questionnaireSectionTypeKey
    }
    
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.configureKeyboardAutoToolbar(VIAApplicableFeatures.default.enableKeyboardAutoToolbar ?? false)

    }
    
    func tearDownViewModel() {
        viewModel = nil
    }
    
    public override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.disableKeyboardAutoToolbar()
    }
    
    public override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        tearDownViewModel()
    }

    deinit {
        debugPrint("AssessmentQuestionnaireSectionViewController deinit")
    }

    // MARK: View config

    func configureAppearance() {
        navigationController?.setNavigationBarHidden(false, animated: false)
        navigationController?.makeNavigationBarTransparent()
        navigationController?.navigationBar.barTintColor = .white
    }

    func configureCollectionView() {
        view.addSubview(collectionView)
        collectionView.snp.makeConstraints { (make) in
            //Xcode 9
            if #available(iOS 11, *) {
                let window = UIApplication.shared.keyWindow
                let topPadding = window?.safeAreaInsets.top
                make.top.equalTo(150 + topPadding!)
                make.left.bottom.right.equalToSuperview()
            } else {
                make.top.left.bottom.right.equalToSuperview()
            }
            //Xcode 9
            //make.top.left.bottom.right.equalToSuperview()
        }
    }

    func configureAdapter() {
        adapter.collectionView = collectionView
        adapter.dataSource = self
    }

    func addBarButtonItems() {
        if !self.isFirstSection {
            self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage.templateImage(asset: ASSAsset.vhrBackArrow), style: .plain, target: self, action: #selector(back(_:)))
        }

        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: CommonStrings.GenericCloseButtonTitle10, style: .plain, target: self, action: #selector(close(_:)))

    }

    // MARK: - Navigation

    func close(_ sender: Any?) {
        collectionView.endEditing(true)
        navigationDelegate?.sectionControllerDidSelectClose(self)
    }

    func next(_ sender: Any?) {
        navigationDelegate?.sectionControllerDidSelectNext(self)
    }

    func back(_ sender: Any?) {
        navigationDelegate?.sectionControllerDidSelectBack(self)
    }

    func done(_ sender: Any?) {
        navigationDelegate?.sectionControllerDidSelectDone(self)
    }

    // MARK: IGListAdapterSource

    public func objects(for listAdapter: IGListAdapter) -> [IGListDiffable] {
        if let data = viewModel?.data {
            return data
        }
        return [] as! [IGListDiffable]
    }

    public func listAdapter(_ listAdapter: IGListAdapter, sectionControllerFor object: Any) -> IGListSectionController {
        if let detail = object as? AssessmentQuestionPresentationDetail, let realm = viewModel?.realm {
            if detail.presentationType == .Date && detail.decorator == .DatePicker {
                return AssessmentDateQuestionSectionController(realm: realm)
            } else if detail.presentationType == .DecimalRange && detail.decorator == .Textbox {
                return AssessmentNumberRangeQuestionSectionController(realm: realm)
            } else if detail.presentationType == .FreeText && detail.decorator == .Textbox {
                return AssessmentFreeTextSectionController(realm: realm)
            } else if detail.presentationType == .MultiSelect && detail.decorator == .Checkbox {
                return AssessmentMultiSelectQuestionSectionController(realm: realm)
            } else if detail.presentationType == .NumberRange && detail.decorator == .Textbox {
                return AssessmentNumberRangeQuestionSectionController(realm: realm)
            } else if detail.presentationType == .ChildNumberRange && detail.decorator == .Textbox {
                return AssessmentNumberRangeSingleLineQuestionSectionController(realm: realm)
            } else if detail.presentationType == .SingleSelect && detail.decorator == .RadioButton {
                return AssessmentSingleSelectQuestionSectionController(realm: realm)
            } else if detail.presentationType == .SingleSelect && detail.decorator == .Toggle {
                return AssessmentToggleQuestionSectionController(realm: realm)
            } else if detail.presentationType == .SingleSelect && detail.decorator == .YesNo {
                return AssessmentYesNoQuestionSectionController(realm: realm)
            } else if detail.presentationType == .Label && detail.decorator == .Label {
                return AssessmentLabelQuestionSectionController(realm: realm)
            } else if detail.presentationType == .Label && detail.decorator == .CollapseUoM {
                return AssessmentLabelQuestionSectionController(realm: realm)
            }else if detail.presentationType == .FormatText && detail.decorator == .Textbox {
                return AssessmentNumberRangeQuestionSectionController(realm: realm)
            }else {
                debugPrint("We do not cater for this combination: \(detail.presentationType.rawValue) + \(detail.decorator.rawValue)")
                return SpacingController()
            }
        } else if object is AssessmentQuestionnaireSectionHeaderFooterDetail {
            return AssessmentQuestionnaireHeaderFooterSectionController()
        } else if object is AssessmentQuestionPrepopulationPresentationDetail {
            return AssessmentQuestionPrepopulationSectionController()
        } else if object is AssessmentQuestionFootnotePresentationDetail {
            return AssessmentQuestionFootnoteSectionController()
        } else if object is AssessmentQuestionnaireSectionNavigationDetail {
            return AssessmentNavigationButtonSectionController()
        } else if object is AssessmentRequiredPrompt {
            return AssessmentRequiredPromptController()
        }
        return SpacingController()
    }

    public func emptyView(for listAdapter: IGListAdapter) -> UIView? { return nil }

    // MARK: VHRQuestionnaireSectionViewModelDelegate

    public func viewModelDidUpdateData() {
        /*
         * No need to update the UI (list adapter) when the update comes from a textfield
         * since the input is directly presented on the view, hence, this checker below.
         */
        if VIAApplicableFeatures.default.enableKeyboardAutoToolbar! {
            if !self.isKeyboardShowing() {
                performAdapterUpdates()
            }
        } else {
            performAdapterUpdates()
        }
    }
    
    func performAdapterUpdates() {
        self.adapter.performUpdates(animated: true, completion: { finished in })
    }
    
    public func viewModelDidUpdateCapturedResults() {
        // reload only the navigation detail, not everything.
        // if we reload everything, the date picker freaks out and the 
        // number range validation disappears immediately after showing
        if let controller = self.adapter.sectionController(for: self.viewModel?.navigationDetail as Any) {
            controller.collectionContext?.performBatch(animated: true, updates: {
                controller.collectionContext?.reload(controller)
            }, completion: nil)
        }
    }
    public func scrollTo(questionDetail: AssessmentQuestionPresentationDetail){
        
        if let details = self.adapter.objects().filter({ $0 is AssessmentQuestionPresentationDetail }) as? [AssessmentQuestionPresentationDetail] {
            if let filtered = details.filter({ $0.text.isEqual(toDiffableObject: questionDetail.text) }).first {
                if let controller = self.adapter.sectionController(for: filtered) {
                    controller.collectionContext?.scroll(to: controller, at: 0, scrollPosition: .centeredVertically, animated: false)
                }
            }
        }
    }
    
    public func reloadSectionController(for questionDetail: AssessmentQuestionPresentationDetail) {
        if let details = self.adapter.objects().filter({ $0 is AssessmentQuestionPresentationDetail }) as? [AssessmentQuestionPresentationDetail] {
            if let filtered = details.filter({ $0.text.isEqual(toDiffableObject: questionDetail.text) }).first {
                if let controller = self.adapter.sectionController(for: filtered) {
                    controller.collectionContext?.performBatch(animated: true, updates: {
                        controller.collectionContext?.reload(controller)
                    }, completion: nil)
                }
            }
        }
    }
}


