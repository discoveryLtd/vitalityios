import VitalityKit
import VitalityKit
import VIAUIKit
import RealmSwift
import VIAUtilities

public class VNALandingViewModel: VNAAssessmentLandingViewModel {

    // MARK: Properties
    public var vnaRealm = DataProvider.newVNARealm()
    public var vnaQuestionnaireData = [VNAAssessmentQuestionnaireCellData]()
    public var vnaHeaderData: VNAAssessmentHeaderCellData?
    public let integerFormatter = NumberFormatter.integerFormatter()
    public var questionnairesFooterText: String? {
        return vnaRealm.allVNAQuestionnaireProgressAndPointsTracking().first?.questionnaireSetTextNote
    }

    // MARK: Lifecycle

    public init() {

    }

    // MARK: Functions

    public func shouldShowOnboarding() -> Bool {
        return !AppSettings.hasShownVNAOnboarding()
    }

    public func configureQuestionnaires() {
        vnaRealm.refresh()
        vnaQuestionnaireData.removeAll()
        configureHeader()
        let  questionnaires = vnaRealm.allSortedVNAQuestionnaires()

        for questionnaire in questionnaires {
            let typeKey = questionnaire.typeKey
            let title = questionnaire.typeName ?? ""
            var timeToComplete = questionnaire.textDescription
            var buttonText = CommonStrings.LandingScreen.StartButton397
            let completionState = questionnaire.getCompletionState()
            if completionState == .Complete {
                buttonText = CommonStrings.LandingScreen.EditButton392
                timeToComplete = CommonStrings.Vna.LandingScreen.CompletedMessage390
            } else if completionState == .InProgress {
                buttonText = CommonStrings.LandingScreen.ContinueButton391
            }
            let completed = questionnaire.completionFlag
            let completeIcon = UIImage.templateImage(asset: .vhcGenericInHealthyRangeSmall)
            let cellData = VNAAssessmentQuestionnaireCellData(title: title,
                                                    buttonText: buttonText,
                                                    timeToComplete: timeToComplete,
                                                    completed: completed,
                                                    completeIcon: completeIcon,
                                                    typeKey: typeKey)
            vnaQuestionnaireData.append(cellData)
        }
    }

    public func configureHeader() {
        vnaHeaderData = nil
        let totalSections = getTotalNumberOfQuestionnaires()
        let completeSections = getTotalNumberOfCompletedQuestionnaires()
        let incompleteSections = totalSections - completeSections
        let completedImage = UIImage.templateImage(asset: .hsCardCheckmarkBig)
        let earnedPoints = getQuestionnairesTotalEarnedPoints()
        let potentialPoints = getQuestionnairesTotalPotentialPoints()
        var title = ""
        var description = getHeaderDescriptionSubtext() ?? ""

        if incompleteSections >= 1 {
            title = CommonStrings.HomeCard.CardEarnTitle292(integerFormatter.string(from: potentialPoints as NSNumber) ?? "")
        } else if incompleteSections <= 0 {
            title = CommonStrings.Vna.LandingScreen.CompletedMessage390
            description = CommonStrings.Vna.Landing.Header.DescriptionCompleted389(integerFormatter.string(from: earnedPoints as NSNumber) ?? "")
        }

        vnaHeaderData = VNAAssessmentHeaderCellData(title: title, description: description, totalSections: totalSections, completeSections: completeSections, completedImage: completedImage)
    }

    public func getTotalNumberOfQuestionnaires() -> Int {
        guard let numberOfQuestionnaires = vnaRealm.allVNAQuestionnaireProgressAndPointsTracking().first?.totalQuestionnaires else { return 0 }
        return numberOfQuestionnaires
    }

    public func getHeaderDescriptionSubtext() -> String? {
        if let description = vnaRealm.allVNAQuestionnaireProgressAndPointsTracking().first?.questionnaireSetTextDescription {
            guard !description.isEmpty else { return nil }
            return description
        }
        return nil
    }

    public func getTotalNumberOfCompletedQuestionnaires() -> Int {
        guard let numberOfCompletedQuestionnaires = vnaRealm.allVNAQuestionnaireProgressAndPointsTracking().first?.totalQuestionnaireCompleted.value else { return 0 }
        return numberOfCompletedQuestionnaires
    }

    public func getQuestionnairesTotalPotentialPoints() -> Int {
        guard let totalPotentialPoints = vnaRealm.allVNAQuestionnaireProgressAndPointsTracking().first?.totalPotentialPoints.value else { return 0 }
        return totalPotentialPoints
    }

    public func getQuestionnairesTotalEarnedPoints() -> Int {
        guard let totalPotentialPoints = vnaRealm.allVNAQuestionnaireProgressAndPointsTracking().first?.totalEarnedPoints.value else { return 0 }
        return totalPotentialPoints
    }

    public func getQuestionnaireAt(index: Int) -> VNAQuestionnaire? {
        let questionnaireTypeKey = vnaQuestionnaireData[index].typeKey
        let questionnaireResult = vnaRealm.allVNAQuestionnaires().filter { $0.typeKey == questionnaireTypeKey }
        let questionnaire = questionnaireResult.first
        return questionnaire
    }

    public func getVNAQuestionnaire(using selectedQuestionnaireTypeKey: Int) -> VNAQuestionnaire? {
        return vnaRealm.getVNAQuestionnaire(using: selectedQuestionnaireTypeKey)
    }

    // MARK: Networking

    public func fetchQuestionnaires(forceUpdate: Bool = false, completion: @escaping (Error?, Bool?) -> Void) {
        if VNACache.vnaDataIsOutdated() || forceUpdate == true {
            let coreRealm = DataProvider.newRealm()
            let partyId = coreRealm.getPartyId()
            let tenantId = coreRealm.getTenantId()
            let membershipId = coreRealm.getMembershipId()
            let setTypeKey = QuestionnaireSetRef.VNA
            let prePopulation = true
            let vnaQuestionnaireParameter = VNAQuestionnaireParameter(setTypeKey: setTypeKey, prePopulation: prePopulation)

            Wire.Member.VNAQuestionnaireProgressAndPointsTracking(tenantId: tenantId, partyId: partyId, vitalityMembershipId: membershipId, payload: vnaQuestionnaireParameter) { error in
                guard error == nil else {
                    completion(error, VNACache.vnaDataIsOutdated())
                    return
                }
                self.configureQuestionnaires()
                completion(nil, nil)
            }
        } else {
            configureQuestionnaires()
            completion(nil, nil)
        }
    }

}
