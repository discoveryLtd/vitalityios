import IGListKit
import VIAUtilities
import VIAUIKit
import VitalityKit
import VitalityKit
import RealmSwift
import TTTAttributedLabel

public final class AssessmentQuestionnaireSectionNavigationDetail: NSObject, IGListDiffable {
    public var isLastSection: Bool = false
    public var isEnabled: Bool = false
    public var uuidString: String = UUID().uuidString

    public init(isLastSection: Bool = false, isEnabled: Bool = false) {
        self.isLastSection = isLastSection
        self.isEnabled = isEnabled
    }

    public func diffIdentifier() -> NSObjectProtocol {
        return uuidString as NSObjectProtocol
    }

    public func isEqual(toDiffableObject object: IGListDiffable?) -> Bool {
        guard self !== object else { return true }
        guard let object = object as? AssessmentQuestionnaireSectionNavigationDetail else { return false }
        return uuidString == object.uuidString
    }
}

public final class AssessmentQuestionnaireSectionHeaderFooterDetail: NSObject, IGListDiffable {
    public var text: TextDetail
    public var textDescription: TextDetail?
    public var textNote: TextDetail?
    public var uuidString = UUID().uuidString

    public init(text: TextDetail, textDescription: TextDetail? = nil, textNote: TextDetail? = nil) {
        self.text = text
        self.textDescription = textDescription
        self.textNote = textNote
    }

    public func diffIdentifier() -> NSObjectProtocol {
        return self.uuidString as NSObjectProtocol
    }

    public func isEqual(toDiffableObject object: IGListDiffable?) -> Bool {
        guard self !== object else { return true }
        guard let object = object as? AssessmentQuestionnaireSectionHeaderFooterDetail else { return false }
        return uuidString == object.uuidString
    }
}

public final class AssessmentValidValuePresentationDetail: NSObject, IGListDiffable {
    public var text: TextDetail
    public var textDescription: TextDetail?
    public var textNote: TextDetail?
    public var rawValidValue: VHRValidValue
    public var uuidString = UUID().uuidString

    public init(text: TextDetail, textDescription: TextDetail? = nil, textNote: TextDetail? = nil, rawValidValue: VHRValidValue) {
        self.text = text
        self.textDescription = textDescription
        self.textNote = textNote
        self.rawValidValue = rawValidValue
    }

    public func diffIdentifier() -> NSObjectProtocol {
        return self.uuidString as NSObjectProtocol
    }

    public func isEqual(toDiffableObject object: IGListDiffable?) -> Bool {
        guard self !== object else { return true }
        guard let object = object as? AssessmentValidValuePresentationDetail else { return false }
        return self.uuidString == object.uuidString
    }

    static private func == (lhs: AssessmentValidValuePresentationDetail, rhs: AssessmentValidValuePresentationDetail) -> Bool {
        return lhs.uuidString == rhs.uuidString
    }
}

public final class AssessmentQuestionPresentationDetail: NSObject, IGListDiffable {
    //public var header : String
    //public var isAnswered : Bool
    public var text: TextDetail
    public var textDescription: TextDetail?
    public var presentationType: QuestionTypeRef
    public var decorator: QuestionDecoratorRef
    public var validValuesPresentationDetail: [AssessmentValidValuePresentationDetail]?
    public var isVisible: Bool
    public var rawQuestionTypeKey: Int

    public init(text: TextDetail, textDescription: TextDetail? = nil, presentationType: QuestionTypeRef, decorator: QuestionDecoratorRef, validValues: [AssessmentValidValuePresentationDetail]? = nil, isVisible: Bool, questionTypeKey: Int) {
        //self.header = "Required"
        //self.isAnswered = isAnswered
        self.text = text
        self.textDescription = textDescription
        self.presentationType = presentationType
        self.decorator = decorator
        self.validValuesPresentationDetail = validValues
        self.isVisible = isVisible
        self.rawQuestionTypeKey = questionTypeKey
    }

    public func diffIdentifier() -> NSObjectProtocol {
        return self.text as NSObjectProtocol
    }

    public func isEqual(toDiffableObject object: IGListDiffable?) -> Bool {
        guard self !== object else { return true }
        guard let object = object as? AssessmentQuestionPresentationDetail else { return false }
        return text.text == object.text.text
    }

    public func rawQuestion(in realm: Realm) -> VHRQuestion? {
        return realm.question(by: self.rawQuestionTypeKey)
    }

    public func answerDetail(for answer: String) -> AssessmentValidValuePresentationDetail? {
        return self.validValuesPresentationDetail?.filter({ $0.rawValidValue.name == answer }).first
    }
}

public final class AssessmentQuestionFootnotePresentationDetail: NSObject, IGListDiffable {
    public var text: TextDetail

    public init(text: TextDetail) {
        self.text = text
    }

    public func diffIdentifier() -> NSObjectProtocol {
        return text as NSObjectProtocol
    }

    public func isEqual(toDiffableObject object: IGListDiffable?) -> Bool {
        guard self !== object else { return true }
        guard let object = object as? AssessmentQuestionFootnotePresentationDetail else { return false }
        return text.text == object.text.text
    }
}

public final class AssessmentQuestionPrepopulationPresentationDetail: NSObject, IGListDiffable {
    public var text: TextDetail

    public init(text: TextDetail) {
        self.text = text
    }

    public func diffIdentifier() -> NSObjectProtocol {
        return text as NSObjectProtocol
    }

    public func isEqual(toDiffableObject object: IGListDiffable?) -> Bool {
        guard self !== object else { return true }
        guard let object = object as? AssessmentQuestionPrepopulationPresentationDetail else { return false }
        return text.text == object.text.text
    }
}

protocol AttributedTextDetailPresentable {
    var text: TextDetail { get }
    var textDescription: TextDetail? { get }
    var textNote: TextDetail? { get }
    func textAndDescriptionDetail() -> AttributedTextDetail
    func textAndDescriptionAndNoteDetail() -> AttributedTextDetail
}

extension AttributedTextDetailPresentable {
    var textDescription: TextDetail? { return nil }
    var textNote: TextDetail? { return nil }
}

extension AttributedTextDetailPresentable {

    /**
     *ge20180411 : Required prompt with Icon
    */
    public func requiredErr() -> NSAttributedString {
        let requiredMsg = NSMutableAttributedString(string: "")
        let icon = NSTextAttachment()
        icon.image = VIAAssessmentsAsset.requiredEnabled.image
        icon.bounds = CGRect(x: 0, y: -3, width: 15, height: 15)
        requiredMsg.append(NSAttributedString(attachment: icon))
        let attr = [NSFontAttributeName: UIFont.systemFont(ofSize: 15)]
        requiredMsg.append(NSAttributedString(string: " Required", attributes: attr))
        return requiredMsg
    }
    
    public func textDetail() -> AttributedTextDetail {
        let string = "\(self.text.text!)"
        return attributedTextDetail(for: string)
    }

    public func textAndDescriptionDetail() -> AttributedTextDetail {
        var string = "\(self.text.text!)"
        if let textDescription = self.textDescription?.text {
            string = string + "\n\(textDescription)"
        }
        return attributedTextDetail(for: string)
    }

    public func textAndDescriptionAndNoteDetail() -> AttributedTextDetail {
        var string = "\(self.text.text!)"
        if let textDescription = self.textDescription?.text {
            string = string + "\n\(textDescription)"
        }
        if let textNote = self.textNote?.text {
            string = string + "\n\(textNote)"
        }
        return attributedTextDetail(for: string)
    }

    public func noteDetail() -> AttributedTextDetail {
        var string = ""
        if let textNote = self.textNote?.text {
            string = textNote
        }
        return attributedTextDetail(for: string, type: .footnote)
    }

    private func attributedTextDetail(for string: String, type: AttributedTextDetail.LayoutType = .normal) -> AttributedTextDetail {
        let attributedString = NSMutableAttributedString(string: string)
        AttributedTextDetail.addAttributesIfNeeded(to: attributedString, from: self.text)
        AttributedTextDetail.addAttributesIfNeeded(to: attributedString, from: self.textDescription)
        AttributedTextDetail.addAttributesIfNeeded(to: attributedString, from: self.textNote)
        return AttributedTextDetail(attributedString: attributedString, type: type)
    }

}

extension AssessmentQuestionnaireSectionHeaderFooterDetail: AttributedTextDetailPresentable { }
extension AssessmentQuestionPresentationDetail: AttributedTextDetailPresentable { }
extension AssessmentValidValuePresentationDetail: AttributedTextDetailPresentable { }
extension AssessmentQuestionFootnotePresentationDetail: AttributedTextDetailPresentable { }
extension AssessmentQuestionPrepopulationPresentationDetail: AttributedTextDetailPresentable { }


public final class AssessmentTextCollectionViewCell: UICollectionViewCell, Nibloadable {

    public static let inset: CGFloat = 15.0

    public var addTopBorder = false

    public var addBottomBorder = false

    // MARK: Views

    public lazy var label: UILabel = {
        let label = UILabel(frame: .zero)
        label.font = UIFont.title2Font()
        label.textColor = UIColor.night()
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    // MARK: Init

    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override init(frame: CGRect) {
        super.init(frame: frame)

        setupSubviews()
    }

    public override func layoutSubviews() {
        super.layoutSubviews()

        contentView.layer.removeAllBorderLayers()
        if addTopBorder {
            contentView.layer.addBorder(edge: .top)
        }
        if addBottomBorder {
            contentView.layer.addBorder(edge: .bottom)
        }
    }

    private func setupSubviews() {
        contentView.backgroundColor = .day()
        contentView.addSubview(self.label)
        self.label.snp.makeConstraints { (make) in
            make.top.bottom.equalToSuperview().inset(AssessmentTextCollectionViewCell.inset)
            make.left.right.equalToSuperview().inset(AssessmentTextCollectionViewCell.inset)
        }
    }

    override public func preferredLayoutAttributesFitting(_ layoutAttributes: UICollectionViewLayoutAttributes) -> UICollectionViewLayoutAttributes {
        return self.defaultContentViewLayoutSizeFitting(layoutAttributes)
    }

    // MARK: Height calculation

    public class func height(with attributedTextDetail: AttributedTextDetail, constrainedTo width: CGFloat) -> CGSize {
        let inset = AssessmentTextCollectionViewCell.inset
        let insets = UIEdgeInsets(top: 0, left: inset, bottom: 0, right: inset)
        let size = TextSize.size(attributedTextDetail.attributedString, width: width, insets: insets)
        let height = size.height + inset * 2
        return CGSize(width: width, height: ceil(height))
    }

}

public final class AssessmentHeaderCollectionViewCell: UICollectionViewCell, Nibloadable {
    
    public static let inset: CGFloat = 15.0
    
    // MARK: Views
    
    public lazy var label: UILabel = {
        let label = UILabel(frame: .zero)
        label.font = UIFont.footnoteFont()
        label.textColor = UIColor.night()
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    // MARK: Init
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupSubviews()
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        contentView.layer.removeAllBorderLayers()
    }
    
    private func setupSubviews() {
        contentView.backgroundColor = UIColor.tableViewBackground()
        contentView.addSubview(self.label)
        self.label.snp.makeConstraints { (make) in
            make.top.bottom.equalToSuperview().inset(AssessmentTextCollectionViewCell.inset)
            make.left.right.equalToSuperview().inset(AssessmentTextCollectionViewCell.inset)
        }
    }
    
    override public func preferredLayoutAttributesFitting(_ layoutAttributes: UICollectionViewLayoutAttributes) -> UICollectionViewLayoutAttributes {
        return self.defaultContentViewLayoutSizeFitting(layoutAttributes)
    }
    
    // MARK: Height calculation
    
    public class func height(with attributedTextDetail: AttributedTextDetail, constrainedTo width: CGFloat) -> CGSize {
        let inset = AssessmentHeaderCollectionViewCell.inset
        let insets = UIEdgeInsets(top: 0, left: inset, bottom: 0, right: inset)
        let size = TextSize.size(attributedTextDetail.attributedString, width: width, insets: insets)
        let height = size.height + inset * 2
        return CGSize(width: width, height: ceil(height))
    }
    
}


public final class AssessmentFootnoteCollectionViewCell: UICollectionViewCell, Nibloadable {

    public static let topInset: CGFloat = 8.0

    public static let sideInset: CGFloat = 15.0

    private static let limitedNumberOfLines: Int = 2

    var isExpanded: Bool = false

    // MARK: Views

    private lazy var label: TTTAttributedLabel = {
        let label = TTTAttributedLabel(frame: .zero)
        label.font = UIFont.footnoteFont()
        label.textColor = UIColor.darkGrey()
        label.numberOfLines = AssessmentFootnoteCollectionViewCell.limitedNumberOfLines
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    // MARK: Init

    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override init(frame: CGRect) {
        super.init(frame: frame)

        setupSubviews()
    }

    private func setupSubviews() {
        contentView.backgroundColor = UIColor.tableViewBackground()
        contentView.addSubview(self.label)
        self.label.snp.makeConstraints { (make) in
            make.top.equalToSuperview().inset(AssessmentFootnoteCollectionViewCell.topInset)
            make.bottom.equalToSuperview()
            make.left.right.equalToSuperview().inset(AssessmentFootnoteCollectionViewCell.sideInset)
        }
    }

    override public func preferredLayoutAttributesFitting(_ layoutAttributes: UICollectionViewLayoutAttributes) -> UICollectionViewLayoutAttributes {
        return self.defaultContentViewLayoutSizeFitting(layoutAttributes)
    }

    // MARK: Height calculation

    func setLabelAttributedText(_ attributedText: NSAttributedString?, isFootnoteExpanded: Bool) {
        if !isFootnoteExpanded {
            // Unicode Character 'HORIZONTAL ELLIPSIS' (U+2026)
            self.label.attributedTruncationToken = attributedText?.addTruncationToken(text: "\u{2026}" + CommonStrings.FootnoteMoreButton342)
            self.label.numberOfLines = AssessmentFootnoteCollectionViewCell.limitedNumberOfLines
            self.label.attributedText = attributedText
        } else {
            self.label.attributedTruncationToken = nil
            self.label.numberOfLines = 0
            let combo = NSMutableAttributedString()
            if let lessString = attributedText?.addTruncationToken(text: " " + CommonStrings.FootnoteLessButton343), let attributedText = attributedText {
                combo.append(attributedText)
                combo.append(lessString)
            }
            self.label.attributedText = combo
        }
    }

    public class func height(with attributedTextDetail: AttributedTextDetail, constrainedTo width: CGFloat, isFootnoteExpanded: Bool) -> CGSize {
        
        let combo = NSMutableAttributedString()
        let attributedText = attributedTextDetail.attributedString
        combo.append(attributedText!)
        
        if let lessString = attributedText?.addTruncationToken(text: " " + CommonStrings.FootnoteLessButton343) {
            combo.append(lessString)
        }
        let topInset = AssessmentFootnoteCollectionViewCell.topInset
        let sideInset = AssessmentFootnoteCollectionViewCell.sideInset
        let sizeLimit = CGSize(width: width - sideInset * 2, height: CGFloat.greatestFiniteMagnitude)
        let numberOfLines = isFootnoteExpanded ? 0 : AssessmentFootnoteCollectionViewCell.limitedNumberOfLines
        let size = TTTAttributedLabel.sizeThatFitsAttributedString(combo, withConstraints: sizeLimit, limitedToNumberOfLines: UInt(numberOfLines))
        let height = size.height + topInset
        return CGSize(width: width, height: ceil(height))
    }

}

fileprivate extension NSAttributedString {

    func addTruncationToken(text: String) -> NSAttributedString {
        let tokenString = text
        let attributedTruncationToken = NSMutableAttributedString(string: tokenString)
        let attributes = self.attributes(at: 0, effectiveRange: nil)

        // add current paragraphy style if present
        if let paragraphStyle = attributes[NSParagraphStyleAttributeName] as? NSMutableParagraphStyle {
            attributedTruncationToken.addAttributes([NSParagraphStyleAttributeName: paragraphStyle], range: attributedTruncationToken.string.nsRange(of: attributedTruncationToken.string))
        }
        // reapply foreground color to whole string
        if let foregroundColor = attributes[NSForegroundColorAttributeName] as? UIColor {
            attributedTruncationToken.addAttributes([NSForegroundColorAttributeName: foregroundColor], range: attributedTruncationToken.string.nsRange(of: attributedTruncationToken.string))
        }
        // set custom text color to token string
        attributedTruncationToken.addAttributes([NSForegroundColorAttributeName: UIColor.currentGlobalTintColor()], range: attributedTruncationToken.string.nsRange(of: tokenString))

        return attributedTruncationToken
    }

}

extension VHRQuestionnaireSection {

    func image() -> UIImage? {
        let type = QuestionnaireSectionsRef(rawValue: self.typeKey) ?? .Unknown
        switch type {
        case .AgeGender:
            return UIImage.templateImage(asset: .vhrSectionOverall)
        case .AlcoholIntake:
            return UIImage.templateImage(asset: .vhrSectionAlcoholIntake)
        case .EatingHabits:
            return UIImage.templateImage(asset: .vhrSectionEatingHabits)
        case .ExSmoker:
            return UIImage.templateImage(asset: .vhrSectionSmoking)
        case .FamilyMedicalHistory:
            return UIImage.templateImage(asset: .vhrSectionFamilyMedical)
        case .KeyHba1c:
            return UIImage.templateImage(asset: .vhrSectionMedicalHistory)
        case .KeyMeasurements:
            return UIImage.templateImage(asset: .vhrSectionKeyMeasures)
        case .KeyMeasurementsBP:
            return UIImage.templateImage(asset: .vhrSectionMedicalHistory)
        case .KeyMeasurementsGluc:
            return UIImage.templateImage(asset: .vhrSectionMedicalHistory)
        case .KeyTotalCholesterol:
            return UIImage.templateImage(asset: .vhrSectionMedicalHistory)
        case .MedicalHistory:
            return UIImage.templateImage(asset: .vhrSectionMedicalHistory)
        case .OverallHealth:
            return UIImage.templateImage(asset: .vhrSectionOverall)
        case .PhysicalActivityLev:
            return UIImage.templateImage(asset: .vhrSectionPhysicalActivity)
        case .SleepingPatterns:
            return UIImage.templateImage(asset: .vhrSectionSleepingPatterns)
        case .Smoker:
            return UIImage.templateImage(asset: .vhrSectionSmoking)
        case .Smoking:
            return UIImage.templateImage(asset: .vhrSectionSmoking)
        case .YourDetails:
            return UIImage.templateImage(asset: .vhrSectionOverall)
        case .YourWellbeing:
            return UIImage.templateImage(asset: .vhrSectionWellbeing)
        default:
            return nil
        }
    }

}

protocol FootnoteToggleable: class {

    var isFootnoteExpanded: Bool { get set }

    var indexForFootnoteCell: Int? { get }

    func toggleFootnote()

}

extension FootnoteToggleable where Self: IGListSectionController {

    func toggleFootnote() {
        isFootnoteExpanded = !isFootnoteExpanded

        if let validIndex = indexForFootnoteCell {
            self.collectionContext?.performBatch(animated: true, updates: {
                self.collectionContext?.reload(in: self, at: IndexSet(integer: validIndex))
            }, completion: { completed in
                //
            })
        }
    }

}
