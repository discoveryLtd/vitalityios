//
//  MWBAssessmentLandingViewModel.swift
//  VitalityActive
//
//  Created by Harry C. Lingad on 2/6/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import VitalityKit
import RealmSwift

public struct MWBAssessmentQuestionnaireCellData {
    var title: String
    var buttonText: String
    var timeToComplete: String
    var completed: Bool
    var completeIcon: UIImage?
    var typeKey: Int
}

public struct MWBAssessmentHeaderCellData {
    var title: String
    var description: String
    var totalSections: Int
    var completeSections: Int
    var completedImage: UIImage?
}

public protocol MWBAssessmentLandingViewModel: class {
    
    var mwbRealm: Realm { get }
    var mwbQuestionnaireData: [MWBAssessmentQuestionnaireCellData] { get }
    var mwbHeaderData: MWBAssessmentHeaderCellData? { get }
    var integerFormatter: NumberFormatter { get }
    var questionnairesFooterText: String? { get }
    
    // MARK: Functions
    
    func shouldShowOnboarding() -> Bool
    
    func configureQuestionnaires()
    
    func configureHeader()
    
    func getTotalNumberOfQuestionnaires() -> Int
    
    func getHeaderDescriptionSubtext() -> String?
    
    func getTotalNumberOfCompletedQuestionnaires() -> Int
    
    func getQuestionnairesTotalPotentialPoints() -> Int
    
    func getQuestionnairesTotalEarnedPoints() -> Int

    func getQuestionnaireAt(index: Int) -> MWBQuestionnaire?
    
    func getQuestionnaire(using selectedQuestionnaireTypeKey: Int) -> MWBQuestionnaire?
    
    func fetchQuestionnaires(forceUpdate: Bool, completion: @escaping (Error?, Bool?) -> Void)
    
}

