import IGListKit
import VIAUIKit
import VIAUtilities
import VitalityKit
import RealmSwift
import VitalityKit

public final class AssessmentToggleQuestionSectionController: IGListSectionController, IGListSectionType, FootnoteToggleable {
    
    private static let trueString = "True" // don't localise
    private static let falseString = "False" // don't localise
    
    // TODO: No current implementation of question 'typeKey' reference data.
    private static let noApplicableDiseaseTypeKey = 143
    
    // MARK: Properties
    
    var realm: Realm
    
    var questionDetail: AssessmentQuestionPresentationDetail! {
        didSet {
            updateData()
        }
    }
    
    var isToggled: Bool = false {
        didSet {
            updateData()
        }
    }
    
    var capturedResult: VHRCapturedResult? {
        return self.questionDetail.rawQuestion(in: self.realm)?.capturedResult()
    }
    
    // MARK: FootnoteToggleable
    
    var isFootnoteExpanded: Bool = false
    
    var indexForFootnoteCell: Int? {
        return shouldAddNote() ? data.count - 1 : nil
    }
    
    // MARK: Lifecycle
    
    public required init(realm: Realm) {
        self.realm = realm
    }
    
    // MARK: Data
    
    var data: [AttributedTextDetail] = []
    
    func updateData() {
        // question
        data = [self.questionDetail.textAndDescriptionDetail()]
        
        // footnote
        if self.questionDetail.textNote != nil {
            data.append(self.questionDetail.noteDetail())
        }
    }
    
    func shouldAddNote() -> Bool {
        return self.questionDetail.textNote != nil
    }
    
    // MARK: IGListSectionType
    
    public func numberOfItems() -> Int {
        return data.count
    }
    
    public func didUpdate(to object: Any) {
        self.questionDetail = object as! AssessmentQuestionPresentationDetail
        generateDefaultFalseAnswerIfNeeded()
        self.isToggled = self.capturedResult?.answer == AssessmentToggleQuestionSectionController.trueString
    }
    
    public func cellForItem(at index: Int) -> UICollectionViewCell {
        let item = data[index]
        if item.type == .normal {
            return attributedTextCell(for: item, at: index)
        } else if item.type == .footnote {
            return footnoteAttributedTextCell(for: item, at: index)
        }
        return collectionContext!.dequeueReusableCell(of: UICollectionViewCell.self, for: self, at: index)
    }
    
    public func sizeForItem(at index: Int) -> CGSize {
        let width = collectionContext!.containerSize.width
        let item = data[index]
        
        if item.type == .normal {
            return AssessmentToggleQuestionCollectionViewCell.height(with: item, constrainedTo: width)
        } else if item.type == .footnote {
            return AssessmentFootnoteCollectionViewCell.height(with: item, constrainedTo: width, isFootnoteExpanded: self.isFootnoteExpanded)
        }
        return CGSize.zero
    }
    
    public func didSelectItem(at index: Int) {
        // tap on footnote
        guard index == 0 else {
            toggleFootnote()
            return
        }
        
        // Only perform this when section is 'MedicalHistory'
        toggleNADSelection()
        
        isToggled = !isToggled
        let answerValue = isToggled ? AssessmentToggleQuestionSectionController.trueString : AssessmentToggleQuestionSectionController.falseString
    
        updateCapturedResult(answer: answerValue)
    }
    
    // MARK: Capture
    
    func generateDefaultFalseAnswerIfNeeded() {
        if self.capturedResult == nil {
            updateCapturedResult(answer: AssessmentToggleQuestionSectionController.falseString)
        }
    }
    
    func updateCapturedResult(answer: String) {
        guard let type = self.questionDetail.rawQuestion(in: realm)?.questionType else { return }
        
        if let controller = self.viewController as? AssessmentQuestionnaireSectionViewController {
            controller.answeringDelegate?.persist(answer,
                                                  unit: nil,
                                                  for: self.questionDetail.rawQuestionTypeKey,
                                                  of: type,
                                                  allowsMultipleAnswers: false,
                                                  answerIsValid: true)
        }
    }
    
    // MARK: Cells
    
    func attributedTextCell(for textDetail: AttributedTextDetail, at index: Int) -> UICollectionViewCell {
        let cell = collectionContext!.dequeueReusableCell(of: AssessmentToggleQuestionCollectionViewCell.self, for: self, at: index) as! AssessmentToggleQuestionCollectionViewCell
        cell.label.attributedText = textDetail.attributedString
        cell.addTopBorder = true
        cell.addBottomBorder = true
        cell.isToggled = isToggled
        return cell
    }
    
    func footnoteAttributedTextCell(for textDetail: AttributedTextDetail, at index: Int) -> UICollectionViewCell {
        let cell = collectionContext!.dequeueReusableCell(of: AssessmentFootnoteCollectionViewCell.self, for: self, at: index) as! AssessmentFootnoteCollectionViewCell
        cell.setLabelAttributedText(textDetail.attributedString, isFootnoteExpanded: self.isFootnoteExpanded)
        return cell
    }
    
    // MARK: Custom
    
    /*
     * 'No applicable disease' and other disease items must not be simultaneously selected.
     * reference ticket: https://jira.vitalityservicing.com/browse/VA-26463
     */
    func toggleNADSelection() {
        if let controller = self.viewController as? AssessmentQuestionnaireSectionViewController, controller.questionnaireSectionTypeKey == QuestionnaireSectionsRef.MedicalHistory.rawValue {
            let nadTypeKey = AssessmentToggleQuestionSectionController.noApplicableDiseaseTypeKey
            if self.questionDetail.rawQuestionTypeKey == nadTypeKey {
                let questions = DataProvider.newVHRRealm().vhrCapturedResults(for: QuestionnaireSectionsRef.MedicalHistory.rawValue)
                for question in questions {
                    if question.questionTypeKey != nadTypeKey {
                        controller.answeringDelegate?.delete("", for: question.questionTypeKey, allowsMultipleAnswers: false)
                    }
                }
            } else {
                controller.answeringDelegate?.delete("", for: nadTypeKey, allowsMultipleAnswers: false)
            }
        }
    }
    
}

public final class AssessmentToggleQuestionCollectionViewCell: UICollectionViewCell, Nibloadable {
    
    public static let inset: CGFloat = 15.0
    
    private static let minimumHeight: CGFloat = 26.0
    
    public var isToggled = false {
        didSet {
            toggleImageViewSelected()
        }
    }
    
    public var addTopBorder = false
    
    public var addBottomBorder = false
    
    // MARK: Views
    
    public lazy var label: UILabel = {
        let label = UILabel(frame: .zero)
        label.font = UIFont.title2Font()
        label.textColor = UIColor.night()
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    public lazy var imageView: UIImageView = {
        let view = UIImageView(frame: .zero)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.contentMode = .top
        view.image = UIImage.templateImage(asset: .vhrCheckMarkInactive)
        view.tintColor = UIColor.mediumGrey()
        return view
    }()
    
    // MARK: Init
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupSubviews()
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        
        contentView.layer.removeAllBorderLayers()
        if addTopBorder {
            contentView.layer.addBorder(edge: .top)
        }
        if addBottomBorder {
            contentView.layer.addBorder(edge: .bottom)
        }
    }
    
    private func setupSubviews() {
        contentView.backgroundColor = .day()
        
        let stackView = UIStackView()
        contentView.addSubview(stackView)
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .horizontal
        stackView.distribution = .fill
        stackView.spacing = 10
        stackView.addArrangedSubview(self.label)
        stackView.addArrangedSubview(self.imageView)
        
        stackView.snp.makeConstraints { (make) in
            make.top.bottom.equalToSuperview().inset(AssessmentToggleQuestionCollectionViewCell.inset)
            make.left.right.equalToSuperview().inset(AssessmentToggleQuestionCollectionViewCell.inset)
            make.height.greaterThanOrEqualTo(imageView.snp.height)
        }
        
        imageView.snp.makeConstraints { (make) in
            make.width.equalTo(AssessmentToggleQuestionCollectionViewCell.minimumHeight).priority(1000)
        }
    }
    
    override public func preferredLayoutAttributesFitting(_ layoutAttributes: UICollectionViewLayoutAttributes) -> UICollectionViewLayoutAttributes {
        return self.defaultContentViewLayoutSizeFitting(layoutAttributes)
    }
    
    // MARK: Height calculation
    
    public class func height(with attributedTextDetail: AttributedTextDetail, constrainedTo width: CGFloat) -> CGSize {
        let inset = AssessmentToggleQuestionCollectionViewCell.inset
        let insets = UIEdgeInsets(top: 0, left: inset, bottom: 0, right: inset)
        let size = TextSize.size(attributedTextDetail.attributedString, width: width, insets: insets)
        let height = max(size.height, AssessmentToggleQuestionCollectionViewCell.minimumHeight) + inset * 2
        return CGSize(width: width, height: ceil(height))
    }
    
    // MARK: Helpers
    
    public func toggleImageViewSelected() {
        if isToggled {
            imageView.image = UIImage.templateImage(asset: .vhrCheckMarkActive)
            imageView.tintColor = UIColor.currentGlobalTintColor()
        } else {
            imageView.image = UIImage.templateImage(asset: .vhrCheckMarkInactive)
            imageView.tintColor = UIColor.mediumGrey()
        }
    }
    
}
