//
//  MWBAssessmentQuestionnaireHeaderFooterSectionController.swift
//  VitalityActive
//
//  Created by Michelle R. Oratil on 14/02/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import IGListKit
import VIAUIKit
import VIAUtilities

public final class MWBAssessmentQuestionnaireHeaderFooterSectionController: IGListSectionController, IGListSectionType {
    
    // MARK: Properties
    
    var headerFooter: MWBAssessmentQuestionnaireSectionHeaderFooterDetail!
    
    // MARK: IGListSectionType
    
    public func numberOfItems() -> Int {
        return 1
    }
    
    public func didUpdate(to object: Any) {
        self.headerFooter = object as! MWBAssessmentQuestionnaireSectionHeaderFooterDetail
    }
    
    public func cellForItem(at index: Int) -> UICollectionViewCell {
        let cell = collectionContext!.dequeueReusableCell(of: MWBAssessmentTextCollectionViewCell.self, for: self, at: index) as! MWBAssessmentTextCollectionViewCell
        cell.label.attributedText = headerFooter.textAndDescriptionDetail().attributedString
        cell.addTopBorder = true
        cell.addBottomBorder = true
        return cell
    }
    
    public func sizeForItem(at index: Int) -> CGSize {
        let width = collectionContext!.containerSize.width
        return MWBAssessmentTextCollectionViewCell.height(with: headerFooter.textAndDescriptionDetail(), constrainedTo: width)
    }
    
    public func didSelectItem(at index: Int) { }
    
}
