//
//  AssessmentRequiredPromptController.swift
//  VitalityActive
//
//  Created by Genie Mae Lorena Edera on 4/16/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import IGListKit
import VIAUIKit
import VIAUtilities
import VitalityKit
import VIACommon

public final class AssessmentRequiredPromptController: IGListSectionController, IGListSectionType {
    
    // MARK: Properties
    
    //var headerFooter: AssessmentQuestionnaireSectionHeaderFooterDetail!
    
    // MARK: IGListSectionType
    
    public func numberOfItems() -> Int {
        return 1
    }
    
    public func didUpdate(to object: Any) {
        //self.headerFooter = object as! AssessmentQuestionnaireSectionHeaderFooterDetail
    }
    
    public func cellForItem(at index: Int) -> UICollectionViewCell {
        return headerAttributedTextCell(for: AttributedTextDetail(attributedString: self.requiredErr()), at: index)
    }
    
    public func requiredErr() -> NSAttributedString {
        let requiredMsg = NSMutableAttributedString(string: "")
        let icon = NSTextAttachment()
        icon.image = VIAAssessmentsAsset.requiredEnabled.image
        icon.bounds = CGRect(x: 0, y: -3, width: 15, height: 15)
        requiredMsg.append(NSAttributedString(attachment: icon))
        let attr = [NSFontAttributeName: UIFont.systemFont(ofSize: 15)]
        requiredMsg.append(NSAttributedString(string: " \(CommonStrings.ErrorRequired289)", attributes: attr))
        return requiredMsg
    }
    
    func headerAttributedTextCell(for textDetail: AttributedTextDetail, at index: Int) -> UICollectionViewCell {
        let cell = collectionContext!.dequeueReusableCell(of: AssessmentHeaderCollectionViewCell.self, for: self, at: index) as! AssessmentHeaderCollectionViewCell
        cell.label.attributedText = textDetail.attributedString
        return cell
    }
    
    public func sizeForItem(at index: Int) -> CGSize {
        let width = collectionContext!.containerSize.width
        if (/**self.isYesSelected == nil && */VIAApplicableFeatures.default.showRequiredPrompt!){
            return AssessmentHeaderCollectionViewCell.height(with: AttributedTextDetail(attributedString: self.requiredErr()), constrainedTo: width)
        }else{
            return CGSize.zero
        }
    }
    
    public func didSelectItem(at index: Int) { }
    
}


public final class AssessmentRequiredPrompt: NSObject, IGListDiffable {
    public var sectionKey: Int?
    public var uuidString = UUID().uuidString
    
    public init(key: Int? = nil) {
        self.sectionKey = key
    }
    
    public func diffIdentifier() -> NSObjectProtocol {
        return self.uuidString as NSObjectProtocol
    }
    
    public func isEqual(toDiffableObject object: IGListDiffable?) -> Bool {
        guard self !== object else { return true }
        guard let object = object as? AssessmentRequiredPrompt else { return false }
        return uuidString == object.uuidString
    }
}
