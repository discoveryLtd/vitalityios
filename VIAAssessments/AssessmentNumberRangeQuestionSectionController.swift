import IGListKit
import VIAUIKit
import VIAUtilities
import VitalityKit
import VitalityKit
import RealmSwift
import VIACommon


public protocol AssessmentCaptureResultsDelegate: class {
    func show(menu: UIAlertController)
    func didSelectUnitOfMeasure(_ unitOfMeasure: UnitOfMeasureRef)
}

public final class AssessmentNumberRangeQuestionSectionController: IGListSectionController, IGListSectionType, AssessmentCaptureResultsDelegate, FootnoteToggleable {
    
    private enum QuestionTypeKeyRef: Int, EnumCollection{
        case AvgHrsMeetingsInADay = 116
        case AvgHrsTVInADay = 117
        case AvgHrsCompInADay = 118
    }
    private let MAXIMUM_HRS_PER_DAY = 24
    
    // MARK: Properties
    
    var realm: Realm
    
    var questionDetail: AssessmentQuestionPresentationDetail! {
        didSet {
            if let validAnswers = self.questionDetail.validValuesPresentationDetail {
                let validValues = (validAnswers.map({ $0.rawValidValue }))
                self.metric = AssessmentMetric(validValues: validValues)
                //for values in validValues {
                //    self.selectedUnitOfMeasureType = UnitOfMeasureRef(rawValue: values.unitOfMeasure.rawValue)!
                //}
            } else if let unitsOfMeasure = self.questionDetail.rawQuestion(in: realm)?.unitsOfMeasure {
//                debugPrint("No valid values to validate against, using question's units of measure")
                let values = unitsOfMeasure.flatMap({ $0.unitOfMeasureRef() })
//                debugPrint("Setting first unit of measure as selected type")
                if let firstUnitOfMeasure = values.first {
                    self.selectedUnitOfMeasureType = firstUnitOfMeasure
                }
                self.metric = AssessmentMetric(unitsOfMeasure: Array(values))
            } else {
//                debugPrint("No valid values to validate against, nor units of measure on the question")
            }
        }
    }
    
    var decimalFormatter = Localization.decimalFormatter
    
    var oneDecimalFormatter = Localization.oneDecimalFormatter
    
    var serviceFormatter = NumberFormatter.serviceFormatter()
    
    var metric: AssessmentMetric?
    
    var selectedUnit: Unit?
    
    var input: String?
    
    var isValid: Bool = false
    
    var selectedUnitOfMeasureType: UnitOfMeasureRef = UnitOfMeasureRef.Unknown
    
    // MARK: FootnoteToggleable
    
    var isFootnoteExpanded: Bool = false
    
    var indexForFootnoteCell: Int? {
        return self.footnoteDetail != nil ? 2 : nil
    }
    
    fileprivate let DUAL_INPUT: CGFloat = 96
    fileprivate let SINGLE_INPUT: CGFloat = 48
    fileprivate var INPUT_HEIGHT: CGFloat = 48
    
    // MARK: Lifecycle
    
    public required init(realm: Realm) {
        self.realm = realm
    }
    
    // MARK: Data
    
    var capturedResult: VHRCapturedResult? {
        return self.questionDetail.rawQuestion(in: self.realm)?.capturedResult()
    }
    
    var footnoteDetail: AttributedTextDetail? {
        if self.questionDetail.textNote != nil {
            return self.questionDetail.noteDetail()
        }
        return nil
    }
    
    // MARK: IGListSectionType
    
    public func numberOfItems() -> Int {
        var count = 2
        if self.footnoteDetail != nil {
            count = count + 1
        }
        return count
    }
    
    
    public func didUpdate(to object: Any) {
        self.questionDetail = object as! AssessmentQuestionPresentationDetail
    }
    
    public func cellForItem(at index: Int) -> UICollectionViewCell {
        if index == 0 {
            return attributedTextCell(for: self.questionDetail.textAndDescriptionDetail(), at: index)
        } else if index == 1 {
            return inputCell(at: index)
        } else if index == 2, let noteDetail = self.footnoteDetail {
            return footnoteAttributedTextCell(for: noteDetail, at: index)
        }
        
        return collectionContext!.dequeueReusableCell(of: UICollectionViewCell.self, for: self, at: index)
    }
    
    public func sizeForItem(at index: Int) -> CGSize {
        let width = collectionContext!.containerSize.width
        
        if index == 0 {
            return AssessmentTextCollectionViewCell.height(with: self.questionDetail.textAndDescriptionDetail(), constrainedTo: width)
        } else if index == 1 {
            var uom: UnitOfMeasureRef = self.selectedUnitOfMeasureType
            if uom == .Unknown{
                if let unit = self.capturedResult?.unitOfMeasure{
                    uom = unit
                }
            }
            if uom == .FootInch || uom == .StonePound{
                INPUT_HEIGHT = DUAL_INPUT
            }
            return CGSize(width: width, height: INPUT_HEIGHT)
        } else if index == 2, let noteDetail = self.footnoteDetail {
            return AssessmentFootnoteCollectionViewCell.height(with: noteDetail, constrainedTo: width, isFootnoteExpanded: self.isFootnoteExpanded)
        }
        
        return CGSize.zero
    }
    
    public func didSelectItem(at index: Int) {
        if index == 2 {
            toggleFootnote()
        }
    }
    
    // MARK: Cells
    
    func attributedTextCell(for textDetail: AttributedTextDetail, at index: Int) -> UICollectionViewCell {
        let cell = collectionContext!.dequeueReusableCell(of: AssessmentTextCollectionViewCell.self, for: self, at: index) as! AssessmentTextCollectionViewCell
        cell.label.attributedText = textDetail.attributedString
        cell.addTopBorder = true
        cell.addBottomBorder = true
        return cell
    }
    
    func footnoteAttributedTextCell(for textDetail: AttributedTextDetail, at index: Int) -> UICollectionViewCell {
        let cell = collectionContext!.dequeueReusableCell(of: AssessmentFootnoteCollectionViewCell.self, for: self, at: index) as! AssessmentFootnoteCollectionViewCell
        cell.setLabelAttributedText(textDetail.attributedString, isFootnoteExpanded: self.isFootnoteExpanded)
        return cell
    }
    
    func forceQuestionUpdate(qDetail: VHRCapturedResult?, isValid: Bool){
        if let questionDetail = qDetail{
            print("===> Update: \(questionDetail)")
            if let controller = self.viewController as? AssessmentQuestionnaireSectionViewController {
                controller.answeringDelegate?.persist(questionDetail.answer ?? "",
                                                      unit: questionDetail.unitOfMeasure,
                                                      for: questionDetail.questionTypeKey,
                                                      of: questionDetail.type,
                                                      allowsMultipleAnswers: false,
                                                      answerIsValid: isValid)
            }
        }
    }
    
    func isAMemberOfGroupQuestion(rawQuestionTypeKey: Int) -> Bool {
        if (rawQuestionTypeKey == QuestionTypeKeyRef.AvgHrsMeetingsInADay.rawValue ||  rawQuestionTypeKey == QuestionTypeKeyRef.AvgHrsTVInADay.rawValue || rawQuestionTypeKey == QuestionTypeKeyRef.AvgHrsCompInADay.rawValue) {
            return true
        }
        return false
    }
    
    func executeGroupValidation(cell: AssessmentNumberRangeAnswerCollectionViewCell, shouldDoUpdate:Bool){
        if let controller = self.viewController as? AssessmentQuestionnaireSectionViewController {
            let computerQuestion = controller.answeringDelegate?.getAnswer(for: QuestionTypeKeyRef.AvgHrsMeetingsInADay.rawValue)
            let meetingQuestion = controller.answeringDelegate?.getAnswer(for: QuestionTypeKeyRef.AvgHrsTVInADay.rawValue)
            let televisionQuestion = controller.answeringDelegate?.getAnswer(for: QuestionTypeKeyRef.AvgHrsCompInADay.rawValue)
            
            let a = Int(computerQuestion?.answer ?? "0") ?? 0
            let b = Int(meetingQuestion?.answer ?? "0") ?? 0
            let c = Int(televisionQuestion?.answer ?? "0") ?? 0
            
            print("===> AnsDexQT:\(self.questionDetail.rawQuestionTypeKey) \n A: \(a)\n B:\(b) \n C:\(c)")
            
            if shouldDoUpdate {
                /*
                 The validity of one should be the validity of All. (Group Validation)
                */
                self.forceQuestionUpdate(qDetail: computerQuestion, isValid: (a + b + c) <= 24 )
                self.forceQuestionUpdate(qDetail: meetingQuestion, isValid: (a + b + c) <= 24 )
                self.forceQuestionUpdate(qDetail: televisionQuestion, isValid: (a + b + c) <= 24 )
            }
                
            cell.updateInputValid((a + b + c) <= 24 ? "" : CommonStrings.Vhr.AssessmentSittingProblemPrompt2412, for: cell.validationErrorLabel)
            }
    }
    func inputCell(at index: Int) -> UICollectionViewCell {
        let cell = collectionContext!.dequeueReusableCell(withNibName: AssessmentNumberRangeAnswerCollectionViewCell.defaultReuseIdentifier, bundle: AssessmentNumberRangeAnswerCollectionViewCell.bundle(), for: self, at: index) as! AssessmentNumberRangeAnswerCollectionViewCell
        cell.captureResultsDelegate = self
        cell.unitButton.isHidden = true
        cell.questionRawTypeKey = self.questionDetail.rawQuestionTypeKey
        
        /* Check if Decimal Input should be disabled */
        if let shouldDisableDecimalInput = VIAApplicableFeatures.default.vhrDisableKeyboardDecimalInput,
            shouldDisableDecimalInput {
            self.disableKeyboardDecimalInput(cell: cell)
        }
        
        // textFieldTextDidChange
        cell.textFieldTextDidChange = { [unowned self] textField, unit, min, max in
            var uomType = self.selectedUnitOfMeasureType
            if let metric = self.metric, metric.validate(textField.text, unit, &uomType, &min, &max) {
                cell.updateInputValid(true, nil, nil, cell.validationErrorLabel)
            }
        }
        
        // textFieldDidBeginEditing
        cell.textFieldDidBeginEditing = { textField, unit, min, max in
            // Reload IQKeyboard manager toolbar on focus.
            textField.reloadKeyboardToolbar()
            
            // Remove grouping separator on textfield focus.
            textField.text = textField.text?.removeNumberGroupingSeparator()
        }
        
        // textFieldDidEndEditing
        cell.textFieldDidEndEditing = { [unowned self] textField, unit, min, max in
            if(textField.text?.isEmpty)! {
                self.updateValues(isValid: false,
                                  uomType: self.selectedUnitOfMeasureType, unit: unit,
                                  inputText: self.getInput(cell: cell))
                self.updateCapturedResult()
                return
            }
            var uomType = self.selectedUnitOfMeasureType
            if let isValid = self.metric?.validate(textField.text, unit, &uomType, &min, &max){
                if self.selectedUnitOfMeasureType == .FootInch || self.selectedUnitOfMeasureType == .StonePound{
                    if let isValidSecondField = self.metric?.validate(cell.secondInputField.text, unit, &uomType, &min, &max){
                        
                        self.updateValues(isValid: isValid && isValidSecondField && !(cell.secondInputField.text?.isEmpty ?? true),
                                          uomType: self.selectedUnitOfMeasureType, unit: unit,
                                          inputText: self.getInput(cell: cell))
                    }
                }else{
                    self.updateValues(isValid: isValid, uomType: self.selectedUnitOfMeasureType, unit: unit, inputText: textField.text)
                }
                
                // check if the unit of measurement is Systolicmillimeterofmercury and MillimeterOfMercury for blood pressure
                if (self.selectedUnitOfMeasureType == .Systolicmillimeterofmercury) || (self.selectedUnitOfMeasureType == .MillimeterOfMercury){
                    if let controller = self.viewController as? AssessmentQuestionnaireSectionViewController {
                        let result = controller.answeringDelegate?.getValue(for: self.selectedUnitOfMeasureType,
                                                                            andNot: self.questionDetail.rawQuestionTypeKey)
                        var systolic = 0.0
                        var diastolic = 0.0
                        
                        if self.questionDetail.rawQuestionTypeKey == 73
                        {
                            systolic = Double(result?.answer ?? "0") ?? 0
                            diastolic = Double(textField.text ?? "0") ?? 0
                        }else{
                            diastolic = Double(result?.answer ?? "0") ?? 0
                            systolic = Double(textField.text ?? "0") ?? 0
                        }
                        
                        if systolic != 0 && diastolic != 0{
                            if !isValid{
                                cell.updateInputValid(isValid, min, max, cell.validationErrorLabel)
                            }else{
                                if self.questionDetail.rawQuestionTypeKey == 73 {
                                    cell.updateInputValid(systolic > diastolic ? "" : CommonStrings.Vhr.Assessment.BloodPressurePrompt2411, for: cell.validationErrorLabel)
                                } else {
                                    if !isValid{
                                        cell.updateInputValid(false, min, max, cell.validationErrorLabel)
                                    } else {
                                        cell.updateInputValid(true, min, max, cell.validationErrorLabel)
                                    }
                                }
                            }
                        }else{
                            cell.updateInputValid(isValid, min, max, cell.validationErrorLabel)
                        }
                        
                        self.updateValues(isValid: systolic > diastolic,
                                          uomType: self.selectedUnitOfMeasureType, unit: unit,
                                          inputText: textField.text)
                        
                        self.updateCapturedResult()
                    }
                } else if VIAApplicableFeatures.default.shouldExecuteGroupValidation() && self.isAMemberOfGroupQuestion(rawQuestionTypeKey: self.questionDetail.rawQuestionTypeKey) {
                    // The sum of the answers of this questions should not be greater the 24 hours.
                    // Dex
                    self.updateCapturedResult()
                    
                    if !isValid{
                        cell.updateInputValid(isValid, min, max, cell.validationErrorLabel)
                    } else {
                        self.executeGroupValidation(cell: cell, shouldDoUpdate: true)
                    }
                }else{
                    self.updateCapturedResult()
                    cell.updateInputValid(isValid, min, max, cell.validationErrorLabel)
                }
            }
            
        }
        
        // secondTextFieldTextDidChange
        cell.secondTextFieldTextDidChange = { [unowned self] textField, unit, min, max in
            var uomType = self.selectedUnitOfMeasureType
            /**
             * Manually override the UnitOfMeasureTypeRef into Inch for checking in the AssessmentMetric.validate
             * This is to manually override the Upper and Lower Limit for Pound (0-11)
             */
            if self.selectedUnitOfMeasureType == .FootInch{
                uomType = .Inch
            }
            
            /**
             * Manually override the UnitOfMeasureTypeRef into Pound for checking in the AssessmentMetric.validate
             * This is to manually override the Upper and Lower Limit for Pound (0-13)
             */
            if self.selectedUnitOfMeasureType == .StonePound{
                uomType = .Pound
            }
            if let metric = self.metric, metric.validate(textField.text, unit, &uomType, &min, &max) {
                cell.updateInputValid(true, nil, nil, cell.secondValidationErrorLabel)
            }
        }
        
        // secondTextFieldDidEndEditing
        cell.secondTextFieldDidEndEditing = { [unowned self] textField, unit, min, max in
            if(textField.text?.isEmpty)! {
                self.updateValues(isValid: false,
                                  uomType: self.selectedUnitOfMeasureType, unit: unit,
                                  inputText: self.getInput(cell: cell))
                self.updateCapturedResult()
                return
            }
            var uomType = self.selectedUnitOfMeasureType
            /**
             * Manually override the UnitOfMeasureTypeRef into Inch for checking in the AssessmentMetric.validate
             * This is to manually override the Upper and Lower Limit for Pound (0-11)
             */
            if self.selectedUnitOfMeasureType == .FootInch{
                uomType = .Inch
            }
            
            /**
             * Manually override the UnitOfMeasureTypeRef into Pound for checking in the AssessmentMetric.validate
             * This is to manually override the Upper and Lower Limit for Pound (0-13)
             */
            if self.selectedUnitOfMeasureType == .StonePound{
                uomType = .Pound
            }
            if let isValid = self.metric?.validate(textField.text, unit, &uomType, &min, &max) {
                if self.selectedUnitOfMeasureType == .FootInch || self.selectedUnitOfMeasureType == .StonePound{
                    if let isValidFirstField = self.metric?.validate(cell.inputTextField.text, unit, &uomType, &min, &max){
                        
                        self.updateValues(isValid: isValid && isValidFirstField && !(cell.inputTextField.text?.isEmpty ?? true),
                                          uomType: self.selectedUnitOfMeasureType, unit: unit,
                                          inputText: self.getInput(cell: cell))
                    }
                }else{
                    self.updateValues(isValid: isValid, uomType: self.selectedUnitOfMeasureType, unit: unit, inputText: textField.text)
                }
                self.updateCapturedResult()
                cell.updateInputValid(isValid, min, max, cell.secondValidationErrorLabel)
            }
            
        }
        
        // configure the cell
        var inputText = localFormatted(from: self.capturedResult?.answer, selectedUnit: self.capturedResult?.unitOfMeasure.unit())
        if let uom = self.capturedResult?.unitOfMeasure{
            if uom == .FootInch || uom == .StonePound{
                inputText = self.capturedResult?.answer ?? ""
            }
        }
        
        if .Unknown == self.selectedUnitOfMeasureType{
            //self.selectedUnitOfMeasureType = ((self.capturedResult?.unitOfMeasure ?? self.metric?.rawUnits.first) ?? .Unknown)
            self.selectedUnitOfMeasureType = self.getUnit()
        }
//        print("-->Self.SelectedUnit: \(self.selectedUnitOfMeasureType.unit().symbol) \n Captured: \(self.capturedResult?.unitOfMeasure.unit().symbol) \n First:\(self.metric?.rawUnits.first?.unit().symbol)")
        //Initialize selected unit of measurement type from captureResult value
        cell.configureCaptureResultCell(units: self.metric?.rawUnits ?? [],
                                        inputPlaceholder: CommonStrings.Assessment.GenericInputPlaceholder507,
                                        inputText: inputText,
                                        inputUnitOfMeasure: self.selectedUnitOfMeasureType)
        
        //Collapse second input view if current height is for SINGLE INPUT
        cell.secondUserInputStackView.isHidden = INPUT_HEIGHT == SINGLE_INPUT
        cell.secondInputField.isHidden = cell.secondUserInputStackView.isHidden
        cell.secondValidationErrorLabel.isHidden = cell.secondUserInputStackView.isHidden
        
        // update validation for cell, to be visually correct
        // directly after being displayed
        let unit = self.selectedUnitOfMeasureType.unit()
        var uomType = self.selectedUnitOfMeasureType
        var min: Double?
        var max: Double?
        //        var secondUnitMin: Double?
        //        var secondUnitMax: Double?
        
        var value = inputText
        var values = [String]()
        var uomSecondType = UnitOfMeasureRef.Unknown
        if uomType == .FootInch || uomType == .StonePound{
            if uomType == .FootInch{
                values = cell.getValueFromAnswer(toSearch: inputText ?? "", uom1: "FT", uom2: "INCH")
                value = values[0]
                uomSecondType = .Inch
            }else if uomType == .StonePound{
                values = cell.getValueFromAnswer(toSearch: inputText ?? "", uom1: "ST", uom2: "LB")
                value = values[0]
                uomSecondType = .Pound
            }
            if values.count > 1{
                if let metric = self.metric?.validate(values[1], unit, &uomSecondType, &min, &max) {
                    cell.updateInputValid(metric, min, max, cell.secondValidationErrorLabel)
                }
            }
        }
        
        if let isValid = self.metric?.validate(value, unit, &uomType, &min, &max) {
            // check if the unit of measurement is Systolicmillimeterofmercury and MillimeterOfMercury for blood pressure
            if (self.selectedUnitOfMeasureType == .Systolicmillimeterofmercury) || (self.selectedUnitOfMeasureType == .MillimeterOfMercury){
                if let controller = self.viewController as? AssessmentQuestionnaireSectionViewController {
                    let result = controller.answeringDelegate?.getValue(for: self.selectedUnitOfMeasureType,
                                                                        andNot: self.questionDetail.rawQuestionTypeKey)
                    let systolic    = Double(result?.answer ?? "0") ?? 0
                    let diastolic   = Double(value ?? "0") ?? 0
                    
                    self.updateValues(isValid: systolic > diastolic, uomType: uomType, unit: unit, inputText: inputText)
                    if systolic != 0 && diastolic != 0{ 
                        if !isValid{
                            cell.updateInputValid(isValid, min, max, cell.validationErrorLabel)
                        }else{
                            if self.questionDetail.rawQuestionTypeKey == 73 {
                                cell.updateInputValid(systolic > diastolic ? "" : CommonStrings.Vhr.Assessment.BloodPressurePrompt2411, for: cell.validationErrorLabel)
                            } else {
                                cell.updateInputValid(isValid, min, max, cell.validationErrorLabel)
                            }
                        }
                    }
                    else{
                        cell.updateInputValid(isValid, min, max, cell.validationErrorLabel)
                    }
                }
            } else if VIAApplicableFeatures.default.shouldExecuteGroupValidation() && self.isAMemberOfGroupQuestion(rawQuestionTypeKey: self.questionDetail.rawQuestionTypeKey) {
                
                if !isValid{
                    cell.updateInputValid(isValid, min, max, cell.validationErrorLabel)
                } else {
                    self.executeGroupValidation(cell: cell, shouldDoUpdate: false)
                }
            }else{
                self.updateValues(isValid: isValid, uomType: uomType, unit: unit, inputText: inputText)
                cell.updateInputValid(isValid, min, max, cell.validationErrorLabel)
            }
        }
        
        return cell
    }
    
    private func getUnit() -> UnitOfMeasureRef{
        //Try to unwrap capturedResult
        guard let capturedResult = self.capturedResult, self.capturedResult?.unitOfMeasure != .Unknown
            else {
            //If captureResult is nil, try to unwrap metric
            guard let metric = self.metric else {
                //if metric is nil, return Unknown as fallback
                return .Unknown
            }
            //try to unwrap first item in rawUnits
            guard let firstUnit = metric.rawUnits.first else {
                //if firstUnit is nil, return Uknown as fallback
                return .Unknown
            }
            //else, return first unit
            return firstUnit
        }
        //else, return unitOfMeasure from capturedResult
        return capturedResult.unitOfMeasure
    }
    private func getInput(cell: AssessmentNumberRangeAnswerCollectionViewCell) -> String{
        
        var firstValString  = ""
        if let firstVal     = Int(cell.inputTextField.text ?? ""){
            firstValString = String(firstVal)
        }
        
        var secondValString = ""
        if let secondVal    = Double(cell.secondInputField.text ?? ""){
            secondValString = String(secondVal)
        }
        
        var firstUoM    = ""
        var secondUoM    = ""
        if self.selectedUnitOfMeasureType == .FootInch{
            firstUoM    = "FT"
            secondUoM   = "INCH"
        }else if self.selectedUnitOfMeasureType == .StonePound{
            firstUoM    = "ST"
            secondUoM   = "LB"
        }
        
        return "\(firstValString) \(firstUoM) \(secondValString) \(secondUoM)"
    }
    
    func updateValues(isValid: Bool, uomType: UnitOfMeasureRef, unit: Unit?, inputText: String?) {
        self.selectedUnitOfMeasureType  = uomType
        self.isValid                    = isValid
        self.selectedUnit               = unit
        self.input                      = inputText
    }
    
    // MARK: CaptureResultsDelegate
    
    public func show(menu: UIAlertController) {
        if let controller = self.viewController {
            controller.present(menu, animated: true, completion: nil)
        }
    }
    
    public func didSelectUnitOfMeasure(_ unitOfMeasure: UnitOfMeasureRef) {
        self.selectedUnitOfMeasureType = unitOfMeasure
        deletePreviousCapturedData()
        
        let previousHeight = INPUT_HEIGHT
        //RELOAD Index
        INPUT_HEIGHT = (unitOfMeasure == .FootInch || unitOfMeasure == .StonePound) ? DUAL_INPUT : SINGLE_INPUT
        
        /**
         * Reload collection context only when changing from single input 
         * to dual input and vice versa
         *
         **/
        if previousHeight != INPUT_HEIGHT {
            /**
             * Add a delay of 350ms before reloading the view to allow the keyboard to move out from view
             * and avoid the BSOD (black screen of doom).
             * reference ticket: https://jira.vitalityservicing.com/browse/FC-24470
             **/
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + .milliseconds(350)) {
                self.collectionContext?.reload(self)
            }
        }
    }
    
    // MARK: Persist
    
    public func deletePreviousCapturedData(){
        
        if let data = self.capturedResult{
            try! realm.write {
                realm.delete(data)
            }
        }
        
    }
    func updateCapturedResult() {
        // safety checks
        guard let validInput = self.input else {
            return
        }
        if selectedUnitOfMeasureType == .FootInch || selectedUnitOfMeasureType == .StonePound{
            guard let type = self.questionDetail.rawQuestion(in: realm)?.questionType else {
                return
            }
            
            // warn and persist
            if self.selectedUnitOfMeasureType == .Unknown {
//                debugPrint("Capturing result with Unknown unit of measure. This happens when neither the question nor the valid values have any UOM configured.")
            }
            if let controller = self.viewController as? AssessmentQuestionnaireSectionViewController {
                controller.answeringDelegate?.persist(validInput,
                                                      unit: self.selectedUnitOfMeasureType,
                                                      for: self.questionDetail.rawQuestionTypeKey,
                                                      of: type,
                                                      allowsMultipleAnswers: false,
                                                      answerIsValid: self.isValid)
            }
        }else{
            guard let systemFormattedNumberString = systemFormatted(from: validInput) else {
                deleteCapturedResult(with: validInput)
                return
            }
            
            guard let type = self.questionDetail.rawQuestion(in: realm)?.questionType else {
                return
            }
            
            // warn and persist
            if self.selectedUnitOfMeasureType == .Unknown {
//                debugPrint("Capturing result with Unknown unit of measure. This happens when neither the question nor the valid values have any UOM configured.")
            }
            if let controller = self.viewController as? AssessmentQuestionnaireSectionViewController {
                controller.answeringDelegate?.persist(systemFormattedNumberString,
                                                      unit: self.selectedUnitOfMeasureType,
                                                      for: self.questionDetail.rawQuestionTypeKey,
                                                      of: type,
                                                      allowsMultipleAnswers: false,
                                                      answerIsValid: self.isValid)
            }
        }
//        print("===>UpdateCapturedResult:\(self.capturedResult)")
    }
    
    func deleteCapturedResult(with text: String) {
        if let controller = self.viewController as? AssessmentQuestionnaireSectionViewController {
            controller.answeringDelegate?.delete(text, for: self.questionDetail.rawQuestionTypeKey, allowsMultipleAnswers: false)
        }
    }
    
    private func systemFormatted(from localizedString: String?) -> String? {
        return VIAApplicableFeatures.default.getVHRSystemFormattedNumber(from: localizedString, selectedUnitOfMeasureType: self.selectedUnitOfMeasureType)
    }
    
    private func localFormatted(from serviceString: String?, selectedUnit: Unit?) -> String? {
        guard let validString = serviceString, let selectedUnit = selectedUnit else { return nil }
        guard let number = serviceFormatter.number(from: validString) else { return nil }
        
        let shouldAcceptDecimalInput = VIAApplicableFeatures.default.shouldAcceptDecimalInput(selectedUnit, questionTypeKey: self.questionDetail.rawQuestionTypeKey, healthAttributeType: .Unknown)
        
        /* Default formatter will be Localization.decimalFormatter */
        var decimalFormatter = Localization.decimalFormatter
        /* If localized formatter is not nil, use the formatter instead. */
        if let formatter = VIAApplicableFeatures.default.decimalFormatter(shouldAcceptDecimalInput: shouldAcceptDecimalInput){
            decimalFormatter = formatter
        }
        
        if self.questionDetail.rawQuestionTypeKey == 73 ||  self.questionDetail.rawQuestionTypeKey == 72 {
            if let tenantID = AppSettings.getAppTenant(), tenantID == .SLI {
                return (number.doubleValue.truncatingRemainder(dividingBy: 1)) == 0 ? "\(number.intValue)" : "\(number.doubleValue)"
            } else {
                return "\(number)"
            }
        } else {
            return decimalFormatter.string(from: number)
        }
    }
    
    func disableKeyboardDecimalInput(cell: AssessmentNumberRangeAnswerCollectionViewCell) {
//        /* Alcohol | Wholegrains */
//        switch self.questionDetail.rawQuestionTypeKey {
//        case 97, 101:
//            cell.inputTextField.keyboardType = .numberPad
//        default:
//            break
//        }
        
        // Check for the question's presentation type and set the keyboard type according to the question type.
        if self.questionDetail.presentationType == .NumberRange {
            cell.inputTextField.keyboardType = .numberPad
            cell.secondInputField.keyboardType = .numberPad
        } else {
            cell.inputTextField.keyboardType = .decimalPad
            cell.secondInputField.keyboardType = .decimalPad
        }
    }
}

public struct AssessmentMetric: CaptureMetric {
    
    var validValues = [VHRValidValue]()
    
    public var units: [Unit] = [Unit]()
    
    public var rawUnits: [UnitOfMeasureRef] = [UnitOfMeasureRef]()
    
    public init(validValues: [VHRValidValue]) {
        self.validValues = validValues
        
        if VIAApplicableFeatures.default.vhrRemoveUnknownUOM() {
            self.setRawUnits(self.removeUnknownValues(validValues: self.validValues).flatMap({ $0.unitOfMeasure }))
        } else {
            self.setRawUnits(self.validValues.flatMap({ $0.unitOfMeasure }))
        }
        
        self.units = AssessmentMetric.configureUnits(for: self.rawUnits)
    }
    
    func removeUnknownValues(validValues: [VHRValidValue]) -> [VHRValidValue] {
        var filteredValidValues: [VHRValidValue] = []
        for values in validValues {
            if values.unitOfMeasure != UnitOfMeasureRef(rawValue: UnitOfMeasureRef.Unknown.rawValue) {
                filteredValidValues.append(values)
            }
        }
        return filteredValidValues
    }
    
    public init(unitsOfMeasure: [UnitOfMeasureRef]) {
        self.setRawUnits(unitsOfMeasure)
        self.units = AssessmentMetric.configureUnits(for: self.rawUnits)
    }
    
    private mutating func setRawUnits(_ unitsOfMeasure: [UnitOfMeasureRef]) {
        self.rawUnits = Array(Set(unitsOfMeasure)) // filter duplicates
    }
    
    func validate(_ input: String?,
                  _ unit: Unit?,
                  _ unitOfMeasureType: inout UnitOfMeasureRef,
                  _ min: inout Double?,
                  _ max: inout Double?) -> Bool {
        
        
        // if the input is empty, the user wants to delete the input
        guard let validString = input, !validString.isEmpty else {
            return true
        }
        
        // check that we have a number and not some random garbage
        let localizedFormatter = NumberFormatter.decimalFormatter()
        // note: spanish locale uses "," as the default decimal separator
        // force decimalSeparator to "." when performing internal number formatting
        if let u = unit, AppSettings.forceDotDecimalSeparator(unit: u) {
            localizedFormatter.decimalSeparator = "."
        }
        
        guard let validInput = localizedFormatter.number(from: validString)?.doubleValue else {
            return false
        }
        
        // if no valid values, then anything is valid!
        if self.validValues.count == 0 {
//            debugPrint("No validValues, letting through any value")
            return true
        }
        
        // if we have valid values, filtr and go mad with validation
        let parameters = self.validValues.filter({ $0.unitOfMeasure.unit().symbol == (unit?.symbol ?? "") })
        
        // TODO: Temporarily disabled. What's this for?
        //        guard parameters.count >= 1 else {
        //            debugPrint("Missing parameters in valid values")
        //            return false
        //        }
        let serviceFormatter = NumberFormatter.serviceFormatter()

        var minResult: Bool?
        var maxResult: Bool?
        
        for parameter in parameters {
            // doesn't matter that this sits in the loop,
            // they all have the same UOM based on the filter
            // we applied above.
            //            unitOfMeasureType = parameter.unitOfMeasure
            
            if let validValue = serviceFormatter.number(from: parameter.value)?.doubleValue {
                if parameter.type == .LowerLimit {
                    //Let's check here if we are receiving Inch then manually override the lower limit to 0
                    if unitOfMeasureType == .Inch && unit == UnitOfMeasureRef.FootInch.unit() {
                        min = 0
                        minResult = validInput >= min!
                        
                        //Let's check here if we are receiving Pound then manually override the lower limit to 0
                    } else if unitOfMeasureType == .Pound && unit == UnitOfMeasureRef.StonePound.unit() {
                        min = 0
                        minResult = validInput >= min!
                    } else {
                        // Let's check here if we are receiving mutipart units then manually override the lower limit
                        // to the whole number part of the double 'validValue'
                        if unit == UnitOfMeasureRef.FootInch.unit() || unit == UnitOfMeasureRef.StonePound.unit() {
                            min = floor(validValue)
                        } else {
                            min = validValue
                        }
                        minResult = validInput >= min!
                    }
                } else if parameter.type == .UpperLimit {
                    //Let's check here if we are receiving Inch then manually override the upper limit to 11
                    if unitOfMeasureType == .Inch && unit == UnitOfMeasureRef.FootInch.unit() {
                        max = 11
                        maxResult = validInput <= max!
                        
                        //Let's check here if we are receiving Pound then manually override the upper limit to 13
                    } else if unitOfMeasureType == .Pound && unit == UnitOfMeasureRef.StonePound.unit() {
                        max = 40
                        maxResult = validInput <= max!
                    } else {
                        // Let's check here if we are receiving mutipart units then manually override the upper limit
                        // to the whole number part of the double 'validValue'
                        if unit == UnitOfMeasureRef.FootInch.unit() || unit == UnitOfMeasureRef.StonePound.unit() {
                            max = floor(validValue)
                        } else {
                            max = validValue
                        }
                        maxResult = validInput <= max!
                    }
                } else {
//                    debugPrint("No value to validate against, letting through any value")
                }
            } else {
//                debugPrint("No value to validate against, letting through any value")
            }
        }
        
        // some metric's valid values only have a lower or only an upper
        // limit. we track the validation thus with optionals in so that
        // in the case where there's only 1 limit,
        // we default the other to true
        return (minResult ?? true) && (maxResult ?? true)
    }
}

class AssessmentNumberRangeAnswerCollectionViewCell: UICollectionViewCell, Nibloadable, UITextFieldDelegate {
    
    // MARK: Outlets
    
    @IBOutlet weak var userInputStackView: UIStackView!
    @IBOutlet weak var inputTextField: UITextField!
    @IBOutlet weak var secondInputField: UITextField!
    @IBOutlet weak var validationErrorLabel: UILabel!
    @IBOutlet weak var secondValidationErrorLabel: UILabel!
    @IBOutlet weak var unitsButton: UIButton!
    @IBOutlet weak var unitButton: UIButton!
    @IBOutlet weak var mainStackView: UIStackView!
    @IBOutlet weak var unitsArrowImageView: UIImageView!
    @IBOutlet weak var inputFieldsStackView: UIStackView!
    @IBOutlet weak var secondUserInputStackView: UIStackView!
    
    // MARK: Properties
    
    var min: Double?
    var max: Double?
    var questionRawTypeKey: Int = 0
    
    var selectedUnit: Unit?{
        didSet{
            if let currentSelected = selectedUnit{
                debugPrint(measurementFormatter.string(from: currentSelected))
            }
        }
    }
    
    weak var captureResultsDelegate: AssessmentCaptureResultsDelegate?
    
    lazy var measurementFormatter: MeasurementFormatter = {
        return Localization.decimalShortStyle
    }()
    
    lazy var decimalFormatter: NumberFormatter = {
        let shouldAcceptDecimalInput = VIAApplicableFeatures.default.shouldAcceptDecimalInput(self.selectedUnit!, questionTypeKey: self.questionRawTypeKey, healthAttributeType: .Unknown)
        
        guard let formatter = VIAApplicableFeatures.default.decimalFormatter(shouldAcceptDecimalInput: shouldAcceptDecimalInput) else {
            return Localization.decimalFormatter
        }
        
        return formatter
    }()
    
    //TODO:
    lazy var oneDecimalFormatter: NumberFormatter = {
        return Localization.oneDecimalFormatter
    }()
    
    public var unitsButtonTitle: String? {
        set {
            unitsButton.setTitle(newValue, for: .normal)
        }
        get {
            return unitsButton.titleLabel?.text
        }
    }
    
    let footInchUnit = Localization.decimalShortStyle.string(from: (UnitOfMeasureRef.FootInch.unit()))
    let stonePoundUnit = Localization.decimalShortStyle.string(from: (UnitOfMeasureRef.StonePound.unit()))
    
    // MARK: Closures
    
    public var textFieldTextDidChange: ((_ textField: UITextField, _ unit: Unit?, _ min: inout Double?, _ max: inout Double?) -> Void)?
    
    public var secondTextFieldTextDidChange: ((_ textField: UITextField, _ unit: Unit?, _ min: inout Double?, _ max: inout Double?) -> Void)?
    
    public var textFieldDidBeginEditing: ((_ textField: UITextField, _ unit: Unit?, _ min: inout Double?, _ max: inout Double?) -> Void)?
    
    public var textFieldDidEndEditing: ((_ textField: UITextField, _ unit: Unit?, _ min: inout Double?, _ max: inout Double?) -> Void)?
    
    public var secondTextFieldDidEndEditing: ((_ textField: UITextField, _ unit: Unit?, _ min: inout Double?, _ max: inout Double?) -> Void)?
    
    public var unitsOfMeasure = [UnitOfMeasureRef]()
    
    // MARK: Lifecycle
    
    override public func awakeFromNib() {
        super.awakeFromNib()
        
        setupInputTextField()
        setupCell()
    }
    
    public override func prepareForReuse() {
        super.prepareForReuse()
        
        setupCell()
    }
    
    func setupCell() {
        unitsButton.setTitle(nil, for: .normal)
        unitsButton.titleLabel?.font = .bodyFont()
        unitsButton.titleLabel?.adjustsFontSizeToFitWidth = true
        unitsButton.setTitleColor(.mediumGrey(), for: .normal)
        let gesture = UITapGestureRecognizer(target: self, action: #selector(unitsButtonTapped(_:)))
        unitsArrowImageView.gestureRecognizers?.forEach({ unitsArrowImageView.removeGestureRecognizer($0) })
        unitsArrowImageView.addGestureRecognizer(gesture)
        
        validationErrorLabel.isHidden = true
        validationErrorLabel.font = .footnoteFont()
        validationErrorLabel.textColor = UIColor.cellErrorLabel()
        
        secondValidationErrorLabel.isHidden = true
        secondValidationErrorLabel.font = .footnoteFont()
        secondValidationErrorLabel.textColor = UIColor.cellErrorLabel()
        
        min = nil
        max = nil
        inputTextField.text = nil
        secondInputField.text = nil
        setValidationErrorText(nil, for: validationErrorLabel)
        setValidationErrorText(nil, for: secondValidationErrorLabel)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        mainStackView.superview?.layer.removeAllBorderLayers()
        mainStackView.superview?.layer.addBorder(edge: .bottom)
        userInputStackView.layer.removeAllBorderLayers()
        userInputStackView.layer.addBorder(edge: .bottom)
        unitsArrowImageView.layer.removeAllBorderLayers()
        unitsArrowImageView.layer.addBorder(edge: .right)
    }
    
    // MARK: - Units button setup
    
    @IBAction func unitsButtonTapped(_ sender: Any) {
        resignFirstResponder()
        showUnitsActionSheet()
    }
    
    // MARK:
    
    func stopEditing(textField: UITextField) {
        textField.endEditing(true)
    }
    
    public func configureCaptureResultCell(units: [UnitOfMeasureRef], inputPlaceholder: String?, inputText: String?, inputUnitOfMeasure: UnitOfMeasureRef?) {
        // units picker button
        if units.count > 1 {
            unitsArrowImageView.image = UIImage(asset: .arrowDrill)
            unitsButton.tintColor = .lightGrey()
        } else {
            unitsArrowImageView.image = nil
        }
        
        var stringUoM = ""
        
        // select units
        unitsOfMeasure = units
        
        //If there is no current selected UoM, fallback to the user input UoM
        if stringUoM.isEmpty{
            if let inputUnit = inputUnitOfMeasure?.unit(){
                self.selectedUnit = inputUnit
                stringUoM = self.unitOfMeasureChecker(unit: inputUnit)
            }
        }
        
        //If there is no user input UoM, fallback to the first item in the options
        if stringUoM.isEmpty{
            if let availableUnit = unitsOfMeasure.first?.unit() {
                self.selectedUnit   = availableUnit
                stringUoM = self.unitOfMeasureChecker(unit: availableUnit)
            }
        }
        
        unitsButtonTitle = stringUoM
        
        self.setUnitsOfMeasureViewVisibility(hidden: self.selectedUnit?.symbol == "")
        
        // other
        
        /**
         *ge20180410 : VACR130 : Remove guidance texts
         */
        if(VIAApplicableFeatures.default.removeAssessmentGuidanceText)!{
            inputTextField.placeholder = nil
        }else{
            inputTextField.placeholder = inputPlaceholder
        }
        inputTextField.text = inputText
        var values = [String]()
        if let uom = inputUnitOfMeasure{
            if uom == .FootInch || uom == .StonePound{
                if uom == .FootInch{
                    values = getValueFromAnswer(toSearch: inputText ?? "", uom1: "FT", uom2: "INCH")
                }else if uom == .StonePound{
                    values = getValueFromAnswer(toSearch: inputText ?? "", uom1: "ST", uom2: "LB")
                }
                
                inputTextField.text     = nil
                secondInputField.text   = nil
                if values.count > 0{
                    inputTextField.text     = values[0]
                }
                if values.count > 1{
                    secondInputField.text   = NSNumber(value: Double(values[1])!).stringValue
                }
            }
        }
        
        if let selectedUnitSymbol = self.selectedUnit?.symbol,
            (selectedUnitSymbol == self.footInchUnit || selectedUnitSymbol == self.stonePoundUnit) {
            
            /**
             *ge20180410 : VACR130 : Remove guidance texts
             * For Ecuador, always display the placeholders for foot-inch.
             */
            if(!VIAApplicableFeatures.default.removeAssessmentGuidanceText!) || AppSettings.getAppTenant() == .EC {
                let unitVal = stringUoM.characters.split{$0 == " "}.map(String.init)
                inputTextField.placeholder = unitVal[0]
                if unitVal.count > 1{
                    self.secondInputField.placeholder = unitVal[1]
                }
            }
        }
    }
    
    /** FC-18466
     * If UOM is Meter, show only its symbol
     */
    func unitOfMeasureChecker(unit: Unit) -> String {
        if VIAApplicableFeatures.default.vhrDisplayUoMAsSymbol() {
            switch (unit) {
            case UnitLength.meters:
                return "m"
            case UnitDuration.minutes:
                return "minutes"
            default:
                return measurementFormatter.string(from: unit)
            }
        } else {
            return measurementFormatter.string(from: unit)
        }
    }
    
    public func getValueFromAnswer(toSearch: String, uom1: String, uom2: String) -> [String]{
        
        var value = [String]()
        let pattern = "(\\d+(?:\\.\\d+)?)"
        let regex = try! NSRegularExpression(pattern: pattern, options: [])
        let nsString = toSearch as NSString
        let results = regex.matches(in: toSearch, range: NSRange(location: 0, length: nsString.length))
        
        //ge20180210 : Found that IN was moved to FT in FE when there is no FT.
        //           : Left FT empty when only provided IN
        //           : Todo: Clarify expected logic
        let beginningRegEx = try! NSRegularExpression(pattern: "(^\\d+)", options: [])
        let findFt = beginningRegEx.matches(in: toSearch, range: NSRange(location: 0, length: nsString.length)).map { nsString.substring(with: $0.range)}
        if (findFt.isEmpty){
            value.append("")
            return value + results.map { nsString.substring(with: $0.range)}
        }else{
            return results.map { nsString.substring(with: $0.range)}
        }
    }
    
    func setUnitsOfMeasureViewVisibility(hidden: Bool) {
        unitsButton.isHidden = hidden
        unitsArrowImageView.isHidden = hidden
        setNeedsUpdateConstraints()
        setNeedsLayout()
    }
    
    // MARK: Actions
    
    func setValidationErrorText(_ text: String?, for label: UILabel) {
        label.text = text
        label.isHidden = text == nil
    }
    
    // MARK: - Units
    
    func showUnitsActionSheet() {
        guard unitsOfMeasure.count > 1 else { return }
        
        let unitsMenu = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        unitsMenu.popoverPresentationController?.sourceView = self
        
        for rawUnitOfMeasure in unitsOfMeasure {
            let unit = rawUnitOfMeasure.unit()
            let title = measurementFormatter.string(from: unit)
            let action = UIAlertAction(title: title, style: .default, handler: { [weak self] (alert: UIAlertAction!) -> Void in
                self?.onSelectUoM(unit: unit, title: title, rawUnitOfMeasure: rawUnitOfMeasure)
                self?.captureResultsDelegate?.didSelectUnitOfMeasure(rawUnitOfMeasure)
            })
            unitsMenu.addAction(action)
        }
        
        let cancelAction = UIAlertAction(title: CommonStrings.CancelButtonTitle24, style: .cancel, handler: nil)
        unitsMenu.addAction(cancelAction)
        
        captureResultsDelegate?.show(menu: unitsMenu)
    }
    
    fileprivate func onSelectUoM(unit: Unit, title: String, rawUnitOfMeasure: UnitOfMeasureRef){
        if self.selectedUnit != rawUnitOfMeasure.unit(){
            self.inputTextField.text = nil
            self.secondInputField.text = nil
        }
        
        self.selectedUnit = unit
        self.unitsButtonTitle = title
        self.inputTextField.text = nil
        self.secondInputField.text = nil
        self.setValidationErrorText(nil, for: self.validationErrorLabel)
        self.setValidationErrorText(nil, for: self.secondValidationErrorLabel)
        
        /*
         * Focus textfield before reloading the view to make it the default uom value.
         */
        if rawUnitOfMeasure.unit() == UnitOfMeasureRef.FootInch.unit() || rawUnitOfMeasure.unit() == UnitOfMeasureRef.StonePound.unit() {
            self.inputTextField.becomeFirstResponder()
        }
        
        //        self.captureResultsDelegate?.didSelectUnitOfMeasure(rawUnitOfMeasure)
    }
    
    // MARK: - Textfield
    
    func setupInputTextField() {
        self.inputTextField.delegate = self
        self.inputTextField.addTarget(self, action: #selector(textFieldEditingChanged(_:)), for: .editingChanged)
        self.inputTextField.keyboardType = .decimalPad
        self.inputTextField.placeholder = nil
        self.inputTextField.font = UIFont.bodyFont()
        
        self.secondInputField.delegate = self
        self.secondInputField.addTarget(self, action: #selector(secondTextFieldTextDidChange(_:)), for: .editingChanged)
        self.secondInputField.keyboardType = .decimalPad
        self.secondInputField.placeholder = nil
        self.secondInputField.font = UIFont.bodyFont()
    }
    
    public func textFieldDidBeginEditing(_ textField: UITextField) {
        guard let action = self.textFieldDidBeginEditing else { return }
        action(textField, selectedUnit, &min, &max)
    }
    
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        // TODO: Deprecated. Implementation moved to shouldChangeCharactersInAssessmentNumberRangeQuestionSection().
        //        /**
        //         As per VA-34566, waist circumference units which is in/cm should also not accept decimal values.
        //         **/
        //        if !(VIAApplicableFeatures.default.shouldAcceptDecimalInput(selectedUnit!, questionTypeKey: questionRawTypeKey, healthAttributeType: .Unknown)) {
        //            let validInputs = NSCharacterSet(charactersIn:"0123456789").inverted
        //            let componentsToSeparate = string.components(separatedBy: validInputs)
        //            let numberFiltered = componentsToSeparate.joined(separator: "")
        //            return string == numberFiltered
        //        }
        //
        //        if ((questionRawTypeKey == 67 || questionRawTypeKey == 68) && (selectedUnit != UnitOfMeasureRef.FootInch.unit() || selectedUnit != UnitOfMeasureRef.StonePound.unit())) || VIAApplicableFeatures.default.limitDecimalPlacesOnAllNumberRangeQuestions() {
        //            return VIAApplicableFeatures.default.shouldChangeCharactersIn(textField.text ?? "", range: range, replacementString: string)
        //        }
        //
        //        return true
        
        return VIAApplicableFeatures.default.shouldChangeCharactersInAssessmentNumberRangeQuestionSection(textField, shouldChangeCharactersIn: range, replacementString: string, selectedUnit: selectedUnit!, questionTypeKey: questionRawTypeKey, healthAttributeType: .Unknown)
    }
    
    public func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == inputTextField{
            guard let action = self.textFieldDidEndEditing else { return }
            action(textField, selectedUnit, &min, &max)
        }else if textField == secondInputField{
            guard let action = self.secondTextFieldDidEndEditing else { return }
            action(textField, selectedUnit, &min, &max)
        }
    }
    
    func textFieldEditingChanged(_ textField: UITextField) {
        guard let action = self.textFieldTextDidChange else { return }
        action(textField, selectedUnit, &min, &max)
    }
    
    func secondTextFieldTextDidChange(_ textField: UITextField) {
        guard let action = self.secondTextFieldTextDidChange else { return }
        action(textField, selectedUnit, &min, &max)
    }
    
    
    // MARK: Validation
    
    public func updateInputValid(_ valid: Bool, _ min: Double?, _ max: Double?, _ errorLabel: UILabel) {
        if valid {
            self.inputIsValid(errorLabel: errorLabel)
        } else {
            self.inputIsNotValid(min: min, max: max, errorLabel: errorLabel)
        }
    }
    
    private func inputIsValid(errorLabel: UILabel) {
        self.setValidationErrorText(nil, for: errorLabel)
    }
    
    private func inputIsNotValid(min: Double?, max: Double?, errorLabel: UILabel) {
        var text: String = ""
        self.min = min
        self.max = max
        if let min = self.min, let max = self.max {
            // For mutiparts, use the whole number part of the validation error display.
            if selectedUnit == UnitOfMeasureRef.FootInch.unit() || selectedUnit == UnitOfMeasureRef.FootInch.unit() {
                text = CommonStrings.ErrorRange180(String(format: "%.0f", min), String(format: "%.0f", max))
            } else {
                if let formattedMin = decimalFormatter.string(from: min as NSNumber), let formattedMax = decimalFormatter.string(from: max as NSNumber) {
                    text = CommonStrings.ErrorRange180(formattedMin, formattedMax)
                }
            }
        } else if let min = self.min, self.max == nil {
            if let formattedMin = decimalFormatter.string(from: min as NSNumber) {
                text = CommonStrings.ErrorRangeBigger281(formattedMin)
            }
        } else if let max = self.max, self.min == nil {
            if let formattedMax = decimalFormatter.string(from: max as NSNumber) {
                text = CommonStrings.ErrorRangeSmaller282(formattedMax)
            }
        }
        self.setValidationErrorText(text, for: errorLabel)
    }
    
    public func updateInputValid(_ message: String, for errorLabel: UILabel) {
        self.setValidationErrorText(message, for: errorLabel)
    }
    
}
