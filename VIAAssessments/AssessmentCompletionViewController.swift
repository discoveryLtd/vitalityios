import Foundation
import UIKit
import VIAUIKit
import Foundation
import UIKit
import VIAUIKit
import VitalityKit

import VitalityKit

public protocol AssessmentCompletionViewControllerDelegate: class {

    func completionControllerDidSelectDone(_ completionViewController: AssessmentCompletionViewController)

    func completionControllerDidSelectNewQuestionnaire(_ completionViewController: AssessmentCompletionViewController, selectedQuestionnaireTypeKey: Int)

}

public class AssessmentCompletionViewController: VIACirclesViewController {

    weak var navigationDelegate: AssessmentCompletionViewControllerDelegate?

    var vhrViewModel: VHRCompletionViewModel? {
        didSet {
            self.viewModel = vhrViewModel
        }
    }

    var vhrQuestionnaireData = [AssessmentQuestionnaireCellData]()

    var selectedQuestionnaireTypeKey = -1

    override public func awakeFromNib() {
        super.awakeFromNib()
    }

    override public func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        addQuestionnaireCellsIfNeeded()
    }

    func addQuestionnaireCellsIfNeeded() {
        guard let questionnaires = vhrViewModel?.incompleteQuestionnaires else {return}
        if questionnaires.count > 0 {
            setupQuestionnaireViews()
        }
    }

    func setupQuestionnaireViews() {
        button.isHidden = true
        footnote.isHidden = true
        setupQuestionnaireData()

        let doneButton = UIButton()
        doneButton.titleLabel?.font = .headlineFont()
        doneButton.translatesAutoresizingMaskIntoConstraints = false
        doneButton.setTitle(CommonStrings.DoneButtonTitle53, for: .normal)
        doneButton.setTitleColor(.onboardingText(), for: .normal)
        doneButton.setTitleColor(UIColor.lightGrey(), for: .selected)
        doneButton.setTitleColor(UIColor.lightGrey(), for: .highlighted)
        doneButton.addTarget(self, action: #selector(buttonTapped(_:)), for: .touchUpInside)

        gradientView.addSubview(doneButton)

        doneButton.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(29)
            make.right.equalToSuperview().offset(-15)
            make.height.equalTo(20)
        }

        let mainStackView = UIStackView()
        mainStackView.alignment = .fill
        mainStackView.distribution = .fill
        mainStackView.axis = .vertical
        mainStackView.spacing = 15

        let topLineView = UIView()
        topLineView.backgroundColor = UIColor.flatWhite()
        topLineView.isUserInteractionEnabled = false
        topLineView.alpha = 0.7

        mainStackView.addArrangedSubview(topLineView)

        topLineView.snp.makeConstraints { make in
            make.top.equalToSuperview()
            make.right.equalToSuperview()
            make.left.equalToSuperview().offset(15)
            make.height.equalTo(1)
        }

        let stackItems = setupTitleTimeStackViews(cellData: vhrQuestionnaireData)
        for item in stackItems {
            mainStackView.addArrangedSubview(item)
        }

        gradientView.addSubview(mainStackView)

        mainStackView.snp.makeConstraints { make in
            make.top.equalTo(message.snp.bottom).offset(35)
            make.left.equalToSuperview()
            make.right.equalToSuperview()
        }
    }

    func setupTitleTimeStackViews(cellData: [AssessmentQuestionnaireCellData]) -> [UIView] {
        var containerViews = [UIView]()
        var buttonIndex = 0
        for data in cellData {

            let titleStackView = UIStackView()
            titleStackView.alignment = .fill
            titleStackView.distribution = .fill
            titleStackView.axis = .vertical
            titleStackView.spacing = 5

            let questionnaireTitleLabel = UILabel()
            questionnaireTitleLabel.text = data.title
            questionnaireTitleLabel.textColor = UIColor.onboardingText()
            questionnaireTitleLabel.font = .headlineFont()

            let timeToCompleteLabel = UILabel()
            timeToCompleteLabel.text = data.timeToComplete
            timeToCompleteLabel.textColor = UIColor.onboardingText()
            timeToCompleteLabel.font = .footnoteFont()

            titleStackView.addArrangedSubview(questionnaireTitleLabel)
            titleStackView.addArrangedSubview(timeToCompleteLabel)


            let containerView = UIView()
            let button = BorderButton()
            button.setupButton()
            button.tag = buttonIndex
            buttonIndex += 1
            button.addTarget(self, action: #selector(navigateToNextQuestionnaire(_:)), for: .touchUpInside)
            button.tintColor = UIColor.onboardingText()
            button.layer.borderColor = UIColor.onboardingText().cgColor
            button.sizeToFit()
            button.setTitle(data.buttonText, for: .normal)
            button.titleLabel?.font = UIFont.subheadlineFont()
            let bottomLineView = UIView()
            bottomLineView.backgroundColor = UIColor.flatWhite()
            bottomLineView.isUserInteractionEnabled = false
            bottomLineView.alpha = 0.7
            containerView.addSubview(titleStackView)
            containerView.addSubview(button)
            containerView.addSubview(bottomLineView)

            bottomLineView.snp.makeConstraints { make in
                make.bottom.equalToSuperview()
                make.right.equalToSuperview()
                make.left.equalToSuperview()
                make.height.equalTo(1)

                titleStackView.snp.makeConstraints { make in
                    make.top.equalToSuperview()
                    make.left.equalToSuperview()
                    make.bottom.equalTo(bottomLineView.snp.top).offset(-15)
                }
                button.snp.makeConstraints { make in
                    make.centerY.equalTo(titleStackView.snp.centerY)
                    make.right.equalToSuperview().offset(-15)
                    make.height.equalTo(28)
                }

                containerViews.append(containerView)
            }

        }
        return containerViews
    }

    func setupQuestionnaireData() {
        if let questionnaires = vhrViewModel?.incompleteQuestionnaires {
            for questionnaire in questionnaires {
                let typeKey = questionnaire.typeKey
                let title = questionnaire.typeName ?? ""
                var timeToComplete = questionnaire.textDescription
                var buttonText = CommonStrings.LandingScreen.StartButton305
                let completionState = questionnaire.getCompletionState()
                if completionState == .Complete {
                    buttonText = CommonStrings.LandingScreen.EditButton307
                    timeToComplete = CommonStrings.LandingScreen.CompletedMessage327
                } else if completionState == .InProgress {
                    buttonText = CommonStrings.LandingScreen.ContinueButton306
                }
                let completed = questionnaire.completionFlag
                let completeIcon = UIImage.templateImage(asset: .vhcGenericInHealthyRangeSmall)
                let cellData = AssessmentQuestionnaireCellData(title: title,
                                                        buttonText: buttonText,
                                                        timeToComplete: timeToComplete,
                                                        completed: completed,
                                                        completeIcon: completeIcon,
                                                        typeKey: typeKey)
                vhrQuestionnaireData.append(cellData)
            }
        }
    }

    override public func buttonTapped(_ sender: UIButton) {
        self.navigationDelegate?.completionControllerDidSelectDone(self)
    }

    func navigateToNextQuestionnaire(_ sender: BorderButton?) {
        guard let tagIndex = sender?.tag else { return }
        guard let incompleteQuestionnaires = vhrViewModel?.incompleteQuestionnaires else { return }

        selectedQuestionnaireTypeKey = incompleteQuestionnaires[tagIndex].typeKey
        self.navigationDelegate?.completionControllerDidSelectNewQuestionnaire(self, selectedQuestionnaireTypeKey: selectedQuestionnaireTypeKey)
    }

    override public func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        /*
         note: If you are to change the segue indentifier string,
         kindly change also the string used in AssessmentQuestionnaireFlowCoordinator. Thanks!
         */
        if segue.identifier == "unwindFromCompletionViewControllerToAssessmentLanding" {
            guard let questionnaires = vhrViewModel?.incompleteQuestionnaires else { return }
            guard let destination = segue.destination as? AssessmentLandingViewController else { return }
            
            if questionnaires.count <= 0 {
                destination.shouldShowVAgeHealthCard = true
            }
        }
    }
}
