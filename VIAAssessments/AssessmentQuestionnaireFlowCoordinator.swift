import Foundation
import VitalityKit
import VIAUIKit
import RealmSwift
import VIACommon
import VIAUtilities

protocol AssessmentSubmitter: class {
    func submit(for viewController: VIAViewController?, with completion:  @escaping (Error?) -> ())
}

class AssessmentQuestionnaireFlowCoordinator {

    // MARK: - Properties

    var navigationController: AssessmentNavigationController!

    var selectedQuestionnaire: VHRQuestionnaire?

    var potentialPoints: Int

    var currentSectionIndex = 0

    var realm = DataProvider.newVHRRealm()
    
    var shouldReloadWhenLandingViewIsPresented: Bool = false

    var totalVisibleSections: Int {
        var count = 0
        guard let questionnaire = self.selectedQuestionnaire else { return count }
        for section in questionnaire.questionnaireSections {
            if section.isVisible {
                count = count + 1
            }
        }
        return count
    }

    var currentSectionTitle: String {
        return self.selectedQuestionnaire?.typeName ?? ""
    }

    var currentQuestionnaireSection: VHRQuestionnaireSection? {
        return selectedQuestionnaire?.sortedVisibleQuestionnaireSections()[currentSectionIndex]
    }

    // MARK: - Init

    deinit {
        debugPrint("VHRQuestionnaireFlowCoordinator deinit")
    }

    init(with navigationController: AssessmentNavigationController, selectedQuestionnaire: VHRQuestionnaire?, potentialPoints: Int) {
        self.selectedQuestionnaire = selectedQuestionnaire
        self.navigationController = navigationController
        self.potentialPoints = potentialPoints
        setCurrentSectionIndex()
        configureNavigationBar()
    }

    // MARK: - Configuration

    func start() {
        configureTopViewController()
    }

    func configureTopViewController() {
        if let rootViewController = self.navigationController?.topViewController as? AssessmentQuestionnaireSectionViewController {
            configureQuestionnaireSectionViewController(rootViewController)
            configureNavigationStack(topViewController: rootViewController)
        }
    }

    func configureVHRDataSharingConsentViewControllerDelegate() {
        if let rootViewController = self.navigationController?.topViewController as? AssessmentDataSharingConsentViewController {
            rootViewController.navigationDelegate = self
            rootViewController.submitter = self
        }
    }

    func configureAssessmentCompletionViewControllerDelegate() {
        if let rootViewController = self.navigationController?.topViewController as? AssessmentCompletionViewController {
            rootViewController.navigationDelegate = self
        }
    }

    func configureNavigationStack(topViewController: AssessmentQuestionnaireSectionViewController) {
        let preceedingViewControllers = getAllPreceedingSectionsViewControllers()

        if preceedingViewControllers.count == 0 {
            self.navigationController.setViewControllers([topViewController], animated: true)
        } else {
            self.navigationController.setViewControllers([], animated: false)
            for viewController in preceedingViewControllers {
                self.navigationController.pushViewController(viewController, animated: false)
            }
            self.navigationController.pushViewController(topViewController, animated: true)
        }
    }

    func setCurrentSectionIndex() {
        currentSectionIndex = 0
        if let currentSectionTypeKey = selectedQuestionnaire?.currentSectionTypeKey {
            guard let section = realm.vhrQuestionnaireSection(by: currentSectionTypeKey) else { return }
            guard let sortedVisibleSections = selectedQuestionnaire?.sortedVisibleQuestionnaireSections() else { return }
            if let index = sortedVisibleSections.index(of: section) {
                currentSectionIndex = index
            }
        }
    }

    func configureQuestionnaireSectionViewController(_ viewController: AssessmentQuestionnaireSectionViewController) {
        guard let questionnaireSection = currentQuestionnaireSection else { return }
        
        viewController.title = selectedQuestionnaire?.typeName
        viewController.navigationDelegate = self
        viewController.answeringDelegate = self
        
        self.navigationController.sectionTitle = questionnaireSection.typeName
        let isFirstSection = currentSectionIndex == 0
        let isLastSection = currentSectionIndex == totalVisibleSections - 1
        let questionnaireSectionTypeKey = questionnaireSection.typeKey
        viewController.configure(isFirstSection: isFirstSection,
                                 isLastSection: isLastSection,
                                 questionnaireSectionTypeKey: questionnaireSectionTypeKey)
    }

    func configureQuestionnaireSectionViewControllerFor(section: VHRQuestionnaireSection, index: Int) -> AssessmentQuestionnaireSectionViewController? {
        guard let storyboard = self.navigationController?.storyboard else { return nil }
        guard let viewController = storyboard.instantiateViewController(withIdentifier: "AssessmentQuestionnaireSectionViewController") as? AssessmentQuestionnaireSectionViewController else { return nil }
        let isFirstSection = index == 0
        let isLastSection = false
        let questionnaireSectionTypeKey = section.typeKey
        viewController.configure(isFirstSection: isFirstSection,
                                 isLastSection: isLastSection,
                                 questionnaireSectionTypeKey: questionnaireSectionTypeKey)
        return viewController
    }

    func configureNavigationBar() {
        navigationController.currentSection = currentSectionIndex + 1
        navigationController.totalSections = totalVisibleSections
        if let questionnaireSection = currentQuestionnaireSection {
            navigationController.sectionIcon = questionnaireSection.image()
            navigationController.sectionTitle = questionnaireSection.typeName
        }
        navigationController.configureNavigationBar()
    }

    func configureWithUpdatedVisibleSections(vhrQuestionnaireSectionViewController: AssessmentQuestionnaireSectionViewController) {
        configureNavigationBar()
        vhrQuestionnaireSectionViewController.viewModel?.isLastSection = currentSectionIndex == totalVisibleSections - 1
    }

    // MARK: - VHR Coordinator Navigation Functions

    func goToNextSection() {
        currentSectionIndex += 1
        configureNavigationBar()

        guard let storyboard = self.navigationController?.storyboard else { return }
        guard let viewController = storyboard.instantiateViewController(withIdentifier: "AssessmentQuestionnaireSectionViewController") as? AssessmentQuestionnaireSectionViewController else { return }

        configureQuestionnaireSectionViewController(viewController)
        navigationController.pushViewController(viewController, animated: true)
    }

    func goToNextQuestionnaire() {
        configureNavigationBar()
        guard let storyboard = self.navigationController?.storyboard else { return }
        guard let viewController = storyboard.instantiateViewController(withIdentifier: "AssessmentQuestionnaireSectionViewController") as? AssessmentQuestionnaireSectionViewController else { return }
        configureQuestionnaireSectionViewController(viewController)
        configureNavigationStack(topViewController: viewController)
    }

    func getAllPreceedingSectionsViewControllers() -> [AssessmentQuestionnaireSectionViewController] {
        var preceedingViewControllers = [AssessmentQuestionnaireSectionViewController]()
        if currentSectionIndex != 0 {
            let preceedingSections = getAllPreceedingSections()
            for section in preceedingSections {
                if let index = preceedingSections.index(of: section) {
                    if let viewController = configureQuestionnaireSectionViewControllerFor(section: section, index: index) {
                        preceedingViewControllers.append(viewController)
                    }
                }
            }
        }
        return preceedingViewControllers
    }

    public func getAllPreceedingSections() -> [VHRQuestionnaireSection] {
        var index = 0
        var preceedingSections = [VHRQuestionnaireSection]()
        if let questionnaire = selectedQuestionnaire {
           let sortedVisibleSections = questionnaire.sortedVisibleQuestionnaireSections()
            while index < currentSectionIndex {
                preceedingSections.append(sortedVisibleSections[index])
                index += 1
            }
        }
        return preceedingSections
    }

    func showVHRDataSharingConsentViewController() {
        guard let storyboard = self.navigationController?.storyboard else { return }
        guard let viewController: AssessmentDataSharingConsentViewController = storyboard.instantiateViewController(withIdentifier: "AssessmentDataSharingConsentViewController") as? AssessmentDataSharingConsentViewController else { return }
        viewController.questionnaire = self.selectedQuestionnaire
        self.navigationController?.pushViewController(viewController, animated: true)
        configureVHRDataSharingConsentViewControllerDelegate()
    }

    func showCompleteViewController() {
        guard let storyboard = self.navigationController?.storyboard else { return }
        guard let viewController: AssessmentCompletionViewController = storyboard.instantiateViewController(withIdentifier: "AssessmentCompletionViewController") as? AssessmentCompletionViewController else { return }

        let incompleteQuestionnaires = realm.incompleteVHRQuestionnaires()
        viewController.vhrViewModel = VHRCompletionViewModel(incompleteQuestionnaires: incompleteQuestionnaires, sectionTitle: currentSectionTitle, potentialPoints: potentialPoints)

        self.navigationController.setViewControllers([viewController], animated: true)
        configureAssessmentCompletionViewControllerDelegate()
    }
}

// MARK: - Delegates

extension AssessmentQuestionnaireFlowCoordinator: AssessmentQuestionnaireSectionViewControllerDelegate {

    func sectionControllerDidSelectNext(_ vhrQuestionnaireSectionViewController: AssessmentQuestionnaireSectionViewController) {
        if currentSectionIndex < totalVisibleSections - 1 {
            goToNextSection()
        } else {
            sectionControllerDidSelectDone(vhrQuestionnaireSectionViewController)
        }
    }

    func sectionControllerDidSelectBack(_ vhrQuestionnaireSectionViewController: AssessmentQuestionnaireSectionViewController) {
        if currentSectionIndex > 0 {
            currentSectionIndex -= 1
            self.navigationController?.popViewController(animated: true)
            if let topView = navigationController.topViewController as? AssessmentQuestionnaireSectionViewController {
               configureQuestionnaireSectionViewController(topView)
            }

            configureNavigationBar()
        }
    }

    func sectionControllerDidSelectClose(_ vhrQuestionnaireSectionViewController: AssessmentQuestionnaireSectionViewController) {
        self.navigationController?.topViewController?.performSegue(withIdentifier: "unwindFromQuestionnaireSectionViewControllerToAssessmentLanding", sender: nil)
    }

    func sectionControllerDidSelectDone(_ controller: AssessmentQuestionnaireSectionViewController) {
        let tenantId = DataProvider.newRealm().getTenantId()
        if VitalityProductFeature.isEnabled((tenantId == 25) ? .VHCDSConsent : .VHRDSConsent) {
            self.showVHRDataSharingConsentViewController()
        } else {
            controller.showHUDOnWindow()
            self.submit(for: controller, with: { [weak self] error in 
                controller.hideHUDFromWindow()
                if let error = error {
                    controller.handleErrorWithAlert(error: error, tryAgain: { [weak self] error in
                        self?.sectionControllerDidSelectDone(controller)
                    })
                    return
                }
                self?.showCompleteViewController()
            })
        }
    }
}

extension AssessmentQuestionnaireFlowCoordinator: AssessmentDataSharingConsentViewControllerDelegate {

    func dataSharingConsentControllerDidSelectAgree() {
        showCompleteViewController()
    }

    func dataSharingConsentControllerDidSelectDisagree(_ controller: AssessmentDataSharingConsentViewController) {
        self.navigationController?.popViewController(animated: true)
        if (navigationController.topViewController as? AssessmentQuestionnaireSectionViewController) != nil {
            navigationController?.navigationBar.isHidden = false
            navigationController?.makeNavigationBarTransparent()
            configureNavigationBar()
        }
    }
}

extension AssessmentQuestionnaireFlowCoordinator: AssessmentCompletionViewControllerDelegate {
    func completionControllerDidSelectDone(_ completionViewController: AssessmentCompletionViewController) {
        /*
         note: If you are to change the segue indentifier string,
         kindly change also the string used in AssessmentCompletionViewController. Thanks!
         */
        self.navigationController?.topViewController?.performSegue(withIdentifier: "unwindFromCompletionViewControllerToAssessmentLanding", sender: nil)
    }

    func completionControllerDidSelectNewQuestionnaire(_ completionViewController: AssessmentCompletionViewController, selectedQuestionnaireTypeKey: Int) {
        selectedQuestionnaire = realm.getVHRQuestionnaire(using: selectedQuestionnaireTypeKey)
        setCurrentSectionIndex()
        goToNextQuestionnaire()
    }
}

// MARK : Submitter

extension AssessmentQuestionnaireFlowCoordinator: AssessmentSubmitter {
    
    
    func submit(for viewController: VIAViewController?, with completion:  @escaping (Error?) -> ()) {
        guard let questionnaireTypeKey = self.selectedQuestionnaire?.typeKey else {
            viewController?.displayUnknownServiceErrorOccurredAlert()
            return
        }
        
        // Call UILocalNotification, Successful notif & Error notif
        // Call API
        // Segue to completion
        
        VHRSubmissionHelper().captureAssesment(with: questionnaireTypeKey) { [weak self] (error) in
            if let error = error {
                completion(error)
            } else {
                self?.markQuestionnaireAsCompleted()
                let currentSectionViewController = self?.navigationController.topViewController as? AssessmentQuestionnaireSectionViewController
                currentSectionViewController?.tearDownViewModel()
                
                viewController?.hideHUDFromWindow()
                NotificationCenter.default.post(name: .VIAVHRQuestionnaireSubmittedNotification, object: nil)
                completion(error)
            }
        }
    }
    
    func markQuestionnaireAsCompleted() {
        do {
            try self.realm.write {
                self.selectedQuestionnaire?.setCompletedAndSubmitted()
            }
        } catch {
            print("Error occured marking VHR questionnaire as completed and submitted")
        }
    }
    
    func reloadVHR(for viewController: VIAViewController?, completion: @escaping (Error?) -> ()) {
        let coreRealm = DataProvider.newRealm()
        let partyId = coreRealm.getPartyId()
        let tenantId = coreRealm.getTenantId()
        let membershipId = coreRealm.getMembershipId()
        let setTypeKey = QuestionnaireSetRef.VHR
        let prePopulation = true
        let vhrQuestionnaireParameter = VHRQuestionnaireParameter(setTypeKey: setTypeKey, prePopulation: prePopulation)
        
        Wire.Member.VHRQuestionnaireProgressAndPointsTracking(tenantId: tenantId, partyId: partyId, vitalityMembershipId: membershipId, payload: vhrQuestionnaireParameter) { error in
            if let error = error {
                completion(error)
                return
            }
            completion(nil)
        }
    }
}

extension BackendErrorHandler where Self: UIViewController {
    func handleErrorWithAlert(error: Error, tryAgain: @escaping () -> Void) {
        switch error {
        case is BackendError:
            if let backendError = error as? BackendError {
                self.handleBackendErrorWithAlert(backendError, tryAgainAction: tryAgain)
            }
            return
        case is FrontendError:
            self.displayUnknownServiceErrorOccurredAlert()
            return
        default:
            self.displayUnknownServiceErrorOccurredAlert()
            return
        }
    }
}
