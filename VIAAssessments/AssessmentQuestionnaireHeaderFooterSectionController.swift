import IGListKit
import VIAUIKit
import VIAUtilities

public final class AssessmentQuestionnaireHeaderFooterSectionController: IGListSectionController, IGListSectionType {

    // MARK: Properties

    var headerFooter: AssessmentQuestionnaireSectionHeaderFooterDetail!

    // MARK: IGListSectionType

    public func numberOfItems() -> Int {
        return 1
    }

    public func didUpdate(to object: Any) {
        self.headerFooter = object as! AssessmentQuestionnaireSectionHeaderFooterDetail
    }

    public func cellForItem(at index: Int) -> UICollectionViewCell {
        let cell = collectionContext!.dequeueReusableCell(of: AssessmentTextCollectionViewCell.self, for: self, at: index) as! AssessmentTextCollectionViewCell
        cell.label.attributedText = headerFooter.textAndDescriptionDetail().attributedString
        cell.addTopBorder = true
        cell.addBottomBorder = true
        return cell
    }

    public func sizeForItem(at index: Int) -> CGSize {
        let width = collectionContext!.containerSize.width
        return AssessmentTextCollectionViewCell.height(with: headerFooter.textAndDescriptionDetail(), constrainedTo: width)
    }

    public func didSelectItem(at index: Int) { }

}
