import Foundation
import VitalityKit
import RealmSwift

public struct AssessmentQuestionnaireCellData {
    var title: String
    var buttonText: String
    var timeToComplete: String
    var completed: Bool
    var completeIcon: UIImage?
    var typeKey: Int
}

public struct AssessmentHeaderCellData {
    var title: String
    var description: String
    var totalSections: Int
    var completeSections: Int
    var completedImage: UIImage?
}

public protocol AssessmentLandingViewModel: class {

    var vhrRealm: Realm { get }
    var vhrQuestionnaireData: [AssessmentQuestionnaireCellData] { get }
    var vhrHeaderData: AssessmentHeaderCellData? { get }
    var integerFormatter: NumberFormatter { get }
    var questionnairesFooterText: String? { get }

    // MARK: Functions

    func shouldShowOnboarding() -> Bool

    func configureQuestionnaires()

    func configureHeader()

    func getTotalNumberOfQuestionnaires() -> Int

    func getHeaderDescriptionSubtext() -> String?

    func getTotalNumberOfCompletedQuestionnaires() -> Int

    func getQuestionnairesTotalPotentialPoints() -> Int

    func getQuestionnairesTotalEarnedPoints() -> Int

    func getQuestionnaireAt(index: Int) -> VHRQuestionnaire?

    func getVHRQuestionnaire(using selectedQuestionnaireTypeKey: Int) -> VHRQuestionnaire?

    func fetchQuestionnaires(forceUpdate: Bool, completion: @escaping (Error?, Bool?) -> Void)

}
