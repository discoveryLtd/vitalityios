//
//  MWBAssessmentQuestionFootnoteSectionController.swift
//  VitalityActive
//
//  Created by Michelle R. Oratil on 14/02/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import IGListKit
import VIAUIKit
import VIAUtilities
import VitalityKit
import RealmSwift
import VitalityKit

public final class MWBAssessmentQuestionFootnoteSectionController: IGListSectionController, IGListSectionType, FootnoteToggleable {
    
    // MARK: Properties
    
    var footnoteDetail: MWBAssessmentQuestionFootnotePresentationDetail!
    
    // MARK: FootnoteToggleable
    
    var isFootnoteExpanded: Bool = false
    
    var indexForFootnoteCell: Int? {
        return 0
    }
    
    // MARK: IGListSectionType
    
    public func numberOfItems() -> Int {
        return 1
    }
    
    public func didUpdate(to object: Any) {
        self.footnoteDetail = object as! MWBAssessmentQuestionFootnotePresentationDetail
    }
    
    public func cellForItem(at index: Int) -> UICollectionViewCell {
        let cell = collectionContext!.dequeueReusableCell(of: MWBAssessmentFootnoteCollectionViewCell.self, for: self, at: index) as! MWBAssessmentFootnoteCollectionViewCell
        cell.setLabelAttributedText(footnoteDetail.textDetail().attributedString, isFootnoteExpanded: self.isFootnoteExpanded)
        return cell
    }
    
    public func sizeForItem(at index: Int) -> CGSize {
        let width = collectionContext!.containerSize.width
        let item = footnoteDetail.textDetail()
        return MWBAssessmentFootnoteCollectionViewCell.height(with: item, constrainedTo: width, isFootnoteExpanded: self.isFootnoteExpanded)
    }
    
    public func didSelectItem(at index: Int) {
        toggleFootnote()
    }
    
}


public final class MWBAssessmentQuestionPrepopulationSectionController: IGListSectionController, IGListSectionType, FootnoteToggleable {
    
    // MARK: Properties
    
    var detail: MWBAssessmentQuestionPrepopulationPresentationDetail!
    
    // MARK: FootnoteToggleable
    
    var isFootnoteExpanded: Bool = false
    
    var indexForFootnoteCell: Int? {
        return 0
    }
    
    // MARK: IGListSectionType
    
    public func numberOfItems() -> Int {
        return 1
    }
    
    public func didUpdate(to object: Any) {
        self.detail = object as! MWBAssessmentQuestionPrepopulationPresentationDetail
    }
    
    public func cellForItem(at index: Int) -> UICollectionViewCell {
        let cell = collectionContext!.dequeueReusableCell(of: MWBAssessmentQuestionPrepopulationCollectionViewCell.self, for: self, at: index) as! MWBAssessmentQuestionPrepopulationCollectionViewCell
        cell.label.attributedText = detail.textDetail().attributedString
        cell.label.font = detail.text.font
        cell.label.textColor = detail.text.textColor
        return cell
    }
    
    public func sizeForItem(at index: Int) -> CGSize {
        let width = collectionContext!.containerSize.width
        let attributedTextDetail = detail.textDetail()
        return MWBAssessmentQuestionPrepopulationCollectionViewCell.height(with: attributedTextDetail, constrainedTo: width)
    }
    
    public func didSelectItem(at index: Int) { }
    
}

public final class MWBAssessmentQuestionPrepopulationCollectionViewCell: UICollectionViewCell, Nibloadable {
    
    public static let sideInset: CGFloat = 15.0
    
    public static let topInset: CGFloat = 8.0
    
    // MARK: Views
    
    public lazy var label: UILabel = {
        let label = UILabel(frame: .zero)
        label.font = UIFont.footnoteFont()
        label.textColor = UIColor.darkGrey()
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    // MARK: Init
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupSubviews()
    }
    
    private func setupSubviews() {
        contentView.backgroundColor = UIColor.tableViewBackground()
        contentView.addSubview(self.label)
        self.label.snp.makeConstraints { (make) in
            make.top.equalToSuperview().inset(MWBAssessmentQuestionPrepopulationCollectionViewCell.topInset)
            make.bottom.equalToSuperview()
            make.left.right.equalToSuperview().inset(MWBAssessmentQuestionPrepopulationCollectionViewCell.sideInset)
        }
    }
    
    override public func preferredLayoutAttributesFitting(_ layoutAttributes: UICollectionViewLayoutAttributes) -> UICollectionViewLayoutAttributes {
        return self.defaultContentViewLayoutSizeFitting(layoutAttributes)
    }
    
    // MARK: Height calculation
    
    public class func height(with attributedTextDetail: AttributedTextDetail, constrainedTo width: CGFloat) -> CGSize {
        let sideInset = MWBAssessmentQuestionPrepopulationCollectionViewCell.sideInset
        let insets = UIEdgeInsets(top: 0, left: sideInset, bottom: 0, right: sideInset)
        let size = TextSize.size(attributedTextDetail.attributedString, width: width, insets: insets)
        let height = size.height + MWBAssessmentQuestionPrepopulationCollectionViewCell.topInset // no bottom inset!
        return CGSize(width: width, height: ceil(height))
    }
    
}
