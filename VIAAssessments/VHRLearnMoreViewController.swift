import VIAUIKit
import VitalityKit
import VIAUtilities
import VIACommon

public final class VHRLearnMoreViewController: VIATableViewController, KnowYourHealthTintable {
    
    let viewModel = VHRLearnMoreViewModel()

    override public func viewDidLoad() {
        super.viewDidLoad()

        self.title = viewModel.title
        self.configureAppearance()
        self.configureTableView()
    }

    override public func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        self.hideBackButtonTitle()
    }

    func configureAppearance() {
        navigationController?.makeNavigationBarTransparent()
        navigationController?.navigationBar.barTintColor = UIColor.white
    }

    func configureTableView() {
        self.tableView.register(VIAGenericContentCell.nib(), forCellReuseIdentifier: VIAGenericContentCell.defaultReuseIdentifier)
        self.tableView.register(VIATableViewCell.nib(), forCellReuseIdentifier: VIATableViewCell.defaultReuseIdentifier)
        self.tableView.estimatedRowHeight = 75
        self.tableView.sectionHeaderHeight = 0
        self.tableView.sectionFooterHeight = 0
        self.tableView.estimatedSectionHeaderHeight = 0
        self.tableView.estimatedSectionFooterHeight = 0
        self.tableView.contentInset = UIEdgeInsets(top: 25, left: 0, bottom: 0, right: 0)
        self.tableView.backgroundColor = .white
        
        if !VIAApplicableFeatures.default.showVHRDisclaimer() {
            self.tableView.separatorColor = .clear
        }
    }
    
    enum Sections: Int, EnumCollection {
        case Content = 0
        case Clickable = 1
    }
    
    enum ClickableItems: Int, EnumCollection {
        case Disclaimer
        
        func title() -> String? {
            switch self {
            case .Disclaimer:
                return CommonStrings.DisclaimerTitle265
            }
        }
        
        func iconName() -> UIImage {
            switch self {
            case .Disclaimer:
                return VIAAssessmentsAsset.vhrDisclaimer.templateImage
            }
        }
    }

    // MARK: - UITableView datasource
    
    override public func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let section = Sections(rawValue: indexPath.section)
        if section == .Content && indexPath.row != viewModel.contentItems.count - 1 {
            cell.separatorInset = UIEdgeInsetsMake(0, max(UIScreen.main.bounds.size.width, UIScreen.main.bounds.size.height), 0, 0)
        } else if section == .Content && indexPath.row == viewModel.contentItems.count - 1 {
            cell.preservesSuperviewLayoutMargins = false
            cell.separatorInset = UIEdgeInsets.zero
            cell.layoutMargins = UIEdgeInsets.zero
        }
    }
    
    public override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let section = Sections(rawValue: indexPath.section)
        if section == .Content {
            return UITableViewAutomaticDimension
        } else {
            return 44
        }
    }

    override public func numberOfSections(in tableView: UITableView) -> Int {
        return VIAApplicableFeatures.default.showVHRDisclaimer() ? Sections.allValues.count : 1
    }

    override public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let section = Sections(rawValue: section)
        if section == .Content {
            return viewModel.contentItems.count
        } else if section == .Clickable {
            return 1
        }
        return 0
    }

    // MARK: UITableView delegate
    
    override public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let section = Sections(rawValue: indexPath.section)
        if section == .Content {
            return self.configureContentCell(indexPath)
        } else if section == .Clickable {
            return self.configureClickableItemCell(indexPath)
        }
        
        return tableView.dequeueReusableCell(withIdentifier: VIATableViewCell.defaultReuseIdentifier, for: indexPath)
    }
    
    override public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let section = Sections(rawValue: indexPath.section)
        if section == .Clickable {
            
            switch indexPath.row {
            case 0:
                showDisclaimer(self)
                break
            default:
                break
            }
        }
    }
    
    func configureContentCell(_ indexPath: IndexPath) -> VIAGenericContentCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: VIAGenericContentCell.defaultReuseIdentifier, for: indexPath) as! VIAGenericContentCell
        cell.isUserInteractionEnabled = false
        
        let item = viewModel.contentItems[indexPath.row]
        
        if indexPath.row == 0 {
            cell.customHeaderFont = viewModel.headingTitleFont
            cell.customContentFont = viewModel.headingMessageFont
        } else {
            cell.customHeaderFont = viewModel.sectionTitleFont
            cell.customContentFont = viewModel.sectionMessageFont
        }
        cell.header = item.header
        cell.content = item.content
        cell.cellImage = item.image
        cell.cellImageTintColor = viewModel.imageTint
        cell.layoutIfNeeded()
        
        return cell
    }
    
    func configureClickableItemCell(_ indexPath: IndexPath) -> VIATableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: VIATableViewCell.defaultReuseIdentifier, for: indexPath) as! VIATableViewCell
        let content = ClickableItems.allValues[indexPath.row]
        
        cell.textLabel?.text = content.title()
        cell.imageView?.image = content.iconName()
        cell.accessoryType = .disclosureIndicator
        cell.preservesSuperviewLayoutMargins = false
        cell.separatorInset = UIEdgeInsets.zero
        cell.layoutMargins = UIEdgeInsets.zero
        cell.imageView?.tintColor = viewModel.imageTint
        return cell
    }
    
    func showDisclaimer(_ sender: Any) {
        self.performSegue(withIdentifier: "showDisclaimer", sender: self)
    }
    
    override public func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDisclaimer", let controller = segue.destination as? VIAWebContentViewController {
            let webContentViewModel = VIAWebContentViewModel(articleId: AppConfigFeature.contentId(for: .VHRDisclaimerContent))
            controller.viewModel = webContentViewModel
            controller.title = CommonStrings.DisclaimerTitle265
        }
    }

}
