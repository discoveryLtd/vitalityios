//
//  MWBAssessmentQuestionnaireAnsweringDelegate.swift
//  VitalityActive
//
//  Created by Michelle R. Oratil on 14/02/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import VitalityKit
import RealmSwift
import VIAUtilities

public protocol MWBAssessmentQuestionnaireAnsweringDelegate: class {
    
    func persist(_ answer: String, unit: UnitOfMeasureRef?, for questionTypeKey: Int, of type: QuestionTypeRef, allowsMultipleAnswers: Bool, answerIsValid: Bool)
    
    func delete(_ answer: String, for questionTypeKey: Int, allowsMultipleAnswers: Bool)
    
}

extension MWBAssessmentQuestionnaireFlowCoordinator: MWBAssessmentQuestionnaireAnsweringDelegate {
    
    func persist(_ answer: String, unit: UnitOfMeasureRef? = nil, for questionTypeKey: Int, of type: QuestionTypeRef, allowsMultipleAnswers: Bool = false, answerIsValid: Bool) {
        updateQuestionnaireStatus()
        update(answer, unit: unit, for: questionTypeKey, of: type, delete: false, allowsMultipleAnswers: allowsMultipleAnswers, answerIsValid: answerIsValid)
        MWBreconfigureViewControllerWithUpdatedVisibleSections()
        //TODO: Is this necessary? we already called this in the update func.
        //        MWBQuestionnaire.updateVisibilityTags(for: answer, for: questionTypeKey, allowsMultipleAnswers: allowsMultipleAnswers)
    }
    
    func delete(_ answer: String, for questionTypeKey: Int, allowsMultipleAnswers: Bool = false) {
        updateQuestionnaireStatus()
        // fetch captured result and delete
        if let capturedResult = realm.mwbCapturedResult(for: questionTypeKey,
                                                        answer: answer,
                                                        allowsMultipleAnswers: allowsMultipleAnswers) {
            try! realm.write {
                realm.delete(capturedResult)
            }
        }
        MWBreconfigureViewControllerWithUpdatedVisibleSections()
        MWBQuestionnaire.updateVisibilityTags(for: answer, for: questionTypeKey, allowsMultipleAnswers: allowsMultipleAnswers)
    }
    
    func update(_ answer: String, unit: UnitOfMeasureRef? = nil, for questionTypeKey: Int, of type: QuestionTypeRef, delete: Bool, allowsMultipleAnswers: Bool = false, answerIsValid: Bool = false) {
        // capture a couple of things regarding the questionnaire, section, and question
        try! realm.write {
            if let questionnaire = selectedQuestionnaire {
                guard let questionnaireSectionTypeKey = selectedQuestionnaire?.sortedVisibleQuestionnaireSections()[currentSectionIndex].typeKey else {
                    return }
                
                let currentCapturedResult = realm.mwbCapturedResult(for: questionTypeKey,
                                                                    answer: answer,
                                                                    allowsMultipleAnswers: allowsMultipleAnswers)
                
                let answerAlreadyExists = currentCapturedResult != nil
                let capturedResult = currentCapturedResult ?? MWBCapturedResult()
                capturedResult.answer = answer
                capturedResult.questionTypeKey = questionTypeKey
                capturedResult.questionnaireTypeKey = questionnaire.typeKey
                capturedResult.questionnaireSectionTypeKey = questionnaireSectionTypeKey
                capturedResult.valid = answerIsValid
                capturedResult.type = type
                if let validUnit = unit {
                    capturedResult.unitOfMeasure = validUnit
                }
                
                if !answerAlreadyExists {
                    realm.add(capturedResult)
                }
            }
        }
        MWBQuestionnaire.updateVisibilityTags(for: answer, for: questionTypeKey, allowsMultipleAnswers: allowsMultipleAnswers)
    }
    
    func MWBreconfigureViewControllerWithUpdatedVisibleSections() {
        // TODO: only perform if any sections in current questionnaire depend on question that has been answered
        if let controller = self.navigationController.topViewController as? MWBAssessmentQuestionnaireSectionViewController {
            self.configureWithUpdatedVisibleSections(mwbQuestionnaireSectionViewController: controller)
        }
    }
    
    func updateQuestionnaireStatus() {
        if let questionnaire = selectedQuestionnaire {
            guard let questionnaireSection = selectedQuestionnaire?.sortedVisibleQuestionnaireSections()[currentSectionIndex] else { return }
            
            // capture current questionnaire section for coming back later,
            // and update the questionnaire state
            try! realm.write {
                questionnaire.currentSectionTypeKey = questionnaireSection.typeKey
                if questionnaire.getCompletionState() == .NotStarted {
                    questionnaire.updateCompletionState(MWBCompletionState.InProgress)
                    NotificationCenter.default.post(name: .VIAMWBQuestionnaireStarted, object: nil)
                }
            }
        }
    }
    
}
