//
//  MWBAssessmentToggleQuestionSectionController.swift
//  VitalityActive
//
//  Created by Michelle R. Oratil on 14/02/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import IGListKit
import VIAUIKit
import VIAUtilities
import VitalityKit
import RealmSwift
import VitalityKit

public final class MWBAssessmentToggleQuestionSectionController: IGListSectionController, IGListSectionType, FootnoteToggleable {
    
    private static let trueString = "True" // don't localise
    private static let falseString = "False" // don't localise
    
    // MARK: Properties
    
    var realm: Realm
    
    var questionDetail: MWBAssessmentQuestionPresentationDetail! {
        didSet {
            updateData()
        }
    }
    
    var isToggled: Bool = false {
        didSet {
            updateData()
        }
    }
    
    var capturedResult: MWBCapturedResult? {
        return self.questionDetail.rawQuestion(in: self.realm)?.capturedResult()
    }
    
    // MARK: FootnoteToggleable
    
    var isFootnoteExpanded: Bool = false
    
    var indexForFootnoteCell: Int? {
        return shouldAddNote() ? data.count - 1 : nil
    }
    
    // MARK: Lifecycle
    
    public required init(realm: Realm) {
        self.realm = realm
    }
    
    // MARK: Data
    
    var data: [AttributedTextDetail] = []
    
    func updateData() {
        // question
        data = [self.questionDetail.textAndDescriptionDetail()]
        
        // footnote
        if self.questionDetail.textNote != nil {
            data.append(self.questionDetail.noteDetail())
        }
    }
    
    func shouldAddNote() -> Bool {
        return self.questionDetail.textNote != nil
    }
    
    // MARK: IGListSectionType
    
    public func numberOfItems() -> Int {
        return data.count
    }
    
    public func didUpdate(to object: Any) {
        self.questionDetail = object as! MWBAssessmentQuestionPresentationDetail
        generateDefaultFalseAnswerIfNeeded()
        self.isToggled = self.capturedResult?.answer == MWBAssessmentToggleQuestionSectionController.trueString
    }
    
    public func cellForItem(at index: Int) -> UICollectionViewCell {
        let item = data[index]
        if item.type == .normal {
            return attributedTextCell(for: item, at: index)
        } else if item.type == .footnote {
            return footnoteAttributedTextCell(for: item, at: index)
        }
        return collectionContext!.dequeueReusableCell(of: UICollectionViewCell.self, for: self, at: index)
    }
    
    public func sizeForItem(at index: Int) -> CGSize {
        let width = collectionContext!.containerSize.width
        let item = data[index]
        
        if item.type == .normal {
            return MWBAssessmentToggleQuestionCollectionViewCell.height(with: item, constrainedTo: width)
        } else if item.type == .footnote {
            return MWBAssessmentFootnoteCollectionViewCell.height(with: item, constrainedTo: width, isFootnoteExpanded: self.isFootnoteExpanded)
        }
        return CGSize.zero
    }
    
    public func didSelectItem(at index: Int) {
        // tap on footnote
        guard index == 0 else {
            toggleFootnote()
            return
        }
        
        isToggled = !isToggled
        
        let answerValue = isToggled ? MWBAssessmentToggleQuestionSectionController.trueString : MWBAssessmentToggleQuestionSectionController.falseString
        updateCapturedResult(answer: answerValue)
    }
    
    // MARK: Capture
    
    func generateDefaultFalseAnswerIfNeeded() {
        if self.capturedResult == nil {
            updateCapturedResult(answer: MWBAssessmentToggleQuestionSectionController.falseString)
        }
    }
    
    func updateCapturedResult(answer: String) {
        guard let type = self.questionDetail.rawQuestion(in: realm)?.questionType else { return }
        
        if let controller = self.viewController as? MWBAssessmentQuestionnaireSectionViewController {
            controller.answeringDelegate?.persist(answer,
                                                  unit: nil,
                                                  for: self.questionDetail.rawQuestionTypeKey,
                                                  of: type,
                                                  allowsMultipleAnswers: false,
                                                  answerIsValid: true)
        }
    }
    
    // MARK: Cells
    
    func attributedTextCell(for textDetail: AttributedTextDetail, at index: Int) -> UICollectionViewCell {
        let cell = collectionContext!.dequeueReusableCell(of: MWBAssessmentToggleQuestionCollectionViewCell.self, for: self, at: index) as! MWBAssessmentToggleQuestionCollectionViewCell
        cell.label.attributedText = textDetail.attributedString
        cell.addTopBorder = true
        cell.addBottomBorder = true
        cell.isToggled = isToggled
        return cell
    }
    
    func footnoteAttributedTextCell(for textDetail: AttributedTextDetail, at index: Int) -> UICollectionViewCell {
        let cell = collectionContext!.dequeueReusableCell(of: MWBAssessmentFootnoteCollectionViewCell.self, for: self, at: index) as! MWBAssessmentFootnoteCollectionViewCell
        cell.setLabelAttributedText(textDetail.attributedString, isFootnoteExpanded: self.isFootnoteExpanded)
        return cell
    }
    
}

public final class MWBAssessmentToggleQuestionCollectionViewCell: UICollectionViewCell, Nibloadable {
    
    public static let inset: CGFloat = 15.0
    
    private static let minimumHeight: CGFloat = 26.0
    
    public var isToggled = false {
        didSet {
            toggleImageViewSelected()
        }
    }
    
    public var addTopBorder = false
    
    public var addBottomBorder = false
    
    // MARK: Views
    
    public lazy var label: UILabel = {
        let label = UILabel(frame: .zero)
        label.font = UIFont.title2Font()
        label.textColor = UIColor.night()
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    public lazy var imageView: UIImageView = {
        let view = UIImageView(frame: .zero)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.contentMode = .top
        view.image = UIImage.templateImage(asset: .vhrCheckMarkInactive)
        view.tintColor = UIColor.mediumGrey()
        return view
    }()
    
    // MARK: Init
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupSubviews()
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        
        contentView.layer.removeAllBorderLayers()
        if addTopBorder {
            contentView.layer.addBorder(edge: .top)
        }
        if addBottomBorder {
            contentView.layer.addBorder(edge: .bottom)
        }
    }
    
    private func setupSubviews() {
        contentView.backgroundColor = .day()
        
        let stackView = UIStackView()
        contentView.addSubview(stackView)
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .horizontal
        stackView.distribution = .fill
        stackView.spacing = 10
        stackView.addArrangedSubview(self.label)
        stackView.addArrangedSubview(self.imageView)
        
        stackView.snp.makeConstraints { (make) in
            make.top.bottom.equalToSuperview().inset(MWBAssessmentToggleQuestionCollectionViewCell.inset)
            make.left.right.equalToSuperview().inset(MWBAssessmentToggleQuestionCollectionViewCell.inset)
            make.height.greaterThanOrEqualTo(imageView.snp.height)
        }
        
        imageView.snp.makeConstraints { (make) in
            make.width.equalTo(MWBAssessmentToggleQuestionCollectionViewCell.minimumHeight).priority(1000)
        }
    }
    
    override public func preferredLayoutAttributesFitting(_ layoutAttributes: UICollectionViewLayoutAttributes) -> UICollectionViewLayoutAttributes {
        return self.defaultContentViewLayoutSizeFitting(layoutAttributes)
    }
    
    // MARK: Height calculation
    
    public class func height(with attributedTextDetail: AttributedTextDetail, constrainedTo width: CGFloat) -> CGSize {
        let inset = MWBAssessmentToggleQuestionCollectionViewCell.inset
        let insets = UIEdgeInsets(top: 0, left: inset, bottom: 0, right: inset)
        let size = TextSize.size(attributedTextDetail.attributedString, width: width, insets: insets)
        let height = max(size.height, MWBAssessmentToggleQuestionCollectionViewCell.minimumHeight) + inset * 2
        return CGSize(width: width, height: ceil(height))
    }
    
    // MARK: Helpers
    
    public func toggleImageViewSelected() {
        if isToggled {
            imageView.image = UIImage.templateImage(asset: .vhrCheckMarkActive)
            imageView.tintColor = UIColor.currentGlobalTintColor()
        } else {
            imageView.image = UIImage.templateImage(asset: .vhrCheckMarkInactive)
            imageView.tintColor = UIColor.mediumGrey()
        }
    }
    
}
