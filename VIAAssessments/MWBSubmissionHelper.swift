//
//  MWBSubmissionHelper.swift
//  VitalityActive
//
//  Created by Harry C. Lingad on 2/5/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import VitalityKit
import RealmSwift
import VIAUtilities
import VIACommon

protocol MWBAssesmentProcessor {
    func captureAssesment(with questionnaireTypeKey: Int, completion: @escaping (_ error: Error?) -> Void)
}

class MWBSubmissionHelper: MWBAssesmentProcessor {
    
    let mwbRealm = DataProvider.newMWBRealm()
    let coreRealm = DataProvider.newRealm()
    
    func captureAssesment(with questionnaireTypeKey: Int, completion: @escaping (Error?) -> Void) {
        let tenantID = coreRealm.getTenantId()
        guard let MWBQuestionnaireProgressAndPointsTracking = mwbRealm.allMWBQuestionnaireProgressAndPointsTracking().first else {
            completion(FrontendError.other)
            return
        }
        
        let captureAssesment = self.setupCaptureAssessment(with: questionnaireTypeKey,
                                                           questionnaireSetTypeKey: MWBQuestionnaireProgressAndPointsTracking.questionnaireSetType.rawValue,
                                                           partyID: coreRealm.getPartyId())
        
        Wire.Events.captureAssessment(tenantId: tenantID, request: captureAssesment) { [weak self] (response, error) in
            if let responseError = error {
                completion(responseError)
                return
            } else {
                self?.mwbRealm.markAllUnsubmittedMWBCapturedResultsAsSubmitted()
                completion(nil)
                return
            }
        }
    }
    
    internal func setupCaptureAssessment(with questionnaireTypeKey: Int, questionnaireSetTypeKey: Int, partyID: Int) -> CaptureAssessmentRequest {
        let capturedResults = mwbRealm.allUnsubmittedAndVisibleMWBCapturedResults(for: questionnaireTypeKey)
        let assessments = [setupAssessment(with: capturedResults, questionnaireTypeKey: questionnaireTypeKey)]
        return CaptureAssessmentRequest(assessment: assessments,
                                        partyId: partyID,
                                        questionnaireSetTypeKey: questionnaireSetTypeKey)
    }
    
//    internal func setupAssessment(with capturedResults: [MWBCapturedResult], questionnaireTypeKey: Int) -> Assessment {
//        var assessmentAnswers = [AssessmentAnswer]()
//        let filteredCapturedResults = capturedResults.filter({ $0.answer != nil })
//        
//        
//        for capturedResult in filteredCapturedResults {
//            guard let answer = capturedResult.answer else { continue }
//            
//            let unitOfMeasure = capturedResult.unitOfMeasure == .Unknown ? nil : String(describing: capturedResult.unitOfMeasure.rawValue)
//            let selectedValue = SelectedValues(value: answer, valueType: capturedResult.type.rawValue, fromValue: nil, toValue: nil, unitOfMeasure: unitOfMeasure)
//            
//            let assessmentAnswer = AssessmentAnswer(answerStatusTypeKey: AssessmentAnswerStatusRef.Answered.rawValue,
//                                                    questionTypeKey: capturedResult.questionTypeKey,
//                                                    selectedValues: [selectedValue])
//            assessmentAnswers.append(assessmentAnswer)
//        }
//        return Assessment(assessmentAnswer: assessmentAnswers,
//                          statusTypeKey: QuestionnaireChannelRef.Mobile.rawValue,
//                          typeKey: questionnaireTypeKey)
//        
//    }
    
    internal func setupAssessment(with capturedResults: [MWBCapturedResult], questionnaireTypeKey: Int) -> Assessment {
        var assessmentAnswers = [AssessmentAnswer]()
        let filteredCapturedResults = capturedResults.filter({ $0.answer != nil })
        
        
        for capturedResult in filteredCapturedResults {
            /* Not sure why was the database designed like this, but let's just take out all the selected values
             * using the current questionTypeKey and consolidate it in our selectedValues
             */
            let selectedValues = getAllSelectedValues(from: filteredCapturedResults, where: capturedResult.questionTypeKey)
            
            /* Let's create the Assessment Answer*/
            let assessmentAnswer = AssessmentAnswer(answerStatusTypeKey: AssessmentAnswerStatusRef.Answered.rawValue,
                                                    questionTypeKey: capturedResult.questionTypeKey,
                                                    selectedValues: selectedValues)
            
            //TODO This is too exhaustive. Refactor this one.
            if !doesContains(capturedResult.questionTypeKey, In: assessmentAnswers){
                /* We should make sure that there are no duplicate questionTypeKey before adding to our JSON request */
                assessmentAnswers.append(assessmentAnswer)
            }
        }
        
        return Assessment(assessmentAnswer: assessmentAnswers,
                          statusTypeKey: QuestionnaireChannelRef.Mobile.rawValue,
                          typeKey: questionnaireTypeKey)
        
    }
    
    private func getAllSelectedValues(from filteredCapturedResults: [MWBCapturedResult],
                                      where questionnaireTypeKey: Int) -> [SelectedValues]{
        var selectedValues:[SelectedValues] = []
        
        for capturedResult in filteredCapturedResults{
            if capturedResult.questionTypeKey == questionnaireTypeKey{
                guard let answer = capturedResult.answer else { continue }
                let unitOfMeasure = capturedResult.unitOfMeasure == .Unknown ? nil : String(describing: capturedResult.unitOfMeasure.rawValue)
                let selectedValue = SelectedValues(value: answer, valueType: capturedResult.type.rawValue, fromValue: nil, toValue: nil, unitOfMeasure: unitOfMeasure)
                selectedValues.append(selectedValue)
            }
        }
        return selectedValues
    }
    
    private func doesContains(_ questionnaireTypeKey: Int, In assessmentAnswers: [AssessmentAnswer]) -> Bool{
        for answer in assessmentAnswers{
            if answer.questionTypeKey ==  questionnaireTypeKey{
                return true
            }
        }
        return false
    }
}

