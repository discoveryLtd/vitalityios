//
//  MWBAssessmentNavigationButtonSectionController.swift
//  VitalityActive
//
//  Created by Michelle R. Oratil on 14/02/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import IGListKit
import VIAUIKit
import VIAUtilities
import VitalityKit
import VIACommon

public final class MWBAssessmentNavigationButtonSectionController: IGListSectionController, IGListSectionType {
    
    // MARK: Properties
    
    var detail: MWBAssessmentQuestionnaireSectionNavigationDetail!
    
    // MARK: IGListSectionType
    
    public func numberOfItems() -> Int {
        return 1
    }
    
    public func didUpdate(to object: Any) {
        self.detail = object as! MWBAssessmentQuestionnaireSectionNavigationDetail
    }
    
    public func cellForItem(at index: Int) -> UICollectionViewCell {
        let cell = collectionContext!.dequeueReusableCell(of: MWBAssessmentNavigationButtonCollectionViewCell.self, for: self, at: index) as! MWBAssessmentNavigationButtonCollectionViewCell
        
        cell.button.setTitle(CommonStrings.NextButtonTitle84, for: .normal)
        if detail.isLastSection {
            cell.button.setTitle(CommonStrings.DoneButtonTitle53, for: .normal)
        }
        
        cell.buttonTapped = self.buttonTapped

        if(VIAApplicableFeatures.default.showRequiredPrompt!){
            cell.button.isEnabled = true
        }else{
            cell.button.isEnabled = self.detail.isEnabled
        }
        
        return cell
    }
    
    public func sizeForItem(at index: Int) -> CGSize {
        let width = collectionContext!.containerSize.width
        return MWBAssessmentNavigationButtonCollectionViewCell.height(constrainedTo: width)
    }
    
    public func didSelectItem(at index: Int) { }
    
    func buttonTapped() {
        if (VIAApplicableFeatures.default.showRequiredPrompt!) {
            /**
             * For SLI Market, when next is clicked, "Required" header should be displayed
             * if no answer is provided.
             */
            let nextStatus:[String: Bool] = ["isNextButtonTapped": true]
            NotificationCenter.default.post(name: .VIAMWBQuestionnaireNextNotification, object: nil, userInfo: nextStatus)
            
            if (self.detail.isEnabled) {
                if let controller = self.viewController as? MWBAssessmentQuestionnaireSectionViewController {
                    controller.next(nil)
                }
            }
        } else {
            if let controller = self.viewController as? MWBAssessmentQuestionnaireSectionViewController {
                controller.next(nil)
            }
        }
    }
}

public final class MWBAssessmentNavigationButtonCollectionViewCell: UICollectionViewCell, Nibloadable {
    
    private static let buttonHeight: CGFloat = 44.0
    
    internal var buttonTapped: () -> Void = {
        debugPrint("MWBAssessmentNavigationButtonCollectionViewCell button tapped")
    }
    
    // MARK: Views
    
    public lazy var button: VIAButton = {
        let button = VIAButton(title: CommonStrings.NextButtonTitle84)
        button.tintColor = UIColor.currentGlobalTintColor()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(buttonTapped(_:)), for: .touchUpInside)
        return button
    }()
    
    // MARK: Init
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupSubviews()
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    private func setupSubviews() {
        contentView.backgroundColor = UIColor.mainViewBackground()
        
        contentView.addSubview(button)
        button.snp.makeConstraints { (make) in
            make.top.bottom.equalToSuperview()
            make.width.equalToSuperview().multipliedBy(0.6)
            make.centerX.equalToSuperview()
            make.height.equalTo(MWBAssessmentNavigationButtonCollectionViewCell.buttonHeight)
        }
    }
    
    override public func preferredLayoutAttributesFitting(_ layoutAttributes: UICollectionViewLayoutAttributes) -> UICollectionViewLayoutAttributes {
        return self.defaultContentViewLayoutSizeFitting(layoutAttributes)
    }
    
    // MARK: Height calculation
    
    public class func height(constrainedTo width: CGFloat) -> CGSize {
        let height = MWBAssessmentNavigationButtonCollectionViewCell.buttonHeight
        return CGSize(width: width, height: ceil(height))
    }
    
    // MARK: Actions
    
    func buttonTapped(_ sender: Any?) {
        buttonTapped()
    }
    
}
