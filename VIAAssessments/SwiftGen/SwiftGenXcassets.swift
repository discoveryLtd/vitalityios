// Generated using SwiftGen, by O.Halligon — https://github.com/SwiftGen/SwiftGen

#if os(OSX)
import AppKit.NSImage
public typealias VIAAssessmentsColor = NSColor
public typealias VIAAssessmentsImage = NSImage
#elseif os(iOS) || os(tvOS) || os(watchOS)
import UIKit.UIImage
public typealias VIAAssessmentsColor = UIColor
public typealias VIAAssessmentsImage = UIImage
#endif

// swiftlint:disable file_length

public typealias VIAAssessmentsAssetType = VIAAssessmentsImageAsset

public struct VIAAssessmentsImageAsset: Equatable {
	fileprivate var name: String

	public var templateImage: VIAAssessmentsImage {
		let bundle = Bundle(for: BundleToken.self)
		#if os(iOS) || os(tvOS)
		let image = VIAAssessmentsImage(named: name, in: bundle, compatibleWith: nil)?.withRenderingMode(.alwaysTemplate)
		#elseif os(OSX)
		let image = bundle.image(forResource: name).withRenderingMode(.alwaysTemplate)
		#elseif os(watchOS)
		let image = VIAAssessmentsImage(named: name).withRenderingMode(.alwaysTemplate)
		#endif
		guard let result = image else { fatalError("Unable to load template image named \(name).") }
		return result
	}

	public var image: VIAAssessmentsImage {
		let bundle = Bundle(for: BundleToken.self)
		#if os(iOS) || os(tvOS)
		let image = VIAAssessmentsImage(named: name, in: bundle, compatibleWith: nil)
		#elseif os(OSX)
		let image = bundle.image(forResource: name)
		#elseif os(watchOS)
		let image = VIAAssessmentsImage(named: name)
		#endif
		guard let result = image else { fatalError("Unable to load image named \(name).") }
		return result
	}

	public static func ==(lhs: VIAAssessmentsImageAsset, rhs: VIAAssessmentsImageAsset) -> Bool {
        return lhs.image == rhs.image
    }
}

public struct VIAAssessmentsColorAsset {
fileprivate var name: String

#if swift(>=3.2)
@available(iOS 11.0, tvOS 11.0, watchOS 4.0, OSX 10.13, *)
var color: VIAAssessmentsColor {
return VIAAssessmentsColor(asset: self)
}
#endif
}

// swiftlint:disable identifier_name line_length nesting type_body_length type_name
public enum VIAAssessmentsAsset {
  public static let vhcGenericInHealthyRangeSmall = VIAAssessmentsImageAsset(name: "vhcGenericInHealthyRangeSmall")
  public static let vhrOnboardingCompleteTime = VIAAssessmentsImageAsset(name: "vhrOnboardingCompleteTime")
  public static let vhrOnboardingVitalityAge = VIAAssessmentsImageAsset(name: "vhrOnboardingVitalityAge")
  public static let arrowDrill = VIAAssessmentsImageAsset(name: "arrowDrill")
  public static let onboardingEarnPoints = VIAAssessmentsImageAsset(name: "onboardingEarnPoints")
  public static let vhrCheckMarkActive = VIAAssessmentsImageAsset(name: "vhrCheckMarkActive")
  public static let hsCardCheckmarkBig = VIAAssessmentsImageAsset(name: "hsCardCheckmarkBig")
  public static let vhrCheckMarkSingle = VIAAssessmentsImageAsset(name: "vhrCheckMarkSingle")
  public enum SectionIcons {
    public static let vnaMedicalHistory = VIAAssessmentsImageAsset(name: "vnaMedicalHistory")
    public static let vnaMeats = VIAAssessmentsImageAsset(name: "vnaMeats")
    public static let vhrSectionFamilyMedical = VIAAssessmentsImageAsset(name: "vhrSectionFamilyMedical")
    public static let vnaSaturatedFattyAcids = VIAAssessmentsImageAsset(name: "vnaSaturatedFattyAcids")
    public static let vhrSectionKeyMeasures = VIAAssessmentsImageAsset(name: "vhrSectionKeyMeasures")
    public static let mwbPsychological = VIAAssessmentsImageAsset(name: "mwbPsychological")
    public static let vnaDietaryPreferences = VIAAssessmentsImageAsset(name: "vnaDietaryPreferences")
    public static let vhrSectionSmoking = VIAAssessmentsImageAsset(name: "vhrSectionSmoking")
    public static let vnaSectionMicronutrientsAndFiber = VIAAssessmentsImageAsset(name: "vnaSectionMicronutrientsAndFiber")
    public static let vhrSectionOverall = VIAAssessmentsImageAsset(name: "vhrSectionOverall")
    public static let vhrSectionPhysicalActivity = VIAAssessmentsImageAsset(name: "vhrSectionPhysicalActivity")
    public static let vnaAllergiesAndIntolerances = VIAAssessmentsImageAsset(name: "vnaAllergiesAndIntolerances")
    public static let vhrSectionMedicalHistory = VIAAssessmentsImageAsset(name: "vhrSectionMedicalHistory")
    public static let vnaSodium = VIAAssessmentsImageAsset(name: "vnaSodium")
    public static let vhrSectionAlcoholIntake = VIAAssessmentsImageAsset(name: "vhrSectionAlcoholIntake")
    public static let vnaSugar = VIAAssessmentsImageAsset(name: "vnaSugar")
    public static let vhrSectionEatingHabits = VIAAssessmentsImageAsset(name: "vhrSectionEatingHabits")
    public static let vhrSectionSleepingPatterns = VIAAssessmentsImageAsset(name: "vhrSectionSleepingPatterns")
    public static let vnaFats = VIAAssessmentsImageAsset(name: "vnaFats")
    public static let vnaEatingHabits = VIAAssessmentsImageAsset(name: "vnaEatingHabits")
    public static let vnaFoodPrep = VIAAssessmentsImageAsset(name: "vnaFoodPrep")
    public static let mwbStressor = VIAAssessmentsImageAsset(name: "mwbStressor")
    public static let vnaMicronutrientsAndFibre = VIAAssessmentsImageAsset(name: "vnaMicronutrientsAndFibre")
    public static let mwbSocial = VIAAssessmentsImageAsset(name: "mwbSocial")
    public static let vnaCalciumAndPotassium = VIAAssessmentsImageAsset(name: "vnaCalciumAndPotassium")
    public static let vnaSnacks = VIAAssessmentsImageAsset(name: "vnaSnacks")
    public static let vhrSectionWellbeing = VIAAssessmentsImageAsset(name: "vhrSectionWellbeing")
    public static let vnaMealpattern = VIAAssessmentsImageAsset(name: "vnaMealpattern")
  }
  public static let vhrLearnMorePoints = VIAAssessmentsImageAsset(name: "vhrLearnMorePoints")
  public static let vhrBackArrow = VIAAssessmentsImageAsset(name: "vhrBackArrow")
  public static let vhrLearnCompleteTime = VIAAssessmentsImageAsset(name: "vhrLearnCompleteTime")
  public static let vhrLearnVitalityAge = VIAAssessmentsImageAsset(name: "vhrLearnVitalityAge")
  public static let vhrGenericLearnMore = VIAAssessmentsImageAsset(name: "vhrGenericLearnMore")
  public static let requiredEnabled = VIAAssessmentsImageAsset(name: "requiredEnabled")
  public static let helpGeneric = VIAAssessmentsImageAsset(name: "helpGeneric")
  public static let vhrDisclaimer = VIAAssessmentsImageAsset(name: "vhrDisclaimer")
  public static let vhrCheckMarkInactive = VIAAssessmentsImageAsset(name: "vhrCheckMarkInactive")
  public static let vhrOnboardingEarnPoints = VIAAssessmentsImageAsset(name: "vhrOnboardingEarnPoints")
  public enum Mwb {
    public static let mwbLearnMorePsychological = VIAAssessmentsImageAsset(name: "mwbLearnMorePsychological")
    public static let mwbLearnMorePoints = VIAAssessmentsImageAsset(name: "mwbLearnMorePoints")
    public static let mwbLearnMoreSocial = VIAAssessmentsImageAsset(name: "mwbLearnMoreSocial")
    public static let mwbLearnMoreTaC = VIAAssessmentsImageAsset(name: "mwbLearnMoreTaC")
    public static let mwbLearnMoreTimeToComplete = VIAAssessmentsImageAsset(name: "mwbLearnMoreTimeToComplete")
    public static let mwbLearnMoreStressor = VIAAssessmentsImageAsset(name: "mwbLearnMoreStressor")
    public static let mwbLearnMoreWellbeing = VIAAssessmentsImageAsset(name: "mwbLearnMoreWellbeing")
  }
  public static let mentalwellbeing = VIAAssessmentsImageAsset(name: "mentalwellbeing")
  public static let onboardingSections = VIAAssessmentsImageAsset(name: "onboardingSections")
  public enum Vna {
    public static let vnaLearnMoreNutritionActive = VIAAssessmentsImageAsset(name: "vnaLearnMoreNutritionActive")
    public static let vnaDisclaimer = VIAAssessmentsImageAsset(name: "vnaDisclaimer")
    public static let vnaNutritiionActive = VIAAssessmentsImageAsset(name: "vnaNutritiionActive")
    public static let vhrSectionMicronutrientsAndFiber = VIAAssessmentsImageAsset(name: "vhrSectionMicronutrientsAndFiber")
  }
  public static let arOnboardingActivated = VIAAssessmentsImageAsset(name: "AROnboardingActivated")

  // swiftlint:disable trailing_comma
  public static let allColors: [VIAAssessmentsColorAsset] = [
  ]
  public static let allImages: [VIAAssessmentsImageAsset] = [
    vhcGenericInHealthyRangeSmall,
    vhrOnboardingCompleteTime,
    vhrOnboardingVitalityAge,
    arrowDrill,
    onboardingEarnPoints,
    vhrCheckMarkActive,
    hsCardCheckmarkBig,
    vhrCheckMarkSingle,
    SectionIcons.vnaMedicalHistory,
    SectionIcons.vnaMeats,
    SectionIcons.vhrSectionFamilyMedical,
    SectionIcons.vnaSaturatedFattyAcids,
    SectionIcons.vhrSectionKeyMeasures,
    SectionIcons.mwbPsychological,
    SectionIcons.vnaDietaryPreferences,
    SectionIcons.vhrSectionSmoking,
    SectionIcons.vnaSectionMicronutrientsAndFiber,
    SectionIcons.vhrSectionOverall,
    SectionIcons.vhrSectionPhysicalActivity,
    SectionIcons.vnaAllergiesAndIntolerances,
    SectionIcons.vhrSectionMedicalHistory,
    SectionIcons.vnaSodium,
    SectionIcons.vhrSectionAlcoholIntake,
    SectionIcons.vnaSugar,
    SectionIcons.vhrSectionEatingHabits,
    SectionIcons.vhrSectionSleepingPatterns,
    SectionIcons.vnaFats,
    SectionIcons.vnaEatingHabits,
    SectionIcons.vnaFoodPrep,
    SectionIcons.mwbStressor,
    SectionIcons.vnaMicronutrientsAndFibre,
    SectionIcons.mwbSocial,
    SectionIcons.vnaCalciumAndPotassium,
    SectionIcons.vnaSnacks,
    SectionIcons.vhrSectionWellbeing,
    SectionIcons.vnaMealpattern,
    vhrLearnMorePoints,
    vhrBackArrow,
    vhrLearnCompleteTime,
    vhrLearnVitalityAge,
    vhrGenericLearnMore,
    requiredEnabled,
    helpGeneric,
    vhrDisclaimer,
    vhrCheckMarkInactive,
    vhrOnboardingEarnPoints,
    Mwb.mwbLearnMorePsychological,
    Mwb.mwbLearnMorePoints,
    Mwb.mwbLearnMoreSocial,
    Mwb.mwbLearnMoreTaC,
    Mwb.mwbLearnMoreTimeToComplete,
    Mwb.mwbLearnMoreStressor,
    Mwb.mwbLearnMoreWellbeing,
    mentalwellbeing,
    onboardingSections,
    Vna.vnaLearnMoreNutritionActive,
    Vna.vnaDisclaimer,
    Vna.vnaNutritiionActive,
    Vna.vhrSectionMicronutrientsAndFiber,
    arOnboardingActivated,
  ]
  // swiftlint:enable trailing_comma
  @available(*, deprecated, renamed: "allImages")
  public static let allValues: [VIAAssessmentsAssetType] = allImages
}
// swiftlint:enable identifier_name line_length nesting type_body_length type_name

extension VIAAssessmentsImage {
@available(iOS 1.0, tvOS 1.0, watchOS 1.0, *)
@available(OSX, deprecated,
message: "This initializer is unsafe on macOS, please use the VIAAssessmentsImageAsset.image property")
convenience init!(asset: VIAAssessmentsAssetType) {
#if os(iOS) || os(tvOS)
let bundle = Bundle(for: BundleToken.self)
self.init(named: asset.name, in: bundle, compatibleWith: nil)
#elseif os(OSX) || os(watchOS)
self.init(named: asset.name)
#endif
}
}

extension VIAAssessmentsColor {
#if swift(>=3.2)
@available(iOS 11.0, tvOS 11.0, watchOS 4.0, OSX 10.13, *)
convenience init!(asset: VIAAssessmentsColorAsset) {
let bundle = Bundle(for: BundleToken.self)
#if os(iOS) || os(tvOS)
self.init(named: asset.name, in: bundle, compatibleWith: nil)
#elseif os(OSX)
self.init(named: asset.name, bundle: bundle)
#elseif os(watchOS)
self.init(named: asset.name)
#endif
}
#endif
}

private final class BundleToken {}
