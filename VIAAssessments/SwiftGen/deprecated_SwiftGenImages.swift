// Generated using SwiftGen, by O.Halligon — https://github.com/AliSoftware/SwiftGen

internal class ASSImagesPlaceholder {
    // Placeholder class to reference lower down
}

#if os(iOS) || os(tvOS) || os(watchOS)
  import UIKit.UIImage
  typealias Image = UIImage
#elseif os(OSX)
  import AppKit.NSImage
  typealias Image = NSImage
#endif

// swiftlint:disable file_length
// swiftlint:disable line_length

// swiftlint:disable type_body_length
@available(*, deprecated, message: "Use VIAAssessments.{image}.image or VIAAssessments.{image}.templateImage instead")
internal enum ASSAsset: String {
  case arOnboardingActivated = "AROnboardingActivated"
  case arrowDrill = "arrowDrill"
  case helpGeneric = "helpGeneric"
  case hsCardCheckmarkBig = "hsCardCheckmarkBig"
  case vhcGenericInHealthyRangeSmall = "vhcGenericInHealthyRangeSmall"
  case vhrBackArrow = "vhrBackArrow"
  case vhrCheckMarkActive = "vhrCheckMarkActive"
  case vhrCheckMarkInactive = "vhrCheckMarkInactive"
  case vhrCheckMarkSingle = "vhrCheckMarkSingle"
  case vhrGenericLearnMore = "vhrGenericLearnMore"
  case vhrLearnCompleteTime = "vhrLearnCompleteTime"
  case vhrLearnMorePoints = "vhrLearnMorePoints"
  case vhrLearnVitalityAge = "vhrLearnVitalityAge"
  case vhrOnboardingCompleteTime = "vhrOnboardingCompleteTime"
  case vhrOnboardingEarnPoints = "vhrOnboardingEarnPoints"
  case vhrOnboardingVitalityAge = "vhrOnboardingVitalityAge"
  case vhrSectionAlcoholIntake = "vhrSectionAlcoholIntake"
  case vhrSectionEatingHabits = "vhrSectionEatingHabits"
  case vhrSectionFamilyMedical = "vhrSectionFamilyMedical"
  case vhrSectionKeyMeasures = "vhrSectionKeyMeasures"
  case vhrSectionMedicalHistory = "vhrSectionMedicalHistory"
  case vhrSectionOverall = "vhrSectionOverall"
  case vhrSectionPhysicalActivity = "vhrSectionPhysicalActivity"
  case vhrSectionSleepingPatterns = "vhrSectionSleepingPatterns"
  case vhrSectionSmoking = "vhrSectionSmoking"
  case vhrSectionWellbeing = "vhrSectionWellbeing"

  var image: Image {
    return Image(asset: self)
  }
}
// swiftlint:enable type_body_length

internal extension UIImage {
    @available(*, deprecated, message: "Use VIAAssessments.{image}.image or VIAAssessments.{image}.templateImage instead")
    class func templateImage(asset: ASSAsset) -> UIImage? {
        let bundle = Bundle(for: ASSImagesPlaceholder.self) // placeholder declared at top of file
        let image = UIImage(named: asset.rawValue, in: bundle, compatibleWith: nil)?.withRenderingMode(.alwaysTemplate)
        return image
    }
}

internal extension Image {
    @available(*, deprecated, message: "Use VIAAssessments.{image}.image or VIAAssessments.{image}.templateImage instead")
    convenience init!(asset: ASSAsset) {
        let bundle = Bundle(for: ASSImagesPlaceholder.self) // placeholder declared at top of file
        self.init(named: asset.rawValue, in: bundle, compatibleWith: nil)
    }
}
