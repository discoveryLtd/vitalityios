//
//  MWBAssessmentNavigationBar.swift
//  VitalityActive
//
//  Created by Michelle R. Oratil on 14/02/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import UIKit
import VIAUIKit

class MWBAssessmentNavigationBar: UINavigationBar {
    
    lazy var mainStackView: UIStackView = {
        let view = UIStackView()
        view.spacing = 22
        view.axis = .vertical
        view.alignment = .center
        view.distribution = UIStackViewDistribution.fill
        self.addSubview(view)
        return view
    }()
    
    lazy var secondaryStackView: UIStackView = {
        let view = UIStackView()
        view.spacing = 10
        view.axis = .horizontal
        view.alignment = .center
        return view
    }()
    
    lazy var sectionLabel: UILabel = {
        let view = UILabel()
        view.font = .title3Font()
        view.textColor = .night()
        return view
    }()
    
    lazy var sectionImageView: UIImageView = UIImageView()
    
    lazy var lineView: VIASegmentedLineView = VIASegmentedLineView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setup()
    }
    
    func setup() {
        //Xcode 9
        if #available(iOS 11.0, *) {
            self.setTitleVerticalPositionAdjustment(0, for: .default)
        } else {
            self.setTitleVerticalPositionAdjustment(-90, for: .default)
        }
        //Xcode 9
        //self.setTitleVerticalPositionAdjustment(-90, for: .default)

    }
    
    public func configure(sectionTitle: String?, sectionIcon: UIImage?, totalSections: Int, currentSection: Int) {
        lineView.items = totalSections
        lineView.activeItem = currentSection
        lineView.setNeedsDisplay()
        if let image = sectionIcon {
            sectionImageView.image = image
            sectionImageView.tintColor = UIColor.knowYourHealthGreen()
        }
        sectionLabel.text = sectionTitle
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        // main
        mainStackView.snp.removeConstraints()
        mainStackView.snp.makeConstraints { (make) in
            //Xcode 9
            if #available(iOS 11.0, *) {
                make.top.equalTo(55)
            }
            //Xcode 9
            make.left.right.equalToSuperview()
            make.centerY.equalToSuperview().offset(15)
        }
        
        // line
        mainStackView.removeArrangedSubview(lineView)
        mainStackView.addArrangedSubview(lineView)
        lineView.snp.removeConstraints()
        lineView.snp.makeConstraints { (make) in
            make.width.equalToSuperview().multipliedBy(0.95)
            make.height.equalTo(20)
        }
        
        // secondary stackview
        mainStackView.removeArrangedSubview(secondaryStackView)
        mainStackView.addArrangedSubview(secondaryStackView)
        secondaryStackView.snp.removeConstraints()
        secondaryStackView.snp.makeConstraints { (make) in
            make.width.lessThanOrEqualToSuperview().multipliedBy(0.95)
        }
        
        // image
        secondaryStackView.removeArrangedSubview(sectionImageView)
        secondaryStackView.addArrangedSubview(sectionImageView)
        sectionImageView.snp.removeConstraints()
        sectionImageView.snp.makeConstraints { (make) in
            make.width.height.equalTo(25)
        }
        
        // label
        secondaryStackView.removeArrangedSubview(sectionLabel)
        secondaryStackView.addArrangedSubview(sectionLabel)
        
        //FIXME: Revisit this to not manipulate internal classes
        for subview in subviews {
            let name = String(describing: type(of: subview))
            //Xcode 9
            if #available(iOS 11.0, *) {
                let window = UIApplication.shared.keyWindow
                let topPadding = window?.safeAreaInsets.top
                if subview is UIStackView || name == "_UIBarBackground" {
                    var frame = subview.frame
                    frame.size.height = 150 + topPadding!
                    subview.frame = frame
                }
            } else {
                if subview is UIStackView || name == "_UIBarBackground" {
                } else {
                    var frame = subview.frame
                    frame.origin.y = 10
                    subview.frame = frame
                }
            }
            //Xcode 9
            //if subview is UIStackView || name == "_UIBarBackground" {
            //  } else {
            //    var frame = subview.frame
            //    frame.origin.y = 10
            //    subview.frame = frame
            //}
        }
    }
    
    override func sizeThatFits(_ size: CGSize) -> CGSize {
        let superSize = super.sizeThatFits(size)
        return CGSize(width: superSize.width, height: 135)
    }
}
