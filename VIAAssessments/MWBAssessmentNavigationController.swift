//
//  MWBAssessmentNavigationController.swift
//  VitalityActive
//
//  Created by Michelle R. Oratil on 14/02/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import UIKit

class MWBAssessmentNavigationController: UINavigationController {
    
    // MARK: Properties
    
    var sectionTitle: String? {
        didSet {
            if let navBar = self.navigationBar as? MWBAssessmentNavigationBar {
                navBar.sectionLabel.text = sectionTitle
            }
        }
    }
    
    var sectionIcon: UIImage?
    
    var totalSections = 1
    
    var currentSection = 0
    
    // MARK: Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    deinit {
        debugPrint("MWBAssessmentNavigationController deinit")
    }
    
    public func configureNavigationBar() {
        if let navBar = self.navigationBar as? MWBAssessmentNavigationBar {
            navBar.configure(sectionTitle: sectionTitle, sectionIcon: sectionIcon, totalSections: totalSections, currentSection: currentSection)
        }
    }
    
}
