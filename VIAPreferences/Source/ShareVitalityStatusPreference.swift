import Foundation

import VIAUtilities
import VitalityKit

public class ShareVitalityStatusPreference: PreferenceDataObject {
    public weak var delegate: PreferenceObjectDelegate?
    fileprivate let realm = DataProvider.newRealm()
    fileprivate let username = AppSettings.getLastLoginUsername()
    
    public override init() {
        super.init()
        
        self.preferenceImage = VIAPreferencesAsset.Privacy.vitalityStatus.templateImage
        self.preferenceTitle = CommonStrings.CommunicationPrefTitleStatus375
        self.preferencesDetail = CommonStrings.CommunicationPrefMessageStatus376
        self.preferencesSwitchAction = self.toggleVitalityStatusPref
    }
    
    public override func preferenceInitialState() -> Bool {
        let type = PreferenceTypeRef.ShareStatusWithEmp.rawValue
        if let shareStatusPreference = realm.partyGeneralPreference(with: type), shareStatusPreference.value == "true" {
            self.preferenceState = true
            return self.preferenceState
        }
        self.preferenceState = false
        return self.preferenceState
    }
    
    public func toggleVitalityStatusPref(sender: UISwitch?) {
        if (sender!.isOn) {
            self.setShareVitalityStatusPreference(currentSwitchState: true)
        } else if (sender!.isOn == false) {
            self.setShareVitalityStatusPreference(currentSwitchState: false)
        }
    }
    
    //Temporarily retained, will remove if there are no future uses
    func setUsernameForShareVitalityStatus(sender: UISwitch) {
        
        let user = AppSettings.getLastLoginUsername()
        
        if !(user.isEmpty) {
            self.preferenceState = true
            AppSettings.setUsernameForShareVitalityStatus(user)
        } else {
            self.preferenceState = false
            sender.setOn(false, animated: true)
        }
    }
    
    //Temporarily retained, will remove if there are no future uses
    func removeUsernameForShareVitalityStatus() {
        self.preferenceState = false
        AppSettings.removeUsernameForShareVitalityStatus()
    }
    
    func setShareVitalityStatusPreference(currentSwitchState: Bool) {
        
        let request = UpdatePartyRequest(partyId: DataProvider.newRealm().getPartyId())
        request.generalPreference = UpdatePartyGeneralPreference(effectiveFrom: Date(), effectiveTo: Date.distantFuture, typeKey: PreferenceTypeRef.ShareStatusWithEmp, value: String(currentSwitchState))
        
        Wire.Party.update(request: request) { (error) in
            if (error != nil) {
                //Revert switch to previous state
                self.preferenceState = !currentSwitchState
                self.delegate?.setPreferenceState(active: self.preferenceState)
            } else {
                let realm = DataProvider.newRealm()
                try! realm.write {
                    let type = PreferenceTypeRef.ShareStatusWithEmp.rawValue
                    if let shareStatusPref = realm.partyGeneralPreference(with: type) {
                        realm.delete(shareStatusPref)
                    }
                    
                    let shareStatusGeneralPreference = PartyGeneralPreference()
                    shareStatusGeneralPreference.type = type
                    if (currentSwitchState == true) {
                        shareStatusGeneralPreference.value = "true"
                    } else {
                        shareStatusGeneralPreference.value = "false"
                    }
                    realm.add(shareStatusGeneralPreference)
                }
                self.preferenceState = currentSwitchState
                self.delegate?.setPreferenceState(active: self.preferenceState)
            }
        }
    }
}
