import Foundation

import VitalityKit
import VIACommon

public class RememberMePreference: PreferenceDataObject {
    fileprivate let realm = DataProvider.newRealm()
    fileprivate let username = AppSettings.getLastLoginUsername()

    public override init() {
        super.init()
        self.preferenceImage = VIAPreferencesAsset.PreferencesOnboarding.setupRememberMe.templateImage
        self.preferenceTitle = CommonStrings.UserPrefs.RememberMeToggleTitle81
        self.preferencesDetail = CommonStrings.UserPrefs.RememberMeToggleMessage82
        self.preferencesSwitchAction = self.toggleRememberMe
        
        
        // This if statement is to check if it is a UKE Market
        if VIAApplicableFeatures.default.rememberMePreferenceDefault!{
            /**
             Check if the app has set a user to remember for the rememberME in its first log in.
             If it was not set, then the app set the user for remember me..
             This will make the rememberMe Preference set to be TRUE in its first log-in.
             */
            if !AppSettings.hasSetFirstLoginUsernameForRememberMe(){
                AppSettings.setUsernameForRememberMe(AppSettings.getLastLoginUsername())
                AppSettings.setFirstLoginUsernameForRememberMe(AppSettings.getLastLoginUsername())
            }
        }else{
            if(self.preferenceState){
                AppSettings.setUsernameForRememberMe(AppSettings.getLastLoginUsername())
            }
        }
        
    }

    public override func preferenceInitialState() -> Bool {
        
        /** This will set the value of the rememberME preference. It the user has set it to true before he log out,
         then when he return it will be true. The same thing happen if the user set the preference to false.
         */
        self.preferenceState = AppSettings.usernameForRememberMe() != nil && AppSettings.usernameForRememberMe() == username
        return preferenceState
    }

    public func toggleRememberMe(sender: UISwitch) {
        if (sender.isOn) {
            self.setUsernameForRememberMe(sender: sender)
        } else {
            self.removeUsernameForRememberMe()
        }
    }

    func setUsernameForRememberMe(sender: UISwitch) {
        AppSettings.setUsernameForRememberMe(AppSettings.getLastLoginUsername())
        sender.setOn(true, animated: true)
    }

    func removeUsernameForRememberMe() {
        self.preferenceState = false
        AppSettings.removeUsernameForRememberMe()
    }
}
