import Foundation
import LocalAuthentication

import VitalityKit
import VIACommon

import SwiftKeychainWrapper
import RNCryptor


public protocol touchIdDelegate {
    func reloadScreen()
    func reloadSecurity()
    func promptInvalidSecurityDetails(errorMessage: String, attempts:Int)
}

public class TouchIdPreference: PreferenceDataObject {
    public var touchIdDelegate: touchIdDelegate?
    fileprivate var passwordAttempt:Int = 0
    
    fileprivate let realm = DataProvider.newRealm()
    fileprivate let username = AppSettings.usernameForTouchId() != nil ? AppSettings.usernameForTouchId() : AppSettings.getLastLoginUsername()
    //    var valid: Bool?
    fileprivate var passwordProvided = ""
    
    //Get secured data from Keychain
    fileprivate let encryptionKeyFromKeychain = KeychainWrapper.standard.string(forKey: "encryptionKey")
    fileprivate let encryptedPasswordFromKeychain = KeychainWrapper.standard.data(forKey: "encryptedPassword")
    
    public override init() {
        super.init()
    
        self.preferenceImage = VIAPreferencesAsset.PreferencesOnboarding.setupTouchID.templateImage
        self.preferenceTitle = CommonStrings.UserPrefs.TouchidToggleTitle79
        self.preferencesDetail = CommonStrings.UserPrefs.TouchidToggleMessage80
        self.preferencesSwitchAction = self.toggleTouchId
    }
    
    public override func preferenceInitialState() -> Bool {
        self.preferenceState = AppSettings.usernameForTouchId() != nil && AppSettings.usernameForTouchId() == username
        return preferenceState
    }
    
    public func toggleTouchId(sender: UISwitch) {
        if (sender.isOn) {
            self.touchIdToogled(sender: sender)
        } else {
            //            self.valid = false
            self.disableTouchId()
            self.touchIdDelegate?.reloadScreen()
        }
    }
    
    public func activateTouchId(sender: UISwitch) {
        self.preferenceState = true
        AppSettings.setUsernameForTouchId(AppSettings.getLastLoginUsername())
        sender.setOn(true, animated: true)
    }
    
    public func disableTouchId() {
        self.preferenceState = false
        AppSettings.removeUsernameForTouchId()
        AppSettings.removeTouchIdDomainState()
    }
    
    public func touchIdToogled(sender: UISwitch) {
        // 1. Create a authentication context
        let authenticationContext = LAContext()
        var authError:NSError?
        
        // 2. Check if the device has a fingerprint sensor
        // To be implemented, out of scope
        
        // 3. Check the fingerprint
        if authenticationContext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &authError) {
            authenticationContext.evaluatePolicy(
                .deviceOwnerAuthenticationWithBiometrics,
                localizedReason: CommonStrings.Login.Touchid.AuthenticateTouchid.Prompt2440,
                reply: {success, error in
                    
                    /* Must run in UI Thread */
                    DispatchQueue.main.async {
                        if success {
                            
                            // Fingerprint recognized, prompt alertview asking for user's Vitality Password
                            self.enterVitalityPassword(touchIdDomainState: authenticationContext.evaluatedPolicyDomainState, sender: sender)
                        } else {
                            
                            // Based on the error code, will display a different error message. For this scode only incorrect fingerprint will be handled that will be triggered after the third incorrect attept as per Local Authentication documentation
                            self.touchIdDelegate?.reloadSecurity()
                            self.displayErrorMessage(error: error as! LAError )
                            sender.setOn(false, animated: true)
                        }
                    }
            })
        }else{
            sender.setOn(false, animated: true)
        }
    }
    
    public func loginWithTouchIdUpdateState(shouldUpdateState : Bool, completion: @escaping (_ success : Bool, _ error : Error?) -> Void) {
        
        let authenticationContext = LAContext()
        
        authenticationContext.localizedFallbackTitle = ""
        
        let localizedReason = Bundle.main.object(forInfoDictionaryKey: "VIAAuthenticateTouchID") as? String ?? "Authenticate with Touch ID"
        
        authenticationContext.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics,
                                             localizedReason: localizedReason)
        { (success, error) in
            
            if success {
                
                completion(success, nil)
            }
                
            else {
                
                completion(false, error)
            }
        }
    }
    
    public func updateTouchIdEvaluationState() {
        
        let authenticationContext = LAContext()
        var authError:NSError?
        
        authenticationContext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error:&authError)
        
        if authError == nil {
            AppSettings.updateTouchIdDomainState(authenticationContext.evaluatedPolicyDomainState!)
        }
    }
    
    public func touchIdIsEnabled() -> Bool {
        
        let authenticationContext = LAContext()
        
        return authenticationContext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error:nil)
    }
    
    public func touchIdDomainStateChanged() -> Bool {
        
        let authenticationContext = LAContext()
        var authError:NSError?
        
        if authenticationContext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &authError) {
            
            if let oldDomainState = AppSettings.touchIdDomainState(), oldDomainState != authenticationContext.evaluatedPolicyDomainState {
                
                return true
            }
                
            else {
                
                return false
            }
        }
        
        return false
    }
    
    func enterVitalityPassword(touchIdDomainState: Data?, sender: UISwitch){
        let alert = UIAlertController(title: CommonStrings.Settings.AlertPasswordRequiredTitle931, message: CommonStrings.Settings.AlertPasswordRequiredMessage942, preferredStyle: .alert)
        let confirmAction = UIAlertAction(title: CommonStrings.OkButtonTitle40, style: UIAlertActionStyle.default, handler: { (UIAlertAction) in
            
            // Codes for validating user password will be placed here
            self.validatePassword(passwordProvided: (alert.textFields?[0].text)!, touchIdDomainState: touchIdDomainState!, sender: sender)
            
        })
        let cancelAction = UIAlertAction(title: CommonStrings.CancelButtonTitle24, style: UIAlertActionStyle.default, handler: { (UIAlertAction) in
            self.disableTouchId()
            self.touchIdDelegate?.reloadScreen()
        })
        
        alert.addTextField { (textField) in
            textField.placeholder = CommonStrings.PasswordFieldPlaceholder19
            textField.isSecureTextEntry = true
        }
        
        alert.view.tintColor = UIButton().tintColor
        alert.addAction(cancelAction)
        alert.addAction(confirmAction)
        UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true, completion: nil)
    }
    
    func authenticationFailed() {
        let title = CommonStrings.Settings.AlertFingerprintNotRecognisedTitle940
        let message = CommonStrings.Settings.AlertFingerprintNotRecognisedMessage941
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: CommonStrings.SettingsButton271, style: UIAlertActionStyle.cancel, handler: { (UIAlertAction) in
//            Rejected by Apple
//            if let url = URL(string: "App-Prefs:root=TOUCHID_PASSCODE") {
//                UIApplication.shared.openURL(url)
//            }
            
            guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
                return
            }
            if UIApplication.shared.canOpenURL(settingsUrl)  {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                    })
                }
                else  {
                    UIApplication.shared.openURL(settingsUrl)
                }
            }
        })
        let continueAction = UIAlertAction(title: CommonStrings.OkButtonTitle40, style: UIAlertActionStyle.default, handler: { (UIAlertAction) in
            self.disableTouchId()
        })
        
        alert.view.tintColor = UIButton().tintColor
        alert.addAction(continueAction)
        alert.addAction(cancelAction)
        UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true, completion: nil)
    }
    
    func displayErrorMessage(error:LAError) {
        var message = ""
        switch error.code {
        case LAError.authenticationFailed:
            self.authenticationFailed()
            self.disableTouchId()
            message = "Authentication was not successful because the user failed to provide valid credentials."
            break
        case LAError.userCancel:
            self.disableTouchId()
            message = "Authentication was canceled by the user"
            break
        case LAError.userFallback:
            self.disableTouchId()
            message = "Authentication was canceled because the user tapped the fallback button"
            break
        case LAError.touchIDNotEnrolled:
            self.disableTouchId()
            message = "Authentication could not start because Touch ID has no enrolled fingers."
        case LAError.passcodeNotSet:
            self.disableTouchId()
            message = "Passcode is not set on the device."
            break
        case LAError.systemCancel:
            self.disableTouchId()
            message = "Authentication was canceled by system"
            break
        default:
            break
        }
        NSLog(message)
    }
    
    func toggleRememberMe() {
        AppSettings.setUsernameForRememberMe(username!)
    }
    
    public func returnDecryptedPassword()-> String {
        
        let decryptedPassword = self.decryptPassword(password: self.encryptedPasswordFromKeychain!, encryptionKey: self.encryptionKeyFromKeychain!)
        
        return decryptedPassword
    }
    
    func decryptPassword(password: Data, encryptionKey: String) -> String {
        do {
            let decrypted = try RNCryptor.decrypt(data: password, withPassword: encryptionKey)
            let converted = NSString(data: decrypted, encoding: String.Encoding.utf8.rawValue)! as String
            return converted
        } catch {
            print(error)
        }
        return "Error Encountered"
    }
    
    func validatePassword(passwordProvided: String, touchIdDomainState: Data, sender: UISwitch){
        let decryptedPassword = self.decryptPassword(password: self.encryptedPasswordFromKeychain!, encryptionKey: self.encryptionKeyFromKeychain!)
        
        //Validate password provided
        if (passwordProvided == decryptedPassword) {
            self.passwordAttempt = 0
            
            AppSettings.setTouchIdDomainState(touchIdDomainState)
            self.toggleRememberMe()
            self.activateTouchId(sender: sender)
            self.touchIdDelegate?.reloadScreen()
        } else {
            self.passwordAttempt += 1
            if VIAApplicableFeatures.default.touchIdValidatePassword() {
                /* Call Login API to validate */
                self.performAccountValidation(password: passwordProvided) { (error) in
                    guard error == nil else {
                        self.performActionOnFailedAttempt()
                        return
                    }
                }
            } else {
                performActionOnFailedAttempt()
            }
        }
    }
    
    func performActionOnFailedAttempt() {
        self.disableTouchId()
        self.touchIdDelegate?.reloadScreen()
        self.touchIdDelegate?.promptInvalidSecurityDetails(errorMessage: CommonStrings.Login.RememberMe.IncorrectPasswordErrorAlertMessage63, attempts: self.passwordAttempt)
    }
    
    func performAccountValidation(password: String, completion: @escaping ((_ error: Error?) -> Void)) {
        let currentEnvironmentPrefix = Wire.default.currentEnvironment.prefix()
        let email = currentEnvironmentPrefix + AppSettings.getLastLoginUsername()
        
        DataProvider.reset()
        Wire.Member.login(email: email, password: password, completion: { error, url in
            completion(error)
        })
    }
}
