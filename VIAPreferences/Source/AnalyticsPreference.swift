import Foundation

import VitalityKit
import VIACommon

import RealmSwift

public class AnalyticsPreference: PreferenceDataObject {
    let realm = DataProvider.newRealm()

    public override init() {
        super.init()
        self.preferenceImage = VIAPreferencesAsset.PreferencesOnboarding.setupAnalytics.templateImage
        self.preferenceTitle = CommonStrings.UserPrefs.AnalyticsToggleTitle73
        self.preferencesDetail = CommonStrings.UserPrefs.AnalyticsToggleMessage74
        self.preferencesSwitchAction = self.toggleAnalyticsPreference
    }

    public override func preferenceInitialState() -> Bool {
        if (AppSettings.getAnalyticsPreference() == nil) {
            AppSettings.addAnalyticsPreference()
        }

        if let permissionAllowed = AppSettings.getAnalyticsPreference() {
            self.preferenceState = permissionAllowed
            return self.preferenceState
        } else {
            self.preferenceState = false
            return self.preferenceState
        }
    }

    public func toggleAnalyticsPreference(sender: UISwitch) {
        if (sender.isOn) {
            self.preferenceState = true
            AppSettings.addAnalyticsPreference()
        } else {
            self.preferenceState = false
            AppSettings.removedAnalyticsPreference()
        }
    }
}
