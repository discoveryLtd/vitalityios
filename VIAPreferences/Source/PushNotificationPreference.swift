import Foundation
import UserNotifications

import VitalityKit
import VIACommon

public class PushNotificationPreferenceObject: PreferenceDataObject {
    fileprivate let realm = DataProvider.newRealm()
    fileprivate let username = AppSettings.getLastLoginUsername()
    public var toggleStatus: Bool?

    public weak var delegate: PreferenceObjectDelegate? {
        didSet {
            if let applyPushNotifToggle = VIAApplicableFeatures.default.applyPushNotifToggle, applyPushNotifToggle{
                self.toggleStatus = false
            }else{
                self.toggleStatus = true
            }
            
            switch self.status {
            case .authorized :
                self.delegate?.setPreferenceEnabled(active: self.toggleStatus!)
                self.delegate?.setPreferenceState(active: true)
                self.delegate?.disableActionButton(active: false)
            case .denied :
                self.delegate?.setPreferenceEnabled(active: self.toggleStatus!)
                self.delegate?.setPreferenceState(active: false)
                self.delegate?.disableActionButton(active: false)
            case .notDetermined:
                self.delegate?.setPreferenceEnabled(active: self.toggleStatus!)
                self.delegate?.setPreferenceState(active: false)
                self.delegate?.disableActionButton(active: true)
            default:
                self.delegate?.setPreferenceEnabled(active: self.toggleStatus!)
                self.delegate?.setPreferenceState(active: false)
                self.delegate?.disableActionButton(active: true)
            }
        }
    }

    public var status: UNAuthorizationStatus = .notDetermined {
        didSet {
            switch self.status {
            case .authorized :
                self.preferenceState = true
                self.setUsernameForNotifications()
            case .denied :
                self.preferenceState = false
            case .notDetermined:
                self.preferenceState = false
            default:
                self.preferenceState = false
            }
        }
    }

    public override init() {
        super.init()
        self.query()
        self.preferenceImage = VIAPreferencesAsset.PreferencesOnboarding.setupNotifications.templateImage
        self.preferenceTitle = CommonStrings.UserPrefs.PushMessageToggleTitle67
        self.preferencesDetail = CommonStrings.UserPrefs.PushMessageToggleMessage68
        self.preferencesSwitchAction = self.toggle
        self.setupActionButton()
    }

    public override func preferenceInitialState() -> Bool {
        // NOTE: Temporary implementation
        self.preferenceState = AppSettings.usernameForNotifications() != nil && AppSettings.usernameForNotifications() == username
        return preferenceState
        //return self.preferenceState
    }

    func setupActionButton() {
        self.preferenceActionButton = PreferenceActionButton()
        self.preferenceActionButton?.preferencesActionButtonTitle = CommonStrings.UserPrefs.PushMessageToggleSettingsLinkButtonTitle69
        self.preferenceActionButton?.preferencesButtonAction = {
            self.openSettingsURL()
        }
    }

    public func toggle(sender: UISwitch?) {
        // NOTE: Temporary implementation
        if (sender?.isOn)! {
            self.setUsernameForNotifications()
            sender?.setOn(true, animated: true)
        } else {
            self.removeUsernameForNotifications()
        }
//        if (sender!.isOn) {
//            request()
//        }
    }

    func setUsernameForNotifications() {
        let email = AppSettings.getLastLoginUsername()
        
        if !(email.isEmpty) {
            self.preferenceState = true
            AppSettings.setUsernameForNotifications(email)
        } else {
            self.preferenceState = false
        }
    }
    
    func removeUsernameForNotifications() {
        self.preferenceState = false
        AppSettings.removeUsernameForNotifications()
    }
    
    func request() {
        PushNotifications.requestPushNotificationPermission(completion: { granted in
            self.query()
        })
    }

    func query() {
        let center = UNUserNotificationCenter.current()
        center.getNotificationSettings { [weak self] (settings) in
            DispatchQueue.main.sync {
                self?.status = settings.authorizationStatus
                self?.preferencesSwitchCompletion()
            }
        }
    }

    func openSettingsURL() {
        guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
            debugPrint("Could not open app")
            return ()
        }

        if UIApplication.shared.canOpenURL(settingsUrl) {
            UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                print("Settings opened: \(success)") // Prints true
            })
        }
    }
}

class PushNotifications {
    
    public class func requestPushNotificationPermission(completion: @escaping (Bool) -> Void) {
        if #available(iOS 10.0, *) {
            let center = UNUserNotificationCenter.current()
            center.delegate = UIApplication.shared.delegate as? UNUserNotificationCenterDelegate
            center.requestAuthorization(options: [.sound, .alert, .badge]) { (granted, error) in
                if error == nil {
                    UIApplication.shared.registerForRemoteNotifications()
                } else {
                    debugPrint("Could not register for notifications")
                }
                completion(granted)
            }
        } else {
            UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.sound, .alert, .badge], categories: nil))
            UIApplication.shared.registerForRemoteNotifications()
        }
    }
    
}

