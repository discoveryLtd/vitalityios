import Foundation

public enum OnboardingPreferencesCategories: Int {
    case communicationPreferences = 0
    case privacyPreferences = 1
    case securityPreferences = 2
}

public enum UserPreferenceHeaderType {
    case communication
    case privacy
    case security
}

public class PreferenceHeader {
    public var preferenceTitle: String?
    public var preferencesDetail: String?
    public var preferenceActionButton: PreferenceActionButton?
    public var type: UserPreferenceHeaderType?
    
    public init(){}
}

public class PreferenceDataObject {
    public var preferenceImage: UIImage?
    public var preferenceTitle: String?
    public var preferencesDetail: String?
    public func preferenceInitialState() -> Bool {return false}
    public var preferenceInitialActive: Bool = true
    public var preferencesSwitchAction:((_ sender: UISwitch) -> (Void)) = {_ in return}
    public var preferencesSwitchCompletion: (() -> Void) = { }
    public var preferenceActionButton: PreferenceActionButton?
    public var preferenceState: Bool = false
}

public protocol PreferenceObjectDelegate: class {
    func setPreferenceState(active: Bool)
    func setPreferenceEnabled(active: Bool)
    func disableActionButton(active: Bool)
}

public class PreferenceActionButton {
    public var preferencesActionButtonTitle: String?
    public var preferencesButtonAction:() -> (Void)? = {return}
    
    public init(){}
}
