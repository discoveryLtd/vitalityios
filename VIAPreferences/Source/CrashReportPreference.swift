import Foundation

import VitalityKit
import VIACommon

import RealmSwift

public class CrashReportPreference: PreferenceDataObject {
    let realm = DataProvider.newRealm()
    
    public override init() {
        super.init()
        self.preferenceImage = VIAPreferencesAsset.PreferencesOnboarding.setupReports.templateImage
        self.preferenceTitle = CommonStrings.UserPrefs.CrashReportsToggleTitle75
        self.preferencesDetail = CommonStrings.UserPrefs.CrashReportsToggleMessage76            
        self.preferencesSwitchAction = self.toggleCrashReportPreference
    }

    public override func preferenceInitialState() -> Bool {
        if (AppSettings.getCrashReportPreference() == nil) {
            AppSettings.addCrashReportPreference()
        }

        if let permissionAllowed = AppSettings.getCrashReportPreference() {
            self.preferenceState = permissionAllowed
            return preferenceState
        } else {
            self.preferenceState = false
            return self.preferenceState
        }
    }

    public func toggleCrashReportPreference(sender: UISwitch) {
        if (sender.isOn) {
            preferenceState = true
            AppSettings.addCrashReportPreference()
        } else {
            preferenceState = false
            AppSettings.removedCrashReportPreference()
        }
    }
}
