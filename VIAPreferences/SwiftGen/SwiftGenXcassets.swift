// Generated using SwiftGen, by O.Halligon — https://github.com/SwiftGen/SwiftGen

#if os(OSX)
import AppKit.NSImage
public typealias VIAPreferencesColor = NSColor
public typealias VIAPreferencesImage = NSImage
#elseif os(iOS) || os(tvOS) || os(watchOS)
import UIKit.UIImage
public typealias VIAPreferencesColor = UIColor
public typealias VIAPreferencesImage = UIImage
#endif

// swiftlint:disable file_length

public typealias VIAPreferencesAssetType = VIAPreferencesImageAsset

public struct VIAPreferencesImageAsset: Equatable {
	fileprivate var name: String

	public var templateImage: VIAPreferencesImage {
		let bundle = Bundle(for: BundleToken.self)
		#if os(iOS) || os(tvOS)
		let image = VIAPreferencesImage(named: name, in: bundle, compatibleWith: nil)?.withRenderingMode(.alwaysTemplate)
		#elseif os(OSX)
		let image = bundle.image(forResource: name).withRenderingMode(.alwaysTemplate)
		#elseif os(watchOS)
		let image = VIAPreferencesImage(named: name).withRenderingMode(.alwaysTemplate)
		#endif
		guard let result = image else { fatalError("Unable to load template image named \(name).") }
		return result
	}

	public var image: VIAPreferencesImage {
		let bundle = Bundle(for: BundleToken.self)
		#if os(iOS) || os(tvOS)
		let image = VIAPreferencesImage(named: name, in: bundle, compatibleWith: nil)
		#elseif os(OSX)
		let image = bundle.image(forResource: name)
		#elseif os(watchOS)
		let image = VIAPreferencesImage(named: name)
		#endif
		guard let result = image else { fatalError("Unable to load image named \(name).") }
		return result
	}

	public static func ==(lhs: VIAPreferencesImageAsset, rhs: VIAPreferencesImageAsset) -> Bool {
        return lhs.image == rhs.image
    }
}

public struct VIAPreferencesColorAsset {
fileprivate var name: String

#if swift(>=3.2)
@available(iOS 11.0, tvOS 11.0, watchOS 4.0, OSX 10.13, *)
var color: VIAPreferencesColor {
return VIAPreferencesColor(asset: self)
}
#endif
}

// swiftlint:disable identifier_name line_length nesting type_body_length type_name
public enum VIAPreferencesAsset {
  public enum Privacy {
    public static let vitalityStatus = VIAPreferencesImageAsset(name: "vitalityStatus")
  }
  public enum PreferencesOnboarding {
    public static let setupNotifications = VIAPreferencesImageAsset(name: "setupNotifications")
    public static let setupEmail = VIAPreferencesImageAsset(name: "setupEmail")
    public static let touchIdIcon = VIAPreferencesImageAsset(name: "touchIdIcon")
    public static let setupRememberMe = VIAPreferencesImageAsset(name: "setupRememberMe")
    public static let setupTouchID = VIAPreferencesImageAsset(name: "setupTouchID")
    public static let setupAnalytics = VIAPreferencesImageAsset(name: "setupAnalytics")
    public static let setupReports = VIAPreferencesImageAsset(name: "setupReports")
  }

  // swiftlint:disable trailing_comma
  public static let allColors: [VIAPreferencesColorAsset] = [
  ]
  public static let allImages: [VIAPreferencesImageAsset] = [
    Privacy.vitalityStatus,
    PreferencesOnboarding.setupNotifications,
    PreferencesOnboarding.setupEmail,
    PreferencesOnboarding.touchIdIcon,
    PreferencesOnboarding.setupRememberMe,
    PreferencesOnboarding.setupTouchID,
    PreferencesOnboarding.setupAnalytics,
    PreferencesOnboarding.setupReports,
  ]
  // swiftlint:enable trailing_comma
  @available(*, deprecated, renamed: "allImages")
  public static let allValues: [VIAPreferencesAssetType] = allImages
}
// swiftlint:enable identifier_name line_length nesting type_body_length type_name

extension VIAPreferencesImage {
@available(iOS 1.0, tvOS 1.0, watchOS 1.0, *)
@available(OSX, deprecated,
message: "This initializer is unsafe on macOS, please use the VIAPreferencesImageAsset.image property")
convenience init!(asset: VIAPreferencesAssetType) {
#if os(iOS) || os(tvOS)
let bundle = Bundle(for: BundleToken.self)
self.init(named: asset.name, in: bundle, compatibleWith: nil)
#elseif os(OSX) || os(watchOS)
self.init(named: asset.name)
#endif
}
}

extension VIAPreferencesColor {
#if swift(>=3.2)
@available(iOS 11.0, tvOS 11.0, watchOS 4.0, OSX 10.13, *)
convenience init!(asset: VIAPreferencesColorAsset) {
let bundle = Bundle(for: BundleToken.self)
#if os(iOS) || os(tvOS)
self.init(named: asset.name, in: bundle, compatibleWith: nil)
#elseif os(OSX)
self.init(named: asset.name, bundle: bundle)
#elseif os(watchOS)
self.init(named: asset.name)
#endif
}
#endif
}

private final class BundleToken {}
