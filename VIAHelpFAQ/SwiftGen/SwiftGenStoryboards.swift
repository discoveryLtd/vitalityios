// Generated using SwiftGen, by O.Halligon — https://github.com/SwiftGen/SwiftGen

// swiftlint:disable sorted_imports
import Foundation
import UIKit
import VIACore

// swiftlint:disable file_length

protocol StoryboardType {
  static var storyboardName: String { get }
}

extension StoryboardType {
  static var storyboard: UIStoryboard {
    return UIStoryboard(name: self.storyboardName, bundle: Bundle(for: BundleToken.self))
  }
}

struct SceneType<T: Any> {
  let storyboard: StoryboardType.Type
  let identifier: String

  func instantiate() -> T {
    guard let controller = storyboard.storyboard.instantiateViewController(withIdentifier: identifier) as? T else {
      fatalError("ViewController '\(identifier)' is not of the expected class \(T.self).")
    }
    return controller
  }
}

struct InitialSceneType<T: Any> {
  let storyboard: StoryboardType.Type

  func instantiate() -> T {
    guard let controller = storyboard.storyboard.instantiateInitialViewController() as? T else {
      fatalError("ViewController is not of the expected class \(T.self).")
    }
    return controller
  }
}

protocol SegueType: RawRepresentable { }

extension UIViewController {
  func perform<S: SegueType>(segue: S, sender: Any? = nil) where S.RawValue == String {
    performSegue(withIdentifier: segue.rawValue, sender: sender)
  }
}

// swiftlint:disable explicit_type_interface identifier_name line_length type_body_length type_name
enum VIAHelpFAQStoryboard {
  enum Help: StoryboardType {
    static let storyboardName = "Help"

    static let initialScene = InitialSceneType<UINavigationController>(storyboard: Help.self)

    static let help = SceneType<UINavigationController>(storyboard: Help.self, identifier: "Help")

    static let viaHelpTopicViewController = SceneType<VIAHelpFAQ.VIAHelpTopicViewController>(storyboard: Help.self, identifier: "VIAHelpTopicViewController")

    static let viaHelpViewController = SceneType<VIAHelpFAQ.VIAHelpViewController>(storyboard: Help.self, identifier: "VIAHelpViewController")
  }
  enum TermsAndConditions: StoryboardType {
    static let storyboardName = "TermsAndConditions"

    static let initialScene = InitialSceneType<VIACore.VIANavigationViewController>(storyboard: TermsAndConditions.self)

    static let viaTermsConditionsNavigationViewController = SceneType<VIACore.VIANavigationViewController>(storyboard: TermsAndConditions.self, identifier: "VIATermsConditionsNavigationViewController")

    static let viaTermsConditionsViewController = SceneType<VIACore.VIATermsConditionsViewController>(storyboard: TermsAndConditions.self, identifier: "VIATermsConditionsViewController")
  }
}

enum StoryboardSegue {
  enum Help: String, SegueType {
    case showHelpTopic
  }
}
// swiftlint:enable explicit_type_interface identifier_name line_length type_body_length type_name

private final class BundleToken {}
