//
//  VIAHelpTopicViewController.swift
//  VitalityActive
//
//  Created by Val Tomol on 01/02/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import VIAUIKit
import VitalityKit
import VIACommon
import VIAUtilities

public class VIAHelpTopicViewController: VIATableViewController, UIWebViewDelegate {
    // MARK: Properties
    var topicContent: Question? = nil
    var filteredQuestions: Array<Question> = []
    var relatedHelpList: Array<Question> = []
    var topRelatedHelpList: Array<Question> = []
    var didToggleYes: Bool = false
    var htmlContentHeight: CGFloat = 0.0
    
    // Phrase app string tokens.
    let titleText: String = CommonStrings.Help.Detailed.PageTitle1423
    let relatedHelpHeaderText: String = CommonStrings.Help.Detailed.SectionHeaderTitle1424.localizedUppercase
    let questionText: String = CommonStrings.Help.Detailed.PromptMessage1425
    let yesButtonText: String = CommonStrings.Help.Detailed.ActionPositive1426
    let noButtonText: String = CommonStrings.Help.Detailed.ActionNegative1427
    let feedbackText: String = CommonStrings.Help.Detailed.PromptSummary1432
    let feedbackFooterText: String = CommonStrings.Help.Detailed.FooterContact1428
//    let feedbackFooterText: String = "Contact 0860 0000 3813 for more help."
    
    // MARK: View Lifecycles
    override public func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = titleText
    }
    
    override public func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        configureTableView()
        configureAppearance()
        
        processTopRelatedHelp(relatedHelpList)
    }
    
    override public func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if topicContent == nil {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    override public func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.hideBackButtonTitle()
    }
    
    public override func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    public override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let sectionValue = Sections(rawValue: section)
        if sectionValue == .topicCell {
            return 1
        } else if sectionValue == .relatedHelpCell {
            return topRelatedHelpList.count
        }
            // TODO: Hide for now. Show once API is available.
            //        else if sectionValue == .feedbackCell {
            //            return 1
            //        }
        else {
            return 0
        }
    }
}

// MARK: View Configuration
extension VIAHelpTopicViewController {
    fileprivate func configureAppearance() {
        navigationController?.makeNavigationBarTransparent()
    }
    
    fileprivate func configureTableView() {
        self.tableView.register(VIAWebViewCell.nib(), forCellReuseIdentifier: VIAWebViewCell.defaultReuseIdentifier)
        self.tableView.register(VIATableViewCell.nib(), forCellReuseIdentifier: VIATableViewCell.defaultReuseIdentifier)
        self.tableView.register(VIAYesNoQuestionCell.nib(), forCellReuseIdentifier: VIAYesNoQuestionCell.defaultReuseIdentifier)
        self.tableView.register(VIALabelTableViewCell.nib(), forCellReuseIdentifier: VIALabelTableViewCell.defaultReuseIdentifier)
        self.tableView.register(VIATableViewSectionHeaderFooterView.nib(), forHeaderFooterViewReuseIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier)
        self.tableView.estimatedRowHeight = 75
        self.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 30, right: 0)
    }
}

// MARK: Enums
extension VIAHelpTopicViewController {
    enum Sections: Int {
        case topicCell = 0
        case relatedHelpCell = 1
        case feedbackCell = 2
    }
}

// MARK: TableView DataSource and Delegate
extension VIAHelpTopicViewController {
    public override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        let sectionValue = Sections(rawValue: section)
        if sectionValue == .topicCell {
            return CGFloat.leastNonzeroMagnitude
        }
        return UITableViewAutomaticDimension
    }
    
    public override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let sectionValue = Sections(rawValue: section)
        if sectionValue == .relatedHelpCell && self.relatedHelpList.count != 0 {
            let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier) as? VIATableViewSectionHeaderFooterView
            view?.labelText = relatedHelpHeaderText
            return view
        } else {
            return nil
        }
    }
    
    public override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    public override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let sectionValue = Sections(rawValue: section)
        let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier) as? VIATableViewSectionHeaderFooterView
//        view?.labelText = feedbackFooterText
        
        if sectionValue == .topicCell &&
            self.relatedHelpList.count == 0 &&
            VIAApplicableFeatures.default.showHelpContactFooter! {
            view?.htmlString = feedbackFooterText
            return view
        } else if sectionValue == .relatedHelpCell &&
            self.relatedHelpList.count != 0 &&
            VIAApplicableFeatures.default.showHelpContactFooter! { // TODO: Replace with .feedbackCell once feedback row is shown.
            return view
        } else {
            return nil
        }
    }
    
    public override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let sectionValue = Sections(rawValue: indexPath.section)
        if sectionValue == .topicCell {
            return self.htmlContentHeight
        } else {
            return UITableViewAutomaticDimension
        }
    }
    
    public override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let sectionValue = Sections(rawValue: indexPath.section)
        if sectionValue == .topicCell {
            return topicViewCell(at: indexPath)
        } else if sectionValue == .relatedHelpCell {
            return retaledHelpViewCell(at: indexPath)
        }
            // TODO: Temporarily hid feedback row. Show once API is available.
            //        else if sectionValue == .feedbackCell {
            //            if didToggleYes {
            //                return feedbackViewCell(at: indexPath)
            //            } else {
            //                return feedbackQuestionViewCell(at: indexPath)
            //            }
            //        }
        else {
            return UITableViewCell()
        }
    }
    
    func topicViewCell(at indexPath: IndexPath) -> VIAWebViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: VIAWebViewCell.defaultReuseIdentifier, for: indexPath) as! VIAWebViewCell
        let htmlContentHeight = self.htmlContentHeight
        cell.webView.tag = indexPath.row
        cell.webView.delegate = self
        cell.loadHtmlContentWithString(self.formatHtml(topicContent?.qContent)!)
        cell.webView.frame = CGRect(x: 0, y: 0, width: cell.frame.size.width, height: htmlContentHeight)
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        return cell
    }
    
    func retaledHelpViewCell(at indexPath: IndexPath) -> VIATableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: VIATableViewCell.defaultReuseIdentifier, for: indexPath) as! VIATableViewCell
        cell.labelText = self.topRelatedHelpList[indexPath.row].qQuestion
        cell.cellImage = nil
        cell.accessoryType = .disclosureIndicator
        return cell
    }
    
    func feedbackQuestionViewCell(at indexPath: IndexPath) -> VIAYesNoQuestionCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: VIAYesNoQuestionCell.defaultReuseIdentifier, for: indexPath) as! VIAYesNoQuestionCell
        cell.setQuestionLabel(text: questionText)
        cell.setYesButton(title: yesButtonText)
        cell.setNoButton(title: noButtonText)
        cell.setYesButtonPressed { _ in
            cell.setYesSelected()
            self.didToggleYes = true
            self.tableView.reloadData()
            NSLog("DID PRESS YES BUTTON")
        }
        cell.setNoButtonPressed { _ in
            cell.setNoSelected()
            NSLog("DID PRESS NO BUTTON")
        }
        cell.selectionStyle = .none
        return cell
    }
    
    func feedbackViewCell(at indexPath: IndexPath) -> VIALabelTableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: VIALabelTableViewCell.defaultReuseIdentifier, for: indexPath) as! VIALabelTableViewCell
        cell.labelText = feedbackText
        cell.configure(font: UIFont.subheadlineSemiBoldFont(), color: UIColor.black)
        cell.accessoryType = .none
        cell.selectionStyle = .none
        return cell
    }
    
    override public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let sectionValue = Sections(rawValue: indexPath.section)
        if sectionValue == .relatedHelpCell {
            let viewController = storyboard?.instantiateViewController(withIdentifier: "VIAHelpTopicViewController") as! VIAHelpTopicViewController
            viewController.topicContent = self.topRelatedHelpList[indexPath.row]
            viewController.filteredQuestions = self.filteredQuestions
            var relatedHelp = self.filteredQuestions
            let selectedIndex = relatedHelp.index(where: {$0 === self.topRelatedHelpList[indexPath.row]})
            if selectedIndex != nil {
                relatedHelp.remove(at: selectedIndex!)
            }
            viewController.relatedHelpList = relatedHelp
            self.navigationController?.pushViewController(viewController, animated: true)
        }
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

// MARK: WebView Delegates

extension VIAHelpTopicViewController {
    public func webViewDidStartLoad(_ webView: UIWebView) {
        // Disable user interaction
        webView.stringByEvaluatingJavaScript(from: "document.documentElement.style.webkitUserSelect='none'")
        webView.stringByEvaluatingJavaScript(from: "document.documentElement.style.webkitTouchCallout='none'")
    }
    
    public func webViewDidFinishLoad(_ webView: UIWebView) {
        if (htmlContentHeight != 0.0) {
            return
        }
        
        webView.frame.size.height = 1
        htmlContentHeight = webView.sizeThatFits(.zero).height
        self.tableView.reloadRows(at: [IndexPath(item: webView.tag, section: Sections.topicCell.rawValue)], with: .automatic)
    }
}

// MARK: Custom Functions
extension VIAHelpTopicViewController {
    func processTopRelatedHelp(_ relatedHelp: Array<Question>) {
        let relatedHelpItems = 3
        if relatedHelp.count > relatedHelpItems {
            for item in 0 ... relatedHelpItems - 1 {
                topRelatedHelpList.append(relatedHelp[item])
            }
        } else {
            topRelatedHelpList = relatedHelp
        }
    }
    
    /* Currently not in use with the current implementation.
    public func didTapFeedbackFooter() {
        if let phoneCallURL:URL = URL(string: "tel:08600003813") {
            let application:UIApplication = UIApplication.shared
            if (application.canOpenURL(phoneCallURL)) {
                let alertController = UIAlertController(title: "MyApp", message: "Are you sure you want to call \n08600003813?", preferredStyle: .alert)
                let yesPressed = UIAlertAction(title: "Yes", style: .default, handler: { (action) in
                    application.openURL(phoneCallURL)
                })
                let noPressed = UIAlertAction(title: "No", style: .default, handler: { (action) in
                    
                })
                alertController.addAction(yesPressed)
                alertController.addAction(noPressed)
                present(alertController, animated: true, completion: nil)
            }
        }
    } */
    
    func formatHtml(_ content: String?) -> String? {
        guard let validContent = content else { return nil }
        return self.wrapperHtml().replacingOccurrences(of: "CONTENT_GOES_HERE", with: validContent)
    }
    
    func wrapperHtml() -> String {
        let bundle = Bundle(for: type(of: self))
        guard let path = bundle.path(forResource: "helpwebcontent", ofType: "html") else {
            debugPrint("File does not exist: webcontent.html")
            return ""
        }
        
        do {
            return try String(contentsOfFile: path, encoding: .utf8)
        } catch let error {
            debugPrint("Could not load html from file webcontent.html: \(error)")
            return ""
        }
    }
}
