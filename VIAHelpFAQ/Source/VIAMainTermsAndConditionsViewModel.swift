import Foundation
import UIKit

import VitalityKit



public class VIAMainTermsAndConditionsViewModel: TermsAndConditionsViewModel, CMSConsumer {

    private var realm = DataProvider.newRealm()

    public weak var delegate: VIATermsConditionsViewController?

    public var articleId: String? {
        return AppConfigFeature.contentId(for: .TermsAndConditionsContent)
    }

    private var agreeActionCallback: (()->Void)?
    public var agreeAction: TermsAndConditionsNavigation {
            return { [weak self] _, _ in
                self?.agreeTermsAndConditions(success: { [weak self] in
                    /*  Keeping it here for reference */
                    //self?.navigateToNextStep()
                    self?.agreeActionCallback?()
                })
            }
        }


    private var disagreeActionCallback: (()->Void)?
    public var disagreeAction :TermsAndConditionsNavigation {
        return { [weak self] _, _ in
            /*  Yeah, very terrible. I will refactor you soon so stay put. */
            // yeah this is all terrible, but it is what it is for now -wvh
            self?.delegate?.confirmDisagreeAndDisplayLogin(disagreeAction: {
                self?.disagreeTermsAndConditions(success: {
                    /*  Keeping it here for reference */
                    //self?.delegate?.onNavigateToLogin()
                    self?.disagreeActionCallback?()
                })
            })
        }
    }

    
    public convenience required init(agreeActionCallback: @escaping()->Void, disagreeActionCallback: @escaping()->Void){
        self.init()
        self.agreeActionCallback    = agreeActionCallback
        self.disagreeActionCallback = disagreeActionCallback
    }


    // MARK: Actions

    func handleErrorOccurred(error: Error?) {
        guard self.delegate != nil else { return }

        let controller = UIAlertController(title: CommonStrings.GenericServiceErrorTitle268,
                                           message: CommonStrings.GenericServiceErrorMessage269,
                                           preferredStyle: .alert)

        let ok = UIAlertAction(title: CommonStrings.OkButtonTitle40, style: .cancel) { action in
        }
        controller.addAction(ok)
        self.delegate?.present(controller, animated: true, completion: nil)
    }

    func agreeTermsAndConditions(success: @escaping (() -> Void)) {
        self.delegate?.showHUDOnWindow()
        //TODO: remove nullable objects

        guard let userInstrution = DataProvider.currentUserInstructionForLoginTermsAndConditions() else {
            self.delegate?.hideHUDFromWindow()
            self.handleErrorOccurred(error: nil)
            return
        }

        let partyId = DataProvider.newRealm().getPartyId()
        let tenantId = DataProvider.newRealm().getTenantId()
        Wire.Events.captureLoginEvents(tenantId: tenantId, partyId: partyId, loginSuccess: true, userInstructionResponseId: "\(userInstrution.id)", userInstructionResponseType: "\(userInstrution.typeKey)") { (error) in
            self.delegate?.hideHUDFromWindow()

            guard error == nil else {
                self.handleErrorOccurred(error: error)
                return
            }

            self.deleteUserInstructionForLoginTermsAndConditions()
            success()
        }
    }

    func deleteUserInstructionForLoginTermsAndConditions() {
        DataProvider.deleteCurrentUserInstructionForLoginTermsAndConditions()
    }

    func disagreeTermsAndConditions(success: @escaping (() -> Void)) {
        self.delegate?.showHUDOnWindow()

        guard let userInstrution = DataProvider.currentUserInstructionForLoginTermsAndConditions() else {
            self.delegate?.hideHUDFromWindow()
            self.handleErrorOccurred(error: nil)
            return
        }
        //TODO: remove nullable value
        let partyId = DataProvider.newRealm().getPartyId()
        let tenantId = DataProvider.newRealm().getTenantId()
        Wire.Events.captureLoginEvents(tenantId: tenantId, partyId: partyId, loginSuccess: false, userInstructionResponseId: "\(userInstrution.id)", userInstructionResponseType: "\(userInstrution.typeKey)") { (error) in
            self.delegate?.hideHUDFromWindow()

            guard error == nil else {
                self.handleErrorOccurred(error: error)
                return
            }

            self.deleteUserInstructionForLoginTermsAndConditions()
            success()
        }
    }
}
