import UIKit
import WebKit
import SnapKit

import VIAUIKit
import VitalityKit
import VIACommon

// MARK: Terms and conditions viewmodel protocol and extension

public typealias TermsAndConditionsNavigation = (_ fromViewController: UIViewController?, _ destinationViewController: UIViewController?) -> Void

public protocol TermsAndConditionsViewModel: CMSConsumer {
    var agreeAction: TermsAndConditionsNavigation { get }
    var disagreeAction: TermsAndConditionsNavigation { get }
    var articleId: String? { get }
    weak var delegate: VIATermsConditionsViewController? { get set }
}

extension TermsAndConditionsViewModel {
    func loadArticleContent(completion: @escaping (_ error: Error?, _ content: String?) -> Void) {
        guard let articleId = self.articleId else {
            completion(CMSError.missingArticleId, nil)
            return
        }

        self.getCMSArticleContent(articleId: articleId, completion: completion)
    }
}

// MARK: Terms and conditions viewcontroller

open class VIATermsConditionsViewController: VIAViewController, WKNavigationDelegate, SafariViewControllerEscapable {

    @IBOutlet var toolbar: UIToolbar!
    @IBOutlet var agreeButton: UIBarButtonItem!
    @IBOutlet var disagreeButton: UIBarButtonItem!
    private lazy var webView: WKWebView = WKWebView(frame: .zero)
    public var onError:(()->Void)?
    
    open var viewModel: TermsAndConditionsViewModel? {
        didSet {
            self.viewModel?.delegate = self
        }
    }

    open override func viewDidLoad() {
        super.viewDidLoad()

        self.agreeButton.title = CommonStrings.AgreeButtonTitle50
        self.disagreeButton.title = CommonStrings.DisagreeButtonTitle49
        self.navigationController?.navigationBar.isHidden = true
        self.toolbar.backgroundColor = UIColor.tableViewBackground()
        self.updateNavigation(agreeEnabled: false)
        self.configureWebView()
    }

    open override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        self.loadContent()
        self.toolbar.layer.addBorder(edge: .top)
        
        initBarButtonItems()
    }

    func configureWebView() {
        self.view.addSubview(self.webView)
        self.webView.navigationDelegate = self
        self.webView.snp.makeConstraints { make in
            make.width.equalTo(self.view)
            make.top.equalTo(self.view).offset(UIApplication.shared.statusBarFrame.height)
            make.bottom.equalTo(self.toolbar.snp.top)
        }
    }

    // MARK: Navigation

    public static func showMainTermsAndConditions(onDisagree:@escaping ()->Void,
                                                  onAgree:@escaping ()->Void) {
        let viewController = UIStoryboard(name: "TermsAndConditions", bundle: Bundle(for: VIATermsConditionsViewController.self)).instantiateViewController(withIdentifier: "VIATermsConditionsViewController")

        guard let termsConditionsViewController = viewController as? VIATermsConditionsViewController else {
            /*  Keeping it here for reference */
            //VIALoginViewController.showLogin()
            onDisagree()
            return
        }

        termsConditionsViewController.onError = onDisagree
        termsConditionsViewController.viewModel = VIAMainTermsAndConditionsViewModel(agreeActionCallback: {
            //TODO Agree post actions
            onAgree()            
        }, disagreeActionCallback: { 
            //TODO Disagree post actions
            onDisagree()
        })
        let window = UIApplication.shared.keyWindow
        window?.rootViewController = termsConditionsViewController
    }

    public func confirmDisagreeAndDisplayLogin(disagreeAction: @escaping () -> Void) {
        let controller = UIAlertController(title: CommonStrings.DisagreeButtonTitle49,
                                           message: CommonStrings.TermsAndConditions.DisagreeAlertMessage51,
                                           preferredStyle: .alert)

        let disagree = UIAlertAction(title: CommonStrings.OkButtonTitle40, style: .default) { action in
            disagreeAction()
        }
        controller.addAction(disagree)

        let cancel = UIAlertAction(title: CommonStrings.CancelButtonTitle24, style: .cancel) { action in
        }
        controller.addAction(cancel)

        let currentViewController = UIApplication.shared.keyWindow?.rootViewController
        currentViewController?.present(controller, animated: true, completion: nil)
    }

    // MARK: Content

    func loadContent() {
        self.showHUDOnView(view: self.view)
        self.viewModel?.loadArticleContent(completion: { [weak self] error, content in
            self?.hideHUDFromView(view: self?.view)
            if let error = error {
                self?.hideHUDFromView(view: self?.view)
                self?.handle(error: error)
            }
            if let validContent = content {
                self?.webView.loadHTMLString(validContent, baseURL: nil)
            }            
        })
    }

    public func handle(error: Error) {
        self.agreeButton.isEnabled = false
        switch error {
        case is BackendError:
            if let backendError = error as? BackendError {
                self.handleBackendErrorWithAlert(backendError, tryAgainAction: { [weak self] in
                    self?.agreeButton.isEnabled = true
                    self?.loadContent()
                })
            }
            break
        case is CMSError:
            self.displayErrorWithUnwindAction()
            break
        default:
            self.displayErrorWithUnwindAction()
            break
        }
    }

    func displayErrorWithUnwindAction() {
        displayUnknownServiceErrorOccurredAlert(okAction: { [weak self] in
            /* navigate back to login */
            //VIALoginViewController.showLogin()
            self?.onError?()
        })
    }
    
    private func initBarButtonItems(){
        if let hideDisagreeButton = VIAApplicableFeatures.default.hideTermsAndConditionsDisagreeButton,
            hideDisagreeButton{
            self.disagreeButton.isEnabled = false
            self.disagreeButton.title = ""
        }
    }

    // MARK: Action

    @IBAction private func agree(_ sender: AnyObject) {
        guard let action = self.viewModel?.agreeAction else {
            return
        }
        action(self, nil)
    }

    @IBAction private func disagree(_ sender: AnyObject) {
        guard let action = self.viewModel?.disagreeAction else {
            return
        }
        action(self, nil)
    }

    func updateNavigation(agreeEnabled: Bool) {
        self.agreeButton.isEnabled = agreeEnabled
    }

    // MARK: WKNavigationDelegate

    public func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        webView.evaluateJavaScript("document.documentElement.outerHTML.toString()", completionHandler: { html, error in
            self.updateNavigation(agreeEnabled: html != nil)
        })
    }
    
    public func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        escapeToSafariViewController(with: navigationAction, decisionHandler: decisionHandler)
    }
    
}
