//
//  HelpQuestionsModel.swift
//  VitalityActive
//
//  Created by Genie Mae Lorena Edera on 12/1/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation

class Question {
    var qID: String = ""
    var qCat: String = ""
    var qQuestion: String = ""
    var qContent: String = ""
    
    init(qID: String, qCat: String, qQuestion: String, qContent: String){
        self.qID = qID
        self.qCat = qCat
        self.qQuestion = qQuestion
        self.qContent = qContent
    }
}
