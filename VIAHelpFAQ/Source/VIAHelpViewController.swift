//
//  VIAHelpViewController.swift
//  VitalityActive
//
//  Created by Wilmar van Heerden on 2016/11/10.
//  Copyright © 2016 Glucode. All rights reserved.
//

import Foundation
import VIAUIKit
import SnapKit
import TTTAttributedLabel
import VitalityKit
import VIACommon

public class VIAHelpViewController: VIATableViewController, UISearchBarDelegate, PrimaryColorTintable {
    
    let suggestionText: String = CommonStrings.Help.EmptyTitle316
    let searchController = UISearchController(searchResultsController: nil)
    private var searchBar = UISearchBar()
    var sourceVCArr = ["Active Rewards","Health Review","S & V"]
    var sourceVC: String = ""
    var searchScope: Int = 1
    var searchScopeArr = ["Active Rewards","All Help"]
    var filteredQuestions = [Question]()
    
    let htmlTags = "<p>|</p>|<div>|</div>|<span>|</span>"
    var faqListArray: [Dictionary<String, String>] = []
    var searchListArray: [Dictionary<String, String>] = []
    var faqSuggestedList: Array<Question> = []
    var faqList: Array<Question> = []
    var searchList: Array<Question> = []
    var selectedTopic: Question? = nil
    var relatedHelpList: Array<Question> = []
    
    var hasStartedInitialLoad: Bool = false
    //var showAllResults: Bool = false
    
    @IBOutlet weak var segmentedView: UIView!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    
   
    
    // Mark: Initializers
    override public func awakeFromNib() {
        
        super.awakeFromNib()
        self.setupTabBarTitle()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        
        super.init(coder: aDecoder)
        self.setupTabBarTitle()
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        self.setupTabBarTitle()
    }
    
     // MARK: View lifecycle
    override public func viewDidLoad() {
        super.viewDidLoad()
        self.extendedLayoutIncludesOpaqueBars = !(self.navigationController?.navigationBar.isTranslucent)!
        self.title = CommonStrings.MenuHelpButton8

        self.sourceVC = sourceVCArr[GlobalUtil.helpSourceVC]
        self.searchScopeArr[0] = self.sourceVC
        self.configureAppearance()
        self.configureTableView()
    
        self.definesPresentationContext = true
    }
    
    override public func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if !hasSuggestionList() && !hasStartedInitialLoad {
            hasStartedInitialLoad = true
            self.loadSuggestions()
        }
    }
    
    override public func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.hideBackButtonTitle()
    }
    
    func setupTabBarTitle() {
        
        self.title = CommonStrings.MenuHelpButton8
        self.tabBarItem.title = CommonStrings.MenuHelpButton8
        self.navigationController?.tabBarItem.title = CommonStrings.MenuHelpButton8
        
    }
    
    func configureAppearance() {
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = CommonStrings.HelpViewSearchbarPlaceholder263
        searchController.searchBar.delegate = self
        searchController.searchBar.textField?.backgroundColor = UIColor(red: 0.94, green: 0.94, blue: 0.94, alpha: 1.00)
        searchController.searchBar.textField?.textColor = UIColor.mediumGrey()
        searchController.hidesNavigationBarDuringPresentation = false
        self.navigationItem.titleView = searchController.searchBar
    }
    
    override public func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showHelpTopic" {
            if let topicViewController = segue.destination as? VIAHelpTopicViewController {
                topicViewController.topicContent = self.selectedTopic
                topicViewController.filteredQuestions = self.filteredQuestions
                topicViewController.relatedHelpList = self.relatedHelpList
            }
        }
    }
    
    // MARK: View Functions
    
    func changeSearchScope(segment: UISegmentedControl) {
        self.searchScope = segment.selectedSegmentIndex
        
        let scope = self.searchScopeArr[self.searchScope]
        filterContentForSearchText(searchController.searchBar.text!, scope: scope, fromSearchField: false)
    }
    
    func configureTableView() {
        tableView.estimatedRowHeight = 75
    }
    
    override public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    override public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let resultCount: Int = 5 // Show 5 results.
        //if !showAllResults {
        if VIAApplicableFeatures.default.shouldLimitSearchResults! {
            if self.filteredQuestions.count >= resultCount {
                return resultCount
            } else {
                return self.filteredQuestions.count
            }
        } else {
            return self.filteredQuestions.count
        }
    }
    
    func hasSuggestionList() -> Bool {
        if self.faqSuggestedList.count > 0 {
            return true
        } else {
            return false
        }
    }
    
    func loadSuggestions() {
        self.showHUDOnView(view: self.navigationController?.view)
        
        // TODO: Replace tag name string 'help' with a more appropriate implementation.
        loadFAQsWithTagName("help") { [weak self] (success, error) in
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + .milliseconds(500)) {
                self?.hideHUDFromView(view: self?.navigationController?.view)
                
                if success {
                    // On success.
                    // Check for more than 5 items in array.
                    if (self?.faqListArray.count)! >= 5 {
                        // Process suggestion list.
                        for item in 1 ... 5 {
                            if let faqItem = self?.faqListArray[item - 1] {
                                
                                let questionItem = Question(qID: String(item),
                                                            qCat: "Generic",
                                                            qQuestion: (self?.htmlStringDecode(faqItem["question"]!))!,
                                                            qContent: (faqItem["question"]?.replacingOccurrences(of: "<div>", with: "<h3>").replacingOccurrences(of: "</div>", with: "</h3>").replacingOccurrences(of: "<p>", with: "<h3>").replacingOccurrences(of: "</p>", with: "</h3>"))! + "\n" + faqItem["answer"]!)
                                self?.faqSuggestedList.append(questionItem)
                            }
                        }
                        self?.showSuggestions()
                    }
                }
            }
        }
    }
    
    func startKeywordSearch(sender: Any) {
        //showAllResults = true
        let keyword: String = sender as! String
        
        // TODO: Replace tag name string 'faq' with a more appropriate implementation.
        loadFAQWithTagNameAndKeyword("faq", keyword: keyword) { [weak self] (success, error) in
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + .milliseconds(500)) {
                if success {
                    // On success.
                    self?.searchList.removeAll()
                    if (self?.searchListArray.count)! > 0 {
                        if let searchListArray = self?.searchListArray {
                            for item in 0 ... searchListArray.count - 1 {
                                let questionItem = Question(qID: String(item),
                                                            qCat: "Generic",
                                                            qQuestion: (self?.htmlStringDecode(searchListArray[item]["question"]!))!,
                                                            qContent: (searchListArray[item]["question"]?.replacingOccurrences(of: "<div>", with: "<h3>").replacingOccurrences(of: "</div>", with: "</h3>").replacingOccurrences(of: "<p>", with: "<h3>").replacingOccurrences(of: "</p>", with: "</h3>"))! + "\n" + searchListArray[item]["answer"]!)
                                self?.searchList.append(questionItem)
                            }
                        }
                    } else {
                        if self?.searchController.searchBar.text?.characters.count != 0 {
                            self?.showNoResultsAlertWithKeyword(keyword)
                        }
                    }
                    let scope = self?.searchScopeArr[(self?.searchScope)!]
                    self?.filterContentForSearchText(keyword, scope: scope!, fromSearchField: true)
                }
            }
        }
    }
    
    func loadFAQsWithTagName(_ tagName: String, completion: @escaping (_ success: Bool, _ error: String?) -> Void) {
        Wire.Content.getFAQByTagName(tagName: tagName) { error, data, result in
            if error != nil {
                self.hasStartedInitialLoad = false
                self.hideHUDFromView(view: self.navigationController?.view)
                self.handleErrorOccurred(error!)
                completion(false, error.debugDescription)
            } else {
                if result != nil {
                    let faqArray: Array = result!
                    self.faqListArray = faqArray as! [Dictionary<String, String>]
                    completion(nil != result, error.debugDescription)
                } else {
                    completion(false, error.debugDescription)
                }
            }
        }
    }
    
    func loadFAQWithTagNameAndKeyword(_ tagName: String, keyword: String, completion: @escaping (_ success: Bool, _ error: String?) -> Void) {
        Wire.Content.getFAQByTagNameAndKeyword(tagName: tagName, keyword: keyword) { error, data, result in
            if error != nil {
                self.hideHUDFromView(view: self.navigationController?.view)
                self.handleErrorOccurred(error!)
                completion(false, error.debugDescription)
            } else {
                if result != nil {
                    let searchArray: Array = result!
                    self.searchListArray = searchArray as! [Dictionary<String, String>]
                    completion(nil != result, error.debugDescription)
                } else {
                    completion(false, error.debugDescription)
                }
            }
        }
    }
    
    func showSuggestions() {
        if hasSuggestionList() {
            self.hideSuggestions()
            let superview = self.tableView
            let stackView = UIStackView()
            stackView.tag = 100
            stackView.axis = .vertical
            stackView.spacing = 20
            stackView.alignment = .center
            
            let headingLabel = UILabel()
            headingLabel.font = UIFont.title2Font()
            headingLabel.textColor = UIColor.night()
            headingLabel.textAlignment = .center
            headingLabel.text = suggestionText
            stackView.addArrangedSubview(headingLabel)
            
            var filteredSuggestions = self.faqSuggestedList.filter({( faqSuggestedList : Question) -> Bool in
                // return (faqSuggestedList.qCat == self.sourceVC)
                return (faqSuggestedList.qCat == "Generic")
            })
            
            for i in 0 ... filteredSuggestions.count - 1 {
                let suggestion = VIAButton(title: removeHtmlTags(filteredSuggestions[i].qQuestion))
                suggestion.hidesBorder = true
                suggestion.tag = i
                suggestion.titleLabel?.numberOfLines = 0
                suggestion.titleLabel?.textAlignment = .center
                suggestion.titleLabel?.lineBreakMode = .byWordWrapping
                suggestion.tintColor = UIColor.currentGlobalTintColor()
                suggestion.addTarget(self, action: #selector(self.suggestionButtonPressed(sender:)), for: .touchUpInside)
                stackView.addArrangedSubview(suggestion)
            }
            
            let spacer = UIView()
            spacer.backgroundColor = .clear
            stackView.addArrangedSubview(spacer)
            
            superview?.addSubview(stackView)
            stackView.snp.makeConstraints { make in
                make.centerX.equalToSuperview()
                make.centerY.equalToSuperview().multipliedBy(0.85)
                make.width.equalToSuperview().multipliedBy(0.80)
                make.height.equalToSuperview().multipliedBy(0.70)
            }
        }
    }
    
    func hideSuggestions() {
        if let viewWithTag = self.tableView.viewWithTag(100) {
            viewWithTag.removeFromSuperview()
        }
    }
    
    func showNoResultsAlertWithKeyword(_ keyword: String) {
        self.hideNoResultsAlert()
        let superview = self.tableView
        let noResultsStackView = UIStackView()
        noResultsStackView.tag = 300
        noResultsStackView.axis = .vertical
        noResultsStackView.spacing = 5
        noResultsStackView.alignment = .center
        
        let titleLabel = UILabel()
        titleLabel.font = UIFont.headlineFont()
        titleLabel.textColor = UIColor.darkGray
        titleLabel.numberOfLines = 0
        titleLabel.textAlignment = .center
//        titleLabel.text = CommonStrings.Help.Landing.NoResultTitle1429
        noResultsStackView.addArrangedSubview(titleLabel)
        
        let messageLabel = UILabel()
        messageLabel.font = UIFont.subheadlineFont()
        messageLabel.textColor = UIColor.darkGray
        messageLabel.numberOfLines = 0
        messageLabel.textAlignment = .center
        messageLabel.text = CommonStrings.Help.Landing.NoResultMessage1430(keyword)
        noResultsStackView.addArrangedSubview(messageLabel)
        
        superview?.addSubview(noResultsStackView)
        noResultsStackView.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.centerY.equalToSuperview().multipliedBy(0.80)
            make.width.equalToSuperview().multipliedBy(0.75)
        }
    }
    
    func hideNoResultsAlert() {
        if let viewWithTag = self.tableView.viewWithTag(300) {
            viewWithTag.removeFromSuperview()
        }
    }
    
    func showScopeSegmentedControl() {
        self.segmentedControl.setTitle(self.searchScopeArr[0], forSegmentAt: 0)
        self.segmentedControl.setTitle(self.searchScopeArr[1], forSegmentAt: 1)
        
        self.segmentedControl.addTarget(self, action: #selector(changeSearchScope), for: .valueChanged)
        self.segmentedControl.addTarget(self, action: #selector(changeSearchScope), for: .touchUpInside)
        self.changeSearchScope(segment: self.segmentedControl)
        self.tableView.tableHeaderView = self.segmentedView
        self.segmentedView.isHidden = false
        self.navigationItem.setHidesBackButton(true, animated:true)
    }
    
    
    func hideScopeSegmentedControl() {
        self.segmentedView.isHidden = true
    }
    
    func showObscureViewOnTable() {
        self.hideObscureViewOnTable()
        let superview = self.tableView
        let obscureView = UIView(frame: CGRect(x: 0, y: 0, width: (superview?.layer.bounds.width)!, height: (superview?.layer.bounds.height)!))
        obscureView.tag = 200
        obscureView.backgroundColor = UIColor.black
        obscureView.alpha = 0.2
        superview?.addSubview(obscureView)
    }
    
    func hideObscureViewOnTable() {
        if let viewWithTag = self.tableView.viewWithTag(200) {
            viewWithTag.removeFromSuperview()
        }
    }
    
    func searchBarIsEmpty() -> Bool {
        return searchController.searchBar.text?.isEmpty ?? true
    }
    
    func searchHasResult() -> Bool {
        if (filteredQuestions.count > 0) {
            return true
        } else {
            return false
        }
    }
    
    func filterContentForSearchText(_ searchText: String, scope: String, fromSearchField: Bool) {
        filteredQuestions = self.searchList.filter({( searchList : Question) -> Bool in
            // return (((scope == self.searchScopeArr[1]) || (searchList.qCat == scope)) && searchList.qQuestion.lowercased().contains(searchText.lowercased()))
            return (((scope == self.searchScopeArr[1]) || (searchList.qCat == scope)))
        })
        
        if searchHasResult() {
            self.hideSuggestions()
            self.hideObscureViewOnTable()
        } else {
            if searchBarIsEmpty() {
                self.showSuggestions()
                if fromSearchField {
                    self.showObscureViewOnTable()
                }
            } else {
                self.hideSuggestions()
                self.hideObscureViewOnTable()
            }
        }
        
        tableView.reloadData()
    }
}

// MARK: TableView DataSource and Delegate
extension VIAHelpViewController {
    override public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .default, reuseIdentifier: "cell")
        let attributed = NSMutableAttributedString(string: removeHtmlTags(self.filteredQuestions[indexPath.row].qQuestion) as String)
        
        /* // Old highlight string implementation.
        let r = (self.filteredQuestions[indexPath.row].qQuestion.lowercased() as NSString).range(of: searchController.searchBar.text!.lowercased())
        if (r.length > 0) {
            attributed.addAttribute(NSForegroundColorAttributeName, value: UIColor.currentGlobalTintColor(), range: r)
        }
        */
        
        let question = searchController.searchBar.text?.lowercased()
        let keywords = question?.components(separatedBy: " ")
        for keyword in keywords! {
            if keyword.characters.count >= 3 {
                let range = (self.filteredQuestions[indexPath.row].qQuestion.lowercased() as NSString).range(of: keyword)
                attributed.addAttribute(NSForegroundColorAttributeName, value: UIColor.currentGlobalTintColor(), range: range)
            }
        }
        
        //if !showAllResults {
        //    cell.textLabel?.numberOfLines = 1
        //} else {
        cell.textLabel?.numberOfLines = 0
        //}
        cell.textLabel?.attributedText = attributed
        return cell
    }
    
    override public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedTopic = self.searchList[Int(self.filteredQuestions[indexPath.row].qID)!]
        
        var relatedHelp = self.filteredQuestions
        relatedHelp.remove(at: indexPath.row)
        self.relatedHelpList = relatedHelp
        self.performSegue(withIdentifier: "showHelpTopic", sender: self)
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

// MARK: Search Controller Delegate
extension VIAHelpViewController: UISearchResultsUpdating {
    public func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        NSObject.cancelPreviousPerformRequests(withTarget: self)
        self.perform(#selector(startKeywordSearch(sender:)), with: searchBar.text)
        //showAllResults = true
    }
    
    public func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        NSObject.cancelPreviousPerformRequests(withTarget: self)
        self.hideObscureViewOnTable()
        self.hideNoResultsAlert()
        
        searchList = []
        let scope = self.searchScopeArr[self.searchScope]
        filterContentForSearchText(searchController.searchBar.text!, scope: scope, fromSearchField: true)
    }
    
    public func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        // TODO: Show for contextual help.
        // self.showScopeSegmentedControl()
        self.searchController.searchBar.setShowsCancelButton(true, animated: true)
    }
    
    public func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        NSObject.cancelPreviousPerformRequests(withTarget: self)
        self.navigationItem.setHidesBackButton(false, animated:true)
        self.searchController.searchBar.setShowsCancelButton(false, animated: true)
        
        self.hideObscureViewOnTable()
        
        if searchHasResult() {
            self.hideSuggestions()
        } else {
            if searchBarIsEmpty() {
                self.showSuggestions()
            }
        }
    }
    
    public func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.hideNoResultsAlert()
        NSObject.cancelPreviousPerformRequests(withTarget: self)
        if searchText.characters.count >= 3 {
            self.perform(#selector(startKeywordSearch(sender:)), with: searchText, afterDelay: 1.0)
        }
    }
    
    public func updateSearchResults(for searchController: UISearchController) {
        let scope = self.searchScopeArr[self.searchScope]
        filterContentForSearchText(searchController.searchBar.text!, scope: scope, fromSearchField: true)
        
        if !searchController.isActive {
            self.showSuggestions()
            self.hideObscureViewOnTable()
        }
    }
    
    public func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        filterContentForSearchText(searchBar.text!, scope: self.searchScopeArr[self.searchScope], fromSearchField: true)
    }
}

// MARK: Custom Functions
extension VIAHelpViewController {
    func suggestionButtonPressed(sender: UIButton) {
        self.selectedTopic = self.faqSuggestedList[sender.tag]
        //self.relatedHelpList = []
        self.performSegue(withIdentifier: "showHelpTopic", sender: self)
    }
    
    func handleErrorOccurred(_ error: Error) {
        let backendError = error as? BackendError ?? BackendError.other
        self.handleBackendErrorWithAlert(backendError)
    }
    
    func htmlStringDecode(_ string: String) -> String {
        guard let data = string.data(using: .utf8) else {
            return ""
        }
        
        let options: [String: Any] = [
            NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,
            NSCharacterEncodingDocumentAttribute: String.Encoding.utf8.rawValue
        ]
        
        guard let attributedString = try? NSAttributedString(data: data, options: options, documentAttributes: nil) else {
            return ""
        }
        return attributedString.string.replacingOccurrences(of: "\n", with: "", options: .regularExpression)
    }
    
    func removeHtmlTags(_ string: String) -> String {
        return string.replacingOccurrences(of: htmlTags, with: "", options: .regularExpression)
    }
}
