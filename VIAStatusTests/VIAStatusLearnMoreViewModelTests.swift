import XCTest

@testable import VIAStatus
@testable import VitalityKit
@testable import VIACommon

class VIAStatusLearnMoreViewModelTests: XCTestCase {
    
    let viewModel = VIAStatusLearnMoreViewModel()
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testTitle() {
        let title = CommonStrings.LearnMoreButtonTitle104
        XCTAssertEqual(viewModel.title, title)
    }
    
    func testNumberOfContentItems() {
        let count = 2
        XCTAssertEqual(viewModel.contentItems.count, count)
    }
}
