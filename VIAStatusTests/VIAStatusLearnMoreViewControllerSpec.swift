import Quick
import Nimble

@testable import VIACore
@testable import VitalityKit
@testable import VIAUIKit
@testable import VitalityKit
@testable import VIAStatus

class VIAStatusLearnMoreViewControllerSpec: QuickSpec {
    
    override func spec() {
        var viewController: VIAStatusLearnMoreViewController!
        
        describe("VIAStatusLearnMoreViewControllerSpec") {
            beforeEach {
                viewController = VIAStatusLearnMoreViewController()
                _ = viewController.view
            }
            
            context("when the view is loaded") {
                it("should not be nil") {
                    expect(viewController).notTo(beNil())
                }
                
                it("should have the correct number of rows") {
                    expect(viewController.tableView.numberOfRows(inSection: 0)).to(equal(2))
                }
            }
            
            context("First Content Cell") {
                var tableViewCell: VIAGenericContentCell!
                
                beforeEach {
                    tableViewCell = viewController.tableView(viewController.tableView, cellForRowAt: IndexPath(row: 0, section: 0)) as! VIAGenericContentCell
                }
                
                it("should show the correct section header") {
                    let header = StatusStrings.Status.LearnMore.Section1Title794
                    expect(tableViewCell.header).to(equal(header))
                }
                
                it("should show the correct section content") {
                    let content = StatusStrings.Status.LearnMore.Subtitle741
                    expect(tableViewCell.content).to(equal(content))
                }
                
                it("should not show section image") {
                    let image = UIImage(asset: VIAStatusAsset.pointsLarge)
                    expect(tableViewCell.cellImage).to(beNil())
                }
            }
            
            context("Second Content Cell") {
                var tableViewCell: VIAGenericContentCell!
                
                beforeEach {
                    tableViewCell = viewController.tableView(viewController.tableView, cellForRowAt: IndexPath(row: 1, section: 0)) as! VIAGenericContentCell
                }
                
                it("should show the correct section header") {
                    let header = StatusStrings.Status.LearnMore.Section1Title742
                    expect(tableViewCell.header).to(equal(header))
                }
                
                it("should show the correct section content") {
                    let content = StatusStrings.Status.LearnMore.Section1Content743
                    expect(tableViewCell.content).to(equal(content))
                }
                
                it("should show the correct section image") {
                    let image = VIAStatusAsset.pointsLarge.templateImage
                    expect(tableViewCell.cellImage).to(equal(image))
                }
            }
        }
    }
}
