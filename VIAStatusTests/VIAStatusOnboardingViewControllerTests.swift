import XCTest

@testable import VIACore
@testable import VitalityKit
@testable import VIAUIKit
@testable import VitalityKit
@testable import VIAStatus

class VIAStatusOnboardingViewControllerTests: XCTestCase {
    
    var viewController: VIAStatusOnboardingViewController!
    var tableViewCell: VIAOnboardingTableViewCell!
    
    override func setUp() {
        super.setUp()
        
        viewController = VIAStatusOnboardingViewController()
        viewController.viewModel = VIAStatusOnboardingViewModel()
        tableViewCell = viewController.tableView(viewController.tableView, cellForRowAt: IndexPath(row: 0, section: 0)) as! VIAOnboardingTableViewCell
        _ = viewController.view
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testNumberOfContentItemsLoaded() {
        XCTAssertEqual(viewController.tableView.numberOfRows(inSection: 0), 2)
    }
    
    func testSectionHeading() {
        let sectionHeading = StatusStrings.Status.OnboardingSection1Title603
        XCTAssertEqual(tableViewCell.heading, sectionHeading)
    }
    
    func testSectionContent() {
        let sectionContent = StatusStrings.Status.OnboardingSection1Message604
        XCTAssertEqual(tableViewCell.content, sectionContent)
    }
    
    func testTableViewSelectionStyle() {
        let selectionStyle = UITableViewCellSelectionStyle.none
        XCTAssertEqual(tableViewCell.selectionStyle, selectionStyle)
    }
}
