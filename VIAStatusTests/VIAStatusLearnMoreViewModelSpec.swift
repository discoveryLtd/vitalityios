import Quick
import Nimble

@testable import VIAStatus
@testable import VIALocalization
@testable import VIACommon

public class VIAStatusLearnMoreViewModelSpec: QuickSpec {
    
    override public func spec() {
        
        describe("VIAStatusLearnMoreViewModelSpec") {
            
            context("after being initalized") {
                let viewModel = VIAStatusLearnMoreViewModel()
                
                it("should have the correct title") {
                    let learnMoreTitle = CommonStrings.LearnMoreButtonTitle104
                    expect(viewModel.title).to(equal(learnMoreTitle))
                }
                
                it("should have the correct number of content items") {
                    let numContentItems = viewModel.contentItems.count
                    expect(viewModel.contentItems.count).toNot(equal(0))
                    expect(viewModel.contentItems.count).to(equal(numContentItems))
                }
                
                it("should have the correct custom fonts") {
                    let headingTitleFont = UIFont.title1Font()
                    let headingMessageFont = UIFont.subheadlineFont()
                    expect(viewModel.headingTitleFont).to(equal(headingTitleFont))
                    expect(viewModel.headingMessageFont).to(equal(headingMessageFont))
                }
                
                it("should have the correct image tint") {
                    let imageTint = UIColor.primaryColor()
                    expect(viewModel.imageTint).to(equal(imageTint))
                }
            }
        }
    }
}
