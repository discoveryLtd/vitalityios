import XCTest
@testable import VIAStatus
@testable import VitalityKit
@testable import VIAUtilities
@testable import VIAUIKit
@testable import VitalityKit
@testable import VIACommon

class StatusHelperTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testStatusDidIncreaseCheck() {
        XCTAssertFalse(StatusHelper.statusDidIncrease())
        let showStatusIncreasedInstruction = UserInstruction()
        showStatusIncreasedInstruction.id = 2
        showStatusIncreasedInstruction.typeKey = 2
        
        let realm = DataProvider.newRealm()
        do {
            try realm.write {
                realm.add(showStatusIncreasedInstruction)
            }
        } catch {
            XCTAssertFalse(true, "Realm crash when writing user instrcution")
        }
        
        XCTAssertTrue(StatusHelper.statusDidIncrease())
    }
    
    func testNewMembershipPeriod() {
        XCTAssertFalse(StatusHelper.newMembershipYearEntered())
        let showStatusIncreasedInstruction = UserInstruction()
        showStatusIncreasedInstruction.id = 3
        showStatusIncreasedInstruction.typeKey = 3
        
        let realm = DataProvider.newRealm()
        do {
            try realm.write {
                realm.add(showStatusIncreasedInstruction)
            }
        } catch {
            XCTAssertFalse(true, "Realm crash when writing user instrcution")
        }
        
        XCTAssertTrue(StatusHelper.newMembershipYearEntered())
    }
    //badge(for status:StatusTypeRef) -> UIImage?
    func testBageForStatusRef() {
        XCTAssertEqual(StatusHelper.badge(for: StatusTypeRef.Blue), VIAUIKitAsset.Status.badgeBlueLarge.image)
        XCTAssertEqual(StatusHelper.badge(for: StatusTypeRef.Bronze), VIAUIKitAsset.Status.badgeBronzeLarge.image)
        XCTAssertEqual(StatusHelper.badge(for: StatusTypeRef.Gold), VIAIconography.default.statusLandingGoldStatusIcon)
        XCTAssertEqual(StatusHelper.badge(for: StatusTypeRef.Silver), VIAUIKitAsset.Status.badgeSilverLarge.image)
        XCTAssertEqual(StatusHelper.badge(for: StatusTypeRef.Platinum), VIAUIKitAsset.Status.badgePlatinumLarge.image)
    }
    
    func testStatusName() {
        XCTAssertEqual(StatusHelper.statusName(for: StatusTypeRef.Platinum), StatusStrings.Status.LevelPlatinumTitle837)
        XCTAssertEqual(StatusHelper.statusName(for: StatusTypeRef.Blue), StatusStrings.Status.LevelBlueTitle833)
        XCTAssertEqual(StatusHelper.statusName(for: StatusTypeRef.Bronze), StatusStrings.Status.LevelBronzeTitle834)
        XCTAssertEqual(StatusHelper.statusName(for: StatusTypeRef.Silver), StatusStrings.Status.LevelSilverTitle835)
        XCTAssertEqual(StatusHelper.statusName(for: StatusTypeRef.Gold), StatusStrings.Status.LevelGoldTitle836)
    }
    
    func testStatusUpdateTitle() {
        XCTAssertEqual(StatusHelper.statusUpdateTitle(for: .Platinum), StatusStrings.Status.IncreasedStatusTitle810(StatusHelper.statusName(for: .Platinum)))
        XCTAssertEqual(StatusHelper.statusUpdateTitle(for: .Blue), StatusStrings.Status.IncreasedStatusTitle810(StatusHelper.statusName(for: .Blue)))
        XCTAssertEqual(StatusHelper.statusUpdateTitle(for: .Bronze), StatusStrings.Status.IncreasedStatusTitle810(StatusHelper.statusName(for: .Bronze)))
        XCTAssertEqual(StatusHelper.statusUpdateTitle(for: .Silver), StatusStrings.Status.IncreasedStatusTitle810(StatusHelper.statusName(for: .Silver)))
        XCTAssertEqual(StatusHelper.statusUpdateTitle(for: .Gold), StatusStrings.Status.IncreasedStatusTitle810(StatusHelper.statusName(for: .Gold)))
    }
    
    func testStatusUpdateSubTitle() {
        let blueStatus: String = StatusHelper.statusName(for: .Blue)
        let bronzeStatus: String = StatusHelper.statusName(for: .Bronze)
        let silverStatus: String = StatusHelper.statusName(for: .Silver)
        let goldStatus: String = StatusHelper.statusName(for: .Gold)
        let platinumStatus: String = StatusHelper.statusName(for: .Platinum)
        
        XCTAssertEqual(StatusHelper.statusUpdateSubtitle(for: ModalType.statusIncreased, status: StatusTypeRef.Blue), StatusStrings.Status.IncreasedStatusMessage811(blueStatus))
        XCTAssertEqual(StatusHelper.statusUpdateSubtitle(for: ModalType.statusIncreased, status: StatusTypeRef.Bronze), StatusStrings.Status.IncreasedStatusMessage811(bronzeStatus))
        XCTAssertEqual(StatusHelper.statusUpdateSubtitle(for: ModalType.statusIncreased, status: StatusTypeRef.Silver), StatusStrings.Status.IncreasedStatusMessage811(silverStatus))
        XCTAssertEqual(StatusHelper.statusUpdateSubtitle(for: ModalType.statusIncreased, status: StatusTypeRef.Gold), StatusStrings.Status.IncreasedStatusMessage811(goldStatus))
        XCTAssertEqual(StatusHelper.statusUpdateSubtitle(for: ModalType.statusIncreased, status: StatusTypeRef.Platinum), StatusStrings.Status.IncreasedStatusMessage811(platinumStatus))
        
        XCTAssertEqual(StatusHelper.statusUpdateSubtitle(for: ModalType.newMembershipYear, status: StatusTypeRef.Blue), StatusStrings.Status.InformationMessage816(blueStatus, blueStatus))
        XCTAssertEqual(StatusHelper.statusUpdateSubtitle(for: ModalType.newMembershipYear, status: StatusTypeRef.Bronze), StatusStrings.Status.InformationMessage816(bronzeStatus, bronzeStatus))
        XCTAssertEqual(StatusHelper.statusUpdateSubtitle(for: ModalType.newMembershipYear, status: StatusTypeRef.Silver), StatusStrings.Status.InformationMessage816(silverStatus, silverStatus))
        XCTAssertEqual(StatusHelper.statusUpdateSubtitle(for: ModalType.newMembershipYear, status: StatusTypeRef.Gold), StatusStrings.Status.InformationMessage816(goldStatus, goldStatus))
        XCTAssertEqual(StatusHelper.statusUpdateSubtitle(for: ModalType.newMembershipYear, status: StatusTypeRef.Platinum), StatusStrings.Status.InformationMessage816(platinumStatus, platinumStatus))
        
        XCTAssertEqual(StatusHelper.statusUpdateSubtitle(for: ModalType.statusReachedAgain, status: StatusTypeRef.Blue), StatusStrings.Status.IncreaseStatusAgainMessage832(blueStatus))
        XCTAssertEqual(StatusHelper.statusUpdateSubtitle(for: ModalType.statusReachedAgain, status: StatusTypeRef.Bronze), StatusStrings.Status.IncreaseStatusAgainMessage832(bronzeStatus))
        XCTAssertEqual(StatusHelper.statusUpdateSubtitle(for: ModalType.statusReachedAgain, status: StatusTypeRef.Silver), StatusStrings.Status.IncreaseStatusAgainMessage832(silverStatus))
        XCTAssertEqual(StatusHelper.statusUpdateSubtitle(for: ModalType.statusReachedAgain, status: StatusTypeRef.Gold), StatusStrings.Status.IncreaseStatusAgainMessage832(goldStatus))
        XCTAssertEqual(StatusHelper.statusUpdateSubtitle(for: ModalType.statusReachedAgain, status: StatusTypeRef.Platinum), StatusStrings.Status.IncreaseStatusAgainMessage832(platinumStatus))
    }
    
    func testEarnPointsMessage() {
        XCTAssertEqual(StatusHelper.earnPointsMessage(for: 1000), StatusStrings.Status.IncreasedStatusPointsNeeded813(String(describing: 1000)))
    }

    func testMainColorForStatus() {
        XCTAssertEqual(StatusHelper.mainColorForStatus(for: StatusTypeRef.Silver), UIColor.darkSilver())
        XCTAssertEqual(StatusHelper.mainColorForStatus(for: StatusTypeRef.Gold), UIColor.darkGold())
        XCTAssertEqual(StatusHelper.mainColorForStatus(for: StatusTypeRef.Platinum), UIColor.darkSilver())
    }
}
