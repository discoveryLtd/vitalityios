import XCTest

@testable import VIACore
@testable import VitalityKit
@testable import VIAUIKit
@testable import VitalityKit
@testable import VIAStatus

class VIAStatusLearnMoreViewControllerTests: XCTestCase {
    
    var viewController: VIAStatusLearnMoreViewController!
    var tableViewCell: VIAGenericContentCell!

    
    override func setUp() {
        super.setUp()
        
        viewController = VIAStatusLearnMoreViewController()
        tableViewCell = viewController.tableView(viewController.tableView, cellForRowAt: IndexPath(row: 0, section: 0)) as! VIAGenericContentCell
        _ = viewController.view
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testViewControllerIsProperlyLoaded() {
        XCTAssertNotNil(viewController)
    }
    
    func testNumberOfRowsInSectionOne() {
        XCTAssertEqual(viewController.tableView.numberOfRows(inSection: 0), 2)
    }
    
    func testSectionHeader() {
        let header = StatusStrings.Status.LearnMore.Section1Title794
        XCTAssertEqual(tableViewCell.header, header)
    }
    
    func testSectionContent() {
        let content = StatusStrings.Status.LearnMore.Subtitle741
        XCTAssertEqual(tableViewCell.content, content)
    }
}
