import XCTest

@testable import VIAStatus
@testable import VitalityKit

class VIAStatusOnboardingViewModelTests: XCTestCase {
    
    let viewModel = VIAStatusOnboardingViewModel()
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testHeading() {
        let vitalityStatusHeading = StatusStrings.Status.OnboardingTitle602
        XCTAssertEqual(viewModel.heading, vitalityStatusHeading)
    }
    
    func testNumberOfContentItems() {
        let numContentItems = 2
        XCTAssertEqual(viewModel.contentItems.count, numContentItems)
    }
    
    func testMainButtonTitle() {
        let mainButtonTitle = CommonStrings.GenericGotItButtonTitle131
        XCTAssertEqual(viewModel.buttonTitle, mainButtonTitle)
    }
    
    func testNoAlternateButtonTitle() {
        XCTAssertNil(viewModel.alternateButtonTitle)
    }
    
    func testLabelTextColor() {
        let labelTextColor = UIColor.genericActive()
        XCTAssertEqual(viewModel.labelTextColor, labelTextColor)
    }
    
    func testMainButtonTintColor() {
        let mainButtonTint = UIColor.primaryColor()
        XCTAssertEqual(viewModel.mainButtonTint, mainButtonTint)
    }
    
    func testMainButtonHighlightedTextColor() {
        let mainButtonHighlightedTextColor = UIColor.day()
        XCTAssertEqual(viewModel.mainButtonHighlightedTextColor, mainButtonHighlightedTextColor)
    }
}
