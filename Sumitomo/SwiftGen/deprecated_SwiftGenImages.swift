// Generated using SwiftGen, by O.Halligon — https://github.com/AliSoftware/SwiftGen

internal class SumitomoImagesPlaceholder {
    // Placeholder class to reference lower down
}

#if os(iOS) || os(tvOS) || os(watchOS)
  import UIKit.UIImage
  typealias Image = UIImage
#elseif os(OSX)
  import AppKit.NSImage
  typealias Image = NSImage
#endif

// swiftlint:disable file_length
// swiftlint:disable line_length

// swiftlint:disable type_body_length
@available(*, deprecated, message: "Use SumitomoAsset.{image}.image or SumitomoAsset.{image}.templateImage instead")
internal enum SUAsset: String {
  case homeLogoEn = "homeLogo_en"
  case homeLogoJa = "homeLogo_ja"
  case loginLogoEn = "loginLogo_en"
  case loginLogoJa = "loginLogo_ja"
  case vhcGenericBloodGlucoseAlternate = "vhcGenericBloodGlucoseAlternate"

  var image: Image {
    return Image(asset: self)
  }
}
// swiftlint:enable type_body_length

internal extension UIImage {
    @available(*, deprecated, message: "Use SumitomoAsset.{image}.image or SumitomoAsset.{image}.templateImage instead")
    class func templateImage(asset: SUAsset) -> UIImage? {
        let bundle = Bundle(for: SumitomoImagesPlaceholder.self) // placeholder declared at top of file
        let image = UIImage(named: asset.rawValue, in: bundle, compatibleWith: nil)?.withRenderingMode(.alwaysTemplate)
        return image
    }
}

internal extension Image {
    convenience init!(asset: SUAsset) {
        @available(*, deprecated, message: "Use SumitomoAsset.{image}.image or SumitomoAsset.{image}.templateImage instead")
        let bundle = Bundle(for: SumitomoImagesPlaceholder.self) // placeholder declared at top of file
        self.init(named: asset.rawValue, in: bundle, compatibleWith: nil)
    }
}
