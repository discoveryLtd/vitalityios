// Generated using SwiftGen, by O.Halligon — https://github.com/SwiftGen/SwiftGen

#if os(OSX)
import AppKit.NSImage
public typealias SumitomoColor = NSColor
public typealias SumitomoImage = NSImage
#elseif os(iOS) || os(tvOS) || os(watchOS)
import UIKit.UIImage
public typealias SumitomoColor = UIColor
public typealias SumitomoImage = UIImage
#endif

// swiftlint:disable file_length

public typealias SumitomoAssetType = SumitomoImageAsset

public struct SumitomoImageAsset: Equatable {
	fileprivate var name: String

	public var templateImage: SumitomoImage {
		let bundle = Bundle(for: BundleToken.self)
		#if os(iOS) || os(tvOS)
		let image = SumitomoImage(named: name, in: bundle, compatibleWith: nil)?.withRenderingMode(.alwaysTemplate)
		#elseif os(OSX)
		let image = bundle.image(forResource: name).withRenderingMode(.alwaysTemplate)
		#elseif os(watchOS)
		let image = SumitomoImage(named: name).withRenderingMode(.alwaysTemplate)
		#endif
		guard let result = image else { fatalError("Unable to load template image named \(name).") }
		return result
	}

	public var image: SumitomoImage {
		let bundle = Bundle(for: BundleToken.self)
		#if os(iOS) || os(tvOS)
		let image = SumitomoImage(named: name, in: bundle, compatibleWith: nil)
		#elseif os(OSX)
		let image = bundle.image(forResource: name)
		#elseif os(watchOS)
		let image = SumitomoImage(named: name)
		#endif
		guard let result = image else { fatalError("Unable to load image named \(name).") }
		return result
	}

	public static func ==(lhs: SumitomoImageAsset, rhs: SumitomoImageAsset) -> Bool {
        return lhs.image == rhs.image
    }
}

public struct SumitomoColorAsset {
fileprivate var name: String

#if swift(>=3.2)
@available(iOS 11.0, tvOS 11.0, watchOS 4.0, OSX 10.13, *)
var color: SumitomoColor {
return SumitomoColor(asset: self)
}
#endif
}

// swiftlint:disable identifier_name line_length nesting type_body_length type_name
public enum SumitomoAsset {
  public static let vhcGenericBloodGlucoseAlternate = SumitomoImageAsset(name: "vhcGenericBloodGlucoseAlternate")
  public static let statusTrophyGoldSmall = SumitomoImageAsset(name: "statusTrophyGoldSmall")
  public static let loginLogoEn = SumitomoImageAsset(name: "loginLogo_en")
  public static let statusTrophyGoldLarge = SumitomoImageAsset(name: "statusTrophyGoldLarge")
  public static let homeLogoJa = SumitomoImageAsset(name: "homeLogo_ja")
  public static let loginLogoJa = SumitomoImageAsset(name: "loginLogo_ja")
  public static let homeLogoEn = SumitomoImageAsset(name: "homeLogo_en")

  // swiftlint:disable trailing_comma
  public static let allColors: [SumitomoColorAsset] = [
  ]
  public static let allImages: [SumitomoImageAsset] = [
    vhcGenericBloodGlucoseAlternate,
    statusTrophyGoldSmall,
    loginLogoEn,
    statusTrophyGoldLarge,
    homeLogoJa,
    loginLogoJa,
    homeLogoEn,
  ]
  // swiftlint:enable trailing_comma
  @available(*, deprecated, renamed: "allImages")
  public static let allValues: [SumitomoAssetType] = allImages
}
// swiftlint:enable identifier_name line_length nesting type_body_length type_name

extension SumitomoImage {
@available(iOS 1.0, tvOS 1.0, watchOS 1.0, *)
@available(OSX, deprecated,
message: "This initializer is unsafe on macOS, please use the SumitomoImageAsset.image property")
convenience init!(asset: SumitomoAssetType) {
#if os(iOS) || os(tvOS)
let bundle = Bundle(for: BundleToken.self)
self.init(named: asset.name, in: bundle, compatibleWith: nil)
#elseif os(OSX) || os(watchOS)
self.init(named: asset.name)
#endif
}
}

extension SumitomoColor {
#if swift(>=3.2)
@available(iOS 11.0, tvOS 11.0, watchOS 4.0, OSX 10.13, *)
convenience init!(asset: SumitomoColorAsset) {
let bundle = Bundle(for: BundleToken.self)
#if os(iOS) || os(tvOS)
self.init(named: asset.name, in: bundle, compatibleWith: nil)
#elseif os(OSX)
self.init(named: asset.name, bundle: bundle)
#elseif os(watchOS)
self.init(named: asset.name)
#endif
}
#endif
}

private final class BundleToken {}
