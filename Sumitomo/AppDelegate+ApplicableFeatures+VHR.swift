//
//  AppDelegate+ApplicableFeatures+VHR.swift
//  VitalityActive
//
//  Created by Dexter Anthony Ambrad on 4/3/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import VIACommon
import VitalityKit
import VIAUtilities

extension AppDelegate{
    
    /**
     * Usage:
     * AssessmentNumberRangeQuestionSectionController.swift
     * VHCBMICaptureResultsCollectionViewCell+TextField.swift
     * VHCBloodPressureCaptureResultsCollectionViewCell.swift
     * VHCSingleCaptureResultsCollectionViewCell.swift
     **/
    public func shouldChangeCharactersInAssessmentNumberRangeQuestionSection(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String, selectedUnit: Unit, questionTypeKey: Int, healthAttributeType: PartyAttributeTypeRef) -> Bool {
        if shouldNotAcceptDecimalInputInAssessmentNumberRangeQuestionSection(selectedUnit, questionTypeKey: questionTypeKey, healthAttributeType: .Unknown) {
            return isInputAWholeNumber(string)
        } else {
            return isInputHasCorrectDecimalPlaces(textField.text ?? "", range: range, replacementString: string)
        }
    }
    
    // This will validate on which UoM should have decimal limits. Else, value must be whole number.
    private func shouldNotAcceptDecimalInputInAssessmentNumberRangeQuestionSection(_ selectedUnit: Unit, questionTypeKey: Int, healthAttributeType: PartyAttributeTypeRef) -> Bool {
        return selectedUnit == UnitOfMeasureRef.PerDay.unit() ||
            selectedUnit == UnitOfMeasureRef.PerWeek.unit() ||
            selectedUnit == UnitOfMeasureRef.FootInch.unit() ||
            selectedUnit == UnitOfMeasureRef.StonePound.unit()
    }
    
    private func isInputAWholeNumber(_ string: String) -> Bool {
        let validInputs = NSCharacterSet(charactersIn:"0123456789").inverted
        let componentsToSeparate = string.components(separatedBy: validInputs)
        let numberFiltered = componentsToSeparate.joined(separator: "")
        return string == numberFiltered
    }
    
    // This will validate the decimal places for other UoMs. In this case, it should be 2 decimal places.
    private func isInputHasCorrectDecimalPlaces(_ text: String, range: NSRange, replacementString string: String) -> Bool {
        let nsText = text as NSString
        let newText = nsText.replacingCharacters(in: range, with: string)
        if let regex = try? NSRegularExpression(pattern: "^[0-9]*((\\.|,)[0-9]{0,1})?$", options: .caseInsensitive) {
            return regex.numberOfMatches(in: newText, options: .reportProgress, range: NSRange(location: 0, length: (newText as NSString).length)) > 0
        }
        return false
    }
    
    public func getVHRSystemFormattedNumber(from localizedString: String?, selectedUnitOfMeasureType: UnitOfMeasureRef) -> String? {
        
        guard let validString = localizedString else { return nil }
        guard let number = NumberFormatter.decimalFormatter().number(from: validString) else { return nil }
        
        return NumberFormatter.oneDecimalFormatter().string(from: number)
    }
    
    /**
     * Usage:
     * AssessmentNumberRangeQuestionSectionController.swift
     **/
    public func shouldAcceptDecimalInput(_ selectedUnit: Unit, questionTypeKey: Int, healthAttributeType: PartyAttributeTypeRef) -> Bool {
        if  selectedUnit == UnitOfMeasureRef.PerDay.unit()   ||
            selectedUnit == UnitOfMeasureRef.PerWeek.unit() ||
            selectedUnit == UnitOfMeasureRef.FootInch.unit() ||
            selectedUnit == UnitOfMeasureRef.StonePound.unit() {
            return false
        }
        return true
    }

    public func shouldExecuteGroupValidation() -> Bool {
        return true
    }
    
    /**
     * Usage:
     * AssessmentNumberRangeQuestionSectionController.swift
     * VNAAssessmentNumberRangeQuestionSectionController.swift
     **/
    public var removeAssessmentGuidanceText: Bool?{
        get{
            return true
        }
    }
    
    /**
     * Usage:
     * Assessments -- VHR, VNA, MWB
     **/
    public var showRequiredPrompt: Bool?{
        get{
            return true
        }
    }
    
    /**
     * Usage:
     * Assessments -- VHR AssessmentQuestionnaireSectionViewModel
     **/
    public func isValid(questionnaireSection: VHRQuestionnaireSection) -> Bool {
        
        var capturedResults = [VHRCapturedResult]()
        var questionsWithoutCapturedResults = [VHRQuestion]()
        
        // exclude Label questions from validation
        let questionsToBeValidated = questionnaireSection.questions.filter({ $0.questionType != .Label })
        
        for question in questionsToBeValidated {
            if let results = question.capturedResults(), results.count > 0, question.isVisible {
                capturedResults.append(contentsOf: results)
            } else if question.isVisible {
                questionsWithoutCapturedResults.append(question)
            }
        }
        
        // validate
        let invalidCapturedResults = capturedResults.filter({ $0.valid == false })
        
        //ge20180418: different validation for medical history because we want to force selection
        if(questionnaireSection.typeCode == "MedicalHistory"){
            var trueCount : Int = 0
            for capturedAnswer in capturedResults {
                if(capturedAnswer.answer == "True"){
                    trueCount += 1
                }
            }
            return trueCount > 0 && invalidCapturedResults.count == 0 && questionsWithoutCapturedResults.count == 0
        }
        
        return invalidCapturedResults.count == 0 && questionsWithoutCapturedResults.count == 0
    }
    
    public var vhrDisableKeyboardDecimalInput: Bool? {
        get {
            return true
        }
    }
    
    public func vhrRemoveUnknownUOM() -> Bool {
        return false
    }
    
    public func vhrDisplayUoMAsSymbol() -> Bool {
        return false
    }
    
    public func decimalFormatter(shouldAcceptDecimalInput: Bool) -> NumberFormatter? {
        let formatter = Localization.decimalFormatter
        
        return formatter
    }
    
    public func showVHRDisclaimer() -> Bool {
        return false
    }
}
