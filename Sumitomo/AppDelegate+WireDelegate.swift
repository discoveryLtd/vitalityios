import Foundation
import VitalityKit

extension AppDelegate: WireDelegate {

    public var qaFrankfurtURL: URL {
        return URL(string: "")!
    }

//    public var devBaseURL: URL {
//        return URL(string: "https://m.dev.vitality.sumitomolife.co.jp/api")!
//    }

    public var testBaseURL: URL {
        return URL(string: "https://m.test.vitality.sumitomolife.co.jp/api")!
    }

    public var qaBaseURL: URL {
        return URL(string: "https://m.qa.vitality.sumitomolife.co.jp/api")!
    }

    public var productionBaseURL: URL {
        return URL(string: "https://www.vitality.sumitomolife.co.jp/api")!
    }

    public var performanceBaseURL: URL {
        return URL(string: "")!
    }
    
    public var qaSouthKoreaBaseURL: URL{
        return URL(string: "")!
    }
}

extension AppDelegate{
    
//    public var devAPIManagerIdentifier: String {
//        return "0000000000000000000000000000000000000000000000000000000000000000000000000000"
//    }
    
    public var testAPIManagerIdentifier: String {
        return "eW1qQ3JPcklvOTBSUXlLOVhVMU9DdU9kN1FrYTpFSWIwTGF5WDJvZXVBUmFHdHQ1V1Z6cEd5bEVh"
    }
    
    public var qaAPIManagerIdentifier: String {
        return "R3VacWo1SExUMmFwUVB3Y1BhZTBLYVRQVjdjYTo3UFJrYTREaXVCZ3JLUTdoRmR2OWZidFpyOWth"
    }
    
    public var productionAPIManagerIdentifier: String {
        return "a19OMkJMSnlpVzVIWHlndHk3c0FFUFFJaFRNYTpTOGt3VTdnUXJWX2pQSTJVT0hyWTdRejRoUGth"
    }
    
    public var qaFrankfurtAPIManagerIdentifier: String {
        return "0000000000000000000000000000000000000000000000000000000000000000000000000000"
    }
    
    public var performanceAPIManagerIdentifier: String {
        return ""
    }
    
    public var qaSouthKoreaAPIManagerIdentifier: String {
        return ""
    }
}
