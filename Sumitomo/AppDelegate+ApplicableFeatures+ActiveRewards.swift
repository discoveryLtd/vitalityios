//
//  AppDelegate+ApplicableFeatures+ActiveRewards.swift
//  VitalityActive
//
//  Created by OJ Garde on 3/17/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import VIAUtilities
import VitalityKit
import VIACommon
import VIAActiveRewards

extension AppDelegate {

    /**
     * Usage: VIAARFlowViewModel.swift
     */
    public func shouldIncludeUsedAndExpiredSpinInHistory() -> Bool {
        return false
    }
    
    /* Usage: VIAARLearnMoreViewController */
    public var hideARLearnMoreBenefitGuide: Bool? {
        get{
            return false
        }
    }
    
    public func shouldFilterPartnerGroups() -> Bool {
        return true
    }
    
    public var partnerDetailViewControllerActionImage: UIImage? {
        get{
            return VIACommonAsset.partnersLearnMore.templateImage
        }
    }
    
    public var showSwapRewardsOption: Bool? {
        get{
            return true
        }
    }
    
    public var redirectToWebVoucher: Bool? {
        get{
            return true
        }
    }
    
    public func getPartnerDetailViewControllerActionTitle(with partnerTypeKey: Int? = nil) -> String {
        /*
         * Unwrap the typeKey with default value to Unknown -1.
         * If typeKey is nil or not defined, it will fallback to the generic button title of the market.
         */
        switch PartnerTypeKeyRef(rawValue: partnerTypeKey ?? -1) {
        case .HotelsDotCom?:
            return CommonStrings.BookNowButtonTitle2188
        default:
            /* NOTE: Title may vary on every market */
            return CommonStrings.LearnMoreButtonTitle104
        }
    }
    
    public func getPartnerRewardInstructions(with voucherString: String) -> String {
        return voucherString
    }
    
    public var convertJoulesToCalories: Bool? {
        get{
            return false
        }
    }
    
    public var filterRewardTypeCode: Bool? {
        get {
            return false
        }
    }
    
    public var filterRewardByStatusOnly: Bool {
        get{
            return false
        }
    }
    
    public func removeAppleWatchOnPartnersData() -> Bool {
        return true
    }
    
    public func navigateToBenefitGuide(segue: UIStoryboardSegue){
        if let url = URL(string: "http://vitality.sumitomolife.co.jp/reward/active/"){
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    
    public func hideRewardsSubtitle() -> Bool{
        return true
    }
    
    public func enabledEmptyStatusView() -> Bool {
        return false
    }
    
    public func hideVitalityCoinsFromRewards() -> Bool {
        return true
    }
    
    public func getActivityHistoryViewModel() -> AnyObject? {
        return VIAARActivityHistoryViewModel()
    }
    
    public func getAvailableRewardViewModel() -> AnyObject? {
        return VAARAvailableRewardViewModel()
    }    
}
