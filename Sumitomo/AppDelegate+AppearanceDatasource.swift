import VIACommon
import VIAUIKit
import VIACore
import UIKit
import VitalityKit

extension AppDelegate: AppearanceColorDataSource {

    class func sumitomoRedColor() -> UIColor {
        return UIColor(red:233 / 255.0, green:34 / 255.0, blue:45 / 255.0, alpha:1.0)
    }

    // MARK: AppearanceDataSource

    public var primaryColor: UIColor {
        return AppDelegate.sumitomoRedColor()
    }

    public var tabBarBackgroundColor: UIColor {
        return .white
    }

    public var tabBarBarTintColor: UIColor {
        return .white
    }

    public var tabBarTintColor: UIColor? {
        return UIColor.primaryColor()
    }

    public var unselectedItemTintColor: UIColor? {
        return nil
    }
    
    public var getInsurerGlobalTint: UIColor? {
        return VIAAppearance.default.primaryColorFromServer
    }
    
    public func getSplashScreenGradientTop() -> UIColor{
        return UIColor(hexString: AppConfigFeature.insurerGradientColor1Hex())
    }
    
    public func getSplashScreenGradientBottom() -> UIColor{
        return UIColor(hexString: AppConfigFeature.insurerGradientColor2Hex())
    }

}

extension AppDelegate: AppearanceIconographyDataSource {
    public func loginLogo(for locale: Locale) -> UIImage {
        if locale.languageCode == "ja" {
            return UIImage(asset: .loginLogoJa)
        }
        return UIImage(asset: .loginLogoEn)
    }
    
    public func splashLogo(for locale: Locale = Locale.current) -> UIImage {
        return UIImage(asset: .loginLogoJa).maskWithColor(UIColor.white)!
    }
    
    public func homeLogo(for locale: Locale) -> UIImage {
        if locale.languageCode == "ja" {
            return UIImage(asset: .homeLogoJa)
        }
        return UIImage(asset: .homeLogoEn)
    }

    public var vhcGenericBloodGlucose: UIImage? {
        get {
            return UIImage(asset: .vhcGenericBloodGlucoseAlternate)
        }
    }
    
    public var statusLandingGoldStatusIcon: UIImage? {
        get {
            return SumitomoAsset.statusTrophyGoldLarge.image
        }
    }
}
