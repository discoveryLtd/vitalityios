//
//  AppDelegate+ApplicableFeatures+ActivationBarcode.swift
//  Sumitomo
//
//  Created by Val Tomol on 19/08/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation

extension AppDelegate {
    public var useAcceptEncodingDeflateHeader: Bool? {
        get {
            return true
        }
    }
}
