//
//  AppDelegate+ApplicableFeatures+AWC.swift
//  VitalityActive
//
//  Created by Von Kervin R. Tuico on 28/05/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import VIACommon
import VitalityKit

extension AppDelegate {
    /**
     * Usage:
     * AWCLandingViewModel.swift
     **/
    public func getVMPURL() -> URL? {
        let currentEnvironment = Wire.default.currentEnvironment
        
        switch currentEnvironment {
        case .develop:
            return URL(string: "")!
        case .test2:
            return URL(string: "https://sli.vmp.test.vitalitydeveloper.com/ja/web/vitality-member-portal/rewards/apple-watch")!
        case .qa:
            return URL(string: "https://www.qa.vitality.sumitomolife.co.jp/ja/web/vitality-member-portal/rewards/apple-watch")!
        case .production:
             return URL(string: "https://www.vitality.sumitomolife.co.jp/ja/web/vitality-member-portal/rewards/apple-watch")!
        default: break
        }
        return nil
    }
}
