//
//  AppDelegate+ApplicableFeatures+Status.swift
//  Sumitomo
//
//  Created by Von Kervin R. Tuico on 29/07/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import VIACommon
import VIAStatus
import VitalityKit

extension AppDelegate{
    
    public func getStatusPointsEarningActivitiesViewCell(tableView: UITableView, indexPath: IndexPath,
                                                         pointsActivityName: String?, potentialPointsString: String?,
                                                         eventKey: Int, activityCode: String?) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: TitleSubtitleTableViewCell.defaultReuseIdentifier, for: indexPath)
        guard let viaCell = cell as? TitleSubtitleTableViewCell,
            let pointsActivityName = pointsActivityName else {
                return tableView.defaultTableViewCell()
        }
        
        viaCell.title.text = pointsActivityName
        
        /* Hide Screenings Points for EventKey == 21. */
        if eventKey == 21 {
            viaCell.subtitle.text = nil
        }else if activityCode == "OFE"{
            /* activityCode == OFE. VA-36325: Hardcoded as advise on the ticket. */
            viaCell.subtitle.text = CommonStrings.Status.PointsIndicationMessageUpTo828("2000")
        }else{
            if let potentialPointsString = potentialPointsString{
                viaCell.subtitle.text = potentialPointsString
            }
        }
        
        viaCell.accessoryType = .disclosureIndicator
        
        return viaCell
    }
    
    public func getAnnualStatusViewModel() -> AnyObject?{
        return VAAnnualStatusViewModel()
    }
    
    public func getPointsEarningActivitiesViewModel() -> AnyObject?{
        return VAPointsEarningActivitiesViewModel()
    }
}
