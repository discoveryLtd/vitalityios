//
//  AppDelegate+ApplicableFeatures+VNA.swift
//  Sumitomo
//
//  Created by Von Kervin R. Tuico on 26/07/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import VIACommon

extension AppDelegate {
    
    public var disableKeyboardDecimalInput: Bool? {
        get {
            return true
        }
    }
}
