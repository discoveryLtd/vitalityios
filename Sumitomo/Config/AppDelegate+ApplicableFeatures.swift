//
//  AppDelegate+ApplicableFeatures.swift
//  Sumitomo
//
//  Created by OJ Garde on 11/12/2017.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import VIACommon
import VitalityKit
import VIAUIKit
import VIACore
/**
 * Main
 **/
extension AppDelegate: ApplicableFeaturesDataSource{
    
    public var hideHelpTab: Bool?{
        get{
            return false
        }
    }
        
    public var showHelpContactFooter: Bool?{
        get{
            return false
        }
    }
    
    public var showPartnerGetStartedLink: Bool?{
        get{
            return true
        }
    }
    
    public var hideMembershipPassAndUpdateTitle: Bool?{
        get{
            return true
        }
    }
    
    public var showPreviousMembershipYearTitle: Bool?{
        get{
            return true
        }
    }
    
    public var getPointPeriod: PointsPeriod? {
        
        return PointsPeriod.currentAndPrevious
    }
    
    public var enableKeyboardAutoToolbar: Bool? {
        get {
            return false
        }
    }

    public func getDataSharingLeftBarButtonTitle() -> String{
        return CommonStrings.BackButton336
    }
    
    public func getDataSharingRightBarButtonTitle() -> String{
        return CommonStrings.NextButtonTitle84
    }
    
    public var showPartnersTabItem: Bool? {
        get{
            return false
        }
    }
    
    public var applyPushNotifToggle: Bool? {
        get{
            return false
        }
    }
    
    public var enableNotificationDialogConfirmation: Bool {
        get{
            return true
        }
    }

    public func shouldDisplayEarningPointsAndPointsLimits(for eventKey: Int) -> Bool{
        return eventKey == 22
    }
    
    public func showHomeInitialScreen(){
        var storyboard = UIStoryboard(coreStoryboard: .home)
        if !VitalityPartyMembership.startedMembership(){
            storyboard = UIStoryboard(coreStoryboard: .loginRestriction)
        }
        let viewController = storyboard.instantiateInitialViewController()
        UIApplication.shared.keyWindow?.rootViewController = viewController
    }

    public func getTextFieldViewCell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        return tableView.dequeueReusableCell(withIdentifier: VIAEmailTextFieldTableViewCell.defaultReuseIdentifier, for: indexPath)
    }
    
    public func getImageControllerConfiguration() -> AnyObject?{
        let maxFileSize = Bundle.main.object(forInfoDictionaryKey: "VAFileUploadLimitInBytes") as? Int ?? 10485760
        return UIImagePickerController.ImageControllerConfiguration(shouldLimitFileSize: true, maxFileSize: maxFileSize,
                                                                    shouldLimitNumberOfAttachProof: true,
                                                                    maxAttachmentCount: 5,
                                                                    includeAllProofs: true) as AnyObject
    }
    
    public func setMinimumDate() -> Date? {
        return DataProvider.newRealm().getRenewalPeriods().first?.effectiveFrom?.setMinimumDate()
    }

    public func navigateToHomeScreen(){
        VIAHomeViewController.showHome()
    }
    
    public func navigateToLoginScreen(){
        VIALoginViewController.showLogin()
    }
    
    public var numberOfDecimalPlaces: Int? {
        get {
            return 1
        }
    }
    
    public func getFirstTimePreferenceViewModel() -> AnyObject? {
        return VIAFirstTimePreferenceViewModel()
    }
}
