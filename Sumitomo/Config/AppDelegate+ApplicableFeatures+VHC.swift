//
//  AppDelegate+ApplicableFeatures+VHC.swift
//  VitalityActive
//
//  Created by OJ Garde on 1/14/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import VIACommon
import VitalityKit
import RealmSwift
import VIAUtilities
import VIAHealthCheck
import VIAUIKit

extension AppDelegate{
    
    /**
     * Usage:
     * VHCLandingViewController.swift
     **/
    public func showVHCHistoryMenu() -> Bool{
        return false
    }
    
    /**
     * Usage:
     * VHCCaptureResultsViewModel.swift
     **/
    public func isCapturedVHCIsValid(bloodPressureValid: Bool, capturedResults: Results<VHCCapturedResult>) -> Bool {
        // nothing captured
        if capturedResults.count == 0 {
            return false
        }
        
        let bloodPressureValid = bloodPressureValid
        
        var allOtherCapturedResultsValid = true
        
        // Let's loop through all the captured result. Once there is an invalid captured result, stop the loop and return false.
        for capturedResult in capturedResults {
            if withDefaultStringValueForVHCCapturedDate() {
                if !(capturedResult.isValid)  && capturedResult.unitOfMeasureType != .Unknown || capturedResult.dateCaptured == NSDate.distantPast{
                    allOtherCapturedResultsValid = false
                    break
                }
            } else {
                if !(capturedResult.isValid)  && capturedResult.unitOfMeasureType != .Unknown{
                    allOtherCapturedResultsValid = false
                    break
                }
            }
        }
        return allOtherCapturedResultsValid && bloodPressureValid
    }
    
    /**
     * Usage:
     * VHCCaptureResultsViewModel.swift
     **/
    public func didVHCBloodPressureCapturedCompletely(systolic: VHCCapturedResult?, diastolic: VHCCapturedResult?) -> Bool{
        return (systolic == nil && diastolic == nil) || (systolic != nil && diastolic != nil)
    }
    
    /**
     * Usage:
     * VHCCaptureResultsCollectionViewController
     **/
    public func onValidateCapturedResults(isBMIValid: Bool, isCholesterolValid: Bool, areInputsValid: Bool, onError: () -> Void, onNext: () -> Void){
        
        /*
         * Add checker for BMI. If both BMI fields are valid. Enable next button.
         */
        if isBMIValid{
            onNext()
        } else {
            onError()
        }
    }
    
    /**
     * Usage:
     * VHCCaptureResultsValidValuesListViewController.swift
     * VHCCaptureResultsCollectionViewController.swift
     **/
    public var reverseValidValuesOptionList: Bool? {
        get{
            return false
        }
    }
    
    /**
     * Usage:
     * VHCHealthAttributeGroup+Extension.swift
     **/
    public var hideCholesterolContent: Bool?{
        get{
            return false
        }
    }
    
    /**
     * Usage:
     * VHCSingleMeasurableInputSectionController.swift
     **/
    public func shouldShowReminderMessageOnVHC(attribute: PartyAttributeTypeRef) -> Bool{
        let attributes: [PartyAttributeTypeRef] = [.TotalCholesterol, .HDLCholesterol]
        
        for validAttribute in attributes{
            return attribute == validAttribute
        }
        return false
    }
    
    /**
     * Usage:
     * VHCCaptureResultsViewModel.swift
     **/
    public var hideCholesterolRatio: Bool?{
        get{
            return false
        }
    }
    
    public func getCommonStringsMeasurementCholesterolSectionTitle() -> String{
        
        return CommonStrings.Measurement.CholesterolTitle138
    }
    
    /**
     * VHC Learn More
     * BodyMassIndexLearnMore.swift
     */
    public func getCommonStringsLearnMoreSection3MessageBmi(party: VitalityParty?) -> String{
        if let birthdate = party?.person?.bornOn{
            
            /**
             * Use this code to simulate age 65.
             * birthdate = Calendar.current.date(byAdding: .year, value: -65, to: birthdate)!
             */
            
            if birthdate.getAge() >= 65{
                /**
                 * Based from ticket https://jira.vitalityservicing.com/browse/VA-17328
                 * Points should be 500 and 2000 for 65 and above
                 */
                return CommonStrings.LearnMore.BmiSection3DynamicMessage227("500", "2,000")
            }
        }
        return CommonStrings.LearnMore.BmiSection3DynamicMessage227("500", "1,500")
    }
    
    /**
     * VHC Learn More
     * WaistCircumferenceLearnMore.swift
     */
    public func getCommonStringsLearnMoreSection3MessageWaistCircumference(party: VitalityParty?) -> String{
        return CommonStrings.LearnMore.WaistCicumferneceSection3Message230
    }
    
    /**
     * VHC Learn More
     * GlucoseLearnMore.swift
     */
    public func getCommonStringsLearnMoreSection3MessageGlucose(party: VitalityParty?) -> String{
        if let birthdate = party?.person?.bornOn{
            
            /**
             * Use this code to simulate age 65.
             * birthdate = Calendar.current.date(byAdding: .year, value: -65, to: birthdate)!
             */
            
            if birthdate.getAge() >= 65{
                /**
                 * Based from ticket https://jira.vitalityservicing.com/browse/VA-17328
                 * Points should be 500 and 2000 for 65 and above
                 */
                return CommonStrings.LearnMore.GlucoseSection3DynamicMessage233("500", "2,000")
            }
        }
        return CommonStrings.LearnMore.GlucoseSection3DynamicMessage233("500", "1,500")
    }
    
    /**
     * VHC Learn More
     * BloodPressureLearnMore.swift
     */
    public func getCommonStringsLearnMoreSection3MessageBloodPressure(party: VitalityParty?) -> String{
        if let birthdate = party?.person?.bornOn{
            
            /**
             * Use this code to simulate age 65.
             * birthdate = Calendar.current.date(byAdding: .year, value: -65, to: birthdate)!
             */
            
            if birthdate.getAge() >= 65{
                /**
                 * Based from ticket https://jira.vitalityservicing.com/browse/VA-17328
                 * Points should be 500 and 2000 for 65 and above
                 */
                return CommonStrings.LearnMore.BloodPressureSection3DynamicMessage236("500", "2,000")
            }
        }
        return CommonStrings.LearnMore.BloodPressureSection3DynamicMessage236("500", "1,500")
    }
    
    /**
     * VHC Learn More
     * CholesterolLearnMore.swift
     */
    public func getCommonStringsLearnMoreSection3MessageCholesterol(party: VitalityParty?) -> String{
        if let birthdate = party?.person?.bornOn{
            
            /**
             * Use this code to simulate age 65.
             * birthdate = Calendar.current.date(byAdding: .year, value: -65, to: birthdate)!
             */
            
            if birthdate.getAge() >= 65{
                /**
                 * Based from ticket https://jira.vitalityservicing.com/browse/VA-17328
                 * Points should be 500 and 2000 for 65 and above
                 */
                return CommonStrings.LearnMore.CholesterolSection3DynamicMessage239("500", "2,000")
            }
        }
        return CommonStrings.LearnMore.CholesterolSection3DynamicMessage239("500", "1,500")
    }
    
    /**
     * VHC Learn More
     * HbA1cLearnMore.swift
     */
    public func getCommonStringsLearnMoreSection3MessageFastingGlucose(party: VitalityParty?) -> String{
        return CommonStrings.LearnMore.Hba1cSection3Message242
    }
    
    /**
     * VHC Learn More
     * UrineProteinLearnMore.swift
     */
    public func getCommonStringsLearnMoreSection3MessageUrineProtein(party: VitalityParty?) -> String{
        if let birthdate = party?.person?.bornOn{
            
            /**
             * Use this code to simulate age 65.
             * birthdate = Calendar.current.date(byAdding: .year, value: -65, to: birthdate)!
             */
            
            if birthdate.getAge() >= 65{
                /**
                 * Based from ticket https://jira.vitalityservicing.com/browse/VA-17328
                 * Points should be 500 and 2000 for 65 and above
                 */
                return CommonStrings.LearnMore.UrineProteinSection3DynamicMessage348("500", "2,000")
            }
        }
        return CommonStrings.LearnMore.UrineProteinSection3DynamicMessage348("500", "1,500")
    }
    
    /**
     * VHC Learn More
     * CholesterolLearnMore.swift
     */
    public func getCholesterolAttributeGroupName() -> (String){
        return CommonStrings.Measurement.CholesterolTitle138
    }
    
    /**
     * VHC Landing HealthCare pdf
     */
    public func getHealthCarePDFTypeKey() -> AppConfigFeature.AppConfigFeatureType{
        return .VHCHealthcareBenefit
    }
    
    /**
     * SelectedPhotosCollectionViewController
     */
    public var shouldLimitNumberOfAttachProof: Bool?{
        get{
            return false
        }
    }
    
    /**Usage:
     * VHCCaptureSummaryViewModel.swift
     */
    public var removeEmptyCapturedData: Bool?{
        get{
            return true
        }
    }
    
    public func persistVHCData() -> Bool {
        return true
    }
    
    public func hideParticipatingPartners() -> Bool {
        return false
    }
    
    /**Usage:
     * VHCOptionListCaptureResultsCollectionViewCell.swift
     * VHCSingleCaptureResultsCollectionViewCell.swift
     * VHCBloodPressureInputSectionController.swift
     * VHCCaptureResultsCollectionViewController.swift
     * VHCCaptureResultsViewModel.swift
     * VHCOptionListInputSectionController.swift
     * VHCSingleMeasurableInputSectionController.swift
     */
    public func withDefaultStringValueForVHCCapturedDate() -> Bool {
        return true
    }
    
    /**
     * VHCBMICaptureResultsCollectionViewCell.swift
     * VHCBMIInputSectionController.swift
     */
    public func getBMIDelimiter() -> String {
        return "##"
    }
    
    public func initMultiUnitField(isValid: Bool?, topInputText: String?, bottomInputText: String?, numberFormatter: NumberFormatter, baseUnit: Double, completion: (String, String) -> Void) {
        var topValue = ""
        var bottomValue = ""
        if let input = topInputText{
            let tVal = String(Int((modf(numberFormatter.number(from: input)?.doubleValue ?? 0).0)))
            //let val = String(getDecimalFormatter().number(from: input)?.doubleValue ?? 0)
            topValue = tVal
        }
        if let input = bottomInputText{
            let bVal = String(Int(round((modf(numberFormatter.number(from: input)?.doubleValue ?? 0).1 * baseUnit))))
            //let val = String(getDecimalFormatter().number(from: input)?.doubleValue ?? 0 * baseUnit)
            bottomValue = bVal
        }
        
        return completion(topValue, bottomValue)
    }
    
    public func getPassedValue(top: Double, bottom: Double, baseUnit: Double, selectedUnitOfMeasureType: UnitOfMeasureRef, isTopInputValid: Bool, isBottomInputValid: Bool, completion: (Bool, String) -> Void) {
        var passedValue = ""
        var valid = isTopInputValid
        if (selectedUnitOfMeasureType == .FootInch) {
            passedValue = "\(top + (bottom/baseUnit))"    // 1 Foot = 12 inches
            valid = (isTopInputValid && isBottomInputValid)
        } else if (selectedUnitOfMeasureType == .StonePound) {
            passedValue = "\(top + (bottom/baseUnit))"    // 1 Stone = 14 Pounds
            valid = (isTopInputValid && isBottomInputValid)
        } else {
            passedValue = String(top)
        }
        
        return completion(valid, passedValue)
    }
    
    public func healthyRangeFeedback(from feedback: VHCHealthAttributeFeedback, isSystolicOrDiastolicHealthy: Bool, explanation: String?) -> VHCFeedback? {
        
        var healthyRangeImage = UIImage()
        var outOfHealthyRangeImage = UIImage()
        
        healthyRangeImage = VIAHealthCheckAsset.vhcGenericInHealthyRangeSmall.templateImage
        outOfHealthyRangeImage = VIAHealthCheckAsset.vhcGenericOutOfHealthyRangeSmall.templateImage
        
        switch feedback.type {
        case .BMIAbove, .BMIBelow, .DiastolicAbove, .FGlucoseOutRange, .HbA1cOutRange, .LDLAbove, .LDLBelow, .SystolicAbove, .TotalCholesterolHigh, .UrinaryProteinOut, .WaistCircumAbove, .WeightAbove, .WeightBelow, .LipidRatioHigh, .RandomGlucoseHigh:
            return VHCFeedback(image: outOfHealthyRangeImage, description: CommonStrings.Range.OutOfHealthyTitle191, explanation: feedback.feedbackTypeName, isInHealthyRange: false)
            
        case .BMIHealthy, .DiastolicHealthy, .FGlucoseInRange, .HbA1cInRange, .LDLHealthy, .SystolicHealthy, .TotalCholesterolGood, .UrinaryProteinIn, .WaistCircumHealthy, .WeightHealthy, .LipidRatioHealthy, .RandomGlucoseHealthy, .RandomGlucoseOk :
            if (feedback.type == .DiastolicHealthy || feedback.type == .SystolicHealthy) && !isSystolicOrDiastolicHealthy {
                if let explanationString = explanation {
                    return VHCFeedback(image: outOfHealthyRangeImage, description: CommonStrings.Range.OutOfHealthyTitle191, explanation: explanationString, isInHealthyRange: false)
                }
                return VHCFeedback(image: outOfHealthyRangeImage, description: CommonStrings.Range.OutOfHealthyTitle191, explanation: feedback.feedbackTypeName, isInHealthyRange: false)
            }
            return  VHCFeedback(image: healthyRangeImage, description: CommonStrings.Range.InHealthyTitle190, explanation: feedback.feedbackTypeName, isInHealthyRange: true)
        default:
            return nil
        }
    }
}
