//
//  File.swift
//  Sumitomo
//
//  Created by OJ Garde on 7/29/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import VIAStatus
import VitalityKit
import RealmSwift

public class VAAnnualStatusViewModel: VIAAnnualStatusViewModel{
    
    public override func potentialPointsForCategory(for index: Int, productCategories: Results<StatusProductFeatureCategory>?) -> String? {
        
        /*
         * Key 21 - Screenings
         * Don't display points for Screenings as per FC-604
         */
        guard let key = productCategories?[index].key, key != 21 else { return nil }
        
        guard index < productCategories?.count ?? 0 else {
            return nil
        }
        guard let potentialPoints = productCategories?[index].potentialPoints else {
            return nil
        }
        guard let pointEarnFlag = productCategories?[index].pointsEarningFlag else {
            return nil
        }
        let pointsCategoryLimit = productCategories?[index].pointsCategoryLimit.value ?? 0
        if pointEarnFlag == "EarnUpToPoints" {
            if pointsCategoryLimit > 0 {
                return CommonStrings.Status.PointsIndicationMessageUpTo828(String(describing: pointsCategoryLimit))
            }
            return CommonStrings.Status.PointsIndicationMessageUpTo828(String(describing: potentialPoints))
        } else if pointEarnFlag == "EarnUpToPointsLimits" {
            if pointsCategoryLimit > 0 {
                return CommonStrings.Status.PointsIndicationMessage829(String(describing: pointsCategoryLimit))
            }
            return CommonStrings.Status.PointsIndicationMessage829(String(describing: potentialPoints))
        } else if pointEarnFlag == "EarnPoints" {
            if pointsCategoryLimit > 0 {
                return CommonStrings.Status.PointsIndicationMessageLimit830(String(describing: pointsCategoryLimit))
            }
            return CommonStrings.Status.PointsIndicationMessageLimit830(String(describing: potentialPoints))
        }
        return String(describing: potentialPoints)
    }

    public override func totalPointsEarnedForCategory(for index: Int, productCategories: Results<StatusProductFeatureCategory>?) -> String? {
        
        /*
         * Key 21 - Screenings
         * Don't display points for Screenings as per FC-604
         */
        guard let key = productCategories?[index].key, key != 21 else { return nil }
        
        guard index < productCategories?.count ?? 0 else {
            return nil
        }
        guard let potentialPoints = productCategories?[index].potentialPoints else {
            return nil
        }
        guard let earnedPoints = productCategories?[index].pointsEarned else {
            return nil
        }
        if let categoryLimit = productCategories?[index].pointsCategoryLimit.value {
            if earnedPoints >= categoryLimit && categoryLimit > 0 {
                return CommonStrings.Status.PointsEarnedIndicationMessage896(String(describing: earnedPoints))
            }
        }
        if earnedPoints >= potentialPoints {
            return CommonStrings.Status.PointsEarnedIndicationMessage896(String(describing: earnedPoints))
        }
        return nil
    }    
}
