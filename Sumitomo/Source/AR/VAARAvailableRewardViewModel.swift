//
//  VAARAvailableRewardViewModel.swift
//  Sumitomo
//
//  Created by wenilyn.a.teorica on 02/05/2019.
//  Copyright © 2019 Glucode. All rights reserved.
//

import VitalityKit
import VIAUtilities
import RealmSwift
import VIAActiveRewards

public class VAARAvailableRewardViewModel: VIAARAvailableRewardViewModel {
    
    internal func filteredRewardToSwap() {
        
    }
    
    internal func availableRewardToSwap(for rewardId: Int) -> Results<ARRewardsSelection>? {
        
        var rewardReferences: [RewardReferences] = []
        
        switch rewardId {
        case RewardReferences.EGiftStarbucks.rawValue:
            rewardReferences = [RewardReferences.SmoothieLawson, RewardReferences.MachiLawson, RewardReferences.YogurtLawson]
            break
        case RewardReferences.SmoothieLawson.rawValue:
            rewardReferences = [RewardReferences.MachiLawson, RewardReferences.YogurtLawson]
            break
        case RewardReferences.YogurtLawson.rawValue:
            rewardReferences = [RewardReferences.MachiLawson]
            break
        default:
            rewardReferences = [RewardReferences.Unknown]
        }
        
        let predicate = NSPredicate(format: "rewardId IN %@", rewardReferences.map({$0.rawValue}))
        let filtered = arRealm.availableSwapRewards().first?.rewardSelections.filter(predicate)
        return filtered

    }
    
    public override func availableSwaps(for currentModel: VIAARRewardViewModel) -> [VIAARRewardViewModel] {
        var models = [VIAARRewardViewModel]()
        var rewardSelections: List<ARRewardsSelection>? = nil
        
        if let selections = availableRewardToSwap(for: currentModel.voucherId) {
            let sortSelections = selections.sorted(byKeyPath: "sortOrder", ascending: true)
            rewardSelections = self.toList(in: sortSelections, with: currentModel.rewardkey ?? 0)
        }
        
        guard let finalSelections = rewardSelections else {
            return []
        }
        
        let options = Array(finalSelections)
        for option in options {
            let value = VIAARRewardHelper.getValueForReward(option)
            
            models.append(VIAARRewardViewModel(id: option.id,
                                               unclaimedRewardId: option.unclaimedRewardId,
                                               subTitle: value,
                                               title: option.rewardName,
                                               image: VIAARRewardHelper.partnerImage(for: option.rewardId),
                                               voucherId: option.rewardId.rawValue,
                                               voucherCodes: nil,
                                               partnerSysRewardId: nil,
                                               instructions: VIAARRewardHelper.partnerRewardInstructions(for: .AvailableToRedeem, rewardValue: value, effectiveFromDate: nil),
                                               expiration: nil,
                                               effectiveFromDate: effectiveFrom,
                                               effectiveToDate: nil,
                                               accepted: nil,
                                               bespokeRewardBoardId: VIAARRewardHelper.bespokeViewController(for: option.rewardId),
                                               linkId: option.rewardValueLinkId,
                                               rewardReference: option.rewardId,
                                               rewardStatus: nil,
                                               rewardkey: option.rewardKey,
                                               awardedRewardStatusEffectiveOn: nil))
        }
        
        return models
    }
}
