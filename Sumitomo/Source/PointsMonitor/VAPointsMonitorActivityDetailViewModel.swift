//
//  VAPointsMonitorActivityDetailViewModel.swift
//  Sumitomo
//
//  Created by wenilyn.a.teorica on 09/05/2019.
//  Copyright © 2019 Glucode. All rights reserved.
//

import VitalityKit
import VIACommon
import VIAPointsMonitor

public class VAPointsMonitorActivityDetailViewModel: PointsMonitorActivityDetailViewModel {
    
    public override func buildUpDetailsFromPointsEntry() {
        
        details.append(Detail(header: CommonStrings.Pm.ActivityDetailSectionSubheading527, description: pointsEntry.typeName))

        for metadata in pointsEntry.metadatas {
            if pointsEntry.typeCode == "Steps" && (metadata.typeCode == "Duration" || metadata.typeCode == "AverageHeartRate") {
                //FC-25207: SLI UAT: Points: Please remove Duration and Ave HR on steps detail screen.
            } else {
                let header = (metadata.typeName ?? metadata.typeCode) ?? ""
                var description = metadata.format()

                if let convertJoulesToCalories = VIAApplicableFeatures.default.convertJoulesToCalories, convertJoulesToCalories{
                    if metadata.typeCode == "EnergyExpenditure" {
                        if let joulesValue = Localization.decimalFormatter.number(from: metadata.value){
                            let calorieValue = UnitEnergy.calories.converter.value(fromBaseUnitValue: joulesValue.doubleValue)
                            description = Localization.decimalFormatter.string(from: calorieValue as NSNumber)!
                        }
                    }
                } else {
                    if let value = Localization.decimalFormatter.number(from: metadata.value){
                        description = Localization.decimalFormatter.string(from: value as NSNumber)!
                    }
                }
                
                if metadata.typeCode == "Duration" {
                    if let duration = String(metadata.value) {
                        description = convertDuration(fromSeconds: duration)
                    }
                }
                
                details.append(Detail(header: header, description: description))
            }
        }
        
        let date = dateFormatter.string(from: pointsEntry.effectiveDate)
        details.append(Detail(header: CommonStrings.DateTitle264, description: date))

    }
}
