//
//  AppDelegate+ApplicableFeatures+OFE.swift
//  VitalityActive
//
//  Created by Val Tomol on 02/05/2018.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import VIACommon
import VitalityKit

extension AppDelegate {
    /**
     * Usage:
     * OFEClaimPointsViewController.swift
     * OFESelectedPhotosCollectionViewController.swift
     * OFEAddWeblinkProofViewController.swift
     * OFESummaryViewController.swift
     **/
    public func includeAllProofs() -> Bool {
        return true
    }
    
    /**
     * Usage:
     * OFEHistoryViewController
     **/
    public func getOFEFormattedDate(dateString: String) -> String{
        
//        let formatter = DateFormatter()
//        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'[z]"
//        let date = formatter.date(from: dateString)
//        
//        let displayFormatter = DateFormatter()
//        displayFormatter.dateFormat = "E, dd MMM YYYY"
//        
//        return displayFormatter.string(from: date!)
        
        // POSSIBLE THIS WILL BE CHANGE AGAIN.. 
        
        let index = dateString.index(dateString.startIndex, offsetBy: 19)
        let mySubstring = dateString.substring(to: index)
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        formatter.locale = Locale(identifier: DeviceLocale.toString())
        let date = formatter.date(from: mySubstring)
        
        let displayFormatter: DateFormatter = {
            let dateFormatter = DateFormatter.dayDateShortMonthyearFormatter()
            return dateFormatter
        }()
        
        return displayFormatter.string(from: date!)
    }
    
    
    /**
     * Usage:
     * OFEHistoryDetailsViewController
     **/
    public func configureOFEHistoryDetailsRow() -> Bool{
        return true
    }
    
    /**
     * Usage:
     * OFEEventTypeViewController
     * OFEClaimPointsViewController
     **/
    public func withDefaultStringValueForOFEEventDate() -> Bool{
        return true
    }
}
