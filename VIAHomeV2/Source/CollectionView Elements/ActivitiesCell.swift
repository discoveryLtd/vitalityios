//
//  ActivitiesCell.swift
//  VIAHomeV2
//
//  Created by Dexter Anthony Ambrad on 3/6/19.
//  Copyright © 2019 Glucode. All rights reserved.
//

import UIKit

class ActivitiesCell: UICollectionViewCell {
    /*
    // MARK: Properties
    let containerView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        return view
    }()
    
    let imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.image = UIImage(named: "heathDataCategoryLogo", in: Bundle(identifier: "com.vitalityactive.VIACore"), compatibleWith: nil)
        return imageView
    }()
    
    let trackButton: UIButton = {
        let button = UIButton()
        button.setTitle("Track", for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 13)
        button.backgroundColor = UIColor.darkGray
        button.setTitleColor(UIColor.white, for: .normal)
        button.layer.cornerRadius = 5
        return button
    }()
    
    let stackView: UIStackView = {
        let view = UIStackView(frame: .zero)
        view.axis = .vertical
        view.distribution = .equalSpacing
        view.alignment = .leading
        view.spacing = 7
        return view
    }()
    
    var header: UILabel = {
        let label = UILabel()
        //        label.text = "Get active 1 more day this week"
        label.font = UIFont.boldSystemFont(ofSize: 18)
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 0
        label.textColor = UIColor.black
        
        return label
    }()
    
    let expiryLabel: UILabel = {
        let label = UILabel()
        label.text = "Expire 10/15/19"
        label.textColor = UIColor.black
        label.font = UIFont.systemFont(ofSize: 10)
        return label
    }()
    
    let spinLabel: UIButton = {
        let button = UIButton()
        button.setTitle("1 Spin", for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 13)
        button.backgroundColor = UIColor.gray
        button.setTitleColor(UIColor.white, for: .normal)
        button.layer.cornerRadius = 13
        button.contentEdgeInsets = UIEdgeInsets(top: 5, left: 10, bottom: 5, right: 10)
        button.isUserInteractionEnabled = false
        return button
    }()
    
    let cellSeperator: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.lightGray.withAlphaComponent(0.1)
        return view
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupContainerView()
        setupContent()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: Config
    func setupContainerView() {
        self.contentView.addSubview(containerView)
        containerView.snp.makeConstraints { (make) in
            make.top.bottom.left.right.equalTo(0)
        }
    }
    
    func setupContent() {
        containerView.addSubview(imageView)
        imageView.snp.makeConstraints { (make) in
            make.left.equalTo(30)
            make.top.equalTo(10)
            make.width.height.equalTo(40)
        }
        
        containerView.addSubview(cellSeperator)
        cellSeperator.snp.makeConstraints { (make) in
            make.bottom.equalToSuperview()
            make.right.left.equalToSuperview().inset(30)
            make.height.equalTo(1)
        }
        
        containerView.addSubview(trackButton)
        trackButton.snp.makeConstraints { (make) in
            make.right.equalTo(-30)
            make.centerY.equalTo(imageView.snp.centerY)
            make.width.equalTo(70)
            make.height.equalTo(25)
        }
        
        containerView.addSubview(stackView)
        stackView.snp.makeConstraints { (make) in
            make.top.equalTo(imageView.snp.top)
            make.left.equalTo(imageView.snp.right).inset(-18)
            make.right.equalTo(trackButton.snp.left).inset(-20)
        }
        
        stackView.addArrangedSubview(header)
        stackView.addArrangedSubview(expiryLabel)
        stackView.addArrangedSubview(spinLabel)
        
    }
    */
}
