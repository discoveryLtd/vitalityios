//
//  HeaderCell.swift
//  VIAHomeV2
//
//  Created by Dexter Anthony Ambrad on 3/6/19.
//  Copyright © 2019 Glucode. All rights reserved.
//

import UIKit

class HeaderCell: UICollectionViewCell {
    /*
    // MARK: Properties
    let containerView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.lightGray
        return view
    }()
    
    let headerLabel: UILabel = {
        let label = UILabel()
        label.text = "5 Ways to be prepared this cold and flu season"
        label.font = UIFont.boldSystemFont(ofSize: 20)
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 0
        label.textColor = UIColor.white
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupContainerView()
        setupHeaderLabel()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: Config
    func setupContainerView() {
        self.contentView.addSubview(containerView)
        containerView.snp.makeConstraints { (make) in
            make.top.bottom.left.right.equalToSuperview()
        }
    }
    
    func setupHeaderLabel() {
        containerView.addSubview(headerLabel)
        headerLabel.snp.makeConstraints { (make) in
            make.left.equalTo(0).inset(20)
            make.bottom.equalTo(0).inset(10)
            make.width.equalTo(containerView.snp.width)
            make.height.equalTo(50)
        }
    }
    */
}
