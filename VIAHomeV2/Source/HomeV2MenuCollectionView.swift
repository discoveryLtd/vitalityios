//
//  HomeV2MenuCollectionView.swift
//  VIAHomeV2
//
//  Created by Dexter Anthony Ambrad on 3/6/19.
//  Copyright © 2019 Glucode. All rights reserved.
//

import UIKit

public class HomeV2MenuCollectionView: UIView, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    // MARK: Properties
    lazy var menuCollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let collectionView = UICollectionView.init(frame: .zero, collectionViewLayout: layout)
        collectionView.backgroundColor = UIColor.clear
        collectionView.dataSource = self
        collectionView.delegate = self
        return collectionView
    }()
    
    let horizontalBarView: UIView = {
        let horizontalBar = UIView()
        horizontalBar.backgroundColor = .vitalityOrange()
        horizontalBar.translatesAutoresizingMaskIntoConstraints = false
        return horizontalBar
    }()
    
    var horizontalBarLeftConstraint: NSLayoutConstraint?
    var homeV2ScreenViewController: HomeV2ScreenViewController?
    
    let cellId = "cellId"
    let menuLabel = ["Activities", "Rewards"]
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        menuCollectionView.register(MenuCell.self, forCellWithReuseIdentifier: cellId)
        setupCollectionView()
        
        let selectedIndexPath = IndexPath(item: 0, section: 0)
        DispatchQueue.main.async {
            self.menuCollectionView.selectItem(at: selectedIndexPath, animated: false, scrollPosition: [])
        }
        
        setupHorizontalBar()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: CollectionView Delegate
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return menuLabel.count
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = menuCollectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! MenuCell
        cell.backgroundColor = .day()
        cell.menuLabel.text = menuLabel[indexPath.row]
        
        return cell
    }
    public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let leftAnchor = CGFloat(indexPath.item) * (self.frame.width / CGFloat(self.menuLabel.count)
        )
        horizontalBarLeftConstraint?.constant = leftAnchor
        //        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
        //            self.layoutIfNeeded()
        //        }, completion: nil)
        
        homeV2ScreenViewController?.chooseTab(index: indexPath.item)
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: frame.width / CGFloat(menuLabel.count), height: frame.height)
    }
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    // MARK: Config
    func setupCollectionView() {
        addSubview(menuCollectionView)
        menuCollectionView.snp.makeConstraints { (make) in
            make.left.right.top.bottom.equalTo(0)
        }
    }
    
    func setupHorizontalBar() {
        addSubview(horizontalBarView)
        horizontalBarLeftConstraint = horizontalBarView.leftAnchor.constraint(equalTo: self.leftAnchor)
        horizontalBarLeftConstraint?.isActive = true
        horizontalBarView.snp.makeConstraints { (make) in
            make.bottom.equalTo(self.snp.bottom)
            make.width.equalTo(self.snp.width).dividedBy(menuLabel.count)
            make.height.equalTo(4)
        }
    }
    
}

class MenuCell: UICollectionViewCell {
    
    // MARK: Properties
    let menuLabel: UILabel = {
        let label = UILabel()
        label.textColor = .genericInactive()
        label.font = UIFont.boldSystemFont(ofSize: 16)
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    override var isHighlighted: Bool {
        didSet {
            menuLabel.textColor = isHighlighted ? .vitalityOrange() : .genericInactive()
        }
    }
    
    override var isSelected: Bool {
        didSet {
            menuLabel.textColor = isSelected ? .vitalityOrange() : .genericInactive()
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: Config
    func setupViews() {
        addSubview(menuLabel)
        menuLabel.snp.makeConstraints { (make) in
            make.center.equalTo(self.snp.center)
        }
    }
    
    
}
