//
//  HomeV2ScreenViewController.swift
//  VIAHomeV2
//
//  Created by Dexter Anthony Ambrad on 3/6/19.
//  Copyright © 2019 Glucode. All rights reserved.
//

import VIACommon
import Foundation
import VIAUIKit
import UIKit
import IGListKit
import SnapKit
import VIAHealthKit
import VitalityKit
import VIAUtilities
import VIAAssessments
//import VIAActiveRewards
import VIAStatus
import VIAPartners
import TTGSnackbar
import RealmSwift
import VIACore

class HomeV2ScreenViewController: VIAViewController {
    
    // MARK: Properties
    lazy var menuBar: HomeV2MenuCollectionView = {
        let mb = HomeV2MenuCollectionView()
        mb.homeV2ScreenViewController = self
        return mb
    }()
    
    lazy var activitiesTabView: ActivitiesTabView = {
        let activities = ActivitiesTabView()
        activities.homeV2ScreenController = self
        return activities
    }()
    
    lazy var rewardsTabView: RewardsTabView = {
        let rewards = RewardsTabView()
        rewards.homeV2ScreenController = self
        return rewards
    }()
    
    // MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let imageView = UIImageView(frame:CGRect(x: 0, y: 0, width: 54, height: 31))
        imageView.contentMode = .scaleAspectFit
        imageView.image = VIAIconography.default.homeLogo(for: Locale.current)
        navigationItem.titleView = imageView
        
        setupMenuBar()
        setupActivitiesTab()
        setupRewardsTab()
        
        self.chooseTab(index: 0)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        configureAppearance()
    }
    
    // MARK: Functions
    func chooseTab(index: Int) {
        if index == 0 {
            activitiesTabView.isHidden = false
            rewardsTabView.isHidden = true
        } else {
            activitiesTabView.isHidden = true
            rewardsTabView.isHidden = false
        }
    }
    
    // MARK: Config
    private func setupMenuBar() {
        view.addSubview(menuBar)
        menuBar.snp.makeConstraints { (make) in
            make.top.equalTo(view.safeAreaLayoutGuide.snp.top)
            make.left.right.equalTo(0)
            make.height.equalTo(50)
        }
    }
    
    private func setupActivitiesTab() {
        view.addSubview(activitiesTabView)
        activitiesTabView.snp.makeConstraints { (make) in
            make.top.equalTo(menuBar.snp.bottom)
            make.bottom.equalTo(view.safeAreaLayoutGuide.snp.bottom)
            make.left.right.equalTo(0)
        }
    }
    
    private func setupRewardsTab() {
        view.addSubview(rewardsTabView)
        rewardsTabView.snp.makeConstraints { (make) in
            make.top.equalTo(menuBar.snp.bottom)
            make.bottom.equalTo(view.safeAreaLayoutGuide.snp.bottom)
            make.left.right.equalTo(0)
        }
    }
    
    func configureAppearance() {
        navigationController?.navigationBar.isHidden = false
        navigationController?.makeNavigationBarTransparent()
        navigationController?.navigationBar.setBackgroundImage(VIACoreAsset.NavBar.transparentPixel.image , for: .default)
        navigationController?.navigationBar.shadowImage = VIACoreAsset.NavBar.transparentPixel.image
        view.backgroundColor = .day()
    }
    
}
