version=`/usr/libexec/PlistBuddy -c "Print CFBundleShortVersionString" "$SRCROOT/$TARGETNAME/Info.plist"`
build=`/usr/libexec/PlistBuddy -c "Print CFBundleVersion" "$SRCROOT/$TARGETNAME/Info.plist"`

/usr/libexec/PlistBuddy "$SRCROOT/$TARGETNAME/Settings.bundle/Root.plist" -c "set PreferenceSpecifiers:0:DefaultValue $version ($build)"