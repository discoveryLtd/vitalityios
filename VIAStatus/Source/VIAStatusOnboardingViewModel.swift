import VitalityKit
import VitalityKit
import VIAUIKit

public class VIAStatusOnboardingViewModel: AnyObject, OnboardingViewModel {

    public var heading: String {
        return CommonStrings.Status.OnboardingTitle602
    }

    public var contentItems: [OnboardingContentItem] {
        var items = [OnboardingContentItem]()

        items.append(OnboardingContentItem(
            heading: CommonStrings.Status.OnboardingSection1Title603,
            content: CommonStrings.Status.OnboardingSection1Message604,
            image: VIAUIKitAsset.Status.statusHelp.image))

        items.append(OnboardingContentItem(
            heading: CommonStrings.Status.OnboardingSection2Title605,
            content: CommonStrings.Status.OnboardingSection2Message606,
            image: VIAUIKitAsset.Status.statusPointsSmall.image))

        return items
    }

    public var buttonTitle: String {
        return CommonStrings.GenericGotItButtonTitle131
    }

    public var alternateButtonTitle: String? {
        return nil
    }

    public var gradientColor: Color {
        return .green
    }

    public var labelTextColor: UIColor {
        return UIColor.genericActive()
    }

    public var mainButtonTint: UIColor {
        return UIColor.primaryColor()
    }

    public var mainButtonHighlightedTextColor: UIColor? {
        return UIColor.day()
    }
}
