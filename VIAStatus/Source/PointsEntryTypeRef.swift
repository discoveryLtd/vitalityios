import VIAUtilities
import VitalityKit

extension PointsEntryTypeRef {
    func string(value: String) -> String? {
        switch self {
        case .Unknown:
            return nil
        case .BMI:
            return CommonStrings.Status.Pointsentry.Bmi857
        case .BMIHR:
            return CommonStrings.Status.Pointsentry.Bmihr858
        case .BloodPressure:
            return CommonStrings.Status.Pointsentry.BloodPressure859
        case .BloodPressureHR:
            return CommonStrings.Status.Pointsentry.BloodPressureHR860
        case .CholesterolHR:
            return CommonStrings.Status.Pointsentry.CholesterolHR861
        case .CotinineTest:
            return CommonStrings.Status.EarnPoints.CotinineTestPass1186
        case .AbdomAorticUS:
            return CommonStrings.Status.EarnPoints.AbdominalAorticUltrasound1178
        case .Cycling:
            return CommonStrings.Status.Pointsentry.Cycling862
        case .DeviceLinking:
            return CommonStrings.Status.Pointsentry.DeviceLinking895
        case .Distance:
            return CommonStrings.Status.Pointsentry.Distance863
        case .EnergyExpend:
            return CommonStrings.Status.Pointsentry.EnergyExpend864
        case .DentalCheckups:
            return CommonStrings.Status.EarnPoints.DentalCheckup1180
        case .EyeTest:
            return CommonStrings.Status.EarnPoints.EyeTest1177
        case .FitHR:
            return CommonStrings.Status.Pointsentry.MeasuredFitHealthyRange880
        case .BloodGlucose:
            return CommonStrings.Status.Pointsentry.BloodGlucose865
        case .GlucoseHR:
            return CommonStrings.Status.Pointsentry.GlucoseHR866
        case .GymVisit:
            return CommonStrings.Status.Pointsentry.GymVisit867
        case .HbA1c:
            return CommonStrings.Status.Pointsentry.HbA1c868
        case .HbA1cHR:
            return CommonStrings.Status.Pointsentry.HbA1cHR869
        case .HealthyFoodSpend:
            return CommonStrings.Status.Pointsentry.HealthyFoodSpend870
        case .Heartrate:
            return CommonStrings.Status.Pointsentry.Heartrate871
        case .LDLCholesterol:
            return CommonStrings.Status.Pointsentry.LdlCholesterol872
        case .LDLCholesterolHR:
            return CommonStrings.Status.Pointsentry.LdlCholesterolHR873
        case .LipidRatio:
            return CommonStrings.Status.Pointsentry.LipidRatio874
        case .LipidRatioHR:
            return CommonStrings.Status.Pointsentry.LipidRatioHR875
        case .MWBPsychological:
            return CommonStrings.Status.Pointsentry.MwbPsychological876
        case .MWBSocial:
            return CommonStrings.Status.Pointsentry.MwbSocial877
        case .MWBStressor:
            return CommonStrings.Status.Pointsentry.MwbStressor878
        case .ManualPoints:
            return CommonStrings.Status.Pointsentry.ManualPointsAlloction879
        case .MeasuredFitComplete:
            return CommonStrings.Status.Pointsentry.MeasuredFitComplete880
        case .NonsmokersDeclrtn:
            return CommonStrings.Status.Pointsentry.NonsmokersDeclarationn881
        case .OFE:
            return CommonStrings.Status.Pointsentry.Ofe882
        case .ParkrunParticipation:
            return CommonStrings.Status.PointsParticipatingParkrun2096(value)
        case .ParkrunVolunteer:
            return CommonStrings.Status.PointsVolunteeringParkrun2097(value)
        case .Running:
            return CommonStrings.Status.Pointsentry.Running883
        case .Speed:
            return CommonStrings.Status.Pointsentry.Speed884
        case .Steps:
            return CommonStrings.Status.Pointsentry.Steps885
        case .Swimming:
            return CommonStrings.Status.Pointsentry.Swimming886
        case .TotCholesterol:
            return CommonStrings.Status.Pointsentry.TotCholesterol887
        case .Triathlon:
            return CommonStrings.Status.Pointsentry.Triathlon888
        case .UrineProtien:
            return CommonStrings.Status.Pointsentry.UrineProtien889
        case .UrineProtienHR:
            return CommonStrings.Status.Pointsentry.UrineProtienHR890
        case .VHRAssmntCompleted:
            return CommonStrings.Status.Pointsentry.VhrAssmntCompleted891
        case .VNAAssmntCompleted:
            return CommonStrings.Status.Pointsentry.VnaAssmntCompleted892
        case .Walking:
            return CommonStrings.Status.Pointsentry.Walking893
        case .Mammography:
            return CommonStrings.Status.Pointsentry.Mammogram2109
        case .GastricCancer:
            return CommonStrings.Status.Pointsentry.StomachCancer2108
        case .Colonoscopy:
            return CommonStrings.Status.Pointsentry.ColonCancer2106
        case .PapSmear:
            return CommonStrings.Status.Pointsentry.PapSmear2110
        case .LungCancer:
            return CommonStrings.Status.Pointsentry.LungCancer2107
        case .Pneumococcal:
            return CommonStrings.Status.Pointsentry.Pneumococcus2111
        default:
            return nil
        }
    }
    
    func image() -> UIImage? {
        switch self {
        case .Unknown:
            return VIAStatusAsset.genericPointsGreen.image
        case .BMI:
            return VIAStatusAsset.bmi.image
        case .BMIHR:
            return VIAStatusAsset.bmi.image
        case .BloodPressure:
            return VIAStatusAsset.bloodPressure.image
        case .BloodPressureHR:
            return VIAStatusAsset.bloodPressure.image
        case .CholesterolHR:
            return VIAStatusAsset.cholesterol.image
        case .CotinineTest:
            return VIAStatusAsset.screening.image
        case .AbdomAorticUS:
            return VIAStatusAsset.screening.image
        case .DentalCheckups:
            return VIAStatusAsset.screening.image
        case .Pneumococcal:
            return VIAStatusAsset.screening.image
        case .Cycling:
            return VIAStatusAsset.genericPointsBlue.image
        case .DeviceLinking:
            return VIAStatusAsset.deviceLinking.image
        case .Distance:
            return VIAStatusAsset.distance.image
        case .EnergyExpend:
            return VIAStatusAsset.calories.image
        case .EyeTest:
            return VIAStatusAsset.screening.image
        case .BloodGlucose:
            return VIAStatusAsset.glucose.image
        case .GlucoseHR:
            return VIAStatusAsset.glucose.image
        case .GymVisit:
            return VIAStatusAsset.genericPointsBlue.image
        case .HbA1c:
            return VIAStatusAsset.hba1c.image
        case .HbA1cHR:
            return VIAStatusAsset.hba1c.image
        case .HealthyFoodSpend:
            return VIAStatusAsset.nutrition.image
        case .Heartrate:
            return VIAStatusAsset.heartRate.image
        case .LDLCholesterol:
            return VIAStatusAsset.cholesterol.image
        case .LDLCholesterolHR:
            return VIAStatusAsset.cholesterol.image
        case .LipidRatio:
            return VIAStatusAsset.assessment.image
        case .LipidRatioHR:
            return VIAStatusAsset.assessment.image
        case .MWBPsychological:
            return VIAStatusAsset.assessment.image
        case .MWBSocial:
            return VIAStatusAsset.assessment.image
        case .MWBStressor:
            return VIAStatusAsset.assessment.image
        case .ManualPoints:
            return VIAStatusAsset.genericPointsBlue.image
        case .MeasuredFitComplete:
            return VIAStatusAsset.genericPointsBlue.image
        case .NonsmokersDeclrtn:
            return VIAStatusAsset.nonSmokers.image
        case .OFE:
            return VIAStatusAsset.genericPointsBlue.image
        case .ParkrunParticipation:
            return VIAStatusAsset.heartRate.image
        case .ParkrunVolunteer:
            return VIAStatusAsset.heartRate.image
        case .Running:
            return VIAStatusAsset.genericPointsBlue.image
        case .Speed:
            return VIAStatusAsset.speed.image
        case .Steps:
            return VIAStatusAsset.steps.image
        case .Swimming:
            return VIAStatusAsset.genericPointsBlue.image
        case .TotCholesterol:
            return VIAStatusAsset.cholesterol.image
        case .Triathlon:
            return VIAStatusAsset.genericPointsBlue.image
        case .UrineProtien:
            return VIAStatusAsset.urineProtein.image
        case .UrineProtienHR:
            return VIAStatusAsset.urineProtein.image
        case .VHRAssmntCompleted:
            return VIAStatusAsset.assessment.image
        case .VNAAssmntCompleted:
            return VIAStatusAsset.assessment.image
        case .Walking:
            return VIAStatusAsset.genericPointsBlue.image
        default:
            return nil
        }
    }
}
