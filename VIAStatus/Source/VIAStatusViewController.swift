import Foundation
import VIAUIKit
import VitalityKit
import VIAUtilities
import VIACommon

import SnapKit
import RealmSwift

public enum MenuItems: Int, EnumCollection {
    case learnMore = 0
    case help = 1
    
    func title() -> String? {
        switch self {
        case .learnMore:
            return CommonStrings.LearnMoreButtonTitle104
        case .help:
            return CommonStrings.HelpButtonTitle141
        }
    }
    
    func image() -> UIImage? {
        switch self {
        case .learnMore:
            return VIAStatusAsset.learnMore.templateImage
        case .help:
            return VIAStatusAsset.questionMark.templateImage
        }
    }
    
    func segueIdentifier() -> String {
        switch self {
        case .learnMore:
            return "showLearnMore"
        case .help:
            return "showHelp"
        }
    }
}

public func menuRows() -> NSMutableIndexSet {
    let indexSet = NSMutableIndexSet()
    indexSet.add(MenuItems.learnMore.rawValue)
    if !VIAApplicableFeatures.default.hideHelpTab! {
        indexSet.add(MenuItems.help.rawValue)
    }
    return indexSet
}

public enum Sections: Int, EnumCollection {
    case Status = 0
    case Annual = 1
    case Info = 2
    case PointsEarning = 3
    case Menu = 4
}

class VIAStatusViewController: VIATableViewController, PrimaryColorTintable {
    
    // MARK: Private
    // TODO: Pull values from CMS and API
    private var pointsEarningRequestSentAndFailed: Bool = false
    private var currentStatus: VIAStatusViewModel?
    private var annualStatus: VIAAnnualStatusViewModel?
    
    // MARK: View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.configureAppearance()
        self.configureTableView()
        showOnboardingIfNeeded()
        self.hideBackButtonTitle()
        annualStatus = VIAApplicableFeatures.default.getAnnualStatusViewModel() as? VIAAnnualStatusViewModel
        loadEarningPointsData()
    }
    
    func loadEarningPointsData() {
        annualStatus?.requestProductFeaturePoints(completion: { [weak self] (error) in
            guard error == nil else {
                self?.pointsEarningRequestSentAndFailed = true
                self?.tableView.reloadSections([Sections.PointsEarning.rawValue], with: UITableViewRowAnimation.automatic)
                return
            }
            self?.pointsEarningRequestSentAndFailed = false
            self?.annualStatus?.fetchProductCategories()
            self?.tableView.reloadSections([Sections.PointsEarning.rawValue], with: UITableViewRowAnimation.automatic)
        })
    }
    
    func configureAppearance() {
        navigationController?.makeNavigationBarTransparent()
        self.title = CommonStrings.Status.LandingMainTitleStatus796
    }
    
    func showOnboardingIfNeeded() {
        if !AppSettings.hasShownVitalityStatusOnboarding() {
            self.performSegue(withIdentifier: "showVitalityStatusOnboarding", sender: nil)
        }
    }
    
    func configureTableView() {
        self.tableView.register(VIATableViewCell.nib(), forCellReuseIdentifier: VIATableViewCell.defaultReuseIdentifier)
        self.tableView.register(PointsEarningTableViewCell.nib(), forCellReuseIdentifier: PointsEarningTableViewCell.defaultReuseIdentifier)
        self.tableView.register(VIAStatusStepCellView.nib(), forCellReuseIdentifier: VIAStatusStepCellView.defaultReuseIdentifier)
        self.tableView.register(VIAStatusAnnualDetailCellView.nib(), forCellReuseIdentifier: VIAStatusAnnualDetailCellView.defaultReuseIdentifier)
        self.tableView.register(VIATableViewSectionHeaderFooterView.nib(), forHeaderFooterViewReuseIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier)
        self.tableView.register(VIAStatusDetailCellView.nib(), forCellReuseIdentifier: VIAStatusDetailCellView.defaultReuseIdentifier)
        self.tableView.register(ActivityIndicatorTableViewCell.nib(), forCellReuseIdentifier: ActivityIndicatorTableViewCell.defaultReuseIdentifier)
        self.tableView.estimatedRowHeight = 75
    }
    
    // MARK: UITableView datasource
    override func numberOfSections(in tableView: UITableView) -> Int {
        return Sections.allValues.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let theSection = Sections(rawValue: section) {
            if theSection == .PointsEarning {
                if annualStatus?.numberOfRows(for: theSection) == 0 {
                    return 1
                } else {
                    return annualStatus?.numberOfRows(for: theSection) ?? 0
                }
            }
            return annualStatus?.numberOfRows(for: theSection) ?? 0
        }
        return 0
    }
    
    // MARK: UITableView delegate
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let theSection = Sections(rawValue: indexPath.section)
        if theSection == .Status {
            return configureStatusCell(indexPath)
        } else if theSection == .Annual {
            return configureAnnualInfoCell(indexPath)
        } else if theSection == .PointsEarning {
            if annualStatus?.productCategoryCount() == 0 {
                return configureActivityCell(indexPath)
            }
            return configureEarningPointsCell(indexPath)
        } else if theSection == .Info {
            return configureStepInfoCell(indexPath)
        } else if theSection == .Menu {
            return configureMenuCell(indexPath)
        }
        
        return tableView.defaultTableViewCell()
    }
    
    func configureActivityCell(_ indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ActivityIndicatorTableViewCell.defaultReuseIdentifier, for: indexPath) as? ActivityIndicatorTableViewCell else {
            return tableView.defaultTableViewCell()
        }
        
        if pointsEarningRequestSentAndFailed {
            cell.activityLabel.textColor = UIColor.primaryColor()
            cell.activityLabel.text = CommonStrings.TryAgainButtonTitle43
            cell.activityIndicatorImageView.image = VIAStatusAsset.refresh.templateImage
        } else {
            let activityIndicator = UIActivityIndicatorView()
            activityIndicator.frame = cell.activityIndicatorImageView.frame
            activityIndicator.frame.origin = CGPoint(x: 0, y: 0)
            activityIndicator.startAnimating()
            activityIndicator.color = UIColor.night()
            cell.activityLabel.text = CommonStrings.Status.Landing.LoadingText894
            cell.activityIndicatorImageView.addSubview(activityIndicator)
        }
        return cell
    }
    
    func configureStatusCell(_ indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: VIAStatusDetailCellView.defaultReuseIdentifier, for: indexPath) as! VIAStatusDetailCellView
        
        cell.title = annualStatus?.nameOfCurrentStatus()
        cell.cellBody = getStatusCellBody()
        cell.badge = annualStatus?.currentStatusImage()
        cell.setProgress(as: annualStatus?.statusProgress() ?? 0.0, of: self.tableView.frame.width)
        cell.selectionStyle = .none
        return cell
    }
    
    func getStatusCellBody() -> String? {
        guard let pointsToMaintainStatus: Int = annualStatus?.pointsToMaintainStatus() else {
            return nil
        }
        //no carry over status
        //no next status
        guard let nextStatusPointsAccount = annualStatus?.pointsToNextStatus(),
            let nextStatusName: String = annualStatus?.nameOfNextStatus()
            else {
                return CommonStrings.Status.LandingPointsFinalMessage824
        }
        //Carry over status
        if pointsToMaintainStatus > 0, let carryOverStatusName = annualStatus?.carryOverStatus() {
            guard let formattedPointsToMaintainStatus = Localization.integerFormatter.string(from: NSNumber(integerLiteral: pointsToMaintainStatus)) else {
                return nil
            }
            return CommonStrings.Status.LandingPointsMaintainMessage819(formattedPointsToMaintainStatus, carryOverStatusName)
        }
        if annualStatus?.pointsToNextStatus() == 0 {
            return CommonStrings.Status.LandingPointsFinalMessage824
        } else {
            guard let formattedPointsToNextStatus = Localization.integerFormatter.string(from: NSNumber(integerLiteral: nextStatusPointsAccount)) else {
                return nil
            }
            return  CommonStrings.Status.LandingPointsTargetMessage797(formattedPointsToNextStatus, nextStatusName)
        }
    }
    
    func configureAnnualInfoCell(_ indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: VIAStatusAnnualDetailCellView.defaultReuseIdentifier, for: indexPath) as! VIAStatusAnnualDetailCellView
        if let annualStatus = annualStatus {
            cell.title = CommonStrings.Status.LandingPointsTotalTitle799
            cell.cellBody = annualStatus.descriptionForStatus()
            cell.points = String(annualStatus.currentStatusPointsCount() ?? 0)
        }
        cell.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        cell.selectionStyle = .none
        return cell
    }
    
    func configureEarningPointsCell(_ indexPath: IndexPath) -> UITableViewCell {
        guard let cell = self.tableView.dequeueReusableCell(withIdentifier: PointsEarningTableViewCell.defaultReuseIdentifier, for: indexPath) as? PointsEarningTableViewCell else {
            return tableView.defaultTableViewCell()
        }
        cell.titleLogo.image = annualStatus?.imageForCategory(for: indexPath.row)
        cell.title.text = annualStatus?.productCategoryName(for: indexPath.row)
        
        cell.message.text = annualStatus?.potentialPointsForCategory(for: indexPath.row, productCategories: annualStatus?.productCategories)
        cell.messageImageView.isHidden = true
        if let totalPointsEarnedString = annualStatus?.totalPointsEarnedForCategory(for: indexPath.row, productCategories: annualStatus?.productCategories) {
            cell.showTotalPotenialPointsEaerned()
            cell.messageImageView.isHidden = false
            cell.messageImageView.image = VIAStatusAsset.checkmark.image
            cell.totalPotentialPointsEarnedImageView.isHidden = false
            cell.totalPotentialPointsEarnedImageView.image = VIAStatusAsset.medal.image
            cell.message.text = CommonStrings.Status.LandingEarningPointsLimitMessage822
            cell.totalPotentialPointsEarnedLabel.text = totalPointsEarnedString
        }
        //Image width = 25 + 15 padding either side = 55
        cell.separatorInset = UIEdgeInsets(top: 0, left: 55 , bottom: 0, right: 0)
        cell.accessoryType = .disclosureIndicator
        return cell
    }
    
    func configureStepInfoCell(_ indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: VIAStatusStepCellView.defaultReuseIdentifier, for: indexPath) as! VIAStatusStepCellView
        cell.title = annualStatus?.possibleStatusName(at: indexPath.row)
        cell.cellBody = annualStatus?.possibleStatusBodyContent(at: indexPath.row)
        cell.badge = annualStatus?.possibleStatusImage(for: indexPath.row)
        cell.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: .greatestFiniteMagnitude)
        cell.selectionStyle = .none
        if indexPath.row == 0 {
            cell.first = true
            cell.last = false
        } else if indexPath.row == (annualStatus?.possibleStatusesCount() ?? 0) - 1 {
            cell.first = false
            cell.last = true
        } else {
            cell.first = false
            cell.last = false
        }
        return cell
    }
    
    func configureMenuCell(_ indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: VIATableViewCell.defaultReuseIdentifier, for: indexPath) as! VIATableViewCell
        
        let menuItem = MenuItems(rawValue: indexPath.row)
        cell.labelText = menuItem?.title()
        cell.cellImage = menuItem?.image()
        cell.accessoryType = .disclosureIndicator
        cell.separatorInset = UIEdgeInsets(top: 0, left: 55, bottom: 0, right: 0)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if Sections(rawValue: section) == .Annual {
            return nil
        }
        if Sections(rawValue: section) == .Info {
            return nil
        }
        
        let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier) as! VIATableViewSectionHeaderFooterView
        
        return view
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        
        if Sections(rawValue: section) == .Annual {
            return CGFloat.leastNormalMagnitude
        }
        
        return 35.0
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if Sections(rawValue: section) == .PointsEarning {
            return UITableViewAutomaticDimension
        }
        return CGFloat.leastNormalMagnitude
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier) as? VIATableViewSectionHeaderFooterView else {
            return nil
        }
        header.labelText = CommonStrings.Status.LandingEarningPointsGroupHeader803
        return header
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let theSection = Sections(rawValue: indexPath.section)
        if theSection == .PointsEarning {
            if pointsEarningRequestSentAndFailed && annualStatus?.productCategoryCount() == 0 {
                pointsEarningRequestSentAndFailed = false
                loadEarningPointsData()
                tableView.reloadSections([Sections.PointsEarning.rawValue], with: UITableViewRowAnimation.automatic)
            } else if annualStatus?.productCategoryCount() != 0 {
                self.performSegue(withIdentifier: "showPointEarningActivities", sender: indexPath)
            }
        } else if theSection == .Menu {
            if let menuItem = MenuItems(rawValue: menuRows().integer(at: indexPath.row)) {
                switch menuItem {
                case .learnMore:
                    learnMoreTapped()
                case .help:
                    helpTapped()
                }
            }
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    // MARK: Navigation
    func learnMoreTapped() {
        self.performSegue(withIdentifier: MenuItems.learnMore.segueIdentifier(), sender: self)
    }
    
    func helpTapped() {
        self.performSegue(withIdentifier: MenuItems.help.segueIdentifier(), sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        segue.destination.hideBackButtonTitle()
        if segue.identifier == "showPointEarningActivities" {
            if let pointsEarningsActivityViewController = segue.destination as? PointsEarningActivitiesViewController {
                if let pointsActivity = sender as? IndexPath {
                    guard let pointsEntryKey = annualStatus?.productCategoryKey(for: pointsActivity.row) else {
                        return
                    }
                    segue.destination.title = self.annualStatus?.productCategoryName(for: pointsActivity.row)
                    let viewModel = VIAApplicableFeatures.default.getPointsEarningActivitiesViewModel() as? PointsEarningActivitiesViewModel
                    viewModel?.setup(with: pointsEntryKey)
                    pointsEarningsActivityViewController.viewModel = viewModel
                }
            }
        }
    }
}
