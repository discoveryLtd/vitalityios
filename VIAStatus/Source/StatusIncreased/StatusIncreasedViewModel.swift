import UIKit
import VitalityKit
import VitalityKit

class StatusIncreasedViewModel {
    internal var realm = DataProvider.newRealm()
    var statusData: StatusIncreased!
    init() {
        let homeScreenData = realm.allHomeScreenData().first
        let vitalityStatus = homeScreenData?.currentVitalityStatus
        let nextStatus = vitalityStatus?.nextVitalityStatusKey
        if let currentStatus = vitalityStatus?.overallVitalityStatusKey, let modalType = currentStatusModalType() {
            var pointsAccount: HomePointsAccount? = nil
            if let nextStatus = nextStatus {
                pointsAccount = realm.pointsAccount(for: nextStatus)
            }
            statusData = StatusIncreased(nextStatus: nextStatus, newStatus: currentStatus, statustype: modalType, newPointsGoal: pointsAccount?.pointsNeeded)
        } else {
            statusData = StatusIncreased(nextStatus: .Unknown, newStatus: .Unknown, statustype: .statusIncreased, newPointsGoal: 0)
        }   
    }
    
    func currentStatusModalType() -> ModalType? {
        if let _ = DataProvider.currentUserInstructionForShowNextStatusAchieved() {
            if newStatusEqualOrLessThanCarryOverStatus() {
                return .statusReachedAgain
            } else {
                return .statusIncreased
            }
        } else if let _ = DataProvider.currentUserInstructionForShowNextStatusAchieved() {
            return .newMembershipYear
        }
        return nil
    }
    
    func newStatusEqualOrLessThanCarryOverStatus() -> Bool {
        let homeScreenData = realm.allHomeScreenData().first
        guard let vitalityStatus = homeScreenData?.currentVitalityStatus else {
            return false
        }
        let currentStatus = vitalityStatus.overallVitalityStatusKey
        guard let currentPointsAccount = realm.pointsAccount(for: currentStatus) else {
            return false
        }
        let carryOverStatus = vitalityStatus.carryOverVitalityStatusKey
        guard let carryOverPointsAccount = realm.pointsAccount(for: carryOverStatus) else {
            return false
        }
        
        if currentPointsAccount.sortOrder <= carryOverPointsAccount.sortOrder {
                return true
        }
        
        return false
    }
    
    func currentStatusBadge() -> UIImage? {
        let newStatus = statusData.newStatus
        return StatusHelper.badge(for: newStatus)
    }
    
    func nextStatusBadge() -> UIImage? {
        if let nextStatus = statusData.nextStatus {
            return StatusHelper.badge(for: nextStatus)
        }
        return nil
    }
    
    func nextStatusTitle() -> String? {
        let newStatus = statusData.newStatus
        return StatusHelper.statusName(for: newStatus)
    }
    
    func nextStatusMessage() -> String? {
        if let newPointsRequirement = statusData.newPointsGoal {
            return StatusHelper.earnPointsMessage(for: newPointsRequirement)
        }
        return nil
    }
    
    func title() -> String? {
        let newStatus = statusData.newStatus
        return StatusHelper.statusUpdateTitle(for: newStatus)
    }
    
    func subTitle() -> String? {
        let newStatus = statusData.newStatus
        let statusType = statusData.statustype
        return StatusHelper.statusUpdateSubtitle(for: statusType, status: newStatus)
    }
    
    func sectionTitle() -> String {
        return CommonStrings.Status.IncreasedStatusGroupHeading812
    }
    
    func statusColour() -> UIColor {
        let newStatus = statusData.newStatus
        return StatusHelper.mainColorForStatus(for: newStatus)
    }
    
    func postStatusModalDisplayed(completion: @escaping (Error?) -> Void) {
        let partyId = realm.getPartyId()
        let tenantId = realm.getTenantId()
        guard let userInstruction = userInstructionForModalType() else {
            completion(BackendError.other)
            return
        }
        Wire.Events.captureLoginEvents(tenantId: tenantId, partyId: partyId, loginSuccess: true, userInstructionResponseId: "\(userInstruction.id)", userInstructionResponseType: "\(userInstruction.typeKey)") { [weak self] error in
            self?.removeDisplayModalInstruction()
            completion(error)
        }
    }
    
    func userInstructionForModalType() -> UserInstruction? {
        switch statusData.statustype {
        case .statusIncreased:
            return DataProvider.currentUserInstructionForShowNextStatusAchieved()
        case .statusReachedAgain:
            return DataProvider.currentUserInstructionForShowNextStatusAchieved()
        case .newMembershipYear:
            return DataProvider.currentUserInstructionForShowCarryOverStatus()
        }
    }
    
    func removeDisplayModalInstruction() {
        switch statusData.statustype {
        case .statusIncreased:
            return DataProvider.deleteCurrentUserInstructionForShowNextStatusAchieved()
        case .statusReachedAgain:
            return DataProvider.deleteCurrentUserInstructionForShowNextStatusAchieved()
        case .newMembershipYear:
            return DataProvider.deleteCurrentUserInstructionForShowCarryOverStatus()
        }
    }
    
    func highestStatusReached() -> Bool {
        if statusData.nextStatus == nil {
            return true
        }
        return false
    }
}
