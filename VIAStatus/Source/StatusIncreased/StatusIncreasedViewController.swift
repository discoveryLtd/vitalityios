import VIAUIKit
import VIAUtilities
import VitalityKit

public enum ModalType {
    case statusIncreased
    case statusReachedAgain
    case newMembershipYear
}

struct StatusIncreased {
    var nextStatus: StatusTypeRef?
    var newStatus: StatusTypeRef
    var statustype: ModalType
    var newPointsGoal: Int?
}

class StatusIncreasedViewController: VIATableViewController, PrimaryColorTintable {
    var viewModel: StatusIncreasedViewModel = StatusIncreasedViewModel()
    override public func viewDidLoad() {
        super.viewDidLoad()
        configureAppearance()
        setupTableView()
        setupHeader()
        addDoneButton()
    }
    
    func configureAppearance() {
        navigationController?.makeNavigationBarInvisible()
        moveTableViewUnderNavBar()
    }
    
    func moveTableViewUnderNavBar() {
        let statusHeight = UIApplication.shared.statusBarFrame.height
        let navigationHeight = navigationController?.navigationBar.frame.height ?? 0
        tableView.contentInset = UIEdgeInsetsMake((navigationHeight + statusHeight) * -1, 0, 0, 0)
    }
    
    func setupTableView() {
        tableView.estimatedRowHeight = 100
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.register(NextStatusCell.nib(), forCellReuseIdentifier: NextStatusCell.defaultReuseIdentifier)
    }
    
    func setupHeader() {
        let header = StatusIncreasedHeader.viewFromNib(owner: nil)
        header?.badge = viewModel.currentStatusBadge()
        header?.title.text = viewModel.title()
        header?.message.text = viewModel.subTitle()
        tableView.tableHeaderView = header
        tableView.sizeHeaderViewToFit()
        if let headerView = tableView.tableHeaderView {
            Confetti.throwConfetti(in: headerView, with: viewModel.statusColour())
        }
    }
    
    func addDoneButton() {
        let doneButton = UIBarButtonItem(title: CommonStrings.DoneButtonTitle53, style: .done, target: self, action: #selector(doneTapped))
        doneButton.tintColor = UIColor.currentGlobalTintColor()
        navigationItem.rightBarButtonItem = doneButton
    }
    
    func doneTapped() {
        viewModel.postStatusModalDisplayed { [weak self] error in
            self?.dismiss(animated: true, completion: nil)
        }
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0 && !viewModel.highestStatusReached() {
            return viewModel.sectionTitle()
        }
        return nil
    }

    // MARK: Datasource
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if viewModel.highestStatusReached() {
            return 1
        }
        return 0
    }
    
    // MARK: Delegate
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let statusCell = tableView.dequeueReusableCell(withIdentifier: NextStatusCell.defaultReuseIdentifier, for: indexPath) as? NextStatusCell else {
            return tableView.defaultTableViewCell()
        }
        
        statusCell.nextStatusBadge.image = viewModel.nextStatusBadge()
        statusCell.titleLabel.text = viewModel.nextStatusTitle()
        statusCell.messageLabel.text = viewModel.nextStatusMessage()
        
        statusCell.selectionStyle = .none
        return statusCell
    }
}
