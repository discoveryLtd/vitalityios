import VIAUIKit
import VitalityKit
import VIACommon

public class VIAStatusLearnMoreViewModel: AnyObject, VIALearnMoreViewModel {
    
    public var title: String {
        return CommonStrings.LearnMoreButtonTitle104
    }
    
    public var contentItems: [LearnMoreContentItem] {
        var items = [LearnMoreContentItem]()
        
        items.append(LearnMoreContentItem(
            header: CommonStrings.Status.LearnMore.Section1Title794,
            content: CommonStrings.Status.LearnMore.Subtitle741
        ))
        
        items.append(LearnMoreContentItem(
            header: CommonStrings.Status.LearnMore.Section2Title795,
            content: CommonStrings.Status.LearnMore.Section1Content743,
            image: VIAStatusAsset.pointsLarge.templateImage
        ))
        
        return items
    }
    
    public var imageTint: UIColor {
        return .primaryColor()
    }
}
