//
//  VIAStatusStepViewModel.swift
//  VitalityActive
//
//  Created by Joshua Ryan on 9/1/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation

class VIAStatusStepViewModel {

	// MARK: Public
	var title: String
	var description: String
	var image: UIImage

	init(title: String, description: String, image: UIImage) {
		self.title = title
		self.description = description
		self.image = image
	}
}
