import VIAUIKit
import VitalityKit

class PointsEntryDetailTableViewController: UITableViewController {
    var viewModel: PointsEntryDetailViewModel?
    var isTestOrRun: Bool? = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
    }

    func setupTableView() {
        tableView.register(VIASubtitleTableViewCell.nib(), forCellReuseIdentifier: VIASubtitleTableViewCell.defaultReuseIdentifier)
        tableView.register(TitleImageHeaderView.nib(), forHeaderFooterViewReuseIdentifier: TitleImageHeaderView.defaultReuseIdentifier)
        tableView.register(VIALabelTableViewCell.nib(), forCellReuseIdentifier: VIALabelTableViewCell.defaultReuseIdentifier)
        tableView.sectionHeaderHeight = UITableViewAutomaticDimension
        tableView.estimatedSectionHeaderHeight = 40
        tableView.estimatedRowHeight = 75
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.backgroundColor = UIColor.tableViewBackground()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        if let isTestOrRun = self.isTestOrRun, isTestOrRun {
            return 2
        }
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let isTestOrRun = self.isTestOrRun, isTestOrRun {
            if section == 0 {
                return viewModel?.pointsEntries?.count ?? 0
            }
            return 1
        }
        return viewModel?.pointsEntries?.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let isTestOrRun = self.isTestOrRun, isTestOrRun {
            if indexPath.section == 0 {
                return earnPointsCell(at: indexPath)
            } else {
                return workoutCell(at: indexPath)
            }
        } else {
            return earnPointsCell(at: indexPath)
        }
    }
    
    func earnPointsCell(at indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: VIASubtitleTableViewCell.defaultReuseIdentifier, for: indexPath)
        let earnPointsCell = cell as? VIASubtitleTableViewCell
        
        earnPointsCell?.headingText = viewModel?.pointValue(for: indexPath.row)
        if let pointsEntryString = viewModel?.pointsEntryString(for: indexPath.row) {
            let boldText = NSMutableAttributedString(string: "\u{2022}\t"+pointsEntryString)
            earnPointsCell?.attributedContentText = generateAttributedParagraph(for: boldText)
        }
        cell.selectionStyle = .none
        return cell
    }
    
    func workoutCell(at indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: VIALabelTableViewCell.defaultReuseIdentifier, for: indexPath)
        let workoutCell = cell as? VIALabelTableViewCell
        
        workoutCell?.labelText = CommonStrings.PotentialPoints.HowToCalculateHeartRate484
        
        cell.selectionStyle = .none
        return cell
    }
    
    func generateAttributedParagraph(for condition: NSMutableAttributedString) -> NSMutableAttributedString {
        let paragrahStyle = NSMutableParagraphStyle()
        paragrahStyle.paragraphSpacing = 0
        paragrahStyle.paragraphSpacingBefore = 0.0
        paragrahStyle.firstLineHeadIndent = 0.0  // First line is the one with bullet point
        paragrahStyle.headIndent = 30 // Set the indent for given bullet character and size font
        
        condition.addAttribute(NSParagraphStyleAttributeName, value: paragrahStyle, range: NSRange(location: 0, length: condition.string.characters.count))
        
        return condition
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if let header = self.tableView.dequeueReusableHeaderFooterView(withIdentifier: TitleImageHeaderView.defaultReuseIdentifier) as? TitleImageHeaderView {
            if let isTestOrRun = isTestOrRun, isTestOrRun {
                if section == 0 {
                    header.title = CommonStrings.PotentialPoints.HeaderTitle476
                } else {
                    header.title = CommonStrings.PotentialPoints.HowToCalculateHeartRateTitle483
                }
            } else {
                if section == 0 {
                    header.title = CommonStrings.PotentialPoints.HeaderTitle476
                }
            }
            
            header.image = viewModel?.pointsEntryImage()
            return header
        }
        return nil
    }
}
