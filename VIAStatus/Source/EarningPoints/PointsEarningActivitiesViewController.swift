import VIAUIKit
import RealmSwift
import VitalityKit
import VIAUtilities
import VIACommon

enum PointsEarningFlag {
    case earnPoints
    case earnUpToPoints
    case earnUpToPoinsLimit
}

enum NavigationType {
    case segueToSubFeatures
    case segueToHowToEarnPoints
    case segueToPointsEntries
}

protocol PointsActivityData {
    func setup(with eventKey: Int)
    func numberOfPointsEarningActivities() -> Int
    func pointsEarningActivityName(at index: Int) -> String?
    func pointsEarningActivityCode(at index: Int) -> String?
    func segueForPointsEarningActivity(at index: Int) -> NavigationType?
    func earnUpToString(for index: Int) -> String?
    func featureKey(at index: Int) -> Int?
    func featureTypeKey(at index: Int) -> Int?
    func showPointsLimitHeader() -> Bool
    func headerTitle() -> String?
    func headerSubtitle() -> String?
    func getEventKey() -> Int
}

class PointsEarningActivitiesViewController: VIATableViewController {
    var viewModel: PointsActivityData?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureAppearance()
        setupTableView()
    }
    
    func configureAppearance() {
        self.navigationController?.makeNavigationBarTransparent()
    }
    
    func setupTableView() {
        tableView.register(TitleSubtitleTableViewCell.nib(), forCellReuseIdentifier: TitleSubtitleTableViewCell.defaultReuseIdentifier)
        tableView.estimatedRowHeight = 30
        tableView.rowHeight = UITableViewAutomaticDimension
        
        self.tableView.contentInset = UIEdgeInsets(top: 10, left: 0, bottom: 0, right: 0)
        let view = TableViewHeaderView.viewFromNib(owner: self)!
        
        // Display 'Earning points and points limits for activities' for GetActiveMobile and Other Markets (IGI, CA, UKE)
        if VIAApplicableFeatures.default.shouldDisplayEarningPointsAndPointsLimits(for: (viewModel?.getEventKey())!) {
            view.header = viewModel?.headerTitle()
            view.content = viewModel?.headerSubtitle()
            tableView.tableHeaderView = view
        }
    }
    
    // Mark: Data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel?.numberOfPointsEarningActivities() ?? 0
    }
    
    // Mark: Delegate
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return VIAApplicableFeatures.default
            .getStatusPointsEarningActivitiesViewCell(tableView: tableView, indexPath: indexPath,
                                                      pointsActivityName: viewModel?.pointsEarningActivityName(at: indexPath.row),
                                                      potentialPointsString: viewModel?.earnUpToString(for: indexPath.row),
                                                      eventKey: viewModel?.getEventKey() ?? -1,
                                                      activityCode: viewModel?.pointsEarningActivityCode(at: indexPath.row))
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let navigationType = viewModel?.segueForPointsEarningActivity(at: indexPath.row)  {
            if let segue = segue(for: navigationType) {
                performSegue(withIdentifier: segue, sender: indexPath.row)
            }
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func segue(for navigationType: NavigationType) -> String? {
        switch navigationType {
        case .segueToHowToEarnPoints:
            return "showPointEarningActivities"
        case .segueToSubFeatures:
            return "showSubFeatureList"
        case .segueToPointsEntries:
            return "showPointsEntries"
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        segue.destination.hideBackButtonTitle()
        if segue.identifier == "showSubFeatureList" {
            guard let index = sender as? Int,
                let key = viewModel?.featureKey(at: index),
                let destination = segue.destination as? SubFeaturePointsEarningTableViewController else {
                return
            }
            destination.title = viewModel?.pointsEarningActivityName(at: index)
            let subFeatureViewModel = SubFeaturePointsEarningViewModel()
            subFeatureViewModel.setup(with: key)
            destination.viewModel = subFeatureViewModel
        } else if segue.identifier == "showPointsEntries" {
            guard let index = sender as? Int,
                let key = viewModel?.featureKey(at: index),
                let destination = segue.destination as? PointsEntryDetailTableViewController else {
                    return
            }
            destination.title = viewModel?.pointsEarningActivityName(at: index)
            let subFeatureViewModel = PointsEntryDetailViewModel()
            subFeatureViewModel.setupWithFeature(key: key)
            destination.viewModel = subFeatureViewModel

        } else if segue.identifier == "showPointEarningActivities" {
            guard let index = sender as? Int, let destination = segue.destination as? VIAPointsEarningPotentialWithTiersTableViewController else {
                print("Incorrect view controller as destination")
                return
            }
            guard let key = viewModel?.featureTypeKey(at: index) else {
                return
            }
            destination.title = viewModel?.pointsEarningActivityName(at: index)
            let asset = PointsEntryTypeRef(rawValue: key)?.image()
            destination.activityImage = asset
            destination.pointsActivityTypeKey = PointsEntryTypeRef(rawValue: key) ?? .Unknown
            destination.viewModel = StatusPotentialPointsViewModel(earningActivityKey: key)
        }
    }
}
