import VitalityKit
import VIAUtilities
import RealmSwift

class PointsEntryDetailViewModel {
    internal var pointsEnteriesKey: Int?
    internal let statusRealm = DataProvider.newStatusRealm()
    
    internal var pointsEntries: List<StatusPointsEntry>?
    
    func setupWithSubFeature(key: Int) {
        pointsEntries = statusRealm.allStatusProductSubFeatures(for: key).first?.productSubfeaturePointsEntries
    }
    
    func setupWithFeature(key: Int) {
        pointsEntries = statusRealm.productFeatureCategoriesPointsInformation(with: key).first?.pointsEntries
    }
    
    func pointValue(for index: Int) -> String? {
        guard let potentialPoints = pointsEntries?[index].potentialPoints else {
            return nil
        }
        return CommonStrings.Status.PointsProgress838(String(describing: potentialPoints))
    }
    
    func pointsEntryString(for index: Int) -> String? {
        guard let typeKey = pointsEntries?[index].typeKey else {
            return nil
        }
        // For Strings that requires Potential Points
        guard let potentialPoints = pointsEntries?[index].potentialPoints else {
            return nil
        }
        let pointsEntryType = PointsEntryTypeRef(rawValue: typeKey)
        return pointsEntryType?.string(value: "\(potentialPoints)")
    }
    
    func pointsEntryImage() -> UIImage? {
        guard let typeKey = pointsEntries?.first?.typeKey else {
            return nil
        }
        let pointsEntryType = PointsEntryTypeRef(rawValue: typeKey)
        return pointsEntryType?.image()
    }
}
