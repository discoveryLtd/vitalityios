import VitalityKit
import RealmSwift

protocol SubFeatureViewModel {
    func setup(with key: Int)
    func subFeatureCount() -> Int
    func nameForSubFeature(at index: Int) -> String?
    func nameForSubFeature(with key: Int) -> String?
    func potentialPointsForSubFeature(at index: Int) -> String?
    func earnUpToString(for index: Int) -> String?
    func subFeatureKey(for index: Int) -> Int?
}

class SubFeaturePointsEarningViewModel: SubFeatureViewModel {
    internal var productFeatureKey: Int?
    internal let statusRealm = DataProvider.newStatusRealm()
    internal var subFeatures: List<StatusProductSubfeatures>? {
        guard let key = productFeatureKey else {
            return nil
        }
        return statusRealm.productFeatureCategoriesPointsInformation(with: key).first?.productSubFeatures
    }
    
    func setup(with key: Int) {
        productFeatureKey = key
    }
    
    func subFeatureCount() -> Int {
        return subFeatures?.count ?? 0
    }
    
    internal func subFeature(for index: Int) -> StatusProductSubfeatures? {
        return subFeatures?[index]
    }
    
    func nameForSubFeature(at index: Int) -> String? {
        return subFeature(for: index)?.name
    }
    
    func nameForSubFeature(with key: Int) -> String? {
        return subFeatures?.filter({ $0.key == key}).first?.name
    }
    
    func potentialPointsForSubFeature(at index: Int) -> String? {
        return subFeature(for: index)?.name
    }
 
    func earnUpToString(for index: Int) -> String? {
        guard let pointsEarningFlag = subFeature(for: index)?.pointsEarningFlag else {
            return nil
        }
        guard let potentialPoints = subFeature(for: index)?.potentialPoints else {
            return nil
        }
        if pointsEarningFlag == "EarnUpToPoints" {
            return CommonStrings.Status.PointsIndicationMessageUpTo828(String(describing: potentialPoints))
        } else if pointsEarningFlag == "EarnUpToPointsLimits" {
            return CommonStrings.Status.PointsIndicationMessage829(String(describing: potentialPoints))
        } else if pointsEarningFlag == "EarnPoints" {
            return CommonStrings.Status.PointsIndicationMessageLimit830(String(describing: potentialPoints))
        }
        return nil
    }
    
    
    func subFeatureKey(for index: Int) -> Int? {
        return subFeatures?[index].key
    }
}
