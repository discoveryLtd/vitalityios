import VIAUIKit

class SubFeaturePointsEarningTableViewController: UITableViewController {
    public var viewModel: SubFeatureViewModel!
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
    }
    
    func setupTableView() {
        tableView.register(TitleSubtitleTableViewCell.nib(), forCellReuseIdentifier: TitleSubtitleTableViewCell.defaultReuseIdentifier)
        tableView.estimatedRowHeight = 50
        tableView.rowHeight = UITableViewAutomaticDimension
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.subFeatureCount()
    }
    
    // Mark: Delegate
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: TitleSubtitleTableViewCell.defaultReuseIdentifier, for: indexPath)
        guard let viaCell = cell as? TitleSubtitleTableViewCell else {
            return tableView.defaultTableViewCell()
        }
        viaCell.title.text = viewModel.potentialPointsForSubFeature(at: indexPath.row)
        viaCell.subtitle.text = viewModel.earnUpToString(for: indexPath.row)
        viaCell.selectionStyle = .none
        viaCell.accessoryType = .disclosureIndicator
        return viaCell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let key = viewModel?.subFeatureKey(for: indexPath.row) else {
            return
        }
        performSegue(withIdentifier: "showPointsEntries", sender: key)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showPointsEntries" {
            let pointsEntriesViewModel = PointsEntryDetailViewModel()
            guard let key = sender as? Int else {
                return
            }
            pointsEntriesViewModel.setupWithSubFeature(key: key)
            guard let destination = segue.destination as? PointsEntryDetailTableViewController else {
                return
            }
            destination.title = viewModel.nameForSubFeature(with: key)
            destination.viewModel = pointsEntriesViewModel
        }
    }
}
