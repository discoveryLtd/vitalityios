import VitalityKit
import RealmSwift

open class PointsEarningActivitiesViewModel: PointsActivityData {
    internal var eventKey: Int = -1

    internal var potentialPointsItems: List<StatusProductFeaturePointsInformation>? {
        let statusRealm = DataProvider.newStatusRealm()
        let category = statusRealm.productFeatureCategories(with: eventKey).first
        return category?.productFeatureAndPointsInformations
    }
    
    internal var eventName: String? {
        let statusRealm = DataProvider.newStatusRealm()
        let category = statusRealm.productFeatureCategories(with: eventKey).first
        return category?.name
    }
    
    internal var eventPointLimit: Int {
        let statusRealm = DataProvider.newStatusRealm()
        let category = statusRealm.productFeatureCategories(with: eventKey).first
        guard let pointsCategoryLimitValue = category?.pointsCategoryLimit.value else { return 0 }
        guard let potentialPoints = category?.potentialPoints else { return 0 }
        
        return (pointsCategoryLimitValue > 0) ? pointsCategoryLimitValue : potentialPoints
    }
    
    public required init(){
        
    }
    
    open func setup(with eventKey: Int) {
        self.eventKey = eventKey
    }
    
    open func numberOfPointsEarningActivities() -> Int {
        return potentialPointsItems?.count ?? 0
    }
    
    open func pointsEarningActivity(at index: Int) -> StatusProductFeaturePointsInformation? {
        let activity = potentialPointsItems?[index]
        return activity
    }
    
    open func pointsEarningActivityName(at index: Int) -> String? {
        guard let activity = pointsEarningActivity(at: index) else {
            return nil
        }
        return activity.name
    }
    
    open func pointsEarningActivityCode(at index: Int) -> String? {
        guard let activity = pointsEarningActivity(at: index) else {
            return nil
        }
        return activity.code
    }
    
    func segueForPointsEarningActivity(at index: Int) -> NavigationType? {
        let productFeature = pointsEarningActivity(at: index)
        if productFeature?.productSubFeatures.count != 0 {
            return .segueToSubFeatures
        }
        guard let pointsEntries = productFeature?.pointsEntries else {
            return nil
        }
        for pointsEntry in pointsEntries {
            if pointsEntry.potentialPointsEntries.count != 0 {
                return .segueToHowToEarnPoints
            }
        }
        return .segueToPointsEntries
    }
    
    open func earnUpToString(for index: Int) -> String? {
        guard let pointsEarningFlag = pointsEarningActivity(at: index)?.pointsEarningFlag else {
            return nil
        }
        guard let potentialPoints = pointsEarningActivity(at: index)?.potentialPoints else {
            return nil
        }
        
        if pointsEarningFlag == "EarnUpToPoints" {
            return CommonStrings.Status.PointsIndicationMessageUpTo828(String(describing: potentialPoints))
        } else if pointsEarningFlag == "EarnUpToPoinsLimit" {
            return CommonStrings.Status.PointsIndicationMessage829(String(describing: potentialPoints))
        } else if pointsEarningFlag == "EarnPoints" {
            return CommonStrings.Status.PointsIndicationMessageLimit830(String(describing: potentialPoints))
        }
        return nil
    }
    
    open func featureKey(at index: Int) -> Int? {
        return pointsEarningActivity(at: index)?.key
    }
    
    open func featureTypeKey(at index: Int) -> Int? {
        return pointsEarningActivity(at: index)?.pointsEntries.first?.typeKey
    }
    
    open func showPointsLimitHeader() -> Bool {
        return eventPointLimit > 0 ? true : false
    }
    
    open func headerTitle() -> String? {
        return CommonStrings.Status.PointsDetailTitle807
    }
    
    open func headerSubtitle() -> String? {
        return CommonStrings.Status.PointsDetailTitle808(String(eventPointLimit), eventName!, eventName!)
    }
    
    open func getEventKey() -> Int {
        return eventKey
    }
}
