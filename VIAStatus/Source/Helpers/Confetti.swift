import UIKit

class Confetti {
    class func throwConfetti(in view: UIView, with color: UIColor) {
        let emitterLayer = CAEmitterLayer()
        emitterLayer.emitterPosition = CGPoint(x: view.bounds.width / 2.0, y: 0)
        emitterLayer.emitterShape = kCAEmitterLayerLine
        emitterLayer.emitterSize = CGSize(width: view.bounds.width, height: 1)
        emitterLayer.renderMode = kCAEmitterLayerAdditive
        let images = [Confetti.drawConfetti(height: 25, color: color, view: view)]
        emitterLayer.emitterCells = []
        for image in images {
            let emitterCell = particleCell(with: image)
            emitterLayer.emitterCells?.append(emitterCell)
        }
        view.clipsToBounds = true
        view.layer.insertSublayer(emitterLayer, at: 0)
    }
    
    internal class func drawConfetti(height: Int, color: UIColor, view: UIView) -> UIImage {
        let renderer = UIGraphicsImageRenderer(size: view.frame.size)
        let img = renderer.image { ctx in
            ctx.cgContext.setFillColor(color.cgColor)
            ctx.cgContext.setStrokeColor(color.cgColor)
            ctx.cgContext.setLineWidth(10)
            let rectangle = CGRect(x: 0, y: 0, width: 7, height: height)
            ctx.cgContext.addRect(rectangle)
            ctx.cgContext.drawPath(using: .fillStroke)
        }
        return img
    }
    
    internal class func particleCell(with image: UIImage?) -> CAEmitterCell {
        let cell = CAEmitterCell()
        cell.birthRate = 15
        cell.lifetime = 20.0
        cell.lifetimeRange = 10
        cell.velocity = 1
        cell.alphaRange = 1 
        cell.velocityRange = 1
        cell.emissionLongitude = CGFloat.pi
        cell.scale = 0.1
        cell.scaleRange = 0.3
        cell.alphaSpeed = -0.1
        cell.xAcceleration = 5
        cell.yAcceleration = 10
        cell.spinRange = 0.5
        cell.contents = image?.cgImage
        return cell
    }
    
}
