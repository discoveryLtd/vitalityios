import VIAUIKit
import VIAUtilities
import VitalityKit
import VitalityKit
import VIACommon

public class StatusHelper {
    
    public class func statusDidIncrease() -> Bool {
        guard DataProvider.currentUserInstructionForShowNextStatusAchieved() != nil else { return false }
        return true
    }
    
    public class func newMembershipYearEntered() -> Bool {
        guard DataProvider.currentUserInstructionForShowCarryOverStatus() != nil else { return false }
        return true
    }
    
    class func badge(for status:StatusTypeRef) -> UIImage? {
        switch status {
        case .Blue:
            return VIAUIKitAsset.Status.badgeBlueLarge.image
        case .Bronze:
            return VIAUIKitAsset.Status.badgeBronzeLarge.image
        case .Silver:
            return VIAUIKitAsset.Status.badgeSilverLarge.image
        case .Gold:
            return VIAIconography.default.statusLandingGoldStatusIcon
        case .Platinum:
            return VIAUIKitAsset.Status.badgePlatinumLarge.image
        default:
            return nil
        }
    }
    
    class func statusName(for status:StatusTypeRef) -> String {
        let statusName : String
        switch status {
        case .Blue:
            statusName = CommonStrings.Status.LevelBlueTitle833
        case .Bronze:
            statusName = CommonStrings.Status.LevelBronzeTitle834
        case .Silver:
            statusName = CommonStrings.Status.LevelSilverTitle835
        case .Gold:
            statusName = CommonStrings.Status.LevelGoldTitle836
        case .Platinum:
            statusName = CommonStrings.Status.LevelPlatinumTitle837
        default:
            statusName = ""
        }
        return statusName
    }
    
    class func statusUpdateTitle(for status:StatusTypeRef) -> String {
        let newStatus: String = StatusHelper.statusName(for: status)
        return CommonStrings.Status.IncreasedStatusTitle810(newStatus)
    }
    
    class func statusUpdateSubtitle(for modalType: ModalType, status: StatusTypeRef) -> String {
        let newStatus: String = StatusHelper.statusName(for: status)
        switch modalType {
        case .statusIncreased:
            return CommonStrings.Status.IncreasedStatusMessage811(newStatus)
        case .newMembershipYear:
            return CommonStrings.Status.InformationMessage816(newStatus, newStatus)
        case .statusReachedAgain:
            return CommonStrings.Status.IncreaseStatusAgainMessage832(newStatus)
        }
    }
    
    class func earnPointsMessage(for newPointsGoal: Int) -> String {
        return CommonStrings.Status.IncreasedStatusPointsNeeded813(String(describing: newPointsGoal))
    }
    
    class func mainColorForStatus(for status:StatusTypeRef) -> UIColor {
        let statusColour : UIColor
        switch status {
        case .Blue:
            statusColour = UIColor.blue
        case .Bronze:
            statusColour = UIColor.darkBronze()
        case .Silver:
            statusColour = UIColor.darkGrey()
        case .Gold:
            statusColour = UIColor.darkGold()
        case .Platinum:
            statusColour = UIColor.darkGrey()
        default:
            statusColour = UIColor.white
        }
        return statusColour
    }
}
