import VIAUIKit

public class ActivityIndicatorTableViewCell: UITableViewCell, Nibloadable {
    @IBOutlet public weak var activityIndicatorImageView: UIImageView!
    @IBOutlet public weak var activityLabel: UILabel!
    
    override public func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override public func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
