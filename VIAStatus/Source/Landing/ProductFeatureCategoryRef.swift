import VIAUtilities

internal extension ProductFeatureCategoryRef {
    func image() -> UIImage? {
        switch self {
        case .AssessmentsMobile :
            return VIAStatusAsset.assessment.image
        case .GetActiveMobile :
            return VIAStatusAsset.getActive.image
        case .NutritionMobile :
            return VIAStatusAsset.nutrition.image
        case .ScreeningsMobile :
            return VIAStatusAsset.screening.image
        default:
            return nil
        }
    }
}
