import VitalityKit
import VIAUIKit
import VitalityKit

public class VIAStatusOnboardingViewController: VIAOnboardingViewController, PrimaryColorTintable {

    override public func awakeFromNib() {
        super.awakeFromNib()

        self.viewModel = VIAStatusOnboardingViewModel()
    }

    override public func viewDidLoad() {
        super.viewDidLoad()

        navigationController?.makeNavigationBarInvisible()
    }

    override public func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        navigationController?.navigationBar.isHidden = false
    }

    // MARK: - Vitality Status Appearance

    override public func configureAppearance() {
        self.tableView.backgroundColor = UIColor.day()
    }

    open override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: VIAOnboardingTableViewCell.defaultReuseIdentifier, for: indexPath) as! VIAOnboardingTableViewCell

        if let viewModel = self.viewModel {
            let item = viewModel.contentItems[indexPath.row]

            cell.setLabelTextColor(color: UIColor.genericActive())
            cell.setHeadingFont(font: UIFont.onboardingCellHeading())
            cell.contentFont(font: UIFont.onboardingSubtitle())

            cell.heading = item.heading
            cell.content = item.content
            cell.contentImage = item.image
        }
        cell.selectionStyle = .none

        return cell
    }

    // MARK: - Actions

    override public func mainButtonTapped(_ sender: UIButton) {
        AppSettings.setHasShownVitalityStatusOnboarding()
        self.dismiss(animated: true, completion: nil)
    }
}
