import Foundation
import VIAUIKit
import VIAUtilities
import VitalityKit

public class VIAStatusLearnMoreViewController: VIATableViewController, PrimaryColorTintable {
    
    let viewModel = VIAStatusLearnMoreViewModel()

    override public func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = viewModel.title
        configureAppearance()
        configureTableView()
    }
    
    func configureAppearance() {
        navigationController?.makeNavigationBarTransparent()
    }
    
    func configureTableView() {
        self.tableView.register(VIAGenericContentCell.nib(), forCellReuseIdentifier: VIAGenericContentCell.defaultReuseIdentifier)
        self.tableView.estimatedRowHeight = 75
        self.tableView.sectionHeaderHeight = 0
        self.tableView.sectionFooterHeight = 0
        self.tableView.estimatedSectionHeaderHeight = 0
        self.tableView.estimatedSectionFooterHeight = 0
        self.tableView.contentInset = UIEdgeInsets(top: 25, left: 0, bottom: 0, right: 0)
        self.tableView.backgroundColor = UIColor.white
        self.tableView.separatorColor = .clear
    }
    
    // MARK: - UITableView datasource
    override public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.contentItems.count
    }
    
    // MARK: - UITableView delegate

    override public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: VIAGenericContentCell.defaultReuseIdentifier, for: indexPath) as? VIAGenericContentCell else {
            return tableView.defaultTableViewCell()
        }
        
        cell.isUserInteractionEnabled = false
        
        let item = viewModel.contentItems[indexPath.row]
        
        if indexPath.row == 0 {
            cell.customHeaderFont = viewModel.headingTitleFont
            cell.customContentFont = viewModel.headingMessageFont
        } else {
            cell.customHeaderFont = viewModel.sectionTitleFont
            cell.customContentFont = viewModel.sectionMessageFont
        }
        cell.header = item.header
        cell.content = item.content
        cell.cellImage = item.image
        cell.layoutIfNeeded()
        cell.cellImageTintColor = viewModel.imageTint
        
        return cell
    }
}
