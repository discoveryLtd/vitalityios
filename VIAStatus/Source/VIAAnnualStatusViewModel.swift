import VitalityKit
import RealmSwift
import VIAUtilities
import VIAUIKit
import VIACommon

open class VIAAnnualStatusViewModel {
    internal let statusRealm = DataProvider.newStatusRealm()
    internal let coreRealm = DataProvider.newRealm()
    internal var productCategories: Results<StatusProductFeatureCategory>?
    internal var homeStatuses: Results<HomeVitalityStatus>? {
        return coreRealm.allHomeVitalityStatuses()
    }
    
    public required init(){
        
    }
    
    // MARK: Public
    open func requestProductFeaturePoints(completion: @escaping (Error?) -> Void) {
        let realm = DataProvider.newRealm()
        let tenantId = realm.getTenantId()
        let partyId = realm.getPartyId()
        let request = ProductFeatureRequest(effectiveDate: Date(), productFeatureCategoryTypeKey: 7)
        Wire.Member.getProductFeaturePoints(tenantId: tenantId, partyId: partyId, request: request) { (error) in
            if let error = error {
                completion(error)
                return
            }
            completion(nil)
        }
    }
    
    open func numberOfRows(for section: Sections) -> Int {
        switch section {
        case .Status:
            return 1
        case .Info:
            return possibleStatusesCount()
        case .Annual:
            return 1
        case .PointsEarning:
            return productCategoryCount()
        case .Menu:
            return menuRows().count
        }
    }
    
    open func descriptionForStatus() -> String? {
        guard let currentStatus = homeStatuses?.first else {
            return nil
        }
        let homeScreenData = coreRealm.allHomeScreenData().first
        let daysRemaining = homeScreenData?.daysLeftInMembershipPeriod?.daysRemaining ?? 0
        
        if currentStatus.overallVitalityStatusKey == currentStatus.lowestVitalityStatusKey {
            return CommonStrings.Status.LandingPointsTotalMessage800(currentStatus.overallVitalityStatusName!, String(describing: daysRemaining), currentStatus.highestVitalityStatusName!)
        } else if currentStatus.pointsStatusKey == currentStatus.highestVitalityStatusKey && currentStatus.overallVitalityStatusKey == currentStatus.highestVitalityStatusKey {
            return CommonStrings.Statu.LandingTotalPointsFinalMessage825(currentStatus.overallVitalityStatusName!, currentStatus.overallVitalityStatusName!)
        } else if currentStatus.pointsToMaintainStatus.value ?? 0 > 0 {
            return CommonStrings.Status.LandingTotalPointsMessage821(currentStatus.overallVitalityStatusName!, String(describing: daysRemaining))
        } else {
            return CommonStrings.Status.LandingPointsMessageNextStatus831(currentStatus.overallVitalityStatusName!, String(describing: daysRemaining))
        }
    }
    
    // MARK: Statuses
    //MARK: Possible statuses
    open func possibleStatuses() -> Array<HomePointsAccount> {
        let homeScreenData = coreRealm.allHomeScreenData().first
        if let possibleStatuses = homeScreenData?.pointsToNextStatuses {
            return Array(possibleStatuses).sorted(by:  { $0.sortOrder < $1.sortOrder })
        }
        return []
    }
    
    open func possibleStatusesCount() -> Int {
        return possibleStatuses().count
    }
    
    open func possibleStatusName(at index: Int) -> String {
        let possibleStatuses = self.possibleStatuses()
        guard index < self.possibleStatuses().count else {
            return ""
        }
        return possibleStatuses[index].statusName
    }
    
    open func possibleStatusBodyContent(at index: Int) -> String {
        if index == 0 {
            return CommonStrings.Status.LandingStatusTracking1Message801
        }
        let possibleStatuses = self.possibleStatuses()
        return CommonStrings.Status.PointsProgress838(String(describing: possibleStatuses[index].statusPoints))
    }
    
    open func possibleStatusImage(for index: Int) -> UIImage? {
        guard index < self.possibleStatuses().count else {
            return nil
        }
        let possibleStatuses = self.possibleStatuses()
        let statusKey = possibleStatuses[index].statusKey
        return possibleStatusImage(for: statusKey)
    }
    
    open func possibleStatusImage(for key: StatusTypeRef) -> UIImage? {
        switch key {
        case .Unknown:
            return nil
        case .Blue:
            return VIAUIKitAsset.Status.badgeBlueLarge.image
        case .Bronze:
            return VIAUIKitAsset.Status.badgeBronzeLarge.image
        case .Gold:
            return VIAIconography.default.statusLandingGoldStatusIcon
        case .Platinum:
            return VIAUIKitAsset.Status.badgePlatinumLarge.image
        case .Silver:
            return VIAUIKitAsset.Status.badgeSilverLarge.image
        }
    }
    
    //MARK: Current statuses
    open func maxPointsEarned() -> Bool {
        guard let homeScreenData = coreRealm.allHomeScreenData().first else {
            return false
        }
        return homeScreenData.calculateStatusProgress() == 1.0
    }
    
    open func pointsToMaintainStatus() -> Int {
        return homeStatuses?.first?.pointsToMaintainStatus.value ?? 0
    }
    
    open func nameOfCurrentStatus() -> String? {
        
        return homeStatuses?.first?.overallVitalityStatusName
    }
    
    open func nameOfNextStatus() -> String? {
        return homeStatuses?.first?.nextVitalityStatusName
    }
    
    open func currentStatusPointsCount() -> Int? {
        return homeStatuses?.first?.totalPoints.value
    }
    
    open func pointsToNextStatus() -> Int? {
        let homeScreenData = coreRealm.allHomeScreenData().first
        return homeScreenData?.nextStatusPointsAccount()?.pointsNeeded
    }
    
    open func statusProgress() -> Float {
        let homeScreenData = coreRealm.allHomeScreenData().first
        return homeScreenData?.calculateStatusProgress() ?? 0.0
    }
    open func carryOverStatus() -> String? {
        return homeStatuses?.first?.carryOverVitalityStatusName ?? nil
    }
    
    open func currentStatusImage() -> UIImage? {
        guard let statusKey = homeStatuses?.first?.overallVitalityStatusKey else {
            return nil
        }
        return possibleStatusImage(for: statusKey)
    }
    
    // MARK: Product categories
    open func productCategoryCount() -> Int{
        return productCategories?.count ?? 0
    }
    
    open func fetchProductCategories() {
        productCategories = statusRealm.productFeatureCategories()
    }
    
    open func productCategoryKey(for index: Int) -> Int? {
        guard index < productCategories?.count ?? 0 else {
            return nil
        }
        return productCategories?[index].key
    }
    
    open func productCategoryName(for index: Int) -> String? {
        guard index < productCategories?.count ?? 0 else {
            return ""
        }
        return productCategories?[index].name
    }
    
    open func potentialPointsForCategory(for index: Int, productCategories: Results<StatusProductFeatureCategory>?) -> String? {
        guard index < productCategories?.count ?? 0 else {
            return nil
        }
        guard let potentialPoints = productCategories?[index].potentialPoints else {
            return nil
        }
        guard let pointEarnFlag = productCategories?[index].pointsEarningFlag else {
            return nil
        }
        let pointsCategoryLimit = productCategories?[index].pointsCategoryLimit.value ?? 0
        if pointEarnFlag == "EarnUpToPoints" {
            if pointsCategoryLimit > 0 {
                return CommonStrings.Status.PointsIndicationMessageUpTo828(String(describing: pointsCategoryLimit))
            }
            return CommonStrings.Status.PointsIndicationMessageUpTo828(String(describing: potentialPoints))
        } else if pointEarnFlag == "EarnPoints" {
            if pointsCategoryLimit > 0 {
                return CommonStrings.Status.PointsIndicationMessage829(String(describing: pointsCategoryLimit))
            }
            return CommonStrings.Status.PointsIndicationMessage829(String(describing: potentialPoints))
        } else if pointEarnFlag == "EarnUpToPointsLimits" {
            if pointsCategoryLimit > 0 {
                return CommonStrings.Status.PointsIndicationMessageLimit830(String(describing: pointsCategoryLimit))
            }
            return CommonStrings.Status.PointsIndicationMessageLimit830(String(describing: potentialPoints))
        }
        return String(describing: potentialPoints)
    }
    
    open func totalPointsEarnedForCategory(for index: Int, productCategories: Results<StatusProductFeatureCategory>?) -> String? {
        guard index < productCategories?.count ?? 0 else {
            return nil
        }
        guard let potentialPoints = productCategories?[index].potentialPoints else {
            return nil
        }
        guard let earnedPoints = productCategories?[index].pointsEarned else {
            return nil
        }
        if let categoryLimit = productCategories?[index].pointsCategoryLimit.value {
            if earnedPoints >= categoryLimit && categoryLimit > 0 {
                return CommonStrings.Status.PointsEarnedIndicationMessage896(String(describing: earnedPoints))
            }
        }
        if earnedPoints >= potentialPoints {
            return CommonStrings.Status.PointsEarnedIndicationMessage896(String(describing: earnedPoints))
        }
        return nil
    }
    
    open func imageForCategory(for index: Int) -> UIImage? {
        guard index < productCategories?.count ?? 0 else {
            return nil
        }
        guard let key = productCategories?[index].key else {
            return nil
        }
        guard let productCategory = ProductFeatureCategoryRef(rawValue: key) else {
            return nil
        }
        return productCategory.image()
    }
}
