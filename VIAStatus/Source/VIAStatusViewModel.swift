//
//  VIAStatusViewModel.swift
//  VitalityActive
//
//  Created by Joshua Ryan on 9/5/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation

class VIAStatusViewModel {

	// MARK: Public
	var title: String
	var description: String
	var image: UIImage
	var progress: Float = 0.0

	init(title: String, description: String, image: UIImage, progress: Float) {
		self.title = title
		self.description = description
		self.image = image
		self.progress = progress
	}
}
