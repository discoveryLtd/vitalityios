import VIAUIKit

class NextStatusCell: UITableViewCell, Nibloadable {

    @IBOutlet weak var nextStatusBadge: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setup()
    }
    
    func setup() {
        titleLabel.font = UIFont.headlineFont()
        messageLabel.font = UIFont.subheadlineFont()
        messageLabel.textColor = UIColor.darkGray
    }
}
