//
//  StatusIncreasedHeader.swift
//  VitalityActive
//
//  Created by admin on 2017/10/11.
//  Copyright © 2017 Glucode. All rights reserved.
//

import VIAUIKit

public class StatusIncreasedHeader: UIView, Nibloadable {

    @IBOutlet weak var newStatusBadge: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var message: UILabel!
    
    public override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    func setup() {
        title.font = UIFont.title1Font()
        message.font = UIFont.subheadlineFont()
        message.textColor = UIColor.darkGray
    }
    
    public var badge: UIImage? {
        set {
            newStatusBadge.image = newValue
        }
        get {
            if let badgeImage = newStatusBadge.image {
                return badgeImage
            }
            return nil
        }
    }
}
