import VIAUIKit

public class TitleSubtitleTableViewCell: UITableViewCell, Nibloadable {
    @IBOutlet weak public var title: UILabel!
    @IBOutlet weak public var subtitle: UILabel!

    override public func awakeFromNib() {
        super.awakeFromNib()
        title.font = UIFont.headlineFont()
        subtitle.font = UIFont.subheadlineFont()
        subtitle.textColor = UIColor.darkGray
    }

    override public func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
