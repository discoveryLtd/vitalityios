//
//  VIAStatus.h
//  VIAStatus
//
//  Created by Wilmar van Heerden on 2017/10/10.
//  Copyright © 2017 Glucode. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for VIAStatus.
FOUNDATION_EXPORT double VIAStatusVersionNumber;

//! Project version string for VIAStatus.
FOUNDATION_EXPORT const unsigned char VIAStatusVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <VIAStatus/PublicHeader.h>


