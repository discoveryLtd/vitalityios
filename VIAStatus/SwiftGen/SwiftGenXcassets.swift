// Generated using SwiftGen, by O.Halligon — https://github.com/SwiftGen/SwiftGen

#if os(OSX)
import AppKit.NSImage
public typealias VIAStatusColor = NSColor
public typealias VIAStatusImage = NSImage
#elseif os(iOS) || os(tvOS) || os(watchOS)
import UIKit.UIImage
public typealias VIAStatusColor = UIColor
public typealias VIAStatusImage = UIImage
#endif

// swiftlint:disable file_length

public typealias VIAStatusAssetType = VIAStatusImageAsset

public struct VIAStatusImageAsset: Equatable {
	fileprivate var name: String

	public var templateImage: VIAStatusImage {
		let bundle = Bundle(for: BundleToken.self)
		#if os(iOS) || os(tvOS)
		let image = VIAStatusImage(named: name, in: bundle, compatibleWith: nil)?.withRenderingMode(.alwaysTemplate)
		#elseif os(OSX)
		let image = bundle.image(forResource: name).withRenderingMode(.alwaysTemplate)
		#elseif os(watchOS)
		let image = VIAStatusImage(named: name).withRenderingMode(.alwaysTemplate)
		#endif
		guard let result = image else { fatalError("Unable to load template image named \(name).") }
		return result
	}

	public var image: VIAStatusImage {
		let bundle = Bundle(for: BundleToken.self)
		#if os(iOS) || os(tvOS)
		let image = VIAStatusImage(named: name, in: bundle, compatibleWith: nil)
		#elseif os(OSX)
		let image = bundle.image(forResource: name)
		#elseif os(watchOS)
		let image = VIAStatusImage(named: name)
		#endif
		guard let result = image else { fatalError("Unable to load image named \(name).") }
		return result
	}

	public static func ==(lhs: VIAStatusImageAsset, rhs: VIAStatusImageAsset) -> Bool {
        return lhs.image == rhs.image
    }
}

public struct VIAStatusColorAsset {
fileprivate var name: String

#if swift(>=3.2)
@available(iOS 11.0, tvOS 11.0, watchOS 4.0, OSX 10.13, *)
var color: VIAStatusColor {
return VIAStatusColor(asset: self)
}
#endif
}

// swiftlint:disable identifier_name line_length nesting type_body_length type_name
public enum VIAStatusAsset {
  public static let pointsLarge = VIAStatusImageAsset(name: "pointsLarge")
  public static let genericHelpGreen = VIAStatusImageAsset(name: "genericHelpGreen")
  public static let genericPointsGreen = VIAStatusImageAsset(name: "genericPointsGreen")
  public static let steps = VIAStatusImageAsset(name: "steps")
  public static let screening = VIAStatusImageAsset(name: "screening")
  public static let speed = VIAStatusImageAsset(name: "speed")
  public static let nutritionAssessment = VIAStatusImageAsset(name: "nutritionAssessment")
  public static let urineProtein = VIAStatusImageAsset(name: "urineProtein")
  public static let genericHelpBlue = VIAStatusImageAsset(name: "genericHelpBlue")
  public static let nutrition = VIAStatusImageAsset(name: "nutrition")
  public static let hba1c = VIAStatusImageAsset(name: "hba1c")
  public static let calories = VIAStatusImageAsset(name: "calories")
  public static let assessment = VIAStatusImageAsset(name: "assessment")
  public static let vhr = VIAStatusImageAsset(name: "vhr")
  public static let deviceLinking = VIAStatusImageAsset(name: "deviceLinking")
  public static let learnMore = VIAStatusImageAsset(name: "learnMore")
  public static let heartRate = VIAStatusImageAsset(name: "heartRate")
  public static let nonSmokers = VIAStatusImageAsset(name: "nonSmokers")
  public static let cholesterol = VIAStatusImageAsset(name: "cholesterol")
  public static let medal = VIAStatusImageAsset(name: "medal")
  public static let bloodPressure = VIAStatusImageAsset(name: "bloodPressure")
  public static let checkmark = VIAStatusImageAsset(name: "checkmark")
  public static let refresh = VIAStatusImageAsset(name: "refresh")
  public static let genericPointsBlue = VIAStatusImageAsset(name: "genericPointsBlue")
  public static let questionMark = VIAStatusImageAsset(name: "questionMark")
  public static let distance = VIAStatusImageAsset(name: "distance")
  public static let mentalWellbeing = VIAStatusImageAsset(name: "mentalWellbeing")
  public static let getActive = VIAStatusImageAsset(name: "getActive")
  public static let bmi = VIAStatusImageAsset(name: "BMI")
  public static let glucose = VIAStatusImageAsset(name: "glucose")

  // swiftlint:disable trailing_comma
  public static let allColors: [VIAStatusColorAsset] = [
  ]
  public static let allImages: [VIAStatusImageAsset] = [
    pointsLarge,
    genericHelpGreen,
    genericPointsGreen,
    steps,
    screening,
    speed,
    nutritionAssessment,
    urineProtein,
    genericHelpBlue,
    nutrition,
    hba1c,
    calories,
    assessment,
    vhr,
    deviceLinking,
    learnMore,
    heartRate,
    nonSmokers,
    cholesterol,
    medal,
    bloodPressure,
    checkmark,
    refresh,
    genericPointsBlue,
    questionMark,
    distance,
    mentalWellbeing,
    getActive,
    bmi,
    glucose,
  ]
  // swiftlint:enable trailing_comma
  @available(*, deprecated, renamed: "allImages")
  public static let allValues: [VIAStatusAssetType] = allImages
}
// swiftlint:enable identifier_name line_length nesting type_body_length type_name

extension VIAStatusImage {
@available(iOS 1.0, tvOS 1.0, watchOS 1.0, *)
@available(OSX, deprecated,
message: "This initializer is unsafe on macOS, please use the VIAStatusImageAsset.image property")
convenience init!(asset: VIAStatusAssetType) {
#if os(iOS) || os(tvOS)
let bundle = Bundle(for: BundleToken.self)
self.init(named: asset.name, in: bundle, compatibleWith: nil)
#elseif os(OSX) || os(watchOS)
self.init(named: asset.name)
#endif
}
}

extension VIAStatusColor {
#if swift(>=3.2)
@available(iOS 11.0, tvOS 11.0, watchOS 4.0, OSX 10.13, *)
convenience init!(asset: VIAStatusColorAsset) {
let bundle = Bundle(for: BundleToken.self)
#if os(iOS) || os(tvOS)
self.init(named: asset.name, in: bundle, compatibleWith: nil)
#elseif os(OSX)
self.init(named: asset.name, bundle: bundle)
#elseif os(watchOS)
self.init(named: asset.name)
#endif
}
#endif
}

private final class BundleToken {}
