//
//  AppDelegate+ApplicableFeatures+MyHealth.swift
//  VitalityActive
//
//  Created by OJ Garde on 3/17/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import VIACommon

extension AppDelegate{
    
    public func getMyHealthCards() -> [MyHealthCardItem] {
        return [MyHealthCardItem.VitalityAge, MyHealthCardItem.Nutrition]
    }
    
    public var reloadMyHealthOnViewAppear: Bool? {
        get{
            return true
        }
    }
    
    public func shouldSkipMyHealthLandingScreen() -> Bool {
        return false
    }
    
    public var concatenateAgeBlurb: Bool? {
        get {
            return false
        }
    }
}
