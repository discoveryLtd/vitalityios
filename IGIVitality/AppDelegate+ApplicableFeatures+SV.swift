//
//  AppDelegate+ApplicableFeatures+SV.swift
//  VitalityActive
//
//  Created by OJ Garde on 2/9/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import VIACommon
import VitalityKit
import VIAUtilities
import VIAUIKit

extension AppDelegate{
    
    /**
     * Usage:
     * SAVLearnMoreViewController.swift
     **/
    public func showSVLearnMorePartnersLink() -> Bool{
        return true
    }
    
    /**
     * Usage:
     * SAVLandingViewController.swift
     **/
    public func showSVHistoryMenu() -> Bool{
        return false
    }
    
    /**
     * Usage:
     * SVImageDetailViewController.swift
     **/
    public func getSVRemoveButtonLabel(numberOfPhotos:Int) -> String{
        return CommonStrings.Proof.RemoveButton179
    }
    
    /**
     * Usage:
     * SVSelectedPhotosCollectionViewController.swift
     **/
    public func shouldAllowProofGreaterThanSelection() -> Bool{
        return false
    }
    
    /* Screenings and Vaccinations Landing */
    public func getSVLandingHeaderMessage() -> String{
        return CommonStrings.Sv.LandingHealthActionMessage1009
    }
    
    public func getSVCardActionTitle(status: CardStatusTypeRef, metadataEarnedPoints: String) -> String?{
        switch(status){
        case CardStatusTypeRef.NotStarted:  return CommonStrings.GetStartedButtonTitle103
        case CardStatusTypeRef.InProgress:  return CommonStrings.HomeCard.CardEarnedPointsMessage121(metadataEarnedPoints)
        case CardStatusTypeRef.Done:        return CommonStrings.HomeCard.EditUpdateMessage253
        default: return nil
        }
    }
    
    /**
     * Usage:
     * SVLearnMoreViewController.swift
     **/
    public func participatingPartnersLinkHeight() -> CGFloat {
        return 50
    }
    
    /**
     * Usage:
     * SVLandingViewController.swift
     **/
    public func checkForHealthcarePDFDisplay() -> Bool {
        return false
    }
    
    public func getSVCompletionViewModel() -> AnyObject?{
        return SVCompletionViewModel()
    }
    
    public func getSVHealthActionCell(at indexPath: IndexPath, tableView: UITableView,
                                      screeningsTotalPotentialPoints: Int, screeningsTotalEarnedPoints: Int,
                                      vaccinationsTotalPotentialPoints: Int, vaccinationsTotalEarnedPoints: Int) -> SAVHealthActionViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: SAVHealthActionViewCell.defaultReuseIdentifier, for: indexPath) as! SAVHealthActionViewCell
        
        
        
        cell.configureSAVHealthActionMainCell(image: nil, measurableTitle: "", messageIcon: nil,
                                              message: nil, footerMessage: nil, isDisclosureIndicatorHidden: true)
        
        var image: UIImage? = UIImage()
        var messageIcon: UIImage? = nil
        var message: String? = nil
        var headerTitle         = ""
        var hasPoints           = false
        if let row  = HealthActionMenu(rawValue: indexPath.row), let points  = SVPointsValue(rawValue: indexPath.row){
            
            if points == SVPointsValue.screenings && screeningsTotalPotentialPoints > 0 {
                if screeningsTotalEarnedPoints > 0 {
                    image       = VIAUIKitAsset.Sv.savScreenings.image.maskWithColor(UIColor.jungleGreen())
                    hasPoints   = true
                } else {
                    image       = VIAUIKitAsset.Sv.savScreenings.image.maskWithColor(UIColor.mediumGrey())
                }
                
                message     = points.value(screeningsTotalEarnedPoints, screeningsTotalPotentialPoints) ?? ""
                
                headerTitle = row.text() ?? ""
                
            }else if points == SVPointsValue.vaccinations && vaccinationsTotalPotentialPoints > 0 {
                if vaccinationsTotalEarnedPoints > 0 {
                    image       = VIAUIKitAsset.Sv.savVaccinations.image.maskWithColor(UIColor.jungleGreen())
                    hasPoints   = true
                } else {
                    image       = VIAUIKitAsset.Sv.savVaccinations.image.maskWithColor(UIColor.mediumGrey())
                }
                
                message     = points.value(vaccinationsTotalEarnedPoints, vaccinationsTotalPotentialPoints) ?? ""
                
                headerTitle = row.text() ?? ""
            }
            
            messageIcon = VIAUIKitAsset.Sv.savPointsInfo.image
            
            cell.configureSAVHealthActionMainCell(image: image,
                                                  measurableTitle: headerTitle,
                                                  messageIcon: messageIcon,
                                                  message: message,
                                                  footerMessage: nil,
                                                  isDisclosureIndicatorHidden: true,
                                                  completed: hasPoints)
        }
        cell.accessoryType = .none
        return cell
    }
    
    /**
     * Usage:
     * SVHealthActionViewController.swift
     **/
    public func withDefaultStringValueForSVTestedDate() -> Bool{
        return false
    }
}
