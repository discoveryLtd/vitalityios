import VIAActiveRewards
import Foundation
import VIAUIKit
import VIAActiveRewards
import VitalityKit
import VIAUtilities
import VIACommon

class VIAIGIFoodPandaRewardViewController: VIACoordinatedTableViewController, VIAABespokeRewardConsumer, VIAARCopyVoucherRewardCellDelegate {
	
	// MARK: Public
	var dataProvider: VIAARRewardViewModel?
	weak var delegate: VIAABespokeRewardDelegate?
	weak var registrationEmailCell: VIATextFieldTableViewCell?
	
	public var appUrl: String = "https://itunes.apple.com/gb/app/apple-store/id758103884?mt=8"
	
	// TODO: Once Foodpanda app implements scheme, test path
	public var launchUrl: String = "foodpanda.pk://home"
	
	// MARK: Private
	private var leftButton = UIBarButtonItem()
	
	// MARK: Lifecycle
	
	override func viewDidLoad() {
		super.viewDidLoad()
		VIAAppearance.setGlobalTintColorToImproveYourHealth()
		configureTableView()
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		configureBarButtonItems()
	}
	
	override func viewDidAppear(_ animated: Bool) {
        navigationController?.navigationBar.isHidden = false
		super.viewDidAppear(animated)
	}
	
	func configureTableView() {
		
		self.tableView.register(VIAARCopyVoucherRewardCell.nib(), forCellReuseIdentifier: VIAARCopyVoucherRewardCell.defaultReuseIdentifier)
		self.tableView.register(VIAARPartnerAppCell.nib(), forCellReuseIdentifier: VIAARPartnerAppCell.defaultReuseIdentifier)
		self.tableView.register(VIAARRewardMarkUsedCell.nib(), forCellReuseIdentifier: VIAARRewardMarkUsedCell.defaultReuseIdentifier)
		
		tableView.separatorInset = UIEdgeInsets.zero
		
		self.tableView.allowsSelection = false
		self.tableView.estimatedRowHeight = 550
		self.tableView.backgroundColor = UIColor.tableViewBackground()
	}
	
	// MARK: Action buttons
	
	func configureBarButtonItems() {
		
		let cancel = UIBarButtonItem(title: CommonStrings.GenericCloseButtonTitle10, style: .plain, target: self, action: #selector(cancelButtonTapped(_:)))
		let done = UIBarButtonItem(title: CommonStrings.DoneButtonTitle53, style: .plain, target: self, action: #selector(doneButtonTapped(_:)))
		
		if dataProvider?.rewardStatus == AwardedRewardStatusRef.Canceled
			|| dataProvider?.rewardStatus == AwardedRewardStatusRef.IssueFailed {
			self.navigationItem.leftBarButtonItem = nil
			self.navigationItem.rightBarButtonItem = done
		} else if dataProvider?.rewardStatus == AwardedRewardStatusRef.Acknowledged {
			self.navigationItem.leftBarButtonItem = nil
			self.navigationItem.rightBarButtonItem = done
		} else if let _ = dataProvider?.accepted, let codes = dataProvider?.voucherCodes, codes.first != "" {
			self.navigationItem.leftBarButtonItem = nil
			self.navigationItem.rightBarButtonItem = done
		} else {
			self.navigationItem.leftBarButtonItem = cancel
		}
		
		self.navigationItem.hidesBackButton = true
		
		self.navigationItem.title = CommonStrings.Ar.HomescreenCard.RewardsTitle785
	}
	
	// MARK: UITableView datasource
	
	override func numberOfSections(in tableView: UITableView) -> Int {
		return 1
	}
	
	override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return 2
	}
	
	// MARK: UITableView delegate
	
	override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		if indexPath.row == 0 {
			return self.configureRewardCell(indexPath)
		} else if indexPath.row == 1 {
			return self.configureFoodpandaAppCell(indexPath)
		}
		
		return tableView.dequeueReusableCell(withIdentifier: VIATableViewCell.defaultReuseIdentifier, for: indexPath)
	}
	
	override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		return UITableViewAutomaticDimension
	}
	
	// MARK: - Status Specific TableCells
	
	// MARK: Available
	func configureRewardCell(_ indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: VIAARCopyVoucherRewardCell.defaultReuseIdentifier, for: indexPath) as! VIAARCopyVoucherRewardCell
        
        cell.delegate = self
		cell.partnerImage = UIImage(asset: IGIVitalityAsset.Rewards.foodpanda)
		cell.header = dataProvider?.title ?? ""
		cell.title = dataProvider?.subTitle ?? ""
		if let expiration = dataProvider?.expiration {
			let dateString = Localization.dayDateLongMonthyearFormatter.string(from: expiration)
			cell.expiration = CommonStrings.Ar.VoucherExpires658(dateString)
		}
		if let code = dataProvider?.voucherCodes?.first {
			cell.code = code
		}

		cell.copyText = CommonStrings.Ar.Rewards.CopyVoucherCode1418.capitalized
		cell.instructions = CommonStrings.Ar.Rewards.FoodpandaVoucherCodeInstruction1957
		
		return cell
	}
	
	func configureFoodpandaAppCell(_ indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: VIAARPartnerAppCell.defaultReuseIdentifier, for: indexPath) as! VIAARPartnerAppCell
		
		// TODO
		cell.appName = CommonStrings.Ar.Rewards.FoodpandaAppDescription1958
		cell.hideBorder()
		cell.cellImage = UIImage(asset: IGIVitalityAsset.Rewards.foodpandaAppIcon)
		
		if let url = URL(string: launchUrl), UIApplication.shared.canOpenURL(url) {
			cell.targetUrl = launchUrl
			// TODO: 
			cell.appButtonText = "OPEN"
		} else {
			cell.targetUrl = appUrl
			// TODO
			cell.appButtonText = "INSTALL"
		}
		return cell
	}
	
	func configureMarkUsed(_ indexPath: IndexPath) -> UITableViewCell {
		return tableView.defaultTableViewCell()
		//        TODO: Once in scope add code back to show "Mark as Used" back
		//		let cell = tableView.dequeueReusableCell(withIdentifier: VIAARRewardMarkUsedCell.defaultReuseIdentifier, for: indexPath) as! VIAARRewardMarkUsedCell
		//		cell.instructions = "Already used this reward? Mark it as used to move it to your reward history."
		//		cell.actionText = "Mark as Used"
		//		cell.buttonAction = { [weak self] in
		//			if let strongSelf = self, let reward = strongSelf.dataProvider {
		//				strongSelf.delegate?.consumeReward(reward, completion: nil)
		//			}
		//		}
		//
		//		return cell
	}
	
	override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
		return nil
	}
	
	override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
		return CGFloat.leastNormalMagnitude
	}
	
	override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
		return CGFloat.leastNormalMagnitude
	}
	
	// MARK: - VIAABespokeRewardConsumer
	
	func swapReward() {
		if let reward = dataProvider {
			delegate?.consumeReward(reward, completion: nil)
		}
	}
	
	// MARK: - IBAction
	
	@IBAction func cancelButtonTapped (_ sender: Any) {
		delegate?.cancel(consumer: self)
	}
	
	@IBAction func doneButtonTapped (_ sender: Any) {
		self.delegate?.cancel(consumer: self)
	}

	@IBAction func copyCode(_ sender: UIButton) {
		if let code = dataProvider?.voucherCodes?.first {
			UIPasteboard.general.string = code
		}
	}

	// MARK: - VIAABespokeRewardConsumer
	
	func setReward(_ reward: VIAARRewardViewModel) {
		dataProvider = reward
	}
	
	func shouldShowTermsAndConditions() -> Bool {
		let coreRealm = DataProvider.newRealm()
		guard let rewardKey = dataProvider?.rewardkey else {
			return true
		}
		let rewardFeatureIsActive = coreRealm.rewardFeatureActive(with: rewardKey)
		return rewardFeatureIsActive
	}

	
	// MARK: - Private
	
	func showTermsAndConditions(with reward: VIAARRewardViewModel) {
		self.performSegue(withIdentifier: "showRewardTerms", sender: reward)
	}
    
    func copyButtonPressed() {
        showVoucherCodeCopiedAlert()
    }
    
    func showVoucherCodeCopiedAlert() {
        showHud(on: self.view.window, with: VIAUIKitAsset.checkMarkCircle.image, display: CommonStrings.Ar.Rewards.VoucherCodeCopied1419)
    }
}
