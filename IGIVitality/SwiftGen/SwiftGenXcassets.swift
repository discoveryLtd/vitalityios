// Generated using SwiftGen, by O.Halligon — https://github.com/SwiftGen/SwiftGen

#if os(OSX)
import AppKit.NSImage
public typealias IGIVitalityColor = NSColor
public typealias IGIVitalityImage = NSImage
#elseif os(iOS) || os(tvOS) || os(watchOS)
import UIKit.UIImage
public typealias IGIVitalityColor = UIColor
public typealias IGIVitalityImage = UIImage
#endif

// swiftlint:disable file_length

public typealias IGIVitalityAssetType = IGIVitalityImageAsset

public struct IGIVitalityImageAsset: Equatable {
	fileprivate var name: String

	public var templateImage: IGIVitalityImage {
		let bundle = Bundle(for: BundleToken.self)
		#if os(iOS) || os(tvOS)
		let image = IGIVitalityImage(named: name, in: bundle, compatibleWith: nil)?.withRenderingMode(.alwaysTemplate)
		#elseif os(OSX)
		let image = bundle.image(forResource: name).withRenderingMode(.alwaysTemplate)
		#elseif os(watchOS)
		let image = IGIVitalityImage(named: name).withRenderingMode(.alwaysTemplate)
		#endif
		guard let result = image else { fatalError("Unable to load template image named \(name).") }
		return result
	}

	public var image: IGIVitalityImage {
		let bundle = Bundle(for: BundleToken.self)
		#if os(iOS) || os(tvOS)
		let image = IGIVitalityImage(named: name, in: bundle, compatibleWith: nil)
		#elseif os(OSX)
		let image = bundle.image(forResource: name)
		#elseif os(watchOS)
		let image = IGIVitalityImage(named: name)
		#endif
		guard let result = image else { fatalError("Unable to load image named \(name).") }
		return result
	}

	public static func ==(lhs: IGIVitalityImageAsset, rhs: IGIVitalityImageAsset) -> Bool {
        return lhs.image == rhs.image
    }
}

public struct IGIVitalityColorAsset {
fileprivate var name: String

#if swift(>=3.2)
@available(iOS 11.0, tvOS 11.0, watchOS 4.0, OSX 10.13, *)
var color: IGIVitalityColor {
return IGIVitalityColor(asset: self)
}
#endif
}

// swiftlint:disable identifier_name line_length nesting type_body_length type_name
public enum IGIVitalityAsset {
  public enum Rewards {
    public static let easyticketsAppIcon = IGIVitalityImageAsset(name: "easyticketsAppIcon")
    public static let foodpandaAppIcon = IGIVitalityImageAsset(name: "foodpandaAppIcon")
    public static let easytickets = IGIVitalityImageAsset(name: "easytickets")
    public static let foodpanda = IGIVitalityImageAsset(name: "foodpanda")
  }
  public static let loginLogo = IGIVitalityImageAsset(name: "loginLogo")
  public static let homeLogo = IGIVitalityImageAsset(name: "homeLogo")

  // swiftlint:disable trailing_comma
  public static let allColors: [IGIVitalityColorAsset] = [
  ]
  public static let allImages: [IGIVitalityImageAsset] = [
    Rewards.easyticketsAppIcon,
    Rewards.foodpandaAppIcon,
    Rewards.easytickets,
    Rewards.foodpanda,
    loginLogo,
    homeLogo,
  ]
  // swiftlint:enable trailing_comma
  @available(*, deprecated, renamed: "allImages")
  public static let allValues: [IGIVitalityAssetType] = allImages
}
// swiftlint:enable identifier_name line_length nesting type_body_length type_name

extension IGIVitalityImage {
@available(iOS 1.0, tvOS 1.0, watchOS 1.0, *)
@available(OSX, deprecated,
message: "This initializer is unsafe on macOS, please use the IGIVitalityImageAsset.image property")
convenience init!(asset: IGIVitalityAssetType) {
#if os(iOS) || os(tvOS)
let bundle = Bundle(for: BundleToken.self)
self.init(named: asset.name, in: bundle, compatibleWith: nil)
#elseif os(OSX) || os(watchOS)
self.init(named: asset.name)
#endif
}
}

extension IGIVitalityColor {
#if swift(>=3.2)
@available(iOS 11.0, tvOS 11.0, watchOS 4.0, OSX 10.13, *)
convenience init!(asset: IGIVitalityColorAsset) {
let bundle = Bundle(for: BundleToken.self)
#if os(iOS) || os(tvOS)
self.init(named: asset.name, in: bundle, compatibleWith: nil)
#elseif os(OSX)
self.init(named: asset.name, bundle: bundle)
#elseif os(watchOS)
self.init(named: asset.name)
#endif
}
#endif
}

private final class BundleToken {}
