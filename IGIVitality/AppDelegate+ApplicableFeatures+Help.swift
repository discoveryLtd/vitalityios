//
//  AppDelegate+ApplicableFeatures+Help.swift
//  IGIVitality
//
//  Created by Dexter Anthony Ambrad on 6/18/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import VIACommon

extension AppDelegate{
    
    /**
     * Usage:
     * VIAHelpViewController.swift
     **/
    public var shouldLimitSearchResults: Bool? {
        get {
            return false
        }
    }
}

