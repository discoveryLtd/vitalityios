import VIACommon
import VIAUIKit
import VIACore
import UIKit
import VitalityKit

extension AppDelegate: AppearanceColorDataSource {

    class func igiColor() -> UIColor {
        return UIColor(red:0.18431, green:0.43529, blue:0.70980, alpha:1.00000)
    }

    // MARK: AppearanceDataSource

    public var primaryColor: UIColor {
        return AppDelegate.igiColor()
    }

    public var tabBarBackgroundColor: UIColor {
        return .white
    }

    public var tabBarBarTintColor: UIColor {
        return .white
    }

    public var tabBarTintColor: UIColor? {
        return UIColor.primaryColor()
    }

    public var unselectedItemTintColor: UIColor? {
        return nil
    }
    
    public var getInsurerGlobalTint: UIColor? {
        return VIAAppearance.default.primaryColorFromServer
    }
    
    public func getSplashScreenGradientTop() -> UIColor{
        return UIColor(hexString: AppConfigFeature.insurerGradientColor1Hex())
    }
    
    public func getSplashScreenGradientBottom() -> UIColor{
        return UIColor(hexString: AppConfigFeature.insurerGradientColor2Hex())
    }
}

extension AppDelegate: AppearanceIconographyDataSource {
    
    public func homeLogo(for locale: Locale) -> UIImage {
        return IGIVitalityAsset.homeLogo.image
    }
    
    public func loginLogo(for locale: Locale) -> UIImage {
        return IGIVitalityAsset.loginLogo.image
    }
    
    public func splashLogo(for locale: Locale) -> UIImage {
        return IGIVitalityAsset.loginLogo.image
    }
}
