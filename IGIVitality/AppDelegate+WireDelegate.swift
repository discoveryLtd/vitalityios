import Foundation
import VitalityKit

extension AppDelegate: WireDelegate {
    
    public var qaFrankfurtURL: URL {
        return URL(string: "")!
    }
    
//    public var devBaseURL: URL {
//        return URL(string: "https://dev.vitalityservicing.com/v1/api")!
//    }
    
    public var testBaseURL: URL {
        return URL(string: "https://test.vitalityservicing.com/v1/api")!
    }
    
    public var qaBaseURL: URL {
        return URL(string: "https://qa.vitalityactive.com/api")!
    }
    
    public var productionBaseURL: URL {
        return URL(string: "https://www.vitalityactive.com/api")!
    }
    
    public var performanceBaseURL: URL {
        return URL(string: "")!
    }
    
    public var qaSouthKoreaBaseURL: URL{
        return URL(string: "")!
    }
}

extension AppDelegate{
    
//    public var devAPIManagerIdentifier: String {
//        return "bkQ4cElmMmZndm5WN0kzcU9MTGVpY3BDaFhZYTpCX3djVzNIVDZ5UTA5emdKSDVOa2tOMmtsOTRh"
//    }
    
    public var testAPIManagerIdentifier: String {
        return "WDVlODVDQ3RQTVFpOWpRZ2p4TERSTkxXSjV3YTpQSVZieHkyanRiOFMwRDc1NHJMa0FXWjR2aTBh"
    }
    
    public var qaAPIManagerIdentifier: String {
        return "bnlSWG9nWFJfWjdCY2Z2YWtwa2pyNnVzSWtJYTp6RHZPQllWZzJvc0poZ1hLc0tGNW1YRmpMX3Nh"
    }
    
    public var productionAPIManagerIdentifier: String {
        return "TTVXaUNqbUo1Nl9mRnlqOVBGMUViY3BBQ3djYTp1XzdSdUx4TTNibXdaYXN3MDJxQkRxbEd0SE1h"
    }
    
    public var qaFrankfurtAPIManagerIdentifier: String {
        return "0000000000000000000000000000000000000000000000000000000000000000000000000000"
    }
    
    public var performanceAPIManagerIdentifier: String {
        return ""
    }
    
    public var qaSouthKoreaAPIManagerIdentifier: String {
        return ""
    }
}
