import Foundation
import UIKit
import VIACore
import VIAUtilities
import VIAUIKit

import Firebase

@UIApplicationMain
public class AppDelegate: UIResponder, UIApplicationDelegate, VitalityActiveConfigurer {

    // MARK: Properties

    public var window: UIWindow?

    // MARK: Convenience

    public class func currentDelegate() -> AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }

    // MARK: UIApplicationDelegate

    public func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]? = nil) -> Bool {

        // Analytics
        FirebaseConfiguration.shared.setLoggerLevel(.min)
        FirebaseApp.configure()
        GoogleTagManagerPatch.patchGoogleTagManagerLogging()

        configureVitalityActiveDelegate()

        /*
         * This will disable the logging of Storyboard and XIB files related warnings.
         * Let's enable this during performance testing or refactoring.
         */
        UserDefaults.standard.setValue(false, forKey: "_UIConstraintBasedLayoutLogUnsatisfiable")
        UserDefaults.standard.setValue(false, forKey: "_UIViewAlertForUnsatisfiableConstraints")
        
        return true
    }

    public func applicationWillResignActive(_ application: UIApplication) {
    }

    public func applicationDidEnterBackground(_ application: UIApplication) {
    }

    public func applicationWillEnterForeground(_ application: UIApplication) {
    }

    public func applicationDidBecomeActive(_ application: UIApplication) {
    }

    public func applicationWillTerminate(_ application: UIApplication) {
    }


    // MARK: URL scheme launch

    public func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        NotificationInfo.postNotification(from: url)
        return true
    }
}

extension AppDelegate: ViewControllerDelegateDataSource{
    
    public func analyticsLogEvent(from originClass: AnyClass) {
        let screenName = NSStringFromClass(originClass)
        debugPrint("logEvent: \(screenName)")
        Analytics.logEvent("screen_view_dup", parameters: ["screen_name": screenName])
    }
}
