import UIKit
import VIAUIKit
import VitalityKit
import VIAPartners

public class NonSmokersPartnerDetailsViewController: PartnersDetailTableViewController {
    
    override public func retrievePartnerDetails() {
        viewModel?.requestEligibilityContent(completion: { [weak self] (error) in
            
            guard error == nil else {
                self?.handleError(error)
                return
            }
            
            self?.tableView.reloadData()
            self?.removeStatusView()
            self?.tableView.backgroundColor = .white
            self?.tableView.separatorColor = .white
        })
    }
    
    override public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
}
