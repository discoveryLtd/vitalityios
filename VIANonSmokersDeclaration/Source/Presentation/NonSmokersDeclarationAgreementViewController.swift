import VitalityKit
import VIAUIKit
import VIACommon
import VIAUtilities

class NonSmokersDeclarationAgreementViewController: CustomTermsConditionsViewController, KnowYourHealthTintable {

    override open func viewDidLoad() {
        super.viewDidLoad()

        self.viewModel = CustomTermsConditionsViewModel(articleId: AppConfigFeature.contentId(for: .NonsmokersDclrtnContent))
        showLeftButton = false
        rightButtonTitle = CommonStrings.DeclareButtonTitle114
        title = CommonStrings.HomeCard.CardTitle96
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        navigationController?.makeNavigationBarTransparent()
        navigationController?.navigationBar.isHidden = false
    }

    override func rightButtonTapped(_ sender: UIBarButtonItem?) {
        if (VitalityProductFeature.isEnabled(.NonSmokersDSConsent)) {
            self.performSegue(withIdentifier: "showPrivacyPolicy", sender: nil)
        } else {
            performDeclaration()
        }
    }

    override func leftButtonTapped(_ sender: UIBarButtonItem?) {
        self.performSegue(withIdentifier: "unwindFromNonSmokersPrivacyPolicyToOnboarding", sender: nil)
    }

    func performDeclaration() {
        self.showHUDOnWindow()
        let events = [EventTypeRef.NonsmokersDeclrtn]
        viewModel?.agreeToTermsAndConditions(with: events, completion: { [weak self] (error) in
            self?.hideHUDFromWindow()
            guard error == nil else {
                self?.handleErrorOccurred(error)
                return
            }
            NotificationCenter.default.post(name: .VIANonSmokersDeclarationCompletedNotification, object: nil)
            self?.performSegue(withIdentifier: StoryboardSegue.NonSmokersDeclaration.showCompletion.rawValue, sender: nil)
        })
    }

    func handleErrorOccurred(_ error: Error?) {
        guard error == nil else {
            debugPrint(error as Any)
            switch error {
            case is BackendError:
                self.handleBackendErrorWithAlert(error as! BackendError, tryAgainAction: { [weak self] in
                    self?.rightButtonTapped(nil)
                })
            default:
                self.displayUnknownServiceErrorOccurredAlert()
            }
            self.hideHUDFromWindow()
            return
        }
    }

    // MARK: Navigation

    @IBAction public func unwindToDeclarationFromPrivacyPolicy(segue: UIStoryboardSegue) {
        debugPrint("unwindToDeclarationFromPrivacyPolicy")
    }

    override func returnToPreviousView() {
        self.performSegue(withIdentifier: "unwindFromNonSmokersPrivacyPolicyToOnboarding", sender: nil)
    }
}
