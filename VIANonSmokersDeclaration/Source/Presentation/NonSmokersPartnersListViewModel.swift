import UIKit

import VitalityKit
import VIAUtilities
import VIAPartners

import RealmSwift

public class NonSmokersPartnersListViewModel: PartnersListTableViewModel {
    
    var partnersData: [PartnersListTableViewModel.PartnerData] = []
    
    func partnerCount() -> Int? {
        return partnersData.count
    }
    
    public override func partnerDetails(at indexPath: IndexPath) -> PartnersListTableViewModel.PartnerData? {
        return partnersData[indexPath.row]
    }
    
    // MARK: - Configuration
    
    public override func configurePartnerData() {
        coreRealm.refresh()
        
        guard let partnerCategory = coreRealm.partnerCategory(with: partnerCategoryType.rawValue) else {
            return
        }
        
        let groups = partnerCategory.partnerGroups.filter({ $0.key == ProductFeatureCategoryRef.StopSmoking.rawValue})
        
        for group in groups {
            for partner in group.partners {
                let partnerData = PartnersListTableViewModel.PartnerData(name: partner.name, longDescription: partner.longDescription, description: partner.shortDescription, logoFileName: partner.logoFileName, typeKey: partner.typeKey, learnMoreUrl: nil, webViewContent: nil, heading: nil)
                
                partnersData.append(partnerData)
            }
        }
    }
}
