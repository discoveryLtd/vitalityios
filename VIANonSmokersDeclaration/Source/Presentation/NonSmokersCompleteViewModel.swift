import Foundation
import UIKit

import VIAUIKit
import VitalityKit

public class NonSmokersCompleteViewModel: AnyObject, CirclesViewModel {

    public required init() {
    }

    public var image: UIImage? {
        return VIAUIKitAsset.ActiveRewards.Onboarding.arOnboardingActivated.image
    }

    public var heading: String? {
        return CommonStrings.Confirmation.CompletedTitle117
    }

    public var message: String? {
        return CommonStrings.Confirmation.CompletedMessage118
    }

    public var footnote: String? {
        return CommonStrings.Confirmation.CompletedFootnotePointsMessage119
    }

    public var buttonTitle: String? {
        return CommonStrings.GreatButtonTitle120
    }
    
    public var buttonHighlightedTextColor: UIColor? {
        return UIColor.knowYourHealthGreen()
    }

    public var gradientColor: Color {
        return .green
    }
}
