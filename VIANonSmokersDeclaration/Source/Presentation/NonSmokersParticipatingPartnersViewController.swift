import VIAUIKit
import VitalityKit
import VIAUtilities
import VIAPartners

public class NonSmokersParticipatingPartnersViewController: VIATableViewController {
    
    public var viewModel = NonSmokersPartnersListViewModel(withPartnerType: .WellnessPartners)
    public var partnersData: [NonSmokersPartnersListViewModel.PartnerData]?
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = CommonStrings.ParticipatingPartnersTitle262
        configureAppearance()
        configureTableView()
        loadPartners()
    }
    
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.hideBackButtonTitle()
    }
    
    func configureTableView() {
        self.tableView.register(NonSmokersParticipatingPartnersHeaderCell.nib(), forCellReuseIdentifier: NonSmokersParticipatingPartnersHeaderCell.defaultReuseIdentifier)
        self.tableView.register(NonSmokersParticipatingPartnerCell.nib(), forCellReuseIdentifier: NonSmokersParticipatingPartnerCell.defaultReuseIdentifier)
        self.tableView.estimatedRowHeight = 75
    }
    
    func configureAppearance() {
        navigationController?.makeNavigationBarTransparent()
        navigationController?.navigationBar.barTintColor = .day()
    }
    
    // MARK: - UITableView DataSource
    
    enum Sections: Int, EnumCollection {
//        case header
        case partners
        
        func numberOfRows() -> Int {
            return 3
//            switch self {
//            case .header:
//                return 1
//            case .partners:
//                return 3
//            }
        }
    }
    
    public override func numberOfSections(in tableView: UITableView) -> Int {
        return Sections.allValues.count
    }
    
    public override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.partnerCount() ?? 0
        //if let theSection = Sections(rawValue: section) {
        //    return theSection.numberOfRows()
        //}
        
        //return 0
    }
    
    public override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let section = Sections(rawValue: indexPath.section) else {
            return UITableViewCell(style: .default, reuseIdentifier: "Cell")
        }
        
        return participatingPartnerCell(at: indexPath)
        
//        switch section {
//        case .header:
//            return headerViewCell(at: indexPath)
//        case .partners:
//            return participatingPartnerCell(at: indexPath)
//        
//        }
    }
    
    func headerViewCell(at indexPath: IndexPath) -> UITableViewCell {
        guard let cell = self.tableView.dequeueReusableCell(withIdentifier: NonSmokersParticipatingPartnersHeaderCell.defaultReuseIdentifier, for: indexPath) as? NonSmokersParticipatingPartnersHeaderCell else {
            return tableView.defaultTableViewCell()
        }
        
        cell.title = CommonStrings.Nsd.PartnerDetailTitle1045
        cell.message = CommonStrings.Nsd.PartnerDetailMessage1046
        cell.selectionStyle = .none
        
        return cell
    }
    
    func participatingPartnerCell(at indexPath: IndexPath) -> UITableViewCell {
        guard let cell = self.tableView.dequeueReusableCell(withIdentifier: NonSmokersParticipatingPartnerCell.defaultReuseIdentifier, for: indexPath) as? NonSmokersParticipatingPartnerCell else {
            return tableView.defaultTableViewCell()
        }
        
        if let partnersDetails = partnersData {
            if partnersDetails.count > indexPath.row {
                cell.partnerName = partnersDetails[indexPath.row].name
                DispatchQueue.global(qos: .background).async { [weak self] in
                    self?.viewModel.partnerLogo(partner: partnersDetails[indexPath.row], indexPath: indexPath) { logo, partner, indexPath in
                        DispatchQueue.main.async {
                            if cell.partnerName == partner.name  {
                                cell.cellImage = logo
                            }
                        }
                    }
                }
            } else {
                
                cell.partnerName = "Partner \(indexPath.row + 1)"
                // TODO revert
//                cell.cellImage = VIACoreAsset.NonSmokersDeclaration.nonsmokersNoimagePartner.image
                cell.cellImageView.contentMode = .center
                cell.cellImageView.clipsToBounds = true
                cell.cellImageView.sizeToFit()
            }
        }
        
        cell.selectionStyle = .none
        
        return cell
    }
    
    // MARK: - UITableView Delegate
    
    public override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        if Sections.allValues[section] == .header {
//            return CGFloat.leastNormalMagnitude
//        }
        return UITableViewAutomaticDimension
    }
    
    public override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    public override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let partnerDetails = partnersData {
            if partnerDetails.count > indexPath.row {
                performSegue(withIdentifier: "showPartnerDetails", sender: self)
            }
        }
    }
    
    // MARK: - Navigation
    
    public override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showPartnerDetails" {
            guard let indexPath = tableView.indexPathForSelectedRow, let partnerDetails = viewModel.partnerDetails(at: indexPath), let detailViewController = segue.destination as? PartnersDetailTableViewController else { return }
            let detailViewModel = PartnersDetailTableViewModel(for: partnerDetails)
            detailViewController.viewModel = detailViewModel
        }
    }
    
    //MARK: Networking
    
    func loadPartners() {
        showFullScreenHUD()
        viewModel.requestPartnersByCategory(completion: { [weak self] (error) in
            self?.hideFullScreenHUD()
            
            guard error == nil else {
                self?.handleError(error)
                return
            }
            self?.viewModel.configurePartnerData()
            if let tempData = self?.viewModel.partnersData {
                self?.partnersData = tempData
            }
            self?.tableView.reloadData()
        })
    }
    
    func handleError(_ error: Error?) {
        let statusView = handleBackendErrorWithStatusView((error as? BackendError) ?? .other, tryAgainAction: { [weak self] in
            self?.loadPartners()
        })
        configureStatusView(statusView)
    }
}
