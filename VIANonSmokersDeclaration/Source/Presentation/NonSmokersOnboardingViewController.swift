import VitalityKit
import VIAUIKit

class NonSmokersOnboardingViewController: VIAOnboardingViewController, KnowYourHealthTintable {

    override func awakeFromNib() {
        super.awakeFromNib()

        self.viewModel = NonSmokersOnboardingViewModel()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        navigationController?.navigationBar.isHidden = false
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        self.hideBackButtonTitle()
    }

    override func mainButtonTapped(_ sender: UIButton) {
        self.performSegue(withIdentifier: "showAgreement", sender: nil)
    }

    override func alternateButtonTapped(_ sender: UIButton) {
        self.performSegue(withIdentifier: "learnMoreSegue", sender: self)
    }

    // MARK: Navigation

    @IBAction public func unwindFromNonSmokersPrivacyPolicyToOnboarding(segue: UIStoryboardSegue) {
        debugPrint("unwindFromNonSmokersPrivacyPolicyToOnboarding")
    }

}
