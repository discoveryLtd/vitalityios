import Foundation

import VIAUIKit
import VitalityKit
import VIAUtilities

public final class NonSmokersLearnMoreViewController: VIATableViewController, KnowYourHealthTintable {
    
    let viewModel = NonSmokersLearnMoreViewModel()

    override public func viewDidLoad() {
        super.viewDidLoad()

        self.title = viewModel.title
        self.configureAppearance()
        self.configureTableView()
    }

    override public func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        self.hideBackButtonTitle()
    }

    func configureAppearance() {
        navigationController?.makeNavigationBarTransparent()
        navigationController?.navigationBar.barTintColor = UIColor.white
    }

    func configureTableView() {
        self.tableView.register(VIAGenericContentCell.nib(), forCellReuseIdentifier: VIAGenericContentCell.defaultReuseIdentifier)
        self.tableView.register(VIATableViewCell.nib(), forCellReuseIdentifier: VIATableViewCell.defaultReuseIdentifier)
        self.tableView.estimatedRowHeight = 150
        self.tableView.sectionHeaderHeight = 0
        self.tableView.sectionFooterHeight = 0
        self.tableView.estimatedSectionHeaderHeight = 0
        self.tableView.estimatedSectionFooterHeight = 0
        self.tableView.contentInset = UIEdgeInsets(top: 25, left: 0, bottom: 0, right: 0)
        self.tableView.backgroundColor = .white
        self.tableView.separatorColor = UIColor.white.withAlphaComponent(0.5)
    }

    // MARK: - UITableView datasource
    
    enum Sections: Int, EnumCollection {
        case content
        case menuItems
        
        func numberOfRows() -> Int {
            let identifier = Bundle.main.object(forInfoDictionaryKey: "VAAppConfigIdentifier") as? String
            switch self {
            case .content:
                return identifier != "Sumitomo" && identifier != "Ecuador" ? 4 : 3
            case .menuItems:
                return identifier != "Sumitomo" && identifier != "IGIVitality" && identifier != "Ecuador" ? 1 : 0
            }
        }
    }

    override public func numberOfSections(in tableView: UITableView) -> Int {
        return Sections.allValues.count
    }

    override public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let theSection = Sections(rawValue: section) {
            return theSection.numberOfRows()
        }
        
        return 0
    }

    // MARK: UITableView delegate
    
    public override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let section = Sections(rawValue: indexPath.section) {
            if section == .content && indexPath.row < section.numberOfRows() - 1 {
                cell.separatorInset = UIEdgeInsetsMake(0, max(UIScreen.main.bounds.size.width, UIScreen.main.bounds.size.height), 0, 0)
            }
        }
    }

    override public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let section = Sections(rawValue: indexPath.section) else { return UITableViewCell(style: .default, reuseIdentifier: "Cell") }
        
        switch section {
        case .content:
            return self.configureContentCell(indexPath)
        case .menuItems:
            return self.configureMenuItemCell(indexPath)
        }
    }
    
    func configureContentCell(_ indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: VIAGenericContentCell.defaultReuseIdentifier, for: indexPath) as! VIAGenericContentCell
        cell.isUserInteractionEnabled = false
        
        let item = viewModel.contentItems[indexPath.row]
        
        if indexPath.row == 0 {
            cell.customHeaderFont = viewModel.headingTitleFont
            cell.customContentFont = viewModel.headingMessageFont
        } else {
            cell.customHeaderFont = viewModel.sectionTitleFont
            cell.customContentFont = viewModel.sectionMessageFont
        }
        cell.header = item.header
        cell.content = item.content
        cell.cellImage = item.image
        cell.cellImageTintColor = viewModel.imageTint 
        cell.layoutIfNeeded()
        
        return cell
    }
    
    func configureMenuItemCell(_ indexPath: IndexPath) -> UITableViewCell {
        guard let cell = self.tableView.dequeueReusableCell(withIdentifier: VIATableViewCell.defaultReuseIdentifier, for: indexPath) as? VIATableViewCell else {
            return self.tableView.defaultTableViewCell()
        }
        
        cell.labelText = CommonStrings.ParticipatingPartnersTitle262
        cell.cellImage = VIANonSmokersDeclarationAsset.NonSmokersDeclaration.cardActiveRewards.image
        cell.cellImageTintColor = viewModel.imageTint
        cell.accessoryType = .disclosureIndicator
        cell.selectionStyle = .none
        
        return cell
    }

    override public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let section = Sections(rawValue: indexPath.section)
        
        if section == .menuItems && indexPath.row == 0 {
            self.performSegue(withIdentifier: "showParticipatingPartners", sender: nil)
        }
    }


    // MARK: Navigation

    @IBAction public func unwindToLearnMoreFromParticipatingPartners (segue: UIStoryboardSegue) {

    }

}
