import VitalityKit
import VIAUIKit

public class NonSmokersOnboardingViewModel: AnyObject, OnboardingViewModel {
    public var heading: String {
        return CommonStrings.HomeCard.CardTitle96
    }

    public var contentItems: [OnboardingContentItem] {
        var items = [OnboardingContentItem]()
        items.append(OnboardingContentItem(heading: CommonStrings.Onboarding.Section1Title99,
                                           content: CommonStrings.Onboarding.Section1Message100,
                                           image: VIANonSmokersDeclarationAsset.NonSmokersDeclaration.pointsIconOnboarding.image))
        items.append(OnboardingContentItem(heading: CommonStrings.Onboarding.Section2Title101,
                                           content: CommonStrings.Onboarding.Section2Message102,
                                           image: VIANonSmokersDeclarationAsset.NonSmokersDeclaration.smokingIconOnboarding.image))
        return items
    }

    public var buttonTitle: String {
        return CommonStrings.GetStartedButtonTitle103
    }

    public var alternateButtonTitle: String? {
        return CommonStrings.LearnMoreButtonTitle104
    }

    public var gradientColor: Color {
        return .green
    }
    
    public var mainButtonHighlightedTextColor: UIColor? {
        return .knowYourHealthGreen()
    }
}
