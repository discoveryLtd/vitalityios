import UIKit

import VIAUIKit

class NonSmokersCompleteViewController: VIACirclesViewController {

    override func awakeFromNib() {
        super.awakeFromNib()

        self.viewModel = NonSmokersCompleteViewModel()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.post(name: .VIANonSmokersDeclarationCompletedNotification, object: nil)
    }
    
    override func buttonTapped(_ sender: UIButton) {
        self.performSegue(withIdentifier: "unwindToHomeViewControllerFromNSDComplete", sender: nil)
    }
}
