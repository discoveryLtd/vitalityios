import VIAUIKit
import VitalityKit
import VIACommon

public class NonSmokersLearnMoreViewModel: AnyObject, VIALearnMoreViewModel {
    
    public var title: String {
        return CommonStrings.LearnMoreButtonTitle104
    }
    
    public var contentItems: [LearnMoreContentItem] {
        var items = [LearnMoreContentItem]()
        
        items.append(LearnMoreContentItem(
            header: CommonStrings.HomeCard.CardTitle96,
            content: CommonStrings.LearnMore.GroupMessage105
        ))
        
        items.append(LearnMoreContentItem(
            header: CommonStrings.LearnMore.Section1Title106,
            content: CommonStrings.LearnMore.Section1Message107,
            image: VIANonSmokersDeclarationAsset.NonSmokersDeclaration.nsdLearnMore1.templateImage
        ))

        items.append(LearnMoreContentItem(
            header: CommonStrings.LearnMore.Section2Title108,
            content: CommonStrings.LearnMore.Section2Message109,
            image: VIANonSmokersDeclarationAsset.NonSmokersDeclaration.nsdLearnMore2.templateImage
        ))

        let identifier = Bundle.main.object(forInfoDictionaryKey: "VAAppConfigIdentifier") as? String
        if identifier != "Sumitomo" && identifier != "Ecuador" {
            items.append(LearnMoreContentItem(
                header: CommonStrings.LearnMore.Section3Title110,
                content: CommonStrings.LearnMore.Section3Message111,
                image: VIANonSmokersDeclarationAsset.NonSmokersDeclaration.nsdLearnMore3.templateImage
            ))
        }
        
        return items
    }
    
    public var imageTint: UIColor {
        return .knowYourHealthGreen()
    }
}
