import Foundation

import VIAUIKit
import VitalityKit

class NonSmokersParticipatingPartnersHeaderCell: UITableViewCell, Nibloadable {
    
    // MARK: - Outlets
    
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var messageLabel: UILabel!
    
    // MARK: - Properties
    
    public var title: String? {
        get {
            return self.titleLabel.text
        }
        set {
            self.titleLabel.text = newValue
        }
    }
    
    public var message: String? {
        get {
            return self.messageLabel.text
        }
        set {
            self.messageLabel.text = newValue
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        titleLabel.text = nil
        titleLabel.numberOfLines = 1
        titleLabel.font = UIFont.title2Font()
        titleLabel.textColor = UIColor.night()
        
        messageLabel.text = nil
        messageLabel.numberOfLines = 2
        messageLabel.font = UIFont.subheadlineFont()
        messageLabel.textColor = UIColor.darkGrey()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.layer.removeAllBorderLayers()
        self.layer.addBorder(edge: .top)
        self.layer.addBorder(edge: .bottom)
    }
}
