import UIKit

import VIAUIKit

import TTTAttributedLabel

class NonSmokersParticipatingPartnerCell: UITableViewCell, Nibloadable {
    
    // MARK: - Outlets
    
    @IBOutlet weak var cellImageView: UIImageView!
    @IBOutlet weak var partnerNameLabel: TTTAttributedLabel!
    
    // Properties: - Properties
    
    public var cellImage: UIImage? {
        get {
            return self.cellImageView.image
        }
        set {
            self.cellImageView.image = newValue
        }
    }
    
    public var partnerName: String? {
        get {
            return self.partnerNameLabel.text as? String
        }
        set {
            self.partnerNameLabel.text = newValue
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.partnerNameLabel.text = nil
        self.partnerNameLabel.font = UIFont.headlineFont()
        self.partnerNameLabel.textColor = UIColor.night()
        
        self.accessoryType = .disclosureIndicator
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        let labelFrame = self.convert(partnerNameLabel.frame, from: partnerNameLabel.superview)
        self.separatorInset = UIEdgeInsets(top: 0, left: labelFrame.origin.x, bottom: 0, right: 0)
    }
}
