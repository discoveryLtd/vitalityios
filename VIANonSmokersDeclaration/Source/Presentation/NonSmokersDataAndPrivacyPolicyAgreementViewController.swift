import VitalityKit
import VIAUtilities
import VIAUIKit
import VIACommon

class NonSmokersDataAndPrivacyPolicyAgreementViewController: CustomTermsConditionsViewController, KnowYourHealthTintable {

    // MARK: View lifecycle

    override open func viewDidLoad() {
        super.viewDidLoad()

        self.viewModel = CustomTermsConditionsViewModel(articleId: AppConfigFeature.contentId(for: .NonsmokersDSConsentContent))
    }

    override func configureAppearance() {
        super.configureAppearance()
        
        navigationController?.makeNavigationBarInvisible()
        navigationController?.navigationBar.isHidden = true
    }

    // MARK: Action buttons

    override func leftButtonTapped(_ sender: UIBarButtonItem?) {
        if (VitalityProductFeature.isEnabled(.NonSmokersDSConsent)) {
            self.performSegue(withIdentifier: "unwindToDeclarationFromPrivacyPolicy", sender: nil)
        } else {
            self.performSegue(withIdentifier: "unwindFromNonSmokersPrivacyPolicyToOnboarding", sender: nil)
        }
    }

    override func rightButtonTapped(_ sender: UIBarButtonItem?) {
        self.showHUDOnWindow()
        var events = [EventTypeRef.NonsmokersDeclrtn]
        if (VitalityProductFeature.isEnabled(.NonSmokersDSConsent)) {
            events.append(EventTypeRef.NonsmokersDatAgr)
        }

        viewModel?.agreeToTermsAndConditions(with: events, completion: { [weak self] (error) in
            self?.hideHUDFromWindow()
            guard error == nil else {
                self?.handleErrorOccurred(error)
                return
            }

            NotificationCenter.default.post(name: .VIANonSmokersDeclarationCompletedNotification, object: nil)
            self?.performSegue(withIdentifier: "showCompletion", sender: nil)
        })
    }

    func handleErrorOccurred(_ error: Error?) {
        guard error == nil else {
            debugPrint(error as Any)
            switch error {
            case is BackendError:
                self.handleBackendErrorWithAlert(error as! BackendError, tryAgainAction: { [weak self] in
                    self?.rightButtonTapped(nil)
                })
            default:
                self.displayUnknownServiceErrorOccurredAlert()
            }
            self.hideHUDFromWindow()
            return
        }
    }

    override func returnToPreviousView() {
        if (VitalityProductFeature.isEnabled(.NonSmokersDSConsent)) {
            self.performSegue(withIdentifier: "unwindToDeclarationFromPrivacyPolicy", sender: nil)
        } else {
            self.performSegue(withIdentifier: "unwindFromNonSmokersPrivacyPolicyToOnboarding", sender: nil)
        }
    }
}
