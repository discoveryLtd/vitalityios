// Generated using SwiftGen, by O.Halligon — https://github.com/SwiftGen/SwiftGen

#if os(OSX)
import AppKit.NSImage
public typealias VIANonSmokersDeclarationColor = NSColor
public typealias VIANonSmokersDeclarationImage = NSImage
#elseif os(iOS) || os(tvOS) || os(watchOS)
import UIKit.UIImage
public typealias VIANonSmokersDeclarationColor = UIColor
public typealias VIANonSmokersDeclarationImage = UIImage
#endif

// swiftlint:disable file_length

public typealias VIANonSmokersDeclarationAssetType = VIANonSmokersDeclarationImageAsset

public struct VIANonSmokersDeclarationImageAsset: Equatable {
	fileprivate var name: String

	public var templateImage: VIANonSmokersDeclarationImage {
		let bundle = Bundle(for: BundleToken.self)
		#if os(iOS) || os(tvOS)
		let image = VIANonSmokersDeclarationImage(named: name, in: bundle, compatibleWith: nil)?.withRenderingMode(.alwaysTemplate)
		#elseif os(OSX)
		let image = bundle.image(forResource: name).withRenderingMode(.alwaysTemplate)
		#elseif os(watchOS)
		let image = VIANonSmokersDeclarationImage(named: name).withRenderingMode(.alwaysTemplate)
		#endif
		guard let result = image else { fatalError("Unable to load template image named \(name).") }
		return result
	}

	public var image: VIANonSmokersDeclarationImage {
		let bundle = Bundle(for: BundleToken.self)
		#if os(iOS) || os(tvOS)
		let image = VIANonSmokersDeclarationImage(named: name, in: bundle, compatibleWith: nil)
		#elseif os(OSX)
		let image = bundle.image(forResource: name)
		#elseif os(watchOS)
		let image = VIANonSmokersDeclarationImage(named: name)
		#endif
		guard let result = image else { fatalError("Unable to load image named \(name).") }
		return result
	}

	public static func ==(lhs: VIANonSmokersDeclarationImageAsset, rhs: VIANonSmokersDeclarationImageAsset) -> Bool {
        return lhs.image == rhs.image
    }
}

public struct VIANonSmokersDeclarationColorAsset {
fileprivate var name: String

#if swift(>=3.2)
@available(iOS 11.0, tvOS 11.0, watchOS 4.0, OSX 10.13, *)
var color: VIANonSmokersDeclarationColor {
return VIANonSmokersDeclarationColor(asset: self)
}
#endif
}

// swiftlint:disable identifier_name line_length nesting type_body_length type_name
public enum VIANonSmokersDeclarationAsset {
  public enum NonSmokersDeclaration {
    public static let nsdLearnMore3 = VIANonSmokersDeclarationImageAsset(name: "nsdLearnMore_3")
    public static let nsdLearnMore2 = VIANonSmokersDeclarationImageAsset(name: "nsdLearnMore_2")
    public static let cardActiveRewards = VIANonSmokersDeclarationImageAsset(name: "cardActiveRewards")
    public static let smokingIconOnboarding = VIANonSmokersDeclarationImageAsset(name: "smokingIconOnboarding")
    public static let nsdLearnMore1 = VIANonSmokersDeclarationImageAsset(name: "nsdLearnMore_1")
    public static let nonsmokersNoimagePartner = VIANonSmokersDeclarationImageAsset(name: "nonsmokersNoimagePartner")
    public static let pointsIconOnboarding = VIANonSmokersDeclarationImageAsset(name: "pointsIconOnboarding")
  }

  // swiftlint:disable trailing_comma
  public static let allColors: [VIANonSmokersDeclarationColorAsset] = [
  ]
  public static let allImages: [VIANonSmokersDeclarationImageAsset] = [
    NonSmokersDeclaration.nsdLearnMore3,
    NonSmokersDeclaration.nsdLearnMore2,
    NonSmokersDeclaration.cardActiveRewards,
    NonSmokersDeclaration.smokingIconOnboarding,
    NonSmokersDeclaration.nsdLearnMore1,
    NonSmokersDeclaration.nonsmokersNoimagePartner,
    NonSmokersDeclaration.pointsIconOnboarding,
  ]
  // swiftlint:enable trailing_comma
  @available(*, deprecated, renamed: "allImages")
  public static let allValues: [VIANonSmokersDeclarationAssetType] = allImages
}
// swiftlint:enable identifier_name line_length nesting type_body_length type_name

extension VIANonSmokersDeclarationImage {
@available(iOS 1.0, tvOS 1.0, watchOS 1.0, *)
@available(OSX, deprecated,
message: "This initializer is unsafe on macOS, please use the VIANonSmokersDeclarationImageAsset.image property")
convenience init!(asset: VIANonSmokersDeclarationAssetType) {
#if os(iOS) || os(tvOS)
let bundle = Bundle(for: BundleToken.self)
self.init(named: asset.name, in: bundle, compatibleWith: nil)
#elseif os(OSX) || os(watchOS)
self.init(named: asset.name)
#endif
}
}

extension VIANonSmokersDeclarationColor {
#if swift(>=3.2)
@available(iOS 11.0, tvOS 11.0, watchOS 4.0, OSX 10.13, *)
convenience init!(asset: VIANonSmokersDeclarationColorAsset) {
let bundle = Bundle(for: BundleToken.self)
#if os(iOS) || os(tvOS)
self.init(named: asset.name, in: bundle, compatibleWith: nil)
#elseif os(OSX)
self.init(named: asset.name, bundle: bundle)
#elseif os(watchOS)
self.init(named: asset.name)
#endif
}
#endif
}

private final class BundleToken {}
