// Generated using SwiftGen, by O.Halligon — https://github.com/SwiftGen/SwiftGen

#if os(OSX)
import AppKit.NSImage
public typealias VitalityAdvanceColor = NSColor
public typealias VitalityAdvanceImage = NSImage
#elseif os(iOS) || os(tvOS) || os(watchOS)
import UIKit.UIImage
public typealias VitalityAdvanceColor = UIColor
public typealias VitalityAdvanceImage = UIImage
#endif

// swiftlint:disable file_length

public typealias VitalityAdvanceAssetType = VitalityAdvanceImageAsset

public struct VitalityAdvanceImageAsset: Equatable {
	fileprivate var name: String

	public var templateImage: VitalityAdvanceImage {
		let bundle = Bundle(for: BundleToken.self)
		#if os(iOS) || os(tvOS)
		let image = VitalityAdvanceImage(named: name, in: bundle, compatibleWith: nil)?.withRenderingMode(.alwaysTemplate)
		#elseif os(OSX)
		let image = bundle.image(forResource: name).withRenderingMode(.alwaysTemplate)
		#elseif os(watchOS)
		let image = VitalityAdvanceImage(named: name).withRenderingMode(.alwaysTemplate)
		#endif
		guard let result = image else { fatalError("Unable to load template image named \(name).") }
		return result
	}

	public var image: VitalityAdvanceImage {
		let bundle = Bundle(for: BundleToken.self)
		#if os(iOS) || os(tvOS)
		let image = VitalityAdvanceImage(named: name, in: bundle, compatibleWith: nil)
		#elseif os(OSX)
		let image = bundle.image(forResource: name)
		#elseif os(watchOS)
		let image = VitalityAdvanceImage(named: name)
		#endif
		guard let result = image else { fatalError("Unable to load image named \(name).") }
		return result
	}

	public static func ==(lhs: VitalityAdvanceImageAsset, rhs: VitalityAdvanceImageAsset) -> Bool {
        return lhs.image == rhs.image
    }
}

public struct VitalityAdvanceColorAsset {
fileprivate var name: String

#if swift(>=3.2)
@available(iOS 11.0, tvOS 11.0, watchOS 4.0, OSX 10.13, *)
var color: VitalityAdvanceColor {
return VitalityAdvanceColor(asset: self)
}
#endif
}

// swiftlint:disable identifier_name line_length nesting type_body_length type_name
public enum VitalityAdvanceAsset {
  public static let vitalityLogo = VitalityAdvanceImageAsset(name: "vitalityLogo")

  // swiftlint:disable trailing_comma
  public static let allColors: [VitalityAdvanceColorAsset] = [
  ]
  public static let allImages: [VitalityAdvanceImageAsset] = [
    vitalityLogo,
  ]
  // swiftlint:enable trailing_comma
  @available(*, deprecated, renamed: "allImages")
  public static let allValues: [VitalityAdvanceAssetType] = allImages
}
// swiftlint:enable identifier_name line_length nesting type_body_length type_name

extension VitalityAdvanceImage {
@available(iOS 1.0, tvOS 1.0, watchOS 1.0, *)
@available(OSX, deprecated,
message: "This initializer is unsafe on macOS, please use the VitalityAdvanceImageAsset.image property")
convenience init!(asset: VitalityAdvanceAssetType) {
#if os(iOS) || os(tvOS)
let bundle = Bundle(for: BundleToken.self)
self.init(named: asset.name, in: bundle, compatibleWith: nil)
#elseif os(OSX) || os(watchOS)
self.init(named: asset.name)
#endif
}
}

extension VitalityAdvanceColor {
#if swift(>=3.2)
@available(iOS 11.0, tvOS 11.0, watchOS 4.0, OSX 10.13, *)
convenience init!(asset: VitalityAdvanceColorAsset) {
let bundle = Bundle(for: BundleToken.self)
#if os(iOS) || os(tvOS)
self.init(named: asset.name, in: bundle, compatibleWith: nil)
#elseif os(OSX)
self.init(named: asset.name, bundle: bundle)
#elseif os(watchOS)
self.init(named: asset.name)
#endif
}
#endif
}

private final class BundleToken {}
