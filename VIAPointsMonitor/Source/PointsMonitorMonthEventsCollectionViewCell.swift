import UIKit

import VIAUIKit
import VitalityKit
import VIAUtilities

import RealmSwift
import SwiftDate

public protocol PointsMonitorMonthRefreshDelegate: class {
    func performRefresh(sender: PointsMonitorMonthTableViewController?)
    func showPreviousMembershipYearLabel(show: Bool)
    func performPush(sender: PointsMonitorMonthTableViewController?)
    func displayCategories(pointsEntries: [Category])
}

public class PointsMonitorMonthEventsCollectionViewCell: UICollectionViewCell {

    // MARK: Lifecycle

    public override func awakeFromNib() {
        super.awakeFromNib()
    }

    public override func prepareForReuse() {
        super.prepareForReuse()
        self.tableViewController.refreshControl?.endRefreshing()
    }

    // MARK: Properties

    public var didSelectItem: (Any?) -> Void = { item in
        debugPrint("PointsMonitorMonthEventsCollectionViewCell didSelectItem \(String(describing: item))")
    }

    func didSelectThing(thing: Any?) {
        self.didSelectItem(thing)
    }

    public var date = Date() {
        didSet {
            self.tableViewController.date = self.date
        }
    }

    public var selectedCategories = [PointsEntryCategoryRef]() {
        didSet {
            self.tableViewController.selectedCategories = self.selectedCategories
        }
    }

    public weak var refreshDelegate: PointsMonitorMonthRefreshDelegate? {
        didSet {
            self.tableViewController.refreshDelegate = self.refreshDelegate
        }
    }

    private lazy var tableViewController: PointsMonitorMonthTableViewController! = {
        let controller = PointsMonitorMonthTableViewController(style: .grouped)
        controller.didSelectItem = self.didSelectThing
        return controller
    }()

    // MARK: Init & view

    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override init(frame: CGRect) {
        super.init(frame: frame)

        self.contentView.backgroundColor = .white
    }

    public override func layoutSubviews() {
        super.layoutSubviews()

        setup()
    }

    func setup() {
        guard let tableViewController = self.tableViewController else {
            debugPrint("tableViewController doesn't exist yet")
            return
        }

        if !self.contentView.subviews.contains(tableViewController.view) {
            self.contentView.addSubview(tableViewController.view)
        }

        tableViewController.view.snp.removeConstraints()
        tableViewController.view.snp.makeConstraints({ (make) in
            make.width.height.equalToSuperview()
            make.left.top.right.bottom.equalToSuperview()
        })
    }

}

public class PointsMonitorMonthTableViewController: VIATableViewController {

    // MARK: Properties
    public weak var refreshDelegate: PointsMonitorMonthRefreshDelegate?
    private var lastUpdatedLabel = UILabel()

    lazy var formatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateStyle = .full
        formatter.timeStyle = .none
        return formatter
    }()

    public var date: Date? {
        didSet {
            guard let date = self.date else { return }
            createSectionsAndData(for: date, categories: selectedCategories)
            self.reloadData()
        }
    }

    public var selectedCategories = [PointsEntryCategoryRef]() {
        didSet {
            guard let date = self.date else { return }
            createSectionsAndData(for: date, categories: selectedCategories)
            self.reloadData()
        }
    }

    public var didSelectItem: (Any?) -> Void = { item in
        debugPrint("PointsMonitorMonthTableViewController didSelectItem \(String(describing: item))")
    }

    // MARK: Data & sections

    var realm = DataProvider.newRealm()

    var sections: [Date] = []

    var pointsEntriesBySection = [Results<PointsEntry>]()

    func createSectionsAndData(for date: Date, categories: [PointsEntryCategoryRef]) {
        // points entries for month of date
        let pointsEntries: Results<PointsEntry>
        if let category = categories.first,
            let pointsCategory = realm.pointsMonitorCategory(with: category),
            categories.count > 0 && pointsCategory.name != CommonStrings.Pm.CategoryFilterAllPointsTitle515 {
                    pointsEntries = realm.pointsEntries(for: categories, during: date)
        } else {
            pointsEntries = realm.pointsEntries(for: date)
            retrieveCategoriesFromList(pointsEntries: pointsEntries)
        }

        // create sections
        var unsortedDates: Set<Date> = []
        for entry in pointsEntries {
            let startOfDay = Calendar.current.startOfDay(for: entry.effectiveDate)
            unsortedDates.insert(startOfDay)
        }
        sections = unsortedDates.sorted(by: { $0 > $1 })

        // create sections for each object
        var objectsBySection = [Results<PointsEntry>]()
        for sectionDate in sections {
            let unsortedObjectsForSection = pointsEntries.filterForElementsBetweenStartAndEnd(day: sectionDate, fieldName: "effectiveDate")
            let sortedObjectsForSection = unsortedObjectsForSection.sorted(byKeyPath: "effectiveDate", ascending: true)
            objectsBySection.append(sortedObjectsForSection)
        }

        pointsEntriesBySection = objectsBySection
        
        checkMembershipYear(selectedDate: date)
    }
    
    func checkMembershipYear(selectedDate: Date){
        if let currentMembershipStartDate = DataProvider.currentMembershipPeriodStartDate(),
            selectedDate.isBefore(date: currentMembershipStartDate, granularity: Calendar.Component.month) ||
                selectedDate.isInSameMonth(date: currentMembershipStartDate){
//            print("currentMembershipStartDate: \(formatter.string(from: currentMembershipStartDate))")
//            print("selectedDate: \(formatter.string(from: selectedDate))")
            self.refreshDelegate?.showPreviousMembershipYearLabel(show: true)
        }else{
            self.refreshDelegate?.showPreviousMembershipYearLabel(show: false)
        }
    }
    
    func retrieveCategoriesFromList(pointsEntries: Results<PointsEntry>){
        var categories = [Category]()
        for entry in pointsEntries{
            switch entry.categoryKey {
            case .Adjustment, .Assessment, .BoosterPoints, .Fitness, .GetActive, .Nutrition, .Screening:
                let categoryImage = PointsEntryHelper.pointEventImageFor(category: entry.categoryKey)
                let category: Category = Category(name: entry.categoryName,
                                                  reference: entry.categoryKey,
                                                  image: categoryImage)
                categories.append(category)
                break
            default:
                let categoryImage = PointsEntryHelper.pointEventImageFor(category: PointsEntryCategoryRef.Other)
                let category: Category = Category(name: CommonStrings.Pm.CategoryFilterPotherTitle521,
                                                  reference: PointsEntryCategoryRef.Other,
                                                  image: categoryImage)
                categories.append(category)
                break
            }
        }
        
        let categoryImage = PointsEntryHelper.pointEventImageFor(category: PointsEntryCategoryRef.Unknown)
        let category: Category = Category(name: CommonStrings.Pm.CategoryFilterAllPointsTitle515,
                                          reference: PointsEntryCategoryRef.Unknown,
                                          image: categoryImage)
        categories.append(category)
        
        let sortedEntries = categories.filterDuplicates(includeElement: { (lhs, rhs) -> Bool in
            return lhs.reference == rhs.reference
        }).sorted { (lhs, rhs) -> Bool in
            return lhs.name < rhs.name
        }
        refreshDelegate?.displayCategories(pointsEntries: sortedEntries)
    }

    // MARK: View lifecycle

    deinit {
        debugPrint("PointsMonitorMonthTableViewController Deinit")
    }

    public override func viewDidLoad() {
        super.viewDidLoad()

        configureTableView()
        configureRefreshControl()
    }
    
    public override func viewWillAppear(_ animated: Bool) {
        self.reloadData()
    }

    // MARK: View configuration

    func configureEmptyStatusView() {
        removeEmptyStatusView()
        let view = VIAStatusView.viewFromNib(owner: self)!

        guard let pointsCategory = selectedCategories.first,
            let selectedCategory = realm.pointsMonitorCategory(with: pointsCategory),
            selectedCategories.count == 1,
            selectedCategory.name != CommonStrings.Pm.CategoryFilterAllPointsTitle515,
            selectedCategory.name != CommonStrings.Pm.CategoryFilterPotherTitle521  else {
                view.heading = CommonStrings.Pm.EmptyStateAllPointsTitle524
                view.message = CommonStrings.Pm.EmptyStateAllPointsMessage525
                view.buttonTitle = CommonStrings.HelpButtonTitle141
                view.hideButtonBorder = false
                view.actionBlock = {
                    self.performPush()
                }
                self.view.addSubview(view)
                
                view.snp.makeConstraints { (make) in
                    make.height.equalToSuperview()
                    make.width.equalToSuperview()
                    make.left.right.top.bottom.equalToSuperview()
                }
                
                return
        }

        switch selectedCategory.categoryId {
        case .Assessment:
            view.heading = CommonStrings.Pm.EmptyStateAssessmentTitle528
            view.message = CommonStrings.Pm.EmptyStateAssessmentMessage529
            break
        case .Screening:
            view.heading = CommonStrings.Pm.EmptyStateScreeningTitle532
            view.message = CommonStrings.Pm.EmptyStateScreeningMessage533
            break
        case .Fitness:
            view.heading = CommonStrings.Pm.EmptyStateFitnessTitle534
            view.message = CommonStrings.Pm.EmptyStateFitnessMessage535
            break
        case .BoosterPoints:
            view.heading = CommonStrings.Pm.EmptyStateBoosterPointsTitle536
            view.message = CommonStrings.Pm.EmptyStateBoosterPointsMessage537
            break
        case .Adjustment:
            view.heading = CommonStrings.Pm.EmptyStatePointsAdjustmentTitle538
            view.message = CommonStrings.Pm.EmptyStatePointsAdjustmentMessage539
            break
        case .Nutrition:
            view.heading = CommonStrings.Pm.EmptyStateNutritionTitle530
            view.message = CommonStrings.Pm.EmptyStateNutritionMessage531
            break
        case .Unknown, .VHC, .VHR, .Other, .HealthyFood, .GetActive, .Vaccinations:
            break
        }

        self.view.addSubview(view)

        view.snp.makeConstraints { (make) in
            make.width.equalToSuperview()
            make.left.right.top.equalToSuperview()
            make.height.equalToSuperview().offset(-62)
            make.bottom.equalToSuperview().offset(-62)
        }
    }

    func removeEmptyStatusView() {
        self.view.subviews.forEach { (view) in
            if view is VIAStatusView {
                view.removeFromSuperview()
            }
        }
    }

    func configureTableView() {
        self.tableView.estimatedRowHeight = 58
        self.tableView.estimatedSectionFooterHeight = 40
        self.tableView.sectionFooterHeight = UITableViewAutomaticDimension
        self.tableView.register(PMPointsEventCell.nib(), forCellReuseIdentifier: PMPointsEventCell.defaultReuseIdentifier)
        self.tableView.register(VIATableViewSectionHeaderFooterView.nib(), forHeaderFooterViewReuseIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier)
        
        lastUpdatedLabel = UILabel(frame: CGRect(x: 0, y:0, width: Int(self.tableView.bounds.width), height: 44))
        lastUpdatedLabel.text = CommonStrings.Pm.RefreshUpdateMessage1552("")
        lastUpdatedLabel.font = lastUpdatedLabel.font.withSize(14)
        lastUpdatedLabel.textAlignment = .center
        lastUpdatedLabel.textColor = UIColor.lightGray
        self.tableView.tableHeaderView = lastUpdatedLabel
    }

    func configureRefreshControl() {
        tableView.refreshControl = UIRefreshControl()
        tableView.refreshControl?.tintColor = UIColor.cellSeperator()
        tableView.refreshControl?.addTarget(self, action: #selector(performRefresh), for: .valueChanged)
    }

    // MARK: Data source

    public override func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }

    public override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pointsEntriesBySection[section].count
    }

    public override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: PMPointsEventCell.defaultReuseIdentifier, for: indexPath) as! PMPointsEventCell

        let pointsEntry = pointsEntriesBySection[indexPath.section][indexPath.row]
        let contributions = generateContributionsFromPointsEvents(results: pointsEntry.activeRewardsProgressTrackerEvents)
        //TODO: - get correct category image from assets based on category string

        var categoryName = ""
        if let category = pointsEntry.category {
            categoryName = category.name
        }
        
        var deviceName: String? = nil
        for metadata in pointsEntry.metadatas {
            if metadata.typeKey == EventMetaDataTypeRef.Manufacturer {
                deviceName = metadata.value
            }
        }
        
        let image = PointsEntryHelper.pointEventImageFor(category: pointsEntry.categoryKey)
        let pointsText = Localization.integerFormatter.string(from: pointsEntry.earnedValue as NSNumber)
        cell.configurePointsMonitorMainCell(image: image,
                                            category: categoryName,
                                            pointsValue: pointsEntry.earnedValue,
                                            pointsText: pointsText,
                                            message: pointsEntry.reasons.first?.name,
                                            contributions: contributions,
                                            description: pointsEntry.typeName,
                                            device: deviceName,
                                            isDisclosureIndicatorHidden: false)
        return cell
    }

    func generateContributionsFromPointsEvents(results: List<ActiveRewardsProgressTrackerEvent>) -> [Contribution] {
        var contributions = [Contribution]()

        for result in results {
            let image: UIImage
            let formattedString = Localization.integerFormatter.string(from: result.points as NSNumber) ?? CommonStrings.GenericNotAvailable270
            let description: String
            if result.goalTypeCode == "AppleWatch" {
                image = UIImage(asset: VIAPointsMonitorAsset.watchContributionSmall)
                description =  CommonStrings.Pm.PointsEarningAppleWatch568(formattedString)
            } else {
                image = UIImage(asset: VIAPointsMonitorAsset.trophyContributionSmall)
                description = CommonStrings.Pm.PointsEarningActiveRewards567(formattedString)
            }
            let contribution = Contribution(image: image, description: description)
            contributions.append(contribution)
        }

        return contributions
    }

    // MARK: Delegate

    public override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier) as! VIATableViewSectionHeaderFooterView
        view.labelText = formatter.string(from: sections[section])
        view.aLabel.textAlignment = .left
        return view
    }

    public override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if (section == self.numberOfSections(in: tableView) - 1) {
            let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier) as! VIATableViewSectionHeaderFooterView
            if let date = self.date {
                let dateString = Localization.shortMonthAndYearFormatter.string(from: date)
                view.labelText = CommonStrings.Pm.FootnoteEmptyStateOtherMessage551(dateString)
            }
            view.set(alignment: .center)
            return view
        }
        return nil
    }

    public override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let pointsEntry = pointsEntriesBySection[indexPath.section][indexPath.row]
        didSelectItem(pointsEntry)
        tableView.deselectRow(at: indexPath, animated: true)
    }

    // MARK: Reload

    func reloadData() {
        guard self.date != nil else {
            removeEmptyStatusView()
            return
        }

        self.tableView.reloadData()
        configureTableView()
        if pointsEntriesBySection.count == 0 {
            configureEmptyStatusView()
        } else {
            removeEmptyStatusView()
        }
        let lastUpdate = Localization.shortTimeFormatter.string(from: Date())
        lastUpdatedLabel.text = CommonStrings.Pm.RefreshUpdateMessage1552(lastUpdate)
    }

    // MARK: Refresh

    func performRefresh() {
        self.refreshDelegate?.performRefresh(sender: self)
    }
    
    // MARL: Push segue.
    
    func performPush() {
        self.refreshDelegate?.performPush(sender: self)
    }
}
