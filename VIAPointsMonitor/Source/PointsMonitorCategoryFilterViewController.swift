import Foundation

import VIAUIKit
import VitalityKit
import VIAUtilities

import RealmSwift
import SnapKit

protocol PointsMonitorCategoryFilterDelegate: class {
    func didSelectCategories(categories: [PointsEntryCategoryRef])
}

public struct Category {
    let name: String
    var reference: PointsEntryCategoryRef
    var image: UIImage
}

public class PointsMonitorCategoryFilterViewController: VIATableViewController {
    
    public override var preferredContentSize: CGSize {
        get {
            let height: CGFloat = tableView.contentSize.height + ((self.navigationController?.navigationBar.frame.size.height) ?? 0)
            return CGSize(width: super.preferredContentSize.width, height: height)
        }
        set { super.preferredContentSize = newValue }
    }
    
    // MARK: Properties
    public var allPointsCategoryReference = PointsEntryCategoryRef.Unknown
    public var selectedCategories = Set<PointsEntryCategoryRef>()
    public var realm = DataProvider.newRealm()
    public var pointsCategories = [Category]()
    
    weak var delegate: PointsMonitorCategoryFilterDelegate?
    
    public override func viewDidLoad() {
        super.viewDidLoad()

        setDefaultSelectedCategory()
        configureTitle()
        configureTableView()
        
        addCategoriesToRealm()
        getCategoriesFromRealm()
        tableView.reloadData()
    }
    
    public override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        delegate?.didSelectCategories(categories: Array(selectedCategories))
    }
    
    public func setDefaultSelectedCategory() {
        if selectedCategories.count == 0 {
            selectedCategories.insert(allPointsCategoryReference)
        }
    }
    
    public func sortCategoriesByIndex(this: Category, that: Category) -> Bool {
        return this.reference.rawValue < that.reference.rawValue
    }
    
    public func configureTitle() {
        let tlabel = UILabel(frame: CGRect(x: 0, y: 0, width: 300, height: 37))
        tlabel.text = CommonStrings.Pm.CategoryFilterHeader514
        tlabel.textColor = UIColor.darkGrey()
        tlabel.font = UIFont.footnoteFont()
        self.navigationController?.navigationBar.barTintColor = .white
        self.navigationItem.titleView = tlabel
    }
    
    public func configureTableView() {
        self.tableView.register(VIATableViewCell.nib(), forCellReuseIdentifier: VIATableViewCell.defaultReuseIdentifier)
        self.tableView.allowsMultipleSelection = true
        self.tableView.alwaysBounceVertical = false
        self.tableView.isScrollEnabled = false
        self.tableView.backgroundColor = .white
        self.tableView.estimatedRowHeight = 50
        self.tableView.estimatedSectionHeaderHeight = 0
        self.tableView.estimatedSectionFooterHeight = 0
        self.tableView.sectionHeaderHeight = 0
        self.tableView.sectionFooterHeight = 0
    }
    
    public override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    public override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pointsCategories.count
    }
    
    public override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: VIATableViewCell.defaultReuseIdentifier, for: indexPath) as! VIATableViewCell
        cell.selectionStyle = .none
        
        let category = pointsCategories[indexPath.row]
        cell.labelText = category.name
        cell.cellImage = category.image
        cell.accessoryType = selectedCategories.contains(category.reference) ? .checkmark : .none
        
        return cell
    }
    
    public override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let newSelectedCategoryReference = pointsCategories[indexPath.row].reference
        selectedCategories = Set([newSelectedCategoryReference])
        self.tableView.reloadData()
        self.dismiss(animated: true, completion: nil)
        return
    }
}


/*
 * Deprecated
 */
extension PointsMonitorCategoryFilterViewController {
    //TODO: remove once categories are available in service
    
    public func getCategoriesFromRealm() {
        let eventCategories = realm.allPointsMonitorCategories()
        
        pointsCategories = [Category]()
        
        for eventCategory in eventCategories {
            let categoryImage = PointsEntryHelper.pointEventImageFor(category: eventCategory.categoryId)
            let category: Category = Category(name: eventCategory.name,
                                              reference: PointsEntryCategoryRef(rawValue: eventCategory.reference) ?? .Unknown,
                                              image: categoryImage)
            let categoryExist = pointsCategories.contains(where: { (value) -> Bool in
                return ((value.name) == category.name)
            })
            if categoryExist != true{
                pointsCategories.append(category)
            }
        }
        pointsCategories.sort(by: sortCategoriesByIndex)
    }
    
    public func addCategoriesToRealm() {
        try! realm.write {
            let objects = realm.objects(PointsMonitorCategory.self)
            realm.delete(objects)
        }
        let productFeatureLinkPointsCategories = realm.ProductFeatureLink(for: .PointsCategory)
        
        PointsMonitorCategory.setupPointsCategory(name:  CommonStrings.Pm.CategoryFilterAllPointsTitle515, categoryId: .Unknown)
        
        PointsMonitorCategory.setupPointsCategory(name: CommonStrings.Pm.CategoryFilterPotherTitle521, categoryId: .Other)
        
        for pointsCategory in productFeatureLinkPointsCategories {
            let pointsCategoryRef: PointsEntryCategoryRef = PointsEntryCategoryRef(rawValue:pointsCategory.linkedKey.value ?? PointsEntryCategoryRef.Unknown.rawValue) ?? PointsEntryCategoryRef.Unknown
            
            switch pointsCategoryRef {
            case .Adjustment:
                PointsMonitorCategory.setupPointsCategory(name: CommonStrings.Pm.CategoryFilterPointsAdjustmentsTitle521, categoryId: pointsCategoryRef)
                break
            case .Assessment:
                PointsMonitorCategory.setupPointsCategory(name: CommonStrings.Pm.CategoryFilterAssessmentTitle516, categoryId: pointsCategoryRef)
                break
            case .BoosterPoints:
                PointsMonitorCategory.setupPointsCategory(name: CommonStrings.Pm.CategoryFilterBoosterPointsTitle520, categoryId: pointsCategoryRef)
                break
            case .Fitness:
                PointsMonitorCategory.setupPointsCategory(name: CommonStrings.Pm.CategoryFilterFitnessTitle519, categoryId: pointsCategoryRef)
                break
            case .GetActive:
                PointsMonitorCategory.setupPointsCategory(name: CommonStrings.Pm.CategoryFilterFitnessTitle519, categoryId: pointsCategoryRef)
                break
            case .Nutrition:
                PointsMonitorCategory.setupPointsCategory(name: CommonStrings.Pm.CategoryFilterNutritionTitle517, categoryId: pointsCategoryRef)
                break
            case .Screening:
                PointsMonitorCategory.setupPointsCategory(name: CommonStrings.Pm.CategoryFilterScreeningTitle518, categoryId: pointsCategoryRef)
                break
                
            case .Unknown, .VHC, .VHR, .HealthyFood, .GetActive, .Vaccinations:
                break
                
            default:
                break
            }
        }
    }
}
