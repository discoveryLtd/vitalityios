import UIKit

import VIAUIKit
import VitalityKit

public class PointsMonitorCalendarViewController: VIAViewController {

    public override func viewDidLoad() {
        super.viewDidLoad()
        configureBarButtons()

    }

    public func configureBarButtons() {
        let rightItem = UIBarButtonItem(title: CommonStrings.GenericCloseButtonTitle10, style: .plain, target: self, action: #selector(closeButtonTapped(_:)))
        navigationItem.rightBarButtonItem = rightItem
    }

    public func closeButtonTapped (_ sender: UIBarButtonItem) {
        self.performSegue(withIdentifier: "unwindToPointsMonitorFromCalendar", sender: self)
    }

    public override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
