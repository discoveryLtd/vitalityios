import Foundation
import VIAUIKit

import VitalityKit
import VIAUtilities

public class PointsEntryHelper {
    public class func pointEventImageFor(category: PointsEntryCategoryRef) -> UIImage {
        switch (category) {
        // not sure how to deal with this yet, since we're importing
        // reference data now. AllPoints is an app only case
        case .Assessment :
            return VIAPointsMonitorAsset.assessmentsCategoryLogo.image
        case .HealthyFood :
            return VIAPointsMonitorAsset.nutritionCategoryLogo.image
        case .Fitness, .GetActive :
            return VIAPointsMonitorAsset.getActiveCategoryLogo.image
        case .Screening :
            return VIAPointsMonitorAsset.screeningCategoryLogo.image
        case .Adjustment :
            return VIAPointsMonitorAsset.pointsAdjustment.image
        case .BoosterPoints :
            return VIAPointsMonitorAsset.boosterPoints.image
        case .Other :
            return VIAPointsMonitorAsset.otherCategoryLogo.image
        case .Vaccinations :
            return VIAPointsMonitorAsset.vaccinationCategoryLogo.image.maskWithColor(UIColor.jungleGreen())!
        default:
            return VIAPointsMonitorAsset.allCategoryLogo.image
        }
    }
}
 
