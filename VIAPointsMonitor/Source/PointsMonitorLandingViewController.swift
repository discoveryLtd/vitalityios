import UIKit

import VIAUIKit
import VitalityKit
import VIAUtilities
import VIACommon

import SnapKit
import RealmSwift

public struct PointsEvent {
    let pointsIcon: UIImage?
    let points: String?
    let date: String?
    let pointsContributions: [Contribution]?
    let description: String?
    let message: String?
    let device: String?
    let arrowIcon: UIImage?
}

public class PointsMonitorLandingViewController: VIAViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UIPopoverPresentationControllerDelegate, PointsMonitorMonthRefreshDelegate, PointsMonitorCategoryFilterDelegate {
    
    let superwide = 10_000
    
    // MARK: Properties
    
    let filterButton = UIButton()
    
    var selectedPointsEntry: PointsEntry?
    
    let monthPickerView = PointsMonitorMonthPickerView.viewFromNib(owner: self)!
    
    let collectionView = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
    
    var didScrollToStartingPosition = false
    
    var activeIndexPath = IndexPath(item: 0, section: 0)
    
    var selectedCategories = [PointsEntryCategoryRef]()
    
    var pointsEntries: [Category] = []
    
    var realm = DataProvider.newRealm()
    
    public var shouldPerformDataRequest: Bool = true
    
    var previousMembershipLabel = UILabel()
    
    // MARK: View lifecycle
    
    
    override public func awakeFromNib() {
        super.awakeFromNib()
        self.setupTabBarTitle()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setupTabBarTitle()
    }
    
    public override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        self.setupTabBarTitle()
    }
    
    public func setupTabBarTitle() {
        self.navigationController?.tabBarItem.title = CommonStrings.MenuPointsButton6
    }
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        
        title = CommonStrings.MenuPointsButton6
        configureTitleView()
        configureSubviews()
        addBarButtonItems()
    }
    
    override public func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.makeNavigationBarTransparent()
        
        if shouldPerformDataRequest {
            shouldPerformDataRequest = false
            performInitialRequest()
        }
    }
    
    override public func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.hideBackButtonTitle()
    }
    
    override public func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        scrollCollectionViewToStartingPositionIfNeeded()
    }
    
    public func addBarButtonItems() {
        //TODO: descoped for now
        
        //        let calendar = UIBarButtonItem(image: UIImage(asset: .calendarFilterIcon), style: .plain, target: self, action: #selector(showCalendarFilter(_:)))
        //        self.navigationItem.rightBarButtonItem = calendar
    }
    
    public func configureTitleView() {
        let containerView = UIView(frame: CGRect(x: 0, y: 0, width: 200, height: 40))
        self.navigationItem.titleView = containerView
        
        let stackView = UIStackView()
        containerView.addSubview(stackView)
        stackView.axis = .horizontal
        stackView.distribution = .fill
        stackView.contentMode = .scaleToFill
        stackView.snp.makeConstraints { (make) in
            make.width.height.equalToSuperview()
            make.left.top.right.bottom.equalToSuperview()
        }
        filterButton.setTitle(CommonStrings.Pm.CategoryFilterAllPointsTitle515, for: .normal)
        filterButton.titleLabel?.textAlignment = .center
        filterButton.setTitleColor(UIColor.primaryColor(), for: .normal)

        let image = VIAPointsMonitorAsset.arrowDownIcon.image.withRenderingMode(.alwaysTemplate)
        filterButton.setImage(image, for: .normal)
        filterButton.tintColor = UIColor.primaryColor()
        
        filterButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 0)
        filterButton.adjustsImageWhenHighlighted = false
        filterButton.semanticContentAttribute = .forceRightToLeft
        filterButton.addTarget(self, action: #selector(showCategoryFilter(_:)), for: .touchUpInside)
        stackView.addArrangedSubview(filterButton)
    }
    
    public func configureSubviews() {
        self.automaticallyAdjustsScrollViewInsets = false
        
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.distribution = .fill
        stackView.alignment = .fill
        self.view.addSubview(stackView)
        
        stackView.snp.makeConstraints { (make) in
            make.leading.trailing.equalToSuperview()
            make.top.equalTo(topLayoutGuide.snp.bottom)
            make.bottom.equalTo(bottomLayoutGuide.snp.top)
        }
        
        stackView.addArrangedSubview(monthPickerView)
        configureMonthPickerView()
        monthPickerView.snp.makeConstraints { (make) in
            make.height.equalTo(58)
            make.width.equalToSuperview()
        }
        
        if let showPrevMembershipTitle = VIAApplicableFeatures.default.showPreviousMembershipYearTitle, showPrevMembershipTitle  {
            let startMonth = DataProvider.currentMembershipPeriodStartDate()?.month
            let startYear = DataProvider.currentMembershipPeriodStartDate()?.year
            let month = monthPickerView.selectedMonth.month
            let year = monthPickerView.selectedMonth.year
            
            if startYear! >= year  {
                if startMonth! > month || startYear! > year {
                    previousMembershipLabel = UILabel()
                    previousMembershipLabel.text = CommonStrings.Pm.PreviousMembershipYearTitle612
                    previousMembershipLabel.textAlignment = .center
                    previousMembershipLabel.textColor = UIColor.lightGray
                    stackView.addArrangedSubview(previousMembershipLabel)
                    previousMembershipLabel.snp.makeConstraints { (make) in
                        make.height.equalTo(55)
                        make.width.equalToSuperview()
                    }
                }
            }
        }
        
        stackView.addArrangedSubview(collectionView)
        configureCollectionView()
    }
    
    public func configureCollectionView() {
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.backgroundColor = UIColor.tableViewBackground()
        collectionView.bounces = false
        collectionView.isScrollEnabled = false
        collectionView.showsHorizontalScrollIndicator = false
        
        self.collectionView.register(PointsMonitorMonthEventsCollectionViewCell.self, forCellWithReuseIdentifier: PointsMonitorMonthEventsCollectionViewCell.defaultReuseIdentifier)
        self.collectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: UICollectionViewCell.defaultReuseIdentifier)
        if let layout = self.collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.minimumLineSpacing = 0
            layout.minimumInteritemSpacing = 0
            layout.scrollDirection = .horizontal
            collectionView.isPagingEnabled = true
            collectionView.collectionViewLayout = layout
        }
    }
    
    public func scrollCollectionViewToStartingPositionIfNeeded() {
        if didScrollToStartingPosition {
            return
        }
        
        // scroll halfway into the collectionview's number of items
        // to allow for enough padding for the user to navigate horizontally
        // in conjunction with the month picker
        self.view.layoutIfNeeded()
        let startingIndexPath = IndexPath(item: superwide / 2, section: 0)
        activeIndexPath = startingIndexPath
        collectionView.scrollToItem(at: startingIndexPath, at: .centeredHorizontally, animated: false)
        didScrollToStartingPosition = true
    }
    
    public func configureMonthPickerView() {
        // we create a superwide set of collectionview cells (see numberOfItemsInSection:)
        // which will serve as placeholders for when a user navigates via the date month picker.
        // the date month picker behaviour is set to go only to the next or previous collectionview cell
        // regardless of how big the actual jump in date range is. this behaviour is similar to the Health app on iOS
        self.monthPickerView.didSelectDate = monthPickerViewDidSelectDate
    }
    
    // MARK: UICollectionViewDataSource
    
    public func numberOfSections(in collectionView: UICollectionView) -> Int {
        configureSubviews()
        return 1
    }
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // we create a superwide set of collectionview cells (all will be empty luckily)
        // which will serve as placeholders for when a user navigates via the date month picker.
        // the date month picker behaviour is set to go only to the next or previous collectionview cell
        // regardless of how big the actual jump in date range is. this behaviour is similar to the Health app on iOS
        return superwide
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: PointsMonitorMonthEventsCollectionViewCell.defaultReuseIdentifier, for: indexPath) as! PointsMonitorMonthEventsCollectionViewCell
        cell.refreshDelegate = self
        
        if indexPath == activeIndexPath {
            cell.date = monthPickerView.selectedMonth
            cell.didSelectItem = self.showDetailView
            cell.selectedCategories = self.selectedCategories
        }
        
        return cell
    }
    
    // MARK: UICollectionViewDelegateFlowLayout
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.collectionView.frame.size.width, height: self.collectionView.frame.size.height)
    }
    
    // MARK: UIScrollView
    
    public func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        debugPrint(collectionView.indexPathsForVisibleItems)
    }
    
    // MARK: Navigation
    
    @IBAction public func unwindToPointsMonitorFromCalendar(segue: UIStoryboardSegue) {
        
    }
    
    public func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
    
    override public func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetailSegue" {
            let nextScene = segue.destination as? PointsMonitorActivityDetailViewController
            nextScene?.viewModel.pointsEntry = selectedPointsEntry
        }
    }
    
    public func showCategoryFilter(_ sender: Any) {
        if let navController = self.storyboard?.instantiateViewController(withIdentifier: "PointsMonitorCategoryFilterNavigationViewController") as? UINavigationController {
            navController.modalPresentationStyle = .popover
            let filterViewController = navController.topViewController as? PointsMonitorCategoryFilterViewController
            filterViewController?.delegate = self
            filterViewController?.selectedCategories = Set(selectedCategories)
            filterViewController?.pointsCategories = self.pointsEntries
            let popoverController = navController.popoverPresentationController
            popoverController?.backgroundColor = .white
            popoverController?.permittedArrowDirections = [.up]
            popoverController?.sourceView = self.navigationItem.titleView
            let centerX = ((self.navigationItem.titleView?.frame.width) ?? 0) / 2
            let height = ((self.navigationItem.titleView?.frame.height) ?? 0)
            popoverController?.sourceRect = CGRect(x: centerX, y: height, width: 1, height: 1)
            popoverController?.delegate = self
            self.present(navController, animated: true, completion: nil)
        }
    }
    
    // MARK: - Category Filter Delegate
    
    public func didSelectCategories(categories: [PointsEntryCategoryRef]) {
        selectedCategories = categories
        updateCategoryFilterText()
        self.reloadData()
    }
    
    
    public func updateCategoryFilterText() {
        if selectedCategories.count == 1 {
            if let selectedCategory = selectedCategories.first {
                if let pointCategory = realm.pointsMonitorCategory(with: selectedCategory) {
                    self.filterButton.setTitle(pointCategory.name, for: .normal)
                }
            }
        } else {
            self.filterButton.setTitle(CommonStrings.Pm.CategoryFilterAllPointsTitle515, for: .normal)
        }
    }
    
    public func showCalendarFilter(_ sender: Any) {
        self.performSegue(withIdentifier: "showCalendarSegue", sender: self)
    }
    
    // MARK: Refresh delegate
    
    public func performRefresh(sender: PointsMonitorMonthTableViewController?) {
        self.refresh()
    }
    
    public func showPreviousMembershipYearLabel(show: Bool){
        self.previousMembershipLabel.isHidden = !show
    }
    
    public func refresh() {
        collectionView.isScrollEnabled = false
        startPointsHistoryRequest(completion: { [unowned self] in
            self.collectionView.isScrollEnabled = true
            self.reloadData()
        })
    }
    
    public func displayCategories(pointsEntries: [Category]) {
        self.pointsEntries = pointsEntries
    }
    
    // MARK: Push delegate
    
    public func performPush(sender: PointsMonitorMonthTableViewController?) {
        self.performSegue(withIdentifier: "showHelpSegue", sender: self)
    }
    
    // MARK: Actions
    
    public func reloadData() {
        didScrollToStartingPosition = false
        scrollCollectionViewToStartingPositionIfNeeded()
        collectionView.reloadItems(at: [activeIndexPath])
    }
    
    public func showDetailView(for item: Any?) {
        selectedPointsEntry = item as? PointsEntry
        self.performSegue(withIdentifier: "showDetailSegue", sender: self)
    }
    
    public func monthPickerViewDidSelectDate(date: Date, direction: Direction) {
        if let currentVisibleIndexPath = collectionView.indexPathsForVisibleItems.first {
            let newVisibleIndexPathRow = max(currentVisibleIndexPath.row + direction.rawValue, 0)
            let newVisibleIndexPath = IndexPath(item: newVisibleIndexPathRow, section: currentVisibleIndexPath.section)
            
            self.activeIndexPath = newVisibleIndexPath
            self.collectionView.reloadItems(at: [newVisibleIndexPath])
            self.collectionView.scrollToItem(at: newVisibleIndexPath, at: .centeredHorizontally, animated: true)
        }
    }
    
    // MARK: Networking + data
    
    public func performInitialRequest() {
        self.showHUDOnView(view: self.view)
        startPointsHistoryRequest { [unowned self] in
            self.hideHUDFromView(view: self.view)
        }
    }
    
    public func startPointsHistoryRequest(completion: (() -> Void)?) {
        let membershipId = DataProvider.newRealm().getMembershipId()
        let tenantId = DataProvider.newRealm().getTenantId()
        
        let pointPeriod = VIAApplicableFeatures.default.getPointPeriod!
        
        Wire.Member.getAllPointsHistory(tenantId: tenantId, vitalityMembershipId: membershipId, pointsPeriod: pointPeriod) { (result, error, useCutomError) in
            if error != nil {
                if useCutomError! {
                    self.showUnableToUpdateError()
                } else {
                    self.handleErrorOccurred(error!)
                }
                debugPrint("Points history parsing error encountered: \(String(describing: error))")
            } else {
                debugPrint("Points history parsing done")
            }
            
            self.realm.refresh()
            self.reloadData()
            if let completionToExecute = completion {
                completionToExecute()
            }
        }
    }
    
    // MARK: Error alerts.
    
    public func handleErrorOccurred(_ error: Error) {
        let backendError = error as? BackendError ?? BackendError.other
        self.handleBackendErrorWithAlert(backendError, tryAgainAction: { [weak self] in
            self?.refresh()
        })
    }
    
    fileprivate func showUnableToUpdateError(){
        let title = CommonStrings.Pm.AlertUnableToUpdateTitle557
        let message = CommonStrings.Pm.AlertUnableToUpdateMessage558
        var actions:[UIAlertAction] = []
        actions.append(UIAlertAction(title: CommonStrings.CancelButtonTitle24, style: .cancel, handler: nil))
        actions.append(UIAlertAction(title: CommonStrings.TryAgainButtonTitle43, style: .default) { (action) in
            self.refresh()
        })
        
        showAlertDialog(title: title, message: message, alertActions: actions)
    }
    
    fileprivate func showAlertDialog(title:String? = nil, message:String? = nil,
                                     alertActions:[UIAlertAction]){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        
        for alertAction in alertActions{
            alert.addAction(alertAction)
        }
        alert.view.tintColor = UIButton().tintColor
        self.present(alert, animated: true, completion: nil)
    }
}
