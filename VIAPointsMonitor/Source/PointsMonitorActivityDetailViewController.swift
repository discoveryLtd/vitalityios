import UIKit

import VIAUIKit
import VitalityKit
import VIAUtilities
import VIACommon
import RealmSwift

public class PointsMonitorActivityDetailViewController: VIATableViewController {

    var reasonText: String?
    var pointsEntry: PointsEntry!
    var device: String?

    var details: [Detail] = []

    var sections: [Sections] = [Sections.Points, Sections.Device, Sections.Details, Sections.Help]

    var viewModel = VIAApplicableFeatures.default.getPointsMonitorActivityDetailViewModel() as? PointsMonitorActivityDetailViewModel ?? PointsMonitorActivityDetailViewModel()

    public override func viewDidLoad() {
        super.viewDidLoad()

        self.title = CommonStrings.Pm.ActivityDetailSectionTitle559

        device = nil
        
        viewModel.buildUpDetailsFromPointsEntry()
        pointsEntry = viewModel.pointsEntry
        details = viewModel.details
        
        configureTableView()
        configureSections()
    }

    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        configureAppearance()
    }

    public func configureAppearance() {
        navigationController?.makeNavigationBarTransparent()
        self.hideBackButtonTitle()
    }

    public override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: - TableView Configuration

    public func configureTableView() {

        self.tableView.register(PMPointsEventCell.nib(), forCellReuseIdentifier: PMPointsEventCell.defaultReuseIdentifier)
        self.tableView.register(ActivityDetailSubtitleCell.nib(), forCellReuseIdentifier: ActivityDetailSubtitleCell.defaultReuseIdentifier)
        self.tableView.register(VIATableViewCell.nib(), forCellReuseIdentifier: VIATableViewCell.defaultReuseIdentifier)
        self.tableView.register(VIATableViewSectionHeaderFooterView.nib(), forHeaderFooterViewReuseIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier)
        self.tableView.estimatedRowHeight = 56

        // add extra inset for the first cell's header
        self.tableView.contentInset = UIEdgeInsets(top: 25, left: 0, bottom: 0, right: 0)
    }

    public func configureSections() {
        guard device != nil else {
            sections.remove(at: sections.index(of: .Device)!)
            return
        }
    }

    // MARK: - PointsEntry data processing

    public func generateContributionsFromPointsEvents(results: List<ActiveRewardsProgressTrackerEvent>) -> [Contribution] {
        var contributions = [Contribution]()

        for result in results {
            let image: UIImage
            let formattedString = Localization.integerFormatter.string(from: result.points as NSNumber) ?? CommonStrings.GenericNotAvailable270
            let description: String
            if result.goalTypeCode == "AppleWatch" {
                image = VIAPointsMonitorAsset.watchContributionSmall.templateImage
                description =  CommonStrings.Pm.PointsEarningAppleWatch568(formattedString)
            } else {
                image = VIAPointsMonitorAsset.trophyContributionSmall.templateImage
                description = CommonStrings.Pm.PointsEarningActiveRewards567(formattedString)
            }
            let contribution = Contribution(image: image, description: description)
            contributions.append(contribution)
        }

        return contributions
    }

    // MARK: UITableView datasource

    public enum Sections: Int, EnumCollection {
        case Points = 0
        case Device = 1
        case Details = 2
        case Help = 3

        func sectionHeaderTitle() -> String? {
            switch self {
            case .Points:
                return CommonStrings.Pm.ActivityDetailSectionHeading1560
            case .Device:
                return CommonStrings.Pm.ActivityDetailSectionHeading2561
            case .Details:
                return CommonStrings.Pm.ActivityDetailSectionHeading3526
            case .Help:
                return nil
            }
        }
    }

    public override func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }

    public override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch sections[section] {
        case .Points:
            return 1
        case .Device:
            return 1
        case .Details:
            return details.count
        case .Help:
            if let hideHelpTab = VIAApplicableFeatures.default.hideHelpTab,
                hideHelpTab{
                return 0
            } else{
                return 1
            }
        }
    }

    // MARK: UITableView delegate

    public override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let theSection = sections[indexPath.section]
        if theSection == .Points {
            return self.configurePointsCell(indexPath)
        } else if theSection == .Device {
            return self.configureDeviceCell(indexPath)
        } else if theSection == .Details {
            return self.configureDetailsCell(indexPath)
        } else if theSection == .Help {
            return self.configureHelpCell(indexPath)
        }

        return UITableViewCell(style: .default, reuseIdentifier: "cell")
    }

    public func configurePointsCell(_ indexPath: IndexPath) -> UITableViewCell {
        let contributions = generateContributionsFromPointsEvents(results: pointsEntry.activeRewardsProgressTrackerEvents)
        guard let cell = tableView.dequeueReusableCell(withIdentifier: PMPointsEventCell.defaultReuseIdentifier, for: indexPath) as? PMPointsEventCell else {
            return tableView.defaultTableViewCell()
        }

        if let reason = pointsEntry.reasons.first?.name {
            reasonText = reason
        } else {
             reasonText = CommonStrings.Pm.PointsDetailNoInformation569
        }
        let pointsText = Localization.integerFormatter.string(from: pointsEntry.earnedValue as NSNumber)
        cell.configurePointsMonitorDetailCell(pointsValue: pointsEntry.earnedValue,
                                              pointsText: pointsText,
                                              message: reasonText,
                                              contributions: contributions)
        
        cell.selectionStyle = .none
        return cell
    }

    public func configureDeviceCell(_ indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: VIATableViewCell.defaultReuseIdentifier, for: indexPath) as? VIATableViewCell else {
            return tableView.defaultTableViewCell()
        }
        
        cell.selectionStyle = .none
        cell.labelText = device
        cell.applyDefaultStyling()
        return cell
    }

    public func configureDetailsCell(_ indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ActivityDetailSubtitleCell.defaultReuseIdentifier, for: indexPath) as? ActivityDetailSubtitleCell else {
            return tableView.defaultTableViewCell()
        }
        
        cell.selectionStyle = .none
        cell.headingText = details[indexPath.row].detailHeader
        cell.contentText = details[indexPath.row].detailDescription
        cell.configureWithSmallHeadingAndLargeContent()
        return cell
    }

    public func configureHelpCell(_ indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: VIATableViewCell.defaultReuseIdentifier, for: indexPath) as? VIATableViewCell else {
            return tableView.defaultTableViewCell()
        }
        
        cell.labelText = CommonStrings.HelpButtonTitle141
        cell.cellImageView.tintColor = UIColor.primaryColor()
        cell.cellImage = VIAPointsMonitorAsset.helpGeneric.templateImage

        cell.applyDefaultStyling()
        cell.accessoryType = .disclosureIndicator
        return cell
    }
    public override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier) as? VIATableViewSectionHeaderFooterView else {
            return nil
        }
        
        if let title = sections[section].sectionHeaderTitle() {
            view.labelText = title
        }
        return view
    }

    public override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let section = sections[indexPath.section]

        if section == .Help {
            self.performSegue(withIdentifier: "showHelpSegue", sender: self)
        }
    }
}
