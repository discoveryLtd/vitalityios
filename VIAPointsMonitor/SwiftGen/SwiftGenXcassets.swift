// Generated using SwiftGen, by O.Halligon — https://github.com/SwiftGen/SwiftGen

#if os(OSX)
import AppKit.NSImage
public typealias VIAPointsMonitorColor = NSColor
public typealias VIAPointsMonitorImage = NSImage
#elseif os(iOS) || os(tvOS) || os(watchOS)
import UIKit.UIImage
public typealias VIAPointsMonitorColor = UIColor
public typealias VIAPointsMonitorImage = UIImage
#endif

// swiftlint:disable file_length

public typealias VIAPointsMonitorAssetType = VIAPointsMonitorImageAsset

public struct VIAPointsMonitorImageAsset: Equatable {
	fileprivate var name: String

	public var templateImage: VIAPointsMonitorImage {
		let bundle = Bundle(for: BundleToken.self)
		#if os(iOS) || os(tvOS)
		let image = VIAPointsMonitorImage(named: name, in: bundle, compatibleWith: nil)?.withRenderingMode(.alwaysTemplate)
		#elseif os(OSX)
		let image = bundle.image(forResource: name).withRenderingMode(.alwaysTemplate)
		#elseif os(watchOS)
		let image = VIAPointsMonitorImage(named: name).withRenderingMode(.alwaysTemplate)
		#endif
		guard let result = image else { fatalError("Unable to load template image named \(name).") }
		return result
	}

	public var image: VIAPointsMonitorImage {
		let bundle = Bundle(for: BundleToken.self)
		#if os(iOS) || os(tvOS)
		let image = VIAPointsMonitorImage(named: name, in: bundle, compatibleWith: nil)
		#elseif os(OSX)
		let image = bundle.image(forResource: name)
		#elseif os(watchOS)
		let image = VIAPointsMonitorImage(named: name)
		#endif
		guard let result = image else { fatalError("Unable to load image named \(name).") }
		return result
	}

	public static func ==(lhs: VIAPointsMonitorImageAsset, rhs: VIAPointsMonitorImageAsset) -> Bool {
        return lhs.image == rhs.image
    }
}

public struct VIAPointsMonitorColorAsset {
fileprivate var name: String

#if swift(>=3.2)
@available(iOS 11.0, tvOS 11.0, watchOS 4.0, OSX 10.13, *)
var color: VIAPointsMonitorColor {
return VIAPointsMonitorColor(asset: self)
}
#endif
}

// swiftlint:disable identifier_name line_length nesting type_body_length type_name
public enum VIAPointsMonitorAsset {
  public static let vaccinationCategoryLogo = VIAPointsMonitorImageAsset(name: "vaccinationCategoryLogo")
  public static let screeningCategoryLogo = VIAPointsMonitorImageAsset(name: "screeningCategoryLogo")
  public static let allCategoryLogo = VIAPointsMonitorImageAsset(name: "allCategoryLogo")
  public static let nutritionCategoryLogo = VIAPointsMonitorImageAsset(name: "nutritionCategoryLogo")
  public static let assessmentsCategoryLogo = VIAPointsMonitorImageAsset(name: "assessmentsCategoryLogo")
  public static let category = VIAPointsMonitorImageAsset(name: "category")
  public static let calendarFilterIcon = VIAPointsMonitorImageAsset(name: "calendarFilterIcon")
  public static let arrowDownIcon = VIAPointsMonitorImageAsset(name: "arrowDownIcon")
  public static let helpGeneric = VIAPointsMonitorImageAsset(name: "helpGeneric")
  public static let trophyContributionSmall = VIAPointsMonitorImageAsset(name: "trophyContributionSmall")
  public static let otherCategoryLogo = VIAPointsMonitorImageAsset(name: "otherCategoryLogo")
  public static let watchContributionSmall = VIAPointsMonitorImageAsset(name: "watchContributionSmall")
  public static let getActiveCategoryLogo = VIAPointsMonitorImageAsset(name: "getActiveCategoryLogo")
  public static let boosterPoints = VIAPointsMonitorImageAsset(name: "boosterPoints")
  public static let pointsAdjustment = VIAPointsMonitorImageAsset(name: "pointsAdjustment")

  // swiftlint:disable trailing_comma
  public static let allColors: [VIAPointsMonitorColorAsset] = [
  ]
  public static let allImages: [VIAPointsMonitorImageAsset] = [
    vaccinationCategoryLogo,
    screeningCategoryLogo,
    allCategoryLogo,
    nutritionCategoryLogo,
    assessmentsCategoryLogo,
    category,
    calendarFilterIcon,
    arrowDownIcon,
    helpGeneric,
    trophyContributionSmall,
    otherCategoryLogo,
    watchContributionSmall,
    getActiveCategoryLogo,
    boosterPoints,
    pointsAdjustment,
  ]
  // swiftlint:enable trailing_comma
  @available(*, deprecated, renamed: "allImages")
  public static let allValues: [VIAPointsMonitorAssetType] = allImages
}
// swiftlint:enable identifier_name line_length nesting type_body_length type_name

extension VIAPointsMonitorImage {
@available(iOS 1.0, tvOS 1.0, watchOS 1.0, *)
@available(OSX, deprecated,
message: "This initializer is unsafe on macOS, please use the VIAPointsMonitorImageAsset.image property")
convenience init!(asset: VIAPointsMonitorAssetType) {
#if os(iOS) || os(tvOS)
let bundle = Bundle(for: BundleToken.self)
self.init(named: asset.name, in: bundle, compatibleWith: nil)
#elseif os(OSX) || os(watchOS)
self.init(named: asset.name)
#endif
}
}

extension VIAPointsMonitorColor {
#if swift(>=3.2)
@available(iOS 11.0, tvOS 11.0, watchOS 4.0, OSX 10.13, *)
convenience init!(asset: VIAPointsMonitorColorAsset) {
let bundle = Bundle(for: BundleToken.self)
#if os(iOS) || os(tvOS)
self.init(named: asset.name, in: bundle, compatibleWith: nil)
#elseif os(OSX)
self.init(named: asset.name, bundle: bundle)
#elseif os(watchOS)
self.init(named: asset.name)
#endif
}
#endif
}

private final class BundleToken {}
