//
//  PointsMonitorActivityDetailViewModel.swift
//  VIAPointsMonitor
//
//  Created by wenilyn.a.teorica on 09/05/2019.
//  Copyright © 2019 Glucode. All rights reserved.
//

import VitalityKit
import VIACommon

public struct Detail {
    let detailHeader: String
    let detailDescription: String
    
    public init(header: String, description: String) {
        detailHeader = header
        detailDescription = description
    }
}

open class PointsMonitorActivityDetailViewModel {
    
    public lazy var dateFormatter = DateFormatter.dayDateShortMonthyearFormatter()
    
    public var pointsEntry: PointsEntry!
    public var details: [Detail] = []
    
    public init() {}

    open func buildUpDetailsFromPointsEntry() {
        
        details.append(Detail(header: CommonStrings.Pm.ActivityDetailSectionSubheading527, description: pointsEntry.typeName))
        
        for metadata in pointsEntry.metadatas {
            let header = (metadata.typeName ?? metadata.typeCode) ?? ""
            
            var description = metadata.format()
            if let convertJoulesToCalories = VIAApplicableFeatures.default.convertJoulesToCalories, convertJoulesToCalories{
                if metadata.typeCode == "EnergyExpenditure" {
                    if let joulesValue = Localization.decimalFormatter.number(from: metadata.value){
                        let calorieValue = UnitEnergy.calories.converter.value(fromBaseUnitValue: joulesValue.doubleValue)
                        description = Localization.decimalFormatter.string(from: calorieValue as NSNumber)!
                    }
                }
            } else {
                if let joulesValue = Localization.decimalFormatter.number(from: metadata.value){
                    description = Localization.decimalFormatter.string(from: joulesValue as NSNumber)!
                }
            }
            
            //Duration
            if let tenantID = AppSettings.getAppTenant(), tenantID == .GI || tenantID == .UKE {
                if metadata.typeCode == "Duration" {
                    if let duration = String(metadata.value) {
                        description = convertDuration(fromSeconds: duration)
                    }
                }
            }
            
            details.append(Detail(header: header, description: description))
        }
        
        let date = dateFormatter.string(from: pointsEntry.effectiveDate)
        details.append(Detail(header: CommonStrings.DateTitle264, description: date))

    }

    public func convertDuration(fromSeconds metadataValue: String) -> String {
        
        /** FC-23282
         * Failed to convert the "metadataValue" when it contains a decimal.
         */
        var convertedValue = ""
        if let tenantID = AppSettings.getAppTenant(), tenantID == .GI {
            convertedValue = convertDurationValue(durationValue: metadataValue) ?? metadataValue
        } else {
            convertedValue = metadataValue
        }
        
        if let durationValue = Localization.integerFormatter.number(from: convertedValue) as? Int {
            let hours = durationValue / 3600
            let minutes = (durationValue % 3600) / 60
            let seconds = (durationValue % 3600) % 60
            return CommonStrings.Ar.EventDetail.DurationHhMmSs851("\(hours)", "\(minutes)", "\(seconds)")
        }
        
        return metadataValue
    }
    
    public func convertDurationValue(durationValue: String) -> String? {
        
        /** Convert the String value to Float
         * This will handle if the String value is in Decimal or Whole Number
         */
        guard let durationValueAsFloat = Float(durationValue) else {
            return durationValue
        }
        
        /* Convert the "durationValueAsFloat" into DataType Int */
        let durationValueAsInt = Int(durationValueAsFloat)
        
        return String(durationValueAsInt)
    }

}
