//
//  VIAARChooseRewardHeaderView.swift
//  glucodeWidgets7.1
//

import UIKit
import TTTAttributedLabel
import VIAUIKit

public final class VIAARChooseRewardHeaderView: UIView {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var contentLabel: TTTAttributedLabel!

    var image: UIImage? {
        didSet {
            self.imageView.image = self.image
        }
    }

    var header: String = "" {
        didSet {
            self.headerLabel.text = self.header
        }
    }

    var content: String = "" {
        didSet {
            self.contentLabel.text = self.content
        }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)

        let view = Bundle.main.loadNibNamed("VIAARChooseRewardHeaderView", owner: self, options: nil)?[0] as! UIView

        view.frame = self.bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.layoutIfNeeded()
        self.addSubview(view)
    }

    public override func layoutSubviews() {
        super.layoutSubviews()
        self.frame = bounds
    }

    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    public override func awakeFromNib() {
        super.awakeFromNib()

        // reset IB values
        self.imageView.image = nil
        self.headerLabel.text = nil
        self.contentLabel.text = nil

        self.contentLabel.verticalAlignment = .top
    }
}
