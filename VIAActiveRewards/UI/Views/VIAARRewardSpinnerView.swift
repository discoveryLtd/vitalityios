//
//  VIAARRewardSpinnerView.swift
//  VitalityActive
//
//  Created by Joshua Ryan on 11/22/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

import UIKit
import VIACarousel
import VIAUIKit

public class VIAARRewardSpinnerView: UIView, Nibloadable {
	
	// MARK: Public
	public var carousel: iCarousel!
	@IBOutlet public weak var spinnerWrapper: UIView!
	@IBOutlet public weak var spinButton: UIButton!
	@IBOutlet public weak var partnersButton: UIButton!
	@IBOutlet weak var titleLabel: UILabel!
	@IBOutlet weak var instructionLabel: UILabel!
	@IBOutlet weak var cellImageView: UIImageView!
	
    public var cellImage: UIImage? {
        set {
            self.cellImageView.image = newValue
        }
        get {
            return self.cellImageView.image
        }
    }
    
	public var title: String = "" {
		didSet {
			self.titleLabel?.text = self.title
		}
	}
	
	public var instructions: String = "" {
		didSet {
			self.instructionLabel.text = self.instructions
		}
	}
	

	// MARK: Lifecycle
	
	public func initializeCarousel(delegate: Any) {
		
		if let delegate = delegate as? iCarouselDataSource {
			self.carousel.dataSource = delegate
		}
		
		if let delegate = delegate as? iCarouselDelegate {
			self.carousel.delegate = delegate
		}

		self.carousel.reloadData()
	}
	
	// MARK: - IBAction
    public func spin(targetIndex: Int, isSpinningUpward: Bool) {
		self.carousel.isScrollEnabled = false
		self.carousel.isPagingEnabled = false
		self.carousel.isUserInteractionEnabled = false
		self.spinButton.isEnabled = false
        let indexOffset: CGFloat = self.carousel.offsetForItem(at: targetIndex)
        let itemsToScroll = (isSpinningUpward) ? CGFloat((self.carousel.numberOfItems * 10)) + indexOffset : CGFloat((self.carousel.numberOfItems * 10)) - indexOffset
        
        // Assign a negative value to 'itemsToScroll' to reverse the direction of the spin.
        self.carousel.scroll(byOffset: (isSpinningUpward) ? itemsToScroll : -itemsToScroll, duration: 5)
	}
	
	public override func awakeFromNib() {
		super.awakeFromNib()
		 
		// reset IB values
		titleLabel.text = nil
		titleLabel.font = .title2Font()
		instructionLabel.text = nil
		
		self.carousel = iCarousel()
		self.carousel.type = .cylinder
		self.carousel.isVertical = true
		
		self.carousel.decelerationRate = 0.95;
		self.spinnerWrapper.addSubview(self.carousel)
		self.carousel.snp.makeConstraints { (make) in
			make.top.bottom.equalToSuperview()
			make.left.right.equalToSuperview()
		}
		
		self.spinButton.tintColor = UIColor.currentGlobalTintColor()
	}
}
