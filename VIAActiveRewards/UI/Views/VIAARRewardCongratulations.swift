//
//  VIAARRewardCongratulations.swift
//  glucodeWidgets7.1
//
//

import UIKit
import VIAUIKit

public protocol VIAARewardConfirmationDelegate: class {
	func confirmReward()
	func swapReward()
}

public final class VIAARRewardCongratulations: UIView, Nibloadable {

	// MARK: Public
	public weak var delegate: VIAARewardConfirmationDelegate?

	// MARK: Private
    @IBOutlet weak var headerLabel: UILabel!
	@IBOutlet weak var disclaimerLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!

    @IBOutlet weak var rewardHeaderLabel: UILabel!
    @IBOutlet weak var rewardContentLabel: UILabel!
    @IBOutlet weak var rewardAchievedDateLabel: UILabel!
    
    @IBOutlet var confirmButton: VIAButton!
	@IBOutlet var swapButton: UIButton!

    override public init(frame: CGRect) {
        super.init(frame: frame)

        let view = Bundle.main.loadNibNamed("VIAARRewardCongratulations", owner: self, options: nil)?[0] as! UIView

        view.frame = self.bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.layoutIfNeeded()
        self.addSubview(view)
    }

    public var header: String = "" {
        didSet {
            self.headerLabel?.text = self.header
        }
    }

    public var image: UIImage? {
        didSet {
            self.imageView?.image = self.image
        }
    }

    public var rewardHeader: String = "" {
        didSet {
            self.rewardHeaderLabel?.text = self.rewardHeader
        }
    }

    public var rewardContent: String = "" {
        didSet {
            self.rewardContentLabel?.text = self.rewardContent
        }
    }
    
    public var rewardDisclaimer: String = "" {
        didSet {
            self.disclaimerLabel?.text = self.rewardDisclaimer
        }
    }
    
    public var rewardAchievedDate: String = "" {
        didSet {
            self.rewardAchievedDateLabel?.text = self.rewardAchievedDate
        }
    }

    public var rewardAchievedDateIsHidden: Bool = true {
        didSet {
            self.rewardAchievedDateLabel.isHidden = rewardAchievedDateIsHidden
            self.setNeedsUpdateConstraints()
            self.updateConstraintsIfNeeded()
            self.setNeedsLayout()
            self.layoutIfNeeded()
        }
    }

    public override func layoutSubviews() {
        super.layoutSubviews()
		self.confirmButton.tintColor = self.tintColor
        self.frame = bounds
    }

    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    public override func awakeFromNib() {
        super.awakeFromNib()

        // reset IB values
        self.headerLabel?.text = nil
        self.imageView?.image = nil
        self.rewardHeaderLabel?.text = nil
        self.rewardContentLabel?.text = nil
        self.rewardAchievedDateLabel?.text = nil
		self.swapButton.addTarget(self, action: #selector(swapReward(_:)), for: .touchUpInside)
		self.confirmButton.addTarget(self, action: #selector(confirmReward(_:)), for: .touchUpInside)
		self.disclaimerLabel.textColor = UIColor.mediumGrey()
    }

	// MARK: IBActions
	@IBAction func confirmReward(_ sender: Any) {
		self.delegate?.confirmReward()
	}

	@IBAction func swapReward(_ sender: Any) {
		self.delegate?.swapReward()
	}
}
