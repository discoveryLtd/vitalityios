//
//  VIAARRewardChosenView.swift
//  VitalityActive
//
//  Created by Joshua Ryan on 6/19/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

import UIKit
import TTTAttributedLabel
import VIAUIKit

public class VIAARRewardChosenView: UIView, Nibloadable {

	// MARK: Private
	@IBOutlet weak var imageView: UIImageView!
	@IBOutlet weak var headerLabel: UILabel!
	@IBOutlet weak var contentLabel: TTTAttributedLabel!
	@IBOutlet weak var voucherLabel: UILabel!
	@IBOutlet weak var instructionLabel: UILabel!
    @IBOutlet weak var voucherCodeLabel: UILabel!
    @IBOutlet var achievedDateLabel: UILabel!
    
	// MARK: Public
	public var header: String = "" {
		didSet {
			self.headerLabel?.text = self.header
		}
	}

	public var image: UIImage? {
		didSet {
			self.imageView?.image = self.image
		}
	}

	public var content: String = "" {
		didSet {
			self.contentLabel?.text = self.content
		}
	}

	public var voucherCode: String = "" {
		didSet {
			self.voucherLabel.text = self.voucherCode
		}
	}
    
    public var achievedDate: String = "" {
        didSet {
            self.achievedDateLabel.text = self.achievedDate
        }
    }

	public var instructions: String = "" {
		didSet {
			self.instructionLabel.text = self.instructions
		}
	}

	public override func layoutSubviews() {
		super.layoutSubviews()
		self.frame = bounds
	}

	public required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
	}

    public var achievedDateIsHidden: Bool = true {
        didSet {
            self.achievedDateLabel.isHidden = achievedDateIsHidden
            self.setNeedsUpdateConstraints()
            self.updateConstraintsIfNeeded()
            self.setNeedsLayout()
            self.layoutIfNeeded()
        }
    }

	override init(frame: CGRect) {
		super.init(frame: frame)

		let view = Bundle.main.loadNibNamed("VIAARRewardChoseView", owner: self, options: nil)?[0] as! UIView

		view.frame = self.bounds
		view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
		view.layoutIfNeeded()
		self.addSubview(view)
	}

	public override func awakeFromNib() {
		super.awakeFromNib()

		// reset IB values
		imageView.image = nil
		headerLabel.text = nil
		contentLabel.text = nil
		voucherLabel.text = nil
        voucherLabel.minimumScaleFactor = 0.6
        voucherLabel.adjustsFontSizeToFitWidth = true
		instructionLabel.text = nil
        achievedDateLabel.text = nil

		contentLabel.verticalAlignment = .top
		voucherLabel.textColor = UIColor.rewardBlue()
        voucherLabel.textAlignment = NSTextAlignment.natural
		instructionLabel.textColor = UIColor.mediumGrey()
	}
}
