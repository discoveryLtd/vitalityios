//
//  VIAARRewardCongratulationsSolution.swift
//  VIAActiveRewards
//
//  Created by Von Kervin R. Tuico on 08/04/2019.
//  Copyright © 2019 Glucode. All rights reserved.
//

import UIKit
import VIAUIKit

public final class VIAARRewardCongratulationsSolution: UIView, Nibloadable {
    
    // MARK: Private
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var headerVoucherRewardNameLabel: UILabel!
    @IBOutlet weak var headerVoucherRewardNameView: UIView!
    
    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet weak var rewardHeaderLabel: UILabel!
    @IBOutlet weak var rewardContentLabel: UILabel!
    @IBOutlet weak var rewardAchievedDateLabel: UILabel!
    
    /* Short Term Solution View */
    @IBOutlet weak var rewardShortTermView: UIView!
    @IBOutlet weak var rewardShortTermEmailedDateLabel: UILabel!
    @IBOutlet weak var rewardShortTermCallNumberLabel: UILabel!
    
    /* Long Term Solution View */
    @IBOutlet weak var rewardLongTermView: UIView!
    @IBOutlet weak var rewardLongTermVoucherTitleLabel: UILabel!
    @IBOutlet weak var rewardLongTermVoucherCodeLabel: UILabel!
    @IBOutlet public weak var rewardLongTermRedeemLabel: UILabel!
    
    @IBOutlet weak var disclaimerLabel: UILabel!
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        
        let view = Bundle.main.loadNibNamed("VIAARRewardCongratulationsSolution", owner: self, options: nil)?[0] as! UIView
        
        view.frame = self.bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.layoutIfNeeded()
        self.addSubview(view)
    }
    
    public var header: String = "" {
        didSet {
            self.headerLabel?.text = self.header
        }
    }
    
    public var headerVoucherRewardName: String = "" {
        didSet {
            self.headerVoucherRewardNameLabel.text = self.headerVoucherRewardName
        }
    }
    
    public var headerVoucherRewardNameViewIsHidden: Bool = true {
        didSet {
            self.headerVoucherRewardNameView.isHidden = headerVoucherRewardNameViewIsHidden
            self.setNeedsUpdateConstraints()
            self.updateConstraintsIfNeeded()
            self.setNeedsLayout()
            self.layoutIfNeeded()
        }
    }
    
    public var image: UIImage? {
        didSet {
            self.imageView?.image = self.image
        }
    }
    
    public var imageViewIsHidden: Bool = false {
        didSet {
            self.imageView.isHidden = imageViewIsHidden
            self.setNeedsUpdateConstraints()
            self.updateConstraintsIfNeeded()
            self.setNeedsLayout()
            self.layoutIfNeeded()
        }
    }
    
    public var rewardHeader: String = "" {
        didSet {
            self.rewardHeaderLabel?.text = self.rewardHeader
        }
    }
    
    public var rewardContent: String = "" {
        didSet {
            self.rewardContentLabel?.text = self.rewardContent
        }
    }
    
    public var rewardDisclaimer: String = "" {
        didSet {
            self.disclaimerLabel?.text = self.rewardDisclaimer
        }
    }
    
    public var rewardAchievedDate: String = "" {
        didSet {
            self.rewardAchievedDateLabel?.text = self.rewardAchievedDate
        }
    }
    
    public var rewardAchievedDateIsHidden: Bool = true {
        didSet {
            self.rewardAchievedDateLabel.isHidden = rewardAchievedDateIsHidden
            self.setNeedsUpdateConstraints()
            self.updateConstraintsIfNeeded()
            self.setNeedsLayout()
            self.layoutIfNeeded()
        }
    }
    
    /** Short Term Solution
     * DidSet Widgets
     */
    
    public var rewardShortTermViewIsHidden: Bool = true {
        didSet {
            self.rewardShortTermView.isHidden = rewardShortTermViewIsHidden
            self.setNeedsUpdateConstraints()
            self.updateConstraintsIfNeeded()
            self.setNeedsLayout()
            self.layoutIfNeeded()
        }
    }
    
    public var rewardShortTermEmailedDate: String = "" {
        didSet {
            self.rewardShortTermEmailedDateLabel.text = rewardShortTermEmailedDate
        }
    }
    
    public var rewardShortTermCallNumber: String = "" {
        didSet {
            self.rewardShortTermCallNumberLabel.text = rewardShortTermCallNumber
        }
    }
    
    /** Long Term Solution
     * DidSet Widgets
     */
    
    public var rewardLongTermViewIsHidden: Bool = true {
        didSet {
            self.rewardLongTermView.isHidden = rewardLongTermViewIsHidden
            self.setNeedsUpdateConstraints()
            self.updateConstraintsIfNeeded()
            self.setNeedsLayout()
            self.layoutIfNeeded()
        }
    }
    
    public var rewardLongTermVoucherTitle: String = "" {
        didSet {
            self.rewardLongTermVoucherTitleLabel.text = rewardLongTermVoucherTitle
        }
    }
    
    public var rewardLongTermVoucherCode: String = "" {
        didSet {
            self.rewardLongTermVoucherCodeLabel.text = rewardLongTermVoucherCode
        }
    }
    
    public var rewardLongTermRedeem: String = "" {
        didSet {
            self.rewardLongTermRedeemLabel.text = rewardLongTermRedeem
        }
    }
    
    public var rewardLongTermRedeemLabelType: UILabel {
        get {
            return self.rewardLongTermRedeemLabel
        }
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        self.frame = bounds
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    public override func awakeFromNib() {
        super.awakeFromNib()
        
        // reset IB values
        self.headerLabel?.text = nil
        self.imageView?.image = nil
        self.rewardHeaderLabel?.text = nil
        self.rewardContentLabel?.text = nil
        self.rewardAchievedDateLabel?.text = nil
        self.disclaimerLabel.textColor = UIColor.mediumGrey()
        
        /* Short Term Solution Default Value */
        //        self.rewardShortTermView.isHidden = true
        self.rewardShortTermEmailedDateLabel.text = nil
        self.rewardShortTermCallNumberLabel.text = nil
        
        /* Long Term Solution Default Value */
        self.rewardLongTermVoucherTitleLabel.text = nil
        self.rewardLongTermVoucherCodeLabel.text = nil
        self.rewardLongTermRedeemLabel.text = nil
    }
    
}
