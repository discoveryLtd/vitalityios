import UIKit
import TTTAttributedLabel
import VIAUIKit

public class PagingStatusTableViewSectionHeaderFooterView: UITableViewHeaderFooterView, Nibloadable {
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var tryAgainButton: BorderButton!
    
    public var actionBlock: (() -> Void) = {
        // no default implementation
    }
    
    public override func awakeFromNib() {
        super.awakeFromNib()
        
        self.setup()
    }
    
    func setup() {
        statusLabel.isHidden = true
        tryAgainButton.isHidden = true
        activityIndicator.hidesWhenStopped = true
        activityIndicator.stopAnimating()
        
        statusLabel.text = nil
        statusLabel.textColor = UIColor.tableViewSectionHeaderFooter()
        statusLabel.font = UIFont.tableViewSectionHeaderFooter()
        statusLabel.backgroundColor = UIColor.tableViewBackground()
        
        tryAgainButton.setupButton()
    }
    
    public func configureSwipeToLoad() {
        statusLabel.isHidden = true
        tryAgainButton.isHidden = true
        activityIndicator.stopAnimating()
    }
    
   public func configureLoadingData() {
        statusLabel.isHidden = true
        tryAgainButton.isHidden = true
        activityIndicator.startAnimating()
    }
    
   public func configureWith(noActivityText: String) {
        statusLabel.isHidden = false
        tryAgainButton.isHidden = true
        activityIndicator.stopAnimating()
        
        statusLabel.text = noActivityText
    }
    
    public func configureWith(errorText: String, tryAgainButtonText: String) {
        statusLabel.isHidden = false
        tryAgainButton.isHidden = false
        activityIndicator.stopAnimating()
        
        statusLabel.text = errorText
        tryAgainButton.setTitle(tryAgainButtonText, for: .normal)
        tryAgainButton.addCustomPadding(top: 0, left: 50, bottom: 0, right: 50)
    }
    
    @IBAction func tryAgainTapped(_ sender: Any) {
        actionBlock()
    }
    
 
}
