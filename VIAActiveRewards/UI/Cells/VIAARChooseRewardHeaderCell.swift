//
//  VIAARChooseRewardHeaderCell.swift
//  VitalityActive
//
//  Created by Joshua Ryan on 6/7/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

import UIKit
import TTTAttributedLabel
import VIAUIKit

public class VIAARChooseRewardHeaderCell: UITableViewCell, Nibloadable {

	@IBOutlet var cellImageView: UIImageView!
	public var cellImage: UIImage? {
		set {
			self.cellImageView.image = newValue
		}
		get {
			return self.cellImageView.image
		}
	}

	@IBOutlet var titleLabel: TTTAttributedLabel!
	public var title: String? {
		set {
			self.titleLabel.text = newValue
		}
		get {
			return self.titleLabel.text as? String
		}
	}

	@IBOutlet public var descriptionLabel: TTTAttributedLabel!
	public var descriptionText: String? {
		set {
			self.descriptionLabel.text = newValue
		}
		get {
			return self.descriptionLabel.text as? String
		}
	}

	public override func awakeFromNib() {
		super.awakeFromNib()

		self.titleLabel.font = UIFont.title2Font()
		self.titleLabel.textColor = UIColor.night()

		self.descriptionLabel.font = UIFont.subheadlineFont()
		self.descriptionLabel.textColor = UIColor.night()

		self.accessoryType = .none
		self.isUserInteractionEnabled = false
	}
}
