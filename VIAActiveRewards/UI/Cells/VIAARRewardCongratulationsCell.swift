//
//  VIAARRewardCongratulationsCell.swift
//  VitalityActive
//
//  Created by Joshua Ryan on 9/22/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

import VIAUIKit

public final class VIAARRewardCongratulationsCell: UITableViewCell, Nibloadable {
	
	// MARK: Public
	public weak var delegate: VIAARewardConfirmationDelegate?
	
	// MARK: Private
	@IBOutlet weak var headerLabel: UILabel!
	@IBOutlet weak var rewardImageView: UIImageView!
	
	@IBOutlet weak var rewardHeaderLabel: UILabel!
	@IBOutlet weak var rewardContentLabel: UILabel!
	
	@IBOutlet weak var stackView: UIStackView!
	
	public var header: String = "" {
		didSet {
			self.headerLabel?.text = self.header
		}
	}
	
	public var rewardImage: UIImage? {
		didSet {
			self.rewardImageView?.image = self.rewardImage
		}
	}
	
	public var rewardHeader: String = "" {
		didSet {
			self.rewardHeaderLabel?.text = self.rewardHeader
		}
	}
	
	public var rewardContent: String = "" {
		didSet {
			self.rewardContentLabel?.text = self.rewardContent
		}
	}
	
	public override func awakeFromNib() {
		super.awakeFromNib()
		setupView()
	}
	
	public func addAdditionalView(view: UIView) {
		self.stackView.addArrangedSubview(view)
	}
	
	func setupView() {

		// reset IB values
		self.headerLabel?.text = nil
		self.rewardImageView?.image = nil
		self.rewardHeaderLabel?.text = nil
		self.rewardContentLabel?.text = nil
        for view in stackView.subviews {
            view.removeFromSuperview()
        }
        stackView.addArrangedSubview(self.headerLabel)
        stackView.addArrangedSubview(self.rewardImageView)
        stackView.addArrangedSubview(self.rewardHeaderLabel)
        stackView.addArrangedSubview(self.rewardContentLabel)
	}
}
