//
//  VIAARClaimedRewardCell.swift
//  VitalityActive
//
//  Created by Joshua Ryan on 6/20/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

import UIKit
import TTTAttributedLabel
import VIAUIKit

public class VIAARClaimedRewardCell: UITableViewCell, Nibloadable {

    @IBOutlet weak var usedLabel: UILabel!
    public var usedText: String? {
        set {
            self.usedLabel.text = newValue
        }
        get {
            return self.usedLabel.text
        }
    }

    @IBOutlet weak var radioButton: UIButton!

    @IBOutlet var cellImageView: UIImageView!
	public var cellImage: UIImage? {
		set {
			self.cellImageView.image = newValue
		}
		get {
			return self.cellImageView.image
		}
	}
    
    @IBOutlet var achievedDateLabel: UILabel!
    public var achievedDateText: String? {
        set {
            self.achievedDateLabel.text = newValue
        }
        get {
            return self.achievedDateLabel.text as? String
        }
    }
    
	@IBOutlet var vendorLabel: TTTAttributedLabel!
	public var vendorText: String? {
		set {
			self.vendorLabel.text = newValue
		}
		get {
			return self.vendorLabel.text as? String
		}
	}

	@IBOutlet var descriptionLabel: UILabel!
	public var descriptionText: String? {
		set {
			self.descriptionLabel.text = newValue
		}
		get {
			return self.descriptionLabel.text as? String
		}
	}

	@IBOutlet var dateLabel: UILabel!
	public var dateText: String? {
		set {
			self.dateLabel.text = newValue
		}
		get {
			return self.dateLabel.text as? String
		}
	}

	@IBOutlet var actionLabel: UILabel!
	public var actionText: String? {
		set {
			self.actionLabel.text = newValue
		}
		get {
			return self.actionLabel.text as? String
		}
	}

	public var actionLabelIsHidden: Bool {
		set {
			self.actionLabel.isHidden = newValue
			self.setNeedsUpdateConstraints()
			self.updateConstraintsIfNeeded()
			self.setNeedsLayout()
			self.layoutIfNeeded()
		}
		get {
			return self.actionLabel.isHidden
		}
	}
    
    public var achievedDateLabelIsHidden: Bool {
        set {
            self.achievedDateLabel.isHidden = newValue
            self.setNeedsUpdateConstraints()
            self.updateConstraintsIfNeeded()
            self.setNeedsLayout()
            self.layoutIfNeeded()
        }
        get {
            return self.achievedDateLabel.isHidden
        }
    }
    
    
    public var usedLabelIsHidden: Bool {
        set {
            self.usedLabel.isHidden = newValue
            self.setNeedsUpdateConstraints()
            self.updateConstraintsIfNeeded()
            self.setNeedsLayout()
            self.layoutIfNeeded()
        }
        get {
            return self.usedLabel.isHidden
        }
    }

    public var radioButtonIsHidden: Bool {
        set {
            self.radioButton.isHidden = newValue
            self.setNeedsUpdateConstraints()
            self.updateConstraintsIfNeeded()
            self.setNeedsLayout()
            self.layoutIfNeeded()
        }
        get {
            return self.radioButton.isHidden
        }
    }

	override public func awakeFromNib() {
		super.awakeFromNib()

		self.vendorLabel.text = nil
		self.vendorLabel.font = UIFont.subheadlineFont()
		self.vendorLabel.textColor = UIColor.night()

		self.descriptionLabel.text = nil
		self.descriptionLabel.font = UIFont.title2Font()
		self.descriptionLabel.textColor = UIColor.night()

		self.dateLabel.text = nil
		self.dateLabel.font = UIFont.subheadlineFont()
		self.dateLabel.textColor = UIColor.mediumGrey()
        self.dateLabel.numberOfLines = 0

		self.actionLabel.text = nil
		self.actionLabel.font = UIFont.subheadlineFont()
		self.actionLabel.textColor = UIColor.cellButton()
        
        self.achievedDateLabel.text = nil
        self.achievedDateLabel.font = UIFont.subheadlineFont()
        self.achievedDateLabel.textColor = UIColor.mediumGrey()
        self.achievedDateLabel.numberOfLines = 0
	}

	public override func layoutSubviews() {
		super.layoutSubviews()
		let labelFrame = self.convert(vendorLabel.frame, from: vendorLabel.superview)
		self.separatorInset = UIEdgeInsets(top: 0, left: labelFrame.origin.x, bottom: 0, right: 0)
	}
}
