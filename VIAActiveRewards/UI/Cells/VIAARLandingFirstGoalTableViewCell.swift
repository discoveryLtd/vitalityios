//
//  VIAARLandingFirstGoalTableViewCell.swift
//  VitalityActive
//
//  Created by Wilmar van Heerden on 2016/11/14.
//  Copyright © 2016 Glucode. All rights reserved.
//

import UIKit
import VIAUIKit

public class VIAARLandingFirstGoalTableViewCell: UITableViewCell, Nibloadable {

    @IBOutlet public var cellImageView: UIImageView!
    @IBOutlet public var headingLabel: UILabel!
    @IBOutlet public var messageLabel: UILabel!

    public override func awakeFromNib() {
        super.awakeFromNib()

        self.backgroundColor = UIColor.day()

        self.cellImageView.backgroundColor = UIColor.day()

        self.headingLabel.text = nil
        self.headingLabel.font = UIFont.title2Font()
        self.headingLabel.textColor = UIColor.night()
        self.headingLabel.numberOfLines = 0
        self.headingLabel.textAlignment = .center
        self.headingLabel.backgroundColor = UIColor.day()

        self.messageLabel.text = nil
        self.messageLabel.font = UIFont.bodyFont()
        self.messageLabel.textColor = UIColor.night()
        self.messageLabel.numberOfLines = 0
        self.messageLabel.textAlignment = .center
        self.messageLabel.backgroundColor = UIColor.day()

        self.layer.borderWidth = 1 / UIScreen.main.scale
        self.layer.borderColor = UIColor.viewBorder().cgColor
    }
}
