import UIKit
import VIAUIKit

public protocol VIAARCopyVoucherRewardCellDelegate: NSObjectProtocol {
    func copyButtonPressed()
}

public final class VIAARCopyVoucherRewardCell: UITableViewCell, Nibloadable {
    
	// MARK: Private
	@IBOutlet weak var partnerImageView: UIImageView!
	@IBOutlet weak var headerLabel: UILabel!
	@IBOutlet weak var titleLabel: UILabel!
	@IBOutlet weak var expirationLabel: UILabel!
	@IBOutlet weak var codeLabel: UILabel!
	@IBOutlet weak var instructionsLabel: UILabel!
	@IBOutlet weak var copyButton: UIButton!
	
	@IBOutlet weak var stackView: UIStackView!
	
	// MARK: Public
    public weak var delegate: VIAARCopyVoucherRewardCellDelegate?
    
	public var partnerImage: UIImage? {
		didSet {
			self.partnerImageView?.image = self.partnerImage
		}
	}
	
	public var header: String = "" {
		didSet {
			self.headerLabel?.text = self.header
		}
	}
	
	public var title: String = "" {
		didSet {
			self.titleLabel?.text = self.title
		}
	}
	
	public var expiration: String = "" {
		didSet {
			self.expirationLabel?.text = self.expiration
		}
	}
	
	public var code: String = "" {
		didSet {
			self.codeLabel?.text = self.code
		}
	}
	
	public var copyText: String = "" {
		didSet {
			self.copyButton.setTitle(self.copyText, for: .normal)
		}
	}
	
	public var instructions: String = "" {
		didSet {
			self.instructionsLabel?.text = self.instructions
		}
	}
	
	public override func awakeFromNib() {
		super.awakeFromNib()
		setupView()
	}
	
	public func addAdditionalView(view: UIView) {
		self.stackView.addArrangedSubview(view)
	}
	
	func setupView() {
		
		// reset IB values
		//self.partnerImageView?.image = nil
		self.headerLabel?.text = nil
		self.headerLabel?.font = UIFont.subheadlineFont()
		self.titleLabel?.text = nil
		self.titleLabel?.font = UIFont.title2Font()
		
		self.expirationLabel?.text = nil
		self.expirationLabel?.font = UIFont.subheadlineFont()
		self.codeLabel?.text = nil
		self.codeLabel?.font = UIFont.title2Font()
		self.instructionsLabel?.text = nil
		self.instructionsLabel?.font = UIFont.subheadlineFont()
	}
	
	@IBAction func copyCode(_ sender: UIButton) {
        if (self.delegate != nil) {
            self.delegate?.copyButtonPressed()
        }
		UIPasteboard.general.string = self.code
	}
}
