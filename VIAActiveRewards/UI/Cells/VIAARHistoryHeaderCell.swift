//
//  VIAARHistoryHeaderCell.swift
//  VitalityActive
//
//  Created by Joshua Ryan on 6/22/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

import UIKit
import VIAUIKit

public class VIAARHistoryHeaderCell: UITableViewHeaderFooterView, Nibloadable {

	@IBOutlet private weak var titleLabel: UILabel!
	public var title: String? {
		set {
			titleLabel.text = newValue
			titleLabel.isHidden = newValue == nil
		}
		get {
			return titleLabel.text
		}
	}

	override public func awakeFromNib() {
		super.awakeFromNib()

		self.backgroundColor = UIColor.mercuryGrey()
		self.titleLabel.text = nil
		self.titleLabel.font = UIFont.subheadlineFont()
		self.titleLabel.textColor = UIColor.tableViewSectionHeaderFooter()
	}
}
