//
//  VIAARGoalCycleSummaryCell.swift
//  glucodeWidgets7.1
//
//

import UIKit
import TTTAttributedLabel
import VIAUIKit

public final class VIAARGoalCycleSummaryCell: UITableViewCell, Nibloadable {

    @IBOutlet weak var arrowImageView: UIImageView!

    @IBOutlet weak var statusLabel: UILabel!
    public var statusText: String? {
        set {
            self.statusLabel.text = newValue
        }
        get {
            return self.statusLabel.text as? String
        }
    }
    
    @IBOutlet weak var statusImageView: UIImageView!
    public var statusImage: UIImage? {
        set {
            self.statusImageView.image = newValue
        }
        get {
            return self.statusImageView.image
        }
    }

    @IBOutlet weak var dateRangeLabel: TTTAttributedLabel!
    public var dateRangeText: String? {
        set {
            self.dateRangeLabel.text = newValue
        }
        get {
            return self.dateRangeLabel.text as? String
        }
    }

    @IBOutlet weak var pointsLabel: TTTAttributedLabel!
    public var pointsText: String? {
        set {
            self.pointsLabel.text = newValue
        }
        get {
            return self.pointsLabel.text as? String
        }
    }

    public func hideArrowIndicator() {
        arrowImageView.isHidden = true
    }
    public func hideDateRange() {
        dateRangeLabel.isHidden = true
    }

    public override func prepareForReuse() {
        super.prepareForReuse()
        setupCell()
    }

    override public func awakeFromNib() {
        super.awakeFromNib()
        setupCell()
    }

    func setupCell() {
        statusLabel.text = nil
        pointsLabel.text = nil
        dateRangeLabel.text = nil
        statusImageView.image = nil

        statusLabel.font = .title2Font()
        pointsLabel.font = UIFont.subheadlineFont()
        dateRangeLabel.font = UIFont.subheadlineFont()

        statusLabel.textColor = .night()
        pointsLabel.textColor = .mediumGrey()
        dateRangeLabel.textColor = .mediumGrey()

        arrowImageView.isHidden = false
        dateRangeLabel.isHidden = false

        pointsLabel.verticalAlignment = .top
    }
}
