//
//  VIAARRewardMarkUsedCell.swift
//  VitalityActive
//
//  Created by Joshua Ryan on 10/11/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

import VIAUIKit

public class VIAARRewardMarkUsedCell: UITableViewCell, Nibloadable {
	
	public var buttonAction: (() -> Void)?
	
	@IBOutlet weak var button: UIButton!
	public var actionText: String? {
		set {
			self.button.setTitle(newValue, for: .normal)
		}
		get {
			return self.button.title(for: .normal)
		}
	}

	@IBOutlet var instructionLabel: UILabel!
	public var instructions: String? {
		set {
			self.instructionLabel.text = newValue
		}
		get {
			return self.instructionLabel.text
		}
	}
	
	public override func awakeFromNib() {
		super.awakeFromNib()
		
		self.button.addTarget(self, action: #selector(buttonTapped(_:)), for: .touchUpInside)
		self.button.setTitle(nil, for: .normal)
		self.instructionLabel.text = nil
		self.instructionLabel.font = UIFont.footnoteFont()
        self.instructionLabel.textColor = UIColor.darkGrey()
		
		self.accessoryType = .none
	}

	func buttonTapped(_ sender: UIButton) {
		guard let action = self.buttonAction else {
			return
		}
		action()
	}
}
