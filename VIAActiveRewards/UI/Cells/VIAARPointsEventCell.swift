//
//  ThisWeekActivity.swift
//  glucodeWidgets7.1
//
//

import UIKit
import TTTAttributedLabel
import VIAUIKit

public final class VIAARPointsEventCell: UITableViewCell, Nibloadable {

    @IBOutlet public var arrowImageView: UIImageView!

    @IBOutlet weak var deviceLabel: TTTAttributedLabel!
    public var deviceText: String? {
        set {
            self.deviceLabel.text = newValue
        }
        get {
            return self.deviceLabel.text as? String
        }
    }

    @IBOutlet weak var pointsLabel: UILabel!
    public var pointsText: String? {
        set {
            self.pointsLabel.text = newValue
        }
        get {
            return self.pointsLabel.text as? String
        }
    }

    @IBOutlet weak var descriptionLabel: UILabel!
    public var descriptionText: String? {
        set {
            self.descriptionLabel.text = newValue
        }
        get {
            return self.descriptionLabel.text as? String
        }
    }

    @IBOutlet weak var dateLabel: UILabel!
    public var dateText: String? {
        set {
            self.dateLabel.text = newValue
        }
        get {
            return self.dateLabel.text as? String
        }
    }

    public override func prepareForReuse() {
        super.prepareForReuse()
        setupCell()
    }

    override public func awakeFromNib() {
        super.awakeFromNib()
        setupCell()
    }

    func setupCell() {
        self.deviceLabel.text = nil
        self.pointsLabel.text = nil
        self.descriptionLabel.text = nil
        self.dateLabel.text = nil

        self.deviceLabel.verticalAlignment = .top
        self.descriptionLabel.numberOfLines = 0

        self.pointsLabel.font = UIFont.title1Font()
        self.descriptionLabel.font = UIFont.subheadlineFont()
        self.deviceLabel.font = UIFont.footnoteFont()
        self.dateLabel.font = UIFont.subheadlineFont()

        self.pointsLabel.textColor = UIColor.night()
        self.descriptionLabel.textColor = UIColor.night()
        self.deviceLabel.textColor = UIColor.mediumGrey()
        self.dateLabel.textColor = UIColor.mediumGrey()
    }

}
