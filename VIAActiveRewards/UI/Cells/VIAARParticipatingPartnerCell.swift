//
//  VIAARParticipatingPartnerCell.swift
//  VitalityActive
//
//  Created by Wilmar van Heerden on 2016/11/16.
//  Copyright © 2016 Glucode. All rights reserved.
//

import UIKit
import TTTAttributedLabel
import VIAUIKit

public class VIAARParticipatingPartnerCell: UITableViewCell, Nibloadable {

    @IBOutlet var cellImageView: UIImageView!
    public var cellImage: UIImage? {
        set {
            self.cellImageView.image = newValue
        }
        get {
            return self.cellImageView.image
        }
    }

    @IBOutlet var partnerNameLabel: TTTAttributedLabel!
    public var partnerName: String? {
        set {
            self.partnerNameLabel.text = newValue
        }
        get {
            return self.partnerNameLabel.text as? String
        }
    }

    @IBOutlet public var descriptionLabel: TTTAttributedLabel!
    public var descriptionText: String? {
        set {
            self.descriptionLabel.text = newValue
        }
        get {
            return self.descriptionLabel.text as? String
        }
    }

    public override func awakeFromNib() {
        super.awakeFromNib()

        self.partnerNameLabel.text = nil
        self.partnerNameLabel.font = UIFont.subheadlineFont()
        self.partnerNameLabel.textColor = UIColor.night()

        self.descriptionLabel.text = nil
        self.descriptionLabel.font = UIFont.headlineFont()
        self.descriptionLabel.textColor = UIColor.night()

        self.accessoryType = .disclosureIndicator
    }

	public func setDefaultInset() {
		let labelFrame = self.convert(partnerNameLabel.frame, from: partnerNameLabel.superview)
		self.separatorInset = UIEdgeInsets(top: 0, left: labelFrame.origin.x, bottom: 0, right: 0)
	}
}
