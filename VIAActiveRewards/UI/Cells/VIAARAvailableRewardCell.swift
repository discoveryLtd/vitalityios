//
//  VIAARAvailableRewardCell.swift
//  VitalityActive
//
//  Created by Wilmar van Heerden on 2016/11/16.
//  Copyright © 2016 Glucode. All rights reserved.
//

import UIKit
import VIAUIKit

public class VIAARAvailableRewardCell: UITableViewCell, Nibloadable {

    @IBOutlet var cellImageView: UIImageView!
    public var cellImage: UIImage? {
        set {
            self.cellImageView.image = newValue
        }
        get {
            return self.cellImageView.image
        }
    }

    @IBOutlet var headingLabel: UILabel!
    public var headingText: String? {
        set {
            self.headingLabel.text = newValue
        }
        get {
            return self.headingLabel.text
        }
    }

    @IBOutlet var descriptionLabel: UILabel!
    public var descriptionText: String? {
        set {
            self.descriptionLabel.text = newValue
        }
        get {
            return self.descriptionLabel.text
        }
    }

    @IBOutlet var actionLabel: UILabel!
    public var actionText: String? {
        set {
            self.actionLabel.text = newValue
        }
        get {
            return self.actionLabel.text
        }
    }
    public var actionLabelIsHidden: Bool {
        set {
            self.actionLabel.isHidden = newValue
            self.setNeedsUpdateConstraints()
            self.updateConstraintsIfNeeded()
            self.setNeedsLayout()
            self.layoutIfNeeded()
        }
        get {
            return self.actionLabel.isHidden
        }
    }

    override public func awakeFromNib() {
        super.awakeFromNib()

        self.headingLabel.text = nil
        self.headingLabel.font = UIFont.title2Font()
        self.headingLabel.textColor = UIColor.night()

        self.descriptionLabel.text = nil
        self.descriptionLabel.font = UIFont.subheadlineFont()
        self.descriptionLabel.textColor = UIColor.mediumGrey()

        self.actionLabel.text = nil
        self.actionLabel.font = UIFont.subheadlineFont()
        self.actionLabel.textColor = UIColor.cellButton()
    }

	public override func layoutSubviews() {
		super.layoutSubviews()
		let labelFrame = self.convert(headingLabel.frame, from: headingLabel.superview)
		self.separatorInset = UIEdgeInsets(top: 0, left: labelFrame.origin.x, bottom: 0, right: 0)
	}
}
