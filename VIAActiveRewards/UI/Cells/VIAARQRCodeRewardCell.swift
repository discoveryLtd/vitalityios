//
//  VIAARQRCodeRewardCell.swift
//  VIAActiveRewards
//
//  Created by Val Tomol on 05/02/2019.
//  Copyright © 2019 Glucode. All rights reserved.
//

import UIKit
import VIAUIKit

public final class VIAARQRCodeRewardCell: UITableViewCell, Nibloadable {
    // MARK: Private
    @IBOutlet weak var partnerImageView: UIImageView!
    @IBOutlet weak var qrCodeImageView: UIImageView!
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var expirationLabel: UILabel!
    @IBOutlet weak var instructionsLabel: UILabel!
    
    @IBOutlet weak var stackView: UIStackView!
    
    // MARK: Public
    public var partnerImage: UIImage? {
        didSet {
            self.partnerImageView?.image = self.partnerImage
        }
    }
    
    public var qrCodeImage: UIImage? {
        didSet {
            self.qrCodeImageView?.image = self.qrCodeImage
        }
    }
    
    public var header: String = "" {
        didSet {
            self.headerLabel?.text = self.header
        }
    }
    
    public var title: String = "" {
        didSet {
            self.titleLabel?.text = self.title
        }
    }
    
    public var expiration: String = "" {
        didSet {
            self.expirationLabel?.text = self.expiration
        }
    }
    
    public var instructions: String = "" {
        didSet {
            self.instructionsLabel?.text = self.instructions
        }
    }
    
    public override func awakeFromNib() {
        super.awakeFromNib()
        setupView()
    }
    
    public func addAdditionalView(view: UIView) {
        self.stackView.addArrangedSubview(view)
    }
    
    func setupView() {
        // reset IB values
        //self.partnerImageView?.image = nil
        self.headerLabel?.text = nil
        self.headerLabel?.font = UIFont.subheadlineFont()
        self.titleLabel?.text = nil
        self.titleLabel?.font = UIFont.title2Font()
        self.titleLabel?.numberOfLines = 0
        
        self.expirationLabel?.text = nil
        self.expirationLabel?.font = UIFont.subheadlineFont()
        self.instructionsLabel?.text = nil
        self.instructionsLabel?.font = UIFont.subheadlineFont()
    }
}
