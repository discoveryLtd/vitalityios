//
//  VIAARPartnerAppCell.swift
//  VitalityActive
//
//  Created by Joshua Ryan on 10/11/17.
//

import UIKit
import VIAUIKit

public class VIAARPartnerAppCell: UITableViewCell, Nibloadable {
	
	@IBOutlet var cellImageView: UIImageView!
	public var cellImage: UIImage? {
		set {
			self.cellImageView.image = newValue
		}
		get {
			return self.cellImageView.image
		}
	}
	
	@IBOutlet var appNameLabel: UILabel!
	public var appName: String? {
		set {
			self.appNameLabel.text = newValue
		}
		get {
			return self.appNameLabel.text
		}
	}
	
	@IBOutlet var appButton: BorderButton!
	public var appButtonText: String? {
		set {
			self.appButton.setTitle(newValue, for: .normal)
		}
		get {
			return self.appButton.title(for: .normal)
		}
	}
	
	public var targetUrl: String = ""
	
	public override func awakeFromNib() {
		super.awakeFromNib()
		
		self.appNameLabel.text = nil
		self.appNameLabel.font = UIFont.bodyFont()
		self.appNameLabel.textColor = UIColor.night()

		self.appButton.setupButton()
		self.appButton.addTarget(self, action: #selector(linkTapped(_:)), for: .touchUpInside)

		self.accessoryType = .none
	}
	
	public func hideBorder() {
		self.appButton.layer.borderWidth = 0
	}
	
	func linkTapped(_ sender: Any) {
		
		guard let url = URL(string: targetUrl) else {
			debugPrint("Could not construct URL with \(targetUrl)")
			return
		}
		
		if #available(iOS 10, *) {
			UIApplication.shared.open(url, options: [:], completionHandler: { (success) in
				print("User sent to \(url): with \(success)")
			})
		} else {
			UIApplication.shared.openURL(url)
		}
	}
}
