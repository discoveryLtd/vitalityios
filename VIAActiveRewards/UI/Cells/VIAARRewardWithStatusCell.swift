//
//  VIAARRewardWithStatusCell.swift
//  VitalityActive
//
//  Created by Joshua Ryan on 10/9/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

import VIAUIKit

public class VIAARRewardWithStatusCell: UITableViewCell, Nibloadable {
	
	// MARK: Private
	@IBOutlet weak var headerLabel: UILabel!
	@IBOutlet weak var rewardImageView: UIImageView!
	
	@IBOutlet weak var rewardHeaderLabel: UILabel!
	@IBOutlet weak var rewardContentLabel: UILabel!
	@IBOutlet weak var rewardExpirationLabel: UILabel!
	@IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var detailsLabel: UITextView!
	
	@IBOutlet weak var stackView: UIStackView!
    
	// MARK: Public
	public var header: String = "" {
		didSet {
			self.headerLabel?.text = self.header
		}
	}
	
	public var rewardImage: UIImage? {
		didSet {
			self.rewardImageView?.image = self.rewardImage
		}
	}
	
	public var rewardHeader: String = "" {
		didSet {
			self.rewardHeaderLabel?.text = self.rewardHeader
		}
	}
	
	public var rewardContent: String = "" {
		didSet {
			self.rewardContentLabel?.text = self.rewardContent
		}
	}
	
	public var rewardExpiration: String = "" {
		didSet {
			self.rewardExpirationLabel?.text = self.rewardExpiration
		}
	}
	
	public var status: String = "" {
		didSet {
			self.statusLabel.text = self.status
		}
	}
	
	public var details: String = "" {
		didSet {
			self.detailsLabel.text = self.details
		}
	}
    
    public var detailsLink: NSMutableAttributedString! {
        didSet {
            self.detailsLabel.attributedText = self.detailsLink
        }
    }
	
	public override func awakeFromNib() {
		super.awakeFromNib()
		setupView()
	}
	
	public func addAdditionalView(view: UIView) {
		self.stackView.addArrangedSubview(view)
	}
	
	func setupView() {
		
		// reset IB values
		self.headerLabel?.text = nil
		self.rewardImageView?.image = nil
		self.rewardHeaderLabel?.text = nil
		self.rewardContentLabel?.text = nil
		self.detailsLabel?.text = nil
		self.statusLabel?.text = nil
	}
}

