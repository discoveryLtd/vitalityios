// Generated using SwiftGen, by O.Halligon — https://github.com/SwiftGen/SwiftGen

#if os(OSX)
import AppKit.NSImage
public typealias VIAActiveRewardsColor = NSColor
public typealias VIAActiveRewardsImage = NSImage
#elseif os(iOS) || os(tvOS) || os(watchOS)
import UIKit.UIImage
public typealias VIAActiveRewardsColor = UIColor
public typealias VIAActiveRewardsImage = UIImage
#endif

// swiftlint:disable file_length

public typealias VIAActiveRewardsAssetType = VIAActiveRewardsImageAsset

public struct VIAActiveRewardsImageAsset: Equatable {
	fileprivate var name: String

	public var templateImage: VIAActiveRewardsImage {
		let bundle = Bundle(for: BundleToken.self)
		#if os(iOS) || os(tvOS)
		let image = VIAActiveRewardsImage(named: name, in: bundle, compatibleWith: nil)?.withRenderingMode(.alwaysTemplate)
		#elseif os(OSX)
		let image = bundle.image(forResource: name).withRenderingMode(.alwaysTemplate)
		#elseif os(watchOS)
		let image = VIAActiveRewardsImage(named: name).withRenderingMode(.alwaysTemplate)
		#endif
		guard let result = image else { fatalError("Unable to load template image named \(name).") }
		return result
	}

	public var image: VIAActiveRewardsImage {
		let bundle = Bundle(for: BundleToken.self)
		#if os(iOS) || os(tvOS)
		let image = VIAActiveRewardsImage(named: name, in: bundle, compatibleWith: nil)
		#elseif os(OSX)
		let image = bundle.image(forResource: name)
		#elseif os(watchOS)
		let image = VIAActiveRewardsImage(named: name)
		#endif
		guard let result = image else { fatalError("Unable to load image named \(name).") }
		return result
	}

	public static func ==(lhs: VIAActiveRewardsImageAsset, rhs: VIAActiveRewardsImageAsset) -> Bool {
        return lhs.image == rhs.image
    }
}

public struct VIAActiveRewardsColorAsset {
fileprivate var name: String

#if swift(>=3.2)
@available(iOS 11.0, tvOS 11.0, watchOS 4.0, OSX 10.13, *)
var color: VIAActiveRewardsColor {
return VIAActiveRewardsColor(asset: self)
}
#endif
}

// swiftlint:disable identifier_name line_length nesting type_body_length type_name
public enum VIAActiveRewardsAsset {
  public enum Goals {
    public static let statusInProgress = VIAActiveRewardsImageAsset(name: "statusInProgress")
    public static let statusPending = VIAActiveRewardsImageAsset(name: "statusPending")
    public static let statusNotAchieved = VIAActiveRewardsImageAsset(name: "statusNotAchieved")
    public static let arrowDrill = VIAActiveRewardsImageAsset(name: "arrowDrill")
    public static let statusAchieved = VIAActiveRewardsImageAsset(name: "statusAchieved")
  }
  public enum ActiveRewards {
    public static let activeRewardsTerms = VIAActiveRewardsImageAsset(name: "ActiveRewardsTerms")
    public enum Rewards {
      public static let noActiveReward = VIAActiveRewardsImageAsset(name: "noActiveReward")
      public static let partnersPlaceholder = VIAActiveRewardsImageAsset(name: "partnersPlaceholder")
      public static let rewardTrophyBig = VIAActiveRewardsImageAsset(name: "rewardTrophyBig")
      public static let activeReward = VIAActiveRewardsImageAsset(name: "activeReward")
      public enum SLIParners {
        public static let partnerLawson = VIAActiveRewardsImageAsset(name: "partnerLawson")
        public static let starbucks200X150 = VIAActiveRewardsImageAsset(name: "starbucks200X150")
        public static let starbucks = VIAActiveRewardsImageAsset(name: "starbucks")
      }
      public enum ECPartners {
        public static let juanValdezSmall = VIAActiveRewardsImageAsset(name: "juanValdezSmall")
        public static let cinemarkLarge = VIAActiveRewardsImageAsset(name: "cinemarkLarge")
        public static let juanValdezLarge = VIAActiveRewardsImageAsset(name: "juanValdezLarge")
        public static let cinemarkSmall = VIAActiveRewardsImageAsset(name: "cinemarkSmall")
      }
      public enum IGIPartners {
        public static let foodpandaSmall = VIAActiveRewardsImageAsset(name: "foodpandaSmall")
        public static let easyticketsLarge = VIAActiveRewardsImageAsset(name: "easyticketsLarge")
        public static let easyticketsSmall = VIAActiveRewardsImageAsset(name: "easyticketsSmall")
        public static let foodpandaLarge = VIAActiveRewardsImageAsset(name: "foodpandaLarge")
      }
      public enum UKEPartners {
        public static let allenCarrSmall = VIAActiveRewardsImageAsset(name: "allenCarrSmall")
        public static let cineworldVueSmall = VIAActiveRewardsImageAsset(name: "cineworldVueSmall")
        public static let vueSmall = VIAActiveRewardsImageAsset(name: "vueSmall")
        public static let weightWatchersSmall = VIAActiveRewardsImageAsset(name: "weightWatchersSmall")
        public static let amazonCoUk = VIAActiveRewardsImageAsset(name: "amazon_co_uk")
        public static let noRewardLarge = VIAActiveRewardsImageAsset(name: "noRewardLarge")
        public static let garminLarge = VIAActiveRewardsImageAsset(name: "garminLarge")
        public static let cineworldSmall = VIAActiveRewardsImageAsset(name: "cineworldSmall")
        public static let starbucksLarge = VIAActiveRewardsImageAsset(name: "starbucksLarge")
        public static let nuffieldHealthLarge = VIAActiveRewardsImageAsset(name: "nuffieldHealthLarge")
        public static let cafeCoffeeDayDefault = VIAActiveRewardsImageAsset(name: "cafeCoffeeDayDefault")
        public static let polarLarge = VIAActiveRewardsImageAsset(name: "polarLarge")
        public static let bookMyShowDefault = VIAActiveRewardsImageAsset(name: "bookMyShowDefault")
        public static let spotifyDefault = VIAActiveRewardsImageAsset(name: "spotifyDefault")
        public static let amazonDe = VIAActiveRewardsImageAsset(name: "amazon_de")
        public static let nuffieldSmall = VIAActiveRewardsImageAsset(name: "nuffieldSmall")
        public static let allenCarrLarge = VIAActiveRewardsImageAsset(name: "allenCarrLarge")
        public static let cineworldVueLarge = VIAActiveRewardsImageAsset(name: "cineworldVueLarge")
        public static let starbucksSmall = VIAActiveRewardsImageAsset(name: "starbucksSmall")
        public static let polarSmall = VIAActiveRewardsImageAsset(name: "polarSmall")
        public static let cineworldLarge = VIAActiveRewardsImageAsset(name: "cineworldLarge")
        public static let noRewardSmall = VIAActiveRewardsImageAsset(name: "noRewardSmall")
        public static let garminSmall = VIAActiveRewardsImageAsset(name: "garminSmall")
        public static let amazonUsa = VIAActiveRewardsImageAsset(name: "amazon_usa")
        public static let vueLarge = VIAActiveRewardsImageAsset(name: "vueLarge")
        public static let weightWatchersLarge = VIAActiveRewardsImageAsset(name: "weightWatchersLarge")
      }
      public static let partnerAmazon = VIAActiveRewardsImageAsset(name: "partnerAmazon")
      public static let partnerStarbucks = VIAActiveRewardsImageAsset(name: "partnerStarbucks")
      public static let partnerWholeFoods = VIAActiveRewardsImageAsset(name: "partnerWholeFoods")
      public enum MarkAsUsed {
        public static let markAsUsed = VIAActiveRewardsImageAsset(name: "markAsUsed")
      }
      public enum MarkAsUnused {
        public static let markAsUnused = VIAActiveRewardsImageAsset(name: "markAsUnused")
      }
      public enum Onboarding {
        public static let onboardingSpinner = VIAActiveRewardsImageAsset(name: "onboardingSpinner")
        public static let mask = VIAActiveRewardsImageAsset(name: "mask")
      }
    }
    public static let activeRewardsHelp = VIAActiveRewardsImageAsset(name: "ActiveRewardsHelp")
    public static let activeRewardsRewards2 = VIAActiveRewardsImageAsset(name: "ActiveRewardsRewards2")
    public static let activeRewardsLearnMore = VIAActiveRewardsImageAsset(name: "ActiveRewardsLearnMore")
    public enum LearnMore {
      public static let arLearnMoreReward = VIAActiveRewardsImageAsset(name: "ARLearnMoreReward")
      public static let arLearnMoreActivate = VIAActiveRewardsImageAsset(name: "ARLearnMoreActivate")
      public static let arLearnMoreTrophy = VIAActiveRewardsImageAsset(name: "ARLearnMoreTrophy")
    }
    public static let activeRewardsActivity = VIAActiveRewardsImageAsset(name: "ActiveRewardsActivity")
    public enum HomeScreen {
      public static let activeRewardsHomeScreenCard = VIAActiveRewardsImageAsset(name: "ActiveRewardsHomeScreenCard")
      public static let activeRewardsTrophy = VIAActiveRewardsImageAsset(name: "ActiveRewardsTrophy")
    }
    public static let arRangeCalendar = VIAActiveRewardsImageAsset(name: "ARRangeCalendar")
    public static let arvhrRequired = VIAActiveRewardsImageAsset(name: "ARVHRRequired")
    public static let activeRewardsRewards = VIAActiveRewardsImageAsset(name: "ActiveRewardsRewards")
    public static let arLandingCalendarBig = VIAActiveRewardsImageAsset(name: "ARLandingCalendarBig")
    public enum Onboarding {
      public static let arOnboardingSpinner = VIAActiveRewardsImageAsset(name: "AROnboardingSpinner")
      public static let arOnboardingCalendar = VIAActiveRewardsImageAsset(name: "AROnboardingCalendar")
      public static let arOnboardingRings = VIAActiveRewardsImageAsset(name: "AROnboardingRings")
      public static let arOnboardingReward = VIAActiveRewardsImageAsset(name: "AROnboardingReward")
      public static let arOnboardingTrophy = VIAActiveRewardsImageAsset(name: "AROnboardingTrophy")
      public static let arOnboardingActivated = VIAActiveRewardsImageAsset(name: "AROnboardingActivated")
    }
  }
  public static let achievedTrophy = VIAActiveRewardsImageAsset(name: "achievedTrophy")

  // swiftlint:disable trailing_comma
  public static let allColors: [VIAActiveRewardsColorAsset] = [
  ]
  public static let allImages: [VIAActiveRewardsImageAsset] = [
    Goals.statusInProgress,
    Goals.statusPending,
    Goals.statusNotAchieved,
    Goals.arrowDrill,
    Goals.statusAchieved,
    ActiveRewards.activeRewardsTerms,
    ActiveRewards.Rewards.noActiveReward,
    ActiveRewards.Rewards.partnersPlaceholder,
    ActiveRewards.Rewards.rewardTrophyBig,
    ActiveRewards.Rewards.activeReward,
    ActiveRewards.Rewards.SLIParners.partnerLawson,
    ActiveRewards.Rewards.SLIParners.starbucks200X150,
    ActiveRewards.Rewards.SLIParners.starbucks,
    ActiveRewards.Rewards.ECPartners.juanValdezSmall,
    ActiveRewards.Rewards.ECPartners.cinemarkLarge,
    ActiveRewards.Rewards.ECPartners.juanValdezLarge,
    ActiveRewards.Rewards.ECPartners.cinemarkSmall,
    ActiveRewards.Rewards.IGIPartners.foodpandaSmall,
    ActiveRewards.Rewards.IGIPartners.easyticketsLarge,
    ActiveRewards.Rewards.IGIPartners.easyticketsSmall,
    ActiveRewards.Rewards.IGIPartners.foodpandaLarge,
    ActiveRewards.Rewards.UKEPartners.allenCarrSmall,
    ActiveRewards.Rewards.UKEPartners.cineworldVueSmall,
    ActiveRewards.Rewards.UKEPartners.vueSmall,
    ActiveRewards.Rewards.UKEPartners.weightWatchersSmall,
    ActiveRewards.Rewards.UKEPartners.amazonCoUk,
    ActiveRewards.Rewards.UKEPartners.noRewardLarge,
    ActiveRewards.Rewards.UKEPartners.garminLarge,
    ActiveRewards.Rewards.UKEPartners.cineworldSmall,
    ActiveRewards.Rewards.UKEPartners.starbucksLarge,
    ActiveRewards.Rewards.UKEPartners.nuffieldHealthLarge,
    ActiveRewards.Rewards.UKEPartners.cafeCoffeeDayDefault,
    ActiveRewards.Rewards.UKEPartners.polarLarge,
    ActiveRewards.Rewards.UKEPartners.bookMyShowDefault,
    ActiveRewards.Rewards.UKEPartners.spotifyDefault,
    ActiveRewards.Rewards.UKEPartners.amazonDe,
    ActiveRewards.Rewards.UKEPartners.nuffieldSmall,
    ActiveRewards.Rewards.UKEPartners.allenCarrLarge,
    ActiveRewards.Rewards.UKEPartners.cineworldVueLarge,
    ActiveRewards.Rewards.UKEPartners.starbucksSmall,
    ActiveRewards.Rewards.UKEPartners.polarSmall,
    ActiveRewards.Rewards.UKEPartners.cineworldLarge,
    ActiveRewards.Rewards.UKEPartners.noRewardSmall,
    ActiveRewards.Rewards.UKEPartners.garminSmall,
    ActiveRewards.Rewards.UKEPartners.amazonUsa,
    ActiveRewards.Rewards.UKEPartners.vueLarge,
    ActiveRewards.Rewards.UKEPartners.weightWatchersLarge,
    ActiveRewards.Rewards.partnerAmazon,
    ActiveRewards.Rewards.partnerStarbucks,
    ActiveRewards.Rewards.partnerWholeFoods,
    ActiveRewards.Rewards.MarkAsUsed.markAsUsed,
    ActiveRewards.Rewards.MarkAsUnused.markAsUnused,
    ActiveRewards.Rewards.Onboarding.onboardingSpinner,
    ActiveRewards.Rewards.Onboarding.mask,
    ActiveRewards.activeRewardsHelp,
    ActiveRewards.activeRewardsRewards2,
    ActiveRewards.activeRewardsLearnMore,
    ActiveRewards.LearnMore.arLearnMoreReward,
    ActiveRewards.LearnMore.arLearnMoreActivate,
    ActiveRewards.LearnMore.arLearnMoreTrophy,
    ActiveRewards.activeRewardsActivity,
    ActiveRewards.HomeScreen.activeRewardsHomeScreenCard,
    ActiveRewards.HomeScreen.activeRewardsTrophy,
    ActiveRewards.arRangeCalendar,
    ActiveRewards.arvhrRequired,
    ActiveRewards.activeRewardsRewards,
    ActiveRewards.arLandingCalendarBig,
    ActiveRewards.Onboarding.arOnboardingSpinner,
    ActiveRewards.Onboarding.arOnboardingCalendar,
    ActiveRewards.Onboarding.arOnboardingRings,
    ActiveRewards.Onboarding.arOnboardingReward,
    ActiveRewards.Onboarding.arOnboardingTrophy,
    ActiveRewards.Onboarding.arOnboardingActivated,
    achievedTrophy,
  ]
  // swiftlint:enable trailing_comma
  @available(*, deprecated, renamed: "allImages")
  public static let allValues: [VIAActiveRewardsAssetType] = allImages
}
// swiftlint:enable identifier_name line_length nesting type_body_length type_name

extension VIAActiveRewardsImage {
@available(iOS 1.0, tvOS 1.0, watchOS 1.0, *)
@available(OSX, deprecated,
message: "This initializer is unsafe on macOS, please use the VIAActiveRewardsImageAsset.image property")
convenience init!(asset: VIAActiveRewardsAssetType) {
#if os(iOS) || os(tvOS)
let bundle = Bundle(for: BundleToken.self)
self.init(named: asset.name, in: bundle, compatibleWith: nil)
#elseif os(OSX) || os(watchOS)
self.init(named: asset.name)
#endif
}
}

extension VIAActiveRewardsColor {
#if swift(>=3.2)
@available(iOS 11.0, tvOS 11.0, watchOS 4.0, OSX 10.13, *)
convenience init!(asset: VIAActiveRewardsColorAsset) {
let bundle = Bundle(for: BundleToken.self)
#if os(iOS) || os(tvOS)
self.init(named: asset.name, in: bundle, compatibleWith: nil)
#elseif os(OSX)
self.init(named: asset.name, bundle: bundle)
#elseif os(watchOS)
self.init(named: asset.name)
#endif
}
#endif
}

private final class BundleToken {}
