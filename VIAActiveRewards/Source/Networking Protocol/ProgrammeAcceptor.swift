import Foundation
import VitalityKit
import VIAUIKit

protocol ProgrammeAcceptor {
    func activateActiveRewardsProgramme(completion: @escaping ((Error?) -> Void))
}

extension ProgrammeAcceptor {
    func activateActiveRewardsProgramme(completion: @escaping ((Error?) -> Void)) {
        let coreRealm = DataProvider.newRealm()
        let partyId = coreRealm.getPartyId()
        let tenantId = coreRealm.getTenantId()
        
        let request = ActivateActiveRewardsRequest()
        Wire.Rewards.activate(tenantId: tenantId, partyId: partyId, request: request, completion: { error in
            guard error == nil else {
                completion(error)
                return
            }
            
            completion(nil)
        })
    }
}
