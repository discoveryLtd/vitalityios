import Foundation
import VitalityKit
import VIAUtilities
import RealmSwift
import VIACommon

protocol ActiveRewardsLandingViewModel: WDAFetchDevicesManager {
    var goalForCurrentCycle: ARGoalTracker? { get }
    func potentialPointsEntryCount() -> Int
    func currentGoalExists() -> Bool
    func futureCycleStartDate() -> Date?
    func cycleEndDate() -> Date?
    func requestGoalProgressAndDetail(completion: @escaping (Error?) -> Void)
    func thisWeeksPotentialPointsEntries(for index: Int) -> ARObjectivePointsEntry?
    func clearExisitingGoalHistoryData()
}

class VIAActiveRewardsLandingViewModel: ActiveRewardsLandingViewModel {
    let arRealm = DataProvider.newARRealm()
    internal var goalForCurrentCycle: ARGoalTracker? {
        let goalTrackers = arRealm.firstGoalTrackersForThisCycle(with: Date())
        return goalTrackers
    }

    internal func currentGoalExists() -> Bool {
        return arRealm.currentGoalExists(for: Date())
    }

    internal func futureCycleStartDate() -> Date? {
        return arRealm.firstFutureGoalTracker(after: Date())?.effectiveFrom
    }

    internal func cycleEndDate() -> Date? {
        return goalForCurrentCycle?.effectiveTo
    }
    
    internal func potentialPointsEntriesForCurrentCycle() -> List<ARObjectivePointsEntry>? {
        guard let objectiveTrackers =  goalForCurrentCycle?.objectiveTrackers.sorted(byKeyPath: "effectiveFrom", ascending: false) else {
            return nil
        }
        let pointsEntries = List<ARObjectivePointsEntry>()
        
        for objectiveTracker in objectiveTrackers {
            let potentialPoints = objectiveTracker.objectivePointsEntries.sorted(byKeyPath: "effectiveDate", ascending: false)
            for pointsEntry in potentialPoints {
                pointsEntries.append(pointsEntry)
            }
        }
        return pointsEntries
    }

    internal func potentialPointsEntryCount() -> Int {
        return (potentialPointsEntriesForCurrentCycle()?.count) ?? 0
    }
    
    func clearExisitingGoalHistoryData() {
        let arRealm = DataProvider.newARRealm()
        arRealm.deleteOldARGoalProgressAndDetailsData()
    }

    //MARK: Networking
    //MARK: Goal Progress
    internal func requestGoalProgressAndDetail(completion: @escaping (Error?) -> Void) {
        let requestParameters = configureGoalProgressAndDetailsRequestParametersForCurrentTwoWeeks()
        let tenantId = DataProvider.newRealm().getTenantId()
        Wire.Events.getGoalProgressAndDetails(tenantId: tenantId, request: requestParameters) { error, response in
            
            guard error == nil else {
                completion(error)
                return
            }
            completion(nil)
        }
    }

    internal func thisWeeksPotentialPointsEntries(for index: Int) -> ARObjectivePointsEntry? {
        guard potentialPointsEntryCount() > index else {
            return nil
        }
        return potentialPointsEntriesForCurrentCycle()?[index]
    }
    
    internal func configureGoalProgressAndDetailsRequestParametersWithRequiredFields() -> GetGoalProgressAndDetailsParameters {
        let partyId = DataProvider.newRealm().getPartyId()
        let goalKeys = [GoalRef.ActiveRewards.rawValue]

        let getGoalProgressAndDetailsParameters = GetGoalProgressAndDetailsParameters(effectiveDateFrom: nil, effectiveDateTo: nil, goalKeys: goalKeys, goalStatusTypeKeys: nil, partyId: partyId)

        return getGoalProgressAndDetailsParameters
    }
    
    internal func configureGoalProgressAndDetailsRequestParametersForCurrentTwoWeeks() -> GetGoalProgressAndDetailsParameters {
        let currentDate = Date()
        var requestParameters = configureGoalProgressAndDetailsRequestParametersWithRequiredFields()
        
        var mondaysDate: Date {
            return Calendar(identifier: .iso8601).date(from: Calendar(identifier: .iso8601).dateComponents([.yearForWeekOfYear, .weekOfYear], from: Date()))!
        }
        
        var nextSundaysDate: Date {
            let calendar = Calendar(identifier: .iso8601)
            var dateComponent = DateComponents()
            dateComponent.day = 13
            return calendar.date(byAdding: dateComponent, to: mondaysDate)!
        }
        
        requestParameters.effectiveDateFrom = mondaysDate
        requestParameters.effectiveDateTo = nextSundaysDate

        return requestParameters
    }
}
