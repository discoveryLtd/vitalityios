import Foundation
import VIAUIKit
import VitalityKit
import VIAUtilities
import VIACommon
import VitalityKit
import SwiftDate

class VIAARLandingViewController: VIATableViewController, ImproveYourHealthTintable {
    private var wdaViewModel: WellnessDevicesContentLookup?
    private var graphCell:VIAGraphTableViewCell!
    private static let noActivityCellIdentifier = "noActivityCellIdentifier"
    private var viewModel: ActiveRewardsLandingViewModel = VIAActiveRewardsLandingViewModel()
    
    // MARK:

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        wdaViewModel = WellnessDevicesContentLookupViewModel()
        navigationItem.setTitle(title: CommonStrings.Ar.Landing.HomeViewTitle710, subtitle: CommonStrings.Ar.Landing.SubheaderWeeklyTarget780)
        viewModel.clearExisitingGoalHistoryData()
        loadData()
        configureTableView()
    }

    override func viewWillAppear(_ animated: Bool) {
        GlobalUtil.helpSourceVC = 0 // AR
        super.viewWillAppear(animated)
        resetBackButtonTitle()
        configureAppearance()
        self.tableView.reloadData()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        hideBackButtonTitle()
    }

    func configureAppearance() {
        setNeedsStatusBarAppearanceUpdate()
        navigationController?.makeNavigationBarTransparent()
    }

    func configureTableView() {        
        tableView.register(VIATableBadgeViewCell.nib(), forCellReuseIdentifier: VIATableBadgeViewCell.defaultReuseIdentifier)
        tableView.register(VIAARPointsEventCell.nib(), forCellReuseIdentifier: VIAARPointsEventCell.defaultReuseIdentifier)
        tableView.register(VIATableViewCell.self, forCellReuseIdentifier: VIATableViewCell.defaultReuseIdentifier)
        tableView.register(VIAGraphTableViewCell.nib(), forCellReuseIdentifier: VIAGraphTableViewCell.defaultReuseIdentifier)
        tableView.register(VIAARLandingFirstGoalTableViewCell.nib(), forCellReuseIdentifier: VIAARLandingFirstGoalTableViewCell.defaultReuseIdentifier)
        tableView.register(VIATableViewCell.self, forCellReuseIdentifier: VIAARLandingViewController.noActivityCellIdentifier)
        tableView.estimatedRowHeight = 75
        tableView.register(VIATableViewSectionHeaderFooterView.nib(), forHeaderFooterViewReuseIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier)
    }
    
    func handleErrorOccurred(_ error: Error?) {
        let backendError = error as? BackendError ?? BackendError.other
        let statusView = handleBackendErrorWithStatusView(backendError, tryAgainAction: { [weak self] in
            self?.loadData()
        })
        configureStatusView(statusView)
    }

    // MARK: UITableView data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return Sections.visible.count
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let section = Sections.visible(with: section)
        if section == .firstGoal {
            return 1
        } else if section == .thisWeeksActivity {
            if viewModel.futureCycleStartDate() == nil && !viewModel.currentGoalExists() {
                return 0
            }
            return max(viewModel.potentialPointsEntryCount(), 1)
        } else if section == .menuItems {
            if let hideHelpTab = VIAApplicableFeatures.default.hideHelpTab,
                hideHelpTab{
                return MenuItems.visible.count-1
            } else{
                return MenuItems.visible.count
            }
        }
        return 0
    }

    // MARK: UITableView delegate

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let section = Sections.visible(with: indexPath.section)
        if section == .firstGoal {
            if viewModel.futureCycleStartDate() != nil && !viewModel.currentGoalExists() {
                return configureFirstGoalCell(indexPath)
            } else {
                return configureGoalGraphCell(indexPath)
            }
        } else if section == .thisWeeksActivity {
            return configureActivityCell(indexPath)
        } else if section == .menuItems {
            return configureMenuItemCell(indexPath)
        }
        return tableView.dequeueReusableCell(withIdentifier: VIATableViewCell.defaultReuseIdentifier, for: indexPath)
    }

    func configureFirstGoalCell(_ indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: VIAARLandingFirstGoalTableViewCell.defaultReuseIdentifier, for: indexPath)
        cell.selectionStyle = .none
        if let firstGoalCell = cell as? VIAARLandingFirstGoalTableViewCell {
            if let cycleStartDate = viewModel.futureCycleStartDate() {
                let cycleStartDateString = Localization.fullDateFormatter.string(from: cycleStartDate)
                firstGoalCell.headingLabel.text = CommonStrings.Ar.Landing.FirstGoalCellTitle677(cycleStartDateString)
            }
            firstGoalCell.messageLabel.text = CommonStrings.Ar.Landing.FirstGoalCellMessage659
            firstGoalCell.cellImageView.image = UIImage(asset: VIAActiveRewardsAsset.ActiveRewards.arLandingCalendarBig)
            return firstGoalCell
        }
        return cell
    }
    
    func configureGoalGraphCell(_ indexPath: IndexPath) -> UITableViewCell {
        if graphCell == nil {
            graphCell = tableView.dequeueReusableCell(withIdentifier: VIAGraphTableViewCell.defaultReuseIdentifier, for: indexPath) as! VIAGraphTableViewCell
            graphCell.selectionStyle = .none
            graphCell.isUserInteractionEnabled = false
            graphCell.dateImageView.contentMode = UIViewContentMode.scaleAspectFit
            
            if graphCell.graph == nil {
                let graphClass = VIAGraphView()
                let graph = VIAGraphView.viewFromNib(owner: graphClass)!
                graphCell.graph = graph
                graphCell.graphView.addSubview(graph)
                graphCell.graph?.snp.makeConstraints { (make) in
                    make.centerX.equalTo(graphCell.graphView.snp.centerX)
                    make.centerY.equalTo(graphCell.graphView.snp.centerY)
                    make.width.equalTo(graphCell.graphView.snp.width)
                    make.height.equalTo(graphCell.graphView.snp.height)
                }
            }
            
            graphCell.graphHeight.constant = view.frame.width*0.7
            graphCell.graphWidth.constant = view.frame.width*0.7
            let ringWidth = view.frame.width*0.06
            graphCell.graph?.ringWidth = ringWidth
        }

        let currentGoalForCycle = viewModel.goalForCurrentCycle
        if currentGoalForCycle?.goalTrackerStatus == .Achieved {
            graphCell.dateImageView.isHidden = true
            graphCell.dateLabel.text = CommonStrings.Ar.LandingAchievedTitle768
        } else {
            graphCell.dateImageView.image = UIImage(asset: VIAActiveRewardsAsset.ActiveRewards.arRangeCalendar)
            if let effectiveFromDate = currentGoalForCycle?.effectiveFrom, let effectiveToDate = currentGoalForCycle?.effectiveTo {
                let startDate = Localization.mediumDateFormatter.string(from: effectiveFromDate)
                let endDate = Localization.mediumDateFormatter.string(from: effectiveToDate)
                graphCell.dateLabel.text = startDate+" - "+endDate
            }
        }
        
        let pointsAcheived = currentGoalForCycle?.totalPointsAcheived() ?? 0
        let pointTarget = currentGoalForCycle?.totalPotentialPoints() ?? 0

        let model = VIAGraphViewModel(total: pointTarget, startingValue: graphCell.graph?.model?.EndingValue ?? 0, endingValue: pointsAcheived)
        graphCell.graph?.progressLabel.text = CommonStrings.Ar.GoalProgressMessage647(String(describing: pointTarget))
        
        
        graphCell.graph?.model = model
        
        return graphCell
    }

    func configureActivityCell(_ indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: VIAARPointsEventCell.defaultReuseIdentifier, for: indexPath)
        cell.isUserInteractionEnabled = true
        if viewModel.potentialPointsEntryCount() == 0 {
            let noActivityCell = tableView.dequeueReusableCell(withIdentifier: VIAARLandingViewController.noActivityCellIdentifier, for: indexPath)
            if let noActivityCell = noActivityCell as? VIATableViewCell {
                noActivityCell.cellImage = nil
                noActivityCell.accessoryType = .none
                noActivityCell.textLabel?.text = CommonStrings.Ar.Landing.NoActivityCellTitle678
                noActivityCell.isUserInteractionEnabled = false
                return noActivityCell
            }
            return noActivityCell
        }
        guard let potentialPointsEntry = viewModel.thisWeeksPotentialPointsEntries(for: indexPath.row) else {
            return cell
        }
        guard let pointsEventCell = cell as? VIAARPointsEventCell else {
            return cell
        }
        pointsEventCell.arrowImageView.image = UIImage(asset: VIAActiveRewardsAsset.Goals.arrowDrill)
        let potentialPoints = potentialPointsEntry.pointsContributed
        pointsEventCell.pointsText = String(describing: potentialPoints)
        
        pointsEventCell.descriptionText = potentialPointsEntry.typeName
        pointsEventCell.dateText = Localization.mediumDateFormatter.string(from: potentialPointsEntry.effectiveDate)
        pointsEventCell.deviceText = potentialPointsEntry.manufacturerName()
        return cell
    }

    func configureMenuItemCell(_ indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: VIATableBadgeViewCell.defaultReuseIdentifier, for: indexPath) as! VIATableBadgeViewCell
        let menuItem = MenuItems.visible[indexPath.row]
        cell.textLabel?.text = menuItem.title()
        cell.imageView?.image = menuItem.image()
        cell.isUserInteractionEnabled = true
        cell.accessoryType = .disclosureIndicator
        
        cell.badgeLabelText = nil
        if menuItem == MenuItems.rewards{
            //let spinCount = DataProvider.newRealm().availableForSpinCount()
            let spinCount = DataProvider.newARRealm().allUnclaimedRewards().filter("awardedRewardStatus != %@", AwardedRewardStatusRef.AvailableToRedeem.rawValue).count
            if spinCount > 0 {
                cell.badgeLabelText = "\(spinCount)"
            } else {
                cell.badgeLabelText = nil
            }
        }
        return cell
    }

    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier) as! VIATableViewSectionHeaderFooterView
        if Sections.visible(with: section) == .thisWeeksActivity {
            if viewModel.futureCycleStartDate() == nil && !viewModel.currentGoalExists() {
                return nil
            }
            view.labelText = CommonStrings.Ar.Landing.ThisWeeksActivitySectionHeaderTitle660
        }
        return view
    }

    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return Sections.heightForHeader(section: section)
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let section = Sections.visible(with: indexPath.section)
        if section == .thisWeeksActivity {
            showPointsEventDetail(indexPath)
        } else if section == .menuItems {
            showMenuItem(indexPath)
        }
    }

    func showPointsEventDetail(_ indexPath: IndexPath) {
        performSegue(withIdentifier: "showPointsEventDetail", sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showPointsEventDetail" {
            if let indexPath = tableView.indexPathForSelectedRow, indexPath.section > 0 {
                let destinationViewController = segue.destination as? VIAARPointsEventDetailViewController
                let selectedEventId = viewModel.thisWeeksPotentialPointsEntries(for: indexPath.row)?.id
                destinationViewController?.viewModel.selectedEventId = selectedEventId
            }
        }
    }

    func showMenuItem(_ indexPath: IndexPath) {
        let menuItem = MenuItems.visible[indexPath.row]
        let segueIdentifier = menuItem.segueIdentifier()
        performSegue(withIdentifier: segueIdentifier, sender: nil)
    }

    //MARK: Networking
    func loadData() {
        if (viewModel.futureCycleStartDate() == nil && !viewModel.currentGoalExists()) {
            self.showHUDOnView(view: self.view)
        }
        self.viewModel.requestGoalProgressAndDetail { [weak self] error in
            self?.hideHUDFromView(view: self?.view)
            self?.getAwardedRewardByPartyIdCount()
            guard error == nil else {
                self?.hideHUDFromView(view: self?.view)
                self?.handleErrorOccurred(error);
                return
            }
            self?.removeStatusView()
            self?.tableView.reloadData()
        }
        fetchWellnessDevices()
    }
    
    func fetchWellnessDevices() {
        viewModel.fetchDevices(completion: { [weak self] (error, isRealmEmpty) in
            if let error = error {
                if isRealmEmpty == false {
                    return
                }
                self?.handleBackendErrorWithAlert(error as? BackendError ?? .other, tryAgainAction: (self?.fetchWellnessDevices) ?? {})
                return
            }
            if self?.wdaViewModel?.noLinkedDevices() ?? false {
                let alert = UIAlertController(title: CommonStrings.Ar.Landing.LinkDeviceDialogTitle766 , message: CommonStrings.Ar.Landing.LinkDeviceDialogMessage767, preferredStyle: UIAlertControllerStyle.alert)
                let cancelAction = UIAlertAction(title: CommonStrings.CancelButtonTitle24, style: UIAlertActionStyle.cancel, handler: { (action) in
                    alert.dismiss(animated: true, completion: nil)
                })
                alert.addAction(cancelAction)
                let linkNow = UIAlertAction(title: CommonStrings.Ar.AlertButtonTitleLinkNow779, style: UIAlertActionStyle.default, handler: { (action) in
                    self?.performSegue(withIdentifier: "showLinkWellnessDevice", sender: nil)
                })
                alert.addAction(linkNow)
                self?.present(alert, animated: true, completion: nil)
            }
        })
        
    }
    func getAwardedRewardByPartyIdCount() {
        self.showHUDOnView(view: self.view)
        let tenantId = DataProvider.newRealm().getTenantId()
        let partyId = DataProvider.newRealm().getPartyId()
        let from = Date()
        let to = Date()
        Wire.Rewards.getAwardedRewardByPartyId(tenantId: tenantId, partyId: partyId, fromDate: from, toDate: to, completion: { [weak self] rewardsRequestError, response in
            self?.hideHUDFromView(view: self?.view)
            guard rewardsRequestError == nil else {
                if let backendError = rewardsRequestError as? BackendError {
                    self?.handleBackendErrorWithAlert(backendError, tryAgainAction: { [weak self] in
                        self?.loadData()
                    })
                }
                return
            }
            self?.tableView.reloadData()
        })
    }
}

extension VIAARLandingViewController {
    enum Sections: Int, EnumCollection {
        case firstGoal = 0
        case thisWeeksActivity = 1
        case menuItems = 2

        static func heightForHeader(section: Int) -> CGFloat {
            if Sections(rawValue: section) == .firstGoal {
                return CGFloat.leastNormalMagnitude
            }
            return UITableViewAutomaticDimension
        }
        
        static var visible: [Sections] {
            let arRealm = DataProvider.newARRealm()
            let weekActivitiesHidden = !(arRealm.currentGoalExists(for: Date()))
            if weekActivitiesHidden, let indexOfActivities = Sections.allValues.index(of: .thisWeeksActivity) {
                var allSections = Sections.allValues
                _ = allSections.remove(at: indexOfActivities)
                return allSections
            }
            return Sections.allValues
        }
        
        static func visible(with rawValue: Int) -> Sections {
            let arRealm = DataProvider.newARRealm()
            let weekActivitiesHidden = !(arRealm.currentGoalExists(for: Date()))
            if rawValue == Sections.thisWeeksActivity.rawValue && weekActivitiesHidden {
                return Sections.menuItems
            }
            return Sections(rawValue: rawValue)!
        }
    }

    enum MenuItems: Int, EnumCollection {
        case activity = 0
        case rewards = 1
        case learnMore = 2
        case help = 3

        func title() -> String? {
            switch self {
            case .activity:
                return CommonStrings.Ar.Landing.ActivityCellTitle711
            case .rewards:
                return CommonStrings.Ar.Landing.RewardsCellTitle695
            case .learnMore:
                return CommonStrings.LearnMoreButtonTitle104
            case .help:
                return CommonStrings.HelpButtonTitle141
            }
        }

        func image() -> UIImage? {
            switch self {
            case .activity:
                return UIImage(asset: VIAActiveRewardsAsset.ActiveRewards.activeRewardsActivity)
            case .rewards:
                return UIImage(asset: VIAActiveRewardsAsset.ActiveRewards.activeRewardsRewards)
            case .learnMore:
                return UIImage(asset: VIAActiveRewardsAsset.ActiveRewards.activeRewardsLearnMore)
            case .help:
                return UIImage(asset: VIAActiveRewardsAsset.ActiveRewards.activeRewardsHelp)
            }
        }
        
        static var visible: [MenuItems] {
            var items = MenuItems.allValues
            if !isRewardsVisible() {
                items.remove(object: .rewards)
            }
            return items
        }
        
        static func isRewardsVisible() -> Bool {
            if VitalityProductFeature.isEnabled(.ARChoice) || VitalityProductFeature.isEnabled(.ARProbabilistic) {
                return true
            }
            
            return false
        }

        func segueIdentifier() -> String {
            switch self {
            case .activity:
                return "showActivityHistory"
            case .rewards:
                return "showRewards"
            case .learnMore:
                return  "showLearnMore"
            case .help:
                return  "showHelp"
            }
        }
        
    }
}
