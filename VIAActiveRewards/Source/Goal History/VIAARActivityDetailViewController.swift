import Foundation
import VIAUIKit
import VitalityKit

class VIAARActivityDetailViewController: VIATableViewController, ImproveYourHealthTintable {
    
    let viewModel = VIAARActivityDetailViewModel()
    
    // MARK: View lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.configureSelectedGoalItemData()
        title = viewModel.selectedGoalItemDateRangeScreenTitle()
        configureAppearance()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        configureTableView()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        hideBackButtonTitle()
    }
    
    // MARK: Configuration
    
    func configureAppearance() {
        navigationController?.makeNavigationBarTransparent()
    }
    
    func configureTableView() {
        tableView.register(VIAARGoalCycleSummaryCell.nib(), forCellReuseIdentifier: VIAARGoalCycleSummaryCell.defaultReuseIdentifier)
        tableView.register(VIALabelTableViewCell.nib(), forCellReuseIdentifier: VIALabelTableViewCell.defaultReuseIdentifier)
        tableView.register(VIAARPointsEventCell.nib(), forCellReuseIdentifier: VIAARPointsEventCell.defaultReuseIdentifier)
        tableView.register(VIATableViewSectionHeaderFooterView.nib(), forHeaderFooterViewReuseIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier)
        tableView.estimatedRowHeight = 75
        tableView.sectionHeaderHeight = 40
        tableView.sectionFooterHeight = 25
        tableView.estimatedSectionHeaderHeight = 40
        tableView.estimatedSectionFooterHeight = 25
    }
    
    
    // MARK: UITableView datasource
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.sectionCount()
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfItems(in: section)
    }
    
    // MARK: UITableView delegate
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let theSection = viewModel.section(at: indexPath.section) else { return tableView.defaultTableViewCell() }
        
        switch theSection {
        case .goalStatus:
            if indexPath.row == 0 {
                return configureGoalCycleSummaryCell(indexPath)
            } else {
                return configureDescriptionCell(indexPath)
            }
        case .events:
            if viewModel.selectedGoalItemHasEvents() {
                return configurePointsEventCell(indexPath)
            } else {
                return configureNoActivityCell(indexPath)
            }
        }
        
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        guard let theSection = viewModel.section(at: section) else { return CGFloat.leastNormalMagnitude }
        
        if theSection == .goalStatus || theSection == .events {
            return UITableViewAutomaticDimension
        }
        
        return CGFloat.leastNormalMagnitude
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        guard let theSection = viewModel.section(at: section) else { return CGFloat.leastNormalMagnitude }
        
        if theSection == .goalStatus || theSection == .events {
            return UITableViewAutomaticDimension
        }
        
        return CGFloat.leastNormalMagnitude
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let theSection = viewModel.section(at: section) else { return nil }
        
        if theSection == .events {
            guard let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier) as? VIATableViewSectionHeaderFooterView else { return tableView.defaultTableViewHeaderFooterView() }
            view.labelText = theSection.sectionHeaderTitle()
            view.set(textColor: .mediumGrey())
            
            return view
        }
        
        return nil
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        guard let theSection = viewModel.section(at: section) else { return nil }
        
        if theSection == .events {
            guard let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier) as? VIATableViewSectionHeaderFooterView else { return tableView.defaultTableViewHeaderFooterView() }
            view.labelText = theSection.sectionFooterText()
            view.set(textColor: .mediumGrey())
            
            return view
        }
        
        return nil
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let theSection = viewModel.section(at: indexPath.section) else { return }
        
        if theSection == .events {
            showPointsEventDetail()
        }
    }
    
    // MARK: Cell configuration
    
    func configureGoalCycleSummaryCell(_ indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: VIAARGoalCycleSummaryCell.defaultReuseIdentifier, for: indexPath) as? VIAARGoalCycleSummaryCell else { return tableView.defaultTableViewCell() }
        cell.selectionStyle = .none
        
        cell.pointsText = viewModel.selectedGoalItemPointsMessage()
        cell.statusText = viewModel.selectedGoalItemStatusTitle()
        cell.statusImage = viewModel.selectedGoalItemStatusIcon()
        cell.hideDateRange()
        cell.hideArrowIndicator()
        
        return cell
    }
    
    func configureDescriptionCell(_ indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: VIALabelTableViewCell.defaultReuseIdentifier, for: indexPath) as? VIALabelTableViewCell else { return tableView.defaultTableViewCell() }
        cell.configure(font: .bodyFont(), color: .night())
        cell.labelText = viewModel.selectedGoalItemStatusDescription()
        cell.isUserInteractionEnabled = false
        return cell
    }
    
    func configurePointsEventCell(_ indexPath: IndexPath) -> UITableViewCell {
       guard let cell = tableView.dequeueReusableCell(withIdentifier: VIAARPointsEventCell.defaultReuseIdentifier, for: indexPath) as? VIAARPointsEventCell else { return tableView.defaultTableViewCell() }
        cell.pointsText = viewModel.pointsForEvent(at: indexPath.row)
        cell.descriptionText = viewModel.descriptionForEvent(at: indexPath.row)
        cell.dateText = viewModel.dateEarnedForEvent(at: indexPath.row)
        cell.deviceText = viewModel.deviceForEvent(at: indexPath.row)
        return cell
    }
    
    func configureNoActivityCell(_ indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: VIALabelTableViewCell.defaultReuseIdentifier, for: indexPath) as? VIALabelTableViewCell else { return tableView.defaultTableViewCell() }
        cell.configure(font: .bodyFont(), color: .night())
        cell.labelText = CommonStrings.Ar.History.NoActivityHeadingTitle694
        cell.isUserInteractionEnabled = false
        return cell
    }
    
    // MARK: Actions
    
    func showPointsEventDetail() {
        performSegue(withIdentifier: "showPointsEventDetail", sender: nil)
    }
    
    // MARK: Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "showPointsEventDetail" {
            guard let indexPath = tableView.indexPathForSelectedRow else { return }
            guard let theSection = viewModel.section(at: indexPath.section) else { return }
            
            if theSection == .events {
                let destinationViewController = segue.destination as? VIAARPointsEventDetailViewController
                let selectedEventId = viewModel.event(at: indexPath.row)?.id
                destinationViewController?.viewModel.selectedEventId = selectedEventId
            }
        }
    }
    
}
