import Foundation
import VitalityKit
import VIAUtilities
import VitalityKit
import VIACommon

public struct EventDetailItem {
    var id: Int
    var deviceName: String?
    var earnedPoints: Int
    var earnedDate: Date
    var metadataItems: [MetadatalItem]
    
    func formattedEarnedPoints() -> String? {
        return Localization.integerFormatter.string(from: earnedPoints as NSNumber)
    }
    
    func metadataItem(at index: Int) -> MetadatalItem? {
        guard let metadataItem = metadataItems.element(at: index) else { return nil }
        return metadataItem
    }
}

struct MetadatalItem {
    var title: String
    var description: String
    var typeCode: String
}

class VIAARPointsEventDetailViewModel {
    
    //MARK: Properties
    
    var selectedEventDetailItem: EventDetailItem?
    
    var visibleSections = [Sections]()
    
    var selectedEventId: Int?
    
    let dateParser = DateParser()
    
    // MARK: Configuration
    
    func configureSelectedEventData() {
        guard let selectedId = selectedEventId else { return }
        let arRealm = DataProvider.newARRealm()
        arRealm.refresh()
        guard let selectedPointsEntry = arRealm.arObjectivePointsEntry(with: selectedId) else { return }
        
        let id = selectedPointsEntry.id
        let earnedPoints = selectedPointsEntry.pointsContributed
        let earnedDate = selectedPointsEntry.effectiveDate
        let deviceName = selectedPointsEntry.manufacturerName()
        let metadataItems = configureMetadata(for: selectedPointsEntry)
        
        selectedEventDetailItem = EventDetailItem(id: id, deviceName: deviceName, earnedPoints: earnedPoints, earnedDate: earnedDate, metadataItems: metadataItems)
        
        configureSections()
    }
    
    func configureSections() {
        guard let eventDetail = selectedEventDetailItem else { return }
        visibleSections = Sections.allValues
        guard eventDetail.deviceName != nil else {
            visibleSections.remove(object: Sections.device)
            return
        }
    }
    
    internal func configureMetadata(for pointsEntry: ARObjectivePointsEntry) -> [MetadatalItem] {
        var allEventMetadata = [MetadatalItem]()
        
        if let description = pointsEntry.typeName {
            let eventDescriptionMetadata = MetadatalItem(title: CommonStrings.Ar.PointsEventDetail.CellTitle684, description: description, typeCode: "")
            allEventMetadata.append(eventDescriptionMetadata)
        }
        
        for metadata in pointsEntry.objectivePointsEntryMetaDatas {
            //Device info should not appear in the details section since it has its own section so we skip over it here
            if metadata.typeKey == .Manufacturer {
                continue
            }
            
            guard var formattedDescription = formatMetadataValue(metadata: metadata) else {
                continue
            }
            
            if let convertJoulesToCalories = VIAApplicableFeatures.default.convertJoulesToCalories, convertJoulesToCalories{
                if metadata.typeName == "EnergyExpenditure" {
                    if let joulesValue = Localization.decimalFormatter.number(from: metadata.value){
                        let calorieValue = UnitEnergy.calories.converter.value(fromBaseUnitValue: joulesValue.doubleValue)
                        formattedDescription = Localization.decimalFormatter.string(from: calorieValue as NSNumber)!
                    }
                }
            }else{
                if let joulesValue = Localization.decimalFormatter.number(from: metadata.value){
                       formattedDescription = Localization.decimalFormatter.string(from: joulesValue as NSNumber)!
                }
            }
            
            let metadataItem = MetadatalItem(title: metadata.typeName, description: formattedDescription, typeCode: metadata.typeCode)
            
            allEventMetadata.append(metadataItem)
        }
        
        let formattedDate = Localization.fullDateFormatter.string(from: pointsEntry.effectiveDate)
        let dateMetadata = MetadatalItem(title: CommonStrings.DateTitle264, description: formattedDate, typeCode: "")
        allEventMetadata.append(dateMetadata)
        
        return allEventMetadata
        
    }
    
    func formatMetadataValue(metadata: ARObjectivePointsEntryMetaData) -> String? {
        return metadata.format()
    }
    
    // MARK: Section Helpers
    
    enum Sections: Int, EnumCollection {
        case points = 0
        case device = 1
        case details = 2
        
        func sectionHeaderTitle() -> String? {
            switch self {
            case .points:
                return CommonStrings.Ar.PointsEventDetail.PointsSectionHeaderTitle716
            case .device:
                return CommonStrings.Ar.PointsEventDetail.DeviceSectionHeaderTitle700
            case .details:
                return CommonStrings.Ar.PointsEventDetail.DetailsSectionHeaderTitle735
            }
        }
    }
    
    func numberOfVisibleSections() -> Int {
        return  visibleSections.count
    }
    
    func visibleSection(at index: Int) -> Sections? {
        guard let section = visibleSections.element(at: index) else { return nil }
        return section
    }
    
    func title(for section: Int) -> String? {
        guard let sectionTitle = visibleSection(at: section)?.sectionHeaderTitle() else { return nil }
        return sectionTitle
    }
    
    func numberOfItems(in section: Int) -> Int {
        guard let currentSection = visibleSection(at: section) else { return 0 }
        switch currentSection {
        case .points:
            return 1
        case .device:
            return 1
        case .details:
            guard let detailCount = selectedEventDetailItem?.metadataItems.count else { return 0 }
            return detailCount
        }
    }
}
