import Foundation
import VitalityKit
import VIAUtilities
import VitalityKit

class VIAARActivityDetailViewModel {
    
    //MARK: Properties
    
    var selectedGoalItem: GoalItem?
    
    var selectedGoalTrackerId: Int?
    
    // MARK: Configuration
    
    func configureSelectedGoalItemData() {
        guard let selectedId = selectedGoalTrackerId else { return }
        let arRealm = DataProvider.newARRealm()
        arRealm.refresh()
        guard let selectedGoalTracker = arRealm.arGoalTracker(with: selectedId) else { return }
        
        let startDate = selectedGoalTracker.effectiveFrom
        let endDate = selectedGoalTracker.effectiveTo
        
        let monitorUntilDate = selectedGoalTracker.monitorUntil
        // filter events upto effectiveTo Date
        var events = selectedGoalTracker.configureEventDataForAllEvents().filter({ $0.earnedDate <= endDate })

        if AppSettings.getAppTenant() == .SLI {
            let dateFormatter = DateFormatter()
            let effectiveTo = dateFormatter.date(from: Localization.shortDayShortMonthFormatter.string(from: endDate)) ?? Date()
            events = selectedGoalTracker.configureEventDataForAllEvents().filter({ $0.earnedDate <= effectiveTo })
        }
        
        let goalTrackerStatus = GoalTrackerStatusRef(rawValue: selectedGoalTracker.goalTrackerStatus.rawValue) ?? GoalTrackerStatusRef.Unknown
        
        var goalTrackerPotentialPoints = 0
        var goalTrackerEarnedPoints = 0
        if let earnedPoints = selectedGoalTracker.objectiveTrackers.first?.pointsAchieved.value, let potentialPoints = selectedGoalTracker.objectiveTrackers.first?.pointsTarget.value {
            goalTrackerEarnedPoints = earnedPoints
            goalTrackerPotentialPoints = potentialPoints
        }
        
        selectedGoalItem = GoalItem(status: goalTrackerStatus, potentialPoints: goalTrackerPotentialPoints, earnedPoints: goalTrackerEarnedPoints, startDate: startDate, endDate: endDate, monitorUntilDate: monitorUntilDate, events: events)
    }
    
    // MARK: Section Helpers
    
    enum Sections: Int, EnumCollection {
        case goalStatus = 0
        case events = 1
        
        
        func sectionHeaderTitle() -> String? {
            switch self {
            case .events:
                return CommonStrings.Ar.ActivityDetail.ActivitySectionHeaderTitle723
            default:
                return nil
            }
        }
        
        func sectionFooterText() -> String? {
            switch self {
            case .events:
                return CommonStrings.Ar.ActivityDetail.ActivitySectionFooterText673
            default:
                return nil
            }
        }
    }
    
    func sectionCount() -> Int {
        return  Sections.allValues.count
    }
    
    func section(at index: Int) -> Sections? {
        return Sections(rawValue: index)
    }
    
    func numberOfItems(in section: Int) -> Int {
        guard let currentSection = self.section(at: section) else { return 0 }
        switch currentSection {
        case .goalStatus:
            var rowsInSection = 0
            guard selectedGoalItemHasStatus() else { return rowsInSection }
            rowsInSection += 1
            guard selectedGoalItemHasStatusDescription() else { return rowsInSection }
            rowsInSection += 1
            return rowsInSection
        case .events:
            guard let eventCount = selectedGoalItem?.events?.count, eventCount > 0 else { return 1 }
            return eventCount
        }
    }
    
    func title(for section: Int) -> String? {
        return self.section(at: section)?.sectionHeaderTitle()
    }
    
    func footer(for section: Int) -> String? {
        return self.section(at: section)?.sectionFooterText()
    }
    
    // MARK: Selected Goal Item helpers
    
    func selectedGoalItemHasStatus() -> Bool {
        return selectedGoalItem?.status != nil
    }
    
    func selectedGoalItemHasEvents() -> Bool {
        guard let eventCount = selectedGoalItem?.events?.count, eventCount > 0 else { return false }
        
        return true
    }
    
    func selectedGoalItemHasStatusDescription() -> Bool {
        return selectedGoalItem?.statusDescription() != nil
    }
    
    func selectedGoalItemStatusTitle() -> String? {
        guard let goalDetailitem = selectedGoalItem else { return nil }
        return goalDetailitem.statusTitle()
    }
    
    func selectedGoalItemStatusDescription() -> String? {
        return selectedGoalItem?.statusDescription()
    }
    
    func selectedGoalItemStatusIcon() -> UIImage? {
        guard let goalDetailitem = selectedGoalItem else { return nil }
        return goalDetailitem.statusIcon()
    }
    
    func selectedGoalItemDateRangeScreenTitle() -> String? {
        guard let selectedItem = selectedGoalItem else {return nil }
        let formattedStartDate = Localization.dayMonthAndYearFormatter.string(from: selectedItem.startDate)
        let formattedEndDate = Localization.dayMonthAndYearFormatter.string(from: selectedItem.endDate)
        return "\(formattedStartDate) - \(formattedEndDate)"
    }
    
    func selectedGoalItemPointsMessage() -> String? {
        guard let earnedPoints = selectedGoalItem?.earnedPoints else { return nil }
        guard let potentialPoints = selectedGoalItem?.potentialPoints else { return nil }
        guard let formattedEarnedPoints = Localization.integerFormatter.string(from: earnedPoints as NSNumber) else { return nil }
        guard let formattedPotentialPoints = Localization.integerFormatter.string(from: potentialPoints as NSNumber) else { return nil }
        
        return CommonStrings.Points.XOfYPointsTitle193(formattedEarnedPoints, formattedPotentialPoints)
    }
    
    // MARK: Events Helpers
    
    func event(at index: Int) -> EventDetailItem? {
        guard let event = selectedGoalItem?.events?.element(at: index) else { return nil }
        return event
    }
    
    func pointsForEvent(at index: Int) -> String? {
        guard let earnedPoints = event(at: index)?.earnedPoints else { return nil }
        return Localization.integerFormatter.string(from: earnedPoints as NSNumber)
    }
    
    func descriptionForEvent(at index: Int) -> String? {
        guard let eventDescription = event(at: index)?.metadataItems.first?.description else { return nil }
        return eventDescription
    }
    
    func dateEarnedForEvent(at index: Int) -> String? {
        guard let currentEvent = event(at: index) else { return nil }
        return Localization.dayDateShortMonthFormatter.string(from: currentEvent.earnedDate)
    }
    
    func deviceForEvent(at index: Int) -> String? {
        guard let eventDevice = event(at: index)?.deviceName else { return nil }
        return eventDevice
    }
    
}

//MARK: Realm Object Extensions

public extension ARGoalTracker {
    
    func configureEventDataForAllEvents() -> [EventDetailItem] {
        var events = [EventDetailItem]()
        
        guard let pointsEntries = objectiveTrackers.first?.objectivePointsEntries else { return events }
        
        
        for pointsEntry in pointsEntries {
            
            let earnedPoints = pointsEntry.pointsContributed
            
            if earnedPoints != 0 {
                
                var metadataItems = [MetadatalItem]()
                
                let id = pointsEntry.id
                let earnedDate = pointsEntry.effectiveDate
                let deviceName = pointsEntry.manufacturerName()
                if let description = pointsEntry.typeName {
                    let metadataItem = MetadatalItem(title: CommonStrings.Ar.PointsEventDetail.CellTitle684, description: description, typeCode: "")
                        metadataItems.append(metadataItem)
                }

                let event = EventDetailItem(id: id, deviceName: deviceName, earnedPoints: earnedPoints, earnedDate: earnedDate, metadataItems: metadataItems)
                events.append(event)
            }
        }
        events = events.sorted(by: { $0.earnedDate < $1.earnedDate })
        return events
    }
    
}
