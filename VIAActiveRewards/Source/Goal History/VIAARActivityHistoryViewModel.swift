import Foundation
import VIAUtilities
import VitalityKit
import RealmSwift
import SwiftDate

struct GoalItem {
    
    var status: GoalTrackerStatusRef
    var potentialPoints: Int
    var earnedPoints: Int
    var startDate: Date
    var endDate: Date
    var monitorUntilDate: Date
    var events: [EventDetailItem]?
    
    func statusIcon() -> UIImage? {
        switch status {
        case .InProgress:
            return UIImage(asset: VIAActiveRewardsAsset.Goals.statusInProgress)
        case .Pending:
            return UIImage(asset: VIAActiveRewardsAsset.Goals.statusPending)
        case .Achieved, .ManuallyAchieved:
            return UIImage(asset: VIAActiveRewardsAsset.Goals.statusAchieved)
        case .NotAchieved, .Cancelled, .ManualCancellation, .SystemCancellation:
            return UIImage(asset: VIAActiveRewardsAsset.Goals.statusNotAchieved)
        case .Unknown:
            return nil
        }
    }
    
    func statusTitle() -> String? {
        switch status {
        case .InProgress:
            return CommonStrings.Ar.GoalInProgressTitle726
        case .Pending:
            return CommonStrings.Ar.GoalPendingTitle693
        case .Achieved, .ManuallyAchieved:
            return CommonStrings.Ar.GoalHistory.GoalAchievedTitle692
        case .NotAchieved, .Cancelled, .ManualCancellation, .SystemCancellation:
            return CommonStrings.Ar.GoalHistory.NotAchievedTitle709
        case .Unknown:
            return nil
        }
    }
    
    func statusDescription() -> String? {
        switch  status {
        case .NotAchieved, .Cancelled, .ManualCancellation, .SystemCancellation:
            return CommonStrings.Ar.GoalHistory.NotAchievedMessage846
        default:
            return nil
        }
    }
    
}

internal  enum FooterState: Int, EnumCollection {
    case swipeToLoad = 0
    case loadingData = 1
    case noMoreActivity = 2
    case errorOccured = 3
}

open class VIAARActivityHistoryViewModel {
    
    //MARK: Properties
    
    public let arRealm = DataProvider.newARRealm()
    
    public var monthSections: [Date] = []
    var tableViewFooterState = FooterState.swipeToLoad
    var goalItemsBySection = [[GoalItem]]()
    var requestDateRanges = [RequestDateRange]()
    public var goalTrackersBySection = Array<Results<ARGoalTracker>>()
    
    internal struct RequestDateRange {
        var effectiveFrom: Date
        var effectiveTo: Date
        var requestStatus: RequestStatus = .unknown
    }
    
    internal enum RequestStatus: Int, EnumCollection {
        case unknown = 0
        case successful = 1
        case unsuccessful = 2
        case noDataReturned = 3
    }
    
    public required init() {
        
    }
    
    // MARK: Configuration
    
    func configureRequestDatesInThreeMonthPeriods(currentDate: Date) -> [RequestDateRange]? {
        var dateRanges = [RequestDateRange]()
        var addedAllOtherDates = false
        
        guard let firstDateRange = configureFirstRequestDateRange(currentDate: currentDate) else { return nil }
        guard let twoYearsBack = currentDate.twoYearsAgo() else {return nil}
        
        dateRanges.append(firstDateRange)
        
        while addedAllOtherDates == false {
            guard let lastAddedDateRange = dateRanges.last else { return dateRanges }
            
            guard let startDate = validStartDate(dateRange: lastAddedDateRange) else { return dateRanges }
            guard let endDate = validEndDate(currentDate: currentDate, dateRange: lastAddedDateRange) else { return dateRanges }
            
            let dateRange = RequestDateRange(effectiveFrom: startDate, effectiveTo: endDate, requestStatus: RequestStatus.unknown)
            dateRanges.append(dateRange)
            
            if endDate.isIn(date: twoYearsBack, granularity: .year) && endDate.isIn(date: twoYearsBack, granularity: .month) {
                addedAllOtherDates = true
            }
        }
        
        dateRanges = dateRanges.sorted(by: { $0.effectiveFrom > $1.effectiveFrom })
        return dateRanges
    }
    
    func configureFirstRequestDateRange(currentDate: Date) -> RequestDateRange? {
        guard let startDate = currentDate.twoMonthsAgo()?.startOfMonth() else { return nil }
        
        let requestDateRange = RequestDateRange(effectiveFrom: startDate, effectiveTo: currentDate, requestStatus: RequestStatus.unknown)
        return requestDateRange
    }
    
    func validEndDate(currentDate: Date, dateRange: RequestDateRange) -> Date? {
        guard let twoMonthsAgo = dateRange.effectiveTo.twoMonthsAgo() else { return nil }
        guard let endDate = twoMonthsAgo.startOfPreviousMonth()?.endOfMonth() else { return nil }
        if endDate < currentDate {
            return endDate
        } else {
            return currentDate
        }
    }
    
    func validStartDate(dateRange: RequestDateRange) -> Date? {
        guard let twoMonthsAgo = dateRange.effectiveFrom.twoMonthsAgo() else { return nil }
        guard let startDate = twoMonthsAgo.startOfPreviousMonth() else { return nil }
        return startDate
    }
    
    open func configureSectionsAndGoalItemData() {
        arRealm.refresh()
        
        let goalTrackers = arRealm.allARGoalTrackersExcludingFutureGoals()
        
        var unsortedDates: Set<Date> = []
        for goalTracker in goalTrackers {
            if isMonthInYearAlreadySaved(monthDate: goalTracker.effectiveTo, dateSet: unsortedDates) == false {
                unsortedDates.insert(goalTracker.effectiveTo)
            }
        }
        
        monthSections = unsortedDates.sorted(by: {$0 > $1})
        
        var ObjectsBySection = [Results<ARGoalTracker>]()
        for section in monthSections {
            let unsortedObjectsForSection = goalTrackers.filterForElementsBetweenStartAndEnd(month: section, fieldName: "effectiveTo")
            let sortedObjectsForSection = unsortedObjectsForSection.sorted(byKeyPath: "effectiveTo", ascending: false)
            ObjectsBySection.append(sortedObjectsForSection)
        }
        goalTrackersBySection = ObjectsBySection
        configureGoalItemsData()
    }
    
    public func isMonthInYearAlreadySaved(monthDate: Date, dateSet: Set<Date>) -> Bool {
        var monthAlreadySaved = false
        
        if dateSet.count == 0 {
            return monthAlreadySaved
        }
        
        for sectionMonthDate in dateSet {
            
            let monthsComparison = Calendar.current.compare(monthDate, to: sectionMonthDate, toGranularity: .month)
            let yearsComparison = Calendar.current.compare(monthDate, to: sectionMonthDate, toGranularity: .year)
            
            if monthsComparison == .orderedSame && yearsComparison == .orderedSame {
                monthAlreadySaved = true
                return monthAlreadySaved
            }
            
        }
        return monthAlreadySaved
    }
    
    public func configureGoalItemsData() {
        arRealm.refresh()
        guard goalTrackersBySection.count > 0 else { return }
        goalItemsBySection.removeAll()
        
        var goalItems = [GoalItem]()
        for goalTrackerSection in goalTrackersBySection {
            for goalTracker in goalTrackerSection {
                let startDate = goalTracker.effectiveFrom
                let endDate = goalTracker.effectiveTo
                let monitorUntilDate = goalTracker.monitorUntil
                let goalTrackerStatus = GoalTrackerStatusRef(rawValue: goalTracker.goalTrackerStatus.rawValue) ?? GoalTrackerStatusRef.Unknown
                var goalTrackerPotentialPoints = 0
                var goalTrackerEarnedPoints = 0
                if let earnedPoints = goalTracker.objectiveTrackers.first?.pointsAchieved.value, let potentialPoints = goalTracker.objectiveTrackers.first?.pointsTarget.value {
                    goalTrackerEarnedPoints = earnedPoints
                    goalTrackerPotentialPoints = potentialPoints
                }
                let goalItem = GoalItem(status: goalTrackerStatus, potentialPoints: goalTrackerPotentialPoints, earnedPoints: goalTrackerEarnedPoints, startDate: startDate, endDate: endDate, monitorUntilDate: monitorUntilDate, events: nil)
                
                goalItems.append(goalItem)
            }
            goalItemsBySection.append(goalItems)
            goalItems.removeAll()
        }
    }
    
    //MARK: Sections Helpers
    
    enum Sections: Int, EnumCollection {
        //TODO: Add help back once in scope again
        case months = 0
    }
    
    func numberOfDateSections() -> Int {
        return monthSections.count
    }
    
    func sectionCount() -> Int {
        return numberOfDateSections()
    }
    
    func sectionType(at index: Int) -> Sections? {
        return Sections.months
    }
    
    func dateTitle(for section: Int) -> String? {
        guard let sectionDate = monthSections.element(at: section) else { return nil }
        return Localization.fullMonthAndYearFormatter.string(from: sectionDate).uppercased()
    }
    
    //MARK: Goal Helpers
    
    func goalTracker(at indexPath: IndexPath) -> ARGoalTracker? {
        guard let goalTrackersInSection = goalTrackersBySection.element(at: indexPath.section) else { return nil }
        if indexPath.row >= 0 && indexPath.row < goalTrackersInSection.count {
            return goalTrackersInSection[indexPath.row]
        }
        return nil
    }
    
    func goalItem(at indexPath: IndexPath) -> GoalItem? {
        guard let goalItemsInSection = goalItemsBySection.element(at: indexPath.section) else { return nil }
        guard let goalItem = goalItemsInSection.element(at: indexPath.row) else { return nil }
        return goalItem
    }
    
    func numberOfGoalItems(in section: Int) -> Int {
        guard let goalItemsInSection = goalItemsBySection.element(at: section) else { return 0 }
        return goalItemsInSection.count
    }
    
    func goalPeriod(at indexPath: IndexPath) -> String? {
        guard let goalitem = goalItem(at: indexPath) else { return nil }
        let startDateString = Localization.shortDayShortMonthFormatter.string(from: goalitem.startDate)
        let endDateString = Localization.shortDayShortMonthFormatter.string(from: goalitem.endDate)
        return "\(startDateString) - \(endDateString)"
    }
    
    func goalPoints(at indexPath: IndexPath) -> String? {
        guard let goalItem = goalItem(at: indexPath) else { return nil }
        guard let formattedEarnedPoints = Localization.integerFormatter.string(from: goalItem.earnedPoints as NSNumber) else { return nil }
        guard let formattedPotentialPoints = Localization.integerFormatter.string(from: goalItem.potentialPoints as NSNumber) else { return nil }
        return CommonStrings.Points.XOfYPointsTitle193(formattedEarnedPoints, formattedPotentialPoints)
    }
    
    func goalStatusTitle(at indexPath: IndexPath) -> String? {
        guard let goalItem = goalItem(at: indexPath) else { return nil }
        return goalItem.statusTitle()
    }
    
    func goalStatusIcon(at indexPath: IndexPath) -> UIImage? {
        guard let goalItem = goalItem(at: indexPath) else { return nil }
        return goalItem.statusIcon()
    }
    
    //Networking
    
    public func requestGoalProgressAndDetail(completion: @escaping (Error?) -> Void) {
        let requestParameters = configureGoalProgressAndDetailsRequestParametersForNextDateRange()
        let tenantId = DataProvider.newRealm().getTenantId()
        
        Wire.Events.getGoalProgressAndDetails(tenantId: tenantId, request: requestParameters) { [weak self] error, response in
            let currentDateRangeIndex = self?.indexOfDateRangeToRequest()
            guard error == nil else {
                self?.update(index: currentDateRangeIndex, requestStatus: RequestStatus.unsuccessful)
                self?.updateTableViewFooterState(state: FooterState.errorOccured)
                completion(error)
                return
            }
            
            guard let serviceResponse = response else {
                self?.update(index: currentDateRangeIndex, requestStatus: RequestStatus.unknown)
                self?.updateTableViewFooterState(state: FooterState.swipeToLoad)
                completion(nil)
                return
            }
            
            if serviceResponse.didRecieveGoalTrackers() {
                self?.update(index: currentDateRangeIndex, requestStatus: RequestStatus.successful)
                self?.updateTableViewFooterState(state: FooterState.swipeToLoad)
            } else {
                self?.update(index: currentDateRangeIndex, requestStatus: RequestStatus.noDataReturned)
                self?.updateTableViewFooterState(state: FooterState.noMoreActivity)
            }
            
            completion(nil)
        }
    }
    
    func indexOfDateRangeToRequest() -> Int? {
        guard requestDateRanges.count > 0 else { return nil }
        
        if requestDateRanges.index(where:{ $0.requestStatus == RequestStatus.noDataReturned }) != nil {
            return nil
        } else if let unsuccessfulDateRangeIndex = requestDateRanges.index(where:{ $0.requestStatus == RequestStatus.unsuccessful }) {
            return unsuccessfulDateRangeIndex
        } else if let unknownDateRangeIndex = requestDateRanges.index(where:{ $0.requestStatus == RequestStatus.unknown }) {
            return unknownDateRangeIndex
        }
        
        return nil
    }
    
    func shouldAttemptToLoadNextRequest() -> Bool {
        guard let dateRangeindex = indexOfDateRangeToRequest() else { return false }
        guard let dateRange = requestDateRanges.element(at: dateRangeindex) else { return  false }
        
        switch dateRange.requestStatus {
        case .unsuccessful, .unknown:
            return true
        case .noDataReturned, .successful:
            return false
        }
        
    }
    
    func updateTableViewFooterState(state: FooterState) {
        tableViewFooterState = state
    }
    
    func currentTableViewFooterState() -> FooterState {
        return tableViewFooterState
    }
    
    func update(index: Int?, requestStatus: RequestStatus) {
        guard let index = index else { return }
        if requestDateRanges.element(at: index) != nil {
            requestDateRanges[index].requestStatus = requestStatus
        }
    }
    
    func sectionsIndexSet() -> IndexSet {
        let indexSet = IndexSet(integersIn: 0...numberOfDateSections())
        return indexSet
    }
    
    internal func configureGoalProgressAndDetailsRequestParametersWithRequiredFields() -> GetGoalProgressAndDetailsParameters {
        let partyId = DataProvider.newRealm().getPartyId()
        let goalKeys = [GoalRef.ActiveRewards.rawValue]
        
        let getGoalProgressAndDetailsParameters = GetGoalProgressAndDetailsParameters(effectiveDateFrom: nil, effectiveDateTo: nil, goalKeys: goalKeys, goalStatusTypeKeys: nil, partyId: partyId)
        
        return getGoalProgressAndDetailsParameters
    }
    
    internal func configureGoalProgressAndDetailsRequestParametersForNextDateRange() -> GetGoalProgressAndDetailsParameters {
        var requestParameters = configureGoalProgressAndDetailsRequestParametersWithRequiredFields()
        
        if requestDateRanges.count == 0 {
            let currentDate = Date()
            if let validRanges = configureRequestDatesInThreeMonthPeriods(currentDate: currentDate) {
                requestDateRanges = validRanges
            }
        }
        
        if let index = indexOfDateRangeToRequest(), let dateRange = requestDateRanges.element(at: index) {
            requestParameters = configureGoalProgressAndDetailsRequestParameters(effectiveFrom: dateRange.effectiveFrom, effectiveTo: dateRange.effectiveTo)
        }
        
        return requestParameters
    }
    
    internal func configureGoalProgressAndDetailsRequestParameters(effectiveFrom: Date, effectiveTo: Date) -> GetGoalProgressAndDetailsParameters {
        var requestParameters = configureGoalProgressAndDetailsRequestParametersWithRequiredFields()
        
        requestParameters.effectiveDateFrom = effectiveFrom
        requestParameters.effectiveDateTo = effectiveTo
        
        return requestParameters
    }
    
}

//MARK: Date extension

fileprivate extension Date {
    func startOfMonth() -> Date? {
        return Calendar.current.date(from: Calendar.current.dateComponents([.year, .month], from: Calendar.current.startOfDay(for: self)))
    }
    
    func endOfMonth() -> Date? {
        guard let startOfMonth = startOfMonth() else { return nil }
        return Calendar.current.date(byAdding: DateComponents(month: 1, day: -1), to: startOfMonth)
    }
    
    func startOfPreviousMonth() -> Date? {
        guard let startOfMonth = startOfMonth() else { return nil }
        return Calendar.current.date(byAdding: DateComponents(month: -1), to: startOfMonth)
    }
    
    func twoMonthsAgo() -> Date? {
        return Calendar.current.date(byAdding: .month, value: -2, to: self)
    }
        
    func twoYearsAgo() -> Date? {
        return Calendar.current.date(byAdding: .month, value: -24, to: self)
    }
}
