//
//  VIAARActivityHistoryViewController.swift
//  VitalityActive
//
//  Created by Wilmar van Heerden on 2016/11/10.
//  Copyright © 2016 Glucode. All rights reserved.
//

import Foundation
import VIAUIKit
import VitalityKit
import VIACommon
import VitalityKit

class VIAARActivityHistoryViewController: VIATableViewController, ImproveYourHealthTintable {
    
    let viewModel = VIAApplicableFeatures.default.getActivityHistoryViewModel() as? VIAARActivityHistoryViewModel
    
    // MARK: View lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadFirstTimeData()
        title = CommonStrings.Ar.Landing.ActivityCellTitle711
        configureAppearance()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        configureTableView()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        hideBackButtonTitle()
    }
    
    func configureAppearance() {
        navigationController?.makeNavigationBarTransparent()
        tableView.tableFooterView = requestStatusFooterView()
    }
    
    func configureTableView() {
        tableView.register(VIAARGoalCycleSummaryCell.nib(), forCellReuseIdentifier: VIAARGoalCycleSummaryCell.defaultReuseIdentifier)
        tableView.register(VIATableViewCell.nib(), forCellReuseIdentifier: VIATableViewCell.defaultReuseIdentifier)
        tableView.register(VIATableViewSectionHeaderFooterView.nib(), forHeaderFooterViewReuseIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier)
        tableView.register(PagingStatusTableViewSectionHeaderFooterView.nib(), forHeaderFooterViewReuseIdentifier: PagingStatusTableViewSectionHeaderFooterView.defaultReuseIdentifier)
        tableView.estimatedRowHeight = 75
        tableView.sectionHeaderHeight = 40
        tableView.sectionFooterHeight = 0
        tableView.estimatedSectionHeaderHeight = 40
        tableView.estimatedSectionFooterHeight = 0
        tableView.separatorInset = UIEdgeInsets(top: 0, left: 55, bottom: 0, right: 0)
        // add extra inset for the first cell's header
        tableView.contentInset = UIEdgeInsets(top: 25, left: 0, bottom: 0, right: 0)
    }
    
    func handleErrorOccurred(_ error: Error?) {
        let backendError = error as? BackendError ?? BackendError.other
            let statusView = handleBackendErrorWithStatusView(backendError, tryAgainAction: { [weak self] in
                self?.loadFirstTimeData()
            })
            configureStatusView(statusView)
    }
    
    // MARK: UITableView datasource
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        guard let sectionCount = viewModel?.sectionCount() else { return 0 }
        
        return sectionCount
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let rowCount = viewModel?.numberOfGoalItems(in: section) else { return 0 }
        
        return rowCount
    }
    
    // MARK: UITableView delegate
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return configureGoalCycleSummaryCell(indexPath)
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let theSectionType = viewModel?.sectionType(at: section) else { return nil }
        
        if theSectionType == .months {
            guard let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier) as?VIATableViewSectionHeaderFooterView else { return tableView.defaultTableViewHeaderFooterView() }
            view.labelText = viewModel?.dateTitle(for: section)
            view.set(textColor: .mediumGrey())
            view.set(alignment: .left)
            
            return view
        }
        
        return nil
    }
    
    func updateFooterView(state: FooterState) {
        
        guard let view = requestStatusFooterView() else { return }
        
        switch state {
        case .swipeToLoad:
            view.configureSwipeToLoad()
        case .loadingData:
            view.configureLoadingData()
        case .errorOccured:
            view.configureWith(errorText: CommonStrings.Ar.GoalHistory.ErrorOccuredFooterText764, tryAgainButtonText: CommonStrings.TryAgainButtonTitle43)
        case .noMoreActivity:
            view.configureWith(noActivityText: CommonStrings.Ar.GoalHistory.NoMoreActivityFooterText739)
        }
        
        tableView.tableFooterView = view
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        showActivityDetail()
    }
    
    
    
    // MARK: Cell configuration
    
    func requestStatusFooterView() -> PagingStatusTableViewSectionHeaderFooterView? {
        let owner = PagingStatusTableViewSectionHeaderFooterView()
        guard let view = PagingStatusTableViewSectionHeaderFooterView.viewFromNib(owner: owner) as? PagingStatusTableViewSectionHeaderFooterView else { return nil }
        view.actionBlock = { [weak self] in
            self?.loadData()
        }
        
        return view
    }
    
    func configureGoalCycleSummaryCell(_ indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: VIAARGoalCycleSummaryCell.defaultReuseIdentifier, for: indexPath) as? VIAARGoalCycleSummaryCell else { return tableView.defaultTableViewCell() }
        cell.selectionStyle = .none
        
        cell.pointsText = viewModel?.goalPoints(at: indexPath)
        cell.dateRangeText = viewModel?.goalPeriod(at: indexPath)
        cell.statusText = viewModel?.goalStatusTitle(at: indexPath)
        cell.statusImage = viewModel?.goalStatusIcon(at: indexPath)
        
        return cell
    }
    
    //MARK: ScrollView delegate
    
    override func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if ((scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height)
        {
            if let shouldAttempt = viewModel?.shouldAttemptToLoadNextRequest(), shouldAttempt {
                loadData()
            }
        }
    }
    
    // MARK: Actions
    
    func showActivityDetail() {
        performSegue(withIdentifier: "showActivityDetail", sender: nil)
    }
    func showHelp() {
        performSegue(withIdentifier: "showHelp", sender: nil)
    }
    
    // MARK: Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "showActivityDetail" {
            guard let indexPath = tableView.indexPathForSelectedRow else { return }
            guard let theSectionType = viewModel?.sectionType(at: indexPath.section) else { return }
            if theSectionType == .months {
                let destinationViewController = segue.destination as? VIAARActivityDetailViewController
                let selectedGoalTracker = viewModel?.goalTracker(at: indexPath)
                destinationViewController?.viewModel.selectedGoalTrackerId = selectedGoalTracker?.id
            }
        }
        
    }
    
    //MARK: Networking
    
    func loadFirstTimeData() {
        showHUDOnView(view: view)
        viewModel?.requestGoalProgressAndDetail { [weak self] error in
            self?.hideHUDFromView(view: self?.view)
            
            guard error == nil else { self?.handleErrorOccurred(error); return }
    
            self?.viewModel?.configureSectionsAndGoalItemData()
            self?.removeStatusView()
            self?.tableView.reloadData()
            
            if self?.viewModel?.sectionCount() == 0 {
                self?.configureEmptyStatusView()
            }
        }
    }
    
    func configureEmptyStatusView() {
        let view = VIAStatusView.viewFromNib(owner: self)!
        
        
        view.heading = CommonStrings.Ar.History.NoActivityHeadingTitle694
        view.message = CommonStrings.Ar.History.NoActivityHeadingMessage727
        view.buttonTitle = CommonStrings.HelpButtonTitle141
        view.actionBlock = { [weak self] in
            self?.performSegue(withIdentifier: "showHelp", sender: nil)
        }
        
        view.hideButtonBorder = false
        
        self.view.addSubview(view)
        
        view.snp.makeConstraints { (make) in
            make.width.equalToSuperview()
            make.left.right.top.equalToSuperview()
            make.height.equalToSuperview().offset(-62)
            make.bottom.equalToSuperview().offset(-62)
        }
    }
    func loadData() {
        viewModel?.updateTableViewFooterState(state: FooterState.loadingData)
        
        if let currentState = viewModel?.currentTableViewFooterState() {
            updateFooterView(state: currentState)
        }
        
        viewModel?.requestGoalProgressAndDetail { [weak self] error in
            if let state = self?.viewModel?.currentTableViewFooterState() {
                self?.updateFooterView(state: state)
            }
            
            if error == nil {
                self?.viewModel?.configureSectionsAndGoalItemData()
                self?.tableView.reloadData()
            }
            
        }
    }
    
}
