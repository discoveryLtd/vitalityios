import Foundation
import VIAUIKit
import VitalityKit
import VIAUtilities

class VIAARPointsEventDetailViewController: VIATableViewController, ImproveYourHealthTintable {
    
    public var viewModel = VIAARPointsEventDetailViewModel()
    
    // MARK: View lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = CommonStrings.Ar.PointsEventDetail.ViewTitle685
        viewModel.configureSelectedEventData()
        configureTableView()
    }
    
    // MARK: Configuration
    
    func configureTableView() {
        tableView.register(ActivityDetailSubtitleCell.nib(), forCellReuseIdentifier: ActivityDetailSubtitleCell.defaultReuseIdentifier)
        tableView.register(VIATableViewCell.nib(), forCellReuseIdentifier: VIATableViewCell.defaultReuseIdentifier)
        tableView.register(VIATableViewSectionHeaderFooterView.nib(), forHeaderFooterViewReuseIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier)
        tableView.estimatedRowHeight = 75
        tableView.sectionHeaderHeight = 40
        tableView.sectionFooterHeight = 25
        tableView.estimatedSectionHeaderHeight = 40
        tableView.estimatedSectionFooterHeight = 25
        // add extra inset for the first cell's header
        tableView.contentInset = UIEdgeInsets(top: 25, left: 0, bottom: 0, right: 0)
    }
    
    // MARK: UITableView datasource
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.numberOfVisibleSections()
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfItems(in: section)
    }
    
    // MARK: UITableView delegate
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let theSection = viewModel.visibleSection(at: indexPath.section) else { return tableView.defaultTableViewCell() }
        
        switch theSection {
        case .points:
            return configurePointsCell(indexPath)
        case .device:
            return configureDeviceCell(indexPath)
        case .details:
            return configureMetadataCell(indexPath)
        }
        
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        guard let theSection = viewModel.visibleSection(at: section) else { return CGFloat.leastNormalMagnitude }
        
        if theSection == .points || theSection == .device || theSection == .details {
            return UITableViewAutomaticDimension
        }
        return CGFloat.leastNormalMagnitude
        
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier) as? VIATableViewSectionHeaderFooterView else { return tableView.defaultTableViewHeaderFooterView() }
        if let title = viewModel.title(for: section) {
            view.labelText = title
        }
        return view
    }
    
    // MARK: Cell configuration
    
    func configurePointsCell(_ indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: VIATableViewCell.defaultReuseIdentifier, for: indexPath) as? VIATableViewCell else { return tableView.defaultTableViewCell() }
        cell.selectionStyle = .none
        cell.accessoryType = .none
        cell.cellImage = nil
        cell.labelText = viewModel.selectedEventDetailItem?.formattedEarnedPoints()
        cell.applyTitle1Styling()
        return cell
    }
    
    func configureDeviceCell(_ indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: VIATableViewCell.defaultReuseIdentifier, for: indexPath) as? VIATableViewCell else { return tableView.defaultTableViewCell() }
        cell.selectionStyle = .none
        cell.accessoryType = .none
        cell.cellImage = nil
        cell.labelText = viewModel.selectedEventDetailItem?.deviceName
        cell.applyDefaultStyling()
        return cell
    }
    
    func configureMetadataCell(_ indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ActivityDetailSubtitleCell.defaultReuseIdentifier, for: indexPath) as? ActivityDetailSubtitleCell else { return tableView.defaultTableViewCell() }
        cell.selectionStyle = .none
        guard let metadataItem = viewModel.selectedEventDetailItem?.metadataItem(at: indexPath.row) else { return cell }
        
        var description = metadataItem.description
        if let tenantID = AppSettings.getAppTenant(), tenantID == .SLI || tenantID == .UKE || tenantID == .GI {
            if metadataItem.typeCode == "Duration" {
                if let duration = Localization.integerFormatter.number(from: metadataItem.description) as? Double {
                    description = CommonStrings.Ar.EventDetail.DurationHhMmSs851("\(duration.toHoursMinutesSeconds().0)", "\(duration.toHoursMinutesSeconds().1)", "\(duration.toHoursMinutesSeconds().2)")
                }
            }
        }
        
        cell.headingText = metadataItem.title
        cell.contentText = description
        
        cell.configureWithSmallHeadingAndLargeContent()
        return cell
    }
    
}
