//
//  VIAAROnboardingActivatedViewController.swift
//  VitalityActive
//
//  Created by Wilmar van Heerden on 2016/10/21.
//  Copyright © 2016 Glucode. All rights reserved.
//

import Foundation
import VIAUIKit
import VitalityKit

class VIAAROnboardingActivatedViewController: VIACirclesViewController {
    internal var wdaContentViewModel: WellnessDevicesContentLookup?
    override func awakeFromNib() {
        super.awakeFromNib()
        viewModel = VIAAROnboardingActivatedViewModel()
        wdaContentViewModel = WellnessDevicesContentLookupViewModel()
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }

    override func buttonTapped(_ sender: UIButton) {
        showHUDOnWindow()
        wdaContentViewModel?.fetchDevices(completion: { [weak self] (error, isRealmEmpty) in
            self?.hideHUDFromWindow()
            if self?.wdaContentViewModel?.noLinkedDevices() ?? true {
                self?.showWellnessDevicesOnboarding()
            } else {
                self?.showActiveRewardsLanding()
            }
        })
    }
    
    func showWellnessDevicesOnboarding() {
        performSegue(withIdentifier: "showWellnessDevicesOnboardingFromAR", sender: nil)
    }
    
    func showActiveRewardsLanding() {
        self.parent?.dismiss(animated: false, completion: nil)
        performSegue(withIdentifier: "unwindToActiveRewardsLandingView", sender: nil)
    }
    
}
