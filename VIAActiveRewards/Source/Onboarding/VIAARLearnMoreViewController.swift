import Foundation
import VIAUIKit
import VIAUtilities
import VitalityKit
import VIACommon
import VitalityKit

class VIAARLearnMoreViewController: VIATableViewController, ImproveYourHealthTintable {
    
    let viewModel = VIAARLearnMoreViewModel()
    var partnersData: [ParticipatingPartnersViewModel.PartnerData]?
    // MARK: View lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = viewModel.title
        self.configureAppearance()
        self.configureTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.hideBackButtonTitle()
    }
    
    
    func configureAppearance() {
        navigationController?.makeNavigationBarTransparent()
    }
    
    func configureTableView() {
        self.tableView.register(VIAGenericContentCell.nib(), forCellReuseIdentifier: VIAGenericContentCell.defaultReuseIdentifier)
        self.tableView.register(VIATableViewCell.nib(), forCellReuseIdentifier: VIATableViewCell.defaultReuseIdentifier)
        self.tableView.estimatedRowHeight = 75
        self.tableView.sectionHeaderHeight = 0
        self.tableView.sectionFooterHeight = 0
        self.tableView.estimatedSectionHeaderHeight = 0
        self.tableView.estimatedSectionFooterHeight = 0
        self.tableView.backgroundColor = .white
        self.tableView.tableFooterView = UIView()
    }
    
    // MARK: UITableView datasource
    
    enum Sections: Int, EnumCollection {
        case Content
        case MenuItems
        
        func numberOfRows() -> Int {
            switch self {
            case .Content:
                return 4
            case .MenuItems:
                if let hideBenefitGuide = VIAApplicableFeatures.default.hideARLearnMoreBenefitGuide, hideBenefitGuide {
                    return 1
                }
                return 2
            }
        }
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return Sections.allValues.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let theSection = Sections(rawValue: section) {
            return theSection.numberOfRows()
        }
        return 0
    }
    
    // MARK: UITableView delegate
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let section = Sections(rawValue: indexPath.section) {
            if section == .Content && indexPath.row < section.numberOfRows() - 1 {
                cell.separatorInset = UIEdgeInsetsMake(0, max(UIScreen.main.bounds.size.width, UIScreen.main.bounds.size.height), 0, 0)
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let section = Sections(rawValue: indexPath.section)
        if section == .Content {
            return self.configureContentCell(indexPath)
        } else if section == .MenuItems {
            return self.configureMenuItemCell(indexPath)
        }
        return UITableViewCell(style: .default, reuseIdentifier: "Cell")
    }
    
    func configureContentCell(_ indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: VIAGenericContentCell.defaultReuseIdentifier, for: indexPath) as! VIAGenericContentCell
        
        let item = viewModel.contentItems[indexPath.row]
        
        if indexPath.row == 0 {
            cell.customHeaderFont = viewModel.headingTitleFont
            cell.customContentFont = viewModel.headingMessageFont
        } else {
            cell.customHeaderFont = viewModel.sectionTitleFont
            cell.customContentFont = viewModel.sectionMessageFont
        }
        cell.header = item.header
        cell.content = item.content
        cell.cellImage = item.image
        cell.selectionStyle = .none
        return cell
    }
    
    func configureMenuItemCell(_ indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: VIATableViewCell.defaultReuseIdentifier, for: indexPath) as! VIATableViewCell
        switch indexPath.row {
        case 0:
            cell.labelText = CommonStrings.Ar.LearnMore.ParticipatingPartnersTitle696
            cell.cellImage = UIImage(asset: VIAActiveRewardsAsset.ActiveRewards.activeRewardsRewards2)
            cell.accessoryType = .disclosureIndicator
            break
        case 1:
            cell.labelText = CommonStrings.Ar.LearnMore.BenefitGuideTitle728
            cell.cellImage = UIImage(asset: VIAActiveRewardsAsset.ActiveRewards.activeRewardsTerms)
            cell.accessoryType = .disclosureIndicator
            break
        default:
            cell.labelText = nil
            cell.cellImage = nil
            cell.accessoryType = .none
            break
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let section = Sections(rawValue: indexPath.section)
        if section == .MenuItems && indexPath.row == 0 {
            self.performSegue(withIdentifier: "showParticipatingPartners", sender: nil)
        } else if section == .MenuItems && indexPath.row == 1 {
            if let tenantID = AppSettings.getAppTenant(), tenantID == .SLI {
                if let url = URL(string: "http://vitality.sumitomolife.co.jp/reward/active/"){
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                }
            } else {
                self.performSegue(withIdentifier: "showBenefitGuide", sender: nil)
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showBenefitGuide"{
            VIAApplicableFeatures.default.navigateToBenefitGuide(segue: segue)
        }else if segue.identifier == "showParticipatingPartners"{
            guard let indexPath = tableView.indexPathForSelectedRow, let partner = self.partnersData?[indexPath.row], let detailVC = segue.destination as? ParticipatingPartnerDetailsViewController else { return }
            detailVC.viewModel = ParticipatingPartnersDetailsViewModel(for: partner)
            }
        }
    }

