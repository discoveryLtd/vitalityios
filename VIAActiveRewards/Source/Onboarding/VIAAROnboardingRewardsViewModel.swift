//
//  VIAAROnboardingRewardsViewModel.swift
//  VIAActiveRewards
//
//  Created by wenilyn.a.teorica on 11/02/2019.
//  Copyright © 2019 Glucode. All rights reserved.
//

import Foundation
import VitalityKit
import VIAUIKit
import VIAUtilities

public class VIAAROnboardingRewardsViewModel: AnyObject, OnboardingViewModel {
    public var heading: String {
        return CommonStrings.Ar.Rewards.OnboardingHeader2431
    }
    
    public var contentItems: [OnboardingContentItem] {
        var items = [OnboardingContentItem]()
        items.append(OnboardingContentItem(heading: CommonStrings.Ar.Rewards.OnboardingSubheader12432,
                                           content: CommonStrings.Ar.Rewards.OnboardingContent12433,
                                           image: UIImage(asset:  VIAActiveRewardsAsset.ActiveRewards.Rewards.Onboarding.onboardingSpinner)))
        items.append(OnboardingContentItem(heading: CommonStrings.Ar.Rewards.OnboardingSubheader22434,
                                           content: CommonStrings.Ar.Rewards.OnboardingContent22435,
                                           image: UIImage(asset: VIAActiveRewardsAsset.ActiveRewards.Rewards.Onboarding.mask)))
        return items
    }
    
    public var buttonTitle: String {
        return CommonStrings.OkButtonTitle40
    }
    
    public var alternateButtonTitle: String? {
        return nil
    }
    
    public var gradientColor: Color {
        return .blue
    }
}
