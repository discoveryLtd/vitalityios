import Foundation
import UIKit
import VIAUIKit
import VitalityKit

class VIAARVHRRequiredViewController: VIAViewController, ImproveYourHealthTintable {

    // VIEWS
    var headerImage: UIImageView = UIImageView()
    var headingLabel: UILabel = UILabel()
    var messageLabel: UILabel = UILabel()
    var actionButton: UIButton = UIButton(type: .system)

    // CONSTRAINTS
    var spacingAboveHeaderImage: Double = 90
    var spacingBetweenHeaderImageAndTitle: Double = 25
    var spacingBetweenTitleAndMessage: Double = 15
    var spacingBetweenMessageAndButton: Double = 55

    var iconSize: Double = 120

    var buttonTitle: String? {
        didSet {
            actionButton.setTitle(buttonTitle, for: .normal)
        }
    }

    var viewImage: UIImage? {
        didSet {
            headerImage.image = viewImage
        }
    }

    var viewHeading: String? {
        didSet {
            headingLabel.text = viewHeading
        }
    }

    var viewMessage: String? {
        didSet {
            messageLabel.text = viewMessage
        }
    }

    var buttonFillColor: UIColor? {
        didSet {
            actionButton.backgroundColor = buttonFillColor
        }
    }

    var buttonTitleColor: UIColor? {
        didSet {
            actionButton.tintColor = buttonTitleColor
        }
    }

    var buttonBorderRadius: Double = 10 {
        didSet {
            actionButton.layer.cornerRadius = CGFloat(buttonBorderRadius)
        }
    }

    var buttonBorderWidth: Double = 1 {
        didSet {
            actionButton.layer.borderWidth = CGFloat(buttonBorderWidth)
        }
    }

    var buttonBorderColor: UIColor? {
        didSet {
            actionButton.layer.borderColor = buttonBorderColor?.cgColor ?? UIColor.clear.cgColor
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = CommonStrings.GetStartedButtonTitle103
        configureAppearance()
        addBarButtonItems()
        addSubviews()
    }

    func configureAppearance() {
        navigationController?.makeNavigationBarTransparent()
        view.backgroundColor = UIColor.white
    }

    func addBarButtonItems() {
        let cancel = UIBarButtonItem(title: CommonStrings.CancelButtonTitle24, style: .plain, target: self, action: #selector(cancel(_:)))
        self.navigationItem.leftBarButtonItem = cancel
    }

    func addSubviews() {
        setupSubviews()
        constainSubview()
        populateSubview()
    }

    func setupSubviews () {
        headerImage.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(headerImage)

        headingLabel.translatesAutoresizingMaskIntoConstraints = false
        headingLabel.font = UIFont.title2Font()
        headingLabel.textAlignment = .center
        headingLabel.numberOfLines = 2
        self.view.addSubview(headingLabel)

        messageLabel.translatesAutoresizingMaskIntoConstraints = false
        messageLabel.font = UIFont.bodyFont()
        messageLabel.textAlignment = .center
        messageLabel.numberOfLines = 3
        self.view.addSubview(messageLabel)

        actionButton.translatesAutoresizingMaskIntoConstraints = false
        actionButton.addTarget(self, action: #selector(viewVHR), for: .touchUpInside)
        actionButton.contentEdgeInsets = UIEdgeInsets(top: 10, left: 40, bottom: 10, right: 40)
        self.view.addSubview(actionButton)
    }

    func constainSubview () {
        let viewMargins = view.layoutMarginsGuide
        headerImage.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        headerImage.topAnchor.constraint(equalTo: topLayoutGuide.bottomAnchor, constant: CGFloat(spacingAboveHeaderImage)).isActive = true
        headerImage.widthAnchor.constraint(equalToConstant: CGFloat(iconSize)).isActive = true
        headerImage.heightAnchor.constraint(equalToConstant: CGFloat(iconSize)).isActive = true

        headingLabel.topAnchor.constraint(equalTo: headerImage.bottomAnchor, constant: CGFloat(spacingBetweenHeaderImageAndTitle)).isActive = true
        headingLabel.leadingAnchor.constraint(equalTo: viewMargins.leadingAnchor, constant: 40.0).isActive = true
        headingLabel.trailingAnchor.constraint(equalTo: viewMargins.trailingAnchor, constant: -40.0).isActive = true

        messageLabel.topAnchor.constraint(equalTo: headingLabel.bottomAnchor, constant: CGFloat(spacingBetweenTitleAndMessage)).isActive = true
        messageLabel.leadingAnchor.constraint(equalTo: viewMargins.leadingAnchor, constant: 3.0).isActive = true
        messageLabel.trailingAnchor.constraint(equalTo: viewMargins.trailingAnchor, constant: -3.0).isActive = true

        actionButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        actionButton.topAnchor.constraint(equalTo: messageLabel.bottomAnchor, constant: CGFloat(spacingBetweenMessageAndButton)).isActive = true
        actionButton.heightAnchor.constraint(equalToConstant: 44.0).isActive = true
    }

    func populateSubview () {
        self.viewImage = UIImage(asset: VIAActiveRewardsAsset.ActiveRewards.arvhrRequired)
        self.viewHeading = CommonStrings.Review.Intro.Header2361
        self.viewMessage = CommonStrings.Ar.GetStartedVhrDescription770
        self.buttonTitle = CommonStrings.Ar.GetStartedCompleteVhrButtonTitle772
        self.buttonFillColor = .clear
        self.buttonBorderColor = UIColor.improveYourHealthBlue()
        self.buttonBorderWidth = 1.0
        self.buttonBorderRadius = 5.0
    }

    // MARK: Navigation

    func cancel(_ sender: Any?) {
        self.performSegue(withIdentifier: "unwindToHomeViewController", sender: nil)
    }

    @IBAction func viewVHR(_ sender: UIButton) {
        self.performSegue(withIdentifier: "viewVHR", sender: nil)
    }
}
