//
//  VIAARParticipatingPartnersViewController.swift
//  VitalityActive
//
//  Created by Wilmar van Heerden on 2016/11/16.
//  Copyright © 2016 Glucode. All rights reserved.
//

import Foundation
import VIAUIKit
import VitalityKit
import VIAUtilities
import VIACommon
import VIAPartners

class VIAARParticipatingPartnersViewController: VIATableViewController {

    // MARK: View lifecycle
    var partnersViewModel = ParticipatingPartnersViewModel(withPartnerType: .RewardPartners)
    var partnersData: [ParticipatingPartnersViewModel.PartnerData]?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = CommonStrings.Ar.LearnMore.ParticipatingPartnersTitle696
        self.configureTableView()
        loadPartners()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.hideBackButtonTitle()
    }
    
    func configureTableView() {
        self.tableView.register(VIAARParticipatingPartnerCell.nib(), forCellReuseIdentifier: VIAARParticipatingPartnerCell.defaultReuseIdentifier)
        self.tableView.estimatedRowHeight = 50
    }

    // MARK: UITableView datasource

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let partners = self.partnersData {
            return partners.count
        }
        return 0
    }

    // MARK: UITableView delegate

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell = self.tableView.dequeueReusableCell(withIdentifier: VIAARParticipatingPartnerCell.defaultReuseIdentifier, for: indexPath) as! VIAARParticipatingPartnerCell
//        cell.partnerName = CommonStrings.Ar.Partners.StarbucksName734
//        cell.descriptionText = CommonStrings.Ar.Partners.ParticipatingPartnersVoucherValue722
//        cell.cellImage = VIAARRewardHelper.partnerImage(for: RewardReferences.StarbucksVoucher)
//        return cell
        return self.configureParticipatingPartnersCell(indexPath)
    }
    func configureParticipatingPartnersCell(_ indexPath: IndexPath) -> UITableViewCell {
        guard let cell = self.tableView.dequeueReusableCell(withIdentifier: VIAARParticipatingPartnerCell.defaultReuseIdentifier, for: indexPath) as? VIAARParticipatingPartnerCell, let partner = partnersData?[indexPath.row] else { return tableView.defaultTableViewCell() }
        
        cell.partnerName = partner.name
        cell.descriptionText = partner.description
        if (partner.name.lowercased().range(of:"starbucks") != nil) {
            cell.descriptionText = CommonStrings.Ar.Partners.StarbucksVoucherValue705
        }
        // TODO: Remove this once server can return IGI partner data.
        if partner.name == "EasyTickets" {
            cell.cellImage = VIAActiveRewardsAsset.ActiveRewards.Rewards.IGIPartners.easyticketsSmall.image
        } else if partner.name == "foodpanda" {
            cell.cellImage = VIAActiveRewardsAsset.ActiveRewards.Rewards.IGIPartners.foodpandaSmall.image
        } else {
            DispatchQueue.global(qos: .background).async { [weak self] in
                self?.partnersViewModel.partnerLogo(partner: partner) { logo, partner in
                    DispatchQueue.main.async {
                        if cell.partnerName == partner.name {
                            cell.cellImage = logo
                        }
                    }
                }
            }
        }
        cell.isUserInteractionEnabled = true
        cell.selectionStyle = .none
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if (self.partnersData?[indexPath.row]) != nil {
            
            /* 223: Apple Watch */
            if self.partnersData?[indexPath.row].typeKey == 223{
                performSegue(withIdentifier: "segueParticipatingPartnerDetails", sender: nil)
            }else{
                let shouldUseEligibility = VIAApplicableFeatures.default.shouldUseGetEligibility ?? true
                if shouldUseEligibility {
                    performSegue(withIdentifier: "showParticipatingPartnerDetails", sender: nil)
                } else {
                    if AppSettings.getAppTenant() == .EC || AppSettings.getAppTenant() == .ASR {
                        // Use table view class for Ecuador for better implementation of the added 'Terms and Conditions' row.
                        performSegue(withIdentifier: "showARParticipatingPartnerDetailTable", sender: nil)
                    } else {
                        performSegue(withIdentifier: "showARParticipatingPartnerDetail", sender: nil)
                    }
                }
            }
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        if segue.identifier == "segueParticipatingPartnerDetails"{
            guard let indexPath = tableView.indexPathForSelectedRow,
                let partner = self.partnersData?[indexPath.row],
                let detailVC = segue.destination as? PartnersDetailTableViewController else { return }
            let partnerData = PartnersListTableViewModel.PartnerData(name: partner.name,
                                                                     longDescription: partner.longDescription,
                                                                     description: partner.description,
                                                                     logoFileName: partner.logoFileName,
                                                                     typeKey: partner.typeKey,
                                                                     learnMoreUrl: partner.learnMoreUrl,
                                                                     webViewContent: partner.webViewContent,
                                                                     heading: nil)
            detailVC.viewModel = PartnersDetailTableViewModel(for: partnerData)
        }else if segue.identifier == "showParticipatingPartnerDetails"{
            guard let indexPath = tableView.indexPathForSelectedRow, let partner = self.partnersData?[indexPath.row], let detailVC = segue.destination as? ParticipatingPartnerDetailsViewController else { return }
            detailVC.viewModel = ParticipatingPartnersDetailsViewModel(for: partner)
        }else if segue.identifier == "showARParticipatingPartnerDetail"{
            guard let indexPath = tableView.indexPathForSelectedRow, let partner = self.partnersData?[indexPath.row], let detailVC = segue.destination as? VIAARPartnerDetailViewController else { return }
            detailVC.partnerViewModel = partnersViewModel
            detailVC.partner = partner
        } else if segue.identifier == "showARParticipatingPartnerDetailTable" {
            guard let indexPath = tableView.indexPathForSelectedRow, let partner = self.partnersData?[indexPath.row], let detailVC = segue.destination as? VIAARPartnerDetailTableViewController else { return }
            detailVC.partnerViewModel = partnersViewModel
            detailVC.partner = partner
        }
    }
    func loadPartners() {
        showFullScreenHUD()
        partnersViewModel.requestPartnersByCategory(completion: { [weak self] (error) in
            self?.hideFullScreenHUD()
            guard error == nil else {
                
                self?.handleError(error)
                return
            }
            self?.partnersViewModel.configurePartnerData()
            if let tempData = self?.partnersViewModel.partnerGroupsData.first?.partners {
                self?.partnersData = tempData
            }
            self?.tableView.reloadData()
        })
    }
    
    func handleError(_ error: Error?) {
        let statusView = handleBackendErrorWithStatusView((error as? BackendError) ?? .other, tryAgainAction: { [weak self] in
            self?.loadPartners()
        })
        configureStatusView(statusView)
    }
}
