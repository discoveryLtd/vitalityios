import Foundation
import UIKit
import VitalityKit
import VIAUIKit
import VIAUtilities

public class VIAAROnboardingGetStartedViewModelBase: OnboardingViewModel {
    
    public var heading: String {
        return CommonStrings.Ar.Onboarding.CommonHeading682
    }
    
    public var contentItems: [OnboardingContentItem] {
        var items = [OnboardingContentItem]()
        items.append(OnboardingContentItem(heading: CommonStrings.Ar.Onboarding.CommonItem1Heading665,
                                           content: CommonStrings.Ar.Onboarding.CommonItem1731,
                                           image: UIImage(asset: VIAActiveRewardsAsset.ActiveRewards.Onboarding.arOnboardingCalendar)))
        items.append(OnboardingContentItem(heading: CommonStrings.Ar.Onboarding.CommonItem2Heading715,
                                           content: CommonStrings.Ar.Onboarding.CommonItem2683,
                                           image: UIImage(asset: VIAActiveRewardsAsset.ActiveRewards.Onboarding.arOnboardingRings)))
        items.append(OnboardingContentItem(heading: CommonStrings.Ar.Onboarding.CommonItem3Heading697,
                                           content: CommonStrings.Ar.Onboarding.CommonItem3666,
                                           image: UIImage(asset: VIAActiveRewardsAsset.ActiveRewards.Onboarding.arOnboardingTrophy)))
        
        let tenantID = AppSettings.getAppTenant()
        
        if tenantID != .IGI {
            
            items.append(OnboardingContentItem(heading: CommonStrings.Ar.Onboarding.ProbabilisticItem4Heading699,
                                           content: CommonStrings.Ar.Onboarding.ProbabilisticItem4698,
                                           image: UIImage(asset: VIAActiveRewardsAsset.ActiveRewards.Onboarding.arOnboardingSpinner)))
        }
        
        return items
    }
    
    public var buttonTitle: String {
        return CommonStrings.GetStartedButtonTitle103
    }
    
    public var alternateButtonTitle: String? {
        return CommonStrings.LearnMoreButtonTitle104
    }
    
    public var gradientColor: Color {
        return .blue
    }
}
