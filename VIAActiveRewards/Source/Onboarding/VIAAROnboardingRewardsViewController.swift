//
//  VIAAROnboardingRewardsViewController.swift
//  VIAActiveRewards
//
//  Created by wenilyn.a.teorica on 11/02/2019.
//  Copyright © 2019 Glucode. All rights reserved.
//

import Foundation
import VIACommon
import VitalityKit
import VIAUIKit

class VIAAROnboardingRewardsViewController: VIAOnboardingViewController {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.viewModel = VIAAROnboardingRewardsViewModel()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.sectionHeaderHeight = 0
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.navigationBar.isHidden = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.viewWillAppear(animated)
        
        self.hideBackButtonTitle()
    }
    
    // MARK: - Navigation
    override func mainButtonTapped(_ sender: UIButton) {
        AppSettings.setHasShownRewardsOnboarding()
        self.dismiss(animated: true, completion: nil)
    }
}
