import Foundation
import UIKit
import VitalityKit
import VIAUIKit

public class VIAAROnboardingGetStartedARProbabilisticViewModel: VIAAROnboardingGetStartedViewModelBase {

    public override var contentItems: [OnboardingContentItem] {
        let contentItemsModified = [OnboardingContentItem](super.contentItems)
        return contentItemsModified
    }
    
    public override init() {
        super.init()
    }
}
