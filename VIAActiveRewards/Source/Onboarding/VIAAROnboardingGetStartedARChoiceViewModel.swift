import Foundation
import UIKit
import VitalityKit
import VIAUIKit

public class VIAAROnboardingGetStartedARChoiceViewModel: VIAAROnboardingGetStartedViewModelBase {

    public override var contentItems: [OnboardingContentItem] {

        let lastItem = OnboardingContentItem(heading: CommonStrings.Ar.Onboarding.DefinedItem4Heading733, content: CommonStrings.Ar.Onboarding.DefinedItem4732, image: UIImage(asset: VIAActiveRewardsAsset.ActiveRewards.Onboarding.arOnboardingSpinner))
        var contentItemsModified = [OnboardingContentItem](super.contentItems)
        contentItemsModified.append(lastItem)
        return contentItemsModified
    }
    
   public override init() {
        super.init()
    }
}
