import Foundation
import VitalityKit
import UIKit
import VitalityKit
import UIKit
import VitalityKit
import VIAUIKit
import VitalityKit
import SafariServices
import VIACommon
import VIAUtilities

class ARMedicallyFitAgreementViewController: CustomTermsConditionsViewController, ProgrammeAcceptor, ImproveYourHealthTintable {

    // MARK: Properties

    var completion: () -> Void = {
        debugPrint("Lets detail screen know that user accepted the privacy policy")
    }

    // MARK: Lifecycle

    override func awakeFromNib() {
        super.awakeFromNib()

        self.viewModel = CustomTermsConditionsViewModel(articleId: AppConfigFeature.contentId(for: .ARMedicallyFitAgreementContent))
    }

    override func configureAppearance() {
        super.configureAppearance()
        navigationController?.makeNavigationBarInvisible()
        navigationController?.navigationBar.isHidden = true
    }

    // MARK: Action buttons

    override func leftButtonTapped(_ sender: UIBarButtonItem?) {
        //let events: [EventTypeRef] = [EventTypeRef.Medicallyfitdisagree]
        //processTermsAndConditions(events: events) { [weak self] in
            self.navigationController?.navigationBar.isHidden = false
            self.performSegue(withIdentifier: "unwindToActiveRewardsOnboardingGetStarted", sender: nil)
        //}
    }

    override func rightButtonTapped(_ sender: UIBarButtonItem?) {
        self.showHUDOnWindow()
        let events: [EventTypeRef] = [EventTypeRef.Medicallyfitagree]
        processTermsAndConditions(events: events, completion: { [weak self] in
            self?.performSegue(withIdentifier: "showAROnboardingActivated", sender: nil)
        })
    }
    
    func processTermsAndConditions(events: [EventTypeRef], completion: @escaping (()-> Void)) {
        viewModel?.agreeToTermsAndConditions(with: events, completion: { [weak self] (error) in
            self?.hideHUDFromWindow()
            guard error == nil else {
                self?.handleBackendErrorWithAlert((error as? BackendError) ?? BackendError.other, tryAgainAction: { [weak self] in
                    self?.rightButtonTapped(nil)
                })
                return
            }
            self?.showHUDOnWindow()
            self?.activateActiveRewardsProgramme(completion: { [weak self] (error) in
                self?.hideHUDFromWindow()
                if let error = error {
                    self?.handleBackendErrorWithAlert((error as? BackendError) ?? BackendError.other, tryAgainAction: { [weak self] in
                        self?.rightButtonTapped(nil)
                    })
                    return
                }
                completion()
            })
        })
    }

    override func returnToPreviousView() {
        self.performSegue(withIdentifier: "unwindToActiveRewardsOnboardingGetStarted", sender: nil)
    }
}
