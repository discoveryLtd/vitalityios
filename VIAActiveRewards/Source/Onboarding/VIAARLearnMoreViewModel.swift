import VIAUIKit
import VitalityKit
import VIACommon

public class VIAARLearnMoreViewModel: AnyObject, VIALearnMoreViewModel {
    
    public var title: String {
        return CommonStrings.LearnMoreButtonTitle104
    }
    
    public var contentItems: [LearnMoreContentItem] {
        var items = [LearnMoreContentItem]()
        
        items.append(LearnMoreContentItem(
            header: CommonStrings.Ar.LearnMore.HowActiveRewardsWorksTitle663,
            content: CommonStrings.Ar.LearnMore.HowActiveRewardsWorksContent714
        ))
        
        items.append(LearnMoreContentItem(
            header: CommonStrings.Ar.LearnMore.ActiveWeeklyPointsTargetTitle661,
            content: CommonStrings.Ar.LearnMore.ActiveWeeklyPointsTargetContent712,
            image: VIAActiveRewardsAsset.ActiveRewards.LearnMore.arLearnMoreActivate.image
        ))
        
        items.append(LearnMoreContentItem(
            header: CommonStrings.Ar.LearnMore.WorkOutReachYourTargetTitle729,
            content: CommonStrings.Ar.LearnMore.WorkOutReachYourTargetContent680,
            image: VIAActiveRewardsAsset.ActiveRewards.LearnMore.arLearnMoreTrophy.image
        ))
        
        items.append(LearnMoreContentItem(
            header: CommonStrings.Ar.LearnMore.ClaimRewardTitle662,
            content: CommonStrings.Ar.LearnMore.ClaimRewardContent713,
            image: VIAActiveRewardsAsset.ActiveRewards.LearnMore.arLearnMoreReward.image
        ))
        
        return items
    }
    
    public var imageTint: UIColor {
        return .primaryColor()
    }
}
