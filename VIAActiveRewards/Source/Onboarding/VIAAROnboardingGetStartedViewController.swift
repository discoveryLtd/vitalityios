//
//  VIAAROnboardingGetStartedViewController.swift
//  VitalityActive
//
//  Created by Wilmar van Heerden on 2016/10/21.
//  Copyright © 2016 Glucode. All rights reserved.
//

import Foundation
import VIAUIKit
import VitalityKit
import VIACommon

class VIAAROnboardingGetStartedViewController: VIAOnboardingViewController, ProgrammeAcceptor, PrimaryColorTintable {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.navigationBar.isHidden = false
        self.hideBackButtonTitle()
    }
    
    // MARK: Button actions
    
    override func mainButtonTapped(_ sender: UIButton?) {
        if VitalityProductFeature.isEnabled(.ARMedicallyFit) {
            self.performSegue(withIdentifier: "showActiveRewardsMedicallyFit", sender: nil)
        } else {
            showHUDOnWindow()
            activateActiveRewardsProgramme(completion: { [weak self] (error) in
                self?.hideHUDFromWindow()
                if let error = error {
                    self?.handleBackendErrorWithAlert((error as? BackendError) ?? BackendError.other, tryAgainAction: { [weak self] in
                        self?.mainButtonTapped(nil)
                    })
                    return
                }
                self?.performSegue(withIdentifier: "showActiveRewardsOnboardingCompletion", sender: nil)
            })
        }
    }
    
    override func alternateButtonTapped(_ sender: UIButton) {
        self.performSegue(withIdentifier: "showLearnMore", sender: nil)
    }
    
    // MARK: Unwind navigation
    
    @IBAction public func unwindToActiveRewardsOnboardingGetStarted(segue: UIStoryboardSegue) {
        debugPrint("User agreed/disagreed to AR medically fit agreement")
    }
    
    @IBAction public func unwindToActiveRewardsLandingView(segue: UIStoryboardSegue) {
        debugPrint("User completed AR onboarding")
        navigationController?.isNavigationBarHidden = false
        guard let customSegue = segue as? VIAOnboardingLandingSwapSegue else { return }
        customSegue.customDestinationViewControllerIdentifier = "VIAARLandingViewController"
    }
}
