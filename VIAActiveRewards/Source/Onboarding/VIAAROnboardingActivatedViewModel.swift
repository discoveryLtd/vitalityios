//
//  VIAAROnboardingActivatedViewModel.swift
//  VitalityActive
//
//  Created by Wilmar van Heerden on 2016/10/18.
//  Copyright © 2016 Glucode. All rights reserved.
//

import Foundation
import UIKit
import VIAUIKit
import VitalityKit

public class VIAAROnboardingActivatedViewModel: AnyObject, CirclesViewModel {

    public required init() {
    }

    public var image: UIImage? {
        return UIImage(asset: VIAActiveRewardsAsset.ActiveRewards.Onboarding.arOnboardingActivated)
    }

    public var heading: String? {
        return CommonStrings.Ar.ActivatedHeading672
    }

    public var message: String? {
        return CommonStrings.Ar.ActivatedMessage645
    }

    public var footnote: String? {
        return nil
    }

    public var buttonTitle: String? {
        return CommonStrings.GenericGotItButtonTitle131
    }

    public var gradientColor: Color {
        return .blue
    }

    public var buttonAction: ((_: UIViewController?) -> Void)? {
        return { viewController in
            guard viewController != nil else { return }
            viewController!.performSegue(withIdentifier: "unwindToActiveRewardsOnboardingGetStarted", sender: nil)
        }
    }
    
}
