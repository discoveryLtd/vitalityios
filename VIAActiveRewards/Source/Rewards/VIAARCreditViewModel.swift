//
//  VIAARCreditViewModel.swift
//  VitalityActive
//
//  Created by Joshua Ryan on 6/28/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

import VitalityKit

public class VIAARCreditViewModel {

	// MARK: Public
	var id: Int = 0
	var earnedDate: Date?
	var expirationDate: Date?
	var used: Bool = false
	var voucher: VIAARRewardViewModel?
    var winningVoucherId: Int?
	var rewardSelectionTypeKey: Int?
	
	// MARK: Private
	private let arRealm = DataProvider.newARRealm()

    public init(id: Int, earnedDate: Date?, expirationDate: Date?, used: Bool, voucher: VIAARRewardViewModel?, winningVoucherId: Int?, rewardSelectionTypeKey: Int?) {
		self.id = id
		self.earnedDate = earnedDate
		self.expirationDate = expirationDate
		self.used = used
		self.voucher = voucher
        self.winningVoucherId = winningVoucherId
		self.rewardSelectionTypeKey = rewardSelectionTypeKey
	}
}
