    //
    //  VIAARRewardsLandingViewController.swift
    //  VitalityActive
    //
    //  Created by Wilmar van Heerden on 2016/11/10.
    //  Copyright © 2016 Glucode. All rights reserved.
    //
    
    import Foundation
    import VIAUIKit
    import SnapKit
    import VitalityKit
    import VIAUtilities
    import VIACommon
    
    public protocol ActiveRewardsRewardsLandingViewModel {
        var availableRewardCredits: [VIAARCreditViewModel] { get }
        var participatingPartners: [VIAARRewardViewModel] { get }
        var pastRewards: [VIAARRewardViewModel] { get }
        var usableRewards: [VIAARRewardViewModel] { get }
        var pastRewardsByMonth: ([String], [[VIAARRewardViewModel]]) { get }
        
        func updateRewardStatusToUsedOrUnused(_ reward: VIAARRewardViewModel, status: AwardedRewardStatusRef, completion: @escaping((Error?, UpdateRewardStatusResponse?) -> Void))
        func submitEventForRewardStatusChange(_ reward: VIAARRewardViewModel, status: AwardedRewardStatusRef, completion: @escaping((Error?) -> Void))
    }
    
    public protocol ARLandingNavDelegate: ARRewardFlowDelegate {
        
        var landingModel: ActiveRewardsRewardsLandingViewModel { get }
        
        func showReward(_ reward: VIAARRewardViewModel)
        func useCredit(_ credit: VIAARCreditViewModel)
        // TODO: should this be a reward or a vendor? Creative shows a list of rewards but links to a vendor
        func showPartner(_ partner: VIAARRewardViewModel)
    }
    
    class VIAARRewardsLandingViewController: VIACoordinatedTableViewController, ImproveYourHealthTintable {
        
        // MARK: Public
        var partnersViewModel = ParticipatingPartnersViewModel(withPartnerType: .RewardPartners)
        var partnersData: [ParticipatingPartnersViewModel.PartnerData]?
        
        internal var viewModel: ActiveRewardsRewardsLandingViewModel {
            get {
                return flowController.landingModel
            }
        }
        
        private var isDoneLoadingRewards: Bool = true
        private var isDoneLoadingPartners: Bool = true
        private var isLoading: Bool = true
        
        // MARK: Private
        
        @IBOutlet var segmentedControl: UISegmentedControl!
        private var flowController: ARLandingNavDelegate = ARRewardsFlowCoordinator()
        private var historyMode = false
        
        private var selectedTag: Int = 0

        // MARK: View lifecycle
        
        override func awakeFromNib() {
            super.awakeFromNib()
            
            self.segmentedControl.setTitle(CommonStrings.Ar.Rewards.CurrentSegmentTitle688, forSegmentAt: 0)
            self.segmentedControl.setTitle(CommonStrings.Ar.Rewards.HistorySegmentTitle670, forSegmentAt: 1)
        }
        
        override func viewDidLoad() {
            super.viewDidLoad()
            
            if AppSettings.getAppTenant() == .SLI {
                showOnboardingIfNeeded()
            }
            
            self.title = nil
            self.configureTableView()
            navigationController?.makeNavigationBarTransparent()
            
            // Load rewards and partners.
            loadData(withPartners: true)
        }
        
        override func viewDidAppear(_ animated: Bool) {
            super.viewDidAppear(animated)
            flowController.configureController(vc: self)
            
            if AppSettings.getAppTenant() == .SLI {
                if !isLoading {
                    tableView.reloadData()
                }
                isLoading = false
                
                let flowCoordinator = self.flowController as! ARRewardsFlowCoordinator
                guard let _ = flowCoordinator.showRewardController else { return }
                if let hasTappedVoucherLinkOnChosenReward = flowCoordinator.showRewardController?.hasTappedVoucherLink, hasTappedVoucherLinkOnChosenReward {
                    flowCoordinator.showRewardController?.hasTappedVoucherLink = false
                    self.markUsedOrUnusedReward(selectedVoucherTag: selectedTag)
                }

            }
            else {
                tableView.reloadData()
            }
        }
        
        override func viewWillAppear(_ animated: Bool) {
            super.viewWillAppear(animated)
            hideBackButtonTitle()
            
            if AppSettings.getAppTenant() == .SLI {
                let flowCoordinator = self.flowController as! ARRewardsFlowCoordinator
                guard let _ = flowCoordinator.showRewardController
                    else {
                        guard let _ = flowCoordinator.spinController, let _ = flowCoordinator.confirmationController else { return }
                        isLoading = true
                        self.loadRewards()
                        return
                }
                guard let _ = flowCoordinator.spinController
                    else {
                        guard let _ = flowCoordinator.confirmationController else { return }
                        isLoading = true
                        self.loadRewards()
                        return
                }
                guard let _ = flowCoordinator.confirmationController else { return }
                isLoading = true
                self.loadRewards()
            }
        }
        
        func configureTableView() {
            self.tableView.register(VIAARParticipatingPartnerCell.nib(), forCellReuseIdentifier: VIAARParticipatingPartnerCell.defaultReuseIdentifier)
            self.tableView.register(VIAARClaimedRewardCell.nib(), forCellReuseIdentifier: VIAARClaimedRewardCell.defaultReuseIdentifier)
            self.tableView.register(VIAARAvailableRewardCell.nib(), forCellReuseIdentifier: VIAARAvailableRewardCell.defaultReuseIdentifier)
            self.tableView.register(VIATableViewCell.nib(), forCellReuseIdentifier: VIATableViewCell.defaultReuseIdentifier)
            self.tableView.register(VIATableViewSectionHeaderFooterView.nib(), forHeaderFooterViewReuseIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier)
            self.tableView.register(VIAARHistoryHeaderCell.nib(), forHeaderFooterViewReuseIdentifier: VIAARHistoryHeaderCell.defaultReuseIdentifier)
            self.tableView.estimatedRowHeight = 75
        }
        
        // MARK: UITableView datasource
        
        enum Sections: Int, EnumCollection {
            case available = 0
            case usableRewards = 1
            case participatingPartners = 2
            
            func sectionHeaderTitle() -> String? {
                switch self {
                case .available:
                    if VitalityProductFeature.isEnabled(.ARProbabilistic) {
                        return CommonStrings.Ar.Rewards.AvailableSpinsSectionTitle721
                    } else { // if VitalityProductFeature.isEnabled(.ARChoice) {
                        return CommonStrings.Ar.Rewards.ChooseRewardsSectionTitle2629 
                    }
                case .usableRewards:
                    return CommonStrings.Ar.Rewards.UseableRewardsSectionTitle706
                case .participatingPartners:
                    return CommonStrings.Ar.LearnMore.ParticipatingPartnersTitle696
                }
            }
            
            func image() -> UIImage? {
                return nil
            }
        }
        
        override func numberOfSections(in tableView: UITableView) -> Int {
            if historyMode {
                return viewModel.pastRewardsByMonth.0.count
            } else {
                return Sections.allValues.count
            }
        }
        
        override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            if historyMode {
                let (_, monthRewards) = viewModel.pastRewardsByMonth
                if section < monthRewards.count  {
                    return !isDoneLoadingRewards ? 0 : monthRewards[section].count
                }
            } else {
                let theSection = Sections(rawValue: section)
                if theSection == .available {
                    return !isDoneLoadingRewards ? 0 : max(1, viewModel.availableRewardCredits.count)
                } else if theSection == .usableRewards {
                    return self.viewModel.usableRewards.count
                } else if theSection == .participatingPartners {
                    if self.viewModel.usableRewards.count == 0 {
                        if let partners = self.partnersData {
                            return partners.count
                        } else {
                            return self.viewModel.participatingPartners.count
                        }
                    }
                    return 0
                }
                return 0
            }
            return 0
        }
        
        // MARK: UITableView delegate
        
        override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            guard let theSection = Sections(rawValue: indexPath.section) else { return tableView.defaultTableViewCell() }
            if historyMode {
                return self.configureHistoricRewardCell(indexPath)
            } else {
                switch theSection {
                case .available:
                    return self.configureAvailableRewardsCell(indexPath)
                case .participatingPartners:
                    return self.configureParticipatingPartnersCell(indexPath)
                case .usableRewards:
                    return self.configureUsableRewardCell(indexPath)
                }
            }
        }
        
        func configureHelpCell(_ indexPath: IndexPath) -> UITableViewCell {
            let section = Sections(rawValue: indexPath.section)
            let cell = self.tableView.dequeueReusableCell(withIdentifier: VIATableViewCell.defaultReuseIdentifier, for: indexPath) as! VIATableViewCell
            cell.labelText = CommonStrings.HelpButtonTitle141
            cell.cellImage = section?.image()
            cell.accessoryType = .disclosureIndicator
            return cell
        }
        
        func configureAvailableRewardsCell(_ indexPath: IndexPath) -> UITableViewCell {
            let cell = self.tableView.dequeueReusableCell(withIdentifier: VIAARAvailableRewardCell.defaultReuseIdentifier, for: indexPath) as! VIAARAvailableRewardCell
            if viewModel.availableRewardCredits.count == 0 {
                if VitalityProductFeature.isEnabled(.ARProbabilistic) {
                    cell.headingText = CommonStrings.Ar.Rewards.NoAvailableSpinsCellTitle730
                    cell.descriptionText = CommonStrings.Ar.Rewards.NoAvailableSpinsCellDescription681
                } else if VitalityProductFeature.isEnabled(.ARChoice) {
                    cell.headingText = CommonStrings.Ar.Rewards.NoRewardsChooseTitle788
                    cell.descriptionText = CommonStrings.Ar.Rewards.EarnedCellDescription717
                }
                cell.cellImage = UIImage(asset: VIAActiveRewardsAsset.ActiveRewards.Rewards.noActiveReward)
                cell.actionLabelIsHidden = true
                cell.isUserInteractionEnabled = false
            } else {
                let model = viewModel.availableRewardCredits[indexPath.row]
                cell.headingText = CommonStrings.Ar.Rewards.EarnedCellTitle668
                
                if let expiration = model.expirationDate, let achieved = model.earnedDate {
                    let expirationString = Localization.dayDateLongMonthyearFormatter.string(from: expiration)
                    let achievedDateString = Localization.dayDateLongMonthyearFormatter.string(from: achieved)
                    cell.descriptionText = CommonStrings.Ar.Rewards.CreditInfo691(achievedDateString, expirationString)
                }
                
                cell.cellImage = UIImage(asset: VIAActiveRewardsAsset.ActiveRewards.Rewards.activeReward)
                if model.voucher != nil {
                    cell.actionText = CommonStrings.Ar.Rewards.SelectRewardTitle703
                } else {
                    if model.rewardSelectionTypeKey == RewardSelectionTypeRef.WheelSpin.rawValue {
                        cell.actionText = CommonStrings.Ar.Rewards.SpinNowTitle704
                    } else {
                        cell.actionText = CommonStrings.Ar.Rewards.ChooseRewardTitle724
                    }
                }
                cell.actionLabelIsHidden = false
                cell.isUserInteractionEnabled = true
            }
            return cell
        }
        
        func configureParticipatingPartnersCell(_ indexPath: IndexPath) -> UITableViewCell {
            guard let cell = self.tableView.dequeueReusableCell(withIdentifier: VIAARParticipatingPartnerCell.defaultReuseIdentifier, for: indexPath) as? VIAARParticipatingPartnerCell, let partner = partnersData?[indexPath.row] else { return tableView.defaultTableViewCell() }
            
            cell.partnerName = partner.name
            cell.descriptionText = partner.description
            if (partner.name.lowercased().range(of:"starbucks") != nil) {
                cell.descriptionText = CommonStrings.Ar.Partners.StarbucksVoucherValue705
            }
            
            // TODO: Remove this once server can return IGI partner data.
            if partner.name.lowercased().range(of:"easytickets") != nil {
                cell.cellImage = VIAActiveRewardsAsset.ActiveRewards.Rewards.IGIPartners.easyticketsSmall.image
            } else if partner.name.lowercased().range(of:"foodpanda") != nil {
                cell.cellImage = VIAActiveRewardsAsset.ActiveRewards.Rewards.IGIPartners.foodpandaSmall.image
            }
            else {
                DispatchQueue.global(qos: .background).async { [weak self] in
                    self?.partnersViewModel.partnerLogo(partner: partner) { logo, partner in
                        DispatchQueue.main.async {
                            if cell.partnerName == partner.name {
                                cell.cellImage = logo
                            }
                        }
                    }
                }
            }
            cell.isUserInteractionEnabled = true
            return cell
        }
        
        func configureUsableRewardCell(_ indexPath: IndexPath) -> UITableViewCell {
            let cell = self.tableView.dequeueReusableCell(withIdentifier: VIAARClaimedRewardCell.defaultReuseIdentifier, for: indexPath) as! VIAARClaimedRewardCell
            guard
                indexPath.row < viewModel.usableRewards.count else {
                    return cell
            }
            let reward = viewModel.usableRewards[indexPath.row]
            cell.vendorText = reward.title
            cell.descriptionText = reward.subTitle
            cell.cellImage = reward.image
            
            let tenantID = AppSettings.getAppTenant()
            cell.achievedDateLabelIsHidden = tenantID != .SLI
            cell.usedLabelIsHidden = tenantID != .SLI
            cell.radioButtonIsHidden = tenantID != .SLI

            if reward.rewardStatus == .Allocated {
                if (reward.title.lowercased().range(of:"starbucks") != nil){
                    cell.actionLabelIsHidden = false
                    cell.dateText = CommonStrings.CardTitleStarbucksFreeDrinkPending1095
                    cell.actionText = CommonStrings.Ar.Rewards.StarbucksViewReward2140
                } else {
                    cell.actionLabelIsHidden = false
                    cell.dateText = CommonStrings.Ar.Rewards.VoucherCodePending1044
                    cell.actionText = CommonStrings.CardActionTitleViewReward1092
                }
                
            } else {
                if let expires = reward.expiration {
                    if (reward.title.lowercased().range(of:"starbucks") != nil){
                        cell.actionLabelIsHidden = false
                        cell.dateText = CommonStrings.Ar.VoucherExpires658(Localization.dayMonthAndYearFormatter.string(from: expires))//CommonStrings.CardTitleStarbucksFreeDrinkPending1095
                        
                        cell.actionText = CommonStrings.Ar.Rewards.UseRewardTitle720
                    } else {
                        
                        var statusString = CommonStrings.Ar.Rewards.UseRewardTitle720
                        
                        if tenantID != .SLI {
                            if reward.rewardStatus == .AvailableToRedeem {
                                statusString = CommonStrings.CardActionTitleRedeemReward1097
                            }
                        }
                        
                        cell.actionLabelIsHidden = false
                        let dateFormatter = DateFormatter()
                        dateFormatter.locale = Locale(identifier: DeviceLocale.toString())
                        dateFormatter.dateStyle = .long
                        let dateString = dateFormatter.string(from: expires)
                        
                        cell.dateText = CommonStrings.Ar.VoucherExpires658(dateString)
                        cell.actionText = statusString
                    }
                    
                }
                
            }
            
            if tenantID == .SLI {
                
                if let status = reward.rewardStatus {
                    setUpUsedUnusedButtonIfNecessary(status:status, tag: indexPath.row, cell: cell)
                }
                
                if let achieved = reward.effectiveFromDate {
                    let dateFormat = Localization.dayDateLongMonthyearFormatter.string(from: achieved)
                    cell.achievedDateText = CommonStrings.Ar.VoucherTargetAchieved2148(dateFormat)

                }
            }
            
            cell.isUserInteractionEnabled = true
            return cell
        }
        
        func configureHistoricRewardCell(_ indexPath: IndexPath) -> UITableViewCell {
            let cell = self.tableView.dequeueReusableCell(withIdentifier: VIAARClaimedRewardCell.defaultReuseIdentifier, for: indexPath) as! VIAARClaimedRewardCell
            
            let (_, monthRewards) = viewModel.pastRewardsByMonth
            cell.cellImage = nil
            if indexPath.section < monthRewards.count, indexPath.item < monthRewards[indexPath.section].count {
                let reward = monthRewards[indexPath.section][indexPath.row]
                cell.vendorText = reward.title
                cell.descriptionText = reward.subTitle
                cell.cellImage = reward.image

                if reward.rewardStatus?.rawValue == AwardedRewardStatusRef.Used.rawValue {
                    if var used = reward.effectiveFromDate {
                        if AppSettings.getAppTenant() == .SLI || AppSettings.getAppTenant() == .EC || AppSettings.getAppTenant() == .ASR {
                            if let effectiveOn = reward.awardedRewardStatusEffectiveOn {
                                used = effectiveOn
                            }
                        }
                        let dateString = Localization.dayDateLongMonthyearFormatter.string(from: used)
                        cell.dateText = CommonStrings.Ar.VoucherUsed671(dateString)
                    }
                } else if let expired = reward.expiration {
                    let dateString = Localization.dayDateLongMonthyearFormatter.string(from: expired)
                    cell.dateText = CommonStrings.Ar.VoucherExpired689(dateString)
                }
                
                cell.isUserInteractionEnabled = AppSettings.getAppTenant() != .EC && AppSettings.getAppTenant() != .ASR
                cell.actionLabelIsHidden = true
                cell.achievedDateLabelIsHidden = true
                cell.usedLabelIsHidden = true
                cell.radioButtonIsHidden = true
            }
            
            return cell
        }
        
        override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
            if historyMode {
                return configureHistoricRewardHeader(section)
            } else {
                return configureCurrentHeader(section)
            }
        }
        
        func configureHistoricRewardHeader(_ section: Int) -> UIView? {
            
            let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: VIAARHistoryHeaderCell.defaultReuseIdentifier) as? VIAARHistoryHeaderCell
            
            let (months, _) = viewModel.pastRewardsByMonth
            guard section < months.count else { return nil }
            
            view?.title = months[section]
            
            return view
        }
        
        func configureCurrentHeader(_ section: Int) -> UIView? {
            let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier) as! VIATableViewSectionHeaderFooterView
            
            if Sections(rawValue: section) == .usableRewards, self.viewModel.usableRewards.count == 0 {
                return nil
            } else if Sections(rawValue: section) == .participatingPartners, self.viewModel.usableRewards.count > 0 {
                return nil
            }
            
            guard let title = Sections(rawValue: section)?.sectionHeaderTitle() else {
                return nil
            }
            
            view.configureWithExtraSpacing(labelText: title, extraTopSpacing: 28, extraBottomSpacing: 0)
            return view
        }
        
        override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
            if !historyMode, configureCurrentHeader(section) == nil {
                return CGFloat.leastNormalMagnitude
            }
            return UITableViewAutomaticDimension // historyMode and if there is a header
        }
        
        override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
            return CGFloat.leastNormalMagnitude
        }
        
        override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return UITableViewAutomaticDimension
        }
        
        override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            
            if !historyMode {
                guard let theSection = Sections(rawValue: indexPath.section) else { return }
                switch theSection {
                case .available:
                    let selectedCredit = viewModel.availableRewardCredits[indexPath.row]
                    flowController.useCredit(selectedCredit)
                case .participatingPartners:
                    if (self.partnersData?[indexPath.row]) != nil {
                        
                        let shouldUseEligibility = VIAApplicableFeatures.default.shouldUseGetEligibility!
                        
                        if shouldUseEligibility {
                            performSegue(withIdentifier: "showParticipatingPartner", sender: shouldUseEligibility)
                        } else {
                            if AppSettings.getAppTenant() == .EC || AppSettings.getAppTenant() == .ASR {
                                // Use table view class for Ecuador for better implementation of the added 'Terms and Conditions' row.
                                performSegue(withIdentifier: "showARPartnerDetailTable", sender: nil)
                            } else {
                                performSegue(withIdentifier: "showARPartnerDetail", sender: shouldUseEligibility)
                            }
                        }
                    }
                case .usableRewards:
                    let selectedVoucher = viewModel.usableRewards[indexPath.row]
                    selectedTag = indexPath.row
                    flowController.showReward(selectedVoucher)
                }
            }
            
            tableView.deselectRow(at: indexPath, animated: true)
        }
        
        // MARK: Actions
        
        @IBAction func toggleRewards(_ sender: Any) {
            if let control = sender as? UISegmentedControl{
                historyMode = control.selectedSegmentIndex == 1
                
                if historyMode {
                    self.loadHistory()
                    /* Disable the call of configureEmptyStatusView for SLI
                     * Fix for VA-37962
                     */
                    if VIAApplicableFeatures.default.enabledEmptyStatusView() {
                        configureEmptyStatusView()
                    }
                    debugPrint("ARsegmentedView - HISTORY TAB")
                } else {
                    self.loadRewards()
                    removeEmptyStatusView()
                    debugPrint("ARsegmentedView - CURRENT TAB")
                }
                self.tableView.reloadData()
            }
        }
        
        
        override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
            super.prepare(for: segue, sender: sender)
            
            if let identifier = segue.identifier, identifier == "showARPartnerDetail" || identifier == "showParticipatingPartner" {
                let sender = sender as! Bool
                if sender {
                    guard let indexPath = tableView.indexPathForSelectedRow, let partner = self.partnersData?[indexPath.row], let detailVC = segue.destination as? ParticipatingPartnerDetailsViewController else { return }
                    detailVC.viewModel = ParticipatingPartnersDetailsViewModel(for: partner)
                } else {
                    guard let indexPath = tableView.indexPathForSelectedRow, let partner = self.partnersData?[indexPath.row], let detailVC = segue.destination as? VIAARPartnerDetailViewController else { return }
                    detailVC.partnerViewModel = partnersViewModel
                    detailVC.partner = partner
                }
            } else if let identifier = segue.identifier, identifier == "showARPartnerDetailTable" {
                guard let indexPath = tableView.indexPathForSelectedRow, let partner = self.partnersData?[indexPath.row], let detailVC = segue.destination as? VIAARPartnerDetailTableViewController else { return }
                detailVC.partnerViewModel = partnersViewModel
                detailVC.partner = partner
            }
        }
        
        // MARK: - Networking
        
        func loadHistory() {
            loadData(startDate: getHistoryStartDate())
        }
        
        private func getHistoryStartDate() -> Date{
            return Calendar.current.date(byAdding: .month, value: -6, to: Date())?.startOf(component: .month) ?? Date()
        }
        
        func loadRewards() {
            if AppSettings.getAppTenant() == .SLI {
                loadData(withPartners: true)
            } else {
                loadData()
            }
        }
        
        func loadData(withPartners: Bool = false, startDate: Date = Date(), endDate: Date = Date()) {
            
            if isDoneLoadingRewards{
                
                showHUDOnView(view: self.view)
                if withPartners {
                    isDoneLoadingPartners = false
                }else{
                    isDoneLoadingRewards = false
                }
                
                let tenantId = DataProvider.newRealm().getTenantId()
                let partyId = DataProvider.newRealm().getPartyId()
                
                Wire.Rewards.getAwardedRewardByPartyId(tenantId: tenantId, partyId: partyId, fromDate: startDate, toDate: endDate, linkedKeys: []) { [weak self] (rewardsRequestError, response) in
                    self?.isDoneLoadingRewards = true
                    self?.checkIfStillLoading()
                    guard rewardsRequestError == nil else {
                        if let backendError = rewardsRequestError as? BackendError {
                            self?.handleBackendErrorWithAlert(backendError, tryAgainAction: { [weak self] in
                                self?.loadData(withPartners: withPartners, startDate: startDate, endDate: endDate)
                            })
                        }
                        self?.isDoneLoadingPartners = true
                        return
                    }
                    
                    if let strongSelf = self {
                        strongSelf.flowController.configureController(vc: strongSelf)
                        DispatchQueue.main.async{ [weak self] in
                            if AppSettings.getAppTenant() == .SLI {
                                UIView.performWithoutAnimation {
                                    let contentOffset = self?.tableView.contentOffset
                                    self?.tableView.reloadData()
                                    self?.tableView.layoutIfNeeded()
                                    if let prevOffset = contentOffset {
                                        self?.tableView.setContentOffset(prevOffset, animated: false)
                                    }
                                    else {
                                        self?.tableView.setContentOffset(.zero, animated: false)
                                    }
                                    self?.executeInstances(withPartners: withPartners)
                                }
                            }
                            else {
                                self?.tableView.reloadData()
                                self?.executeInstances(withPartners: withPartners)
                            }
                        }
                    }
                    
                    return
                }
            }
        }
        
        func executeInstances(withPartners: Bool) {
            // Load partners only the on the first instance.
            if withPartners {
                self.loadPartners()
            }
            
            /* Only execute below condition for SLI Market.
             * Fix for VA-37962
             */
            if !VIAApplicableFeatures.default.enabledEmptyStatusView() {
                /* Identify if History Mode is True */
                if self.historyMode {
                    /* If there is no pastRewards, show the status view. */
                    guard self.viewModel.pastRewardsByMonth.1.count == 0 else {
                        self.removeEmptyStatusView()
                        return
                    }
                    
                    self.configureEmptyStatusView()
                }
            }
        }
        
        func loadPartners() {
            partnersViewModel.requestPartnersByCategory(completion: { [weak self] (error) in
                self?.isDoneLoadingPartners = true
                guard error == nil else {
                    
                    self?.handleError(error)
                    return
                }
                self?.partnersViewModel.configurePartnerData()
                let partnersCount = self?.partnersViewModel.partnerGroupsData.count ?? 0
                
                if partnersCount > 0 {
                    guard let partnersDatas = self?.partnersViewModel.partnerGroupsData[0].partners else { return }
                    
                    if VIAApplicableFeatures.default.removeAppleWatchOnPartnersData() {
                        var partnersDataWithoutAppleWatch: [ParticipatingPartnersViewModel.PartnerData] = []
                        
                        for partnerData in partnersDatas {
                            if partnerData.typeKey != 223 {
                                partnersDataWithoutAppleWatch.append(partnerData)
                            }
                        }
                        self?.partnersData = partnersDataWithoutAppleWatch
                    } else {
                        self?.partnersData = partnersDatas
                    }
                }
                DispatchQueue.main.async{ [weak self] in
                    self?.tableView.reloadData()
                    self?.checkIfStillLoading()
                }
            })
        }
        
        func checkIfStillLoading() {
            if isDoneLoadingPartners && isDoneLoadingRewards  {
                hideHUDFromView(view: self.view)
            }
        }
        
        func handleError(_ error: Error?) {
            let statusView = handleBackendErrorWithStatusView((error as? BackendError) ?? .other, tryAgainAction: { [weak self] in
                self?.loadPartners()
            })
            configureStatusView(statusView)
        }
    }
    
    extension VIAARRewardsLandingViewController {
        func configureEmptyStatusView() {
            removeEmptyStatusView()
            
            let view = VIAStatusView.viewFromNib(owner: self)!
            
            view.heading = CommonStrings.History.EmptyStateTitle250
            view.message = CommonStrings.Ar.NoHistoryMessage1155
            
            self.view.addSubview(view)
            
            view.snp.makeConstraints { (make) in
                make.width.equalToSuperview()
                make.left.right.top.equalToSuperview()
                make.height.equalToSuperview().offset(-62)
                make.bottom.equalToSuperview().offset(-62)
            }
        }
        
        func removeEmptyStatusView() {
            self.view.subviews.forEach { (view) in
                if view is VIAStatusView {
                    view.removeFromSuperview()
                }
            }
        }
    }

    // MARK: Mark as Used / Unused
    extension VIAARRewardsLandingViewController {
        func setUpUsedUnusedButtonIfNecessary (status: AwardedRewardStatusRef, tag: Int, cell: VIAARClaimedRewardCell){
            
            let usedUnusedImage = status == .AvailableToRedeem ? VIAActiveRewardsAsset.ActiveRewards.Rewards.MarkAsUsed.markAsUsed : VIAActiveRewardsAsset.ActiveRewards.Rewards.MarkAsUnused.markAsUnused
            
            cell.usedText = CommonStrings.Ar.Voucher.RadioButtonHeader2426
            cell.radioButton.setImage(UIImage(asset: usedUnusedImage), for: .normal)
            cell.radioButton.tag = tag
            cell.radioButton.addTarget(self, action: #selector(radioButtonIsTapped), for: .touchUpInside)
        }
        
        func radioButtonIsTapped (sender:UIButton!) {
            markUsedOrUnusedReward(selectedVoucherTag: sender.tag)
        }
        
        func markUsedOrUnusedReward (selectedVoucherTag: Int) {
            let title = CommonStrings.Ar.Voucher.PopupHeader2427
            let message = CommonStrings.Ar.Voucher.PopupBody2428
            let markeAsUsedButton  = CommonStrings.Ar.Voucher.PopupButtonUsed2429
            let markeAsUnusedButton  = CommonStrings.Ar.Voucher.PopupButtonUnused2430
            let reward = viewModel.usableRewards[selectedVoucherTag]
            let currentStatus = reward.rewardStatus ?? AwardedRewardStatusRef.Unknown
            
            let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: markeAsUsedButton, style: .default, handler: { (action) -> Void in
                if !self.isRadiotButtonSelected(status: currentStatus) && currentStatus != AwardedRewardStatusRef.Unknown {
                    let toBeStatus = AwardedRewardStatusRef.AvailableToRedeem
                    self.updateReward(reward: reward, status: toBeStatus)
                }
            }))
            
            alert.addAction(UIAlertAction(title: markeAsUnusedButton, style: .default, handler: { (action) -> Void in
                if self.isRadiotButtonSelected(status: currentStatus) && currentStatus != AwardedRewardStatusRef.Unknown {
                    let toBeStatus = AwardedRewardStatusRef.Issued
                    debugPrint(reward)
                    self.updateReward(reward: reward, status: toBeStatus)
                }
            }))

            self.view.window!.rootViewController!.present(alert, animated: true, completion: nil)
        }
        
        func isRadiotButtonSelected(status: AwardedRewardStatusRef) -> Bool {
            if status == .AvailableToRedeem {
                return true
            } else {
                return false
            }
        }
        
        func updateReward (reward: VIAARRewardViewModel, status: AwardedRewardStatusRef) {
            showHUDOnView(view: self.view)
            
            viewModel.updateRewardStatusToUsedOrUnused(reward, status: status, completion: { [weak self] (error, response) in
                if error != nil {
                    self?.hideHUDFromView(view: self?.view)
                    self?.showGenericError()
                    return
                } else {
                    self?.viewModel.submitEventForRewardStatusChange(reward, status: status, completion: { [weak self] (error) in
                        self?.hideHUDFromView(view: self?.view)
                        if error != nil {
                            self?.showGenericError()
                            return
                        }
                    })
                }
                self?.loadRewards()
            })
        }
    }

    // MARK: Show Onboarding once
    extension VIAARRewardsLandingViewController {
        func showOnboardingIfNeeded() {
            if !AppSettings.hasShownRewardsOnboarding() {
                self.performSegue(withIdentifier: "showOnboarding", sender: self)
            }
        }
    }

    // MARK: Error Message
    extension VIAARRewardsLandingViewController{
        func showGenericError() {
            
            let controller = UIAlertController(title: CommonStrings.GenericServiceErrorTitle268,
                                               message: CommonStrings.GenericServiceErrorMessage269,
                                               preferredStyle: .alert)
            let ok = UIAlertAction(title: CommonStrings.OkButtonTitle40, style: .default) { action in }
            controller.addAction(ok)
            present(controller, animated: true, completion: nil)
        }
    }

