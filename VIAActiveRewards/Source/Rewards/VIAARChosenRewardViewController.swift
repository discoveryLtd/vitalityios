//
//  VIAARChosenRewardViewController.swift
//  VitalityActive
//
//  Created by Joshua Ryan on 6/12/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

import UIKit
import VIAUIKit
import VitalityKit
import VIACommon

protocol ARRewardConsumptionDelegate: ARRewardFlowDelegate {
	func useReward(_ reward: VIAARRewardViewModel, consumer: VIAViewController?, completion: @escaping (Error?) -> Void)
}


public class VIAARChosenRewardViewController: VIACoordinatedViewController, ImproveYourHealthTintable {

	// MARK: Public

	var viewModel: VIAARRewardViewModel?
	weak var delegate: ARRewardConsumptionDelegate?
    public var parentJourney: ARRewardsParentJourney?
    private var flowController: ARLandingNavDelegate = ARRewardsFlowCoordinator()
    
	// MARK: Private

	private var chosenView: VIAARRewardChosenView?
    public var hasTappedVoucherLink: Bool = false

	// MARK: Lifecycle

	override public func viewDidLoad() {
		super.viewDidLoad()
        
		navigationController?.navigationBar.isHidden = false
		configureChosenRewardView()
		configureBarButtons()
	}

	func configureChosenRewardView() {

		if let viewModel = viewModel {

			let chosenView = VIAARRewardChosenView.viewFromNib(owner: self)!

			chosenView.frame = .zero
			chosenView.image = viewModel.image
			chosenView.header = viewModel.title
			chosenView.content = viewModel.subTitle
            
            let tenantID = AppSettings.getAppTenant()
            chosenView.achievedDateIsHidden = tenantID != .SLI
            
            if let effectiveFromDate = viewModel.effectiveFromDate{
                let dateFormat = Localization.dayDateLongMonthyearFormatter.string(from: effectiveFromDate)
                chosenView.achievedDate = CommonStrings.Ar.VoucherTargetAchieved2148(dateFormat)
            }

            /**
            * If sliWebDirect is true, display the vourcher partnerSysRewardId
            * instead of the voucher code
            **/
            if let sliWebDirect = VIAApplicableFeatures.default.redirectToWebVoucher, sliWebDirect {
                if (viewModel.partnerSysRewardId?.first) != nil{
                    //chosenView.voucherCode = url
                    chosenView.voucherCodeLabel.text = nil
                    chosenView.voucherLabel.attributedText = NSAttributedString(string: "\(CommonStrings.Ar.VoucherCodeClickableContent2147)", attributes:
                        [NSUnderlineStyleAttributeName: NSUnderlineStyle.styleSingle.rawValue])
                    let linkTap = UITapGestureRecognizer(target: self, action: #selector(voucherLinkTapped))
                    chosenView.voucherLabel.isUserInteractionEnabled = true
                    chosenView.addGestureRecognizer(linkTap)
                }
            } else {
                if let code = viewModel.voucherCodes?.first {
                    chosenView.voucherCode = code
                }
                if let instructions = viewModel.instructions {
                    chosenView.instructions = instructions
                }
            }
			
			chosenView.layoutMargins = UIEdgeInsets(top: 0, left: 23.0, bottom: 0, right: 23.0)

			self.view.addSubview(chosenView)

			chosenView.snp.makeConstraints({ (make) in
				make.top.left.bottom.right.equalToSuperview()
			})

			self.chosenView = chosenView

		}
	}

	// MARK: Action buttons
    
    @objc
    func voucherLinkTapped(sender:UITapGestureRecognizer) {
        print("tap working")
        self.hasTappedVoucherLink = true
        self.performSegue(withIdentifier: "showWebVoucher", sender: nil)
    }
	func configureBarButtons() {
		let rightItem = UIBarButtonItem(title: CommonStrings.DoneButtonTitle53, style: .plain, target: self, action: #selector(doneButtonTapped(_:)))
		navigationItem.rightBarButtonItem = rightItem
		navigationItem.leftBarButtonItem = nil
		navigationItem.hidesBackButton = true
	}

	func doneButtonTapped (_ sender: UIBarButtonItem) {
        guard let unwindTo = parentJourney else {
            handleDoneTappedInRewardJourney()
            return
        }
        
        switch unwindTo {
        case .HomeCard:
            unwindToHomeScreen(sender)
            return
        default:
            handleDoneTappedInRewardJourney()
            return
        }
        

	}
    
    func unwindToHomeScreen(_ sender: Any?) {
        self.performSegue(withIdentifier: "unwindToHomeViewController", sender: nil)
    }
    
    func handleDoneTappedInRewardJourney() {
        if let landing = self.navigationController?.viewControllers.filter({$0 is VIAARRewardsLandingViewController}).first {
            
            self.navigationController?.popToViewController(landing, animated: true)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
    override public func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showWebVoucher", let controller = segue.destination as? VIAWebVoucherViewController {
            controller.setHTMLContent(as: (viewModel?.partnerSysRewardId?.first)!)
            controller.title = viewModel?.title
        }
    }
}
