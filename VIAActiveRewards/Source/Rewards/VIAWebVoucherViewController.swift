import UIKit
import WebKit
import SnapKit

import VIAUIKit
import VitalityKit
import VIACommon
import WebKit
import SafariServices

public class VIAWebVoucherViewController: VIAViewController, UIWebViewDelegate,
SafariViewControllerPresenter, SafariViewControllerEscapable {
    
    @IBOutlet var webView: WKWebView!
    internal var htmlContent: String?
    public var parentJourney: ARRewardsParentJourney?
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.makeNavigationBarInvisibleWithoutTint()
        self.configureWebView()
        self.configureBarButtons()
        self.configureAppearance()
        self.loadContent()
    }
    
    open override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    func configureAppearance() {
        navigationController?.makeNavigationBarTransparent()
        self.webView.backgroundColor = .white
        self.webView.isOpaque = true
    }
    
    func configureBarButtons() {
        let rightItem = UIBarButtonItem(title: CommonStrings.DoneButtonTitle53, style: .plain, target: self, action: #selector(doneButtonTapped(_:)))
        navigationItem.rightBarButtonItem = rightItem
        navigationItem.leftBarButtonItem = nil
        navigationItem.hidesBackButton = true
    }
    
    func doneButtonTapped (_ sender: UIBarButtonItem) {
        guard let unwindTo = parentJourney else {
            handleDoneTappedInRewardJourney()
            return
        }
        
        switch unwindTo {
        case .HomeCard:
            unwindToHomeScreen(sender)
            return
        default:
            handleDoneTappedInRewardJourney()
            return
        }
    }
    
    func unwindToHomeScreen(_ sender: Any?) {
        self.performSegue(withIdentifier: "unwindToHomeViewController", sender: nil)
    }
    
    func handleDoneTappedInRewardJourney() {
        if let landing = self.navigationController?.viewControllers.filter({$0 is VIAARRewardsLandingViewController}).first {
            
            self.navigationController?.popToViewController(landing, animated: true)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func configureWebView() {
        let pref = WKPreferences()
        pref.javaScriptEnabled = true
        pref.javaScriptCanOpenWindowsAutomatically = true
        
        /* Create a config using pref*/
        let configuration = WKWebViewConfiguration()
        configuration.preferences = pref
        
        webView = WKWebView(frame: .zero, configuration: configuration)
        webView.uiDelegate = self
        webView.navigationDelegate = self
        view = webView
    }
    
    public func setHTMLContent(as html: String) {
        htmlContent = html
    }
    
    func loadContent() {
        self.showHUDOnView(view: self.webView)
        if let htmlContent = htmlContent, let url = URL(string: htmlContent) {
            self.webView.load(URLRequest(url: url))
        }
    }
}

extension VIAWebVoucherViewController: WKNavigationDelegate{
    
    public func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        
        guard let destinationURL = navigationAction.request.url, let scheme = destinationURL.scheme else {
            decisionHandler(.allow)
            return
        }

        guard ["http", "https"].contains(scheme.lowercased()) else {
            decisionHandler(.allow)
            return
        }

        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            self.presentModally(url: destinationURL, parentViewController: self, delegate: self)
            decisionHandler(.cancel)
        }
    }

    public func webView(_ webView: WKWebView, decidePolicyFor navigationResponse: WKNavigationResponse, decisionHandler: @escaping (WKNavigationResponsePolicy) -> Swift.Void){
        
        guard let response:HTTPURLResponse = navigationResponse.response as? HTTPURLResponse else {return}
        
        /* If authentication is needed, we will load the URL to SFSafariViewController to handle popups. */
        if response.statusCode == 401{
            guard let url = response.url else {return}
            self.presentModally(url: url, parentViewController: self, delegate: self)
        }
        decisionHandler(.allow)
    }
    
    public func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        self.hideHUDFromView(view: self.webView)
    }
}

extension VIAWebVoucherViewController: WKUIDelegate{
    
    public func webView(_ webView: WKWebView, createWebViewWith configuration: WKWebViewConfiguration, for navigationAction: WKNavigationAction, windowFeatures: WKWindowFeatures) -> WKWebView?{
        
        /* This will redirect the URL to SFSafariViewController so we could handle popups */
        guard let url = navigationAction.request.url else {return nil}
        self.presentModally(url: url, parentViewController: self, delegate: self)
        
        return nil
    }
}

extension VIAWebVoucherViewController: SFSafariViewControllerDelegate{
    
    public func safariViewControllerDidFinish(_ controller: SFSafariViewController){
        handleDoneTappedInRewardJourney()
    }
}

