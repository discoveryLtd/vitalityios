import VitalityKit
import VIAUtilities
import VIACommon

public class VIAARRewardHelper {
    public init() {}
    
    private var completion: ((Error?) -> (Void)) = { error in
        debugPrint("Complete")
    }

    static public func confirmedReward(reward: VIAARRewardViewModel, completion: @escaping (_ error: Error?) -> Void) {
        let coreRealm = DataProvider.newRealm()
        let tenantId = coreRealm.getTenantId()
        let partyId = coreRealm.getPartyId()

        // ProcessEventsV2 - PartyReferenceEvent
        let eventSourceTypeKey: EventSourceRef = .MobileApp
        let eventCapturedOn = Date()
        let eventOccuredOn = Date()
        let applicableTo = ProcessEventsV2ApplicableTo(referenceTypeKey: eventSourceTypeKey.rawValue,
                                                       value: String(partyId))
        let reportedBy = ProcessEventsV2PartyReferenceEventsReportedBy(referenceTypeKey: eventSourceTypeKey.rawValue,
                                                                       value: String(partyId))
        let typeKey = EventTypeRef.RewardDSAgree
        
        var eventMetaDatas = [ProcessEventsV2EventMetaData]()
        
        let rewardName = ProcessEventsV2EventMetaData(typeKey: EventMetaDataTypeRef.RewardName.rawValue,
                                                      value: reward.title,
                                                            unitOfMeasure: nil)
        eventMetaDatas.append(rewardName)
        
        let instructionkey = "2"
        let instructionKey = ProcessEventsV2EventMetaData(typeKey: EventMetaDataTypeRef.InstructionTypeKey.rawValue,
                                                          value: instructionkey,
                                                          unitOfMeasure: nil)
        eventMetaDatas.append(instructionKey)
        
        guard let key = reward.rewardkey else { return }
        let rewardKey = ProcessEventsV2EventMetaData(typeKey: EventMetaDataTypeRef.RewardKey.rawValue,
                                                     value: "\(key)",
                                                      unitOfMeasure: nil)
        eventMetaDatas.append(rewardKey)

        let processEvent = ProcessEventsV2PartyReferenceEvent(applicableTo: applicableTo,
                                                              associatedEvents: [],
                                                              eventCapturedOn: eventCapturedOn,
                                                              eventExternalReference: nil,
                                                              eventId: nil,
                                                              eventMetaDatas: eventMetaDatas,
                                                              eventOccurredOn: eventOccuredOn,
                                                              eventSourceTypeKey: eventSourceTypeKey,
                                                              reportedBy: reportedBy,
                                                              typeKey: typeKey.rawValue)
        
        var events = [ProcessEventsV2PartyReferenceEvent]()
        events.append(processEvent)
        
        Wire.Events.processEventsV2(tenantId: tenantId, events: events, completion: { (response, error) in
            guard error == nil else {
                completion(error)
                return
            }
            completion(nil)
        })
    }
    
    /** FC-26793 : UKE : Change Request
     * Added VoucherNumber to the Completion Block.
     * VoucherNumber is required to show the Voucher Code.
     */
    static public func selectReward(with reward: VIAARRewardViewModel, with type: ExchangeTypeRef, completion: @escaping (_ error: Error?, _ newRewardId: Int?, _ voucherNumber: String?) -> ()) {
        let coreRealm = DataProvider.newRealm()
        let tenantId = coreRealm.getTenantId()
        
        guard let linkId = reward.linkId, let unclaimedRewardId = reward.unclaimedRewardId else {
            completion(BackendError.other, nil, nil)
            return
        }
        
        Wire.Rewards.exchangeReward(tenantId: tenantId,
                                    awardedRewardId: unclaimedRewardId,
                                    rewardValueLinkId: linkId,
                                    type: type) { (error, newRewardId, voucherNumber) in
                                        guard error == nil else {
                                            completion(error, nil, nil)
                                            return
                                        }
                                        completion(nil, newRewardId, voucherNumber)
                                        return
        }
    }
    
    static public func updateRewardStatusToUsed(reward: VIAARRewardViewModel, completion: @escaping (_ error: Error?, _ response: UpdateRewardStatusResponse?) -> Void) {
        let coreRealm = DataProvider.newRealm()
        coreRealm.refresh()
        let tenantId = coreRealm.getTenantId()
        let partyId = coreRealm.getPartyId()
        let usedStatus = AwardedRewardStatusRef.Used
        guard let unclaimedRewardId = reward.unclaimedRewardId else {
            completion(BackendError.other, nil)
            return
        }
        Wire.Rewards.updateRewardStatus(tenantId: tenantId, partyId: partyId, awardedRewardId: unclaimedRewardId, status: usedStatus) { error, response in
            guard error == nil else {
                completion(error, nil)
                return
            }

            completion(nil, response)
        }
    }
    
    static public func updateRewardStatusToAcknowledged(reward: VIAARRewardViewModel, completion: @escaping (_ error: Error?, _ response: UpdateRewardStatusResponse?) -> Void) {
        let coreRealm = DataProvider.newRealm()
        coreRealm.refresh()
        let tenantId = coreRealm.getTenantId()
        let partyId = coreRealm.getPartyId()
        let usedStatus = AwardedRewardStatusRef.Acknowledged
        guard let unclaimedRewardId = reward.unclaimedRewardId else {
            completion(BackendError.other, nil)
            return
        }
        
        Wire.Rewards.updateRewardAcknowledgedStatus(tenantId: tenantId, partyId: partyId, awardedRewardId: unclaimedRewardId, status: usedStatus) { error, response in
            guard error == nil else {
                completion(error, nil)
                return
            }
            
            completion(nil, response)
        }
    }
    
    static public func getValueForReward(_ reward: ARRewardsSelection) -> String {
        /*
         * Ahmed Varachia:
         * As per the attached config sheet shouldn't this be:
         * Reward Reward Category Reward Type Provider Value Type Value Description
         * Name of the Reward Chose a reward category Chose a type of reward Reward provider name 
         * The type of value for the reward The value of the reward. If Value Type = Percentage, 
         * this value will be a percentage value. If Value type = SpecificItem, this value will
         * be a quantity.It isnot neccesasry to include the currancy for monetory amounts 
         * "The descriptions will only be needed if the reward type is a specificItem e.g. Cup of coffee.
         * 
         * DO NOT ENTER A DESCRIPTION IF THE VALUE TYPE IS NOT SPECIFIC ITEM."
         * MACHI café drink(S) FoodAndBeverage Voucher Lawson SpecificItem 1 MACHI café drink(S)
         * Smoothies FoodAndBeverage Voucher Lawson SpecificItem 1 Smoothies
         * eGift (JPY500) FoodAndBeverage Voucher Starbucks Coffee Japan Monetary 500 eGift (JPY500)
         */
        var value = ""
        /* Checking of Value Percentage show go first */
        if reward.rewardValueTypeKey == RewardValueTypeRef.Percentage.rawValue,
            let rewardPercent = reward.rewardValuePercent.value {
            value = String(describing: rewardPercent)
        }
        /* If it is not a Percentage, check if it is a Value Amount */
        else if reward.rewardValueTypeKey == RewardValueTypeRef.Monetary.rawValue,
            let rewardAmount = reward.rewardValueAmount {
            value = rewardAmount
        }
        /* Last to be checked if it is Value Quantity */
        else if reward.rewardValueTypeKey == RewardValueTypeRef.SpecificItem.rawValue,
            let rewardQuantity = reward.rewardValueQuantity.value {
            if (rewardQuantity != 0){
                value = String(describing: rewardQuantity)
            }
        }
        
        var type = ""
        if reward.rewardValueTypeKey == RewardValueTypeRef.SpecificItem.rawValue {
            type = reward.rewardValueItemDescription ?? ""
        } else {
            type = reward.rewardTypeName ?? ""
        }
        
        switch reward.rewardId {
        case .Cineworld, .CineworldOrVue, .Vue:
            return  "\(value) \(type)"
        case .StarbucksVoucher:
            if (value == "1") {
                return type
            } else {
                return "\(value) \(type)"
            }
        case .EGiftStarbucks, .MachiLawson, .SmoothieLawson, .YogurtLawson:
            if VIAApplicableFeatures.default.hideRewardsSubtitle(){
                return ""
            }
            return  "\(value) \(type)"
        case .Cinemark:
            return CommonStrings.Ar.Rewards.CinemarkAppDescription2131
        case .JuanValdez:
            return CommonStrings.Ar.Rewards.JuanValdezAppDescription2579
        default:
            if (value == "") {
                return type
            } else {
                return "\(value) \(type)"
            }
        }
    }
    
    static public func getReward(_ rewardId: Int, completion: @escaping (Error?, VIAARRewardViewModel?) -> Void) {
        let coreRealm = DataProvider.newRealm()
        let tenantId = coreRealm.getTenantId()
        
        Wire.Rewards.getAwardedRewardById(tenantId: tenantId, awardedRewardId: rewardId) { (error) in
            let arRealm = DataProvider.newARRealm()
            arRealm.refresh()
            guard let voucher = arRealm.objects(ARVoucher.self).filter("id == %@", rewardId).first else {
                return completion(nil, nil)
            }
            let subTitle = VIAARRewardHelper.getValueForVoucher(voucher)
            let bespokeRewardBoardId = VIAARRewardHelper.bespokeViewController(for: voucher.rewardId)
            /** FC-26793 : UKE : Change Request
             * Added "exchangeOn" to determine if the selected Reward has already called the "exchangeReward" api.
             * Added "awardedRewardReferences" to get the Voucher Codes of the Reward.
             */
            
            var rewardImage: UIImage
            if getFeatureGroupValue().lowercased() == "india" {
                rewardImage = VIAARRewardHelper.partnerImage(for: RewardReferences.Unknown)
            } else {
                rewardImage = VIAARRewardHelper.partnerImage(for: voucher.rewardId)
            }
            
            let dataProvider = VIAARRewardViewModel(id: voucher.id, unclaimedRewardId: nil,
                                                    subTitle: subTitle,
                                                    title: voucher.rewardName,
                                                    image: rewardImage,
                                                    voucherId: voucher.id,
                                                    voucherCodes: voucher.voucherCodes(),
                                                    partnerSysRewardId: voucher.partnerSysRewardIds(),
                                                    instructions: VIAARRewardHelper.partnerRewardInstructions(for: AwardedRewardStatusRef(rawValue: voucher.awardedRewardStatus), rewardValue: subTitle, effectiveFromDate: voucher.effectiveFrom),
                                                    expiration: voucher.effectiveTo,
                                                    effectiveFromDate: voucher.effectiveFrom,
                                                    effectiveToDate: voucher.effectiveTo,
                                                    accepted: voucher.awardedOn,
                                                    bespokeRewardBoardId: bespokeRewardBoardId,
                                                    linkId: voucher.rewardValueLinkId,
                                                    rewardReference: voucher.rewardId,
                                                    rewardStatus: AwardedRewardStatusRef(rawValue: voucher.awardedRewardStatus),
                                                    rewardkey: voucher.rewardkey,
                                                    awardedRewardStatusEffectiveOn: voucher.awardedRewardStatusEffectiveOn,
                                                    exchangeOn: voucher.exchangeExchangeOn,
                                                    awardedRewardReferences: voucher.voucherCodes())
            return completion(error, dataProvider)
        }
    }
    
    static public func getCredit(_ rewardId: Int, completion: @escaping (Error?, VIAARCreditViewModel?) -> Void) {
        let coreRealm = DataProvider.newRealm()
        let tenantId = coreRealm.getTenantId()
        
        Wire.Rewards.getAwardedRewardById(tenantId: tenantId, awardedRewardId: rewardId) { (error) in
            let arRealm = DataProvider.newARRealm()
            arRealm.refresh()
            guard let credit = arRealm.objects(ARUnclaimedReward.self).filter("id == %@", rewardId).first else {
                return completion(nil, nil)
            }
            
            let dataProvider = VIAARCreditViewModel(id: credit.id,
                                                    earnedDate: credit.effectiveFrom,
                                                    expirationDate: credit.effectiveTo,
                                                    used: false,
                                                    voucher: nil,
                                                    winningVoucherId: credit.outcomeRewardValueLinkId.value,
                                                    rewardSelectionTypeKey: credit.rewardSelectionTypeKey.value)
            return completion(error, dataProvider)
        }
    }
        
    static public func isProbabolistic(reward: ARUnclaimedReward) -> Bool {
        return reward.rewardSelectionTypeCode == "1"
    }
    
    static public func getValueForUnclaimedReward(_ reward: ARUnclaimedReward) -> String {
        
        var value = ""
        if let rewardQuantity = reward.rewardValueQuantity.value {
            value = String(describing: rewardQuantity)
        } else if let rewardAmount = reward.rewardValueAmount {
            value = rewardAmount
        } else if let rewardPercent = reward.rewardValuePercent.value {
            value = String(describing: rewardPercent)
        }
        
        var type = ""
        if reward.rewardValueTypeKey == RewardValueTypeRef.SpecificItem.rawValue {
            type = reward.rewardValueItemDescription ?? ""
        } else {
            type = reward.rewardTypeName ?? ""
        }
        
        switch reward.rewardId {
        case .Cineworld, .CineworldOrVue, .Vue:
            return  "\(value) \(type)"
        case .StarbucksVoucher:
            if (value == "1") {
                return type
            } else {
                return  "\(value) \(type)"
            }
        case .EGiftStarbucks, .MachiLawson, .SmoothieLawson, .YogurtLawson:
            if VIAApplicableFeatures.default.hideRewardsSubtitle(){
                return ""
            }
            return  "\(value) \(type)"
        default:
            return  "\(value) \(type)"
        }
        
    }
    
    static public func getValueForRewardSelection(_ voucher: ARRewardsSelection) -> String {
        
        var value = ""
        if let rewardQuantity = voucher.rewardValueQuantity.value {
            value = String(describing: rewardQuantity)
        } else if let rewardAmount = voucher.rewardValueAmount {
            value = rewardAmount
        } else if let rewardPercent = voucher.rewardValuePercent.value {
            value = String(describing: rewardPercent)
        }
        
        var type = ""
        if voucher.rewardValueTypeKey == RewardValueTypeRef.SpecificItem.rawValue {
            type = voucher.rewardValueItemDescription ?? ""
        } else {
            type = voucher.rewardTypeName ?? ""
        }
        
        switch voucher.rewardId {
        case .Cineworld, .CineworldOrVue, .Vue:
            return  "\(value) \(type)"
        case .StarbucksVoucher:
            if (value == "1") {
                return type
            } else {
                return  "\(value) \(type)"
            }
        case .EGiftStarbucks, .MachiLawson, .SmoothieLawson, .YogurtLawson:
            if VIAApplicableFeatures.default.hideRewardsSubtitle(){
                return ""
            }
            return  "\(value) \(type)"
        default:
            return  "\(value) \(type)"
        }
        
    }
    
    static public func getValueForVoucher(_ voucher: ARVoucher) -> String {
        var value = ""
        if let rewardQuantity = voucher.rewardValueQuantity.value {
            value = String(describing: rewardQuantity)
        } else if let rewardAmount = voucher.rewardValueAmount {
            value = rewardAmount
        } else if let rewardPercent = voucher.rewardValuePercent.value {
            value = String(describing: rewardPercent)
        }
        
        var type = ""
        if voucher.rewardValueTypeKey == RewardValueTypeRef.SpecificItem.rawValue {
            type = voucher.rewardValueItemDescription ?? ""
        } else {
            type = voucher.rewardTypeName ?? ""
        }
        
        switch voucher.rewardId {
        case .Cineworld, .CineworldOrVue, .Vue:
            return  "\(value) \(type)"
        case .StarbucksVoucher:
            if (value == "1") {
                return type
            } else {
                return  "\(value) \(type)"
            }
        case .EGiftStarbucks, .MachiLawson, .SmoothieLawson, .YogurtLawson:
            if VIAApplicableFeatures.default.hideRewardsSubtitle(){
                return ""
            }
            return  "\(value) \(type)"
        case .EasyTickets:
            let percent = voucher.rewardValueTypeKey
            if (percent == 2) {
                return CommonStrings.Ar.Rewards.RewardPercentageDiscountKeyword2137(value)
            } else {
                return  "\(value) \(type)"
            }
            
        case .FoodPanda:
            return  CommonStrings.Ar.Rewards.RewardAmountDiscountKeyword2136(value)
        case .Cinemark:
            return CommonStrings.Ar.Rewards.CinemarkAppDescription2131
        case .JuanValdez:
            return CommonStrings.Ar.Rewards.JuanValdezAppDescription2579
            /** FC-26793 : UKE : Change Request
             * Added the below case to show the RewardValueAmount even if the entry point is from the Voucher Card.
             * ex. "amount" : "GBP 5"
             */
        case .AmazonSmileVoucher, .BookMyShow, .CafeCoffeeDay, .Spotify:
            if voucher.exchangeExchangeOn != nil {
                if let rewardAmount = voucher.rewardValueAmount {
                    value = rewardAmount
                    return "\(value) \(type)"
                }
                return "\(value) \(type)"
            }
            return "\(value) \(type)"
        default:
            return "\(value) \(type)"
        }
        
    }
    
    public static func partnerImage(for partnerKey: RewardReferences) -> UIImage {
        let arRealm = DataProvider.newARRealm()
        let vouchers = arRealm.allVouchers()
        
        switch partnerKey {
            
        case RewardReferences.WheelspinSLI:
            return VIAActiveRewardsAsset.ActiveRewards.Rewards.activeReward.image
        case RewardReferences.MachiLawson:
            return VIAActiveRewardsAsset.ActiveRewards.Rewards.SLIParners.partnerLawson.image
        case RewardReferences.SmoothieLawson:
            return VIAActiveRewardsAsset.ActiveRewards.Rewards.SLIParners.partnerLawson.image
        case RewardReferences.YogurtLawson:
            return VIAActiveRewardsAsset.ActiveRewards.Rewards.SLIParners.partnerLawson.image
        case RewardReferences.EGiftStarbucks:
            return VIAActiveRewardsAsset.ActiveRewards.Rewards.SLIParners.starbucks.image
        case RewardReferences.Unknown:
            return VIAActiveRewardsAsset.ActiveRewards.Rewards.activeReward.image
        case RewardReferences.Wheelspin:
            return VIAActiveRewardsAsset.ActiveRewards.Rewards.activeReward.image
        case RewardReferences.ZapposVoucher:
            return VIAActiveRewardsAsset.ActiveRewards.Rewards.activeReward.image
        case RewardReferences.Token:
            return VIAActiveRewardsAsset.ActiveRewards.Rewards.activeReward.image
        case RewardReferences.ChooseReward:
            return VIAActiveRewardsAsset.ActiveRewards.Rewards.activeReward.image
        case RewardReferences.CineworldOrVue:
            return VIAActiveRewardsAsset.ActiveRewards.Rewards.UKEPartners.cineworldVueSmall.image
        case RewardReferences.Cineworld:
            return VIAActiveRewardsAsset.ActiveRewards.Rewards.UKEPartners.cineworldSmall.image
        case RewardReferences.Vue:
            return VIAActiveRewardsAsset.ActiveRewards.Rewards.UKEPartners.vueSmall.image
        case RewardReferences.StarbucksVoucher:
            return VIAActiveRewardsAsset.ActiveRewards.Rewards.UKEPartners.starbucksSmall.image
        case RewardReferences.EasyTickets:
            return VIAActiveRewardsAsset.ActiveRewards.Rewards.IGIPartners.easyticketsSmall.image
        case RewardReferences.FoodPanda:
            return VIAActiveRewardsAsset.ActiveRewards.Rewards.IGIPartners.foodpandaSmall.image
        case RewardReferences.Cinemark:
            return VIAActiveRewardsAsset.ActiveRewards.Rewards.ECPartners.cinemarkSmall.image
        case RewardReferences.JuanValdez:
            return VIAActiveRewardsAsset.ActiveRewards.Rewards.ECPartners.juanValdezSmall.image
        case RewardReferences.noReward:
            return VIAActiveRewardsAsset.ActiveRewards.Rewards.UKEPartners.noRewardSmall.image
            
        /* FC-26793 : UKE CR Interim Voucher Solution */
        case RewardReferences.AmazonSmileVoucher:
            /** FC-26793 : UKE : Change Request
             * Get the WheelSpin ID of the Award.
             */
            guard let wheelSpinID = vouchers.first?.rewardId else {
                return VIAActiveRewardsAsset.ActiveRewards.Rewards.partnerAmazon.image
            }
            
            /* Determine the icons per country using the WheelSpin ID.
             * This is only for the SpinWheel Icons.
             */
            switch wheelSpinID {
            case RewardReferences.WheelSpinPOLAND:
                return VIAActiveRewardsAsset.ActiveRewards.Rewards.UKEPartners.amazonDe.image
            case RewardReferences.WheelSpinCHANNELISLAND:
                return VIAActiveRewardsAsset.ActiveRewards.Rewards.UKEPartners.amazonCoUk.image
            case RewardReferences.WheelSpinUSA:
                return VIAActiveRewardsAsset.ActiveRewards.Rewards.UKEPartners.amazonUsa.image
            default:
                /** FC-26793 : UKE : Change Request
                 * On Click of Voucher Card & Available Rewards Card.
                 */
                guard let rewardValueLinkID = vouchers.first?.rewardValueLinkId else {
                    return VIAActiveRewardsAsset.ActiveRewards.Rewards.partnerAmazon.image
                }
                
                /* Determine the icons per country using the rewardValueLinkID.
                 * This is only for the Voucher Card & Available Rewards Card.
                 */
                switch rewardValueLinkID {
                case 260000054, 260000053:
                    return VIAActiveRewardsAsset.ActiveRewards.Rewards.UKEPartners.amazonDe.image
                case 260000001, 260000011:
                    return VIAActiveRewardsAsset.ActiveRewards.Rewards.UKEPartners.amazonCoUk.image
                case 260000050, 260000051, 260000052:
                    return VIAActiveRewardsAsset.ActiveRewards.Rewards.UKEPartners.amazonUsa.image
                default: return VIAActiveRewardsAsset.ActiveRewards.Rewards.partnerAmazon.image
                }
            }
        case RewardReferences.BookMyShow:
            /** FC-26793 : UKE : Change Request
             * Determine Accounts FeatureGroup
             */
            let coreRealm = DataProvider.newRealm()
            var featureGroupValue = ""
            let partyDetails = coreRealm.currentVitalityParty()
            if let references = partyDetails?.references {
                if !references.isEmpty {
                    for reference in references {
                        /* "15" = Feeature Group */
                        if reference.type == "15" {
                            featureGroupValue = reference.value
                            break
                        }
                    }
                }
            }
            
            /** Check if the FeatureGroup is India
             * If YES, return a black image.
             * If NO, return the BookMyShow Image.
             */
            if featureGroupValue.lowercased() == "india" {
                return UIImage()
            } else {
                return VIAActiveRewardsAsset.ActiveRewards.Rewards.UKEPartners.bookMyShowDefault.image
            }
        case RewardReferences.CafeCoffeeDay:
            return UIImage()
        case RewardReferences.Spotify:
            return VIAActiveRewardsAsset.ActiveRewards.Rewards.UKEPartners.spotifyDefault.image
            
        /* FC-26012 : UKE Segmentation */
        case RewardReferences.WheelSpinSP:
            return VIAActiveRewardsAsset.ActiveRewards.Rewards.activeReward.image
        case RewardReferences.WheelSpinIN:
            return VIAActiveRewardsAsset.ActiveRewards.Rewards.activeReward.image
        case RewardReferences.GoalAchievedReward:
            return VIAActiveRewardsAsset.ActiveRewards.Rewards.activeReward.image
        default:
            return VIAActiveRewardsAsset.ActiveRewards.Rewards.activeReward.image
        }
    }
    
    public static func partnerRewardInstructions(for rewardStatus: AwardedRewardStatusRef?, rewardValue: String?, effectiveFromDate: Date?) -> String {
        guard let status = rewardStatus else {
            return ""
        }
        switch status {
        case .Acknowledged:
            return ""
        case .Allocated:
            let voucherString: String = rewardValue ?? ""
            var localizedDate: String = ""
            if let date = effectiveFromDate {
                localizedDate = Localization.dayMonthAndYearFormatter.string(from: date)
            }
            return CommonStrings.Ar.Rewards.PendingStarbucksRewardFooterHtml1079(voucherString, voucherString)
        case .AvailableToRedeem:
            return ""
        case .Canceled:
            return ""
        case .Expired:
            return ""
        case .Issued:
            let voucherString: String = rewardValue ?? ""
            return VIAApplicableFeatures.default.getPartnerRewardInstructions(with: voucherString)
        case .IssueFailed:
            return ""
        case .PartnerRegistration:
            return ""
        case .Used:
            return ""
        case .Unknown:
            return ""
        }
    }
    
    public static func bespokeViewController(for partnerKey: RewardReferences) -> String? {
        switch partnerKey {
        case RewardReferences.Unknown:
            return nil
        case RewardReferences.Wheelspin:
            return nil
        case RewardReferences.ZapposVoucher:
            return nil
        case RewardReferences.Token:
            return nil
        case RewardReferences.ChooseReward:
            return nil
        case RewardReferences.CineworldOrVue:
            return "VIAUKECinemaRewards"
        case RewardReferences.Cineworld:
            return "VIAUKECinemaRewards"
        case RewardReferences.Vue:
            return "VIAUKECinemaRewards"
        case RewardReferences.EasyTickets:
            return "VIAIGIEasyTicketsRewards"
        case RewardReferences.FoodPanda:
            return "VIAIGIFoodPandaRewards"
        case RewardReferences.JuanValdez:
            return "VIAECJuanValdezRewards"
        case RewardReferences.Cinemark:
            return "VIAECCinemarkRewards"
        case RewardReferences.noReward:
            return nil
        case RewardReferences.StarbucksVoucher:
            return "VIAUKEStarbucksRewards"
            
        case RewardReferences.MachiLawson:
            return nil
        case RewardReferences.SmoothieLawson:
            return nil
        case RewardReferences.YogurtLawson:
            return nil
        case RewardReferences.EGiftStarbucks:
            return nil
        
        /* FC-26793 : UKE CR Interim Voucher Solution */
        case RewardReferences.AmazonSmileVoucher:
            return "VIAUKEVoucherSolutionsRewards"
        case RewardReferences.CafeCoffeeDay:
            return "VIAUKEVoucherSolutionsRewards"
        case RewardReferences.BookMyShow:
            return "VIAUKEVoucherSolutionsRewards"
        case RewardReferences.Spotify:
            return "VIAUKEVoucherSolutionsRewards"
            
        default:
            return nil
        }
    }
    
    public static func bespokeAlternateTermsView(for partnerKey: RewardReferences) -> String? {
        switch partnerKey {
            
        case RewardReferences.EasyTickets:
            return "DataPrivacy"
        case RewardReferences.FoodPanda:
            return "DataPrivacy"
        default:
            return nil
        }
    }
}

// MARK: Mark as Used / Unused
extension VIAARRewardHelper {
    static public func updateRewardStatusToUsedOrUnused(reward: VIAARRewardViewModel, status: AwardedRewardStatusRef, completion: @escaping (_ error: Error?, _ response: UpdateRewardStatusResponse?) -> Void) {
        
        let coreRealm = DataProvider.newRealm()
        coreRealm.refresh()
        let tenantId = coreRealm.getTenantId()
        let partyId = coreRealm.getPartyId()
        
        Wire.Rewards.updateRewardAcknowledgedStatus(tenantId: tenantId, partyId: partyId, awardedRewardId: reward.id, status: status) { error, response in
            guard error == nil else {
                completion(error, nil)
                return
            }
            
            completion(nil, response)
        }
    }

    static public func submitEventForRewardStatusChange(reward: VIAARRewardViewModel, status: AwardedRewardStatusRef, completion: @escaping (_ error: Error?) -> Void) {
        let coreRealm = DataProvider.newRealm()
        let tenantId = coreRealm.getTenantId()
        let partyId = coreRealm.getPartyId()
        
        // ProcessEventsV2 - PartyReferenceEvent
        let eventSourceTypeKey: EventSourceRef = .MobileApp
        let eventCapturedOn = Date()
        let eventOccuredOn = Date()
        let applicableTo = ProcessEventsV2ApplicableTo(referenceTypeKey: eventSourceTypeKey.rawValue,
                                                       value: String(partyId))
        let reportedBy = ProcessEventsV2PartyReferenceEventsReportedBy(referenceTypeKey: eventSourceTypeKey.rawValue,
                                                                       value: String(partyId))
        var typeKey = EventTypeRef.Unknown
        if status == .AvailableToRedeem {
            typeKey = EventTypeRef.UserSelected
        } else {
            typeKey = EventTypeRef.UserUnselected
        }
        
        var eventMetaDatas = [ProcessEventsV2EventMetaData]()
        
        let rewardName = ProcessEventsV2EventMetaData(typeKey: EventMetaDataTypeRef.RewardName.rawValue,
                                                      value: reward.title,
                                                      unitOfMeasure: nil)
        eventMetaDatas.append(rewardName)
        
        guard let key = reward.rewardkey else { return }
        let rewardKey = ProcessEventsV2EventMetaData(typeKey: EventMetaDataTypeRef.RewardKey.rawValue,
                                                     value: "\(key)",
                                                     unitOfMeasure: nil)
        eventMetaDatas.append(rewardKey)
        
        let processEvent = ProcessEventsV2PartyReferenceEvent(applicableTo: applicableTo,
                                                              associatedEvents: [],
                                                              eventCapturedOn: eventCapturedOn,
                                                              eventExternalReference: nil,
                                                              eventId: nil,
                                                              eventMetaDatas: eventMetaDatas,
                                                              eventOccurredOn: eventOccuredOn,
                                                              eventSourceTypeKey: eventSourceTypeKey,
                                                              reportedBy: reportedBy,
                                                              typeKey: typeKey.rawValue)
        
        var events = [ProcessEventsV2PartyReferenceEvent]()
        events.append(processEvent)
        
        Wire.Events.processEventsV2(tenantId: tenantId, events: events, completion: { (response, error) in
            guard error == nil else {
                completion(error)
                return
            }
            completion(nil)
        })
    }
    
    /** FC-26793 : UKE : Change Request
     * Determine the Feature Group of the Account.
     */
    static public func getFeatureGroupValue() -> String {
        let coreRealm = DataProvider.newRealm()
        var featureGroupValue = ""
        guard let partyDetails = coreRealm.currentVitalityParty() else {
            return ""
        }
        
        let references = partyDetails.references
        guard !references.isEmpty else {
            return ""
        }
        
        for reference in references {
            /* "15" = Feeature Group */
            if reference.type == "15" {
                featureGroupValue = reference.value
                return featureGroupValue
            }
        }
        return ""
    }
}
