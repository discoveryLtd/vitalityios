//
//  VIAARSwapRewardViewController.swift
//  VitalityActive
//
//  Created by Joshua Ryan on 6/12/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import VIAUIKit
import VitalityKit
import VIACommon
import VIAUtilities


protocol ARRewardSwapDelegate: ARRewardFlowDelegate {
    
    func swapReward(_ selectedReward: VIAARRewardViewModel)
    
}

class VIAARSwapRewardViewController: VIACoordinatedTableViewController, ImproveYourHealthTintable {
    
    // MARK: Public
    
    public var currentModel: VIAARRewardViewModel?
    public weak var delegate: ARRewardSwapDelegate?
    
    // MARK: Private
    
    private var viewModel = VIAApplicableFeatures.default.getAvailableRewardViewModel() as? VIAARAvailableRewardViewModel ?? VIAARAvailableRewardViewModel()
    
    private var options = [VIAARRewardViewModel]()
    
    private var selectedIndex = IndexPath(row: 0, section: 0)
    
    
    
    // MARK: View lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = CommonStrings.Ar.Rewards.SwapTitle669
        self.configureTableView()
        self.addBarButtonItems()
        
        if let currentModel = currentModel {
            options = viewModel.availableSwaps(for: currentModel)
        }
    }
    
    func configureTableView() {
        self.tableView.register(VIAARParticipatingPartnerCell.nib(), forCellReuseIdentifier: VIAARParticipatingPartnerCell.defaultReuseIdentifier)
        self.tableView.register(VIATableViewSectionHeaderFooterView.nib(), forHeaderFooterViewReuseIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier)
        self.tableView.estimatedRowHeight = 100
    }
    
    // MARK: Action buttons
    
    func addBarButtonItems() {
        let cancel = UIBarButtonItem(title: CommonStrings.CancelButtonTitle24, style: .plain, target: self, action: #selector(cancelButtonTapped(_:)))
        self.navigationItem.leftBarButtonItem = cancel
        
        let done = UIBarButtonItem(title: CommonStrings.Ar.Rewards.ChooseRewardButtonTitle686, style: .plain, target: self, action: #selector(doneButtonTapped(_:)))
        self.navigationItem.rightBarButtonItem = done
    }
    
    func cancelButtonTapped (_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func doneButtonTapped (_ sender: UIBarButtonItem) {
        if selectedIndex.section == 0 {
            // Should the done button just be disabled?
            self.navigationController?.popViewController(animated: true)
        } else {
            let reward = options[selectedIndex.row]
            delegate?.swapReward(reward)
        }
    }
    
    // MARK: UITableView datasource
    
    enum Sections: Int, EnumCollection {
        case current = 0
        case rewards = 1
        
        func sectionHeaderTitle() -> String? {
            switch self {
            case .current:
                return CommonStrings.Ar.Rewards.CurrentTitle687
            case .rewards:
                return CommonStrings.Ar.Rewards.AvailableSwapsTitle667
            }
        }
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return Sections.allValues.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let section = Sections(rawValue: section)
        if section == .current {
            return 1
        } else if section == .rewards {
            return options.count
        }
        return 0
    }
    
    // MARK: UITableView delegate
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let section = Sections(rawValue: indexPath.section)
        var cell: UITableViewCell?
        
        if section == .current {
            cell = self.configureHeaderCell(indexPath)
        } else if section == .rewards {
            cell = self.configureOptionCell(indexPath)
        } else {
            cell = tableView.dequeueReusableCell(withIdentifier: VIATableViewCell.defaultReuseIdentifier, for: indexPath)
        }
        
        if indexPath == selectedIndex {
            if indexPath.section == 0 {
                cell?.accessoryType = .none
            } else {
                cell?.accessoryType = .checkmark
            }
        } else {
            cell?.accessoryType = .none
        }
        
        cell?.tintColor = UIColor.rewardBlue()
        
        return cell!
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let title = Sections(rawValue: section)?.sectionHeaderTitle() else { return nil }
        let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier) as? VIATableViewSectionHeaderFooterView
        view?.configureWithExtraSpacing(labelText: title, extraTopSpacing: 28, extraBottomSpacing: 0)
        return view
    }
    
    func configureHeaderCell(_ indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: VIAARParticipatingPartnerCell.defaultReuseIdentifier, for: indexPath) as! VIAARParticipatingPartnerCell
        
        if let currentModel = currentModel {
            cell.partnerName = currentModel.title
            cell.descriptionText = currentModel.subTitle
            cell.cellImage = currentModel.image
        }
        return cell
    }
    
    func configureOptionCell(_ indexPath: IndexPath) -> UITableViewCell {
        let item = options[indexPath.row]
        
        let cell = self.tableView.dequeueReusableCell(withIdentifier: VIAARParticipatingPartnerCell.defaultReuseIdentifier, for: indexPath) as! VIAARParticipatingPartnerCell
        cell.partnerName = item.title
        cell.descriptionText = item.subTitle
        cell.cellImage = item.image
        cell.accessoryType = .none
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndex = indexPath
        tableView.reloadData()
    }
}
