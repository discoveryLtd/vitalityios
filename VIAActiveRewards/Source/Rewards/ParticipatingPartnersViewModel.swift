import UIKit
import VitalityKit
import VIAUtilities
import RealmSwift
import VIACommon

class ParticipatingPartnersViewModel: CMSConsumer {
    
    init(withPartnerType partnerCategoryType: ProductFeatureCategoryRef) {
        self.partnerCategoryType = partnerCategoryType
    }
    
    // MARK: - Properties
    
    struct PartnerData {
        var name: String
        var longDescription: String
        var description: String
        var logoFileName: String
        var typeKey: Int
        var learnMoreUrl: URL?
        var webViewContent: String?
    }
    
    struct GroupData {
        var name: String?
        var partners: [PartnerData]
    }
    
    let coreRealm = DataProvider.newPartnerRealm()
    public var partnerCategoryType: ProductFeatureCategoryRef
    public var partnerGroupsData = [GroupData]()
    
    // MARK: - Configuration 
    
    func configurePartnerData() {
        coreRealm.refresh()
        
        guard let partnerCategory = coreRealm.partnerCategory(with: partnerCategoryType.rawValue) else {
            return
        }
        var groupsData = [GroupData]()
        let groups = getPartnerGroups(partnerCategory: partnerCategory)

        for group in groups {
            var groupData = GroupData(name: group.name, partners:[PartnerData]())
            for partner in group.partners {
                let partnerData = PartnerData(name: partner.name, longDescription: partner.longDescription, description: partner.shortDescription, logoFileName: partner.logoFileName, typeKey: partner.typeKey, learnMoreUrl: nil, webViewContent: nil)
                groupData.partners.append(partnerData)
            }
            groupsData.append(groupData)
        }
        partnerGroupsData = groupsData
    }
    func getPartnerGroups(partnerCategory: PartnerCategory) -> List<PartnerGroup>{
        if VIAApplicableFeatures.default.shouldFilterPartnerGroups() {
            return List(partnerCategory.partnerGroups.filter("key == %@", partnerCategoryType.rawValue))
        }
        
        return partnerCategory.partnerGroups
    }
    // MARK: - Networking
    
    public func requestPartnersByCategory(completion: @escaping (Error?) -> Void) {
        let requestParameters = configureGetPartnersByCategoryRequestParameters()
        let tenantId = DataProvider.newRealm().getTenantId()
        let partyId = DataProvider.newRealm().getPartyId()
        Wire.Member.getPartnersByCategory(tenantId: tenantId, partyId: partyId, request: requestParameters) { error, result in
            guard error == nil else {
                completion(error)
                return
            }
            completion(nil)
        }
    }
    
    func configureGetPartnersByCategoryRequestParameters() -> GetPartnersByCategoryParameters {
        let effectiveDate = Date()
        let productFeatureCategoryTypeKey = partnerCategoryType.rawValue
        let requestParameters = GetPartnersByCategoryParameters(effectiveDate: effectiveDate, productFeatureCategoryTypeKey: productFeatureCategoryTypeKey)
        
        return requestParameters
    }
    
    func partnerLogo(partner: PartnerData, completion: @escaping ((_: UIImage, _: PartnerData) -> Void)) {
        //Case where logo already exists on disk
        let documentsURL = FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask).first!
        let cachedLogoURL = documentsURL.appendingPathComponent(partner.logoFileName)
        if let cachedLogo = UIImage(contentsOfFile: cachedLogoURL.path) {
            debugPrint("Partner image retrieved successfully from disk")
            return completion(cachedLogo, partner)
        }
        
        //Otherwise dowload logo from liferay
        self.downloadCMSImageToCache(file: partner.logoFileName) { url, error in
            let defaultLogo = VIAActiveRewardsAsset.ActiveRewards.Rewards.partnersPlaceholder.image
            guard url != nil, error == nil, let filePath = url?.path, FileManager.default.fileExists(atPath: filePath) else {
                debugPrint("Could not retrieve downloaded partner image from disk")
                return completion(defaultLogo, partner)
            }
            
            if let logo = UIImage(contentsOfFile: filePath) {
                debugPrint("Partner image retrieved successfully from service")
                return completion(logo, partner)
            }
            
            debugPrint("Falied to download partner image")
            return completion(defaultLogo, partner)
        }
    }
    
}
