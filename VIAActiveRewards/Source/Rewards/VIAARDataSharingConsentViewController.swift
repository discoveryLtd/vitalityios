import Foundation
import VIAUIKit
import VitalityKit
import VIAUtilities
import VIACommon

protocol VIAARDataSharingDelegate{
    func dataSharingAccepted()
}

class VIAARDataSharingConsentViewController: CustomTermsConditionsViewController, ImproveYourHealthTintable {
    
    var dataSharingDelegate: VIAARDataSharingDelegate?
    var rewardSelectionViewController = VIAARRewardSelectionViewController()
    
    // MARK: View lifecycle
    
    override open func viewDidLoad() {
        super.viewDidLoad()
        
        let reward = rewardSelectionViewController.viewModel.availableRewards.element(at: rewardSelectionViewController.selectedIndex.row)
        
        switch reward?.rewardkey {
        case 8:
            self.viewModel = CustomTermsConditionsViewModel(articleId: AppConfigFeature.contentId(for: .AREasyTicketsDS))
            break
        case 9:
            self.viewModel = CustomTermsConditionsViewModel(articleId: AppConfigFeature.contentId(for: .ARFoodPandaDS))
            break
        case 20:
            // TODO: valtomol-AR Get juan valdez privacy content id once it is made available on the login response.
            self.viewModel = CustomTermsConditionsViewModel(articleId: "ar-juanvaldez-privacy-policy")
            break
        case 21:
            // TODO: valtomol-AR Get cinemark content id once it is made available on the login response.
            self.viewModel = CustomTermsConditionsViewModel(articleId: "ar-cinemark-privacy-policy")
            break
        default:
            self.viewModel = CustomTermsConditionsViewModel(articleId: AppConfigFeature.contentId(for: .VHCDSConsentContent))
            break
        }
        
        rightButtonTitle = VIAApplicableFeatures.default.getDataSharingRightBarButtonTitle()
        leftButtonTitle = VIAApplicableFeatures.default.getDataSharingLeftBarButtonTitle()
    }
    
    override func configureAppearance() {
        super.configureAppearance()
        
        navigationController?.makeNavigationBarInvisible()
        navigationController?.navigationBar.isHidden = true
    }
    
    // MARK: Action buttons
    
    override func leftButtonTapped(_ sender: UIBarButtonItem?) {
        self.performSegue(withIdentifier: "unwindFromVIAARDataSharingConsentToList", sender: nil)
    }
    
    override func rightButtonTapped(_ sender: UIBarButtonItem?) {
//        self.showHUDOnWindow()
//
//        let events = [EventTypeRef.VHCDataPrivacyAgree]
//        viewModel?.agreeToTermsAndConditions(with: events, completion: { [weak self] (error) in
//            self?.hideHUDFromWindow()
//            guard error == nil else {
//                self?.handleErrorOccurred(error)
//                return
//            }
//            self?.accept()
//        })
        self.dataSharingDelegate?.dataSharingAccepted()
        self.performSegue(withIdentifier: "unwindFromVIAARDataSharingConsentToList", sender: nil)
    }

    func accept() {
        guard let reward = rewardSelectionViewController.viewModel.availableRewards.element(at: rewardSelectionViewController.selectedIndex.row) else {
            return
        }
        
        rewardSelectionViewController.delegate?.selectionCompleted(reward)
        self.dismiss(animated: false, completion: nil)
    }
    
    func handleErrorOccurred(_ error: Error?) {
        guard error == nil else {
            debugPrint(error as Any)
            switch error {
            case is BackendError:
                self.handleBackendErrorWithAlert(error as! BackendError, tryAgainAction: { [weak self] in
                    self?.rightButtonTapped(nil)
                })
            default:
                self.displayUnknownServiceErrorOccurredAlert()
            }
            self.hideHUDFromWindow()
            return
        }
    }
    
    override func returnToPreviousView() {
        self.performSegue(withIdentifier: "unwindFromVIAARDataSharingConsentToList", sender: nil)
    }
}
