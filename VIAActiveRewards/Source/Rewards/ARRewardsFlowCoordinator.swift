    //
    //  ARRewardsFlowCoordinator.swift
    //  VitalityActive
    //
    //  Created by Joshua Ryan on 6/30/17.
    //  Copyright © 2017 Glucode. All rights reserved.
    //
    
    import VitalityKit
    import VIAUIKit
    import VIACommon
    import VIAUtilities
    
    public protocol ARRewardFlowDelegate: VIAFlowDelegate {
        
        func loadArticleContent(articleId: String?, completion: @escaping ((_ error: Error?, _ content: String?) -> Void))
    }
    
    public protocol VIAABespokeRewardDelegate: class {
        func confirmReward(_ reward: VIAARRewardViewModel, consumer: UIViewController?, completion: ((Error?, Int?, String?) -> Void)?)
        func consumeReward(_ reward: VIAARRewardViewModel, completion: ((Error?) -> Void)?)
        func updateRewardStatusToUsed(_ reward: VIAARRewardViewModel, completion: ((Error?, UpdateRewardStatusResponse?)-> Void)?)
        func cancel(consumer: UIViewController?)
        func setRewardVoucher(as reward: VIAARRewardViewModel)
        func showChosen(rewardId: Int, consumer: UIViewController?)
    }
    
    public protocol VIAABespokeRewardConsumer: class {
        func setReward(_ reward: VIAARRewardViewModel)
        var delegate: VIAABespokeRewardDelegate? {get set}
    }
    
    public class ARRewardsFlowCoordinator: CMSConsumer, ARRewardFlowDelegate {
        
        // MARK: Public
        
        weak var spinController: VIAARRewardSpinViewController?
        weak var selectController: VIAARRewardSelectionViewController?
        weak var swapController: VIAARSwapRewardViewController?
        weak var termController: VIAARRewardTermsViewControllers?
        weak var showRewardController: VIAARChosenRewardViewController?
        weak var confirmationController: VIAARRewardConfirmationViewController?
        weak var webController: VIAWebVoucherViewController?
        weak var landingController: UIViewController?
        
        // MARK: Private
        
        fileprivate var viewModel: VIAARFlowViewModel = VIAARFlowViewModel()
        
        // State related items to inject into VCs
        fileprivate var currentVoucher: VIAARRewardViewModel?
        
        fileprivate var currentReward: VIAARRewardViewModel?
        
        fileprivate var currentCredit: VIAARCreditViewModel?
        
        public init() {
            
            
        }
        
        // MARK: View Configuration
        
        public func configureController(vc: UIViewController) {
            if let vc = vc as? VIACoordinatable {
                vc.coordinator = self
            }
            
            if let vc = vc as? VIAARChosenRewardViewController, let currentVoucher = currentVoucher {
                showRewardController = vc
                vc.delegate = self
                vc.title = CommonStrings.Ar.Rewards.ChosenRewardTitle736
                vc.viewModel = currentVoucher
            } else if let vc = vc as? VIAARRewardSpinViewController, let currentCredit = currentCredit {
                spinController = vc
                vc.delegate = self
                vc.credit = currentCredit
            } else if let vc = vc as? VIAARRewardSelectionViewController, let _ = currentCredit {
                selectController = vc
                vc.delegate = self
                vc.credit = currentCredit
            } else if let vc = vc as? VIAARSwapRewardViewController, let currentVoucher = currentVoucher {
                swapController = vc
                vc.delegate = self
                vc.currentModel = currentVoucher
            } else if let vc = vc as? VIAARRewardTermsViewControllers {
                termController = vc
                vc.delegate = self
                
                vc.navigationController?.makeNavigationBarInvisible()
                vc.navigationController?.navigationBar.isHidden = true
                vc.navigationController?.makeNavigationBarTransparent()
                
                if let currentVoucher = currentVoucher {
                    vc.dataProvider = currentVoucher
                } else if let currentReward = currentReward {
                    vc.dataProvider = currentReward
                }
            } else if let vc = vc as? VIAWebVoucherViewController, let currentVoucher = currentVoucher {
                if let htmlString = currentVoucher.partnerSysRewardId?.first {
                    webController = vc
                    vc.setHTMLContent(as: htmlString)
                    vc.title = currentVoucher.title
                }
                
            } else if let vc = vc as? VIAARRewardConfirmationViewController, let voucher = currentVoucher {
                confirmationController = vc
                vc.delegate = self
                vc.viewModel = voucher
            } else if let vc = vc as? VIAARRewardsLandingViewController {
                landingController = vc
            } else if let vc = vc as? VIAUKENoRewardViewController, let reward = currentReward {
                landingController = vc
                vc.delegate = self
                vc.viewModel = reward
            } else if let nav = vc as? UINavigationController, let rootVc = nav.viewControllers.first {
                configureController(vc: rootVc)
            } else if let vc = vc as? VIAABespokeRewardConsumer, let reward = currentReward {
                vc.setReward(reward)
                vc.delegate = self
            } else if landingController == nil {
                landingController = vc
            }
        }
        
        // MARK: CMSConsumer
        public func loadArticleContent(articleId: String?, completion: @escaping ((Error?, String?) -> Void)) {
            self.getCMSArticleContent(articleId: articleId, completion: completion)
        }
        
        fileprivate func updateRewardStatus(_ creditModel: VIAARCreditViewModel, rewardModel: VIAARRewardViewModel, error: Error?, completion: @escaping () -> Void) {
            
            self.spinController?.hideHUDFromWindow()
            
            if let error = error {
                debugPrint(error)
                switch error {
                case is BackendError:
                    self.termController?.handleBackendErrorWithAlert(error as! BackendError, tryAgainAction: { [weak self] in
                        self?.processReward(reward: rewardModel, completion: completion)
                    })
                default:
                    self.termController?.displayUnknownServiceErrorOccurredAlert()
                }
                debugPrint(error)
            } else {
                self.currentReward = rewardModel
                completion()
            }
        }
        
        // MARK: Internal
        
        fileprivate func processReward(reward: VIAARRewardViewModel, completion: @escaping () -> Void) {
            
            self.spinController?.showHUDOnWindow()
            
            if let creditModel = currentCredit {
                
                if let swapRewardEnabled = VIAApplicableFeatures.default.showSwapRewardsOption, swapRewardEnabled {
                    viewModel.updateRewardStatusToAcknowledged(creditModel, rewardModel: reward, completion: { error, response in
                        self.updateRewardStatus(creditModel, rewardModel: reward, error: error,  completion: completion)
                    })
                } else {
                    viewModel.selectVoucherForReward(creditModel, rewardModel: reward, completion: { error, voucherModel in
                        self.updateRewardStatus(creditModel, rewardModel: reward, error: error,  completion: completion)
                    })
                }
            }
        }
        
        fileprivate func showBespokeReward(boardId: String!, navController: UINavigationController, modal: Bool = false) {
            
            // Try both the current module bundle and the active binary main bundle
            var bundle = Bundle(for: VIAARRewardSpinViewController.self)
            if bundle.path(forResource: boardId, ofType: "storyboardc") == nil {
                bundle = Bundle.main
            }
            
            // Ensure that the compiled storyboard exists in bundle
            if let _ = bundle.path(forResource: boardId, ofType: "storyboardc") {
                let storyboard = UIStoryboard(name: boardId, bundle: bundle)
                
                var vc: UIViewController?
                
                if currentVoucher == nil, let reward = currentReward, let viewId = VIAARRewardHelper.bespokeAlternateTermsView(for: reward.rewardReference) {
                    vc = storyboard.instantiateViewController(withIdentifier: viewId)
                } else {
                    vc = storyboard.instantiateInitialViewController()
                }
                
                if let vc = vc {
                    configureController(vc: vc)
                    
                    if modal {
                        navController.present(UINavigationController(rootViewController: vc), animated: true, completion: nil)
                    } else {
                        navController.pushViewController(vc, animated: true)
                    }
                } else {
                    print("View not found for Bespoke reward (\(boardId)")
                }
            } else {
                print("Bespoke rewards storyboard (\(boardId) not found in bundle.")
            }
        }
        
        fileprivate func showChosenReward(with rewardId: Int, consumer: UIViewController?) {
            viewModel.getVoucher(with: rewardId, completion: { [weak self] (error, voucher) in
                guard error == nil else {
                    (consumer as? VIAViewController)?.handleBackendErrorWithAlert(error as? BackendError ?? .other)
                    return
                }
                if let tenantID = AppSettings.getAppTenant(), tenantID == .SLI, let prevVoucherEffectiveFromDate = self?.currentVoucher?.effectiveFromDate {
                    voucher?.effectiveFromDate = prevVoucherEffectiveFromDate
                }
                if let boardId = voucher?.bespokeRewardBoardId, let navController = consumer?.navigationController {
                    self?.currentReward = voucher
                    self?.currentVoucher = voucher
                    self?.showBespokeReward(boardId: boardId, navController: navController)
                } else {
                    if let sliWebDirect = VIAApplicableFeatures.default.redirectToWebVoucher, sliWebDirect {
                        self?.currentVoucher = voucher
                        //self?.confirmationController?.performSegue(withIdentifier: "showWebVoucher", sender: nil)
                        self?.confirmationController?.performSegue(withIdentifier: "skipRewardTerms", sender: nil)
                    } else {
                        consumer?.performSegue(withIdentifier: "showChosenReward", sender: nil)
                    }
                    return
                }
            })
        }
    }
    
    extension ARRewardsFlowCoordinator: ARLandingNavDelegate {
        
        public var landingModel: ActiveRewardsRewardsLandingViewModel {
            get {
                return self.viewModel
            }
        }
        
        public func showReward(_ reward: VIAARRewardViewModel) {
            self.currentVoucher = reward
            self.currentReward = reward
            
            //TODO: Add checker for SLI Market to redirect to VIAARWebViewController to show voucher
            
            if let boardId = reward.bespokeRewardBoardId, let navController = landingController?.navigationController {
                self.showBespokeReward(boardId: boardId, navController: navController, modal: true)
            } else {
                //if let sliWebDirect = VIAApplicableFeatures.default.redirectToWebVoucher, sliWebDirect {
                //    landingController?.performSegue(withIdentifier: "showWebVoucher", sender: nil)
                //}
                //else {
                    landingController?.performSegue(withIdentifier: "showUsableReward", sender: nil)
                //}
            }
        }
        
        public func useCredit(_ credit: VIAARCreditViewModel) {
            
            currentCredit = credit
            
            if let voucher = credit.voucher {
                currentVoucher = voucher
                
                if let boardId = voucher.bespokeRewardBoardId, let navController = landingController?.navigationController {
                    self.showBespokeReward(boardId: boardId, navController: navController, modal: true)
                } else {
                    landingController?.performSegue(withIdentifier: "resumeRewardSelection", sender: nil)
                }
            } else {
                if credit.rewardSelectionTypeKey == RewardSelectionTypeRef.WheelSpin.rawValue {
                    landingController?.performSegue(withIdentifier: "showRewardSpinner", sender: nil)
                } else {
                    landingController?.performSegue(withIdentifier: "showChooseReward", sender: nil)
                }
            }
        }
        
        public func showPartner(_ partner: VIAARRewardViewModel) {
            landingController?.performSegue(withIdentifier: "showParticipatingPartner", sender: partner)
        }
    }
    
    extension ARRewardsFlowCoordinator: ARRewardSpinCompletionDelegate {
        
        public func setRewardCredit(as credit: VIAARCreditViewModel) {
            self.currentCredit = credit
        }
        
        func spinCompleted(_ winningReward: VIAARRewardViewModel) {
            if winningReward.id == RewardReferences.noReward.rawValue {
                self.currentReward = winningReward
                spinController?.performSegue(withIdentifier: "showNoReward", sender: nil)
                return
            }
            
            self.processReward(reward: winningReward, completion: { [weak self] in
                
                if let showSwap = VIAApplicableFeatures.default.showSwapRewardsOption, showSwap {
                    self?.currentVoucher = winningReward
                    self?.spinController?.performSegue(withIdentifier: "showConfirmReward", sender: nil)
                }
                    
                else {
                    if let boardId = winningReward.bespokeRewardBoardId, let navController = self?.spinController?.navigationController {
                        self?.showBespokeReward(boardId: boardId, navController: navController)
                    } else {
                        self?.spinController?.performSegue(withIdentifier: "showConfirmReward", sender: nil)
                    }
                }
            })
        }
        
        func spinCanceled() {
            spinController?.dismiss(animated: true, completion: nil)
        }
    }
    
    extension ARRewardsFlowCoordinator: ARRewardSelectionCompletionDelegate {
        
        public func setARSelectRewardCredit(as credit: VIAARCreditViewModel) {
            self.currentCredit = credit
        }
        
        func selectionCompleted(_ reward: VIAARRewardViewModel) {
            
            self.selectController?.showHUDOnWindow()
            self.showSelectionChosenReward(reward)
        }
        
        func selectionCanceled() {
            selectController?.dismiss(animated: true, completion: nil)
        }

        fileprivate func showSelectionChosenReward(_ reward: VIAARRewardViewModel) {
            
            viewModel.confirmVoucher(reward, completion: { [weak self] error, newRewardId, voucherNumber in
                
                self?.selectController?.hideHUDFromWindow()
                
                if let error = error {
                    
                    self?.selectController?.handleBackendErrorWithAlert(error as? BackendError ?? .other, tryAgainAction: { [weak self] in
                        self?.showSelectionChosenReward(reward)
                    })
                } else {
                    
                    if let newRewardId = newRewardId {
                        self?.showChosenReward(with: newRewardId, consumer: self?.selectController)
                    }
                }
            })
            
        }
    }
    
    extension ARRewardsFlowCoordinator: ARRewardSwapDelegate {
        
        func swapReward(_ selectedReward: VIAARRewardViewModel) {
            
            self.swapController?.showHUDOnWindow()
            
            VIAARRewardHelper.selectReward(with: selectedReward, with: .Swapped, completion: { [weak self] error, newRewardId, voucherNumber in
                
                self?.swapController?.hideHUDFromWindow()
                
                if let error = error {
                    self?.swapController?.handleBackendErrorWithAlert((error as? BackendError) ?? BackendError.other, tryAgainAction: { [weak self] in
                        self?.swapReward(selectedReward)
                    })
                }
                
                if let newRewardId = newRewardId {
                    
                    self?.showChosenReward(with: newRewardId, consumer: self?.swapController)
                }
            })
            
            /*if let boardId = selectedReward.bespokeRewardBoardId, let navController = self?.swapController?.navigationController {
             self?.showBespokeReward(boardId: boardId, navController: navController)
             } else {
             self?.currentReward = selectedReward
             self?.currentVoucher = selectedReward
             self?.confirmationController?.reloadChosenRewardContent(with: selectedReward)
             self?.swapController?.performSegue(withIdentifier: "showChosenSwappedReward", sender: nil)
             }*/
        }
    }
    
    extension ARRewardsFlowCoordinator: ARRewardTermCompletionDelegate {
        public func setTermController(as termController: VIAARRewardTermsViewControllers) {
            self.termController = termController
        }
        
        public func setCurrentRewardVoucher(as reward: VIAARRewardViewModel) {
            self.currentVoucher = reward
        }
        
        public func termsAccepted(for reward: VIAARRewardViewModel) {
            viewModel.confirmVoucher(reward, completion: { [weak self] error, newRewardId, voucherNumber in
                if let error = error {
                    self?.termController?.hideHUDFromWindow()
                    self?.termController?.handleBackendErrorWithAlert(error as? BackendError ?? .other, tryAgainAction: { [weak self] in
                        self?.termsAccepted(for: reward)
                    })
                } else {
                    self?.termController?.hideHUDFromWindow()
                    if let newRewardId = newRewardId {
                        self?.showChosenReward(with: newRewardId, consumer: self?.termController)
                    }
                }
            })
        }
        
        public func termsDeclined() {
            if let confirm = termController?.navigationController?.viewControllers.filter({$0 is VIAARRewardConfirmationViewController}).first {
                termController?.navigationController?.popToViewController(confirm, animated: true)
            } else if let currentVoucher = currentVoucher, let _ = currentVoucher.bespokeRewardBoardId, let navController = termController?.navigationController {
                navController.popViewController(animated: true)
            } else {
                termController?.performSegue(withIdentifier: "showConfirmSelectedReward", sender: nil)
            }
        }
    }
    
    extension ARRewardsFlowCoordinator: ARRewardConsumptionDelegate {
        func useReward(_ reward: VIAARRewardViewModel, consumer: VIAViewController?, completion: @escaping (Error?) -> Void) {
            viewModel.useVoucher(reward, completion: { error in
                completion(error)
            })
        }
    }
    
    extension ARRewardsFlowCoordinator: ARRewardConfirmationDelegate {
        func showSwapReward() {
            confirmationController?.performSegue(withIdentifier: "swapCurrentReward", sender: nil)
        }
        
        func showTermsAndConditions(for newReward: VIAARRewardViewModel) {
            
            confirmationController?.performSegue(withIdentifier: "showRewardTerms", sender: nil)
        }
        
        func skipTermsAndConditions(for newReward: Int, with consumer: UIViewController?) {
            
            if let consumer = consumer {
                consumer.showHUDOnWindow()
            }
            
            self.showChosenRewardAR(with: newReward, consumer: consumer)
        }
        
        fileprivate func showChosenRewardAR(with rewardId: Int, consumer: UIViewController?) {
            
            viewModel.getVoucher(with: rewardId, completion: { [weak self] (error, voucher) in
                
                if let consumer = consumer {
                    consumer.hideHUDFromWindow()
                }
                
                guard error == nil else {
                    (consumer as? VIAViewController)?.handleBackendErrorWithAlert(error as? BackendError ?? .other)
                    return
                }
                
                if let tenantID = AppSettings.getAppTenant(), tenantID == .SLI, let prevVoucherEffectiveFromDate = self?.currentVoucher?.effectiveFromDate {
                    voucher?.effectiveFromDate = prevVoucherEffectiveFromDate
                }

                self?.currentVoucher = voucher
                // This is called when clicking Select Voucher SLI
                //if let showWebVoucher = VIAApplicableFeatures.default.redirectToWebVoucher, showWebVoucher {
                //    self?.confirmationController?.performSegue(withIdentifier: "showWebVoucher", sender: voucher)
                //} else {
                    self?.confirmationController?.performSegue(withIdentifier: "skipRewardTerms", sender: nil)
                //}
                return
            })
        }
        
        func declineReward() {
            declineReward(consumer: confirmationController)
        }
        
        func declineReward(consumer: UIViewController?) {
            if let landing = consumer?.navigationController?.viewControllers.filter({$0 is VIAARRewardsLandingViewController}).first {
                
                consumer?.navigationController?.popToViewController(landing, animated: true)
            } else if let _ = consumer?.navigationController {
                consumer?.navigationController?.dismiss(animated: true, completion: nil)
            } else {
                consumer?.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    extension ARRewardsFlowCoordinator: VIAABespokeRewardDelegate {
        public func updateRewardStatusToUsed(_ reward: VIAARRewardViewModel, completion: ((Error?, UpdateRewardStatusResponse?) -> Void)?) {
            viewModel.updateRewardStatusToUsed(reward) { (error, response) in
                completion?(error, response)
            }
        }
        
        public func consumeReward(_ reward: VIAARRewardViewModel, completion: ((Error?) -> Void)?) {
            viewModel.useVoucher(reward, completion: { error in
                completion?(error)
            })
        }
        
        public func confirmReward(_ reward: VIAARRewardViewModel, consumer: UIViewController?, completion: ((Error?, Int?, String?) -> Void)?) {
            
            //consumer?.showHUDOnWindow()
            
            viewModel.confirmVoucher(reward, completion: { [weak self] error, newRewardId, voucherNumber in
                
                //consumer?.hideHUDFromWindow()
                
                if consumer != nil {
                    
                    self?.currentVoucher = reward
                    
                    if let reward = newRewardId {
                        
                        /** FC-26793 : UKE CR Interim Voucher Solution
                         * Redirect back if "reward" has a value.
                         * Don't call the showChosen function as it doubles the call of the services that result in error.
                         */
                        switch self?.currentVoucher?.rewardReference.rawValue {
                        case RewardReferences.AmazonSmileVoucher.rawValue,
                             RewardReferences.BookMyShow.rawValue,
                             RewardReferences.CafeCoffeeDay.rawValue,
                             RewardReferences.Spotify.rawValue:
                            completion?(nil, newRewardId, voucherNumber)
                            break
                        default:
                            self?.showChosen(rewardId: reward, consumer: consumer)
                            break
                        }
                        return
                    }
                }
                
                completion?(error, newRewardId, voucherNumber)
                return
            })
        }
        
        public func cancel(consumer: UIViewController?) {
            if let landing = consumer?.navigationController?.viewControllers.filter({$0 is VIAARRewardsLandingViewController}).first {
                consumer?.navigationController?.popToViewController(landing, animated: true)
            } else if let _ = consumer?.navigationController {
                consumer?.navigationController?.dismiss(animated: true, completion: nil)
            } else {
                consumer?.dismiss(animated: true, completion: nil)
            }
        }
        
        public func setRewardVoucher(as reward: VIAARRewardViewModel) {
            self.currentVoucher = reward
        }
        
        public func showChosen(rewardId: Int, consumer: UIViewController?) {
            self.showChosenReward(with: rewardId, consumer: consumer)
        }
    }
