import VitalityKit
import Foundation
import VIAUtilities

public class VIAARRewardViewModel {
	public var id: Int = 0
	public var subTitle: String
	public var title: String
	public var instructions: String?
	public var voucherId: Int = -1
    public var voucherCodes: [String]?
    public var partnerSysRewardId: [String]?
	public var image: UIImage
    public var effectiveFromDate: Date?
    public var effectiveToDate: Date?
	public var expiration: Date?
	public var accepted: Date?
    public var linkId: Int?
    public var rewardReference: RewardReferences
    public var rewardStatus: AwardedRewardStatusRef?
    public var unclaimedRewardId: Int?
    public var rewardkey: Int?
    public var awardedRewardStatusEffectiveOn: Date?
    public var exchangeOn: Date?
    public var awardedRewardReferences: [String]?
    
	var bespokeRewardBoardId: String?

    public init(id: Int, unclaimedRewardId: Int?, subTitle: String, title: String, image: UIImage, voucherId: Int, voucherCodes: [String]?, partnerSysRewardId: [String]?=nil, instructions: String?, expiration: Date?, effectiveFromDate: Date?, effectiveToDate: Date?, accepted: Date?, bespokeRewardBoardId: String?, linkId: Int?, rewardReference: RewardReferences, rewardStatus: AwardedRewardStatusRef?, rewardkey: Int?, awardedRewardStatusEffectiveOn: Date?, exchangeOn: Date? = nil, awardedRewardReferences: [String]? = nil) {
		self.id = id
		self.subTitle = subTitle
		self.title = title
		self.image = image
		self.voucherCodes = voucherCodes
        self.partnerSysRewardId = partnerSysRewardId
		self.voucherId = voucherId
		self.instructions = instructions
		self.expiration = expiration
        self.effectiveFromDate = effectiveFromDate
        self.effectiveToDate = effectiveToDate
		self.accepted = accepted
		self.bespokeRewardBoardId = bespokeRewardBoardId
        self.linkId = linkId
        self.rewardReference = rewardReference
        self.rewardStatus = rewardStatus
        self.unclaimedRewardId = unclaimedRewardId
        self.rewardkey = rewardkey
        self.awardedRewardStatusEffectiveOn = awardedRewardStatusEffectiveOn
        self.exchangeOn = exchangeOn
        self.awardedRewardReferences = awardedRewardReferences
	}

	init(id: Int, unclaimedRewardId: Int?, subTitle: String, title: String, image: UIImage, expiration: Date?, rewardReference: RewardReferences, rewardStatus: AwardedRewardStatusRef?) {
		self.id = id
		self.subTitle = subTitle
		self.title = title
		self.image = image
		self.voucherCodes = []
		self.instructions = ""
		self.expiration = expiration
        self.rewardReference = rewardReference
        self.rewardStatus = rewardStatus
        self.unclaimedRewardId = unclaimedRewardId
	}

	init(id: Int, unclaimedRewardId: Int?, subTitle: String, title: String, image: UIImage, voucherId: Int, voucherCodes: [String], instructions: String, rewardReference: RewardReferences, rewardStatus: AwardedRewardStatusRef?) {
		self.id = id
		self.subTitle = subTitle
		self.title = title
		self.image = image
		self.voucherCodes = voucherCodes
		self.voucherId = voucherId
		self.instructions = instructions
        self.rewardReference = rewardReference
        self.rewardStatus = rewardStatus
        self.unclaimedRewardId = unclaimedRewardId
	}

	init(id: Int, subTitle: String, title: String, image: UIImage, rewardReference: RewardReferences, rewardStatus: AwardedRewardStatusRef?) {
		self.id = id
		self.subTitle = subTitle
		self.title = title
		self.image = image
		self.voucherCodes = []
		self.instructions = ""
        self.rewardReference = rewardReference
        self.rewardStatus = rewardStatus
	}
    
    public func statusString() -> String {
        guard let status = self.rewardStatus else {
            return ""
        }
        //TODO : Localize if required
        switch status {
        case .Acknowledged:
            return "Acknowledged"
        case .Allocated:
            return CommonStrings.Ar.GoalPendingTitle693
        case .AvailableToRedeem:
            return "Available to redeem"
        case .Canceled:
            return "Canceled"
        case .Expired:
            return "Expired"
        case .Issued:
            return CommonStrings.Ar.Rewards.AvailableTitle1086
        case .IssueFailed:
            return "Issue failed"
        case .PartnerRegistration:
            return "Partner Registration"
        case .Used:
            return "Used"
        case .Unknown:
            return ""
        }
    }
}
