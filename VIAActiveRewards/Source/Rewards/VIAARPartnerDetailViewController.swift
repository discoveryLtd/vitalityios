import Foundation
import VIAUIKit
import VitalityKit
import VIACommon
import VIAUtilities

class VIAARPartnerDetailViewController: VIACoordinatedViewController {
    
    public var partnerViewModel: ParticipatingPartnersViewModel?
    public var partner: ParticipatingPartnersViewModel.PartnerData?
    private var partnerView: ARPartnersDetailView?
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.hideBackButtonTitle()
        configureAppearance()
        configureView()
        title = partner!.name
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    func configureAppearance() {
        navigationController?.makeNavigationBarTransparent()
    }
    
    func configureView() {
        if partnerViewModel != nil {
            guard let partnerView = ARPartnersDetailView.viewFromNib(owner: self)
                else {
                return
            }
            
            let partner = self.partner!
            
            DispatchQueue.global(qos: .background).async { [weak self] in
                self?.partnerViewModel?.partnerLogo(partner: partner) { logo, partner in
                    DispatchQueue.main.async {
                        partnerView.arPartnerImage = logo
                    }
                }
            }
            partnerView.arPartnerDescriptionText = partner.longDescription
            //partnerView.layoutMargins = UIEdgeInsets(top: 0, left: 23.0, bottom: 0, right: 23.0)
            self.view.addSubview(partnerView)
            partnerView.snp.makeConstraints({ (make) in
                make.top.bottom.right.left.equalToSuperview()
            })
            
            self.partnerView = partnerView
        }
    }
}

