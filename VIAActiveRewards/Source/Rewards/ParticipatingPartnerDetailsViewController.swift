import Foundation
import VIAUIKit
import VIAUtilities
import VitalityKit
import VIACommon
import SafariServices

class ParticipatingPartnerDetailsViewController: VIAViewController, UIWebViewDelegate, SafariViewControllerEscapable {
    
    public var viewModel: ParticipatingPartnersDetailsViewModel?
    @IBOutlet var webView: UIWebView!
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.hideBackButtonTitle()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideBackButtonTitle()
        retrievePartnerDetails()
        configureAppearance()
        title = viewModel?.partner.name
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    func configureAppearance() {
        navigationController?.makeNavigationBarTransparent()
        self.webView.backgroundColor = .white
        self.webView.isOpaque = true
    }
    
    // MARK: - Networking
    
    func retrievePartnerDetails() {
        self.showHUDOnView(view: self.webView)
		
		// TODO: Remove this ugliness after IGI partner info is implemented server side.
		if viewModel?.partner.name == "foodpanda" {
			self.webView.loadHTMLString(RewardPartnerWebContentProvider.foodpandaHtml, baseURL: nil)
			self.hideHUDFromView(view: self.webView)
			return
		} else if viewModel?.partner.name == "EasyTickets" {
			self.webView.loadHTMLString(RewardPartnerWebContentProvider.easyticketsHtml, baseURL: nil)
			self.hideHUDFromView(view: self.webView)
			return
		}

        viewModel?.requestEligibilityContent(completion: { [weak self] (error) in
            self?.hideFullScreenHUD()
            
            guard error == nil else {
                self?.handleError(error)
                return
            }
            if let webContent = self?.viewModel?.partner.webViewContent {
                self?.webView.loadHTMLString(webContent, baseURL: nil)
            }
            self?.hideHUDFromView(view: self?.webView)
        })
    }
    
    func handleError(_ error: Error?) {
        self.hideHUDFromView(view: self.webView)
        let statusView = handleBackendErrorWithStatusView((error as? BackendError) ?? .other, tryAgainAction: { [weak self] in
            self?.showHUDOnView(view: self?.webView)
            self?.retrievePartnerDetails()
        })
        configureStatusView(statusView)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showPartnerTermsAndConditions" {
            if let termsAndConditionsVC = segue.destination as? ParticipatingPartnerTermsAndConditionsViewController {
                termsAndConditionsVC.articleId = sender as? String
                let termsViewModel = PartnerTermsAndConditionsViewModel(articleId: termsAndConditionsVC.articleId)
                termsAndConditionsVC.viewModel = termsViewModel
            }
        }
    }
    
    // MARK: UIWebView delegate
    
    public func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        if let destinationURL = request.url {
            if destinationURL.absoluteString.components(separatedBy: "://")[0] == "action" {
                if navigationType == .linkClicked {
                    let contentId = destinationURL.absoluteString.components(separatedBy: "://")[1]
                    self.performSegue(withIdentifier: "showPartnerTermsAndConditions", sender: contentId)
                }
            }
        }

        return escapeToSafariViewController(navigationType: navigationType, request: request)
    }
}
