import Foundation
import VIAUIKit
import VIAUtilities
import VitalityKit
import VIACommon
import SafariServices

class PartnerTermsAndConditionsViewModel: CMSConsumer {
    
    var articleId: String?
    
    public init(articleId: String?) {
        self.articleId = articleId
    }
    
    func getCMSContent(completion: @escaping ((Error?, String?) -> Void)) {
        if let articleId = articleId {
            getCMSArticleContent(articleId: articleId, completion: completion)
        } else {
            completion(nil, nil)
        }
    }
    
}

class ParticipatingPartnerTermsAndConditionsViewController: VIAViewController, UIWebViewDelegate, SafariViewControllerEscapable {

    var viewModel: PartnerTermsAndConditionsViewModel?
    var articleId: String?
    
    @IBOutlet var webView: UIWebView!
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.hideBackButtonTitle()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = CommonStrings.Settings.TermsConditionsTitle905
        configureAppearance()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        retrieveTermsAndConditions()
    }
    
    func configureAppearance() {
        navigationController?.makeNavigationBarTransparent()
        self.webView.backgroundColor = .white
        self.webView.isOpaque = true
    }
    
    // MARK: - Networking
    
    func retrieveTermsAndConditions() {
        self.showHUDOnView(view: self.webView)
        self.viewModel?.getCMSContent(completion: { [weak self] (error, articleContent) in
            self?.hideHUDFromView(view: self?.webView)
            if let error = error as? BackendError {
                self?.handleBackendErrorWithAlert(error)
                return
            }
            if let content = articleContent {
                self?.webView.loadHTMLString(content, baseURL: nil)
            }
        })
    }
    
    func handleError(_ error: Error?) {
        self.hideHUDFromView(view: self.webView)
        let statusView = handleBackendErrorWithStatusView((error as? BackendError) ?? .other, tryAgainAction: { [weak self] in
            self?.showHUDOnView(view: self?.webView)
            self?.retrieveTermsAndConditions()
        })
        configureStatusView(statusView)
    }
    
    // MARK: UIWebView delegate
    
    public func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        return escapeToSafariViewController(navigationType: navigationType, request: request)
    }
}
