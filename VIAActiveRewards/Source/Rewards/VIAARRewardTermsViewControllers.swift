//
//  VIAARRewardTermsViewControllers.swift
//  VitalityActive
//
//  Created by Joshua Ryan on 6/12/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import VIAUIKit
import VitalityKit
import VIAUtilities
import VIACommon

public protocol ARRewardTermCompletionDelegate: ARRewardFlowDelegate {
    
    func termsAccepted(for reward: VIAARRewardViewModel)
    func termsDeclined()
    func setTermController(as termController: VIAARRewardTermsViewControllers)
    func setCurrentRewardVoucher(as reward: VIAARRewardViewModel)
}

public class VIAARRewardTermsViewControllers: VIACoordinatedViewController, ImproveYourHealthTintable {
    // MARK: Public
    public var dataProvider: VIAARRewardViewModel?
    public var delegate: ARRewardTermCompletionDelegate?
    
    // MARK: Private
    @IBOutlet private var agreeButton: UIBarButtonItem?
    @IBOutlet private var disagreeButton: UIBarButtonItem?
    @IBOutlet private var webView: UIWebView?
    
    private var hasLoadedContent: Bool = false
    
    // MARK: Lifecycle
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        configureTermsView()
    }
    
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        delegate?.setTermController(as: self)
        delegate?.setCurrentRewardVoucher(as: dataProvider!)
        loadWebContent()
    }
    
    func configureTermsView() {
        navigationController?.makeNavigationBarInvisible()
        navigationController?.navigationBar.isHidden = true
        navigationController?.makeNavigationBarTransparent()
    }
    
    func loadWebContent() {
        if self.hasLoadedContent {
            return
        }
        self.showHUDOnView(view: self.view)
        
        var articleId = AppConfigFeature.contentId(for: .SBDSConsentContent)
        if self.dataProvider?.rewardkey == 8 {
            articleId = AppConfigFeature.contentId(for: .AREasyTicketsDS)
        }
        else if (self.dataProvider?.rewardkey == 9){
            articleId = AppConfigFeature.contentId(for: .ARFoodPandaDS)
        }
        
        self.delegate?.loadArticleContent(articleId: articleId, completion: { [weak self] error, content in
            self?.hideHUDFromView(view: self?.view)
            if let error = error {
                self?.agreeButton?.isEnabled = false
                switch error {
                case is BackendError:
                    if let backendError = error as? BackendError {
                        self?.handleBackendErrorWithAlert(backendError, tryAgainAction: { [weak self] in
                            self?.agreeButton?.isEnabled = true
                            self?.loadWebContent()
                        })
                    }
                    break
                case is CMSError:
                    self?.displayErrorWithUnwindAction()
                    break
                default:
                    self?.displayErrorWithUnwindAction()
                    break
                }
            }
            if let validContent = content {
                self?.webView?.loadHTMLString(validContent, baseURL: nil)
            }
        })
    }
    
    func displayErrorWithUnwindAction() {
        displayUnknownServiceErrorOccurredAlert(okAction: { [weak self] in
            if let confirm = self?.navigationController?.viewControllers.filter({$0 is VIAARRewardConfirmationViewController}).first {
                self?.navigationController?.popToViewController(confirm, animated: true)
            }
        })
    }
    
    // MARK: Action buttons
    @IBAction func leftButtonTapped() {
        if let tenantID = AppSettings.getAppTenant() {
            if tenantID != .IGI {
                navigationController?.navigationBar.isHidden = false
            }
        }
        delegate?.termsDeclined()
    }
    
    @IBAction func rightButtonTapped() {
        if let tenantID = AppSettings.getAppTenant() {
            if tenantID != .IGI {
                navigationController?.navigationBar.isHidden = false
            }
        }
        if let dataProvider = dataProvider {
            self.showHUDOnWindow()
            delegate?.termsAccepted(for: dataProvider)
        }
    }
}
