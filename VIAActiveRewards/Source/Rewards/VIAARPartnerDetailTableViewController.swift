//
//  VIAARPartnerDetailTableViewController.swift
//  VIAActiveRewards
//
//  Created by Val Tomol on 14/02/2019.
//  Copyright © 2019 Glucode. All rights reserved.
//

import Foundation
import VIAUIKit
import VitalityKit
import VIACommon
import VIAUtilities

class VIAARPartnerDetailTableViewController: VIATableViewController {
    public var partnerViewModel: ParticipatingPartnersViewModel?
    public var partner: ParticipatingPartnersViewModel.PartnerData?
    
    fileprivate let SEGUE_T_AND_C = "showTermsAndConditions"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = partner!.name
        
        configureTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        configureAppearance()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        hideBackButtonTitle()
    }
    
    func configureAppearance() {
        navigationController?.makeNavigationBarTransparent()
    }
    
    func configureTableView() {
        self.tableView.register(VIATableViewCell.nib(), forCellReuseIdentifier: VIATableViewCell.defaultReuseIdentifier)
        self.tableView.estimatedRowHeight = 100
        self.tableView.sectionHeaderHeight = 0
        
        self.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        
        guard let partnerView = ARPartnersDetailView.viewFromNib(owner: self) else { return }
        let partner = self.partner!
        DispatchQueue.global(qos: .background).async { [weak self] in
            self?.partnerViewModel?.partnerLogo(partner: partner) { logo, partner in
                DispatchQueue.main.async {
                    partnerView.arPartnerImage = logo
                }
            }
        }
        partnerView.arPartnerDescriptionText = partner.longDescription
        partnerView.disableScrollAndAdjustViewHeight()
        self.tableView.tableHeaderView = partnerView
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == SEGUE_T_AND_C {
            if let webContentViewController = segue.destination as? VIAWebContentViewController {
                webContentViewController.title = CommonStrings.TermsAndConditions.ScreenTitle94 // TODO: valtomol Replace with ActiveRewardsStrings
                
                switch partner?.typeKey {
                case 257:
                    // TODO: valtomol-AR Get cinemark t&c content id once it is made available on the login response.
                    webContentViewController.viewModel = VIAWebContentViewModel(articleId: "ar-cinemark-terms-and-conditions")
                case 258:
                    // TODO: valtomol-AR Get juan valdez t&c content id once it is made available on the login response.
                    webContentViewController.viewModel = VIAWebContentViewModel(articleId: "ar-juanvaldez-terms-and-conditions")
                default:
                    webContentViewController.viewModel = VIAWebContentViewModel(articleId: AppConfigFeature.contentId(for: .TermsAndConditionsContent))
                }
            }
        }
    }
    
    enum Menu: Int {
        case termsAndConditions = 0
    }
    
    var menuRows: NSMutableIndexSet {
        let indexSet = NSMutableIndexSet()
        indexSet.add(Menu.termsAndConditions.rawValue)
        
        return indexSet
    }
    
    // UITableView Data Source and Delegate
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuRows.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return configureMenuRow(at: indexPath)
    }
    
    func configureMenuRow(at indexPath: IndexPath) -> VIATableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: VIATableViewCell.defaultReuseIdentifier, for: indexPath) as! VIATableViewCell
        
        if let menuItem = Menu(rawValue: menuRows.integer(at: indexPath.row)) {
            switch menuItem {
            case .termsAndConditions:
                cell.labelText = CommonStrings.TermsAndConditions.ScreenTitle94 // TODO: valtomol Replace with ActiveRewardsStrings
                cell.cellImage = UIImage(asset: VIAActiveRewardsAsset.ActiveRewards.activeRewardsTerms)
            }
        }
        
        cell.accessoryType = .disclosureIndicator
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: SEGUE_T_AND_C, sender: nil)
    }
}
