//
//  VIAARRewardSelectionViewController.swift
//  VitalityActive
//
//  Created by Joshua Ryan on 6/7/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import VIAUIKit
import VitalityKit
import VIACommon
import VIAUtilities

protocol ARRewardSelectionCompletionDelegate: ARRewardFlowDelegate {

	func selectionCompleted(_ winningReward: VIAARRewardViewModel)
	func selectionCanceled()
}


public class VIAARRewardSelectionViewController: VIACoordinatedTableViewController, ImproveYourHealthTintable {
    
    // MARK: Public
    public var parentJourney: ARRewardsParentJourney?
	public var credit: VIAARCreditViewModel? {
		didSet {
			viewModel.rewardId = credit?.id
		}
	}
    var delegate: ARRewardSelectionCompletionDelegate?
    var viewModel = VIAARAvailableRewardViewModel()
	
	// MARK: Private
	public var selectedIndex = IndexPath(row: 0, section: 0)

	// MARK: View lifecycle
    
	override public func viewDidLoad() {
		super.viewDidLoad()
        
		self.title = CommonStrings.Ar.Rewards.ChooseRewardTitle724
		self.tableView.sectionHeaderHeight = 0
		self.tableView.sectionFooterHeight = 0
		self.configureTableView()
		self.addBarButtonItems()
	}
    
    override public func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        configureAppearance()
    }
    
    public func setARSelectCompletionDelegate(_ delegate: ARRewardsFlowCoordinator) {
        self.delegate = delegate
    }
    
    func configureAppearance() {
        navigationController?.navigationBar.isHidden = false
        navigationController?.navigationBar.tintColor = UIColor.improveYourHealthBlue()
    }

	func configureTableView() {
		self.tableView.register(VIAARParticipatingPartnerCell.nib(), forCellReuseIdentifier: VIAARParticipatingPartnerCell.defaultReuseIdentifier)
		self.tableView.register(VIAARChooseRewardHeaderCell.nib(), forCellReuseIdentifier: VIAARChooseRewardHeaderCell.defaultReuseIdentifier)
		self.tableView.estimatedRowHeight = 100
		self.tableView.separatorInset = UIEdgeInsets(top: 25, left: 0, bottom: 0, right: 0)
	}

	// MARK: Action buttons

	func addBarButtonItems() {
		let cancel = UIBarButtonItem(title: CommonStrings.CancelButtonTitle24, style: .plain, target: self, action: #selector(cancelButtonTapped(_:)))
		self.navigationItem.leftBarButtonItem = cancel

		let done = UIBarButtonItem(title: CommonStrings.Ar.Rewards.ChooseRewardButtonTitle686, style: .plain, target: self, action: #selector(doneButtonTapped(_:)))
		self.navigationItem.rightBarButtonItem = done
	}

	func cancelButtonTapped (_ sender: UIBarButtonItem) {
        guard let unwindTo = parentJourney else {
            delegate?.selectionCanceled()
            return
        }
        
        switch unwindTo {
        case .HomeCard:
            unwindToHomeScreen(sender)
            return
        default:
            delegate?.selectionCanceled()
            return
        }
    
	}
    
    override public func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? VIAARDataSharingConsentViewController{
            vc.dataSharingDelegate = self as VIAARDataSharingDelegate
            vc.rewardSelectionViewController = self
        }
    }

	func doneButtonTapped (_ sender: UIBarButtonItem) {
        
        let section = Sections(rawValue: selectedIndex.section)
        if section == .header {
            return
        }
        guard let reward = viewModel.availableRewards.element(at: selectedIndex.row) else {
            return
        }
        
        self.performSegue(withIdentifier: "showDataSharingConsent", sender: nil)
        
//        delegate?.selectionCompleted(reward)
	}

	// MARK: UITableView datasource

	enum Sections: Int, EnumCollection {
		case header = 0
		case rewards = 1
	}

	override public func numberOfSections(in tableView: UITableView) -> Int {
		return Sections.allValues.count
	}

	override public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		let section = Sections(rawValue: section)
		if section == .header {
			return 1
		} else if section == .rewards {
			return viewModel.availableRewards.count
		}
		return 0
	}

	// MARK: UITableView delegate

	override public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let section = Sections(rawValue: indexPath.section)

		if section == .header {
			return self.configureHeaderCell(indexPath)
		} else if section == .rewards {
			return self.configureOptionCell(indexPath)
		}

		return tableView.dequeueReusableCell(withIdentifier: VIATableViewCell.defaultReuseIdentifier, for: indexPath)
	}

	override public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		if indexPath.section == 0 {
			return 310
		}

		return UITableViewAutomaticDimension
	}

	func configureHeaderCell(_ indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: VIAARChooseRewardHeaderCell.defaultReuseIdentifier, for: indexPath) as! VIAARChooseRewardHeaderCell
		cell.cellImage = UIImage(asset: VIAActiveRewardsAsset.ActiveRewards.Rewards.rewardTrophyBig)
		cell.title = CommonStrings.Ar.Rewards.ChooseRewardBodyTitle674
		cell.descriptionText = CommonStrings.Ar.Rewards.ChooseRewardInstruction646
		return cell
	}

	func configureOptionCell(_ indexPath: IndexPath) -> UITableViewCell {
		let item = viewModel.availableRewards[indexPath.row]

		let cell = self.tableView.dequeueReusableCell(withIdentifier: VIAARParticipatingPartnerCell.defaultReuseIdentifier, for: indexPath) as! VIAARParticipatingPartnerCell
		cell.partnerName = item.title
		cell.descriptionText = item.subTitle
		cell.cellImage = item.image

		if indexPath == selectedIndex {
			cell.accessoryType = .checkmark
		} else {
			cell.accessoryType = .none
		}

		cell.tintColor = UIColor.rewardBlue()

		return cell
	}

	override public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		selectedIndex = indexPath
		tableView.reloadData()
	}
    
    func unwindToHomeScreen(_ sender: Any?) {
        self.performSegue(withIdentifier: "unwindToHomeViewController", sender: nil)
    }
    
    @IBAction public func unwindFromVIAARDataSharingConsentToList(segue: UIStoryboardSegue) {}
    
}

extension VIAARRewardSelectionViewController: VIAARDataSharingDelegate {
    func dataSharingAccepted() {
        let section = Sections(rawValue: selectedIndex.section)
        if section == .header {
            return
        }
        guard let reward = viewModel.availableRewards.element(at: selectedIndex.row) else {
            return
        }
        
        delegate?.selectionCompleted(reward)
    }
}
