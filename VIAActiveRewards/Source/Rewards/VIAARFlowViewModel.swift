//
//  VIAActiveRewardsRewardsLandingViewModel.swift
//  VitalityActive
//
//  Created by Wilmar van Heerden on 2016/11/15.
//  Copyright © 2016 Glucode. All rights reserved.
//
import VIAUtilities
import VitalityKit
import VIACommon
import RealmSwift

class VIAARFlowViewModel: ActiveRewardsRewardsLandingViewModel {
    
    // MARK: Private
    
    private let arRealm = DataProvider.newARRealm()
    
    // MARK: Public (internal)
    
    var availableRewardCredits: [VIAARCreditViewModel] {
        var models = [VIAARCreditViewModel]()
        let unclaimedRewards = arRealm.allUnclaimedRewards().filter("awardedRewardStatus != %@", AwardedRewardStatusRef.AvailableToRedeem.rawValue).sorted(byKeyPath: "awardedOn", ascending: false)
        for unclaimedReward in unclaimedRewards {
            
            let winningVoucherId = unclaimedReward.outcomeRewardValueLinkId.value ?? 0
            let unclaimedRewardId = unclaimedReward.unclaimedRewardId
            
            if unclaimedReward.awardedRewardStatus == AwardedRewardStatusRef.Acknowledged.rawValue {
                
                if let arRewardSelection = arRealm.objects(ARRewardsSelection.self).filter("rewardValueLinkId == %@ && unclaimedRewardId == %@", winningVoucherId, unclaimedRewardId).first {
                    
                    let subTitle = VIAARRewardHelper.getValueForRewardSelection(arRewardSelection)
                    let bespokeRewardBoardId = VIAARRewardHelper.bespokeViewController(for: arRewardSelection.rewardId)
                    let dataProvider = VIAARRewardViewModel(id: arRewardSelection.id,
                                                            unclaimedRewardId: arRewardSelection.unclaimedRewardId,
                                                            subTitle: subTitle,
                                                            title: arRewardSelection.rewardName,
                                                            image: VIAARRewardHelper.partnerImage(for: arRewardSelection.rewardId),
                                                            voucherId: arRewardSelection.id,
                                                            voucherCodes: nil,
                                                            partnerSysRewardId: nil,
                                                            instructions: VIAARRewardHelper.partnerRewardInstructions(for: AwardedRewardStatusRef(rawValue: AwardedRewardStatusRef.Acknowledged.rawValue), rewardValue: "", effectiveFromDate: unclaimedReward.effectiveFrom),
                                                            expiration: unclaimedReward.effectiveTo,
                                                            effectiveFromDate: unclaimedReward.effectiveFrom,
                                                            effectiveToDate: unclaimedReward.effectiveTo,
                                                            accepted: unclaimedReward.awardedOn,
                                                            bespokeRewardBoardId: bespokeRewardBoardId,
                                                            linkId: arRewardSelection.rewardValueLinkId,
                                                            rewardReference: arRewardSelection.rewardId,
                                                            rewardStatus: AwardedRewardStatusRef(rawValue: unclaimedReward.awardedRewardStatus),
                                                            rewardkey: unclaimedReward.rewardkey,
                                                            awardedRewardStatusEffectiveOn: unclaimedReward.awardedRewardStatusEffectiveOn)
                    
                    models.append(VIAARCreditViewModel(id: unclaimedReward.id, earnedDate: unclaimedReward.effectiveFrom, expirationDate: unclaimedReward.effectiveTo, used: false, voucher: dataProvider, winningVoucherId: unclaimedReward.outcomeRewardValueLinkId.value, rewardSelectionTypeKey: unclaimedReward.rewardSelectionTypeKey.value))
                }
                
            } else {
                
                models.append(VIAARCreditViewModel(id: unclaimedReward.id, earnedDate: unclaimedReward.effectiveFrom, expirationDate: unclaimedReward.effectiveTo, used: false, voucher: nil, winningVoucherId: unclaimedReward.outcomeRewardValueLinkId.value, rewardSelectionTypeKey: unclaimedReward.rewardSelectionTypeKey.value))
                
            }
        }
        
        return models
    }
    
    var participatingPartners: [VIAARRewardViewModel] {
        var partnerModels = [VIAARRewardViewModel]()
        
        // TODO: Remove target specific hard coded partners once server can provide said information.
        
        let identifier = Bundle.main.object(forInfoDictionaryKey: "VAAppConfigIdentifier") as? String
        
        if identifier == "UKEssentials" {
            partnerModels.append(VIAARRewardViewModel(id: RewardReferences.Cineworld.rawValue, subTitle: CommonStrings.Ar.Partners.CineworldVoucherValue1056, title: CommonStrings.Ar.Partners.CineworldName1054, image: VIAARRewardHelper.partnerImage(for: RewardReferences.Cineworld), rewardReference: RewardReferences.Cineworld,
                                                      rewardStatus: nil))
            partnerModels.append(VIAARRewardViewModel(id: RewardReferences.Vue.rawValue, subTitle: CommonStrings.Ar.Partners.VueVoucherValue1057, title: CommonStrings.Ar.Partners.VueName1055, image: VIAARRewardHelper.partnerImage(for: RewardReferences.Vue), rewardReference: RewardReferences.Vue,
                                                      rewardStatus: nil))
            partnerModels.append(VIAARRewardViewModel(id: RewardReferences.StarbucksVoucher.rawValue, subTitle: CommonStrings.Ar.Partners.StarbucksVoucherValue705, title: CommonStrings.Ar.Partners.StarbucksName734, image: VIAARRewardHelper.partnerImage(for: RewardReferences.StarbucksVoucher), rewardReference: RewardReferences.StarbucksVoucher,
                                                      rewardStatus: nil))
        } else if identifier == "IGIVitality" {
            partnerModels.append(VIAARRewardViewModel(id: RewardReferences.EasyTickets.rawValue, subTitle: "Get 50% discounts on your next movie!", title: "EasyTickets", image: VIAARRewardHelper.partnerImage(for: RewardReferences.EasyTickets), rewardReference: RewardReferences.EasyTickets,
                                                      rewardStatus: nil))
            partnerModels.append(VIAARRewardViewModel(id: RewardReferences.FoodPanda.rawValue, subTitle: "PKR 500 Discount", title: "foodpanda", image: VIAARRewardHelper.partnerImage(for: RewardReferences.FoodPanda), rewardReference: RewardReferences.FoodPanda,
                                                      rewardStatus: nil))
        }
        
        return partnerModels
    }
    
    var usableRewards: [VIAARRewardViewModel] {
        var items = [VIAARRewardViewModel]()
        let vouchers: [ARVoucher] = Array(arRealm.allEffectiveVouchers())
        
        if vouchers.count > 0 {
            for voucher in vouchers {
                var effectiveFrom = voucher.effectiveFrom
                effectiveFrom = getAchievedDate(voucher: voucher)
                
                let subTitle: String? = VIAARRewardHelper.getValueForVoucher(voucher)
                let bespokeRewardBoardId = VIAARRewardHelper.bespokeViewController(for: voucher.rewardId)
                let instructions = VIAARRewardHelper.partnerRewardInstructions(for: AwardedRewardStatusRef(rawValue: voucher.awardedRewardStatus),
                                                                               rewardValue: subTitle,
                                                                               effectiveFromDate: voucher.effectiveFrom)
                
                if VIAApplicableFeatures.default.hideVitalityCoinsFromRewards() {
                    if voucher.rewardkey != 13 && voucher.rewardTypeKey != 8 {
                        items = appendRewardDetails(items: items, voucher: voucher, subTitle: subTitle, bespokeRewardBoardId: bespokeRewardBoardId, instructions: instructions, effectiveFrom: effectiveFrom)
                    }
                }
                //FC-25464: Device Cashback Reward Displaying in Active Rewards
                  else if AppSettings.getAppTenant() == .IGI {
                    if voucher.rewardkey != 12 && voucher.rewardTypeKey != 4 {
                        items = appendRewardDetails(items: items, voucher: voucher, subTitle: subTitle, bespokeRewardBoardId: bespokeRewardBoardId, instructions: instructions, effectiveFrom: effectiveFrom)
                    }
                } else {
                    items = appendRewardDetails(items: items, voucher: voucher, subTitle: subTitle, bespokeRewardBoardId: bespokeRewardBoardId, instructions: instructions, effectiveFrom: effectiveFrom)
                }
            }
        }
        
        var sortedItems = [VIAARRewardViewModel]()
        sortedItems = items.sorted(by: { $0.effectiveFromDate! < $1.effectiveFromDate! })
        
        return sortedItems
    }
    
    func getAchievedDate(voucher: ARVoucher) -> Date? {
        
        var effectiveFrom = voucher.effectiveFrom

        let tenantID = AppSettings.getAppTenant()
        let isSLITenant = tenantID == .SLI ? true : false
        let isNotVitalityCoins = voucher.rewardkey != 13 && voucher.rewardTypeKey != 8 ? true : false
        
        if isSLITenant && isNotVitalityCoins {
            let usedSpins = arRealm.allUnclaimedUsedRewards().filter("awardedRewardStatus == %@", AwardedRewardStatusRef.Used.rawValue)

            if let _ = voucher.exchangeFromRewardId.value {
                if let usedSpin = usedSpins.filter("id == %@", voucher.exchangeFromRewardId).first, let effectiveFromDate = usedSpin.effectiveFrom {
                    effectiveFrom = effectiveFromDate
                } else {
                    Wire.Rewards.getAwardedRewardById(tenantId: (tenantID?.rawValue)!, awardedRewardId: voucher.exchangeFromRewardId.value!) { (error) in
                    }
                }
                return effectiveFrom
            }
        }
        
        return effectiveFrom
    }
    
    func appendRewardDetails(items: [VIAARRewardViewModel], voucher: ARVoucher,
                             subTitle: String?, bespokeRewardBoardId: String?,
                             instructions: String?, effectiveFrom: Date?) -> [VIAARRewardViewModel] {
        
        var rewards = items
        rewards.append(VIAARRewardViewModel(id: voucher.id,
                                          unclaimedRewardId: nil,
                                          subTitle: subTitle ?? "",
                                          title: voucher.rewardName,
                                          image: VIAARRewardHelper.partnerImage(for: voucher.rewardId),
                                          voucherId: voucher.id,
                                          voucherCodes: voucher.voucherCodes(),
                                          partnerSysRewardId: voucher.partnerSysRewardIds(),
                                          instructions: instructions,
                                          expiration: voucher.effectiveTo,
                                          effectiveFromDate: effectiveFrom,
                                          effectiveToDate: voucher.effectiveTo,
                                          accepted: voucher.awardedOn,
                                          bespokeRewardBoardId: bespokeRewardBoardId,
                                          linkId: voucher.rewardValueLinkId,
                                          rewardReference: voucher.rewardId,
                                          rewardStatus: AwardedRewardStatusRef(rawValue: voucher.awardedRewardStatus),
                                          rewardkey: voucher.rewardkey,
                                          awardedRewardStatusEffectiveOn: voucher.awardedRewardStatusEffectiveOn))
        return rewards
    }
    
    var pastRewards: [VIAARRewardViewModel] {
        var items = [VIAARRewardViewModel]()
        var vouchers: [ARVoucher] = Array()
        var rewards: [ARUnclaimedReward] = Array()
        
        // Vouchers
        let expiredVouchers: [ARVoucher] = Array(arRealm.allExpiredVouchers().filter("(rewardTypeKey != %@)",
                                                                                    RewardTypeRef.NoReward.rawValue))
        let usedVouchers: [ARVoucher] = Array(arRealm.allUsedVouchers().filter("(rewardTypeKey != %@)",
                                                                               RewardTypeRef.NoReward.rawValue))
        let canceledVouchers: [ARVoucher] = Array(arRealm.allCanceledVouchers().filter("(rewardTypeKey != %@)",
                                                                                       RewardTypeRef.NoReward.rawValue))
        let issueFailedVouchers: [ARVoucher] = Array(arRealm.allIssueFailedVouchers().filter("(rewardTypeKey != %@)",
                                                                                       RewardTypeRef.NoReward.rawValue))

        vouchers.append(contentsOf: expiredVouchers)
        vouchers.append(contentsOf: usedVouchers)
        vouchers.append(contentsOf: canceledVouchers)
        vouchers.append(contentsOf: issueFailedVouchers)
        
        vouchers = vouchers.sorted(by: { (this: ARVoucher, that: ARVoucher) -> Bool in
            
            var thisDate = this.effectiveTo
            var thatDate = that.effectiveTo
            
            // NOTE: effectiveTo and effectiveFrom are currently the values for the most recent status, not necessariy a vouchers to and from
            if this.awardedRewardStatus == AwardedRewardStatusRef.Used.rawValue {
                thisDate = this.effectiveFrom
            }
            
            if that.awardedRewardStatus == AwardedRewardStatusRef.Used.rawValue {
                thatDate = that.effectiveFrom
            }
            
            if thisDate == nil {
                return false
            }
            else if thatDate == nil {
                return true
            }
            
            return thisDate! > thatDate!
        })
        
        for voucher in vouchers {
            let subTitle: String? = VIAARRewardHelper.getValueForVoucher(voucher)
            let bespokeRewardBoardId = VIAARRewardHelper.bespokeViewController(for: voucher.rewardId)
            let instructions = VIAARRewardHelper.partnerRewardInstructions(for: AwardedRewardStatusRef(rawValue: voucher.awardedRewardStatus),
                                                                           rewardValue: subTitle,
                                                                           effectiveFromDate: voucher.effectiveFrom)
            
            items.append(VIAARRewardViewModel(id: voucher.id,
                                              unclaimedRewardId: nil,
                                              subTitle: subTitle ?? "",
                                              title: voucher.rewardName,
                                              image: VIAARRewardHelper.partnerImage(for: voucher.rewardId),
                                              voucherId: voucher.id,
                                              voucherCodes: voucher.voucherCodes(),
                                              partnerSysRewardId: voucher.partnerSysRewardIds(),
                                              instructions: instructions,
                                              expiration: voucher.effectiveTo,
                                              effectiveFromDate: voucher.effectiveFrom,
                                              effectiveToDate: voucher.effectiveTo,
                                              accepted: nil,
                                              bespokeRewardBoardId: bespokeRewardBoardId,
                                              linkId: voucher.rewardValueLinkId,
                                              rewardReference: voucher.rewardId,
                                              rewardStatus: AwardedRewardStatusRef(rawValue: voucher.awardedRewardStatus),
                                              rewardkey: voucher.rewardkey,
                                              awardedRewardStatusEffectiveOn: voucher.awardedRewardStatusEffectiveOn))
        }
        //Rewards

        let expiredRewards: [ARUnclaimedReward] = Array(arRealm.allUnclaimedExpiredRewards().filter("(rewardTypeKey != %@)",
                                                                                     RewardTypeRef.NoReward.rawValue))
        var usedRewards: [ARUnclaimedReward] = []
        /* Only include rewardTypeCode filter on IGI */
        if let filterRewardTypeCode = VIAApplicableFeatures.default.filterRewardTypeCode, filterRewardTypeCode {
            usedRewards = Array(arRealm.allUnclaimedUsedRewards().filter("(rewardTypeKey != %@)", RewardTypeRef.NoReward.rawValue).filter("rewardTypeCode != %@", "RewardOpportunity"))
        } else {
            usedRewards = Array(arRealm.allUnclaimedUsedRewards().filter("(rewardTypeKey != %@)", RewardTypeRef.NoReward.rawValue))
        }
        
        if VIAApplicableFeatures.default.shouldIncludeUsedAndExpiredSpinInHistory() {
            rewards.append(contentsOf: expiredRewards)
            rewards.append(contentsOf: usedRewards)
            
            rewards = rewards.sorted(by: { (this: ARUnclaimedReward, that: ARUnclaimedReward) -> Bool in
                
                var thisDate = this.effectiveTo
                var thatDate = that.effectiveTo
                
                // NOTE: effectiveTo and effectiveFrom are currently the values for the most recent status, not necessariy a rewards to and from
                if this.awardedRewardStatus == AwardedRewardStatusRef.Used.rawValue {
                    thisDate = this.effectiveFrom
                }
                
                if that.awardedRewardStatus == AwardedRewardStatusRef.Used.rawValue {
                    thatDate = that.effectiveFrom
                }
                
                if thisDate == nil {
                    return false
                }
                else if thatDate == nil {
                    return true
                }
                
                return thisDate! > thatDate!
            })
            
            for reward in rewards {
                let subTitle: String? = VIAARRewardHelper.getValueForUnclaimedReward(reward)
                let bespokeRewardBoardId = VIAARRewardHelper.bespokeViewController(for: reward.rewardId)
                let instructions = VIAARRewardHelper.partnerRewardInstructions(for: AwardedRewardStatusRef(rawValue: reward.awardedRewardStatus),
                                                                               rewardValue: subTitle,
                                                                  effectiveFromDate: reward.effectiveFrom)
                
                items.append(VIAARRewardViewModel(id: reward.id,
                                                  unclaimedRewardId: reward.unclaimedRewardId,
                                                  subTitle: subTitle ?? "",
                                                  title: reward.rewardName,
                                                  image: VIAARRewardHelper.partnerImage(for: reward.rewardId),
                                                  voucherId: reward.id,
                                                  voucherCodes: nil,
                                                  partnerSysRewardId: nil,
                                                  instructions: instructions,
                                                  expiration: reward.effectiveTo,
                                                  effectiveFromDate: reward.effectiveFrom,
                                                  effectiveToDate: reward.effectiveTo,
                                                  accepted: nil,
                                                  bespokeRewardBoardId: bespokeRewardBoardId,
                                                  linkId: nil,
                                                  rewardReference: reward.rewardId,
                                                  rewardStatus: AwardedRewardStatusRef(rawValue: reward.awardedRewardStatus),
                                                  rewardkey: reward.rewardkey,
                                                  awardedRewardStatusEffectiveOn: reward.awardedRewardStatusEffectiveOn))
            }
        }
        return items
    }
    
    var pastRewardsByMonth: ([String],[[VIAARRewardViewModel]]) {
        var months = [String]()
        var items = [[VIAARRewardViewModel]]()
        let pastRewards = self.pastRewards
        
        for reward in pastRewards {
            var month: String?

            if reward.rewardStatus?.rawValue == AwardedRewardStatusRef.Used.rawValue {
                if var date = reward.effectiveFromDate{
                    if AppSettings.getAppTenant() == .SLI ||
                        AppSettings.getAppTenant() == .EC ||
                        AppSettings.getAppTenant() == .ASR {
                        if let effectiveOn = reward.awardedRewardStatusEffectiveOn {
                            date = effectiveOn
                        }
                    }
                    month = Localization.fullMonthAndYearFormatter.string(from: date)
                }
            } else if let date = reward.expiration {
                month = Localization.fullMonthAndYearFormatter.string(from: date)
            }
            
            if let month = month {
                if !months.contains(month) {
                    months.append(month)
                    items.append([VIAARRewardViewModel]())
                }
                
                if let index = months.index(of: month) {
                    items[index].append(reward)
                }
            }
        }
        
        return (months, items)
    }
    
    /** FC-26793 : UKE : Change Request
     * Added VoucherNumber to the Completion Block.
     * VoucherNumber is required to show the Voucher Code.
     */
    func confirmVoucher(_ reward: VIAARRewardViewModel, completion: @escaping (Error?, Int?, String?) -> Void) {
        guard let tenant = AppSettings.getAppTenant() else { return }
        
        switch tenant {
        case .CA, .SLI:
            VIAARRewardHelper.selectReward(with: reward, with: ExchangeTypeRef.Swapped, completion: { (error, newRewardId, voucherNumber) in
                completion(error, newRewardId, nil)
            })
        case .UKE:
            VIAARRewardHelper.confirmedReward(reward: reward, completion: { (error) in
                guard (error != nil) else {
                    VIAARRewardHelper.selectReward(with: reward, with: ExchangeTypeRef.Selected, completion: { (error, newRewardId, voucherNumber) in
                        completion(error, newRewardId, voucherNumber)
                    })
                    return
                }
                completion(error, nil, nil)
            })
        default:
            VIAARRewardHelper.selectReward(with: reward, with: ExchangeTypeRef.Selected, completion: { (error, newRewardId, voucherNumber) in
                completion(error, newRewardId, nil)
            })
        }
    }
    
    func updateRewardStatusToUsed(_ reward: VIAARRewardViewModel, completion: @escaping(Error?, UpdateRewardStatusResponse?) -> Void ) {
        VIAARRewardHelper.updateRewardStatusToUsed(reward: reward) { (error, response) in
            completion(error, response)
        }
    }
    
    func updateRewardStatusToAcknowledged(_ creditModel: VIAARCreditViewModel, rewardModel: VIAARRewardViewModel,completion: @escaping(Error?, UpdateRewardStatusResponse?) -> Void ) {
        VIAARRewardHelper.updateRewardStatusToAcknowledged(reward: rewardModel) { (error, response) in
            completion(error, response)
        }
    }
    
    func selectVoucherForReward(_ creditModel: VIAARCreditViewModel, rewardModel: VIAARRewardViewModel, completion: @escaping (Error?, VIAARRewardViewModel?) -> Void) {
        completion(nil, nil)
    }
    
    func useVoucher(_ reward: VIAARRewardViewModel, completion: @escaping (Error?) -> Void) {
        //		let credit = arRealm.objects(ARRewardCredit.self).filter("id = \(creditModel.id)").first
        //
        //		guard credit != nil else {
        //			fatalError("Illegal state: No credit found for \(creditModel.id)")
        //		}
        //		guard let voucher = credit?.voucher else {
        //			fatalError("Illegal state: No voucher selected to confirm")
        //		}
        //
        //		try! arRealm.write {
        //			voucher.dateRedeemed = Date()
        //		}
        //
        //		let voucherModel = VIAARRewardViewModel(id: voucherModel.id,
        //												subTitle: voucherModel.subTitle,
        //												title: voucherModel.title,
        //												image: voucherModel.image,
        //												voucherId: voucher.id,
        //												voucherCode: voucher.code,
        //												instructions: voucherModel.instructions,
        //												value: voucherModel.value,
        //												expiration: voucher.dateExpiration,
        //												used: voucher.dateRedeemed,
        //												accepted: voucher.dateConfirmed,
        //												bespokeRewardBoardId: voucher.reward?.bespokeRewardBoardId)
        //
        //		completion(nil, voucherModel)
        completion(nil)
    }
    
    func getVoucher(with id: Int, completion: @escaping (Error?, VIAARRewardViewModel?) -> Void) {
        VIAARRewardHelper.getReward(id) { (error, voucher) in
            return completion(error, voucher)
        }
    }
}

// MARK: Mark as Used / Unused
extension VIAARFlowViewModel {
    func updateRewardStatusToUsedOrUnused(_ reward: VIAARRewardViewModel, status: AwardedRewardStatusRef, completion: @escaping((Error?, UpdateRewardStatusResponse?) -> Void)) {
        VIAARRewardHelper.updateRewardStatusToUsedOrUnused(reward: reward, status: status) { (error, response) in
            completion(error, response)
        }
    }
    
    func submitEventForRewardStatusChange(_ reward: VIAARRewardViewModel, status: AwardedRewardStatusRef, completion: @escaping((Error?) -> Void)) {
        VIAARRewardHelper.submitEventForRewardStatusChange(reward: reward, status: status) { (error) in
            completion(error)
        }
    }
}
