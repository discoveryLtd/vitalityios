import Foundation
import VitalityKit
import RealmSwift
import VIACommon

class ParticipatingPartnersDetailsViewModel {
    
    var partner: ParticipatingPartnersViewModel.PartnerData
    init(for partner: ParticipatingPartnersViewModel.PartnerData) {
        self.partner = partner
    }
    
    //MARK: Networking
    
    public func requestEligibilityContent(completion: @escaping (Error?) -> Void) {
        let tenantId = DataProvider.newRealm().getTenantId()
        let partyId = DataProvider.newRealm().getPartyId()
        let membershipId = DataProvider.newRealm().getMembershipId()
        let productFeatureKey = partner.typeKey
        Wire.Member.getEligibilityContent(tenantId: tenantId, partyId: partyId, membershipId: membershipId, productFeatureKey: productFeatureKey) { [weak self] error, urlString, content in
            guard error == nil else {
                completion(error)
                return
            }
            if let webContent = content {
                self?.partner.webViewContent = RewardPartnerWebContentProvider.templateHtml.replacingOccurrences(of: "<body></body>", with: "<body>\(webContent)</body>")
            }
            completion(nil)
        }
    }

}
