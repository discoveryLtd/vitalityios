//
//  VIAARRewardSpinViewController.swift
//  VitalityActive
//
//  Created by Joshua Ryan on 6/6/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import VIAUIKit
import VitalityKit
import VIACommon
import VIAUtilities
import VIACarousel
import pop

public enum ARRewardsParentJourney: Int {
    case HomeCard = 0
    case Rewards = 1
}

protocol ARRewardSpinCompletionDelegate: ARRewardFlowDelegate {
    
    func spinCompleted(_ winningReward: VIAARRewardViewModel)
    func spinCanceled()
    func setRewardCredit(as credit: VIAARCreditViewModel)
    
}

public class VIAARRewardSpinViewController: VIACoordinatedViewController, ImproveYourHealthTintable {
    
    // MARK: Public
    public var parentJourney: ARRewardsParentJourney?
    public var credit: VIAARCreditViewModel? {
        didSet {
            viewModel.rewardId = credit?.id
            viewModel.effectiveFrom = credit?.earnedDate
        }
    }
    var delegate: ARRewardSpinCompletionDelegate?
    @IBOutlet weak var spinnerView: VIAARRewardSpinnerView?
    
    var viewModel = VIAARAvailableRewardViewModel()
    var currentItemIndex: Int = 0
    var isSpinningUpward: Bool = true
    
    // MARK: Private
    
    // Wheel card size and spacing
    var itemWidth = CGFloat(268)
    var itemHeight = CGFloat(73)
    
    // MARK: View lifecycle
    override public func viewDidLoad() {
        super.viewDidLoad()
        self.title = CommonStrings.Ar.Rewards.SpinTitle719
        self.addSpinnerView()
        self.addBarButtonItems()
    }
    
    override public func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.spinnerView?.initializeCarousel(delegate: self)
        self.spinnerView?.cellImage = UIImage(asset: VIAActiveRewardsAsset.ActiveRewards.Rewards.rewardTrophyBig)
        self.spinnerView?.title = CommonStrings.Ar.Rewards.SpinBodyTitle701
        self.spinnerView?.instructions = CommonStrings.Ar.Rewards.SpinInstruction718
        self.spinnerView?.spinButton.addTarget(self, action: #selector(spinNow), for: .touchUpInside)
        self.spinnerView?.spinButton.setTitle(CommonStrings.Ar.Rewards.SpinNowTitle704, for: .normal)
        self.spinnerView?.partnersButton.addTarget(self, action: #selector(showPartners), for: .touchUpInside)
        self.spinnerView?.partnersButton.setTitle(CommonStrings.Ar.LearnMore.ParticipatingPartnersTitle696, for: .normal)
    }
    
    override public func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.hideBackButtonTitle()
    }
    
    public func setSpinCompletionDelegate(_ delegate: ARRewardsFlowCoordinator) {
        self.delegate = delegate
    }
    
    func addSpinnerView() {
        
        guard let spinnerView = VIAARRewardSpinnerView.viewFromNib(owner: self) else {
            return
        }
        
        self.spinnerView = spinnerView
        self.view.addSubview(spinnerView)
        
        spinnerView.snp.makeConstraints { (make) in
            make.top.equalTo(topLayoutGuide.snp.bottom)
            make.bottom.equalTo(bottomLayoutGuide.snp.top)
            make.left.right.equalToSuperview()
        }
    }
    
    // MARK: Action buttons
    
    func addBarButtonItems() {
        let cancel = UIBarButtonItem(title: CommonStrings.CancelButtonTitle24, style: .plain, target: self, action: #selector(cancelButtonTapped(_:)))
        self.navigationItem.leftBarButtonItem = cancel
    }
    
    func cancelButtonTapped (_ sender: UIBarButtonItem) {
        guard let unwindTo = parentJourney else {
            delegate?.spinCanceled()
            return
        }
        switch unwindTo {
        case .HomeCard:
            unwindToHomeScreen(sender)
            return
        default:
            delegate?.spinCanceled()
            return
        }
    }
    
    // MARK: IBAction
    
    @IBAction func spinNow(sender: Any?) {
        guard let winningId = credit?.winningVoucherId, let index = viewModel.availableRewards.index(where: { $0.linkId == winningId }) else {
            // TODO: Error message
            return
        }
        
        self.spinnerView?.spin(targetIndex: index, isSpinningUpward: isSpinningUpward)
    }
    
    @IBAction func showPartners(sender: Any?) {
        performSegue(withIdentifier: "showParticipatingPartners", sender: nil)
    }
    
    // MARK: VIARewardCarouselDelegate
    func completeRewardSelection() {
        let rewards = viewModel.availableRewards
        for reward in rewards {
            if reward.linkId == credit?.winningVoucherId {
                delegate?.spinCompleted(reward)
                debugPrint("\(reward.title)")
                return
            }
        }
    }
    
    func unwindToHomeScreen(_ sender: Any?) {
        self.performSegue(withIdentifier: "unwindToHomeViewController", sender: nil)
    }
}

extension VIAARRewardSpinViewController: iCarouselDelegate, iCarouselDataSource {
    
    // MARK: iCarouselDataSource
    
    public func numberOfItems(in carousel: iCarousel) -> Int {
        return self.viewModel.availableRewards.count
    }
    
    public func carousel(_ carousel: iCarousel, viewForItemAt index: Int, reusing view: UIView?) -> UIView {
        var labelTop: UILabel
        var labelBottom: UILabel
        var itemView: UIView
        let currentModel = self.viewModel.availableRewards[index]
        
        //reuse view if available, otherwise create a new view
        //		if let reusedView = view {
        //			itemView = reusedView
        //			labelTop = itemView.viewWithTag(0)?.viewWithTag(1)?.viewWithTag(2) as! UILabel
        //			labelBottom = itemView.viewWithTag(0)?.viewWithTag(1)?.viewWithTag(3) as! UILabel
        //		} else {
        
        itemView = UIView(frame: CGRect(x: 0, y: 0, width: self.itemWidth, height: self.itemHeight))
        itemView.layer.cornerRadius = 5.0
        itemView.layer.borderWidth = 1.0
        itemView.layer.borderColor = UIColor.viewBorder().cgColor
        itemView.clipsToBounds = true
        itemView.backgroundColor = .white
        
        // top label
        labelTop = UILabel(frame: CGRect(x: 0, y: 0, width: 230, height: 20))
        labelTop.textAlignment = .left
        labelTop.font = UIFont.subheadlineFont()
        labelTop.tag = 2
        
        // bottom label
        labelBottom = UILabel(frame: CGRect(x: 0, y: 0, width: 230, height: 28))
        labelBottom.textAlignment = .left
        labelBottom.font = UIFont.title2Font()
        labelBottom.tag = 3
        
        // container for all items
        let stackViewContainer = UIStackView()
        stackViewContainer.axis = .horizontal
        stackViewContainer.distribution = .fillProportionally
        stackViewContainer.alignment = .center
        stackViewContainer.spacing = 8
        stackViewContainer.translatesAutoresizingMaskIntoConstraints = false
        stackViewContainer.layoutMargins = UIEdgeInsets(top: 0, left: 13, bottom: 0, right: 0)
        stackViewContainer.isLayoutMarginsRelativeArrangement = true
        stackViewContainer.tag = 0
        
        // labels container
        let stackViewLabels = UIStackView()
        stackViewLabels.axis = .vertical
        stackViewLabels.distribution = .fillProportionally
        stackViewLabels.translatesAutoresizingMaskIntoConstraints = false
        stackViewLabels.addArrangedSubview(labelTop)
        stackViewLabels.addArrangedSubview(labelBottom)
        stackViewLabels.tag = 1
        
        // image view
        let imgView = UIImageView(frame: CGRect(x: 0, y: 0, width: 65, height: 53))
        imgView.image = currentModel.image
        imgView.contentMode = .scaleAspectFit
        let c1 = imgView.widthAnchor.constraint(equalToConstant: 65)
        c1.isActive = true
        c1.identifier = "img-w"
        let c2 = imgView.heightAnchor.constraint(equalToConstant: 53)
        c2.isActive = true
        c2.identifier = "img-h"
        
        stackViewContainer.addArrangedSubview(imgView)
        
        stackViewContainer.addArrangedSubview(stackViewLabels)
        
        itemView.addSubview(stackViewContainer)
        
        itemView.addConstraints([
            stackViewContainer.leftAnchor.constraint(equalTo: itemView.leftAnchor),
            stackViewContainer.topAnchor.constraint(equalTo: itemView.topAnchor),
            stackViewContainer.rightAnchor.constraint(equalTo: itemView.rightAnchor),
            stackViewContainer.bottomAnchor.constraint(equalTo: itemView.bottomAnchor)])
        
        //		}
        
        labelTop.text = currentModel.title
        labelBottom.text = currentModel.subTitle
        
        return itemView
    }
    
    // MARK: iCarouselDelegate
    
    public func carouselItemWidth(_ carousel: iCarousel) -> CGFloat {
        return 90
    }
    
    public func carousel(_ carousel: iCarousel, valueFor option: iCarouselOption, withDefault value: CGFloat) -> CGFloat {
        
        if let spinnerContainer = spinnerView?.spinnerWrapper {
            if option == .radius {
                return floor(spinnerContainer.frame.size.height / 2);
            } else if option == .showBackfaces {
                return 0.0;
            } else if option == .count {
                
                // TODO: obj-c version had the following, but iCarousel.sizeHeight isn't a thing
                //((self.surpriseRewardViewControllerView.spinnerContainerView.frame.size.height/2 * M_PI * 2)/(((self.spinnerView.sizeHeight / 5) + 15) + 5));
                
                return ((spinnerContainer.frame.size.height/2 * .pi * 2) / ((spinnerContainer.frame.size.height / 5) + 18));
            } else if option == .fadeMinAlpha {
                return 1.0;
            }
        }
        
        return value;
    }
    
    public func carousel(_ carousel: iCarousel, shouldSelectItemAt index: Int) -> Bool {
        if let spinButton = spinnerView?.spinButton, spinButton.isEnabled {
            return true
        }
        return false
    }
    
    public func carouselWillBeginDecelerating(_ carousel: iCarousel) {
        self.spinNow(sender: self)
    }
    
    public func carouselCurrentItemIndexDidChange(_ carousel: iCarousel) {
        isSpinningUpward = isSpinningUpward(carousel)
    }
    
    func isSpinningUpward(_ carousel: iCarousel) -> Bool {
        let previousItemIndex = currentItemIndex
        currentItemIndex = carousel.currentItemIndex
        
        if previousItemIndex < currentItemIndex {
            return (carousel.currentItemIndex == carousel.numberOfItems - 1 && previousItemIndex == 0) ? false : true
        } else {
            return (carousel.currentItemIndex == 0 && previousItemIndex == carousel.numberOfItems - 1) ? true : false
        }
    }
    
    public func carouselDidEndScrollingAnimation(_ carousel: iCarousel) {
        if let spinButton = self.spinnerView?.spinButton, let currentView = carousel.currentItemView, !spinButton.isEnabled {
            currentView.layer.borderColor = self.colorForItemAt(index: carousel.currentItemIndex).cgColor
            currentView.layer.borderWidth = 2.0
            currentView.layer.cornerRadius = 4.0
            currentView.layer.masksToBounds = true
            bounce(view: currentView)
            spinnerView?.partnersButton.isEnabled = true
            
            // TODO: Delay added just to show / test bounce. We probably want to adjust delay, buttons and/or show congratulations alert
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + .seconds(1)) {
                self.completeRewardSelection()
            }
        }
    }
    
    // MARK: Item-delegate-ish-stuff
    // TODO: Should be in item data model?
    func colorForItemAt(index: Int) -> UIColor {
        // TODO: Need per item color logic?
        return UIColor.currentGlobalTintColor()
    }
    
    func imageForItemAt(index: Int) -> UIImage {
        // TODO: Need logic
        return UIImage(asset: VIAActiveRewardsAsset.ActiveRewards.Rewards.partnerStarbucks)
    }
    
    // MARK: Animation
    public func bounce(view: UIView!) {
        let initialSize = CGSize(width: 1.0, height: 1.0)
        let animationSize = CGSize(width: 1.1, height: 1.1)
        
        if let spring = POPSpringAnimation(propertyNamed: kPOPLayerScaleXY) {
            spring.fromValue = NSValue(cgSize:initialSize);
            spring.toValue = NSValue (cgSize:animationSize);
            spring.springBounciness = 10;
            spring.springSpeed = 20;
            
            spring.animationDidReachToValueBlock = { pop_animation in
                
                let spring = POPSpringAnimation(propertyNamed: kPOPLayerScaleXY)
                spring?.fromValue = NSValue(cgSize:animationSize);
                spring?.toValue = NSValue (cgSize:initialSize);
                spring?.springBounciness = 20;
                spring?.springSpeed = 20;
                
                view.layer.pop_add(spring, forKey: "bounceBack")
            }
            
            view.layer.pop_add(spring, forKey: "bounce")
        }
    }
}
