//
//  VIAARAvailableRewardViewModel.swift
//  VitalityActive
//
//  Created by Joshua Ryan on 6/28/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

import VitalityKit
import VIAUtilities
import RealmSwift

open class VIAARAvailableRewardViewModel {
    public var rewardId: Int?
    public var effectiveFrom: Date?
    public let arRealm = DataProvider.newARRealm()
    
    public init() {}
    
    public var availableRewards: [VIAARRewardViewModel] {
        var models = [VIAARRewardViewModel]()
        var rewardSelections: List<ARRewardsSelection>? = nil
        guard let id = rewardId else {
            return []
        }
        
        if let selections = arRealm.unclaimedRewards(with: id).first?.rewardSelections {
            rewardSelections = selections
        } else if let selections = arRealm.rewardVouchers(with: id).first?.rewardSelections {
            rewardSelections = selections
        }
        
        let sort = rewardSelections?.sorted(byKeyPath: "sortOrder", ascending: true)
        guard let finalSelections = sort else {
            return []
        }
        
        let options = Array(finalSelections)
        for option in options {
            let value = VIAARRewardHelper.getValueForReward(option)
            
            models.append(VIAARRewardViewModel(id: option.id,
                                               unclaimedRewardId: option.unclaimedRewardId,
                                               subTitle: value,
                                               title: option.rewardName,
                                               image: VIAARRewardHelper.partnerImage(for: option.rewardId),
                                               voucherId: option.rewardId.rawValue,
                                               voucherCodes: nil,
                                               partnerSysRewardId: nil,
                                               instructions: VIAARRewardHelper.partnerRewardInstructions(for: .AvailableToRedeem, rewardValue: value, effectiveFromDate: nil),
                                               expiration: nil,
                                               effectiveFromDate: effectiveFrom,
                                               effectiveToDate: nil,
                                               accepted: nil,
                                               bespokeRewardBoardId: VIAARRewardHelper.bespokeViewController(for: option.rewardId),
                                               linkId: option.rewardValueLinkId,
                                               rewardReference: option.rewardId,
                                               rewardStatus: nil,
                                               rewardkey: option.rewardKey,
                                               awardedRewardStatusEffectiveOn: nil))
        }
        return models
    }
    
    public func toList(in results: Results<ARRewardsSelection>, with id: Int) -> List<ARRewardsSelection>{
        let src = Array(results)
        let dest = List<ARRewardsSelection>()
        
        for item in src {
            dest.append(item)
        }
        
        return dest
    }
    
    open func availableSwaps(for currentModel: VIAARRewardViewModel) -> [VIAARRewardViewModel] {
        var models = [VIAARRewardViewModel]()
        var rewardSelections: List<ARRewardsSelection>? = nil
        
        if let selections = arRealm.availableSwapRewards().first?.rewardSelections.filter("rewardValueLinkId < %@", currentModel.linkId ?? 0) {
            
            let sortSelections = selections.sorted(byKeyPath: "sortOrder", ascending: true)
            rewardSelections = self.toList(in: sortSelections, with: currentModel.rewardkey ?? 0)
        }

        guard let finalSelections = rewardSelections else {
            return []
        }
        
        let options = Array(finalSelections)
        for option in options {
            let value = VIAARRewardHelper.getValueForReward(option)
            
            models.append(VIAARRewardViewModel(id: option.id,
                                               unclaimedRewardId: option.unclaimedRewardId,
                                               subTitle: value,
                                               title: option.rewardName,
                                               image: VIAARRewardHelper.partnerImage(for: option.rewardId),
                                               voucherId: option.rewardId.rawValue,
                                               voucherCodes: nil,
                                               partnerSysRewardId: nil,
                                               instructions: VIAARRewardHelper.partnerRewardInstructions(for: .AvailableToRedeem, rewardValue: value, effectiveFromDate: nil),
                                               expiration: nil,
                                               effectiveFromDate: effectiveFrom,
                                               effectiveToDate: nil,
                                               accepted: nil,
                                               bespokeRewardBoardId: VIAARRewardHelper.bespokeViewController(for: option.rewardId),
                                               linkId: option.rewardValueLinkId,
                                               rewardReference: option.rewardId,
                                               rewardStatus: nil,
                                               rewardkey: option.rewardKey,
                                               awardedRewardStatusEffectiveOn: nil))
        }
        
        return models
    }
}
