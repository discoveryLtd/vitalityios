import Foundation
import VIAUIKit
import SnapKit
import VitalityKit
import VIAUtilities
import VIACommon

class ParticipatingPartnersTableViewController: VIACoordinatedTableViewController {
    
    var partnersViewModel = ParticipatingPartnersViewModel(withPartnerType: .RewardPartners)
    var partnersData: [ParticipatingPartnersViewModel.PartnerData]?
    
    private var viewModel: ActiveRewardsRewardsLandingViewModel {
        get {
            return flowController.landingModel
        }
    }
    
    private var flowController: ARLandingNavDelegate = ARRewardsFlowCoordinator()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadPartners()
        setupTableView()
        title = CommonStrings.Ar.LearnMore.ParticipatingPartnersTitle696
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.hideBackButtonTitle()
    }
    
    func loadPartners() {
        
        showHUDOnView(view: self.view)
        
        partnersViewModel.requestPartnersByCategory(completion: { [weak self] (error) in
            self?.hideHUDFromView(view: self?.view)
            guard error == nil else {
                
                self?.handleError(error)
                return
            }
            self?.partnersViewModel.configurePartnerData()
            if let tempData = self?.partnersViewModel.partnerGroupsData[0].partners {
                self?.partnersData = tempData
            }
            self?.tableView.reloadData()
        })
    }
    
    func handleError(_ error: Error?) {
        let statusView = handleBackendErrorWithStatusView((error as? BackendError) ?? .other, tryAgainAction: { [weak self] in
            self?.loadPartners()
        })
        configureStatusView(statusView)
    }
    
    func setupTableView() {
        self.tableView.register(VIAARParticipatingPartnerCell.nib(), forCellReuseIdentifier: VIAARParticipatingPartnerCell.defaultReuseIdentifier)
        self.tableView.estimatedRowHeight = 75
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let partners = self.partnersData {
            return partners.count
        } else {
            return self.viewModel.participatingPartners.count
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        return self.configureParticipatingPartnersCell(indexPath)
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if (self.partnersData?[indexPath.row]) != nil {
            
            let shouldUseEligibility = VIAApplicableFeatures.default.shouldUseGetEligibility!
            
            if shouldUseEligibility {
                performSegue(withIdentifier: "showARParticipatingPartner", sender: shouldUseEligibility)
            } else {
                performSegue(withIdentifier: "showARPartnerDetail", sender: shouldUseEligibility)
            }
        }
    }
    
    func configureParticipatingPartnersCell(_ indexPath: IndexPath) -> UITableViewCell {
        guard let cell = self.tableView.dequeueReusableCell(withIdentifier: VIAARParticipatingPartnerCell.defaultReuseIdentifier, for: indexPath) as? VIAARParticipatingPartnerCell, let partner = partnersData?[indexPath.row] else { return tableView.defaultTableViewCell() }
        
        cell.partnerName = partner.name
        cell.descriptionText = partner.description
        if (partner.name.lowercased().range(of:"starbucks") != nil) {
            cell.descriptionText = CommonStrings.Ar.Partners.StarbucksVoucherValue705
        }
        
        // TODO: Remove this once server can return IGI partner data.
        if partner.name.lowercased().range(of:"easytickets") != nil {
            cell.cellImage = VIAActiveRewardsAsset.ActiveRewards.Rewards.IGIPartners.easyticketsSmall.image
        } else if partner.name.lowercased().range(of:"foodpanda") != nil {
            cell.cellImage = VIAActiveRewardsAsset.ActiveRewards.Rewards.IGIPartners.foodpandaSmall.image
        }
        else {
            DispatchQueue.global(qos: .background).async { [weak self] in
                self?.partnersViewModel.partnerLogo(partner: partner) { logo, partner in
                    DispatchQueue.main.async {
                        if cell.partnerName == partner.name {
                            cell.cellImage = logo
                        }
                    }
                }
            }
        }
        cell.isUserInteractionEnabled = true
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        if let identifier = segue.identifier, identifier == "showARPartnerDetail" || identifier == "showARParticipatingPartner" {
            
            let sender = sender as! Bool
            
            if sender {
                
                guard let indexPath = tableView.indexPathForSelectedRow, let partner = self.partnersData?[indexPath.row], let detailVC = segue.destination as? ParticipatingPartnerDetailsViewController else { return }
                detailVC.viewModel = ParticipatingPartnersDetailsViewModel(for: partner)
            } else {
                
                guard let indexPath = tableView.indexPathForSelectedRow, let partner = self.partnersData?[indexPath.row], let detailVC = segue.destination as? VIAARPartnerDetailViewController else { return }
                detailVC.partnerViewModel = partnersViewModel
                detailVC.partner = partner
            }
        }
    }
}
