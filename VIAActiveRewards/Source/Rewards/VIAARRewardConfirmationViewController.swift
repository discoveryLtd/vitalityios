//
//  VIAARRewardConfirmationViewController.swift
//  VitalityActive
//
//  Created by Joshua Ryan on 6/12/17.
//  Copyright © 2017 Glucode. All rights reserved.
//

import Foundation
import VIAUIKit
import VitalityKit
import VitalityKit
import VIAUtilities
import VIACommon


protocol ARRewardConfirmationDelegate: ARRewardFlowDelegate {
    
    func showSwapReward()
    func showTermsAndConditions(for newReward: VIAARRewardViewModel)
    func skipTermsAndConditions(for newReward: Int, with consumer: UIViewController?)
    func declineReward()
    
}

class VIAARRewardConfirmationViewController: VIACoordinatedViewController, VIAARewardConfirmationDelegate, ImproveYourHealthTintable {
    
    // MARK: Public
    var viewModel: VIAARRewardViewModel?
    weak var delegate: ARRewardConfirmationDelegate?
    
    // MARK: Private
    private var leftButton = UIBarButtonItem()
    private var congratulationsView: VIAARRewardCongratulations?
    
    // MARK: Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureCongratulationsView()
        addBarButtonItems()
    }
    
    func reloadChosenRewardContent(with newViewModel: VIAARRewardViewModel) {
        
        viewModel = newViewModel
        
        self.configureCongratulationsView()
    }
    
    func configureCongratulationsView() {
        if let viewModel = viewModel {
            guard let congratulationsView = VIAARRewardCongratulations.viewFromNib(owner: self) else {
                return
            }
            
            let tenantID = AppSettings.getAppTenant()
            congratulationsView.rewardAchievedDateIsHidden = tenantID != .SLI
            
            if let achievedDate = viewModel.effectiveFromDate {
                let dateFormat = Localization.dayDateLongMonthyearFormatter.string(from: achievedDate)
                congratulationsView.rewardAchievedDate = CommonStrings.Ar.VoucherTargetAchieved2148(dateFormat)
            }

            congratulationsView.frame = .zero
            congratulationsView.image = viewModel.image
            congratulationsView.header = CommonStrings.Ar.Rewards.ConfirmRewardTitle690
            congratulationsView.rewardHeader = viewModel.title
            congratulationsView.rewardContent = viewModel.subTitle
            congratulationsView.delegate = self
            congratulationsView.confirmButton.setTitle(CommonStrings.Ar.Rewards.ConfirmRewardButtonConfirm725, for: .normal)
            congratulationsView.swapButton.setTitle(CommonStrings.Ar.Rewards.ConfirmRewardSwapButton675, for: .normal)
            congratulationsView.rewardDisclaimer = CommonStrings.Ar.Partners.StarbucksRewardFootnote1062
            congratulationsView.layoutMargins = UIEdgeInsets(top: 0, left: 23.0, bottom: 0, right: 23.0)
            self.view.addSubview(congratulationsView)
            congratulationsView.snp.makeConstraints({ (make) in
                make.top.left.bottom.right.equalToSuperview()
            })
            
            if viewModel.rewardReference == .MachiLawson {
                congratulationsView.swapButton.isHidden = true
            }
            
            self.congratulationsView = congratulationsView
        }
    }
    
    // MARK: Action buttons
    
    func addBarButtonItems() {
        let cancel = UIBarButtonItem(title: CommonStrings.CancelButtonTitle24, style: .plain, target: self, action: #selector(cancelButtonTapped(_:)))
        self.navigationItem.leftBarButtonItem = cancel
    }
    
    // MARK: - VIAARewardConfirmationDelegate
    
    func swapReward() {
        delegate?.showSwapReward()
    }
    
    func confirmReward() {
        guard let model = viewModel else {
            return
        }
        
        showHUDOnWindow()
        VIAARRewardHelper.selectReward(with: model, with: ExchangeTypeRef.Selected) { [weak self] error, newRewardId, voucherNumber  in
            
            self?.hideHUDFromWindow()
            
            guard error == nil else {
                self?.handleBackendErrorWithAlert((error as? BackendError) ?? BackendError.other, tryAgainAction: { [weak self] in
                    self?.confirmReward()
                })
                return
            }
            
            if let newRewardId = newRewardId {
                
                if let shouldSkipTC = VIAApplicableFeatures.default.showSwapRewardsOption, shouldSkipTC {
                    self?.delegate?.skipTermsAndConditions(for: newRewardId, with: self)
                } else {
                    if self?.shouldShowTAndC() ?? true {
                        self?.delegate?.showTermsAndConditions(for: (self?.viewModel)!)
                    } else {
                        self?.delegate?.skipTermsAndConditions(for: newRewardId, with: self)
                    }
                }
            }
        }
    }
    
    func shouldShowTAndC() -> Bool {
        let coreRealm = DataProvider.newRealm()
        _ = coreRealm.rewardFeatureActive(with:(viewModel?.linkId)!)
        return false
    }
    
    func cancelButtonTapped (_ sender: UIBarButtonItem) {
        delegate?.declineReward()
    }
}
