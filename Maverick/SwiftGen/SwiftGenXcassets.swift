// Generated using SwiftGen, by O.Halligon — https://github.com/SwiftGen/SwiftGen

#if os(OSX)
import AppKit.NSImage
public typealias MaverickColor = NSColor
public typealias MaverickImage = NSImage
#elseif os(iOS) || os(tvOS) || os(watchOS)
import UIKit.UIImage
public typealias MaverickColor = UIColor
public typealias MaverickImage = UIImage
#endif

// swiftlint:disable file_length

public typealias MaverickAssetType = MaverickImageAsset

public struct MaverickImageAsset: Equatable {
	fileprivate var name: String

	public var templateImage: MaverickImage {
		let bundle = Bundle(for: BundleToken.self)
		#if os(iOS) || os(tvOS)
		let image = MaverickImage(named: name, in: bundle, compatibleWith: nil)?.withRenderingMode(.alwaysTemplate)
		#elseif os(OSX)
		let image = bundle.image(forResource: name).withRenderingMode(.alwaysTemplate)
		#elseif os(watchOS)
		let image = MaverickImage(named: name).withRenderingMode(.alwaysTemplate)
		#endif
		guard let result = image else { fatalError("Unable to load template image named \(name).") }
		return result
	}

	public var image: MaverickImage {
		let bundle = Bundle(for: BundleToken.self)
		#if os(iOS) || os(tvOS)
		let image = MaverickImage(named: name, in: bundle, compatibleWith: nil)
		#elseif os(OSX)
		let image = bundle.image(forResource: name)
		#elseif os(watchOS)
		let image = MaverickImage(named: name)
		#endif
		guard let result = image else { fatalError("Unable to load image named \(name).") }
		return result
	}

	public static func ==(lhs: MaverickImageAsset, rhs: MaverickImageAsset) -> Bool {
        return lhs.image == rhs.image
    }
}

public struct MaverickColorAsset {
fileprivate var name: String

#if swift(>=3.2)
@available(iOS 11.0, tvOS 11.0, watchOS 4.0, OSX 10.13, *)
var color: MaverickColor {
return MaverickColor(asset: self)
}
#endif
}

// swiftlint:disable identifier_name line_length nesting type_body_length type_name
public enum MaverickAsset {
  public static let vitalityLogo = MaverickImageAsset(name: "vitalityLogo")
  public enum TabBarIcons {
    public static let tabBarHealthInactive = MaverickImageAsset(name: "tabBarHealthInactive")
    public static let tabBarProfileActive = MaverickImageAsset(name: "tabBarProfileActive")
    public static let tabBarPointsInactive = MaverickImageAsset(name: "tabBarPointsInactive")
    public static let tabBarHomeInactive = MaverickImageAsset(name: "tabBarHomeInactive")
    public static let mybenefits = MaverickImageAsset(name: "mybenefits")
    public static let tabBarHelpActive = MaverickImageAsset(name: "tabBarHelpActive")
    public static let tabBarProfileInactive = MaverickImageAsset(name: "tabBarProfileInactive")
    public static let tabBarPointsActive = MaverickImageAsset(name: "tabBarPointsActive")
    public static let tabBarHomeActive = MaverickImageAsset(name: "tabBarHomeActive")
    public static let mybenefitsInactive = MaverickImageAsset(name: "mybenefitsInactive")
    public static let tabBarHealthActive = MaverickImageAsset(name: "tabBarHealthActive")
    public static let tabBarHelpInactive = MaverickImageAsset(name: "tabBarHelpInactive")
  }
  public static let vitalityOneLogo = MaverickImageAsset(name: "vitalityOneLogo")

  // swiftlint:disable trailing_comma
  public static let allColors: [MaverickColorAsset] = [
  ]
  public static let allImages: [MaverickImageAsset] = [
    vitalityLogo,
    TabBarIcons.tabBarHealthInactive,
    TabBarIcons.tabBarProfileActive,
    TabBarIcons.tabBarPointsInactive,
    TabBarIcons.tabBarHomeInactive,
    TabBarIcons.mybenefits,
    TabBarIcons.tabBarHelpActive,
    TabBarIcons.tabBarProfileInactive,
    TabBarIcons.tabBarPointsActive,
    TabBarIcons.tabBarHomeActive,
    TabBarIcons.mybenefitsInactive,
    TabBarIcons.tabBarHealthActive,
    TabBarIcons.tabBarHelpInactive,
    vitalityOneLogo,
  ]
  // swiftlint:enable trailing_comma
  @available(*, deprecated, renamed: "allImages")
  public static let allValues: [MaverickAssetType] = allImages
}
// swiftlint:enable identifier_name line_length nesting type_body_length type_name

extension MaverickImage {
@available(iOS 1.0, tvOS 1.0, watchOS 1.0, *)
@available(OSX, deprecated,
message: "This initializer is unsafe on macOS, please use the MaverickImageAsset.image property")
convenience init!(asset: MaverickAssetType) {
#if os(iOS) || os(tvOS)
let bundle = Bundle(for: BundleToken.self)
self.init(named: asset.name, in: bundle, compatibleWith: nil)
#elseif os(OSX) || os(watchOS)
self.init(named: asset.name)
#endif
}
}

extension MaverickColor {
#if swift(>=3.2)
@available(iOS 11.0, tvOS 11.0, watchOS 4.0, OSX 10.13, *)
convenience init!(asset: MaverickColorAsset) {
let bundle = Bundle(for: BundleToken.self)
#if os(iOS) || os(tvOS)
self.init(named: asset.name, in: bundle, compatibleWith: nil)
#elseif os(OSX)
self.init(named: asset.name, bundle: bundle)
#elseif os(watchOS)
self.init(named: asset.name)
#endif
}
#endif
}

private final class BundleToken {}
