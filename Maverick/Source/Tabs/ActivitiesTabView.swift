//
//  ActivitiesTabView.swift
//  Maverick
//
//  Created by ted.philip.l.lat on 02/01/2019.
//  Copyright © 2019 Glucode. All rights reserved.
//

import UIKit
import IGListKit

class ActivitiesTabView: UICollectionView, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    // MARK: Properties
    let activitiesCellId = "activitiesCellId"
    let headerCellId = "headerCellId"
    let cellTitles = [""]
    
    var maverickHomeScreenController: MaverickHomeScreenViewController?
    
    // MARK: Delegates
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch indexPath.item {
        case 0:
            let storyboard = UIStoryboard(name: "Promotions", bundle: nil)
            let viewController = storyboard.instantiateInitialViewController()
            maverickHomeScreenController?.present(viewController!, animated: true, completion: nil)
        case 1:
            maverickHomeScreenController?.performSegue(withIdentifier: "showGetActiveScreen", sender: self)
        case 3:
            maverickHomeScreenController?.performSegue(withIdentifier: "showGetFluShot", sender: self)
        default:
            return
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let arrayTitles = ["Get active 1 more day this week", "Eat fruits 2 more days this week", "Get a flu shot", "Take health review"]
        if indexPath.item >= 1 {
            let activitiesCell = self.dequeueReusableCell(withReuseIdentifier: activitiesCellId, for: indexPath) as! ActivitiesCell
            activitiesCell.header.text = arrayTitles[indexPath.item - 1]
            
            return activitiesCell
        } else {
            let headerCell = self.dequeueReusableCell(withReuseIdentifier: headerCellId, for: indexPath) as! HeaderCell
            
            return headerCell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let cellSize: CGSize = {
            if indexPath.item == 0 {
                return CGSize(width: self.frame.width, height: 250)
            } else {
                return CGSize(width: self.frame.width, height: 120)
            }
        }()
        
        return cellSize
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    override init(frame: CGRect, collectionViewLayout layout: UICollectionViewLayout) {
        let layout = UICollectionViewFlowLayout()
        layout.minimumInteritemSpacing = 0;
        layout.minimumLineSpacing = 0;
        super.init(frame: frame, collectionViewLayout: layout)
        
        self.backgroundColor = UIColor.lightGray.withAlphaComponent(0.1)
        self.delegate = self
        self.dataSource = self
        
        self.register(ActivitiesCell.self, forCellWithReuseIdentifier: activitiesCellId)
        self.register(HeaderCell.self, forCellWithReuseIdentifier: headerCellId)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
