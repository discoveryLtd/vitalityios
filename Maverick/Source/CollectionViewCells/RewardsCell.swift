//
//  RewardsCell.swift
//  Maverick
//
//  Created by ted.philip.l.lat on 03/01/2019.
//  Copyright © 2019 Glucode. All rights reserved.
//

import UIKit

class RewardsCell: UICollectionViewCell {
    
    // MARK: Properties
    let containerView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        view.layer.borderWidth = 0
        view.layer.shadowOffset = CGSize(width: 0, height: 3)
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOpacity = 0.1
        view.layer.cornerRadius = 10
        return view
    }()
    
    let header: UILabel = {
        let label = UILabel()
        label.text = "Earn spins to get gift cards"
        label.font = UIFont.boldSystemFont(ofSize: 18)
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 0
        label.textColor = UIColor.black
        
        return label
    }()
    
    let stackView: UIStackView = {
        let view = UIStackView(frame: .zero)
        view.axis = .vertical
        view.distribution = .equalSpacing
        view.spacing = 2
        return view
    }()
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.text = "250"
        label.textColor = UIColor.black
        label.font = UIFont.boldSystemFont(ofSize: 40)
        return label
    }()
    
    let subTitleLabel: UILabel = {
        let label = UILabel()
        label.text = "more points until your $5 gift card"
        label.textColor = UIColor.gray
        label.font = UIFont.systemFont(ofSize: 12)
        return label
    }()
    
    let startPointLabel: UILabel = {
        let label = UILabel()
        label.text = "0 \npoints"
        label.font = UIFont.boldSystemFont(ofSize: 12)
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 0
        label.textColor = UIColor.black
        return label
    }()
    
    let endPointLabel: UILabel = {
        let label = UILabel()
//        let attributedString = "<b>500</b> \npoints"
        label.text = "500 \npoints"
        label.font = UIFont.boldSystemFont(ofSize: 12)
        label.lineBreakMode = .byWordWrapping
        label.textAlignment = .right
        label.numberOfLines = 0
        label.textColor = UIColor.black
        return label
    }()
    
    let progressBar: UIProgressView = {
        let progressView = UIProgressView(progressViewStyle: .bar)
        progressView.setProgress(0.5, animated: true)
        let gradientImage = UIImage.gradientImage(with: progressView.frame,
                                                  colors: [UIColor.lightGray.cgColor, UIColor.black.cgColor],
                                                  locations: nil)
        progressView.trackTintColor = UIColor.lightGray
        progressView.progressImage = gradientImage
        progressView.clipsToBounds = true
        progressView.layer.cornerRadius = 6
        progressView.layer.sublayers![1].cornerRadius = 6
        progressView.subviews[1].clipsToBounds = true
        return progressView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupContainerView()
        setupContent()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: Config
    func setupContainerView() {
        self.contentView.addSubview(containerView)
        containerView.snp.makeConstraints { (make) in
            make.bottom.top.equalToSuperview()
            make.left.right.equalToSuperview().inset(30)
        }
    }
    
    func setupContent() {
        containerView.addSubview(header)
        header.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            make.top.equalToSuperview().inset(30)
        }
        
        containerView.addSubview(stackView)
        stackView.snp.makeConstraints { (make) in
            make.top.equalTo(header.snp.bottom).inset(-30)
            make.left.right.equalToSuperview().inset(20)
        }
        
        containerView.addSubview(progressBar)
        progressBar.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            make.left.right.equalToSuperview().inset(20)
            make.top.equalTo(stackView.snp.bottom).inset(-15)
            make.height.equalTo(12)
        }
        
        containerView.addSubview(startPointLabel)
        startPointLabel.snp.makeConstraints { (make) in
            make.top.equalTo(progressBar.snp.bottom).inset(-10)
            make.left.equalToSuperview().inset(20)
        }
        
        containerView.addSubview(endPointLabel)
        endPointLabel.snp.makeConstraints { (make) in
            make.top.equalTo(progressBar.snp.bottom).inset(-10)
            make.right.equalToSuperview().inset(20)
        }
        
        stackView.addArrangedSubview(titleLabel)
        stackView.addArrangedSubview(subTitleLabel)
        
    }
}

fileprivate extension UIImage {
    static func gradientImage(with bounds: CGRect,
                              colors: [CGColor],
                              locations: [NSNumber]?) -> UIImage? {
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = bounds
        gradientLayer.colors = colors
        // This makes it horizontal
        gradientLayer.startPoint = CGPoint(x: 0.0,
                                           y: 0.5)
        gradientLayer.endPoint = CGPoint(x: 1.0,
                                         y: 0.5)
        
        UIGraphicsBeginImageContext(gradientLayer.bounds.size)
        gradientLayer.render(in: UIGraphicsGetCurrentContext()!)
        guard let image = UIGraphicsGetImageFromCurrentImageContext() else { return nil }
        UIGraphicsEndImageContext()
        return image
    }
}

extension String{
    func convertHtml() -> NSAttributedString{
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do{
            return try NSAttributedString(data: data, options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: String.Encoding.utf8.rawValue], documentAttributes: nil)
        }catch{
            return NSAttributedString()
        }
    }
}

