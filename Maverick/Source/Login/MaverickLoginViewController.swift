//
//  MaverickLoginViewController.swift
//  Maverick
//
//  Created by Genie Mae Lorena Edera on 1/29/19.
//  Copyright © 2019 Glucode. All rights reserved.
//


import VIACommon
import UIKit
import VIAUIKit
import VitalityKit
import TTTAttributedLabel
import LocalAuthentication
import VIAHealthKit
import HealthKit
import VIAAssessments
import VIAWellnessDevices

let login = "Login"
let maverickLoginRegistration = "MaverickLoginViewController"

protocol MLoginViewController {
    var viewModel: MaverickLoginViewModel { get set }
    var loginHasFailedAtLeastOnce: Bool { get set }
}

public class MaverickLoginViewController: VIAViewController, MLoginViewController, UITableViewDelegate, UITableViewDataSource, PrimaryColorTintable, VIAAppleHealthHandler {
    
    // MARK: Properties
    
    @IBOutlet var logoImageView: UIImageView!
    @IBOutlet var tableView: UITableView!
    var defaultCellSeparatorColor: UIColor?
    var viewModel: MaverickLoginViewModel = MaverickLoginViewModel()
    var loginHasFailedAtLeastOnce = false
    var loginButton: VIAButton = VIAButton(title: LoginStrings.Login.LoginButtonTitle20)
    var loggingInSpinner: UIActivityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
    var registerButton: VIAButton = VIAButton(title: "Create an account")
    var loginFaceIDButton: VIAButton = VIAButton(title: "Login with Face ID")
    
    let colors = Colors()
    
    // MARK: View lifecycle
    
    public override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    public override func viewDidLoad() {
        debugPrint("::mav -on mav login")
        super.viewDidLoad()
        
        view.backgroundColor = UIColor.clear
        let backgroundLayer = colors.gl
        backgroundLayer.frame = view.frame
        view.layer.insertSublayer(backgroundLayer, at: 0)
        
        self.logoImageView.image = VIAIconography.default.loginLogo()
        self.logoImageView.backgroundColor = UIColor.clear
        self.logoImageView.isOpaque = false
        self.logoImageView.contentMode = UIViewContentMode.scaleAspectFill

        self.configureTableView()
        self.tableView.reloadData()
    }
    
    public static func showLogin(upgradeUrl: String? = nil) {
        var storyboard: UIStoryboard?
        storyboard = UIStoryboard(name: "MaverickLoginViewController", bundle: nil)
        let viewController = storyboard?.instantiateViewController(withIdentifier: "MaverickLoginViewController") as! MaverickLoginViewController
        UIApplication.shared.keyWindow?.rootViewController = viewController
    }
    
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        UIApplication.shared.statusBarStyle = .default
        self.configureKeyboardAutoToolbar(VIAApplicableFeatures.default.enableKeyboardAutoToolbar ?? false)
    }
    
    public override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.disableKeyboardAutoToolbar()
    }
    
    public override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    func configureTableView() {
        self.defaultCellSeparatorColor = self.tableView.separatorColor
        
        let deviceType = UIDevice.current.getDeviceType()
        switch deviceType {
        case .AppleIphone6S, .AppleIphone7:
            if #available(iOS 8.0, *) {
                if((UIScreen.main.bounds.size.height == 667.0 || UIScreen.main.bounds.size.height == 568.0) && UIScreen.main.nativeScale < UIScreen.main.scale) {
                    //enablePageScrollingOnLoginTableView()
                }
            }
            break
        case .AppleIphone5C, .AppleIphone5, .AppleIphone5S, .AppleIphoneSE, .Simulator:
            //enablePageScrollingOnLoginTableView()
            break
        default:
            self.tableView.isScrollEnabled = false
        }
        self.tableView.separatorInset = UIEdgeInsets(top: 0, left: 38, bottom: 0, right: 0)
        self.tableView.separatorColor = .clear
        self.tableView.backgroundColor = UIColor.clear
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 75
        self.tableView.register(MaverickTextFieldTableViewCell.nib(), forCellReuseIdentifier: MaverickTextFieldTableViewCell.defaultReuseIdentifier)
        self.tableView.register(VIATextFieldTableViewCell.nib(), forCellReuseIdentifier: VIATextFieldTableViewCell.defaultReuseIdentifier)
        self.tableView.register(VIAEmailTextFieldTableViewCell.nib(), forCellReuseIdentifier: VIAEmailTextFieldTableViewCell.defaultReuseIdentifier)
        self.configureTableViewFooterView()
    }
    
    func configureTableViewFooterView() {
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: self.tableView.frame.size.width, height: 0))
        footerView.backgroundColor = UIColor.clear

        self.loginFaceIDButton.isEnabled = true
        self.loginFaceIDButton.hidesBorder = true
        self.loginFaceIDButton.tintColor = UIColor.white
        self.loginFaceIDButton.highlightedTextColor = UIColor.white
        self.loginFaceIDButton.titleLabel?.font = UIFont.tableViewFooterLabelButton()
        footerView.addSubview(self.loginFaceIDButton)
        
        loginButton.isEnabled = false
        self.loginButton.alpha = 1.0
        self.loginButton.translatesAutoresizingMaskIntoConstraints = false
        self.loginButton.backgroundColor = UIColor.white
        self.loginButton.tintColor = UIColor.black
        self.loginButton.highlightedTextColor = UIColor.black
        self.loginButton.hidesBorder = true
        self.loginButton.layer.cornerRadius = 22
        self.loginButton.layer.borderWidth = 0
        
        loggingInSpinner.frame = CGRect(x: 70.0, y: 17.0, width: 10.0, height: 10.0)
        loggingInSpinner.startAnimating()
        loggingInSpinner.alpha = 0.0
        loginButton.addSubview(loggingInSpinner)
        
        self.loginButton.addTarget(self, action: #selector(performLogin(_:)), for: .touchUpInside)
        footerView.addSubview(self.loginButton)
        
        self.registerButton.hidesBorder = true
        self.registerButton.tintColor = UIColor.white
        self.registerButton.highlightedTextColor = UIColor.white
        self.registerButton.titleLabel?.font = UIFont.tableViewFooterLabelButton()
        footerView.addSubview(self.registerButton)
        
        let views = ["loginFaceID": self.loginFaceIDButton, "register": self.registerButton, "footer": footerView, "login": self.loginButton]
        footerView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[loginFaceID(22)]-100-[login(44)]-20-[register(22)]|", options: [], metrics: nil, views: views))
        
        footerView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[loginFaceID]|", options: [], metrics: nil, views: views))
        footerView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[login]|", options: [], metrics: nil, views: views))
        footerView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-[register]-|", options: [], metrics: nil, views: views))
        
        var frame = footerView.frame
        let newHeight = footerView.systemLayoutSizeFitting(UILayoutFittingCompressedSize).height
        frame.size.height = newHeight
        footerView.frame = CGRect(x: 0, y: frame.size.height , width: frame.size.width, height: frame.size.height)
        self.tableView.tableFooterView = footerView
    }
    
    // MARK: UITableView datasource
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 1 {
            let cell = VIAApplicableFeatures.default.getTextFieldViewCell(tableView: tableView, indexPath: indexPath) as! MaverickTextFieldTableViewCell
            
            cell.setHeadingLabelText(text: "")
            cell.setTextFieldPlaceholder(placeholder: "Username")
            cell.selectionStyle = .none
            cell.extendsEdgesToContentView = true
            cell.setTextFieldText(text: self.viewModel.email)
            
            cell.setTextFieldKeyboardType(type: .emailAddress)
            cell.textFieldTextDidChange = { textField in
                textField.clearButtonMode = .whileEditing
                self.viewModel.email = textField.text
                self.toggleLoginButtonIsEnabled()
                if(self.viewModel.emailValid) {
                    UIView.performWithoutAnimation {
                        tableView.beginUpdates()
                        cell.setErrorMessage(message: nil)
                        tableView.endUpdates()
                    }
                }
                if(cell.isEmpty()){
                    cell.setHeadingLabelText(text: "")
                }else{
                    cell.setHeadingLabelText(text: "Username")
                }
            }
            
            cell.backgroundColor = UIColor.clear
            cell.isOpaque = false
            return cell
            
        } else if indexPath.row == 2 {
            let cell = VIAApplicableFeatures.default.getTextFieldViewCell(tableView: tableView, indexPath: indexPath) as! MaverickTextFieldTableViewCell
            cell.backgroundColor = UIColor.clear
            cell.isOpaque = false
            cell.textField.backgroundColor = UIColor.clear
            cell.textField.isOpaque = false
            cell.setTextFieldPlaceholder(placeholder: LoginStrings.PasswordFieldPlaceholder19)
            cell.setHeadingLabelText(text: "")
            
            cell.selectionStyle = .none
            cell.setTextFieldSecureTextEntry(secureTextEntry: true)
            cell.showHelpButton()
            cell.extendsEdgesToContentView = true
            cell.setTextFieldText(text: self.viewModel.password)
            cell.setTextFieldKeyboardType(type: .default)
            cell.textFieldTextDidChange = { textField in
                textField.clearButtonMode = .whileEditing
                self.viewModel.password = textField.text
                self.toggleLoginButtonIsEnabled()
                if(cell.isEmpty()){
                    cell.setHeadingLabelText(text: "")
                }else{
                    cell.setHeadingLabelText(text: LoginStrings.PasswordFieldPlaceholder19)
                }
            }
            return cell
        }
        // blank cell
        let blankCell = UITableViewCell(style: .default, reuseIdentifier: "cell")
        blankCell.selectionStyle = .none
        blankCell.backgroundColor = UIColor.clear
        blankCell.isOpaque = false
        return blankCell
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return UITableViewAutomaticDimension
        } else if indexPath.row == 3 {
            return 28
        }
        return UITableViewAutomaticDimension
    }
    
    // MARK: UITableView delegate
    
    public func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    public func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
    
    public func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//        if indexPath.section == 0 && indexPath.row == 0 {
//            tableView.separatorColor = .yellow
//                //self.defaultCellSeparatorColor
//        } else {
//            tableView.separatorColor = .red //clear
//        }
    }
    
    // MARK: Actions
    
    func dismissKeyboard(_ gesture: UITapGestureRecognizer?) {
        self.tableView.endEditing(true)
    }
    
    func performLogin(_ sender: UIButton?) {
        let storyboard = UIStoryboard(name: "MaverickHome", bundle: Bundle.main)
        let viewController = storyboard.instantiateInitialViewController()
        UIApplication.shared.keyWindow?.rootViewController = viewController
        
        
//        self.loginButton.isEnabled = false
//        loggingInSpinner.color = UIColor.primaryColor()
//        loggingInSpinner.alpha = 1.0
//        self.loginButton.setTitle(LoginStrings.Login.LoggingInButtonTitle21, for: .normal)
//        
//        self.dismissKeyboard(nil)
        

        
//        self.viewModel.loginComplete = { [weak self] error in
//            self?.loginComplete(error)
//            self?.loginButton.isEnabled = true
//        }
//
//        self.viewModel.performLogin()
    }
    
    func loginComplete(_ error: Error?) {
        loggingInSpinner.alpha = 0.0
        resetLoginButton()
        
        guard error == nil else {
            switch error {
            case is LoginError:
                self.handleLoginError(error as! LoginError)
            case is RegistrationError:
                self.handleLoginError(error as! LoginError)
            case is BackendError:
                self.handleBackendErrorWithAlert(error as! BackendError, tryAgainAction: { [weak self] in
                    self?.performLogin(nil)
                })
            default:
                self.displayUnknownServiceErrorOccurredAlert()
            }
            self.hideHUDFromWindow()
            return
        }
        
    }
    
    func handleLoginError(_ error: LoginError) {
        self.displayLoginErrorMessage(error: error )
        resetLoginButton()
    }
    
    func resetLoginButton() {
        self.loginButton.setTitle(LoginStrings.Login.LoginButtonTitle20, for: .normal)
    }
    
    func displayLoginErrorMessage(error: LoginError) {
        
        var dialogTitle = ""
        var dialogMessage = ""
        
        if error == .accountLockedOut {
            dialogTitle = LoginStrings.Login.IncorrectEmailPasswordErrorTitle47
            dialogMessage = LoginStrings.Login.AccountLockedErrorAlertMessage738
        }
        else {
            dialogTitle = LoginStrings.Login.IncorrectEmailPasswordErrorTitle47
            dialogMessage = LoginStrings.Login.IncorrectEmailPasswordErrorMessage48
        }
        
        let controller = UIAlertController(title: dialogTitle,
                                           message: dialogMessage,
                                           preferredStyle: .alert)
        
        
        let ok = UIAlertAction(title: CommonStrings.OkButtonTitle40, style: .cancel) { action in
        }
        controller.addAction(ok)
        
        self.loginHasFailedAtLeastOnce = true
        self.present(controller, animated: true, completion: nil)
    }
    
    func toggleLoginButtonIsEnabled() {
        if self.viewModel.isFieldEmpty || !viewModel.emailValid {
            self.loginButton.isEnabled = false
            self.loginButton.alpha = 0.5
            
        } else {
            self.loginButton.isEnabled = true
            self.loginButton.alpha = 1.0
        }
    }
    
}

class Colors {
    let colorTop = UIColor(red: 237/255.0, green: 90/255.0, blue: 49/255.0, alpha: 1.0).cgColor
    let colorBottom = UIColor(red: 212/255.0, green: 29/255.0, blue: 124/255.0, alpha: 1.0).cgColor
    
    let gl: CAGradientLayer
    
    init() {
        gl = CAGradientLayer()
        gl.colors = [ colorTop, colorBottom]
        gl.locations = [ 0.0, 1.0]
    }
}

