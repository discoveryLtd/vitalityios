//
//  PromotionsViewController.swift
//  Maverick
//
//  Created by ted.philip.l.lat on 14/01/2019.
//  Copyright © 2019 Glucode. All rights reserved.
//

import UIKit

class PromotionsViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    // MARK: Properties
    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.minimumInteritemSpacing = 0;
        layout.minimumLineSpacing = 0;
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.backgroundColor = UIColor.clear
        collectionView.contentInsetAdjustmentBehavior = .never
        collectionView.delegate = self
        collectionView.dataSource = self
        return collectionView
    }()
    
    let cellId = "cellId"
    
    // MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.register(PromotionCell.self, forCellWithReuseIdentifier: cellId)
        
        setupCollectionView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    // MARK: CollectionView Delegates
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! PromotionCell
        cell.closeButton.addTarget(self, action: #selector(close), for: .touchUpInside)
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        return CGSize(width: view.frame.width, height: 1900)
    }
    
    // MARK: Config
    func setupCollectionView() {
        view.addSubview(collectionView)
        collectionView.snp.makeConstraints { (make) in
            make.top.left.right.bottom.equalToSuperview()
        }
    }
    
    //MARK: Functions
    func close(sender: UIButton!) {
        self.dismiss(animated: true, completion: nil)
    }

}

class PromotionCell: UICollectionViewCell {
    
    // MARK: Properties

    let containerView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        return view
    }()
    
    let headerView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.gray
        return view
    }()

    let stackView: UIStackView = {
        let view = UIStackView(frame: .zero)
        view.axis = .vertical
        view.distribution = .equalSpacing
        view.alignment = .leading
        view.spacing = 10
        return view
    }()
    
    let articleLabel: UILabel = {
        let label = UILabel()
        label.text = "Article"
        label.font = UIFont.boldSystemFont(ofSize: 16)
        label.textColor = UIColor.lightGray
        return label
    }()
    
    let articleTitleLabel: UILabel = {
        let label = UILabel()
        label.text = "5 ways to be prepared this cold and flu season"
        label.font = UIFont.boldSystemFont(ofSize: 27)
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 0
        return label
    }()
    
    let contentTextView: UILabel = {
        let label = UILabel()
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 0
        label.text = "Get Your 5 a Day. Fruits & Vegetables that is! It’s so important to give your body the vitamins ad nutrients it needs to keep your immunity going strong. Don’t wait until you or a family member is sick to start. Adding just a few more colorful fruits and vegetables to you meals and snacks now can make all the of the difference in a few months! Try adding a cup of your favorite berries to your yogurt for an easy and satisfying breakfast or adding a handful of spinach to your pasta for dinner! \n\nExercise. While the weather’s still pretty nice, get outdoors with the whole family! You don’t have to go on a long run or go to the gym to be active. Go on a nature walk or scavenger hunt with your family. You can set out to collect items from the woods like acorns and pine cones on a hike for decorating your home for the season. Go for a bike ride in the country and enjoy the Fall foliage, plant some tulip or daffodil buds in your garden for spring, or a play catch with your family dog. All of these things keep you active and it’s so good for your mind, body and spirit. \n\nHydrate. Soups are a tasty way to get extra vegetables and water into your diet while making you feel warm and cozy on chilly Fall evening. I don’t know about you, but I can easily add 2 soup meals to your menu plan this time of year and no one complains. Not even my little ones! Soup also helps curb your appetite by making you feel longer, in turn, preventing you from overeating. Soups also really help to keep your body hydrated too. In addition to soup, add a cup of tea in the afternoon and evening too. \n\nRest. Make sure everyone in your family is getting enough sleep every night. I know life gets busy, but your body needs it! Make it a point to get on a normal sleep schedule. If you are adding a little more exercise to your family’s daily routine, you may even have an easier time getting to sleep. Try adding a new warm, snuggly blanket to your bed and a few drops of calming lavender oil to your evening lotion or hand cream after a warm bath. These little things can help relax you pay off in the long run. \n\nBe Prepared. Colds & Flu, happen. Sometimes you can take really great care of yourself and your family and someone still ends up feeling a little under the weather. Being prepared and having a few quality “Get Well Items” ready to go in your home allows you to relax and rest instead of running to the store last minute. I know when children aren’t feeling well, the last thing I want to do when they are snuggled in their bed resting and I have a pot of handmade soup cooking on the stove is running out to the store. Take a few minutes now to prepare for that unexpected illness."
        return label
    }()

    let closeButton: UIButton = {
        let button = UIButton()
        button.setTitle("x", for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 18)
        button.backgroundColor = UIColor.white
        button.setTitleColor(UIColor.gray, for: .normal)
        button.layer.cornerRadius = 20
        return button
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupContainerView()
        setupSubviews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: Config
    func setupContainerView() {
        self.contentView.addSubview(containerView)
        containerView.snp.makeConstraints { (make) in
            make.top.bottom.left.right.equalToSuperview()
        }
    }
    
    func setupSubviews() {
        containerView.addSubview(headerView)
        headerView.snp.makeConstraints { (make) in
            make.left.right.top.equalToSuperview()
            make.height.equalTo(250)
        }

        containerView.addSubview(closeButton)
        closeButton.snp.makeConstraints { (make) in
            make.right.equalToSuperview().inset(20)
            make.top.equalToSuperview().inset(40)
            make.width.height.equalTo(40)
        }

        containerView.addSubview(stackView)
        stackView.snp.makeConstraints { (make) in
            make.right.left.equalToSuperview().inset(30)
            make.top.equalTo(headerView.snp.bottom).inset(-20)
        }
        
        stackView.addArrangedSubview(articleLabel)
        stackView.addArrangedSubview(articleTitleLabel)
        stackView.addArrangedSubview(contentTextView)
    }
    
}
