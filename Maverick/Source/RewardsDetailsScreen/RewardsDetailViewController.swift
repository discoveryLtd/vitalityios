//
//  RewardsDetailViewController.swift
//  Maverick
//
//  Created by ted.philip.l.lat on 14/01/2019.
//  Copyright © 2019 Glucode. All rights reserved.
//

import UIKit

class RewardsDetailViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    // MARK: Properties
    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.minimumInteritemSpacing = 0;
        layout.minimumLineSpacing = 0;
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.backgroundColor = UIColor.clear
        collectionView.contentInsetAdjustmentBehavior = .never
        collectionView.delegate = self
        collectionView.dataSource = self
        return collectionView
    }()
    
    let cellId = "cellId"
    let rewardsCellId = "rewardsCellId"
    let activitiesCellId = "activitiesCellId"
    
    override func viewDidLoad() {
        super.viewDidLoad()

        collectionView.register(FirstCell.self, forCellWithReuseIdentifier: cellId)
        collectionView.register(RewardsCell.self, forCellWithReuseIdentifier: rewardsCellId)
        collectionView.register(ActivitiesCell.self, forCellWithReuseIdentifier: activitiesCellId)
        
        setupCollectionView()
    }
    
    // MARK: CollectionView Delegates
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 6
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        switch indexPath.item {
        case 0:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! FirstCell
            
            return cell
        case 1:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: rewardsCellId, for: indexPath) as! RewardsCell
            cell.containerView.layer.borderWidth = 0
            cell.containerView.layer.shadowOffset = CGSize(width: 0, height: 0)
            cell.containerView.layer.shadowColor = nil
            cell.containerView.layer.shadowOpacity = 0
            cell.containerView.layer.cornerRadius = 0
            cell.header.text = "Today"
            
            cell.containerView.snp.remakeConstraints { (make) in
                make.bottom.top.equalToSuperview()
                make.left.right.equalToSuperview().inset(10)
            }
            
            cell.header.snp.remakeConstraints { (make) in
                make.left.equalToSuperview().inset(20)
                make.top.equalToSuperview().inset(20)
            }
            
            return cell
        default:
            
            let arrayTitles = ["Get active 1 more day this week", "Eat fruits 2 more days this week", "Get a flu shot", "Take health review"]
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: activitiesCellId, for: indexPath) as! ActivitiesCell
            cell.header.text = arrayTitles[indexPath.item - 2]
            
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        switch indexPath.item {
        case 0:
            return CGSize(width: view.frame.width, height: 450)
        case 1:
            return CGSize(width: view.frame.width, height: 230)
        default:
            return CGSize(width: view.frame.width, height: 120)
        }
    }
    
    // MARK: Config
    func setupCollectionView() {
        view.addSubview(collectionView)
        collectionView.snp.makeConstraints { (make) in
            make.top.left.right.equalToSuperview()
            make.bottom.equalTo(view.safeAreaLayoutGuide.snp.bottom)
        }
    }

}

class FirstCell: UICollectionViewCell {
    
    // MARK: Properties
    
    let containerView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        return view
    }()
    
    let headerView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.gray
        return view
    }()
    
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.text = "Earn spins to get gift cards"
        label.font = UIFont.boldSystemFont(ofSize: 30)
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 0
        return label
    }()
    
    let bodyLabel: UILabel = {
        let label = UILabel()
        label.text = "Complete select activities to earn spins on the wheel. Prize includes points and gift cards."
        label.font = UIFont.systemFont(ofSize: 17)
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 0
        return label
    }()
    
    let cellSeperator: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.lightGray.withAlphaComponent(0.1)
        return view
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupContainerView()
        setupSubviews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: Config
    func setupContainerView() {
        self.contentView.addSubview(containerView)
        containerView.snp.makeConstraints { (make) in
            make.top.bottom.left.right.equalToSuperview()
        }
    }
    
    func setupSubviews() {
        containerView.addSubview(headerView)
        headerView.snp.makeConstraints { (make) in
            make.left.right.top.equalToSuperview()
            make.height.equalTo(250)
        }
        
        containerView.addSubview(titleLabel)
        titleLabel.snp.makeConstraints { (make) in
            make.top.equalTo(headerView.snp.bottom).offset(20)
            make.left.equalToSuperview().offset(30)
            make.right.equalToSuperview().offset(-30)
        }
        
        containerView.addSubview(bodyLabel)
        bodyLabel.snp.makeConstraints { (make) in
            make.top.equalTo(titleLabel.snp.bottom).offset(20)
            make.left.equalToSuperview().offset(30)
            make.right.equalToSuperview().offset(-30)
        }
        
        containerView.addSubview(cellSeperator)
        cellSeperator.snp.makeConstraints { (make) in
            make.bottom.equalToSuperview()
            make.right.left.equalToSuperview().inset(30)
            make.height.equalTo(1)
        }
        
    }
    
}
