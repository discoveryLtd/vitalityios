//
//  AppDelegate+ApplicableFeatures+Login.swift
//  VitalityActive
//
//  Created by OJ Garde on 3/17/18.
//  Copyright © 2018 Glucode. All rights reserved.
//

import Foundation
import VitalityKit

extension AppDelegate{    
    
    public var enableUserToResendInsurerCode: Bool? {
        get{
            return false
        }
    }
    
    public func showLoginRestriction() -> Bool {
        return true
    }
    
    //TODO This should be aligned in the PhraseApp. All markets should use the same key.
    public func getEmailPlaceholder() -> (String){
        
        return LoginStrings.EmailFieldPlaceholder18
    }
    
    public var enableDelayInSplashScreenDisplay: Bool? {
        get{
            return false
        }
    }
    
    /**
     * Usage:
     * VIARegistrationViewController.swift
     **/
    public var showDateOfBirthField: Bool?{
        get{
            return true
        }
    }
    
    // TODO: Temporary condition to apply home card resizing on CA
    public var resizeHomeCard: Bool? {
        get{
            return true
        }
    }
}
