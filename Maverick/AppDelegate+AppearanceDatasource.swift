import VIACommon
import VIAUIKit
import VIACore
import UIKit
import VitalityKit

extension AppDelegate: AppearanceColorDataSource {
    class func vitalityOrangeColor() -> UIColor {
        return UIColor(red: 255.0 / 255.0, green: 110.0 / 255.0, blue: 0.0, alpha: 1.0)
    }

    // MARK: AppearanceDataSource

    public var primaryColor: UIColor {
        return AppDelegate.vitalityOrangeColor()
    }

    public var tabBarBackgroundColor: UIColor {
        return UIColor.primaryColor()
    }

    public var tabBarBarTintColor: UIColor {
        return UIColor.primaryColor()
    }

    public var tabBarTintColor: UIColor? {
        return UIColor.day()
    }

    public var unselectedItemTintColor: UIColor? {
        return UIColor(white: 1.0, alpha: 0.6)
    }
    
    public var getInsurerGlobalTint: UIColor? {
//        return VIAAppearance.default.primaryColorFromServer
        return AppDelegate.vitalityOrangeColor()
    }
    
    public func getSplashScreenGradientTop() -> UIColor{
//        return UIColor(hexString: AppConfigFeature.insurerGradientColor1Hex())
        return .day()
    }
    
    public func getSplashScreenGradientBottom() -> UIColor{
//        return UIColor(hexString: AppConfigFeature.insurerGradientColor2Hex())
        return .day()
    }
}

extension AppDelegate: AppearanceIconographyDataSource {
    
    public func homeLogo(for locale: Locale) -> UIImage {
        return MaverickAsset.vitalityLogo.image
    }

    public func loginLogo(for locale: Locale) -> UIImage {
        return MaverickAsset.vitalityLogo.image
        //return MaverickAsset.vitalityOneLogo.image //G: Show Sales Prototype Login
    }
    
    public func splashLogo(for locale: Locale = Locale.current) -> UIImage {
        return MaverickAsset.vitalityLogo.image
    }
}
