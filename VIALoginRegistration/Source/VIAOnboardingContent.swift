//
//  VIAOnboardingContent.swift
//  VitalityActive
//
//  Created by Wilmar van Heerden on 2016/09/21.
//  Copyright © 2016 Glucode. All rights reserved.
//

import Foundation
import VitalityKit

public extension VIAFirstTimeOnboardingViewController {
    class func skipButtonTitle() -> String {
        return CommonStrings.SkipButtonTitle11
    }

    class func heading1() -> String {
        return CommonStrings.Onboarding.KnowYourHealthTitle12
    }

    class func heading2() -> String {
        return CommonStrings.Onboarding.ImproveYourHealthTitle14
    }

    class func heading3() -> String {
        return CommonStrings.Onboarding.EnjoyWeeklyRewardsTitle16
    }

    class func description1() -> String {
        return CommonStrings.Onboarding.KnowYourHealthDescription13
    }

    class func description2() -> String {
        return CommonStrings.Onboarding.ImproveYourHealthDescription15
    }

    class func description3() -> String {
        return CommonStrings.Onboarding.EnjoyWeeklyRewardsDescription17
    }
}
