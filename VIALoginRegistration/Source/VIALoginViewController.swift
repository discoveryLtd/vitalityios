import VIACommon
import UIKit
import VIAUIKit
import VitalityKit
import TTTAttributedLabel
import LocalAuthentication
import VIAHealthKit
import HealthKit
import VIAAssessments
import VIAWellnessDevices

let loginRegistration = "LoginRegistration"
let login = "Login"

protocol LoginViewController {
    var viewModel: VIALoginViewModel { get set }
    var loginHasFailedAtLeastOnce: Bool { get set }
}

public class VIALoginViewController: VIAViewController, LoginViewController, UITableViewDelegate, UITableViewDataSource, PrimaryColorTintable, VIAAppleHealthHandler {
    
    // MARK: Properties
    
    @IBOutlet var logoImageView: UIImageView!
    @IBOutlet var tableView: UITableView!
    var defaultCellSeparatorColor: UIColor?
    var viewModel: VIALoginViewModel = VIALoginViewModel()
    var loginHasFailedAtLeastOnce = false
    var loginButton: VIAButton = VIAButton(title: CommonStrings.Login.LoginButtonTitle20)
    var loggingInSpinner: UIActivityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
    var registerButton: VIAButton = VIAButton(title: CommonStrings.Login.RegisterButtonTitle23)
    
    // MARK: View lifecycle
    
    public override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        
        self.logoImageView.image = VIAIconography.default.loginLogo()
        self.view.backgroundColor = UIColor.day()
        self.configureTableView()
        self.populateUsernameIfSaved()
        self.tableView.reloadData()
        hideRegisterButton()
        
//        debugPrint("[TouchID] viewDidLoad()")
//        self.perfomLoginWithTouchId()
    }
    
    func hideRegisterButton(){
        if let tenantIDFromInfoPlist = AppSettings.getAppTenant() {
            if tenantIDFromInfoPlist == .MLI {
                self.registerButton.isHidden = true
            }
        }
    }
    
    func populateUsernameIfSaved() {
        if let username = AppSettings.usernameForRememberMe() {
            if let environment = AppSettings.usernameEnvForRememberMe() {
                viewModel.email = environment + username
                return
            }
            
            viewModel.email = Wire.default.currentEnvironment.prefix() + username
        }
    }
    
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        UIApplication.shared.statusBarStyle = .default
        self.configureKeyboardAutoToolbar(VIAApplicableFeatures.default.enableKeyboardAutoToolbar ?? false)
    }
    
    public override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.disableKeyboardAutoToolbar()
    }
    
    public override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.perfomLoginWithTouchId()

        /**
         * Uncomment below to perform auto-login
         */
//        #if DEBUG
//            if let validEmail = AppSettings.usernameForRememberMe(){
//                viewModel.email =  Wire.default.currentEnvironment.prefix() + validEmail
//                viewModel.password = "TestPass123"
//                self.tableView.reloadData()
//                performLogin(self.loginButton)
//            }
//        #endif
    }
    
    func configureTableView() {
        self.defaultCellSeparatorColor = self.tableView.separatorColor
        
        let deviceType = UIDevice.current.getDeviceType()
        switch deviceType {
        case .AppleIphone6S, .AppleIphone7:
            if #available(iOS 8.0, *) {
                if((UIScreen.main.bounds.size.height == 667.0 || UIScreen.main.bounds.size.height == 568.0) && UIScreen.main.nativeScale < UIScreen.main.scale) {
                    enablePageScrollingOnLoginTableView()
                }
            }
            break
        case .AppleIphone5C, .AppleIphone5, .AppleIphone5S, .AppleIphoneSE, .Simulator:
            enablePageScrollingOnLoginTableView()
            break
        default:
            self.tableView.isScrollEnabled = false
        }
        self.tableView.separatorInset = UIEdgeInsets(top: 0, left: 38, bottom: 0, right: 0)
        self.tableView.separatorColor = .clear
        self.tableView.backgroundColor = UIColor.day()
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 75
        self.tableView.register(VIATextFieldTableViewCell.nib(), forCellReuseIdentifier: VIATextFieldTableViewCell.defaultReuseIdentifier)
        self.tableView.register(VIAEmailTextFieldTableViewCell.nib(), forCellReuseIdentifier: VIAEmailTextFieldTableViewCell.defaultReuseIdentifier)
        self.configureTableViewFooterView()
    }
    
    func configureTableViewFooterView() {
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: self.tableView.frame.size.width, height: 0))
        
        // login button
        loginButton.isEnabled = false
        self.loginButton.alpha = 0.5
        self.loginButton.translatesAutoresizingMaskIntoConstraints = false
        
        loggingInSpinner.frame = CGRect(x: 70.0, y: 17.0, width: 10.0, height: 10.0)
        loggingInSpinner.startAnimating()
        loggingInSpinner.alpha = 0.0
        loginButton.addSubview(loggingInSpinner)
        
        self.loginButton.addTarget(self, action: #selector(performLogin(_:)), for: .touchUpInside)
        footerView.addSubview(self.loginButton)
        
        // forgot password
        let forgotPassword = VIAButton(title: CommonStrings.Login.ForgotPasswordButtonTitle22)
        forgotPassword.hidesBorder = true
        forgotPassword.highlightedTextColor = forgotPassword.tintColor
        forgotPassword.titleLabel?.font = UIFont.tableViewFooterLabelButton()
        forgotPassword.addTarget(self, action: #selector(showForgotPassword(_:)), for: .touchUpInside)
        footerView.addSubview(forgotPassword)
        
        // register
        if let enableTouchID = VIAApplicableFeatures.default.enableTouchID,
            enableTouchID, self.viewModel.checkTouchIdPrefenceState() {
            
            viewModel.isUserNotYou = true
            
            self.registerButton.setTitle(CommonStrings.Login.RememberMe.NotYouButtonTitle60, for: .normal)
            self.registerButton.addTarget(self, action: #selector(showRegistrationForNotYou(_:)), for: .touchUpInside)
        }
        
        else {
            
            viewModel.isUserNotYou = false
            
            self.registerButton.setTitle(CommonStrings.Login.RegisterButtonTitle23, for: .normal)
            self.registerButton.addTarget(self, action: #selector(showRegistration(_:)), for: .touchUpInside)
        }
        
        self.registerButton.hidesBorder = true
        self.registerButton.highlightedTextColor = registerButton.tintColor
        self.registerButton.titleLabel?.font = UIFont.tableViewFooterLabelButton()
        footerView.addSubview(self.registerButton)
        
        // constraints
        let views = ["register": self.registerButton, "forgot": forgotPassword, "footer": footerView, "login": self.loginButton]
        footerView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[login(44)]-30-[forgot(22)]-10-[register(22)]|", options: [], metrics: nil, views: views))
        footerView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[login]|", options: [], metrics: nil, views: views))
        footerView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-[forgot]-|", options: [], metrics: nil, views: views))
        footerView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-[register]-|", options: [], metrics: nil, views: views))
        
        // fix frame
        var frame = footerView.frame
        let newHeight = footerView.systemLayoutSizeFitting(UILayoutFittingCompressedSize).height
        frame.size.height = newHeight
        footerView.frame = frame
        
        self.tableView.tableFooterView = footerView
    }
    
    // MARK: UITableView datasource
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 1 {
            let cell = VIAApplicableFeatures.default.getTextFieldViewCell(tableView: tableView, indexPath: indexPath) as! VIATextFieldTableViewCell
            
            cell.setTextFieldPlaceholder(placeholder: VIAApplicableFeatures.default.getEmailPlaceholder())
            cell.cellImageConfig.templateImage = VIALoginRegistrationAsset.LoginAndRegistration.loginEmail.templateImage
            cell.selectionStyle = .none
            cell.extendsEdgesToContentView = true
            cell.setTextFieldText(text: self.viewModel.email)
                
            if self.viewModel.isUserNotYou {
                cell.setHeadingLabelText(text: CommonStrings.Login.RememberMe.EnterPasswordTitle61)
                cell.setTextFieldEnabled(state: false)
            }
                    
            else {
                cell.setHeadingLabelText(text: nil)
                cell.setTextFieldEnabled(state: true)
            }
                
            cell.setTextFieldKeyboardType(type: .emailAddress)
            cell.textFieldTextDidChange = { textField in
                textField.clearButtonMode = .whileEditing
                self.viewModel.email = textField.text
                self.toggleLoginButtonIsEnabled()
                if(self.viewModel.emailValid) {
                    UIView.performWithoutAnimation {
                        tableView.beginUpdates()
                        cell.setErrorMessage(message: nil)
                        tableView.endUpdates()
                    }
                }
            }
                
            cell.textFieldDidEndEditing = { textField in
                tableView.reloadRows(at: [indexPath], with: .automatic)
            }
            
            if self.viewModel.isEmailDirty && !self.viewModel.emailValid {
                cell.setErrorMessage(message: self.viewModel.emailErrorMessage)
            } else {
                cell.setErrorMessage(message: nil)
            }
                
            return cell
        } else if indexPath.row == 2 {
            let cell = VIAApplicableFeatures.default.getTextFieldViewCell(tableView: tableView, indexPath: indexPath) as! VIATextFieldTableViewCell
            cell.cellImageConfig.templateImage = VIALoginRegistrationAsset.LoginAndRegistration.loginPassword.templateImage
            cell.setTextFieldPlaceholder(placeholder: CommonStrings.PasswordFieldPlaceholder19)
            cell.setHeadingLabelText(text: nil)
            cell.selectionStyle = .none
            cell.setTextFieldSecureTextEntry(secureTextEntry: true)
            cell.extendsEdgesToContentView = true
            cell.setTextFieldText(text: self.viewModel.password)
            cell.setTextFieldKeyboardType(type: .default)
            cell.textFieldTextDidChange = { textField in
                textField.clearButtonMode = .whileEditing
                self.viewModel.password = textField.text
                self.toggleLoginButtonIsEnabled()
            }
            
            if let enableTouchID = VIAApplicableFeatures.default.enableTouchID,
                enableTouchID,self.viewModel.checkTouchIdPrefenceState(){
                cell.leftImage = VIALoginRegistrationAsset.PreferencesOnboarding.touchIdIcon.image
                cell.touchIdIconGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(perfomLoginWithTouchId))
            }
            return cell
        }
        
        // blank cell
        let blankCell = UITableViewCell(style: .default, reuseIdentifier: "cell")
        blankCell.selectionStyle = .none
        return blankCell
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return UITableViewAutomaticDimension
        } else if indexPath.row == 3 {
            return 28
        }
        return UITableViewAutomaticDimension
    }
    
    // MARK: UITableView delegate
    
    public func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    public func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
    
    public func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.section == 0 && indexPath.row == 0 {
            tableView.separatorColor = self.defaultCellSeparatorColor
        } else {
            tableView.separatorColor = .clear
        }
    }
    
    // MARK: Actions
    
    func dismissKeyboard(_ gesture: UITapGestureRecognizer?) {
        self.tableView.endEditing(true)
    }
    
    func performLogin(_ sender: UIButton?) {
        self.loginButton.isEnabled = false
        loggingInSpinner.color = UIColor.primaryColor()
        loggingInSpinner.alpha = 1.0
        self.loginButton.setTitle(CommonStrings.Login.LoggingInButtonTitle21, for: .normal)
        
        self.dismissKeyboard(nil)
        
        self.viewModel.loginComplete = { [weak self] error in
            self?.loginComplete(error)
            self?.loginButton.isEnabled = true
        }
        
        self.viewModel.performLogin()
    }
    
    func loginComplete(_ error: Error?) {
        loggingInSpinner.alpha = 0.0
        resetLoginButton()
        
        guard error == nil else {
            switch error {
            case is LoginError:
                self.handleLoginError(error as! LoginError)
            case is RegistrationError:
                self.handleLoginError(error as! LoginError)
            case is BackendError:
                self.handleBackendErrorWithAlert(error as! BackendError, tryAgainAction: { [weak self] in
                    self?.performLogin(nil)
                })
            default:
                self.displayUnknownServiceErrorOccurredAlert()
            }
            self.hideHUDFromWindow()
            return
        }
        
        if DataProvider.currentVitalityPartyIsValid(){

            AppSettings.updateLastLoginDate()
            //HomeScreenDataController.loadData(realm: DataProvider.newRealm(), completion: nil)
            
            // for Apple Health device auto-sync
            configureAutoSync()
            
            VIASplashScreenViewController.showInsurerSplashScreen(skipAppConfigRequest: true)
        }else{
            debugPrint("[DebugMode] current Vitality Party is INVALID. User should be logged out.")
            NotificationCenter.default.post(name: .VIAServerSecurityException, object: nil)
        }
    }
    
    func handleLoginError(_ error: LoginError) {
        self.displayLoginErrorMessage(error: error )
        resetLoginButton()
    }
    
    func resetLoginButton() {
        self.loginButton.setTitle(CommonStrings.Login.LoginButtonTitle20, for: .normal)
    }
    
    func displayLoginErrorMessage(error: LoginError) {
        
        var dialogTitle = ""
        var dialogMessage = ""
        
        if error == .accountLockedOut {
            dialogTitle = CommonStrings.Login.IncorrectEmailPasswordErrorTitle47
            dialogMessage = CommonStrings.Login.AccountLockedErrorAlertMessage738
        }
        else {
            dialogTitle = CommonStrings.Login.IncorrectEmailPasswordErrorTitle47
            dialogMessage = CommonStrings.Login.IncorrectEmailPasswordErrorMessage48
        }
        
        let controller = UIAlertController(title: dialogTitle,
                                           message: dialogMessage,
                                           preferredStyle: .alert)
        
        if error == .accountLockedOut {
            let resetPassword = UIAlertAction(title: CommonStrings.Login.ResetPasswordButtonTitle22, style: .default) { action in
                self.showForgotPassword(nil)
            }
            controller.addAction(resetPassword)
        }
        else if self.loginHasFailedAtLeastOnce {
            let forgotPassword = UIAlertAction(title: CommonStrings.Login.ForgotPasswordButtonTitle22, style: .default) { action in
                self.showForgotPassword(nil)
            }
            controller.addAction(forgotPassword)
        }
        
        let ok = UIAlertAction(title: CommonStrings.OkButtonTitle40, style: .cancel) { action in
        }
        controller.addAction(ok)
        
        self.loginHasFailedAtLeastOnce = true
        self.present(controller, animated: true, completion: nil)
    }
    
    func toggleLoginButtonIsEnabled() {
        if self.viewModel.isFieldEmpty || !viewModel.emailValid {
            self.loginButton.isEnabled = false
            self.loginButton.alpha = 0.5
            
        } else {
            self.loginButton.isEnabled = true
            self.loginButton.alpha = 1.0
        }
    }
    
    
    func perfomLoginWithTouchId() {
//        debugPrint("[TouchID] perfomLoginWithTouchId()")
        
        if let enableTouchID = VIAApplicableFeatures.default.enableTouchID,
            enableTouchID, self.viewModel.checkTouchIdPrefenceState() {
            
            if self.viewModel.checkIfTouchIdIsEnabled() {
                
                if self.viewModel.checkTouchIdDomainStateHasChanged() {
                    
                    self.displayMessageForDomainStateChanged()
                }
                    
                else {
                    
                    self.viewModel.loginWithTouchId(shouldUpdateState: false, completion: { error in
                        
                        if error == nil {
                            
                            self.viewModel.passwordForTouchID()
                            DispatchQueue.main.async {
                                self.performLogin(self.loginButton)
                            }
                        }
                    })
                }
            }
                
            else {
                
                self.displayErrorAlertController(title: CommonStrings.Login.Touchid.TouchIdDisabledTitle1142, message:  CommonStrings.Login.Touchid.TouchIdDisabledMessage1143)
            }
        }
    }
    
    func displayMessageForDomainStateChanged() {
        
        let alert = UIAlertController(title: CommonStrings.Login.Touchid.FingerprintChangeTitle1140, message:  CommonStrings.Login.Touchid.FingerprintChangeMessage1141, preferredStyle: .alert)
        
        let yes = UIAlertAction(title: "Yes", style: .default) { action in
           
            alert.dismiss(animated: true, completion: nil)
            
            self.viewModel.updateTouchIdPreference()
        }
        
        let no = UIAlertAction(title: "No", style: .cancel) { action in
            
            alert.dismiss(animated: true, completion: nil)
            
            self.displayErrorAlertController(title: CommonStrings.Login.Touchid.TouchIdDisabledTitle1142, message:  CommonStrings.Login.Touchid.TouchIdDisabledMessage1143)
        }
        
        alert.addAction(yes)
        alert.addAction(no)
        self.present(alert, animated: true, completion: nil)
    }
    
    func displayErrorAlertController(title : String, message : String) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let ok = UIAlertAction(title: CommonStrings.OkButtonTitle40, style: .cancel) { action in
            alert.dismiss(animated: true, completion: nil)
            
            self.viewModel.turnOffLoginWithTouchId()
        }
        
        alert.addAction(ok)
        self.present(alert, animated: true, completion: nil)
    }
    
    // MARK: Navigation
    
    public static func showLogin(upgradeUrl: String? = nil) {
        let storyboard = UIStoryboard(name: "LoginRegistration", bundle: nil)
//        if Bundle.main.path(forResource: loginRegistration, ofType: "storyboardc") != nil {
//            storyboard = UIStoryboard(name: "LoginRegistration", bundle: nil)
//        } else {
//            storyboard = UIStoryboard(coreStoryboard: .loginRegistration)
//        }
        
        guard let viewController = storyboard.instantiateInitialViewController() as? VIAFirstTimeOnboardingViewController else { return }
        viewController.displayLoginImmediately = true
        UIApplication.shared.keyWindow?.rootViewController = viewController
        
        logOut()
        
        if let enableOTAUpdate = Bundle.main.object(forInfoDictionaryKey: "VIAEnableOTAUpdate") as? Bool, enableOTAUpdate{
            guard let appURL = upgradeUrl, let url = URL(string: appURL) else { return }
            let alert = UIAlertController(title: CommonStrings.AppUpdate.HeaderTitle2126,
                                          message: CommonStrings.AppUpdate.Message2127, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: CommonStrings.AppUpdate.UpdateButton2128, style: .default, handler: { (action) in
                if UIApplication.shared.canOpenURL(url){
                    UIApplication.shared.open(url) 
                }
            }))
            viewController.present(alert, animated: true, completion: nil)
        }
    }
    
    private static func logOut() {
        Wire.cancelAllTasks()
        DataProvider.reset()
        
        NotificationCenter.default.removeObserver(AssessmentLandingViewController.self, name: .VIAVHRQuestionnaireSubmittedNotification, object: nil)
        NotificationCenter.default.removeObserver(AssessmentLandingViewController.self, name: .VIAVHRQuestionnaireStarted, object: nil)
    }
    
    public override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showForgotPassword" {
            let navController = segue.destination as? UINavigationController
            let viewController = navController?.topViewController as? VIAForgotPasswordViewController
            viewController?.viewModel.email = self.viewModel.email
        }
        
        if segue.identifier == "showRegistration" {
            let navController = segue.destination as? UINavigationController
            let viewController = navController?.topViewController as? VIARegistrationViewController
            viewController?.viewModel.registrationDetail.email = self.viewModel.email
        }
    }
    
    @IBAction public func unwindToLogin(segue: UIStoryboardSegue) {
        // typically called when register / forgot password was cancelled
    }
    
    @IBAction public func unwindToLoginAfterSuccessfulRegistration(segue: UIStoryboardSegue) {
        if let controller = segue.source as? VIARegistrationViewController {
            self.viewModel.email = controller.viewModel.registrationDetail.email
            self.viewModel.password = controller.viewModel.registrationDetail.password
            self.tableView.reloadData()
            self.performLogin(self.loginButton)
        }
    }
    
    func showRegistrationForNotYou(_ sender: UIButton?) {
        
        viewModel.isUserNotYou = false
        
        sender?.setTitle(CommonStrings.Login.RegisterButtonTitle23, for: .normal)
        sender?.removeTarget(self, action: #selector(showRegistrationForNotYou(_:)), for: .touchUpInside)
        sender?.addTarget(self, action: #selector(showRegistration(_:)), for: .touchUpInside)
        
        AppSettings.removeUsernameForRememberMe()
        viewModel.email = ""
        viewModel.turnOffLoginWithTouchId()
        
        self.tableView.reloadData()
    }
    
    func showRegistration(_ sender: UIButton?) {
        self.performSegue(withIdentifier: "showRegistration", sender: nil)
    }
    
    func showForgotPassword(_ sender: UIButton?) {
        self.performSegue(withIdentifier: "showForgotPassword", sender: nil)
    }
}

extension VIALoginViewController {
    /* Enables table view scrool bar to fix the UI issue on the following iPhone devices:
     
     - iPhone5C
     - iPhone5
     - iPhone5S
     - iPhoneSE
     - iPhone6S  : Zoomed view
     - iPhone7   : Zoomed view
     
     */
    func enablePageScrollingOnLoginTableView() {
        self.tableView.isScrollEnabled = true
        self.tableView.showsVerticalScrollIndicator = false
        self.tableView.showsHorizontalScrollIndicator = false
        
    }
}
