import UIKit
import VIAUIKit
import VitalityKit
import VitalityKit
import VIACommon

public class VIAForgotPasswordViewController: VIATableViewController {
    
    public var viewController: String?
    
    public var emailCell: VIATextFieldTableViewCell?
    public let viewModel = VIAForgotPasswordViewModel()
    @IBOutlet weak var doneButton: UIBarButtonItem!

    public override func viewDidLoad() {
        super.viewDidLoad()
        let tapAwayGestureRecognizer: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(removeKeyboard))
        view.addGestureRecognizer(tapAwayGestureRecognizer)
        title = CommonStrings.ForgotPassword.ScreenTitle52
        setupNavigationbarItemTitles()
        configureTableView()
    }

    func setupNavigationbarItemTitles() {
        navigationItem.rightBarButtonItem?.title = CommonStrings.DoneButtonTitle53
        navigationItem.leftBarButtonItem?.title = CommonStrings.CancelButtonTitle24
    }

    func removeKeyboard() {
        tableView.endEditing(true)
        if shouldShowErrorMessage() {
            showInvalidEmailMessage()
        }
    }

    func shouldShowErrorMessage() -> Bool {
        return !viewModel.emailValid && viewModel.email != "" && viewModel.email != nil
    }

    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        tableView.sizeHeaderViewToFit()
        update()
    }

    public override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        // make first cell first responder
        let cell = tableView.cellForRow(at: IndexPath(row: 0, section: 0))
        if cell != nil { cell!.becomeFirstResponder() }
    }

    // MARK: Configure views

    func configureTableView() {
        tableView.estimatedRowHeight = 75

        tableView.register(VIATextFieldTableViewCell.nib(), forCellReuseIdentifier: VIATextFieldTableViewCell.defaultReuseIdentifier)
        tableView.register(VIAEmailTextFieldTableViewCell.nib(), forCellReuseIdentifier: VIAEmailTextFieldTableViewCell.defaultReuseIdentifier)

        configureTableViewHeaderView()
    }

    func configureTableViewHeaderView() {
        let view = VIATableViewHeaderFooterTextView.viewFromNib(owner: self) as! VIATableViewHeaderFooterTextView
        view.setLabelText(text: CommonStrings.ForgotPassword.InstructionMessage54)
        view.location = .top
        tableView.tableHeaderView = view
    }

    // MARK: UITableView data source & delegate

    public override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    public override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }

    public override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = VIAApplicableFeatures.default.getTextFieldViewCell(tableView: tableView, indexPath: indexPath) as! VIATextFieldTableViewCell
        cell.selectionStyle = .none

        var imageConfig = VIATextFieldCellImageConfig()
        imageConfig.templateImage = VIALoginRegistrationAsset.LoginAndRegistration.loginEmail.templateImage
        cell.cellImageConfig = imageConfig

        cell.setTextFieldPlaceholder(placeholder: CommonStrings.Registration.EmailFieldPlaceholder27)
        cell.setHeadingLabelText(text: CommonStrings.Registration.EmailFieldTitle26)
        cell.setTextFieldText(text: viewModel.email)
        cell.setTextFieldKeyboardType(type: .emailAddress)

        cell.textFieldTextDidChange = { [unowned self] textField in
            self.viewModel.email = textField.text

            if(!self.shouldShowErrorMessage()) {
                UIView.performWithoutAnimation {
                    tableView.beginUpdates()
                    cell.setErrorMessage(message: nil)
                    tableView.endUpdates()
                }
            }
            self.update()
        }

        cell.textFieldDidEndEditing = { [unowned self] textField in
            self.tableView.reloadRows(at: [indexPath], with: .automatic)
        }

        if viewModel.isDirty && shouldShowErrorMessage() {
            cell.setErrorMessage(message: CommonStrings.Registration.InvalidEmailFootnoteError35)
        } else {
            cell.setErrorMessage(message: nil)
        }

        emailCell = cell
        return cell
    }

    public override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }

    func showInvalidEmailMessage() {
        UIView.performWithoutAnimation {
            tableView.beginUpdates()
            guard emailCell != nil else { return }
            let errorMessage = CommonStrings.Registration.InvalidEmailFootnoteError35
            emailCell!.setErrorMessage(message: viewModel.emailValid ? nil : errorMessage)
            tableView.endUpdates()
        }
    }

    public override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }

    // MARK: Actions
    func update() {
        let validEmail = viewModel.emailValid
        navigationItem.rightBarButtonItem?.isEnabled = validEmail
        viewModel.isDirty = validEmail
    }

    func showNotRegisteredAlert() {
        let alert = UIAlertController(title: CommonStrings.ForgotPassword.EmailNotRegisteredAlertTitle56, message: CommonStrings.ForgotPassword.EmailNotRegisteredAlertMessage57, preferredStyle: .alert)
        let okayAlertAction = UIAlertAction(title: CommonStrings.OkButtonTitle40, style: .default) { alertAction in
            alert.dismiss(animated: true, completion: nil)
        }
        alert.addAction(okayAlertAction)
        present(alert, animated: true, completion: nil)
    }

    func forgotPasswordRequestFailure(_ error: Error) {
        hideHUDFromWindow()
        doneButton.isEnabled = true
        switch error {
        case is ForgotPasswordError:
            if error as! ForgotPasswordError == ForgotPasswordError.notRegistered {
                showNotRegisteredAlert()
            }
        default:
            handleBackendErrorWithAlert(error as! BackendError, tryAgainAction: { [weak self] in
                self?.submit()
            })
        }
    }

    @IBAction func submit(_ sender: AnyObject? = nil) {
        doneButton.isEnabled = false
        removeKeyboard()
        showHUDOnWindow()
        viewModel.success = { [weak self] in
            self?.hideHUDFromWindow()
            self?.doneButton.isEnabled = true
            self?.passwordRequestDidComplete()
        }
        viewModel.failure = forgotPasswordRequestFailure

        viewModel.requestPassword()
    }

    @IBAction func cancel(_ sender: AnyObject) {
        tableView.endEditing(true)
        
        guard let editEmailVC = self.viewController, editEmailVC == "EditEmailViewController" else {
            performSegue(withIdentifier: "unwindToLogin", sender: nil)
            return
        }
        
        performSegue(withIdentifier: "unwindToEditEmailViewController", sender: nil)
    }

    func passwordRequestDidComplete() {
        performSegue(withIdentifier: "showFeedback", sender: nil)
    }

    public override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showFeedback" {
            prepareFeedbackViewController(segue.destination)
            if (segue.destination is VIAFeedbackViewController) {
                navigationController?.setNavigationBarHidden(true, animated: false)
            }
        }
    }

    func prepareFeedbackViewController(_ viewcontroller: UIViewController?) {
        guard let feedbackViewController = viewcontroller as! VIAFeedbackViewController? else {
            return
        }

        feedbackViewController.headingText = CommonStrings.ForgotPassword.ConfirmationScreen.EmailSentMessageTitle58
        feedbackViewController.bodyText = CommonStrings.ForgotPassword.ConfirmationScreen.EmailSentMessage59
        feedbackViewController.setButtonTitle(title: CommonStrings.DoneButtonTitle53)

        //REVERT
        let image = VIALoginRegistrationAsset.Feedback.feedbackEmail.templateImage
        feedbackViewController.imageDetail = FeedbackImageDetail(image, UIColor.primaryColor())
        
        if let editEmailVC = self.viewController, editEmailVC == "EditEmailViewController"{
            feedbackViewController.isHierarchyFromEditEmail = true
        }
    }
}
