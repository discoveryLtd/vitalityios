import UIKit
import VIAUIKit
import VitalityKit
import VitalityKit
import VIACommon
import TTTAttributedLabel
import VIAUtilities

class VIARegistrationViewController: VIATableViewController, PrimaryColorTintable {

    public var viewModel: VIARegistrationViewModel = VIARegistrationViewModel()
    //var isResendInsurerCodeEnable = true
    var footer = VIATableViewSectionHeaderFooterView()
    var range = NSRange()
    var showDateOfBirthField: Bool = false
    
    @IBOutlet var registerButton: UIBarButtonItem!
    @IBOutlet var cancelButton: UIBarButtonItem!

    override func viewDidLoad() {
        super.viewDidLoad()

        title = CommonStrings.Registration.ScreenTitle25
        setupNavigationbarItemTitles()
        configureTableView()
        
        guard let showDateOfBirthValue = VIAApplicableFeatures.default.showDateOfBirthField else { return }
        self.showDateOfBirthField = showDateOfBirthValue
    }

    func setupNavigationbarItemTitles() {
        cancelButton.title = CommonStrings.CancelButtonTitle24
        registerButton.title = CommonStrings.Login.RegisterButtonTitle23
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        updateNavigationButtonsEnabled()
        self.configureKeyboardAutoToolbar(VIAApplicableFeatures.default.enableKeyboardAutoToolbar ?? false)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        // make first cell first responder
        //let cell = tableView.cellForRow(at: IndexPath(row: 0, section: 0))
        //if cell != nil { cell!.becomeFirstResponder() }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.disableKeyboardAutoToolbar()
    }

    func configureTableView() {
        tableView.register(VIATextFieldTableViewCell.nib(), forCellReuseIdentifier: VIATextFieldTableViewCell.defaultReuseIdentifier)
        tableView.register(VIAEmailTextFieldTableViewCell.nib(), forCellReuseIdentifier: VIAEmailTextFieldTableViewCell.defaultReuseIdentifier)
        tableView.register(VIARightDetailTableViewCell.nib(), forCellReuseIdentifier: VIARightDetailTableViewCell.defaultReuseIdentifier)
        tableView.estimatedRowHeight = 75
        tableView.separatorInset = UIEdgeInsets(top: 0, left: 52, bottom: 0, right: 0)

        tableView.register(VIATableViewSectionHeaderFooterView.nib(), forHeaderFooterViewReuseIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier)
        tableView.estimatedSectionFooterHeight = 75
    }
    
    // MARK: - UITableView enums
    
    enum Sections: Int, EnumCollection {
        case registrationFields = 0
    }
    
    enum RegistrationTextField: Int, EnumCollection {
        case email = 0
        case password = 1
        case confirmPassword = 2
        case dob = 3
        case code = 4
    }
    
    var registrationRows: NSMutableIndexSet {
        let indexSet = NSMutableIndexSet()
        
        indexSet.add(RegistrationTextField.email.rawValue)
        indexSet.add(RegistrationTextField.password.rawValue)
        indexSet.add(RegistrationTextField.confirmPassword.rawValue)
        if showDateOfBirthField {
            indexSet.add(RegistrationTextField.dob.rawValue)
        }
        indexSet.add(RegistrationTextField.code.rawValue)
        
        return indexSet
    }

    // MARK: - UITableView datasource

    override func numberOfSections(in tableView: UITableView) -> Int {
        return Sections.allValues.count
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return registrationRows.count
    }
        
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return registrationCell(tableView: tableView, at: indexPath)
    }
    
    func registrationCell(tableView: UITableView, at indexPath: IndexPath) -> UITableViewCell{
        
        guard var cell = tableView.dequeueReusableCell(withIdentifier: VIATextFieldTableViewCell.defaultReuseIdentifier, for: indexPath) as? VIATextFieldTableViewCell else {
            return UITableViewCell(style: .default, reuseIdentifier: "cell")
        }
        
        var imageConfig: VIATextFieldCellImageConfig = VIATextFieldCellImageConfig()
        var headingText: String? = nil
        var textFieldPlaceholder: String? = nil
        var secureTextEntry = false
        var keyboardType: UIKeyboardType = .default
        var errorMessage: String? = nil
        var textFieldText: String? = nil
        var currentFieldIsValid = false
        var currentFieldIsDirty = false
        
        let rowValue = RegistrationTextField(rawValue: registrationRows.integer(at: indexPath.row))
        if rowValue == .email {
            cell = VIAApplicableFeatures.default.getTextFieldViewCell(tableView: tableView, indexPath: indexPath) as! VIATextFieldTableViewCell
            
            imageConfig = registrationImageConfig(image: VIALoginRegistrationAsset.LoginAndRegistration.loginEmail.templateImage)
            headingText = CommonStrings.Registration.EmailFieldTitle26
            textFieldPlaceholder = CommonStrings.Registration.EmailFieldPlaceholder27
            keyboardType = .emailAddress
            textFieldText = viewModel.registrationDetail.email
            errorMessage = viewModel.registrationDetail.emailErrorMessage
            currentFieldIsValid = viewModel.registrationDetail.emailValid
            currentFieldIsDirty = viewModel.registrationDetail.isEmailDirty
            cell.isDOBField = false
            cell.textFieldTextDidChange = { [unowned self] textField in
                guard textField.text != nil else {
                    return
                }
                if textField.text!.range(of:"—") != nil {
                    self.reloadEmailTextField(textField, at:indexPath)
                }
                self.viewModel.registrationDetail.email = textField.text
                self.updateNavigationButtonsEnabled()
                if(self.viewModel.registrationDetail.emailValid) {
                    UIView.performWithoutAnimation {
                        tableView.beginUpdates()
                        cell.setErrorMessage(message: nil)
                        tableView.endUpdates()
                    }
                }
            }
            cell.textFieldDidEndEditing = { textField in
                tableView.reloadRows(at: [indexPath], with: .automatic)
            }
        } else if rowValue == .password {
            cell = VIAApplicableFeatures.default.getTextFieldViewCell(tableView: tableView, indexPath: indexPath) as! VIATextFieldTableViewCell
            
            imageConfig = registrationImageConfig(image: VIALoginRegistrationAsset.LoginAndRegistration.loginPassword.templateImage)
            headingText = CommonStrings.PasswordFieldPlaceholder19
            textFieldPlaceholder = CommonStrings.Registration.PasswordFieldPlaceholder28
            secureTextEntry = true
            keyboardType = .default
            textFieldText = viewModel.registrationDetail.password
            errorMessage = viewModel.registrationDetail.passwordErrorMessage
            currentFieldIsValid = viewModel.registrationDetail.passwordValid
            currentFieldIsDirty = viewModel.registrationDetail.isPasswordDirty
            cell.isDOBField = false
            cell.textFieldTextDidChange = { [unowned self] textField in
                self.viewModel.registrationDetail.password = textField.text
                self.clearConfirmPasswordTextField()
                self.updateNavigationButtonsEnabled()
                if(self.viewModel.registrationDetail.passwordValid) {
                    UIView.performWithoutAnimation {
                        tableView.beginUpdates()
                        cell.setErrorMessage(message: nil)
                        tableView.endUpdates()
                    }
                }
            }
            cell.textFieldDidEndEditing = { [unowned self] textField in
                self.reloadPasswordTextField(textField, at:indexPath)
            }
        } else if rowValue == .confirmPassword {
            cell = VIAApplicableFeatures.default.getTextFieldViewCell(tableView: tableView, indexPath: indexPath) as! VIATextFieldTableViewCell
            
            imageConfig = registrationImageConfig(image: VIALoginRegistrationAsset.LoginAndRegistration.loginConfirmPassword.templateImage)
            headingText = CommonStrings.Registration.ConfirmPasswordFieldTitle30
            textFieldPlaceholder = CommonStrings.Registration.ConfirmPasswordFieldPlaceholder31
            secureTextEntry = true
            keyboardType = .default
            textFieldText = viewModel.registrationDetail.confirmPassword
            errorMessage = viewModel.registrationDetail.confirmPasswordErrorMessage
            currentFieldIsValid = viewModel.registrationDetail.confirmPasswordValid
            currentFieldIsDirty = viewModel.registrationDetail.isConfirmPasswordDirty
            cell.isDOBField = false
            cell.textFieldTextDidChange = { [unowned self] textField in
                self.viewModel.registrationDetail.confirmPassword = textField.text
                self.updateNavigationButtonsEnabled()
                if(self.viewModel.registrationDetail.confirmPasswordValid) {
                    UIView.performWithoutAnimation {
                        tableView.beginUpdates()
                        cell.setErrorMessage(message: nil)
                        tableView.endUpdates()
                    }
                    self.updateNavigationButtonsEnabled()
                }
            }
            cell.textFieldDidEndEditing = { [unowned self] textField in
                self.reloadPasswordTextField(textField, at:indexPath)
            }
        } else if rowValue == .dob {
            imageConfig = registrationImageConfig(image: VIALoginRegistrationAsset.LoginAndRegistration.loginBirth.templateImage)
            headingText = CommonStrings.Settings.ProfileLandingDateOfBirth916
            textFieldPlaceholder = CommonStrings.Common.DateFormat.Yyyymmdd2197
            keyboardType = .numberPad
            textFieldText = viewModel.registrationDetail.bornOn
            errorMessage = CommonStrings.Registration.InvalidDateFormat2359
            currentFieldIsValid = viewModel.registrationDetail.bornOnValid
            currentFieldIsDirty = viewModel.registrationDetail.isBornOnDirty
            cell.isDOBField = true
            cell.textFieldTextDidChange = { [unowned self] textField in
                self.viewModel.registrationDetail.bornOn = textField.text
                self.updateNavigationButtonsEnabled()
                if (self.viewModel.registrationDetail.bornOnValid) {
                    UIView.performWithoutAnimation {
                        tableView.beginUpdates()
                        cell.setErrorMessage(message: nil)
                        tableView.endUpdates()
                    }
                }
                cell.textFieldDidEndEditing = { textField in
                    tableView.reloadRows(at: [indexPath], with: .automatic)
                }
            }
        } else if rowValue == .code {
            imageConfig = registrationImageConfig(image: VIALoginRegistrationAsset.LoginAndRegistration.loginRegistrationCode.templateImage)
            headingText = CommonStrings.Registration.RegistrationCodeFieldTitle32
            textFieldPlaceholder = CommonStrings.Registration.RegistrationCodeFieldPlaceholder33
            keyboardType = .numberPad
            textFieldText = viewModel.registrationDetail.registrationCode
            errorMessage = viewModel.registrationDetail.registrationCodeErrorMessage
            currentFieldIsValid = viewModel.registrationDetail.registrationCodeValid
            currentFieldIsDirty = viewModel.registrationDetail.isRegistrationCodeDirty
            cell.isDOBField = false
            cell.textFieldTextDidChange = { [unowned self] textField in
                self.viewModel.registrationDetail.registrationCode = textField.text
                self.updateNavigationButtonsEnabled()
                if(self.viewModel.registrationDetail.registrationCodeValid) {
                    UIView.performWithoutAnimation {
                        tableView.beginUpdates()
                        cell.setErrorMessage(message: nil)
                        tableView.endUpdates()
                    }
                }
            }
            cell.textFieldDidEndEditing = { textField in
                tableView.reloadRows(at: [indexPath], with: .automatic)
            }
        }
        
        cell.selectionStyle = .none
        cell.cellImageConfig = imageConfig
        cell.setTextFieldText(text: textFieldText)
        cell.setHeadingLabelText(text: headingText)
        cell.setTextFieldPlaceholder(placeholder: textFieldPlaceholder)
        cell.setTextFieldSecureTextEntry(secureTextEntry: secureTextEntry)
        cell.setTextFieldKeyboardType(type: keyboardType)
        
        if currentFieldIsDirty && !currentFieldIsValid {
            cell.setErrorMessage(message: errorMessage)
        } else {
            cell.setErrorMessage(message: nil)
        }
        
        return cell
        
    }

    // MARK: - Insurer Code
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let lastSection = Sections.allValues.count - 1
        if section == lastSection {
            if let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: VIATableViewSectionHeaderFooterView.defaultReuseIdentifier) as? VIATableViewSectionHeaderFooterView {
                view.labelText = CommonStrings.Registration.RegistrationCodeFieldFootnote34
                
                //ge20180221: For CR VA-26325
                if(VIAApplicableFeatures.default.enableUserToResendInsurerCode!){
                    //waiting for CMS (to remove)
                    
                    let footnote = "\(CommonStrings.Insurercode.ResendInsurerCodeFootnoteText2252) \(CommonStrings.Registration.CodeFieldFootnoteResendText34)"
                    //let footnote = LoginStrings.Insurercode.ResendInsurerCodeFootnoteText999
                    let range = (footnote as NSString).range(of: CommonStrings.Registration.CodeFieldFootnoteResendText34)
                    let attribute = NSMutableAttributedString.init(string: footnote)
                    //if isResendInsurerCodeEnable{
                    attribute.addAttribute(NSForegroundColorAttributeName, value: UIColor.primaryColor() , range: range)
                    view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(requestCode(sender:))))
                    //}
                    view.attributedLabelText = attribute
                    //view.addRect(rect: self.rect)
                    self.footer = view
                    self.range = range
                }
                return view
            }
        }
        return nil
    }
    
    /**
        //ge20180221: Get Rect that contains "Resend Insurer Code", and make it act like UIButton
    */
    func getRectOfRangeInString(str: NSAttributedString, range: NSRange, maxWidth: CGFloat) -> CGRect {
        let textStorage = NSTextStorage(attributedString: str)
        let textContainer = NSTextContainer(size: CGSize(width: maxWidth, height: CGFloat.greatestFiniteMagnitude))
        let layoutManager = NSLayoutManager()
        layoutManager.addTextContainer(textContainer)
        textStorage.addLayoutManager(layoutManager)
        textContainer.lineFragmentPadding = 0
        let pointer = UnsafeMutablePointer<NSRange>.allocate(capacity: 1)
        layoutManager.characterRange(forGlyphRange: range, actualGlyphRange: pointer)
        return layoutManager.boundingRect(forGlyphRange: pointer.move(), in: textContainer)
    }
    
    /**
        //ge20180221: "Resend Insurer Code" button action
     */
    func requestCode(sender: UITapGestureRecognizer) {
        if sender.didTapAttributedText(label: self.footer.aLabel, inRange: self.range) {
            perform(segue: StoryboardSegue.LoginRegistration.showResendCode, sender: self)
        }
    }
    
    public override func prepare(for segue: UIStoryboardSegue, sender: Any?){
        if (segue.identifier == "showResendCode"){
            let destination = segue.destination as! UINavigationController
            let vc = destination.topViewController as! VIAResendInsurerCodeViewController
            vc.regModel = self.viewModel
        }
    }

    // MARK: - UITableView delegate

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! VIATextFieldTableViewCell
        _ = cell.becomeFirstResponder()
    }

    // MARK: - Actions

    func updateNavigationButtonsEnabled() {
        navigationItem.rightBarButtonItem?.isEnabled = viewModel.registrationDetail.isValid
    }

    @IBAction func cancel(_ sender: AnyObject) {
        tableView.endEditing(true)
        perform(segue: StoryboardSegue.LoginRegistration.unwindToLogin, sender: self)
    }

    @IBAction func registerTapped(_ sender: AnyObject?) {
        updateNavigationButtonsEnabled()

        tableView.endEditing(true)
        showHUDOnWindow()

        performRegistration()
    }

    func performRegistration() {
        viewModel.performRegistration { [weak self] error in
            self?.hideHUDFromWindow()
            guard error == nil else {
                
                switch error {
                case is RegistrationError:
                    self?.handleRegistrationError(error as! RegistrationError)
                case is BackendError:
                    self?.handleBackendErrorWithAlert(error as! BackendError, tryAgainAction: { [weak self] in
                        self?.registerTapped(nil)
                    })
                default:
                    self?.displayOtherRegistrationError()
                }
                self?.tableView.reloadData()
                return
            }

            self?.registrationDidComplete()
        }
    }

    func registrationDidComplete() {
        
        AppSettings.resetAll()
        
        perform(segue: StoryboardSegue.LoginRegistration.unwindToLoginAfterSuccessfulRegistration, sender: self)
    }

    // MARK: Error handling

    func handleRegistrationError(_ error: RegistrationError) {
        switch error {
        case .invalidEmail:
            displayEmailregistrationCodeCombinationError()
            //isResendInsurerCodeEnable = true
        case .invalidEmailregistrationCodeCombination:
            displayEmailregistrationCodeCombinationError()
            //isResendInsurerCodeEnable = true
        case .connectivityError:
            displayConnectivityError()
        case .userAlreadyExists:
            displayUserAlreadyExistsError()
            //isResendInsurerCodeEnable = false
        case .invalidDateOfBirth:
            displayDateOfBirthError()
        case .emptyDateOfBirth:
            displayEmptyDateOfBirthError()
        }
        //self.tableView.reloadSections(IndexSet(integer: 0), with: .automatic)
        
    }

    func displayEmailregistrationCodeCombinationError() {
        let controller = UIAlertController(title: CommonStrings.Registration.InvalidEmailOrRegistrationCodeAlertTitle38,
                                           message: CommonStrings.Registration.InvalidEmailOrRegistrationCodeAlertMessage39,
                                           preferredStyle: .alert)

        let ok = UIAlertAction(title: CommonStrings.OkButtonTitle40, style: .default) { action in }
        controller.addAction(ok)

        present(controller, animated: true, completion: nil)
    }

    func displayConnectivityError() {
        let controller = UIAlertController(title: CommonStrings.ConnectivityErrorAlertTitle44,
                                           message: CommonStrings.ConnectivityErrorAlertMessage45,
                                           preferredStyle: .alert)

        let tryAgain = UIAlertAction(title: CommonStrings.TryAgainButtonTitle43, style: .default) { [weak self] action in
            self?.registerTapped(nil)
        }
        controller.addAction(tryAgain)

        let cancel = UIAlertAction(title: CommonStrings.CancelButtonTitle24, style: .cancel) { action in }
        controller.addAction(cancel)

        present(controller, animated: true, completion: nil)
    }

    func displayUserAlreadyExistsError() {
        let controller = UIAlertController(title: CommonStrings.Registration.UnableToRegisterAlertTitle41,
                                           message: CommonStrings.Registration.UnableToRegisterAlertMessage46,
                                           preferredStyle: .alert)

        let ok = UIAlertAction(title: CommonStrings.OkButtonTitle40, style: .cancel) { action in }
        controller.addAction(ok)

        present(controller, animated: true, completion: nil)
    }

    func displayOtherRegistrationError() {
        let controller = UIAlertController(title: CommonStrings.Registration.UnableToRegisterAlertTitle41,
                                           message: CommonStrings.Registration.UnableToRegisterAlertMessage42,
                                           preferredStyle: .alert)

        let tryAgain = UIAlertAction(title: CommonStrings.TryAgainButtonTitle43, style: .default) { [weak self] action in
            self?.registerTapped(nil)
        }
        controller.addAction(tryAgain)

        let cancel = UIAlertAction(title: CommonStrings.CancelButtonTitle24, style: .cancel) { action in }
        controller.addAction(cancel)

        present(controller, animated: true, completion: nil)
    }
    
    func displayDateOfBirthError() {
        let controller = UIAlertController(title: CommonStrings.Settings.ProfileLandingDateOfBirth916,
                                           message: CommonStrings.Registration.DateOfBirthDoesNotMatch2070,
                                           preferredStyle: .alert)
        
        let ok = UIAlertAction(title: CommonStrings.OkButtonTitle40, style: .default) { action in }
        controller.addAction(ok)
        
        present(controller, animated: true, completion: nil)
    }
    
    func displayEmptyDateOfBirthError(){
        let controller = UIAlertController(title: CommonStrings.Settings.ProfileLandingDateOfBirth916,
                                           message: CommonStrings.Registration.DateOfBirthDoesNotMatch2070,
                                           preferredStyle: .alert)
        
        let ok = UIAlertAction(title: CommonStrings.OkButtonTitle40, style: .default) { action in }
        controller.addAction(ok)
        
        present(controller, animated: true, completion: nil)
    }

    // MARK: - Images

    func registrationImageConfig(image: UIImage) -> VIATextFieldCellImageConfig {
        var imageConfig = VIATextFieldCellImageConfig()
        imageConfig.templateImage = image
        return imageConfig
    }
    
    func reloadEmailTextField(_ textField: UITextField, at indexPath: IndexPath) {
        guard textField.text != nil else {
            return
        }
        let email = textField.text!.replacingOccurrences(of: "—", with: "--")
        textField.text = email
        tableView.reloadRows(at: [indexPath], with: .automatic)
    }
    
    func reloadPasswordTextField(_ textField: UITextField, at indexPath: IndexPath) {
        let wasSecured = textField.isSecureTextEntry
        tableView.reloadRows(at: [indexPath], with: .automatic)
        if let row = tableView.cellForRow(at: indexPath) as? VIATextFieldTableViewCell {
            row.setSecureField(asPreviouslyVisible:!wasSecured)
        }
    }
    
    /**
     * Clear 'confirm password' textfield everytime there is a change in the 'password' textfield
     * to reconfirm the updated 'password' textfield.text
     * https://jira.vitalityservicing.com/browse/VA-31717
    **/
    func clearConfirmPasswordTextField() {
        if let confirmPasswordString = self.viewModel.registrationDetail.confirmPassword {
            if !confirmPasswordString.isEmpty {
                self.viewModel.registrationDetail.confirmPassword = ""
                let section = 0
                let indexPath = IndexPath(item: RegistrationTextField.confirmPassword.rawValue, section: section)
                tableView.reloadRows(at: [indexPath], with: .automatic)
            }
        }
    }
}

extension UIGestureRecognizer {
    /**
     //ge20180228 : Returns `true` if the tap gesture was within the specified range of the attributed text of the label.
     */
    func didTapAttributedText(label: UILabel, inRange targetRange: NSRange) -> Bool {
        guard let attributedString = label.attributedText else { fatalError("Not able to fetch attributed string.") }
        
        var offsetXDivisor: CGFloat
        switch label.textAlignment {
        case .center: offsetXDivisor = 0.5
        case .right: offsetXDivisor = 1.0
        default: offsetXDivisor = 0.0
        }
        
        let layoutManager = NSLayoutManager()
        let textContainer = NSTextContainer(size: .zero)
        let textStorage = NSTextStorage(attributedString: attributedString)
        let labelSize = label.bounds.size
        
        layoutManager.addTextContainer(textContainer)
        textStorage.addLayoutManager(layoutManager)
        
        textContainer.lineFragmentPadding = 0.0
        textContainer.lineBreakMode = label.lineBreakMode
        textContainer.maximumNumberOfLines = label.numberOfLines
        textContainer.size = labelSize
        
        let locationOfTouchInLabel = self.location(in: label)
        let textBoundingBox = layoutManager.usedRect(for: textContainer)
        
        let offsetX = (labelSize.width - textBoundingBox.size.width) * offsetXDivisor - textBoundingBox.origin.x
        let offsetY = (labelSize.height - textBoundingBox.size.height) * 0.5 - textBoundingBox.origin.y
        let textContainerOffset = CGPoint(x: offsetX, y: offsetY)
        
        let locationTouchX = locationOfTouchInLabel.x - textContainerOffset.x
        let locationTouchY = locationOfTouchInLabel.y - textContainerOffset.y
        let locationOfTouch = CGPoint(x: locationTouchX, y: locationTouchY)
        
        let indexOfCharacter = layoutManager.characterIndex(for: locationOfTouch, in: textContainer, fractionOfDistanceBetweenInsertionPoints: nil)
        
        return NSLocationInRange(indexOfCharacter, targetRange)
    }
}
