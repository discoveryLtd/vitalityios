//
//  VIAResendInsurerCodeViewController.swift
//  VitalityActive
//
//  Created by Genie Mae Lorena Edera on 2/21/18.
//  Copyright © 2018 Glucode. All rights reserved.
//
// TODO:
//  api commu
//  phrase app


import UIKit

import VIAUIKit
import VitalityKit
import VIACommon

class VIAResendInsurerCodeViewController: VIATableViewController {
    public let viewModel = VIAResendInsurerCodeViewModel()
    public var regModel: VIARegistrationViewModel?

    @IBOutlet var doneButton: UIBarButtonItem!
    @IBOutlet var cancelButton: UIBarButtonItem!
    @IBOutlet var registeredEmail: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = CommonStrings.Registration.CodeFieldFootnoteResendText34
        setupNavigationbarItemTitles()
        self.configureTableView()

    }
    
    @IBAction func cancel(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }
    @IBAction func done(_ sender: AnyObject?) {
        doneButton.isEnabled = false
        showHUDOnWindow()
        tableView.endEditing(true)
        
        performResendInsurerCode()
    }
    func performResendInsurerCode(){
        viewModel.initiateInsurerCodeRequest { [weak self] error in
            self?.hideHUDFromWindow()
            self?.doneButton.isEnabled = true
            
            guard error == nil else {
                
                switch error {
                case is ResendInsurerCodeErrorStatus:
                    self?.handleResendInsurerCodeErrorStatus(error as! ResendInsurerCodeErrorStatus)
                case is BackendError:
                    self?.handleBackendErrorWithAlert(error as! BackendError, tryAgainAction: { [weak self] in
                        self?.done(nil)
                    })
                default:
                    self?.showGenericError()
                }
                return
            }
            // Successful sending of insurer code
            self?.performSegue(withIdentifier: "showFeedback", sender: nil)
        }
    }
    func handleResendInsurerCodeErrorStatus(_ error: ResendInsurerCodeErrorStatus) {
        switch error {
            case .userNotFound:
                self.showError()
            case .userAlreadyRegistered:
                self.showRedirectionToLoginMessage()
        }
    }
    func setupNavigationbarItemTitles() {
        cancelButton.title = CommonStrings.CancelButtonTitle24
        doneButton.title = CommonStrings.DoneButtonTitle53
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView.sizeHeaderViewToFit()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        let cell = tableView.cellForRow(at: IndexPath(row: 0, section: 0))
        if cell != nil { cell!.becomeFirstResponder() }
    }
    
    func configureTableView() {
        let footnote = CommonStrings.Insurercode.VitalityEmail2253
        
        self.tableView.register(VIATextFieldTableViewCell.nib(), forCellReuseIdentifier: VIATextFieldTableViewCell.defaultReuseIdentifier)
        self.tableView.estimatedRowHeight = 75
        let view = VIATableViewHeaderFooterTextView.viewFromNib(owner: self)!
        //view.setLabelText(text: LoginStrings.Insurercode.VitalityEmail999)
        view.setLabelText(text: footnote)
        view.location = .top
        tableView.tableHeaderView = view
    }
    
    // MARK: - UITableView datasource
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell    = tableView.dequeueReusableCell(withIdentifier: VIATextFieldTableViewCell.defaultReuseIdentifier, for: indexPath) as! VIATextFieldTableViewCell

        var imageConfig: VIATextFieldCellImageConfig = VIATextFieldCellImageConfig()
        imageConfig.templateImage = VIALoginRegistrationAsset.LoginAndRegistration.loginEmail.templateImage
        let headingText = CommonStrings.Insurercode.EmailAddressPlaceholder2249
        let textFieldPlaceholder = CommonStrings.Registration.EmailFieldPlaceholder27
        let secureTextEntry = false
        let keyboardType: UIKeyboardType = .emailAddress
        let errorMessage = CommonStrings.Registration.InvalidEmailFootnoteError35
        let currentFieldIsValid = self.viewModel.emailValid 
        let currentFieldIsDirty = self.viewModel.isDirty
        
        viewModel.email = viewModel.email ?? regModel?.registrationDetail.email
        self.navigationItem.rightBarButtonItem?.isEnabled = self.viewModel.emailValid
        
        cell.textFieldTextDidChange = { [unowned self] textField in

            self.viewModel.email = textField.text
            self.navigationItem.rightBarButtonItem?.isEnabled = self.viewModel.emailValid
            if(self.viewModel.emailValid) {
                UIView.performWithoutAnimation {
                    tableView.beginUpdates()
                    cell.setErrorMessage(message: nil)
                    tableView.endUpdates()
                }
            }
            guard textField.text != nil else {
                return
            }
            if textField.text!.range(of:"—") != nil {
                self.reloadEmailTextField(textField, at:indexPath)
            }
        }
        cell.textFieldDidEndEditing = { textField in
            tableView.reloadRows(at: [indexPath], with: .automatic)
        }
        
        if currentFieldIsDirty && !currentFieldIsValid {
            cell.setErrorMessage(message: errorMessage)
        } else {
            cell.setErrorMessage(message: nil)
        }
        
        cell.selectionStyle = .none
        cell.cellImageConfig = imageConfig
        cell.setTextFieldText(text: viewModel.email)
        cell.setHeadingLabelText(text: headingText)
        cell.setTextFieldPlaceholder(placeholder: textFieldPlaceholder)
        cell.setTextFieldSecureTextEntry(secureTextEntry: secureTextEntry)
        cell.setTextFieldKeyboardType(type: keyboardType)
        return cell
    }
    
    func reloadEmailTextField(_ textField: UITextField, at indexPath: IndexPath) {
        guard textField.text != nil else {
            return
        }
        let email = textField.text!.replacingOccurrences(of: "—", with: "--")
        textField.text = email
        tableView.reloadRows(at: [indexPath], with: .automatic)
    }
    
    // MARK: - UITableView delegate
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! VIATextFieldTableViewCell
        _ = cell.becomeFirstResponder()
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
    
    // MARK: - Completion

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showFeedback" {
            prepareFeedbackViewController(segue.destination)
            if (segue.destination is VIAFeedbackViewController) {
                navigationController?.setNavigationBarHidden(true, animated: false)
            }
        }
    }
    
    func prepareFeedbackViewController(_ viewcontroller: UIViewController?) {
        guard let feedbackViewController = viewcontroller as! VIAFeedbackViewController? else {
            return
        }
        
        feedbackViewController.headingText = CommonStrings.ForgotPassword.ConfirmationScreen.EmailSentMessageTitle58
        //TODO: update string
        feedbackViewController.bodyText = CommonStrings.Insurercode.EmailWithInsurerCode2250
        feedbackViewController.setButtonTitle(title: CommonStrings.DoneButtonTitle53)

        let image = VIALoginRegistrationAsset.Feedback.feedbackEmail.templateImage
        feedbackViewController.imageDetail = FeedbackImageDetail(image, UIColor.primaryColor())
    }
    
    func showError() {
        let controller = UIAlertController(title: CommonStrings.Login.EmailNotRegisteredErrorAlertTitle338,
                                           message: CommonStrings.ForgotPassword.EmailNotRegisteredAlertMessage57,
                                           preferredStyle: .alert)
        let ok = UIAlertAction(title: CommonStrings.OkButtonTitle40, style: .default) { action in }
        controller.addAction(ok)
        present(controller, animated: true, completion: nil)
    }
    func showGenericError() {
        
        let controller = UIAlertController(title: CommonStrings.GenericServiceErrorTitle268,
                                           message: CommonStrings.GenericServiceErrorMessage269,
                                           preferredStyle: .alert)
        let ok = UIAlertAction(title: CommonStrings.OkButtonTitle40, style: .default) { action in }
        controller.addAction(ok)
        present(controller, animated: true, completion: nil)
    }
    func showRedirectionToLoginMessage() {
        let controller = UIAlertController(title: CommonStrings.LoginResendInsurerCodeErrorTitle2282,
                                           message: CommonStrings.LoginResendInsurerCodeErrorMessage2281,
                                           preferredStyle: .alert)
        let ok = UIAlertAction(title: CommonStrings.OkButtonTitle40, style: .default) { action in
            self.view.window?.rootViewController?.dismiss(animated: true, completion: nil)
        }
        controller.addAction(ok)
        present(controller, animated: true, completion: nil)
    }
    func displayConnectivityError() {
        let controller = UIAlertController(title: CommonStrings.ConnectivityErrorAlertTitle44,
                                           message: CommonStrings.ConnectivityErrorAlertMessage45,
                                           preferredStyle: .alert)
        
        let tryAgain = UIAlertAction(title: CommonStrings.TryAgainButtonTitle43, style: .default) { action in
            self.done(nil)
        }
        controller.addAction(tryAgain)
        
        let cancel = UIAlertAction(title: CommonStrings.CancelButtonTitle24, style: .cancel) { action in }
        controller.addAction(cancel)
        
        present(controller, animated: true, completion: nil)
    }
}
