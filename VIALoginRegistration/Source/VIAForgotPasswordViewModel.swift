//
//  VIAForgotPasswordViewModel.swift
//  VitalityActive
//
//  Created by Wilmar van Heerden on 2016/09/26.
//  Copyright © 2016 Glucode. All rights reserved.
//

import Foundation
import VIAUtilities
import VitalityKit

public class VIAForgotPasswordViewModel: AnyObject {

    public var success: (() -> Void)?
    public var failure: ((_ error: Error) -> Void)?
    public var isDirty = false

    public var email: String? {
        didSet {
            self.isDirty = !String.isNilOrEmpty(string: self.email)
        }
    }

    public var emailValid: Bool {
        if self.email == nil {
            return false
        }
        return self.email!.isValidEmail()
    }

    public init() {
    }

    public func requestPassword() {
        guard (self.success != nil && self.failure != nil)  else {
            return
        }
        guard let email = self.email else {
            return
        }

        Wire.Member.forgotPassword(email: email) { error in
            if (error != nil) {
                self.failure!(error!)
                return
            } else {
                self.success!()
                return
            }
        }
    }
}
