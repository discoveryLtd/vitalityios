// Generated using SwiftGen, by O.Halligon — https://github.com/SwiftGen/SwiftGen

// swiftlint:disable sorted_imports
import Foundation
import UIKit

// swiftlint:disable file_length

protocol StoryboardType {
  static var storyboardName: String { get }
}

extension StoryboardType {
  static var storyboard: UIStoryboard {
    return UIStoryboard(name: self.storyboardName, bundle: Bundle(for: BundleToken.self))
  }
}

struct SceneType<T: Any> {
  let storyboard: StoryboardType.Type
  let identifier: String

  func instantiate() -> T {
    guard let controller = storyboard.storyboard.instantiateViewController(withIdentifier: identifier) as? T else {
      fatalError("ViewController '\(identifier)' is not of the expected class \(T.self).")
    }
    return controller
  }
}

struct InitialSceneType<T: Any> {
  let storyboard: StoryboardType.Type

  func instantiate() -> T {
    guard let controller = storyboard.storyboard.instantiateInitialViewController() as? T else {
      fatalError("ViewController is not of the expected class \(T.self).")
    }
    return controller
  }
}

protocol SegueType: RawRepresentable { }

extension UIViewController {
  func perform<S: SegueType>(segue: S, sender: Any? = nil) where S.RawValue == String {
    performSegue(withIdentifier: segue.rawValue, sender: sender)
  }
}

// swiftlint:disable explicit_type_interface identifier_name line_length type_body_length type_name
enum VIALoginRegistrationStoryboard {
  enum LoginRegistration: StoryboardType {
    static let storyboardName = "LoginRegistration"

    static let initialScene = InitialSceneType<VIAFirstTimeOnboardingViewController>(storyboard: LoginRegistration.self)

    static let loginViewController = SceneType<VIALoginRegistration.VIALoginViewController>(storyboard: LoginRegistration.self, identifier: "LoginViewController")

    static let viaFirstTimeOnboardingViewController = SceneType<VIAFirstTimeOnboardingViewController>(storyboard: LoginRegistration.self, identifier: "VIAFirstTimeOnboardingViewController")

    static let viaForgotPasswordNavigationViewController = SceneType<VIALoginRegistration.VIANavigationViewController>(storyboard: LoginRegistration.self, identifier: "VIAForgotPasswordNavigationViewController")

    static let viaForgotPasswordViewController = SceneType<VIALoginRegistration.VIAForgotPasswordViewController>(storyboard: LoginRegistration.self, identifier: "VIAForgotPasswordViewController")

    static let viaRegistrationNavigationController = SceneType<VIALoginRegistration.VIANavigationViewController>(storyboard: LoginRegistration.self, identifier: "VIARegistrationNavigationController")

    static let viaRegistrationViewController = SceneType<VIALoginRegistration.VIARegistrationViewController>(storyboard: LoginRegistration.self, identifier: "VIARegistrationViewController")

    static let viaResendInsurerCodeViewController = SceneType<VIALoginRegistration.VIAResendInsurerCodeViewController>(storyboard: LoginRegistration.self, identifier: "VIAResendInsurerCodeViewController")

    static let viaSplashScreenViewController = SceneType<VIALoginRegistration.VIASplashScreenViewController>(storyboard: LoginRegistration.self, identifier: "VIASplashScreenViewController")
  }
  enum LoginRestriction: StoryboardType {
    static let storyboardName = "LoginRestriction"

    static let initialScene = InitialSceneType<UINavigationController>(storyboard: LoginRestriction.self)

    static let loginRestriction = SceneType<VIALoginRegistration.LoginRestrictionViewController>(storyboard: LoginRestriction.self, identifier: "LoginRestriction")
  }
}

enum StoryboardSegue {
  enum LoginRegistration: String, SegueType {
    case showFeedback
    case showForgotPassword
    case showRegistration
    case showResendCode
    case unwindToEditEmailViewController
    case unwindToLogin
    case unwindToLoginAfterSuccessfulRegistration
  }
}
// swiftlint:enable explicit_type_interface identifier_name line_length type_body_length type_name

private final class BundleToken {}
