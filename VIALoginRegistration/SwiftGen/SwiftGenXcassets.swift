// Generated using SwiftGen, by O.Halligon — https://github.com/SwiftGen/SwiftGen

#if os(OSX)
import AppKit.NSImage
public typealias VIALoginRegistrationColor = NSColor
public typealias VIALoginRegistrationImage = NSImage
#elseif os(iOS) || os(tvOS) || os(watchOS)
import UIKit.UIImage
public typealias VIALoginRegistrationColor = UIColor
public typealias VIALoginRegistrationImage = UIImage
#endif

// swiftlint:disable file_length

public typealias VIALoginRegistrationAssetType = VIALoginRegistrationImageAsset

public struct VIALoginRegistrationImageAsset: Equatable {
	fileprivate var name: String

	public var templateImage: VIALoginRegistrationImage {
		let bundle = Bundle(for: BundleToken.self)
		#if os(iOS) || os(tvOS)
		let image = VIALoginRegistrationImage(named: name, in: bundle, compatibleWith: nil)?.withRenderingMode(.alwaysTemplate)
		#elseif os(OSX)
		let image = bundle.image(forResource: name).withRenderingMode(.alwaysTemplate)
		#elseif os(watchOS)
		let image = VIALoginRegistrationImage(named: name).withRenderingMode(.alwaysTemplate)
		#endif
		guard let result = image else { fatalError("Unable to load template image named \(name).") }
		return result
	}

	public var image: VIALoginRegistrationImage {
		let bundle = Bundle(for: BundleToken.self)
		#if os(iOS) || os(tvOS)
		let image = VIALoginRegistrationImage(named: name, in: bundle, compatibleWith: nil)
		#elseif os(OSX)
		let image = bundle.image(forResource: name)
		#elseif os(watchOS)
		let image = VIALoginRegistrationImage(named: name)
		#endif
		guard let result = image else { fatalError("Unable to load image named \(name).") }
		return result
	}

	public static func ==(lhs: VIALoginRegistrationImageAsset, rhs: VIALoginRegistrationImageAsset) -> Bool {
        return lhs.image == rhs.image
    }
}

public struct VIALoginRegistrationColorAsset {
fileprivate var name: String

#if swift(>=3.2)
@available(iOS 11.0, tvOS 11.0, watchOS 4.0, OSX 10.13, *)
var color: VIALoginRegistrationColor {
return VIALoginRegistrationColor(asset: self)
}
#endif
}

// swiftlint:disable identifier_name line_length nesting type_body_length type_name
public enum VIALoginRegistrationAsset {
  public enum LoginRestriction {
    public static let membershipInactive = VIALoginRegistrationImageAsset(name: "membershipInactive")
  }
  public enum LoginAndRegistration {
    public static let loginConfirmPassword = VIALoginRegistrationImageAsset(name: "loginConfirmPassword")
    public static let loginEmail = VIALoginRegistrationImageAsset(name: "loginEmail")
    public static let loginPassword = VIALoginRegistrationImageAsset(name: "loginPassword")
    public static let loginRegistrationCode = VIALoginRegistrationImageAsset(name: "loginRegistrationCode")
    public static let loginBirth = VIALoginRegistrationImageAsset(name: "loginBirth")
  }
  public enum PreferencesOnboarding {
    public static let setupNotifications = VIALoginRegistrationImageAsset(name: "setupNotifications")
    public static let setupEmail = VIALoginRegistrationImageAsset(name: "setupEmail")
    public static let touchIdIcon = VIALoginRegistrationImageAsset(name: "touchIdIcon")
    public static let setupRememberMe = VIALoginRegistrationImageAsset(name: "setupRememberMe")
    public static let setupTouchID = VIALoginRegistrationImageAsset(name: "setupTouchID")
    public static let setupAnalytics = VIALoginRegistrationImageAsset(name: "setupAnalytics")
    public static let setupReports = VIALoginRegistrationImageAsset(name: "setupReports")
  }
  public enum Feedback {
    public static let feedbackEmail = VIALoginRegistrationImageAsset(name: "feedbackEmail")
    public static let feedbackSubmitted = VIALoginRegistrationImageAsset(name: "feedbackSubmitted")
  }

  // swiftlint:disable trailing_comma
  public static let allColors: [VIALoginRegistrationColorAsset] = [
  ]
  public static let allImages: [VIALoginRegistrationImageAsset] = [
    LoginRestriction.membershipInactive,
    LoginAndRegistration.loginConfirmPassword,
    LoginAndRegistration.loginEmail,
    LoginAndRegistration.loginPassword,
    LoginAndRegistration.loginRegistrationCode,
    LoginAndRegistration.loginBirth,
    PreferencesOnboarding.setupNotifications,
    PreferencesOnboarding.setupEmail,
    PreferencesOnboarding.touchIdIcon,
    PreferencesOnboarding.setupRememberMe,
    PreferencesOnboarding.setupTouchID,
    PreferencesOnboarding.setupAnalytics,
    PreferencesOnboarding.setupReports,
    Feedback.feedbackEmail,
    Feedback.feedbackSubmitted,
  ]
  // swiftlint:enable trailing_comma
  @available(*, deprecated, renamed: "allImages")
  public static let allValues: [VIALoginRegistrationAssetType] = allImages
}
// swiftlint:enable identifier_name line_length nesting type_body_length type_name

extension VIALoginRegistrationImage {
@available(iOS 1.0, tvOS 1.0, watchOS 1.0, *)
@available(OSX, deprecated,
message: "This initializer is unsafe on macOS, please use the VIALoginRegistrationImageAsset.image property")
convenience init!(asset: VIALoginRegistrationAssetType) {
#if os(iOS) || os(tvOS)
let bundle = Bundle(for: BundleToken.self)
self.init(named: asset.name, in: bundle, compatibleWith: nil)
#elseif os(OSX) || os(watchOS)
self.init(named: asset.name)
#endif
}
}

extension VIALoginRegistrationColor {
#if swift(>=3.2)
@available(iOS 11.0, tvOS 11.0, watchOS 4.0, OSX 10.13, *)
convenience init!(asset: VIALoginRegistrationColorAsset) {
let bundle = Bundle(for: BundleToken.self)
#if os(iOS) || os(tvOS)
self.init(named: asset.name, in: bundle, compatibleWith: nil)
#elseif os(OSX)
self.init(named: asset.name, bundle: bundle)
#elseif os(watchOS)
self.init(named: asset.name)
#endif
}
#endif
}

private final class BundleToken {}
