#import <Foundation/Foundation.h>
#import <HealthKit/HealthKit.h>
#import <UIKit/UIKit.h>

@class VITDevice, VITGDLReading;

extern NSString * _Nonnull const kAppleHardwareDeviceBundleIdentifierPrefix;
extern NSString * _Nonnull const kNikePlusRunClubBundleIdentifierPrefix;
extern NSString * _Nonnull const kMEMLocalNotificationIdentifierKey;
extern NSString * _Nonnull const kHealthKitSyncReminderNotificationIdentifier;

typedef NS_ENUM(NSInteger, VITHealthKitPermission) {
    VITHealthKitPermissionNone = 0,
    VITHealthKitPermissionFitness,
    VITHealthKitPermissionCount
};

typedef NS_ENUM(NSInteger, VITHealthKitHelperExecutionStatus) {
    VITHealthKitHelperExecutionStatusIdle = 0,
    VITHealthKitHelperExecutionStatusRunningQueries = 1,
    VITHealthKitHelperExecutionStatusSubmittingReadings = 2
};

typedef NS_ENUM(NSInteger, VITHealthKitHelperStatus) {
    VITHealthKitHelperStatusHealthKitNotAvailable = 0,
    VITHealthKitHelperStatusLinkedToDifferentUser = 1,
    VITHealthKitHelperStatusNotLinkedYet = 2,
    VITHealthKitHelperStatusAvailable = 3
};

typedef void (^VITHealthKitHelperPermissionRequestDidCompleteBlock)(BOOL success, NSError * _Nullable error);



@interface VITHealthKitHelper : NSObject

@property (nonatomic, assign, readonly) VITHealthKitHelperExecutionStatus executionStatus;
@property (nonatomic, assign, readonly) VITHealthKitHelperStatus status;
@property (nonatomic, strong, readonly) NSPredicate * _Null_unspecified appleSourcePredicate;
@property (nonatomic, strong, readonly) HKHealthStore * _Nullable healthStore;

@property (nonatomic, strong) NSDateFormatter  * _Nullable fullFormatter;

+ (instancetype _Nonnull)sharedHelper;

- (void)requestPermission:(VITHealthKitPermission)permission withCompletionBlock:(VITHealthKitHelperPermissionRequestDidCompleteBlock _Nullable)completion; // Completion block will run on main queue.
+ (BOOL)hasPresentedHealthKitUsageOnboardingForPermission:(VITHealthKitPermission)permission;

+ (VITDevice * _Nonnull)deviceForSubmission;

- (void)setUserEntityNumber:(NSString * _Nullable)userEntityNumber; // We assign the entityNumber from [MEMUser currentUser] here, to not be reliant on the VSL version of the value. It is the same value for both WSG and VSL.
- (NSSet *_Nonnull)readTypesForFitness;

- (void)generateReadingsWithForcedResubmission:(BOOL)forcedResubmission completion:(void (^ _Nonnull)(NSArray<VITGDLReading*> *_Nullable readings))completion;
- (void)readingsSubmittedSuccessfully;
- (NSDate *_Nullable)lastQueryDate;
- (NSDate * _Nullable)lastSubmittedDate;
- (void)delink;

// For testing and internal use only
+ (NSCompoundPredicate *_Nullable)compoundPredicateWithAppleDeviceSourcesForStartDate:(NSDate *_Nullable)startDate endDate:(NSDate *_Nullable)endDate;

- (HKSampleQuery *_Nonnull)forcedResubmissionWorkoutsQueryWithCompletion:(void (^ _Nonnull)(NSArray *_Nullable readings))completion;
- (HKAnchoredObjectQuery *_Nonnull)anchoredWorkoutsQueryWithCompletion:(void (^ _Nonnull)(NSArray *_Nullable readings))completion;

@end
