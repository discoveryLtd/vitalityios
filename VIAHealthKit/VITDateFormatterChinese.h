//
//  VITDateFormatterChineese.h
//  VitalityCore
//
//  Created by Discovery on 2014/11/05.
//  Copyright (c) 2014 Glucode. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VITDateFormatterLocalized.h"

@interface VITDateFormatterChinese : NSObject <VITDateFormatterLocalized>

@end

