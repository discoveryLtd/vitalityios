#import "VITDevice.h"

@implementation VITDevice

- (instancetype)initWithWithDeviceID:(NSString *)deviceID manufacturer:(NSString *)manufacturer make:(NSString *)make model:(NSString *)model
{
    self = [super init];
    if (self) {
        _deviceID = deviceID;
        _manufacturer = manufacturer;
        _make = make;
        _model = model;
    }
    return self;
}

@end
