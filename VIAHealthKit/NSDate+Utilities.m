#import "NSDate+Utilities.h"


@implementation NSDate (Utilities)

+ (NSInteger)indexOfDateClosestToDate:(NSDate *)compareDate inDatesArray:(NSArray *)dates
{
	NSInteger closestIndex = NSNotFound;
	if (dates.count == 0) {
		return closestIndex;
	}
	
	NSTimeInterval closestTimeDifference = fabs([compareDate timeIntervalSinceDate:[dates objectAtIndex:0]]);
	closestIndex = 0;
	NSInteger indexCount = 0;
	for (NSDate *date in dates) {
		NSTimeInterval timeDifference = fabs([compareDate timeIntervalSinceDate:date]);
		if (timeDifference < closestTimeDifference) {
			closestIndex = indexCount;
			closestTimeDifference = timeDifference;
		}
		indexCount++;
	}
	
	return closestIndex;
}

+ (NSDateFormatter *)shortStyleFormatter
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
	[formatter setDateStyle:NSDateFormatterShortStyle];
    return formatter;
}

+ (NSDateFormatter *)formatter
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZ"];
    return formatter;
}

+ (NSDateFormatter *)formatterWithZone
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZ"];
    return formatter;
}

+ (NSDateFormatter *)formatterWithZoneNoTime
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-ddZ"];
    return formatter;
}

+ (NSDateFormatter *)outputFormatter
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    return formatter;
}

+ (NSDateFormatter *)weekdayDateMonthYearHoursAndSecondsFormatter
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"EEEE, dd MMM yyyy HH:mm"];
    return formatter;
}

+ (NSDateFormatter *)outputFormatterPlusTime
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
    return formatter;
}

+ (NSDateFormatter *)outputFormatterPlusTimeZone
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZ"];
    return formatter;
}

+ (NSDateFormatter *)outputFormatterWithTimeZoneAbbreviation
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_ZA"];
    formatter.dateFormat = @"EEE MMM d HH:mm:ss z yyyy";
    return formatter;
}

+ (NSDateFormatter *)weekdayDateMonthYearFormatter {
    NSDateFormatter *formatter = [NSDateFormatter new];
    formatter.dateFormat = @"EEEE, d MMMM yyyy";
    return formatter;
}

+ (NSString *)sanitizeTimeZoneString:(NSString *)dateString
{
    if (!dateString) {
        return nil;
    }
    
    NSError *error = NULL;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"(\\+\\d\\d):(\\d\\d)"
                                                                           options:NSRegularExpressionCaseInsensitive
                                                                             error:&error];
    
    if (error) {
        return nil;
    }
    
    NSString *modifiedString = [regex stringByReplacingMatchesInString:dateString
                                                               options:0
                                                                 range:NSMakeRange(0, [dateString length])
                                                          withTemplate:@"$1$2"];
    return modifiedString;
}

+ (NSDateFormatter *)monthYearFormatter
{
	NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
	[formatter setDateFormat:@"MMMM yyyy"];
	[formatter setCalendar:[NSCalendar currentCalendar]];
	return formatter;
}

+ (NSDateFormatter *)yearMonthFormatter
{
	NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
	[formatter setDateFormat:@"yyyy-MM"];
	[formatter setCalendar:[NSCalendar currentCalendar]];
	return formatter;
}

+ (NSDateFormatter *)yearFormatter
{
	NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
	[formatter setDateFormat:@"yyyy"];
	[formatter setCalendar:[NSCalendar currentCalendar]];
	return formatter;
}

+ (NSDateFormatter *)monthFormatter
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MMMM"];
    [formatter setCalendar:[NSCalendar currentCalendar]];
    return formatter;
}

+ (NSDateFormatter *)monthShortFormatter
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MMM"];
    [formatter setCalendar:[NSCalendar currentCalendar]];
    return formatter;
}


+ (NSDateFormatter *)dayFormatter
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd"];
    [formatter setCalendar:[NSCalendar currentCalendar]];
    return formatter;
}

+ (NSDateFormatter *)timelineDateFormatter
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"EEEE, dd MMMM yyyy"];
    [formatter setCalendar:[NSCalendar currentCalendar]];
    return formatter;
}

+ (NSDateFormatter *)weekDayDateShortMonthYearFormatter {
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setDateFormat:@"EE, d MMM, yyyy"];
    return formatter;
}

- (NSString *)yearMonthString
{
	NSString *dateString = [[NSDate yearMonthFormatter] stringFromDate:self];
	return dateString;
}

- (NSString *)yearMonthDayString
{
    NSString *dateString = [[NSDate yearMonthDayAdjacentFormatter] stringFromDate:self];
    return dateString;
}

- (NSString *)GMTPlusTwoTimeString
{
    NSDateFormatter *formatter = [NSDate timeAdjacentFormatter];
    [formatter setTimeZone:[NSDate GMTPlus2TimeZone]];
    NSString *dateString = [formatter stringFromDate:self];
    return dateString;
}

+ (NSDateFormatter *)yearMonthDayAdjacentFormatter
{
	NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
	[formatter setDateFormat:@"yyyyMMdd"];
	[formatter setCalendar:[NSCalendar currentCalendar]];
	return formatter;
}

+ (NSTimeZone *)GMTPlus2TimeZone
{
    NSInteger GMTPlusTwoSeconds = 3600 * 2;
    return [NSTimeZone timeZoneForSecondsFromGMT:GMTPlusTwoSeconds];
}

+ (NSDateFormatter *)timeAdjacentFormatter
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"HH:mm"];
    [formatter setCalendar:[NSCalendar currentCalendar]];
    return formatter;
}

- (NSString *)dayMonthAbbreviation
{
	NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
	[formatter setDateFormat:@"dd MMM"];
	NSString *dateString = [formatter stringFromDate:self];
	return dateString;
}

- (NSString *)dayMonthYearAbbreviation
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd MMM yyyy"];
    NSString *dateString = [formatter stringFromDate:self];
    return dateString;
}

- (NSString *)dayMonthYearAbbreviationWithFormatter:(NSDateFormatter *)formatter
{
    [formatter setDateFormat:@"dd MMM yyyy"];
    return [formatter stringFromDate:self];
}

+ (NSDateFormatter *)dayMonthYearAbbreviationWithFormatter {
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setDateFormat:@"dd MMM yyyy"];
    return formatter;
}

- (NSString *)timelineDateString
{
    NSString *dateString = [[NSDate timelineDateFormatter] stringFromDate:self];
    return dateString;
}

- (NSString *)dayMonthYear
{
    NSString *dateString = [[NSDate dayMonthYearDateFormatter] stringFromDate:self];
    return dateString;
}

+ (NSDateFormatter *)dayAbbreviatedMonthYearDateFormatter
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd MMM yyyy"];
    [formatter setCalendar:[NSCalendar currentCalendar]];
    return formatter;
}

+ (NSDateFormatter *)dayMonthYearDateFormatter
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd MMMM yyyy"];
    [formatter setCalendar:[NSCalendar currentCalendar]];
    return formatter;
}

+ (NSDateFormatter *)nonZeroPaddedDayMonthYearDateFormatter
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"d MMMM yyyy"];
    [formatter setCalendar:[NSCalendar currentCalendar]];
    return formatter;
}

+ (NSDateComponents *)yearDateComponentForYearCountSinceNow:(NSInteger)count
{
	NSDateComponents *yearComponent = [[NSCalendar currentCalendar] components:NSCalendarUnitYear fromDate:[NSDate date]];
	[yearComponent setYear:[yearComponent year] - count];
	return yearComponent;
}

+ (NSString *)yearDateTitleForYearCountSinceNow:(NSInteger)count
{
	NSDateComponents *yearComponents = [NSDate yearDateComponentForYearCountSinceNow:count];
	NSString *dateString = [NSString stringWithFormat:@"%ld",(long)[yearComponents year]];
	return dateString;
}

+ (NSString *)monthStringForIndex:(NSInteger)monthIndex
{
	NSDateComponents *components = [[NSDateComponents alloc] init];
	[components setMonth:monthIndex];
	NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
	[formatter setDateFormat:@"MMM"];
	NSDate *date = [components date];
	return [formatter stringFromDate:date];
}

+ (NSDate *)dateWithYearsSinceNow:(NSInteger)years
{
	NSDate *date = [NSDate dateWithYears:years sinceDate:[NSDate date]];
	return date;
}

+ (NSDate *)dateWithMonthsSinceNow:(NSInteger)months
{
    NSDate *date = [NSDate dateWithMonths:months sinceDate:[NSDate date]];
    return date;
}


+ (BOOL)nonOverlappingStartDate:(NSDate **)startDate endDate:(NSDate **)endDate forPeriodFromDate:(NSDate *)fromDate toDate:(NSDate *)toDate
{
    // INSIDE EXISTING DATE RANGE
    BOOL nonOverlappingPeriodFound = YES;
    NSDate *theStartDate = *startDate;
    NSDate *theEndDate = *endDate;

    if (([fromDate compare:theStartDate] == NSOrderedAscending || [fromDate compare:theStartDate] == NSOrderedSame) && ([toDate compare:theEndDate] == NSOrderedDescending || [toDate compare:theEndDate] == NSOrderedSame)) {

        nonOverlappingPeriodFound = NO;
        theStartDate = [NSDate dateWithFirstDayOfMonth:[NSDate date]];
        theEndDate = [NSDate dateWithFirstDayOfMonth:[NSDate date]];
    } else {

         // startDate < fromDate && endDate >= fromDate
        if ([fromDate compare:theStartDate] == NSOrderedDescending && ([fromDate compare:theEndDate] == NSOrderedAscending || [fromDate compare:theEndDate] == NSOrderedSame)) {

            theEndDate = fromDate;
        } else {
//            fromDate <= startDate && toDate > startDate
            if (([fromDate compare:theStartDate] == NSOrderedAscending || [fromDate compare:theStartDate] == NSOrderedSame) && [toDate compare:theStartDate] == NSOrderedDescending) {
                theStartDate = toDate;
            }
        }
    }
    *startDate = theStartDate;
    *endDate = theEndDate;
    return nonOverlappingPeriodFound;
}

+ (NSDate *)dateWithFirstDayOfMonth:(NSDate *)date
{
    if (!date) {
        return nil;
    }

	NSDateComponents *yearComponents = [[NSCalendar currentCalendar] components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:date];
	[yearComponents setDay:1];
	NSDate *returnDate = [[NSCalendar currentCalendar] dateFromComponents:yearComponents];
	return returnDate;
}

+ (NSDate *)dateWithFirstDayOfNextMonth:(NSDate *)date
{
    if (!date) {
        return nil;
    }

	NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:date];
    [components setMonth:components.month+1];
    [components setDay:1];
	NSDate *returnDate = [[NSCalendar currentCalendar] dateFromComponents:components];
	return returnDate;
}

+ (NSDate *)dateFirstOfMonthWithYearsSinceNow:(NSInteger)years
{
	NSDateComponents *yearComponents = [[NSCalendar currentCalendar] components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:[NSDate date]];
	[yearComponents setYear:[yearComponents year] + years];
	[yearComponents setDay:1];
	NSDate *returnDate = [[NSCalendar currentCalendar] dateFromComponents:yearComponents];
	return returnDate;
}

+ (NSInteger)numberOfMonthsRemainingInYearFromDate:(NSDate *)givenDate
{
	NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitMonth fromDate:givenDate];
	return 12 - ([components month]-1);
}

+ (NSInteger)numberOfMonthsPastForCurrentYear
{
	NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitMonth fromDate:[NSDate date]];
	return [components month] - 1;
}

+ (NSInteger )numberOfYearsSinceDate:(NSDate *)startDate
{
    NSDate *currentDate = [NSDate date];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:NSCalendarUnitYear fromDate:currentDate];
    NSInteger currentYear = [components year];

    components = [calendar components:NSCalendarUnitYear fromDate:startDate];
    NSInteger startYear = [components year];

    NSInteger count = currentYear - startYear + 1;
    return count;
}

+ (NSDate *)dateWithYears:(NSInteger)years sinceDate:(NSDate *)date {
	NSDateComponents *yearComponents = [[NSCalendar currentCalendar] components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:date];
	[yearComponents setYear:[yearComponents year] + years];
	NSDate *returnDate = [[NSCalendar currentCalendar] dateFromComponents:yearComponents];
	return returnDate;
}

+ (NSDate *)dateWithMonths:(NSInteger)months sinceDate:(NSDate *)date
{
    NSDateComponents *monthComponents = [[NSCalendar currentCalendar] components:NSCalendarUnitYear | NSCalendarUnitMonth fromDate:date];
    [monthComponents setMonth:[monthComponents month] + months];
    NSDate *returnDate = [[NSCalendar currentCalendar] dateFromComponents:monthComponents];
    return returnDate;
}

+ (NSDate *)dateWithSouthAfricanIDNumber:(NSString *)idNumber
{
    if (idNumber.length < 6) {
        return nil;
    }
	NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
	[formatter setDateFormat:@"yyMMdd"];
	NSDate *date = [formatter dateFromString:[idNumber substringToIndex:6]];
    NSComparisonResult comparison = [date compare:[NSDate date]];
    if (comparison == NSOrderedDescending || comparison == NSOrderedSame) {
        [formatter setDateFormat:@"yyyyMMdd"];
        date = [formatter dateFromString:[[NSString stringWithFormat:@"19%@",idNumber] substringToIndex:8]];
    }
    formatter = nil;
	return date;
}

- (NSDate *)dateExcludingTime
{
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:self];
    return [[NSCalendar currentCalendar] dateFromComponents:components];
}

+ (NSDate *)dateForLastDayOfMonthWithDate:(NSDate *)date
{
	NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitMonth | NSCalendarUnitYear | NSCalendarUnitDay fromDate:date];
	[components setMonth:[components month]+1];
	[components setDay:0];
	NSDate *returnDate = [[NSCalendar currentCalendar] dateFromComponents:components];
	return returnDate;
}

+ (BOOL)dateIsToday:(NSDate *)date
{
    if (date) {
        NSCalendar *calender = [NSCalendar currentCalendar];
        NSDateComponents *components = [calender components:(NSCalendarUnitEra|NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay) fromDate:[NSDate date]];
        NSDate *today = [calender dateFromComponents:components];
        components = [calender components:(NSCalendarUnitEra|NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay) fromDate:date];
        NSDate *otherDate = [calender dateFromComponents:components];
        return [today isEqualToDate:otherDate];
    } else {
        return NO;
    }
}

+ (BOOL)dateIsTomorrow:(NSDate *)date
{
    if (date) {
        NSDate *formattedDate = [self configureDateWithYearMonthDayComponents:date];
        NSDate *today = [self configureDateWithYearMonthDayComponents:[NSDate date]];
        NSDate *yesterday = [self dateWithNumberOfDays:+1 fromDate:today];
        return [formattedDate isEqualToDate:yesterday];
    } else {
        return NO;
    }
}

+ (BOOL)dateIsYesterday:(NSDate *)date
{
    if (date) {
        NSDate *formattedDate = [self configureDateWithYearMonthDayComponents:date];
        NSDate *today = [self configureDateWithYearMonthDayComponents:[NSDate date]];
        NSDate *yesterday = [self dateWithNumberOfDays:-1 fromDate:today];
        return [formattedDate isEqualToDate:yesterday];
    } else {
        return NO;
    }
}

+ (NSInteger)ageFromDateOfBirth:(NSDate *)dateOfBirth
{
    if (!dateOfBirth) {
        return 999;
    }

    NSDate *now = [NSDate date];
    NSDateComponents *ageComponents = [[NSCalendar currentCalendar]
                                       components:NSCalendarUnitYear
                                       fromDate:dateOfBirth
                                       toDate:now
                                       options:0];
    return [ageComponents year];
}

+ (NSDate *)threeYearsBackJanuaryDate {
	NSDateComponents *yearComponent = [[NSCalendar currentCalendar] components:NSCalendarUnitYear fromDate:[NSDate date]];
	[yearComponent setYear:[yearComponent year] - 3];
	NSDateFormatter *dateFormatter = [NSDate outputFormatter];
	[dateFormatter setCalendar:[NSCalendar currentCalendar]];
	NSDate *date = [dateFormatter dateFromString:[NSString stringWithFormat:@"%ld-01-01",(long)[yearComponent year]]];
	return date;
}

+ (NSDate *)dateWithNumberOfMonthsSinceToday:(NSInteger)months {
	return [NSDate dateWithNumberOfMonths:months sinceDate:[NSDate date]];
}

+ (NSDate *)dateWithNumberOfYearsSinceToday:(NSInteger)years {
	return [NSDate dateWithNumberOfYears:years sinceDate:[NSDate date]];
}

+ (NSDate *)dateWithNumberOfMonths:(NSInteger)months sinceDate:(NSDate *)date {
	NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *dateComponents = [calendar components:(NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear) fromDate:date];
    [dateComponents setMonth:[dateComponents month] + months];
    NSDate *producedDate = [calendar dateFromComponents:dateComponents];
	return producedDate;
}

+ (NSDate *)dateWithNumberOfYears:(NSInteger)years sinceDate:(NSDate *)date {
	NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *dateComponents = [calendar components:(NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear) fromDate:date];
    [dateComponents setYear:[dateComponents year] + years];
    NSDate *producedDate = [calendar dateFromComponents:dateComponents];
	return producedDate;
}

+ (NSDateFormatter *)formatterZoneExcluded {
	NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
    return formatter;
}

+ (NSDateFormatter *)DMYFormatter
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd/MM/yyyy"];
    return formatter;
}

- (NSString *)dayString {
	NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
	[formatter setDateFormat:@"dd"];
	NSString *dateString = [formatter stringFromDate:self];
	return dateString;
}

+ (NSDateFormatter *)formatterNoMilliSeconds
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZ"];
    return formatter;
}

- (NSString *)yearString {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
	[formatter setDateFormat:@"yyyy"];
	[formatter setCalendar:[NSCalendar currentCalendar]];
    return [formatter stringFromDate:self];
}

- (NSString *)healthRecordDateString {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"ccc dd MMM yyyy"];
	[formatter setCalendar:[NSCalendar currentCalendar]];
    NSString *dateString = [formatter stringFromDate:self];
	return dateString;
}

+ (NSDateFormatter *)formatterHealthRecordDate
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"cccc, dd MMM yyyy"];
    return formatter;
}

+ (NSDateFormatter *)shortTimeFormatter
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    [formatter setDateStyle:NSDateFormatterNoStyle];
    return formatter;
}

+ (NSDateFormatter *)outputFormatterWithZone
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-ddZ"];
    return formatter;
}

+ (NSDate *)dateTwoWeeksAgoFromToday {
    NSDate *currentDate = [NSDate date];
    NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
    [dateComponents setDay:-14];
    NSDate *fourteenDaysAgo = [[NSCalendar currentCalendar] dateByAddingComponents:dateComponents toDate:currentDate options:0];
    return fourteenDaysAgo;
}

+ (NSInteger)numberOfHoursBetweenStartDate:(NSDate *)startDate andEndDate:(NSDate *)endDate
{
    return [[[NSCalendar currentCalendar] components:NSCalendarUnitHour fromDate:startDate toDate:endDate options:0] hour];
}

+ (NSDateComponents *)numberOfWeeksAndDaysBetweenStartDate:(NSDate *)startDate andEndDate:(NSDate *)endDate
{
    return [[NSCalendar currentCalendar] components:NSCalendarUnitWeekOfYear | NSCalendarUnitDay fromDate:startDate toDate:endDate options:0];
}

+ (NSDate *)configureDateWithYearMonthDayComponents:(NSDate *)date
{
    NSDateComponents *dateComponents = [[NSCalendar currentCalendar] components:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitWeekday) fromDate:date];
    return [[NSCalendar currentCalendar] dateFromComponents:dateComponents];
}

+ (NSDate *)dateWithNumberOfDays:(int)numberOfDays fromDate:(NSDate *)date
{
    NSDateComponents *components = [[NSDateComponents alloc] init];
    components.day = numberOfDays;
    return [[NSCalendar currentCalendar] dateByAddingComponents:components toDate:date options:0];
}

+ (NSString *)formatedDateString:(NSDate *)date withFormat:(NSDateFormatter *)dateFormat
{
    [NSException raise:@"Review!" format:@"Old code"];
    return @"";
//    if ([NSDate dateIsToday:date]) {
//        return [NSString stringWithFormat:@"%@ at %@",MEMCommonLocalizedString(@"MEM_CALENDAR_TODAY_STRING", @"Mem calendar today string"),[dateFormat stringFromDate:date]];
//    } else if ([NSDate dateIsYesterday:date]) {
//        return [NSString stringWithFormat:@"%@ at %@",MEMCommonLocalizedString(@"MEM_CALENDAR_YESTERDAY_STRING", @"Mem calendar yesterday string"),[dateFormat stringFromDate:date]];
//    }
//    return [dateFormat stringFromDate:date];
}

+ (NSDateFormatter *)vit_timeOnlyFormatter
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"HH:mm"];
    return formatter;
}

+ (NSDateFormatter *)vit_fullFormatter
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"EEE, d MMM yyyy"];
    return formatter;
}

+ (NSDateFormatter *)vit_naturalDateFormatterWithDashes
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"d-MMM-yyyy"];
    return formatter;
}

+ (NSDateFormatter *)vit_naturalDateFormatter
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"d MMM yyyy"];
    return formatter;
}


+ (NSDateFormatter *)vit_dayWithDateFormatter
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"EEE, dd-MM-yyyy"];
    return formatter;
}

+ (NSDateFormatter *)vit_formatter
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setLocale:[NSDate enUSPOSIXLocale]];
    [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZ"];
    return formatter;
}

+ (NSDateFormatter *)vit_formatterWithZone
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setLocale:[NSDate enUSPOSIXLocale]];
    [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZ"];
    return formatter;
}

+ (NSDateFormatter *)vit_formatterWithTime
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setLocale:[NSDate enUSPOSIXLocale]];
    [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
    return formatter;
}

+ (NSDateFormatter *)vit_formatterWithZoneAlternative
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-ddZZZZZ"];
    return formatter;
}

+ (NSDateFormatter *)vit_outputFormatter
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    return formatter;
}

+ (NSDateFormatter *)vit_reverseOutputFormatter
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd-MM-yyyy"];
    return formatter;
}

+ (NSDateFormatter *)vit_outputFormatterPlusTime
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setLocale:[NSDate enUSPOSIXLocale]];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    return formatter;
}

+ (NSDateFormatter *)vit_outputFormatterPlusMillisecondsPlusTimeZone
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setLocale:[NSDate enUSPOSIXLocale]];
    [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ"];
    return formatter;
}

+ (NSDateFormatter *)vit_monthYearFormatter
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MMMM yyyy"];
    [formatter setCalendar:NSCalendar.currentCalendar];
    return formatter;
}

+ (NSDateFormatter *)vit_dayMonthYearFormatter
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd MMMM yyyy"];
    [formatter setCalendar:NSCalendar.currentCalendar];
    return formatter;
}

+ (NSDateFormatter *)vit_yearFormatter
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy"];
    [formatter setCalendar:NSCalendar.currentCalendar];
    return formatter;
}

+ (NSDateFormatter *)vit_yearMonthFormatter
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM"];
    [formatter setCalendar:NSCalendar.currentCalendar];
    return formatter;
}

+ (NSDateFormatter *)vit_yearMonthDayAdjacentFormatter
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyyMMdd"];
    [formatter setCalendar:NSCalendar.currentCalendar];
    return formatter;
}

#pragma mark - Helper methods

+ (NSInteger)vit_ageFromDateOfBirth:(NSDate *)dateOfBirth
{
    if (!dateOfBirth) {
        return 999;
    }
    
    NSDate *now = [NSDate date];
    NSDateComponents *ageComponents = [NSCalendar.currentCalendar
                                       components:NSCalendarUnitYear
                                       fromDate:dateOfBirth
                                       toDate:now
                                       options:0];
    return [ageComponents year];
}

+ (NSDate *)vit_removeTimeStampFromDate:(NSDate *)date
{
    if (!date) {
        return nil;
    }
    
    NSCalendar *calendar = NSCalendar.currentCalendar;
    NSDateComponents *components = [calendar components:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond) fromDate:date];
    components.hour = 0;
    components.minute = 0;
    components.second = 0;
    date = [calendar dateFromComponents:components];
    return date;
}

+ (NSDate *)vit_removeDateFromTimeStamp:(NSDate *)date
{
    if (!date) {
        return nil;
    }
    
    NSCalendar *calendar = NSCalendar.currentCalendar;
    NSDateComponents *components = [calendar components:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond) fromDate:date];
    components.year = 2000;
    components.month = 1;
    components.day = 1;
    date = [calendar dateFromComponents:components];
    return date;
}

+ (BOOL)vit_time:(NSDate *)time isBetweenStartTime:(NSDate *)startTime andEndTime:(NSDate *)endTime
{
    time = [NSDate vit_removeDateFromTimeStamp:time];
    startTime = [NSDate vit_removeDateFromTimeStamp:startTime];
    endTime = [NSDate vit_removeDateFromTimeStamp:endTime];
    
    if ([time compare:startTime] == NSOrderedAscending)
        return NO;
    
    if ([time compare:endTime] == NSOrderedDescending)
        return NO;
    
    return YES;
}

+ (BOOL)vit_date:(NSDate *)date isBetweenDate:(NSDate *)beginDate andDate:(NSDate *)endDate
{
    if ([date compare:beginDate] == NSOrderedAscending)
        return NO;
    
    if ([date compare:endDate] == NSOrderedDescending)
        return NO;
    
    return YES;
}

+ (NSString *)vit_sanitizeTimeZoneString:(NSString *)dateString
{
    if (!dateString) {
        return nil;
    }
    
    NSError *error = NULL;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"(\\+\\d\\d):(\\d\\d)"
                                                                           options:NSRegularExpressionCaseInsensitive
                                                                             error:&error];
    
    if (error) {
        return nil;
    }
    
    NSString *modifiedString = [regex stringByReplacingMatchesInString:dateString
                                                               options:0
                                                                 range:NSMakeRange(0, [dateString length])
                                                          withTemplate:@"$1$2"];
    return modifiedString;
}

+ (NSLocale *)enUSPOSIXLocale {
    NSLocale *enUSPOSIXLocale = [NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"];
    return enUSPOSIXLocale;
}


@end
