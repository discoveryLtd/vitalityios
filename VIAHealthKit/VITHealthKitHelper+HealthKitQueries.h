#import <HealthKit/HealthKit.h>
#import "VITHealthKitHelper.h"

@interface VITHealthKitHelper (HealthKitQueries)

+ (HKActivitySummaryQuery * _Nonnull)latestActivitySummaryQueryWithCompletion:(void (^ _Nullable)(HKActivitySummary * _Nullable))completion;
+ (HKBiologicalSex)latestBiologicalSex;
+ (HKSampleQuery *_Nullable)latestBodyMassReadingQueryForEndDate:(NSDate *_Nullable)endDate completion:(void (^ _Nullable)(HKQuantitySample *_Nullable))completion;
+ (HKStatisticsQuery *_Nonnull)walkingRunningDistanceForStartDate:(NSDate *_Nonnull)startDate endDate:(NSDate *_Nonnull)endDate completion:(void (^ _Nullable)(HKQuantity *_Nullable))completion;
+ (HKStatisticsQuery *_Nonnull)activeEnergyBurnedForStartDate:(NSDate *_Nonnull)startDate endDate:(NSDate *_Nonnull)endDate completion:(void (^ _Nullable)(HKQuantity *_Nullable))completion;
+ (NSPredicate *_Nullable)newAppleDeviceSourcePredicate;
+ (HKAnchoredObjectQuery *_Nonnull)heartRateSamplesQueryForWorkout:(HKWorkout *_Nonnull)workout completion:(void (^ _Nullable)(NSArray<__kindof HKSample *> *_Nullable sampleObjects, NSArray<HKDeletedObject *> *_Nullable deletedObjects, NSError *_Nullable error))completion;

@end
