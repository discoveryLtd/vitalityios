#import <VIAHealthKit/VIAHealthKit.h>

@interface VITHealthKitHelper (Notifications)

- (void)scheduleLocalReminderNotificationWithMessage:(NSString *)message; //Schedules a local notification for each Friday 07:00 to remind the user to sync Health data.
+ (void)cancelLocalNotification;

@end
