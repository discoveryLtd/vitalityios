#import "VITHealthKitHelper+DateHelpers.h"

@implementation VITHealthKitHelper (DateHelpers)

+ (NSDate *)now
{
    return [NSDate date];
}

+ (NSDate *)twoWeeksAgo
{
    NSCalendar *calendar = NSCalendar.currentCalendar;
    NSDate *now = [self now];
    NSDateComponents *components = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay fromDate:now];
    components.day -= 14;
    NSDate *twoWeeksAgo = [calendar dateFromComponents:components];
    return twoWeeksAgo;
}

+ (NSDate *)todayAtTwelveMidnight
{
    NSCalendar *calendar = NSCalendar.currentCalendar;
    NSDate *now = [self now];
    NSDateComponents *components = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay|NSCalendarUnitHour|NSCalendarUnitMinute|NSCalendarUnitSecond fromDate:now];
    components.hour = 0;
    components.minute = 0;
    components.second = 0;
    NSDate *todayAtMidnight = [calendar dateFromComponents:components];
    return todayAtMidnight;
}

+ (NSDate *)todayAtElevenFiftyNinePM
{
    NSCalendar *calendar = NSCalendar.currentCalendar;
    NSDate *now = [self now];
    NSDateComponents *components = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay|NSCalendarUnitHour|NSCalendarUnitMinute|NSCalendarUnitSecond fromDate:now];
    components.hour = 23;
    components.minute = 59;
    components.second = 59;
    NSDate *todayAtElevenFiftyNinePm = [calendar dateFromComponents:components];
    return todayAtElevenFiftyNinePm;
}

+ (NSDate *)yesterday
{
    NSCalendar *calendar = NSCalendar.currentCalendar;
    NSDate *now = [self now];
    NSDateComponents *components = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay fromDate:now];
    components.day -= 1;
    NSDate *yesterday = [calendar dateFromComponents:components];
    return yesterday;
}

+ (NSDate *)dayBeforeYesterday
{
    NSCalendar *calendar = NSCalendar.currentCalendar;
    NSDate *startDate = [self yesterday];
    NSDate *dayBeforeYesterday = [calendar dateByAddingUnit:NSCalendarUnitDay value:-1 toDate:startDate options:0];
    return dayBeforeYesterday;
}

+ (NSDate *)thisFridaySevenOClock
{
    return [self dateForFridaySevenOClock:YES];
}

+ (NSDate *)nextFridaySevenOClock
{
    return [self dateForFridaySevenOClock:NO];
}

+ (NSDate *)dateForFridaySevenOClock:(BOOL)thisFriday
{
    NSDate *now = [self now];
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    
    //Get Friday's date
    NSDateComponents *weekdayComponents = [calendar components:NSCalendarUnitWeekday fromDate:now];
    NSInteger weekdayToday = [weekdayComponents weekday];
    
    NSDate *friday;
    if (weekdayToday == 6) { //Today is Friday
        NSDateComponents *componentsToSubtract = [[NSDateComponents alloc] init];
        [componentsToSubtract setDay:weekdayToday+1]; //Next Friday
        friday = [calendar dateByAddingComponents:componentsToSubtract toDate:now options:0];
    }else{
        NSDateComponents *componentsToSubtract = [[NSDateComponents alloc] init];
        if (!thisFriday) {
            [componentsToSubtract setDay:((13 - weekdayToday) % 7) + 7];
        }else{
            [componentsToSubtract setDay:(13 - weekdayToday) % 7];
        }
        
        friday = [calendar dateByAddingComponents:componentsToSubtract toDate:now options:0];
        
    }
    
    //Set time to 07:00t
    NSDateComponents *components = [calendar components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitWeekOfMonth | NSCalendarUnitWeekday fromDate:friday];
    [components setHour:7];
    friday = [calendar dateFromComponents:components];
    return friday;
}

@end
