#import "VITGDLReading.h"
#import "NSDate+Utilities.h"
#import "HKWorkout+HKWorkoutActivityTypeStringRepresentation.h"
#import "VITVitalityHeartRateCalculator.h"

@implementation VITGDLReading

+ (VITGDLReading *)readingForWorkout:(HKWorkout *)workout statisticsCollection:(HKStatisticsCollection *)collection
{
    if (!workout) {
        return nil;
    }

    VITGDLReading *reading = [VITGDLReading new];
    reading.readingClassification = VITGDLReadingClassificationExercise;
    reading.startTime = workout.startDate;
    reading.endTime = workout.endDate;
    reading.duration = @(ceil(workout.duration));

    HKQuantity *energy = workout.totalEnergyBurned;
    double calories = [energy doubleValueForUnit:[HKUnit kilocalorieUnit]];
    if (calories > 0) {
        reading.energyExpenditureValue = @(ceil(calories));
        reading.energyExpenditureUnitOfMeasurement = @"KILO_CALORIES";
    }

    HKQuantity *distance = workout.totalDistance;
    double meters = [distance doubleValueForUnit:[HKUnit meterUnit]];
    if (meters > 0) {
        reading.distance = @(round(meters));
        reading.distanceUnitOfMeasurement = @"METERS";
    }

    reading.identifier = workout.UUID.UUIDString;
    reading.readingDescription = [workout activityTypeIdentifier];

    if (collection.statistics.count > 0) {
        reading.heartRateMinimum = @([VITVitalityHeartRateCalculator minimumHeartRateWithStatisticsCollection:collection]);
        reading.heartRateMaximum = @([VITVitalityHeartRateCalculator maximumHeartRateWithStatisticsCollection:collection]);
        reading.heartRateAverage = @([VITVitalityHeartRateCalculator averageHeartRateWithStatisticsCollection:collection]);
    }

    return reading;
}

+ (VITGDLReading *)readingForStepsWithStepsResult:(HKStatistics *)stepsResult distanceQuantity:(HKQuantity *)distanceQuantity energyQuantity:(HKQuantity *)energyQuantity entityNumber:(NSString *)entityNumber
{
    if (!stepsResult || !stepsResult.sumQuantity) {
        return nil;
    }
    
    VITGDLReading *reading = [VITGDLReading new];
    NSString *identifier = [NSString stringWithFormat:@"%@.%@.%@", entityNumber, stepsResult.startDate, stepsResult.endDate];
    identifier = [identifier stringByReplacingOccurrencesOfString:@" +0000" withString:@""];
    identifier = [identifier stringByReplacingOccurrencesOfString:@" 22:00:00" withString:@""];
    reading.identifier = identifier;
    reading.readingClassification = VITGDLReadingClassificationSteps;
    reading.startTime = stepsResult.startDate;
    reading.endTime = stepsResult.endDate;
    
    HKQuantity *quantity = stepsResult.sumQuantity;
    double value = [quantity doubleValueForUnit:[HKUnit countUnit]];
    reading.totalSteps = @(round(value));
 
    return reading;
}

#pragma mark - Debug

- (NSString *)debugDescription
{
    return [NSString stringWithFormat:@"Reading details:\n%@: %@\n%@: %@\n%@: %@\n%@: %@\n%@: %@\n%@: %@\n%@: %@\n%@: %@\n%@: %@\n%@: %@\n------------------------------------",
            NSStringFromSelector(@selector(startTime)), self.startTime,
            NSStringFromSelector(@selector(endTime)), self.endTime,
            NSStringFromSelector(@selector(energyExpenditureValue)), self.energyExpenditureValue,
            NSStringFromSelector(@selector(energyExpenditureUnitOfMeasurement)), self.energyExpenditureUnitOfMeasurement,
            NSStringFromSelector(@selector(distance)), self.distance,
            NSStringFromSelector(@selector(distanceUnitOfMeasurement)), self.distanceUnitOfMeasurement,
            NSStringFromSelector(@selector(totalSteps)), self.totalSteps,
            NSStringFromSelector(@selector(heartRateMinimum)), self.heartRateMinimum,
            NSStringFromSelector(@selector(heartRateMaximum)), self.heartRateMaximum,
            NSStringFromSelector(@selector(heartRateAverage)), self.heartRateAverage
            ];
}

@end
