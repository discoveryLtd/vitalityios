#import "VITHealthKitHelper.h"
#import "VITHealthKitHelper+HealthKitQueries.h"

  
/**
 Note: Use only this bundle identifier for Debug-Simulator Purposes
 NSString * const kAppleHardwareDeviceBundleIdentifierPrefix = @"com.apple.Health";
 
 For actual devices use:
 NSString * const kAppleHardwareDeviceBundleIdentifierPrefix = @"com.apple.health";
 */
  
NSString * const kAppleHardwareDeviceBundleIdentifierPrefix = @"com.apple.health";
NSString * const kNikePlusRunClubBundleIdentifierPrefix = @"com.nike.nikeplus-gps";

@implementation VITHealthKitHelper (HealthKitQueries)

#pragma mark - Workouts

+ (HKAnchoredObjectQuery *_Nonnull)heartRateSamplesQueryForWorkout:(HKWorkout *_Nonnull)workout completion:(void (^ _Nullable)(NSArray<__kindof HKSample *> *_Nullable sampleObjects, NSArray<HKDeletedObject *> *_Nullable deletedObjects, NSError *_Nullable error))completion
{
    NSPredicate *predicate = [VITHealthKitHelper compoundPredicateWithAppleDeviceSourcesForStartDate:workout.startDate endDate:workout.endDate];
    HKQuantityType *heartRateType = [HKQuantityType quantityTypeForIdentifier:HKQuantityTypeIdentifierHeartRate];

    HKAnchoredObjectQuery *query = [[HKAnchoredObjectQuery alloc] initWithType:heartRateType predicate:predicate anchor:nil limit:HKObjectQueryNoLimit resultsHandler:^(HKAnchoredObjectQuery * _Nonnull anchoredObjectQuery , NSArray<__kindof HKSample *> * _Nullable sampleObjects, NSArray<HKDeletedObject *> * _Nullable deletedObjects, HKQueryAnchor * _Nullable newAnchor, NSError * _Nullable error) {
        if (completion) completion(sampleObjects, deletedObjects, error);
    }];

    return query;
}

#pragma mark - Walking / running distance and calories for the day

+ (HKStatisticsQuery *)walkingRunningDistanceForStartDate:(NSDate *)startDate endDate:(NSDate *)endDate completion:(void (^ _Nullable)(HKQuantity *_Nullable))completion
{
    HKQuantityType *type = [HKQuantityType quantityTypeForIdentifier:HKQuantityTypeIdentifierDistanceWalkingRunning];
    NSPredicate *dateRangePredicate = [VITHealthKitHelper compoundPredicateWithAppleDeviceSourcesForStartDate:startDate endDate:endDate];
    HKStatisticsQuery *query = [[HKStatisticsQuery alloc] initWithQuantityType:type quantitySamplePredicate:dateRangePredicate options:HKStatisticsOptionCumulativeSum completionHandler:^(HKStatisticsQuery * _Nonnull query, HKStatistics * _Nullable result, NSError * _Nullable error) {
        if (completion) completion(result.sumQuantity);
    }];
    return query;
}

+ (HKStatisticsQuery *)activeEnergyBurnedForStartDate:(NSDate *)startDate endDate:(NSDate *)endDate completion:(void (^ _Nullable)(HKQuantity *_Nullable))completion
{
    HKQuantityType *type = [HKQuantityType quantityTypeForIdentifier:HKQuantityTypeIdentifierActiveEnergyBurned];
    NSPredicate *dateRangePredicate = [VITHealthKitHelper compoundPredicateWithAppleDeviceSourcesForStartDate:startDate endDate:endDate];
    HKStatisticsQuery *query = [[HKStatisticsQuery alloc] initWithQuantityType:type quantitySamplePredicate:dateRangePredicate options:HKStatisticsOptionCumulativeSum completionHandler:^(HKStatisticsQuery * _Nonnull query, HKStatistics * _Nullable result, NSError * _Nullable error) {
        if (completion) completion(result.sumQuantity);
    }];
    return query;
}

+ (NSPredicate *_Nullable)newAppleDeviceSourcePredicate
{
    HKHealthStore *healthStore = [[VITHealthKitHelper sharedHelper] healthStore];

    dispatch_group_t queriesGroup = dispatch_group_create();
    NSMutableSet *appleDeviceSources = [NSMutableSet set];

    void (^sourcesQueryCompletionHandler)(HKSourceQuery *query, NSSet *sources, NSError *error) = ^void(HKSourceQuery *query, NSSet *sources, NSError *error) {
        for (HKSource *source in sources) {
            DLog(@"%@", source.bundleIdentifier);
            if ([source.bundleIdentifier hasPrefix:kAppleHardwareDeviceBundleIdentifierPrefix] || [source.bundleIdentifier hasPrefix:kNikePlusRunClubBundleIdentifierPrefix]) {
                [appleDeviceSources addObject:source];
            }
        }
        dispatch_group_leave(queriesGroup);
    };

    // query Apple devices that provide step data
    dispatch_group_enter(queriesGroup);
    HKSampleType *sampleType = [HKQuantityType quantityTypeForIdentifier:HKQuantityTypeIdentifierStepCount];
    HKSourceQuery *sourceQuery = [[HKSourceQuery alloc] initWithSampleType:sampleType samplePredicate:nil completionHandler:sourcesQueryCompletionHandler];
    [healthStore executeQuery:sourceQuery];

    // query Apple devices that provide workout data
    dispatch_group_enter(queriesGroup);
    sampleType = [HKWorkoutType workoutType];
    sourceQuery = [[HKSourceQuery alloc] initWithSampleType:sampleType samplePredicate:nil completionHandler:sourcesQueryCompletionHandler];
    [healthStore executeQuery:sourceQuery];

    // query Apple devices that provide heart rate data
    dispatch_group_enter(queriesGroup);
    sampleType = [HKQuantityType quantityTypeForIdentifier:HKQuantityTypeIdentifierHeartRate];
    sourceQuery = [[HKSourceQuery alloc] initWithSampleType:sampleType samplePredicate:nil completionHandler:sourcesQueryCompletionHandler];
    [healthStore executeQuery:sourceQuery];

    // query Apple devices that provide body mass data
    dispatch_group_enter(queriesGroup);
    sampleType = [HKQuantityType quantityTypeForIdentifier:HKQuantityTypeIdentifierBodyMass];
    sourceQuery = [[HKSourceQuery alloc] initWithSampleType:sampleType samplePredicate:nil completionHandler:sourcesQueryCompletionHandler];
    [healthStore executeQuery:sourceQuery];

    // for activity summary and biological sex we can't query for source, it can
    // only be written by Apple devices

    // leave the group and finalise
    dispatch_group_wait(queriesGroup, DISPATCH_TIME_FOREVER);

    NSPredicate *predicate = [HKQuery predicateForObjectsFromSources:[NSSet setWithArray:appleDeviceSources.allObjects]];
    DLog(@"HealthKit: appleSourcesPredicate configured");

    return predicate;
}

@end
