#import <Foundation/Foundation.h>

@interface VITDevice : NSObject

@property(nonatomic, strong) NSString *manufacturer;
@property(nonatomic, strong) NSString *make;
@property(nonatomic, strong) NSString *model;
@property(nonatomic, strong) NSString *deviceID;

- (instancetype)initWithWithDeviceID:(NSString *)deviceID manufacturer:(NSString *)manufacturer make:(NSString *)make model:(NSString *)model;

@end
