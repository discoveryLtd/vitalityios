#import <Foundation/Foundation.h>

NSString *HealthKitPermissionRequestDidCompleteNotification = @"HealthKitPermissionRequestDidCompleteNotification";
NSString *ShouldDisplayHealthKitOnboardingViewControllerNotification = @"ShouldDisplayHealthKitOnboardingViewControllerNotification";
NSString *DidDismissHealthKitOnboardingViewControllerNotification = @"DidDismissHealthKitOnboardingViewControllerNotification";
NSString *HealthKitSyncDidCompleteNotification = @"HealthKitSyncDidCompleteNotification";
NSString *HealthKitDidDelinkNotification = @"HealthKitDidDelinkNotification";
