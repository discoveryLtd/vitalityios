#import <Foundation/Foundation.h>

@interface NSString (Utilities)

- (NSString *)sha256;

@end
