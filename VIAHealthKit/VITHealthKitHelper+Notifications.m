#import "VITHealthKitHelper+Notifications.h"

@implementation VITHealthKitHelper (Notifications)

- (void)scheduleLocalReminderNotificationWithMessage:(NSString *)message
{
    // request permission
    UIApplication *application = [UIApplication sharedApplication];
    UIUserNotificationType types = UIUserNotificationTypeAlert|UIUserNotificationTypeBadge|UIUserNotificationTypeSound;
    [application registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:types categories:nil]];
    // schedule
    [VITHealthKitHelper scheduleWorkoutSyncReminderLocalNotificationWithLastSyncDate:self.lastQueryDate currentDate:[NSDate date] message:message];
}

+ (void)scheduleWorkoutSyncReminderLocalNotificationWithLastSyncDate:(NSDate *)lastSyncDate currentDate:(NSDate *)now message:(NSString *)message
{
    [self cancelAllSyncReminderNotifications];
    UILocalNotification *syncReminderNotification = [UILocalNotification new];
    syncReminderNotification.fireDate = [self calculateSyncReminderFireDateForLastSyncDate:lastSyncDate now:now];
    syncReminderNotification.soundName = UILocalNotificationDefaultSoundName;
    syncReminderNotification.alertBody = message;
    syncReminderNotification.userInfo = @{
                                          kMEMLocalNotificationIdentifierKey: kHealthKitSyncReminderNotificationIdentifier
                                          };
    [[UIApplication sharedApplication] scheduleLocalNotification:syncReminderNotification];
}

+ (void)cancelAllSyncReminderNotifications
{
    for (UILocalNotification *notification in [[UIApplication sharedApplication] scheduledLocalNotifications]) {
        if ([notification.userInfo[kMEMLocalNotificationIdentifierKey] isEqualToString:kHealthKitSyncReminderNotificationIdentifier]) {
            [[UIApplication sharedApplication] cancelLocalNotification:notification];
        }
    }
}

+ (NSDate *)calculateSyncReminderFireDateForLastSyncDate:(NSDate *)lastSyncDate now:(NSDate *)now
{
    NSDate *thisWeekFriday7AM = [self dateOfFridayInSameWeekAsDate:now];
    NSDateComponents *timeBetween = [[NSCalendar currentCalendar] components:NSCalendarUnitHour fromDate:lastSyncDate toDate:thisWeekFriday7AM options:0];
    if ([self isDate:now afterDate:thisWeekFriday7AM] || (timeBetween.hour < 24)) {
        return [self oneWeekAfterDate:thisWeekFriday7AM];
    } else {
        return thisWeekFriday7AM;
    }
}

+ (NSDate *)dateOfFridayInSameWeekAsDate:(NSDate *)date
{
    NSDateComponents *componentsThisWeekFriday = [[NSCalendar currentCalendar] components:NSCalendarUnitWeekOfMonth | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:date];
    componentsThisWeekFriday.weekday = 6; // Friday, with Sunday as first day.
    componentsThisWeekFriday.hour = 7; // 7 AM.
    return [[NSCalendar currentCalendar] dateFromComponents:componentsThisWeekFriday];
}

+ (BOOL)isDate:(NSDate *)date1 afterDate:(NSDate *)date2
{
    return ([date1 compare:date2] == NSOrderedDescending);
}

+ (NSDate *)oneWeekAfterDate:(NSDate *)date
{
    NSDateComponents *componentsOneWeek = [NSDateComponents new];
    componentsOneWeek.day = 7;
    return [[NSCalendar currentCalendar] dateByAddingComponents:componentsOneWeek toDate:date options:0];
}

+ (void)cancelLocalNotification
{
    for (UILocalNotification *localNotification in [[UIApplication sharedApplication] scheduledLocalNotifications]) {
        if ([localNotification.userInfo valueForKey:kMEMLocalNotificationIdentifierKey]) {
            [[UIApplication sharedApplication] cancelLocalNotification:localNotification];
        }
    }
}

@end
