#import "VITVitalityWorkoutActiveInterval.h"

@implementation VITVitalityWorkoutActiveInterval

- (NSString *)description {
    return [NSString stringWithFormat:@"%@ - %@", self.startDate, self.endDate];
}

+ (NSArray *)activeIntervalsForWorkout:(HKWorkout *)workout {
    NSMutableArray *events = [NSMutableArray new];
    VITVitalityWorkoutActiveInterval *interval = [VITVitalityWorkoutActiveInterval new];
    interval.startDate = workout.startDate;
    
    for (HKWorkoutEvent *event in workout.workoutEvents) {
        if (interval && event.type == HKWorkoutEventTypePause) {
            interval.endDate = event.date;
            [events addObject:interval];
            interval = nil;
            DLog(@"Workout paused at %@", event.date);
        }
        
        if (event.type == HKWorkoutEventTypeResume) {
            interval = [VITVitalityWorkoutActiveInterval new];
            interval.startDate = event.date;
            DLog(@"Found resumed at %@", event.date);
        }
    }
    
    if (interval) {
        interval.endDate = workout.endDate;
        [events addObject:interval];
    }
    
    return events;
}

@end
