#import "VITVitalityHeartRateCalculator.h"
#import <HealthKit/HealthKit.h>

@implementation VITVitalityHeartRateCalculator

+ (double)averageHeartRateWithStatisticsCollection:(HKStatisticsCollection *)statisticsCollection
{
    NSUInteger numberOfStatistics = statisticsCollection.statistics.count;
    if (numberOfStatistics == 0) {
        return 0.0;
    }

    double averageHeartRate = 0.0;
    for (HKStatistics *statistics in statisticsCollection.statistics) {
        double beatsPerMinute = [statistics.averageQuantity doubleValueForUnit:[HKUnit unitFromString:@"count/min"]];
        averageHeartRate += beatsPerMinute;
    }
    return round(averageHeartRate / numberOfStatistics);
}

+ (double)maximumHeartRateWithStatisticsCollection:(HKStatisticsCollection *)statisticsCollection
{
    return [self heartRateQuantityDataOfType:VITVitalityHeartRateCalculatorDataTypeMaximum withStatisticsCollection:statisticsCollection];
}

+ (double)minimumHeartRateWithStatisticsCollection:(HKStatisticsCollection *)statisticsCollection
{
    return [self heartRateQuantityDataOfType:VITVitalityHeartRateCalculatorDataTypeMinimum withStatisticsCollection:statisticsCollection];
}

+ (double)heartRateQuantityDataOfType:(VITVitalityHeartRateCalculatorDataType)dateType withStatisticsCollection:(HKStatisticsCollection *)statisticsCollection
{
    NSArray<HKStatistics *> *statistics = statisticsCollection.statistics;
    if (statistics.count == 0) {
        return 0.0;
    }

    double finalValue = [self initialValueForStatistics:statistics forDateType:dateType];
    for (HKStatistics *statistic in statistics) {
        if (dateType == VITVitalityHeartRateCalculatorDataTypeMinimum) {
            finalValue = [self minimumValueBetweenCurrentMinimum:finalValue andStatistic:statistic];
        } else {
            finalValue = [self maximumValueBetweenCurrentMaximum:finalValue andStatistic:statistic];
        }
    }
    return finalValue;
}

+ (double)maximumValueBetweenCurrentMaximum:(double)currentMaximum andStatistic:(HKStatistics *)statistic
{
    double quantity = [statistic.maximumQuantity doubleValueForUnit:[HKUnit unitFromString:@"count/min"]];
    if (quantity > currentMaximum) {
        currentMaximum = quantity;
    }
    return currentMaximum;
}

+ (double)minimumValueBetweenCurrentMinimum:(double)currentMinimum andStatistic:(HKStatistics *)statistic
{
    double quantity = [statistic.minimumQuantity doubleValueForUnit:[HKUnit unitFromString:@"count/min"]];
    if (quantity < currentMinimum) {
        currentMinimum = quantity;
    }
    return currentMinimum;
}

+ (double)initialValueForStatistics:(NSArray<HKStatistics *> *)statistics forDateType:(VITVitalityHeartRateCalculatorDataType)dateType
{
    double finalValue;
    if (dateType == VITVitalityHeartRateCalculatorDataTypeMinimum) {
        finalValue = [statistics[0].minimumQuantity doubleValueForUnit:[HKUnit unitFromString:@"count/min"]];
    } else {
        finalValue = [statistics[0].maximumQuantity doubleValueForUnit:[HKUnit unitFromString:@"count/min"]];
    }
    return finalValue;
}

@end
