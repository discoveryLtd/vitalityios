#import "NSString+Utilities.h"
#import "NSData+Utilities.h"

@implementation NSString (Utilities)

- (NSString *)sha256
{
    return [[[self dataUsingEncoding:NSUTF8StringEncoding] SHA256Hash] hexadecimalString];
}

@end


