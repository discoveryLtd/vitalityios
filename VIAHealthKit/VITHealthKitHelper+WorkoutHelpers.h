#import "VITHealthKitHelper.h"
#import <HealthKit/HealthKit.h>

@interface VITHealthKitHelper (WorkoutHelpers)

+ (NSArray *)excludeUserEnteredAndNonAppleWorkouts:(NSArray *)workouts;
+ (NSArray *)filterOutHeartRateResults:(NSArray<HKSample*> *)heartRateSamples forPausedWorkoutEventsInWorkout:(HKWorkout *)workout;

@end
