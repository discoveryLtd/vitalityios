#import "VITHealthKitHelper.h"
#import "VITGDLReading.h"
#import "VITHealthKitHelper+DateHelpers.h"
#import "VITHealthKitHelper+WorkoutHelpers.h"
#import "VITDevice.h"
#import "VITHealthKitHelper+HealthKitQueries.h"
#import "NSDate+Utilities.h"
#import "VITNotifications.h"
#import "VITVitalityWorkoutActiveInterval.h"
#import "UIDeviceHardware.h"
#import "NSString+Utilities.h"
#import "VITHealthKitHelper+Notifications.h"

NSString * const kVITUserDefaultsHasRequestedHealthKitUsage = @"VITUserDefaultsHasRequestedHealthKitUsage";
NSString * const kVITHealthKitLastQueryDate = @"VITHealthKitLastQueryDate";
NSString * const kVITHealthKitLastSubmittedDate = @"VITHealthKitLastSubmittedDate";
NSString * const kVITHealthKitLinkedUserEntityNumberHash = @"VITHealthKitLinkedUserEntityNumberHash";
NSString * const kApple = @"Apple";
NSString * const kHealthKitSyncReminderNotificationIdentifier = @"VITHealthKitSyncReminderNotificationIdentifier";
NSString * const kMEMLocalNotificationIdentifierKey = @"kMEMLocalNotificationIdentifierKey";

@interface VITHealthKitHelper()

@property (nonatomic, assign) VITHealthKitPermission currentPermissionRequest;
@property (nonatomic, copy) VITHealthKitHelperPermissionRequestDidCompleteBlock currentPermissionRequestCompletionBlock;
@property (nonatomic, strong) NSMutableDictionary *anchors;
@property (nonatomic, strong) NSDateFormatter *outputFormatter;
@property (nonatomic, strong) NSNumber *healthKitLinkedToCurrentLoggedInUser;
@property (nonatomic, strong) NSString *userEntityNumberHash;
@property (nonatomic, strong) NSString *userEntityNumberRaw;
@property (nonatomic, strong) HKObserverQuery *moveGoalProgressObserverQuery;

- (NSString *)keyForPermission:(VITHealthKitPermission)permission;

@end

@implementation VITHealthKitHelper

@synthesize healthStore = _healthStore;
@synthesize executionStatus = _executionStatus;
@synthesize status = _status;
@synthesize appleSourcePredicate = _appleSourcePredicate;

#pragma mark - Initialisers

+ (instancetype _Nonnull)sharedHelper
{
    static VITHealthKitHelper *sharedHelper;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        sharedHelper = (VITHealthKitHelper *) [[[self class] alloc] init];
        sharedHelper.currentPermissionRequest = VITHealthKitPermissionNone;
        [[NSNotificationCenter defaultCenter] addObserver:sharedHelper selector:@selector(didDismissOnboardingViewController:) name:DidDismissHealthKitOnboardingViewControllerNotification object:nil];
        
        if (![HKHealthStore isHealthDataAvailable]) {
            DLog(@"HealthKit data not available");
        }
    });
    
    return sharedHelper;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self forKeyPath:DidDismissHealthKitOnboardingViewControllerNotification];
}

#pragma mark - Getters

+ (NSUserDefaults *)defaults
{
    NSString *identifier = [NSString stringWithFormat:@"%@.%@", [[NSBundle mainBundle] bundleIdentifier], @"HealthKit"];
    NSUserDefaults *defaults = [[NSUserDefaults alloc] initWithSuiteName:identifier];
    return defaults;
}

- (NSDateFormatter *)fullFormatter
{
    if (_fullFormatter) {
        return _fullFormatter;
    }
    _fullFormatter = [NSDate vit_formatterWithZone];
    return _fullFormatter;
}

- (NSDateFormatter *)outputFormatter
{
    if (_outputFormatter) {
        return _outputFormatter;
    }
    _outputFormatter = [NSDate vit_outputFormatter];
    return _outputFormatter;
}

- (HKHealthStore *)healthStore
{
    if (![HKHealthStore isHealthDataAvailable]) {
        return nil;
    }
    
    if (_healthStore) {
        return _healthStore;
    }
    _healthStore = [[HKHealthStore alloc] init];
    return _healthStore;
}

+ (NSError *)genericHealthKitAccessError
{
    return [[NSError alloc] initWithDomain:@"VitalityErrorDomain" code:-1 userInfo:@{NSLocalizedDescriptionKey : @"HealthKit not available or enabled"}];
}

- (NSSet *)healthKitReadTypesForPermission:(VITHealthKitPermission)permission
{
    // we have to read the types from the info.plist, since each market's app can
    // be configured for different HealthKit sample types. so ultimately we loop
    // over each type's types in the plist, try to create a sample type of it and
    // add it ultimately to the set of read types.
    NSDictionary *types = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"VAHealthKitReadTypes"];
    NSMutableSet *set = [NSSet set].mutableCopy;
    
    switch (permission) {
        case VITHealthKitPermissionFitness: {
            NSArray *fitnessTypes = types[@"Fitness"];
            for (NSString *type in fitnessTypes) {
                HKQuantityType *t = [HKQuantityType quantityTypeForIdentifier:type];
                if (t != nil) {
                    [set addObject:t];
                } else if ([type isEqualToString:NSStringFromClass([HKWorkoutType class])]) {
                    [set addObject:[HKWorkoutType workoutType]];
                }
            }
            break;
        }
        case VITHealthKitPermissionNone:
        case VITHealthKitPermissionCount:
            break;
    }
    
    return [NSSet setWithSet:set];
}

- (NSSet *)readTypesForFitness
{
    return [self healthKitReadTypesForPermission:VITHealthKitPermissionFitness];
}

- (NSDate *_Nullable)lastQueryDate
{
    NSDate *lastQueryDate = nil;
    NSString *formattedDateString = [[VITHealthKitHelper defaults] stringForKey:kVITHealthKitLastQueryDate];
    if (formattedDateString) {
        lastQueryDate = [self.fullFormatter dateFromString:formattedDateString];
    }
    return lastQueryDate;
}

- (NSDate *_Nullable)lastSubmittedDate
{
    NSDate *lastSubmittedDate = nil;
    NSString *formattedDateString = [[VITHealthKitHelper defaults] stringForKey:kVITHealthKitLastSubmittedDate];
    if (formattedDateString) {
        lastSubmittedDate = [self.fullFormatter dateFromString:formattedDateString];
    }
    return lastSubmittedDate;
}

- (VITHealthKitHelperStatus)status
{
    if (![HKHealthStore isHealthDataAvailable]) {
        return VITHealthKitHelperStatusHealthKitNotAvailable;
    }
    
    if (![VITHealthKitHelper linkedUserEntityNumberHash]) {
        return VITHealthKitHelperStatusNotLinkedYet;
    }
    
    if (self.healthKitLinkedToCurrentLoggedInUser && !self.healthKitLinkedToCurrentLoggedInUser.boolValue) {
        return VITHealthKitHelperStatusLinkedToDifferentUser;
    }
    
    return VITHealthKitHelperStatusAvailable;
}

+ (VITDevice *)deviceForSubmission
{
    NSString *deviceID = [UIDevice currentDevice].identifierForVendor.UUIDString;
    deviceID = [deviceID stringByReplacingOccurrencesOfString:@"-" withString:@""];
    NSString *makeIdentifier = [UIDeviceHardware platform];
    NSString *modelIdentifier = [UIDeviceHardware platformString];
    VITDevice *device = [[VITDevice alloc] initWithWithDeviceID:deviceID manufacturer:kApple make:makeIdentifier model:modelIdentifier];
    
    return device;
}

#pragma mark - Setters

- (void)setUserEntityNumber:(NSString * _Nullable)userEntityNumber
{
    _userEntityNumberHash = [userEntityNumber sha256];
    _userEntityNumberRaw = userEntityNumber;
    
    self.healthKitLinkedToCurrentLoggedInUser = nil;
    NSString *linkedUserEntityNumber = [[VITHealthKitHelper defaults] objectForKey:kVITHealthKitLinkedUserEntityNumberHash];
    if (linkedUserEntityNumber) {
        self.healthKitLinkedToCurrentLoggedInUser = @([linkedUserEntityNumber isEqualToString:self.userEntityNumberHash]);
    }
}

#pragma mark - Permissions

- (NSString *)keyForPermission:(VITHealthKitPermission)permission
{
    NSString *key = nil;
    
    switch (permission) {
        case VITHealthKitPermissionFitness:
            key = @"VITHealthKitPermissionFitness";
            break;
        case VITHealthKitPermissionNone:
        case VITHealthKitPermissionCount:
            key = nil;
            break;
    }
    
    key = [NSString stringWithFormat:@"%@%@", kVITUserDefaultsHasRequestedHealthKitUsage, key];
    return key;
}

- (void)setHealthKitUsageOnboardingViewPresented:(BOOL)presented forPermission:(VITHealthKitPermission)permission
{
    NSString *key = [self keyForPermission:permission];
    NSNumber *object = @(presented);
    [[VITHealthKitHelper defaults] setObject:object forKey:key]; // don't use setBoolForKey:
    [[VITHealthKitHelper defaults] synchronize];
}

+ (BOOL)hasPresentedHealthKitUsageOnboardingForPermission:(VITHealthKitPermission)permission
{
    NSString *key = [[VITHealthKitHelper sharedHelper] keyForPermission:permission];
    NSNumber *object = [[VITHealthKitHelper defaults] objectForKey:key];
    return object.boolValue;
}

- (void)displayHealthIntegrationOnboardingView
{
    [[NSNotificationCenter defaultCenter] postNotificationName:ShouldDisplayHealthKitOnboardingViewControllerNotification object:nil];
}

- (void)requestPermission:(VITHealthKitPermission)permission withCompletionBlock:(VITHealthKitHelperPermissionRequestDidCompleteBlock _Nullable)completion
{
    // no access to healthkit, or incorret permission passed through
    if (self.status == VITHealthKitHelperStatusHealthKitNotAvailable || permission == VITHealthKitPermissionNone) {
        [self finalizeRequestWithSuccess:NO error:[VITHealthKitHelper genericHealthKitAccessError]];
        return;
    }
    
    self.currentPermissionRequest = permission;
    self.currentPermissionRequestCompletionBlock = completion;
    
    if ([VITHealthKitHelper hasPresentedHealthKitUsageOnboardingForPermission:permission]) {
        [self performHealthKitPermissionRequest];
    }
    else {
        [self displayHealthIntegrationOnboardingView];
    }
}

- (void)performHealthKitPermissionRequest
{
    NSSet *readTypes = [self healthKitReadTypesForPermission:self.currentPermissionRequest];
    if (readTypes.count < 1) {
        return; // need at least one permission, otherwise we crash
    }
    
    // no sharing back into HealthKit allowed yet
    __weak typeof(self) weakSelf = self;
    [self.healthStore requestAuthorizationToShareTypes:nil readTypes:readTypes completion:^(BOOL success, NSError *error) {
        // if permissions request finished successfully (not necessarily granted)
        // mark that the question has been asked
        if (success) {
            [weakSelf setHealthKitUsageOnboardingViewPresented:YES forPermission:self.currentPermissionRequest];
            [VITHealthKitHelper setLinkedUserEntityNumberHash:weakSelf.userEntityNumberHash];
        }
        
        // if permissions request finished successfully (not necessarily granted)
        // post a notification so the parent app can query for the unique device
        // token which will be used in all subsequent healthkit requests.
        // this token is generated only once and lives perpetually with
        // the current installation of the app.
        if (success) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [[NSNotificationCenter defaultCenter] postNotificationName:HealthKitPermissionRequestDidCompleteNotification object:@(weakSelf.currentPermissionRequest)];
            });
        }
        
        // run completion block
        if (weakSelf.currentPermissionRequestCompletionBlock) {
            [weakSelf finalizeRequestWithSuccess:success error:error];
        }
    }];
}

- (void)finalizeRequestWithSuccess:(BOOL)success error:(NSError *)error
{
    dispatch_async(dispatch_get_main_queue(), ^{
        if (self.currentPermissionRequestCompletionBlock) {
            self.currentPermissionRequestCompletionBlock(success, error);
        }
        self.currentPermissionRequest = VITHealthKitPermissionNone;
        self.currentPermissionRequestCompletionBlock = nil;
    });
}

- (void)configureAppleSourcePredicate
{
    if (_appleSourcePredicate) {
        return;
    }
    _appleSourcePredicate = [VITHealthKitHelper newAppleDeviceSourcePredicate];
}

#pragma mark - Device token & linked user entity number

+ (NSString *)linkedUserEntityNumberHash
{
    NSString *entityNumberHash = [[VITHealthKitHelper defaults] objectForKey:kVITHealthKitLinkedUserEntityNumberHash];
    return entityNumberHash;
}

+ (void)setLinkedUserEntityNumberHash:(NSString *)userEntityNumberHash
{
    if (userEntityNumberHash) {
        [[VITHealthKitHelper defaults] setObject:userEntityNumberHash forKey:kVITHealthKitLinkedUserEntityNumberHash];
    }
}

#pragma mark - Actions

- (void)didDismissOnboardingViewController:(NSNotification *)notification
{
    [self performHealthKitPermissionRequest];
}

+ (void)setLastQueryDate
{
    NSString *formattedDateString = [[VITHealthKitHelper sharedHelper].fullFormatter stringFromDate:[VITHealthKitHelper now]];
    [[VITHealthKitHelper defaults] setObject:formattedDateString forKey:kVITHealthKitLastQueryDate];
    DLog(@"Updated last query date");
}

+ (void)setLastSubmittedDate
{
    NSString *formattedDateString = [[VITHealthKitHelper sharedHelper].fullFormatter stringFromDate:[VITHealthKitHelper now]];
    [[VITHealthKitHelper defaults] setObject:formattedDateString forKey:kVITHealthKitLastSubmittedDate];
    DLog(@"Updated last submitted date");
}

- (void)delink
{
    [[VITHealthKitHelper defaults] removeObjectForKey:kVITHealthKitLastQueryDate];
    [[VITHealthKitHelper defaults] removeObjectForKey:kVITHealthKitLastSubmittedDate];
    [[VITHealthKitHelper defaults] removeObjectForKey:kVITHealthKitLinkedUserEntityNumberHash];
    [[VITHealthKitHelper defaults] removeObjectForKey:HKQuantityTypeIdentifierStepCount];
    [[VITHealthKitHelper defaults] removeObjectForKey:HKWorkoutTypeIdentifier];
    [VITHealthKitHelper cancelLocalNotification];
    
    for (NSInteger permission = 0; permission < VITHealthKitPermissionCount; permission++) {
        NSString *permissionKey = [self keyForPermission:(VITHealthKitPermission)permission];
        [[VITHealthKitHelper defaults] removeObjectForKey:permissionKey];
    }
    
    [[VITHealthKitHelper defaults] synchronize];

    _status = VITHealthKitHelperStatusNotLinkedYet;
    [[NSNotificationCenter defaultCenter] postNotificationName:HealthKitDidDelinkNotification object:nil];
    DLog(@"Successfully delinked HealthKit");
}

#pragma mark - HealthKit data queries

typedef NSArray *(^ReadingsFromStatisticsCollectionResultBlock)(HKStatisticsCollection *results);

- (void)generateReadingsWithForcedResubmission:(BOOL)forcedResubmission completion:(void (^ _Nonnull)(NSArray<VITGDLReading*> *_Nullable readings))mainCompletion
{
    // check if we have asked for the relevant permissions and completed onboarding
    // before attempting this task
    if (![VITHealthKitHelper hasPresentedHealthKitUsageOnboardingForPermission:VITHealthKitPermissionFitness]) {
        DLog(@"Has not presented permissions/onboarding for VITHealthKitPermissionFitness");
        return mainCompletion(nil);
    }
    
    // already busy
    if (self.executionStatus != VITHealthKitHelperExecutionStatusIdle) {
        DLog(@"HealthKit already busy with generateReadingsAndSubmitReadings");
        return mainCompletion(nil);
    }
    
    _executionStatus = VITHealthKitHelperExecutionStatusRunningQueries;
    
    NSMutableArray<VITGDLReading*> *allReadingsFromAllQueries = [NSMutableArray new];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        
        // this method uses a dispatch group and will block
        // this current async call until the the predicate
        // has been configured.
        [self configureAppleSourcePredicate];
        DLog(@"HealthKit finished configuring device predicate");
        
        dispatch_group_t queriesGroup = dispatch_group_create();
        
        // completion block is the same for all queries.
        // each query has to add the readings it constructed to the
        // array containing ALL readings that need to be submitted.
        void (^completion)(NSArray<VITGDLReading*> *queryReadings) = ^void(NSArray<VITGDLReading*> *queryReadings) {
            if (queryReadings.count > 0) {
                [allReadingsFromAllQueries addObjectsFromArray:queryReadings];
            }
            dispatch_group_leave(queriesGroup);
        };
        
        NSMutableArray *healthKitQueries = [NSMutableArray new];
        
        // workouts
        if (forcedResubmission) {
            [healthKitQueries addObject:[self forcedResubmissionWorkoutsQueryWithCompletion:completion]];
        }
        else {
            [healthKitQueries addObject:[self anchoredWorkoutsQueryWithCompletion:completion]];
        }
        
        // steps
        HKStatisticsCollectionQuery *stepCountQuery = [self stepCountQueryWithReadingsFromStatisticsResultsBlock:[self stepCountReadingFromHealthKitQueryResultWithForcedResubmission:forcedResubmission]
                                                                                              forcedResubmission:forcedResubmission
                                                                                                      completion:completion];
        if (stepCountQuery) [healthKitQueries addObject:stepCountQuery];

        // execute 'em all
        for (HKQuery *query in healthKitQueries) {
            dispatch_group_enter(queriesGroup);
            [self.healthStore executeQuery:query];
        }
        
        DLog(@"HealthKit executing queries...");
        dispatch_group_wait(queriesGroup, DISPATCH_TIME_FOREVER);
        
        DLog(@"HealthKit queries done");
        [VITHealthKitHelper setLastQueryDate];
        dispatch_async(dispatch_get_main_queue(), ^{
            mainCompletion([NSArray arrayWithArray:allReadingsFromAllQueries]);
            _executionStatus = VITHealthKitHelperExecutionStatusIdle;
        });
    });
}

- (void)readingsSubmittedSuccessfully
{
    DLog(@"Successfully uploaded HealthKit readings to GDL");
    [self saveSyncingAnchors];
    [VITHealthKitHelper setLastSubmittedDate];
}

#pragma mark - StepCount query

- (HKStatisticsCollectionQuery *)stepCountQueryWithReadingsFromStatisticsResultsBlock:(ReadingsFromStatisticsCollectionResultBlock)readingsBlock forcedResubmission:(BOOL)forcedResubmission completion:(void(^)(NSArray *readings))completion
{
    HKQuantityType *quantityType = [HKObjectType quantityTypeForIdentifier:HKQuantityTypeIdentifierStepCount];
    
    NSDateComponents *intervalComponents = [[NSDateComponents alloc] init];
    intervalComponents.day = 1;
    
    // get the last date we submitted, otherwise start with 2 weeks ago
    NSCalendar *calendar = NSCalendar.currentCalendar;
    NSDate *anchorDate = [VITHealthKitHelper anchorDateForStepCountQuery];
    if (forcedResubmission) {
        anchorDate = [VITHealthKitHelper twoWeeksAgo];
    }
    
    HKStatisticsCollectionQuery *query = nil;
    if (![calendar isDateInYesterday:anchorDate]) {
        query = [[HKStatisticsCollectionQuery alloc] initWithQuantityType:quantityType quantitySamplePredicate:self.appleSourcePredicate options:HKStatisticsOptionCumulativeSum anchorDate:anchorDate intervalComponents:intervalComponents];
        query.statisticsUpdateHandler = nil;
        query.initialResultsHandler = ^(HKStatisticsCollectionQuery *statisticsCollectionQuery , HKStatisticsCollection *results, NSError *error) {
            DLog(@"HealthKit stepCount query initial results complete");
            if (!results) {
                DLog(@"Could not fetch statistics collection for stepCount: %@", error);
                completion(nil);
            }
            else {
                // dispatch async, because HealthKit callback queues are sucky
                // when you have completion blocks with dispatch groups inside.
                // if we don't do this, the readingsBlock will block the current
                // thread during the query for walking distance and active energy burned.
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
                    NSArray *readings = readingsBlock(results);
                    completion(readings);
                });
            }
        };
    }
    
    return query;
}

- (ReadingsFromStatisticsCollectionResultBlock)stepCountReadingFromHealthKitQueryResultWithForcedResubmission:(BOOL)forcedResubmission
{
    ReadingsFromStatisticsCollectionResultBlock block = ^NSArray *(HKStatisticsCollection *results) {
        
        NSString *identifier = HKQuantityTypeIdentifierStepCount;
        NSMutableArray *readings = [NSMutableArray new];
        
        // get the last date we submitted
        // --------------------------------------------------------
        NSDateFormatter *dateFormatter = [NSDate vit_outputFormatter];
        NSString *startDateString = [[VITHealthKitHelper defaults] objectForKey:identifier];
        NSDate *endDate = [VITHealthKitHelper todayAtElevenFiftyNinePM];
        // if there is no startDateString yet (most likely this is because it's the first time this query has run)
        // then query for the last 2 weeks worth of step data
        NSDate *startDate = [VITHealthKitHelper twoWeeksAgo];
        if (startDateString) {
            startDate = [NSDate vit_removeTimeStampFromDate:[dateFormatter dateFromString:startDateString]];
        }
        if (forcedResubmission) {
            startDate = [VITHealthKitHelper twoWeeksAgo];
        }
        // --------------------------------------------------------

        // enumerate the new data
        [results enumerateStatisticsFromDate:startDate toDate:endDate withBlock:^(HKStatistics *stepsResult, BOOL *stop) {
            if (stepsResult.sumQuantity) {
                
                __block HKQuantity *distance = nil;
                __block HKQuantity *energy = nil;
                
                VITGDLReading *reading = [VITGDLReading readingForStepsWithStepsResult:stepsResult distanceQuantity:distance energyQuantity:energy entityNumber:[VITHealthKitHelper sharedHelper].userEntityNumberRaw];
                if (reading) [readings addObject:reading];
            }
        }];
        
        // return all the readings
        self.anchors[identifier] = [dateFormatter stringFromDate:endDate];
        return readings;
    };
    
    return block;
}

#pragma mark - Workouts query

+ (NSCompoundPredicate *_Nullable)compoundPredicateWithAppleDeviceSourcesForDateIntervals:(NSArray <VITVitalityWorkoutActiveInterval *>*)intervals
{
    NSMutableArray *intervalPredicates = [NSMutableArray new];
    
    for (VITVitalityWorkoutActiveInterval *interval in intervals) {
        NSPredicate *dateRangePredicate = [HKQuery predicateForSamplesWithStartDate:interval.startDate endDate:interval.endDate options:(HKQueryOptionStrictStartDate|HKQueryOptionStrictEndDate)];
        [intervalPredicates addObject:dateRangePredicate];
    }
    
    NSCompoundPredicate *orDatesPredicate = [NSCompoundPredicate orPredicateWithSubpredicates:intervalPredicates];
    NSCompoundPredicate *compoundPredicate = [NSCompoundPredicate andPredicateWithSubpredicates:@[[VITHealthKitHelper sharedHelper].appleSourcePredicate, orDatesPredicate]];
    return compoundPredicate;
}

+ (NSCompoundPredicate *_Nullable)compoundPredicateWithAppleDeviceSourcesForStartDate:(NSDate *_Nullable)startDate endDate:(NSDate *_Nullable)endDate
{
    NSPredicate *dateRangePredicate = [HKQuery predicateForSamplesWithStartDate:startDate endDate:endDate options:(HKQueryOptionStrictStartDate|HKQueryOptionStrictEndDate)];
    NSCompoundPredicate *compoundPredicate = [NSCompoundPredicate andPredicateWithSubpredicates:@[[VITHealthKitHelper sharedHelper].appleSourcePredicate, dateRangePredicate]];
    return compoundPredicate;
}

+ (NSSortDescriptor *)startDateSortDescriptor
{
    return [NSSortDescriptor sortDescriptorWithKey:HKSampleSortIdentifierStartDate ascending:YES];
}

- (HKSampleQuery *_Nonnull)forcedResubmissionWorkoutsQueryWithCompletion:(void (^ _Nonnull)(NSArray *_Nullable readings))completion
{
    NSCompoundPredicate *predicate = [VITHealthKitHelper compoundPredicateWithAppleDeviceSourcesForStartDate:[VITHealthKitHelper twoWeeksAgo] endDate:[VITHealthKitHelper now]];
    HKSampleQuery *query = [[HKSampleQuery alloc] initWithSampleType:[HKWorkoutType workoutType] predicate:predicate limit:HKObjectQueryNoLimit sortDescriptors:@[[VITHealthKitHelper startDateSortDescriptor]] resultsHandler:^(HKSampleQuery *sampleQuery, NSArray *workoutResults, NSError *error) {
        
        if (!workoutResults || workoutResults.count == 0) {
            DLog(@"No new HealthKit workouts to sync: %@", error);
            completion(nil);
            return;
        }

        [self generateWorkoutReadingsFromWorkouts:workoutResults completion:completion];
    }];
    
    return query;
}

- (HKAnchoredObjectQuery *_Nonnull)anchoredWorkoutsQueryWithCompletion:(void (^ _Nonnull)(NSArray *_Nullable readings))completion
{
    NSString *workoutTypeIdentifier = HKWorkoutTypeIdentifier;
    HKQueryAnchor *anchor = [VITHealthKitHelper workoutSyncQueryAnchor];
    
    // query
    HKAnchoredObjectQuery *query = [[HKAnchoredObjectQuery alloc] initWithType:[HKWorkoutType workoutType] predicate:self.appleSourcePredicate anchor:anchor limit:HKObjectQueryNoLimit resultsHandler:^(HKAnchoredObjectQuery * _Nonnull anchoredObjectQuery , NSArray<__kindof HKSample *> * _Nullable sampleObjects, NSArray<HKDeletedObject *> * _Nullable deletedObjects, HKQueryAnchor * _Nullable newAnchor, NSError * _Nullable error) {
        
        if (!sampleObjects || sampleObjects.count == 0) {
            DLog(@"No new HealthKit workouts to sync: %@", error);
            completion(nil);
            return;
        }
        
        self.anchors[workoutTypeIdentifier] = newAnchor;
        [self generateWorkoutReadingsFromWorkouts:sampleObjects completion:completion];
    }];
    
    return query;
}

- (void)generateWorkoutReadingsFromWorkouts:(NSArray *)workouts completion:(void(^)(NSArray *readings))completion
{
    DLog(@"HealthKit workouts query complete with number of workouts: %lu", (unsigned long)workouts.count);
    NSMutableArray *workoutReadings = [NSMutableArray new];
    NSArray *validWorkouts = [VITHealthKitHelper excludeUserEnteredAndNonAppleWorkouts:workouts];

    if (validWorkouts.count == 0) {
        // if we reach this point and there are no
        // valid workouts it is likely that there were some, but they got thrown out,
        // so we have to finish up with an empty completion callback
        completion(nil);
    }
    else {

        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{

            // process each workout individually, getting the heart rate
            NSInteger processed = 0;
            for (HKWorkout *workout in validWorkouts) {
                dispatch_group_t workoutMetadataGroup = dispatch_group_create();
                NSDateComponents *intervalComponents = [[NSDateComponents alloc] init];
                intervalComponents.second = 60;

                __block HKStatisticsCollection *statisticsCollection = nil;
                if (workout.startDate) {
                    NSArray *activeEvents = [VITVitalityWorkoutActiveInterval activeIntervalsForWorkout:workout];
                    
                    NSCompoundPredicate *activeIntervalPredicate = [VITHealthKitHelper compoundPredicateWithAppleDeviceSourcesForDateIntervals:activeEvents];
                    HKStatisticsCollectionQuery *averageQuery = [[HKStatisticsCollectionQuery alloc] initWithQuantityType:[HKObjectType quantityTypeForIdentifier:HKQuantityTypeIdentifierHeartRate]
                                                                                                  quantitySamplePredicate:activeIntervalPredicate
                                                                                                                  options:(HKStatisticsOptionDiscreteAverage|HKStatisticsOptionDiscreteMin|HKStatisticsOptionDiscreteMax)
                                                                                                               anchorDate:workout.startDate
                                                                                                       intervalComponents:intervalComponents];
                    averageQuery.statisticsUpdateHandler = nil;
                    averageQuery.initialResultsHandler = ^(HKStatisticsCollectionQuery *query, HKStatisticsCollection *statisticsResults, NSError *error) {
                        statisticsCollection = statisticsResults;
                        dispatch_group_leave(workoutMetadataGroup);
                    };
                    
                    dispatch_group_enter(workoutMetadataGroup);
                    [self.healthStore executeQuery:averageQuery];
                    dispatch_group_wait(workoutMetadataGroup, DISPATCH_TIME_FOREVER);
                }

                VITGDLReading *workoutReading = [VITGDLReading readingForWorkout:workout statisticsCollection:statisticsCollection];
                if (workoutReading)  {
                    [workoutReadings addObject:workoutReading];
                }
                processed++;

                if (processed == validWorkouts.count) {
                    completion(workoutReadings);
                }
            }
        });
    }
}

#pragma mark - Anchors

- (NSMutableDictionary *)anchors
{
    if (_anchors) {
        return _anchors;
    }
    _anchors = [NSMutableDictionary new];
    return _anchors;
}

- (void)saveSyncingAnchors
{
    [VITHealthKitHelper saveSyncingAnchors:self.anchors];
    _anchors = nil;
}

+ (void)saveSyncingAnchors:(NSDictionary *)anchors
{
    for (NSString *identifier in anchors.allKeys) {
        id anchor = anchors[identifier];
        id objectToBeSaved = anchor;
        if ([anchor isKindOfClass:[HKQueryAnchor class]]) {
            objectToBeSaved = [NSKeyedArchiver archivedDataWithRootObject:anchor];
        }
        
        [[VITHealthKitHelper defaults] setObject:objectToBeSaved forKey:identifier];
    }
    
    DLog(@"Updated query anchors");
}

+ (NSDate *)anchorDateForStepCountQuery
{
    NSDate *anchorDate = [VITHealthKitHelper twoWeeksAgo];
    NSDateFormatter *dateFormatter = [NSDate vit_outputFormatter];
    NSString *key = HKQuantityTypeIdentifierStepCount;
    NSString *anchorDateString = [[VITHealthKitHelper defaults] objectForKey:key];
    if (anchorDateString) {
        anchorDate = [NSDate vit_removeTimeStampFromDate:[dateFormatter dateFromString:anchorDateString]];
    }
    if (!anchorDate) {
        anchorDate = [VITHealthKitHelper twoWeeksAgo];
    }
    return anchorDate;
}

+ (HKQueryAnchor *)workoutSyncQueryAnchor
{
    HKQueryAnchor *anchor = nil;
    NSString *workoutTypeIdentifier = HKWorkoutTypeIdentifier;
    id currentAnchorObject = [[VITHealthKitHelper defaults] objectForKey:workoutTypeIdentifier];
    
    
    // return the anchor that is already in the correct format
    if ([currentAnchorObject isKindOfClass:[NSData class]]) {
        anchor = [NSKeyedUnarchiver unarchiveObjectWithData:currentAnchorObject];
    }
    // else translate any old anchors to the new data type
    else {
        // not ideal coming from an unsigned integerValue, but the value we receive here,
        // NSTaggedPointerString, doesn't respond to unsignedIntegerValue
        if ([currentAnchorObject respondsToSelector:@selector(integerValue)]) {
            NSInteger anchorValue = [currentAnchorObject integerValue];
            anchor = [HKQueryAnchor anchorFromValue:(NSUInteger)anchorValue];
            NSData *anchorData = [NSKeyedArchiver archivedDataWithRootObject:anchor];
            [[VITHealthKitHelper defaults] setObject:anchorData forKey:workoutTypeIdentifier];
        }
    }
    
    return anchor;
}

@end
