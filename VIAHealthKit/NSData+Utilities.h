#import <Foundation/Foundation.h>
#import <CommonCrypto/CommonDigest.h>
#import <CommonCrypto/CommonCryptor.h>
#import <CommonCrypto/CommonHMAC.h>

@interface NSData (Crypto)

- (NSData *)SHA256Hash;
- (NSString *)hexadecimalString;

@end
