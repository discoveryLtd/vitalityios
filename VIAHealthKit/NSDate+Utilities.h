#import <Foundation/Foundation.h>

@interface NSDate (Utilities)

/*!
 * @method formatter
 * @discussion Convenience method that returns a formatter. Uses format: yyyy-MM-dd'T'HH:mm:ss.SSSZ
 */
+ (NSDateFormatter *)formatter;
/*!
 * @method outputFormatter
 * @discussion Convenience method that returns a formatter. Uses format: yyyy-MM-dd
 */
+ (NSDateFormatter *)outputFormatter;
/*!
 * @method outputFormatterPlusTime
 * @discussion Convenience method that returns a formatter. Uses format: yyyy-MM-dd'T'HH:mm:ss
 */
+ (NSDateFormatter *)outputFormatterPlusTime;

+ (NSDateFormatter *)weekdayDateMonthYearHoursAndSecondsFormatter;
/*!
 * @method formatterWithZone
 * @discussion Convenience method that returns a formatter. Uses format: yyyy-MM-dd'T'HH:mm:ssZ
 */
+ (NSDateFormatter *)formatterWithZone;

+ (NSDateFormatter *)formatterWithZoneNoTime;

/*!
 * @method outputFormatterPlusTimeZone
 * @discussion Convenience method that returns a formatter. Uses format: yyyy-MM-dd'T'HH:mm:ss.SSSZ
 */
+ (NSDateFormatter *)outputFormatterPlusTimeZone;

/**
 *  Convenience method that returns a formatter. Uses format EEE MMM d HH:mm:ss z yyyy
 *  Locale is set to "en_ZA"
 *
 *  @return NSDateFormatter with Locale set to "en_ZA"
 */
+ (NSDateFormatter *)outputFormatterWithTimeZoneAbbreviation;

+ (NSDateFormatter *)weekdayDateMonthYearFormatter;

/*!
 * @method sanitizeTimeZoneString:
 * @discussion Takes a date string and sanitizes the timezone part if it exists.
 */
+ (NSString *)sanitizeTimeZoneString:(NSString *)dateString;

- (NSString *)timelineDateString;

/*!
 * @method yearMonthFormatter
 * @discussion Covenience method that returns a formatter for the format '2010-01'.
 */
+ (NSDateFormatter *)yearMonthFormatter;

/*!
 * @method yearFormatter
 * @discussion Covenience method that returns a formatter for the format '2010'.
 */
+ (NSDateFormatter *)yearFormatter;

/*!
 * @method monthFormatter
 * @discussion Covenience method that returns a formatter for the format 'January'.
 */
+ (NSDateFormatter *)monthFormatter;

/*!
 * @method monthShortFormatter
 * @discussion Covenience method that returns a formatter for the format 'Jan'.
 */
+ (NSDateFormatter *)monthShortFormatter;

/*!
 * @method dayFormatter
 * @discussion Covenience method that returns a formatter for the format '23'.
 */
+ (NSDateFormatter *)dayFormatter;

+ (NSDateFormatter *)timelineDateFormatter;

/*!
 * @method yearMonthDayAdjacentFormatter
 * @discussion Covenience method that returns a formatter for the format '20110106'.
 */
+ (NSDateFormatter *)yearMonthDayAdjacentFormatter;

/*!
 * @method yearMonthString
 * @discussion Converts the receiver to a string object only specifying the year and month.
 */
- (NSString *)yearMonthString;

/**
 *  Converts the receiver to a string object specifying the year, month and day.
 *
 *  @return NSString in the format of 20140131
 */
- (NSString *)yearMonthDayString;

- (NSString *)GMTPlusTwoTimeString;

/*!
 * @method dayMonthAbbreviation
 * @discussion Converts the receiver to a string with format '06 Sept'.
 */
- (NSString *)dayMonthAbbreviation;

- (NSString *)dayMonthYear;

/*!
 * @method dayFormatter
 * @discussion Covenience method that returns a formatter for the format '14 January 2016'.
 */
+ (NSDateFormatter *)dayMonthYearDateFormatter;

/*!
 * @method dayFormatter
 * @discussion Covenience method that returns a formatter for the format '14 Jan 2016'.
 */
+ (NSDateFormatter *)dayAbbreviatedMonthYearDateFormatter;

+ (NSDateFormatter *)nonZeroPaddedDayMonthYearDateFormatter;

/*!
 * @method dayMonthYearAbbreviation
 * @discussion Converts the receiver to a string with format '06 Sept 2015'.
 */
- (NSString *)dayMonthYearAbbreviation;
- (NSString *)dayMonthYearAbbreviationWithFormatter:(NSDateFormatter *)formatter;

+ (NSDateFormatter *)dayMonthYearAbbreviationWithFormatter;

/*!
 * @method dateExcludingTime
 * @discussion Returns the date instance but with the time components 00:00:00.
 */
- (NSDate *)dateExcludingTime;

/*!
 * @method yearDateComponentForYearCountSinceNow:
 * @discussion Date component based on the provided year count since now (the current date's year).
 * @result Returns a date component object with only the year component.
 */
+ (NSDateComponents *)yearDateComponentForYearCountSinceNow:(NSInteger)count;

/*!
 * @method yearDateTitleForYearCountSinceNow:
 * @discussion String representation of the provided year count since now (the current date's year).
 * @result Returns a string object for the year.
 */
+ (NSString *)yearDateTitleForYearCountSinceNow:(NSInteger)count;
/*!
 * @method monthYearFormatter
 * @discussion Converts the receiver to a string with format 'MMMM yyyy'.
 */
+ (NSDateFormatter *)monthYearFormatter;

/*!
 * @method monthStringForIndex:
 * @discussion Returns the string for the given month index
 * @example 1 == JAN; 2 == FEB
 */
+ (NSString *)monthStringForIndex:(NSInteger)monthIndex;

/*!
 * @method dateWithYearsSinceNow:
 * @discussion Moves the current date ([NSDate date]) the given number of years ahead in time.
 * @example Today = 2012. Years since now = 3. Date returned = 2015.
 */
+ (NSDate *)dateWithYearsSinceNow:(NSInteger)years;

/*!
 * @method dateWithMonthsSinceNow:
 * @discussion Moves the current date ([NSDate date]) the given number of months ahead in time.
 * @example Today = January. Months since now = 2. Date returned = March.
 */
+ (NSDate *)dateWithMonthsSinceNow:(NSInteger)months;

+ (NSDate *)dateWithMonths:(NSInteger)months sinceDate:(NSDate *)date;

/*!
 * @method dateWithYears:sinceDate:
 * @discussion Moves the given date the given number of years ahead in time.
 * @example Given date = 2012. Years since now = 3. Date returned = 2015.
 */
+ (NSDate *)dateWithYears:(NSInteger)years sinceDate:(NSDate *)date;
/*!
 * @method numberOfMonthsPastForCurrentYear
 * @discussion Calculates the number of months that already went by the current year.
 * @example 2012-10-01. Will return 9.
 */
+ (NSInteger)numberOfMonthsPastForCurrentYear;

+ (NSInteger )numberOfYearsSinceDate:(NSDate *)startDate;

/*!
 * @method dateWithFirstDayOfMonth:
 * @discussion Returns a date with the date's day changed to the first of the month of the given date.
 * @param NSDate
 */
+ (NSDate *)dateWithFirstDayOfMonth:(NSDate *)date;
/*!
 * @method dateWithFirstDayOfNextMonth:
 * @discussion Returns a date set to the first day of the coming month for the date provided.
 */
+ (NSDate *)dateWithFirstDayOfNextMonth:(NSDate *)date;
/*!
 * @method dateFirstOfMonthWithYearsSinceNow:
 * @discussion Returns a date set to the first day of the month and moved the given number of years into the future. (Negative years will move the date back into the past)
 * @example Today = 2012-04-20. years = 3. Return = 2015-04-01.
 */
+ (NSDate *)dateFirstOfMonthWithYearsSinceNow:(NSInteger)years;
/*!
 * @method dateForLastDayOfMonthWithDate:
 * @discussion Returns a date based on the given date with the day changed to the last day of the given date's month.
 * @example Date = 2012-01-05. Returns 2012-01-31
 */
+ (NSDate *)dateForLastDayOfMonthWithDate:(NSDate *)date;
/*!
 * @method numberOfMonthsRemainingInYearFromDate:
 * @discussion Calculates and returns the number of months remaining in the year from the given date.
 */
+ (NSInteger)numberOfMonthsRemainingInYearFromDate:(NSDate *)givenDate;
/*!
 * @method dateWithSouthAfricanIDNumber:
 * @discussion Calculates and returns the date of birth from a South African id number.
 */
+ (NSDate *)dateWithSouthAfricanIDNumber:(NSString *)idNumber;
/*!
 * @method threeYearsBackJanuaryDate
 * @discussion The from date for the past three years.
 * @result Returns a date object.
 */
+ (NSDate *)threeYearsBackJanuaryDate;
/*!
 * @method dateIsToday
 * @discussion Validates whether the given date is today.
 */
+ (BOOL)dateIsToday:(NSDate *)date;

+ (BOOL)dateIsTomorrow:(NSDate *)date;

+ (BOOL)dateIsYesterday:(NSDate *)date;

/*!
 * @method indexOfDateClosestToDate:inDatesArray:
 * @discussion Finds and returns the index of a date closest to the specified date within the specified dates array;
 * @param comapareDate Date to compare against
 * @param dates Array of dates to search through for the closest date.
 * @result int The resulting index.
 */
+ (NSInteger)indexOfDateClosestToDate:(NSDate *)compareDate inDatesArray:(NSArray *)dates;

+ (BOOL)nonOverlappingStartDate:(NSDate **)startDate endDate:(NSDate **)endDate forPeriodFromDate:(NSDate *)fromDate toDate:(NSDate *)toDate;
/*!
 * @method ageFromDateOfBirth:
 * @discussion Calculates the age based on the given date.
 */
+ (NSInteger)ageFromDateOfBirth:(NSDate *)dateOfBirth;

+ (NSDateFormatter *)shortStyleFormatter;

+ (NSDate *)dateWithNumberOfMonthsSinceToday:(NSInteger)months;

+ (NSDate *)dateWithNumberOfYearsSinceToday:(NSInteger)years;

+ (NSDate *)dateWithNumberOfMonths:(NSInteger)months sinceDate:(NSDate *)date;

+ (NSDate *)dateWithNumberOfYears:(NSInteger)years sinceDate:(NSDate *)date;

+ (NSDateFormatter *)formatterZoneExcluded;

+ (NSDateFormatter *)DMYFormatter;

+ (NSDateFormatter *)timeAdjacentFormatter;

- (NSString *)dayString;

+ (NSDateFormatter *)formatterNoMilliSeconds;

- (NSString *)yearString;

- (NSString *)healthRecordDateString;

+ (NSDateFormatter *)shortTimeFormatter;

+ (NSDateFormatter *)outputFormatterWithZone;

+ (NSDateFormatter *)formatterHealthRecordDate;

+ (NSTimeZone *)GMTPlus2TimeZone;

+ (NSDate *)dateTwoWeeksAgoFromToday;

+ (NSDateFormatter *)weekDayDateShortMonthYearFormatter;

+ (NSInteger)numberOfHoursBetweenStartDate:(NSDate *)startDate andEndDate:(NSDate *)endDate;

+ (NSDateComponents *)numberOfWeeksAndDaysBetweenStartDate:(NSDate *)startDate andEndDate:(NSDate *)endDate;

+ (NSString *)formatedDateString:(NSDate *)date withFormat:(NSDateFormatter *)dateFormat;

+ (NSDateFormatter *)vit_timeOnlyFormatter; // 24 hour.
+ (NSDateFormatter *)vit_fullFormatter;
+ (NSDateFormatter *)vit_dayWithDateFormatter;
+ (NSDateFormatter *)vit_formatter;
+ (NSDateFormatter *)vit_naturalDateFormatterWithDashes;
+ (NSDateFormatter *)vit_naturalDateFormatter;
+ (NSDateFormatter *)vit_outputFormatter;
+ (NSDateFormatter *)vit_reverseOutputFormatter;
+ (NSDateFormatter *)vit_outputFormatterPlusTime;
+ (NSDateFormatter *)vit_formatterWithZone;
+ (NSDateFormatter *)vit_formatterWithTime;

+ (NSDateFormatter *)vit_formatterWithZoneAlternative;
+ (NSDateFormatter *)vit_outputFormatterPlusMillisecondsPlusTimeZone;

+ (NSDateFormatter *)vit_yearFormatter;
+ (NSDateFormatter *)vit_dayMonthYearFormatter;

+ (NSDateFormatter *)vit_monthYearFormatter;
+ (NSDateFormatter *)vit_yearMonthFormatter;
+ (NSDateFormatter *)vit_yearMonthDayAdjacentFormatter;

+ (NSDate *)vit_removeTimeStampFromDate:(NSDate *)date;
+ (NSDate *)vit_removeDateFromTimeStamp:(NSDate *)date;

+ (BOOL)vit_date:(NSDate *)date isBetweenDate:(NSDate *)beginDate andDate:(NSDate*)endDate;
+ (BOOL)vit_time:(NSDate *)time isBetweenStartTime:(NSDate *)startTime andEndTime:(NSDate *)endTime;

/*!
 * @method sanitizeTimeZoneString:
 * @discussion Takes a date string and sanitizes the timezone part if it exists.
 */
+ (NSString *)vit_sanitizeTimeZoneString:(NSString *)dateString;

+ (NSInteger)vit_ageFromDateOfBirth:(NSDate *)dateOfBirth;

@end
