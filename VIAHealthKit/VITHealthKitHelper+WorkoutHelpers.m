#import "VITHealthKitHelper+WorkoutHelpers.h"

@implementation VITHealthKitHelper (WorkoutHelpers)

+ (NSArray *)excludeUserEnteredAndNonAppleWorkouts:(NSArray *)workouts
{
    NSMutableArray *validWorkouts = [NSMutableArray new];
    for (HKWorkout *workout in workouts) {

        if ([workout.metadata[HKMetadataKeyWasUserEntered] boolValue]) {
            DLog(@"HealthKit workout was user entered, skipping");
            continue;
        }

        if (![workout.sourceRevision.source.bundleIdentifier hasPrefix:kAppleHardwareDeviceBundleIdentifierPrefix] && ![workout.sourceRevision.source.bundleIdentifier hasPrefix:kNikePlusRunClubBundleIdentifierPrefix]) {
            DLog(@"Workout is not from Apple hardware");
            continue;
        }

        if ([workout.sourceRevision.source.bundleIdentifier hasPrefix:kNikePlusRunClubBundleIdentifierPrefix] && ![workout.metadata[@"WorkoutSource"] isEqualToString:@"AppleWatch"]) {
            DLog(@"Nike+ workout is not from Apple Watch");
            continue;
        }

        [validWorkouts addObject:workout];
    }
    return [NSArray arrayWithArray:validWorkouts];
}

+ (NSArray *)filterOutHeartRateResults:(NSArray<HKSample*> *)heartRateSamples forPausedWorkoutEventsInWorkout:(HKWorkout *)workout
{
    if (workout.workoutEvents.count == 0) {
        return heartRateSamples;
    }

    NSMutableArray *heartRateSamplesToBeRemoved = [NSMutableArray new];
    NSSortDescriptor *startDateSortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"date" ascending:YES];
    NSArray *sortedEvents = [workout.workoutEvents sortedArrayUsingDescriptors:@[startDateSortDescriptor]];

    for (NSInteger i = 0; i < sortedEvents.count; i++) {
        HKWorkoutEvent *currentEvent = (HKWorkoutEvent *)sortedEvents[i];
        if (currentEvent.type == HKWorkoutEventTypePause) {
            HKWorkoutEvent *pauseEvent = currentEvent;
            NSDate *pauseIntervalStartDate = pauseEvent.date;
            NSDate *pauseIntervalEndDate = nil;

            HKWorkoutEvent *followingResumeEvent = nil;
            NSInteger indexOfFollowingResumeEvent = i + 1;
            if (indexOfFollowingResumeEvent < sortedEvents.count) {
                followingResumeEvent = sortedEvents[indexOfFollowingResumeEvent];
                pauseIntervalEndDate = followingResumeEvent.date;
            }
            else {
                pauseIntervalEndDate = workout.endDate;
            }

            NSPredicate *pauseIntervalPredicate = [NSPredicate predicateWithFormat:@"(startDate > %@) AND (endDate < %@)", pauseIntervalStartDate, pauseIntervalEndDate];
            NSArray *intervalHeartRateSamples = [heartRateSamples filteredArrayUsingPredicate:pauseIntervalPredicate];
            [heartRateSamplesToBeRemoved addObjectsFromArray:intervalHeartRateSamples];
        }
    }

    NSMutableArray *tempArray = [heartRateSamples mutableCopy];
    [tempArray removeObjectsInArray:heartRateSamplesToBeRemoved];
    return [NSArray arrayWithArray:tempArray];
}

@end
