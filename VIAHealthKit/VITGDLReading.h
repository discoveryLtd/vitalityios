#import <HealthKit/HealthKit.h>

static NSString *const kDefaultWeightUnitOfMeasurement = @"KILOGRAM";

extern NSString * const VITGDLReadingObjectDeviceModel;

typedef NS_ENUM(NSInteger, VITGDLReadingClassification) {
    VITGDLReadingClassificationSteps = 1,
    VITGDLReadingClassificationExercise,
};

@interface VITGDLReading : NSObject

+ (VITGDLReading *)readingForWorkout:(HKWorkout *)workout statisticsCollection:(HKStatisticsCollection *)collection;
+ (VITGDLReading *)readingForStepsWithStepsResult:(HKStatistics *)stepsResult distanceQuantity:(HKQuantity *)distanceQuantity energyQuantity:(HKQuantity *)energyQuantity entityNumber:(NSString *)entityNumber;

@property (nonatomic, strong) NSString *identifier;
@property (nonatomic, strong) NSString *readingDescription;

@property (nonatomic, assign) VITGDLReadingClassification readingClassification;

// Distance
@property (nonatomic, strong) NSNumber * distance;
@property (nonatomic, strong) NSString *distanceUnitOfMeasurement;

// Speed
@property (nonatomic, strong) NSNumber * speedMinimum;
@property (nonatomic, strong) NSNumber * speedMaximum;
@property (nonatomic, strong) NSNumber * speedAverage;
@property (nonatomic, strong) NSString *speedUnitOfMeasurement;

// Heart rate
@property (nonatomic, strong) NSNumber *heartRateMinimum;
@property (nonatomic, strong) NSNumber *heartRateMaximum;
@property (nonatomic, strong) NSNumber *heartRateAverage;

// Time
@property (nonatomic, strong) NSDate *startTime;
@property (nonatomic, strong) NSDate *endTime;

// Weight
@property (nonatomic, strong) NSNumber *weightBmi;
@property (nonatomic, strong) NSString *weightUnitOfMeasurement;
@property (nonatomic, strong) NSNumber *weightValue;

// Location
@property (nonatomic, strong) NSNumber *locationLongitude;
@property (nonatomic, strong) NSNumber *locationLatitude;

// Steps
@property (nonatomic, strong) NSString *steps;
@property (nonatomic, strong) NSNumber *totalSteps;

// Energy expenditure
@property (nonatomic, strong) NSNumber *energyExpenditureValue;
@property (nonatomic, strong) NSString *energyExpenditureUnitOfMeasurement;

// Duration
@property (nonatomic, strong) NSNumber *duration;
@property (nonatomic, strong) NSString *durationUnitOfMeasurement;

// Height
@property (nonatomic, strong) NSNumber *height;
@property (nonatomic, strong) NSString *heightUnitOfMeasurement;

// Misc
@property (nonatomic, strong) NSString *notes;
@property (nonatomic, strong) NSString *origin;

@end
