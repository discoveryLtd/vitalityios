#import <HealthKit/HealthKit.h>

@interface HKWorkout (HKWorkoutActivityTypeStringRepresentation)

- (NSString *)activityTypeIdentifier;

@end
