#import "HKWorkout+HKWorkoutActivityTypeStringRepresentation.h"

@implementation HKWorkout (HKWorkoutActivityTypeStringRepresentation)

- (NSString *)activityTypeIdentifier
{
    HKWorkoutActivityType type = self.workoutActivityType;
    NSString *value = nil;
    
    switch (type) {
        case HKWorkoutActivityTypeAmericanFootball:
            value = @"HKWorkoutActivityTypeAmericanFootball";
            break;
        case HKWorkoutActivityTypeArchery:
            value = @"HKWorkoutActivityTypeArchery";
            break;
        case HKWorkoutActivityTypeAustralianFootball:
            value = @"HKWorkoutActivityTypeAustralianFootball";
            break;
        case HKWorkoutActivityTypeBadminton:
            value = @"HKWorkoutActivityTypeBadminton";
            break;
        case HKWorkoutActivityTypeBaseball:
            value = @"HKWorkoutActivityTypeBaseball";
            break;
        case HKWorkoutActivityTypeBasketball:
            value = @"HKWorkoutActivityTypeBasketball";
            break;
        case HKWorkoutActivityTypeBowling:
            value = @"HKWorkoutActivityTypeBowling";
            break;
        case HKWorkoutActivityTypeBoxing:
            value = @"HKWorkoutActivityTypeBoxing";
            break;
        case HKWorkoutActivityTypeClimbing:
            value = @"HKWorkoutActivityTypeClimbing";
            break;
        case HKWorkoutActivityTypeCricket:
            value = @"HKWorkoutActivityTypeCricket";
            break;
        case HKWorkoutActivityTypeCrossTraining:
            value = @"HKWorkoutActivityTypeCrossTraining";
            break;
        case HKWorkoutActivityTypeCurling:
            value = @"HKWorkoutActivityTypeCurling";
            break;
        case HKWorkoutActivityTypeCycling:
            value = @"HKWorkoutActivityTypeCycling";
            break;
        case HKWorkoutActivityTypeDance:
            value = @"HKWorkoutActivityTypeDance";
            break;
        case HKWorkoutActivityTypeElliptical:
            value = @"HKWorkoutActivityTypeElliptical";
            break;
        case HKWorkoutActivityTypeEquestrianSports:
            value = @"HKWorkoutActivityTypeEquestrianSports";
            break;
        case HKWorkoutActivityTypeFencing:
            value = @"HKWorkoutActivityTypeFencing";
            break;
        case HKWorkoutActivityTypeFishing:
            value = @"HKWorkoutActivityTypeFishing";
            break;
        case HKWorkoutActivityTypeFunctionalStrengthTraining:
            value = @"HKWorkoutActivityTypeFunctionalStrengthTraining";
            break;
        case HKWorkoutActivityTypeGolf:
            value = @"HKWorkoutActivityTypeGolf";
            break;
        case HKWorkoutActivityTypeGymnastics:
            value = @"HKWorkoutActivityTypeGymnastics";
            break;
        case HKWorkoutActivityTypeHandball:
            value = @"HKWorkoutActivityTypeHandball";
            break;
        case HKWorkoutActivityTypeHiking:
            value = @"HKWorkoutActivityTypeHiking";
            break;
        case HKWorkoutActivityTypeHockey:
            value = @"HKWorkoutActivityTypeHockey";
            break;
        case HKWorkoutActivityTypeHunting:
            value = @"HKWorkoutActivityTypeHunting";
            break;
        case HKWorkoutActivityTypeLacrosse:
            value = @"HKWorkoutActivityTypeLacrosse";
            break;
        case HKWorkoutActivityTypeMartialArts:
            value = @"HKWorkoutActivityTypeMartialArts";
            break;
        case HKWorkoutActivityTypeMindAndBody:
            value = @"HKWorkoutActivityTypeMindAndBody";
            break;
        case HKWorkoutActivityTypeMixedMetabolicCardioTraining:
            value = @"HKWorkoutActivityTypeMixedMetabolicCardioTraining";
            break;
        case HKWorkoutActivityTypePaddleSports:
            value = @"HKWorkoutActivityTypePaddleSports";
            break;
        case HKWorkoutActivityTypePlay:
            value = @"HKWorkoutActivityTypePlay";
            break;
        case HKWorkoutActivityTypePreparationAndRecovery:
            value = @"HKWorkoutActivityTypePreparationAndRecovery";
            break;
        case HKWorkoutActivityTypeRacquetball:
            value = @"HKWorkoutActivityTypeRacquetball";
            break;
        case HKWorkoutActivityTypeRowing:
            value = @"HKWorkoutActivityTypeRowing";
            break;
        case HKWorkoutActivityTypeRugby:
            value = @"HKWorkoutActivityTypeRugby";
            break;
        case HKWorkoutActivityTypeRunning:
            value = @"HKWorkoutActivityTypeRunning";
            break;
        case HKWorkoutActivityTypeSailing:
            value = @"HKWorkoutActivityTypeSailing";
            break;
        case HKWorkoutActivityTypeSkatingSports:
            value = @"HKWorkoutActivityTypeSkatingSports";
            break;
        case HKWorkoutActivityTypeSnowSports:
            value = @"HKWorkoutActivityTypeSnowSports";
            break;
        case HKWorkoutActivityTypeSoccer:
            value = @"HKWorkoutActivityTypeSoccer";
            break;
        case HKWorkoutActivityTypeSoftball:
            value = @"HKWorkoutActivityTypeSoftball";
            break;
        case HKWorkoutActivityTypeSquash:
            value = @"HKWorkoutActivityTypeSquash";
            break;
        case HKWorkoutActivityTypeStairClimbing:
            value = @"HKWorkoutActivityTypeStairClimbing";
            break;
        case HKWorkoutActivityTypeSurfingSports:
            value = @"HKWorkoutActivityTypeSurfingSports";
            break;
        case HKWorkoutActivityTypeSwimming:
            value = @"HKWorkoutActivityTypeSwimming";
            break;
        case HKWorkoutActivityTypeTableTennis:
            value = @"HKWorkoutActivityTypeTableTennis";
            break;
        case HKWorkoutActivityTypeTennis:
            value = @"HKWorkoutActivityTypeTennis";
            break;
        case HKWorkoutActivityTypeTrackAndField:
            value = @"HKWorkoutActivityTypeTrackAndField";
            break;
        case HKWorkoutActivityTypeTraditionalStrengthTraining:
            value = @"HKWorkoutActivityTypeTraditionalStrengthTraining";
            break;
        case HKWorkoutActivityTypeVolleyball:
            value = @"HKWorkoutActivityTypeVolleyball";
            break;
        case HKWorkoutActivityTypeWalking:
            value = @"HKWorkoutActivityTypeWalking";
            break;
        case HKWorkoutActivityTypeWaterFitness:
            value = @"HKWorkoutActivityTypeWaterFitness";
            break;
        case HKWorkoutActivityTypeWaterPolo:
            value = @"HKWorkoutActivityTypeWaterPolo";
            break;
        case HKWorkoutActivityTypeWaterSports:
            value = @"HKWorkoutActivityTypeWaterSports";
            break;
        case HKWorkoutActivityTypeWrestling:
            value = @"HKWorkoutActivityTypeWrestling";
            break;
        case HKWorkoutActivityTypeYoga:
            value = @"HKWorkoutActivityTypeYoga";
            break;
        case HKWorkoutActivityTypeOther:
            value = @"HKWorkoutActivityTypeOther";
            break;
        case HKWorkoutActivityTypeBarre:
            value = @"HKWorkoutActivityTypeBarre";
            break;
        case HKWorkoutActivityTypeCoreTraining:
            value = @"HKWorkoutActivityTypeCoreTraining";
            break;
        case HKWorkoutActivityTypeCrossCountrySkiing:
            value = @"HKWorkoutActivityTypeCrossCountrySkiing";
            break;
        case HKWorkoutActivityTypeDownhillSkiing:
            value = @"HKWorkoutActivityTypeDownhillSkiing";
            break;
        case HKWorkoutActivityTypeFlexibility:
            value = @"HKWorkoutActivityTypeFlexibility";
            break;
        case HKWorkoutActivityTypeHighIntensityIntervalTraining:
            value = @"HKWorkoutActivityTypeHighIntensityIntervalTraining";
            break;
        case HKWorkoutActivityTypeJumpRope:
            value = @"HKWorkoutActivityTypeJumpRope";
            break;
        case HKWorkoutActivityTypeKickboxing:
            value = @"HKWorkoutActivityTypeKickboxing";
            break;
        case HKWorkoutActivityTypePilates:
            value = @"HKWorkoutActivityTypePilates";
            break;
        case HKWorkoutActivityTypeSnowboarding:
            value = @"HKWorkoutActivityTypeSnowboarding";
            break;
        case HKWorkoutActivityTypeStairs:
            value = @"HKWorkoutActivityTypeStairs";
            break;
        case HKWorkoutActivityTypeStepTraining:
            value = @"HKWorkoutActivityTypeStepTraining";
            break;
        case HKWorkoutActivityTypeWheelchairWalkPace:
            value = @"HKWorkoutActivityTypeWheelchairWalkPace";
            break;
        case HKWorkoutActivityTypeWheelchairRunPace:
            value = @"HKWorkoutActivityTypeWheelchairRunPace";
            break;
        default:
            value = @"HKWorkoutActivityTypeOther";
            break;
    }
    value = [value stringByReplacingOccurrencesOfString:@"HKWorkoutActivityType" withString:@""].uppercaseString;
    return value;
}

@end
