//
//  VITDateFormatterChineese.m
//  VitalityCore
//
//  Created by Discovery on 2014/11/05.
//  Copyright (c) 2014 Glucode. All rights reserved.
//

#import "VITDateFormatterChinese.h"

@implementation VITDateFormatterChinese

+ (NSDateFormatter *)vit_timeOnlyFormatter
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"YYYY年M月d日"];
    return formatter;
}

+ (NSDateFormatter *)vit_fullFormatter
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"YYYY年M月d日"];
    return formatter;
}

+ (NSDateFormatter *)vit_naturalDateFormatterWithDashes
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"YYYY年M月d日"];
    return formatter;
}

+ (NSDateFormatter *)vit_naturalDateFormatter
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"YYYY年M月d日"];
    return formatter;
}

@end
