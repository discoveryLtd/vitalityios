#import <Foundation/Foundation.h>

@class HKStatisticsCollection;

typedef NS_ENUM(NSUInteger, VITVitalityHeartRateCalculatorDataType) {
    VITVitalityHeartRateCalculatorDataTypeMinimum,
    VITVitalityHeartRateCalculatorDataTypeMaximum
};

@interface VITVitalityHeartRateCalculator : NSObject

+ (double)averageHeartRateWithStatisticsCollection:(HKStatisticsCollection *)statisticsCollection;
+ (double)maximumHeartRateWithStatisticsCollection:(HKStatisticsCollection *)statisticsCollection;
+ (double)minimumHeartRateWithStatisticsCollection:(HKStatisticsCollection *)statisticsCollection;
+ (double)heartRateQuantityDataOfType:(VITVitalityHeartRateCalculatorDataType)dateType withStatisticsCollection:(HKStatisticsCollection *)statisticsCollection;

@end
