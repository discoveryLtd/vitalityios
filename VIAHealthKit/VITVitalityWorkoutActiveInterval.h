#import <Foundation/Foundation.h>
#import <HealthKit/HealthKit.h>

@interface VITVitalityWorkoutActiveInterval : NSObject

@property (nonatomic, strong) NSDate *startDate;
@property (nonatomic, strong) NSDate *endDate;

+ (NSArray *)activeIntervalsForWorkout:(HKWorkout *)workout;

@end
