//
//  VIAHealthKit.h
//  VIAHealthKit
//
//  Created by Wilmar van Heerden on 2017/06/29.
//  Copyright © 2017 Glucode. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for VIAHealthKit.
FOUNDATION_EXPORT double VIAHealthKitVersionNumber;

//! Project version string for VIAHealthKit.
FOUNDATION_EXPORT const unsigned char VIAHealthKitVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <VIAHealthKit/PublicHeader.h>
#import <VIAHealthKit/VITHealthKitHelper.h>
#import <VIAHealthKit/VITHealthKitHelper+Notifications.h>
#import <VIAHealthKit/VITNotifications.h>
#import <VIAHealthKit/VITGDLReading.h>
#import <VIAHealthKit/VITDevice.h>
