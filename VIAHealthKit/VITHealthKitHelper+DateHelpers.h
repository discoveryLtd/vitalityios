#import "VITHealthKitHelper.h"

@interface VITHealthKitHelper (DateHelpers)

+ (NSDate *)now;
+ (NSDate *)twoWeeksAgo;
+ (NSDate *)todayAtTwelveMidnight;
+ (NSDate *)todayAtElevenFiftyNinePM;
+ (NSDate *)yesterday;
+ (NSDate *)dayBeforeYesterday;
+ (NSDate *)thisFridaySevenOClock;
+ (NSDate *)nextFridaySevenOClock;
+ (NSDate *)dateForFridaySevenOClock:(BOOL)thisFriday;

@end
